// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.async;

import com.fasterxml.aalto.util.TextBuilder;
import com.fasterxml.aalto.util.CharsetNames;
import com.fasterxml.aalto.impl.ErrorConsts;
import com.fasterxml.aalto.util.DataUtil;
import com.fasterxml.aalto.in.ReaderConfig;
import com.fasterxml.aalto.in.ByteBasedPNameTable;
import com.fasterxml.aalto.in.PName;
import com.fasterxml.aalto.util.XmlCharTypes;
import com.fasterxml.aalto.AsyncInputFeeder;
import com.fasterxml.aalto.in.ByteBasedScanner;

public abstract class AsyncByteScanner extends ByteBasedScanner implements AsyncInputFeeder
{
    protected static final int EVENT_INCOMPLETE = 257;
    protected static final int PENDING_STATE_ATTR_VALUE_AMP = -60;
    protected static final int PENDING_STATE_ATTR_VALUE_AMP_HASH = -61;
    protected static final int PENDING_STATE_ATTR_VALUE_AMP_HASH_X = -62;
    protected static final int PENDING_STATE_ATTR_VALUE_DEC_DIGIT = -64;
    protected static final int PENDING_STATE_ATTR_VALUE_ENTITY_NAME = -63;
    protected static final int PENDING_STATE_ATTR_VALUE_HEX_DIGIT = -65;
    protected static final int PENDING_STATE_CDATA_BRACKET1 = -30;
    protected static final int PENDING_STATE_CDATA_BRACKET2 = -31;
    protected static final int PENDING_STATE_COMMENT_HYPHEN1 = -20;
    protected static final int PENDING_STATE_COMMENT_HYPHEN2 = -21;
    protected static final int PENDING_STATE_CR = -1;
    protected static final int PENDING_STATE_ENT_IN_DEC_DIGIT = -72;
    protected static final int PENDING_STATE_ENT_IN_HEX_DIGIT = -73;
    protected static final int PENDING_STATE_ENT_SEEN_HASH = -70;
    protected static final int PENDING_STATE_ENT_SEEN_HASH_X = -71;
    protected static final int PENDING_STATE_PI_QMARK = -15;
    protected static final int PENDING_STATE_TEXT_AMP = -80;
    protected static final int PENDING_STATE_TEXT_AMP_HASH = -81;
    protected static final int PENDING_STATE_TEXT_BRACKET1 = -85;
    protected static final int PENDING_STATE_TEXT_BRACKET2 = -86;
    protected static final int PENDING_STATE_TEXT_DEC_ENTITY = -82;
    protected static final int PENDING_STATE_TEXT_HEX_ENTITY = -83;
    protected static final int PENDING_STATE_TEXT_IN_ENTITY = -84;
    protected static final int PENDING_STATE_XMLDECL_LT = -5;
    protected static final int PENDING_STATE_XMLDECL_LTQ = -6;
    protected static final int PENDING_STATE_XMLDECL_TARGET = -7;
    protected static final int STATE_CDATA_C = 2;
    protected static final int STATE_CDATA_CD = 3;
    protected static final int STATE_CDATA_CDA = 4;
    protected static final int STATE_CDATA_CDAT = 5;
    protected static final int STATE_CDATA_CDATA = 6;
    protected static final int STATE_CDATA_CONTENT = 1;
    protected static final int STATE_COMMENT_CONTENT = 1;
    protected static final int STATE_COMMENT_HYPHEN = 2;
    protected static final int STATE_COMMENT_HYPHEN2 = 3;
    protected static final int STATE_DEFAULT = 0;
    protected static final int STATE_DTD_AFTER_DOCTYPE = 2;
    protected static final int STATE_DTD_AFTER_PUBLIC = 8;
    protected static final int STATE_DTD_AFTER_PUBLIC_ID = 12;
    protected static final int STATE_DTD_AFTER_ROOT_NAME = 5;
    protected static final int STATE_DTD_AFTER_SYSTEM = 9;
    protected static final int STATE_DTD_AFTER_SYSTEM_ID = 15;
    protected static final int STATE_DTD_BEFORE_IDS = 6;
    protected static final int STATE_DTD_BEFORE_PUBLIC_ID = 10;
    protected static final int STATE_DTD_BEFORE_ROOT_NAME = 3;
    protected static final int STATE_DTD_BEFORE_SYSTEM_ID = 13;
    protected static final int STATE_DTD_DOCTYPE = 1;
    protected static final int STATE_DTD_EXPECT_CLOSING_GT = 50;
    protected static final int STATE_DTD_INT_SUBSET = 16;
    protected static final int STATE_DTD_PUBLIC_ID = 11;
    protected static final int STATE_DTD_PUBLIC_OR_SYSTEM = 7;
    protected static final int STATE_DTD_ROOT_NAME = 4;
    protected static final int STATE_DTD_SYSTEM_ID = 14;
    protected static final int STATE_EE_NEED_GT = 1;
    protected static final int STATE_PI_AFTER_TARGET = 1;
    protected static final int STATE_PI_AFTER_TARGET_QMARK = 3;
    protected static final int STATE_PI_AFTER_TARGET_WS = 2;
    protected static final int STATE_PI_IN_DATA = 5;
    protected static final int STATE_PI_IN_TARGET = 4;
    protected static final int STATE_PROLOG_DECL = 3;
    protected static final int STATE_PROLOG_INITIAL = 1;
    protected static final int STATE_PROLOG_SEEN_LT = 2;
    protected static final int STATE_SE_ATTR_NAME = 4;
    protected static final int STATE_SE_ATTR_VALUE_NORMAL = 7;
    protected static final int STATE_SE_ATTR_VALUE_NSDECL = 8;
    protected static final int STATE_SE_ELEM_NAME = 1;
    protected static final int STATE_SE_SEEN_SLASH = 9;
    protected static final int STATE_SE_SPACE_OR_ATTRNAME = 3;
    protected static final int STATE_SE_SPACE_OR_ATTRVALUE = 6;
    protected static final int STATE_SE_SPACE_OR_END = 2;
    protected static final int STATE_SE_SPACE_OR_EQ = 5;
    protected static final int STATE_TEXT_AMP = 4;
    protected static final int STATE_TEXT_AMP_NAME = 6;
    protected static final int STATE_TREE_NAMED_ENTITY_START = 6;
    protected static final int STATE_TREE_NUMERIC_ENTITY_START = 5;
    protected static final int STATE_TREE_SEEN_AMP = 2;
    protected static final int STATE_TREE_SEEN_EXCL = 3;
    protected static final int STATE_TREE_SEEN_LT = 1;
    protected static final int STATE_TREE_SEEN_SLASH = 4;
    protected static final int STATE_XMLDECL_AFTER_ENCODING = 10;
    protected static final int STATE_XMLDECL_AFTER_ENCODING_VALUE = 13;
    protected static final int STATE_XMLDECL_AFTER_STANDALONE = 16;
    protected static final int STATE_XMLDECL_AFTER_STANDALONE_VALUE = 19;
    protected static final int STATE_XMLDECL_AFTER_VERSION = 4;
    protected static final int STATE_XMLDECL_AFTER_VERSION_VALUE = 7;
    protected static final int STATE_XMLDECL_AFTER_XML = 1;
    protected static final int STATE_XMLDECL_BEFORE_ENCODING = 8;
    protected static final int STATE_XMLDECL_BEFORE_STANDALONE = 14;
    protected static final int STATE_XMLDECL_BEFORE_VERSION = 2;
    protected static final int STATE_XMLDECL_ENCODING = 9;
    protected static final int STATE_XMLDECL_ENCODING_EQ = 11;
    protected static final int STATE_XMLDECL_ENCODING_VALUE = 12;
    protected static final int STATE_XMLDECL_ENDQ = 20;
    protected static final int STATE_XMLDECL_STANDALONE = 15;
    protected static final int STATE_XMLDECL_STANDALONE_EQ = 17;
    protected static final int STATE_XMLDECL_STANDALONE_VALUE = 18;
    protected static final int STATE_XMLDECL_VERSION = 3;
    protected static final int STATE_XMLDECL_VERSION_EQ = 5;
    protected static final int STATE_XMLDECL_VERSION_VALUE = 6;
    protected XmlCharTypes _charTypes;
    protected int _currQuad;
    protected int _currQuadBytes;
    protected boolean _elemAllNsBound;
    protected boolean _elemAttrCount;
    protected PName _elemAttrName;
    protected int _elemAttrPtr;
    protected byte _elemAttrQuote;
    protected int _elemNsPtr;
    protected boolean _endOfInput;
    protected int _entityValue;
    protected boolean _inDtdDeclaration;
    protected int _nextEvent;
    protected int _pendingInput;
    protected int[] _quadBuffer;
    protected int _quadCount;
    protected int _state;
    protected int _surroundingEvent;
    protected ByteBasedPNameTable _symbols;
    
    public AsyncByteScanner(final ReaderConfig readerConfig) {
        super(readerConfig);
        this._quadBuffer = new int[32];
        this._nextEvent = 257;
        this._surroundingEvent = 257;
        this._pendingInput = 0;
        this._endOfInput = false;
        this._currQuadBytes = 0;
        this._entityValue = 0;
    }
    
    private final PName _parseNewXmlDeclName(final byte b) {
        final int currQuad = b & 0xFF;
        if (currQuad < 65) {
            this.throwUnexpectedChar(currQuad, "; expected a name start character");
        }
        this._quadCount = 0;
        this._currQuad = currQuad;
        this._currQuadBytes = 1;
        return this._parseXmlDeclName();
    }
    
    private final PName _parseXmlDeclName() {
        int currQuad = this._currQuad;
        while (true) {
            final int currQuadBytes = this._currQuadBytes;
            Label_0305: {
                int currQuad4 = 0;
                Label_0235: {
                    int currQuad3 = 0;
                    Label_0165: {
                        int currQuad2;
                        if (currQuadBytes != 0) {
                            currQuad2 = currQuad;
                            if (currQuadBytes != 1) {
                                currQuad3 = currQuad;
                                if (currQuadBytes == 2) {
                                    break Label_0165;
                                }
                                currQuad4 = currQuad;
                                if (currQuadBytes != 3) {
                                    break Label_0305;
                                }
                                break Label_0235;
                            }
                        }
                        else {
                            if (super._inputPtr >= super._inputEnd) {
                                return null;
                            }
                            final int n = currQuad2 = (this._nextByte() & 0xFF);
                            if (n < 65 && (n < 45 || n > 58 || (currQuad2 = n) == 47)) {
                                return this._findXmlDeclName(n, 0);
                            }
                        }
                        if (super._inputPtr >= super._inputEnd) {
                            this._currQuad = currQuad2;
                            this._currQuadBytes = 1;
                            return null;
                        }
                        final int n2 = this._nextByte() & 0xFF;
                        if (n2 < 65 && (n2 < 45 || n2 > 58 || n2 == 47)) {
                            return this._findXmlDeclName(currQuad2, 1);
                        }
                        currQuad3 = (currQuad2 << 8 | n2);
                    }
                    if (super._inputPtr >= super._inputEnd) {
                        this._currQuad = currQuad3;
                        this._currQuadBytes = 2;
                        return null;
                    }
                    final int n3 = this._nextByte() & 0xFF;
                    if (n3 < 65 && (n3 < 45 || n3 > 58 || n3 == 47)) {
                        return this._findXmlDeclName(currQuad3, 2);
                    }
                    currQuad4 = (currQuad3 << 8 | n3);
                }
                if (super._inputPtr >= super._inputEnd) {
                    this._currQuad = currQuad4;
                    this._currQuadBytes = 3;
                    return null;
                }
                final int n4 = this._nextByte() & 0xFF;
                if (n4 < 65 && (n4 < 45 || n4 > 58 || n4 == 47)) {
                    return this._findXmlDeclName(currQuad4, 3);
                }
                currQuad = (currQuad4 << 8 | n4);
            }
            final int quadCount = this._quadCount;
            if (quadCount == 0) {
                this._quadBuffer[0] = currQuad;
                this._quadCount = 1;
            }
            else {
                final int[] quadBuffer = this._quadBuffer;
                if (quadCount >= quadBuffer.length) {
                    this._quadBuffer = DataUtil.growArrayBy(quadBuffer, quadBuffer.length);
                }
                this._quadBuffer[this._quadCount++] = currQuad;
            }
            this._currQuadBytes = 0;
        }
    }
    
    private int handleDTD() {
        if (this._pendingInput == -1 && !this.handlePartialCR()) {
            return 257;
        }
        if (this._state == 16) {
            if (!this.handleDTDInternalSubset(false)) {
                return 257;
            }
            this._state = 50;
        }
    Label_0732_Outer:
        while (super._inputPtr < super._inputEnd) {
            final int state = this._state;
            Label_0230: {
                Label_1219: {
                    if (state != 50) {
                        int state2 = 0;
                        Label_1211: {
                            String str = null;
                            StringBuilder sb = null;
                        Label_0790:
                            while (true) {
                                Label_0775: {
                                    Label_0758: {
                                        Label_0729: {
                                            final byte nextByte2;
                                            int n = 0;
                                            String s = null;
                                            final byte nextByte7;
                                            final byte nextByte9;
                                            switch (state) {
                                                default: {
                                                    this.throwInternal();
                                                    continue Label_0732_Outer;
                                                }
                                                case 14: {
                                                    if (!this.parseDtdId(super._textBuilder.getBufferWithoutReset(), super._textBuilder.getCurrentLength(), true)) {
                                                        continue Label_0732_Outer;
                                                    }
                                                    this.verifyAndSetSystemId();
                                                    this._state = 15;
                                                    if (super._inputPtr >= super._inputEnd) {
                                                        continue Label_0732_Outer;
                                                    }
                                                }
                                                case 15: {
                                                    if (!this.asyncSkipSpace()) {
                                                        continue Label_0732_Outer;
                                                    }
                                                    final byte nextByte = this._nextByte();
                                                    if (nextByte == 62) {
                                                        break Label_0230;
                                                    }
                                                    if (nextByte != 91) {
                                                        this.reportPrologUnexpChar(true, this.decodeCharForError(this._elemAttrQuote), " (expected either '[' for internal subset, or '>' to end DOCTYPE)");
                                                    }
                                                    this._state = 16;
                                                    if (this.handleDTDInternalSubset(true)) {
                                                        this._state = 50;
                                                        break Label_1219;
                                                    }
                                                    return 257;
                                                }
                                                case 11: {
                                                    if (!this.parseDtdId(super._textBuilder.getBufferWithoutReset(), super._textBuilder.getCurrentLength(), false)) {
                                                        continue Label_0732_Outer;
                                                    }
                                                    this.verifyAndSetPublicId();
                                                    this._state = 12;
                                                    if (super._inputPtr >= super._inputEnd) {
                                                        continue Label_0732_Outer;
                                                    }
                                                }
                                                case 12:
                                                    Label_0397: {
                                                        nextByte2 = this._nextByte();
                                                        if (nextByte2 != 32 && nextByte2 != 13 && nextByte2 != 10 && nextByte2 != 9) {
                                                            this.reportPrologUnexpChar(true, this.decodeCharForError(nextByte2), " (expected space after PUBLIC ID)");
                                                            break Label_0397;
                                                        }
                                                        this._state = 13;
                                                        break Label_0397;
                                                    }
                                                case 13: {
                                                    if (!this.asyncSkipSpace()) {
                                                        continue Label_0732_Outer;
                                                    }
                                                    final byte nextByte3 = this._nextByte();
                                                    this._elemAttrQuote = nextByte3;
                                                    if (nextByte3 != 34 && nextByte3 != 39) {
                                                        this.reportPrologUnexpChar(true, this.decodeCharForError(nextByte3), " (expected '\"' or ''' for SYSTEM ID)");
                                                    }
                                                    final char[] resetWithEmpty = super._textBuilder.resetWithEmpty();
                                                    if (super._inputPtr < super._inputEnd && this.parseDtdId(resetWithEmpty, 0, true)) {
                                                        this.verifyAndSetSystemId();
                                                        this._state = 15;
                                                        continue Label_0732_Outer;
                                                    }
                                                    state2 = 14;
                                                    break Label_1211;
                                                }
                                                case 10: {
                                                    if (!this.asyncSkipSpace()) {
                                                        continue Label_0732_Outer;
                                                    }
                                                    final byte nextByte4 = this._nextByte();
                                                    this._elemAttrQuote = nextByte4;
                                                    if (nextByte4 != 34 && nextByte4 != 39) {
                                                        this.reportPrologUnexpChar(true, this.decodeCharForError(nextByte4), " (expected '\"' or ''' for PUBLIC ID)");
                                                    }
                                                    final char[] resetWithEmpty2 = super._textBuilder.resetWithEmpty();
                                                    if (super._inputPtr < super._inputEnd && this.parseDtdId(resetWithEmpty2, 0, false)) {
                                                        this.verifyAndSetPublicId();
                                                        this._state = 12;
                                                        continue Label_0732_Outer;
                                                    }
                                                    this._state = 11;
                                                    continue Label_0732_Outer;
                                                }
                                                case 9: {
                                                    final byte nextByte5 = this._nextByte();
                                                    if (nextByte5 != 32 && nextByte5 != 13 && nextByte5 != 10 && nextByte5 != 9) {
                                                        n = this.decodeCharForError(nextByte5);
                                                        s = " (expected space after SYSTEM keyword)";
                                                        break;
                                                    }
                                                    this._state = 13;
                                                    continue Label_0732_Outer;
                                                }
                                                case 8: {
                                                    final byte nextByte6 = this._nextByte();
                                                    if (nextByte6 == 32 || nextByte6 == 13) {
                                                        final int state3 = 10;
                                                        break Label_0732;
                                                    }
                                                    int state3 = 10;
                                                    if (nextByte6 == 10) {
                                                        break Label_0732;
                                                    }
                                                    if (nextByte6 == 9) {
                                                        state3 = state3;
                                                        break Label_0732;
                                                    }
                                                    n = this.decodeCharForError(nextByte6);
                                                    s = " (expected space after PUBLIC keyword)";
                                                    break;
                                                }
                                                case 7: {
                                                    final PName pName = this.parsePName();
                                                    if (pName == null) {
                                                        break Label_0729;
                                                    }
                                                    str = pName.getPrefixedName();
                                                    if ("PUBLIC".equals(str)) {
                                                        break Label_0758;
                                                    }
                                                    if ("SYSTEM".equals(str)) {
                                                        break Label_0775;
                                                    }
                                                    sb = new StringBuilder();
                                                    break Label_0790;
                                                }
                                                case 4: {
                                                    final PName pName2 = this.parsePName();
                                                    super._tokenName = pName2;
                                                    if (pName2 == null) {
                                                        continue Label_0732_Outer;
                                                    }
                                                    this._state = 5;
                                                    if (super._inputPtr >= super._inputEnd) {
                                                        continue Label_0732_Outer;
                                                    }
                                                }
                                                case 5:
                                                    Label_0931: {
                                                        nextByte7 = this._nextByte();
                                                        if (nextByte7 == 62) {
                                                            break Label_0230;
                                                        }
                                                        if (nextByte7 != 32 && nextByte7 != 13 && nextByte7 != 10 && nextByte7 != 9) {
                                                            this.reportPrologUnexpChar(true, this.decodeCharForError(nextByte7), " (expected space after root name in DOCTYPE declaration)");
                                                            break Label_0931;
                                                        }
                                                        this._state = 6;
                                                        break Label_0931;
                                                    }
                                                case 6: {
                                                    if (!this.asyncSkipSpace()) {
                                                        continue Label_0732_Outer;
                                                    }
                                                    final byte nextByte8 = this._nextByte();
                                                    if (nextByte8 == 62) {
                                                        break Label_0230;
                                                    }
                                                    final PName newName = this.parseNewName(nextByte8);
                                                    if (newName == null) {
                                                        break Label_0729;
                                                    }
                                                    str = newName.getPrefixedName();
                                                    if ("PUBLIC".equals(str)) {
                                                        break Label_0758;
                                                    }
                                                    if ("SYSTEM".equals(str)) {
                                                        break Label_0775;
                                                    }
                                                    sb = new StringBuilder();
                                                    break Label_0790;
                                                }
                                                case 1: {
                                                    final PName pName3 = this.parsePName();
                                                    super._tokenName = pName3;
                                                    if (pName3 == null) {
                                                        this._state = 1;
                                                        return 257;
                                                    }
                                                    if (!"DOCTYPE".equals(pName3.getPrefixedName())) {
                                                        this.reportPrologProblem(true, "expected 'DOCTYPE'");
                                                    }
                                                    if (super._inputPtr >= super._inputEnd) {
                                                        continue Label_0732_Outer;
                                                    }
                                                }
                                                case 2:
                                                    Label_1132: {
                                                        nextByte9 = this._nextByte();
                                                        if (nextByte9 != 32 && nextByte9 != 13 && nextByte9 != 10 && nextByte9 != 9) {
                                                            this.reportPrologUnexpChar(true, this.decodeCharForError(nextByte9), " (expected space after 'DOCTYPE')");
                                                            break Label_1132;
                                                        }
                                                        this._state = 3;
                                                        break Label_1132;
                                                    }
                                                case 3: {
                                                    if (!this.asyncSkipSpace()) {
                                                        continue Label_0732_Outer;
                                                    }
                                                    super._tokenName = this.parseNewName(this._nextByte());
                                                    final int state3 = 4;
                                                    break Label_0732;
                                                }
                                                case 0: {
                                                    final PName newName2 = this.parseNewName((byte)68);
                                                    super._tokenName = newName2;
                                                    if (newName2 == null) {
                                                        this._state = 1;
                                                        return 257;
                                                    }
                                                    if (!"DOCTYPE".equals(newName2.getPrefixedName())) {
                                                        this.reportPrologProblem(true, "expected 'DOCTYPE'");
                                                    }
                                                    state2 = 2;
                                                    break Label_1211;
                                                }
                                            }
                                            this.reportPrologUnexpChar(true, n, s);
                                            continue Label_0732_Outer;
                                        }
                                        int state3 = 7;
                                        this._state = state3;
                                        continue Label_0732_Outer;
                                    }
                                    int state3 = 8;
                                    continue;
                                }
                                int state3 = 9;
                                continue;
                            }
                            sb.append("unexpected token '");
                            sb.append(str);
                            sb.append("': expected either PUBLIC or SYSTEM");
                            this.reportPrologProblem(true, sb.toString());
                            continue Label_0732_Outer;
                        }
                        this._state = state2;
                        continue;
                    }
                }
                if (!this.asyncSkipSpace()) {
                    continue;
                }
                final byte nextByte10 = this._nextByte();
                if (nextByte10 != 62) {
                    this.reportPrologUnexpChar(true, nextByte10, "expected '>' to end DTD");
                }
            }
            this._state = 0;
            this._nextEvent = 257;
            return 11;
        }
        return super._currToken;
    }
    
    private final int handlePrologDeclStart(final boolean b) {
        if (super._inputPtr >= super._inputEnd) {
            return 257;
        }
        final byte nextByte = this._nextByte();
        if (nextByte == 45) {
            this._nextEvent = 5;
            this._state = 0;
            return this.handleComment();
        }
        if (nextByte == 68) {
            this._nextEvent = 11;
            this._state = 0;
            return this.handleDTD();
        }
        this.reportPrologUnexpChar(b, this.decodeCharForError(nextByte), " (expected '-' for COMMENT)");
        return 257;
    }
    
    private int handleXmlDeclaration() {
        if (this._pendingInput == -1 && !this.handlePartialCR()) {
            return 257;
        }
    Label_0637_Outer:
        while (super._inputPtr < super._inputEnd) {
            final int state = this._state;
            final int n = 18;
            Label_1088: {
                int state3 = 0;
                Label_1045: {
                    Label_1042: {
                    Label_0801:
                        while (true) {
                            Label_0560: {
                                Label_0530: {
                                    int state2 = 0;
                                    switch (state) {
                                        default: {
                                            this.throwInternal();
                                            continue Label_0637_Outer;
                                        }
                                        case 18: {
                                            if (!this.parseXmlDeclAttr(super._textBuilder.getBufferWithoutReset(), super._textBuilder.getCurrentLength())) {
                                                state2 = n;
                                                break;
                                            }
                                            this.verifyAndSetXmlStandalone();
                                            this._state = 19;
                                            if (super._inputPtr >= super._inputEnd) {
                                                continue Label_0637_Outer;
                                            }
                                        }
                                        case 19: {
                                            if (!this.asyncSkipSpace()) {
                                                continue Label_0637_Outer;
                                            }
                                            if (this._nextByte() != 63) {
                                                this.reportPrologUnexpChar(true, this.decodeCharForError(this._prevByte()), " (expected '?>' to end xml declaration)");
                                            }
                                            this._state = 20;
                                            if (super._inputPtr >= super._inputEnd) {
                                                continue Label_0637_Outer;
                                            }
                                        }
                                        case 20: {
                                            super._tokenName = null;
                                            this._state = 0;
                                            this._nextEvent = 257;
                                            if (this._nextByte() != 62) {
                                                this.reportPrologUnexpChar(true, this.decodeCharForError(this._prevByte()), " (expected '>' to end xml declaration)");
                                            }
                                            this._activateEncoding();
                                            return 7;
                                        }
                                        case 15: {
                                            final PName parseXmlDeclName = this._parseXmlDeclName();
                                            super._tokenName = parseXmlDeclName;
                                            if (parseXmlDeclName == null) {
                                                continue Label_0637_Outer;
                                            }
                                            if (!parseXmlDeclName.hasPrefixedName("standalone")) {
                                                this.reportInputProblem("Unexpected keyword 'encoding' in XML declaration: expected 'standalone'");
                                            }
                                            this._state = 16;
                                            if (super._inputPtr >= super._inputEnd) {
                                                continue Label_0637_Outer;
                                            }
                                        }
                                        case 16: {
                                            if (!this.asyncSkipSpace()) {
                                                continue Label_0637_Outer;
                                            }
                                            final byte nextByte = this._nextByte();
                                            if (nextByte != 61) {
                                                this.reportPrologUnexpChar(true, this.decodeCharForError(nextByte), " (expected '=' after 'standalone' in xml declaration)");
                                            }
                                            this._state = 17;
                                            if (super._inputPtr >= super._inputEnd) {
                                                continue Label_0637_Outer;
                                            }
                                        }
                                        case 17: {
                                            if (!this.asyncSkipSpace()) {
                                                continue Label_0637_Outer;
                                            }
                                            final byte nextByte2 = this._nextByte();
                                            this._elemAttrQuote = nextByte2;
                                            if (nextByte2 != 34 && nextByte2 != 39) {
                                                this.reportPrologUnexpChar(true, this.decodeCharForError(nextByte2), " (expected '\"' or ''' in xml declaration for standalone value)");
                                            }
                                            final char[] resetWithEmpty = super._textBuilder.resetWithEmpty();
                                            state2 = n;
                                            if (super._inputPtr >= super._inputEnd) {
                                                break;
                                            }
                                            if (!this.parseXmlDeclAttr(resetWithEmpty, 0)) {
                                                state2 = n;
                                                break;
                                            }
                                            this.verifyAndSetXmlStandalone();
                                            this._state = 19;
                                            continue Label_0637_Outer;
                                        }
                                        case 13: {
                                            break Label_0560;
                                        }
                                        case 12: {
                                            if (!this.parseXmlDeclAttr(super._textBuilder.getBufferWithoutReset(), super._textBuilder.getCurrentLength())) {
                                                break Label_0530;
                                            }
                                            this.verifyAndSetXmlEncoding();
                                            this._state = 13;
                                            if (super._inputPtr >= super._inputEnd) {
                                                continue Label_0637_Outer;
                                            }
                                            break Label_0560;
                                        }
                                        case 14: {
                                            if (!this.asyncSkipSpace()) {
                                                continue Label_0637_Outer;
                                            }
                                            final byte nextByte3 = this._nextByte();
                                            if (nextByte3 == 63) {
                                                break Label_1088;
                                            }
                                            final PName parseNewXmlDeclName = this._parseNewXmlDeclName(nextByte3);
                                            if ((super._tokenName = parseNewXmlDeclName) == null) {
                                                state3 = 15;
                                                break Label_1045;
                                            }
                                            if (!parseNewXmlDeclName.hasPrefixedName("standalone")) {
                                                final StringBuilder sb = new StringBuilder();
                                                sb.append("Unexpected keyword '");
                                                sb.append(super._tokenName.getPrefixedName());
                                                sb.append("' in XML declaration: expected 'standalone'");
                                                this.reportInputProblem(sb.toString());
                                            }
                                            break Label_0801;
                                        }
                                        case 9: {
                                            final PName parseXmlDeclName2 = this._parseXmlDeclName();
                                            super._tokenName = parseXmlDeclName2;
                                            if (parseXmlDeclName2 == null) {
                                                continue Label_0637_Outer;
                                            }
                                            if (parseXmlDeclName2.hasPrefixedName("encoding")) {
                                                this._state = 10;
                                            }
                                            else {
                                                if (super._tokenName.hasPrefixedName("standalone")) {
                                                    break Label_0801;
                                                }
                                                final StringBuilder sb2 = new StringBuilder();
                                                sb2.append("Unexpected keyword '");
                                                sb2.append(super._tokenName.getPrefixedName());
                                                sb2.append("' in XML declaration: expected 'encoding'");
                                                this.reportInputProblem(sb2.toString());
                                            }
                                            if (super._inputPtr >= super._inputEnd) {
                                                continue Label_0637_Outer;
                                            }
                                        }
                                        case 10: {
                                            if (!this.asyncSkipSpace()) {
                                                continue Label_0637_Outer;
                                            }
                                            final byte nextByte4 = this._nextByte();
                                            if (nextByte4 != 61) {
                                                this.reportPrologUnexpChar(true, this.decodeCharForError(nextByte4), " (expected '=' after 'encoding' in xml declaration)");
                                            }
                                            this._state = 11;
                                            if (super._inputPtr >= super._inputEnd) {
                                                continue Label_0637_Outer;
                                            }
                                        }
                                        case 11: {
                                            if (!this.asyncSkipSpace()) {
                                                continue Label_0637_Outer;
                                            }
                                            final byte nextByte5 = this._nextByte();
                                            this._elemAttrQuote = nextByte5;
                                            if (nextByte5 != 34 && nextByte5 != 39) {
                                                this.reportPrologUnexpChar(true, this.decodeCharForError(nextByte5), " (expected '\"' or ''' in xml declaration for encoding value)");
                                            }
                                            this._state = 12;
                                            final char[] resetWithEmpty2 = super._textBuilder.resetWithEmpty();
                                            if (super._inputPtr >= super._inputEnd) {
                                                break Label_0530;
                                            }
                                            if (!this.parseXmlDeclAttr(resetWithEmpty2, 0)) {
                                                break Label_0530;
                                            }
                                            this.verifyAndSetXmlEncoding();
                                            state3 = 13;
                                            break Label_1045;
                                        }
                                        case 6: {
                                            if (!this.parseXmlDeclAttr(super._textBuilder.getBufferWithoutReset(), super._textBuilder.getCurrentLength())) {
                                                break Label_1042;
                                            }
                                            this.verifyAndSetXmlVersion();
                                            this._state = 7;
                                            if (super._inputPtr >= super._inputEnd) {
                                                continue Label_0637_Outer;
                                            }
                                        }
                                        case 7: {
                                            final byte nextByte6 = this._nextByte();
                                            if (nextByte6 == 63) {
                                                break Label_1088;
                                            }
                                            if (nextByte6 != 32 && nextByte6 != 13 && nextByte6 != 10 && nextByte6 != 9) {
                                                this.reportPrologUnexpChar(true, this.decodeCharForError(nextByte6), " (expected space after version value in xml declaration)");
                                            }
                                            else {
                                                this._state = 8;
                                            }
                                            if (super._inputPtr >= super._inputEnd) {
                                                continue Label_0637_Outer;
                                            }
                                        }
                                        case 8: {
                                            if (!this.asyncSkipSpace()) {
                                                continue Label_0637_Outer;
                                            }
                                            final byte nextByte7 = this._nextByte();
                                            if (nextByte7 == 63) {
                                                break Label_1088;
                                            }
                                            final PName parseNewXmlDeclName2 = this._parseNewXmlDeclName(nextByte7);
                                            if ((super._tokenName = parseNewXmlDeclName2) == null) {
                                                state2 = 9;
                                                break;
                                            }
                                            if (parseNewXmlDeclName2.hasPrefixedName("encoding")) {
                                                state3 = 10;
                                                break Label_1045;
                                            }
                                            if (super._tokenName.hasPrefixedName("standalone")) {
                                                break Label_0801;
                                            }
                                            final StringBuilder sb3 = new StringBuilder();
                                            sb3.append("Unexpected keyword '");
                                            sb3.append(super._tokenName.getPrefixedName());
                                            sb3.append("' in XML declaration: expected 'encoding'");
                                            this.reportInputProblem(sb3.toString());
                                            continue Label_0637_Outer;
                                        }
                                        case 3: {
                                            final PName parseXmlDeclName3 = this._parseXmlDeclName();
                                            super._tokenName = parseXmlDeclName3;
                                            if (parseXmlDeclName3 == null) {
                                                continue Label_0637_Outer;
                                            }
                                            if (!parseXmlDeclName3.hasPrefixedName("version")) {
                                                final StringBuilder sb4 = new StringBuilder();
                                                sb4.append("Unexpected keyword '");
                                                sb4.append(super._tokenName.getPrefixedName());
                                                sb4.append("' in XML declaration: expected 'version'");
                                                this.reportInputProblem(sb4.toString());
                                            }
                                            this._state = 4;
                                            if (super._inputPtr >= super._inputEnd) {
                                                continue Label_0637_Outer;
                                            }
                                        }
                                        case 4: {
                                            if (!this.asyncSkipSpace()) {
                                                continue Label_0637_Outer;
                                            }
                                            final byte nextByte8 = this._nextByte();
                                            if (nextByte8 != 61) {
                                                this.reportPrologUnexpChar(true, this.decodeCharForError(nextByte8), " (expected '=' after 'version' in xml declaration)");
                                            }
                                            this._state = 5;
                                            if (super._inputPtr >= super._inputEnd) {
                                                continue Label_0637_Outer;
                                            }
                                        }
                                        case 5: {
                                            if (!this.asyncSkipSpace()) {
                                                continue Label_0637_Outer;
                                            }
                                            final byte nextByte9 = this._nextByte();
                                            this._elemAttrQuote = nextByte9;
                                            if (nextByte9 != 34 && nextByte9 != 39) {
                                                this.reportPrologUnexpChar(true, this.decodeCharForError(nextByte9), " (expected '\"' or ''' in xml declaration for version value)");
                                            }
                                            final char[] resetWithEmpty3 = super._textBuilder.resetWithEmpty();
                                            if (super._inputPtr >= super._inputEnd) {
                                                break Label_1042;
                                            }
                                            if (!this.parseXmlDeclAttr(resetWithEmpty3, 0)) {
                                                break Label_1042;
                                            }
                                            this.verifyAndSetXmlVersion();
                                            this._state = 7;
                                            continue Label_0637_Outer;
                                        }
                                        case 1: {
                                            final byte nextByte10 = this._nextByte();
                                            if (nextByte10 != 32 && nextByte10 != 13 && nextByte10 != 10 && nextByte10 != 9) {
                                                this.reportPrologUnexpChar(true, this.decodeCharForError(nextByte10), " (expected space after 'xml' in xml declaration)");
                                            }
                                            else {
                                                this._state = 2;
                                            }
                                            if (super._inputPtr >= super._inputEnd) {
                                                continue Label_0637_Outer;
                                            }
                                        }
                                        case 2: {
                                            if (!this.asyncSkipSpace()) {
                                                continue Label_0637_Outer;
                                            }
                                            final PName parseNewXmlDeclName3 = this._parseNewXmlDeclName(this._nextByte());
                                            if ((super._tokenName = parseNewXmlDeclName3) == null) {
                                                state3 = 3;
                                                break Label_1045;
                                            }
                                            if (!parseNewXmlDeclName3.hasPrefixedName("version")) {
                                                final StringBuilder sb5 = new StringBuilder();
                                                sb5.append("Unexpected keyword '");
                                                sb5.append(super._tokenName.getPrefixedName());
                                                sb5.append("' in XML declaration: expected 'version'");
                                                this.reportInputProblem(sb5.toString());
                                            }
                                            state3 = 4;
                                            break Label_1045;
                                        }
                                    }
                                    this._state = state2;
                                    continue Label_0637_Outer;
                                }
                                state3 = 12;
                                break Label_1045;
                            }
                            final byte nextByte11 = this._nextByte();
                            if (nextByte11 == 63) {
                                break Label_1088;
                            }
                            if (nextByte11 != 32 && nextByte11 != 13 && nextByte11 != 10 && nextByte11 != 9) {
                                this.reportPrologUnexpChar(true, this.decodeCharForError(nextByte11), " (expected space after encoding value in xml declaration)");
                            }
                            else {
                                this._state = 14;
                            }
                            if (super._inputPtr >= super._inputEnd) {
                                continue Label_0637_Outer;
                            }
                            continue;
                        }
                        this._state = 16;
                        continue Label_0637_Outer;
                    }
                    state3 = 6;
                }
                this._state = state3;
                continue Label_0637_Outer;
            }
            this._state = 20;
        }
        return 257;
    }
    
    private final boolean parseDtdId(char[] array, int n, final boolean b) {
        final byte elemAttrQuote = this._elemAttrQuote;
        while (super._inputPtr < super._inputEnd) {
            final int n2 = this._nextByte() & 0xFF;
            if (n2 == elemAttrQuote) {
                super._textBuilder.setCurrentLength(n);
                return true;
            }
            if (!b && !this.validPublicIdChar(n2)) {
                final int decodeCharForError = this.decodeCharForError((byte)n2);
                final StringBuilder sb = new StringBuilder();
                sb.append(" (not valid in ");
                String str;
                if (b) {
                    str = "SYSTEM";
                }
                else {
                    str = "PUBLIC";
                }
                sb.append(str);
                sb.append(" ID)");
                this.reportPrologUnexpChar(true, decodeCharForError, sb.toString());
            }
            char[] finishCurrentSegment = array;
            int n3;
            if ((n3 = n) >= array.length) {
                finishCurrentSegment = super._textBuilder.finishCurrentSegment();
                n3 = 0;
            }
            finishCurrentSegment[n3] = (char)n2;
            n = n3 + 1;
            array = finishCurrentSegment;
        }
        super._textBuilder.setCurrentLength(n);
        return false;
    }
    
    private final Boolean startXmlDeclaration() {
        if (super._inputPtr >= super._inputEnd) {
            return null;
        }
        if (this._pendingInput == -5) {
            if (this._currentByte() != 63) {
                this._pendingInput = 0;
                this._state = 2;
                return Boolean.FALSE;
            }
            final int inputPtr = super._inputPtr + 1;
            super._inputPtr = inputPtr;
            this._pendingInput = -6;
            if (inputPtr >= super._inputEnd) {
                return null;
            }
        }
        final int pendingInput = this._pendingInput;
        if (pendingInput == -6) {
            final PName parseNewXmlDeclName = this._parseNewXmlDeclName(this._nextByte());
            if ((super._tokenName = parseNewXmlDeclName) == null) {
                this._pendingInput = -7;
                return null;
            }
            if (!"xml".equals(parseNewXmlDeclName.getPrefixedName())) {
                this._pendingInput = 0;
                this._state = 1;
                this._nextEvent = 3;
                this.checkPITargetName(super._tokenName);
                return Boolean.FALSE;
            }
        }
        else if (pendingInput == -7) {
            final PName parseXmlDeclName = this._parseXmlDeclName();
            if ((super._tokenName = parseXmlDeclName) == null) {
                return null;
            }
            if (!"xml".equals(parseXmlDeclName.getPrefixedName())) {
                this._pendingInput = 0;
                this._state = 1;
                this._nextEvent = 3;
                this.checkPITargetName(super._tokenName);
                return Boolean.FALSE;
            }
        }
        else {
            this.throwInternal();
        }
        this._pendingInput = 0;
        this._nextEvent = 7;
        this._state = 1;
        return Boolean.TRUE;
    }
    
    public void _activateEncoding() {
        if (this._symbols == null) {
            this._charTypes = super._config.getCharTypes();
            this._symbols = super._config.getBBSymbols();
        }
    }
    
    @Override
    public void _closeSource() {
        this._endOfInput = true;
    }
    
    public abstract byte _currentByte();
    
    public final PName _findXmlDeclName(int n, final int n2) {
        int quadCount;
        final int n3 = quadCount = this._quadCount;
        if (n2 == 0) {
            final int[] quadBuffer = this._quadBuffer;
            quadCount = n3 - 1;
            n = quadBuffer[quadCount];
        }
        PName pName;
        if (quadCount != 0) {
            if (quadCount != 1) {
                if (quadCount != 2) {
                    pName = null;
                }
                else {
                    final int[] quadBuffer2 = this._quadBuffer;
                    pName = AsyncXmlDeclHelper.find(quadBuffer2[0], quadBuffer2[1], n);
                }
            }
            else {
                pName = AsyncXmlDeclHelper.find(this._quadBuffer[0], n);
            }
        }
        else {
            pName = AsyncXmlDeclHelper.find(n);
        }
        if (pName != null) {
            --super._inputPtr;
            return pName;
        }
        this._activateEncoding();
        return this.findPName(n, n2);
    }
    
    public abstract byte _nextByte();
    
    public abstract byte _prevByte();
    
    @Override
    public void _releaseBuffers() {
        super._releaseBuffers();
        if (this._symbols.maybeDirty()) {
            super._config.updateBBSymbols(this._symbols);
        }
    }
    
    public int _startDocumentNoXmlDecl() {
        this._activateEncoding();
        return super._currToken = 7;
    }
    
    public final PName addPName(final ByteBasedPNameTable byteBasedPNameTable, final int n, final int[] array, final int n2, final int n3) {
        return this.addUTFPName(byteBasedPNameTable, this._charTypes, n, array, n2, n3);
    }
    
    public abstract boolean asyncSkipSpace();
    
    public void checkPITargetName(final PName pName) {
        final String localName = pName.getLocalName();
        if (localName.length() == 3 && localName.equalsIgnoreCase("xml") && !pName.hasPrefix()) {
            this.reportInputProblem(ErrorConsts.ERR_WF_PI_XML_TARGET);
        }
    }
    
    @Override
    public int decodeCharForError(final byte b) {
        return b;
    }
    
    @Override
    public void endOfInput() {
        this._endOfInput = true;
    }
    
    public final PName findPName(int n, int n2) {
        --super._inputPtr;
        int quadCount;
        final int n3 = quadCount = this._quadCount;
        int n4 = n;
        n = n2;
        if (n2 == 0) {
            final int[] quadBuffer = this._quadBuffer;
            quadCount = n3 - 1;
            n4 = quadBuffer[quadCount];
            n = 4;
        }
        if (quadCount > 1) {
            final int[] quadBuffer2 = this._quadBuffer;
            if (quadCount >= quadBuffer2.length) {
                this._quadBuffer = DataUtil.growArrayBy(quadBuffer2, quadBuffer2.length);
            }
            final int[] quadBuffer3 = this._quadBuffer;
            n2 = quadCount + 1;
            quadBuffer3[quadCount] = n4;
            final int calcHash = ByteBasedPNameTable.calcHash(quadBuffer3, n2);
            PName pName;
            if ((pName = this._symbols.findSymbol(calcHash, this._quadBuffer, n2)) == null) {
                pName = this.addPName(this._symbols, calcHash, this._quadBuffer, n2, n);
            }
            return pName;
        }
        if (quadCount == 0) {
            n2 = ByteBasedPNameTable.calcHash(n4);
            PName pName2;
            if ((pName2 = this._symbols.findSymbol(n2, n4, 0)) == null) {
                final int[] quadBuffer4 = this._quadBuffer;
                quadBuffer4[0] = n4;
                pName2 = this.addPName(this._symbols, n2, quadBuffer4, 1, n);
            }
            return pName2;
        }
        final int n5 = this._quadBuffer[0];
        n2 = ByteBasedPNameTable.calcHash(n5, n4);
        PName pName3;
        if ((pName3 = this._symbols.findSymbol(n2, n5, n4)) == null) {
            final int[] quadBuffer5 = this._quadBuffer;
            quadBuffer5[1] = n4;
            pName3 = this.addPName(this._symbols, n2, quadBuffer5, 2, n);
        }
        return pName3;
    }
    
    @Override
    public void finishCData() {
        this.throwInternal();
    }
    
    @Override
    public abstract void finishCharacters();
    
    @Override
    public void finishComment() {
        this.throwInternal();
    }
    
    @Override
    public void finishDTD(final boolean b) {
        this.throwInternal();
    }
    
    @Override
    public void finishPI() {
        this.throwInternal();
    }
    
    @Override
    public void finishSpace() {
        this.throwInternal();
    }
    
    @Override
    public final void finishToken() {
        super._tokenIncomplete = false;
        final int currToken = super._currToken;
        if (currToken != 3) {
            if (currToken != 4) {
                if (currToken != 5) {
                    if (currToken != 6) {
                        if (currToken != 11) {
                            if (currToken != 12) {
                                ErrorConsts.throwInternalError();
                            }
                            else {
                                this.finishCData();
                            }
                        }
                        else {
                            this.finishDTD(true);
                        }
                    }
                    else {
                        this.finishSpace();
                    }
                }
                else {
                    this.finishComment();
                }
            }
            else {
                this.finishCharacters();
            }
        }
        else {
            this.finishPI();
        }
    }
    
    public abstract boolean handleAttrValue();
    
    public abstract int handleComment();
    
    public abstract boolean handleDTDInternalSubset(final boolean p0);
    
    public abstract boolean handleNsDecl();
    
    public abstract int handlePI();
    
    public abstract boolean handlePartialCR();
    
    public abstract int handleStartElement();
    
    public abstract int handleStartElementStart(final byte p0);
    
    @Override
    public boolean loadMore() {
        this.throwInternal();
        return false;
    }
    
    @Override
    public final int nextFromProlog(final boolean b) {
        if (super._currToken != 257) {
            this.setStartLocation();
            if (super._currToken == 7) {
                super._currToken = 257;
                final PName tokenName = super._tokenName;
                if (tokenName != null) {
                    this._nextEvent = 3;
                    this._state = 1;
                    this.checkPITargetName(tokenName);
                    return this.handlePI();
                }
            }
            else {
                this._nextEvent = 257;
                super._currToken = 257;
                this._state = 0;
            }
        }
        final int nextEvent = this._nextEvent;
        if (nextEvent == 257) {
            if (this._state == 1) {
                if (super._inputPtr >= super._inputEnd) {
                    return super._currToken;
                }
                if (this._pendingInput != 0) {
                    final Boolean startXmlDeclaration = this.startXmlDeclaration();
                    if (startXmlDeclaration == null) {
                        return 257;
                    }
                    if (startXmlDeclaration == Boolean.FALSE) {
                        return this._startDocumentNoXmlDecl();
                    }
                    return this.handleXmlDeclaration();
                }
                else {
                    if (this._currentByte() != 60) {
                        this._state = 0;
                        return this._startDocumentNoXmlDecl();
                    }
                    ++super._inputPtr;
                    this._pendingInput = -5;
                    final Boolean startXmlDeclaration2 = this.startXmlDeclaration();
                    if (startXmlDeclaration2 == null) {
                        return 257;
                    }
                    if (startXmlDeclaration2 == Boolean.FALSE) {
                        return this._startDocumentNoXmlDecl();
                    }
                    return this.handleXmlDeclaration();
                }
            }
            else {
                if (this._pendingInput != 0 && !this.handlePartialCR()) {
                    return super._currToken;
                }
                while (this._state == 0) {
                    if (super._inputPtr >= super._inputEnd) {
                        if (this._endOfInput) {
                            this.setStartLocation();
                            return -1;
                        }
                        return super._currToken;
                    }
                    else {
                        final byte nextByte = this._nextByte();
                        if (nextByte == 60) {
                            this._state = 2;
                            break;
                        }
                        if (nextByte != 32 && nextByte != 13 && nextByte != 10 && nextByte != 9) {
                            this.reportPrologUnexpChar(b, this.decodeCharForError(nextByte), null);
                        }
                        else {
                            if (this.asyncSkipSpace()) {
                                continue;
                            }
                            if (this._endOfInput) {
                                this.setStartLocation();
                                return -1;
                            }
                            return super._currToken;
                        }
                    }
                }
                final int state = this._state;
                if (state == 2) {
                    if (super._inputPtr >= super._inputEnd) {
                        return super._currToken;
                    }
                    final byte nextByte2 = this._nextByte();
                    if (nextByte2 == 33) {
                        this._state = 3;
                        return this.handlePrologDeclStart(b);
                    }
                    if (nextByte2 == 63) {
                        this._nextEvent = 3;
                        this._state = 0;
                        return this.handlePI();
                    }
                    if (nextByte2 == 47 || !b) {
                        this.reportPrologUnexpElement(b, nextByte2);
                    }
                    return this.handleStartElementStart(nextByte2);
                }
                else {
                    if (state == 3) {
                        return this.handlePrologDeclStart(b);
                    }
                    return this.throwInternal();
                }
            }
        }
        else {
            if (nextEvent == 1) {
                return this.handleStartElement();
            }
            if (nextEvent == 3) {
                return this.handlePI();
            }
            if (nextEvent == 5) {
                return this.handleComment();
            }
            if (nextEvent == 7) {
                return this.handleXmlDeclaration();
            }
            if (nextEvent != 11) {
                return this.throwInternal();
            }
            return this.handleDTD();
        }
    }
    
    public abstract PName parseNewName(final byte p0);
    
    public abstract PName parsePName();
    
    public boolean parseXmlDeclAttr(char[] array, int n) {
        final byte elemAttrQuote = this._elemAttrQuote;
        while (super._inputPtr < super._inputEnd) {
            final int n2 = this._nextByte() & 0xFF;
            if (n2 == elemAttrQuote) {
                super._textBuilder.setCurrentLength(n);
                return true;
            }
            if (n2 <= 32 || n2 > 122) {
                this.reportPrologUnexpChar(true, this.decodeCharForError((byte)n2), " (not valid in XML pseudo-attribute values)");
            }
            char[] finishCurrentSegment = array;
            int n3;
            if ((n3 = n) >= array.length) {
                finishCurrentSegment = super._textBuilder.finishCurrentSegment();
                n3 = 0;
            }
            finishCurrentSegment[n3] = (char)n2;
            n = n3 + 1;
            array = finishCurrentSegment;
        }
        super._textBuilder.setCurrentLength(n);
        return false;
    }
    
    public void reportInvalidOther(final int n, final int inputPtr) {
        super._inputPtr = inputPtr;
        this.reportInvalidOther(n);
    }
    
    @Override
    public void skipCData() {
        this.throwInternal();
    }
    
    @Override
    public abstract boolean skipCharacters();
    
    @Override
    public void skipComment() {
        this.throwInternal();
    }
    
    @Override
    public void skipPI() {
        this.throwInternal();
    }
    
    @Override
    public void skipSpace() {
        this.throwInternal();
    }
    
    public abstract int startCharacters(final byte p0);
    
    public int throwInternal() {
        throw new IllegalStateException("Internal error: should never execute this code path");
    }
    
    public boolean validPublicIdChar(final int n) {
        return n == 10 || n == 13 || n == 32 || (n >= 48 && n <= 57) || (n >= 64 && n <= 90) || (n >= 97 && n <= 122) || n == 33 || (n >= 35 && n <= 37) || (n >= 39 && n <= 47) || (n >= 58 && n <= 59) || n == 61 || n == 63 || n == 95;
    }
    
    public void verifyAndAppendEntityCharacter(int n) {
        this.verifyXmlChar(n);
        int n2 = n;
        if (n >> 16 != 0) {
            n -= 65536;
            super._textBuilder.append((char)(n >> 10 | 0xD800));
            n2 = ((n & 0x3FF) | 0xDC00);
        }
        super._textBuilder.append((char)n2);
    }
    
    public void verifyAndSetPublicId() {
        super._publicId = super._textBuilder.contentsAsString();
    }
    
    public void verifyAndSetSystemId() {
        super._systemId = super._textBuilder.contentsAsString();
    }
    
    public void verifyAndSetXmlEncoding() {
        final String normalize = CharsetNames.normalize(super._textBuilder.contentsAsString());
        if ("UTF-8" != normalize && "US-ASCII" != normalize && "ISO-8859-1" != normalize) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unsupported encoding '");
            sb.append(normalize);
            sb.append("': only UTF-8 and US-ASCII support by async parser");
            this.reportInputProblem(sb.toString());
        }
        super._config.setXmlEncoding(normalize);
        if (normalize != null) {
            super._config.setActualEncoding(normalize);
        }
        this._charTypes = super._config.getCharTypes();
    }
    
    public void verifyAndSetXmlStandalone() {
        ReaderConfig readerConfig;
        Boolean xmlStandalone;
        if (super._textBuilder.equalsString("yes")) {
            readerConfig = super._config;
            xmlStandalone = Boolean.TRUE;
        }
        else {
            if (!super._textBuilder.equalsString("no")) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid standalone value '");
                sb.append(super._textBuilder.contentsAsString());
                sb.append("': can only use 'yes' and 'no'");
                this.reportInputProblem(sb.toString());
                return;
            }
            readerConfig = super._config;
            xmlStandalone = Boolean.FALSE;
        }
        readerConfig.setXmlStandalone(xmlStandalone);
    }
    
    public void verifyAndSetXmlVersion() {
        final TextBuilder textBuilder = super._textBuilder;
        String xmlVersion = "1.0";
        if (!textBuilder.equalsString("1.0")) {
            final TextBuilder textBuilder2 = super._textBuilder;
            xmlVersion = "1.1";
            if (!textBuilder2.equalsString("1.1")) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unrecognized XML version '");
                sb.append(super._textBuilder.contentsAsString());
                sb.append("' (expected '1.0' or '1.1')");
                this.reportInputProblem(sb.toString());
                return;
            }
        }
        super._config.setXmlVersion(xmlVersion);
    }
}

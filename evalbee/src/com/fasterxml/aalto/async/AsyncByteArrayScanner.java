// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.async;

import com.fasterxml.aalto.in.EntityNames;
import com.fasterxml.aalto.in.NsDeclaration;
import javax.xml.stream.XMLStreamException;
import com.fasterxml.aalto.in.ElementScope;
import com.fasterxml.aalto.util.DataUtil;
import com.fasterxml.aalto.in.PName;
import com.fasterxml.aalto.in.ReaderConfig;
import com.fasterxml.aalto.AsyncByteArrayFeeder;

public class AsyncByteArrayScanner extends AsyncByteScanner implements AsyncByteArrayFeeder
{
    protected byte[] _inputBuffer;
    protected int _origBufferLen;
    
    public AsyncByteArrayScanner(final ReaderConfig readerConfig) {
        super(readerConfig);
        super._state = 1;
        super._currToken = 257;
    }
    
    private int finishStartElement(final boolean isEmptyTag) {
        super._isEmptyTag = isEmptyTag;
        int attrCount;
        if ((attrCount = super._attrCollector.finishLastValue(super._elemAttrPtr)) < 0) {
            attrCount = super._attrCollector.getCount();
            this.reportInputProblem(super._attrCollector.getErrorMsg());
        }
        super._attrCount = attrCount;
        ++super._depth;
        if (!super._elemAllNsBound) {
            final boolean bound = super._tokenName.isBound();
            int i = 0;
            if (!bound) {
                this.reportUnboundPrefix(super._tokenName, false);
            }
            while (i < super._attrCount) {
                final PName name = super._attrCollector.getName(i);
                if (!name.isBound()) {
                    this.reportUnboundPrefix(name, true);
                }
                ++i;
            }
        }
        return super._currToken = 1;
    }
    
    private final boolean handleAndAppendPending() {
        final int inputPtr = super._inputPtr;
        if (inputPtr >= super._inputEnd) {
            return false;
        }
        final int pendingInput = super._pendingInput;
        super._pendingInput = 0;
        if (pendingInput < 0) {
            if (pendingInput == -1) {
                if (this._inputBuffer[inputPtr] == 10) {
                    super._inputPtr = inputPtr + 1;
                }
                this.markLF();
                super._textBuilder.append('\n');
                return true;
            }
            this.throwInternal();
        }
        final int[] text_CHARS = super._charTypes.TEXT_CHARS;
        final int n = pendingInput & 0xFF;
        final int n2 = text_CHARS[n];
        if (n2 != 5) {
            int pendingInput2 = 0;
            Label_0179: {
                int n11 = 0;
                Label_0175: {
                    int n3 = 0;
                    Label_0170: {
                        if (n2 == 6) {
                            final byte[] inputBuffer = this._inputBuffer;
                            final int inputPtr2 = super._inputPtr;
                            final int inputPtr3 = inputPtr2 + 1;
                            super._inputPtr = inputPtr3;
                            n3 = (inputBuffer[inputPtr2] & 0xFF);
                            final int n4 = pendingInput >> 8;
                            int n5;
                            if (n4 == 0) {
                                if (inputPtr3 >= super._inputEnd) {
                                    break Label_0170;
                                }
                                super._inputPtr = inputPtr3 + 1;
                                n5 = this.decodeUtf8_3(pendingInput, n3, inputBuffer[inputPtr3] & 0xFF);
                            }
                            else {
                                n5 = this.decodeUtf8_3(n, n4, n3);
                            }
                            super._textBuilder.append((char)n5);
                            return true;
                        }
                        if (n2 != 7) {
                            this.throwInternal();
                            return true;
                        }
                        final byte[] inputBuffer2 = this._inputBuffer;
                        final int inputPtr4 = super._inputPtr;
                        final int inputPtr5 = inputPtr4 + 1;
                        super._inputPtr = inputPtr5;
                        n3 = (inputBuffer2[inputPtr4] & 0xFF);
                        final int n6 = pendingInput >> 8;
                        int n8;
                        if (n6 == 0) {
                            final int inputEnd = super._inputEnd;
                            if (inputPtr5 >= inputEnd) {
                                break Label_0170;
                            }
                            final int inputPtr6 = inputPtr5 + 1;
                            super._inputPtr = inputPtr6;
                            final int n7 = inputBuffer2[inputPtr5] & 0xFF;
                            if (inputPtr6 >= inputEnd) {
                                pendingInput2 = (n3 << 8 | pendingInput | n7 << 16);
                                break Label_0179;
                            }
                            super._inputPtr = inputPtr6 + 1;
                            n8 = this.decodeUtf8_4(pendingInput, n3, n7, inputBuffer2[inputPtr6] & 0xFF);
                        }
                        else {
                            final int n9 = n6 & 0xFF;
                            final int n10 = pendingInput >> 16;
                            if (n10 == 0) {
                                if (inputPtr5 >= super._inputEnd) {
                                    n11 = n3 << 16;
                                    break Label_0175;
                                }
                                super._inputPtr = inputPtr5 + 1;
                                n8 = this.decodeUtf8_4(n, n9, n3, inputBuffer2[inputPtr5] & 0xFF);
                            }
                            else {
                                n8 = this.decodeUtf8_4(n, n9, n10, n3);
                            }
                        }
                        super._textBuilder.appendSurrogate(n8);
                        return true;
                    }
                    n11 = n3 << 8;
                }
                pendingInput2 = (n11 | pendingInput);
            }
            super._pendingInput = pendingInput2;
            return false;
        }
        super._textBuilder.append((char)this.decodeUtf8_2(pendingInput));
        return true;
    }
    
    private final boolean handleAttrValuePending() {
        final int pendingInput = super._pendingInput;
        if (pendingInput == -1) {
            if (!this.handlePartialCR()) {
                return false;
            }
            char[] array;
            if (super._elemAttrPtr >= (array = super._attrCollector.continueValue()).length) {
                array = super._attrCollector.valueBufferFull();
            }
            array[super._elemAttrPtr++] = ' ';
            return true;
        }
        else {
            int inputPtr = super._inputPtr;
            final int inputEnd = super._inputEnd;
            if (inputPtr >= inputEnd) {
                return false;
            }
            int n = 0;
            Label_0324: {
                PName tokenName = null;
                Label_0309: {
                    Label_0265: {
                        if (pendingInput == -60) {
                            final byte[] inputBuffer = this._inputBuffer;
                            final int inputPtr2 = inputPtr + 1;
                            super._inputPtr = inputPtr2;
                            final byte b = inputBuffer[inputPtr];
                            if (b == 35) {
                                super._pendingInput = -61;
                                if (inputPtr2 >= inputEnd) {
                                    return false;
                                }
                                if (inputBuffer[inputPtr2] == 120) {
                                    super._pendingInput = -62;
                                    if ((super._inputPtr = inputPtr2 + 1) >= inputEnd) {
                                        return false;
                                    }
                                    break Label_0265;
                                }
                            }
                            else {
                                tokenName = this.parseNewEntityName(b);
                                if (tokenName == null) {
                                    super._pendingInput = -63;
                                    return false;
                                }
                                final int decodeGeneralEntity = this.decodeGeneralEntity(tokenName);
                                if ((n = decodeGeneralEntity) == 0) {
                                    n = decodeGeneralEntity;
                                    break Label_0309;
                                }
                                break Label_0324;
                            }
                        }
                        else if (pendingInput == -61) {
                            if (this._inputBuffer[inputPtr] == 120) {
                                super._pendingInput = -62;
                                ++inputPtr;
                                if ((super._inputPtr = inputPtr) >= inputEnd) {
                                    return false;
                                }
                                break Label_0265;
                            }
                        }
                        else {
                            if (pendingInput == -62) {
                                break Label_0265;
                            }
                            if (pendingInput == -63) {
                                tokenName = this.parseEntityName();
                                if (tokenName == null) {
                                    return false;
                                }
                                final int decodeGeneralEntity2 = this.decodeGeneralEntity(tokenName);
                                if ((n = decodeGeneralEntity2) == 0) {
                                    n = decodeGeneralEntity2;
                                    break Label_0309;
                                }
                                break Label_0324;
                            }
                            else {
                                if (pendingInput == -64) {
                                    n = this.handleDecEntityInAttribute(false);
                                    break Label_0324;
                                }
                                if (pendingInput == -65) {
                                    n = this.handleHexEntityInAttribute(false);
                                    break Label_0324;
                                }
                                n = this.handleAttrValuePendingUTF8();
                                break Label_0324;
                            }
                        }
                        n = this.handleDecEntityInAttribute(true);
                        break Label_0324;
                    }
                    n = this.handleHexEntityInAttribute(true);
                    break Label_0324;
                }
                super._tokenName = tokenName;
                this.reportUnexpandedEntityInAttr(super._elemAttrName, false);
            }
            if (n == 0) {
                return false;
            }
            final char[] continueValue = super._attrCollector.continueValue();
            int n2 = n;
            char[] valueBufferFull = continueValue;
            if (n >> 16 != 0) {
                final int n3 = n - 65536;
                valueBufferFull = continueValue;
                if (super._elemAttrPtr >= continueValue.length) {
                    valueBufferFull = super._attrCollector.valueBufferFull();
                }
                valueBufferFull[super._elemAttrPtr++] = (char)(n3 >> 10 | 0xD800);
                n2 = ((n3 & 0x3FF) | 0xDC00);
            }
            char[] valueBufferFull2 = valueBufferFull;
            if (super._elemAttrPtr >= valueBufferFull.length) {
                valueBufferFull2 = super._attrCollector.valueBufferFull();
            }
            valueBufferFull2[super._elemAttrPtr++] = (char)n2;
            return true;
        }
    }
    
    private final int handleAttrValuePendingUTF8() {
        int pendingInput = super._pendingInput;
        super._pendingInput = 0;
        final int[] text_CHARS = super._charTypes.TEXT_CHARS;
        final int n = pendingInput & 0xFF;
        final int n2 = text_CHARS[n];
        if (n2 != 5) {
            int n8 = 0;
            Label_0112: {
                int n3 = 0;
                Label_0107: {
                    if (n2 == 6) {
                        final byte[] inputBuffer = this._inputBuffer;
                        final int inputPtr = super._inputPtr;
                        final int inputPtr2 = inputPtr + 1;
                        super._inputPtr = inputPtr2;
                        n3 = (inputBuffer[inputPtr] & 0xFF);
                        final int n4 = pendingInput >> 8;
                        int n5;
                        if (n4 == 0) {
                            if (inputPtr2 >= super._inputEnd) {
                                break Label_0107;
                            }
                            super._inputPtr = inputPtr2 + 1;
                            n5 = this.decodeUtf8_3(pendingInput, n3, inputBuffer[inputPtr2] & 0xFF);
                        }
                        else {
                            n5 = this.decodeUtf8_3(n, n4, n3);
                        }
                        return n5;
                    }
                    if (n2 != 7) {
                        this.throwInternal();
                        return 0;
                    }
                    final byte[] inputBuffer2 = this._inputBuffer;
                    final int inputPtr3 = super._inputPtr;
                    final int inputPtr4 = inputPtr3 + 1;
                    super._inputPtr = inputPtr4;
                    n3 = (inputBuffer2[inputPtr3] & 0xFF);
                    final int n6 = pendingInput >> 8;
                    int n9;
                    if (n6 == 0) {
                        final int inputEnd = super._inputEnd;
                        if (inputPtr4 >= inputEnd) {
                            break Label_0107;
                        }
                        final int inputPtr5 = inputPtr4 + 1;
                        super._inputPtr = inputPtr5;
                        final int n7 = inputBuffer2[inputPtr4] & 0xFF;
                        if (inputPtr5 >= inputEnd) {
                            pendingInput |= n3 << 8;
                            n8 = n7 << 16;
                            break Label_0112;
                        }
                        super._inputPtr = inputPtr5 + 1;
                        n9 = this.decodeUtf8_4(pendingInput, n3, n7, inputBuffer2[inputPtr5] & 0xFF);
                    }
                    else {
                        final int n10 = n6 & 0xFF;
                        final int n11 = pendingInput >> 16;
                        if (n11 == 0) {
                            if (inputPtr4 >= super._inputEnd) {
                                n8 = n3 << 16;
                                break Label_0112;
                            }
                            super._inputPtr = inputPtr4 + 1;
                            n9 = this.decodeUtf8_4(n, n10, n3, inputBuffer2[inputPtr4] & 0xFF);
                        }
                        else {
                            n9 = this.decodeUtf8_4(n, n10, n11, n3);
                        }
                    }
                    return n9;
                }
                n8 = n3 << 8;
            }
            super._pendingInput = (pendingInput | n8);
            return 0;
        }
        return this.decodeUtf8_2(pendingInput);
    }
    
    private int handleCData() {
        if (super._state == 1) {
            return this.parseCDataContents();
        }
        final int inputPtr = super._inputPtr;
        if (inputPtr >= super._inputEnd) {
            return 257;
        }
        final byte[] inputBuffer = this._inputBuffer;
        super._inputPtr = inputPtr + 1;
        return this.handleCDataStartMarker(inputBuffer[inputPtr]);
    }
    
    private int handleCDataStartMarker(byte b) {
        final int state = super._state;
        Label_0334: {
            byte b5 = 0;
            Label_0276: {
                byte b4 = 0;
                Label_0219: {
                    byte b3 = 0;
                    Label_0162: {
                        byte b2;
                        if (state != 0) {
                            b2 = b;
                            if (state != 2) {
                                b3 = b;
                                if (state == 3) {
                                    break Label_0162;
                                }
                                b4 = b;
                                if (state == 4) {
                                    break Label_0219;
                                }
                                b5 = b;
                                if (state == 5) {
                                    break Label_0276;
                                }
                                if (state != 6) {
                                    return this.throwInternal();
                                }
                                break Label_0334;
                            }
                        }
                        else {
                            if (b != 67) {
                                this.reportTreeUnexpChar(this.decodeCharForError(b), " (expected 'C' for CDATA)");
                            }
                            super._state = 2;
                            final int inputPtr = super._inputPtr;
                            if (inputPtr >= super._inputEnd) {
                                return 257;
                            }
                            final byte[] inputBuffer = this._inputBuffer;
                            super._inputPtr = inputPtr + 1;
                            b2 = inputBuffer[inputPtr];
                        }
                        if (b2 != 68) {
                            this.reportTreeUnexpChar(this.decodeCharForError(b2), " (expected 'D' for CDATA)");
                        }
                        super._state = 3;
                        final int inputPtr2 = super._inputPtr;
                        if (inputPtr2 >= super._inputEnd) {
                            return 257;
                        }
                        final byte[] inputBuffer2 = this._inputBuffer;
                        super._inputPtr = inputPtr2 + 1;
                        b3 = inputBuffer2[inputPtr2];
                    }
                    if (b3 != 65) {
                        this.reportTreeUnexpChar(this.decodeCharForError(b3), " (expected 'A' for CDATA)");
                    }
                    super._state = 4;
                    final int inputPtr3 = super._inputPtr;
                    if (inputPtr3 >= super._inputEnd) {
                        return 257;
                    }
                    final byte[] inputBuffer3 = this._inputBuffer;
                    super._inputPtr = inputPtr3 + 1;
                    b4 = inputBuffer3[inputPtr3];
                }
                if (b4 != 84) {
                    this.reportTreeUnexpChar(this.decodeCharForError(b4), " (expected 'T' for CDATA)");
                }
                super._state = 5;
                final int inputPtr4 = super._inputPtr;
                if (inputPtr4 >= super._inputEnd) {
                    return 257;
                }
                final byte[] inputBuffer4 = this._inputBuffer;
                super._inputPtr = inputPtr4 + 1;
                b5 = inputBuffer4[inputPtr4];
            }
            if (b5 != 65) {
                this.reportTreeUnexpChar(this.decodeCharForError(b5), " (expected 'A' for CDATA)");
            }
            super._state = 6;
            final int inputPtr5 = super._inputPtr;
            if (inputPtr5 >= super._inputEnd) {
                return 257;
            }
            final byte[] inputBuffer5 = this._inputBuffer;
            super._inputPtr = inputPtr5 + 1;
            b = inputBuffer5[inputPtr5];
        }
        if (b != 91) {
            this.reportTreeUnexpChar(this.decodeCharForError(b), " (expected '[' for CDATA)");
        }
        super._textBuilder.resetWithEmpty();
        super._state = 1;
        if (super._inputPtr >= super._inputEnd) {
            return 257;
        }
        return this.parseCDataContents();
    }
    
    private final int handleDecEntityInAttribute(final boolean b) {
        byte b3;
        final byte b2 = b3 = this._inputBuffer[super._inputPtr++];
        if (b) {
            if (b2 < 48 || b2 > 57) {
                this.throwUnexpectedChar(this.decodeCharForError(b2), " expected a digit (0 - 9) for character entity");
            }
            super._pendingInput = -64;
            super._entityValue = b2 - 48;
            final int inputPtr = super._inputPtr;
            if (inputPtr >= super._inputEnd) {
                return 0;
            }
            final byte[] inputBuffer = this._inputBuffer;
            super._inputPtr = inputPtr + 1;
            b3 = inputBuffer[inputPtr];
        }
        while (b3 != 59) {
            final int n = b3 - 48;
            if (n < 0 || n > 9) {
                this.throwUnexpectedChar(this.decodeCharForError(b3), " expected a digit (0 - 9) for character entity");
            }
            if ((super._entityValue = super._entityValue * 10 + n) > 1114111) {
                this.reportEntityOverflow();
            }
            final int inputPtr2 = super._inputPtr;
            if (inputPtr2 >= super._inputEnd) {
                return 0;
            }
            final byte[] inputBuffer2 = this._inputBuffer;
            super._inputPtr = inputPtr2 + 1;
            b3 = inputBuffer2[inputPtr2];
        }
        this.verifyXmlChar(super._entityValue);
        super._pendingInput = 0;
        return super._entityValue;
    }
    
    private int handleEndElement() {
        final int state = super._state;
        Label_0266: {
            if (state == 0) {
                final PName tokenName = super._tokenName;
                while (super._quadCount < tokenName.sizeInQuads() - 1) {
                    while (true) {
                        final int currQuadBytes = super._currQuadBytes;
                        if (currQuadBytes >= 4) {
                            if (super._currQuad != tokenName.getQuad(super._quadCount)) {
                                this.reportUnexpectedEndTag(tokenName.getPrefixedName());
                            }
                            super._currQuadBytes = 0;
                            super._currQuad = 0;
                            ++super._quadCount;
                            break;
                        }
                        final int inputPtr = super._inputPtr;
                        if (inputPtr >= super._inputEnd) {
                            return 257;
                        }
                        final int currQuad = super._currQuad;
                        final byte[] inputBuffer = this._inputBuffer;
                        super._inputPtr = inputPtr + 1;
                        super._currQuad = ((inputBuffer[inputPtr] & 0xFF) | currQuad << 8);
                        super._currQuadBytes = currQuadBytes + 1;
                    }
                }
                final int lastQuad = tokenName.getLastQuad();
                while (true) {
                    do {
                        final int inputPtr2 = super._inputPtr;
                        if (inputPtr2 >= super._inputEnd) {
                            return 257;
                        }
                        final int currQuad2 = super._currQuad;
                        final byte[] inputBuffer2 = this._inputBuffer;
                        super._inputPtr = inputPtr2 + 1;
                        if ((super._currQuad = ((inputBuffer2[inputPtr2] & 0xFF) | currQuad2 << 8)) == lastQuad) {
                            super._state = 1;
                            break Label_0266;
                        }
                    } while (++super._currQuadBytes <= 3);
                    this.reportUnexpectedEndTag(tokenName.getPrefixedName());
                    continue;
                }
            }
            if (state != 1) {
                this.throwInternal();
            }
        }
        if (super._pendingInput != 0 && !this.handlePartialCR()) {
            return 257;
        }
        while (true) {
            final int inputPtr3 = super._inputPtr;
            final int inputEnd = super._inputEnd;
            if (inputPtr3 >= inputEnd) {
                return 257;
            }
            final byte[] inputBuffer3 = this._inputBuffer;
            final int inputPtr4 = inputPtr3 + 1;
            super._inputPtr = inputPtr4;
            final int n = inputBuffer3[inputPtr3] & 0xFF;
            if (n > 32) {
                if (n != 62) {
                    this.throwUnexpectedChar(this.decodeCharForError((byte)n), " expected space or closing '>'");
                }
                return super._currToken = 2;
            }
            if (n != 10) {
                if (n == 13) {
                    if (inputPtr4 >= inputEnd) {
                        super._pendingInput = -1;
                        return 257;
                    }
                    if (inputBuffer3[inputPtr4] == 10) {
                        super._inputPtr = inputPtr4 + 1;
                    }
                }
                else {
                    if (n != 32 && n != 9) {
                        this.throwInvalidSpace(n);
                        continue;
                    }
                    continue;
                }
            }
            this.markLF();
        }
    }
    
    private int handleEndElementStart() {
        --super._depth;
        final PName name = super._currElem.getName();
        super._tokenName = name;
        final int sizeInQuads = name.sizeInQuads();
        final int inputEnd = super._inputEnd;
        final int inputPtr = super._inputPtr;
        int i = 0;
        if (inputEnd - inputPtr < (sizeInQuads << 2) + 1) {
            super._nextEvent = 2;
            super._state = 0;
            super._currQuadBytes = 0;
            super._currQuad = 0;
            super._quadCount = 0;
            return this.handleEndElement();
        }
        final byte[] inputBuffer = this._inputBuffer;
        int n;
        for (n = sizeInQuads - 1; i < n; ++i) {
            final int inputPtr2 = super._inputPtr;
            final byte b = inputBuffer[inputPtr2];
            final byte b2 = inputBuffer[inputPtr2 + 1];
            final byte b3 = inputBuffer[inputPtr2 + 2];
            final byte b4 = inputBuffer[inputPtr2 + 3];
            super._inputPtr = inputPtr2 + 4;
            if ((b << 24 | (b2 & 0xFF) << 16 | (b3 & 0xFF) << 8 | (b4 & 0xFF)) != super._tokenName.getQuad(i)) {
                this.reportUnexpectedEndTag(super._tokenName.getPrefixedName());
            }
        }
        final int quad = super._tokenName.getQuad(n);
        final int inputPtr3 = super._inputPtr;
        final int inputPtr4 = inputPtr3 + 1;
        super._inputPtr = inputPtr4;
        final int n2 = inputBuffer[inputPtr3] & 0xFF;
        if (n2 != quad) {
            final int inputPtr5 = inputPtr4 + 1;
            super._inputPtr = inputPtr5;
            final int n3 = n2 << 8 | (inputBuffer[inputPtr4] & 0xFF);
            if (n3 != quad) {
                final int inputPtr6 = inputPtr5 + 1;
                super._inputPtr = inputPtr6;
                final int n4 = n3 << 8 | (inputBuffer[inputPtr5] & 0xFF);
                if (n4 != quad) {
                    super._inputPtr = inputPtr6 + 1;
                    if (((inputBuffer[inputPtr6] & 0xFF) | n4 << 8) != quad) {
                        this.reportUnexpectedEndTag(super._tokenName.getPrefixedName());
                    }
                }
            }
        }
        byte b5 = this._inputBuffer[super._inputPtr++];
        while (true) {
            final int n5 = b5 & 0xFF;
            if (n5 > 32) {
                if (n5 != 62) {
                    this.throwUnexpectedChar(this.decodeCharForError((byte)n5), " expected space or closing '>'");
                }
                return super._currToken = 2;
            }
            Label_0465: {
                if (n5 != 10) {
                    if (n5 == 13) {
                        final int inputPtr7 = super._inputPtr;
                        if (inputPtr7 >= super._inputEnd) {
                            super._pendingInput = -1;
                            break;
                        }
                        if (this._inputBuffer[inputPtr7] == 10) {
                            super._inputPtr = inputPtr7 + 1;
                        }
                    }
                    else {
                        if (n5 != 32 && n5 != 9) {
                            this.throwInvalidSpace(n5);
                        }
                        break Label_0465;
                    }
                }
                this.markLF();
            }
            final int inputPtr8 = super._inputPtr;
            if (inputPtr8 >= super._inputEnd) {
                break;
            }
            final byte[] inputBuffer2 = this._inputBuffer;
            super._inputPtr = inputPtr8 + 1;
            b5 = inputBuffer2[inputPtr8];
        }
        super._nextEvent = 2;
        super._state = 1;
        return 257;
    }
    
    private final int handleHexEntityInAttribute(final boolean b) {
        byte b2 = this._inputBuffer[super._inputPtr++];
        int n = b ? 1 : 0;
        while (b2 != 59) {
            int entityValue = 0;
            Label_0109: {
                if (b2 <= 57 && b2 >= 48) {
                    entityValue = b2 - 48;
                }
                else {
                    if (b2 <= 70 && b2 >= 65) {
                        entityValue = b2 - 65;
                    }
                    else {
                        if (b2 > 102 || b2 < 97) {
                            this.throwUnexpectedChar(this.decodeCharForError(b2), " expected a hex digit (0-9a-fA-F) for character entity");
                            entityValue = b2;
                            break Label_0109;
                        }
                        entityValue = b2 - 97;
                    }
                    entityValue += 10;
                }
            }
            int n2;
            if (n != 0) {
                super._pendingInput = -65;
                n2 = 0;
            }
            else {
                final int n3 = entityValue += super._entityValue << 4;
                n2 = n;
                if (n3 > 1114111) {
                    this.reportEntityOverflow();
                    n2 = n;
                    entityValue = n3;
                }
            }
            super._entityValue = entityValue;
            final int inputPtr = super._inputPtr;
            if (inputPtr >= super._inputEnd) {
                return 0;
            }
            final byte[] inputBuffer = this._inputBuffer;
            super._inputPtr = inputPtr + 1;
            b2 = inputBuffer[inputPtr];
            n = n2;
        }
        this.verifyXmlChar(super._entityValue);
        super._pendingInput = 0;
        return super._entityValue;
    }
    
    private final boolean handleNsValuePending() {
        final int pendingInput = super._pendingInput;
        if (pendingInput == -1) {
            if (!this.handlePartialCR()) {
                return false;
            }
            char[] nameBuffer;
            final char[] array = nameBuffer = super._nameBuffer;
            if (super._elemNsPtr >= array.length) {
                nameBuffer = DataUtil.growArrayBy(array, array.length);
                super._nameBuffer = nameBuffer;
            }
            nameBuffer[super._elemNsPtr++] = ' ';
            return true;
        }
        else {
            int inputPtr = super._inputPtr;
            final int inputEnd = super._inputEnd;
            if (inputPtr >= inputEnd) {
                return false;
            }
            int n = 0;
            Label_0328: {
                PName tokenName = null;
                Label_0313: {
                    Label_0269: {
                        if (pendingInput == -60) {
                            final byte[] inputBuffer = this._inputBuffer;
                            final int inputPtr2 = inputPtr + 1;
                            super._inputPtr = inputPtr2;
                            final byte b = inputBuffer[inputPtr];
                            if (b == 35) {
                                super._pendingInput = -61;
                                if (inputPtr2 >= inputEnd) {
                                    return false;
                                }
                                if (inputBuffer[inputPtr2] == 120) {
                                    super._pendingInput = -62;
                                    if ((super._inputPtr = inputPtr2 + 1) >= inputEnd) {
                                        return false;
                                    }
                                    break Label_0269;
                                }
                            }
                            else {
                                tokenName = this.parseNewEntityName(b);
                                if (tokenName == null) {
                                    super._pendingInput = -63;
                                    return false;
                                }
                                final int decodeGeneralEntity = this.decodeGeneralEntity(tokenName);
                                if ((n = decodeGeneralEntity) == 0) {
                                    n = decodeGeneralEntity;
                                    break Label_0313;
                                }
                                break Label_0328;
                            }
                        }
                        else if (pendingInput == -61) {
                            if (this._inputBuffer[inputPtr] == 120) {
                                super._pendingInput = -62;
                                ++inputPtr;
                                if ((super._inputPtr = inputPtr) >= inputEnd) {
                                    return false;
                                }
                                break Label_0269;
                            }
                        }
                        else {
                            if (pendingInput == -62) {
                                break Label_0269;
                            }
                            if (pendingInput == -63) {
                                tokenName = this.parseEntityName();
                                if (tokenName == null) {
                                    return false;
                                }
                                final int decodeGeneralEntity2 = this.decodeGeneralEntity(tokenName);
                                if ((n = decodeGeneralEntity2) == 0) {
                                    n = decodeGeneralEntity2;
                                    break Label_0313;
                                }
                                break Label_0328;
                            }
                            else {
                                if (pendingInput == -64) {
                                    n = this.handleDecEntityInAttribute(false);
                                    break Label_0328;
                                }
                                if (pendingInput == -65) {
                                    n = this.handleHexEntityInAttribute(false);
                                    break Label_0328;
                                }
                                n = this.handleAttrValuePendingUTF8();
                                break Label_0328;
                            }
                        }
                        n = this.handleDecEntityInAttribute(true);
                        break Label_0328;
                    }
                    n = this.handleHexEntityInAttribute(true);
                    break Label_0328;
                }
                super._tokenName = tokenName;
                this.reportUnexpandedEntityInAttr(super._elemAttrName, false);
            }
            if (n == 0) {
                return false;
            }
            final char[] nameBuffer2 = super._nameBuffer;
            int n2 = n;
            char[] growArrayBy = nameBuffer2;
            if (n >> 16 != 0) {
                final int n3 = n - 65536;
                growArrayBy = nameBuffer2;
                if (super._elemNsPtr >= nameBuffer2.length) {
                    growArrayBy = DataUtil.growArrayBy(nameBuffer2, nameBuffer2.length);
                    super._nameBuffer = growArrayBy;
                }
                growArrayBy[super._elemNsPtr++] = (char)(n3 >> 10 | 0xD800);
                n2 = ((n3 & 0x3FF) | 0xDC00);
            }
            char[] growArrayBy2 = growArrayBy;
            if (super._elemNsPtr >= growArrayBy.length) {
                growArrayBy2 = DataUtil.growArrayBy(growArrayBy, growArrayBy.length);
                super._nameBuffer = growArrayBy2;
            }
            growArrayBy2[super._elemNsPtr++] = (char)n2;
            return true;
        }
    }
    
    private void initAttribute(final byte elemAttrQuote) {
        super._elemAttrQuote = elemAttrQuote;
        final PName elemAttrName = super._elemAttrName;
        final String prefix = elemAttrName.getPrefix();
        PName bindName = null;
        boolean b = false;
        Label_0086: {
            Label_0084: {
                if (prefix == null) {
                    bindName = elemAttrName;
                    if (elemAttrName.getLocalName() != "xmlns") {
                        break Label_0084;
                    }
                }
                else if (prefix != "xmlns") {
                    final PName pName = bindName = this.bindName(elemAttrName, prefix);
                    if (super._elemAllNsBound) {
                        super._elemAllNsBound = pName.isBound();
                        bindName = pName;
                    }
                    break Label_0084;
                }
                b = true;
                bindName = elemAttrName;
                break Label_0086;
            }
            b = false;
        }
        if (b) {
            super._state = 8;
            super._elemNsPtr = 0;
            ++super._currNsCount;
        }
        else {
            super._state = 7;
            super._attrCollector.startNewValue(bindName, super._elemAttrPtr);
        }
    }
    
    private void initStartElement(PName bindName) {
        final String prefix = bindName.getPrefix();
        boolean bound;
        if (prefix == null) {
            bound = true;
        }
        else {
            bindName = this.bindName(bindName, prefix);
            bound = bindName.isBound();
        }
        super._elemAllNsBound = bound;
        super._tokenName = bindName;
        super._currElem = new ElementScope(bindName, super._currElem);
        super._attrCount = 0;
        super._currNsCount = 0;
        super._elemAttrPtr = 0;
        super._state = 2;
    }
    
    private int skipEntityInCharacters() {
        final int inputPtr = super._inputPtr;
        if (inputPtr + 3 <= super._inputEnd) {
            final byte[] inputBuffer = this._inputBuffer;
            final int n = inputPtr + 1;
            final byte b = inputBuffer[inputPtr];
            if (b == 35) {
                if (inputBuffer[n] == 120) {
                    return this.handleHexEntityInCharacters(n + 1);
                }
                return this.handleDecEntityInCharacters(n);
            }
            else if (b == 97) {
                final int n2 = n + 1;
                final byte b2 = inputBuffer[n];
                if (b2 == 109) {
                    final int n3 = n2 + 1;
                    if (n3 < inputPtr && inputBuffer[n2] == 112 && inputBuffer[n3] == 59) {
                        super._inputPtr = n2 + 2;
                        return 38;
                    }
                }
                else if (b2 == 112) {
                    final int n4 = n2 + 2;
                    if (n4 < inputPtr && inputBuffer[n2] == 111 && inputBuffer[n2 + 1] == 115 && inputBuffer[n4] == 59) {
                        super._inputPtr = n2 + 3;
                        return 39;
                    }
                }
            }
            else if (b == 103) {
                if (inputBuffer[n] == 116 && inputBuffer[n + 1] == 59) {
                    super._inputPtr = n + 2;
                    return 62;
                }
            }
            else if (b == 108) {
                if (inputBuffer[n] == 116 && inputBuffer[n + 1] == 59) {
                    super._inputPtr = n + 2;
                    return 60;
                }
            }
            else if (b == 113) {
                final int n5 = n + 3;
                if (n5 < inputPtr && inputBuffer[n] == 117 && inputBuffer[n + 1] == 111 && inputBuffer[n + 2] == 116 && inputBuffer[n5] == 59) {
                    super._inputPtr = n + 4;
                    return 39;
                }
            }
        }
        return 0;
    }
    
    private final boolean skipPending() {
        final int inputPtr = super._inputPtr;
        final int inputEnd = super._inputEnd;
        if (inputPtr >= inputEnd) {
            return false;
        }
        final int pendingInput = super._pendingInput;
        int pendingInput4 = 0;
        Label_0138: {
            if (pendingInput < 0) {
                do {
                    final int pendingInput2 = super._pendingInput;
                    if (pendingInput2 == -1) {
                        super._pendingInput = 0;
                        final byte[] inputBuffer = this._inputBuffer;
                        final int inputPtr2 = super._inputPtr;
                        if (inputBuffer[inputPtr2] == 10) {
                            super._inputPtr = inputPtr2 + 1;
                        }
                        this.markLF();
                        return true;
                    }
                    int pendingInput3 = 0;
                    switch (pendingInput2) {
                        default: {
                            this.throwInternal();
                            continue;
                        }
                        case -80: {
                            final byte b = this._inputBuffer[super._inputPtr++];
                            if (b == 35) {
                                pendingInput3 = -81;
                                break;
                            }
                            final PName newEntityName = this.parseNewEntityName(b);
                            if (newEntityName == null) {
                                pendingInput4 = -84;
                                break Label_0138;
                            }
                            if (this.decodeGeneralEntity(newEntityName) == 0) {
                                super._tokenName = newEntityName;
                                super._nextEvent = 9;
                            }
                            super._pendingInput = 0;
                            return true;
                        }
                        case -81: {
                            super._entityValue = 0;
                            final byte[] inputBuffer2 = this._inputBuffer;
                            final int inputPtr3 = super._inputPtr;
                            if (inputBuffer2[inputPtr3] == 120) {
                                super._inputPtr = inputPtr3 + 1;
                                if (this.decodeHexEntity()) {
                                    super._pendingInput = 0;
                                    return true;
                                }
                                pendingInput4 = -83;
                                break Label_0138;
                            }
                            else {
                                if (this.decodeDecEntity()) {
                                    super._pendingInput = 0;
                                    return true;
                                }
                                pendingInput4 = -82;
                                break Label_0138;
                            }
                            break;
                        }
                        case -82: {
                            if (this.decodeDecEntity()) {
                                super._pendingInput = 0;
                                return true;
                            }
                            return false;
                        }
                        case -83: {
                            if (this.decodeHexEntity()) {
                                super._pendingInput = 0;
                                return true;
                            }
                            return false;
                        }
                        case -84: {
                            final PName entityName = this.parseEntityName();
                            if (entityName == null) {
                                return false;
                            }
                            if (this.decodeGeneralEntity(entityName) == 0) {
                                super._tokenName = entityName;
                                super._nextEvent = 9;
                            }
                            super._pendingInput = 0;
                            return true;
                        }
                        case -85: {
                            final byte[] inputBuffer3 = this._inputBuffer;
                            final int inputPtr4 = super._inputPtr;
                            if (inputBuffer3[inputPtr4] != 93) {
                                super._pendingInput = 0;
                                return true;
                            }
                            super._inputPtr = inputPtr4 + 1;
                            pendingInput3 = -86;
                            break;
                        }
                        case -86: {
                            final byte[] inputBuffer4 = this._inputBuffer;
                            final int inputPtr5 = super._inputPtr;
                            final byte b2 = inputBuffer4[inputPtr5];
                            if (b2 == 93) {
                                super._inputPtr = inputPtr5 + 1;
                                continue;
                            }
                            if (b2 == 62) {
                                super._inputPtr = inputPtr5 + 1;
                                this.reportInputProblem("Encountered ']]>' in text segment");
                            }
                            super._pendingInput = 0;
                            return true;
                        }
                    }
                    super._pendingInput = pendingInput3;
                } while (super._inputPtr < super._inputEnd);
                return false;
            }
            final int[] text_CHARS = super._charTypes.TEXT_CHARS;
            final int n = pendingInput & 0xFF;
            final int n2 = text_CHARS[n];
            Label_0823: {
                if (n2 != 5) {
                    int n8 = 0;
                    Label_0570: {
                        int n3;
                        if (n2 != 6) {
                            if (n2 != 7) {
                                this.throwInternal();
                                break Label_0823;
                            }
                            final byte[] inputBuffer5 = this._inputBuffer;
                            final int inputPtr6 = inputPtr + 1;
                            super._inputPtr = inputPtr6;
                            n3 = (inputBuffer5[inputPtr] & 0xFF);
                            final int n4 = pendingInput >> 8;
                            if (n4 == 0) {
                                if (inputPtr6 < inputEnd) {
                                    final int inputPtr7 = inputPtr6 + 1;
                                    super._inputPtr = inputPtr7;
                                    final int n5 = inputBuffer5[inputPtr6] & 0xFF;
                                    if (inputPtr7 >= inputEnd) {
                                        pendingInput4 = (n3 << 8 | pendingInput | n5 << 16);
                                        break Label_0138;
                                    }
                                    super._inputPtr = inputPtr7 + 1;
                                    this.decodeUtf8_4(pendingInput, n3, n5, inputBuffer5[inputPtr7] & 0xFF);
                                    break Label_0823;
                                }
                            }
                            else {
                                final int n6 = n4 & 0xFF;
                                final int n7 = pendingInput >> 16;
                                if (n7 != 0) {
                                    this.decodeUtf8_4(n, n6, n7, n3);
                                    break Label_0823;
                                }
                                if (inputPtr6 >= inputEnd) {
                                    n8 = n3 << 16;
                                    break Label_0570;
                                }
                                super._inputPtr = inputPtr6 + 1;
                                this.decodeUtf8_4(n, n6, n3, inputBuffer5[inputPtr6] & 0xFF);
                                break Label_0823;
                            }
                        }
                        else {
                            final byte[] inputBuffer6 = this._inputBuffer;
                            final int inputPtr8 = inputPtr + 1;
                            super._inputPtr = inputPtr8;
                            n3 = (inputBuffer6[inputPtr] & 0xFF);
                            final int n9 = pendingInput >> 8;
                            if (n9 != 0) {
                                this.decodeUtf8_3(n, n9, n3);
                                break Label_0823;
                            }
                            if (inputPtr8 < inputEnd) {
                                super._inputPtr = inputPtr8 + 1;
                                this.decodeUtf8_3(pendingInput, n3, inputBuffer6[inputPtr8] & 0xFF);
                                break Label_0823;
                            }
                        }
                        n8 = n3 << 8;
                    }
                    pendingInput4 = (n8 | pendingInput);
                    break Label_0138;
                }
                this.skipUtf8_2(pendingInput);
            }
            super._pendingInput = 0;
            return true;
        }
        super._pendingInput = pendingInput4;
        return false;
    }
    
    @Override
    public final byte _currentByte() {
        return this._inputBuffer[super._inputPtr];
    }
    
    @Override
    public final byte _nextByte() {
        return this._inputBuffer[super._inputPtr++];
    }
    
    @Override
    public final byte _prevByte() {
        return this._inputBuffer[super._inputPtr - 1];
    }
    
    @Override
    public boolean asyncSkipSpace() {
        while (true) {
            int inputPtr = super._inputPtr;
            final int inputEnd = super._inputEnd;
            if (inputPtr >= inputEnd) {
                break;
            }
            final byte[] inputBuffer = this._inputBuffer;
            final byte b = inputBuffer[inputPtr];
            if ((b & 0xFF) > 32) {
                if (super._pendingInput == -1) {
                    this.markLF();
                    super._pendingInput = 0;
                }
                return true;
            }
            ++inputPtr;
            super._inputPtr = inputPtr;
            if (b != 10) {
                if (b == 13) {
                    if (inputPtr >= inputEnd) {
                        super._pendingInput = -1;
                        break;
                    }
                    if (inputBuffer[inputPtr] == 10) {
                        super._inputPtr = inputPtr + 1;
                    }
                }
                else {
                    if (b != 32 && b != 9) {
                        this.throwInvalidSpace(b);
                        continue;
                    }
                    continue;
                }
            }
            this.markLF();
        }
        return false;
    }
    
    public final boolean decodeDecEntity() {
        int entityValue = super._entityValue;
        while (true) {
            final int inputPtr = super._inputPtr;
            if (inputPtr >= super._inputEnd) {
                super._entityValue = entityValue;
                return false;
            }
            final byte[] inputBuffer = this._inputBuffer;
            super._inputPtr = inputPtr + 1;
            final byte b = inputBuffer[inputPtr];
            if (b == 59) {
                super._entityValue = entityValue;
                return true;
            }
            final int n = b - 48;
            if (n < 0 || n > 9) {
                this.throwUnexpectedChar(this.decodeCharForError(b), " expected a digit (0 - 9) for character entity");
            }
            final int entityValue2 = entityValue * 10 + n;
            if ((entityValue = entityValue2) <= 1114111) {
                continue;
            }
            super._entityValue = entityValue2;
            this.reportEntityOverflow();
            entityValue = entityValue2;
        }
    }
    
    public final int decodeGeneralEntity(final PName pName) {
        final byte b = this._inputBuffer[super._inputPtr++];
        if (b != 59) {
            final int decodeCharForError = this.decodeCharForError(b);
            final StringBuilder sb = new StringBuilder();
            sb.append(" expected ';' following entity name (\"");
            sb.append(pName.getPrefixedName());
            sb.append("\")");
            this.throwUnexpectedChar(decodeCharForError, sb.toString());
        }
        final String prefixedName = pName.getPrefixedName();
        if (prefixedName == "amp") {
            return 38;
        }
        if (prefixedName == "lt") {
            return 60;
        }
        if (prefixedName == "apos") {
            return 39;
        }
        if (prefixedName == "quot") {
            return 34;
        }
        if (prefixedName == "gt") {
            return 62;
        }
        return 0;
    }
    
    public final boolean decodeHexEntity() {
        int entityValue = super._entityValue;
        while (true) {
            final int inputPtr = super._inputPtr;
            if (inputPtr >= super._inputEnd) {
                super._entityValue = entityValue;
                return false;
            }
            final byte[] inputBuffer = this._inputBuffer;
            super._inputPtr = inputPtr + 1;
            final byte b = inputBuffer[inputPtr];
            if (b == 59) {
                super._entityValue = entityValue;
                return true;
            }
            int n = 0;
            Label_0126: {
                if (b <= 57 && b >= 48) {
                    n = b - 48;
                }
                else {
                    if (b <= 70 && b >= 65) {
                        n = b - 65;
                    }
                    else {
                        if (b > 102 || b < 97) {
                            this.throwUnexpectedChar(this.decodeCharForError(b), " expected a hex digit (0-9a-fA-F) for character entity");
                            n = b;
                            break Label_0126;
                        }
                        n = b - 97;
                    }
                    n += 10;
                }
            }
            final int entityValue2 = entityValue = (entityValue << 4) + n;
            if (entityValue2 <= 1114111) {
                continue;
            }
            super._entityValue = entityValue2;
            this.reportEntityOverflow();
            entityValue = entityValue2;
        }
    }
    
    public final int decodeUtf8_2(final int n) {
        final byte[] inputBuffer = this._inputBuffer;
        final int inputPtr = super._inputPtr;
        final int inputPtr2 = inputPtr + 1;
        super._inputPtr = inputPtr2;
        final byte b = inputBuffer[inputPtr];
        if ((b & 0xC0) != 0x80) {
            this.reportInvalidOther(b & 0xFF, inputPtr2);
        }
        return (n & 0x1F) << 6 | (b & 0x3F);
    }
    
    public final int decodeUtf8_3(int n) {
        final int n2 = n & 0xF;
        final byte[] inputBuffer = this._inputBuffer;
        n = super._inputPtr;
        final int inputPtr = n + 1;
        super._inputPtr = inputPtr;
        n = inputBuffer[n];
        if ((n & 0xC0) != 0x80) {
            this.reportInvalidOther(n & 0xFF, inputPtr);
        }
        final byte[] inputBuffer2 = this._inputBuffer;
        final int inputPtr2 = super._inputPtr;
        final int inputPtr3 = inputPtr2 + 1;
        super._inputPtr = inputPtr3;
        final byte b = inputBuffer2[inputPtr2];
        if ((b & 0xC0) != 0x80) {
            this.reportInvalidOther(b & 0xFF, inputPtr3);
        }
        final int n3 = n = (((n & 0x3F) | n2 << 6) << 6 | (b & 0x3F));
        if (n2 >= 13 && (n = n3) >= 55296) {
            if (n3 >= 57344) {
                n = n3;
                if (n3 < 65534 || (n = n3) > 65535) {
                    return n;
                }
            }
            n = this.handleInvalidXmlChar(n3);
        }
        return n;
    }
    
    public final int decodeUtf8_3(final int n, int handleInvalidXmlChar, int n2) {
        if ((handleInvalidXmlChar & 0xC0) != 0x80) {
            this.reportInvalidOther(handleInvalidXmlChar & 0xFF, super._inputPtr - 1);
        }
        if ((n2 & 0xC0) != 0x80) {
            this.reportInvalidOther(n2 & 0xFF, super._inputPtr);
        }
        n2 = (handleInvalidXmlChar = ((handleInvalidXmlChar & 0x3F) << 6 | (n & 0xF) << 12 | (n2 & 0x3F)));
        if (n >= 13 && (handleInvalidXmlChar = n2) >= 55296) {
            if (n2 >= 57344) {
                handleInvalidXmlChar = n2;
                if (n2 < 65534 || (handleInvalidXmlChar = n2) > 65535) {
                    return handleInvalidXmlChar;
                }
            }
            handleInvalidXmlChar = this.handleInvalidXmlChar(n2);
        }
        return handleInvalidXmlChar;
    }
    
    public final int decodeUtf8_4(final int n) {
        final byte[] inputBuffer = this._inputBuffer;
        final int inputPtr = super._inputPtr;
        final int inputPtr2 = inputPtr + 1;
        super._inputPtr = inputPtr2;
        final byte b = inputBuffer[inputPtr];
        if ((b & 0xC0) != 0x80) {
            this.reportInvalidOther(b & 0xFF, inputPtr2);
        }
        final byte[] inputBuffer2 = this._inputBuffer;
        final int inputPtr3 = super._inputPtr;
        final int inputPtr4 = inputPtr3 + 1;
        super._inputPtr = inputPtr4;
        final byte b2 = inputBuffer2[inputPtr3];
        if ((b2 & 0xC0) != 0x80) {
            this.reportInvalidOther(b2 & 0xFF, inputPtr4);
        }
        final byte[] inputBuffer3 = this._inputBuffer;
        final int inputPtr5 = super._inputPtr;
        final int inputPtr6 = inputPtr5 + 1;
        super._inputPtr = inputPtr6;
        final byte b3 = inputBuffer3[inputPtr5];
        if ((b3 & 0xC0) != 0x80) {
            this.reportInvalidOther(b3 & 0xFF, inputPtr6);
        }
        return ((((n & 0x7) << 6 | (b & 0x3F)) << 6 | (b2 & 0x3F)) << 6 | (b3 & 0x3F)) - 65536;
    }
    
    public final int decodeUtf8_4(final int n, final int n2, final int n3, final int n4) {
        if ((n2 & 0xC0) != 0x80) {
            this.reportInvalidOther(n2 & 0xFF, super._inputPtr - 2);
        }
        if ((n3 & 0xC0) != 0x80) {
            this.reportInvalidOther(n3 & 0xFF, super._inputPtr - 1);
        }
        if ((n4 & 0xC0) != 0x80) {
            this.reportInvalidOther(n4 & 0xFF, super._inputPtr);
        }
        return ((((n & 0x7) << 6 | (n2 & 0x3F)) << 6 | (n3 & 0x3F)) << 6 | (n4 & 0x3F)) - 65536;
    }
    
    @Override
    public void feedInput(final byte[] inputBuffer, final int inputPtr, final int origBufferLen) {
        if (super._inputPtr < super._inputEnd) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Still have ");
            sb.append(super._inputEnd - super._inputPtr);
            sb.append(" unread bytes");
            throw new XMLStreamException(sb.toString());
        }
        if (!super._endOfInput) {
            final long pastBytesOrChars = super._pastBytesOrChars;
            final int origBufferLen2 = this._origBufferLen;
            super._pastBytesOrChars = pastBytesOrChars + origBufferLen2;
            super._rowStartOffset -= origBufferLen2;
            this._inputBuffer = inputBuffer;
            super._inputPtr = inputPtr;
            super._inputEnd = inputPtr + origBufferLen;
            this._origBufferLen = origBufferLen;
            return;
        }
        throw new XMLStreamException("Already closed, can not feed more input");
    }
    
    @Override
    public final void finishCharacters() {
        if (super._pendingInput != 0) {
            this.throwInternal();
        }
        final int[] text_CHARS = super._charTypes.TEXT_CHARS;
        final byte[] inputBuffer = this._inputBuffer;
        char[] array = super._textBuilder.getBufferWithoutReset();
        int currentLength = super._textBuilder.getCurrentLength();
        Label_0801: {
            Block_23: {
            Label_0746:
                while (true) {
                    final int inputPtr = super._inputPtr;
                    if (inputPtr >= super._inputEnd) {
                        break Label_0801;
                    }
                    final int length = array.length;
                    final int n = 0;
                    int n2;
                    if ((n2 = currentLength) >= length) {
                        array = super._textBuilder.finishCurrentSegment();
                        n2 = 0;
                    }
                    final int inputEnd = super._inputEnd;
                    final int n3 = array.length - n2 + inputPtr;
                    currentLength = n2;
                    int i = inputPtr;
                    int n4;
                    if (n3 < (n4 = inputEnd)) {
                        n4 = n3;
                        i = inputPtr;
                        currentLength = n2;
                    }
                    while (i < n4) {
                        final int inputPtr2 = i + 1;
                        final int n5 = inputBuffer[i] & 0xFF;
                        final int n6 = text_CHARS[n5];
                        if (n6 != 0) {
                            super._inputPtr = inputPtr2;
                            int pendingInput = 0;
                            Label_0715: {
                                int n17 = 0;
                                int n18 = 0;
                                Label_0549: {
                                    char[] array2 = null;
                                    int n7 = 0;
                                    int n8 = 0;
                                    Label_0840: {
                                        int n15 = 0;
                                        int n16 = 0;
                                        Label_0610: {
                                            switch (n6) {
                                                default: {
                                                    array2 = array;
                                                    n7 = currentLength;
                                                    n8 = n5;
                                                    break Label_0840;
                                                }
                                                case 11: {
                                                    byte b = 0;
                                                    int n9 = 1;
                                                    byte b2;
                                                    while (true) {
                                                        final int inputPtr3 = super._inputPtr;
                                                        b2 = b;
                                                        if (inputPtr3 >= super._inputEnd) {
                                                            break;
                                                        }
                                                        b = inputBuffer[inputPtr3];
                                                        if (b != 93) {
                                                            b2 = b;
                                                            break;
                                                        }
                                                        super._inputPtr = inputPtr3 + 1;
                                                        ++n9;
                                                    }
                                                    char[] finishCurrentSegment = array;
                                                    int n10 = currentLength;
                                                    int n11 = n9;
                                                    if (b2 == 62) {
                                                        finishCurrentSegment = array;
                                                        n10 = currentLength;
                                                        if ((n11 = n9) > 1) {
                                                            this.reportIllegalCDataEnd();
                                                            n11 = n9;
                                                            n10 = currentLength;
                                                            finishCurrentSegment = array;
                                                        }
                                                    }
                                                    while (true) {
                                                        --n11;
                                                        array2 = finishCurrentSegment;
                                                        n7 = n10;
                                                        n8 = n5;
                                                        if (n11 <= 0) {
                                                            break Label_0840;
                                                        }
                                                        final int n12 = n10 + 1;
                                                        finishCurrentSegment[n10] = ']';
                                                        if (n12 >= finishCurrentSegment.length) {
                                                            finishCurrentSegment = super._textBuilder.finishCurrentSegment();
                                                            n10 = 0;
                                                        }
                                                        else {
                                                            n10 = n12;
                                                        }
                                                    }
                                                    break;
                                                }
                                                case 10: {
                                                    final int handleEntityInCharacters = this.handleEntityInCharacters();
                                                    if (handleEntityInCharacters == 0) {
                                                        break Label_0746;
                                                    }
                                                    array2 = array;
                                                    n7 = currentLength;
                                                    n8 = handleEntityInCharacters;
                                                    if (handleEntityInCharacters >> 16 == 0) {
                                                        break Label_0840;
                                                    }
                                                    final int n13 = handleEntityInCharacters - 65536;
                                                    final int n14 = currentLength + 1;
                                                    array[currentLength] = (char)(n13 >> 10 | 0xD800);
                                                    n15 = n13;
                                                    if ((n16 = n14) >= array.length) {
                                                        n15 = n13;
                                                        break;
                                                    }
                                                    break Label_0610;
                                                }
                                                case 7: {
                                                    final int inputEnd2 = super._inputEnd;
                                                    if (inputEnd2 - inputPtr2 < 3) {
                                                        pendingInput = n5;
                                                        if (inputEnd2 <= inputPtr2) {
                                                            break Label_0715;
                                                        }
                                                        final byte[] inputBuffer2 = this._inputBuffer;
                                                        final int inputPtr4 = inputPtr2 + 1;
                                                        super._inputPtr = inputPtr4;
                                                        n17 = (pendingInput = (n5 | (inputBuffer2[inputPtr2] & 0xFF) << 8));
                                                        if (inputEnd2 > inputPtr4) {
                                                            super._inputPtr = inputPtr4 + 1;
                                                            n18 = (inputBuffer2[inputPtr4] & 0xFF) << 16;
                                                            break Label_0549;
                                                        }
                                                        break Label_0715;
                                                    }
                                                    else {
                                                        final int decodeUtf8_4 = this.decodeUtf8_4(n5);
                                                        final int n19 = currentLength + 1;
                                                        array[currentLength] = (char)(decodeUtf8_4 >> 10 | 0xD800);
                                                        n15 = decodeUtf8_4;
                                                        if ((n16 = n19) >= array.length) {
                                                            n15 = decodeUtf8_4;
                                                            break;
                                                        }
                                                        break Label_0610;
                                                    }
                                                    break;
                                                }
                                                case 6: {
                                                    final int inputEnd3 = super._inputEnd;
                                                    if (inputEnd3 - inputPtr2 >= 2) {
                                                        n8 = this.decodeUtf8_3(n5);
                                                        array2 = array;
                                                        n7 = currentLength;
                                                        break Label_0840;
                                                    }
                                                    pendingInput = n5;
                                                    if (inputEnd3 > inputPtr2) {
                                                        final byte[] inputBuffer3 = this._inputBuffer;
                                                        super._inputPtr = inputPtr2 + 1;
                                                        n18 = (inputBuffer3[inputPtr2] & 0xFF) << 8;
                                                        n17 = n5;
                                                        break Label_0549;
                                                    }
                                                    break Label_0715;
                                                }
                                                case 5: {
                                                    if (inputPtr2 >= super._inputEnd) {
                                                        pendingInput = n5;
                                                        break Label_0715;
                                                    }
                                                    n8 = this.decodeUtf8_2(n5);
                                                    array2 = array;
                                                    n7 = currentLength;
                                                    break Label_0840;
                                                }
                                                case 4: {
                                                    this.reportInvalidInitial(n5);
                                                }
                                                case 9: {
                                                    break Label_0746;
                                                }
                                                case 3: {
                                                    this.markLF();
                                                    array2 = array;
                                                    n7 = currentLength;
                                                    n8 = n5;
                                                    break Label_0840;
                                                }
                                                case 1: {
                                                    this.handleInvalidXmlChar(n5);
                                                }
                                                case 2: {
                                                    final int inputPtr5 = super._inputPtr;
                                                    if (inputPtr5 >= super._inputEnd) {
                                                        break Block_23;
                                                    }
                                                    if (inputBuffer[inputPtr5] == 10) {
                                                        super._inputPtr = inputPtr5 + 1;
                                                    }
                                                    this.markLF();
                                                    n8 = 10;
                                                    n7 = currentLength;
                                                    array2 = array;
                                                    break Label_0840;
                                                }
                                            }
                                            array = super._textBuilder.finishCurrentSegment();
                                            n16 = n;
                                        }
                                        final int n20 = (n15 & 0x3FF) | 0xDC00;
                                        array2 = array;
                                        n7 = n16;
                                        n8 = n20;
                                    }
                                    array2[n7] = (char)n8;
                                    currentLength = n7 + 1;
                                    array = array2;
                                    continue Label_0801;
                                }
                                pendingInput = (n17 | n18);
                            }
                            super._pendingInput = pendingInput;
                            break Label_0801;
                        }
                        array[currentLength] = (char)n5;
                        i = inputPtr2;
                        ++currentLength;
                    }
                    super._inputPtr = i;
                }
                --super._inputPtr;
                break Label_0801;
            }
            super._pendingInput = -1;
        }
        super._textBuilder.setCurrentLength(currentLength);
    }
    
    public final int finishCharactersCoalescing() {
        if (super._pendingInput != 0 && !this.handleAndAppendPending()) {
            return 257;
        }
        throw new UnsupportedOperationException();
    }
    
    @Override
    public boolean handleAttrValue() {
        if (super._pendingInput != 0) {
            if (!this.handleAttrValuePending()) {
                return false;
            }
            super._pendingInput = 0;
        }
        char[] array = super._attrCollector.continueValue();
        final int[] attr_CHARS = super._charTypes.ATTR_CHARS;
        final byte elemAttrQuote = super._elemAttrQuote;
    Label_0045:
        while (super._inputPtr < super._inputEnd) {
            char[] valueBufferFull = array;
            if (super._elemAttrPtr >= array.length) {
                valueBufferFull = super._attrCollector.valueBufferFull();
            }
            final int inputEnd = super._inputEnd;
            final int n = super._inputPtr + (valueBufferFull.length - super._elemAttrPtr);
            int n2;
            if (n < (n2 = inputEnd)) {
                n2 = n;
            }
            byte[] inputBuffer;
            int inputPtr2;
            int pendingInput;
            int n3;
            while (true) {
                final int inputPtr = super._inputPtr;
                array = valueBufferFull;
                if (inputPtr >= n2) {
                    continue Label_0045;
                }
                inputBuffer = this._inputBuffer;
                inputPtr2 = inputPtr + 1;
                super._inputPtr = inputPtr2;
                pendingInput = (inputBuffer[inputPtr] & 0xFF);
                n3 = attr_CHARS[pendingInput];
                if (n3 != 0) {
                    break;
                }
                valueBufferFull[super._elemAttrPtr++] = (char)pendingInput;
            }
            int n4 = 0;
            Label_0654: {
                if (n3 != 14) {
                    switch (n3) {
                        default: {
                            array = valueBufferFull;
                            n4 = pendingInput;
                            break Label_0654;
                        }
                        case 7: {
                            final int inputEnd2 = super._inputEnd;
                            if (inputEnd2 - inputPtr2 < 3) {
                                int pendingInput2 = pendingInput;
                                if (inputEnd2 > inputPtr2) {
                                    final int inputPtr3 = inputPtr2 + 1;
                                    super._inputPtr = inputPtr3;
                                    pendingInput2 = (pendingInput | (inputBuffer[inputPtr2] & 0xFF) << 8);
                                    if (inputEnd2 > inputPtr3) {
                                        super._inputPtr = inputPtr3 + 1;
                                        pendingInput2 |= (inputBuffer[inputPtr3] & 0xFF) << 16;
                                    }
                                }
                                super._pendingInput = pendingInput2;
                                return false;
                            }
                            final int decodeUtf8_4 = this.decodeUtf8_4(pendingInput);
                            final int elemAttrPtr = super._elemAttrPtr;
                            final int elemAttrPtr2 = elemAttrPtr + 1;
                            super._elemAttrPtr = elemAttrPtr2;
                            valueBufferFull[elemAttrPtr] = (char)(decodeUtf8_4 >> 10 | 0xD800);
                            final int n5 = (decodeUtf8_4 & 0x3FF) | 0xDC00;
                            array = valueBufferFull;
                            n4 = n5;
                            if (elemAttrPtr2 >= valueBufferFull.length) {
                                n4 = n5;
                                break;
                            }
                            break Label_0654;
                        }
                        case 6: {
                            final int inputEnd3 = super._inputEnd;
                            if (inputEnd3 - inputPtr2 < 2) {
                                int pendingInput3 = pendingInput;
                                if (inputEnd3 > inputPtr2) {
                                    super._inputPtr = inputPtr2 + 1;
                                    pendingInput3 = (pendingInput | (inputBuffer[inputPtr2] & 0xFF) << 8);
                                }
                                super._pendingInput = pendingInput3;
                                return false;
                            }
                            n4 = this.decodeUtf8_3(pendingInput);
                            array = valueBufferFull;
                            break Label_0654;
                        }
                        case 5: {
                            if (inputPtr2 >= super._inputEnd) {
                                super._pendingInput = pendingInput;
                                return false;
                            }
                            n4 = this.decodeUtf8_2(pendingInput);
                            array = valueBufferFull;
                            break Label_0654;
                        }
                        case 4: {
                            this.reportInvalidInitial(pendingInput);
                        }
                        case 9: {
                            this.throwUnexpectedChar(pendingInput, "'<' not allowed in attribute value");
                        }
                        case 10: {
                            final int handleEntityInAttributeValue = this.handleEntityInAttributeValue();
                            if (handleEntityInAttributeValue <= 0) {
                                if (handleEntityInAttributeValue < 0) {
                                    return false;
                                }
                                this.reportUnexpandedEntityInAttr(super._elemAttrName, false);
                            }
                            array = valueBufferFull;
                            n4 = handleEntityInAttributeValue;
                            if (handleEntityInAttributeValue >> 16 == 0) {
                                break Label_0654;
                            }
                            final int n6 = handleEntityInAttributeValue - 65536;
                            final int elemAttrPtr3 = super._elemAttrPtr;
                            final int elemAttrPtr4 = elemAttrPtr3 + 1;
                            super._elemAttrPtr = elemAttrPtr4;
                            valueBufferFull[elemAttrPtr3] = (char)(n6 >> 10 | 0xD800);
                            final int n7 = (n6 & 0x3FF) | 0xDC00;
                            array = valueBufferFull;
                            n4 = n7;
                            if (elemAttrPtr4 >= valueBufferFull.length) {
                                n4 = n7;
                                break;
                            }
                            break Label_0654;
                        }
                        case 1: {
                            this.handleInvalidXmlChar(pendingInput);
                        }
                        case 2: {
                            final int inputPtr4 = super._inputPtr;
                            if (inputPtr4 >= super._inputEnd) {
                                super._pendingInput = -1;
                                return false;
                            }
                            if (this._inputBuffer[inputPtr4] == 10) {
                                super._inputPtr = inputPtr4 + 1;
                            }
                        }
                        case 3: {
                            this.markLF();
                        }
                        case 8: {
                            n4 = 32;
                            array = valueBufferFull;
                            break Label_0654;
                        }
                    }
                    array = super._attrCollector.valueBufferFull();
                }
                else {
                    array = valueBufferFull;
                    if ((n4 = pendingInput) == elemAttrQuote) {
                        return true;
                    }
                }
            }
            array[super._elemAttrPtr++] = (char)n4;
        }
        return false;
    }
    
    public final int handleCDataPending() {
        final int pendingInput = super._pendingInput;
        int n = 0;
        if (pendingInput == -30) {
            int inputPtr = super._inputPtr;
            final int inputEnd = super._inputEnd;
            if (inputPtr >= inputEnd) {
                return 257;
            }
            if (this._inputBuffer[inputPtr] != 93) {
                super._textBuilder.append(']');
                return super._pendingInput = 0;
            }
            ++inputPtr;
            super._inputPtr = inputPtr;
            super._pendingInput = -31;
            if (inputPtr >= inputEnd) {
                return 257;
            }
        }
        while (super._pendingInput == -31) {
            final int inputPtr2 = super._inputPtr;
            if (inputPtr2 >= super._inputEnd) {
                return 257;
            }
            final byte[] inputBuffer = this._inputBuffer;
            final int inputPtr3 = inputPtr2 + 1;
            super._inputPtr = inputPtr3;
            final byte b = inputBuffer[inputPtr2];
            if (b == 62) {
                super._pendingInput = 0;
                super._state = 0;
                super._nextEvent = 257;
                return 12;
            }
            if (b != 93) {
                super._inputPtr = inputPtr3 - 1;
                super._textBuilder.append("]]");
                return super._pendingInput = 0;
            }
            super._textBuilder.append(']');
        }
        if (!this.handleAndAppendPending()) {
            n = 257;
        }
        return n;
    }
    
    @Override
    public final int handleComment() {
        final int state = super._state;
        if (state == 1) {
            return this.parseCommentContents();
        }
        final int inputPtr = super._inputPtr;
        if (inputPtr >= super._inputEnd) {
            return 257;
        }
        final byte[] inputBuffer = this._inputBuffer;
        super._inputPtr = inputPtr + 1;
        final byte b = inputBuffer[inputPtr];
        if (state == 0) {
            if (b != 45) {
                this.reportTreeUnexpChar(this.decodeCharForError(b), " (expected '-' for COMMENT)");
            }
            super._state = 1;
            super._textBuilder.resetWithEmpty();
            return this.parseCommentContents();
        }
        if (state == 3) {
            if (b != 62) {
                this.reportDoubleHyphenInComments();
            }
            super._state = 0;
            super._nextEvent = 257;
            return 5;
        }
        return this.throwInternal();
    }
    
    public int handleCommentPending() {
        int inputPtr = super._inputPtr;
        final int inputEnd = super._inputEnd;
        int n = 257;
        if (inputPtr >= inputEnd) {
            return 257;
        }
        if (super._pendingInput == -20) {
            if (this._inputBuffer[inputPtr] != 45) {
                super._pendingInput = 0;
                super._textBuilder.append("-");
                return 0;
            }
            ++inputPtr;
            super._inputPtr = inputPtr;
            super._pendingInput = -21;
            if (inputPtr >= inputEnd) {
                return 257;
            }
        }
        if (super._pendingInput == -21) {
            super._pendingInput = 0;
            if (this._inputBuffer[super._inputPtr++] != 62) {
                this.reportDoubleHyphenInComments();
            }
            super._state = 0;
            super._nextEvent = 257;
            return 5;
        }
        if (this.handleAndAppendPending()) {
            n = 0;
        }
        return n;
    }
    
    @Override
    public final boolean handleDTDInternalSubset(final boolean b) {
        char[] array;
        int currentLength;
        if (b) {
            array = super._textBuilder.resetWithEmpty();
            super._elemAttrQuote = 0;
            super._inDtdDeclaration = false;
            currentLength = 0;
        }
        else {
            if (super._pendingInput != 0 && !this.handleAndAppendPending()) {
                return false;
            }
            array = super._textBuilder.getBufferWithoutReset();
            currentLength = super._textBuilder.getCurrentLength();
        }
        final int[] dtd_CHARS = super._charTypes.DTD_CHARS;
        final byte[] inputBuffer = this._inputBuffer;
    Label_0785:
        while (true) {
            while (super._inputPtr < super._inputEnd) {
                int n = currentLength;
                char[] array2 = array;
                if (currentLength >= array.length) {
                    array2 = super._textBuilder.finishCurrentSegment();
                    n = 0;
                }
                final int inputEnd = super._inputEnd;
                final int n2 = super._inputPtr + (array2.length - n);
                int n3 = n;
                int n4;
                if (n2 < (n4 = inputEnd)) {
                    n4 = n2;
                    n3 = n;
                }
                int inputPtr2;
                int n5;
                int n6;
                while (true) {
                    final int inputPtr = super._inputPtr;
                    currentLength = n3;
                    array = array2;
                    if (inputPtr >= n4) {
                        continue Label_0785;
                    }
                    inputPtr2 = inputPtr + 1;
                    super._inputPtr = inputPtr2;
                    n5 = (inputBuffer[inputPtr] & 0xFF);
                    n6 = dtd_CHARS[n5];
                    if (n6 != 0) {
                        break;
                    }
                    array2[n3] = (char)n5;
                    ++n3;
                }
                int pendingInput = 0;
                Label_0646: {
                    int n11 = 0;
                    Label_0486: {
                        int n7 = 0;
                        int n8 = 0;
                        switch (n6) {
                            default: {
                                n7 = n3;
                                n8 = n5;
                                array = array2;
                                break;
                            }
                            case 11: {
                                n7 = n3;
                                n8 = n5;
                                array = array2;
                                if (super._inDtdDeclaration) {
                                    break;
                                }
                                n7 = n3;
                                n8 = n5;
                                array = array2;
                                if (super._elemAttrQuote == 0) {
                                    super._textBuilder.setCurrentLength(n3);
                                    return true;
                                }
                                break;
                            }
                            case 10: {
                                n7 = n3;
                                n8 = n5;
                                array = array2;
                                if (super._elemAttrQuote == 0) {
                                    super._inDtdDeclaration = false;
                                    n7 = n3;
                                    n8 = n5;
                                    array = array2;
                                    break;
                                }
                                break;
                            }
                            case 9: {
                                n7 = n3;
                                n8 = n5;
                                array = array2;
                                if (!super._inDtdDeclaration) {
                                    super._inDtdDeclaration = true;
                                    n7 = n3;
                                    n8 = n5;
                                    array = array2;
                                    break;
                                }
                                break;
                            }
                            case 7: {
                                final int inputEnd2 = super._inputEnd;
                                if (inputEnd2 - inputPtr2 >= 3) {
                                    final int decodeUtf8_4 = this.decodeUtf8_4(n5);
                                    final int n9 = n3 + 1;
                                    array2[n3] = (char)(decodeUtf8_4 >> 10 | 0xD800);
                                    int n10;
                                    if (n9 >= array2.length) {
                                        array2 = super._textBuilder.finishCurrentSegment();
                                        n10 = 0;
                                    }
                                    else {
                                        n10 = n9;
                                    }
                                    n8 = (0xDC00 | (decodeUtf8_4 & 0x3FF));
                                    n7 = n10;
                                    array = array2;
                                    break;
                                }
                                pendingInput = n5;
                                if (inputEnd2 <= inputPtr2) {
                                    break Label_0646;
                                }
                                final byte[] inputBuffer2 = this._inputBuffer;
                                final int inputPtr3 = inputPtr2 + 1;
                                super._inputPtr = inputPtr3;
                                n5 = (pendingInput = (n5 | (inputBuffer2[inputPtr2] & 0xFF) << 8));
                                if (inputEnd2 > inputPtr3) {
                                    super._inputPtr = inputPtr3 + 1;
                                    n11 = (inputBuffer2[inputPtr3] & 0xFF) << 16;
                                    break Label_0486;
                                }
                                break Label_0646;
                            }
                            case 6: {
                                final int inputEnd3 = super._inputEnd;
                                if (inputEnd3 - inputPtr2 >= 2) {
                                    n8 = this.decodeUtf8_3(n5);
                                    n7 = n3;
                                    array = array2;
                                    break;
                                }
                                pendingInput = n5;
                                if (inputEnd3 > inputPtr2) {
                                    final byte[] inputBuffer3 = this._inputBuffer;
                                    super._inputPtr = inputPtr2 + 1;
                                    n11 = (inputBuffer3[inputPtr2] & 0xFF) << 8;
                                    break Label_0486;
                                }
                                break Label_0646;
                            }
                            case 5: {
                                if (inputPtr2 >= super._inputEnd) {
                                    pendingInput = n5;
                                    break Label_0646;
                                }
                                n8 = this.decodeUtf8_2(n5);
                                n7 = n3;
                                array = array2;
                                break;
                            }
                            case 4: {
                                this.reportInvalidInitial(n5);
                            }
                            case 8: {
                                final byte elemAttrQuote = super._elemAttrQuote;
                                if (elemAttrQuote == 0) {
                                    super._elemAttrQuote = (byte)n5;
                                    n7 = n3;
                                    n8 = n5;
                                    array = array2;
                                    break;
                                }
                                n7 = n3;
                                n8 = n5;
                                array = array2;
                                if (elemAttrQuote == n5) {
                                    super._elemAttrQuote = 0;
                                    n7 = n3;
                                    n8 = n5;
                                    array = array2;
                                    break;
                                }
                                break;
                            }
                            case 3: {
                                this.markLF();
                                n7 = n3;
                                n8 = n5;
                                array = array2;
                                break;
                            }
                            case 1: {
                                this.handleInvalidXmlChar(n5);
                            }
                            case 2: {
                                final int inputPtr4 = super._inputPtr;
                                if (inputPtr4 >= super._inputEnd) {
                                    super._pendingInput = -1;
                                    break Label_0785;
                                }
                                if (inputBuffer[inputPtr4] == 10) {
                                    super._inputPtr = inputPtr4 + 1;
                                }
                                this.markLF();
                                n8 = 10;
                                array = array2;
                                n7 = n3;
                                break;
                            }
                        }
                        array[n7] = (char)n8;
                        currentLength = n7 + 1;
                        continue;
                    }
                    pendingInput = (n5 | n11);
                }
                super._pendingInput = pendingInput;
                super._textBuilder.setCurrentLength(n3);
                return false;
            }
            int n3 = currentLength;
            continue Label_0785;
        }
    }
    
    public int handleDecEntityInCharacters(int n) {
        final byte[] inputBuffer = this._inputBuffer;
        final int n2 = n + 1;
        byte b = inputBuffer[n];
        final int inputEnd = super._inputEnd;
        int n3 = 0;
        n = n2;
        while (true) {
            if (b > 57 || b < 48) {
                this.throwUnexpectedChar(this.decodeCharForError(b), " expected a digit (0 - 9) for character entity");
            }
            n3 = n3 * 10 + (b - 48);
            if (n3 > 1114111) {
                this.reportEntityOverflow();
            }
            if (n >= inputEnd) {
                return 0;
            }
            final byte[] inputBuffer2 = this._inputBuffer;
            final int inputPtr = n + 1;
            b = inputBuffer2[n];
            if (b == 59) {
                super._inputPtr = inputPtr;
                this.verifyXmlChar(n3);
                return n3;
            }
            n = inputPtr;
        }
    }
    
    public int handleEntityInAttributeValue() {
        final int inputPtr = super._inputPtr;
        final int inputEnd = super._inputEnd;
        int pendingInput;
        if (inputPtr >= inputEnd) {
            pendingInput = -60;
        }
        else {
            final byte[] inputBuffer = this._inputBuffer;
            int n = inputPtr + 1;
            super._inputPtr = n;
            final byte b = inputBuffer[inputPtr];
            if (b == 35) {
                super._pendingInput = -61;
                if (n >= inputEnd) {
                    return -1;
                }
                int n2;
                if (inputBuffer[n] == 120) {
                    super._pendingInput = -62;
                    ++n;
                    if ((super._inputPtr = n) >= inputEnd) {
                        return -1;
                    }
                    n2 = this.handleHexEntityInAttribute(true);
                }
                else {
                    n2 = this.handleDecEntityInAttribute(true);
                }
                if (n2 == 0) {
                    return -1;
                }
                return n2;
            }
            else {
                final PName newEntityName = this.parseNewEntityName(b);
                if (newEntityName == null) {
                    pendingInput = -63;
                }
                else {
                    final int decodeGeneralEntity = this.decodeGeneralEntity(newEntityName);
                    if (decodeGeneralEntity != 0) {
                        return decodeGeneralEntity;
                    }
                    super._tokenName = newEntityName;
                    return 0;
                }
            }
        }
        super._pendingInput = pendingInput;
        return -1;
    }
    
    public int handleEntityInCharacters() {
        final int inputPtr = super._inputPtr;
        if (inputPtr + 3 <= super._inputEnd) {
            final byte[] inputBuffer = this._inputBuffer;
            final int n = inputPtr + 1;
            final byte b = inputBuffer[inputPtr];
            if (b == 35) {
                if (inputBuffer[n] == 120) {
                    return this.handleHexEntityInCharacters(n + 1);
                }
                return this.handleDecEntityInCharacters(n);
            }
            else if (b == 97) {
                final int n2 = n + 1;
                final byte b2 = inputBuffer[n];
                if (b2 == 109) {
                    final int n3 = n2 + 1;
                    if (n3 < inputPtr && inputBuffer[n2] == 112 && inputBuffer[n3] == 59) {
                        super._inputPtr = n2 + 2;
                        return 38;
                    }
                }
                else if (b2 == 112) {
                    final int n4 = n2 + 2;
                    if (n4 < inputPtr && inputBuffer[n2] == 111 && inputBuffer[n2 + 1] == 115 && inputBuffer[n4] == 59) {
                        super._inputPtr = n2 + 3;
                        return 39;
                    }
                }
            }
            else if (b == 103) {
                if (inputBuffer[n] == 116 && inputBuffer[n + 1] == 59) {
                    super._inputPtr = n + 2;
                    return 62;
                }
            }
            else if (b == 108) {
                if (inputBuffer[n] == 116 && inputBuffer[n + 1] == 59) {
                    super._inputPtr = n + 2;
                    return 60;
                }
            }
            else if (b == 113) {
                final int n5 = n + 3;
                if (n5 < inputPtr && inputBuffer[n] == 117 && inputBuffer[n + 1] == 111 && inputBuffer[n + 2] == 116 && inputBuffer[n5] == 59) {
                    super._inputPtr = n + 4;
                    return 39;
                }
            }
        }
        return 0;
    }
    
    public int handleEntityStartingToken() {
        super._textBuilder.resetWithEmpty();
        final byte b = this._inputBuffer[super._inputPtr++];
        if (b == 35) {
            super._textBuilder.resetWithEmpty();
            super._state = 5;
            super._pendingInput = -70;
            if (super._inputPtr >= super._inputEnd) {
                return 257;
            }
            return this.handleNumericEntityStartingToken();
        }
        else {
            final PName newEntityName = this.parseNewEntityName(b);
            if (newEntityName == null) {
                super._state = 6;
                return 257;
            }
            final int decodeGeneralEntity = this.decodeGeneralEntity(newEntityName);
            if (decodeGeneralEntity == 0) {
                super._tokenName = newEntityName;
                super._currToken = 9;
                return super._nextEvent = 9;
            }
            super._textBuilder.resetWithChar((char)decodeGeneralEntity);
            super._nextEvent = 0;
            super._currToken = 4;
            if (super._cfgLazyParsing) {
                super._tokenIncomplete = true;
            }
            else {
                this.finishCharacters();
            }
            return super._currToken;
        }
    }
    
    public int handleHexEntityInCharacters(int n) {
        final byte[] inputBuffer = this._inputBuffer;
        final int n2 = n + 1;
        byte b = inputBuffer[n];
        final int inputEnd = super._inputEnd;
        int n3 = 0;
        n = n2;
        while (true) {
            int n4 = 0;
            Label_0108: {
                if (b <= 57 && b >= 48) {
                    n4 = b - 48;
                }
                else {
                    if (b <= 70 && b >= 65) {
                        n4 = b - 65;
                    }
                    else {
                        if (b > 102 || b < 97) {
                            this.throwUnexpectedChar(this.decodeCharForError(b), " expected a hex digit (0-9a-fA-F) for character entity");
                            n4 = b;
                            break Label_0108;
                        }
                        n4 = b - 97;
                    }
                    n4 += 10;
                }
            }
            n3 = (n3 << 4) + n4;
            if (n3 > 1114111) {
                this.reportEntityOverflow();
            }
            if (n >= inputEnd) {
                return 0;
            }
            final byte[] inputBuffer2 = this._inputBuffer;
            final int inputPtr = n + 1;
            b = inputBuffer2[n];
            if (b == 59) {
                super._inputPtr = inputPtr;
                this.verifyXmlChar(n3);
                return n3;
            }
            n = inputPtr;
        }
    }
    
    public int handleNamedEntityStartingToken() {
        final PName entityName = this.parseEntityName();
        if (entityName == null) {
            return super._nextEvent;
        }
        final int decodeGeneralEntity = this.decodeGeneralEntity(entityName);
        if (decodeGeneralEntity == 0) {
            super._tokenName = entityName;
            return super._currToken = 9;
        }
        super._textBuilder.resetWithChar((char)decodeGeneralEntity);
        super._nextEvent = 0;
        super._currToken = 4;
        if (super._cfgLazyParsing) {
            super._tokenIncomplete = true;
        }
        else {
            this.finishCharacters();
        }
        return super._currToken;
    }
    
    @Override
    public boolean handleNsDecl() {
        final int[] attr_CHARS = super._charTypes.ATTR_CHARS;
        final char[] nameBuffer = super._nameBuffer;
        final byte elemAttrQuote = super._elemAttrQuote;
        char[] array = nameBuffer;
        if (super._pendingInput != 0) {
            if (!this.handleNsValuePending()) {
                return false;
            }
            super._pendingInput = 0;
            array = nameBuffer;
        }
    Label_0050:
        while (super._inputPtr < super._inputEnd) {
            char[] growArrayBy = array;
            if (super._elemNsPtr >= array.length) {
                growArrayBy = DataUtil.growArrayBy(array, array.length);
                super._nameBuffer = growArrayBy;
            }
            final int inputEnd = super._inputEnd;
            final int n = super._inputPtr + (growArrayBy.length - super._elemNsPtr);
            int n2;
            if (n < (n2 = inputEnd)) {
                n2 = n;
            }
            byte[] inputBuffer;
            int inputPtr2;
            int pendingInput;
            int n3;
            while (true) {
                final int inputPtr = super._inputPtr;
                array = growArrayBy;
                if (inputPtr >= n2) {
                    continue Label_0050;
                }
                inputBuffer = this._inputBuffer;
                inputPtr2 = inputPtr + 1;
                super._inputPtr = inputPtr2;
                pendingInput = (inputBuffer[inputPtr] & 0xFF);
                n3 = attr_CHARS[pendingInput];
                if (n3 != 0) {
                    break;
                }
                growArrayBy[super._elemNsPtr++] = (char)pendingInput;
            }
            int n4 = 0;
            if (n3 != 14) {
                switch (n3) {
                    default: {
                        array = growArrayBy;
                        n4 = pendingInput;
                        break;
                    }
                    case 7: {
                        final int inputEnd2 = super._inputEnd;
                        if (inputEnd2 - inputPtr2 < 3) {
                            int pendingInput2 = pendingInput;
                            if (inputEnd2 > inputPtr2) {
                                final int inputPtr3 = inputPtr2 + 1;
                                super._inputPtr = inputPtr3;
                                pendingInput2 = (pendingInput | (inputBuffer[inputPtr2] & 0xFF) << 8);
                                if (inputEnd2 > inputPtr3) {
                                    super._inputPtr = inputPtr3 + 1;
                                    pendingInput2 |= (inputBuffer[inputPtr3] & 0xFF) << 16;
                                }
                            }
                            super._pendingInput = pendingInput2;
                            return false;
                        }
                        final int decodeUtf8_4 = this.decodeUtf8_4(pendingInput);
                        final int elemNsPtr = super._elemNsPtr;
                        final int elemNsPtr2 = elemNsPtr + 1;
                        super._elemNsPtr = elemNsPtr2;
                        growArrayBy[elemNsPtr] = (char)(decodeUtf8_4 >> 10 | 0xD800);
                        final int n5 = (decodeUtf8_4 & 0x3FF) | 0xDC00;
                        array = growArrayBy;
                        n4 = n5;
                        if (elemNsPtr2 >= growArrayBy.length) {
                            array = DataUtil.growArrayBy(growArrayBy, growArrayBy.length);
                            super._nameBuffer = array;
                            n4 = n5;
                            break;
                        }
                        break;
                    }
                    case 6: {
                        final int inputEnd3 = super._inputEnd;
                        if (inputEnd3 - inputPtr2 < 2) {
                            int pendingInput3 = pendingInput;
                            if (inputEnd3 > inputPtr2) {
                                super._inputPtr = inputPtr2 + 1;
                                pendingInput3 = (pendingInput | (inputBuffer[inputPtr2] & 0xFF) << 8);
                            }
                            super._pendingInput = pendingInput3;
                            return false;
                        }
                        n4 = this.decodeUtf8_3(pendingInput);
                        array = growArrayBy;
                        break;
                    }
                    case 5: {
                        if (inputPtr2 >= super._inputEnd) {
                            super._pendingInput = pendingInput;
                            return false;
                        }
                        n4 = this.decodeUtf8_2(pendingInput);
                        array = growArrayBy;
                        break;
                    }
                    case 4: {
                        this.reportInvalidInitial(pendingInput);
                    }
                    case 9: {
                        this.throwUnexpectedChar(pendingInput, "'<' not allowed in attribute value");
                    }
                    case 10: {
                        final int handleEntityInAttributeValue = this.handleEntityInAttributeValue();
                        if (handleEntityInAttributeValue <= 0) {
                            if (handleEntityInAttributeValue < 0) {
                                return false;
                            }
                            this.reportUnexpandedEntityInAttr(super._elemAttrName, true);
                        }
                        array = growArrayBy;
                        n4 = handleEntityInAttributeValue;
                        if (handleEntityInAttributeValue >> 16 == 0) {
                            break;
                        }
                        final int n6 = handleEntityInAttributeValue - 65536;
                        final int elemNsPtr3 = super._elemNsPtr;
                        final int elemNsPtr4 = elemNsPtr3 + 1;
                        super._elemNsPtr = elemNsPtr4;
                        growArrayBy[elemNsPtr3] = (char)(n6 >> 10 | 0xD800);
                        final int n7 = (n6 & 0x3FF) | 0xDC00;
                        array = growArrayBy;
                        n4 = n7;
                        if (elemNsPtr4 >= growArrayBy.length) {
                            array = DataUtil.growArrayBy(growArrayBy, growArrayBy.length);
                            super._nameBuffer = array;
                            n4 = n7;
                            break;
                        }
                        break;
                    }
                    case 1: {
                        this.handleInvalidXmlChar(pendingInput);
                    }
                    case 2: {
                        final int inputPtr4 = super._inputPtr;
                        if (inputPtr4 >= super._inputEnd) {
                            super._pendingInput = -1;
                            return false;
                        }
                        if (this._inputBuffer[inputPtr4] == 10) {
                            super._inputPtr = inputPtr4 + 1;
                        }
                    }
                    case 3: {
                        this.markLF();
                    }
                    case 8: {
                        n4 = 32;
                        array = growArrayBy;
                        break;
                    }
                }
            }
            else {
                array = growArrayBy;
                if ((n4 = pendingInput) == elemAttrQuote) {
                    final int elemNsPtr5 = super._elemNsPtr;
                    if (elemNsPtr5 == 0) {
                        this.bindNs(super._elemAttrName, "");
                    }
                    else {
                        this.bindNs(super._elemAttrName, super._config.canonicalizeURI(growArrayBy, elemNsPtr5));
                    }
                    return true;
                }
            }
            array[super._elemNsPtr++] = (char)n4;
        }
        return false;
    }
    
    public int handleNumericEntityStartingToken() {
        if (super._pendingInput == -70) {
            final byte[] inputBuffer = this._inputBuffer;
            final int inputPtr = super._inputPtr;
            final byte b = inputBuffer[inputPtr];
            super._entityValue = 0;
            if (b == 120) {
                super._pendingInput = -73;
                if ((super._inputPtr = inputPtr + 1) >= super._inputEnd) {
                    return 257;
                }
            }
            else {
                super._pendingInput = -72;
            }
        }
        if (super._pendingInput == -73) {
            if (!this.decodeHexEntity()) {
                return 257;
            }
        }
        else if (!this.decodeDecEntity()) {
            return 257;
        }
        this.verifyAndAppendEntityCharacter(super._entityValue);
        super._currToken = 4;
        if (super._cfgLazyParsing) {
            super._tokenIncomplete = true;
        }
        else {
            this.finishCharacters();
        }
        super._pendingInput = 0;
        return super._currToken;
    }
    
    @Override
    public int handlePI() {
        if (super._state == 5) {
            return this.parsePIData();
        }
        Label_0407: {
            Label_0335: {
                while (true) {
                    final int inputPtr = super._inputPtr;
                    if (inputPtr >= super._inputEnd) {
                        return 257;
                    }
                    final int state = super._state;
                    if (state != 0) {
                        if (state != 1) {
                            if (state == 2) {
                                break;
                            }
                            if (state != 3) {
                                if (state != 4) {
                                    return this.throwInternal();
                                }
                                final PName pName = this.parsePName();
                                if ((super._tokenName = pName) == null) {
                                    return 257;
                                }
                                this.checkPITargetName(pName);
                                super._state = 1;
                                continue;
                            }
                            else {
                                final byte[] inputBuffer = this._inputBuffer;
                                super._inputPtr = inputPtr + 1;
                                final byte b = inputBuffer[inputPtr];
                                if (b != 62) {
                                    this.reportMissingPISpace(this.decodeCharForError(b));
                                }
                                break Label_0407;
                            }
                        }
                    }
                    else {
                        final byte[] inputBuffer2 = this._inputBuffer;
                        super._inputPtr = inputPtr + 1;
                        final PName newName = this.parseNewName(inputBuffer2[inputPtr]);
                        if ((super._tokenName = newName) == null) {
                            super._state = 4;
                            return 257;
                        }
                        super._state = 1;
                        this.checkPITargetName(newName);
                        if (super._inputPtr >= super._inputEnd) {
                            return 257;
                        }
                    }
                    final byte[] inputBuffer3 = this._inputBuffer;
                    final int inputPtr2 = super._inputPtr;
                    final int inputPtr3 = inputPtr2 + 1;
                    super._inputPtr = inputPtr3;
                    final byte b2 = inputBuffer3[inputPtr2];
                    if (b2 == 63) {
                        if (inputPtr3 < super._inputEnd && inputBuffer3[inputPtr3] == 62) {
                            super._inputPtr = inputPtr3 + 1;
                            break Label_0407;
                        }
                        super._state = 3;
                    }
                    else {
                        if (b2 != 32 && b2 != 13 && b2 != 10 && b2 != 9) {
                            this.reportMissingPISpace(this.decodeCharForError(b2));
                            break;
                        }
                        break Label_0335;
                    }
                }
                if (!this.asyncSkipSpace()) {
                    return 257;
                }
                super._state = 5;
                super._textBuilder.resetWithEmpty();
                return this.parsePIData();
            }
            if (!this.asyncSkipSpace()) {
                super._state = 2;
                return 257;
            }
            super._textBuilder.resetWithEmpty();
            final int inputPtr4 = super._inputPtr;
            if (inputPtr4 + 1 < super._inputEnd) {
                final byte[] inputBuffer4 = this._inputBuffer;
                if (inputBuffer4[inputPtr4] == 63 && inputBuffer4[inputPtr4 + 1] == 62) {
                    super._inputPtr = inputPtr4 + 2;
                    break Label_0407;
                }
            }
            super._state = 5;
            return this.parsePIData();
        }
        super._state = 0;
        super._nextEvent = 257;
        return 3;
    }
    
    public int handlePIPending() {
        final int pendingInput = super._pendingInput;
        int n = 257;
        if (pendingInput != -15) {
            if (this.handleAndAppendPending()) {
                n = 0;
            }
            return n;
        }
        final int inputPtr = super._inputPtr;
        if (inputPtr >= super._inputEnd) {
            return 257;
        }
        final byte b = this._inputBuffer[inputPtr];
        super._pendingInput = 0;
        if (b != 62) {
            super._textBuilder.append('?');
            return 0;
        }
        super._inputPtr = inputPtr + 1;
        super._state = 0;
        super._nextEvent = 257;
        return 3;
    }
    
    @Override
    public final boolean handlePartialCR() {
        if (super._pendingInput != -1) {
            this.throwInternal();
        }
        final int inputPtr = super._inputPtr;
        if (inputPtr >= super._inputEnd) {
            return false;
        }
        super._pendingInput = 0;
        if (this._inputBuffer[inputPtr] == 10) {
            super._inputPtr = inputPtr + 1;
        }
        ++super._currRow;
        super._rowStartOffset = super._inputPtr;
        return true;
    }
    
    @Override
    public int handleStartElement() {
    Label_0000:
        while (true) {
            final int inputPtr = super._inputPtr;
            if (inputPtr >= super._inputEnd) {
                return 257;
            }
            Label_0336: {
                switch (super._state) {
                    default: {
                        this.throwInternal();
                        continue;
                    }
                    case 9: {
                        final byte[] inputBuffer = this._inputBuffer;
                        super._inputPtr = inputPtr + 1;
                        final byte b = inputBuffer[inputPtr];
                        if (b != 62) {
                            this.throwUnexpectedChar(this.decodeCharForError(b), " expected '>'");
                        }
                        return this.finishStartElement(true);
                    }
                    case 8: {
                        if (!this.handleNsDecl()) {
                            return 257;
                        }
                        break;
                    }
                    case 7: {
                        if (!this.handleAttrValue()) {
                            return 257;
                        }
                        break;
                    }
                    case 1: {
                        final PName pName = this.parsePName();
                        if (pName == null) {
                            return 257;
                        }
                        this.initStartElement(pName);
                        if (super._inputPtr >= super._inputEnd) {
                            return 257;
                        }
                    }
                    case 2: {
                        Label_0357: {
                            if (super._pendingInput != 0) {
                                if (!this.handlePartialCR()) {
                                    return 257;
                                }
                            }
                            else {
                                final byte[] inputBuffer2 = this._inputBuffer;
                                final int inputPtr2 = super._inputPtr;
                                final int inputPtr3 = inputPtr2 + 1;
                                super._inputPtr = inputPtr3;
                                final byte b2 = inputBuffer2[inputPtr2];
                                final int n = b2 & 0xFF;
                                if (n <= 32) {
                                    if (n != 10) {
                                        if (n == 13) {
                                            if (inputPtr3 >= super._inputEnd) {
                                                super._pendingInput = -1;
                                                return 257;
                                            }
                                            if (inputBuffer2[inputPtr3] == 10) {
                                                super._inputPtr = inputPtr3 + 1;
                                            }
                                        }
                                        else {
                                            if (n != 32 && n != 9) {
                                                this.throwInvalidSpace(n);
                                            }
                                            break Label_0357;
                                        }
                                    }
                                    this.markLF();
                                }
                                else {
                                    if (n == 62) {
                                        return this.finishStartElement(false);
                                    }
                                    if (n == 47) {
                                        break Label_0336;
                                    }
                                    this.throwUnexpectedChar(this.decodeCharForError(b2), " expected space, or '>' or \"/>\"");
                                }
                            }
                        }
                        super._state = 3;
                        if (super._inputPtr >= super._inputEnd) {
                            return 257;
                        }
                    }
                    case 3:
                    case 5:
                    case 6: {
                        if (super._pendingInput != 0) {
                            if (!this.handlePartialCR()) {
                                return 257;
                            }
                            if (super._inputPtr >= super._inputEnd) {
                                return 257;
                            }
                        }
                        byte b3 = this._inputBuffer[super._inputPtr++];
                        while (true) {
                            final int n2 = b3 & 0xFF;
                            if (n2 <= 32) {
                                Label_0524: {
                                    if (n2 != 10) {
                                        if (n2 == 13) {
                                            final int inputPtr4 = super._inputPtr;
                                            if (inputPtr4 >= super._inputEnd) {
                                                super._pendingInput = -1;
                                                return 257;
                                            }
                                            if (this._inputBuffer[inputPtr4] == 10) {
                                                super._inputPtr = inputPtr4 + 1;
                                            }
                                        }
                                        else {
                                            if (n2 != 32 && n2 != 9) {
                                                this.throwInvalidSpace(n2);
                                            }
                                            break Label_0524;
                                        }
                                    }
                                    this.markLF();
                                }
                                final int inputPtr5 = super._inputPtr;
                                if (inputPtr5 >= super._inputEnd) {
                                    return 257;
                                }
                                final byte[] inputBuffer3 = this._inputBuffer;
                                super._inputPtr = inputPtr5 + 1;
                                b3 = inputBuffer3[inputPtr5];
                            }
                            else {
                                final int state = super._state;
                                if (state != 3) {
                                    if (state == 5) {
                                        if (b3 != 61) {
                                            this.throwUnexpectedChar(this.decodeCharForError(b3), " expected '='");
                                        }
                                        super._state = 6;
                                        continue Label_0000;
                                    }
                                    if (state != 6) {
                                        this.throwInternal();
                                    }
                                    if (b3 != 34 && b3 != 39) {
                                        this.throwUnexpectedChar(this.decodeCharForError(b3), " Expected a quote");
                                    }
                                    this.initAttribute(b3);
                                    continue Label_0000;
                                }
                                else {
                                    if (b3 == 47) {
                                        break Label_0336;
                                    }
                                    if (b3 == 62) {
                                        return this.finishStartElement(false);
                                    }
                                    final PName newName = this.parseNewName(b3);
                                    if (newName == null) {
                                        super._state = 4;
                                        return 257;
                                    }
                                    super._state = 5;
                                    super._elemAttrName = newName;
                                    continue Label_0000;
                                }
                            }
                        }
                        break;
                    }
                    case 4: {
                        final PName pName2 = this.parsePName();
                        if (pName2 == null) {
                            return 257;
                        }
                        super._elemAttrName = pName2;
                        super._state = 5;
                        continue;
                    }
                }
                super._state = 2;
                continue;
            }
            super._state = 9;
        }
    }
    
    @Override
    public int handleStartElementStart(final byte b) {
        final PName newName = this.parseNewName(b);
        super._nextEvent = 1;
        if (newName == null) {
            super._state = 1;
            return 257;
        }
        this.initStartElement(newName);
        return this.handleStartElement();
    }
    
    @Override
    public final boolean needMoreInput() {
        return super._inputPtr >= super._inputEnd && !super._endOfInput;
    }
    
    @Override
    public int nextFromTree() {
        final int currToken = super._currToken;
        if (currToken != 257) {
            if (currToken == 1) {
                if (super._isEmptyTag) {
                    --super._depth;
                    return super._currToken = 2;
                }
            }
            else if (currToken == 2) {
                super._currElem = super._currElem.getParent();
                while (true) {
                    final NsDeclaration lastNsDecl = super._lastNsDecl;
                    if (lastNsDecl == null || lastNsDecl.getLevel() < super._depth) {
                        break;
                    }
                    super._lastNsDecl = super._lastNsDecl.unbind();
                }
            }
            this.setStartLocation();
            if (super._tokenIncomplete) {
                if (!this.skipCharacters()) {
                    return 257;
                }
                super._tokenIncomplete = false;
            }
            super._nextEvent = 257;
            super._currToken = 257;
            super._state = 0;
        }
        Label_0480: {
            if (super._nextEvent == 257) {
                if (super._state == 0) {
                    if (super._pendingInput != 0) {
                        super._nextEvent = 4;
                        return this.startCharactersPending();
                    }
                    final int inputPtr = super._inputPtr;
                    if (inputPtr >= super._inputEnd) {
                        return super._currToken;
                    }
                    final byte[] inputBuffer = this._inputBuffer;
                    super._inputPtr = inputPtr + 1;
                    final byte b = inputBuffer[inputPtr];
                    if (b == 60) {
                        super._state = 1;
                    }
                    else {
                        if (b != 38) {
                            super._nextEvent = 4;
                            return this.startCharacters(b);
                        }
                        super._state = 2;
                    }
                }
                final int inputPtr2 = super._inputPtr;
                final int inputEnd = super._inputEnd;
                if (inputPtr2 >= inputEnd) {
                    return super._currToken;
                }
                final int state = super._state;
                if (state == 1) {
                    final byte[] inputBuffer2 = this._inputBuffer;
                    super._inputPtr = inputPtr2 + 1;
                    final byte b2 = inputBuffer2[inputPtr2];
                    if (b2 == 33) {
                        super._state = 3;
                    }
                    else {
                        if (b2 == 63) {
                            super._nextEvent = 3;
                            super._state = 0;
                            return this.handlePI();
                        }
                        if (b2 == 47) {
                            return this.handleEndElementStart();
                        }
                        return this.handleStartElementStart(b2);
                    }
                }
                else {
                    if (state == 2) {
                        return this.handleEntityStartingToken();
                    }
                    if (state == 6) {
                        return this.handleNamedEntityStartingToken();
                    }
                    if (state == 5) {
                        return this.handleNumericEntityStartingToken();
                    }
                }
                if (super._state == 3) {
                    final int inputPtr3 = super._inputPtr;
                    if (inputPtr3 >= inputEnd) {
                        return super._currToken;
                    }
                    final byte[] inputBuffer3 = this._inputBuffer;
                    super._inputPtr = inputPtr3 + 1;
                    final byte b3 = inputBuffer3[inputPtr3];
                    if (b3 == 45) {
                        super._nextEvent = 5;
                    }
                    else {
                        if (b3 != 91) {
                            this.reportTreeUnexpChar(this.decodeCharForError(b3), " (expected either '-' for COMMENT or '[CDATA[' for CDATA section)");
                            break Label_0480;
                        }
                        super._nextEvent = 12;
                    }
                    super._state = 0;
                }
                else {
                    this.throwInternal();
                }
            }
        }
        final int nextEvent = super._nextEvent;
        if (nextEvent == 1) {
            return this.handleStartElement();
        }
        if (nextEvent == 2) {
            return this.handleEndElement();
        }
        if (nextEvent != 3) {
            if (nextEvent != 4) {
                if (nextEvent == 5) {
                    return this.handleComment();
                }
                if (nextEvent == 12) {
                    return this.handleCData();
                }
            }
            else {
                if (!super._cfgLazyParsing && super._cfgCoalescing) {
                    return this.finishCharactersCoalescing();
                }
                if (super._pendingInput != 0) {
                    return this.startCharactersPending();
                }
                this.throwInternal();
            }
            return this.throwInternal();
        }
        return this.handlePI();
    }
    
    public final int parseCDataContents() {
        if (super._pendingInput != 0) {
            final int handleCDataPending = this.handleCDataPending();
            if (handleCDataPending != 0) {
                return handleCDataPending;
            }
        }
        char[] bufferWithoutReset = super._textBuilder.getBufferWithoutReset();
        int currentLength = super._textBuilder.getCurrentLength();
        final int[] other_CHARS = super._charTypes.OTHER_CHARS;
        final byte[] inputBuffer = this._inputBuffer;
    Label_0650:
        while (true) {
            while (super._inputPtr < super._inputEnd) {
                final int length = bufferWithoutReset.length;
                final int n = 0;
                final int n2 = 0;
                char[] array = bufferWithoutReset;
                int n3;
                if ((n3 = currentLength) >= length) {
                    array = super._textBuilder.finishCurrentSegment();
                    n3 = 0;
                }
                final int inputEnd = super._inputEnd;
                final int n4 = super._inputPtr + (array.length - n3);
                int n5 = n3;
                int n6;
                if (n4 < (n6 = inputEnd)) {
                    n6 = n4;
                    n5 = n3;
                }
                int inputPtr2;
                int n7;
                int n8;
                while (true) {
                    final int inputPtr = super._inputPtr;
                    bufferWithoutReset = array;
                    currentLength = n5;
                    if (inputPtr >= n6) {
                        continue Label_0650;
                    }
                    inputPtr2 = inputPtr + 1;
                    super._inputPtr = inputPtr2;
                    n7 = (inputBuffer[inputPtr] & 0xFF);
                    n8 = other_CHARS[n7];
                    if (n8 != 0) {
                        break;
                    }
                    array[n5] = (char)n7;
                    ++n5;
                }
                int pendingInput2 = 0;
                Label_0645: {
                    int n10 = 0;
                    Label_0805: {
                        Label_0585: {
                            if (n8 != 11) {
                                int n9 = n7;
                                int pendingInput = 0;
                                Label_0482: {
                                    int n13 = 0;
                                    switch (n8) {
                                        default: {
                                            bufferWithoutReset = array;
                                            n10 = n5;
                                            break Label_0805;
                                        }
                                        case 7: {
                                            final int inputEnd2 = super._inputEnd;
                                            if (inputEnd2 - inputPtr2 >= 3) {
                                                final int decodeUtf8_4 = this.decodeUtf8_4(n7);
                                                final int n11 = n5 + 1;
                                                array[n5] = (char)(decodeUtf8_4 >> 10 | 0xD800);
                                                int n12;
                                                if (n11 >= array.length) {
                                                    array = super._textBuilder.finishCurrentSegment();
                                                    n12 = n2;
                                                }
                                                else {
                                                    n12 = n11;
                                                }
                                                n7 = ((decodeUtf8_4 & 0x3FF) | 0xDC00);
                                                bufferWithoutReset = array;
                                                n10 = n12;
                                                break Label_0805;
                                            }
                                            pendingInput = n7;
                                            if (inputEnd2 <= inputPtr2) {
                                                break Label_0482;
                                            }
                                            final byte[] inputBuffer2 = this._inputBuffer;
                                            final int inputPtr3 = inputPtr2 + 1;
                                            super._inputPtr = inputPtr3;
                                            n7 = (pendingInput = (n7 | (inputBuffer2[inputPtr2] & 0xFF) << 8));
                                            if (inputEnd2 > inputPtr3) {
                                                super._inputPtr = inputPtr3 + 1;
                                                n13 = (inputBuffer2[inputPtr3] & 0xFF) << 16;
                                                break;
                                            }
                                            break Label_0482;
                                        }
                                        case 6: {
                                            final int inputEnd3 = super._inputEnd;
                                            if (inputEnd3 - inputPtr2 >= 2) {
                                                n7 = this.decodeUtf8_3(n7);
                                                bufferWithoutReset = array;
                                                n10 = n5;
                                                break Label_0805;
                                            }
                                            pendingInput = n7;
                                            if (inputEnd3 > inputPtr2) {
                                                final byte[] inputBuffer3 = this._inputBuffer;
                                                super._inputPtr = inputPtr2 + 1;
                                                n13 = (inputBuffer3[inputPtr2] & 0xFF) << 8;
                                                break;
                                            }
                                            break Label_0482;
                                        }
                                        case 5: {
                                            if (inputPtr2 >= super._inputEnd) {
                                                pendingInput = n7;
                                                break Label_0482;
                                            }
                                            n7 = this.decodeUtf8_2(n7);
                                            bufferWithoutReset = array;
                                            n10 = n5;
                                            break Label_0805;
                                        }
                                        case 4: {
                                            this.reportInvalidInitial(n7);
                                            break Label_0585;
                                        }
                                        case 1: {
                                            this.handleInvalidXmlChar(n7);
                                        }
                                        case 2: {
                                            final int inputPtr4 = super._inputPtr;
                                            if (inputPtr4 >= super._inputEnd) {
                                                pendingInput2 = -1;
                                                break Label_0645;
                                            }
                                            final byte b = inputBuffer[inputPtr4];
                                            n9 = 10;
                                            if (b == 10) {
                                                super._inputPtr = inputPtr4 + 1;
                                                n9 = n9;
                                            }
                                        }
                                        case 3: {
                                            this.markLF();
                                            bufferWithoutReset = array;
                                            n10 = n5;
                                            n7 = n9;
                                            break Label_0805;
                                        }
                                    }
                                    pendingInput = (n7 | n13);
                                }
                                super._pendingInput = pendingInput;
                                break Label_0650;
                            }
                        }
                        final int inputPtr5 = super._inputPtr;
                        if (inputPtr5 >= super._inputEnd) {
                            pendingInput2 = -30;
                            break Label_0645;
                        }
                        bufferWithoutReset = array;
                        n10 = n5;
                        if (this._inputBuffer[inputPtr5] == 93) {
                            super._inputPtr = inputPtr5 + 1;
                            while (true) {
                                final int inputPtr6 = super._inputPtr;
                                if (inputPtr6 >= super._inputEnd) {
                                    pendingInput2 = -31;
                                    break Label_0645;
                                }
                                final byte b2 = this._inputBuffer[inputPtr6];
                                if (b2 == 62) {
                                    super._inputPtr = inputPtr6 + 1;
                                    super._textBuilder.setCurrentLength(n5);
                                    super._state = 0;
                                    super._nextEvent = 257;
                                    return 12;
                                }
                                if (b2 != 93) {
                                    final int n14 = n5 + 1;
                                    array[n5] = ']';
                                    int n15;
                                    if (n14 >= array.length) {
                                        array = super._textBuilder.finishCurrentSegment();
                                        n15 = n;
                                    }
                                    else {
                                        n15 = n14;
                                    }
                                    currentLength = n15 + 1;
                                    array[n15] = ']';
                                    bufferWithoutReset = array;
                                    continue Label_0650;
                                }
                                super._inputPtr = inputPtr6 + 1;
                                final int n16 = n5 + 1;
                                array[n5] = ']';
                                if (n16 >= array.length) {
                                    array = super._textBuilder.finishCurrentSegment();
                                    n5 = 0;
                                }
                                else {
                                    n5 = n16;
                                }
                            }
                        }
                    }
                    bufferWithoutReset[n10] = (char)n7;
                    currentLength = n10 + 1;
                    continue;
                }
                super._pendingInput = pendingInput2;
                super._textBuilder.setCurrentLength(n5);
                return 257;
            }
            int n5 = currentLength;
            continue Label_0650;
        }
    }
    
    public int parseCommentContents() {
        if (super._pendingInput != 0) {
            final int handleCommentPending = this.handleCommentPending();
            if (handleCommentPending != 0) {
                return handleCommentPending;
            }
        }
        char[] bufferWithoutReset = super._textBuilder.getBufferWithoutReset();
        int currentLength = super._textBuilder.getCurrentLength();
        final int[] other_CHARS = super._charTypes.OTHER_CHARS;
        final byte[] inputBuffer = this._inputBuffer;
    Label_0633:
        while (true) {
            while (super._inputPtr < super._inputEnd) {
                final int length = bufferWithoutReset.length;
                final int n = 0;
                char[] array = bufferWithoutReset;
                int n2;
                if ((n2 = currentLength) >= length) {
                    array = super._textBuilder.finishCurrentSegment();
                    n2 = 0;
                }
                final int inputEnd = super._inputEnd;
                final int n3 = super._inputPtr + (array.length - n2);
                int n4 = n2;
                int n5;
                if (n3 < (n5 = inputEnd)) {
                    n5 = n3;
                    n4 = n2;
                }
                int inputPtr2;
                int n6;
                int n7;
                while (true) {
                    final int inputPtr = super._inputPtr;
                    bufferWithoutReset = array;
                    currentLength = n4;
                    if (inputPtr >= n5) {
                        continue Label_0633;
                    }
                    inputPtr2 = inputPtr + 1;
                    super._inputPtr = inputPtr2;
                    n6 = (inputBuffer[inputPtr] & 0xFF);
                    n7 = other_CHARS[n6];
                    if (n7 != 0) {
                        break;
                    }
                    array[n4] = (char)n6;
                    ++n4;
                }
                int pendingInput2 = 0;
                Label_0521: {
                    int n9 = 0;
                    Label_0687: {
                        Label_0570: {
                            if (n7 != 13) {
                                int n8 = n6;
                                int pendingInput = 0;
                                Label_0470: {
                                    int n12 = 0;
                                    switch (n7) {
                                        default: {
                                            n9 = n4;
                                            break Label_0687;
                                        }
                                        case 7: {
                                            final int inputEnd2 = super._inputEnd;
                                            if (inputEnd2 - inputPtr2 >= 3) {
                                                final int decodeUtf8_4 = this.decodeUtf8_4(n6);
                                                final int n10 = n4 + 1;
                                                array[n4] = (char)(decodeUtf8_4 >> 10 | 0xD800);
                                                int n11;
                                                if (n10 >= array.length) {
                                                    array = super._textBuilder.finishCurrentSegment();
                                                    n11 = n;
                                                }
                                                else {
                                                    n11 = n10;
                                                }
                                                n6 = ((decodeUtf8_4 & 0x3FF) | 0xDC00);
                                                n9 = n11;
                                                break Label_0687;
                                            }
                                            pendingInput = n6;
                                            if (inputEnd2 <= inputPtr2) {
                                                break Label_0470;
                                            }
                                            final byte[] inputBuffer2 = this._inputBuffer;
                                            final int inputPtr3 = inputPtr2 + 1;
                                            super._inputPtr = inputPtr3;
                                            n6 = (pendingInput = (n6 | (inputBuffer2[inputPtr2] & 0xFF) << 8));
                                            if (inputEnd2 > inputPtr3) {
                                                super._inputPtr = inputPtr3 + 1;
                                                n12 = (inputBuffer2[inputPtr3] & 0xFF) << 16;
                                                break;
                                            }
                                            break Label_0470;
                                        }
                                        case 6: {
                                            final int inputEnd3 = super._inputEnd;
                                            if (inputEnd3 - inputPtr2 >= 2) {
                                                n6 = this.decodeUtf8_3(n6);
                                                n9 = n4;
                                                break Label_0687;
                                            }
                                            pendingInput = n6;
                                            if (inputEnd3 > inputPtr2) {
                                                final byte[] inputBuffer3 = this._inputBuffer;
                                                super._inputPtr = inputPtr2 + 1;
                                                n12 = (inputBuffer3[inputPtr2] & 0xFF) << 8;
                                                break;
                                            }
                                            break Label_0470;
                                        }
                                        case 5: {
                                            if (inputPtr2 >= super._inputEnd) {
                                                pendingInput = n6;
                                                break Label_0470;
                                            }
                                            n6 = this.decodeUtf8_2(n6);
                                            n9 = n4;
                                            break Label_0687;
                                        }
                                        case 4: {
                                            this.reportInvalidInitial(n6);
                                            break Label_0570;
                                        }
                                        case 1: {
                                            this.handleInvalidXmlChar(n6);
                                        }
                                        case 2: {
                                            final int inputPtr4 = super._inputPtr;
                                            if (inputPtr4 >= super._inputEnd) {
                                                pendingInput2 = -1;
                                                break Label_0521;
                                            }
                                            final byte b = inputBuffer[inputPtr4];
                                            n8 = 10;
                                            if (b == 10) {
                                                super._inputPtr = inputPtr4 + 1;
                                                n8 = n8;
                                            }
                                        }
                                        case 3: {
                                            this.markLF();
                                            n9 = n4;
                                            n6 = n8;
                                            break Label_0687;
                                        }
                                    }
                                    pendingInput = (n6 | n12);
                                }
                                super._pendingInput = pendingInput;
                                break Label_0633;
                            }
                        }
                        final int inputPtr5 = super._inputPtr;
                        final int inputEnd4 = super._inputEnd;
                        if (inputPtr5 >= inputEnd4) {
                            pendingInput2 = -20;
                            break Label_0521;
                        }
                        final byte[] inputBuffer4 = this._inputBuffer;
                        n9 = n4;
                        if (inputBuffer4[inputPtr5] == 45) {
                            final int inputPtr6 = inputPtr5 + 1;
                            if ((super._inputPtr = inputPtr6) >= inputEnd4) {
                                pendingInput2 = -21;
                                break Label_0521;
                            }
                            super._inputPtr = inputPtr6 + 1;
                            if (inputBuffer4[inputPtr6] != 62) {
                                this.reportDoubleHyphenInComments();
                            }
                            super._textBuilder.setCurrentLength(n4);
                            super._state = 0;
                            super._nextEvent = 257;
                            return 5;
                        }
                    }
                    array[n9] = (char)n6;
                    currentLength = n9 + 1;
                    bufferWithoutReset = array;
                    continue;
                }
                super._pendingInput = pendingInput2;
                super._textBuilder.setCurrentLength(n4);
                return 257;
            }
            int n4 = currentLength;
            continue Label_0633;
        }
    }
    
    public final PName parseEntityName() {
        int currQuad = super._currQuad;
        while (true) {
            final int currQuadBytes = super._currQuadBytes;
            Label_0492: {
                int currQuad4 = 0;
                Label_0380: {
                    int currQuad3 = 0;
                    Label_0250: {
                        int currQuad2;
                        if (currQuadBytes != 0) {
                            currQuad2 = currQuad;
                            if (currQuadBytes != 1) {
                                currQuad3 = currQuad;
                                if (currQuadBytes == 2) {
                                    break Label_0250;
                                }
                                currQuad4 = currQuad;
                                if (currQuadBytes != 3) {
                                    break Label_0492;
                                }
                                break Label_0380;
                            }
                        }
                        else {
                            final int inputPtr = super._inputPtr;
                            if (inputPtr >= super._inputEnd) {
                                return null;
                            }
                            final byte[] inputBuffer = this._inputBuffer;
                            final int inputPtr2 = inputPtr + 1;
                            super._inputPtr = inputPtr2;
                            int n = currQuad2 = (inputBuffer[inputPtr] & 0xFF);
                            if (n < 65 && (n < 45 || n > 58 || (currQuad2 = n) == 47)) {
                                if (super._quadCount == 1) {
                                    final int n2 = super._quadBuffer[0];
                                    if (n2 == EntityNames.ENTITY_APOS_QUAD) {
                                        super._inputPtr = inputPtr2 - 1;
                                        return EntityNames.ENTITY_APOS;
                                    }
                                    if ((n = n2) == EntityNames.ENTITY_QUOT_QUAD) {
                                        super._inputPtr = inputPtr2 - 1;
                                        return EntityNames.ENTITY_QUOT;
                                    }
                                }
                                return this.findPName(n, 0);
                            }
                        }
                        final int inputPtr3 = super._inputPtr;
                        if (inputPtr3 >= super._inputEnd) {
                            super._currQuad = currQuad2;
                            super._currQuadBytes = 1;
                            return null;
                        }
                        final byte[] inputBuffer2 = this._inputBuffer;
                        super._inputPtr = inputPtr3 + 1;
                        final int n3 = inputBuffer2[inputPtr3] & 0xFF;
                        if (n3 < 65 && (n3 < 45 || n3 > 58 || n3 == 47)) {
                            return this.findPName(currQuad2, 1);
                        }
                        currQuad3 = (currQuad2 << 8 | n3);
                    }
                    final int inputPtr4 = super._inputPtr;
                    if (inputPtr4 >= super._inputEnd) {
                        super._currQuad = currQuad3;
                        super._currQuadBytes = 2;
                        return null;
                    }
                    final byte[] inputBuffer3 = this._inputBuffer;
                    final int inputPtr5 = inputPtr4 + 1;
                    super._inputPtr = inputPtr5;
                    final int n4 = inputBuffer3[inputPtr4] & 0xFF;
                    if (n4 < 65 && (n4 < 45 || n4 > 58 || n4 == 47)) {
                        if (super._quadCount == 0) {
                            if (currQuad3 == EntityNames.ENTITY_GT_QUAD) {
                                super._inputPtr = inputPtr5 - 1;
                                return EntityNames.ENTITY_GT;
                            }
                            if (currQuad3 == EntityNames.ENTITY_LT_QUAD) {
                                super._inputPtr = inputPtr5 - 1;
                                return EntityNames.ENTITY_LT;
                            }
                        }
                        return this.findPName(currQuad3, 2);
                    }
                    currQuad4 = (currQuad3 << 8 | n4);
                }
                final int inputPtr6 = super._inputPtr;
                if (inputPtr6 >= super._inputEnd) {
                    super._currQuad = currQuad4;
                    super._currQuadBytes = 3;
                    return null;
                }
                final byte[] inputBuffer4 = this._inputBuffer;
                final int inputPtr7 = inputPtr6 + 1;
                super._inputPtr = inputPtr7;
                final int n5 = inputBuffer4[inputPtr6] & 0xFF;
                if (n5 < 65 && (n5 < 45 || n5 > 58 || n5 == 47)) {
                    if (super._quadCount == 0 && currQuad4 == EntityNames.ENTITY_AMP_QUAD) {
                        super._inputPtr = inputPtr7 - 1;
                        return EntityNames.ENTITY_AMP;
                    }
                    return this.findPName(currQuad4, 3);
                }
                else {
                    currQuad = (currQuad4 << 8 | n5);
                }
            }
            final int quadCount = super._quadCount;
            if (quadCount == 0) {
                super._quadBuffer[0] = currQuad;
                super._quadCount = 1;
            }
            else {
                final int[] quadBuffer = super._quadBuffer;
                if (quadCount >= quadBuffer.length) {
                    super._quadBuffer = DataUtil.growArrayBy(quadBuffer, quadBuffer.length);
                }
                super._quadBuffer[super._quadCount++] = currQuad;
            }
            super._currQuadBytes = 0;
        }
    }
    
    public final PName parseNewEntityName(final byte b) {
        final int currQuad = b & 0xFF;
        if (currQuad < 65) {
            this.throwUnexpectedChar(currQuad, "; expected a name start character");
        }
        super._quadCount = 0;
        super._currQuad = currQuad;
        super._currQuadBytes = 1;
        return this.parseEntityName();
    }
    
    @Override
    public final PName parseNewName(final byte b) {
        final int currQuad = b & 0xFF;
        if (currQuad < 65) {
            this.throwUnexpectedChar(currQuad, "; expected a name start character");
        }
        super._quadCount = 0;
        super._currQuad = currQuad;
        super._currQuadBytes = 1;
        return this.parsePName();
    }
    
    public int parsePIData() {
        if (super._pendingInput != 0) {
            final int handlePIPending = this.handlePIPending();
            if (handlePIPending != 0) {
                return handlePIPending;
            }
        }
        char[] bufferWithoutReset = super._textBuilder.getBufferWithoutReset();
        int currentLength = super._textBuilder.getCurrentLength();
        final int[] other_CHARS = super._charTypes.OTHER_CHARS;
        final byte[] inputBuffer = this._inputBuffer;
    Label_0589:
        while (true) {
            while (super._inputPtr < super._inputEnd) {
                final int length = bufferWithoutReset.length;
                final int n = 0;
                char[] array = bufferWithoutReset;
                int n2;
                if ((n2 = currentLength) >= length) {
                    array = super._textBuilder.finishCurrentSegment();
                    n2 = 0;
                }
                final int inputEnd = super._inputEnd;
                final int n3 = super._inputPtr + (array.length - n2);
                int n4 = n2;
                int n5;
                if (n3 < (n5 = inputEnd)) {
                    n5 = n3;
                    n4 = n2;
                }
                int inputPtr2;
                int n6;
                int n7;
                while (true) {
                    final int inputPtr = super._inputPtr;
                    bufferWithoutReset = array;
                    currentLength = n4;
                    if (inputPtr >= n5) {
                        continue Label_0589;
                    }
                    inputPtr2 = inputPtr + 1;
                    super._inputPtr = inputPtr2;
                    n6 = (inputBuffer[inputPtr] & 0xFF);
                    n7 = other_CHARS[n6];
                    if (n7 != 0) {
                        break;
                    }
                    array[n4] = (char)n6;
                    ++n4;
                }
                int pendingInput2 = 0;
                Label_0521: {
                    int n9 = 0;
                    Label_0644: {
                        Label_0570: {
                            if (n7 != 12) {
                                int n8 = n6;
                                int pendingInput = 0;
                                Label_0470: {
                                    int n12 = 0;
                                    switch (n7) {
                                        default: {
                                            n9 = n4;
                                            break Label_0644;
                                        }
                                        case 7: {
                                            final int inputEnd2 = super._inputEnd;
                                            if (inputEnd2 - inputPtr2 >= 3) {
                                                final int decodeUtf8_4 = this.decodeUtf8_4(n6);
                                                final int n10 = n4 + 1;
                                                array[n4] = (char)(decodeUtf8_4 >> 10 | 0xD800);
                                                int n11;
                                                if (n10 >= array.length) {
                                                    array = super._textBuilder.finishCurrentSegment();
                                                    n11 = n;
                                                }
                                                else {
                                                    n11 = n10;
                                                }
                                                n6 = ((decodeUtf8_4 & 0x3FF) | 0xDC00);
                                                n9 = n11;
                                                break Label_0644;
                                            }
                                            pendingInput = n6;
                                            if (inputEnd2 <= inputPtr2) {
                                                break Label_0470;
                                            }
                                            final byte[] inputBuffer2 = this._inputBuffer;
                                            final int inputPtr3 = inputPtr2 + 1;
                                            super._inputPtr = inputPtr3;
                                            n6 = (pendingInput = (n6 | (inputBuffer2[inputPtr2] & 0xFF) << 8));
                                            if (inputEnd2 > inputPtr3) {
                                                super._inputPtr = inputPtr3 + 1;
                                                n12 = (inputBuffer2[inputPtr3] & 0xFF) << 16;
                                                break;
                                            }
                                            break Label_0470;
                                        }
                                        case 6: {
                                            final int inputEnd3 = super._inputEnd;
                                            if (inputEnd3 - inputPtr2 >= 2) {
                                                n6 = this.decodeUtf8_3(n6);
                                                n9 = n4;
                                                break Label_0644;
                                            }
                                            pendingInput = n6;
                                            if (inputEnd3 > inputPtr2) {
                                                final byte[] inputBuffer3 = this._inputBuffer;
                                                super._inputPtr = inputPtr2 + 1;
                                                n12 = (inputBuffer3[inputPtr2] & 0xFF) << 8;
                                                break;
                                            }
                                            break Label_0470;
                                        }
                                        case 5: {
                                            if (inputPtr2 >= super._inputEnd) {
                                                pendingInput = n6;
                                                break Label_0470;
                                            }
                                            n6 = this.decodeUtf8_2(n6);
                                            n9 = n4;
                                            break Label_0644;
                                        }
                                        case 4: {
                                            this.reportInvalidInitial(n6);
                                            break Label_0570;
                                        }
                                        case 1: {
                                            this.handleInvalidXmlChar(n6);
                                        }
                                        case 2: {
                                            final int inputPtr4 = super._inputPtr;
                                            if (inputPtr4 >= super._inputEnd) {
                                                pendingInput2 = -1;
                                                break Label_0521;
                                            }
                                            final byte b = inputBuffer[inputPtr4];
                                            n8 = 10;
                                            if (b == 10) {
                                                super._inputPtr = inputPtr4 + 1;
                                                n8 = n8;
                                            }
                                        }
                                        case 3: {
                                            this.markLF();
                                            n9 = n4;
                                            n6 = n8;
                                            break Label_0644;
                                        }
                                    }
                                    pendingInput = (n6 | n12);
                                }
                                super._pendingInput = pendingInput;
                                break Label_0589;
                            }
                        }
                        final int inputPtr5 = super._inputPtr;
                        if (inputPtr5 >= super._inputEnd) {
                            pendingInput2 = -15;
                            break Label_0521;
                        }
                        n9 = n4;
                        if (this._inputBuffer[inputPtr5] == 62) {
                            super._inputPtr = inputPtr5 + 1;
                            super._textBuilder.setCurrentLength(n4);
                            super._state = 0;
                            super._nextEvent = 257;
                            return 3;
                        }
                    }
                    array[n9] = (char)n6;
                    currentLength = n9 + 1;
                    bufferWithoutReset = array;
                    continue;
                }
                super._pendingInput = pendingInput2;
                super._textBuilder.setCurrentLength(n4);
                return 257;
            }
            int n4 = currentLength;
            continue Label_0589;
        }
    }
    
    @Override
    public final PName parsePName() {
        int currQuad = super._currQuad;
        while (true) {
            final int currQuadBytes = super._currQuadBytes;
            Label_0365: {
                int currQuad4 = 0;
                Label_0280: {
                    int currQuad3 = 0;
                    Label_0195: {
                        int currQuad2;
                        if (currQuadBytes != 0) {
                            currQuad2 = currQuad;
                            if (currQuadBytes != 1) {
                                currQuad3 = currQuad;
                                if (currQuadBytes == 2) {
                                    break Label_0195;
                                }
                                currQuad4 = currQuad;
                                if (currQuadBytes != 3) {
                                    break Label_0365;
                                }
                                break Label_0280;
                            }
                        }
                        else {
                            final int inputPtr = super._inputPtr;
                            if (inputPtr >= super._inputEnd) {
                                return null;
                            }
                            final byte[] inputBuffer = this._inputBuffer;
                            super._inputPtr = inputPtr + 1;
                            final int n = currQuad2 = (inputBuffer[inputPtr] & 0xFF);
                            if (n < 65 && (n < 45 || n > 58 || (currQuad2 = n) == 47)) {
                                return this.findPName(n, 0);
                            }
                        }
                        final int inputPtr2 = super._inputPtr;
                        if (inputPtr2 >= super._inputEnd) {
                            super._currQuad = currQuad2;
                            super._currQuadBytes = 1;
                            return null;
                        }
                        final byte[] inputBuffer2 = this._inputBuffer;
                        super._inputPtr = inputPtr2 + 1;
                        final int n2 = inputBuffer2[inputPtr2] & 0xFF;
                        if (n2 < 65 && (n2 < 45 || n2 > 58 || n2 == 47)) {
                            return this.findPName(currQuad2, 1);
                        }
                        currQuad3 = (currQuad2 << 8 | n2);
                    }
                    final int inputPtr3 = super._inputPtr;
                    if (inputPtr3 >= super._inputEnd) {
                        super._currQuad = currQuad3;
                        super._currQuadBytes = 2;
                        return null;
                    }
                    final byte[] inputBuffer3 = this._inputBuffer;
                    super._inputPtr = inputPtr3 + 1;
                    final int n3 = inputBuffer3[inputPtr3] & 0xFF;
                    if (n3 < 65 && (n3 < 45 || n3 > 58 || n3 == 47)) {
                        return this.findPName(currQuad3, 2);
                    }
                    currQuad4 = (currQuad3 << 8 | n3);
                }
                final int inputPtr4 = super._inputPtr;
                if (inputPtr4 >= super._inputEnd) {
                    super._currQuad = currQuad4;
                    super._currQuadBytes = 3;
                    return null;
                }
                final byte[] inputBuffer4 = this._inputBuffer;
                super._inputPtr = inputPtr4 + 1;
                final int n4 = inputBuffer4[inputPtr4] & 0xFF;
                if (n4 < 65 && (n4 < 45 || n4 > 58 || n4 == 47)) {
                    return this.findPName(currQuad4, 3);
                }
                currQuad = (currQuad4 << 8 | n4);
            }
            final int quadCount = super._quadCount;
            if (quadCount == 0) {
                super._quadBuffer[0] = currQuad;
                super._quadCount = 1;
            }
            else {
                final int[] quadBuffer = super._quadBuffer;
                if (quadCount >= quadBuffer.length) {
                    super._quadBuffer = DataUtil.growArrayBy(quadBuffer, quadBuffer.length);
                }
                super._quadBuffer[super._quadCount++] = currQuad;
            }
            super._currQuadBytes = 0;
        }
    }
    
    @Override
    public boolean skipCharacters() {
        if (super._pendingInput != 0 && !this.skipPending()) {
            return false;
        }
        final int[] text_CHARS = super._charTypes.TEXT_CHARS;
        final byte[] inputBuffer = this._inputBuffer;
        Block_19: {
        Label_0433:
            while (true) {
                final int inputPtr = super._inputPtr;
                final int inputEnd = super._inputEnd;
                int i;
                if ((i = inputPtr) >= inputEnd) {
                    return false;
                }
                while (i < inputEnd) {
                    final int inputPtr2 = i + 1;
                    int n = inputBuffer[i] & 0xFF;
                    final int n2 = text_CHARS[n];
                    if (n2 != 0) {
                        super._inputPtr = inputPtr2;
                        int pendingInput = 0;
                        Label_0412: {
                            int n4 = 0;
                            switch (n2) {
                                default: {
                                    continue Block_19;
                                }
                                case 11: {
                                    byte b = 0;
                                    int n3 = 1;
                                    while (true) {
                                        final int inputPtr3 = super._inputPtr;
                                        if (inputPtr3 >= super._inputEnd) {
                                            break;
                                        }
                                        b = inputBuffer[inputPtr3];
                                        if (b != 93) {
                                            break;
                                        }
                                        super._inputPtr = inputPtr3 + 1;
                                        ++n3;
                                    }
                                    if (b == 62 && n3 > 1) {
                                        this.reportIllegalCDataEnd();
                                        continue Block_19;
                                    }
                                    continue Block_19;
                                }
                                case 10: {
                                    if (this.skipEntityInCharacters() == 0) {
                                        super._pendingInput = -80;
                                        return super._inputPtr < super._inputEnd && this.skipPending();
                                    }
                                    continue Block_19;
                                }
                                case 7: {
                                    final int inputEnd2 = super._inputEnd;
                                    if (inputEnd2 - inputPtr2 >= 3) {
                                        this.decodeUtf8_4(n);
                                        continue Block_19;
                                    }
                                    pendingInput = n;
                                    if (inputEnd2 <= inputPtr2) {
                                        break Label_0412;
                                    }
                                    final byte[] inputBuffer2 = this._inputBuffer;
                                    final int inputPtr4 = inputPtr2 + 1;
                                    super._inputPtr = inputPtr4;
                                    n = (pendingInput = (n | (inputBuffer2[inputPtr2] & 0xFF) << 8));
                                    if (inputEnd2 > inputPtr4) {
                                        super._inputPtr = inputPtr4 + 1;
                                        n4 = (inputBuffer2[inputPtr4] & 0xFF) << 16;
                                        break;
                                    }
                                    break Label_0412;
                                }
                                case 6: {
                                    final int inputEnd3 = super._inputEnd;
                                    if (inputEnd3 - inputPtr2 >= 2) {
                                        this.decodeUtf8_3(n);
                                        continue Block_19;
                                    }
                                    pendingInput = n;
                                    if (inputEnd3 > inputPtr2) {
                                        final byte[] inputBuffer3 = this._inputBuffer;
                                        super._inputPtr = inputPtr2 + 1;
                                        n4 = (inputBuffer3[inputPtr2] & 0xFF) << 8;
                                        break;
                                    }
                                    break Label_0412;
                                }
                                case 5: {
                                    if (inputPtr2 >= super._inputEnd) {
                                        pendingInput = n;
                                        break Label_0412;
                                    }
                                    this.skipUtf8_2(n);
                                    continue Block_19;
                                }
                                case 4: {
                                    this.reportInvalidInitial(n);
                                }
                                case 9: {
                                    break Label_0433;
                                }
                                case 2: {
                                    final int inputPtr5 = super._inputPtr;
                                    if (inputPtr5 >= super._inputEnd) {
                                        break Block_19;
                                    }
                                    if (inputBuffer[inputPtr5] == 10) {
                                        super._inputPtr = inputPtr5 + 1;
                                    }
                                }
                                case 3: {
                                    this.markLF();
                                    continue Block_19;
                                }
                                case 1: {
                                    this.handleInvalidXmlChar(n);
                                }
                            }
                            pendingInput = (n | n4);
                        }
                        super._pendingInput = pendingInput;
                        return false;
                    }
                    i = inputPtr2;
                }
                super._inputPtr = i;
            }
            --super._inputPtr;
            return true;
        }
        super._pendingInput = -1;
        return false;
    }
    
    @Override
    public boolean skipCoalescedText() {
        this.throwInternal();
        return false;
    }
    
    public final void skipUtf8_2(int inputPtr) {
        final byte[] inputBuffer = this._inputBuffer;
        final int inputPtr2 = super._inputPtr;
        inputPtr = inputPtr2 + 1;
        super._inputPtr = inputPtr;
        final byte b = inputBuffer[inputPtr2];
        if ((b & 0xC0) != 0x80) {
            this.reportInvalidOther(b & 0xFF, inputPtr);
        }
    }
    
    @Override
    public final int startCharacters(final byte b) {
        int pendingInput = b & 0xFF;
        Label_0376: {
            switch (super._charTypes.TEXT_CHARS[pendingInput]) {
                case 9:
                case 10: {
                    this.throwInternal();
                    break;
                }
                case 7: {
                    final int inputEnd = super._inputEnd;
                    final int inputPtr = super._inputPtr;
                    if (inputEnd - inputPtr < 3) {
                        int pendingInput2 = pendingInput;
                        if (inputEnd > inputPtr) {
                            final byte[] inputBuffer = this._inputBuffer;
                            final int inputPtr2 = inputPtr + 1;
                            super._inputPtr = inputPtr2;
                            pendingInput2 = (pendingInput | (inputBuffer[inputPtr] & 0xFF) << 8);
                            if (inputEnd > inputPtr2) {
                                super._inputPtr = inputPtr2 + 1;
                                pendingInput2 |= (inputBuffer[inputPtr2] & 0xFF) << 16;
                            }
                        }
                        super._pendingInput = pendingInput2;
                        return 257;
                    }
                    super._textBuilder.resetWithSurrogate(this.decodeUtf8_4(pendingInput));
                    break Label_0376;
                }
                case 6: {
                    final int inputEnd2 = super._inputEnd;
                    final int inputPtr3 = super._inputPtr;
                    if (inputEnd2 - inputPtr3 < 2) {
                        int pendingInput3 = pendingInput;
                        if (inputEnd2 > inputPtr3) {
                            final byte[] inputBuffer2 = this._inputBuffer;
                            super._inputPtr = inputPtr3 + 1;
                            pendingInput3 = (pendingInput | (inputBuffer2[inputPtr3] & 0xFF) << 8);
                        }
                        super._pendingInput = pendingInput3;
                        return 257;
                    }
                    pendingInput = this.decodeUtf8_3(pendingInput);
                    break;
                }
                case 5: {
                    if (super._inputPtr >= super._inputEnd) {
                        super._pendingInput = pendingInput;
                        return 257;
                    }
                    pendingInput = this.decodeUtf8_2(pendingInput);
                    break;
                }
                case 4: {
                    this.reportInvalidInitial(pendingInput);
                    break;
                }
                case 3: {
                    this.markLF();
                    break;
                }
                case 1: {
                    this.handleInvalidXmlChar(pendingInput);
                }
                case 2: {
                    final int inputPtr4 = super._inputPtr;
                    if (inputPtr4 >= super._inputEnd) {
                        super._pendingInput = -1;
                        return 257;
                    }
                    if (this._inputBuffer[inputPtr4] == 10) {
                        super._inputPtr = inputPtr4 + 1;
                    }
                    this.markLF();
                    pendingInput = 10;
                    break;
                }
            }
            super._textBuilder.resetWithChar((char)pendingInput);
        }
        if (super._cfgCoalescing && !super._cfgLazyParsing) {
            return this.finishCharactersCoalescing();
        }
        super._currToken = 4;
        if (super._cfgLazyParsing) {
            super._tokenIncomplete = true;
        }
        else {
            this.finishCharacters();
        }
        return super._currToken;
    }
    
    public int startCharactersPending() {
        final int inputPtr = super._inputPtr;
        final int inputEnd = super._inputEnd;
        if (inputPtr >= inputEnd) {
            return 257;
        }
        final int pendingInput = super._pendingInput;
        super._pendingInput = 0;
        Label_0452: {
            if (pendingInput == -1) {
                if (this._inputBuffer[inputPtr] == 10) {
                    super._inputPtr = inputPtr + 1;
                }
                this.markLF();
                super._textBuilder.resetWithChar('\n');
            }
            else {
                final int[] text_CHARS = super._charTypes.TEXT_CHARS;
                final int n = pendingInput & 0xFF;
                final int n2 = text_CHARS[n];
                if (n2 != 5) {
                    int pendingInput2 = 0;
                    Label_0171: {
                        int n11 = 0;
                        Label_0167: {
                            int n3 = 0;
                            Label_0162: {
                                if (n2 == 6) {
                                    final byte[] inputBuffer = this._inputBuffer;
                                    final int inputPtr2 = inputPtr + 1;
                                    super._inputPtr = inputPtr2;
                                    n3 = (inputBuffer[inputPtr] & 0xFF);
                                    final int n4 = pendingInput >> 8;
                                    int n5;
                                    if (n4 == 0) {
                                        if (inputPtr2 >= inputEnd) {
                                            break Label_0162;
                                        }
                                        super._inputPtr = inputPtr2 + 1;
                                        n5 = this.decodeUtf8_3(pendingInput, n3, inputBuffer[inputPtr2] & 0xFF);
                                    }
                                    else {
                                        n5 = this.decodeUtf8_3(n, n4, n3);
                                    }
                                    super._textBuilder.resetWithChar((char)n5);
                                    break Label_0452;
                                }
                                if (n2 != 7) {
                                    this.throwInternal();
                                    break Label_0452;
                                }
                                final byte[] inputBuffer2 = this._inputBuffer;
                                final int inputPtr3 = inputPtr + 1;
                                super._inputPtr = inputPtr3;
                                n3 = (inputBuffer2[inputPtr] & 0xFF);
                                final int n6 = pendingInput >> 8;
                                int n8;
                                if (n6 == 0) {
                                    if (inputPtr3 >= inputEnd) {
                                        break Label_0162;
                                    }
                                    final int inputPtr4 = inputPtr3 + 1;
                                    super._inputPtr = inputPtr4;
                                    final int n7 = inputBuffer2[inputPtr3] & 0xFF;
                                    if (inputPtr4 >= inputEnd) {
                                        pendingInput2 = (n3 << 8 | pendingInput | n7 << 16);
                                        break Label_0171;
                                    }
                                    super._inputPtr = inputPtr4 + 1;
                                    n8 = this.decodeUtf8_4(pendingInput, n3, n7, inputBuffer2[inputPtr4] & 0xFF);
                                }
                                else {
                                    final int n9 = n6 & 0xFF;
                                    final int n10 = pendingInput >> 16;
                                    if (n10 == 0) {
                                        if (inputPtr3 >= inputEnd) {
                                            n11 = n3 << 16;
                                            break Label_0167;
                                        }
                                        super._inputPtr = inputPtr3 + 1;
                                        n8 = this.decodeUtf8_4(n, n9, n3, inputBuffer2[inputPtr3] & 0xFF);
                                    }
                                    else {
                                        n8 = this.decodeUtf8_4(n, n9, n10, n3);
                                    }
                                }
                                super._textBuilder.resetWithSurrogate(n8);
                                return super._currToken = 4;
                            }
                            n11 = n3 << 8;
                        }
                        pendingInput2 = (n11 | pendingInput);
                    }
                    super._pendingInput = pendingInput2;
                    return 257;
                }
                super._textBuilder.resetWithChar((char)this.decodeUtf8_2(pendingInput));
            }
        }
        if (super._cfgCoalescing && !super._cfgLazyParsing) {
            return this.finishCharactersCoalescing();
        }
        super._currToken = 4;
        if (super._cfgLazyParsing) {
            super._tokenIncomplete = true;
        }
        else {
            this.finishCharacters();
        }
        return super._currToken;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("asyncScanner; curr=");
        sb.append(super._currToken);
        sb.append(" next=");
        sb.append(super._nextEvent);
        sb.append(", state = ");
        sb.append(super._state);
        return sb.toString();
    }
}

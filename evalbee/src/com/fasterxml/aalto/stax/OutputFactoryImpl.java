// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.stax;

import org.codehaus.stax2.ri.Stax2WriterAdapter;
import javax.xml.stream.XMLStreamWriter;
import org.codehaus.stax2.ri.Stax2EventWriterImpl;
import javax.xml.stream.XMLEventWriter;
import com.fasterxml.aalto.impl.IoStreamException;
import com.fasterxml.aalto.util.URLUtil;
import com.fasterxml.aalto.dom.DOMWriterImpl;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamResult;
import com.fasterxml.aalto.impl.StreamExceptionBase;
import org.codehaus.stax2.io.Stax2Result;
import javax.xml.transform.Result;
import com.fasterxml.aalto.out.WNameTable;
import com.fasterxml.aalto.out.XmlWriter;
import com.fasterxml.aalto.out.NonRepairingStreamWriter;
import com.fasterxml.aalto.out.RepairingStreamWriter;
import com.fasterxml.aalto.out.CharXmlWriter;
import java.io.OutputStreamWriter;
import com.fasterxml.aalto.out.AsciiXmlWriter;
import com.fasterxml.aalto.out.Latin1XmlWriter;
import java.io.IOException;
import javax.xml.stream.XMLStreamException;
import com.fasterxml.aalto.out.WNameFactory;
import com.fasterxml.aalto.out.Utf8XmlWriter;
import com.fasterxml.aalto.util.CharsetNames;
import org.codehaus.stax2.XMLStreamWriter2;
import java.io.Writer;
import java.io.OutputStream;
import com.fasterxml.aalto.out.WriterConfig;
import org.codehaus.stax2.XMLOutputFactory2;

public final class OutputFactoryImpl extends XMLOutputFactory2
{
    protected final WriterConfig _config;
    
    public OutputFactoryImpl() {
        this._config = new WriterConfig();
    }
    
    private XMLStreamWriter2 createSW(final OutputStream out, final Writer writer, final String s, final boolean b) {
        final WriterConfig nonShared = this._config.createNonShared();
        if (b) {
            nonShared.doAutoCloseOutput(true);
        }
        XmlWriter xmlWriter = null;
        WNameTable wNameTable = null;
        Label_0243: {
            if (writer == null) {
                String normalize;
                if (s == null) {
                    normalize = "UTF-8";
                }
                else {
                    normalize = s;
                    if (s != "UTF-8" && (normalize = s) != "ISO-8859-1" && (normalize = s) != "US-ASCII") {
                        normalize = CharsetNames.normalize(s);
                    }
                }
                nonShared.setActualEncodingIfNotSet(normalize);
                if (normalize == "UTF-8") {
                    try {
                        xmlWriter = new Utf8XmlWriter(nonShared, out);
                        wNameTable = this._config.getUtf8Symbols(xmlWriter);
                        break Label_0243;
                    }
                    catch (final IOException th) {
                        throw new XMLStreamException(th);
                    }
                }
                if (normalize == "ISO-8859-1") {
                    xmlWriter = new Latin1XmlWriter(nonShared, out);
                    wNameTable = this._config.getLatin1Symbols(xmlWriter);
                }
                else if (normalize == "US-ASCII") {
                    xmlWriter = new AsciiXmlWriter(nonShared, out);
                    wNameTable = this._config.getAsciiSymbols(xmlWriter);
                }
                else {
                    xmlWriter = new CharXmlWriter(nonShared, new OutputStreamWriter(out, normalize));
                    wNameTable = this._config.getCharSymbols(xmlWriter);
                }
            }
            else {
                String encoding;
                if ((encoding = s) == null) {
                    encoding = CharsetNames.findEncodingFor(writer);
                }
                if (encoding != null) {
                    nonShared.setActualEncodingIfNotSet(encoding);
                }
                xmlWriter = new CharXmlWriter(nonShared, writer);
                wNameTable = this._config.getCharSymbols(xmlWriter);
            }
        }
        if (nonShared.willRepairNamespaces()) {
            return (XMLStreamWriter2)new RepairingStreamWriter(nonShared, xmlWriter, wNameTable);
        }
        return (XMLStreamWriter2)new NonRepairingStreamWriter(nonShared, xmlWriter, wNameTable);
    }
    
    private XMLStreamWriter2 createSW(final Result result) {
        OutputStream outputStream = null;
        Writer writer = null;
        boolean b = false;
        String s = null;
        Label_0126: {
            if (result instanceof Stax2Result) {
                final Stax2Result stax2Result = (Stax2Result)result;
                try {
                    outputStream = stax2Result.constructOutputStream();
                    if (outputStream == null) {
                        writer = stax2Result.constructWriter();
                    }
                    else {
                        writer = null;
                    }
                    b = true;
                    s = null;
                    break Label_0126;
                }
                catch (final IOException ex) {
                    throw new StreamExceptionBase(ex);
                }
            }
            if (result instanceof StreamResult) {
                final StreamResult streamResult = (StreamResult)result;
                s = streamResult.getSystemId();
                outputStream = streamResult.getOutputStream();
                if (outputStream == null) {
                    writer = streamResult.getWriter();
                }
                else {
                    writer = null;
                }
                b = false;
            }
            else if (result instanceof SAXResult) {
                s = ((SAXResult)result).getSystemId();
                if (s == null || s.length() == 0) {
                    throw new StreamExceptionBase("Can not create a stream writer for a SAXResult that does not have System Id (support for using SAX input source not implemented)");
                }
                b = true;
                writer = null;
                outputStream = null;
            }
            else {
                if (result instanceof DOMResult) {
                    return (XMLStreamWriter2)DOMWriterImpl.createFrom(this._config.createNonShared(), (DOMResult)result);
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Can not create XMLStreamWriter for Result type ");
                sb.append(result.getClass());
                sb.append(" (unrecognized type)");
                throw new IllegalArgumentException(sb.toString());
            }
        }
        if (outputStream != null) {
            return this.createSW(outputStream, null, null, b);
        }
        if (writer != null) {
            return this.createSW(null, writer, null, b);
        }
        if (s != null && s.length() > 0) {
            try {
                return this.createSW(URLUtil.outputStreamFromURL(URLUtil.urlFromSystemId(s)), null, null, true);
            }
            catch (final IOException ex2) {
                throw new IoStreamException(ex2);
            }
        }
        throw new StreamExceptionBase("Can not create XMLStreamWriter for passed-in Result -- neither writer, output stream nor system id (to create one) was accessible");
    }
    
    public void configureForRobustness() {
        this._config.configureForRobustness();
    }
    
    public void configureForSpeed() {
        this._config.configureForSpeed();
    }
    
    public void configureForXmlConformance() {
        this._config.configureForXmlConformance();
    }
    
    public XMLEventWriter createXMLEventWriter(final OutputStream outputStream) {
        return this.createXMLEventWriter(outputStream, null);
    }
    
    public XMLEventWriter createXMLEventWriter(final OutputStream outputStream, final String s) {
        return (XMLEventWriter)new Stax2EventWriterImpl(this.createSW(outputStream, null, s, false));
    }
    
    public XMLEventWriter createXMLEventWriter(final Writer writer) {
        return (XMLEventWriter)new Stax2EventWriterImpl(this.createSW(null, writer, null, false));
    }
    
    public XMLEventWriter createXMLEventWriter(final Writer writer, final String s) {
        return (XMLEventWriter)new Stax2EventWriterImpl(this.createSW(null, writer, s, false));
    }
    
    public XMLEventWriter createXMLEventWriter(final XMLStreamWriter xmlStreamWriter) {
        return (XMLEventWriter)new Stax2EventWriterImpl(Stax2WriterAdapter.wrapIfNecessary(xmlStreamWriter));
    }
    
    public XMLEventWriter createXMLEventWriter(final Result result) {
        return (XMLEventWriter)new Stax2EventWriterImpl(this.createSW(result));
    }
    
    public XMLStreamWriter createXMLStreamWriter(final OutputStream outputStream) {
        return this.createXMLStreamWriter(outputStream, null);
    }
    
    public XMLStreamWriter createXMLStreamWriter(final OutputStream outputStream, final String s) {
        return (XMLStreamWriter)this.createSW(outputStream, null, s, false);
    }
    
    public XMLStreamWriter createXMLStreamWriter(final Writer writer) {
        return (XMLStreamWriter)this.createSW(null, writer, null, false);
    }
    
    public XMLStreamWriter createXMLStreamWriter(final Result result) {
        return (XMLStreamWriter)this.createSW(result);
    }
    
    public XMLStreamWriter2 createXMLStreamWriter(final Writer writer, final String s) {
        return this.createSW(null, writer, s, false);
    }
    
    public Object getProperty(final String s) {
        return this._config.getProperty(s, true);
    }
    
    public boolean isPropertySupported(final String s) {
        return this._config.isPropertySupported(s);
    }
    
    public void setProperty(final String s, final Object o) {
        this._config.setProperty(s, o);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.stax;

import javax.xml.namespace.QName;
import org.codehaus.stax2.ri.Stax2EventFactoryImpl;

public final class EventFactoryImpl extends Stax2EventFactoryImpl
{
    public QName createQName(final String namespaceURI, final String localPart) {
        return new QName(namespaceURI, localPart);
    }
    
    public QName createQName(final String namespaceURI, final String localPart, final String prefix) {
        return new QName(namespaceURI, localPart, prefix);
    }
}

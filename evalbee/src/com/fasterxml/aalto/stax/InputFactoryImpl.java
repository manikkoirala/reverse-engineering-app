// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.stax;

import javax.xml.stream.XMLResolver;
import javax.xml.stream.XMLReporter;
import org.codehaus.stax2.ri.Stax2ReaderAdapter;
import org.codehaus.stax2.ri.Stax2FilteredStreamReader;
import javax.xml.stream.StreamFilter;
import javax.xml.stream.XMLStreamReader;
import org.codehaus.stax2.ri.evt.Stax2FilteredEventReader;
import org.codehaus.stax2.ri.evt.Stax2EventReaderAdapter;
import javax.xml.stream.EventFilter;
import javax.xml.stream.XMLEventReader;
import com.fasterxml.aalto.evt.EventAllocatorImpl;
import com.fasterxml.aalto.async.AsyncByteArrayScanner;
import com.fasterxml.aalto.AsyncByteArrayFeeder;
import com.fasterxml.aalto.async.AsyncByteScanner;
import com.fasterxml.aalto.async.AsyncStreamReaderImpl;
import com.fasterxml.aalto.async.AsyncByteBufferScanner;
import com.fasterxml.aalto.AsyncByteBufferFeeder;
import com.fasterxml.aalto.AsyncXMLStreamReader;
import java.nio.ByteBuffer;
import org.codehaus.stax2.io.Stax2CharArraySource;
import org.codehaus.stax2.io.Stax2ByteArraySource;
import org.xml.sax.InputSource;
import javax.xml.stream.XMLStreamException;
import com.fasterxml.aalto.dom.DOMReaderImpl;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamSource;
import org.codehaus.stax2.io.Stax2Source;
import javax.xml.transform.Source;
import java.net.URL;
import com.fasterxml.aalto.in.CharSourceBootstrapper;
import java.io.Reader;
import java.io.IOException;
import com.fasterxml.aalto.impl.IoStreamException;
import com.fasterxml.aalto.in.InputBootstrapper;
import java.io.InputStream;
import com.fasterxml.aalto.in.ByteSourceBootstrapper;
import java.io.FileInputStream;
import com.fasterxml.aalto.util.URLUtil;
import java.io.File;
import com.fasterxml.aalto.evt.EventReaderImpl;
import org.codehaus.stax2.XMLEventReader2;
import org.codehaus.stax2.XMLStreamReader2;
import com.fasterxml.aalto.in.ReaderConfig;
import javax.xml.stream.util.XMLEventAllocator;
import com.fasterxml.aalto.AsyncXMLInputFactory;

public final class InputFactoryImpl extends AsyncXMLInputFactory
{
    protected XMLEventAllocator _allocator;
    final ReaderConfig _config;
    
    public InputFactoryImpl() {
        this._allocator = null;
        this._config = new ReaderConfig();
    }
    
    public void configureForConvenience() {
        this._config.configureForConvenience();
    }
    
    public void configureForLowMemUsage() {
        this._config.configureForLowMemUsage();
    }
    
    public void configureForRoundTripping() {
        this._config.configureForRoundTripping();
    }
    
    public void configureForSpeed() {
        this._config.configureForSpeed();
    }
    
    public void configureForXmlConformance() {
        this._config.configureForXmlConformance();
    }
    
    public XMLEventReader2 constructER(final XMLStreamReader2 xmlStreamReader2) {
        return (XMLEventReader2)new EventReaderImpl(this.createEventAllocator(), xmlStreamReader2);
    }
    
    public XMLStreamReader2 constructSR(final File file, final boolean b) {
        try {
            return (XMLStreamReader2)StreamReaderImpl.construct(ByteSourceBootstrapper.construct(this.getNonSharedConfig(URLUtil.fileToSystemId(file), null, null, b, true), new FileInputStream(file)));
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    public XMLStreamReader2 constructSR(final InputStream inputStream, final String s, final boolean b) {
        return (XMLStreamReader2)StreamReaderImpl.construct(ByteSourceBootstrapper.construct(this.getNonSharedConfig(null, null, s, b, false), inputStream));
    }
    
    public XMLStreamReader2 constructSR(final String s, final InputStream inputStream, final boolean b) {
        return (XMLStreamReader2)StreamReaderImpl.construct(ByteSourceBootstrapper.construct(this.getNonSharedConfig(null, s, null, b, false), inputStream));
    }
    
    public XMLStreamReader2 constructSR(final String s, final Reader reader, final boolean b) {
        return (XMLStreamReader2)StreamReaderImpl.construct(CharSourceBootstrapper.construct(this.getNonSharedConfig(null, s, null, b, false), reader));
    }
    
    public XMLStreamReader2 constructSR(final URL url, final boolean b) {
        try {
            return (XMLStreamReader2)StreamReaderImpl.construct(ByteSourceBootstrapper.construct(this.getNonSharedConfig(URLUtil.urlToSystemId(url), null, null, b, true), URLUtil.inputStreamFromURL(url)));
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    public XMLStreamReader2 constructSR(final Source source, final boolean b) {
        if (source instanceof Stax2Source) {
            return this.constructSR2((Stax2Source)source, b);
        }
        String publicId = null;
        String encoding = null;
        final boolean b2 = source instanceof StreamSource;
        final Reader reader = null;
        InputStream inputStream = null;
        final Reader reader2 = null;
        String systemId;
        String s;
        String s2;
        Reader reader4;
        if (b2) {
            final StreamSource streamSource = (StreamSource)source;
            systemId = streamSource.getSystemId();
            final String publicId2 = streamSource.getPublicId();
            inputStream = streamSource.getInputStream();
            Reader reader3 = reader2;
            if (inputStream == null) {
                reader3 = streamSource.getReader();
            }
            s = publicId2;
            s2 = null;
            reader4 = reader3;
        }
        else if (source instanceof SAXSource) {
            final SAXSource saxSource = (SAXSource)source;
            String systemId2 = saxSource.getSystemId();
            final InputSource inputSource = saxSource.getInputSource();
            Reader reader5;
            if (inputSource != null) {
                final String systemId3 = inputSource.getSystemId();
                publicId = inputSource.getPublicId();
                encoding = inputSource.getEncoding();
                inputStream = inputSource.getByteStream();
                Reader characterStream = reader;
                if (inputStream == null) {
                    characterStream = inputSource.getCharacterStream();
                }
                reader5 = characterStream;
                systemId2 = systemId3;
            }
            else {
                reader5 = null;
            }
            s = publicId;
            s2 = encoding;
            reader4 = reader5;
            systemId = systemId2;
        }
        else {
            if (source instanceof DOMSource) {
                return (XMLStreamReader2)DOMReaderImpl.createFrom((DOMSource)source, this.getNonSharedConfig(null, null, null, b, false));
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Can not instantiate StAX reader for XML source type ");
            sb.append(source.getClass());
            sb.append(" (unrecognized type)");
            throw new IllegalArgumentException(sb.toString());
        }
        if (inputStream != null) {
            return (XMLStreamReader2)StreamReaderImpl.construct(ByteSourceBootstrapper.construct(this.getNonSharedConfig(s, systemId, s2, b, false), inputStream));
        }
        if (reader4 != null) {
            return (XMLStreamReader2)StreamReaderImpl.construct(CharSourceBootstrapper.construct(this.getNonSharedConfig(s, systemId, s2, b, false), reader4));
        }
        if (systemId != null && systemId.length() > 0) {
            final ReaderConfig nonSharedConfig = this.getNonSharedConfig(s, systemId, s2, b, true);
            try {
                return (XMLStreamReader2)StreamReaderImpl.construct(ByteSourceBootstrapper.construct(nonSharedConfig, URLUtil.inputStreamFromURL(URLUtil.urlFromSystemId(systemId))));
            }
            catch (final IOException ex) {
                throw new IoStreamException(ex);
            }
        }
        throw new XMLStreamException("Can not create Stax reader for the Source passed -- neither reader, input stream nor system id was accessible; can not use other types of sources (like embedded SAX streams)");
    }
    
    public XMLStreamReader2 constructSR2(final Stax2Source stax2Source, final boolean b) {
        final ReaderConfig nonSharedConfig = this.getNonSharedConfig(stax2Source.getPublicId(), stax2Source.getSystemId(), stax2Source.getEncoding(), b, true);
        InputBootstrapper inputBootstrapper;
        if (stax2Source instanceof Stax2ByteArraySource) {
            final Stax2ByteArraySource stax2ByteArraySource = (Stax2ByteArraySource)stax2Source;
            inputBootstrapper = ByteSourceBootstrapper.construct(nonSharedConfig, stax2ByteArraySource.getBuffer(), stax2ByteArraySource.getBufferStart(), stax2ByteArraySource.getBufferLength());
        }
        else if (stax2Source instanceof Stax2CharArraySource) {
            final Stax2CharArraySource stax2CharArraySource = (Stax2CharArraySource)stax2Source;
            inputBootstrapper = CharSourceBootstrapper.construct(nonSharedConfig, stax2CharArraySource.getBuffer(), stax2CharArraySource.getBufferStart(), stax2CharArraySource.getBufferLength());
        }
        else {
            try {
                final InputStream constructInputStream = stax2Source.constructInputStream();
                if (constructInputStream != null) {
                    return (XMLStreamReader2)StreamReaderImpl.construct(ByteSourceBootstrapper.construct(nonSharedConfig, constructInputStream));
                }
                final Reader constructReader = stax2Source.constructReader();
                if (constructReader != null) {
                    return (XMLStreamReader2)StreamReaderImpl.construct(CharSourceBootstrapper.construct(nonSharedConfig, constructReader));
                }
                throw new IllegalArgumentException("Can not create stream reader for given Stax2Source: neither InputStream nor Reader available");
            }
            catch (final IOException ex) {
                throw new IoStreamException(ex);
            }
        }
        return (XMLStreamReader2)StreamReaderImpl.construct(inputBootstrapper);
    }
    
    @Override
    public AsyncXMLStreamReader<AsyncByteBufferFeeder> createAsyncFor(final ByteBuffer byteBuffer) {
        final ReaderConfig nonSharedConfig = this.getNonSharedConfig(null, null, null, false, false);
        nonSharedConfig.setActualEncoding("UTF-8");
        final AsyncByteBufferScanner asyncByteBufferScanner = new AsyncByteBufferScanner(nonSharedConfig);
        asyncByteBufferScanner.feedInput(byteBuffer);
        return new AsyncStreamReaderImpl<AsyncByteBufferFeeder>(asyncByteBufferScanner);
    }
    
    @Override
    public AsyncXMLStreamReader<AsyncByteArrayFeeder> createAsyncFor(final byte[] array) {
        return this.createAsyncFor(array, 0, array.length);
    }
    
    @Override
    public AsyncXMLStreamReader<AsyncByteArrayFeeder> createAsyncFor(final byte[] array, final int n, final int n2) {
        final ReaderConfig nonSharedConfig = this.getNonSharedConfig(null, null, null, false, false);
        nonSharedConfig.setActualEncoding("UTF-8");
        final AsyncByteArrayScanner asyncByteArrayScanner = new AsyncByteArrayScanner(nonSharedConfig);
        asyncByteArrayScanner.feedInput(array, n, n2);
        return new AsyncStreamReaderImpl<AsyncByteArrayFeeder>(asyncByteArrayScanner);
    }
    
    @Override
    public AsyncXMLStreamReader<AsyncByteArrayFeeder> createAsyncForByteArray() {
        final ReaderConfig nonSharedConfig = this.getNonSharedConfig(null, null, null, false, false);
        nonSharedConfig.setActualEncoding("UTF-8");
        return new AsyncStreamReaderImpl<AsyncByteArrayFeeder>(new AsyncByteArrayScanner(nonSharedConfig));
    }
    
    @Override
    public AsyncXMLStreamReader<AsyncByteBufferFeeder> createAsyncForByteBuffer() {
        final ReaderConfig nonSharedConfig = this.getNonSharedConfig(null, null, null, false, false);
        nonSharedConfig.setActualEncoding("UTF-8");
        return new AsyncStreamReaderImpl<AsyncByteBufferFeeder>(new AsyncByteBufferScanner(nonSharedConfig));
    }
    
    public XMLEventAllocator createEventAllocator() {
        final XMLEventAllocator allocator = this._allocator;
        if (allocator != null) {
            return allocator.newInstance();
        }
        EventAllocatorImpl eventAllocatorImpl;
        if (this._config.willPreserveLocation()) {
            eventAllocatorImpl = EventAllocatorImpl.getDefaultInstance();
        }
        else {
            eventAllocatorImpl = EventAllocatorImpl.getFastInstance();
        }
        return (XMLEventAllocator)eventAllocatorImpl;
    }
    
    public XMLEventReader createFilteredReader(final XMLEventReader xmlEventReader, final EventFilter eventFilter) {
        return (XMLEventReader)new Stax2FilteredEventReader(Stax2EventReaderAdapter.wrapIfNecessary(xmlEventReader), eventFilter);
    }
    
    public XMLStreamReader createFilteredReader(final XMLStreamReader xmlStreamReader, final StreamFilter streamFilter) {
        final Stax2FilteredStreamReader stax2FilteredStreamReader = new Stax2FilteredStreamReader(xmlStreamReader, streamFilter);
        if (!streamFilter.accept((XMLStreamReader)stax2FilteredStreamReader)) {
            stax2FilteredStreamReader.next();
        }
        return (XMLStreamReader)stax2FilteredStreamReader;
    }
    
    public XMLEventReader createXMLEventReader(final InputStream inputStream) {
        return this.createXMLEventReader(inputStream, null);
    }
    
    public XMLEventReader createXMLEventReader(final InputStream inputStream, final String s) {
        return (XMLEventReader)this.constructER(this.constructSR(inputStream, s, true));
    }
    
    public XMLEventReader createXMLEventReader(final Reader reader) {
        return this.createXMLEventReader(null, reader);
    }
    
    public XMLEventReader createXMLEventReader(final String s, final InputStream inputStream) {
        return (XMLEventReader)this.constructER(this.constructSR(s, inputStream, true));
    }
    
    public XMLEventReader createXMLEventReader(final String s, final Reader reader) {
        return (XMLEventReader)this.constructER(this.constructSR(s, reader, true));
    }
    
    public XMLEventReader createXMLEventReader(final XMLStreamReader xmlStreamReader) {
        return (XMLEventReader)this.constructER(Stax2ReaderAdapter.wrapIfNecessary(xmlStreamReader));
    }
    
    public XMLEventReader createXMLEventReader(final Source source) {
        return (XMLEventReader)this.constructER(this.constructSR(source, true));
    }
    
    public XMLEventReader2 createXMLEventReader(final File file) {
        return this.constructER(this.constructSR(file, true));
    }
    
    public XMLEventReader2 createXMLEventReader(final URL url) {
        return this.constructER(this.constructSR(url, true));
    }
    
    public XMLStreamReader createXMLStreamReader(final InputStream inputStream) {
        return (XMLStreamReader)this.constructSR(inputStream, null, false);
    }
    
    public XMLStreamReader createXMLStreamReader(final InputStream inputStream, final String s) {
        return (XMLStreamReader)this.constructSR(inputStream, s, false);
    }
    
    public XMLStreamReader createXMLStreamReader(final Reader reader) {
        return (XMLStreamReader)this.constructSR(null, reader, false);
    }
    
    public XMLStreamReader createXMLStreamReader(final String s, final InputStream inputStream) {
        return (XMLStreamReader)this.constructSR(s, inputStream, false);
    }
    
    public XMLStreamReader createXMLStreamReader(final String s, final Reader reader) {
        return (XMLStreamReader)this.constructSR(s, reader, false);
    }
    
    public XMLStreamReader createXMLStreamReader(final Source source) {
        return (XMLStreamReader)this.constructSR(source, false);
    }
    
    public XMLStreamReader2 createXMLStreamReader(final File file) {
        return this.constructSR(file, false);
    }
    
    public XMLStreamReader2 createXMLStreamReader(final URL url) {
        return this.constructSR(url, false);
    }
    
    public XMLEventAllocator getEventAllocator() {
        return this._allocator;
    }
    
    public ReaderConfig getNonSharedConfig(final String s, final String s2, final String s3, final boolean b, final boolean b2) {
        final ReaderConfig nonShared = this._config.createNonShared(s2, s, s3);
        if (b) {
            nonShared.doParseLazily(false);
        }
        if (b2) {
            nonShared.doAutoCloseInput(true);
        }
        return nonShared;
    }
    
    public Object getProperty(final String s) {
        return this._config.getProperty(s, true);
    }
    
    public XMLReporter getXMLReporter() {
        return this._config.getXMLReporter();
    }
    
    public XMLResolver getXMLResolver() {
        return this._config.getXMLResolver();
    }
    
    public boolean isPropertySupported(final String s) {
        return this._config.isPropertySupported(s);
    }
    
    public void setEventAllocator(final XMLEventAllocator allocator) {
        this._allocator = allocator;
    }
    
    public void setProperty(final String s, final Object o) {
        this._config.setProperty(s, o);
    }
    
    public void setXMLReporter(final XMLReporter xmlReporter) {
        this._config.setXMLReporter(xmlReporter);
    }
    
    public void setXMLResolver(final XMLResolver xmlResolver) {
        this._config.setXMLResolver(xmlResolver);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.stax;

import com.fasterxml.aalto.util.XmlNames;
import com.fasterxml.aalto.WFCException;
import com.fasterxml.aalto.impl.IoStreamException;
import java.io.IOException;
import org.codehaus.stax2.validation.XMLValidator;
import org.codehaus.stax2.validation.XMLValidationSchema;
import org.codehaus.stax2.validation.ValidationProblemHandler;
import java.util.NoSuchElementException;
import java.io.Writer;
import java.util.Collections;
import org.codehaus.stax2.validation.DTDValidationSchema;
import javax.xml.namespace.NamespaceContext;
import com.fasterxml.aalto.util.TextAccumulator;
import org.codehaus.stax2.ri.Stax2Util$ByteAggregator;
import com.fasterxml.aalto.UncheckedStreamException;
import org.codehaus.stax2.XMLStreamLocation2;
import org.codehaus.stax2.ri.typed.ValueDecoderFactory$QNameDecoder;
import javax.xml.namespace.QName;
import org.codehaus.stax2.ri.typed.ValueDecoderFactory$LongArrayDecoder;
import org.codehaus.stax2.ri.typed.ValueDecoderFactory$LongDecoder;
import org.codehaus.stax2.ri.typed.ValueDecoderFactory$IntegerDecoder;
import java.math.BigInteger;
import org.codehaus.stax2.ri.typed.ValueDecoderFactory$IntArrayDecoder;
import org.codehaus.stax2.ri.typed.ValueDecoderFactory$IntDecoder;
import org.codehaus.stax2.ri.typed.ValueDecoderFactory$FloatArrayDecoder;
import org.codehaus.stax2.ri.typed.ValueDecoderFactory$FloatDecoder;
import org.codehaus.stax2.ri.typed.ValueDecoderFactory$DoubleArrayDecoder;
import org.codehaus.stax2.ri.typed.ValueDecoderFactory$DoubleDecoder;
import org.codehaus.stax2.ri.typed.ValueDecoderFactory$DecimalDecoder;
import java.math.BigDecimal;
import org.codehaus.stax2.ri.typed.ValueDecoderFactory$BooleanDecoder;
import org.codehaus.stax2.typed.Base64Variant;
import org.codehaus.stax2.typed.Base64Variants;
import org.codehaus.stax2.typed.TypedArrayDecoder;
import javax.xml.stream.XMLStreamException;
import com.fasterxml.aalto.impl.ErrorConsts;
import com.fasterxml.aalto.in.InputBootstrapper;
import org.codehaus.stax2.typed.TypedValueDecoder;
import javax.xml.stream.Location;
import org.codehaus.stax2.typed.TypedXMLStreamException;
import com.fasterxml.aalto.in.ReaderConfig;
import com.fasterxml.aalto.in.XmlScanner;
import org.codehaus.stax2.ri.typed.ValueDecoderFactory;
import com.fasterxml.aalto.in.PName;
import org.codehaus.stax2.ri.typed.CharArrayBase64Decoder;
import org.codehaus.stax2.LocationInfo;
import org.codehaus.stax2.DTDInfo;
import org.codehaus.stax2.AttributeInfo;
import org.codehaus.stax2.XMLStreamReader2;

public class StreamReaderImpl implements XMLStreamReader2, AttributeInfo, DTDInfo, LocationInfo
{
    private static final int MASK_GET_ELEMENT_TEXT = 4688;
    private static final int MASK_GET_TEXT = 6768;
    private static final int MASK_GET_TEXT_WITH_WRITER = 6776;
    private static final int MASK_GET_TEXT_XXX = 4208;
    private static final int MASK_TYPED_ACCESS_ARRAY = 4182;
    private static final int MASK_TYPED_ACCESS_BINARY = 4178;
    static final int STATE_CLOSED = 3;
    static final int STATE_EPILOG = 2;
    static final int STATE_PROLOG = 0;
    static final int STATE_TREE = 1;
    protected int _attrCount;
    protected CharArrayBase64Decoder _base64Decoder;
    protected final boolean _cfgCoalesceText;
    protected final boolean _cfgReportTextAsChars;
    protected PName _currName;
    protected int _currToken;
    protected ValueDecoderFactory _decoderFactory;
    protected PName _dtdRootName;
    protected int _parseState;
    protected final XmlScanner _scanner;
    
    public StreamReaderImpl(final XmlScanner scanner) {
        this._base64Decoder = null;
        this._scanner = scanner;
        this._currToken = 7;
        final ReaderConfig config = scanner.getConfig();
        this._cfgCoalesceText = config.willCoalesceText();
        this._cfgReportTextAsChars = (config.willReportCData() ^ true);
    }
    
    private TypedXMLStreamException _constructTypeException(final IllegalArgumentException ex, final String s) {
        return new TypedXMLStreamException(s, ex.getMessage(), (Location)this.getStartLocation(), ex);
    }
    
    private TypedXMLStreamException _constructTypeException(final String s, final String s2) {
        return new TypedXMLStreamException(s2, s, (Location)this.getStartLocation());
    }
    
    private void _handleEmptyValue(final TypedValueDecoder typedValueDecoder) {
        try {
            typedValueDecoder.handleEmptyValue();
        }
        catch (final IllegalArgumentException ex) {
            throw this._constructTypeException(ex, "");
        }
    }
    
    public static StreamReaderImpl construct(final InputBootstrapper inputBootstrapper) {
        return new StreamReaderImpl(inputBootstrapper.bootstrap());
    }
    
    private void throwNotTextXxx(final int n) {
        final StringBuilder sb = new StringBuilder();
        sb.append("getTextXxx() methods can not be called on ");
        sb.append(ErrorConsts.tokenTypeDesc(this._currToken));
        throw new IllegalStateException(sb.toString());
    }
    
    private void throwNotTextual(final int n) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Not a textual event (");
        sb.append(ErrorConsts.tokenTypeDesc(this._currToken));
        sb.append(")");
        throw new IllegalStateException(sb.toString());
    }
    
    public CharArrayBase64Decoder _base64Decoder() {
        if (this._base64Decoder == null) {
            this._base64Decoder = new CharArrayBase64Decoder();
        }
        return this._base64Decoder;
    }
    
    public void _closeScanner(final boolean b) {
        if (this._parseState != 3) {
            this._parseState = 3;
            if (this._currToken != 8) {
                this._currToken = 8;
            }
        }
        this._scanner.close(b);
    }
    
    public XMLStreamException _constructUnexpectedInTyped(final int n) {
        String string;
        if (n == 1) {
            string = "Element content can not contain child START_ELEMENT when using Typed Access methods";
        }
        else {
            final StringBuilder sb = new StringBuilder();
            sb.append("Expected a text token, got ");
            sb.append(ErrorConsts.tokenTypeDesc(n));
            string = sb.toString();
        }
        return (XMLStreamException)this._constructTypeException(string, null);
    }
    
    public final ValueDecoderFactory _decoderFactory() {
        if (this._decoderFactory == null) {
            this._decoderFactory = new ValueDecoderFactory();
        }
        return this._decoderFactory;
    }
    
    public void _reportNonTextEvent(final int n) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Expected a text token, got ");
        sb.append(ErrorConsts.tokenTypeDesc(n));
        sb.append(".");
        this.throwWfe(sb.toString());
    }
    
    public final void close() {
        this._closeScanner(false);
    }
    
    public final void closeCompletely() {
        this._closeScanner(true);
    }
    
    public final int findAttributeIndex(final String s, final String s2) {
        return this._scanner.findAttrIndex(s, s2);
    }
    
    public final void getAttributeAs(final int n, final TypedValueDecoder typedValueDecoder) {
        if (this._currToken == 1) {
            try {
                this._scanner.decodeAttrValue(n, typedValueDecoder);
                return;
            }
            catch (final IllegalArgumentException ex) {
                throw this._constructTypeException(ex, this.getAttributeValue(n));
            }
        }
        throw new IllegalStateException(ErrorConsts.ERR_STATE_NOT_STELEM);
    }
    
    public final int getAttributeAsArray(final int n, final TypedArrayDecoder typedArrayDecoder) {
        if (this._currToken == 1) {
            return this._scanner.decodeAttrValues(n, typedArrayDecoder);
        }
        throw new IllegalStateException(ErrorConsts.ERR_STATE_NOT_STELEM);
    }
    
    public final byte[] getAttributeAsBinary(final int n) {
        return this.getAttributeAsBinary(n, Base64Variants.getDefaultVariant());
    }
    
    public final byte[] getAttributeAsBinary(final int n, final Base64Variant base64Variant) {
        if (this._currToken == 1) {
            return this._scanner.decodeAttrBinaryValue(n, base64Variant, this._base64Decoder());
        }
        throw new IllegalStateException(ErrorConsts.ERR_STATE_NOT_STELEM);
    }
    
    public final boolean getAttributeAsBoolean(final int n) {
        final ValueDecoderFactory$BooleanDecoder booleanDecoder = this._decoderFactory().getBooleanDecoder();
        this.getAttributeAs(n, (TypedValueDecoder)booleanDecoder);
        return booleanDecoder.getValue();
    }
    
    public final BigDecimal getAttributeAsDecimal(final int n) {
        final ValueDecoderFactory$DecimalDecoder decimalDecoder = this._decoderFactory().getDecimalDecoder();
        this.getAttributeAs(n, (TypedValueDecoder)decimalDecoder);
        return decimalDecoder.getValue();
    }
    
    public final double getAttributeAsDouble(final int n) {
        final ValueDecoderFactory$DoubleDecoder doubleDecoder = this._decoderFactory().getDoubleDecoder();
        this.getAttributeAs(n, (TypedValueDecoder)doubleDecoder);
        return doubleDecoder.getValue();
    }
    
    public final double[] getAttributeAsDoubleArray(final int n) {
        final ValueDecoderFactory$DoubleArrayDecoder doubleArrayDecoder = this._decoderFactory().getDoubleArrayDecoder();
        this.getAttributeAsArray(n, (TypedArrayDecoder)doubleArrayDecoder);
        return doubleArrayDecoder.getValues();
    }
    
    public final float getAttributeAsFloat(final int n) {
        final ValueDecoderFactory$FloatDecoder floatDecoder = this._decoderFactory().getFloatDecoder();
        this.getAttributeAs(n, (TypedValueDecoder)floatDecoder);
        return floatDecoder.getValue();
    }
    
    public final float[] getAttributeAsFloatArray(final int n) {
        final ValueDecoderFactory$FloatArrayDecoder floatArrayDecoder = this._decoderFactory().getFloatArrayDecoder();
        this.getAttributeAsArray(n, (TypedArrayDecoder)floatArrayDecoder);
        return floatArrayDecoder.getValues();
    }
    
    public final int getAttributeAsInt(final int n) {
        final ValueDecoderFactory$IntDecoder intDecoder = this._decoderFactory().getIntDecoder();
        this.getAttributeAs(n, (TypedValueDecoder)intDecoder);
        return intDecoder.getValue();
    }
    
    public final int[] getAttributeAsIntArray(final int n) {
        final ValueDecoderFactory$IntArrayDecoder intArrayDecoder = this._decoderFactory().getIntArrayDecoder();
        this.getAttributeAsArray(n, (TypedArrayDecoder)intArrayDecoder);
        return intArrayDecoder.getValues();
    }
    
    public final BigInteger getAttributeAsInteger(final int n) {
        final ValueDecoderFactory$IntegerDecoder integerDecoder = this._decoderFactory().getIntegerDecoder();
        this.getAttributeAs(n, (TypedValueDecoder)integerDecoder);
        return integerDecoder.getValue();
    }
    
    public final long getAttributeAsLong(final int n) {
        final ValueDecoderFactory$LongDecoder longDecoder = this._decoderFactory().getLongDecoder();
        this.getAttributeAs(n, (TypedValueDecoder)longDecoder);
        return longDecoder.getValue();
    }
    
    public final long[] getAttributeAsLongArray(final int n) {
        final ValueDecoderFactory$LongArrayDecoder longArrayDecoder = this._decoderFactory().getLongArrayDecoder();
        this.getAttributeAsArray(n, (TypedArrayDecoder)longArrayDecoder);
        return longArrayDecoder.getValues();
    }
    
    public final QName getAttributeAsQName(final int n) {
        final ValueDecoderFactory$QNameDecoder qNameDecoder = this._decoderFactory().getQNameDecoder(this.getNamespaceContext());
        this.getAttributeAs(n, (TypedValueDecoder)qNameDecoder);
        return this.verifyQName(qNameDecoder.getValue());
    }
    
    public final int getAttributeCount() {
        if (this._currToken == 1) {
            return this._attrCount;
        }
        throw new IllegalStateException(ErrorConsts.ERR_STATE_NOT_STELEM);
    }
    
    public final int getAttributeIndex(final String s, final String s2) {
        if (this._currToken == 1) {
            return this.findAttributeIndex(s, s2);
        }
        throw new IllegalStateException(ErrorConsts.ERR_STATE_NOT_STELEM);
    }
    
    public final AttributeInfo getAttributeInfo() {
        if (this._currToken == 1) {
            return (AttributeInfo)this;
        }
        throw new IllegalStateException(ErrorConsts.ERR_STATE_NOT_STELEM);
    }
    
    public final String getAttributeLocalName(final int n) {
        if (this._currToken == 1) {
            if (n >= this._attrCount || n < 0) {
                this.reportInvalidAttrIndex(n);
            }
            return this._scanner.getAttrLocalName(n);
        }
        throw new IllegalStateException(ErrorConsts.ERR_STATE_NOT_STELEM);
    }
    
    public final QName getAttributeName(final int n) {
        if (this._currToken == 1) {
            if (n >= this._attrCount || n < 0) {
                this.reportInvalidAttrIndex(n);
            }
            return this._scanner.getAttrQName(n);
        }
        throw new IllegalStateException(ErrorConsts.ERR_STATE_NOT_STELEM);
    }
    
    public final String getAttributeNamespace(final int n) {
        if (this._currToken == 1) {
            if (n >= this._attrCount || n < 0) {
                this.reportInvalidAttrIndex(n);
            }
            String attrNsURI;
            if ((attrNsURI = this._scanner.getAttrNsURI(n)) == null) {
                attrNsURI = "";
            }
            return attrNsURI;
        }
        throw new IllegalStateException(ErrorConsts.ERR_STATE_NOT_STELEM);
    }
    
    public final String getAttributePrefix(final int n) {
        if (this._currToken == 1) {
            if (n >= this._attrCount || n < 0) {
                this.reportInvalidAttrIndex(n);
            }
            String attrPrefix;
            if ((attrPrefix = this._scanner.getAttrPrefix(n)) == null) {
                attrPrefix = "";
            }
            return attrPrefix;
        }
        throw new IllegalStateException(ErrorConsts.ERR_STATE_NOT_STELEM);
    }
    
    public final String getAttributeType(final int n) {
        if (this._currToken == 1) {
            if (n >= this._attrCount || n < 0) {
                this.reportInvalidAttrIndex(n);
            }
            return this._scanner.getAttrType(n);
        }
        throw new IllegalStateException(ErrorConsts.ERR_STATE_NOT_STELEM);
    }
    
    public final String getAttributeValue(final int n) {
        if (this._currToken == 1) {
            if (n >= this._attrCount || n < 0) {
                this.reportInvalidAttrIndex(n);
            }
            return this._scanner.getAttrValue(n);
        }
        throw new IllegalStateException(ErrorConsts.ERR_STATE_NOT_STELEM);
    }
    
    public final String getAttributeValue(final String s, final String s2) {
        if (this._currToken == 1) {
            return this._scanner.getAttrValue(s, s2);
        }
        throw new IllegalStateException(ErrorConsts.ERR_STATE_NOT_STELEM);
    }
    
    public final String getCharacterEncodingScheme() {
        return this._scanner.getConfig().getXmlDeclEncoding();
    }
    
    public ReaderConfig getConfig() {
        return this._scanner.getConfig();
    }
    
    public final XMLStreamLocation2 getCurrentLocation() {
        return this._scanner.getCurrentLocation();
    }
    
    public final DTDInfo getDTDInfo() {
        if (this._currToken != 11) {
            return null;
        }
        return (DTDInfo)this;
    }
    
    public final String getDTDInternalSubset() {
        if (this._currToken != 11) {
            return null;
        }
        try {
            return this._scanner.getText();
        }
        catch (final XMLStreamException ex) {
            throw UncheckedStreamException.createFrom(ex);
        }
    }
    
    public final String getDTDPublicId() {
        return this._scanner.getDTDPublicId();
    }
    
    public final String getDTDRootName() {
        final int currToken = this._currToken;
        String prefixedName = null;
        if (currToken != 11) {
            return null;
        }
        final PName currName = this._currName;
        if (currName != null) {
            prefixedName = currName.getPrefixedName();
        }
        return prefixedName;
    }
    
    public final String getDTDSystemId() {
        return this._scanner.getDTDSystemId();
    }
    
    public final int getDepth() {
        int depth = this._scanner.getDepth();
        if (this._currToken == 2) {
            ++depth;
        }
        return depth;
    }
    
    public final void getElementAs(final TypedValueDecoder typedValueDecoder) {
        final String trim = this.getElementText().trim();
        if (trim.length() == 0) {
            this._handleEmptyValue(typedValueDecoder);
            return;
        }
        try {
            typedValueDecoder.decode(trim);
        }
        catch (final IllegalArgumentException ex) {
            throw this._constructTypeException(ex, trim);
        }
    }
    
    public final byte[] getElementAsBinary() {
        return this.getElementAsBinary(Base64Variants.getDefaultVariant());
    }
    
    public final byte[] getElementAsBinary(final Base64Variant base64Variant) {
        final Stax2Util$ByteAggregator byteAggregator = this._base64Decoder().getByteAggregator();
        byte[] array = byteAggregator.startAggregation();
        int n = 0;
    Block_1:
        while (true) {
            int length = array.length;
            n = 0;
            int elementAsBinary;
            do {
                elementAsBinary = this.readElementAsBinary(array, n, length, base64Variant);
                if (elementAsBinary < 1) {
                    break Block_1;
                }
                n += elementAsBinary;
            } while ((length -= elementAsBinary) > 0);
            array = byteAggregator.addFullBlock(array);
        }
        return byteAggregator.aggregateAll(array, n);
    }
    
    public final boolean getElementAsBoolean() {
        final ValueDecoderFactory$BooleanDecoder booleanDecoder = this._decoderFactory().getBooleanDecoder();
        this.getElementAs((TypedValueDecoder)booleanDecoder);
        return booleanDecoder.getValue();
    }
    
    public final BigDecimal getElementAsDecimal() {
        final ValueDecoderFactory$DecimalDecoder decimalDecoder = this._decoderFactory().getDecimalDecoder();
        this.getElementAs((TypedValueDecoder)decimalDecoder);
        return decimalDecoder.getValue();
    }
    
    public final double getElementAsDouble() {
        final ValueDecoderFactory$DoubleDecoder doubleDecoder = this._decoderFactory().getDoubleDecoder();
        this.getElementAs((TypedValueDecoder)doubleDecoder);
        return doubleDecoder.getValue();
    }
    
    public final float getElementAsFloat() {
        final ValueDecoderFactory$FloatDecoder floatDecoder = this._decoderFactory().getFloatDecoder();
        this.getElementAs((TypedValueDecoder)floatDecoder);
        return floatDecoder.getValue();
    }
    
    public final int getElementAsInt() {
        final ValueDecoderFactory$IntDecoder intDecoder = this._decoderFactory().getIntDecoder();
        this.getElementAs((TypedValueDecoder)intDecoder);
        return intDecoder.getValue();
    }
    
    public final BigInteger getElementAsInteger() {
        final ValueDecoderFactory$IntegerDecoder integerDecoder = this._decoderFactory().getIntegerDecoder();
        this.getElementAs((TypedValueDecoder)integerDecoder);
        return integerDecoder.getValue();
    }
    
    public final long getElementAsLong() {
        final ValueDecoderFactory$LongDecoder longDecoder = this._decoderFactory().getLongDecoder();
        this.getElementAs((TypedValueDecoder)longDecoder);
        return longDecoder.getValue();
    }
    
    public final QName getElementAsQName() {
        final ValueDecoderFactory$QNameDecoder qNameDecoder = this._decoderFactory().getQNameDecoder(this.getNamespaceContext());
        this.getElementAs((TypedValueDecoder)qNameDecoder);
        return this.verifyQName(qNameDecoder.getValue());
    }
    
    public final String getElementText() {
        if (this._currToken != 1) {
            this.throwWfe(ErrorConsts.ERR_STATE_NOT_STELEM);
        }
        while (true) {
            final int next = this.next();
            if (next == 2) {
                return "";
            }
            if (next == 5) {
                continue;
            }
            if (next == 3) {
                continue;
            }
            if ((1 << next & 0x1250) == 0x0) {
                this._reportNonTextEvent(next);
            }
            final String text = this._scanner.getText();
            TextAccumulator textAccumulator = null;
            while (true) {
                final int next2 = this.next();
                if (next2 == 2) {
                    break;
                }
                if ((1 << next2 & 0x1250) != 0x0) {
                    TextAccumulator textAccumulator2;
                    if ((textAccumulator2 = textAccumulator) == null) {
                        textAccumulator2 = new TextAccumulator();
                        textAccumulator2.addText(text);
                    }
                    textAccumulator2.addText(this.getText());
                    textAccumulator = textAccumulator2;
                }
                else {
                    if (next2 == 5 || next2 == 3) {
                        continue;
                    }
                    this._reportNonTextEvent(next2);
                }
            }
            String andClear;
            if (textAccumulator == null) {
                andClear = text;
            }
            else {
                andClear = textAccumulator.getAndClear();
            }
            return andClear;
        }
    }
    
    public final String getEncoding() {
        return this._scanner.getConfig().getActualEncoding();
    }
    
    public final XMLStreamLocation2 getEndLocation() {
        return this._scanner.getEndLocation();
    }
    
    public final long getEndingByteOffset() {
        return this._scanner.getEndingByteOffset();
    }
    
    public final long getEndingCharOffset() {
        return this._scanner.getEndingCharOffset();
    }
    
    public final int getEventType() {
        int currToken;
        final int n = currToken = this._currToken;
        if (n == 12) {
            if (!this._cfgCoalesceText) {
                currToken = n;
                if (!this._cfgReportTextAsChars) {
                    return currToken;
                }
            }
            currToken = 4;
        }
        return currToken;
    }
    
    @Deprecated
    public final Object getFeature(final String s) {
        return null;
    }
    
    public final int getIdAttributeIndex() {
        return -1;
    }
    
    public Location getLastCharLocation() {
        return (Location)this._scanner.getCurrentLocation();
    }
    
    public final String getLocalName() {
        final int currToken = this._currToken;
        if (currToken != 1 && currToken != 2 && currToken != 9) {
            throw new IllegalStateException("Current state not START_ELEMENT, END_ELEMENT or ENTITY_REFERENCE");
        }
        return this._currName.getLocalName();
    }
    
    public final Location getLocation() {
        return (Location)this.getStartLocation();
    }
    
    public final LocationInfo getLocationInfo() {
        return (LocationInfo)this;
    }
    
    public final QName getName() {
        final int currToken = this._currToken;
        if (currToken != 1 && currToken != 2) {
            throw new IllegalStateException(ErrorConsts.ERR_STATE_NOT_ELEM);
        }
        return this._scanner.getQName();
    }
    
    public final NamespaceContext getNamespaceContext() {
        return this._scanner;
    }
    
    public final int getNamespaceCount() {
        final int currToken = this._currToken;
        if (currToken != 1 && currToken != 2) {
            throw new IllegalStateException(ErrorConsts.ERR_STATE_NOT_ELEM);
        }
        return this._scanner.getNsCount();
    }
    
    public final String getNamespacePrefix(final int n) {
        final int currToken = this._currToken;
        if (currToken != 1 && currToken != 2) {
            throw new IllegalStateException(ErrorConsts.ERR_STATE_NOT_ELEM);
        }
        String namespacePrefix;
        if ((namespacePrefix = this._scanner.getNamespacePrefix(n)) == null) {
            namespacePrefix = "";
        }
        return namespacePrefix;
    }
    
    public final String getNamespaceURI() {
        final int currToken = this._currToken;
        if (currToken != 1 && currToken != 2) {
            throw new IllegalStateException(ErrorConsts.ERR_STATE_NOT_ELEM);
        }
        String namespaceURI;
        if ((namespaceURI = this._scanner.getNamespaceURI()) == null) {
            namespaceURI = "";
        }
        return namespaceURI;
    }
    
    public final String getNamespaceURI(final int n) {
        final int currToken = this._currToken;
        if (currToken != 1 && currToken != 2) {
            throw new IllegalStateException(ErrorConsts.ERR_STATE_NOT_ELEM);
        }
        String namespaceURI;
        if ((namespaceURI = this._scanner.getNamespaceURI(n)) == null) {
            namespaceURI = "";
        }
        return namespaceURI;
    }
    
    public final String getNamespaceURI(final String s) {
        final int currToken = this._currToken;
        if (currToken != 1 && currToken != 2) {
            throw new IllegalStateException(ErrorConsts.ERR_STATE_NOT_ELEM);
        }
        return this._scanner.getNamespaceURI(s);
    }
    
    public final NamespaceContext getNonTransientNamespaceContext() {
        return this._scanner.getNonTransientNamespaceContext();
    }
    
    public final int getNotationAttributeIndex() {
        return -1;
    }
    
    public final String getPIData() {
        if (this._currToken == 3) {
            try {
                return this._scanner.getText();
            }
            catch (final XMLStreamException ex) {
                throw UncheckedStreamException.createFrom(ex);
            }
        }
        throw new IllegalStateException(ErrorConsts.ERR_STATE_NOT_PI);
    }
    
    public final String getPITarget() {
        if (this._currToken == 3) {
            return this._currName.getLocalName();
        }
        throw new IllegalStateException(ErrorConsts.ERR_STATE_NOT_PI);
    }
    
    public final String getPrefix() {
        final int currToken = this._currToken;
        if (currToken != 1 && currToken != 2) {
            throw new IllegalStateException(ErrorConsts.ERR_STATE_NOT_ELEM);
        }
        String prefix;
        if ((prefix = this._currName.getPrefix()) == null) {
            prefix = "";
        }
        return prefix;
    }
    
    public final String getPrefixedName() {
        final int currToken = this._currToken;
        if (currToken == 1 || currToken == 2) {
            return this._currName.getPrefixedName();
        }
        if (currToken == 3) {
            return this.getPITarget();
        }
        if (currToken == 9) {
            return this.getLocalName();
        }
        if (currToken == 11) {
            return this.getDTDRootName();
        }
        throw new IllegalStateException("Current state not START_ELEMENT, END_ELEMENT, ENTITY_REFERENCE, PROCESSING_INSTRUCTION or DTD");
    }
    
    public final Object getProcessedDTD() {
        return null;
    }
    
    public final DTDValidationSchema getProcessedDTDSchema() {
        return null;
    }
    
    public Object getProperty(final String s) {
        if (s.equals("javax.xml.stream.entities")) {
            return Collections.EMPTY_LIST;
        }
        if (s.equals("javax.xml.stream.notations")) {
            return Collections.EMPTY_LIST;
        }
        return this._scanner.getConfig().getProperty(s, false);
    }
    
    public XmlScanner getScanner() {
        return this._scanner;
    }
    
    public final XMLStreamLocation2 getStartLocation() {
        return this._scanner.getStartLocation();
    }
    
    public final long getStartingByteOffset() {
        return this._scanner.getStartingByteOffset();
    }
    
    public final long getStartingCharOffset() {
        return this._scanner.getStartingCharOffset();
    }
    
    public final int getText(final Writer writer, final boolean b) {
        final int currToken = this._currToken;
        if ((1 << currToken & 0x1A78) == 0x0) {
            this.throwNotTextual(currToken);
        }
        return this._scanner.getText(writer, b);
    }
    
    public final String getText() {
        final int currToken = this._currToken;
        if ((1 << currToken & 0x1A70) == 0x0) {
            this.throwNotTextual(currToken);
        }
        try {
            return this._scanner.getText();
        }
        catch (final XMLStreamException ex) {
            throw UncheckedStreamException.createFrom(ex);
        }
    }
    
    public final int getTextCharacters(int textCharacters, final char[] array, final int n, final int n2) {
        final int currToken = this._currToken;
        if ((1 << currToken & 0x1070) == 0x0) {
            this.throwNotTextXxx(currToken);
        }
        try {
            textCharacters = this._scanner.getTextCharacters(textCharacters, array, n, n2);
            return textCharacters;
        }
        catch (final XMLStreamException ex) {
            throw UncheckedStreamException.createFrom(ex);
        }
    }
    
    public final char[] getTextCharacters() {
        final int currToken = this._currToken;
        if ((1 << currToken & 0x1070) == 0x0) {
            this.throwNotTextXxx(currToken);
        }
        try {
            return this._scanner.getTextCharacters();
        }
        catch (final XMLStreamException ex) {
            throw UncheckedStreamException.createFrom(ex);
        }
    }
    
    public final int getTextLength() {
        final int currToken = this._currToken;
        if ((1 << currToken & 0x1070) == 0x0) {
            this.throwNotTextXxx(currToken);
        }
        try {
            return this._scanner.getTextLength();
        }
        catch (final XMLStreamException ex) {
            throw UncheckedStreamException.createFrom(ex);
        }
    }
    
    public final int getTextStart() {
        final int currToken = this._currToken;
        if ((1 << currToken & 0x1070) == 0x0) {
            this.throwNotTextXxx(currToken);
        }
        return 0;
    }
    
    public String getVersion() {
        return this._scanner.getConfig().getXmlDeclVersion();
    }
    
    public int handlePrologEoi(final boolean b) {
        this.close();
        if (b) {
            this.throwUnexpectedEOI(ErrorConsts.SUFFIX_IN_PROLOG);
        }
        return 8;
    }
    
    public void handleTreeEoi() {
        this._currToken = 8;
        this.throwUnexpectedEOI(ErrorConsts.SUFFIX_IN_TREE);
    }
    
    public final boolean hasName() {
        final int currToken = this._currToken;
        boolean b = true;
        if (currToken != 1) {
            b = (currToken == 2 && b);
        }
        return b;
    }
    
    public final boolean hasNext() {
        return this._currToken != 8;
    }
    
    public final boolean hasText() {
        final int currToken = this._currToken;
        boolean b = true;
        if ((1 << currToken & 0x1A70) == 0x0) {
            b = false;
        }
        return b;
    }
    
    public final boolean isAttributeSpecified(final int n) {
        if (this._currToken == 1) {
            return this._scanner.isAttrSpecified(n);
        }
        throw new IllegalStateException(ErrorConsts.ERR_STATE_NOT_STELEM);
    }
    
    public final boolean isCharacters() {
        return this.getEventType() == 4;
    }
    
    public final boolean isEmptyElement() {
        return this._currToken == 1 && this._scanner.isEmptyTag();
    }
    
    public final boolean isEndElement() {
        return this._currToken == 2;
    }
    
    public final boolean isPropertySupported(final String s) {
        return this._scanner.getConfig().isPropertySupported(s);
    }
    
    public final boolean isStandalone() {
        final int xmlDeclStandalone = this._scanner.getConfig().getXmlDeclStandalone();
        boolean b = true;
        if (xmlDeclStandalone != 1) {
            b = false;
        }
        return b;
    }
    
    public final boolean isStartElement() {
        final int currToken = this._currToken;
        boolean b = true;
        if (currToken != 1) {
            b = false;
        }
        return b;
    }
    
    public final boolean isWhiteSpace() {
        final int currToken = this._currToken;
        if (currToken != 4) {
            if (currToken != 12) {
                return currToken == 6;
            }
        }
        try {
            return this._scanner.isTextWhitespace();
        }
        catch (final XMLStreamException ex) {
            throw UncheckedStreamException.createFrom(ex);
        }
    }
    
    public final int next() {
        final int parseState = this._parseState;
        boolean b = true;
        if (parseState == 1) {
            final int nextFromTree = this._scanner.nextFromTree();
            if (nextFromTree == -1) {
                this.handleTreeEoi();
            }
            if ((this._currToken = nextFromTree) == 12) {
                if (this._cfgCoalesceText || this._cfgReportTextAsChars) {
                    return 4;
                }
            }
            else {
                this._currName = this._scanner.getName();
                if (nextFromTree == 2) {
                    if (this._scanner.hasEmptyStack()) {
                        this._parseState = 2;
                    }
                }
                else if (nextFromTree == 1) {
                    this._attrCount = this._scanner.getAttrCount();
                }
            }
            return nextFromTree;
        }
        int nextFromProlog2;
        if (parseState == 0) {
            final int nextFromProlog = this._scanner.nextFromProlog(true);
            if (nextFromProlog == 1) {
                this._parseState = 1;
                this._attrCount = this._scanner.getAttrCount();
                nextFromProlog2 = nextFromProlog;
            }
            else if ((nextFromProlog2 = nextFromProlog) == 11) {
                if (this._dtdRootName != null) {
                    this.throwWfe("Duplicate DOCTYPE declaration");
                }
                this._dtdRootName = this._scanner.getName();
                nextFromProlog2 = nextFromProlog;
            }
        }
        else {
            if (parseState != 2) {
                throw new NoSuchElementException();
            }
            nextFromProlog2 = this._scanner.nextFromProlog(false);
        }
        if (nextFromProlog2 < 0) {
            if (this._parseState != 0) {
                b = false;
            }
            return this.handlePrologEoi(b);
        }
        this._currName = this._scanner.getName();
        return this._currToken = nextFromProlog2;
    }
    
    public final int nextTag() {
        int next = 0;
    Label_0055:
        while (true) {
            next = this.next();
            Label_0074: {
                if (next != 12) {
                    switch (next) {
                        case 3:
                        case 5:
                        case 6: {
                            continue;
                        }
                        default: {
                            break Label_0074;
                        }
                        case 1:
                        case 2: {
                            break Label_0055;
                        }
                        case 4: {
                            break;
                        }
                    }
                }
                if (this.isWhiteSpace()) {
                    continue;
                }
                this.throwWfe("Received non-all-whitespace CHARACTERS or CDATA event in nextTag().");
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Received event ");
            sb.append(ErrorConsts.tokenTypeDesc(next));
            sb.append(", instead of START_ELEMENT or END_ELEMENT.");
            this.throwWfe(sb.toString());
        }
        return next;
    }
    
    public final int readElementAsArray(final TypedArrayDecoder typedArrayDecoder) {
        final int currToken = this._currToken;
        if ((1 << currToken & 0x1056) != 0x0) {
            final int n = 0;
            int n2 = 0;
            final int n3 = -1;
            while (true) {
                int n4 = 0;
                Label_0210: {
                    boolean b;
                    int n5;
                    if (currToken == 1) {
                        if (this._scanner.isEmptyTag()) {
                            this.next();
                            return -1;
                        }
                        while (true) {
                            final int next = this.next();
                            if (next == 2) {
                                return -1;
                            }
                            if (next == 5) {
                                continue;
                            }
                            if (next == 3) {
                                continue;
                            }
                            n4 = next;
                            n2 = n;
                            if (next == 4) {
                                break Label_0210;
                            }
                            if (next == 12) {
                                n4 = next;
                                n2 = n;
                                break Label_0210;
                            }
                            throw this._constructUnexpectedInTyped(next);
                        }
                    }
                    else {
                        b = false;
                        n5 = currToken;
                    }
                    int n6 = n2;
                    Label_0219: {
                        if (n5 != 2) {
                            int n7;
                            if (n5 != 4 && n5 != 12 && n5 != 6) {
                                n7 = n2;
                                if (n5 != 5) {
                                    if (n5 != 3) {
                                        throw this._constructUnexpectedInTyped(n5);
                                    }
                                    n7 = n2;
                                }
                            }
                            else {
                                n7 = n2 + this._scanner.decodeElements(typedArrayDecoder, b);
                                if (!typedArrayDecoder.hasRoom()) {
                                    n6 = n7;
                                    break Label_0219;
                                }
                            }
                            final int next2 = this.next();
                            n2 = n7;
                            n4 = next2;
                            break Label_0210;
                        }
                    }
                    int n8 = n3;
                    if (n6 > 0) {
                        n8 = n6;
                    }
                    return n8;
                }
                boolean b = true;
                int n5 = n4;
                continue;
            }
        }
        throw new IllegalStateException(ErrorConsts.ERR_STATE_NOT_STELEM_OR_TEXT);
    }
    
    public final int readElementAsBinary(final byte[] array, final int n, final int n2) {
        return this.readElementAsBinary(array, n, n2, Base64Variants.getDefaultVariant());
    }
    
    public final int readElementAsBinary(final byte[] array, int i, int j, final Base64Variant base64Variant) {
        if (array == null) {
            throw new IllegalArgumentException("resultBuffer is null");
        }
        if (i < 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Illegal offset (");
            sb.append(i);
            sb.append("), must be [0, ");
            sb.append(array.length);
            sb.append("[");
            throw new IllegalArgumentException(sb.toString());
        }
        if (j >= 1) {
            if (i + j <= array.length) {
                final CharArrayBase64Decoder base64Decoder = this._base64Decoder();
                final int currToken = this._currToken;
                final int n = -1;
                if ((1 << currToken & 0x1052) == 0x0) {
                    if (currToken != 2) {
                        throw new IllegalStateException(ErrorConsts.ERR_STATE_NOT_STELEM_OR_TEXT);
                    }
                    if (!base64Decoder.hasData()) {
                        return -1;
                    }
                }
                else if (currToken == 1) {
                    if (this._scanner.isEmptyTag()) {
                        this.next();
                        return -1;
                    }
                    while (true) {
                        final int next = this.next();
                        if (next == 2) {
                            return -1;
                        }
                        if (next == 5) {
                            continue;
                        }
                        if (next == 3) {
                            continue;
                        }
                        if (next != 4 && next != 12) {
                            throw this._constructUnexpectedInTyped(next);
                        }
                        this._scanner.resetForDecoding(base64Variant, base64Decoder, true);
                        break;
                    }
                }
                final int n2 = 0;
                int n3 = i;
                i = n2;
                try {
                    while (true) {
                        final int decode = base64Decoder.decode(array, n3, j);
                        n3 += decode;
                        i += decode;
                        j -= decode;
                        if (j < 1) {
                            break;
                        }
                        if (this._currToken == 2) {
                            break;
                        }
                        int next2;
                        while (true) {
                            next2 = this.next();
                            if (next2 != 5 && next2 != 3) {
                                if (next2 == 6) {
                                    continue;
                                }
                                break;
                            }
                        }
                        if (next2 == 2) {
                            final int endOfContent = base64Decoder.endOfContent();
                            if (endOfContent < 0) {
                                throw this._constructTypeException("Incomplete base64 triplet at the end of decoded content", "");
                            }
                            if (endOfContent > 0) {
                                continue;
                            }
                            break;
                        }
                        else {
                            this._scanner.resetForDecoding(base64Variant, base64Decoder, false);
                        }
                    }
                    j = n;
                    if (i > 0) {
                        j = i;
                    }
                    return j;
                }
                catch (final IllegalArgumentException ex) {
                    throw this._constructTypeException(ex.getMessage(), "");
                }
            }
        }
        if (j == 0) {
            return 0;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Illegal maxLength (");
        sb2.append(j);
        sb2.append("), has to be positive number, and offset+maxLength can not exceed");
        sb2.append(array.length);
        throw new IllegalArgumentException(sb2.toString());
    }
    
    public final int readElementAsDoubleArray(final double[] array, final int n, final int n2) {
        return this.readElementAsArray((TypedArrayDecoder)this._decoderFactory().getDoubleArrayDecoder(array, n, n2));
    }
    
    public final int readElementAsFloatArray(final float[] array, final int n, final int n2) {
        return this.readElementAsArray((TypedArrayDecoder)this._decoderFactory().getFloatArrayDecoder(array, n, n2));
    }
    
    public final int readElementAsIntArray(final int[] array, final int n, final int n2) {
        return this.readElementAsArray((TypedArrayDecoder)this._decoderFactory().getIntArrayDecoder(array, n, n2));
    }
    
    public final int readElementAsLongArray(final long[] array, final int n, final int n2) {
        return this.readElementAsArray((TypedArrayDecoder)this._decoderFactory().getLongArrayDecoder(array, n, n2));
    }
    
    public void reportInvalidAttrIndex(final int i) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Illegal attribute index, ");
        sb.append(i);
        sb.append(", current START_ELEMENT has ");
        sb.append(this._attrCount);
        sb.append(" attributes");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public final void require(final int n, String str, String namespaceURI) {
        int currToken;
        final int n2 = currToken = this._currToken;
        Label_0048: {
            if (n2 != n && (currToken = n2) == 12) {
                if (!this._cfgCoalesceText) {
                    currToken = n2;
                    if (!this._cfgReportTextAsChars) {
                        break Label_0048;
                    }
                }
                currToken = 4;
            }
        }
        if (n != currToken) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Expected type ");
            sb.append(ErrorConsts.tokenTypeDesc(n));
            sb.append(", current type ");
            sb.append(ErrorConsts.tokenTypeDesc(currToken));
            this.throwWfe(sb.toString());
        }
        if (namespaceURI != null) {
            if (currToken != 1 && currToken != 2 && currToken != 9) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Expected non-null local name, but current token not a START_ELEMENT, END_ELEMENT or ENTITY_REFERENCE (was ");
                sb2.append(ErrorConsts.tokenTypeDesc(this._currToken));
                sb2.append(")");
                this.throwWfe(sb2.toString());
            }
            final String localName = this.getLocalName();
            if (localName != namespaceURI && !localName.equals(namespaceURI)) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Expected local name '");
                sb3.append(namespaceURI);
                sb3.append("'; current local name '");
                sb3.append(localName);
                sb3.append("'.");
                this.throwWfe(sb3.toString());
            }
        }
        if (str != null) {
            if (currToken != 1 && currToken != 2) {
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("Expected non-null NS URI, but current token not a START_ELEMENT or END_ELEMENT (was ");
                sb4.append(ErrorConsts.tokenTypeDesc(currToken));
                sb4.append(")");
                this.throwWfe(sb4.toString());
            }
            namespaceURI = this.getNamespaceURI();
            if (str.length() == 0) {
                if (namespaceURI == null || namespaceURI.length() <= 0) {
                    return;
                }
                final StringBuilder sb5 = new StringBuilder();
                sb5.append("Expected empty namespace, instead have '");
                sb5.append(namespaceURI);
                sb5.append("'.");
                str = sb5.toString();
            }
            else {
                if (str == namespaceURI || str.equals(namespaceURI)) {
                    return;
                }
                final StringBuilder sb6 = new StringBuilder();
                sb6.append("Expected namespace '");
                sb6.append(str);
                sb6.append("'; have '");
                sb6.append(namespaceURI);
                sb6.append("'.");
                str = sb6.toString();
            }
            this.throwWfe(str);
        }
    }
    
    @Deprecated
    public final void setFeature(final String s, final Object o) {
    }
    
    public final boolean setProperty(final String s, final Object o) {
        return this._scanner.getConfig().setProperty(s, o);
    }
    
    public final ValidationProblemHandler setValidationProblemHandler(final ValidationProblemHandler validationProblemHandler) {
        return null;
    }
    
    public final void skipElement() {
        if (this._currToken == 1) {
            int n = 1;
            while (true) {
                final int next = this.next();
                if (next == 1) {
                    ++n;
                }
                else {
                    if (next == 2 && --n == 0) {
                        break;
                    }
                    continue;
                }
            }
            return;
        }
        throw new IllegalStateException(ErrorConsts.ERR_STATE_NOT_STELEM);
    }
    
    public final boolean standaloneSet() {
        return this._scanner.getConfig().getXmlDeclStandalone() != 0;
    }
    
    public final XMLValidator stopValidatingAgainst(final XMLValidationSchema xmlValidationSchema) {
        return null;
    }
    
    public final XMLValidator stopValidatingAgainst(final XMLValidator xmlValidator) {
        return null;
    }
    
    public void throwFromIOE(final IOException ex) {
        throw new IoStreamException(ex);
    }
    
    public void throwUnexpectedEOI(final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Unexpected End-of-input");
        sb.append(str);
        this.throwWfe(sb.toString());
    }
    
    public void throwWfe(final String s) {
        throw new WFCException(s, this.getLastCharLocation());
    }
    
    @Override
    public final String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("[Aalto stream reader, scanner: ");
        sb.append(this._scanner);
        sb.append("]");
        return sb.toString();
    }
    
    public final XMLValidator validateAgainst(final XMLValidationSchema xmlValidationSchema) {
        return null;
    }
    
    public QName verifyQName(final QName qName) {
        final String localPart = qName.getLocalPart();
        final int illegalNameChar = XmlNames.findIllegalNameChar(localPart, false);
        if (illegalNameChar >= 0) {
            final String prefix = qName.getPrefix();
            String string;
            if (prefix != null && prefix.length() > 0) {
                final StringBuilder sb = new StringBuilder();
                sb.append(prefix);
                sb.append(":");
                sb.append(localPart);
                string = sb.toString();
            }
            else {
                string = localPart;
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Invalid local name \"");
            sb2.append(localPart);
            sb2.append("\" (character at #");
            sb2.append(illegalNameChar);
            sb2.append(" is invalid)");
            throw this._constructTypeException(sb2.toString(), string);
        }
        return qName;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto;

import java.nio.ByteBuffer;
import org.codehaus.stax2.XMLInputFactory2;

public abstract class AsyncXMLInputFactory extends XMLInputFactory2
{
    public abstract AsyncXMLStreamReader<AsyncByteBufferFeeder> createAsyncFor(final ByteBuffer p0);
    
    public abstract AsyncXMLStreamReader<AsyncByteArrayFeeder> createAsyncFor(final byte[] p0);
    
    public abstract AsyncXMLStreamReader<AsyncByteArrayFeeder> createAsyncFor(final byte[] p0, final int p1, final int p2);
    
    public abstract AsyncXMLStreamReader<AsyncByteArrayFeeder> createAsyncForByteArray();
    
    public abstract AsyncXMLStreamReader<AsyncByteBufferFeeder> createAsyncForByteBuffer();
}

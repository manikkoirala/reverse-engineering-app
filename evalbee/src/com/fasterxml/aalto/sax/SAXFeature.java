// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.sax;

public enum SAXFeature
{
    private static final SAXFeature[] $VALUES;
    
    EXTERNAL_GENERAL_ENTITIES("external-general-entities"), 
    EXTERNAL_PARAMETER_ENTITIES("external-parameter-entities"), 
    IS_STANDALONE("is-standalone"), 
    LEXICAL_HANDLER_PARAMETER_ENTITIES("lexical-handler/parameter-entities"), 
    NAMESPACES("namespaces"), 
    NAMESPACE_PREFIXES("namespace-prefixes"), 
    RESOLVE_DTD_URIS("resolve-dtd-uris");
    
    public static final String STD_FEATURE_PREFIX = "http://xml.org/sax/features/";
    
    STRING_INTERNING("string-interning"), 
    UNICODE_NORMALIZATION_CHECKING("unicode-normalization-checking"), 
    USE_ATTRIBUTES2("use-attributes2"), 
    USE_ENTITY_RESOLVER2("use-entity-resolver2"), 
    USE_LOCATOR2("use-locator2"), 
    VALIDATION("validation"), 
    XMLNS_URIS("xmlns-uris"), 
    XML_1_1("xml-1.1");
    
    private final String mSuffix;
    
    private SAXFeature(final String mSuffix) {
        this.mSuffix = mSuffix;
    }
    
    public String getSuffix() {
        return this.mSuffix;
    }
    
    public String toExternal() {
        final StringBuilder sb = new StringBuilder();
        sb.append("http://xml.org/sax/features/");
        sb.append(this.mSuffix);
        return sb.toString();
    }
}

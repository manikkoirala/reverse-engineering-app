// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.sax;

import org.xml.sax.SAXNotSupportedException;
import javax.xml.parsers.SAXParser;
import com.fasterxml.aalto.stax.InputFactoryImpl;
import javax.xml.parsers.SAXParserFactory;

public class SAXParserFactoryImpl extends SAXParserFactory
{
    final InputFactoryImpl mStaxFactory;
    
    public SAXParserFactoryImpl() {
        this.mStaxFactory = new InputFactoryImpl();
    }
    
    public static SAXParserFactory newInstance() {
        return new SAXParserFactoryImpl();
    }
    
    @Override
    public boolean getFeature(final String s) {
        final SAXFeature stdFeature = SAXUtil.findStdFeature(s);
        if (stdFeature != null) {
            final Boolean fixedStdFeatureValue = SAXUtil.getFixedStdFeatureValue(stdFeature);
            if (fixedStdFeatureValue != null) {
                return fixedStdFeatureValue;
            }
            if (SAXParserFactoryImpl$1.$SwitchMap$com$fasterxml$aalto$sax$SAXFeature[stdFeature.ordinal()] == 1) {
                return true;
            }
        }
        SAXUtil.reportUnknownFeature(s);
        return false;
    }
    
    @Override
    public SAXParser newSAXParser() {
        return new SAXParserImpl(this.mStaxFactory);
    }
    
    @Override
    public void setFeature(final String s, final boolean b) {
        final SAXFeature stdFeature = SAXUtil.findStdFeature(s);
        if (stdFeature == null) {
            SAXUtil.reportUnknownFeature(s);
            return;
        }
        final int n = SAXParserFactoryImpl$1.$SwitchMap$com$fasterxml$aalto$sax$SAXFeature[stdFeature.ordinal()];
        int n2 = 0;
        while (true) {
            switch (n) {
                default: {
                    n2 = n2;
                }
                case 9: {
                    if (n2 != 0) {
                        return;
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Setting std feature ");
                    sb.append(stdFeature);
                    sb.append(" to ");
                    sb.append(b);
                    sb.append(" not supported");
                    throw new SAXNotSupportedException(sb.toString());
                }
                case 5: {
                    n2 = (b ? 1 : 0);
                    continue;
                }
                case 2:
                case 3:
                case 6:
                case 13:
                case 15: {
                    n2 = ((b ^ true) ? 1 : 0);
                    continue;
                }
                case 1:
                case 4:
                case 7:
                case 8:
                case 10:
                case 11:
                case 12:
                case 14: {
                    n2 = 1;
                    continue;
                }
            }
            break;
        }
    }
    
    @Override
    public void setNamespaceAware(final boolean namespaceAware) {
        if (namespaceAware) {
            super.setNamespaceAware(namespaceAware);
            return;
        }
        throw new IllegalArgumentException("Non-namespace-aware mode not implemented");
    }
    
    @Override
    public void setValidating(final boolean validating) {
        if (!validating) {
            super.setValidating(validating);
            return;
        }
        throw new IllegalArgumentException("Validating mode not implemented");
    }
}

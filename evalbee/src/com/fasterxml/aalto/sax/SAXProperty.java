// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.sax;

public enum SAXProperty
{
    private static final SAXProperty[] $VALUES;
    
    DECLARATION_HANDLER("declaration-handler"), 
    DOCUMENT_XML_VERSION("document-xml-version"), 
    DOM_NODE("dom-node"), 
    LEXICAL_HANDLER("lexical-handler");
    
    public static final String STD_PROPERTY_PREFIX = "http://xml.org/sax/properties/";
    
    XML_STRING("xml-string");
    
    private final String mSuffix;
    
    private SAXProperty(final String mSuffix) {
        this.mSuffix = mSuffix;
    }
    
    public String getSuffix() {
        return this.mSuffix;
    }
    
    public String toExternal() {
        final StringBuilder sb = new StringBuilder();
        sb.append("http://xml.org/sax/properties/");
        sb.append(this.mSuffix);
        return sb.toString();
    }
}

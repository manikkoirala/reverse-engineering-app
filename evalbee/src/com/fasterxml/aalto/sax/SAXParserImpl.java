// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.sax;

import org.xml.sax.AttributeList;
import java.util.Locale;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.DocumentHandler;
import org.xml.sax.HandlerBase;
import org.xml.sax.InputSource;
import com.fasterxml.aalto.in.ReaderConfig;
import org.xml.sax.Locator;
import org.xml.sax.SAXParseException;
import org.xml.sax.Attributes;
import com.fasterxml.aalto.stax.InputFactoryImpl;
import com.fasterxml.aalto.in.XmlScanner;
import org.xml.sax.ext.LexicalHandler;
import org.xml.sax.ErrorHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.DTDHandler;
import org.xml.sax.ext.DeclHandler;
import org.xml.sax.ContentHandler;
import com.fasterxml.aalto.in.AttributeCollector;
import org.xml.sax.ext.Locator2;
import org.xml.sax.ext.Attributes2;
import org.xml.sax.XMLReader;
import org.xml.sax.Parser;
import javax.xml.parsers.SAXParser;

class SAXParserImpl extends SAXParser implements Parser, XMLReader, Attributes2, Locator2
{
    protected AttributeCollector _attrCollector;
    private int _attrCount;
    protected ContentHandler _contentHandler;
    private DeclHandler _declHandler;
    protected DTDHandler _dtdHandler;
    private EntityResolver _entityResolver;
    private ErrorHandler _errorHandler;
    private LexicalHandler _lexicalHandler;
    protected XmlScanner _scanner;
    final InputFactoryImpl _staxFactory;
    
    public SAXParserImpl(final InputFactoryImpl staxFactory) {
        this._staxFactory = staxFactory;
    }
    
    private final void fireAuxEvent(final int i, final boolean b) {
        if (i != 3) {
            if (i != 5) {
                if (i != 6) {
                    if (i != 11) {
                        if (i != 12) {
                            if (i == -1) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("Unexpected end-of-input in ");
                                String str;
                                if (b) {
                                    str = "tree";
                                }
                                else {
                                    str = "prolog";
                                }
                                sb.append(str);
                                this.throwSaxException(sb.toString());
                            }
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Internal error: unexpected type, ");
                            sb2.append(i);
                            throw new RuntimeException(sb2.toString());
                        }
                        final LexicalHandler lexicalHandler = this._lexicalHandler;
                        if (lexicalHandler != null) {
                            lexicalHandler.startCDATA();
                            this._scanner.fireSaxCharacterEvents(this._contentHandler);
                            this._lexicalHandler.endCDATA();
                        }
                        else {
                            this._scanner.fireSaxCharacterEvents(this._contentHandler);
                        }
                    }
                    else if (this._lexicalHandler != null) {
                        this._lexicalHandler.startDTD(this._scanner.getName().getPrefixedName(), this._scanner.getDTDPublicId(), this._scanner.getDTDSystemId());
                        this._lexicalHandler.endDTD();
                    }
                }
                else if (b) {
                    this._scanner.fireSaxSpaceEvents(this._contentHandler);
                }
            }
            else {
                this._scanner.fireSaxCommentEvent(this._lexicalHandler);
            }
        }
        else {
            this._scanner.fireSaxPIEvent(this._contentHandler);
        }
    }
    
    private final void fireEndTag() {
        this._scanner.fireSaxEndElement(this._contentHandler);
    }
    
    private final void fireEvents() {
        while (true) {
            final int nextFromProlog = this._scanner.nextFromProlog(true);
            if (nextFromProlog == 1) {
                break;
            }
            this.fireAuxEvent(nextFromProlog, false);
        }
        this.fireStartTag();
        int n = 1;
        while (true) {
            final int nextFromTree = this._scanner.nextFromTree();
            if (nextFromTree == 1) {
                this.fireStartTag();
                ++n;
            }
            else if (nextFromTree == 2) {
                this.fireEndTag();
                if (--n < 1) {
                    break;
                }
                continue;
            }
            else if (nextFromTree == 4) {
                this._scanner.fireSaxCharacterEvents(this._contentHandler);
            }
            else {
                this.fireAuxEvent(nextFromTree, true);
            }
        }
        while (true) {
            final int nextFromProlog2 = this._scanner.nextFromProlog(false);
            if (nextFromProlog2 == -1) {
                break;
            }
            if (nextFromProlog2 == 6) {
                continue;
            }
            this.fireAuxEvent(nextFromProlog2, false);
        }
    }
    
    private final void fireStartTag() {
        this._attrCount = this._scanner.getAttrCount();
        this._scanner.fireSaxStartElement(this._contentHandler, this);
    }
    
    private void throwSaxException(final Exception ex) {
        final SAXParseException ex2 = new SAXParseException(ex.getMessage(), this, ex);
        if (ex2.getCause() == null) {
            ex2.initCause(ex);
        }
        final ErrorHandler errorHandler = this._errorHandler;
        if (errorHandler != null) {
            errorHandler.fatalError(ex2);
        }
        throw ex2;
    }
    
    private void throwSaxException(final String message) {
        final SAXParseException ex = new SAXParseException(message, this);
        final ErrorHandler errorHandler = this._errorHandler;
        if (errorHandler != null) {
            errorHandler.fatalError(ex);
        }
        throw ex;
    }
    
    @Override
    public int getColumnNumber() {
        final XmlScanner scanner = this._scanner;
        int currentColumnNr;
        if (scanner != null) {
            currentColumnNr = scanner.getCurrentColumnNr();
        }
        else {
            currentColumnNr = -1;
        }
        return currentColumnNr;
    }
    
    @Override
    public ContentHandler getContentHandler() {
        return this._contentHandler;
    }
    
    @Override
    public DTDHandler getDTDHandler() {
        return this._dtdHandler;
    }
    
    @Override
    public String getEncoding() {
        final ReaderConfig config = this._scanner.getConfig();
        String s;
        if ((s = config.getActualEncoding()) == null && (s = config.getXmlDeclEncoding()) == null) {
            s = config.getExternalEncoding();
        }
        return s;
    }
    
    @Override
    public EntityResolver getEntityResolver() {
        return this._entityResolver;
    }
    
    @Override
    public ErrorHandler getErrorHandler() {
        return this._errorHandler;
    }
    
    @Override
    public boolean getFeature(final String s) {
        final SAXFeature stdFeature = SAXUtil.findStdFeature(s);
        if (stdFeature != null) {
            final Boolean fixedStdFeatureValue = SAXUtil.getFixedStdFeatureValue(stdFeature);
            if (fixedStdFeatureValue != null) {
                return fixedStdFeatureValue;
            }
            if (SAXParserImpl$1.$SwitchMap$com$fasterxml$aalto$sax$SAXFeature[stdFeature.ordinal()] == 1) {
                return true;
            }
        }
        SAXUtil.reportUnknownFeature(s);
        return false;
    }
    
    @Override
    public int getIndex(final String s) {
        final AttributeCollector attrCollector = this._attrCollector;
        int index;
        if (attrCollector == null) {
            index = -1;
        }
        else {
            index = attrCollector.findIndex(null, s);
        }
        return index;
    }
    
    @Override
    public int getIndex(final String s, final String s2) {
        final AttributeCollector attrCollector = this._attrCollector;
        int index;
        if (attrCollector == null) {
            index = -1;
        }
        else {
            index = attrCollector.findIndex(s, s2);
        }
        return index;
    }
    
    @Override
    public int getLength() {
        return this._attrCount;
    }
    
    @Override
    public int getLineNumber() {
        final XmlScanner scanner = this._scanner;
        int currentLineNr;
        if (scanner != null) {
            currentLineNr = scanner.getCurrentLineNr();
        }
        else {
            currentLineNr = -1;
        }
        return currentLineNr;
    }
    
    @Override
    public String getLocalName(final int n) {
        String localName;
        if (n >= 0 && n < this._attrCount) {
            localName = this._attrCollector.getName(n).getLocalName();
        }
        else {
            localName = null;
        }
        return localName;
    }
    
    @Override
    public final Parser getParser() {
        return this;
    }
    
    @Override
    public Object getProperty(final String s) {
        final SAXProperty stdProperty = SAXUtil.findStdProperty(s);
        if (stdProperty != null) {
            final int n = SAXParserImpl$1.$SwitchMap$com$fasterxml$aalto$sax$SAXProperty[stdProperty.ordinal()];
            if (n == 1) {
                return this._declHandler;
            }
            if (n == 2) {
                return this._scanner.getConfig().getXmlDeclVersion();
            }
            if (n == 3) {
                return null;
            }
            if (n == 4) {
                return this._lexicalHandler;
            }
            if (n == 5) {
                return null;
            }
        }
        SAXUtil.reportUnknownProperty(s);
        return null;
    }
    
    @Override
    public String getPublicId() {
        final XmlScanner scanner = this._scanner;
        String inputPublicId;
        if (scanner != null) {
            inputPublicId = scanner.getInputPublicId();
        }
        else {
            inputPublicId = null;
        }
        return inputPublicId;
    }
    
    @Override
    public String getQName(final int n) {
        String prefixedName;
        if (n >= 0 && n < this._attrCount) {
            prefixedName = this._attrCollector.getName(n).getPrefixedName();
        }
        else {
            prefixedName = null;
        }
        return prefixedName;
    }
    
    @Override
    public String getSystemId() {
        final XmlScanner scanner = this._scanner;
        String inputSystemId;
        if (scanner != null) {
            inputSystemId = scanner.getInputSystemId();
        }
        else {
            inputSystemId = null;
        }
        return inputSystemId;
    }
    
    @Override
    public String getType(final int n) {
        String attrType;
        if (n >= 0 && n < this._attrCount) {
            attrType = this._scanner.getAttrType(n);
        }
        else {
            attrType = null;
        }
        return attrType;
    }
    
    @Override
    public String getType(String attrType) {
        final int index = this.getIndex(attrType);
        if (index < 0) {
            attrType = null;
        }
        else {
            attrType = this._scanner.getAttrType(index);
        }
        return attrType;
    }
    
    @Override
    public String getType(String attrType, final String s) {
        final int index = this.getIndex(attrType, s);
        if (index < 0) {
            attrType = null;
        }
        else {
            attrType = this._scanner.getAttrType(index);
        }
        return attrType;
    }
    
    @Override
    public String getURI(final int n) {
        if (n >= 0 && n < this._attrCount) {
            String nsUri;
            if ((nsUri = this._attrCollector.getName(n).getNsUri()) == null) {
                nsUri = "";
            }
            return nsUri;
        }
        return null;
    }
    
    @Override
    public String getValue(final int n) {
        String value;
        if (n >= 0 && n < this._attrCount) {
            value = this._attrCollector.getValue(n);
        }
        else {
            value = null;
        }
        return value;
    }
    
    @Override
    public String getValue(String value) {
        final int index = this.getIndex(value);
        if (index < 0) {
            value = null;
        }
        else {
            value = this._attrCollector.getValue(index);
        }
        return value;
    }
    
    @Override
    public String getValue(String value, final String s) {
        final int index = this.getIndex(value, s);
        if (index < 0) {
            value = null;
        }
        else {
            value = this._attrCollector.getValue(index);
        }
        return value;
    }
    
    @Override
    public final XMLReader getXMLReader() {
        return this;
    }
    
    @Override
    public String getXMLVersion() {
        return this._scanner.getConfig().getXmlDeclVersion();
    }
    
    @Override
    public boolean isDeclared(final int n) {
        return false;
    }
    
    @Override
    public boolean isDeclared(final String s) {
        return false;
    }
    
    @Override
    public boolean isDeclared(final String s, final String s2) {
        return false;
    }
    
    @Override
    public boolean isNamespaceAware() {
        return true;
    }
    
    @Override
    public boolean isSpecified(final int n) {
        return true;
    }
    
    @Override
    public boolean isSpecified(final String s) {
        return true;
    }
    
    @Override
    public boolean isSpecified(final String s, final String s2) {
        return true;
    }
    
    @Override
    public boolean isValidating() {
        return false;
    }
    
    @Override
    public void parse(final String systemId) {
        this.parse(new InputSource(systemId));
    }
    
    @Override
    public void parse(final InputSource p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokevirtual   org/xml/sax/InputSource.getEncoding:()Ljava/lang/String;
        //     4: astore_2       
        //     5: aload_1        
        //     6: invokevirtual   org/xml/sax/InputSource.getSystemId:()Ljava/lang/String;
        //     9: astore          5
        //    11: aload_0        
        //    12: getfield        com/fasterxml/aalto/sax/SAXParserImpl._staxFactory:Lcom/fasterxml/aalto/stax/InputFactoryImpl;
        //    15: aload           5
        //    17: aload_1        
        //    18: invokevirtual   org/xml/sax/InputSource.getPublicId:()Ljava/lang/String;
        //    21: aload_2        
        //    22: iconst_0       
        //    23: iconst_0       
        //    24: invokevirtual   com/fasterxml/aalto/stax/InputFactoryImpl.getNonSharedConfig:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/fasterxml/aalto/in/ReaderConfig;
        //    27: astore          4
        //    29: aload           4
        //    31: iconst_0       
        //    32: invokevirtual   com/fasterxml/aalto/in/ReaderConfig.doParseLazily:(Z)V
        //    35: aload_1        
        //    36: invokevirtual   org/xml/sax/InputSource.getCharacterStream:()Ljava/io/Reader;
        //    39: astore_3       
        //    40: aload_3        
        //    41: ifnonnull       108
        //    44: aload_1        
        //    45: invokevirtual   org/xml/sax/InputSource.getByteStream:()Ljava/io/InputStream;
        //    48: astore_2       
        //    49: aload_2        
        //    50: astore_1       
        //    51: aload_2        
        //    52: ifnonnull       110
        //    55: aload           5
        //    57: ifnull          97
        //    60: aload           5
        //    62: invokestatic    com/fasterxml/aalto/util/URLUtil.urlFromSystemId:(Ljava/lang/String;)Ljava/net/URL;
        //    65: invokestatic    com/fasterxml/aalto/util/URLUtil.inputStreamFromURL:(Ljava/net/URL;)Ljava/io/InputStream;
        //    68: astore_1       
        //    69: goto            110
        //    72: astore_2       
        //    73: new             Lorg/xml/sax/SAXException;
        //    76: dup            
        //    77: aload_2        
        //    78: invokespecial   org/xml/sax/SAXException.<init>:(Ljava/lang/Exception;)V
        //    81: astore_1       
        //    82: aload_1        
        //    83: invokevirtual   java/lang/Throwable.getCause:()Ljava/lang/Throwable;
        //    86: ifnonnull       95
        //    89: aload_1        
        //    90: aload_2        
        //    91: invokevirtual   java/lang/Throwable.initCause:(Ljava/lang/Throwable;)Ljava/lang/Throwable;
        //    94: pop            
        //    95: aload_1        
        //    96: athrow         
        //    97: new             Lorg/xml/sax/SAXException;
        //   100: dup            
        //   101: ldc_w           "Invalid InputSource passed: neither character or byte stream passed, nor system id specified"
        //   104: invokespecial   org/xml/sax/SAXException.<init>:(Ljava/lang/String;)V
        //   107: athrow         
        //   108: aconst_null    
        //   109: astore_1       
        //   110: aload_0        
        //   111: getfield        com/fasterxml/aalto/sax/SAXParserImpl._contentHandler:Lorg/xml/sax/ContentHandler;
        //   114: astore_2       
        //   115: aload_2        
        //   116: ifnull          135
        //   119: aload_2        
        //   120: aload_0        
        //   121: invokeinterface org/xml/sax/ContentHandler.setDocumentLocator:(Lorg/xml/sax/Locator;)V
        //   126: aload_0        
        //   127: getfield        com/fasterxml/aalto/sax/SAXParserImpl._contentHandler:Lorg/xml/sax/ContentHandler;
        //   130: invokeinterface org/xml/sax/ContentHandler.startDocument:()V
        //   135: aload_3        
        //   136: ifnull          157
        //   139: aload           4
        //   141: aload_3        
        //   142: invokestatic    com/fasterxml/aalto/in/CharSourceBootstrapper.construct:(Lcom/fasterxml/aalto/in/ReaderConfig;Ljava/io/Reader;)Lcom/fasterxml/aalto/in/CharSourceBootstrapper;
        //   145: invokevirtual   com/fasterxml/aalto/in/CharSourceBootstrapper.bootstrap:()Lcom/fasterxml/aalto/in/XmlScanner;
        //   148: astore_2       
        //   149: aload_0        
        //   150: aload_2        
        //   151: putfield        com/fasterxml/aalto/sax/SAXParserImpl._scanner:Lcom/fasterxml/aalto/in/XmlScanner;
        //   154: goto            170
        //   157: aload           4
        //   159: aload_1        
        //   160: invokestatic    com/fasterxml/aalto/in/ByteSourceBootstrapper.construct:(Lcom/fasterxml/aalto/in/ReaderConfig;Ljava/io/InputStream;)Lcom/fasterxml/aalto/in/ByteSourceBootstrapper;
        //   163: invokevirtual   com/fasterxml/aalto/in/ByteSourceBootstrapper.bootstrap:()Lcom/fasterxml/aalto/in/XmlScanner;
        //   166: astore_2       
        //   167: goto            149
        //   170: aload_0        
        //   171: aload_0        
        //   172: getfield        com/fasterxml/aalto/sax/SAXParserImpl._scanner:Lcom/fasterxml/aalto/in/XmlScanner;
        //   175: invokevirtual   com/fasterxml/aalto/in/XmlScanner.getAttrCollector:()Lcom/fasterxml/aalto/in/AttributeCollector;
        //   178: putfield        com/fasterxml/aalto/sax/SAXParserImpl._attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;
        //   181: aload_0        
        //   182: invokespecial   com/fasterxml/aalto/sax/SAXParserImpl.fireEvents:()V
        //   185: aload_0        
        //   186: getfield        com/fasterxml/aalto/sax/SAXParserImpl._contentHandler:Lorg/xml/sax/ContentHandler;
        //   189: astore_2       
        //   190: aload_2        
        //   191: ifnull          200
        //   194: aload_2        
        //   195: invokeinterface org/xml/sax/ContentHandler.endDocument:()V
        //   200: aload_0        
        //   201: getfield        com/fasterxml/aalto/sax/SAXParserImpl._scanner:Lcom/fasterxml/aalto/in/XmlScanner;
        //   204: astore_2       
        //   205: aload_2        
        //   206: ifnull          219
        //   209: aload_2        
        //   210: iconst_0       
        //   211: invokevirtual   com/fasterxml/aalto/in/XmlScanner.close:(Z)V
        //   214: aload_0        
        //   215: aconst_null    
        //   216: putfield        com/fasterxml/aalto/sax/SAXParserImpl._scanner:Lcom/fasterxml/aalto/in/XmlScanner;
        //   219: aload_3        
        //   220: ifnull          227
        //   223: aload_3        
        //   224: invokevirtual   java/io/Reader.close:()V
        //   227: aload_1        
        //   228: ifnull          297
        //   231: aload_1        
        //   232: invokevirtual   java/io/InputStream.close:()V
        //   235: goto            297
        //   238: astore_2       
        //   239: goto            298
        //   242: astore_2       
        //   243: aload_0        
        //   244: aload_2        
        //   245: invokespecial   com/fasterxml/aalto/sax/SAXParserImpl.throwSaxException:(Ljava/lang/Exception;)V
        //   248: aload_0        
        //   249: getfield        com/fasterxml/aalto/sax/SAXParserImpl._contentHandler:Lorg/xml/sax/ContentHandler;
        //   252: astore_2       
        //   253: aload_2        
        //   254: ifnull          263
        //   257: aload_2        
        //   258: invokeinterface org/xml/sax/ContentHandler.endDocument:()V
        //   263: aload_0        
        //   264: getfield        com/fasterxml/aalto/sax/SAXParserImpl._scanner:Lcom/fasterxml/aalto/in/XmlScanner;
        //   267: astore_2       
        //   268: aload_2        
        //   269: ifnull          282
        //   272: aload_2        
        //   273: iconst_0       
        //   274: invokevirtual   com/fasterxml/aalto/in/XmlScanner.close:(Z)V
        //   277: aload_0        
        //   278: aconst_null    
        //   279: putfield        com/fasterxml/aalto/sax/SAXParserImpl._scanner:Lcom/fasterxml/aalto/in/XmlScanner;
        //   282: aload_3        
        //   283: ifnull          290
        //   286: aload_3        
        //   287: invokevirtual   java/io/Reader.close:()V
        //   290: aload_1        
        //   291: ifnull          297
        //   294: goto            231
        //   297: return         
        //   298: aload_0        
        //   299: getfield        com/fasterxml/aalto/sax/SAXParserImpl._contentHandler:Lorg/xml/sax/ContentHandler;
        //   302: astore          4
        //   304: aload           4
        //   306: ifnull          316
        //   309: aload           4
        //   311: invokeinterface org/xml/sax/ContentHandler.endDocument:()V
        //   316: aload_0        
        //   317: getfield        com/fasterxml/aalto/sax/SAXParserImpl._scanner:Lcom/fasterxml/aalto/in/XmlScanner;
        //   320: astore          4
        //   322: aload           4
        //   324: ifnull          338
        //   327: aload           4
        //   329: iconst_0       
        //   330: invokevirtual   com/fasterxml/aalto/in/XmlScanner.close:(Z)V
        //   333: aload_0        
        //   334: aconst_null    
        //   335: putfield        com/fasterxml/aalto/sax/SAXParserImpl._scanner:Lcom/fasterxml/aalto/in/XmlScanner;
        //   338: aload_3        
        //   339: ifnull          346
        //   342: aload_3        
        //   343: invokevirtual   java/io/Reader.close:()V
        //   346: aload_1        
        //   347: ifnull          354
        //   350: aload_1        
        //   351: invokevirtual   java/io/InputStream.close:()V
        //   354: aload_2        
        //   355: athrow         
        //   356: astore_2       
        //   357: goto            214
        //   360: astore_2       
        //   361: goto            227
        //   364: astore_1       
        //   365: goto            297
        //   368: astore_2       
        //   369: goto            277
        //   372: astore_2       
        //   373: goto            290
        //   376: astore          4
        //   378: goto            333
        //   381: astore_3       
        //   382: goto            346
        //   385: astore_1       
        //   386: goto            354
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                 
        //  -----  -----  -----  -----  -------------------------------------
        //  60     69     72     97     Ljava/io/IOException;
        //  139    149    242    376    Ljavax/xml/stream/XMLStreamException;
        //  139    149    238    389    Any
        //  149    154    242    376    Ljavax/xml/stream/XMLStreamException;
        //  149    154    238    389    Any
        //  157    167    242    376    Ljavax/xml/stream/XMLStreamException;
        //  157    167    238    389    Any
        //  170    185    242    376    Ljavax/xml/stream/XMLStreamException;
        //  170    185    238    389    Any
        //  209    214    356    360    Ljavax/xml/stream/XMLStreamException;
        //  223    227    360    364    Ljava/io/IOException;
        //  231    235    364    368    Ljava/io/IOException;
        //  243    248    238    389    Any
        //  272    277    368    372    Ljavax/xml/stream/XMLStreamException;
        //  286    290    372    376    Ljava/io/IOException;
        //  327    333    376    381    Ljavax/xml/stream/XMLStreamException;
        //  342    346    381    385    Ljava/io/IOException;
        //  350    354    385    389    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException
        //     at java.base/java.util.Collections$1.remove(Collections.java:4714)
        //     at java.base/java.util.AbstractCollection.removeAll(AbstractCollection.java:385)
        //     at com.strobel.decompiler.ast.AstBuilder.convertLocalVariables(AstBuilder.java:3018)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2501)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public void parse(final InputSource inputSource, final HandlerBase handlerBase) {
        if (handlerBase != null) {
            if (this._contentHandler == null) {
                this.setDocumentHandler(handlerBase);
            }
            if (this._entityResolver == null) {
                this.setEntityResolver(handlerBase);
            }
            if (this._errorHandler == null) {
                this.setErrorHandler(handlerBase);
            }
            if (this._dtdHandler == null) {
                this.setDTDHandler(handlerBase);
            }
        }
        this.parse(inputSource);
    }
    
    @Override
    public void parse(final InputSource inputSource, final DefaultHandler defaultHandler) {
        if (defaultHandler != null) {
            if (this._contentHandler == null) {
                this.setContentHandler(defaultHandler);
            }
            if (this._entityResolver == null) {
                this.setEntityResolver(defaultHandler);
            }
            if (this._errorHandler == null) {
                this.setErrorHandler(defaultHandler);
            }
            if (this._dtdHandler == null) {
                this.setDTDHandler(defaultHandler);
            }
        }
        this.parse(inputSource);
    }
    
    @Override
    public void setContentHandler(final ContentHandler contentHandler) {
        this._contentHandler = contentHandler;
    }
    
    @Override
    public void setDTDHandler(final DTDHandler dtdHandler) {
        this._dtdHandler = dtdHandler;
    }
    
    @Override
    public void setDocumentHandler(final DocumentHandler documentHandler) {
        this.setContentHandler(new DocHandlerWrapper(documentHandler));
    }
    
    @Override
    public void setEntityResolver(final EntityResolver entityResolver) {
        this._entityResolver = entityResolver;
    }
    
    @Override
    public void setErrorHandler(final ErrorHandler errorHandler) {
        this._errorHandler = errorHandler;
    }
    
    @Override
    public void setFeature(final String s, final boolean b) {
        if (SAXUtil.findStdFeature(s) == null) {
            SAXUtil.reportUnknownFeature(s);
        }
    }
    
    @Override
    public void setLocale(final Locale locale) {
    }
    
    @Override
    public void setProperty(String value, final Object obj) {
        final SAXProperty stdProperty = SAXUtil.findStdProperty(value);
        Label_0098: {
            if (stdProperty != null) {
                final int n = SAXParserImpl$1.$SwitchMap$com$fasterxml$aalto$sax$SAXProperty[stdProperty.ordinal()];
                if (n == 1) {
                    this._declHandler = (DeclHandler)obj;
                    return;
                }
                if (n != 2) {
                    if (n != 3) {
                        if (n != 4) {
                            if (n != 5) {
                                break Label_0098;
                            }
                        }
                        else {
                            this._lexicalHandler = (LexicalHandler)obj;
                        }
                    }
                    return;
                }
                final ReaderConfig config = this._scanner.getConfig();
                if (obj == null) {
                    value = null;
                }
                else {
                    value = String.valueOf(obj);
                }
                config.setXmlVersion(value);
                return;
            }
        }
        SAXUtil.reportUnknownFeature(value);
    }
    
    public static final class AttributesWrapper implements AttributeList
    {
        Attributes mAttrs;
        
        @Override
        public int getLength() {
            return this.mAttrs.getLength();
        }
        
        @Override
        public String getName(final int n) {
            String s;
            if ((s = this.mAttrs.getQName(n)) == null) {
                s = this.mAttrs.getLocalName(n);
            }
            return s;
        }
        
        @Override
        public String getType(final int n) {
            return this.mAttrs.getType(n);
        }
        
        @Override
        public String getType(final String s) {
            return this.mAttrs.getType(s);
        }
        
        @Override
        public String getValue(final int n) {
            return this.mAttrs.getValue(n);
        }
        
        @Override
        public String getValue(final String s) {
            return this.mAttrs.getValue(s);
        }
        
        public void setAttributes(final Attributes mAttrs) {
            this.mAttrs = mAttrs;
        }
    }
    
    public static final class DocHandlerWrapper implements ContentHandler
    {
        final AttributesWrapper mAttrWrapper;
        final DocumentHandler mDocHandler;
        
        public DocHandlerWrapper(final DocumentHandler mDocHandler) {
            this.mAttrWrapper = new AttributesWrapper();
            this.mDocHandler = mDocHandler;
        }
        
        @Override
        public void characters(final char[] array, final int n, final int n2) {
            this.mDocHandler.characters(array, n, n2);
        }
        
        @Override
        public void endDocument() {
            this.mDocHandler.endDocument();
        }
        
        @Override
        public void endElement(final String s, String s2, final String s3) {
            if (s3 != null) {
                s2 = s3;
            }
            this.mDocHandler.endElement(s2);
        }
        
        @Override
        public void endPrefixMapping(final String s) {
        }
        
        @Override
        public void ignorableWhitespace(final char[] array, final int n, final int n2) {
            this.mDocHandler.ignorableWhitespace(array, n, n2);
        }
        
        @Override
        public void processingInstruction(final String s, final String s2) {
            this.mDocHandler.processingInstruction(s, s2);
        }
        
        @Override
        public void setDocumentLocator(final Locator documentLocator) {
            this.mDocHandler.setDocumentLocator(documentLocator);
        }
        
        @Override
        public void skippedEntity(final String s) {
        }
        
        @Override
        public void startDocument() {
            this.mDocHandler.startDocument();
        }
        
        @Override
        public void startElement(final String s, String s2, final String s3, final Attributes attributes) {
            if (s3 != null) {
                s2 = s3;
            }
            this.mAttrWrapper.setAttributes(attributes);
            this.mDocHandler.startElement(s2, this.mAttrWrapper);
        }
        
        @Override
        public void startPrefixMapping(final String s, final String s2) {
        }
    }
}

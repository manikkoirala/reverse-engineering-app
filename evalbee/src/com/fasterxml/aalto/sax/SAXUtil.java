// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.sax;

import org.xml.sax.SAXNotRecognizedException;
import java.util.HashMap;

public final class SAXUtil
{
    static final HashMap<String, SAXFeature> sStdFeatures;
    static final HashMap<String, SAXProperty> sStdProperties;
    
    static {
        final SAXFeature[] values = SAXFeature.values();
        sStdFeatures = new HashMap<String, SAXFeature>(values.length);
        final int length = values.length;
        final int n = 0;
        for (final SAXFeature value : values) {
            SAXUtil.sStdFeatures.put(value.getSuffix(), value);
        }
        final SAXProperty[] values2 = SAXProperty.values();
        sStdProperties = new HashMap<String, SAXProperty>(values2.length);
        for (int length2 = values2.length, j = n; j < length2; ++j) {
            final SAXProperty value2 = values2[j];
            SAXUtil.sStdProperties.put(value2.getSuffix(), value2);
        }
    }
    
    private SAXUtil() {
    }
    
    public static SAXFeature findStdFeature(String substring) {
        if (substring.startsWith("http://xml.org/sax/features/")) {
            substring = substring.substring(28);
            return SAXUtil.sStdFeatures.get(substring);
        }
        return null;
    }
    
    public static SAXProperty findStdProperty(String substring) {
        if (substring.startsWith("http://xml.org/sax/properties/")) {
            substring = substring.substring(30);
            return SAXUtil.sStdProperties.get(substring);
        }
        return null;
    }
    
    public static Boolean getFixedStdFeatureValue(final SAXFeature saxFeature) {
        switch (SAXUtil$1.$SwitchMap$com$fasterxml$aalto$sax$SAXFeature[saxFeature.ordinal()]) {
            default: {
                return null;
            }
            case 15: {
                return Boolean.FALSE;
            }
            case 14: {
                return Boolean.TRUE;
            }
            case 13: {
                return Boolean.FALSE;
            }
            case 10:
            case 11:
            case 12: {
                return Boolean.TRUE;
            }
            case 9: {
                return Boolean.FALSE;
            }
            case 8: {
                return Boolean.TRUE;
            }
            case 7: {
                return Boolean.FALSE;
            }
            case 6: {
                return Boolean.FALSE;
            }
            case 5: {
                return Boolean.TRUE;
            }
            case 4: {
                return Boolean.TRUE;
            }
            case 2: {
                return Boolean.FALSE;
            }
            case 1: {
                return Boolean.FALSE;
            }
        }
    }
    
    public static void reportUnknownFeature(final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Feature '");
        sb.append(str);
        sb.append("' not recognized");
        throw new SAXNotRecognizedException(sb.toString());
    }
    
    public static void reportUnknownProperty(final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Property '");
        sb.append(str);
        sb.append("' not recognized");
        throw new SAXNotRecognizedException(sb.toString());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto;

public interface AsyncInputFeeder
{
    void endOfInput();
    
    boolean needMoreInput();
}

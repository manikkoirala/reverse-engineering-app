// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto;

public interface AsyncByteArrayFeeder extends AsyncInputFeeder
{
    void feedInput(final byte[] p0, final int p1, final int p2);
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto;

import java.io.PrintWriter;
import java.io.PrintStream;
import javax.xml.stream.XMLStreamException;

public class UncheckedStreamException extends RuntimeException
{
    private UncheckedStreamException(final XMLStreamException cause) {
        super(cause);
    }
    
    public static UncheckedStreamException createFrom(final XMLStreamException ex) {
        return new UncheckedStreamException(ex);
    }
    
    @Override
    public String getLocalizedMessage() {
        return this.getCause().getLocalizedMessage();
    }
    
    @Override
    public String getMessage() {
        return this.getCause().getMessage();
    }
    
    @Override
    public StackTraceElement[] getStackTrace() {
        return this.getCause().getStackTrace();
    }
    
    @Override
    public void printStackTrace() {
        this.getCause().printStackTrace();
    }
    
    @Override
    public void printStackTrace(final PrintStream s) {
        this.getCause().printStackTrace(s);
    }
    
    @Override
    public void printStackTrace(final PrintWriter s) {
        this.getCause().printStackTrace(s);
    }
    
    @Override
    public String toString() {
        return this.getCause().toString();
    }
}

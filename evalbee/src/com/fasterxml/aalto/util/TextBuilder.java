// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.util;

import java.util.List;
import org.codehaus.stax2.ri.typed.CharArrayBase64Decoder;
import org.codehaus.stax2.typed.Base64Variant;
import java.io.Writer;
import org.xml.sax.ext.LexicalHandler;
import org.xml.sax.ContentHandler;
import org.codehaus.stax2.typed.TypedArrayDecoder;
import java.util.ArrayList;
import com.fasterxml.aalto.in.ReaderConfig;

public final class TextBuilder
{
    static final int DEF_INITIAL_BUFFER_SIZE = 500;
    static final int INT_SPACE = 32;
    public static final int MAX_INDENT_SPACES = 32;
    public static final int MAX_INDENT_TABS = 8;
    static final int MAX_SEGMENT_LENGTH = 262144;
    private static final String sIndSpaces = "\n                                 ";
    private static final char[] sIndSpacesArray;
    private static final String[] sIndSpacesStrings;
    private static final String sIndTabs = "\n\t\t\t\t\t\t\t\t\t";
    private static final char[] sIndTabsArray;
    private static final String[] sIndTabsStrings;
    static final char[] sNoChars;
    private final ReaderConfig _config;
    private char[] _currentSegment;
    private int _currentSize;
    private char[] _decodeBuffer;
    private int _decodeEnd;
    private int _decodePtr;
    private boolean _isIndentation;
    private char[] _resultArray;
    private int _resultLen;
    private String _resultString;
    private int _segmentSize;
    private ArrayList<char[]> _segments;
    
    static {
        sNoChars = new char[0];
        sIndSpacesStrings = new String[(sIndSpacesArray = "\n                                 ".toCharArray()).length];
        sIndTabsStrings = new String[(sIndTabsArray = "\n\t\t\t\t\t\t\t\t\t".toCharArray()).length];
    }
    
    private TextBuilder(final ReaderConfig config) {
        this._isIndentation = false;
        this._config = config;
    }
    
    private final char[] allocBuffer(int max) {
        max = Math.max(500, max);
        final ReaderConfig config = this._config;
        if (config != null) {
            final char[] allocMediumCBuffer = config.allocMediumCBuffer(max);
            if (allocMediumCBuffer != null) {
                return allocMediumCBuffer;
            }
        }
        return new char[max];
    }
    
    private char[] buildResultArray() {
        final String resultString = this._resultString;
        if (resultString != null) {
            return resultString.toCharArray();
        }
        final int size = this.size();
        if (size < 1) {
            return TextBuilder.sNoChars;
        }
        final char[] array = new char[size];
        final ArrayList<char[]> segments = this._segments;
        int n2;
        if (segments != null) {
            final int size2 = segments.size();
            int index = 0;
            int n = 0;
            while (true) {
                n2 = n;
                if (index >= size2) {
                    break;
                }
                final char[] array2 = this._segments.get(index);
                final int length = array2.length;
                System.arraycopy(array2, 0, array, n, length);
                n += length;
                ++index;
            }
        }
        else {
            n2 = 0;
        }
        System.arraycopy(this._currentSegment, 0, array, n2, this._currentSize);
        return array;
    }
    
    private int calcNewSize(final int n) {
        int n2;
        if (n < 8000) {
            n2 = n;
        }
        else {
            n2 = n >> 1;
        }
        return Math.min(n + n2, 262144);
    }
    
    public static TextBuilder createRecyclableBuffer(final ReaderConfig readerConfig) {
        return new TextBuilder(readerConfig);
    }
    
    private void expand(final int a) {
        if (this._segments == null) {
            this._segments = new ArrayList<char[]>();
        }
        final char[] currentSegment = this._currentSegment;
        this._segments.add(currentSegment);
        final int length = currentSegment.length;
        this._segmentSize += length;
        final char[] currentSegment2 = new char[Math.max(a, this.calcNewSize(length))];
        this._currentSize = 0;
        this._currentSegment = currentSegment2;
    }
    
    private final void resetForDecode() {
        this._decodePtr = 0;
        final ArrayList<char[]> segments = this._segments;
        int decodeEnd;
        if (segments != null && segments.size() != 0) {
            final char[] contentsAsArray = this.contentsAsArray();
            this._decodeBuffer = contentsAsArray;
            decodeEnd = contentsAsArray.length;
        }
        else if (this._isIndentation) {
            final char[] resultArray = this._resultArray;
            this._decodeBuffer = resultArray;
            decodeEnd = resultArray.length;
        }
        else {
            this._decodeBuffer = this._currentSegment;
            decodeEnd = this._currentSize;
        }
        this._decodeEnd = decodeEnd;
    }
    
    public void append(final char c) {
        this._resultString = null;
        this._resultArray = null;
        final char[] currentSegment = this._currentSegment;
        if (this._currentSize >= currentSegment.length) {
            this.expand(1);
        }
        currentSegment[this._currentSize++] = c;
    }
    
    public void append(final String s) {
        this._resultString = null;
        this._resultArray = null;
        final int length = s.length();
        final char[] currentSegment = this._currentSegment;
        final int length2 = currentSegment.length;
        final int currentSize = this._currentSize;
        final int n = length2 - currentSize;
        if (n >= length) {
            s.getChars(0, length, currentSegment, currentSize);
            this._currentSize += length;
        }
        else {
            int currentSize2 = length;
            if (n > 0) {
                s.getChars(0, n, currentSegment, currentSize);
                currentSize2 = length - n;
            }
            this.expand(currentSize2);
            s.getChars(n, n + currentSize2, this._currentSegment, 0);
            this._currentSize = currentSize2;
        }
    }
    
    public void append(final char[] array, final int n, final int n2) {
        this._resultString = null;
        this._resultArray = null;
        final char[] currentSegment = this._currentSegment;
        final int length = currentSegment.length;
        final int currentSize = this._currentSize;
        final int n3 = length - currentSize;
        if (n3 >= n2) {
            System.arraycopy(array, n, currentSegment, currentSize, n2);
            this._currentSize += n2;
        }
        else {
            int n4 = n;
            int currentSize2 = n2;
            if (n3 > 0) {
                System.arraycopy(array, n, currentSegment, currentSize, n3);
                n4 = n + n3;
                currentSize2 = n2 - n3;
            }
            this.expand(currentSize2);
            System.arraycopy(array, n4, this._currentSegment, 0, currentSize2);
            this._currentSize = currentSize2;
        }
    }
    
    public void appendSurrogate(final int n) {
        this.append((char)(n >> 10 | 0xD800));
        this.append((char)((n & 0x3FF) | 0xDC00));
    }
    
    public char[] contentsAsArray() {
        char[] resultArray;
        if ((resultArray = this._resultArray) == null) {
            resultArray = this.buildResultArray();
            this._resultArray = resultArray;
        }
        return resultArray;
    }
    
    public String contentsAsString() {
        if (this._resultString == null) {
            final char[] resultArray = this._resultArray;
            if (resultArray != null) {
                this._resultString = new String(resultArray);
            }
            else {
                final int segmentSize = this._segmentSize;
                final int currentSize = this._currentSize;
                if (segmentSize == 0) {
                    String resultString;
                    if (currentSize == 0) {
                        resultString = "";
                    }
                    else {
                        resultString = new String(this._currentSegment, 0, currentSize);
                    }
                    return this._resultString = resultString;
                }
                final StringBuilder sb = new StringBuilder(segmentSize + currentSize);
                final ArrayList<char[]> segments = this._segments;
                if (segments != null) {
                    for (int size = segments.size(), i = 0; i < size; ++i) {
                        final char[] str = this._segments.get(i);
                        sb.append(str, 0, str.length);
                    }
                }
                sb.append(this._currentSegment, 0, currentSize);
                this._resultString = sb.toString();
            }
        }
        return this._resultString;
    }
    
    public int contentsToArray(int n, final char[] array, int n2, int n3) {
        final ArrayList<char[]> segments = this._segments;
        int n4 = 0;
        int n5 = n;
        int n6 = n2;
        int n7 = n3;
        if (segments != null) {
            final int size = segments.size();
            int i = 0;
            int n8 = 0;
            while (i < size) {
                final char[] array2 = this._segments.get(i);
                final int length = array2.length;
                final int n9 = length - n;
                if (n9 < 1) {
                    n -= length;
                }
                else {
                    if (n9 >= n3) {
                        System.arraycopy(array2, n, array, n2, n3);
                        return n8 + n3;
                    }
                    System.arraycopy(array2, n, array, n2, n9);
                    n8 += n9;
                    n2 += n9;
                    n3 -= n9;
                    n = 0;
                }
                ++i;
            }
            n4 = n8;
            n7 = n3;
            n6 = n2;
            n5 = n;
        }
        n2 = n4;
        if (n7 > 0) {
            n2 = this._currentSize - n5;
            if ((n = n7) > n2) {
                n = n2;
            }
            n2 = n4;
            if (n > 0) {
                System.arraycopy(this._currentSegment, n5, array, n6, n);
                n2 = n4 + n;
            }
        }
        return n2;
    }
    
    public int decodeElements(final TypedArrayDecoder p0, final boolean p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: ifeq            8
        //     4: aload_0        
        //     5: invokespecial   com/fasterxml/aalto/util/TextBuilder.resetForDecode:()V
        //     8: aload_0        
        //     9: getfield        com/fasterxml/aalto/util/TextBuilder._decodePtr:I
        //    12: istore_3       
        //    13: aload_0        
        //    14: getfield        com/fasterxml/aalto/util/TextBuilder._decodeBuffer:[C
        //    17: astore          10
        //    19: aload_0        
        //    20: getfield        com/fasterxml/aalto/util/TextBuilder._decodeEnd:I
        //    23: istore          9
        //    25: iconst_0       
        //    26: istore          5
        //    28: iload_3        
        //    29: istore          6
        //    31: iload           6
        //    33: istore          7
        //    35: iload           5
        //    37: istore          8
        //    39: iload_3        
        //    40: istore          4
        //    42: iload_3        
        //    43: iload           9
        //    45: if_icmpge       179
        //    48: aload           10
        //    50: iload_3        
        //    51: caload         
        //    52: istore          4
        //    54: iload           4
        //    56: bipush          32
        //    58: if_icmpgt       87
        //    61: iload_3        
        //    62: iconst_1       
        //    63: iadd           
        //    64: istore          4
        //    66: iload           4
        //    68: istore_3       
        //    69: iload           4
        //    71: iload           9
        //    73: if_icmplt       48
        //    76: iload           6
        //    78: istore          7
        //    80: iload           5
        //    82: istore          8
        //    84: goto            179
        //    87: iload_3        
        //    88: iconst_1       
        //    89: iadd           
        //    90: istore          6
        //    92: iload           6
        //    94: iload           9
        //    96: if_icmpge       119
        //    99: aload           10
        //   101: iload           6
        //   103: caload         
        //   104: istore          4
        //   106: iload           4
        //   108: bipush          32
        //   110: if_icmple       119
        //   113: iinc            6, 1
        //   116: goto            92
        //   119: iinc            5, 1
        //   122: iload           6
        //   124: iconst_1       
        //   125: iadd           
        //   126: istore          4
        //   128: aload_1        
        //   129: aload           10
        //   131: iload_3        
        //   132: iload           6
        //   134: invokevirtual   org/codehaus/stax2/typed/TypedArrayDecoder.decodeValue:([CII)Z
        //   137: ifeq            150
        //   140: iload_3        
        //   141: istore          7
        //   143: iload           5
        //   145: istore          8
        //   147: goto            179
        //   150: aload_0        
        //   151: iload           4
        //   153: putfield        com/fasterxml/aalto/util/TextBuilder._decodePtr:I
        //   156: iload_3        
        //   157: istore          6
        //   159: iload           4
        //   161: istore_3       
        //   162: goto            31
        //   165: astore_1       
        //   166: goto            195
        //   169: astore_1       
        //   170: iload_3        
        //   171: istore          4
        //   173: iload           7
        //   175: istore_3       
        //   176: goto            195
        //   179: iload           4
        //   181: istore_3       
        //   182: aload_0        
        //   183: iload           4
        //   185: putfield        com/fasterxml/aalto/util/TextBuilder._decodePtr:I
        //   188: iload           8
        //   190: ireturn        
        //   191: astore_1       
        //   192: iload_3        
        //   193: istore          4
        //   195: new             Lorg/codehaus/stax2/typed/TypedXMLStreamException;
        //   198: dup            
        //   199: new             Ljava/lang/String;
        //   202: dup            
        //   203: aload           10
        //   205: iload_3        
        //   206: iload           4
        //   208: iload_3        
        //   209: isub           
        //   210: iconst_1       
        //   211: isub           
        //   212: invokespecial   java/lang/String.<init>:([CII)V
        //   215: aload_1        
        //   216: invokevirtual   java/lang/Throwable.getMessage:()Ljava/lang/String;
        //   219: aload_1        
        //   220: invokespecial   org/codehaus/stax2/typed/TypedXMLStreamException.<init>:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/IllegalArgumentException;)V
        //   223: athrow         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                
        //  -----  -----  -----  -----  ------------------------------------
        //  19     25     191    195    Ljava/lang/IllegalArgumentException;
        //  128    140    165    169    Ljava/lang/IllegalArgumentException;
        //  150    156    165    169    Ljava/lang/IllegalArgumentException;
        //  182    188    169    179    Ljava/lang/IllegalArgumentException;
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException
        //     at java.base/java.util.Collections$1.remove(Collections.java:4714)
        //     at java.base/java.util.AbstractCollection.removeAll(AbstractCollection.java:385)
        //     at com.strobel.decompiler.ast.AstBuilder.convertLocalVariables(AstBuilder.java:3018)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2501)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public boolean endsWith(final String s) {
        final ArrayList<char[]> segments = this._segments;
        int size;
        if (segments == null) {
            size = 0;
        }
        else {
            size = segments.size();
        }
        int n = s.length() - 1;
        char[] currentSegment = this._currentSegment;
        int n2 = this._currentSize;
    Label_0041:
        while (true) {
            --n2;
            int i = n;
            while (i >= 0) {
                if (s.charAt(i) != currentSegment[n2]) {
                    return false;
                }
                n = i - 1;
                if (n == 0) {
                    break;
                }
                final int n3 = n2 - 1;
                i = n;
                if ((n2 = n3) >= 0) {
                    continue;
                }
                if (--size < 0) {
                    return false;
                }
                currentSegment = this._segments.get(size);
                n2 = currentSegment.length;
                continue Label_0041;
            }
            break;
        }
        return true;
    }
    
    public boolean equalsString(final String s) {
        final int length = s.length();
        if (length != this.size()) {
            return false;
        }
        final ArrayList<char[]> segments = this._segments;
        char[] array;
        if (segments != null && segments.size()) {
            array = this.contentsAsArray();
        }
        else {
            array = this._currentSegment;
        }
        for (int i = 0; i < length; ++i) {
            if (array[i] != s.charAt(i)) {
                return false;
            }
        }
        return true;
    }
    
    public char[] finishCurrentSegment() {
        if (this._segments == null) {
            this._segments = new ArrayList<char[]>();
        }
        this._segments.add(this._currentSegment);
        final int length = this._currentSegment.length;
        this._segmentSize += length;
        final char[] currentSegment = new char[this.calcNewSize(length)];
        this._currentSize = 0;
        return this._currentSegment = currentSegment;
    }
    
    public void fireSaxCharacterEvents(final ContentHandler contentHandler) {
        final char[] resultArray = this._resultArray;
        if (resultArray != null) {
            contentHandler.characters(resultArray, 0, this._resultLen);
        }
        else {
            final ArrayList<char[]> segments = this._segments;
            if (segments != null) {
                for (int size = segments.size(), i = 0; i < size; ++i) {
                    final char[] array = this._segments.get(i);
                    contentHandler.characters(array, 0, array.length);
                }
            }
            final int currentSize = this._currentSize;
            if (currentSize > 0) {
                contentHandler.characters(this._currentSegment, 0, currentSize);
            }
        }
    }
    
    public void fireSaxCommentEvent(final LexicalHandler lexicalHandler) {
        final char[] resultArray = this._resultArray;
        if (resultArray != null) {
            lexicalHandler.comment(resultArray, 0, this._resultLen);
        }
        else {
            final ArrayList<char[]> segments = this._segments;
            if (segments != null && segments.size() > 0) {
                final char[] contentsAsArray = this.contentsAsArray();
                lexicalHandler.comment(contentsAsArray, 0, contentsAsArray.length);
            }
            else {
                lexicalHandler.comment(this._currentSegment, 0, this._currentSize);
            }
        }
    }
    
    public void fireSaxSpaceEvents(final ContentHandler contentHandler) {
        final char[] resultArray = this._resultArray;
        if (resultArray != null) {
            contentHandler.ignorableWhitespace(resultArray, 0, this._resultLen);
        }
        else {
            final ArrayList<char[]> segments = this._segments;
            if (segments != null) {
                for (int size = segments.size(), i = 0; i < size; ++i) {
                    final char[] array = this._segments.get(i);
                    contentHandler.ignorableWhitespace(array, 0, array.length);
                }
            }
            final int currentSize = this._currentSize;
            if (currentSize > 0) {
                contentHandler.ignorableWhitespace(this._currentSegment, 0, currentSize);
            }
        }
    }
    
    public char[] getBufferWithoutReset() {
        return this._currentSegment;
    }
    
    public int getCurrentLength() {
        return this._currentSize;
    }
    
    public char[] getTextBuffer() {
        final ArrayList<char[]> segments = this._segments;
        if (segments != null && segments.size()) {
            return this.contentsAsArray();
        }
        final char[] resultArray = this._resultArray;
        if (resultArray != null) {
            return resultArray;
        }
        return this._currentSegment;
    }
    
    public boolean isAllWhitespace() {
        if (this._isIndentation) {
            return true;
        }
        final ArrayList<char[]> segments = this._segments;
        if (segments != null) {
            for (int size = segments.size(), i = 0; i < size; ++i) {
                final char[] array = this._segments.get(i);
                for (int length = array.length, j = 0; j < length; ++j) {
                    if (array[j] > ' ') {
                        return false;
                    }
                }
            }
        }
        final char[] currentSegment = this._currentSegment;
        for (int currentSize = this._currentSize, k = 0; k < currentSize; ++k) {
            if (currentSegment[k] > ' ') {
                return false;
            }
        }
        return true;
    }
    
    public int rawContentsTo(final Writer writer) {
        final char[] resultArray = this._resultArray;
        if (resultArray != null) {
            writer.write(resultArray);
            return this._resultArray.length;
        }
        final String resultString = this._resultString;
        if (resultString != null) {
            writer.write(resultString);
            return this._resultString.length();
        }
        final ArrayList<char[]> segments = this._segments;
        int n2;
        if (segments != null) {
            final int size = segments.size();
            int index = 0;
            int n = 0;
            while (true) {
                n2 = n;
                if (index >= size) {
                    break;
                }
                final char[] cbuf = this._segments.get(index);
                writer.write(cbuf);
                n += cbuf.length;
                ++index;
            }
        }
        else {
            n2 = 0;
        }
        final int currentSize = this._currentSize;
        int n3 = n2;
        if (currentSize > 0) {
            writer.write(this._currentSegment, 0, currentSize);
            n3 = n2 + this._currentSize;
        }
        return n3;
    }
    
    public void recycle(final boolean b) {
        if (this._config != null && this._currentSegment != null) {
            if (b) {
                this._resultString = null;
                this._resultArray = null;
            }
            else if (this._segmentSize + this._currentSize > 0) {
                return;
            }
            final ArrayList<char[]> segments = this._segments;
            if (segments != null && segments.size() > 0) {
                this._segments.clear();
                this._segmentSize = 0;
            }
            final char[] currentSegment = this._currentSegment;
            this._currentSegment = null;
            this._config.freeMediumCBuffer(currentSegment);
        }
    }
    
    public void resetForBinaryDecode(final Base64Variant base64Variant, final CharArrayBase64Decoder charArrayBase64Decoder, final boolean b) {
        final ArrayList<char[]> segments = this._segments;
        if ((segments == null || segments.size() == 0) && this._isIndentation) {
            final char[] resultArray = this._resultArray;
            charArrayBase64Decoder.init(base64Variant, b, resultArray, 0, resultArray.length, (List)null);
            return;
        }
        charArrayBase64Decoder.init(base64Variant, b, this._currentSegment, 0, this._currentSize, (List)this._segments);
    }
    
    public void resetWithChar(final char c) {
        this._resultString = null;
        this._resultArray = null;
        this._isIndentation = false;
        final ArrayList<char[]> segments = this._segments;
        if (segments != null && segments.size() > 0) {
            this._segments.clear();
            this._segmentSize = 0;
        }
        this._currentSize = 1;
        if (this._currentSegment == null) {
            this._currentSegment = this.allocBuffer(1);
        }
        this._currentSegment[0] = c;
    }
    
    public char[] resetWithEmpty() {
        this._resultString = null;
        this._resultArray = null;
        this._isIndentation = false;
        final ArrayList<char[]> segments = this._segments;
        if (segments != null && segments.size() > 0) {
            this._segments.clear();
            this._segmentSize = 0;
        }
        this._currentSize = 0;
        if (this._currentSegment == null) {
            this._currentSegment = this.allocBuffer(0);
        }
        return this._currentSegment;
    }
    
    public void resetWithIndentation(final int n, final char c) {
        final ArrayList<char[]> segments = this._segments;
        if (segments != null && segments.size() > 0) {
            this._segments.clear();
            this._segmentSize = 0;
        }
        this._currentSize = -1;
        this._isIndentation = true;
        final int endIndex = n + 1;
        this._resultLen = endIndex;
        String resultString;
        if (c == '\t') {
            this._resultArray = TextBuilder.sIndTabsArray;
            final String[] sIndTabsStrings = TextBuilder.sIndTabsStrings;
            if ((resultString = sIndTabsStrings[n]) == null) {
                resultString = "\n\t\t\t\t\t\t\t\t\t".substring(0, endIndex);
                sIndTabsStrings[n] = resultString;
            }
        }
        else {
            this._resultArray = TextBuilder.sIndSpacesArray;
            final String[] sIndSpacesStrings = TextBuilder.sIndSpacesStrings;
            if ((resultString = sIndSpacesStrings[n]) == null) {
                resultString = "\n                                 ".substring(0, endIndex);
                sIndSpacesStrings[n] = resultString;
            }
        }
        this._resultString = resultString;
    }
    
    public void resetWithSurrogate(final int n) {
        this._resultString = null;
        this._resultArray = null;
        this._isIndentation = false;
        final ArrayList<char[]> segments = this._segments;
        if (segments != null && segments.size() > 0) {
            this._segments.clear();
            this._segmentSize = 0;
        }
        this._currentSize = 2;
        if (this._currentSegment == null) {
            this._currentSegment = this.allocBuffer(2);
        }
        final char[] currentSegment = this._currentSegment;
        currentSegment[0] = (char)(n >> 10 | 0xD800);
        currentSegment[1] = (char)((n & 0x3FF) | 0xDC00);
    }
    
    public void setCurrentLength(final int currentSize) {
        this._currentSize = currentSize;
    }
    
    public int size() {
        final int currentSize = this._currentSize;
        if (currentSize < 0) {
            return this._resultLen;
        }
        return currentSize + this._segmentSize;
    }
    
    @Override
    public String toString() {
        this._resultString = null;
        this._resultArray = null;
        return this.contentsAsString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.util;

public final class TextAccumulator
{
    private StringBuilder mBuilder;
    private String mText;
    
    public TextAccumulator() {
        this.mText = null;
        this.mBuilder = null;
    }
    
    public void addText(final String s) {
        final int length = s.length();
        if (length > 0) {
            final String mText = this.mText;
            if (mText != null) {
                (this.mBuilder = new StringBuilder(mText.length() + length)).append(this.mText);
                this.mText = null;
            }
            final StringBuilder mBuilder = this.mBuilder;
            if (mBuilder != null) {
                mBuilder.append(s);
            }
            else {
                this.mText = s;
            }
        }
    }
    
    public void addText(final char[] str, final int offset, int n) {
        n -= offset;
        if (n > 0) {
            final String mText = this.mText;
            if (mText != null) {
                (this.mBuilder = new StringBuilder(mText.length() + n)).append(this.mText);
                this.mText = null;
            }
            else if (this.mBuilder == null) {
                this.mBuilder = new StringBuilder(n);
            }
            this.mBuilder.append(str, offset, n);
        }
    }
    
    public String getAndClear() {
        String s = this.mText;
        if (s != null) {
            this.mText = null;
        }
        else {
            final StringBuilder mBuilder = this.mBuilder;
            if (mBuilder != null) {
                s = mBuilder.toString();
                this.mBuilder = null;
            }
            else {
                s = "";
            }
        }
        return s;
    }
    
    public boolean hasText() {
        return this.mBuilder != null || this.mText != null;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.util;

public interface XmlConsts
{
    public static final char CHAR_CR = '\r';
    public static final char CHAR_LF = '\n';
    public static final char CHAR_NULL = '\0';
    public static final char CHAR_SPACE = ' ';
    public static final int MAX_UNICODE_CHAR = 1114111;
    public static final String STAX_DEFAULT_OUTPUT_ENCODING = "UTF-8";
    public static final String STAX_DEFAULT_OUTPUT_VERSION = "1.0";
    public static final String XML_DECL_KW_ENCODING = "encoding";
    public static final String XML_DECL_KW_STANDALONE = "standalone";
    public static final String XML_DECL_KW_VERSION = "version";
    public static final String XML_SA_NO = "no";
    public static final String XML_SA_YES = "yes";
    public static final int XML_V_10 = 256;
    public static final String XML_V_10_STR = "1.0";
    public static final int XML_V_11 = 272;
    public static final String XML_V_11_STR = "1.1";
    public static final int XML_V_UNKNOWN = 0;
}

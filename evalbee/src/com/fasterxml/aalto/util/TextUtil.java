// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.util;

public final class TextUtil
{
    static final int CHAR_SPACE = 32;
    
    private TextUtil() {
    }
    
    public static boolean isAllWhitespace(final String s, final boolean b) {
        for (int length = s.length(), i = 0; i < length; ++i) {
            final char char1 = s.charAt(i);
            if (char1 > ' ' && (!b || char1 != '\u0085')) {
                return false;
            }
        }
        return true;
    }
    
    public static boolean isAllWhitespace(final char[] array, final int n, final int n2, final boolean b) {
        for (int i = n; i < n2 + n; ++i) {
            final char c = array[i];
            if (c > ' ' && (!b || c != '\u0085')) {
                return false;
            }
        }
        return true;
    }
}

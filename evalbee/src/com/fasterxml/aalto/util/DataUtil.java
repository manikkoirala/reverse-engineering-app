// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.util;

import java.lang.reflect.Array;

public final class DataUtil
{
    static final char[] EMPTY_CHAR_ARRAY;
    
    static {
        EMPTY_CHAR_ARRAY = new char[0];
    }
    
    private DataUtil() {
    }
    
    public static char[] getEmptyCharArray() {
        return DataUtil.EMPTY_CHAR_ARRAY;
    }
    
    public static Object growAnyArrayBy(final Object o, final int n) {
        if (o != null) {
            final int length = Array.getLength(o);
            final Object instance = Array.newInstance(o.getClass().getComponentType(), n + length);
            System.arraycopy(o, 0, instance, 0, length);
            return instance;
        }
        throw new IllegalArgumentException("Null array");
    }
    
    public static char[] growArrayBy(final char[] array, final int n) {
        if (array == null) {
            return new char[n];
        }
        final int length = array.length;
        final char[] array2 = new char[n + length];
        System.arraycopy(array, 0, array2, 0, length);
        return array2;
    }
    
    public static int[] growArrayBy(final int[] array, final int n) {
        if (array == null) {
            return new int[n];
        }
        final int length = array.length;
        final int[] array2 = new int[n + length];
        System.arraycopy(array, 0, array2, 0, length);
        return array2;
    }
    
    public static String[] growArrayBy(final String[] array, final int n) {
        if (array == null) {
            return new String[n];
        }
        final int length = array.length;
        final String[] array2 = new String[n + length];
        System.arraycopy(array, 0, array2, 0, length);
        return array2;
    }
}

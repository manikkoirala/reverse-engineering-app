// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.util;

public final class BufferRecycler
{
    private volatile byte[] mFullBBuffer;
    private volatile char[] mFullCBuffer;
    private volatile char[] mMediumCBuffer;
    private volatile char[] mSmallCBuffer;
    
    public BufferRecycler() {
        this.mSmallCBuffer = null;
        this.mMediumCBuffer = null;
        this.mFullCBuffer = null;
        this.mFullBBuffer = null;
    }
    
    public byte[] getFullBBuffer(final int n) {
        synchronized (this) {
            final byte[] mFullBBuffer = this.mFullBBuffer;
            if (mFullBBuffer != null && mFullBBuffer.length >= n) {
                this.mFullBBuffer = null;
                return mFullBBuffer;
            }
            return null;
        }
    }
    
    public char[] getFullCBuffer(final int n) {
        synchronized (this) {
            final char[] mFullCBuffer = this.mFullCBuffer;
            if (mFullCBuffer != null && mFullCBuffer.length >= n) {
                this.mFullCBuffer = null;
                return mFullCBuffer;
            }
            return null;
        }
    }
    
    public char[] getMediumCBuffer(final int n) {
        synchronized (this) {
            final char[] mMediumCBuffer = this.mMediumCBuffer;
            if (mMediumCBuffer != null && mMediumCBuffer.length >= n) {
                this.mMediumCBuffer = null;
                return mMediumCBuffer;
            }
            return null;
        }
    }
    
    public char[] getSmallCBuffer(final int n) {
        synchronized (this) {
            final char[] mSmallCBuffer = this.mSmallCBuffer;
            if (mSmallCBuffer != null && mSmallCBuffer.length >= n) {
                this.mSmallCBuffer = null;
                return mSmallCBuffer;
            }
            return null;
        }
    }
    
    public void returnFullBBuffer(final byte[] mFullBBuffer) {
        synchronized (this) {
            this.mFullBBuffer = mFullBBuffer;
        }
    }
    
    public void returnFullCBuffer(final char[] mFullCBuffer) {
        synchronized (this) {
            this.mFullCBuffer = mFullCBuffer;
        }
    }
    
    public void returnMediumCBuffer(final char[] mMediumCBuffer) {
        synchronized (this) {
            this.mMediumCBuffer = mMediumCBuffer;
        }
    }
    
    public void returnSmallCBuffer(final char[] mSmallCBuffer) {
        synchronized (this) {
            this.mSmallCBuffer = mSmallCBuffer;
        }
    }
}

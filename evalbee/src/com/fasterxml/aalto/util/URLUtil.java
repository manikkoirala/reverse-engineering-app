// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.util;

import java.io.IOException;
import java.net.MalformedURLException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.io.File;

public final class URLUtil
{
    private URLUtil() {
    }
    
    public static String fileToSystemId(final File file) {
        return fileToURL(file).toExternalForm();
    }
    
    public static URL fileToURL(final File file) {
        final String absolutePath = file.getAbsolutePath();
        final char separatorChar = File.separatorChar;
        String replace = absolutePath;
        if (separatorChar != '/') {
            replace = absolutePath.replace(separatorChar, '/');
        }
        String string = replace;
        if (replace.length() > 0) {
            string = replace;
            if (replace.charAt(0) != '/') {
                final StringBuilder sb = new StringBuilder();
                sb.append("/");
                sb.append(replace);
                string = sb.toString();
            }
        }
        return new URL("file", "", string);
    }
    
    public static InputStream inputStreamFromURL(final URL url) {
        if ("file".equals(url.getProtocol())) {
            final String host = url.getHost();
            if (host == null || host.length() == 0) {
                return new FileInputStream(url.getPath());
            }
        }
        return url.openStream();
    }
    
    public static OutputStream outputStreamFromURL(final URL url) {
        if ("file".equals(url.getProtocol())) {
            final String host = url.getHost();
            if (host == null || host.length() == 0) {
                return new FileOutputStream(url.getPath());
            }
        }
        return url.openConnection().getOutputStream();
    }
    
    private static void throwIoException(final MalformedURLException cause, final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append("[resolving systemId '");
        sb.append(str);
        sb.append("']: ");
        sb.append(cause.toString());
        final IOException ex = new IOException(sb.toString());
        ex.initCause(cause);
        throw ex;
    }
    
    public static URL urlFromCurrentDir() {
        return fileToURL(new File("a").getAbsoluteFile().getParentFile());
    }
    
    public static URL urlFromSystemId(final String s) {
        try {
            final int index = s.indexOf(58, 0);
            if (index >= 3 && index <= 8) {
                return new URL(s);
            }
            return fileToURL(new File(s));
        }
        catch (final MalformedURLException ex) {
            throwIoException(ex, s);
            return null;
        }
    }
    
    public static URL urlFromSystemId(final String spec, URL context) {
        if (context == null) {
            return urlFromSystemId(spec);
        }
        try {
            context = new URL(context, spec);
            return context;
        }
        catch (final MalformedURLException ex) {
            throwIoException(ex, spec);
            return null;
        }
    }
    
    public static String urlToSystemId(final URL url) {
        return url.toExternalForm();
    }
}

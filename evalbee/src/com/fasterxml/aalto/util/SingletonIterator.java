// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.util;

import java.util.NoSuchElementException;
import java.util.Iterator;

public final class SingletonIterator implements Iterator<String>
{
    private boolean _done;
    private final String _value;
    
    public SingletonIterator(final String value) {
        this._done = false;
        this._value = value;
    }
    
    @Override
    public boolean hasNext() {
        return this._done ^ true;
    }
    
    @Override
    public String next() {
        if (!this._done) {
            this._done = true;
            return this._value;
        }
        throw new NoSuchElementException();
    }
    
    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }
}

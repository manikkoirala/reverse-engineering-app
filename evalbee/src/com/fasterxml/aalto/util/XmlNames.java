// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.util;

public final class XmlNames
{
    public static int findIllegalNameChar(final String s, final boolean b) {
        int n = 0;
        final char char1 = s.charAt(0);
        final int length = s.length();
        if (char1 >= '\ud800' && char1 < '\ue000') {
            if (length < 2) {
                return 0;
            }
            if (!validSurrogateNameChar(char1, s.charAt(1))) {
                return 1;
            }
            n = 1;
        }
        else if (b) {
            if (!XmlChars.is11NameStartChar(char1)) {
                return 0;
            }
        }
        else if (!XmlChars.is10NameStartChar(char1)) {
            return 0;
        }
        int i;
        final int n2 = i = n + 1;
        if (b) {
            int index;
            for (int j = n2; j < length; j = index + 1) {
                final char char2 = s.charAt(j);
                if (char2 >= '\ud800' && char2 < '\ue000') {
                    index = j + 1;
                    if (index >= length) {
                        return j;
                    }
                    if (!validSurrogateNameChar(char2, s.charAt(index))) {
                        return j;
                    }
                }
                else {
                    index = j;
                    if (!XmlChars.is11NameChar(char2)) {
                        return j;
                    }
                }
            }
        }
        else {
            while (i < length) {
                final char char3 = s.charAt(i);
                int index2;
                if (char3 >= '\ud800' && char3 < '\ue000') {
                    index2 = i + 1;
                    if (index2 >= length) {
                        return i;
                    }
                    if (!validSurrogateNameChar(char3, s.charAt(index2))) {
                        return i;
                    }
                }
                else {
                    index2 = i;
                    if (!XmlChars.is10NameChar(char3)) {
                        return i;
                    }
                }
                i = index2 + 1;
            }
        }
        return -1;
    }
    
    private static boolean validSurrogateNameChar(final char c, final char c2) {
        return false;
    }
}

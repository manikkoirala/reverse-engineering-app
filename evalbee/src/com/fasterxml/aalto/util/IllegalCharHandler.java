// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.util;

public interface IllegalCharHandler
{
    char convertIllegalChar(final int p0);
    
    public static class ReplacingIllegalCharHandler implements IllegalCharHandler, XmlConsts
    {
        private final char replacedChar;
        
        public ReplacingIllegalCharHandler(final char replacedChar) {
            this.replacedChar = replacedChar;
        }
        
        @Override
        public char convertIllegalChar(final int n) {
            return this.replacedChar;
        }
    }
}

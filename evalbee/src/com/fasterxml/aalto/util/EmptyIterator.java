// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.util;

import java.util.NoSuchElementException;
import java.util.Iterator;

public final class EmptyIterator<T> implements Iterator<T>
{
    static final EmptyIterator<Object> sInstance;
    
    static {
        sInstance = new EmptyIterator<Object>();
    }
    
    private EmptyIterator() {
    }
    
    public static <T> EmptyIterator<T> getInstance() {
        return (EmptyIterator<T>)EmptyIterator.sInstance;
    }
    
    @Override
    public boolean hasNext() {
        return false;
    }
    
    @Override
    public T next() {
        throw new NoSuchElementException();
    }
    
    @Override
    public void remove() {
        throw new IllegalStateException();
    }
}

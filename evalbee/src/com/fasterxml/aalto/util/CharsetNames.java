// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.util;

import java.io.OutputStreamWriter;
import java.io.Writer;

public final class CharsetNames implements XmlConsts
{
    public static final String CS_EBCDIC = "EBCDIC";
    public static final String CS_ISO_LATIN1 = "ISO-8859-1";
    public static final String CS_SHIFT_JIS = "Shift_JIS";
    public static final String CS_US_ASCII = "US-ASCII";
    public static final String CS_UTF16 = "UTF-16";
    public static final String CS_UTF16BE = "UTF-16BE";
    public static final String CS_UTF16LE = "UTF-16LE";
    public static final String CS_UTF32 = "UTF-32";
    public static final String CS_UTF32BE = "UTF-32BE";
    public static final String CS_UTF32LE = "UTF-32LE";
    public static final String CS_UTF8 = "UTF-8";
    private static final int EOS = 65536;
    
    public static boolean encodingStartsWith(final String s, final String s2) {
        int n3;
        for (int length = s.length(), length2 = s2.length(), index = 0, index2 = 0; index < length || index2 < length2; index = n3) {
            int char1;
            if (index >= length) {
                char1 = 65536;
            }
            else {
                final int n = index + 1;
                char1 = s.charAt(index);
                index = n;
            }
            int char2;
            if (index2 >= length2) {
                char2 = 65536;
            }
            else {
                final int n2 = index2 + 1;
                char2 = s2.charAt(index2);
                index2 = n2;
            }
            int char3 = char1;
            int index3 = index;
            if (char1 == char2) {
                n3 = index;
            }
            else {
                int char4;
                int index4;
                while (true) {
                    if (char3 > 32 && char3 != 95) {
                        char4 = char2;
                        index4 = index2;
                        if (char3 != 45) {
                            break;
                        }
                    }
                    if (index3 >= length) {
                        char3 = 65536;
                    }
                    else {
                        char3 = s.charAt(index3);
                        ++index3;
                    }
                }
                while (char4 <= 32 || char4 == 95 || char4 == 45) {
                    if (index4 >= length2) {
                        char4 = 65536;
                    }
                    else {
                        char4 = s2.charAt(index4);
                        ++index4;
                    }
                }
                n3 = index3;
                index2 = index4;
                if (char3 != char4) {
                    if (char4 == 65536) {
                        return true;
                    }
                    if (char3 == 65536) {
                        return false;
                    }
                    n3 = index3;
                    index2 = index4;
                    if (Character.toLowerCase((char)char3) != Character.toLowerCase((char)char4)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
    
    public static boolean equalEncodings(final String s, final String s2) {
        int n3;
        for (int length = s.length(), length2 = s2.length(), index = 0, index2 = 0; index < length || index2 < length2; index = n3) {
            int char1;
            if (index >= length) {
                char1 = 65536;
            }
            else {
                final int n = index + 1;
                char1 = s.charAt(index);
                index = n;
            }
            int char2;
            if (index2 >= length2) {
                char2 = 65536;
            }
            else {
                final int n2 = index2 + 1;
                char2 = s2.charAt(index2);
                index2 = n2;
            }
            int char3 = char1;
            int index3 = index;
            if (char1 == char2) {
                n3 = index;
            }
            else {
                int char4;
                int index4;
                while (true) {
                    if (char3 > 32 && char3 != 95) {
                        char4 = char2;
                        index4 = index2;
                        if (char3 != 45) {
                            break;
                        }
                    }
                    if (index3 >= length) {
                        char3 = 65536;
                    }
                    else {
                        char3 = s.charAt(index3);
                        ++index3;
                    }
                }
                while (char4 <= 32 || char4 == 95 || char4 == 45) {
                    if (index4 >= length2) {
                        char4 = 65536;
                    }
                    else {
                        char4 = s2.charAt(index4);
                        ++index4;
                    }
                }
                n3 = index3;
                index2 = index4;
                if (char3 != char4) {
                    if (char3 != 65536) {
                        if (char4 != 65536) {
                            n3 = index3;
                            index2 = index4;
                            if (Character.toLowerCase((char)char3) == Character.toLowerCase((char)char4)) {
                                continue;
                            }
                        }
                    }
                    return false;
                }
            }
        }
        return true;
    }
    
    public static String findEncodingFor(final Writer writer) {
        if (writer instanceof OutputStreamWriter) {
            return normalize(((OutputStreamWriter)writer).getEncoding());
        }
        return null;
    }
    
    public static String normalize(String substring) {
        final String s = substring;
        if (s != null && substring.length() >= 3) {
            final boolean b = false;
            final char char1 = s.charAt(0);
            int n = 0;
            char char2 = '\0';
            Label_0094: {
                if (char1 != 'c') {
                    substring = s;
                    n = (b ? 1 : 0);
                    if ((char2 = char1) != 'C') {
                        break Label_0094;
                    }
                }
                final char char3 = s.charAt(1);
                if (char3 != 's') {
                    substring = s;
                    n = (b ? 1 : 0);
                    char2 = char1;
                    if (char3 != 'S') {
                        break Label_0094;
                    }
                }
                substring = s.substring(2);
                char2 = substring.charAt(0);
                n = 1;
            }
            Label_0629: {
                if (char2 != 'A') {
                    Label_0619: {
                        if (char2 != 'C') {
                            Label_0598: {
                                if (char2 != 'E') {
                                    Label_0586: {
                                        if (char2 != 'S') {
                                            if (char2 != 'U') {
                                                if (char2 == 'a') {
                                                    break Label_0629;
                                                }
                                                if (char2 == 'c') {
                                                    break Label_0619;
                                                }
                                                if (char2 == 'e') {
                                                    break Label_0598;
                                                }
                                                if (char2 == 's') {
                                                    break Label_0586;
                                                }
                                                if (char2 != 'u') {
                                                    Label_0193: {
                                                        if (char2 != 'I') {
                                                            if (char2 != 'J') {
                                                                if (char2 == 'i') {
                                                                    break Label_0193;
                                                                }
                                                                if (char2 != 'j') {
                                                                    return substring;
                                                                }
                                                            }
                                                            if (equalEncodings(substring, "JIS_Encoding")) {
                                                                return "Shift_JIS";
                                                            }
                                                            return substring;
                                                        }
                                                    }
                                                    if (substring == "ISO-8859-1" || equalEncodings(substring, "ISO-8859-1") || equalEncodings(substring, "ISO-Latin1")) {
                                                        return "ISO-8859-1";
                                                    }
                                                    if (!encodingStartsWith(substring, "ISO-10646")) {
                                                        return substring;
                                                    }
                                                    final String substring2 = substring.substring(substring.indexOf("10646") + 5);
                                                    if (equalEncodings(substring2, "UCS-Basic")) {
                                                        return "US-ASCII";
                                                    }
                                                    if (equalEncodings(substring2, "Unicode-Latin1")) {
                                                        return "ISO-8859-1";
                                                    }
                                                    if (equalEncodings(substring2, "UCS-2")) {
                                                        return "UTF-16";
                                                    }
                                                    if (equalEncodings(substring2, "UCS-4")) {
                                                        return "UTF-32";
                                                    }
                                                    if (equalEncodings(substring2, "UTF-1")) {
                                                        return "US-ASCII";
                                                    }
                                                    if (equalEncodings(substring2, "J-1")) {
                                                        return "US-ASCII";
                                                    }
                                                    if (equalEncodings(substring2, "US-ASCII")) {
                                                        return "US-ASCII";
                                                    }
                                                    return substring;
                                                }
                                            }
                                            if (substring.length() < 2) {
                                                return substring;
                                            }
                                            final char char4 = substring.charAt(1);
                                            Label_0562: {
                                                if (char4 != 'C') {
                                                    if (char4 != 'N') {
                                                        if (char4 == 'c') {
                                                            break Label_0562;
                                                        }
                                                        if (char4 != 'n') {
                                                            Label_0510: {
                                                                if (char4 != 'S') {
                                                                    if (char4 != 'T') {
                                                                        if (char4 == 's') {
                                                                            break Label_0510;
                                                                        }
                                                                        if (char4 != 't') {
                                                                            return substring;
                                                                        }
                                                                    }
                                                                    if (substring == "UTF-8" || equalEncodings(substring, "UTF-8")) {
                                                                        return "UTF-8";
                                                                    }
                                                                    if (equalEncodings(substring, "UTF-16BE")) {
                                                                        return "UTF-16BE";
                                                                    }
                                                                    if (equalEncodings(substring, "UTF-16LE")) {
                                                                        return "UTF-16LE";
                                                                    }
                                                                    if (equalEncodings(substring, "UTF-16")) {
                                                                        return "UTF-16";
                                                                    }
                                                                    if (equalEncodings(substring, "UTF-32BE")) {
                                                                        return "UTF-32BE";
                                                                    }
                                                                    if (equalEncodings(substring, "UTF-32LE")) {
                                                                        return "UTF-32LE";
                                                                    }
                                                                    if (equalEncodings(substring, "UTF-32")) {
                                                                        return "UTF-32";
                                                                    }
                                                                    if (equalEncodings(substring, "UTF")) {
                                                                        return "UTF-16";
                                                                    }
                                                                    return substring;
                                                                }
                                                            }
                                                            if (equalEncodings(substring, "US-ASCII")) {
                                                                return "US-ASCII";
                                                            }
                                                            return substring;
                                                        }
                                                    }
                                                    if (n == 0) {
                                                        return substring;
                                                    }
                                                    if (equalEncodings(substring, "Unicode")) {
                                                        return "UTF-16";
                                                    }
                                                    if (equalEncodings(substring, "UnicodeAscii")) {
                                                        return "ISO-8859-1";
                                                    }
                                                    if (equalEncodings(substring, "UnicodeAscii")) {
                                                        return "US-ASCII";
                                                    }
                                                    return substring;
                                                }
                                            }
                                            if (equalEncodings(substring, "UCS-2")) {
                                                return "UTF-16";
                                            }
                                            if (equalEncodings(substring, "UCS-4")) {
                                                return "UTF-32";
                                            }
                                            return substring;
                                        }
                                    }
                                    if (equalEncodings(substring, "Shift_JIS")) {
                                        return "Shift_JIS";
                                    }
                                    return substring;
                                }
                            }
                            if (substring.startsWith("EBCDIC") || substring.startsWith("ebcdic")) {
                                return "EBCDIC";
                            }
                            return substring;
                        }
                    }
                    encodingStartsWith(substring, "cs");
                    return substring;
                }
            }
            if (substring == "ASCII" || equalEncodings(substring, "ASCII")) {
                return "US-ASCII";
            }
            return substring;
        }
        return s;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.util;

public class XmlCharTypes
{
    public static final int CT_AMP = 10;
    public static final int CT_ATTR_QUOTE = 14;
    public static final int CT_DTD_GT = 10;
    public static final int CT_DTD_LT = 9;
    public static final int CT_DTD_PERCENT = 12;
    public static final int CT_DTD_QUOTE = 8;
    public static final int CT_DTD_RBRACKET = 11;
    public static final int CT_GT = 17;
    public static final int CT_HYPHEN = 13;
    public static final int CT_INVALID = 1;
    public static final int CT_LBRACKET = 16;
    public static final int CT_LT = 9;
    public static final int CT_MULTIBYTE_2 = 5;
    public static final int CT_MULTIBYTE_3 = 6;
    public static final int CT_MULTIBYTE_4 = 7;
    public static final int CT_MULTIBYTE_N = 4;
    public static final int CT_NAME_ANY = 3;
    public static final int CT_NAME_COLON = 1;
    public static final int CT_NAME_NONE = 0;
    public static final int CT_NAME_NONFIRST = 2;
    public static final int CT_OK = 0;
    public static final int CT_QMARK = 12;
    public static final int CT_RBRACKET = 11;
    public static final int CT_WS_CR = 2;
    public static final int CT_WS_LF = 3;
    public static final int CT_WS_TAB = 8;
    public static final int[] PUBID_CHARS;
    public static final int PUBID_INVALID = 0;
    public static final int PUBID_OK = 1;
    public final int[] ATTR_CHARS;
    public final int[] DTD_CHARS;
    public final int[] NAME_CHARS;
    public final int[] OTHER_CHARS;
    public final int[] TEXT_CHARS;
    
    static {
        PUBID_CHARS = new int[256];
        for (int i = 0; i <= 25; ++i) {
            final int[] pubid_CHARS = XmlCharTypes.PUBID_CHARS;
            pubid_CHARS[i + 97] = (pubid_CHARS[i + 65] = 1);
        }
        for (int j = 48; j <= 57; ++j) {
            XmlCharTypes.PUBID_CHARS[j] = 1;
        }
        final int[] pubid_CHARS2 = XmlCharTypes.PUBID_CHARS;
        pubid_CHARS2[13] = (pubid_CHARS2[10] = 1);
        pubid_CHARS2[45] = (pubid_CHARS2[32] = 1);
        pubid_CHARS2[40] = (pubid_CHARS2[39] = 1);
        pubid_CHARS2[43] = (pubid_CHARS2[41] = 1);
        pubid_CHARS2[46] = (pubid_CHARS2[44] = 1);
        pubid_CHARS2[58] = (pubid_CHARS2[47] = 1);
        pubid_CHARS2[63] = (pubid_CHARS2[61] = 1);
        pubid_CHARS2[33] = (pubid_CHARS2[59] = 1);
        pubid_CHARS2[35] = (pubid_CHARS2[42] = 1);
        pubid_CHARS2[36] = (pubid_CHARS2[64] = 1);
        pubid_CHARS2[37] = (pubid_CHARS2[95] = 1);
    }
    
    public XmlCharTypes() {
        this(256);
    }
    
    public XmlCharTypes(final int n) {
        this.TEXT_CHARS = new int[n];
        this.ATTR_CHARS = new int[n];
        this.NAME_CHARS = new int[n];
        this.DTD_CHARS = new int[n];
        this.OTHER_CHARS = new int[n];
    }
    
    public static void fillIn8BitAttrRange(final int[] array) {
        fillInCommonTextRange(array);
        array[9] = 8;
        array[60] = 9;
        array[38] = 10;
        array[34] = (array[39] = 14);
    }
    
    public static void fillIn8BitDtdRange(final int[] array) {
        fillInCommonTextRange(array);
        array[34] = (array[39] = 8);
        array[60] = 9;
        array[62] = 10;
        array[93] = 11;
        array[37] = 12;
    }
    
    public static void fillIn8BitNameRange(final int[] array) {
        for (int i = 97; i <= 122; ++i) {
            array[i] = 3;
        }
        for (int j = 65; j <= 90; ++j) {
            array[j] = 3;
        }
        array[95] = 3;
        array[58] = 1;
        array[46] = (array[45] = 2);
        for (int k = 48; k <= 57; ++k) {
            array[k] = 2;
        }
    }
    
    public static void fillIn8BitTextRange(final int[] array) {
        fillInCommonTextRange(array);
        array[60] = 9;
        array[38] = 10;
        array[93] = 11;
    }
    
    private static void fillInCommonTextRange(final int[] array) {
        for (int i = 0; i < 32; ++i) {
            array[i] = 1;
        }
        array[13] = 2;
        array[10] = 3;
        array[9] = 0;
    }
    
    public static void fillInLatin1Chars(final int[] array, final int[] array2, final int[] array3, final int[] array4, final int[] array5) {
        fillIn8BitTextRange(array);
        fillIn8BitAttrRange(array2);
        fillIn8BitNameRange(array3);
        for (int i = 192; i <= 255; ++i) {
            if (i != 215 && i != 247) {
                array3[i] = 3;
            }
        }
        array3[183] = 2;
        fillIn8BitDtdRange(array4);
        fillIn8BitTextRange(array5);
        array5[60] = (array5[38] = 0);
        array5[93] = 11;
        array5[63] = 12;
        array5[45] = 13;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.util;

public final class XmlChars
{
    static final int SIZE = 394;
    static final int[] sXml10Chars;
    static final int[] sXml10StartChars;
    
    static {
        final int[] sXml10StartChars2 = new int[394];
        SETBITS(sXml10StartChars = sXml10StartChars2, 65, 90);
        SETBITS(sXml10StartChars2, 95);
        SETBITS(sXml10StartChars2, 97, 122);
        SETBITS(sXml10StartChars2, 192, 214);
        SETBITS(sXml10StartChars2, 216, 246);
        SETBITS(sXml10StartChars2, 248, 255);
        SETBITS(sXml10StartChars2, 256, 305);
        SETBITS(sXml10StartChars2, 308, 318);
        SETBITS(sXml10StartChars2, 321, 328);
        SETBITS(sXml10StartChars2, 330, 382);
        SETBITS(sXml10StartChars2, 384, 451);
        SETBITS(sXml10StartChars2, 461, 496);
        SETBITS(sXml10StartChars2, 500, 501);
        SETBITS(sXml10StartChars2, 506, 535);
        SETBITS(sXml10StartChars2, 592, 680);
        SETBITS(sXml10StartChars2, 699, 705);
        SETBITS(sXml10StartChars2, 902);
        SETBITS(sXml10StartChars2, 904, 906);
        SETBITS(sXml10StartChars2, 908);
        SETBITS(sXml10StartChars2, 910, 929);
        SETBITS(sXml10StartChars2, 931, 974);
        SETBITS(sXml10StartChars2, 976, 982);
        SETBITS(sXml10StartChars2, 986);
        SETBITS(sXml10StartChars2, 988);
        SETBITS(sXml10StartChars2, 990);
        SETBITS(sXml10StartChars2, 992);
        SETBITS(sXml10StartChars2, 994, 1011);
        SETBITS(sXml10StartChars2, 1025, 1036);
        SETBITS(sXml10StartChars2, 1038, 1103);
        SETBITS(sXml10StartChars2, 1105, 1116);
        SETBITS(sXml10StartChars2, 1118, 1153);
        SETBITS(sXml10StartChars2, 1168, 1220);
        SETBITS(sXml10StartChars2, 1223, 1224);
        SETBITS(sXml10StartChars2, 1227, 1228);
        SETBITS(sXml10StartChars2, 1232, 1259);
        SETBITS(sXml10StartChars2, 1262, 1269);
        SETBITS(sXml10StartChars2, 1272, 1273);
        SETBITS(sXml10StartChars2, 1329, 1366);
        SETBITS(sXml10StartChars2, 1369);
        SETBITS(sXml10StartChars2, 1377, 1414);
        SETBITS(sXml10StartChars2, 1488, 1514);
        SETBITS(sXml10StartChars2, 1520, 1522);
        SETBITS(sXml10StartChars2, 1569, 1594);
        SETBITS(sXml10StartChars2, 1601, 1610);
        SETBITS(sXml10StartChars2, 1649, 1719);
        SETBITS(sXml10StartChars2, 1722, 1726);
        SETBITS(sXml10StartChars2, 1728, 1742);
        SETBITS(sXml10StartChars2, 1744, 1747);
        SETBITS(sXml10StartChars2, 1749);
        SETBITS(sXml10StartChars2, 1765, 1766);
        SETBITS(sXml10StartChars2, 2309, 2361);
        SETBITS(sXml10StartChars2, 2365);
        SETBITS(sXml10StartChars2, 2392, 2401);
        SETBITS(sXml10StartChars2, 2437, 2444);
        SETBITS(sXml10StartChars2, 2447, 2448);
        SETBITS(sXml10StartChars2, 2451, 2472);
        SETBITS(sXml10StartChars2, 2474, 2480);
        SETBITS(sXml10StartChars2, 2482);
        SETBITS(sXml10StartChars2, 2486, 2489);
        SETBITS(sXml10StartChars2, 2524);
        SETBITS(sXml10StartChars2, 2525);
        SETBITS(sXml10StartChars2, 2527, 2529);
        SETBITS(sXml10StartChars2, 2544);
        SETBITS(sXml10StartChars2, 2545);
        SETBITS(sXml10StartChars2, 2565, 2570);
        SETBITS(sXml10StartChars2, 2575);
        SETBITS(sXml10StartChars2, 2576);
        SETBITS(sXml10StartChars2, 2579, 2600);
        SETBITS(sXml10StartChars2, 2602, 2608);
        SETBITS(sXml10StartChars2, 2610);
        SETBITS(sXml10StartChars2, 2611);
        SETBITS(sXml10StartChars2, 2613);
        SETBITS(sXml10StartChars2, 2614);
        SETBITS(sXml10StartChars2, 2616);
        SETBITS(sXml10StartChars2, 2617);
        SETBITS(sXml10StartChars2, 2649, 2652);
        SETBITS(sXml10StartChars2, 2654);
        SETBITS(sXml10StartChars2, 2674, 2676);
        SETBITS(sXml10StartChars2, 2693, 2699);
        SETBITS(sXml10StartChars2, 2701);
        SETBITS(sXml10StartChars2, 2703, 2705);
        SETBITS(sXml10StartChars2, 2707, 2728);
        SETBITS(sXml10StartChars2, 2730, 2736);
        SETBITS(sXml10StartChars2, 2738, 2739);
        SETBITS(sXml10StartChars2, 2741, 2745);
        SETBITS(sXml10StartChars2, 2749);
        SETBITS(sXml10StartChars2, 2784);
        SETBITS(sXml10StartChars2, 2821, 2828);
        SETBITS(sXml10StartChars2, 2831);
        SETBITS(sXml10StartChars2, 2832);
        SETBITS(sXml10StartChars2, 2835, 2856);
        SETBITS(sXml10StartChars2, 2858, 2864);
        SETBITS(sXml10StartChars2, 2866);
        SETBITS(sXml10StartChars2, 2867);
        SETBITS(sXml10StartChars2, 2870, 2873);
        SETBITS(sXml10StartChars2, 2877);
        SETBITS(sXml10StartChars2, 2908);
        SETBITS(sXml10StartChars2, 2909);
        SETBITS(sXml10StartChars2, 2911, 2913);
        SETBITS(sXml10StartChars2, 2949, 2954);
        SETBITS(sXml10StartChars2, 2958, 2960);
        SETBITS(sXml10StartChars2, 2962, 2965);
        SETBITS(sXml10StartChars2, 2969, 2970);
        SETBITS(sXml10StartChars2, 2972);
        SETBITS(sXml10StartChars2, 2974);
        SETBITS(sXml10StartChars2, 2975);
        SETBITS(sXml10StartChars2, 2979);
        SETBITS(sXml10StartChars2, 2980);
        SETBITS(sXml10StartChars2, 2984, 2986);
        SETBITS(sXml10StartChars2, 2990, 2997);
        SETBITS(sXml10StartChars2, 2999, 3001);
        SETBITS(sXml10StartChars2, 3077, 3084);
        SETBITS(sXml10StartChars2, 3086, 3088);
        SETBITS(sXml10StartChars2, 3090, 3112);
        SETBITS(sXml10StartChars2, 3114, 3123);
        SETBITS(sXml10StartChars2, 3125, 3129);
        SETBITS(sXml10StartChars2, 3168);
        SETBITS(sXml10StartChars2, 3169);
        SETBITS(sXml10StartChars2, 3205, 3212);
        SETBITS(sXml10StartChars2, 3214, 3216);
        SETBITS(sXml10StartChars2, 3218, 3240);
        SETBITS(sXml10StartChars2, 3242, 3251);
        SETBITS(sXml10StartChars2, 3253, 3257);
        SETBITS(sXml10StartChars2, 3294);
        SETBITS(sXml10StartChars2, 3296);
        SETBITS(sXml10StartChars2, 3297);
        SETBITS(sXml10StartChars2, 3333, 3340);
        SETBITS(sXml10StartChars2, 3342, 3344);
        SETBITS(sXml10StartChars2, 3346, 3368);
        SETBITS(sXml10StartChars2, 3370, 3385);
        SETBITS(sXml10StartChars2, 3424);
        SETBITS(sXml10StartChars2, 3425);
        SETBITS(sXml10StartChars2, 3585, 3630);
        SETBITS(sXml10StartChars2, 3632);
        SETBITS(sXml10StartChars2, 3634);
        SETBITS(sXml10StartChars2, 3635);
        SETBITS(sXml10StartChars2, 3648, 3653);
        SETBITS(sXml10StartChars2, 3713);
        SETBITS(sXml10StartChars2, 3714);
        SETBITS(sXml10StartChars2, 3716);
        SETBITS(sXml10StartChars2, 3719);
        SETBITS(sXml10StartChars2, 3720);
        SETBITS(sXml10StartChars2, 3722);
        SETBITS(sXml10StartChars2, 3725);
        SETBITS(sXml10StartChars2, 3732, 3735);
        SETBITS(sXml10StartChars2, 3737, 3743);
        SETBITS(sXml10StartChars2, 3745, 3747);
        SETBITS(sXml10StartChars2, 3749);
        SETBITS(sXml10StartChars2, 3751);
        SETBITS(sXml10StartChars2, 3754);
        SETBITS(sXml10StartChars2, 3755);
        SETBITS(sXml10StartChars2, 3757);
        SETBITS(sXml10StartChars2, 3758);
        SETBITS(sXml10StartChars2, 3760);
        SETBITS(sXml10StartChars2, 3762);
        SETBITS(sXml10StartChars2, 3763);
        SETBITS(sXml10StartChars2, 3773);
        SETBITS(sXml10StartChars2, 3776, 3780);
        SETBITS(sXml10StartChars2, 3904, 3911);
        SETBITS(sXml10StartChars2, 3913, 3945);
        SETBITS(sXml10StartChars2, 4256, 4293);
        SETBITS(sXml10StartChars2, 4304, 4342);
        SETBITS(sXml10StartChars2, 4352);
        SETBITS(sXml10StartChars2, 4354, 4355);
        SETBITS(sXml10StartChars2, 4357, 4359);
        SETBITS(sXml10StartChars2, 4361);
        SETBITS(sXml10StartChars2, 4363, 4364);
        SETBITS(sXml10StartChars2, 4366, 4370);
        SETBITS(sXml10StartChars2, 4412);
        SETBITS(sXml10StartChars2, 4414);
        SETBITS(sXml10StartChars2, 4416);
        SETBITS(sXml10StartChars2, 4428);
        SETBITS(sXml10StartChars2, 4430);
        SETBITS(sXml10StartChars2, 4432);
        SETBITS(sXml10StartChars2, 4436, 4437);
        SETBITS(sXml10StartChars2, 4441);
        SETBITS(sXml10StartChars2, 4447, 4449);
        SETBITS(sXml10StartChars2, 4451);
        SETBITS(sXml10StartChars2, 4453);
        SETBITS(sXml10StartChars2, 4455);
        SETBITS(sXml10StartChars2, 4457);
        SETBITS(sXml10StartChars2, 4461, 4462);
        SETBITS(sXml10StartChars2, 4466, 4467);
        SETBITS(sXml10StartChars2, 4469);
        SETBITS(sXml10StartChars2, 4510);
        SETBITS(sXml10StartChars2, 4520);
        SETBITS(sXml10StartChars2, 4523);
        SETBITS(sXml10StartChars2, 4526, 4527);
        SETBITS(sXml10StartChars2, 4535, 4536);
        SETBITS(sXml10StartChars2, 4538);
        SETBITS(sXml10StartChars2, 4540, 4546);
        SETBITS(sXml10StartChars2, 4587);
        SETBITS(sXml10StartChars2, 4592);
        SETBITS(sXml10StartChars2, 4601);
        SETBITS(sXml10StartChars2, 7680, 7835);
        SETBITS(sXml10StartChars2, 7840, 7929);
        SETBITS(sXml10StartChars2, 7936, 7957);
        SETBITS(sXml10StartChars2, 7960, 7965);
        SETBITS(sXml10StartChars2, 7968, 8005);
        SETBITS(sXml10StartChars2, 8008, 8013);
        SETBITS(sXml10StartChars2, 8016, 8023);
        SETBITS(sXml10StartChars2, 8025);
        SETBITS(sXml10StartChars2, 8027);
        SETBITS(sXml10StartChars2, 8029);
        SETBITS(sXml10StartChars2, 8031, 8061);
        SETBITS(sXml10StartChars2, 8064, 8116);
        SETBITS(sXml10StartChars2, 8118, 8124);
        SETBITS(sXml10StartChars2, 8126);
        SETBITS(sXml10StartChars2, 8130, 8132);
        SETBITS(sXml10StartChars2, 8134, 8140);
        SETBITS(sXml10StartChars2, 8144, 8147);
        SETBITS(sXml10StartChars2, 8150, 8155);
        SETBITS(sXml10StartChars2, 8160, 8172);
        SETBITS(sXml10StartChars2, 8178, 8180);
        SETBITS(sXml10StartChars2, 8182, 8188);
        SETBITS(sXml10StartChars2, 8486);
        SETBITS(sXml10StartChars2, 8490, 8491);
        SETBITS(sXml10StartChars2, 8494);
        SETBITS(sXml10StartChars2, 8576, 8578);
        SETBITS(sXml10StartChars2, 12353, 12436);
        SETBITS(sXml10StartChars2, 12449, 12538);
        SETBITS(sXml10StartChars2, 12549, 12588);
        SETBITS(sXml10StartChars2, 12295);
        SETBITS(sXml10StartChars2, 12321, 12329);
        final int[] sXml10Chars2 = new int[394];
        System.arraycopy(sXml10StartChars2, 0, sXml10Chars = sXml10Chars2, 0, 394);
        SETBITS(sXml10Chars2, 45, 46);
        SETBITS(sXml10Chars2, 48, 57);
        SETBITS(sXml10Chars2, 183);
        SETBITS(sXml10Chars2, 768, 837);
        SETBITS(sXml10Chars2, 864, 865);
        SETBITS(sXml10Chars2, 1155, 1158);
        SETBITS(sXml10Chars2, 1425, 1441);
        SETBITS(sXml10Chars2, 1443, 1465);
        SETBITS(sXml10Chars2, 1467, 1469);
        SETBITS(sXml10Chars2, 1471);
        SETBITS(sXml10Chars2, 1473, 1474);
        SETBITS(sXml10Chars2, 1476);
        SETBITS(sXml10Chars2, 1611, 1618);
        SETBITS(sXml10Chars2, 1648);
        SETBITS(sXml10Chars2, 1750, 1756);
        SETBITS(sXml10Chars2, 1757, 1759);
        SETBITS(sXml10Chars2, 1760, 1764);
        SETBITS(sXml10Chars2, 1767, 1768);
        SETBITS(sXml10Chars2, 1770, 1773);
        SETBITS(sXml10Chars2, 2305, 2307);
        SETBITS(sXml10Chars2, 2364);
        SETBITS(sXml10Chars2, 2366, 2380);
        SETBITS(sXml10Chars2, 2381);
        SETBITS(sXml10Chars2, 2385, 2388);
        SETBITS(sXml10Chars2, 2402);
        SETBITS(sXml10Chars2, 2403);
        SETBITS(sXml10Chars2, 2433, 2435);
        SETBITS(sXml10Chars2, 2492);
        SETBITS(sXml10Chars2, 2494);
        SETBITS(sXml10Chars2, 2495);
        SETBITS(sXml10Chars2, 2496, 2500);
        SETBITS(sXml10Chars2, 2503);
        SETBITS(sXml10Chars2, 2504);
        SETBITS(sXml10Chars2, 2507, 2509);
        SETBITS(sXml10Chars2, 2519);
        SETBITS(sXml10Chars2, 2530);
        SETBITS(sXml10Chars2, 2531);
        SETBITS(sXml10Chars2, 2562);
        SETBITS(sXml10Chars2, 2620);
        SETBITS(sXml10Chars2, 2622);
        SETBITS(sXml10Chars2, 2623);
        SETBITS(sXml10Chars2, 2624, 2626);
        SETBITS(sXml10Chars2, 2631);
        SETBITS(sXml10Chars2, 2632);
        SETBITS(sXml10Chars2, 2635, 2637);
        SETBITS(sXml10Chars2, 2672);
        SETBITS(sXml10Chars2, 2673);
        SETBITS(sXml10Chars2, 2689, 2691);
        SETBITS(sXml10Chars2, 2748);
        SETBITS(sXml10Chars2, 2750, 2757);
        SETBITS(sXml10Chars2, 2759, 2761);
        SETBITS(sXml10Chars2, 2763, 2765);
        SETBITS(sXml10Chars2, 2817, 2819);
        SETBITS(sXml10Chars2, 2876);
        SETBITS(sXml10Chars2, 2878, 2883);
        SETBITS(sXml10Chars2, 2887);
        SETBITS(sXml10Chars2, 2888);
        SETBITS(sXml10Chars2, 2891, 2893);
        SETBITS(sXml10Chars2, 2902);
        SETBITS(sXml10Chars2, 2903);
        SETBITS(sXml10Chars2, 2946);
        SETBITS(sXml10Chars2, 2947);
        SETBITS(sXml10Chars2, 3006, 3010);
        SETBITS(sXml10Chars2, 3014, 3016);
        SETBITS(sXml10Chars2, 3018, 3021);
        SETBITS(sXml10Chars2, 3031);
        SETBITS(sXml10Chars2, 3073, 3075);
        SETBITS(sXml10Chars2, 3134, 3140);
        SETBITS(sXml10Chars2, 3142, 3144);
        SETBITS(sXml10Chars2, 3146, 3149);
        SETBITS(sXml10Chars2, 3157, 3158);
        SETBITS(sXml10Chars2, 3202, 3203);
        SETBITS(sXml10Chars2, 3262, 3268);
        SETBITS(sXml10Chars2, 3270, 3272);
        SETBITS(sXml10Chars2, 3274, 3277);
        SETBITS(sXml10Chars2, 3285, 3286);
        SETBITS(sXml10Chars2, 3330, 3331);
        SETBITS(sXml10Chars2, 3390, 3395);
        SETBITS(sXml10Chars2, 3398, 3400);
        SETBITS(sXml10Chars2, 3402, 3405);
        SETBITS(sXml10Chars2, 3415);
        SETBITS(sXml10Chars2, 3633);
        SETBITS(sXml10Chars2, 3636, 3642);
        SETBITS(sXml10Chars2, 3655, 3662);
        SETBITS(sXml10Chars2, 3761);
        SETBITS(sXml10Chars2, 3764, 3769);
        SETBITS(sXml10Chars2, 3771, 3772);
        SETBITS(sXml10Chars2, 3784, 3789);
        SETBITS(sXml10Chars2, 3864, 3865);
        SETBITS(sXml10Chars2, 3893);
        SETBITS(sXml10Chars2, 3895);
        SETBITS(sXml10Chars2, 3897);
        SETBITS(sXml10Chars2, 3902);
        SETBITS(sXml10Chars2, 3903);
        SETBITS(sXml10Chars2, 3953, 3972);
        SETBITS(sXml10Chars2, 3974, 3979);
        SETBITS(sXml10Chars2, 3984, 3989);
        SETBITS(sXml10Chars2, 3991);
        SETBITS(sXml10Chars2, 3993, 4013);
        SETBITS(sXml10Chars2, 4017, 4023);
        SETBITS(sXml10Chars2, 4025);
        SETBITS(sXml10Chars2, 8400, 8412);
        SETBITS(sXml10Chars2, 8417);
        SETBITS(sXml10Chars2, 12330, 12335);
        SETBITS(sXml10Chars2, 12441);
        SETBITS(sXml10Chars2, 12442);
        SETBITS(sXml10Chars2, 1632, 1641);
        SETBITS(sXml10Chars2, 1776, 1785);
        SETBITS(sXml10Chars2, 2406, 2415);
        SETBITS(sXml10Chars2, 2534, 2543);
        SETBITS(sXml10Chars2, 2662, 2671);
        SETBITS(sXml10Chars2, 2790, 2799);
        SETBITS(sXml10Chars2, 2918, 2927);
        SETBITS(sXml10Chars2, 3047, 3055);
        SETBITS(sXml10Chars2, 3174, 3183);
        SETBITS(sXml10Chars2, 3302, 3311);
        SETBITS(sXml10Chars2, 3430, 3439);
        SETBITS(sXml10Chars2, 3664, 3673);
        SETBITS(sXml10Chars2, 3792, 3801);
        SETBITS(sXml10Chars2, 3872, 3881);
        SETBITS(sXml10Chars2, 183);
        SETBITS(sXml10Chars2, 720);
        SETBITS(sXml10Chars2, 721);
        SETBITS(sXml10Chars2, 903);
        SETBITS(sXml10Chars2, 1600);
        SETBITS(sXml10Chars2, 3654);
        SETBITS(sXml10Chars2, 3782);
        SETBITS(sXml10Chars2, 12293);
        SETBITS(sXml10Chars2, 12337, 12341);
        SETBITS(sXml10Chars2, 12445, 12446);
        SETBITS(sXml10Chars2, 12540, 12542);
    }
    
    private XmlChars() {
    }
    
    private static void SETBITS(final int[] array, final int n) {
        final int n2 = n >> 5;
        array[n2] |= 1 << (n & 0x1F);
    }
    
    private static void SETBITS(final int[] array, int i, int n) {
        int j = i & 0x1F;
        final int n2 = n & 0x1F;
        final int n3 = i >> 5;
        final int n4 = n >> 5;
        i = j;
        if (n3 == n4) {
            while (j <= n2) {
                array[n3] |= 1 << j;
                ++j;
            }
        }
        else {
            while (true) {
                n = n3;
                if (i > 31) {
                    break;
                }
                array[n3] |= 1 << i;
                ++i;
            }
            while (++n < n4) {
                array[n] = -1;
            }
            for (i = 0; i <= n2; ++i) {
                array[n4] |= 1 << i;
            }
        }
    }
    
    public static String getCharDesc(final int n) {
        final char c = (char)n;
        if (Character.isISOControl(c)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("(CTRL-CHAR, code ");
            sb.append(n);
            sb.append(")");
            return sb.toString();
        }
        if (n > 255) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("'");
            sb2.append(c);
            sb2.append("' (code ");
            sb2.append(n);
            sb2.append(" / 0x");
            sb2.append(Integer.toHexString(n));
            sb2.append(")");
            return sb2.toString();
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("'");
        sb3.append(c);
        sb3.append("' (code ");
        sb3.append(n);
        sb3.append(")");
        return sb3.toString();
    }
    
    public static final boolean is10NameChar(final int n) {
        boolean b = false;
        final boolean b2 = false;
        if (n <= 12588) {
            if ((1 << (n & 0x1F) & XmlChars.sXml10Chars[n >> 5]) != 0x0) {
                b = true;
            }
            return b;
        }
        if (n < 44032) {
            boolean b3 = b2;
            if (n >= 19968) {
                b3 = b2;
                if (n <= 40869) {
                    b3 = true;
                }
            }
            return b3;
        }
        return n <= 55203;
    }
    
    public static final boolean is10NameStartChar(final int n) {
        boolean b = false;
        final boolean b2 = false;
        if (n <= 12588) {
            if ((1 << (n & 0x1F) & XmlChars.sXml10StartChars[n >> 5]) != 0x0) {
                b = true;
            }
            return b;
        }
        if (n < 44032) {
            boolean b3 = b2;
            if (n >= 19968) {
                b3 = b2;
                if (n <= 40869) {
                    b3 = true;
                }
            }
            return b3;
        }
        return n <= 55203;
    }
    
    public static final boolean is11NameChar(final int n) {
        final boolean b = false;
        final boolean b2 = false;
        final boolean b3 = false;
        boolean b4 = false;
        if (n > 12271) {
            boolean b5 = b3;
            if (n >= 12289) {
                if (n <= 55295) {
                    return true;
                }
                b5 = b3;
                if (n >= 63744) {
                    if (n <= 65533) {
                        if (n > 64975) {
                            final boolean b6 = b2;
                            if (n < 65008) {
                                return b6;
                            }
                        }
                        return true;
                    }
                    b5 = b3;
                    if (n > 65535) {
                        b5 = b3;
                        if (n <= 983039) {
                            b5 = true;
                        }
                    }
                }
            }
            return b5;
        }
        if (n < 8192) {
            if ((n >= 192 && n != 894) || n == 183) {
                b4 = true;
            }
            return b4;
        }
        if (n >= 11264) {
            return true;
        }
        boolean b7 = b;
        if (n >= 8204) {
            if (n > 8591) {
                b7 = b;
            }
            else {
                if (n >= 8304) {
                    return true;
                }
                if (n != 8204 && n != 8205 && n != 8255) {
                    b7 = b;
                    if (n != 8256) {
                        return b7;
                    }
                }
                b7 = true;
            }
        }
        return b7;
    }
    
    public static final boolean is11NameStartChar(final int n) {
        final boolean b = true;
        boolean b2 = true;
        final boolean b3 = true;
        final boolean b4 = true;
        final boolean b5 = true;
        final boolean b6 = true;
        if (n > 12271) {
            if (n >= 12289) {
                if (n <= 55295) {
                    return true;
                }
                if (n >= 63744) {
                    if (n <= 65533) {
                        boolean b7 = b4;
                        if (n > 64975) {
                            b7 = (n >= 65008 && b4);
                        }
                        return b7;
                    }
                    return n > 65535 && n <= 983039 && b5;
                }
            }
            return false;
        }
        if (n < 768) {
            return n >= 192 && n != 215 && n != 247 && b6;
        }
        if (n >= 11264) {
            return true;
        }
        if (n < 880 || n > 8591) {
            return false;
        }
        if (n < 8192) {
            return n != 894 && b;
        }
        if (n >= 8304) {
            if (n > 8591) {
                b2 = false;
            }
            return b2;
        }
        boolean b8 = b3;
        if (n != 8204) {
            b8 = (n == 8205 && b3);
        }
        return b8;
    }
}

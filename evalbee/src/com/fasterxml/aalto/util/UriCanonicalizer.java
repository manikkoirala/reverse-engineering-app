// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.util;

import java.util.AbstractMap;
import java.util.Map;
import java.util.LinkedHashMap;

public final class UriCanonicalizer
{
    private BoundedHashMap mURIs;
    
    public UriCanonicalizer() {
        this.mURIs = null;
    }
    
    private void init() {
        this.mURIs = new BoundedHashMap();
    }
    
    public String canonicalizeURI(final char[] value, final int count) {
        synchronized (this) {
            final CanonicalKey key = new CanonicalKey(value, count);
            final BoundedHashMap murIs = this.mURIs;
            if (murIs == null) {
                this.init();
            }
            else {
                final String s = ((AbstractMap<K, String>)murIs).get(key);
                if (s != null) {
                    return s;
                }
            }
            final CanonicalKey safeClone = key.safeClone();
            final String intern = new String(value, 0, count).intern();
            this.mURIs.put(safeClone, intern);
            return intern;
        }
    }
    
    public static final class BoundedHashMap extends LinkedHashMap<CanonicalKey, String>
    {
        private static final int DEFAULT_SIZE = 64;
        private static final int MAX_SIZE = 716;
        
        public BoundedHashMap() {
            super(64, 0.7f, true);
        }
        
        public boolean removeEldestEntry(final Map.Entry<CanonicalKey, String> entry) {
            return this.size() >= 716;
        }
    }
    
    public static final class CanonicalKey
    {
        final char[] mChars;
        final int mHash;
        final int mLength;
        
        public CanonicalKey(final char[] mChars, final int mLength) {
            this.mChars = mChars;
            this.mLength = mLength;
            this.mHash = calcKeyHash(mChars, mLength);
        }
        
        public CanonicalKey(final char[] mChars, final int mLength, final int mHash) {
            this.mChars = mChars;
            this.mLength = mLength;
            this.mHash = mHash;
        }
        
        public static int calcKeyHash(final char[] array, int n) {
            int i = 1;
            if (n <= 8) {
                int n2 = array[0];
                while (i < n) {
                    n2 = n2 * 31 + array[i];
                    ++i;
                }
                return n2;
            }
            int n3 = array[0] ^ n;
            final int n4 = n - 4;
            int n5 = 2;
            n = 2;
            int n6;
            while (true) {
                n6 = n3 * 31;
                if (n5 >= n4) {
                    break;
                }
                n3 = n6 + array[n5];
                n5 += n;
                ++n;
            }
            n = array[n4];
            return array[n4 + 3] ^ (n6 ^ (n << 2) + array[n4 + 1]) * 31 + (array[n4 + 2] << 2);
        }
        
        @Override
        public boolean equals(final Object o) {
            if (o == this) {
                return true;
            }
            if (o == null) {
                return false;
            }
            if (o.getClass() != CanonicalKey.class) {
                return false;
            }
            final CanonicalKey canonicalKey = (CanonicalKey)o;
            final int mLength = canonicalKey.mLength;
            final int mLength2 = this.mLength;
            if (mLength != mLength2) {
                return false;
            }
            final char[] mChars = this.mChars;
            final char[] mChars2 = canonicalKey.mChars;
            for (int i = 0; i < mLength2; ++i) {
                if (mChars[i] != mChars2[i]) {
                    return false;
                }
            }
            return true;
        }
        
        @Override
        public int hashCode() {
            return this.mHash;
        }
        
        public CanonicalKey safeClone() {
            final int mLength = this.mLength;
            final char[] array = new char[mLength];
            System.arraycopy(this.mChars, 0, array, 0, mLength);
            return new CanonicalKey(array, this.mLength, this.mHash);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("{URI, hash: 0x");
            sb.append(Integer.toHexString(this.mHash));
            sb.append("}");
            return sb.toString();
        }
    }
}

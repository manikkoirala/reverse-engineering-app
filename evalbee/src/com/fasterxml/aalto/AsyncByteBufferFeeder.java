// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto;

import java.nio.ByteBuffer;

public interface AsyncByteBufferFeeder extends AsyncInputFeeder
{
    void feedInput(final ByteBuffer p0);
}

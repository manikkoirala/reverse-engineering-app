// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.impl;

import java.io.IOException;

public class IoStreamException extends StreamExceptionBase
{
    public IoStreamException(final IOException ex) {
        super(ex);
    }
    
    public IoStreamException(final String s) {
        super(s);
    }
}

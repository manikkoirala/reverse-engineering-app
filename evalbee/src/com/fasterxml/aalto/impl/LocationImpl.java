// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.impl;

import org.codehaus.stax2.XMLStreamLocation2;

public class LocationImpl implements XMLStreamLocation2
{
    private static final LocationImpl EMPTY;
    protected final int _charOffset;
    protected final int _col;
    protected transient String _desc;
    protected final String _publicId;
    protected final int _row;
    protected final String _systemId;
    
    static {
        EMPTY = new LocationImpl("", "", -1, -1, -1);
    }
    
    public LocationImpl(final String publicId, final String systemId, final int n, final int row, final int col) {
        this._desc = null;
        this._publicId = publicId;
        this._systemId = systemId;
        int charOffset = n;
        if (n < 0) {
            charOffset = Integer.MAX_VALUE;
        }
        this._charOffset = charOffset;
        this._col = col;
        this._row = row;
    }
    
    private void appendDesc(final StringBuffer sb) {
        String str;
        if (this._systemId != null) {
            sb.append("[row,col,system-id]: ");
            str = this._systemId;
        }
        else if (this._publicId != null) {
            sb.append("[row,col,public-id]: ");
            str = this._publicId;
        }
        else {
            sb.append("[row,col {unknown-source}]: ");
            str = null;
        }
        sb.append('[');
        sb.append(this._row);
        sb.append(',');
        sb.append(this._col);
        if (str != null) {
            sb.append(',');
            sb.append('\"');
            sb.append(str);
            sb.append('\"');
        }
        sb.append(']');
    }
    
    public static LocationImpl fromZeroBased(final String s, final String s2, final long n, final int n2, final int n3) {
        return new LocationImpl(s, s2, (int)n, n2 + 1, n3 + 1);
    }
    
    public static LocationImpl getEmptyLocation() {
        return LocationImpl.EMPTY;
    }
    
    public int getCharacterOffset() {
        return this._charOffset;
    }
    
    public int getColumnNumber() {
        return this._col;
    }
    
    public XMLStreamLocation2 getContext() {
        return null;
    }
    
    public int getLineNumber() {
        return this._row;
    }
    
    public String getPublicId() {
        return this._publicId;
    }
    
    public String getSystemId() {
        return this._systemId;
    }
    
    @Override
    public String toString() {
        if (this._desc == null) {
            final StringBuffer sb = new StringBuffer(100);
            this.appendDesc(sb);
            this._desc = sb.toString();
        }
        return this._desc;
    }
}

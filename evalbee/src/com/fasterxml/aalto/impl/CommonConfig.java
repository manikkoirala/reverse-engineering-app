// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.impl;

import java.util.HashMap;

public abstract class CommonConfig
{
    protected static final String IMPL_NAME = "aalto";
    protected static final String IMPL_VERSION = "0.9";
    static final int PROP_IMPL_NAME = 1;
    static final int PROP_IMPL_VERSION = 2;
    static final int PROP_SUPPORTS_XML11 = 3;
    static final int PROP_SUPPORTS_XMLID = 4;
    static final HashMap<String, Integer> sStdProperties;
    protected int _flagMods;
    protected int _flags;
    
    static {
        final HashMap<String, Integer> hashMap = sStdProperties = new HashMap<String, Integer>(16);
        final Integer value = 1;
        hashMap.put("org.codehaus.stax2.implName", value);
        hashMap.put("org.codehaus.stax2.implVersion", 2);
        hashMap.put("org.codehaus.stax2.supportsXml11", 3);
        hashMap.put("org.codehaus.stax2.supportXmlId", 4);
        hashMap.put("http://java.sun.com/xml/stream/properties/implementation-name", value);
    }
    
    public CommonConfig(final int flags, final int flagMods) {
        this._flags = flags;
        this._flagMods = flagMods;
    }
    
    public abstract String getActualEncoding();
    
    public abstract String getExternalEncoding();
    
    public Object getProperty(final String s, final boolean b) {
        final Integer n = CommonConfig.sStdProperties.get(s);
        if (n != null) {
            final int intValue = n;
            if (intValue == 1) {
                return "aalto";
            }
            if (intValue == 2) {
                return "0.9";
            }
            if (intValue == 3) {
                return Boolean.FALSE;
            }
            if (intValue == 4) {
                return Boolean.FALSE;
            }
        }
        if (!b) {
            return null;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unrecognized property '");
        sb.append(s);
        sb.append("'");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public final boolean hasExplicitFlag(final int n) {
        return this.hasFlag(n) && this.hasFlagBeenModified(n);
    }
    
    public final boolean hasFlag(final int n) {
        return (n & this._flags) != 0x0;
    }
    
    public final boolean hasFlagBeenModified(final int n) {
        return (n & this._flagMods) != 0x0;
    }
    
    public boolean isPropertySupported(final String key) {
        return CommonConfig.sStdProperties.containsKey(key);
    }
    
    public abstract boolean isXml11();
    
    public final void setFlag(final int n, final boolean b) {
        int flags;
        if (b) {
            flags = (this._flags | n);
        }
        else {
            flags = (this._flags & ~n);
        }
        this._flags = flags;
        this._flagMods |= n;
    }
    
    public boolean setProperty(final String s, final Object o) {
        if (CommonConfig.sStdProperties.get(s) != null) {
            return false;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unrecognized property '");
        sb.append(s);
        sb.append("'");
        throw new IllegalArgumentException(sb.toString());
    }
}

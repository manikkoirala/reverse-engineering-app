// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.impl;

import javax.xml.stream.Location;
import javax.xml.stream.XMLStreamException;

public class StreamExceptionBase extends XMLStreamException
{
    final String mMsg;
    
    public StreamExceptionBase(final String s) {
        super(s);
        this.mMsg = s;
    }
    
    public StreamExceptionBase(final String s, final Location location) {
        super(s, location);
        this.mMsg = s;
    }
    
    public StreamExceptionBase(final String s, final Location location, final Throwable t) {
        super(s, location, t);
        this.mMsg = s;
        if (t != null && this.getCause() == null) {
            this.initCause(t);
        }
    }
    
    public StreamExceptionBase(final Throwable t) {
        super(t.getMessage(), t);
        this.mMsg = t.getMessage();
        if (this.getCause() == null) {
            this.initCause(t);
        }
    }
    
    public String getLocationDesc() {
        final Location location = this.getLocation();
        String string;
        if (location == null) {
            string = null;
        }
        else {
            string = location.toString();
        }
        return string;
    }
    
    @Override
    public String getMessage() {
        final String locationDesc = this.getLocationDesc();
        if (locationDesc == null) {
            return super.getMessage();
        }
        final StringBuilder sb = new StringBuilder(this.mMsg.length() + locationDesc.length() + 20);
        sb.append(this.mMsg);
        sb.append('\n');
        sb.append(" at ");
        sb.append(locationDesc);
        return sb.toString();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getName());
        sb.append(": ");
        sb.append(this.getMessage());
        return sb.toString();
    }
}

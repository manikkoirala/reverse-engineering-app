// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.io;

import java.io.IOException;
import java.io.OutputStream;
import com.fasterxml.aalto.out.WriterConfig;
import java.io.Writer;

public final class UTF8Writer extends Writer
{
    static final int SURR1_FIRST = 55296;
    static final int SURR1_LAST = 56319;
    static final int SURR2_FIRST = 56320;
    static final int SURR2_LAST = 57343;
    final boolean mAutoCloseOutput;
    final WriterConfig mConfig;
    OutputStream mOut;
    byte[] mOutBuffer;
    final int mOutBufferLast;
    int mOutPtr;
    int mSurrogate;
    
    public UTF8Writer(final WriterConfig mConfig, final OutputStream mOut, final boolean mAutoCloseOutput) {
        this.mSurrogate = 0;
        this.mConfig = mConfig;
        this.mAutoCloseOutput = mAutoCloseOutput;
        this.mOut = mOut;
        final byte[] allocFullBBuffer = mConfig.allocFullBBuffer(4000);
        this.mOutBuffer = allocFullBBuffer;
        this.mOutBufferLast = allocFullBBuffer.length - 4;
        this.mOutPtr = 0;
    }
    
    private int convertSurrogate(final int i) {
        final int mSurrogate = this.mSurrogate;
        this.mSurrogate = 0;
        if (i >= 56320 && i <= 57343) {
            return (mSurrogate - 55296 << 10) + 65536 + (i - 56320);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Broken surrogate pair: first char 0x");
        sb.append(Integer.toHexString(mSurrogate));
        sb.append(", second 0x");
        sb.append(Integer.toHexString(i));
        sb.append("; illegal combination");
        throw new IOException(sb.toString());
    }
    
    private void throwIllegal(final int n) {
        if (n > 1114111) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Illegal character point (0x");
            sb.append(Integer.toHexString(n));
            sb.append(") to output; max is 0x10FFFF as per RFC 3629");
            throw new IOException(sb.toString());
        }
        if (n < 55296) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Illegal character point (0x");
            sb2.append(Integer.toHexString(n));
            sb2.append(") to output");
            throw new IOException(sb2.toString());
        }
        if (n <= 56319) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Unmatched first part of surrogate pair (0x");
            sb3.append(Integer.toHexString(n));
            sb3.append(")");
            throw new IOException(sb3.toString());
        }
        final StringBuilder sb4 = new StringBuilder();
        sb4.append("Unmatched second part of surrogate pair (0x");
        sb4.append(Integer.toHexString(n));
        sb4.append(")");
        throw new IOException(sb4.toString());
    }
    
    @Override
    public void close() {
        final OutputStream mOut = this.mOut;
        if (mOut != null) {
            final int mOutPtr = this.mOutPtr;
            if (mOutPtr > 0) {
                mOut.write(this.mOutBuffer, 0, mOutPtr);
                this.mOutPtr = 0;
            }
            final OutputStream mOut2 = this.mOut;
            this.mOut = null;
            final byte[] mOutBuffer = this.mOutBuffer;
            this.mOutBuffer = null;
            if (this.mAutoCloseOutput) {
                mOut2.close();
            }
            this.mConfig.freeFullBBuffer(mOutBuffer);
            final int mSurrogate = this.mSurrogate;
            this.mSurrogate = 0;
            if (mSurrogate > 0) {
                this.throwIllegal(mSurrogate);
            }
        }
    }
    
    @Override
    public void flush() {
        final int mOutPtr = this.mOutPtr;
        if (mOutPtr > 0) {
            this.mOut.write(this.mOutBuffer, 0, mOutPtr);
            this.mOutPtr = 0;
        }
        this.mOut.flush();
    }
    
    @Override
    public void write(int mOutPtr) {
        int convertSurrogate;
        if (this.mSurrogate > 0) {
            convertSurrogate = this.convertSurrogate(mOutPtr);
        }
        else {
            convertSurrogate = mOutPtr;
            if (mOutPtr >= 55296 && (convertSurrogate = mOutPtr) <= 57343) {
                if (mOutPtr > 56319) {
                    this.throwIllegal(mOutPtr);
                }
                this.mSurrogate = mOutPtr;
                return;
            }
        }
        mOutPtr = this.mOutPtr;
        if (mOutPtr >= this.mOutBufferLast) {
            this.mOut.write(this.mOutBuffer, 0, mOutPtr);
            this.mOutPtr = 0;
        }
        if (convertSurrogate < 128) {
            final byte[] mOutBuffer = this.mOutBuffer;
            mOutPtr = this.mOutPtr++;
            mOutBuffer[mOutPtr] = (byte)convertSurrogate;
        }
        else {
            mOutPtr = this.mOutPtr;
            if (convertSurrogate < 2048) {
                final byte[] mOutBuffer2 = this.mOutBuffer;
                final int n = mOutPtr + 1;
                mOutBuffer2[mOutPtr] = (byte)(convertSurrogate >> 6 | 0xC0);
                mOutPtr = n + 1;
                mOutBuffer2[n] = (byte)((convertSurrogate & 0x3F) | 0x80);
            }
            else if (convertSurrogate <= 65535) {
                final byte[] mOutBuffer3 = this.mOutBuffer;
                final int n2 = mOutPtr + 1;
                mOutBuffer3[mOutPtr] = (byte)(convertSurrogate >> 12 | 0xE0);
                mOutPtr = n2 + 1;
                mOutBuffer3[n2] = (byte)((convertSurrogate >> 6 & 0x3F) | 0x80);
                mOutBuffer3[mOutPtr] = (byte)((convertSurrogate & 0x3F) | 0x80);
                ++mOutPtr;
            }
            else {
                if (convertSurrogate > 1114111) {
                    this.throwIllegal(convertSurrogate);
                }
                final byte[] mOutBuffer4 = this.mOutBuffer;
                final int n3 = mOutPtr + 1;
                mOutBuffer4[mOutPtr] = (byte)(convertSurrogate >> 18 | 0xF0);
                mOutPtr = n3 + 1;
                mOutBuffer4[n3] = (byte)((convertSurrogate >> 12 & 0x3F) | 0x80);
                final int n4 = mOutPtr + 1;
                mOutBuffer4[mOutPtr] = (byte)((convertSurrogate >> 6 & 0x3F) | 0x80);
                mOutPtr = n4 + 1;
                mOutBuffer4[n4] = (byte)((convertSurrogate & 0x3F) | 0x80);
            }
            this.mOutPtr = mOutPtr;
        }
    }
    
    @Override
    public void write(final String s) {
        this.write(s, 0, s.length());
    }
    
    @Override
    public void write(final String s, int mOutPtr, int i) {
        if (i < 2) {
            if (i == 1) {
                this.write(s.charAt(mOutPtr));
            }
            return;
        }
        int n = mOutPtr;
        int n2 = i;
        if (this.mSurrogate > 0) {
            final char char1 = s.charAt(mOutPtr);
            n2 = i - 1;
            this.write(this.convertSurrogate(char1));
            n = mOutPtr + 1;
        }
        mOutPtr = this.mOutPtr;
        final byte[] mOutBuffer = this.mOutBuffer;
        final int mOutBufferLast = this.mOutBufferLast;
        final int n3 = n2 + n;
        i = n;
        int mOutPtr2;
        while (true) {
            mOutPtr2 = mOutPtr;
            if (i >= n3) {
                break;
            }
            int n4;
            if ((n4 = mOutPtr) >= mOutBufferLast) {
                this.mOut.write(mOutBuffer, 0, mOutPtr);
                n4 = 0;
            }
            final int n5 = i + 1;
            final char char2 = s.charAt(i);
            i = n4;
            mOutPtr = n5;
            char mSurrogate = '\0';
            Label_0255: {
                if ((mSurrogate = char2) < '\u0080') {
                    mOutPtr = n4 + 1;
                    mOutBuffer[n4] = (byte)char2;
                    i = n3 - n5;
                    final int n6 = mOutBufferLast - mOutPtr;
                    int n7;
                    if ((n7 = i) > n6) {
                        n7 = n6;
                    }
                    int n8;
                    char char3;
                    int n9;
                    for (i = n5; i < n7 + n5; i = n8, mOutPtr = n9) {
                        n8 = i + 1;
                        char3 = s.charAt(i);
                        if (char3 >= '\u0080') {
                            i = mOutPtr;
                            mOutPtr = n8;
                            mSurrogate = char3;
                            break Label_0255;
                        }
                        n9 = mOutPtr + 1;
                        mOutBuffer[mOutPtr] = (byte)char3;
                    }
                    continue;
                }
            }
            if (mSurrogate < '\u0800') {
                final int n10 = i + 1;
                mOutBuffer[i] = (byte)(mSurrogate >> 6 | 0xC0);
                final int n11 = n10 + 1;
                mOutBuffer[n10] = (byte)((mSurrogate & '?') | 0x80);
                i = mOutPtr;
                mOutPtr = n11;
            }
            else if (mSurrogate >= '\ud800' && mSurrogate <= '\udfff') {
                if (mSurrogate > '\udbff') {
                    this.mOutPtr = i;
                    this.throwIllegal(mSurrogate);
                }
                this.mSurrogate = mSurrogate;
                if (mOutPtr >= n3) {
                    mOutPtr2 = i;
                    break;
                }
                final int n12 = mOutPtr + 1;
                final int convertSurrogate = this.convertSurrogate(s.charAt(mOutPtr));
                if (convertSurrogate > 1114111) {
                    this.mOutPtr = i;
                    this.throwIllegal(convertSurrogate);
                }
                final int n13 = i + 1;
                mOutBuffer[i] = (byte)(convertSurrogate >> 18 | 0xF0);
                mOutPtr = n13 + 1;
                mOutBuffer[n13] = (byte)((convertSurrogate >> 12 & 0x3F) | 0x80);
                i = mOutPtr + 1;
                mOutBuffer[mOutPtr] = (byte)((convertSurrogate >> 6 & 0x3F) | 0x80);
                mOutPtr = i + 1;
                mOutBuffer[i] = (byte)((convertSurrogate & 0x3F) | 0x80);
                i = n12;
            }
            else {
                final int n14 = i + 1;
                mOutBuffer[i] = (byte)(mSurrogate >> 12 | 0xE0);
                final int n15 = n14 + 1;
                mOutBuffer[n14] = (byte)((mSurrogate >> 6 & 0x3F) | 0x80);
                mOutBuffer[n15] = (byte)((mSurrogate & '?') | 0x80);
                i = mOutPtr;
                mOutPtr = n15 + 1;
            }
        }
        this.mOutPtr = mOutPtr2;
    }
    
    @Override
    public void write(final char[] array) {
        this.write(array, 0, array.length);
    }
    
    @Override
    public void write(final char[] array, int mOutPtr, int i) {
        if (i < 2) {
            if (i == 1) {
                this.write(array[mOutPtr]);
            }
            return;
        }
        int n = mOutPtr;
        int n2 = i;
        if (this.mSurrogate > 0) {
            final char c = array[mOutPtr];
            n2 = i - 1;
            this.write(this.convertSurrogate(c));
            n = mOutPtr + 1;
        }
        mOutPtr = this.mOutPtr;
        final byte[] mOutBuffer = this.mOutBuffer;
        final int mOutBufferLast = this.mOutBufferLast;
        final int n3 = n2 + n;
        i = n;
        int mOutPtr2;
        while (true) {
            mOutPtr2 = mOutPtr;
            if (i >= n3) {
                break;
            }
            int n4;
            if ((n4 = mOutPtr) >= mOutBufferLast) {
                this.mOut.write(mOutBuffer, 0, mOutPtr);
                n4 = 0;
            }
            final int n5 = i + 1;
            final char c2 = array[i];
            i = n4;
            mOutPtr = n5;
            char mSurrogate = '\0';
            Label_0247: {
                if ((mSurrogate = c2) < '\u0080') {
                    mOutPtr = n4 + 1;
                    mOutBuffer[n4] = (byte)c2;
                    final int n6 = n3 - n5;
                    i = mOutBufferLast - mOutPtr;
                    int n7;
                    if ((n7 = n6) > i) {
                        n7 = i;
                    }
                    int n8;
                    char c3;
                    int n9;
                    for (i = n5; i < n7 + n5; i = n8, mOutPtr = n9) {
                        n8 = i + 1;
                        c3 = array[i];
                        if (c3 >= '\u0080') {
                            i = mOutPtr;
                            mOutPtr = n8;
                            mSurrogate = c3;
                            break Label_0247;
                        }
                        n9 = mOutPtr + 1;
                        mOutBuffer[mOutPtr] = (byte)c3;
                    }
                    continue;
                }
            }
            if (mSurrogate < '\u0800') {
                final int n10 = i + 1;
                mOutBuffer[i] = (byte)(mSurrogate >> 6 | 0xC0);
                final int n11 = n10 + 1;
                mOutBuffer[n10] = (byte)((mSurrogate & '?') | 0x80);
                i = mOutPtr;
                mOutPtr = n11;
            }
            else if (mSurrogate >= '\ud800' && mSurrogate <= '\udfff') {
                if (mSurrogate > '\udbff') {
                    this.mOutPtr = i;
                    this.throwIllegal(mSurrogate);
                }
                this.mSurrogate = mSurrogate;
                if (mOutPtr >= n3) {
                    mOutPtr2 = i;
                    break;
                }
                final int n12 = mOutPtr + 1;
                final int convertSurrogate = this.convertSurrogate(array[mOutPtr]);
                if (convertSurrogate > 1114111) {
                    this.mOutPtr = i;
                    this.throwIllegal(convertSurrogate);
                }
                final int n13 = i + 1;
                mOutBuffer[i] = (byte)(convertSurrogate >> 18 | 0xF0);
                mOutPtr = n13 + 1;
                mOutBuffer[n13] = (byte)((convertSurrogate >> 12 & 0x3F) | 0x80);
                i = mOutPtr + 1;
                mOutBuffer[mOutPtr] = (byte)((convertSurrogate >> 6 & 0x3F) | 0x80);
                mOutPtr = i + 1;
                mOutBuffer[i] = (byte)((convertSurrogate & 0x3F) | 0x80);
                i = n12;
            }
            else {
                final int n14 = i + 1;
                mOutBuffer[i] = (byte)(mSurrogate >> 12 | 0xE0);
                final int n15 = n14 + 1;
                mOutBuffer[n14] = (byte)((mSurrogate >> 6 & 0x3F) | 0x80);
                mOutBuffer[n15] = (byte)((mSurrogate & '?') | 0x80);
                i = mOutPtr;
                mOutPtr = n15 + 1;
            }
        }
        this.mOutPtr = mOutPtr2;
    }
}

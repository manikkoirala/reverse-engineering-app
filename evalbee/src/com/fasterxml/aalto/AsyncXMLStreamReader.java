// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto;

import com.fasterxml.aalto.in.ReaderConfig;
import org.codehaus.stax2.XMLStreamReader2;

public interface AsyncXMLStreamReader<F extends AsyncInputFeeder> extends XMLStreamReader2
{
    public static final int EVENT_INCOMPLETE = 257;
    
    ReaderConfig getConfig();
    
    F getInputFeeder();
}

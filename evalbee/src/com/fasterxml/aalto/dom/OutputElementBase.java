// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.dom;

import javax.xml.stream.XMLStreamException;
import org.codehaus.stax2.ri.EmptyIterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import javax.xml.namespace.NamespaceContext;

public abstract class OutputElementBase implements NamespaceContext
{
    public static final int PREFIX_MISBOUND = 2;
    public static final int PREFIX_OK = 1;
    public static final int PREFIX_UNBOUND = 0;
    protected String _defaultNsURI;
    protected boolean _nsMapShared;
    protected BijectiveNsMap _nsMapping;
    protected NamespaceContext _rootNsContext;
    
    public OutputElementBase() {
        this._nsMapping = null;
        this._nsMapShared = false;
        this._defaultNsURI = "";
        this._rootNsContext = null;
    }
    
    public OutputElementBase(final OutputElementBase outputElementBase, final BijectiveNsMap nsMapping) {
        this._nsMapping = nsMapping;
        this._nsMapShared = (nsMapping != null);
        this._defaultNsURI = outputElementBase._defaultNsURI;
        this._rootNsContext = outputElementBase._rootNsContext;
    }
    
    public final void addPrefix(final String s, final String s2) {
        final BijectiveNsMap nsMapping = this._nsMapping;
        if (nsMapping == null) {
            this._nsMapping = BijectiveNsMap.createEmpty();
        }
        else if (this._nsMapShared) {
            this._nsMapping = nsMapping.createChild();
            this._nsMapShared = false;
        }
        this._nsMapping.addMapping(s, s2);
    }
    
    public final String generateMapping(final String s, final String s2, final int[] array) {
        final BijectiveNsMap nsMapping = this._nsMapping;
        if (nsMapping == null) {
            this._nsMapping = BijectiveNsMap.createEmpty();
        }
        else if (this._nsMapShared) {
            this._nsMapping = nsMapping.createChild();
            this._nsMapShared = false;
        }
        return this._nsMapping.addGeneratedMapping(s, this._rootNsContext, s2, array);
    }
    
    public final String getDefaultNsUri() {
        return this._defaultNsURI;
    }
    
    public final String getExplicitPrefix(String prefix) {
        final BijectiveNsMap nsMapping = this._nsMapping;
        if (nsMapping != null) {
            final String prefixByUri = nsMapping.findPrefixByUri(prefix);
            if (prefixByUri != null) {
                return prefixByUri;
            }
        }
        final NamespaceContext rootNsContext = this._rootNsContext;
        if (rootNsContext != null) {
            prefix = rootNsContext.getPrefix(prefix);
            if (prefix != null && prefix.length() > 0) {
                return prefix;
            }
        }
        return null;
    }
    
    public abstract String getNameDesc();
    
    @Override
    public final String getNamespaceURI(String namespaceURI) {
        if (namespaceURI.length() == 0) {
            return this._defaultNsURI;
        }
        final BijectiveNsMap nsMapping = this._nsMapping;
        if (nsMapping != null) {
            final String uriByPrefix = nsMapping.findUriByPrefix(namespaceURI);
            if (uriByPrefix != null) {
                return uriByPrefix;
            }
        }
        final NamespaceContext rootNsContext = this._rootNsContext;
        if (rootNsContext != null) {
            namespaceURI = rootNsContext.getNamespaceURI(namespaceURI);
        }
        else {
            namespaceURI = null;
        }
        return namespaceURI;
    }
    
    @Override
    public final String getPrefix(String prefix) {
        if (this._defaultNsURI.equals(prefix)) {
            return "";
        }
        final BijectiveNsMap nsMapping = this._nsMapping;
        if (nsMapping != null) {
            final String prefixByUri = nsMapping.findPrefixByUri(prefix);
            if (prefixByUri != null) {
                return prefixByUri;
            }
        }
        final NamespaceContext rootNsContext = this._rootNsContext;
        if (rootNsContext != null) {
            prefix = rootNsContext.getPrefix(prefix);
        }
        else {
            prefix = null;
        }
        return prefix;
    }
    
    @Override
    public final Iterator<String> getPrefixes(final String anObject) {
        ArrayList list;
        if (this._defaultNsURI.equals(anObject)) {
            list = new ArrayList();
            list.add("");
        }
        else {
            list = null;
        }
        final BijectiveNsMap nsMapping = this._nsMapping;
        List<String> prefixesBoundToUri = list;
        if (nsMapping != null) {
            prefixesBoundToUri = nsMapping.getPrefixesBoundToUri(anObject, list);
        }
        final NamespaceContext rootNsContext = this._rootNsContext;
        List<String> list2 = prefixesBoundToUri;
        if (rootNsContext != null) {
            final Iterator<String> prefixes = rootNsContext.getPrefixes(anObject);
            while (true) {
                list2 = prefixesBoundToUri;
                if (!prefixes.hasNext()) {
                    break;
                }
                final String s = prefixes.next();
                if (s.length() == 0) {
                    continue;
                }
                List<String> list3;
                if (prefixesBoundToUri == null) {
                    list3 = new ArrayList<String>();
                }
                else {
                    list3 = prefixesBoundToUri;
                    if (prefixesBoundToUri.contains(s)) {
                        continue;
                    }
                }
                list3.add(s);
                prefixesBoundToUri = list3;
            }
        }
        if (list2 == null) {
            return EmptyIterator.getInstance();
        }
        return list2.iterator();
    }
    
    public final int isPrefixValid(String defaultNsURI, String uriByPrefix, final boolean b) {
        String s = uriByPrefix;
        if (uriByPrefix == null) {
            s = "";
        }
        int n = 2;
        if (defaultNsURI == null || defaultNsURI.length() == 0) {
            if (b) {
                defaultNsURI = this._defaultNsURI;
                if (s == defaultNsURI || s.equals(defaultNsURI)) {
                    return 1;
                }
            }
            else if (s.length() == 0) {
                return 1;
            }
            return 2;
        }
        if ("xml".equals(defaultNsURI)) {
            if (!s.equals("http://www.w3.org/XML/1998/namespace")) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Namespace prefix 'xml' can not be bound to non-default namespace ('");
                sb.append(s);
                sb.append("'); has to be the default '");
                sb.append("http://www.w3.org/XML/1998/namespace");
                sb.append("'");
                this.throwOutputError(sb.toString());
            }
            return 1;
        }
        final BijectiveNsMap nsMapping = this._nsMapping;
        if (nsMapping != null) {
            uriByPrefix = nsMapping.findUriByPrefix(defaultNsURI);
        }
        else {
            uriByPrefix = null;
        }
        String namespaceURI = uriByPrefix;
        if (uriByPrefix == null) {
            final NamespaceContext rootNsContext = this._rootNsContext;
            namespaceURI = uriByPrefix;
            if (rootNsContext != null) {
                namespaceURI = rootNsContext.getNamespaceURI(defaultNsURI);
            }
        }
        if (namespaceURI == null) {
            return 0;
        }
        if (namespaceURI == s || namespaceURI.equals(s)) {
            n = 1;
        }
        return n;
    }
    
    public abstract boolean isRoot();
    
    public void relink(final OutputElementBase outputElementBase) {
        final BijectiveNsMap nsMapping = outputElementBase._nsMapping;
        this._nsMapping = nsMapping;
        this._nsMapShared = (nsMapping != null);
        this._defaultNsURI = outputElementBase._defaultNsURI;
        this._rootNsContext = outputElementBase._rootNsContext;
    }
    
    public abstract void setDefaultNsUri(final String p0);
    
    public abstract void setRootNsContext(final NamespaceContext p0);
    
    public final void throwOutputError(final String msg) {
        throw new XMLStreamException(msg);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.dom;

import javax.xml.namespace.NamespaceContext;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

public class DOMOutputElement extends OutputElementBase
{
    private boolean _defaultNsSet;
    private Element _element;
    private DOMOutputElement _parent;
    
    private DOMOutputElement() {
        this._parent = null;
        this._element = null;
        super._nsMapping = null;
        super._nsMapShared = false;
        super._defaultNsURI = "";
        super._rootNsContext = null;
        this._defaultNsSet = false;
    }
    
    private DOMOutputElement(final DOMOutputElement parent, final Element element, final BijectiveNsMap nsMapping) {
        super(parent, nsMapping);
        this._parent = parent;
        this._element = element;
        super._nsMapping = nsMapping;
        super._nsMapShared = (nsMapping != null);
        super._defaultNsURI = parent._defaultNsURI;
        super._rootNsContext = parent._rootNsContext;
        this._defaultNsSet = false;
    }
    
    public static DOMOutputElement createRoot() {
        return new DOMOutputElement();
    }
    
    private void relink(final DOMOutputElement parent, final Element element) {
        super.relink(parent);
        (this._parent = parent).appendNode(this._element = element);
        this._defaultNsSet = false;
    }
    
    public void addAttribute(final String s, final String s2) {
        this._element.setAttribute(s, s2);
    }
    
    public void addAttribute(final String s, final String s2, final String s3) {
        this._element.setAttributeNS(s, s2, s3);
    }
    
    public void addToPool(final DOMOutputElement parent) {
        this._parent = parent;
    }
    
    public void appendChild(final Node node) {
        this._element.appendChild(node);
    }
    
    public void appendNode(final Node node) {
        Object o;
        if (this.isRoot()) {
            o = this._element.getOwnerDocument();
        }
        else {
            o = this._element;
        }
        ((Node)o).appendChild(node);
    }
    
    public DOMOutputElement createAndAttachChild(final Element element) {
        Object o;
        if (this.isRoot()) {
            o = element.getOwnerDocument();
        }
        else {
            o = this._element;
        }
        ((Node)o).appendChild(element);
        return this.createChild(element);
    }
    
    public DOMOutputElement createChild(final Element element) {
        return new DOMOutputElement(this, element, super._nsMapping);
    }
    
    @Override
    public String getNameDesc() {
        final Element element = this._element;
        if (element != null) {
            return element.getLocalName();
        }
        return "#error";
    }
    
    public DOMOutputElement getParent() {
        return this._parent;
    }
    
    @Override
    public boolean isRoot() {
        return this._parent == null;
    }
    
    public DOMOutputElement reuseAsChild(final DOMOutputElement domOutputElement, final Element element) {
        final DOMOutputElement parent = this._parent;
        this.relink(domOutputElement, element);
        return parent;
    }
    
    @Override
    public void setDefaultNsUri(final String defaultNsURI) {
        super._defaultNsURI = defaultNsURI;
        this._defaultNsSet = true;
    }
    
    @Override
    public void setRootNsContext(final NamespaceContext rootNsContext) {
        super._rootNsContext = rootNsContext;
        if (!this._defaultNsSet) {
            final String namespaceURI = rootNsContext.getNamespaceURI("");
            if (namespaceURI != null && namespaceURI.length() > 0) {
                super._defaultNsURI = namespaceURI;
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.dom;

import com.fasterxml.aalto.WFCException;
import javax.xml.stream.Location;
import java.util.Collections;
import javax.xml.transform.dom.DOMSource;
import com.fasterxml.aalto.in.ReaderConfig;
import org.codehaus.stax2.ri.dom.DOMWrappingReader;

public class DOMReaderImpl extends DOMWrappingReader
{
    protected final ReaderConfig _config;
    
    public DOMReaderImpl(final DOMSource domSource, final ReaderConfig config) {
        super(domSource, config.willSupportNamespaces(), config.willCoalesceText());
        this._config = config;
        if (config.hasInternNamesBeenEnabled()) {
            this.setInternNames(config.willInternNames());
        }
        if (config.hasInternNsURIsBeenEnabled()) {
            this.setInternNsURIs(config.willInternNsURIs());
        }
    }
    
    public static DOMReaderImpl createFrom(final DOMSource domSource, final ReaderConfig readerConfig) {
        return new DOMReaderImpl(domSource, readerConfig);
    }
    
    public Object getProperty(final String s) {
        if (s.equals("javax.xml.stream.entities")) {
            return Collections.EMPTY_LIST;
        }
        if (s.equals("javax.xml.stream.notations")) {
            return Collections.EMPTY_LIST;
        }
        return this._config.getProperty(s, true);
    }
    
    public boolean isPropertySupported(final String s) {
        return this._config.isPropertySupported(s);
    }
    
    public boolean setProperty(final String s, final Object o) {
        return this._config.setProperty(s, o);
    }
    
    public void throwStreamException(final String s, final Location location) {
        throw new WFCException(s, location);
    }
}

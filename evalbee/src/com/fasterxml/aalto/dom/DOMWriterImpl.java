// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.dom;

import org.codehaus.stax2.ri.EmptyNamespaceContext;
import javax.xml.namespace.NamespaceContext;
import org.w3c.dom.Document;
import javax.xml.transform.dom.DOMResult;
import org.w3c.dom.Element;
import javax.xml.stream.XMLStreamException;
import org.w3c.dom.Node;
import java.util.HashMap;
import com.fasterxml.aalto.out.WriterConfig;
import org.codehaus.stax2.ri.dom.DOMWrappingWriter;

public final class DOMWriterImpl extends DOMWrappingWriter
{
    protected int[] _autoNsSeq;
    protected String _automaticNsPrefix;
    protected final WriterConfig _config;
    protected DOMOutputElement _currElem;
    protected DOMOutputElement _openElement;
    protected String _suggestedDefNs;
    HashMap<String, String> _suggestedPrefixes;
    
    private DOMWriterImpl(final WriterConfig config, final Node node) {
        super(node, true, config.willRepairNamespaces());
        this._suggestedDefNs = null;
        this._suggestedPrefixes = null;
        this._config = config;
        this._autoNsSeq = null;
        this._automaticNsPrefix = config.getAutomaticNsPrefix();
        final short nodeType = node.getNodeType();
        if (nodeType != 1) {
            if (nodeType != 9 && nodeType != 11) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Can not create an XMLStreamWriter for a DOM node of type ");
                sb.append(node.getClass());
                throw new XMLStreamException(sb.toString());
            }
            this._currElem = DOMOutputElement.createRoot();
            this._openElement = null;
        }
        else {
            final DOMOutputElement child = DOMOutputElement.createRoot().createChild((Element)node);
            this._currElem = child;
            this._openElement = child;
        }
    }
    
    public static DOMWriterImpl createFrom(final WriterConfig writerConfig, final DOMResult domResult) {
        return new DOMWriterImpl(writerConfig, domResult.getNode());
    }
    
    private final String validateElemPrefix(String defaultNsUri, final String s, final DOMOutputElement domOutputElement) {
        if (s != null && s.length() != 0) {
            if (domOutputElement.isPrefixValid(defaultNsUri, s, true) == 1) {
                return defaultNsUri;
            }
            return null;
        }
        else {
            defaultNsUri = domOutputElement.getDefaultNsUri();
            if (defaultNsUri != null && defaultNsUri.length() != 0) {
                return null;
            }
            return "";
        }
    }
    
    public void appendLeaf(final Node node) {
        this._currElem.appendNode(node);
        this._openElement = null;
    }
    
    public void createStartElem(final String str, String s, final String str2, final boolean b) {
        DOMOutputElement domOutputElement = null;
        Label_0452: {
            if (!super.mNsAware) {
                if (str != null && str.length() > 0) {
                    DOMWrappingWriter.throwOutputError("Can not specify non-empty uri/prefix in non-namespace mode");
                }
                domOutputElement = this._currElem.createAndAttachChild(super.mDocument.createElement(str2));
            }
            else {
                DOMOutputElement currElem = null;
                Element element = null;
                Label_0446: {
                    if (super.mNsRepairing) {
                        final String validateElemPrefix = this.validateElemPrefix(s, str, this._currElem);
                        if (validateElemPrefix == null) {
                            String s2;
                            if ((s2 = s) == null) {
                                s2 = "";
                            }
                            final String generateElemPrefix = this.generateElemPrefix(s2, str, this._currElem);
                            final boolean b2 = generateElemPrefix.length() != 0;
                            s = str2;
                            if (b2) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append(generateElemPrefix);
                                sb.append(":");
                                sb.append(str2);
                                s = sb.toString();
                            }
                            final DOMOutputElement andAttachChild = this._currElem.createAndAttachChild(super.mDocument.createElementNS(str, s));
                            this._openElement = andAttachChild;
                            if (b2) {
                                this.writeNamespace(generateElemPrefix, str);
                                andAttachChild.addPrefix(generateElemPrefix, str);
                            }
                            else {
                                this.writeDefaultNamespace(str);
                                andAttachChild.setDefaultNsUri(str);
                            }
                            domOutputElement = andAttachChild;
                            break Label_0452;
                        }
                        s = str2;
                        if (validateElemPrefix.length() != 0) {
                            currElem = this._currElem;
                            final Document mDocument = super.mDocument;
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append(validateElemPrefix);
                            sb2.append(":");
                            sb2.append(str2);
                            element = mDocument.createElementNS(str, sb2.toString());
                            break Label_0446;
                        }
                    }
                    else {
                        String str3;
                        if ((str3 = s) == null) {
                            str3 = s;
                            if (str != null) {
                                str3 = s;
                                if (str.length() > 0) {
                                    final HashMap<String, String> suggestedPrefixes = this._suggestedPrefixes;
                                    if (suggestedPrefixes == null) {
                                        s = null;
                                    }
                                    else {
                                        s = suggestedPrefixes.get(str);
                                    }
                                    str3 = s;
                                    if (s == null) {
                                        final StringBuilder sb3 = new StringBuilder();
                                        sb3.append("Can not find prefix for namespace \"");
                                        sb3.append(str);
                                        sb3.append("\"");
                                        DOMWrappingWriter.throwOutputError(sb3.toString());
                                        str3 = s;
                                    }
                                }
                            }
                        }
                        s = str2;
                        if (str3 != null) {
                            s = str2;
                            if (str3.length() != 0) {
                                final StringBuilder sb4 = new StringBuilder();
                                sb4.append(str3);
                                sb4.append(":");
                                sb4.append(str2);
                                s = sb4.toString();
                            }
                        }
                    }
                    final DOMOutputElement currElem2 = this._currElem;
                    element = super.mDocument.createElementNS(str, s);
                    currElem = currElem2;
                }
                domOutputElement = currElem.createAndAttachChild(element);
            }
        }
        this._openElement = domOutputElement;
        if (!b) {
            this._currElem = domOutputElement;
        }
    }
    
    public final String findElemPrefix(String defaultNsUri, final DOMOutputElement domOutputElement) {
        if (defaultNsUri != null && defaultNsUri.length() != 0) {
            return this._currElem.getPrefix(defaultNsUri);
        }
        defaultNsUri = domOutputElement.getDefaultNsUri();
        if (defaultNsUri != null && defaultNsUri.length() > 0) {
            return null;
        }
        return "";
    }
    
    public final String findOrCreateAttrPrefix(String generateMapping, final String key, final DOMOutputElement domOutputElement) {
        final String s = null;
        final String s2 = null;
        String s3 = s;
        if (key != null) {
            if (key.length() == 0) {
                s3 = s;
            }
            else {
                if (generateMapping != null) {
                    final int prefixValid = domOutputElement.isPrefixValid(generateMapping, key, false);
                    if (prefixValid == 1) {
                        return generateMapping;
                    }
                    if (prefixValid == 0) {
                        domOutputElement.addPrefix(generateMapping, key);
                        this.writeNamespace(generateMapping, key);
                        return generateMapping;
                    }
                }
                final String explicitPrefix = domOutputElement.getExplicitPrefix(key);
                if (explicitPrefix != null) {
                    return explicitPrefix;
                }
                if (generateMapping == null) {
                    final HashMap<String, String> suggestedPrefixes = this._suggestedPrefixes;
                    if (suggestedPrefixes != null) {
                        generateMapping = suggestedPrefixes.get(key);
                    }
                    else {
                        generateMapping = explicitPrefix;
                    }
                }
                String s4 = null;
                Label_0147: {
                    if (generateMapping != null) {
                        s4 = s2;
                        if (generateMapping.length() == 0) {
                            break Label_0147;
                        }
                        if (domOutputElement.getNamespaceURI(generateMapping) != null) {
                            s4 = s2;
                            break Label_0147;
                        }
                    }
                    s4 = generateMapping;
                }
                if ((generateMapping = s4) == null) {
                    if (this._autoNsSeq == null) {
                        (this._autoNsSeq = new int[] { 0 })[0] = 1;
                    }
                    generateMapping = this._currElem.generateMapping(this._automaticNsPrefix, key, this._autoNsSeq);
                }
                domOutputElement.addPrefix(generateMapping, key);
                this.writeNamespace(generateMapping, key);
                s3 = generateMapping;
            }
        }
        return s3;
    }
    
    public final String generateElemPrefix(String suggestedDefNs, final String s, final DOMOutputElement domOutputElement) {
        if (s != null && s.length() != 0) {
            String generateMapping;
            if ((generateMapping = suggestedDefNs) == null) {
                suggestedDefNs = this._suggestedDefNs;
                if (suggestedDefNs != null && suggestedDefNs.equals(s)) {
                    generateMapping = "";
                }
                else {
                    final HashMap<String, String> suggestedPrefixes = this._suggestedPrefixes;
                    if (suggestedPrefixes == null) {
                        suggestedDefNs = null;
                    }
                    else {
                        suggestedDefNs = suggestedPrefixes.get(s);
                    }
                    generateMapping = suggestedDefNs;
                    if (suggestedDefNs == null) {
                        if (this._autoNsSeq == null) {
                            (this._autoNsSeq = new int[] { 0 })[0] = 1;
                        }
                        generateMapping = domOutputElement.generateMapping(this._automaticNsPrefix, s, this._autoNsSeq);
                    }
                }
            }
            return generateMapping;
        }
        return "";
    }
    
    public NamespaceContext getNamespaceContext() {
        if (!super.mNsAware) {
            return (NamespaceContext)EmptyNamespaceContext.getInstance();
        }
        return this._currElem;
    }
    
    public String getPrefix(final String s) {
        if (!super.mNsAware) {
            return null;
        }
        final NamespaceContext mNsContext = super.mNsContext;
        if (mNsContext != null) {
            final String prefix = mNsContext.getPrefix(s);
            if (prefix != null) {
                return prefix;
            }
        }
        return this._currElem.getPrefix(s);
    }
    
    public Object getProperty(final String s) {
        return this._config.getProperty(s, false);
    }
    
    public boolean isPropertySupported(final String s) {
        return this._config.isPropertySupported(s);
    }
    
    public void outputAttribute(String string, String string2, final String s, final String s2) {
        final DOMOutputElement openElement = this._openElement;
        if (openElement != null) {
            if (super.mNsAware) {
                String orCreateAttrPrefix = string2;
                if (super.mNsRepairing) {
                    orCreateAttrPrefix = this.findOrCreateAttrPrefix(string2, string, openElement);
                }
                string2 = s;
                if (orCreateAttrPrefix != null) {
                    string2 = s;
                    if (orCreateAttrPrefix.length() > 0) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append(orCreateAttrPrefix);
                        sb.append(":");
                        sb.append(s);
                        string2 = sb.toString();
                    }
                }
                this._openElement.addAttribute(string, string2, s2);
            }
            else {
                string = s;
                if (string2 != null) {
                    string = s;
                    if (string2.length() > 0) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append(string2);
                        sb2.append(":");
                        sb2.append(s);
                        string = sb2.toString();
                    }
                }
                this._openElement.addAttribute(string, s2);
            }
            return;
        }
        throw new IllegalStateException("No currently open START_ELEMENT, cannot write attribute");
    }
    
    public void setDefaultNamespace(final String s) {
        String suggestedDefNs = null;
        Label_0015: {
            if (s != null) {
                suggestedDefNs = s;
                if (s.length() != 0) {
                    break Label_0015;
                }
            }
            suggestedDefNs = null;
        }
        this._suggestedDefNs = suggestedDefNs;
    }
    
    public void setPrefix(final String value, final String key) {
        if (value == null) {
            throw new NullPointerException("Can not pass null 'prefix' value");
        }
        if (value.length() == 0) {
            this.setDefaultNamespace(key);
            return;
        }
        if (key != null) {
            Label_0190: {
                StringBuilder sb;
                String str;
                if (value.equals("xml")) {
                    if (key.equals("http://www.w3.org/XML/1998/namespace")) {
                        break Label_0190;
                    }
                    sb = new StringBuilder();
                    sb.append("Trying to redeclare prefix 'xml' from its default URI 'http://www.w3.org/XML/1998/namespace' to \"");
                    sb.append(key);
                    str = "\"";
                }
                else {
                    if (value.equals("xmlns")) {
                        if (!key.equals("http://www.w3.org/2000/xmlns/")) {
                            DOMWrappingWriter.throwOutputError("Trying to declare prefix 'xmlns' (illegal as per NS 1.1 #4)");
                        }
                        return;
                    }
                    if (key.equals("http://www.w3.org/XML/1998/namespace")) {
                        sb = new StringBuilder();
                        sb.append("Trying to bind URI 'http://www.w3.org/XML/1998/namespace to prefix \"");
                        sb.append(value);
                        str = "\" (can only bind to 'xml')";
                    }
                    else {
                        if (!key.equals("http://www.w3.org/2000/xmlns/")) {
                            break Label_0190;
                        }
                        sb = new StringBuilder();
                        sb.append("Trying to bind URI 'http://www.w3.org/2000/xmlns/ to prefix \"");
                        sb.append(value);
                        str = "\" (can not be explicitly bound)";
                    }
                }
                sb.append(str);
                DOMWrappingWriter.throwOutputError(sb.toString());
            }
            if (this._suggestedPrefixes == null) {
                this._suggestedPrefixes = new HashMap<String, String>(16);
            }
            this._suggestedPrefixes.put(key, value);
            return;
        }
        throw new NullPointerException("Can not pass null 'uri' value");
    }
    
    public boolean setProperty(final String s, final Object o) {
        return this._config.setProperty(s, o);
    }
    
    public void writeAttribute(final String s, final String s2) {
        this.outputAttribute(null, null, s, s2);
    }
    
    public void writeAttribute(final String s, final String s2, final String s3) {
        this.outputAttribute(s, null, s2, s3);
    }
    
    public void writeAttribute(final String s, final String s2, final String s3, final String s4) {
        this.outputAttribute(s2, s, s3, s4);
    }
    
    public void writeDTD(final String s, final String s2, final String s3, final String s4) {
        if (this._currElem == null) {
            this.reportUnsupported("writeDTD()");
            return;
        }
        throw new IllegalStateException("Operation only allowed to the document before adding root element");
    }
    
    public void writeDefaultNamespace(final String defaultNamespace) {
        if (this._openElement != null) {
            this.setDefaultNamespace(defaultNamespace);
            this._openElement.addAttribute("http://www.w3.org/2000/xmlns/", "xmlns", defaultNamespace);
            return;
        }
        throw new IllegalStateException("No currently open START_ELEMENT, cannot write attribute");
    }
    
    public void writeEmptyElement(final String s) {
        this.writeEmptyElement(null, s);
    }
    
    public void writeEmptyElement(final String s, final String s2) {
        this.createStartElem(s, null, s2, true);
    }
    
    public void writeEmptyElement(final String s, final String s2, final String s3) {
        String s4 = s;
        if (s == null) {
            s4 = "";
        }
        this.createStartElem(s3, s4, s2, true);
    }
    
    public void writeEndDocument() {
        this._openElement = null;
        this._currElem = null;
    }
    
    public void writeEndElement() {
        final DOMOutputElement currElem = this._currElem;
        if (currElem != null && !currElem.isRoot()) {
            this._openElement = null;
            this._currElem = this._currElem.getParent();
            return;
        }
        throw new IllegalStateException("No open start element to close");
    }
    
    public void writeNamespace(final String s, final String s2) {
        if (s != null && s.length() != 0) {
            if (!super.mNsAware) {
                DOMWrappingWriter.throwOutputError("Can not write namespaces with non-namespace writer.");
            }
            this.outputAttribute("http://www.w3.org/2000/xmlns/", "xmlns", s, s2);
            this._currElem.addPrefix(s, s2);
            return;
        }
        this.writeDefaultNamespace(s2);
    }
    
    public void writeStartElement(final String s) {
        this.writeStartElement(null, s);
    }
    
    public void writeStartElement(final String s, final String s2) {
        this.createStartElem(s, null, s2, false);
    }
    
    public void writeStartElement(final String s, final String s2, final String s3) {
        this.createStartElem(s3, s, s2, false);
    }
}

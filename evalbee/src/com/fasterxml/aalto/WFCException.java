// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto;

import javax.xml.stream.Location;
import com.fasterxml.aalto.impl.StreamExceptionBase;

public class WFCException extends StreamExceptionBase
{
    public WFCException(final String s, final Location location) {
        super(s, location);
    }
}

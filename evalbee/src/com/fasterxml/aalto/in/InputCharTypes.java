// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.in;

import com.fasterxml.aalto.util.XmlCharTypes;

public final class InputCharTypes extends XmlCharTypes
{
    public static final int CT_INPUT_NAME_MB_2 = 5;
    public static final int CT_INPUT_NAME_MB_3 = 6;
    public static final int CT_INPUT_NAME_MB_4 = 7;
    public static final int CT_INPUT_NAME_MB_N = 4;
    private static XmlCharTypes sAsciiCharTypes;
    private static XmlCharTypes sLatin1CharTypes;
    private static final XmlCharTypes sUtf8CharTypes;
    
    static {
        final XmlCharTypes xmlCharTypes = sUtf8CharTypes = new XmlCharTypes();
        fillInUtf8Chars(xmlCharTypes.TEXT_CHARS, xmlCharTypes.ATTR_CHARS, xmlCharTypes.NAME_CHARS, xmlCharTypes.DTD_CHARS, xmlCharTypes.OTHER_CHARS);
    }
    
    public static void fillInIllegalAsciiRange(final int[] array) {
        for (int i = 128; i <= 255; ++i) {
            array[i] = 1;
        }
    }
    
    private static void fillInMultiByteNameRange(final int[] array) {
        for (int i = 128; i < 256; ++i) {
            int n;
            if ((i & 0xE0) == 0xC0) {
                n = 5;
            }
            else if ((i & 0xF0) == 0xE0) {
                n = 6;
            }
            else if ((i & 0xF8) == 0xF0) {
                n = 7;
            }
            else {
                n = 1;
            }
            array[i] = n;
        }
    }
    
    private static void fillInMultiByteTextRange(final int[] array) {
        for (int i = 128; i < 256; ++i) {
            int n;
            if ((i & 0xE0) == 0xC0) {
                n = 5;
            }
            else if ((i & 0xF0) == 0xE0) {
                n = 6;
            }
            else if ((i & 0xF8) == 0xF0) {
                n = 7;
            }
            else {
                n = 1;
            }
            array[i] = n;
        }
    }
    
    public static void fillInUtf8Chars(final int[] array, final int[] array2, final int[] array3, final int[] array4, final int[] array5) {
        XmlCharTypes.fillIn8BitTextRange(array);
        fillInMultiByteTextRange(array);
        XmlCharTypes.fillIn8BitAttrRange(array2);
        fillInMultiByteTextRange(array2);
        XmlCharTypes.fillIn8BitNameRange(array3);
        fillInMultiByteNameRange(array3);
        XmlCharTypes.fillIn8BitDtdRange(array4);
        fillInMultiByteTextRange(array4);
        array5[93] = 11;
        array5[62] = 17;
        XmlCharTypes.fillIn8BitTextRange(array5);
        fillInMultiByteTextRange(array5);
        array5[60] = (array5[38] = 0);
        array5[93] = 11;
        array5[63] = 12;
        array5[45] = 13;
    }
    
    public static final XmlCharTypes getAsciiCharTypes() {
        synchronized (InputCharTypes.class) {
            if (InputCharTypes.sAsciiCharTypes == null) {
                final XmlCharTypes xmlCharTypes = InputCharTypes.sAsciiCharTypes = new XmlCharTypes();
                XmlCharTypes.fillInLatin1Chars(xmlCharTypes.TEXT_CHARS, xmlCharTypes.ATTR_CHARS, xmlCharTypes.NAME_CHARS, xmlCharTypes.DTD_CHARS, xmlCharTypes.OTHER_CHARS);
                fillInIllegalAsciiRange(InputCharTypes.sAsciiCharTypes.TEXT_CHARS);
                fillInIllegalAsciiRange(InputCharTypes.sAsciiCharTypes.ATTR_CHARS);
                fillInIllegalAsciiRange(InputCharTypes.sAsciiCharTypes.NAME_CHARS);
                fillInIllegalAsciiRange(InputCharTypes.sAsciiCharTypes.DTD_CHARS);
                fillInIllegalAsciiRange(InputCharTypes.sAsciiCharTypes.OTHER_CHARS);
            }
            return InputCharTypes.sAsciiCharTypes;
        }
    }
    
    public static final XmlCharTypes getLatin1CharTypes() {
        synchronized (InputCharTypes.class) {
            if (InputCharTypes.sLatin1CharTypes == null) {
                final XmlCharTypes xmlCharTypes = InputCharTypes.sLatin1CharTypes = new XmlCharTypes();
                XmlCharTypes.fillInLatin1Chars(xmlCharTypes.TEXT_CHARS, xmlCharTypes.ATTR_CHARS, xmlCharTypes.NAME_CHARS, xmlCharTypes.DTD_CHARS, xmlCharTypes.OTHER_CHARS);
            }
            return InputCharTypes.sLatin1CharTypes;
        }
    }
    
    public static final XmlCharTypes getUtf8CharTypes() {
        return InputCharTypes.sUtf8CharTypes;
    }
}

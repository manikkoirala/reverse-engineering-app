// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.in;

import com.fasterxml.aalto.util.XmlCharTypes;
import javax.xml.stream.XMLResolver;
import javax.xml.stream.XMLReporter;
import com.fasterxml.aalto.util.UriCanonicalizer;
import com.fasterxml.aalto.util.IllegalCharHandler;
import java.util.HashMap;
import com.fasterxml.aalto.util.BufferRecycler;
import java.lang.ref.SoftReference;
import com.fasterxml.aalto.impl.CommonConfig;

public final class ReaderConfig extends CommonConfig
{
    public static final int DEFAULT_CHAR_BUFFER_LEN = 4000;
    static final int DEFAULT_FLAGS = 7957;
    public static final int DEFAULT_SMALL_BUFFER_LEN = 60;
    static final int F_AUTO_CLOSE_INPUT = 8192;
    static final int F_COALESCING = 2;
    static final int F_DTD_AWARE = 4;
    static final int F_DTD_VALIDATING = 8;
    static final int F_EXPAND_ENTITIES = 16;
    static final int F_INTERN_NAMES = 512;
    static final int F_INTERN_NS_URIS = 1024;
    static final int F_LAZY_PARSING = 256;
    static final int F_NS_AWARE = 1;
    static final int F_PRESERVE_LOCATION = 4096;
    static final int F_REPORT_CDATA = 2048;
    public static final int STANDALONE_NO = 2;
    public static final int STANDALONE_UNKNOWN = 0;
    public static final int STANDALONE_YES = 1;
    static final ThreadLocal<SoftReference<BufferRecycler>> _recyclerRef;
    private static final HashMap<String, Object> sProperties;
    protected BufferRecycler _currRecycler;
    private IllegalCharHandler illegalCharHandler;
    private String mActualEncoding;
    private final UriCanonicalizer mCanonicalizer;
    private final EncodingContext mEncCtxt;
    private final String mExtEncoding;
    private final String mPublicId;
    private XMLReporter mReporter;
    private XMLResolver mResolver;
    private final String mSystemId;
    private String mXmlDeclEncoding;
    private int mXmlDeclStandalone;
    private String mXmlDeclVersion;
    
    static {
        final HashMap<String, Object> hashMap = sProperties = new HashMap<String, Object>();
        final Boolean true = Boolean.TRUE;
        hashMap.put("javax.xml.stream.isNamespaceAware", true);
        hashMap.put("javax.xml.stream.isValidating", 8);
        hashMap.put("javax.xml.stream.isCoalescing", 2);
        hashMap.put("javax.xml.stream.isReplacingEntityReferences", 16);
        final Boolean false = Boolean.FALSE;
        hashMap.put("javax.xml.stream.isSupportingExternalEntities", false);
        hashMap.put("javax.xml.stream.supportDTD", 4);
        hashMap.put("javax.xml.stream.reporter", null);
        hashMap.put("javax.xml.stream.resolver", null);
        hashMap.put("javax.xml.stream.allocator", null);
        hashMap.put("com.ctc.wstx.lazyParsing", 256);
        hashMap.put("org.codehaus.stax2.internNames", 512);
        hashMap.put("org.codehaus.stax2.internNsUris", 1024);
        hashMap.put("org.codehaus.stax2.closeInputSource", 8192);
        hashMap.put("org.codehaus.stax2.preserveLocation", 4096);
        hashMap.put("org.codehaus.stax2.reportPrologWhitespace", false);
        hashMap.put("http://java.sun.com/xml/stream/properties/report-cdata-event", 2048);
        hashMap.put("org.codehaus.stax2.preserveLocation", true);
        hashMap.put("org.codehaus.stax2.propDtdOverride", null);
        _recyclerRef = new ThreadLocal<SoftReference<BufferRecycler>>();
    }
    
    public ReaderConfig() {
        this(null, null, null, new EncodingContext(), 7957, 0, null, null, new UriCanonicalizer());
    }
    
    private ReaderConfig(final String mPublicId, final String mSystemId, final String mExtEncoding, final EncodingContext mEncCtxt, final int flags, final int flagMods, final XMLReporter mReporter, final XMLResolver mResolver, final UriCanonicalizer mCanonicalizer) {
        super(flags, flagMods);
        this.mActualEncoding = null;
        this.mXmlDeclVersion = null;
        this.mXmlDeclEncoding = null;
        this.mXmlDeclStandalone = 0;
        this._currRecycler = null;
        this.mPublicId = mPublicId;
        this.mSystemId = mSystemId;
        this.mExtEncoding = mExtEncoding;
        final SoftReference softReference = ReaderConfig._recyclerRef.get();
        if (softReference != null) {
            this._currRecycler = (BufferRecycler)softReference.get();
        }
        this.mEncCtxt = mEncCtxt;
        super._flags = flags;
        super._flagMods = flagMods;
        this.mReporter = mReporter;
        this.mResolver = mResolver;
        this.mCanonicalizer = mCanonicalizer;
    }
    
    private BufferRecycler createRecycler() {
        final BufferRecycler referent = new BufferRecycler();
        ReaderConfig._recyclerRef.set(new SoftReference<BufferRecycler>(referent));
        return referent;
    }
    
    public byte[] allocFullBBuffer(final int n) {
        final BufferRecycler currRecycler = this._currRecycler;
        if (currRecycler != null) {
            final byte[] fullBBuffer = currRecycler.getFullBBuffer(n);
            if (fullBBuffer != null) {
                return fullBBuffer;
            }
        }
        return new byte[n];
    }
    
    public char[] allocFullCBuffer(final int n) {
        final BufferRecycler currRecycler = this._currRecycler;
        if (currRecycler != null) {
            final char[] fullCBuffer = currRecycler.getFullCBuffer(n);
            if (fullCBuffer != null) {
                return fullCBuffer;
            }
        }
        return new char[n];
    }
    
    public char[] allocMediumCBuffer(final int n) {
        final BufferRecycler currRecycler = this._currRecycler;
        if (currRecycler != null) {
            final char[] mediumCBuffer = currRecycler.getMediumCBuffer(n);
            if (mediumCBuffer != null) {
                return mediumCBuffer;
            }
        }
        return new char[n];
    }
    
    public char[] allocSmallCBuffer(final int n) {
        final BufferRecycler currRecycler = this._currRecycler;
        if (currRecycler != null) {
            final char[] smallCBuffer = currRecycler.getSmallCBuffer(n);
            if (smallCBuffer != null) {
                return smallCBuffer;
            }
        }
        return new char[n];
    }
    
    public String canonicalizeURI(final char[] array, final int n) {
        return this.mCanonicalizer.canonicalizeURI(array, n);
    }
    
    public void configureForConvenience() {
        this.doCoalesceText(true);
        this.doPreserveLocation(true);
    }
    
    public void configureForLowMemUsage() {
        this.doCoalesceText(false);
        this.doPreserveLocation(false);
    }
    
    public void configureForRoundTripping() {
        this.doCoalesceText(false);
    }
    
    public void configureForSpeed() {
        this.doCoalesceText(false);
        this.doPreserveLocation(false);
    }
    
    public void configureForXmlConformance() {
    }
    
    public ReaderConfig createNonShared(final String s, final String s2, final String s3) {
        return new ReaderConfig(s, s2, s3, this.mEncCtxt, super._flags, super._flagMods, this.mReporter, this.mResolver, this.mCanonicalizer);
    }
    
    public void doAutoCloseInput(final boolean b) {
        this.setFlag(8192, b);
    }
    
    public void doCoalesceText(final boolean b) {
        this.setFlag(2, b);
    }
    
    public void doParseLazily(final boolean b) {
        this.setFlag(256, b);
    }
    
    public void doPreserveLocation(final boolean b) {
        this.setFlag(4096, b);
    }
    
    public void doReportCData(final boolean b) {
        this.setFlag(2048, b);
    }
    
    public int findPropertyId(final String key) {
        final Integer n = ReaderConfig.sProperties.get(key);
        int intValue;
        if (n == null) {
            intValue = -1;
        }
        else {
            intValue = n;
        }
        return intValue;
    }
    
    public void freeFullBBuffer(final byte[] array) {
        if (this._currRecycler == null) {
            this._currRecycler = this.createRecycler();
        }
        this._currRecycler.returnFullBBuffer(array);
    }
    
    public void freeFullCBuffer(final char[] array) {
        if (this._currRecycler == null) {
            this._currRecycler = this.createRecycler();
        }
        this._currRecycler.returnFullCBuffer(array);
    }
    
    public void freeMediumCBuffer(final char[] array) {
        if (this._currRecycler == null) {
            this._currRecycler = this.createRecycler();
        }
        this._currRecycler.returnMediumCBuffer(array);
    }
    
    public void freeSmallCBuffer(final char[] array) {
        if (this._currRecycler == null) {
            this._currRecycler = this.createRecycler();
        }
        this._currRecycler.returnSmallCBuffer(array);
    }
    
    @Override
    public String getActualEncoding() {
        return this.mActualEncoding;
    }
    
    public ByteBasedPNameTable getBBSymbols() {
        final String mActualEncoding = this.mActualEncoding;
        if (mActualEncoding == "UTF-8") {
            return this.mEncCtxt.getUtf8Symbols();
        }
        if (mActualEncoding == "ISO-8859-1") {
            return this.mEncCtxt.getLatin1Symbols();
        }
        if (mActualEncoding == "US-ASCII") {
            return this.mEncCtxt.getAsciiSymbols();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Internal error, unknown encoding '");
        sb.append(this.mActualEncoding);
        sb.append("'");
        throw new Error(sb.toString());
    }
    
    public CharBasedPNameTable getCBSymbols() {
        return this.mEncCtxt.getSymbols();
    }
    
    public XmlCharTypes getCharTypes() {
        final String mActualEncoding = this.mActualEncoding;
        if (mActualEncoding == "UTF-8") {
            return InputCharTypes.getUtf8CharTypes();
        }
        if (mActualEncoding == "ISO-8859-1") {
            return InputCharTypes.getLatin1CharTypes();
        }
        if (mActualEncoding == "US-ASCII") {
            return InputCharTypes.getAsciiCharTypes();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Internal error, unknown encoding '");
        sb.append(this.mActualEncoding);
        sb.append("'");
        throw new Error(sb.toString());
    }
    
    @Override
    public String getExternalEncoding() {
        return this.mExtEncoding;
    }
    
    public IllegalCharHandler getIllegalCharHandler() {
        return this.illegalCharHandler;
    }
    
    @Override
    public final Object getProperty(final String s, final boolean b) {
        final HashMap<String, Object> sProperties = ReaderConfig.sProperties;
        final Object value = sProperties.get(s);
        if (value != null) {
            boolean b2;
            if (value instanceof Boolean) {
                b2 = (boolean)value;
            }
            else {
                if (!(value instanceof Integer)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Internal error: unrecognized property value type: ");
                    sb.append(((Integer)value).getClass().getName());
                    throw new RuntimeException(sb.toString());
                }
                b2 = this.hasFlag((int)value);
            }
            return b2;
        }
        if (sProperties.containsKey(s)) {
            return null;
        }
        return super.getProperty(s, b);
    }
    
    public String getPublicId() {
        return this.mPublicId;
    }
    
    public String getSystemId() {
        return this.mSystemId;
    }
    
    public XMLReporter getXMLReporter() {
        return this.mReporter;
    }
    
    public XMLResolver getXMLResolver() {
        return this.mResolver;
    }
    
    public String getXmlDeclEncoding() {
        return this.mXmlDeclEncoding;
    }
    
    public int getXmlDeclStandalone() {
        return this.mXmlDeclStandalone;
    }
    
    public String getXmlDeclVersion() {
        return this.mXmlDeclVersion;
    }
    
    public boolean hasInternNamesBeenEnabled() {
        return this.hasExplicitFlag(512);
    }
    
    public boolean hasInternNsURIsBeenEnabled() {
        return this.hasExplicitFlag(1024);
    }
    
    @Override
    public boolean isPropertySupported(final String key) {
        return ReaderConfig.sProperties.containsKey(key) || super.isPropertySupported(key);
    }
    
    @Override
    public boolean isXml11() {
        return false;
    }
    
    public void setActualEncoding(final String mActualEncoding) {
        this.mActualEncoding = mActualEncoding;
    }
    
    public void setIllegalCharHandler(final IllegalCharHandler illegalCharHandler) {
        this.illegalCharHandler = illegalCharHandler;
    }
    
    @Override
    public boolean setProperty(final String s, final Object o) {
        final HashMap<String, Object> sProperties = ReaderConfig.sProperties;
        final Object value = sProperties.get(s);
        if (value == null) {
            return !sProperties.containsKey(s) && super.setProperty(s, o);
        }
        if (value instanceof Boolean) {
            return false;
        }
        if (value instanceof Integer) {
            this.setFlag((int)value, (boolean)o);
            return true;
        }
        throw new RuntimeException("Internal error");
    }
    
    public void setXMLReporter(final XMLReporter mReporter) {
        this.mReporter = mReporter;
    }
    
    public void setXMLResolver(final XMLResolver mResolver) {
        this.mResolver = mResolver;
    }
    
    public void setXmlDeclInfo(int mXmlDeclStandalone, final String mXmlDeclEncoding, final String s) {
        String mXmlDeclVersion;
        if (mXmlDeclStandalone == 256) {
            mXmlDeclVersion = "1.0";
        }
        else if (mXmlDeclStandalone == 272) {
            mXmlDeclVersion = "1.1";
        }
        else {
            mXmlDeclVersion = null;
        }
        this.mXmlDeclVersion = mXmlDeclVersion;
        this.mXmlDeclEncoding = mXmlDeclEncoding;
        if (s == "yes") {
            mXmlDeclStandalone = 1;
        }
        else if (s == "no") {
            mXmlDeclStandalone = 2;
        }
        else {
            mXmlDeclStandalone = 0;
        }
        this.mXmlDeclStandalone = mXmlDeclStandalone;
    }
    
    public final void setXmlEncoding(final String mXmlDeclEncoding) {
        this.mXmlDeclEncoding = mXmlDeclEncoding;
    }
    
    public final void setXmlStandalone(final Boolean b) {
        int mXmlDeclStandalone;
        if (b == null) {
            mXmlDeclStandalone = 0;
        }
        else if (b) {
            mXmlDeclStandalone = 1;
        }
        else {
            mXmlDeclStandalone = 2;
        }
        this.mXmlDeclStandalone = mXmlDeclStandalone;
    }
    
    public final void setXmlVersion(final String mXmlDeclVersion) {
        this.mXmlDeclVersion = mXmlDeclVersion;
    }
    
    public void updateBBSymbols(final ByteBasedPNameTable byteBasedPNameTable) {
        final String mActualEncoding = this.mActualEncoding;
        if (mActualEncoding == "UTF-8") {
            this.mEncCtxt.updateUtf8Symbols(byteBasedPNameTable);
        }
        else if (mActualEncoding == "ISO-8859-1") {
            this.mEncCtxt.updateLatin1Symbols(byteBasedPNameTable);
        }
        else {
            if (mActualEncoding != "US-ASCII") {
                final StringBuilder sb = new StringBuilder();
                sb.append("Internal error, unknown encoding '");
                sb.append(this.mActualEncoding);
                sb.append("'");
                throw new Error(sb.toString());
            }
            this.mEncCtxt.updateAsciiSymbols(byteBasedPNameTable);
        }
    }
    
    public void updateCBSymbols(final CharBasedPNameTable charBasedPNameTable) {
        this.mEncCtxt.updateSymbols(charBasedPNameTable);
    }
    
    public boolean willAutoCloseInput() {
        return this.hasFlag(8192);
    }
    
    public boolean willCoalesceText() {
        return this.hasFlag(2);
    }
    
    public boolean willExpandEntities() {
        return this.hasFlag(16);
    }
    
    public boolean willInternNames() {
        return this.hasFlag(512);
    }
    
    public boolean willInternNsURIs() {
        return this.hasFlag(1024);
    }
    
    public boolean willParseLazily() {
        return this.hasFlag(256);
    }
    
    public boolean willPreserveLocation() {
        return this.hasFlag(4096);
    }
    
    public boolean willReportCData() {
        return this.hasFlag(2048);
    }
    
    public boolean willSupportNamespaces() {
        return true;
    }
    
    public static final class EncodingContext
    {
        ByteBasedPNameTable mAsciiTable;
        CharBasedPNameTable mGeneralTable;
        ByteBasedPNameTable mLatin1Table;
        ByteBasedPNameTable mUtf8Table;
        
        public ByteBasedPNameTable getAsciiSymbols() {
            synchronized (this) {
                if (this.mAsciiTable == null) {
                    this.mAsciiTable = new ByteBasedPNameTable(64);
                }
                return new ByteBasedPNameTable(this.mAsciiTable);
            }
        }
        
        public ByteBasedPNameTable getLatin1Symbols() {
            synchronized (this) {
                if (this.mLatin1Table == null) {
                    this.mLatin1Table = new ByteBasedPNameTable(64);
                }
                return new ByteBasedPNameTable(this.mLatin1Table);
            }
        }
        
        public CharBasedPNameTable getSymbols() {
            synchronized (this) {
                if (this.mGeneralTable == null) {
                    this.mGeneralTable = new CharBasedPNameTable(64);
                }
                return new CharBasedPNameTable(this.mGeneralTable);
            }
        }
        
        public ByteBasedPNameTable getUtf8Symbols() {
            synchronized (this) {
                if (this.mUtf8Table == null) {
                    this.mUtf8Table = new ByteBasedPNameTable(64);
                }
                return new ByteBasedPNameTable(this.mUtf8Table);
            }
        }
        
        public void updateAsciiSymbols(final ByteBasedPNameTable byteBasedPNameTable) {
            synchronized (this) {
                this.mAsciiTable.mergeFromChild(byteBasedPNameTable);
            }
        }
        
        public void updateLatin1Symbols(final ByteBasedPNameTable byteBasedPNameTable) {
            synchronized (this) {
                this.mLatin1Table.mergeFromChild(byteBasedPNameTable);
            }
        }
        
        public void updateSymbols(final CharBasedPNameTable charBasedPNameTable) {
            synchronized (this) {
                this.mGeneralTable.mergeFromChild(charBasedPNameTable);
            }
        }
        
        public void updateUtf8Symbols(final ByteBasedPNameTable byteBasedPNameTable) {
            synchronized (this) {
                this.mUtf8Table.mergeFromChild(byteBasedPNameTable);
            }
        }
    }
}

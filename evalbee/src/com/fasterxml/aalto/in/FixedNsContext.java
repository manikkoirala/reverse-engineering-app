// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.in;

import org.codehaus.stax2.ri.EmptyIterator;
import org.codehaus.stax2.ri.SingletonIterator;
import java.util.Iterator;
import java.util.ArrayList;
import javax.xml.namespace.NamespaceContext;

public final class FixedNsContext implements NamespaceContext
{
    public static final FixedNsContext EMPTY_CONTEXT;
    protected final String[] _declarationData;
    protected final NsDeclaration _lastDeclaration;
    protected ArrayList<String> _tmpDecl;
    
    static {
        EMPTY_CONTEXT = new FixedNsContext(null, new String[0]);
    }
    
    private FixedNsContext(final NsDeclaration lastDeclaration, final String[] declarationData) {
        this._tmpDecl = null;
        this._lastDeclaration = lastDeclaration;
        this._declarationData = declarationData;
    }
    
    @Override
    public final String getNamespaceURI(final String s) {
        if (s != null) {
            if (s.length() > 0) {
                if (s.equals("xml")) {
                    return "http://www.w3.org/XML/1998/namespace";
                }
                if (s.equals("xmlns")) {
                    return "http://www.w3.org/2000/xmlns/";
                }
            }
            final String[] declarationData = this._declarationData;
            for (int length = declarationData.length, i = 0; i < length; i += 2) {
                if (s.equals(declarationData[i])) {
                    return declarationData[i + 1];
                }
            }
            return null;
        }
        throw new IllegalArgumentException("Null prefix not allowed");
    }
    
    @Override
    public final String getPrefix(final String s) {
        if (s == null || s.length() == 0) {
            throw new IllegalArgumentException("Illegal to pass null/empty prefix as argument.");
        }
        if (s.equals("http://www.w3.org/XML/1998/namespace")) {
            return "xml";
        }
        if (s.equals("http://www.w3.org/2000/xmlns/")) {
            return "xmlns";
        }
        final String[] declarationData = this._declarationData;
    Label_0111:
        for (int length = declarationData.length, i = 1; i < length; i += 2) {
            if (s.equals(declarationData[i])) {
                final int n = i - 1;
                final String s2 = declarationData[n];
                for (int j = i + 1; j < length; j += 2) {
                    if (declarationData[j] == s2) {
                        continue Label_0111;
                    }
                }
                return declarationData[n];
            }
        }
        return null;
    }
    
    @Override
    public final Iterator<String> getPrefixes(String anObject) {
        if (anObject != null && anObject.length() != 0) {
            if (anObject.equals("http://www.w3.org/XML/1998/namespace")) {
                anObject = "xml";
            }
            else if (anObject.equals("http://www.w3.org/2000/xmlns/")) {
                anObject = "xmlns";
            }
            else {
                final String[] declarationData = this._declarationData;
                final int length = declarationData.length;
                ArrayList list = null;
                int i = 1;
                String e = null;
                while (i < length) {
                    final String s = declarationData[i];
                    ArrayList list2 = null;
                    String e2 = null;
                    Label_0191: {
                        if (s != anObject) {
                            list2 = list;
                            e2 = e;
                            if (!s.equals(anObject)) {
                                break Label_0191;
                            }
                        }
                        e2 = declarationData[i - 1];
                        for (int j = i + 1; j < length; j += 2) {
                            if (declarationData[j] == e2) {
                                list2 = list;
                                e2 = e;
                                break Label_0191;
                            }
                        }
                        if (e == null) {
                            list2 = list;
                        }
                        else {
                            if ((list2 = list) == null) {
                                list2 = new ArrayList();
                                list2.add(e);
                            }
                            list2.add(e2);
                            e2 = e;
                        }
                    }
                    i += 2;
                    list = list2;
                    e = e2;
                }
                if (list != null) {
                    return list.iterator();
                }
                if (e != null) {
                    return (Iterator<String>)SingletonIterator.create((Object)e);
                }
                return EmptyIterator.getInstance();
            }
            return (Iterator<String>)SingletonIterator.create((Object)anObject);
        }
        throw new IllegalArgumentException("Illegal to pass null/empty prefix as argument.");
    }
    
    public FixedNsContext reuseOrCreate(final NsDeclaration nsDeclaration) {
        if (nsDeclaration == this._lastDeclaration) {
            return this;
        }
        if (this == FixedNsContext.EMPTY_CONTEXT) {
            final ArrayList list = new ArrayList();
            for (NsDeclaration prev = nsDeclaration; prev != null; prev = prev.getPrev()) {
                list.add(prev.getPrefix());
                list.add(prev.getCurrNsURI());
            }
            return new FixedNsContext(nsDeclaration, list.toArray(new String[list.size()]));
        }
        final ArrayList<String> tmpDecl = this._tmpDecl;
        if (tmpDecl == null) {
            this._tmpDecl = new ArrayList<String>();
        }
        else {
            tmpDecl.clear();
        }
        for (NsDeclaration prev2 = nsDeclaration; prev2 != null; prev2 = prev2.getPrev()) {
            this._tmpDecl.add(prev2.getPrefix());
            this._tmpDecl.add(prev2.getCurrNsURI());
        }
        final ArrayList<String> tmpDecl2 = this._tmpDecl;
        return new FixedNsContext(nsDeclaration, tmpDecl2.toArray(new String[tmpDecl2.size()]));
    }
    
    @Override
    public String toString() {
        if (this == FixedNsContext.EMPTY_CONTEXT) {
            return "[EMPTY non-transient NsContext]";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append('[');
        for (int length = this._declarationData.length, i = 0; i < length; i += 2) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append('\"');
            sb.append(this._declarationData[i]);
            sb.append("\"->\"");
            sb.append(this._declarationData[i + 1]);
            sb.append('\"');
        }
        sb.append(']');
        return sb.toString();
    }
}

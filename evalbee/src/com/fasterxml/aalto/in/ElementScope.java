// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.in;

public final class ElementScope
{
    PName mName;
    ElementScope mParent;
    
    public ElementScope(final PName mName, final ElementScope mParent) {
        this.mParent = mParent;
        this.mName = mName;
    }
    
    public PName getName() {
        return this.mName;
    }
    
    public ElementScope getParent() {
        return this.mParent;
    }
    
    @Override
    public String toString() {
        if (this.mParent == null) {
            return this.mName.toString();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(this.mParent.toString());
        sb.append("/");
        sb.append(this.mName);
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.in;

import java.io.InputStream;

public final class MergedStream extends InputStream
{
    final ReaderConfig mConfig;
    byte[] mData;
    final int mEnd;
    final InputStream mIn;
    int mPtr;
    
    public MergedStream(final ReaderConfig mConfig, final InputStream mIn, final byte[] mData, final int mPtr, final int mEnd) {
        this.mConfig = mConfig;
        this.mIn = mIn;
        this.mData = mData;
        this.mPtr = mPtr;
        this.mEnd = mEnd;
    }
    
    private void freeBuffers() {
        final byte[] mData = this.mData;
        if (mData != null) {
            this.mData = null;
            final ReaderConfig mConfig = this.mConfig;
            if (mConfig != null) {
                mConfig.freeFullBBuffer(mData);
            }
        }
    }
    
    @Override
    public int available() {
        if (this.mData != null) {
            return this.mEnd - this.mPtr;
        }
        return this.mIn.available();
    }
    
    @Override
    public void close() {
        this.freeBuffers();
        this.mIn.close();
    }
    
    @Override
    public void mark(final int readlimit) {
        if (this.mData == null) {
            this.mIn.mark(readlimit);
        }
    }
    
    @Override
    public boolean markSupported() {
        return this.mData == null && this.mIn.markSupported();
    }
    
    @Override
    public int read() {
        final byte[] mData = this.mData;
        if (mData != null) {
            final int mPtr = this.mPtr;
            final int mPtr2 = mPtr + 1;
            this.mPtr = mPtr2;
            final byte b = mData[mPtr];
            if (mPtr2 >= this.mEnd) {
                this.freeBuffers();
            }
            return b & 0xFF;
        }
        return this.mIn.read();
    }
    
    @Override
    public int read(final byte[] array) {
        return this.read(array, 0, array.length);
    }
    
    @Override
    public int read(final byte[] b, int n, final int len) {
        final byte[] mData = this.mData;
        if (mData != null) {
            final int mEnd = this.mEnd;
            final int mPtr = this.mPtr;
            final int n2 = mEnd - mPtr;
            int n3;
            if ((n3 = len) > n2) {
                n3 = n2;
            }
            System.arraycopy(mData, mPtr, b, n, n3);
            n = this.mPtr + n3;
            if ((this.mPtr = n) >= this.mEnd) {
                this.freeBuffers();
            }
            return n3;
        }
        return this.mIn.read(b, n, len);
    }
    
    @Override
    public void reset() {
        if (this.mData == null) {
            this.mIn.reset();
        }
    }
    
    @Override
    public long skip(long n) {
        long n5;
        if (this.mData != null) {
            final int mEnd = this.mEnd;
            final int mPtr = this.mPtr;
            final long n2 = mEnd - mPtr;
            if (n2 > n) {
                this.mPtr = mPtr + (int)n;
                return n;
            }
            this.freeBuffers();
            final long n3 = n2 + 0L;
            final long n4 = n - n2;
            n = n3;
            n5 = n4;
        }
        else {
            final long n6 = 0L;
            n5 = n;
            n = n6;
        }
        long n7 = n;
        if (n5 > 0L) {
            n7 = n + this.mIn.skip(n5);
        }
        return n7;
    }
}

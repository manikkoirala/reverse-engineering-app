// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.in;

import com.fasterxml.aalto.impl.ErrorConsts;

public final class PNameC extends PName
{
    protected final int mHash;
    
    public PNameC(final String s, final String s2, final String s3, final int mHash) {
        super(s, s2, s3);
        this.mHash = mHash;
    }
    
    public static int calcHash(final String s) {
        int char1 = s.charAt(0);
        for (int length = s.length(), i = 1; i < length; ++i) {
            char1 = char1 * 31 + s.charAt(i);
        }
        return char1;
    }
    
    public static int calcHash(final char[] array, int n, final int n2) {
        n = array[0];
        for (int i = 1; i < n2; ++i) {
            n = n * 31 + array[i];
        }
        return n;
    }
    
    public static PNameC construct(final String s) {
        return construct(s, calcHash(s));
    }
    
    public static PNameC construct(final String s, final int n) {
        final int index = s.indexOf(58);
        if (index < 0) {
            return new PNameC(s, null, s, n);
        }
        return new PNameC(s, s.substring(0, index).intern(), s.substring(index + 1).intern(), n);
    }
    
    @Override
    public PName createBoundName(final NsBinding namespaceBinding) {
        final PNameC pNameC = new PNameC(super._prefixedName, super._prefix, super._localName, this.mHash);
        pNameC._namespaceBinding = namespaceBinding;
        return pNameC;
    }
    
    public boolean equalsPName(final char[] array, final int n, final int n2, int i) {
        if (i != this.mHash) {
            return false;
        }
        final String prefixedName = super._prefixedName;
        if (n2 != prefixedName.length()) {
            return false;
        }
        for (i = 0; i < n2; ++i) {
            if (array[n + i] != prefixedName.charAt(i)) {
                return false;
            }
        }
        return true;
    }
    
    public int getCustomHash() {
        return this.mHash;
    }
    
    @Override
    public int getFirstQuad() {
        ErrorConsts.throwInternalError();
        return 0;
    }
    
    @Override
    public final int getLastQuad() {
        ErrorConsts.throwInternalError();
        return 0;
    }
    
    @Override
    public int getQuad(final int n) {
        ErrorConsts.throwInternalError();
        return 0;
    }
    
    @Override
    public int hashCode() {
        return this.mHash;
    }
    
    @Override
    public int sizeInQuads() {
        ErrorConsts.throwInternalError();
        return 0;
    }
}

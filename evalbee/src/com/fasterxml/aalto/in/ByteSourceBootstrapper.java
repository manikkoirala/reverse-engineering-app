// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.in;

import com.fasterxml.aalto.util.CharsetNames;
import com.fasterxml.aalto.impl.LocationImpl;
import javax.xml.stream.Location;
import java.io.UnsupportedEncodingException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.IOException;
import com.fasterxml.aalto.impl.IoStreamException;
import java.io.CharConversionException;
import java.io.InputStream;

public final class ByteSourceBootstrapper extends InputBootstrapper
{
    private static final byte BYTE_CR = 13;
    private static final byte BYTE_LF = 10;
    private static final byte BYTE_NULL = 0;
    final InputStream _in;
    final byte[] _inputBuffer;
    private int _inputLen;
    private int _inputPtr;
    boolean mBigEndian;
    boolean mByteSizeFound;
    int mBytesPerChar;
    boolean mHadBOM;
    
    private ByteSourceBootstrapper(final ReaderConfig readerConfig, final InputStream in) {
        super(readerConfig);
        this.mBigEndian = true;
        this.mBytesPerChar = 0;
        this.mHadBOM = false;
        this.mByteSizeFound = false;
        this._in = in;
        this._inputBuffer = readerConfig.allocFullBBuffer(4000);
        this._inputPtr = 0;
        this._inputLen = 0;
    }
    
    private ByteSourceBootstrapper(final ReaderConfig readerConfig, final byte[] inputBuffer, final int inputPtr, final int n) {
        super(readerConfig);
        this.mBigEndian = true;
        this.mBytesPerChar = 0;
        this.mHadBOM = false;
        this.mByteSizeFound = false;
        this._in = null;
        this._inputBuffer = inputBuffer;
        this._inputPtr = inputPtr;
        this._inputLen = n + inputPtr;
        super._inputProcessed = -inputPtr;
    }
    
    public static ByteSourceBootstrapper construct(final ReaderConfig readerConfig, final InputStream inputStream) {
        return new ByteSourceBootstrapper(readerConfig, inputStream);
    }
    
    public static ByteSourceBootstrapper construct(final ReaderConfig readerConfig, final byte[] array, final int n, final int n2) {
        return new ByteSourceBootstrapper(readerConfig, array, n, n2);
    }
    
    private void determineStreamEncoding() {
        final boolean ensureLoaded = this.ensureLoaded(4);
        final boolean b = false;
        if (ensureLoaded) {
            final int inputPtr = this._inputPtr;
            final byte[] inputBuffer = this._inputBuffer;
            final int n = (inputBuffer[inputPtr + 3] & 0xFF) | (inputBuffer[inputPtr] << 24 | (inputBuffer[inputPtr + 1] & 0xFF) << 16 | (inputBuffer[inputPtr + 2] & 0xFF) << 8);
            Label_0329: {
                Label_0323: {
                    if (n != -16842752) {
                        Label_0315: {
                            if (n != -131072) {
                                if (n != 65279) {
                                    Label_0286: {
                                        if (n != 65534) {
                                            final int n2 = n >>> 16;
                                            Label_0128: {
                                                Label_0123: {
                                                    if (n2 != 65279) {
                                                        Label_0154: {
                                                            Label_0149: {
                                                                if (n2 != 65534) {
                                                                    if (n >>> 8 == 15711167) {
                                                                        this._inputPtr = inputPtr + 3;
                                                                    }
                                                                    else {
                                                                        switch (n) {
                                                                            case 3932223: {
                                                                                break Label_0123;
                                                                            }
                                                                            case 1006649088: {
                                                                                break Label_0149;
                                                                            }
                                                                            case 1010792557: {
                                                                                break;
                                                                            }
                                                                            default: {
                                                                                break Label_0329;
                                                                            }
                                                                            case 1282385812: {
                                                                                this.reportEBCDIC();
                                                                                break Label_0329;
                                                                            }
                                                                            case 1006632960: {
                                                                                this.mBytesPerChar = 4;
                                                                                break Label_0154;
                                                                            }
                                                                            case 60: {
                                                                                this.mBigEndian = true;
                                                                                break Label_0315;
                                                                            }
                                                                            case 15360: {
                                                                                break Label_0286;
                                                                            }
                                                                            case 3932160: {
                                                                                break Label_0323;
                                                                            }
                                                                        }
                                                                    }
                                                                    this.mBytesPerChar = 1;
                                                                    break Label_0128;
                                                                }
                                                                this._inputPtr = inputPtr + 2;
                                                            }
                                                            this.mBytesPerChar = 2;
                                                        }
                                                        this.mBigEndian = false;
                                                        break Label_0329;
                                                    }
                                                    this._inputPtr = inputPtr + 2;
                                                }
                                                this.mBytesPerChar = 2;
                                            }
                                            this.mBigEndian = true;
                                            break Label_0329;
                                        }
                                    }
                                    this.reportWeirdUCS4("2143");
                                    break Label_0329;
                                }
                                this.mBigEndian = true;
                            }
                            else {
                                this.mBigEndian = false;
                            }
                            this._inputPtr = inputPtr + 4;
                        }
                        this.mBytesPerChar = 4;
                        break Label_0329;
                    }
                }
                this.reportWeirdUCS4("3412");
            }
            final int inputPtr2 = this._inputPtr;
            this.mHadBOM = (inputPtr2 > inputPtr);
            super._inputRowStart = inputPtr2;
        }
        boolean mByteSizeFound = b;
        if (this.mBytesPerChar > 0) {
            mByteSizeFound = true;
        }
        if (!(this.mByteSizeFound = mByteSizeFound)) {
            this.mBytesPerChar = 1;
            this.mBigEndian = true;
        }
    }
    
    private void reportEBCDIC() {
        throw new CharConversionException("Unsupported encoding (EBCDIC)");
    }
    
    private void reportWeirdUCS4(final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Unsupported UCS-4 endianness (");
        sb.append(str);
        sb.append(") detected");
        throw new CharConversionException(sb.toString());
    }
    
    private void verifyEncoding(final String str, final int i) {
        if (this.mByteSizeFound && i != this.mBytesPerChar) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Declared encoding '");
            sb.append(str);
            sb.append("' uses ");
            sb.append(i);
            sb.append(" bytes per character; but physical encoding appeared to use ");
            sb.append(this.mBytesPerChar);
            sb.append("; cannot decode");
            this.reportXmlProblem(sb.toString());
        }
    }
    
    private void verifyEncoding(final String str, final int n, final boolean b) {
        if (this.mByteSizeFound) {
            this.verifyEncoding(str, n);
            if (b != this.mBigEndian) {
                String str2;
                if (b) {
                    str2 = "big";
                }
                else {
                    str2 = "little";
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Declared encoding '");
                sb.append(str);
                sb.append("' has different endianness (");
                sb.append(str2);
                sb.append(" endian) than what physical ordering appeared to be; cannot decode");
                this.reportXmlProblem(sb.toString());
            }
        }
    }
    
    @Override
    public final XmlScanner bootstrap() {
        try {
            try {
                final XmlScanner doBootstrap = this.doBootstrap();
                super._config.freeSmallCBuffer(super.mKeyword);
                return doBootstrap;
            }
            finally {}
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
        super._config.freeSmallCBuffer(super.mKeyword);
    }
    
    @Override
    public int checkKeyword(final String s) {
        if (this.mBytesPerChar > 1) {
            return this.checkMbKeyword(s);
        }
        return this.checkSbKeyword(s);
    }
    
    public int checkMbKeyword(final String s) {
        for (int length = s.length(), i = 1; i < length; ++i) {
            final int nextMultiByte = this.nextMultiByte();
            if (nextMultiByte == 0) {
                this.reportNull();
            }
            if (nextMultiByte != s.charAt(i)) {
                return nextMultiByte;
            }
        }
        return 0;
    }
    
    public int checkSbKeyword(final String s) {
        for (int length = s.length(), i = 1; i < length; ++i) {
            final int inputPtr = this._inputPtr;
            byte nextByte;
            if (inputPtr < this._inputLen) {
                final byte[] inputBuffer = this._inputBuffer;
                this._inputPtr = inputPtr + 1;
                nextByte = inputBuffer[inputPtr];
            }
            else {
                nextByte = this.nextByte();
            }
            if (nextByte == 0) {
                this.reportNull();
            }
            final int n = nextByte & 0xFF;
            if (n != s.charAt(i)) {
                return n;
            }
        }
        return 0;
    }
    
    public XmlScanner doBootstrap() {
        this.determineStreamEncoding();
        String verifyXmlEncoding = null;
        Label_0035: {
            if (this.hasXmlDeclaration()) {
                this.readXmlDeclaration();
                final String mFoundEncoding = super.mFoundEncoding;
                if (mFoundEncoding != null) {
                    verifyXmlEncoding = this.verifyXmlEncoding(mFoundEncoding);
                    break Label_0035;
                }
            }
            verifyXmlEncoding = null;
        }
        final String s = "UTF-16BE";
        String actualEncoding = verifyXmlEncoding;
        if (verifyXmlEncoding == null) {
            final int mBytesPerChar = this.mBytesPerChar;
            if (mBytesPerChar == 2) {
                if (this.mBigEndian) {
                    actualEncoding = "UTF-16BE";
                }
                else {
                    actualEncoding = "UTF-16LE";
                }
            }
            else if (mBytesPerChar == 4) {
                if (this.mBigEndian) {
                    actualEncoding = "UTF-32BE";
                }
                else {
                    actualEncoding = "UTF-32LE";
                }
            }
            else {
                actualEncoding = "UTF-8";
            }
        }
        super._config.setActualEncoding(actualEncoding);
        super._config.setXmlDeclInfo(super.mDeclaredXmlVersion, super.mFoundEncoding, super.mStandalone);
        if (actualEncoding != "UTF-8" && actualEncoding != "ISO-8859-1") {
            if (actualEncoding != "US-ASCII") {
                if (actualEncoding.startsWith("UTF-32")) {
                    return new ReaderScanner(super._config, new Utf32Reader(super._config, this._in, this._inputBuffer, this._inputPtr, this._inputLen, this.mBigEndian));
                }
                InputStream in = this._in;
                if (this._inputPtr < this._inputLen) {
                    in = new MergedStream(super._config, in, this._inputBuffer, this._inputPtr, this._inputLen);
                }
                String charsetName;
                if ((charsetName = actualEncoding) == "UTF-16") {
                    String s2;
                    if (this.mBigEndian) {
                        s2 = s;
                    }
                    else {
                        s2 = "UTF-16LE";
                    }
                    charsetName = s2;
                }
                try {
                    return new ReaderScanner(super._config, new InputStreamReader(in, charsetName));
                }
                catch (final UnsupportedEncodingException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unsupported encoding: ");
                    sb.append(ex.getMessage());
                    throw new IoStreamException(sb.toString());
                }
            }
        }
        return new Utf8Scanner(super._config, this._in, this._inputBuffer, this._inputPtr, this._inputLen);
    }
    
    public boolean ensureLoaded(final int n) {
        int read;
        for (int i = this._inputLen - this._inputPtr; i < n; i += read) {
            final InputStream in = this._in;
            if (in == null) {
                read = -1;
            }
            else {
                final byte[] inputBuffer = this._inputBuffer;
                final int inputLen = this._inputLen;
                read = in.read(inputBuffer, inputLen, inputBuffer.length - inputLen);
            }
            if (read < 1) {
                return false;
            }
            this._inputLen += read;
        }
        return true;
    }
    
    @Override
    public Location getLocation() {
        final int inputProcessed = super._inputProcessed;
        final int inputPtr = this._inputPtr;
        final int n = inputProcessed + inputPtr;
        final int n2 = inputPtr - super._inputRowStart;
        final int mBytesPerChar = this.mBytesPerChar;
        int n3 = n;
        int n4 = n2;
        if (mBytesPerChar > 1) {
            n3 = n / mBytesPerChar;
            n4 = n2 / mBytesPerChar;
        }
        return (Location)LocationImpl.fromZeroBased(super._config.getPublicId(), super._config.getSystemId(), n3, super._inputRow, n4);
    }
    
    @Override
    public int getNext() {
        if (this.mBytesPerChar > 1) {
            return this.nextMultiByte();
        }
        final int inputPtr = this._inputPtr;
        byte nextByte;
        if (inputPtr < this._inputLen) {
            final byte[] inputBuffer = this._inputBuffer;
            this._inputPtr = inputPtr + 1;
            nextByte = inputBuffer[inputPtr];
        }
        else {
            nextByte = this.nextByte();
        }
        return nextByte & 0xFF;
    }
    
    @Override
    public int getNextAfterWs(final boolean b) {
        int n;
        if (this.mBytesPerChar > 1) {
            n = this.skipMbWs();
        }
        else {
            n = this.skipSbWs();
        }
        if (b && n == 0) {
            this.reportUnexpectedChar(this.getNext(), "; expected a white space");
        }
        if (this.mBytesPerChar > 1) {
            return this.nextMultiByte();
        }
        final int inputPtr = this._inputPtr;
        byte nextByte;
        if (inputPtr < this._inputLen) {
            final byte[] inputBuffer = this._inputBuffer;
            this._inputPtr = inputPtr + 1;
            nextByte = inputBuffer[inputPtr];
        }
        else {
            nextByte = this.nextByte();
        }
        return nextByte & 0xFF;
    }
    
    public boolean hasXmlDeclaration() {
        final int mBytesPerChar = this.mBytesPerChar;
        if (mBytesPerChar == 1) {
            if (this.ensureLoaded(6)) {
                final byte[] inputBuffer = this._inputBuffer;
                final int inputPtr = this._inputPtr;
                if (inputBuffer[inputPtr] == 60 && inputBuffer[inputPtr + 1] == 63 && inputBuffer[inputPtr + 2] == 120 && inputBuffer[inputPtr + 3] == 109 && inputBuffer[inputPtr + 4] == 108 && (inputBuffer[inputPtr + 5] & 0xFF) <= 32) {
                    this._inputPtr = inputPtr + 6;
                    return true;
                }
            }
        }
        else if (this.ensureLoaded(mBytesPerChar * 6)) {
            final int inputPtr2 = this._inputPtr;
            if (this.nextMultiByte() == 60 && this.nextMultiByte() == 63 && this.nextMultiByte() == 120 && this.nextMultiByte() == 109 && this.nextMultiByte() == 108 && this.nextMultiByte() <= 32) {
                return true;
            }
            this._inputPtr = inputPtr2;
        }
        return false;
    }
    
    public void loadMore() {
        final int inputProcessed = super._inputProcessed;
        final int inputLen = this._inputLen;
        super._inputProcessed = inputProcessed + inputLen;
        super._inputRowStart -= inputLen;
        this._inputPtr = 0;
        final InputStream in = this._in;
        int read;
        if (in == null) {
            read = -1;
        }
        else {
            final byte[] inputBuffer = this._inputBuffer;
            read = in.read(inputBuffer, 0, inputBuffer.length);
        }
        this._inputLen = read;
        if (this._inputLen < 1) {
            this.reportEof();
        }
    }
    
    public byte nextByte() {
        if (this._inputPtr >= this._inputLen) {
            this.loadMore();
        }
        return this._inputBuffer[this._inputPtr++];
    }
    
    public int nextMultiByte() {
        final int inputPtr = this._inputPtr;
        byte nextByte;
        if (inputPtr < this._inputLen) {
            final byte[] inputBuffer = this._inputBuffer;
            this._inputPtr = inputPtr + 1;
            nextByte = inputBuffer[inputPtr];
        }
        else {
            nextByte = this.nextByte();
        }
        final int inputPtr2 = this._inputPtr;
        byte nextByte2;
        if (inputPtr2 < this._inputLen) {
            final byte[] inputBuffer2 = this._inputBuffer;
            this._inputPtr = inputPtr2 + 1;
            nextByte2 = inputBuffer2[inputPtr2];
        }
        else {
            nextByte2 = this.nextByte();
        }
        int n3 = 0;
        Label_0287: {
            int n;
            int n2;
            if (this.mBytesPerChar == 2) {
                final boolean mBigEndian = this.mBigEndian;
                n = (nextByte & 0xFF);
                if (mBigEndian) {
                    n <<= 8;
                    n2 = (nextByte2 & 0xFF);
                }
                else {
                    n2 = (nextByte2 & 0xFF) << 8;
                }
            }
            else {
                final int inputPtr3 = this._inputPtr;
                byte nextByte3;
                if (inputPtr3 < this._inputLen) {
                    final byte[] inputBuffer3 = this._inputBuffer;
                    this._inputPtr = inputPtr3 + 1;
                    nextByte3 = inputBuffer3[inputPtr3];
                }
                else {
                    nextByte3 = this.nextByte();
                }
                final int inputPtr4 = this._inputPtr;
                byte nextByte4;
                if (inputPtr4 < this._inputLen) {
                    final byte[] inputBuffer4 = this._inputBuffer;
                    this._inputPtr = inputPtr4 + 1;
                    nextByte4 = inputBuffer4[inputPtr4];
                }
                else {
                    nextByte4 = this.nextByte();
                }
                if (!this.mBigEndian) {
                    n3 = ((nextByte & 0xFF) | ((nextByte2 & 0xFF) << 8 | ((nextByte3 & 0xFF) << 16 | nextByte4 << 24)));
                    break Label_0287;
                }
                n = (nextByte << 24 | (nextByte2 & 0xFF) << 16 | (nextByte3 & 0xFF) << 8);
                n2 = (nextByte4 & 0xFF);
            }
            n3 = (n | n2);
        }
        if (n3 == 0) {
            this.reportNull();
        }
        return n3;
    }
    
    @Override
    public void pushback() {
        this._inputPtr -= this.mBytesPerChar;
    }
    
    @Override
    public int readQuotedValue(final char[] array, final int n) {
        final int length = array.length;
        final int mBytesPerChar = this.mBytesPerChar;
        int i = 0;
        boolean b = true;
        if (mBytesPerChar <= 1) {
            b = false;
        }
        while (i < length) {
            int n2 = 10;
            if (b) {
                final int nextMultiByte = this.nextMultiByte();
                if (nextMultiByte != 13 && nextMultiByte != 10) {
                    n2 = nextMultiByte;
                }
                else {
                    this.skipMbLF(nextMultiByte);
                }
            }
            else {
                final int inputPtr = this._inputPtr;
                byte nextByte;
                if (inputPtr < this._inputLen) {
                    final byte[] inputBuffer = this._inputBuffer;
                    this._inputPtr = inputPtr + 1;
                    nextByte = inputBuffer[inputPtr];
                }
                else {
                    nextByte = this.nextByte();
                }
                if (nextByte == 0) {
                    this.reportNull();
                }
                if (nextByte != 13 && nextByte != 10) {
                    n2 = nextByte;
                }
                else {
                    this.skipSbLF(nextByte);
                }
                n2 &= 0xFF;
            }
            if (n2 == n) {
                if (i >= length) {
                    i = -1;
                }
                return i;
            }
            if (i >= length) {
                continue;
            }
            array[i] = (char)n2;
            ++i;
        }
        return -1;
    }
    
    public void skipMbLF(final int n) {
        if (n == 13 && this.nextMultiByte() != 10) {
            this._inputPtr -= this.mBytesPerChar;
        }
        ++super._inputRow;
        super._inputRowStart = this._inputPtr;
    }
    
    public int skipMbWs() {
        int n = 0;
        while (true) {
            final int nextMultiByte = this.nextMultiByte();
            if (nextMultiByte > 32) {
                break;
            }
            if (nextMultiByte != 13 && nextMultiByte != 10) {
                if (nextMultiByte == 0) {
                    this.reportNull();
                }
            }
            else {
                this.skipMbLF(nextMultiByte);
            }
            ++n;
        }
        this._inputPtr -= this.mBytesPerChar;
        return n;
    }
    
    public void skipSbLF(final byte b) {
        if (b == 13) {
            final int inputPtr = this._inputPtr;
            byte nextByte;
            if (inputPtr < this._inputLen) {
                final byte[] inputBuffer = this._inputBuffer;
                this._inputPtr = inputPtr + 1;
                nextByte = inputBuffer[inputPtr];
            }
            else {
                nextByte = this.nextByte();
            }
            if (nextByte != 10) {
                --this._inputPtr;
            }
        }
        ++super._inputRow;
        super._inputRowStart = this._inputPtr;
    }
    
    public int skipSbWs() {
        int n = 0;
        while (true) {
            final int inputPtr = this._inputPtr;
            byte nextByte;
            if (inputPtr < this._inputLen) {
                final byte[] inputBuffer = this._inputBuffer;
                this._inputPtr = inputPtr + 1;
                nextByte = inputBuffer[inputPtr];
            }
            else {
                nextByte = this.nextByte();
            }
            if ((nextByte & 0xFF) > 32) {
                break;
            }
            if (nextByte != 13 && nextByte != 10) {
                if (nextByte == 0) {
                    this.reportNull();
                }
            }
            else {
                this.skipSbLF(nextByte);
            }
            ++n;
        }
        --this._inputPtr;
        return n;
    }
    
    public String verifyXmlEncoding(String normalize) {
        normalize = CharsetNames.normalize(normalize);
        if (normalize != "UTF-8") {
            if (normalize != "ISO-8859-1") {
                if (normalize != "US-ASCII") {
                    int n = 2;
                    Label_0047: {
                        if (normalize != "UTF-16") {
                            Label_0062: {
                                if (normalize != "UTF-16LE") {
                                    if (normalize != "UTF-16BE") {
                                        n = 4;
                                        if (normalize == "UTF-32") {
                                            break Label_0047;
                                        }
                                        if (normalize == "UTF-32LE") {
                                            break Label_0062;
                                        }
                                        if (normalize != "UTF-32BE") {
                                            return normalize;
                                        }
                                    }
                                    this.verifyEncoding(normalize, n, true);
                                    return normalize;
                                }
                            }
                            this.verifyEncoding(normalize, n, false);
                            return normalize;
                        }
                    }
                    this.verifyEncoding(normalize, n);
                    return normalize;
                }
            }
        }
        this.verifyEncoding(normalize, 1);
        return normalize;
    }
}

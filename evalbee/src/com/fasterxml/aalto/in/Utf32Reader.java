// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.in;

import java.io.IOException;
import java.io.CharConversionException;
import java.io.InputStream;
import java.io.Reader;

public final class Utf32Reader extends Reader
{
    static final char NULL_CHAR = '\0';
    final boolean mBigEndian;
    protected byte[] mBuffer;
    int mByteCount;
    int mCharCount;
    final ReaderConfig mConfig;
    protected InputStream mIn;
    protected int mLength;
    protected int mPtr;
    char mSurrogate;
    char[] mTmpBuf;
    
    public Utf32Reader(final ReaderConfig mConfig, final InputStream inputStream, final byte[] array, final int n, final int n2, final boolean mBigEndian) {
        this.mSurrogate = '\0';
        this.mCharCount = 0;
        this.mByteCount = 0;
        this.mTmpBuf = null;
        this.mConfig = mConfig;
        this.mBigEndian = mBigEndian;
    }
    
    private boolean loadMore(int off) {
        this.mByteCount += this.mLength - off;
        int mLength;
        if (off > 0) {
            mLength = off;
            if (this.mPtr > 0) {
                for (int i = 0; i < off; ++i) {
                    final byte[] mBuffer = this.mBuffer;
                    mBuffer[i] = mBuffer[this.mPtr + i];
                }
                this.mPtr = 0;
                mLength = off;
            }
        }
        else {
            this.mPtr = 0;
            off = this.mIn.read(this.mBuffer);
            if ((mLength = off) < 1) {
                this.mLength = 0;
                if (off < 0) {
                    this.freeBuffers();
                    return false;
                }
                this.reportStrangeStream();
                mLength = off;
            }
        }
        this.mLength = mLength;
        while (true) {
            off = this.mLength;
            if (off >= 4) {
                break;
            }
            final InputStream mIn = this.mIn;
            final byte[] mBuffer2 = this.mBuffer;
            off = mIn.read(mBuffer2, off, mBuffer2.length - off);
            if (off < 1) {
                if (off < 0) {
                    this.freeBuffers();
                    this.reportUnexpectedEOF(this.mLength, 4);
                }
                this.reportStrangeStream();
            }
            this.mLength += off;
        }
        return true;
    }
    
    private void reportInvalid(final int i, final int n, final String str) {
        final int mByteCount = this.mByteCount;
        final int mPtr = this.mPtr;
        final int mCharCount = this.mCharCount;
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid UTF-32 character 0x");
        sb.append(Integer.toHexString(i));
        sb.append(str);
        sb.append(" at char #");
        sb.append(mCharCount + n);
        sb.append(", byte #");
        sb.append(mByteCount + mPtr - 1);
        sb.append(")");
        throw new CharConversionException(sb.toString());
    }
    
    private void reportUnexpectedEOF(final int i, final int j) {
        final int mByteCount = this.mByteCount;
        final int mCharCount = this.mCharCount;
        final StringBuilder sb = new StringBuilder();
        sb.append("Unexpected EOF in the middle of a 4-byte UTF-32 char: got ");
        sb.append(i);
        sb.append(", needed ");
        sb.append(j);
        sb.append(", at char #");
        sb.append(mCharCount);
        sb.append(", byte #");
        sb.append(mByteCount + i);
        sb.append(")");
        throw new CharConversionException(sb.toString());
    }
    
    @Override
    public void close() {
        final InputStream mIn = this.mIn;
        if (mIn != null) {
            this.mIn = null;
            this.freeBuffers();
            mIn.close();
        }
    }
    
    public final void freeBuffers() {
        final byte[] mBuffer = this.mBuffer;
        if (mBuffer != null) {
            this.mBuffer = null;
            final ReaderConfig mConfig = this.mConfig;
            if (mConfig != null) {
                mConfig.freeFullBBuffer(mBuffer);
            }
        }
    }
    
    @Override
    public int read() {
        if (this.mTmpBuf == null) {
            this.mTmpBuf = new char[1];
        }
        if (this.read(this.mTmpBuf, 0, 1) < 1) {
            return -1;
        }
        return this.mTmpBuf[0];
    }
    
    @Override
    public int read(final char[] array, int n, int n2) {
        if (this.mBuffer == null) {
            return -1;
        }
        if (n2 < 1) {
            return n2;
        }
        if (n < 0 || n + n2 > array.length) {
            this.reportBounds(array, n, n2);
        }
        final int n3 = n2 + n;
        final char mSurrogate = this.mSurrogate;
        if (mSurrogate != '\0') {
            n2 = n + 1;
            array[n] = mSurrogate;
            this.mSurrogate = '\0';
        }
        else {
            n2 = this.mLength - this.mPtr;
            if (n2 < 4 && !this.loadMore(n2)) {
                return -1;
            }
            n2 = n;
        }
        final byte[] mBuffer = this.mBuffer;
        int n4;
        while (true) {
            n4 = n2;
            if (n2 >= n3) {
                break;
            }
            final int mPtr = this.mPtr;
            int n5;
            int n6;
            if (this.mBigEndian) {
                n5 = (mBuffer[mPtr] << 24 | (mBuffer[mPtr + 1] & 0xFF) << 16 | (mBuffer[mPtr + 2] & 0xFF) << 8);
                n6 = (mBuffer[mPtr + 3] & 0xFF);
            }
            else {
                n5 = ((mBuffer[mPtr] & 0xFF) | (mBuffer[mPtr + 1] & 0xFF) << 8 | (mBuffer[mPtr + 2] & 0xFF) << 16);
                n6 = mBuffer[mPtr + 3] << 24;
            }
            final int n7 = n5 | n6;
            this.mPtr = mPtr + 4;
            n4 = n2;
            int n8 = n7;
            Label_0442: {
                if (n7 >= 55296) {
                    if (n7 > 1114111) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("(above ");
                        sb.append(Integer.toHexString(1114111));
                        sb.append(") ");
                        this.reportInvalid(n7, n2 - n, sb.toString());
                    }
                    if (n7 > 65535) {
                        final int n9 = n7 - 65536;
                        n4 = n2 + 1;
                        array[n2] = (char)((n9 >> 10) + 55296);
                        n8 = ((n9 & 0x3FF) | 0xDC00);
                        if (n4 >= n3) {
                            this.mSurrogate = (char)n8;
                            break;
                        }
                    }
                    else {
                        int n10;
                        String s;
                        if (n7 < 57344) {
                            n10 = n2 - n;
                            s = "(a surrogate char) ";
                        }
                        else {
                            n4 = n2;
                            if ((n8 = n7) < 65534) {
                                break Label_0442;
                            }
                            n10 = n2 - n;
                            s = "";
                        }
                        this.reportInvalid(n7, n10, s);
                        n4 = n2;
                        n8 = n7;
                    }
                }
            }
            n2 = n4 + 1;
            array[n4] = (char)n8;
            if (this.mPtr >= this.mLength) {
                n4 = n2;
                break;
            }
        }
        n = n4 - n;
        this.mCharCount += n;
        return n;
    }
    
    public void reportBounds(final char[] array, final int i, final int j) {
        final StringBuilder sb = new StringBuilder();
        sb.append("read(buf,");
        sb.append(i);
        sb.append(",");
        sb.append(j);
        sb.append("), cbuf[");
        sb.append(array.length);
        sb.append("]");
        throw new ArrayIndexOutOfBoundsException(sb.toString());
    }
    
    public void reportStrangeStream() {
        throw new IOException("Strange I/O stream, returned 0 bytes on read");
    }
}

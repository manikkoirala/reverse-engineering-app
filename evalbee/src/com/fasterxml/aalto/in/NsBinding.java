// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.in;

final class NsBinding
{
    public static final NsBinding XMLNS_BINDING;
    public static final NsBinding XML_BINDING;
    public final String mPrefix;
    public String mURI;
    
    static {
        XML_BINDING = new NsBinding("xml", "http://www.w3.org/XML/1998/namespace", null);
        XMLNS_BINDING = new NsBinding("xmlns", "http://www.w3.org/2000/xmlns/", null);
    }
    
    public NsBinding(final String s) {
        if (s != "xml" && s != "xmlns") {
            this.mPrefix = s;
            this.mURI = null;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Trying to create non-singleton binding for ns prefix '");
        sb.append(s);
        sb.append("'");
        throw new RuntimeException(sb.toString());
    }
    
    private NsBinding(final String mPrefix, final String muri, final Object o) {
        this.mPrefix = mPrefix;
        this.mURI = muri;
    }
    
    public static final NsBinding createDefaultNs() {
        return new NsBinding(null);
    }
    
    public boolean isImmutable() {
        return this == NsBinding.XML_BINDING || this == NsBinding.XMLNS_BINDING;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.in;

public final class EntityNames
{
    public static final PNameC ENTITY_AMP;
    public static final int ENTITY_AMP_QUAD;
    public static final PNameC ENTITY_APOS;
    public static final int ENTITY_APOS_QUAD;
    public static final PNameC ENTITY_GT;
    public static final int ENTITY_GT_QUAD;
    public static final PNameC ENTITY_LT;
    public static final int ENTITY_LT_QUAD;
    public static final PNameC ENTITY_QUOT;
    public static final int ENTITY_QUOT_QUAD;
    
    static {
        ENTITY_AMP = PNameC.construct("amp");
        ENTITY_APOS = PNameC.construct("apos");
        ENTITY_GT = PNameC.construct("gt");
        ENTITY_LT = PNameC.construct("lt");
        ENTITY_QUOT = PNameC.construct("quot");
        ENTITY_AMP_QUAD = 6385008;
        ENTITY_APOS_QUAD = 1634758515;
        ENTITY_GT_QUAD = 26484;
        ENTITY_LT_QUAD = 27764;
        ENTITY_QUOT_QUAD = 1903521652;
    }
    
    private EntityNames() {
    }
}

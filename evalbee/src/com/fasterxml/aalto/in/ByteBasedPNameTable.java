// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.in;

import com.fasterxml.aalto.util.NameTable;

public final class ByteBasedPNameTable extends NameTable
{
    static final int INITIAL_COLLISION_LEN = 32;
    static final int LAST_VALID_BUCKET = 254;
    static final int MIN_HASH_SIZE = 16;
    private int mCollCount;
    private int mCollEnd;
    private Bucket[] mCollList;
    private boolean mCollListShared;
    private int mCount;
    private int[] mMainHash;
    private int mMainHashMask;
    private boolean mMainHashShared;
    private ByteBasedPName[] mMainNames;
    private boolean mMainNamesShared;
    private transient boolean mNeedRehash;
    
    public ByteBasedPNameTable(final int n) {
        int n2 = 16;
        int n3;
        if (n < 16) {
            n3 = n2;
        }
        else {
            n3 = n;
            if ((n - 1 & n) != 0x0) {
                while ((n3 = n2) < n) {
                    n2 += n2;
                }
            }
        }
        this.mCount = 0;
        this.mMainHashShared = false;
        this.mMainNamesShared = false;
        this.mMainHashMask = n3 - 1;
        this.mMainHash = new int[n3];
        this.mMainNames = new ByteBasedPName[n3];
        this.mCollListShared = true;
        this.mCollList = null;
        this.mCollEnd = 0;
        this.mNeedRehash = false;
    }
    
    public ByteBasedPNameTable(final ByteBasedPNameTable byteBasedPNameTable) {
        this.mCount = byteBasedPNameTable.mCount;
        this.mMainHashMask = byteBasedPNameTable.mMainHashMask;
        this.mMainHash = byteBasedPNameTable.mMainHash;
        this.mMainNames = byteBasedPNameTable.mMainNames;
        this.mCollList = byteBasedPNameTable.mCollList;
        this.mCollCount = byteBasedPNameTable.mCollCount;
        this.mCollEnd = byteBasedPNameTable.mCollEnd;
        this.mNeedRehash = false;
        this.mMainHashShared = true;
        this.mMainNamesShared = true;
        this.mCollListShared = true;
    }
    
    public static final int calcHash(int n) {
        n *= 31;
        n ^= n >>> 16;
        return n ^ n >>> 8;
    }
    
    public static final int calcHash(int n, final int n2) {
        n = n * 31 + n2;
        n ^= n >>> 16;
        return n ^ n >>> 8;
    }
    
    public static final int calcHash(final int[] array, int n) {
        int n2 = array[0];
        for (int i = 1; i < n; ++i) {
            n2 = n2 * 31 + array[i];
        }
        n = (n2 >>> 16 ^ n2);
        return n ^ n >>> 8;
    }
    
    public static int[] calcQuads(final byte[] array) {
        final int length = array.length;
        final int[] array2 = new int[(length + 3) / 4];
        int n3;
        for (int i = 0; i < length; i = n3 + 1) {
            final int n = array[i] & 0xFF;
            final int n2 = n3 = i + 1;
            int n4 = n;
            if (n2 < length) {
                final int n5 = n << 8 | (array[n2] & 0xFF);
                int n6 = n3 = n2 + 1;
                n4 = n5;
                if (n6 < length) {
                    final int n7 = n5 << 8 | (array[n6] & 0xFF);
                    n3 = ++n6;
                    n4 = n7;
                    if (n6 < length) {
                        n4 = (n7 << 8 | (array[n6] & 0xFF));
                        n3 = n6;
                    }
                }
            }
            array2[n3 >> 2] = n4;
        }
        return array2;
    }
    
    private void doAddSymbol(int n, final ByteBasedPName byteBasedPName) {
        if (this.mMainHashShared) {
            this.unshareMain();
        }
        if (this.mNeedRehash) {
            this.rehash();
        }
        ++this.mCount;
        final int n2 = this.mMainHashMask & n;
        if (this.mMainNames[n2] == null) {
            this.mMainHash[n2] = n << 8;
            if (this.mMainNamesShared) {
                this.unshareNames();
            }
            this.mMainNames[n2] = byteBasedPName;
        }
        else {
            if (this.mCollListShared) {
                this.unshareCollision();
            }
            ++this.mCollCount;
            final int n3 = this.mMainHash[n2];
            n = (n3 & 0xFF);
            if (n == 0) {
                final int mCollEnd = this.mCollEnd;
                if (mCollEnd <= 254) {
                    this.mCollEnd = mCollEnd + 1;
                    if ((n = mCollEnd) >= this.mCollList.length) {
                        this.expandCollision();
                        n = mCollEnd;
                    }
                }
                else {
                    n = this.findBestBucket();
                }
                this.mMainHash[n2] = ((n3 & 0xFFFFFF00) | n + 1);
            }
            else {
                --n;
            }
            final Bucket[] mCollList = this.mCollList;
            mCollList[n] = new Bucket(byteBasedPName, mCollList[n]);
        }
        n = this.mMainHash.length;
        final int mCount = this.mCount;
        if (mCount > n >> 1) {
            final int n4 = n >> 2;
            if (mCount > n - n4 || this.mCollCount >= n4) {
                this.mNeedRehash = true;
            }
        }
    }
    
    private void expandCollision() {
        final Bucket[] mCollList = this.mCollList;
        final int length = mCollList.length;
        System.arraycopy(mCollList, 0, this.mCollList = new Bucket[length + length], 0, length);
    }
    
    private int findBestBucket() {
        final Bucket[] mCollList = this.mCollList;
        final int mCollEnd = this.mCollEnd;
        int n = Integer.MAX_VALUE;
        int n2 = -1;
        int n3;
        for (int i = 0; i < mCollEnd; ++i, n = n3) {
            final int length = mCollList[i].length();
            if (length < (n3 = n)) {
                if (length == 1) {
                    return i;
                }
                n2 = i;
                n3 = length;
            }
        }
        return n2;
    }
    
    private void rehash() {
        final int n = 0;
        this.mNeedRehash = false;
        this.mMainNamesShared = false;
        final int length = this.mMainHash.length;
        final int n2 = length + length;
        this.mMainHash = new int[n2];
        this.mMainHashMask = n2 - 1;
        final ByteBasedPName[] mMainNames = this.mMainNames;
        this.mMainNames = new ByteBasedPName[n2];
        int i = 0;
        int j = 0;
        while (i < length) {
            final ByteBasedPName byteBasedPName = mMainNames[i];
            int n3 = j;
            if (byteBasedPName != null) {
                n3 = j + 1;
                final int hashCode = byteBasedPName.hashCode();
                final int n4 = this.mMainHashMask & hashCode;
                this.mMainNames[n4] = byteBasedPName;
                this.mMainHash[n4] = hashCode << 8;
            }
            ++i;
            j = n3;
        }
        final int mCollEnd = this.mCollEnd;
        if (mCollEnd == 0) {
            return;
        }
        this.mCollCount = 0;
        this.mCollEnd = 0;
        this.mCollListShared = false;
        final Bucket[] mCollList = this.mCollList;
        this.mCollList = new Bucket[mCollList.length];
        for (int k = n; k < mCollEnd; ++k) {
            int n5;
            for (Bucket mNext = mCollList[k]; mNext != null; mNext = mNext.mNext, j = n5) {
                n5 = j + 1;
                final ByteBasedPName mName = mNext.mName;
                final int hashCode2 = mName.hashCode();
                final int n6 = this.mMainHashMask & hashCode2;
                final int[] mMainHash = this.mMainHash;
                final int n7 = mMainHash[n6];
                final ByteBasedPName[] mMainNames2 = this.mMainNames;
                if (mMainNames2[n6] == null) {
                    mMainHash[n6] = hashCode2 << 8;
                    mMainNames2[n6] = mName;
                }
                else {
                    ++this.mCollCount;
                    int bestBucket = n7 & 0xFF;
                    if (bestBucket == 0) {
                        final int mCollEnd2 = this.mCollEnd;
                        if (mCollEnd2 <= 254) {
                            this.mCollEnd = mCollEnd2 + 1;
                            if ((bestBucket = mCollEnd2) >= this.mCollList.length) {
                                this.expandCollision();
                                bestBucket = mCollEnd2;
                            }
                        }
                        else {
                            bestBucket = this.findBestBucket();
                        }
                        this.mMainHash[n6] = ((n7 & 0xFFFFFF00) | bestBucket + 1);
                    }
                    else {
                        --bestBucket;
                    }
                    final Bucket[] mCollList2 = this.mCollList;
                    mCollList2[bestBucket] = new Bucket(mName, mCollList2[bestBucket]);
                }
            }
        }
        if (j == this.mCount) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Internal error: count after rehash ");
        sb.append(j);
        sb.append("; should be ");
        sb.append(this.mCount);
        throw new Error(sb.toString());
    }
    
    private void unshareCollision() {
        final Bucket[] mCollList = this.mCollList;
        if (mCollList == null) {
            this.mCollList = new Bucket[32];
        }
        else {
            final int length = mCollList.length;
            System.arraycopy(mCollList, 0, this.mCollList = new Bucket[length], 0, length);
        }
        this.mCollListShared = false;
    }
    
    private void unshareMain() {
        final int[] mMainHash = this.mMainHash;
        final int length = mMainHash.length;
        System.arraycopy(mMainHash, 0, this.mMainHash = new int[length], 0, length);
        this.mMainHashShared = false;
    }
    
    private void unshareNames() {
        final ByteBasedPName[] mMainNames = this.mMainNames;
        final int length = mMainNames.length;
        System.arraycopy(mMainNames, 0, this.mMainNames = new ByteBasedPName[length], 0, length);
        this.mMainNamesShared = false;
    }
    
    public ByteBasedPName addSymbol(final int n, final String s, final int n2, final int n3, final int n4) {
        final ByteBasedPName constructPName = ByteBasedPNameFactory.getInstance().constructPName(n, s, n2, n3, n4);
        this.doAddSymbol(n, constructPName);
        return constructPName;
    }
    
    public ByteBasedPName addSymbol(final int n, final String s, final int n2, final int[] array, final int n3) {
        final ByteBasedPName constructPName = ByteBasedPNameFactory.getInstance().constructPName(n, s, n2, array, n3);
        this.doAddSymbol(n, constructPName);
        return constructPName;
    }
    
    public ByteBasedPName findSymbol(final int n, final int n2, final int n3) {
        final int n4 = this.mMainHashMask & n;
        final int n5 = this.mMainHash[n4];
        if ((n5 >> 8 ^ n) << 8 == 0) {
            final ByteBasedPName byteBasedPName = this.mMainNames[n4];
            if (byteBasedPName == null) {
                return null;
            }
            if (byteBasedPName.equals(n2, n3)) {
                return byteBasedPName;
            }
        }
        else if (n5 == 0) {
            return null;
        }
        final int n6 = n5 & 0xFF;
        if (n6 > 0) {
            final Bucket bucket = this.mCollList[n6 - 1];
            if (bucket != null) {
                return bucket.find(n, n2, n3);
            }
        }
        return null;
    }
    
    public ByteBasedPName findSymbol(final int n, final int[] array, int n2) {
        if (n2 < 3) {
            final int n3 = 0;
            final int n4 = array[0];
            if (n2 < 2) {
                n2 = n3;
            }
            else {
                n2 = array[1];
            }
            return this.findSymbol(n, n4, n2);
        }
        final int n5 = this.mMainHashMask & n;
        final int n6 = this.mMainHash[n5];
        if ((n6 >> 8 ^ n) << 8 == 0) {
            final ByteBasedPName byteBasedPName = this.mMainNames[n5];
            if (byteBasedPName == null) {
                return null;
            }
            if (byteBasedPName.equals(array, n2)) {
                return byteBasedPName;
            }
        }
        else if (n6 == 0) {
            return null;
        }
        final int n7 = n6 & 0xFF;
        if (n7 > 0) {
            final Bucket bucket = this.mCollList[n7 - 1];
            if (bucket != null) {
                return bucket.find(n, array, n2);
            }
        }
        return null;
    }
    
    public void markAsShared() {
        this.mMainHashShared = true;
        this.mMainNamesShared = true;
        this.mCollListShared = true;
    }
    
    @Override
    public boolean maybeDirty() {
        return this.mMainHashShared ^ true;
    }
    
    public boolean mergeFromChild(final ByteBasedPNameTable byteBasedPNameTable) {
        final int mCount = byteBasedPNameTable.mCount;
        if (mCount <= this.mCount) {
            return false;
        }
        this.mCount = mCount;
        this.mMainHashMask = byteBasedPNameTable.mMainHashMask;
        this.mMainHash = byteBasedPNameTable.mMainHash;
        this.mMainNames = byteBasedPNameTable.mMainNames;
        this.mCollList = byteBasedPNameTable.mCollList;
        this.mCollCount = byteBasedPNameTable.mCollCount;
        this.mCollEnd = byteBasedPNameTable.mCollEnd;
        byteBasedPNameTable.markAsShared();
        return true;
    }
    
    public void nuke() {
        this.mMainHash = null;
        this.mMainNames = null;
        this.mCollList = null;
    }
    
    @Override
    public int size() {
        return this.mCount;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("[PNameTable, size: ");
        sb.append(this.mCount);
        sb.append('/');
        sb.append(this.mMainHash.length);
        sb.append(", ");
        sb.append(this.mCollCount);
        sb.append(" coll; avg length: ");
        int mCount = this.mCount;
        for (int i = 0; i < this.mCollEnd; ++i) {
            for (int length = this.mCollList[i].length(), j = 1; j <= length; ++j) {
                mCount += j;
            }
        }
        final int mCount2 = this.mCount;
        double d;
        if (mCount2 == 0) {
            d = 0.0;
        }
        else {
            d = mCount / (double)mCount2;
        }
        sb.append(d);
        sb.append(']');
        return sb.toString();
    }
    
    public static final class Bucket
    {
        final ByteBasedPName mName;
        final Bucket mNext;
        
        public Bucket(final ByteBasedPName mName, final Bucket mNext) {
            this.mName = mName;
            this.mNext = mNext;
        }
        
        public ByteBasedPName find(final int n, final int n2, final int n3) {
            if (this.mName.hashEquals(n, n2, n3)) {
                return this.mName;
            }
            for (Bucket bucket = this.mNext; bucket != null; bucket = bucket.mNext) {
                final ByteBasedPName mName = bucket.mName;
                if (mName.hashEquals(n, n2, n3)) {
                    return mName;
                }
            }
            return null;
        }
        
        public ByteBasedPName find(final int n, final int[] array, final int n2) {
            if (this.mName.hashEquals(n, array, n2)) {
                return this.mName;
            }
            for (Bucket bucket = this.mNext; bucket != null; bucket = bucket.mNext) {
                final ByteBasedPName mName = bucket.mName;
                if (mName.hashEquals(n, array, n2)) {
                    return mName;
                }
            }
            return null;
        }
        
        public int length() {
            Bucket bucket = this.mNext;
            int n = 1;
            while (bucket != null) {
                ++n;
                bucket = bucket.mNext;
            }
            return n;
        }
    }
}

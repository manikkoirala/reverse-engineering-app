// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.in;

public final class PNameN extends ByteBasedPName
{
    final int mQuadLen;
    final int[] mQuads;
    
    public PNameN(final String s, final String s2, final String s3, final int n, final int[] mQuads, final int mQuadLen) {
        super(s, s2, s3, n);
        this.mQuads = mQuads;
        this.mQuadLen = mQuadLen;
    }
    
    @Override
    public PName createBoundName(final NsBinding namespaceBinding) {
        final PNameN pNameN = new PNameN(super._prefixedName, super._prefix, super._localName, super.mHash, this.mQuads, this.mQuadLen);
        pNameN._namespaceBinding = namespaceBinding;
        return pNameN;
    }
    
    @Override
    public boolean equals(final int n, final int n2) {
        final int mQuadLen = this.mQuadLen;
        final boolean b = false;
        final boolean b2 = false;
        boolean b3 = b;
        if (mQuadLen < 3) {
            if (mQuadLen == 1) {
                boolean b4 = b2;
                if (this.mQuads[0] == n) {
                    b4 = b2;
                    if (n2 == 0) {
                        b4 = true;
                    }
                }
                return b4;
            }
            final int[] mQuads = this.mQuads;
            b3 = b;
            if (mQuads[0] == n) {
                b3 = b;
                if (mQuads[1] == n2) {
                    b3 = true;
                }
            }
        }
        return b3;
    }
    
    @Override
    public boolean equals(final int[] array, final int n) {
        if (n == this.mQuadLen) {
            for (int i = 0; i < n; ++i) {
                if (array[i] != this.mQuads[i]) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    @Override
    public int getFirstQuad() {
        return this.mQuads[0];
    }
    
    @Override
    public int getLastQuad() {
        return this.mQuads[this.mQuadLen - 1];
    }
    
    @Override
    public int getQuad(int n) {
        if (n < this.mQuadLen) {
            n = this.mQuads[n];
        }
        else {
            n = 0;
        }
        return n;
    }
    
    @Override
    public boolean hashEquals(int mQuadLen, final int n, final int n2) {
        final int mHash = super.mHash;
        final boolean b = false;
        final boolean b2 = false;
        boolean b3 = b;
        if (mQuadLen == mHash) {
            mQuadLen = this.mQuadLen;
            b3 = b;
            if (mQuadLen < 3) {
                if (mQuadLen == 1) {
                    boolean b4 = b2;
                    if (this.mQuads[0] == n) {
                        b4 = b2;
                        if (n2 == 0) {
                            b4 = true;
                        }
                    }
                    return b4;
                }
                final int[] mQuads = this.mQuads;
                b3 = b;
                if (mQuads[0] == n) {
                    b3 = b;
                    if (mQuads[1] == n2) {
                        b3 = true;
                    }
                }
            }
        }
        return b3;
    }
    
    @Override
    public boolean hashEquals(int i, final int[] array, final int n) {
        if (i == super.mHash && n == this.mQuadLen) {
            for (i = 0; i < n; ++i) {
                if (array[i] != this.mQuads[i]) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    @Override
    public int sizeInQuads() {
        return this.mQuadLen;
    }
}

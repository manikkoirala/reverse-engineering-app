// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.in;

public abstract class ByteBasedPName extends PName
{
    protected final int mHash;
    
    public ByteBasedPName(final String s, final String s2, final String s3, final int mHash) {
        super(s, s2, s3);
        this.mHash = mHash;
    }
    
    public abstract boolean equals(final int p0, final int p1);
    
    public abstract boolean equals(final int[] p0, final int p1);
    
    @Override
    public abstract int getFirstQuad();
    
    @Override
    public abstract int getQuad(final int p0);
    
    @Override
    public int hashCode() {
        return this.mHash;
    }
    
    public abstract boolean hashEquals(final int p0, final int p1, final int p2);
    
    public abstract boolean hashEquals(final int p0, final int[] p1, final int p2);
    
    @Override
    public abstract int sizeInQuads();
}

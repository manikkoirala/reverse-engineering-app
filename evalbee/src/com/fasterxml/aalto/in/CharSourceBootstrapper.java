// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.in;

import javax.xml.stream.XMLReporter;
import java.text.MessageFormat;
import com.fasterxml.aalto.impl.ErrorConsts;
import com.fasterxml.aalto.util.CharsetNames;
import com.fasterxml.aalto.impl.LocationImpl;
import javax.xml.stream.Location;
import java.io.IOException;
import com.fasterxml.aalto.impl.IoStreamException;
import java.io.Reader;

public final class CharSourceBootstrapper extends InputBootstrapper
{
    static final char CHAR_BOM_MARKER = '\ufeff';
    static final int DEFAULT_BUFFER_SIZE = 4000;
    final Reader _in;
    final char[] _inputBuffer;
    private int _inputLast;
    private int _inputPtr;
    
    private CharSourceBootstrapper(final ReaderConfig readerConfig, final Reader in) {
        super(readerConfig);
        this._in = in;
        this._inputBuffer = readerConfig.allocFullCBuffer(4000);
        this._inputPtr = 0;
        this._inputLast = 0;
    }
    
    private CharSourceBootstrapper(final ReaderConfig readerConfig, final char[] inputBuffer, final int inputPtr, final int n) {
        super(readerConfig);
        this._in = null;
        this._inputBuffer = inputBuffer;
        this._inputPtr = inputPtr;
        this._inputLast = inputPtr + n;
    }
    
    public static CharSourceBootstrapper construct(final ReaderConfig readerConfig, final Reader reader) {
        return new CharSourceBootstrapper(readerConfig, reader);
    }
    
    public static CharSourceBootstrapper construct(final ReaderConfig readerConfig, final char[] array, final int n, final int n2) {
        return new CharSourceBootstrapper(readerConfig, array, n, n2);
    }
    
    @Override
    public final XmlScanner bootstrap() {
        try {
            try {
                final XmlScanner doBootstrap = this.doBootstrap();
                super._config.freeSmallCBuffer(super.mKeyword);
                return doBootstrap;
            }
            finally {}
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
        super._config.freeSmallCBuffer(super.mKeyword);
    }
    
    @Override
    public int checkKeyword(final String s) {
        for (int length = s.length(), i = 1; i < length; ++i) {
            final int inputPtr = this._inputPtr;
            char nextChar;
            if (inputPtr < this._inputLast) {
                final char[] inputBuffer = this._inputBuffer;
                this._inputPtr = inputPtr + 1;
                nextChar = inputBuffer[inputPtr];
            }
            else {
                nextChar = this.nextChar();
            }
            if (nextChar != s.charAt(i)) {
                return nextChar;
            }
            if (nextChar == '\0') {
                this.reportNull();
            }
        }
        return 0;
    }
    
    public XmlScanner doBootstrap() {
        if (this._inputPtr >= this._inputLast) {
            this.initialLoad(7);
        }
        final int inputLast = this._inputLast;
        final int inputPtr = this._inputPtr;
        String verifyXmlEncoding = null;
        Label_0192: {
            if (inputLast - inputPtr >= 7) {
                final char[] inputBuffer = this._inputBuffer;
                char c;
                if ((c = inputBuffer[inputPtr]) == '\ufeff') {
                    final int inputPtr2 = inputPtr + 1;
                    this._inputPtr = inputPtr2;
                    c = inputBuffer[inputPtr2];
                }
                if (c == '<') {
                    final int inputPtr3 = this._inputPtr;
                    if (inputBuffer[inputPtr3 + 1] == '?' && inputBuffer[inputPtr3 + 2] == 'x' && inputBuffer[inputPtr3 + 3] == 'm' && inputBuffer[inputPtr3 + 4] == 'l' && inputBuffer[inputPtr3 + 5] <= ' ') {
                        this._inputPtr = inputPtr3 + 6;
                        this.readXmlDeclaration();
                        final String mFoundEncoding = super.mFoundEncoding;
                        if (mFoundEncoding != null) {
                            verifyXmlEncoding = this.verifyXmlEncoding(mFoundEncoding);
                            break Label_0192;
                        }
                    }
                }
                else if (c == '\u00ef') {
                    throw new IoStreamException("Unexpected first character (char code 0xEF), not valid in xml document: could be mangled UTF-8 BOM marker. Make sure that the Reader uses correct encoding or pass an InputStream instead");
                }
            }
            verifyXmlEncoding = null;
        }
        super._config.setActualEncoding(verifyXmlEncoding);
        super._config.setXmlDeclInfo(super.mDeclaredXmlVersion, super.mFoundEncoding, super.mStandalone);
        return new ReaderScanner(super._config, this._in, this._inputBuffer, this._inputPtr, this._inputLast);
    }
    
    @Override
    public Location getLocation() {
        final String publicId = super._config.getPublicId();
        final String systemId = super._config.getSystemId();
        final int inputProcessed = super._inputProcessed;
        final int inputPtr = this._inputPtr;
        return (Location)LocationImpl.fromZeroBased(publicId, systemId, inputProcessed + inputPtr, super._inputRow, inputPtr - super._inputRowStart);
    }
    
    @Override
    public int getNext() {
        final int inputPtr = this._inputPtr;
        char nextChar;
        if (inputPtr < this._inputLast) {
            final char[] inputBuffer = this._inputBuffer;
            this._inputPtr = inputPtr + 1;
            nextChar = inputBuffer[inputPtr];
        }
        else {
            nextChar = this.nextChar();
        }
        return nextChar;
    }
    
    @Override
    public int getNextAfterWs(final boolean b) {
        int n = 0;
        char nextChar;
        while (true) {
            final int inputPtr = this._inputPtr;
            if (inputPtr < this._inputLast) {
                final char[] inputBuffer = this._inputBuffer;
                this._inputPtr = inputPtr + 1;
                nextChar = inputBuffer[inputPtr];
            }
            else {
                nextChar = this.nextChar();
            }
            if (nextChar > ' ') {
                break;
            }
            if (nextChar != '\r' && nextChar != '\n') {
                if (nextChar == '\0') {
                    this.reportNull();
                }
            }
            else {
                this.skipCRLF(nextChar);
            }
            ++n;
        }
        if (b && n == 0) {
            this.reportUnexpectedChar(nextChar, "; expected a white space");
        }
        return nextChar;
    }
    
    public boolean initialLoad(final int n) {
        this._inputPtr = 0;
        this._inputLast = 0;
        if (this._in == null) {
            return false;
        }
        while (true) {
            final int inputLast = this._inputLast;
            if (inputLast >= n) {
                return true;
            }
            final Reader in = this._in;
            final char[] inputBuffer = this._inputBuffer;
            final int read = in.read(inputBuffer, inputLast, inputBuffer.length - inputLast);
            if (read < 1) {
                return false;
            }
            this._inputLast += read;
        }
    }
    
    public void loadMore() {
        final int inputProcessed = super._inputProcessed;
        final int inputLast = this._inputLast;
        super._inputProcessed = inputProcessed + inputLast;
        super._inputRowStart -= inputLast;
        if (this._in == null) {
            this.reportEof();
        }
        this._inputPtr = 0;
        final Reader in = this._in;
        final char[] inputBuffer = this._inputBuffer;
        if ((this._inputLast = in.read(inputBuffer, 0, inputBuffer.length)) < 1) {
            this.reportEof();
        }
    }
    
    public char nextChar() {
        if (this._inputPtr >= this._inputLast) {
            this.loadMore();
        }
        return this._inputBuffer[this._inputPtr++];
    }
    
    @Override
    public void pushback() {
        --this._inputPtr;
    }
    
    @Override
    public int readQuotedValue(final char[] array, final int n) {
        final int length = array.length;
        int n2 = 0;
        while (true) {
            final int inputPtr = this._inputPtr;
            char nextChar;
            if (inputPtr < this._inputLast) {
                final char[] inputBuffer = this._inputBuffer;
                this._inputPtr = inputPtr + 1;
                nextChar = inputBuffer[inputPtr];
            }
            else {
                nextChar = this.nextChar();
            }
            if (nextChar != '\r' && nextChar != '\n') {
                if (nextChar == '\0') {
                    this.reportNull();
                }
            }
            else {
                this.skipCRLF(nextChar);
            }
            if (nextChar == n) {
                break;
            }
            if (n2 >= length) {
                continue;
            }
            array[n2] = nextChar;
            ++n2;
        }
        if (n2 >= length) {
            n2 = -1;
        }
        return n2;
    }
    
    public void skipCRLF(final char c) {
        if (c == '\r') {
            final int inputPtr = this._inputPtr;
            char nextChar;
            if (inputPtr < this._inputLast) {
                final char[] inputBuffer = this._inputBuffer;
                this._inputPtr = inputPtr + 1;
                nextChar = inputBuffer[inputPtr];
            }
            else {
                nextChar = this.nextChar();
            }
            if (nextChar != '\n') {
                --this._inputPtr;
            }
        }
        ++super._inputRow;
        super._inputRowStart = this._inputPtr;
    }
    
    public String verifyXmlEncoding(String externalEncoding) {
        final String normalize = CharsetNames.normalize(externalEncoding);
        externalEncoding = super._config.getExternalEncoding();
        if (externalEncoding != null && normalize != null && !externalEncoding.equalsIgnoreCase(normalize)) {
            final XMLReporter xmlReporter = super._config.getXMLReporter();
            if (xmlReporter != null) {
                xmlReporter.report(MessageFormat.format(ErrorConsts.W_MIXED_ENCODINGS, externalEncoding, normalize), ErrorConsts.WT_XML_DECL, this, this.getLocation());
            }
        }
        return normalize;
    }
}

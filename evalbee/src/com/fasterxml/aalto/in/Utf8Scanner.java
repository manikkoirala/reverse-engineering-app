// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.in;

import com.fasterxml.aalto.util.XmlCharTypes;
import com.fasterxml.aalto.util.XmlChars;
import com.fasterxml.aalto.impl.ErrorConsts;
import com.fasterxml.aalto.util.DataUtil;
import java.io.InputStream;

public final class Utf8Scanner extends StreamScanner
{
    public Utf8Scanner(final ReaderConfig readerConfig, final InputStream inputStream, final byte[] array, final int n, final int n2) {
        super(readerConfig, inputStream, array, n, n2);
    }
    
    private final int collectValue(int inputPtr, final byte b, final PName pName) {
        char[] startNewValue = super._attrCollector.startNewValue(pName, inputPtr);
        final int[] attr_CHARS = super._charTypes.ATTR_CHARS;
        int n4 = 0;
    Block_13:
        while (true) {
            int n;
            if ((n = super._inputPtr) >= super._inputEnd) {
                this.loadMoreGuaranteed();
                n = super._inputPtr;
            }
            char[] valueBufferFull = startNewValue;
            if (inputPtr >= startNewValue.length) {
                valueBufferFull = super._attrCollector.valueBufferFull();
            }
            final int inputEnd = super._inputEnd;
            final int n2 = valueBufferFull.length - inputPtr + n;
            int i = n;
            int n3 = inputEnd;
            n4 = inputPtr;
            if (n2 < inputEnd) {
                n3 = n2;
                n4 = inputPtr;
                i = n;
            }
            while (i < n3) {
                final byte[] inputBuffer = super._inputBuffer;
                inputPtr = i + 1;
                final int n5 = inputBuffer[i] & 0xFF;
                final int n6 = attr_CHARS[n5];
                if (n6 != 0) {
                    super._inputPtr = inputPtr;
                    int n7 = 0;
                    if (n6 != 14) {
                        switch (n6) {
                            default: {
                                inputPtr = n5;
                                n7 = n4;
                                break;
                            }
                            case 7: {
                                inputPtr = this.decodeUtf8_4(n5);
                                n7 = n4 + 1;
                                valueBufferFull[n4] = (char)(0xD800 | inputPtr >> 10);
                                inputPtr = ((inputPtr & 0x3FF) | 0xDC00);
                                char[] valueBufferFull2 = valueBufferFull;
                                if (n7 >= valueBufferFull.length) {
                                    valueBufferFull2 = super._attrCollector.valueBufferFull();
                                }
                                valueBufferFull = valueBufferFull2;
                                break;
                            }
                            case 6: {
                                inputPtr = this.decodeUtf8_3(n5);
                                n7 = n4;
                                break;
                            }
                            case 5: {
                                inputPtr = this.decodeUtf8_2(n5);
                                n7 = n4;
                                break;
                            }
                            case 4: {
                                this.reportInvalidInitial(n5);
                            }
                            case 9: {
                                this.throwUnexpectedChar(n5, "'<' not allowed in attribute value");
                            }
                            case 10: {
                                inputPtr = this.handleEntityInText(false);
                                if (inputPtr == 0) {
                                    this.reportUnexpandedEntityInAttr(pName, false);
                                }
                                if (inputPtr >> 16 != 0) {
                                    inputPtr -= 65536;
                                    n7 = n4 + 1;
                                    valueBufferFull[n4] = (char)(0xD800 | inputPtr >> 10);
                                    char[] valueBufferFull3 = valueBufferFull;
                                    if (n7 >= valueBufferFull.length) {
                                        valueBufferFull3 = super._attrCollector.valueBufferFull();
                                    }
                                    inputPtr = ((inputPtr & 0x3FF) | 0xDC00);
                                    valueBufferFull = valueBufferFull3;
                                    break;
                                }
                                n7 = n4;
                                break;
                            }
                            case 1: {
                                this.handleInvalidXmlChar(n5);
                            }
                            case 2: {
                                if (super._inputPtr >= super._inputEnd) {
                                    this.loadMoreGuaranteed();
                                }
                                final byte[] inputBuffer2 = super._inputBuffer;
                                inputPtr = super._inputPtr;
                                if (inputBuffer2[inputPtr] == 10) {
                                    super._inputPtr = inputPtr + 1;
                                }
                            }
                            case 3: {
                                this.markLF();
                            }
                            case 8: {
                                inputPtr = 32;
                                n7 = n4;
                                break;
                            }
                        }
                    }
                    else {
                        inputPtr = n5;
                        n7 = n4;
                        if (n5 == b) {
                            break Block_13;
                        }
                    }
                    valueBufferFull[n7] = (char)inputPtr;
                    inputPtr = n7 + 1;
                    startNewValue = valueBufferFull;
                    continue Block_13;
                }
                valueBufferFull[n4] = (char)n5;
                ++n4;
                i = inputPtr;
            }
            super._inputPtr = i;
            startNewValue = valueBufferFull;
            inputPtr = n4;
        }
        return n4;
    }
    
    private final int decodeMultiByteChar(int inputPtr, int inputPtr2) {
        int n = 0;
        Label_0077: {
            if ((inputPtr & 0xE0) == 0xC0) {
                n = (inputPtr & 0x1F);
            }
            else {
                if ((inputPtr & 0xF0) == 0xE0) {
                    n = (inputPtr & 0xF);
                    inputPtr = 2;
                    break Label_0077;
                }
                if ((inputPtr & 0xF8) == 0xF0) {
                    n = (inputPtr & 0x7);
                    inputPtr = 3;
                    break Label_0077;
                }
                this.reportInvalidInitial(inputPtr & 0xFF);
                n = inputPtr;
            }
            inputPtr = 1;
        }
        int inputPtr3 = inputPtr2;
        if (inputPtr2 >= super._inputEnd) {
            this.loadMoreGuaranteed();
            inputPtr3 = super._inputPtr;
        }
        final byte[] inputBuffer = super._inputBuffer;
        final int n2 = inputPtr3 + 1;
        inputPtr2 = inputBuffer[inputPtr3];
        if ((inputPtr2 & 0xC0) != 0x80) {
            this.reportInvalidOther(inputPtr2 & 0xFF, n2);
        }
        final int n3 = n << 6 | (inputPtr2 & 0x3F);
        inputPtr2 = n2;
        int n4 = n3;
        if (inputPtr > 1) {
            int inputPtr4;
            if ((inputPtr4 = n2) >= super._inputEnd) {
                this.loadMoreGuaranteed();
                inputPtr4 = super._inputPtr;
            }
            final byte[] inputBuffer2 = super._inputBuffer;
            inputPtr2 = inputPtr4 + 1;
            final byte b = inputBuffer2[inputPtr4];
            if ((b & 0xC0) != 0x80) {
                this.reportInvalidOther(b & 0xFF, inputPtr2);
            }
            n4 = (n3 << 6 | (b & 0x3F));
            if (inputPtr > 2) {
                if ((inputPtr = inputPtr2) >= super._inputEnd) {
                    this.loadMoreGuaranteed();
                    inputPtr = super._inputPtr;
                }
                final byte[] inputBuffer3 = super._inputBuffer;
                inputPtr2 = inputPtr + 1;
                inputPtr = inputBuffer3[inputPtr];
                if ((inputPtr & 0xC0) != 0x80) {
                    this.reportInvalidOther(inputPtr & 0xFF, inputPtr2);
                }
                n4 = -(n4 << 6 | (inputPtr & 0x3F));
            }
        }
        super._inputPtr = inputPtr2;
        return n4;
    }
    
    private final int decodeUtf8_2(final int n) {
        if (super._inputPtr >= super._inputEnd) {
            this.loadMoreGuaranteed();
        }
        final byte[] inputBuffer = super._inputBuffer;
        final int inputPtr = super._inputPtr;
        final int inputPtr2 = inputPtr + 1;
        super._inputPtr = inputPtr2;
        final byte b = inputBuffer[inputPtr];
        if ((b & 0xC0) != 0x80) {
            this.reportInvalidOther(b & 0xFF, inputPtr2);
        }
        return (n & 0x1F) << 6 | (b & 0x3F);
    }
    
    private final int decodeUtf8_3(int n) {
        if (super._inputPtr >= super._inputEnd) {
            this.loadMoreGuaranteed();
        }
        final int n2 = n & 0xF;
        final byte[] inputBuffer = super._inputBuffer;
        n = super._inputPtr;
        final int inputPtr = n + 1;
        super._inputPtr = inputPtr;
        n = inputBuffer[n];
        if ((n & 0xC0) != 0x80) {
            this.reportInvalidOther(n & 0xFF, inputPtr);
        }
        if (super._inputPtr >= super._inputEnd) {
            this.loadMoreGuaranteed();
        }
        final byte[] inputBuffer2 = super._inputBuffer;
        final int inputPtr2 = super._inputPtr;
        final int inputPtr3 = inputPtr2 + 1;
        super._inputPtr = inputPtr3;
        final byte b = inputBuffer2[inputPtr2];
        if ((b & 0xC0) != 0x80) {
            this.reportInvalidOther(b & 0xFF, inputPtr3);
        }
        final int n3 = n = (((n & 0x3F) | n2 << 6) << 6 | (b & 0x3F));
        if (n2 >= 13 && (n = n3) >= 55296) {
            if (n3 >= 57344) {
                n = n3;
                if (n3 < 65534 || (n = n3) > 65535) {
                    return n;
                }
            }
            n = this.handleInvalidXmlChar(n3);
        }
        return n;
    }
    
    private final int decodeUtf8_3fast(int n) {
        final int n2 = n & 0xF;
        final byte[] inputBuffer = super._inputBuffer;
        n = super._inputPtr;
        final int inputPtr = n + 1;
        super._inputPtr = inputPtr;
        n = inputBuffer[n];
        if ((n & 0xC0) != 0x80) {
            this.reportInvalidOther(n & 0xFF, inputPtr);
        }
        final byte[] inputBuffer2 = super._inputBuffer;
        final int inputPtr2 = super._inputPtr;
        final int inputPtr3 = inputPtr2 + 1;
        super._inputPtr = inputPtr3;
        final byte b = inputBuffer2[inputPtr2];
        if ((b & 0xC0) != 0x80) {
            this.reportInvalidOther(b & 0xFF, inputPtr3);
        }
        final int n3 = n = (((n & 0x3F) | n2 << 6) << 6 | (b & 0x3F));
        if (n2 >= 13 && (n = n3) >= 55296) {
            if (n3 >= 57344) {
                n = n3;
                if (n3 < 65534 || (n = n3) > 65535) {
                    return n;
                }
            }
            n = this.handleInvalidXmlChar(n3);
        }
        return n;
    }
    
    private final int decodeUtf8_4(final int n) {
        if (super._inputPtr >= super._inputEnd) {
            this.loadMoreGuaranteed();
        }
        final byte[] inputBuffer = super._inputBuffer;
        final int inputPtr = super._inputPtr;
        final int inputPtr2 = inputPtr + 1;
        super._inputPtr = inputPtr2;
        final byte b = inputBuffer[inputPtr];
        if ((b & 0xC0) != 0x80) {
            this.reportInvalidOther(b & 0xFF, inputPtr2);
        }
        if (super._inputPtr >= super._inputEnd) {
            this.loadMoreGuaranteed();
        }
        final byte[] inputBuffer2 = super._inputBuffer;
        final int inputPtr3 = super._inputPtr;
        final int inputPtr4 = inputPtr3 + 1;
        super._inputPtr = inputPtr4;
        final byte b2 = inputBuffer2[inputPtr3];
        if ((b2 & 0xC0) != 0x80) {
            this.reportInvalidOther(b2 & 0xFF, inputPtr4);
        }
        if (super._inputPtr >= super._inputEnd) {
            this.loadMoreGuaranteed();
        }
        final byte[] inputBuffer3 = super._inputBuffer;
        final int inputPtr5 = super._inputPtr;
        final int inputPtr6 = inputPtr5 + 1;
        super._inputPtr = inputPtr6;
        final byte b3 = inputBuffer3[inputPtr5];
        if ((b3 & 0xC0) != 0x80) {
            this.reportInvalidOther(b3 & 0xFF, inputPtr6);
        }
        return ((((n & 0x7) << 6 | (b & 0x3F)) << 6 | (b2 & 0x3F)) << 6 | (b3 & 0x3F)) - 65536;
    }
    
    private void handleNsDeclaration(final PName pName, final byte b) {
        char[] nameBuffer = super._nameBuffer;
        int n = 0;
        while (true) {
            if (super._inputPtr >= super._inputEnd) {
                this.loadMoreGuaranteed();
            }
            final byte[] inputBuffer = super._inputBuffer;
            final int inputPtr = super._inputPtr;
            final int inputPtr2 = inputPtr + 1;
            super._inputPtr = inputPtr2;
            int n2 = inputBuffer[inputPtr];
            if (n2 == b) {
                break;
            }
            char[] array = null;
            int n3 = 0;
            Label_0464: {
                int n5 = 0;
                int n6 = 0;
                Label_0190: {
                    if (n2 == 38) {
                        final int handleEntityInText = this.handleEntityInText(false);
                        if (handleEntityInText == 0) {
                            this.reportUnexpandedEntityInAttr(pName, true);
                        }
                        array = nameBuffer;
                        n3 = n;
                        n2 = handleEntityInText;
                        if (handleEntityInText >> 16 == 0) {
                            break Label_0464;
                        }
                        array = nameBuffer;
                        if (n >= nameBuffer.length) {
                            array = DataUtil.growArrayBy(nameBuffer, nameBuffer.length);
                            super._nameBuffer = array;
                        }
                        final int n4 = handleEntityInText - 65536;
                        n5 = n + 1;
                        array[n] = (char)(n4 >> 10 | 0xD800);
                        n6 = n4;
                    }
                    else {
                        if (n2 == 60) {
                            this.throwUnexpectedChar(n2, "'<' not allowed in attribute value");
                            array = nameBuffer;
                            n3 = n;
                            break Label_0464;
                        }
                        final int n7 = n2 & 0xFF;
                        array = nameBuffer;
                        n3 = n;
                        if ((n2 = n7) < 32) {
                            if (n7 != 10) {
                                if (n7 == 13) {
                                    if (inputPtr2 >= super._inputEnd) {
                                        this.loadMoreGuaranteed();
                                    }
                                    final byte[] inputBuffer2 = super._inputBuffer;
                                    final int inputPtr3 = super._inputPtr;
                                    if (inputBuffer2[inputPtr3] == 10) {
                                        super._inputPtr = inputPtr3 + 1;
                                    }
                                }
                                else if (n7 < 0) {
                                    final int decodeMultiByteChar = this.decodeMultiByteChar(n7, inputPtr2);
                                    array = nameBuffer;
                                    n3 = n;
                                    if ((n2 = decodeMultiByteChar) < 0) {
                                        final byte b2 = (byte)(-decodeMultiByteChar);
                                        array = nameBuffer;
                                        if (n >= nameBuffer.length) {
                                            array = DataUtil.growArrayBy(nameBuffer, nameBuffer.length);
                                            super._nameBuffer = array;
                                        }
                                        final int n8 = b2 - 65536;
                                        n5 = n + 1;
                                        array[n] = (char)(n8 >> 10 | 0xD800);
                                        n6 = n8;
                                        break Label_0190;
                                    }
                                    break Label_0464;
                                }
                                else {
                                    array = nameBuffer;
                                    n3 = n;
                                    if ((n2 = n7) != 9) {
                                        this.throwInvalidSpace(n7);
                                        n2 = n7;
                                        n3 = n;
                                        array = nameBuffer;
                                    }
                                    break Label_0464;
                                }
                            }
                            this.markLF();
                            array = nameBuffer;
                            n3 = n;
                            n2 = n7;
                        }
                        break Label_0464;
                    }
                }
                final int n9 = (n6 & 0x3FF) | 0xDC00;
                n3 = n5;
                n2 = n9;
            }
            nameBuffer = array;
            if (n3 >= array.length) {
                nameBuffer = DataUtil.growArrayBy(array, array.length);
                super._nameBuffer = nameBuffer;
            }
            nameBuffer[n3] = (char)n2;
            n = n3 + 1;
        }
        String canonicalizeURI;
        if (n == 0) {
            canonicalizeURI = "";
        }
        else {
            canonicalizeURI = super._config.canonicalizeURI(nameBuffer, n);
        }
        this.bindNs(pName, canonicalizeURI);
    }
    
    private final void skipUtf8_2(int inputPtr) {
        if (super._inputPtr >= super._inputEnd) {
            this.loadMoreGuaranteed();
        }
        final byte[] inputBuffer = super._inputBuffer;
        final int inputPtr2 = super._inputPtr;
        inputPtr = inputPtr2 + 1;
        super._inputPtr = inputPtr;
        final byte b = inputBuffer[inputPtr2];
        if ((b & 0xC0) != 0x80) {
            this.reportInvalidOther(b & 0xFF, inputPtr);
        }
    }
    
    private final void skipUtf8_3(int n) {
        if (super._inputPtr >= super._inputEnd) {
            this.loadMoreGuaranteed();
        }
        n &= 0xF;
        if (n >= 13) {
            final byte[] inputBuffer = super._inputBuffer;
            final int inputPtr = super._inputPtr;
            final int inputPtr2 = inputPtr + 1;
            super._inputPtr = inputPtr2;
            final byte b = inputBuffer[inputPtr];
            if ((b & 0xC0) != 0x80) {
                this.reportInvalidOther(b & 0xFF, inputPtr2);
            }
            if (super._inputPtr >= super._inputEnd) {
                this.loadMoreGuaranteed();
            }
            final byte[] inputBuffer2 = super._inputBuffer;
            final int inputPtr3 = super._inputPtr;
            final int inputPtr4 = inputPtr3 + 1;
            super._inputPtr = inputPtr4;
            final byte b2 = inputBuffer2[inputPtr3];
            if ((b2 & 0xC0) != 0x80) {
                this.reportInvalidOther(b2 & 0xFF, inputPtr4);
            }
            n = ((n << 6 | (b & 0x3F)) << 6 | (b2 & 0x3F));
            if (n >= 55296 && (n < 57344 || (n >= 65534 && n <= 65535))) {
                this.handleInvalidXmlChar(n);
            }
        }
        else {
            final byte[] inputBuffer3 = super._inputBuffer;
            final int inputPtr5 = super._inputPtr;
            n = inputPtr5 + 1;
            super._inputPtr = n;
            final byte b3 = inputBuffer3[inputPtr5];
            if ((b3 & 0xC0) != 0x80) {
                this.reportInvalidOther(b3 & 0xFF, n);
            }
            if (super._inputPtr >= super._inputEnd) {
                this.loadMoreGuaranteed();
            }
            final byte[] inputBuffer4 = super._inputBuffer;
            final int inputPtr6 = super._inputPtr;
            n = inputPtr6 + 1;
            super._inputPtr = n;
            final byte b4 = inputBuffer4[inputPtr6];
            if ((b4 & 0xC0) != 0x80) {
                this.reportInvalidOther(b4 & 0xFF, n);
            }
        }
    }
    
    private final void skipUtf8_4(int inputPtr) {
        final int inputPtr2 = super._inputPtr;
        if (inputPtr2 + 4 > super._inputEnd) {
            this.skipUtf8_4Slow(inputPtr);
            return;
        }
        final byte[] inputBuffer = super._inputBuffer;
        inputPtr = inputPtr2 + 1;
        super._inputPtr = inputPtr;
        final byte b = inputBuffer[inputPtr2];
        if ((b & 0xC0) != 0x80) {
            this.reportInvalidOther(b & 0xFF, inputPtr);
        }
        final byte[] inputBuffer2 = super._inputBuffer;
        final int inputPtr3 = super._inputPtr;
        inputPtr = inputPtr3 + 1;
        super._inputPtr = inputPtr;
        final byte b2 = inputBuffer2[inputPtr3];
        if ((b2 & 0xC0) != 0x80) {
            this.reportInvalidOther(b2 & 0xFF, inputPtr);
        }
        final byte[] inputBuffer3 = super._inputBuffer;
        final int inputPtr4 = super._inputPtr;
        inputPtr = inputPtr4 + 1;
        super._inputPtr = inputPtr;
        final byte b3 = inputBuffer3[inputPtr4];
        if ((b3 & 0xC0) != 0x80) {
            this.reportInvalidOther(b3 & 0xFF, inputPtr);
        }
    }
    
    private final void skipUtf8_4Slow(int inputPtr) {
        if (super._inputPtr >= super._inputEnd) {
            this.loadMoreGuaranteed();
        }
        final byte[] inputBuffer = super._inputBuffer;
        final int inputPtr2 = super._inputPtr;
        inputPtr = inputPtr2 + 1;
        super._inputPtr = inputPtr;
        final byte b = inputBuffer[inputPtr2];
        if ((b & 0xC0) != 0x80) {
            this.reportInvalidOther(b & 0xFF, inputPtr);
        }
        if (super._inputPtr >= super._inputEnd) {
            this.loadMoreGuaranteed();
        }
        final byte[] inputBuffer2 = super._inputBuffer;
        final int inputPtr3 = super._inputPtr;
        inputPtr = inputPtr3 + 1;
        super._inputPtr = inputPtr;
        final byte b2 = inputBuffer2[inputPtr3];
        if ((b2 & 0xC0) != 0x80) {
            this.reportInvalidOther(b2 & 0xFF, inputPtr);
        }
        if (super._inputPtr >= super._inputEnd) {
            this.loadMoreGuaranteed();
        }
        final byte[] inputBuffer3 = super._inputBuffer;
        final int inputPtr4 = super._inputPtr;
        inputPtr = inputPtr4 + 1;
        super._inputPtr = inputPtr;
        final byte b3 = inputBuffer3[inputPtr4];
        if ((b3 & 0xC0) != 0x80) {
            this.reportInvalidOther(b3 & 0xFF, inputPtr);
        }
    }
    
    @Override
    public int decodeCharForError(final byte b) {
        if (b >= 0) {
            return b;
        }
        int n = 0;
        int n2 = 0;
        Label_0083: {
            if ((b & 0xE0) == 0xC0) {
                n = (b & 0x1F);
            }
            else {
                if ((b & 0xF0) == 0xE0) {
                    n = (b & 0xF);
                    n2 = 2;
                    break Label_0083;
                }
                if ((b & 0xF8) == 0xF0) {
                    n = (b & 0x7);
                    n2 = 3;
                    break Label_0083;
                }
                this.reportInvalidInitial(b & 0xFF);
                n = b;
            }
            n2 = 1;
        }
        final byte nextByte = this.nextByte();
        if ((nextByte & 0xC0) != 0x80) {
            this.reportInvalidOther(nextByte & 0xFF);
        }
        int n3 = n << 6 | (nextByte & 0x3F);
        if (n2 > 1) {
            final byte nextByte2 = this.nextByte();
            if ((nextByte2 & 0xC0) != 0x80) {
                this.reportInvalidOther(nextByte2 & 0xFF);
            }
            final int n4 = n3 = (n3 << 6 | (nextByte2 & 0x3F));
            if (n2 > 2) {
                final byte nextByte3 = this.nextByte();
                if ((nextByte3 & 0xC0) != 0x80) {
                    this.reportInvalidOther(nextByte3 & 0xFF);
                }
                n3 = (n4 << 6 | (nextByte3 & 0x3F));
            }
        }
        return n3;
    }
    
    @Override
    public final void finishCData() {
        final int[] other_CHARS = super._charTypes.OTHER_CHARS;
        final byte[] inputBuffer = super._inputBuffer;
        char[] resetWithEmpty = super._textBuilder.resetWithEmpty();
        int n = 0;
        int currentLength = 0;
    Block_16:
        while (true) {
            int n2;
            if ((n2 = super._inputPtr) >= super._inputEnd) {
                this.loadMoreGuaranteed();
                n2 = super._inputPtr;
            }
            char[] array = resetWithEmpty;
            int n3;
            if ((n3 = n) >= resetWithEmpty.length) {
                array = super._textBuilder.finishCurrentSegment();
                n3 = 0;
            }
            final int inputEnd = super._inputEnd;
            final int n4 = array.length - n3 + n2;
            n = n3;
            int i = n2;
            int n5;
            if (n4 < (n5 = inputEnd)) {
                n5 = n4;
                i = n2;
                n = n3;
            }
            while (i < n5) {
                final int inputPtr = i + 1;
                final int n6 = inputBuffer[i] & 0xFF;
                final int n7 = other_CHARS[n6];
                if (n7 != 0) {
                    super._inputPtr = inputPtr;
                    int n8 = 0;
                    int n9 = 0;
                    Label_0572: {
                        if (n7 != 11) {
                            switch (n7) {
                                default: {
                                    n8 = n;
                                    n9 = n6;
                                    break Label_0572;
                                }
                                case 7: {
                                    final int decodeUtf8_4 = this.decodeUtf8_4(n6);
                                    n8 = n + 1;
                                    array[n] = (char)(decodeUtf8_4 >> 10 | 0xD800);
                                    if (n8 >= array.length) {
                                        array = super._textBuilder.finishCurrentSegment();
                                        n8 = 0;
                                    }
                                    n9 = ((decodeUtf8_4 & 0x3FF) | 0xDC00);
                                    break Label_0572;
                                }
                                case 6: {
                                    final int decodeUtf8_5 = this.decodeUtf8_3(n6);
                                    n8 = n;
                                    n9 = decodeUtf8_5;
                                    break Label_0572;
                                }
                                case 5: {
                                    final int decodeUtf8_6 = this.decodeUtf8_2(n6);
                                    n8 = n;
                                    n9 = decodeUtf8_6;
                                    break Label_0572;
                                }
                                case 4: {
                                    this.reportInvalidInitial(n6);
                                    break;
                                }
                                case 3: {
                                    this.markLF();
                                    n8 = n;
                                    n9 = n6;
                                    break Label_0572;
                                }
                                case 1: {
                                    this.handleInvalidXmlChar(n6);
                                }
                                case 2: {
                                    if (super._inputPtr >= super._inputEnd) {
                                        this.loadMoreGuaranteed();
                                    }
                                    final int inputPtr2 = super._inputPtr;
                                    if (inputBuffer[inputPtr2] == 10) {
                                        super._inputPtr = inputPtr2 + 1;
                                    }
                                    this.markLF();
                                    final int n10 = 10;
                                    n8 = n;
                                    n9 = n10;
                                    break Label_0572;
                                }
                            }
                        }
                        int n11 = 0;
                        byte b;
                        while (true) {
                            if (super._inputPtr >= super._inputEnd) {
                                this.loadMoreGuaranteed();
                            }
                            final byte[] inputBuffer2 = super._inputBuffer;
                            final int inputPtr3 = super._inputPtr;
                            b = inputBuffer2[inputPtr3];
                            if (b != 93) {
                                break;
                            }
                            super._inputPtr = inputPtr3 + 1;
                            ++n11;
                        }
                        int n12;
                        if (b == 62 && n11 >= 1) {
                            n12 = 1;
                        }
                        else {
                            n12 = 0;
                        }
                        char[] array2 = array;
                        currentLength = n;
                        int n13 = n11;
                        int n14 = n12;
                        while (true) {
                            Label_0462: {
                                if (n12 == 0) {
                                    break Label_0462;
                                }
                                n13 = n11 - 1;
                                n14 = n12;
                                currentLength = n;
                                array2 = array;
                            }
                            if (n13 > 0) {
                                n = currentLength + 1;
                                array2[currentLength] = ']';
                                if (n >= array2.length) {
                                    array = super._textBuilder.finishCurrentSegment();
                                    n = 0;
                                    n11 = n13;
                                    n12 = n14;
                                    continue;
                                }
                                array = array2;
                                n11 = n13;
                                n12 = n14;
                                continue;
                            }
                            else {
                                array = array2;
                                n8 = currentLength;
                                n9 = n6;
                                if (n14 != 0) {
                                    break Block_16;
                                }
                            }
                            break;
                        }
                    }
                    array[n8] = (char)n9;
                    n = n8 + 1;
                    resetWithEmpty = array;
                    continue Block_16;
                }
                array[n] = (char)n6;
                i = inputPtr;
                ++n;
            }
            super._inputPtr = i;
            resetWithEmpty = array;
        }
        ++super._inputPtr;
        super._textBuilder.setCurrentLength(currentLength);
        if (super._cfgCoalescing && !super._entityPending) {
            this.finishCoalescedText();
        }
    }
    
    @Override
    public final void finishCharacters() {
        final int tmpChar = super._tmpChar;
        char[] array;
        int checkInTreeIndentation;
        if (tmpChar < 0) {
            int n = -tmpChar;
            array = super._textBuilder.resetWithEmpty();
            int n3;
            if (n >> 16 != 0) {
                final int n2 = n - 65536;
                array[0] = (char)(n2 >> 10 | 0xD800);
                n = ((n2 & 0x3FF) | 0xDC00);
                n3 = 1;
            }
            else {
                n3 = 0;
            }
            final int n4 = n3 + 1;
            array[n3] = (char)n;
            checkInTreeIndentation = n4;
        }
        else if (tmpChar != 13 && tmpChar != 10) {
            array = super._textBuilder.resetWithEmpty();
            checkInTreeIndentation = 0;
        }
        else {
            ++super._inputPtr;
            checkInTreeIndentation = this.checkInTreeIndentation(tmpChar);
            if (checkInTreeIndentation < 0) {
                return;
            }
            array = super._textBuilder.getBufferWithoutReset();
        }
        final int[] text_CHARS = super._charTypes.TEXT_CHARS;
        final byte[] inputBuffer = super._inputBuffer;
        char[] array2 = array;
        Label_0728: {
        Label_0718:
            while (true) {
                int n5;
                if ((n5 = super._inputPtr) >= super._inputEnd) {
                    this.loadMoreGuaranteed();
                    n5 = super._inputPtr;
                }
                char[] array3 = array2;
                int n6;
                if ((n6 = checkInTreeIndentation) >= array2.length) {
                    array3 = super._textBuilder.finishCurrentSegment();
                    n6 = 0;
                }
                final int inputEnd = super._inputEnd;
                final int n7 = array3.length - n6 + n5;
                checkInTreeIndentation = n6;
                int i = n5;
                int n8;
                if (n7 < (n8 = inputEnd)) {
                    n8 = n7;
                    i = n5;
                    checkInTreeIndentation = n6;
                }
                while (i < n8) {
                    final int inputPtr = i + 1;
                    final int n9 = inputBuffer[i] & 0xFF;
                    final int n10 = text_CHARS[n9];
                    if (n10 != 0) {
                        super._inputPtr = inputPtr;
                        int n11 = 0;
                        int n12 = 0;
                        Label_0827: {
                            int n19 = 0;
                            Label_0638: {
                                int n20 = 0;
                                Label_0636: {
                                    switch (n10) {
                                        default: {
                                            array2 = array3;
                                            n11 = checkInTreeIndentation;
                                            n12 = n9;
                                            break Label_0827;
                                        }
                                        case 11: {
                                            int n13 = 1;
                                            byte b;
                                            while (true) {
                                                if (super._inputPtr >= super._inputEnd) {
                                                    this.loadMoreGuaranteed();
                                                }
                                                final int inputPtr2 = super._inputPtr;
                                                b = inputBuffer[inputPtr2];
                                                if (b != 93) {
                                                    break;
                                                }
                                                super._inputPtr = inputPtr2 + 1;
                                                ++n13;
                                            }
                                            char[] finishCurrentSegment = array3;
                                            int n14 = checkInTreeIndentation;
                                            int n15 = n13;
                                            if (b == 62) {
                                                finishCurrentSegment = array3;
                                                n14 = checkInTreeIndentation;
                                                if ((n15 = n13) > 1) {
                                                    this.reportIllegalCDataEnd();
                                                    n15 = n13;
                                                    n14 = checkInTreeIndentation;
                                                    finishCurrentSegment = array3;
                                                }
                                            }
                                            while (true) {
                                                array2 = finishCurrentSegment;
                                                n11 = n14;
                                                n12 = n9;
                                                if (n15 <= 1) {
                                                    break Label_0827;
                                                }
                                                final int n16 = n14 + 1;
                                                finishCurrentSegment[n14] = ']';
                                                if (n16 >= finishCurrentSegment.length) {
                                                    finishCurrentSegment = super._textBuilder.finishCurrentSegment();
                                                    n14 = 0;
                                                }
                                                else {
                                                    n14 = n16;
                                                }
                                                --n15;
                                            }
                                            break;
                                        }
                                        case 10: {
                                            final int handleEntityInText = this.handleEntityInText(false);
                                            if (handleEntityInText == 0) {
                                                super._entityPending = true;
                                                break Label_0728;
                                            }
                                            array2 = array3;
                                            n11 = checkInTreeIndentation;
                                            n12 = handleEntityInText;
                                            if (handleEntityInText >> 16 == 0) {
                                                break Label_0827;
                                            }
                                            final int n17 = handleEntityInText - 65536;
                                            final int n18 = checkInTreeIndentation + 1;
                                            array3[checkInTreeIndentation] = (char)(n17 >> 10 | 0xD800);
                                            n19 = n17;
                                            if ((n20 = n18) >= array3.length) {
                                                n19 = n17;
                                                break;
                                            }
                                            break Label_0636;
                                        }
                                        case 7: {
                                            final int decodeUtf8_4 = this.decodeUtf8_4(n9);
                                            final int n21 = checkInTreeIndentation + 1;
                                            array3[checkInTreeIndentation] = (char)(decodeUtf8_4 >> 10 | 0xD800);
                                            n19 = decodeUtf8_4;
                                            n20 = n21;
                                            if (n21 >= array3.length) {
                                                n19 = decodeUtf8_4;
                                                break;
                                            }
                                            break Label_0636;
                                        }
                                        case 6: {
                                            if (super._inputEnd - inputPtr >= 2) {
                                                n12 = this.decodeUtf8_3fast(n9);
                                                array2 = array3;
                                                n11 = checkInTreeIndentation;
                                                break Label_0827;
                                            }
                                            n12 = this.decodeUtf8_3(n9);
                                            array2 = array3;
                                            n11 = checkInTreeIndentation;
                                            break Label_0827;
                                        }
                                        case 5: {
                                            n12 = this.decodeUtf8_2(n9);
                                            array2 = array3;
                                            n11 = checkInTreeIndentation;
                                            break Label_0827;
                                        }
                                        case 4: {
                                            this.reportInvalidInitial(n9);
                                        }
                                        case 9: {
                                            break Label_0718;
                                        }
                                        case 3: {
                                            this.markLF();
                                            array2 = array3;
                                            n11 = checkInTreeIndentation;
                                            n12 = n9;
                                            break Label_0827;
                                        }
                                        case 1: {
                                            this.handleInvalidXmlChar(n9);
                                        }
                                        case 2: {
                                            if (super._inputPtr >= super._inputEnd) {
                                                this.loadMoreGuaranteed();
                                            }
                                            final int inputPtr3 = super._inputPtr;
                                            if (inputBuffer[inputPtr3] == 10) {
                                                super._inputPtr = inputPtr3 + 1;
                                            }
                                            this.markLF();
                                            n12 = 10;
                                            n11 = checkInTreeIndentation;
                                            array2 = array3;
                                            break Label_0827;
                                        }
                                    }
                                    array3 = super._textBuilder.finishCurrentSegment();
                                    n11 = 0;
                                    break Label_0638;
                                }
                                n11 = n20;
                            }
                            n12 = ((n19 & 0x3FF) | 0xDC00);
                            array2 = array3;
                        }
                        array2[n11] = (char)n12;
                        checkInTreeIndentation = n11 + 1;
                        continue Label_0728;
                    }
                    array3[checkInTreeIndentation] = (char)n9;
                    i = inputPtr;
                    ++checkInTreeIndentation;
                }
                super._inputPtr = i;
                array2 = array3;
            }
            --super._inputPtr;
        }
        super._textBuilder.setCurrentLength(checkInTreeIndentation);
        if (super._cfgCoalescing && !super._entityPending) {
            this.finishCoalescedText();
        }
    }
    
    public final void finishCoalescedCData() {
        final int[] other_CHARS = super._charTypes.OTHER_CHARS;
        final byte[] inputBuffer = super._inputBuffer;
        char[] array = super._textBuilder.getBufferWithoutReset();
        int currentLength = super._textBuilder.getCurrentLength();
        int currentLength2 = 0;
    Block_16:
        while (true) {
            int n;
            if ((n = super._inputPtr) >= super._inputEnd) {
                this.loadMoreGuaranteed();
                n = super._inputPtr;
            }
            final int length = array.length;
            final int n2 = 0;
            int n3;
            if ((n3 = currentLength) >= length) {
                array = super._textBuilder.finishCurrentSegment();
                n3 = 0;
            }
            final int inputEnd = super._inputEnd;
            final int n4 = array.length - n3 + n;
            currentLength = n3;
            int i = n;
            int n5;
            if (n4 < (n5 = inputEnd)) {
                n5 = n4;
                i = n;
                currentLength = n3;
            }
            while (i < n5) {
                final int inputPtr = i + 1;
                final int n6 = inputBuffer[i] & 0xFF;
                final int n7 = other_CHARS[n6];
                if (n7 != 0) {
                    super._inputPtr = inputPtr;
                    int n8 = 0;
                    int n9 = 0;
                    Label_0572: {
                        if (n7 != 11) {
                            switch (n7) {
                                default: {
                                    n8 = currentLength;
                                    n9 = n6;
                                    break Label_0572;
                                }
                                case 7: {
                                    final int decodeUtf8_4 = this.decodeUtf8_4(n6);
                                    final int n10 = currentLength + 1;
                                    array[currentLength] = (char)(decodeUtf8_4 >> 10 | 0xD800);
                                    int n11;
                                    if (n10 >= array.length) {
                                        array = super._textBuilder.finishCurrentSegment();
                                        n11 = n2;
                                    }
                                    else {
                                        n11 = n10;
                                    }
                                    final int n12 = 0xDC00 | (decodeUtf8_4 & 0x3FF);
                                    n8 = n11;
                                    n9 = n12;
                                    break Label_0572;
                                }
                                case 6: {
                                    final int decodeUtf8_5 = this.decodeUtf8_3(n6);
                                    n8 = currentLength;
                                    n9 = decodeUtf8_5;
                                    break Label_0572;
                                }
                                case 5: {
                                    final int decodeUtf8_6 = this.decodeUtf8_2(n6);
                                    n8 = currentLength;
                                    n9 = decodeUtf8_6;
                                    break Label_0572;
                                }
                                case 4: {
                                    this.reportInvalidInitial(n6);
                                    break;
                                }
                                case 3: {
                                    this.markLF();
                                    n8 = currentLength;
                                    n9 = n6;
                                    break Label_0572;
                                }
                                case 1: {
                                    this.handleInvalidXmlChar(n6);
                                }
                                case 2: {
                                    if (super._inputPtr >= super._inputEnd) {
                                        this.loadMoreGuaranteed();
                                    }
                                    final int inputPtr2 = super._inputPtr;
                                    if (inputBuffer[inputPtr2] == 10) {
                                        super._inputPtr = inputPtr2 + 1;
                                    }
                                    this.markLF();
                                    final int n13 = 10;
                                    n8 = currentLength;
                                    n9 = n13;
                                    break Label_0572;
                                }
                            }
                        }
                        int n14 = 0;
                        byte b;
                        while (true) {
                            if (super._inputPtr >= super._inputEnd) {
                                this.loadMoreGuaranteed();
                            }
                            final byte[] inputBuffer2 = super._inputBuffer;
                            final int inputPtr3 = super._inputPtr;
                            b = inputBuffer2[inputPtr3];
                            if (b != 93) {
                                break;
                            }
                            super._inputPtr = inputPtr3 + 1;
                            ++n14;
                        }
                        int n15;
                        if (b == 62 && n14 >= 1) {
                            n15 = 1;
                        }
                        else {
                            n15 = 0;
                        }
                        char[] array2 = array;
                        currentLength2 = currentLength;
                        int n16 = n14;
                        int n17 = n15;
                        while (true) {
                            Label_0480: {
                                if (n15 == 0) {
                                    break Label_0480;
                                }
                                n16 = n14 - 1;
                                n17 = n15;
                                currentLength2 = currentLength;
                                array2 = array;
                            }
                            if (n16 > 0) {
                                currentLength = currentLength2 + 1;
                                array2[currentLength2] = ']';
                                if (currentLength >= array2.length) {
                                    array = super._textBuilder.finishCurrentSegment();
                                    currentLength = 0;
                                    n14 = n16;
                                    n15 = n17;
                                    continue;
                                }
                                array = array2;
                                n14 = n16;
                                n15 = n17;
                                continue;
                            }
                            else {
                                array = array2;
                                n8 = currentLength2;
                                n9 = n6;
                                if (n17 != 0) {
                                    break Block_16;
                                }
                            }
                            break;
                        }
                    }
                    array[n8] = (char)n9;
                    currentLength = n8 + 1;
                    continue Block_16;
                }
                array[currentLength] = (char)n6;
                i = inputPtr;
                ++currentLength;
            }
            super._inputPtr = i;
        }
        ++super._inputPtr;
        super._textBuilder.setCurrentLength(currentLength2);
    }
    
    public final void finishCoalescedCharacters() {
        final int[] text_CHARS = super._charTypes.TEXT_CHARS;
        final byte[] inputBuffer = super._inputBuffer;
        char[] array = super._textBuilder.getBufferWithoutReset();
        int currentLength = super._textBuilder.getCurrentLength();
        Label_0609: {
        Label_0599:
            while (true) {
                int n;
                if ((n = super._inputPtr) >= super._inputEnd) {
                    this.loadMoreGuaranteed();
                    n = super._inputPtr;
                }
                final int length = array.length;
                final int n2 = 0;
                int n3;
                if ((n3 = currentLength) >= length) {
                    array = super._textBuilder.finishCurrentSegment();
                    n3 = 0;
                }
                final int inputEnd = super._inputEnd;
                final int n4 = array.length - n3 + n;
                currentLength = n3;
                int i = n;
                int n5;
                if (n4 < (n5 = inputEnd)) {
                    n5 = n4;
                    i = n;
                    currentLength = n3;
                }
                while (i < n5) {
                    final int inputPtr = i + 1;
                    final int n6 = inputBuffer[i] & 0xFF;
                    final int n7 = text_CHARS[n6];
                    if (n7 != 0) {
                        super._inputPtr = inputPtr;
                        char[] array2 = null;
                        int n8 = 0;
                        int n9 = 0;
                        Label_0690: {
                            int n16 = 0;
                            int n17 = 0;
                            Label_0517: {
                                switch (n7) {
                                    default: {
                                        array2 = array;
                                        n8 = currentLength;
                                        n9 = n6;
                                        break Label_0690;
                                    }
                                    case 11: {
                                        int n10 = 1;
                                        byte b;
                                        while (true) {
                                            if (super._inputPtr >= super._inputEnd) {
                                                this.loadMoreGuaranteed();
                                            }
                                            final int inputPtr2 = super._inputPtr;
                                            b = inputBuffer[inputPtr2];
                                            if (b != 93) {
                                                break;
                                            }
                                            super._inputPtr = inputPtr2 + 1;
                                            ++n10;
                                        }
                                        char[] finishCurrentSegment = array;
                                        int n11 = currentLength;
                                        int n12 = n10;
                                        if (b == 62) {
                                            finishCurrentSegment = array;
                                            n11 = currentLength;
                                            if ((n12 = n10) > 1) {
                                                this.reportIllegalCDataEnd();
                                                n12 = n10;
                                                n11 = currentLength;
                                                finishCurrentSegment = array;
                                            }
                                        }
                                        while (true) {
                                            array2 = finishCurrentSegment;
                                            n8 = n11;
                                            n9 = n6;
                                            if (n12 <= 1) {
                                                break Label_0690;
                                            }
                                            final int n13 = n11 + 1;
                                            finishCurrentSegment[n11] = ']';
                                            if (n13 >= finishCurrentSegment.length) {
                                                finishCurrentSegment = super._textBuilder.finishCurrentSegment();
                                                n11 = 0;
                                            }
                                            else {
                                                n11 = n13;
                                            }
                                            --n12;
                                        }
                                        break;
                                    }
                                    case 10: {
                                        final int handleEntityInText = this.handleEntityInText(false);
                                        if (handleEntityInText == 0) {
                                            super._entityPending = true;
                                            break Label_0609;
                                        }
                                        array2 = array;
                                        n8 = currentLength;
                                        n9 = handleEntityInText;
                                        if (handleEntityInText >> 16 == 0) {
                                            break Label_0690;
                                        }
                                        final int n14 = handleEntityInText - 65536;
                                        final int n15 = currentLength + 1;
                                        array[currentLength] = (char)(n14 >> 10 | 0xD800);
                                        n16 = n14;
                                        if ((n17 = n15) >= array.length) {
                                            n16 = n14;
                                            break;
                                        }
                                        break Label_0517;
                                    }
                                    case 7: {
                                        final int decodeUtf8_4 = this.decodeUtf8_4(n6);
                                        final int n18 = currentLength + 1;
                                        array[currentLength] = (char)(decodeUtf8_4 >> 10 | 0xD800);
                                        n16 = decodeUtf8_4;
                                        n17 = n18;
                                        if (n18 >= array.length) {
                                            n16 = decodeUtf8_4;
                                            break;
                                        }
                                        break Label_0517;
                                    }
                                    case 6: {
                                        if (super._inputEnd - inputPtr >= 2) {
                                            n9 = this.decodeUtf8_3fast(n6);
                                            array2 = array;
                                            n8 = currentLength;
                                            break Label_0690;
                                        }
                                        n9 = this.decodeUtf8_3(n6);
                                        array2 = array;
                                        n8 = currentLength;
                                        break Label_0690;
                                    }
                                    case 5: {
                                        n9 = this.decodeUtf8_2(n6);
                                        array2 = array;
                                        n8 = currentLength;
                                        break Label_0690;
                                    }
                                    case 4: {
                                        this.reportInvalidInitial(n6);
                                    }
                                    case 9: {
                                        break Label_0599;
                                    }
                                    case 3: {
                                        this.markLF();
                                        array2 = array;
                                        n8 = currentLength;
                                        n9 = n6;
                                        break Label_0690;
                                    }
                                    case 1: {
                                        this.handleInvalidXmlChar(n6);
                                    }
                                    case 2: {
                                        if (super._inputPtr >= super._inputEnd) {
                                            this.loadMoreGuaranteed();
                                        }
                                        final int inputPtr3 = super._inputPtr;
                                        if (inputBuffer[inputPtr3] == 10) {
                                            super._inputPtr = inputPtr3 + 1;
                                        }
                                        this.markLF();
                                        n9 = 10;
                                        n8 = currentLength;
                                        array2 = array;
                                        break Label_0690;
                                    }
                                }
                                array = super._textBuilder.finishCurrentSegment();
                                n17 = n2;
                            }
                            n9 = ((n16 & 0x3FF) | 0xDC00);
                            array2 = array;
                            n8 = n17;
                        }
                        array2[n8] = (char)n9;
                        currentLength = n8 + 1;
                        array = array2;
                        continue Label_0609;
                    }
                    array[currentLength] = (char)n6;
                    i = inputPtr;
                    ++currentLength;
                }
                super._inputPtr = i;
            }
            --super._inputPtr;
        }
        super._textBuilder.setCurrentLength(currentLength);
    }
    
    public final void finishCoalescedText() {
        while (super._inputPtr < super._inputEnd || this.loadMore()) {
            final byte[] inputBuffer = super._inputBuffer;
            final int inputPtr = super._inputPtr;
            if (inputBuffer[inputPtr] == 60) {
                if (inputPtr + 3 >= super._inputEnd && !this.loadAndRetain(3)) {
                    return;
                }
                final byte[] inputBuffer2 = super._inputBuffer;
                final int inputPtr2 = super._inputPtr;
                if (inputBuffer2[inputPtr2 + 1] != 33 || inputBuffer2[inputPtr2 + 2] != 91) {
                    return;
                }
                super._inputPtr = inputPtr2 + 3;
                for (int i = 0; i < 6; ++i) {
                    if (super._inputPtr >= super._inputEnd) {
                        this.loadMoreGuaranteed();
                    }
                    final byte b = super._inputBuffer[super._inputPtr++];
                    if (b != (byte)"CDATA[".charAt(i)) {
                        final int decodeCharForError = this.decodeCharForError(b);
                        final StringBuilder sb = new StringBuilder();
                        sb.append(" (expected '");
                        sb.append("CDATA[".charAt(i));
                        sb.append("' for CDATA section)");
                        this.reportTreeUnexpChar(decodeCharForError, sb.toString());
                    }
                }
                this.finishCoalescedCData();
            }
            else {
                this.finishCoalescedCharacters();
                if (super._entityPending) {
                    return;
                }
                continue;
            }
        }
    }
    
    @Override
    public final void finishComment() {
        final int[] other_CHARS = super._charTypes.OTHER_CHARS;
        final byte[] inputBuffer = super._inputBuffer;
        char[] resetWithEmpty = super._textBuilder.resetWithEmpty();
        int currentLength = 0;
        int inputPtr3 = 0;
    Block_11:
        while (true) {
            int n;
            if ((n = super._inputPtr) >= super._inputEnd) {
                this.loadMoreGuaranteed();
                n = super._inputPtr;
            }
            char[] array = resetWithEmpty;
            int n2;
            if ((n2 = currentLength) >= resetWithEmpty.length) {
                array = super._textBuilder.finishCurrentSegment();
                n2 = 0;
            }
            final int inputEnd = super._inputEnd;
            final int n3 = array.length - n2 + n;
            currentLength = n2;
            int i = n;
            int n4;
            if (n3 < (n4 = inputEnd)) {
                n4 = n3;
                i = n;
                currentLength = n2;
            }
            while (i < n4) {
                final int inputPtr = i + 1;
                int n5 = inputBuffer[i] & 0xFF;
                final int n6 = other_CHARS[n5];
                if (n6 != 0) {
                    super._inputPtr = inputPtr;
                    int n7 = 0;
                    Label_0462: {
                        if (n6 != 13) {
                            switch (n6) {
                                default: {
                                    n7 = currentLength;
                                    break Label_0462;
                                }
                                case 7: {
                                    final int decodeUtf8_4 = this.decodeUtf8_4(n5);
                                    int n8 = currentLength + 1;
                                    array[currentLength] = (char)(decodeUtf8_4 >> 10 | 0xD800);
                                    if (n8 >= array.length) {
                                        array = super._textBuilder.finishCurrentSegment();
                                        n8 = 0;
                                    }
                                    final int n9 = (decodeUtf8_4 & 0x3FF) | 0xDC00;
                                    n7 = n8;
                                    n5 = n9;
                                    break Label_0462;
                                }
                                case 6: {
                                    n5 = this.decodeUtf8_3(n5);
                                    n7 = currentLength;
                                    break Label_0462;
                                }
                                case 5: {
                                    n5 = this.decodeUtf8_2(n5);
                                    n7 = currentLength;
                                    break Label_0462;
                                }
                                case 4: {
                                    this.reportInvalidInitial(n5);
                                    break;
                                }
                                case 3: {
                                    this.markLF();
                                    n7 = currentLength;
                                    break Label_0462;
                                }
                                case 1: {
                                    this.handleInvalidXmlChar(n5);
                                }
                                case 2: {
                                    if (super._inputPtr >= super._inputEnd) {
                                        this.loadMoreGuaranteed();
                                    }
                                    final int inputPtr2 = super._inputPtr;
                                    if (inputBuffer[inputPtr2] == 10) {
                                        super._inputPtr = inputPtr2 + 1;
                                    }
                                    this.markLF();
                                    n5 = 10;
                                    n7 = currentLength;
                                    break Label_0462;
                                }
                            }
                        }
                        if (super._inputPtr >= super._inputEnd) {
                            this.loadMoreGuaranteed();
                        }
                        final byte[] inputBuffer2 = super._inputBuffer;
                        inputPtr3 = super._inputPtr;
                        n7 = currentLength;
                        if (inputBuffer2[inputPtr3] == 45) {
                            break Block_11;
                        }
                    }
                    array[n7] = (char)n5;
                    currentLength = n7 + 1;
                    resetWithEmpty = array;
                    continue Block_11;
                }
                array[currentLength] = (char)n5;
                i = inputPtr;
                ++currentLength;
            }
            super._inputPtr = i;
            resetWithEmpty = array;
        }
        if ((super._inputPtr = inputPtr3 + 1) >= super._inputEnd) {
            this.loadMoreGuaranteed();
        }
        if (super._inputBuffer[super._inputPtr++] != 62) {
            this.reportDoubleHyphenInComments();
        }
        super._textBuilder.setCurrentLength(currentLength);
    }
    
    @Override
    public final void finishDTD(final boolean b) {
        char[] resetWithEmpty;
        if (b) {
            resetWithEmpty = super._textBuilder.resetWithEmpty();
        }
        else {
            resetWithEmpty = null;
        }
        final int[] dtd_CHARS = super._charTypes.DTD_CHARS;
        int n = 0;
        int n3;
        int n2 = n3 = 0;
        int currentLength = 0;
        char[] array = null;
    Block_9:
        while (true) {
            int n4;
            if ((n4 = super._inputPtr) >= super._inputEnd) {
                this.loadMoreGuaranteed();
                n4 = super._inputPtr;
            }
            final int inputEnd = super._inputEnd;
            currentLength = n;
            int i = n4;
            int n5 = inputEnd;
            if ((array = resetWithEmpty) != null) {
                int n6 = n;
                char[] finishCurrentSegment = resetWithEmpty;
                if (n >= resetWithEmpty.length) {
                    finishCurrentSegment = super._textBuilder.finishCurrentSegment();
                    n6 = 0;
                }
                final int n7 = finishCurrentSegment.length - n6 + n4;
                currentLength = n6;
                i = n4;
                n5 = inputEnd;
                array = finishCurrentSegment;
                if (n7 < inputEnd) {
                    n5 = n7;
                    array = finishCurrentSegment;
                    i = n4;
                    currentLength = n6;
                }
            }
            while (i < n5) {
                final byte[] inputBuffer = super._inputBuffer;
                final int inputPtr = i + 1;
                final int n8 = inputBuffer[i] & 0xFF;
                final int n9 = dtd_CHARS[n8];
                if (n9 != 0) {
                    super._inputPtr = inputPtr;
                    int n10 = 0;
                    int n11 = 0;
                    int n12 = 0;
                    int n13 = 0;
                    char[] finishCurrentSegment2 = null;
                    switch (n9) {
                        default: {
                            n10 = currentLength;
                            n11 = n2;
                            n12 = n3;
                            n13 = n8;
                            finishCurrentSegment2 = array;
                            break;
                        }
                        case 11: {
                            n10 = currentLength;
                            n11 = n2;
                            n12 = n3;
                            n13 = n8;
                            finishCurrentSegment2 = array;
                            if (n3 != 0) {
                                break;
                            }
                            n10 = currentLength;
                            n11 = n2;
                            n12 = n3;
                            n13 = n8;
                            finishCurrentSegment2 = array;
                            if (n2 == 0) {
                                break Block_9;
                            }
                            break;
                        }
                        case 10: {
                            n10 = currentLength;
                            n11 = n2;
                            n12 = n3;
                            n13 = n8;
                            finishCurrentSegment2 = array;
                            if (n2 == 0) {
                                n12 = 0;
                                n10 = currentLength;
                                n11 = n2;
                                n13 = n8;
                                finishCurrentSegment2 = array;
                                break;
                            }
                            break;
                        }
                        case 9: {
                            n10 = currentLength;
                            n11 = n2;
                            n12 = n3;
                            n13 = n8;
                            finishCurrentSegment2 = array;
                            if (n3 == 0) {
                                n12 = 1;
                                n10 = currentLength;
                                n11 = n2;
                                n13 = n8;
                                finishCurrentSegment2 = array;
                                break;
                            }
                            break;
                        }
                        case 7: {
                            final int decodeUtf8_4 = this.decodeUtf8_4(n8);
                            n10 = currentLength;
                            n11 = n2;
                            n12 = n3;
                            n13 = decodeUtf8_4;
                            finishCurrentSegment2 = array;
                            if (array == null) {
                                break;
                            }
                            n10 = currentLength + 1;
                            array[currentLength] = (char)(decodeUtf8_4 >> 10 | 0xD800);
                            n13 = (0xDC00 | (decodeUtf8_4 & 0x3FF));
                            if (n10 >= array.length) {
                                finishCurrentSegment2 = super._textBuilder.finishCurrentSegment();
                                n10 = 0;
                                n11 = n2;
                                n12 = n3;
                                break;
                            }
                            n11 = n2;
                            n12 = n3;
                            finishCurrentSegment2 = array;
                            break;
                        }
                        case 6: {
                            n13 = this.decodeUtf8_3(n8);
                            n10 = currentLength;
                            n11 = n2;
                            n12 = n3;
                            finishCurrentSegment2 = array;
                            break;
                        }
                        case 5: {
                            n13 = this.decodeUtf8_2(n8);
                            n10 = currentLength;
                            n11 = n2;
                            n12 = n3;
                            finishCurrentSegment2 = array;
                            break;
                        }
                        case 4: {
                            this.reportInvalidInitial(n8);
                        }
                        case 8: {
                            if (n2 == 0) {
                                n11 = n8;
                                n10 = currentLength;
                                n12 = n3;
                                n13 = n8;
                                finishCurrentSegment2 = array;
                                break;
                            }
                            n10 = currentLength;
                            n11 = n2;
                            n12 = n3;
                            n13 = n8;
                            finishCurrentSegment2 = array;
                            if (n2 == n8) {
                                n11 = 0;
                                n10 = currentLength;
                                n12 = n3;
                                n13 = n8;
                                finishCurrentSegment2 = array;
                                break;
                            }
                            break;
                        }
                        case 3: {
                            this.markLF();
                            n10 = currentLength;
                            n11 = n2;
                            n12 = n3;
                            n13 = n8;
                            finishCurrentSegment2 = array;
                            break;
                        }
                        case 1: {
                            this.handleInvalidXmlChar(n8);
                        }
                        case 2: {
                            if (super._inputPtr >= super._inputEnd) {
                                this.loadMoreGuaranteed();
                            }
                            final byte[] inputBuffer2 = super._inputBuffer;
                            final int inputPtr2 = super._inputPtr;
                            if (inputBuffer2[inputPtr2] == 10) {
                                super._inputPtr = inputPtr2 + 1;
                            }
                            this.markLF();
                            n13 = 10;
                            finishCurrentSegment2 = array;
                            n12 = n3;
                            n11 = n2;
                            n10 = currentLength;
                            break;
                        }
                    }
                    n = n10;
                    n2 = n11;
                    n3 = n12;
                    if ((resetWithEmpty = finishCurrentSegment2) != null) {
                        finishCurrentSegment2[n10] = (char)n13;
                        n = n10 + 1;
                        n2 = n11;
                        n3 = n12;
                        resetWithEmpty = finishCurrentSegment2;
                        continue Block_9;
                    }
                    continue Block_9;
                }
                else {
                    int n14 = currentLength;
                    if (array != null) {
                        array[currentLength] = (char)n8;
                        n14 = currentLength + 1;
                    }
                    i = inputPtr;
                    currentLength = n14;
                }
            }
            super._inputPtr = i;
            n = currentLength;
            resetWithEmpty = array;
        }
        if (array != null) {
            super._textBuilder.setCurrentLength(currentLength);
        }
        final byte skipInternalWs = this.skipInternalWs(false, null);
        if (skipInternalWs != 62) {
            this.throwUnexpectedChar(this.decodeCharForError(skipInternalWs), " expected '>' after the internal subset");
        }
    }
    
    @Override
    public final void finishPI() {
        final int[] other_CHARS = super._charTypes.OTHER_CHARS;
        final byte[] inputBuffer = super._inputBuffer;
        char[] resetWithEmpty = super._textBuilder.resetWithEmpty();
        int currentLength = 0;
        int inputPtr3 = 0;
    Block_11:
        while (true) {
            int n;
            if ((n = super._inputPtr) >= super._inputEnd) {
                this.loadMoreGuaranteed();
                n = super._inputPtr;
            }
            char[] array = resetWithEmpty;
            int n2;
            if ((n2 = currentLength) >= resetWithEmpty.length) {
                array = super._textBuilder.finishCurrentSegment();
                n2 = 0;
            }
            final int inputEnd = super._inputEnd;
            final int n3 = array.length - n2 + n;
            currentLength = n2;
            int i = n;
            int n4;
            if (n3 < (n4 = inputEnd)) {
                n4 = n3;
                i = n;
                currentLength = n2;
            }
            while (i < n4) {
                final int inputPtr = i + 1;
                int n5 = inputBuffer[i] & 0xFF;
                final int n6 = other_CHARS[n5];
                if (n6 != 0) {
                    super._inputPtr = inputPtr;
                    int n7 = 0;
                    Label_0417: {
                        if (n6 != 12) {
                            switch (n6) {
                                default: {
                                    n7 = currentLength;
                                    break Label_0417;
                                }
                                case 7: {
                                    final int decodeUtf8_4 = this.decodeUtf8_4(n5);
                                    int n8 = currentLength + 1;
                                    array[currentLength] = (char)(decodeUtf8_4 >> 10 | 0xD800);
                                    if (n8 >= array.length) {
                                        array = super._textBuilder.finishCurrentSegment();
                                        n8 = 0;
                                    }
                                    final int n9 = (decodeUtf8_4 & 0x3FF) | 0xDC00;
                                    n7 = n8;
                                    n5 = n9;
                                    break Label_0417;
                                }
                                case 6: {
                                    n5 = this.decodeUtf8_3(n5);
                                    n7 = currentLength;
                                    break Label_0417;
                                }
                                case 5: {
                                    n5 = this.decodeUtf8_2(n5);
                                    n7 = currentLength;
                                    break Label_0417;
                                }
                                case 4: {
                                    this.reportInvalidInitial(n5);
                                    break;
                                }
                                case 3: {
                                    this.markLF();
                                    n7 = currentLength;
                                    break Label_0417;
                                }
                                case 1: {
                                    this.handleInvalidXmlChar(n5);
                                }
                                case 2: {
                                    if (super._inputPtr >= super._inputEnd) {
                                        this.loadMoreGuaranteed();
                                    }
                                    final int inputPtr2 = super._inputPtr;
                                    if (inputBuffer[inputPtr2] == 10) {
                                        super._inputPtr = inputPtr2 + 1;
                                    }
                                    this.markLF();
                                    n5 = 10;
                                    n7 = currentLength;
                                    break Label_0417;
                                }
                            }
                        }
                        if (super._inputPtr >= super._inputEnd) {
                            this.loadMoreGuaranteed();
                        }
                        final byte[] inputBuffer2 = super._inputBuffer;
                        inputPtr3 = super._inputPtr;
                        n7 = currentLength;
                        if (inputBuffer2[inputPtr3] == 62) {
                            break Block_11;
                        }
                    }
                    array[n7] = (char)n5;
                    currentLength = n7 + 1;
                    resetWithEmpty = array;
                    continue Block_11;
                }
                array[currentLength] = (char)n5;
                i = inputPtr;
                ++currentLength;
            }
            super._inputPtr = i;
            resetWithEmpty = array;
        }
        super._inputPtr = inputPtr3 + 1;
        super._textBuilder.setCurrentLength(currentLength);
    }
    
    @Override
    public final void finishSpace() {
        final int tmpChar = super._tmpChar;
        final int n = 0;
        char[] array;
        int checkPrologIndentation;
        if (tmpChar != 13 && tmpChar != 10) {
            array = super._textBuilder.resetWithEmpty();
            array[0] = (char)tmpChar;
            checkPrologIndentation = 1;
        }
        else {
            checkPrologIndentation = this.checkPrologIndentation(tmpChar);
            if (checkPrologIndentation < 0) {
                return;
            }
            array = super._textBuilder.getBufferWithoutReset();
        }
        int inputPtr = super._inputPtr;
        while (true) {
            int inputPtr2 = inputPtr;
            if (inputPtr >= super._inputEnd) {
                if (!this.loadMore()) {
                    break;
                }
                inputPtr2 = super._inputPtr;
            }
            final int n2 = super._inputBuffer[inputPtr2] & 0xFF;
            if (n2 > 32) {
                inputPtr = inputPtr2;
                break;
            }
            inputPtr = inputPtr2 + 1;
            int n3;
            int n4;
            if (n2 == 10) {
                this.markLF(inputPtr);
                n3 = inputPtr;
                n4 = n2;
            }
            else if (n2 == 13) {
                int inputPtr3;
                if ((inputPtr3 = inputPtr) >= super._inputEnd) {
                    if (!this.loadMore()) {
                        if (checkPrologIndentation >= array.length) {
                            array = super._textBuilder.finishCurrentSegment();
                            checkPrologIndentation = n;
                        }
                        final int n5 = checkPrologIndentation + 1;
                        array[checkPrologIndentation] = '\n';
                        checkPrologIndentation = n5;
                        break;
                    }
                    inputPtr3 = super._inputPtr;
                }
                n3 = inputPtr3;
                if (super._inputBuffer[inputPtr3] == 10) {
                    n3 = inputPtr3 + 1;
                }
                this.markLF(n3);
                n4 = 10;
            }
            else {
                n3 = inputPtr;
                if ((n4 = n2) != 32) {
                    n3 = inputPtr;
                    if ((n4 = n2) != 9) {
                        super._inputPtr = inputPtr;
                        this.throwInvalidSpace(n2);
                        n4 = n2;
                        n3 = inputPtr;
                    }
                }
            }
            int n6 = checkPrologIndentation;
            char[] finishCurrentSegment = array;
            if (checkPrologIndentation >= array.length) {
                finishCurrentSegment = super._textBuilder.finishCurrentSegment();
                n6 = 0;
            }
            finishCurrentSegment[n6] = (char)n4;
            checkPrologIndentation = n6 + 1;
            array = finishCurrentSegment;
            inputPtr = n3;
        }
        super._inputPtr = inputPtr;
        super._textBuilder.setCurrentLength(checkPrologIndentation);
    }
    
    @Override
    public final void finishToken() {
        super._tokenIncomplete = false;
        final int currToken = super._currToken;
        if (currToken != 3) {
            if (currToken != 4) {
                if (currToken != 5) {
                    if (currToken != 6) {
                        if (currToken != 11) {
                            if (currToken != 12) {
                                ErrorConsts.throwInternalError();
                            }
                            else {
                                this.finishCData();
                            }
                        }
                        else {
                            this.finishDTD(true);
                        }
                    }
                    else {
                        this.finishSpace();
                    }
                }
                else {
                    this.finishComment();
                }
            }
            else {
                this.finishCharacters();
            }
        }
        else {
            this.finishPI();
        }
    }
    
    @Override
    public final int handleEntityInText(final boolean b) {
        if (super._inputPtr >= super._inputEnd) {
            this.loadMoreGuaranteed();
        }
        final byte[] inputBuffer = super._inputBuffer;
        final int inputPtr = super._inputPtr;
        final int inputPtr2 = inputPtr + 1;
        super._inputPtr = inputPtr2;
        byte b2 = inputBuffer[inputPtr];
        if (b2 == 35) {
            return this.handleCharEntity();
        }
        String s;
        if (b2 == 97) {
            if (inputPtr2 < super._inputEnd) {
                super._inputPtr = inputPtr2 + 1;
                b2 = inputBuffer[inputPtr2];
            }
            else {
                b2 = this.loadOne();
            }
            if (b2 == 109) {
                final int inputPtr3 = super._inputPtr;
                if (inputPtr3 < super._inputEnd) {
                    final byte[] inputBuffer2 = super._inputBuffer;
                    super._inputPtr = inputPtr3 + 1;
                    b2 = inputBuffer2[inputPtr3];
                }
                else {
                    b2 = this.loadOne();
                }
                if (b2 == 112) {
                    final int inputPtr4 = super._inputPtr;
                    if (inputPtr4 < super._inputEnd) {
                        final byte[] inputBuffer3 = super._inputBuffer;
                        super._inputPtr = inputPtr4 + 1;
                        b2 = inputBuffer3[inputPtr4];
                    }
                    else {
                        b2 = this.loadOne();
                    }
                    if (b2 == 59) {
                        return 38;
                    }
                    s = "amp";
                }
                else {
                    s = "am";
                }
            }
            else if (b2 == 112) {
                final int inputPtr5 = super._inputPtr;
                if (inputPtr5 < super._inputEnd) {
                    final byte[] inputBuffer4 = super._inputBuffer;
                    super._inputPtr = inputPtr5 + 1;
                    b2 = inputBuffer4[inputPtr5];
                }
                else {
                    b2 = this.loadOne();
                }
                if (b2 == 111) {
                    final int inputPtr6 = super._inputPtr;
                    if (inputPtr6 < super._inputEnd) {
                        final byte[] inputBuffer5 = super._inputBuffer;
                        super._inputPtr = inputPtr6 + 1;
                        b2 = inputBuffer5[inputPtr6];
                    }
                    else {
                        b2 = this.loadOne();
                    }
                    if (b2 == 115) {
                        final int inputPtr7 = super._inputPtr;
                        if (inputPtr7 < super._inputEnd) {
                            final byte[] inputBuffer6 = super._inputBuffer;
                            super._inputPtr = inputPtr7 + 1;
                            b2 = inputBuffer6[inputPtr7];
                        }
                        else {
                            b2 = this.loadOne();
                        }
                        if (b2 == 59) {
                            return 39;
                        }
                        s = "apos";
                    }
                    else {
                        s = "apo";
                    }
                }
                else {
                    s = "ap";
                }
            }
            else {
                s = "a";
            }
        }
        else if (b2 == 108) {
            if (inputPtr2 < super._inputEnd) {
                super._inputPtr = inputPtr2 + 1;
                b2 = inputBuffer[inputPtr2];
            }
            else {
                b2 = this.loadOne();
            }
            if (b2 == 116) {
                final int inputPtr8 = super._inputPtr;
                if (inputPtr8 < super._inputEnd) {
                    final byte[] inputBuffer7 = super._inputBuffer;
                    super._inputPtr = inputPtr8 + 1;
                    b2 = inputBuffer7[inputPtr8];
                }
                else {
                    b2 = this.loadOne();
                }
                if (b2 == 59) {
                    return 60;
                }
                s = "lt";
            }
            else {
                s = "l";
            }
        }
        else if (b2 == 103) {
            if (inputPtr2 < super._inputEnd) {
                super._inputPtr = inputPtr2 + 1;
                b2 = inputBuffer[inputPtr2];
            }
            else {
                b2 = this.loadOne();
            }
            if (b2 == 116) {
                final int inputPtr9 = super._inputPtr;
                if (inputPtr9 < super._inputEnd) {
                    final byte[] inputBuffer8 = super._inputBuffer;
                    super._inputPtr = inputPtr9 + 1;
                    b2 = inputBuffer8[inputPtr9];
                }
                else {
                    b2 = this.loadOne();
                }
                if (b2 == 59) {
                    return 62;
                }
                s = "gt";
            }
            else {
                s = "g";
            }
        }
        else if (b2 == 113) {
            if (inputPtr2 < super._inputEnd) {
                super._inputPtr = inputPtr2 + 1;
                b2 = inputBuffer[inputPtr2];
            }
            else {
                b2 = this.loadOne();
            }
            if (b2 == 117) {
                final int inputPtr10 = super._inputPtr;
                if (inputPtr10 < super._inputEnd) {
                    final byte[] inputBuffer9 = super._inputBuffer;
                    super._inputPtr = inputPtr10 + 1;
                    b2 = inputBuffer9[inputPtr10];
                }
                else {
                    b2 = this.loadOne();
                }
                if (b2 == 111) {
                    final int inputPtr11 = super._inputPtr;
                    if (inputPtr11 < super._inputEnd) {
                        final byte[] inputBuffer10 = super._inputBuffer;
                        super._inputPtr = inputPtr11 + 1;
                        b2 = inputBuffer10[inputPtr11];
                    }
                    else {
                        b2 = this.loadOne();
                    }
                    if (b2 == 116) {
                        final int inputPtr12 = super._inputPtr;
                        if (inputPtr12 < super._inputEnd) {
                            final byte[] inputBuffer11 = super._inputBuffer;
                            super._inputPtr = inputPtr12 + 1;
                            b2 = inputBuffer11[inputPtr12];
                        }
                        else {
                            b2 = this.loadOne();
                        }
                        if (b2 == 59) {
                            return 34;
                        }
                        s = "quot";
                    }
                    else {
                        s = "quo";
                    }
                }
                else {
                    s = "qu";
                }
            }
            else {
                s = "q";
            }
        }
        else {
            s = "";
        }
        final int[] name_CHARS = super._charTypes.NAME_CHARS;
        final char[] nameBuffer = super._nameBuffer;
        final int length = s.length();
        int index = 0;
        byte b3;
        char[] growArrayBy;
        int count;
        while (true) {
            b3 = b2;
            growArrayBy = nameBuffer;
            count = index;
            if (index >= length) {
                break;
            }
            nameBuffer[index] = s.charAt(index);
            ++index;
        }
        while (b3 != 59) {
            final int n = b3 & 0xFF;
            final int n2 = name_CHARS[n];
            int is10NameStartChar = 1;
            int n3 = 0;
            char[] growArrayBy2 = null;
            int n4 = 0;
            Label_1099: {
                Label_0936: {
                    if (n2 != 0 && n2 != 1 && n2 != 2) {
                        n3 = n;
                        growArrayBy2 = growArrayBy;
                        n4 = count;
                        if (n2 != 3) {
                            if (n2 != 5) {
                                if (n2 != 6) {
                                    if (n2 != 7) {
                                        break Label_0936;
                                    }
                                    final int decodeUtf8_4 = this.decodeUtf8_4(n);
                                    final boolean is10NameStartChar2 = XmlChars.is10NameStartChar(decodeUtf8_4);
                                    n3 = decodeUtf8_4;
                                    growArrayBy2 = growArrayBy;
                                    is10NameStartChar = (is10NameStartChar2 ? 1 : 0);
                                    n4 = count;
                                    if (is10NameStartChar2) {
                                        growArrayBy2 = growArrayBy;
                                        if (count >= growArrayBy.length) {
                                            growArrayBy2 = DataUtil.growArrayBy(growArrayBy, growArrayBy.length);
                                            super._nameBuffer = growArrayBy2;
                                        }
                                        final int n5 = decodeUtf8_4 - 65536;
                                        growArrayBy2[count] = (char)(n5 >> 10 | 0xD800);
                                        n3 = ((n5 & 0x3FF) | 0xDC00);
                                        n4 = count + 1;
                                        is10NameStartChar = (is10NameStartChar2 ? 1 : 0);
                                    }
                                    break Label_1099;
                                }
                                else {
                                    n3 = this.decodeUtf8_3(n);
                                }
                            }
                            else {
                                n3 = this.decodeUtf8_2(n);
                            }
                            is10NameStartChar = (XmlChars.is10NameStartChar(n3) ? 1 : 0);
                            growArrayBy2 = growArrayBy;
                            n4 = count;
                        }
                        break Label_1099;
                    }
                    else if (count > 0) {
                        n4 = count;
                        growArrayBy2 = growArrayBy;
                        n3 = n;
                        break Label_1099;
                    }
                }
                is10NameStartChar = 0;
                n3 = n;
                growArrayBy2 = growArrayBy;
                n4 = count;
            }
            if (is10NameStartChar == 0) {
                this.reportInvalidNameChar(n3, n4);
            }
            growArrayBy = growArrayBy2;
            if (n4 >= growArrayBy2.length) {
                growArrayBy = DataUtil.growArrayBy(growArrayBy2, growArrayBy2.length);
                super._nameBuffer = growArrayBy;
            }
            growArrayBy[n4] = (char)n3;
            if (super._inputPtr >= super._inputEnd) {
                this.loadMoreGuaranteed();
            }
            b3 = super._inputBuffer[super._inputPtr++];
            count = n4 + 1;
        }
        final String s2 = new String(growArrayBy, 0, count);
        super._tokenName = new PNameC(s2, null, s2, 0);
        if (super._config.willExpandEntities()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("General entity reference (&");
            sb.append(s2);
            sb.append(";) encountered in entity expanding mode: operation not (yet) implemented");
            this.reportInputProblem(sb.toString());
        }
        if (b) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("General entity reference (&");
            sb2.append(s2);
            sb2.append(";) encountered in attribute value, in non-entity-expanding mode: no way to handle it");
            this.reportInputProblem(sb2.toString());
        }
        return 0;
    }
    
    @Override
    public int handleStartElement(final byte b) {
        super._currToken = 1;
        final int n = 0;
        super._currNsCount = 0;
        PName tokenName = this.parsePName(b);
        final String prefix = tokenName.getPrefix();
        int bound;
        if (prefix == null) {
            bound = 1;
        }
        else {
            tokenName = this.bindName(tokenName, prefix);
            bound = (tokenName.isBound() ? 1 : 0);
        }
        super._tokenName = tokenName;
        super._currElem = new ElementScope(tokenName, super._currElem);
        int collectValue = 0;
    Label_0633_Outer:
        while (true) {
            if (super._inputPtr >= super._inputEnd) {
                this.loadMoreGuaranteed();
            }
            final byte b2 = super._inputBuffer[super._inputPtr++];
            final int n2 = b2 & 0xFF;
            byte b3;
            int decodeCharForError;
            if (n2 <= 32) {
                int n3 = n2;
                int n4;
                do {
                    Label_0217: {
                        if (n3 != 10) {
                            if (n3 == 13) {
                                if (super._inputPtr >= super._inputEnd) {
                                    this.loadMoreGuaranteed();
                                }
                                final byte[] inputBuffer = super._inputBuffer;
                                final int inputPtr = super._inputPtr;
                                if (inputBuffer[inputPtr] == 10) {
                                    super._inputPtr = inputPtr + 1;
                                }
                            }
                            else {
                                if (n3 != 32 && n3 != 9) {
                                    this.throwInvalidSpace(n3);
                                }
                                break Label_0217;
                            }
                        }
                        this.markLF();
                    }
                    if (super._inputPtr >= super._inputEnd) {
                        this.loadMoreGuaranteed();
                    }
                    b3 = super._inputBuffer[super._inputPtr++];
                    n4 = (b3 & 0xFF);
                } while ((n3 = n4) <= 32);
                decodeCharForError = n4;
            }
            else {
                b3 = b2;
                if ((decodeCharForError = n2) != 47) {
                    b3 = b2;
                    if ((decodeCharForError = n2) != 62) {
                        decodeCharForError = this.decodeCharForError(b2);
                        this.throwUnexpectedChar(decodeCharForError, " expected space, or '>' or \"/>\"");
                        b3 = b2;
                    }
                }
            }
            if (decodeCharForError == 47) {
                if (super._inputPtr >= super._inputEnd) {
                    this.loadMoreGuaranteed();
                }
                final byte b4 = super._inputBuffer[super._inputPtr++];
                if (b4 != 62) {
                    this.throwUnexpectedChar(this.decodeCharForError(b4), " expected '>'");
                }
                super._isEmptyTag = true;
                break;
            }
            if (decodeCharForError == 62) {
                super._isEmptyTag = false;
                break;
            }
            if (decodeCharForError == 60) {
                this.reportInputProblem("Unexpected '<' character in element (missing closing '>'?)");
            }
            final PName pName = this.parsePName(b3);
            final String prefix2 = pName.getPrefix();
            while (true) {
                Label_0631: {
                    Label_0584: {
                        int bound2;
                        PName pName2;
                        if (prefix2 == null) {
                            bound2 = bound;
                            pName2 = pName;
                            if (pName.getLocalName() == "xmlns") {
                                break Label_0584;
                            }
                            break Label_0631;
                        }
                        else {
                            if (prefix2 == "xmlns") {
                                break Label_0584;
                            }
                            final PName bindName = this.bindName(pName, prefix2);
                            bound2 = bound;
                            pName2 = bindName;
                            if (bound != 0) {
                                bound2 = (bindName.isBound() ? 1 : 0);
                                pName2 = bindName;
                            }
                            break Label_0631;
                        }
                        byte b5;
                        int n5;
                        while (true) {
                            if (super._inputPtr >= super._inputEnd) {
                                this.loadMoreGuaranteed();
                            }
                            final byte[] inputBuffer2 = super._inputBuffer;
                            final int inputPtr2 = super._inputPtr;
                            final int inputPtr3 = inputPtr2 + 1;
                            super._inputPtr = inputPtr3;
                            b5 = inputBuffer2[inputPtr2];
                            n5 = (b5 & 0xFF);
                            if (n5 > 32) {
                                break;
                            }
                            if (n5 != 10) {
                                if (n5 == 13) {
                                    if (inputPtr3 >= super._inputEnd) {
                                        this.loadMoreGuaranteed();
                                    }
                                    final byte[] inputBuffer3 = super._inputBuffer;
                                    final int inputPtr4 = super._inputPtr;
                                    if (inputBuffer3[inputPtr4] == 10) {
                                        super._inputPtr = inputPtr4 + 1;
                                    }
                                }
                                else {
                                    if (n5 != 32 && n5 != 9) {
                                        this.throwInvalidSpace(n5);
                                        continue Label_0633_Outer;
                                    }
                                    continue Label_0633_Outer;
                                }
                            }
                            this.markLF();
                        }
                        if (n5 != 61) {
                            this.throwUnexpectedChar(this.decodeCharForError(b5), " expected '='");
                        }
                        byte b6;
                        int n6;
                        while (true) {
                            if (super._inputPtr >= super._inputEnd) {
                                this.loadMoreGuaranteed();
                            }
                            final byte[] inputBuffer4 = super._inputBuffer;
                            final int inputPtr5 = super._inputPtr;
                            final int inputPtr6 = inputPtr5 + 1;
                            super._inputPtr = inputPtr6;
                            b6 = inputBuffer4[inputPtr5];
                            n6 = (b6 & 0xFF);
                            if (n6 > 32) {
                                break;
                            }
                            if (n6 != 10) {
                                if (n6 == 13) {
                                    if (inputPtr6 >= super._inputEnd) {
                                        this.loadMoreGuaranteed();
                                    }
                                    final byte[] inputBuffer5 = super._inputBuffer;
                                    final int inputPtr7 = super._inputPtr;
                                    if (inputBuffer5[inputPtr7] == 10) {
                                        super._inputPtr = inputPtr7 + 1;
                                    }
                                }
                                else {
                                    if (n6 != 32 && n6 != 9) {
                                        this.throwInvalidSpace(n6);
                                        continue Label_0633_Outer;
                                    }
                                    continue Label_0633_Outer;
                                }
                            }
                            this.markLF();
                        }
                        if (n6 != 34 && n6 != 39) {
                            this.throwUnexpectedChar(this.decodeCharForError(b6), " Expected a quote");
                        }
                        final boolean b7;
                        if (b7) {
                            this.handleNsDeclaration(pName2, b6);
                            ++super._currNsCount;
                            bound = bound2;
                            continue Label_0633_Outer;
                        }
                        collectValue = this.collectValue(collectValue, b6, pName2);
                        bound = bound2;
                        continue Label_0633_Outer;
                    }
                    final boolean b7 = true;
                    int bound2 = bound;
                    PName pName2 = pName;
                    continue;
                }
                final boolean b7 = false;
                continue;
            }
        }
        int attrCount;
        if ((attrCount = super._attrCollector.finishLastValue(collectValue)) < 0) {
            attrCount = super._attrCollector.getCount();
            this.reportInputProblem(super._attrCollector.getErrorMsg());
        }
        super._attrCount = attrCount;
        ++super._depth;
        if (bound == 0) {
            if (!tokenName.isBound()) {
                this.reportUnboundPrefix(super._tokenName, false);
            }
            for (int attrCount2 = super._attrCount, i = n; i < attrCount2; ++i) {
                final PName name = super._attrCollector.getName(i);
                if (!name.isBound()) {
                    this.reportUnboundPrefix(name, true);
                }
            }
        }
        return 1;
    }
    
    @Override
    public String parsePublicId(final byte b) {
        char[] array = super._nameBuffer;
        final int[] pubid_CHARS = XmlCharTypes.PUBID_CHARS;
        int count = 0;
        int n = 0;
        while (true) {
            if (super._inputPtr >= super._inputEnd) {
                this.loadMoreGuaranteed();
            }
            final byte b2 = super._inputBuffer[super._inputPtr++];
            if (b2 == b) {
                break;
            }
            final int n2 = b2 & 0xFF;
            if (pubid_CHARS[n2] != 1) {
                this.throwUnexpectedChar(n2, " in public identifier");
            }
            if (n2 <= 32) {
                n = 1;
            }
            else {
                char[] growArrayBy = array;
                int n3 = count;
                int n4;
                if ((n4 = n) != 0) {
                    growArrayBy = array;
                    int n5;
                    if ((n5 = count) >= array.length) {
                        growArrayBy = DataUtil.growArrayBy(array, array.length);
                        super._nameBuffer = growArrayBy;
                        n5 = 0;
                    }
                    growArrayBy[n5] = ' ';
                    n3 = n5 + 1;
                    n4 = 0;
                }
                array = growArrayBy;
                if ((count = n3) >= growArrayBy.length) {
                    array = DataUtil.growArrayBy(growArrayBy, growArrayBy.length);
                    super._nameBuffer = array;
                    count = 0;
                }
                array[count] = (char)n2;
                ++count;
                n = n4;
            }
        }
        return new String(array, 0, count);
    }
    
    @Override
    public String parseSystemId(final byte b) {
        char[] array = super._nameBuffer;
        final int[] attr_CHARS = super._charTypes.ATTR_CHARS;
        int count = 0;
        while (true) {
            if (super._inputPtr >= super._inputEnd) {
                this.loadMoreGuaranteed();
            }
            final int n = super._inputBuffer[super._inputPtr++] & 0xFF;
            final int n2 = attr_CHARS[n];
            char[] finishCurrentSegment = array;
            int n3 = count;
            int n4 = n;
            Label_0364: {
                if (n2 != 0) {
                    if (n2 != 14) {
                        switch (n2) {
                            default: {
                                finishCurrentSegment = array;
                                n3 = count;
                                n4 = n;
                                break Label_0364;
                            }
                            case 7: {
                                final int decodeUtf8_4 = this.decodeUtf8_4(n);
                                finishCurrentSegment = array;
                                int n5 = count;
                                if (count >= array.length) {
                                    finishCurrentSegment = super._textBuilder.finishCurrentSegment();
                                    n5 = 0;
                                }
                                finishCurrentSegment[n5] = (char)(decodeUtf8_4 >> 10 | 0xD800);
                                final int n6 = 0xDC00 | (decodeUtf8_4 & 0x3FF);
                                n3 = n5 + 1;
                                n4 = n6;
                                break Label_0364;
                            }
                            case 6: {
                                n4 = this.decodeUtf8_3(n);
                                finishCurrentSegment = array;
                                n3 = count;
                                break Label_0364;
                            }
                            case 5: {
                                n4 = this.decodeUtf8_2(n);
                                finishCurrentSegment = array;
                                n3 = count;
                                break Label_0364;
                            }
                            case 4: {
                                this.reportInvalidInitial(n);
                                break;
                            }
                            case 3: {
                                this.markLF();
                                finishCurrentSegment = array;
                                n3 = count;
                                n4 = n;
                                break Label_0364;
                            }
                            case 1: {
                                this.handleInvalidXmlChar(n);
                            }
                            case 2: {
                                if (super._inputPtr >= super._inputEnd) {
                                    this.loadMoreGuaranteed();
                                }
                                final byte[] inputBuffer = super._inputBuffer;
                                final int inputPtr = super._inputPtr;
                                if (inputBuffer[inputPtr] == 10) {
                                    super._inputPtr = inputPtr + 1;
                                }
                                this.markLF();
                                n4 = 10;
                                finishCurrentSegment = array;
                                n3 = count;
                                break Label_0364;
                            }
                        }
                    }
                    finishCurrentSegment = array;
                    n3 = count;
                    if ((n4 = n) == b) {
                        break;
                    }
                }
            }
            array = finishCurrentSegment;
            if ((count = n3) >= finishCurrentSegment.length) {
                array = DataUtil.growArrayBy(finishCurrentSegment, finishCurrentSegment.length);
                super._nameBuffer = array;
                count = 0;
            }
            array[count] = (char)n4;
            ++count;
        }
        return new String(array, 0, count);
    }
    
    public void reportInvalidOther(final int n, final int inputPtr) {
        super._inputPtr = inputPtr;
        this.reportInvalidOther(n);
    }
    
    @Override
    public final void skipCData() {
        final int[] other_CHARS = super._charTypes.OTHER_CHARS;
        final byte[] inputBuffer = super._inputBuffer;
    Block_10:
        while (true) {
            int i;
            int n;
            if ((i = super._inputPtr) >= (n = super._inputEnd)) {
                this.loadMoreGuaranteed();
                i = super._inputPtr;
                n = super._inputEnd;
            }
            while (i < n) {
                final int inputPtr = i + 1;
                final int n2 = inputBuffer[i] & 0xFF;
                final int n3 = other_CHARS[n2];
                if (n3 != 0) {
                    super._inputPtr = inputPtr;
                    if (n3 != 11) {
                        switch (n3) {
                            default: {
                                continue Block_10;
                            }
                            case 7: {
                                this.skipUtf8_4(n2);
                                continue Block_10;
                            }
                            case 6: {
                                this.skipUtf8_3(n2);
                                continue Block_10;
                            }
                            case 5: {
                                this.skipUtf8_2(n2);
                                continue Block_10;
                            }
                            case 4: {
                                this.reportInvalidInitial(n2);
                                break;
                            }
                            case 2: {
                                if (super._inputPtr >= super._inputEnd) {
                                    this.loadMoreGuaranteed();
                                }
                                final int inputPtr2 = super._inputPtr;
                                if (inputBuffer[inputPtr2] == 10) {
                                    super._inputPtr = inputPtr2 + 1;
                                }
                            }
                            case 3: {
                                this.markLF();
                                continue Block_10;
                            }
                            case 1: {
                                this.handleInvalidXmlChar(n2);
                            }
                        }
                    }
                    int n4 = 0;
                    byte b;
                    int n5;
                    int inputPtr4;
                    do {
                        if (super._inputPtr >= super._inputEnd) {
                            this.loadMoreGuaranteed();
                        }
                        n5 = n4 + 1;
                        final byte[] inputBuffer2 = super._inputBuffer;
                        final int inputPtr3 = super._inputPtr;
                        inputPtr4 = inputPtr3 + 1;
                        super._inputPtr = inputPtr4;
                        b = inputBuffer2[inputPtr3];
                        n4 = n5;
                    } while (b == 93);
                    if (b != 62) {
                        super._inputPtr = inputPtr4 - 1;
                        continue Block_10;
                    }
                    if (n5 > 1) {
                        break Block_10;
                    }
                    continue Block_10;
                }
                else {
                    i = inputPtr;
                }
            }
            super._inputPtr = i;
        }
    }
    
    @Override
    public final boolean skipCharacters() {
        final int[] text_CHARS = super._charTypes.TEXT_CHARS;
        final byte[] inputBuffer = super._inputBuffer;
    Label_0250:
        while (true) {
            int i;
            int n;
            if ((i = super._inputPtr) >= (n = super._inputEnd)) {
                this.loadMoreGuaranteed();
                i = super._inputPtr;
                n = super._inputEnd;
            }
            while (i < n) {
                final int inputPtr = i + 1;
                final int n2 = inputBuffer[i] & 0xFF;
                final int n3 = text_CHARS[n2];
                if (n3 != 0) {
                    super._inputPtr = inputPtr;
                    switch (n3) {
                        default: {
                            continue Label_0250;
                        }
                        case 11: {
                            int n4 = 1;
                            byte b;
                            while (true) {
                                if (super._inputPtr >= super._inputEnd) {
                                    this.loadMoreGuaranteed();
                                }
                                final int inputPtr2 = super._inputPtr;
                                b = inputBuffer[inputPtr2];
                                if (b != 93) {
                                    break;
                                }
                                super._inputPtr = inputPtr2 + 1;
                                ++n4;
                            }
                            if (b == 62 && n4 > 1) {
                                this.reportIllegalCDataEnd();
                                continue Label_0250;
                            }
                            continue Label_0250;
                        }
                        case 10: {
                            if (this.handleEntityInText(false) == 0) {
                                return true;
                            }
                            continue Label_0250;
                        }
                        case 7: {
                            this.skipUtf8_4(n2);
                            continue Label_0250;
                        }
                        case 6: {
                            this.skipUtf8_3(n2);
                            continue Label_0250;
                        }
                        case 5: {
                            this.skipUtf8_2(n2);
                            continue Label_0250;
                        }
                        case 4: {
                            this.reportInvalidInitial(n2);
                        }
                        case 9: {
                            break Label_0250;
                        }
                        case 2: {
                            if (super._inputPtr >= super._inputEnd) {
                                this.loadMoreGuaranteed();
                            }
                            final int inputPtr3 = super._inputPtr;
                            if (inputBuffer[inputPtr3] == 10) {
                                super._inputPtr = inputPtr3 + 1;
                            }
                        }
                        case 3: {
                            this.markLF();
                            continue Label_0250;
                        }
                        case 1: {
                            this.handleInvalidXmlChar(n2);
                        }
                    }
                }
                else {
                    i = inputPtr;
                }
            }
            super._inputPtr = i;
        }
        --super._inputPtr;
        return false;
    }
    
    @Override
    public final boolean skipCoalescedText() {
        while (true) {
            final int inputPtr = super._inputPtr;
            final int inputEnd = super._inputEnd;
            int i = 0;
            if (inputPtr >= inputEnd && !this.loadMore()) {
                return false;
            }
            final byte[] inputBuffer = super._inputBuffer;
            final int inputPtr2 = super._inputPtr;
            if (inputBuffer[inputPtr2] == 60) {
                if (inputPtr2 + 3 >= super._inputEnd && !this.loadAndRetain(3)) {
                    return false;
                }
                final byte[] inputBuffer2 = super._inputBuffer;
                final int inputPtr3 = super._inputPtr;
                if (inputBuffer2[inputPtr3 + 1] != 33 || inputBuffer2[inputPtr3 + 2] != 91) {
                    return false;
                }
                super._inputPtr = inputPtr3 + 3;
                while (i < 6) {
                    if (super._inputPtr >= super._inputEnd) {
                        this.loadMoreGuaranteed();
                    }
                    final byte b = super._inputBuffer[super._inputPtr++];
                    if (b != (byte)"CDATA[".charAt(i)) {
                        final int decodeCharForError = this.decodeCharForError(b);
                        final StringBuilder sb = new StringBuilder();
                        sb.append(" (expected '");
                        sb.append("CDATA[".charAt(i));
                        sb.append("' for CDATA section)");
                        this.reportTreeUnexpChar(decodeCharForError, sb.toString());
                    }
                    ++i;
                }
                this.skipCData();
            }
            else {
                if (this.skipCharacters()) {
                    return true;
                }
                continue;
            }
        }
    }
    
    @Override
    public final void skipComment() {
        final int[] other_CHARS = super._charTypes.OTHER_CHARS;
        final byte[] inputBuffer = super._inputBuffer;
        int inputPtr3 = 0;
    Block_8:
        while (true) {
            int i;
            int n;
            if ((i = super._inputPtr) >= (n = super._inputEnd)) {
                this.loadMoreGuaranteed();
                i = super._inputPtr;
                n = super._inputEnd;
            }
            while (i < n) {
                final int inputPtr = i + 1;
                final int n2 = inputBuffer[i] & 0xFF;
                final int n3 = other_CHARS[n2];
                if (n3 != 0) {
                    super._inputPtr = inputPtr;
                    if (n3 != 13) {
                        switch (n3) {
                            default: {
                                continue Block_8;
                            }
                            case 7: {
                                this.skipUtf8_4(n2);
                                continue Block_8;
                            }
                            case 6: {
                                this.skipUtf8_3(n2);
                                continue Block_8;
                            }
                            case 5: {
                                this.skipUtf8_2(n2);
                                continue Block_8;
                            }
                            case 4: {
                                this.reportInvalidInitial(n2);
                                break;
                            }
                            case 2: {
                                if (super._inputPtr >= super._inputEnd) {
                                    this.loadMoreGuaranteed();
                                }
                                final int inputPtr2 = super._inputPtr;
                                if (inputBuffer[inputPtr2] == 10) {
                                    super._inputPtr = inputPtr2 + 1;
                                }
                            }
                            case 3: {
                                this.markLF();
                                continue Block_8;
                            }
                            case 1: {
                                this.handleInvalidXmlChar(n2);
                            }
                        }
                    }
                    if (super._inputPtr >= super._inputEnd) {
                        this.loadMoreGuaranteed();
                    }
                    final byte[] inputBuffer2 = super._inputBuffer;
                    inputPtr3 = super._inputPtr;
                    if (inputBuffer2[inputPtr3] == 45) {
                        break Block_8;
                    }
                    continue Block_8;
                }
                else {
                    i = inputPtr;
                }
            }
            super._inputPtr = i;
        }
        ++inputPtr3;
        if ((super._inputPtr = inputPtr3) >= super._inputEnd) {
            this.loadMoreGuaranteed();
        }
        if (super._inputBuffer[super._inputPtr++] != 62) {
            this.reportDoubleHyphenInComments();
        }
    }
    
    @Override
    public final void skipPI() {
        final int[] other_CHARS = super._charTypes.OTHER_CHARS;
        final byte[] inputBuffer = super._inputBuffer;
        int inputPtr3 = 0;
    Block_8:
        while (true) {
            int i;
            int n;
            if ((i = super._inputPtr) >= (n = super._inputEnd)) {
                this.loadMoreGuaranteed();
                i = super._inputPtr;
                n = super._inputEnd;
            }
            while (i < n) {
                final int inputPtr = i + 1;
                final int n2 = inputBuffer[i] & 0xFF;
                final int n3 = other_CHARS[n2];
                if (n3 != 0) {
                    super._inputPtr = inputPtr;
                    if (n3 != 12) {
                        switch (n3) {
                            default: {
                                continue Block_8;
                            }
                            case 7: {
                                this.skipUtf8_4(n2);
                                continue Block_8;
                            }
                            case 6: {
                                this.skipUtf8_3(n2);
                                continue Block_8;
                            }
                            case 5: {
                                this.skipUtf8_2(n2);
                                continue Block_8;
                            }
                            case 4: {
                                this.reportInvalidInitial(n2);
                                break;
                            }
                            case 2: {
                                if (super._inputPtr >= super._inputEnd) {
                                    this.loadMoreGuaranteed();
                                }
                                final int inputPtr2 = super._inputPtr;
                                if (inputBuffer[inputPtr2] == 10) {
                                    super._inputPtr = inputPtr2 + 1;
                                }
                            }
                            case 3: {
                                this.markLF();
                                continue Block_8;
                            }
                            case 1: {
                                this.handleInvalidXmlChar(n2);
                            }
                        }
                    }
                    if (super._inputPtr >= super._inputEnd) {
                        this.loadMoreGuaranteed();
                    }
                    final byte[] inputBuffer2 = super._inputBuffer;
                    inputPtr3 = super._inputPtr;
                    if (inputBuffer2[inputPtr3] == 62) {
                        break Block_8;
                    }
                    continue Block_8;
                }
                else {
                    i = inputPtr;
                }
            }
            super._inputPtr = i;
        }
        super._inputPtr = inputPtr3 + 1;
    }
    
    @Override
    public final void skipSpace() {
        int inputPtr = super._inputPtr;
        while (true) {
            int inputPtr2;
            if ((inputPtr2 = inputPtr) >= super._inputEnd) {
                if (!this.loadMore()) {
                    break;
                }
                inputPtr2 = super._inputPtr;
            }
            final int n = super._inputBuffer[inputPtr2] & 0xFF;
            if (n > 32) {
                inputPtr = inputPtr2;
                break;
            }
            ++inputPtr2;
            if (n == 10) {
                inputPtr = inputPtr2;
            }
            else if (n == 13) {
                int inputPtr3;
                if ((inputPtr3 = inputPtr2) >= super._inputEnd) {
                    if (!this.loadMore()) {
                        inputPtr = inputPtr2;
                        break;
                    }
                    inputPtr3 = super._inputPtr;
                }
                inputPtr = inputPtr3;
                if (super._inputBuffer[inputPtr3] == 10) {
                    inputPtr = inputPtr3 + 1;
                }
            }
            else {
                inputPtr = inputPtr2;
                if (n == 32) {
                    continue;
                }
                inputPtr = inputPtr2;
                if (n != 9) {
                    super._inputPtr = inputPtr2;
                    this.throwInvalidSpace(n);
                    inputPtr = inputPtr2;
                    continue;
                }
                continue;
            }
            this.markLF(inputPtr);
        }
        super._inputPtr = inputPtr;
    }
}

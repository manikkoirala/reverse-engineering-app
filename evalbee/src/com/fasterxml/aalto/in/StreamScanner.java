// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.in;

import java.io.IOException;
import com.fasterxml.aalto.impl.IoStreamException;
import com.fasterxml.aalto.impl.ErrorConsts;
import com.fasterxml.aalto.util.DataUtil;
import java.io.InputStream;
import com.fasterxml.aalto.util.XmlCharTypes;

public abstract class StreamScanner extends ByteBasedScanner
{
    protected final XmlCharTypes _charTypes;
    protected InputStream _in;
    protected byte[] _inputBuffer;
    protected int[] _quadBuffer;
    protected final ByteBasedPNameTable _symbols;
    
    public StreamScanner(final ReaderConfig readerConfig, final InputStream in, final byte[] inputBuffer, final int inputPtr, final int inputEnd) {
        super(readerConfig);
        this._quadBuffer = new int[32];
        this._charTypes = readerConfig.getCharTypes();
        this._symbols = readerConfig.getBBSymbols();
        this._in = in;
        this._inputBuffer = inputBuffer;
        super._inputPtr = inputPtr;
        super._inputEnd = inputEnd;
    }
    
    private final PName findPName(final int n, final int n2) {
        --super._inputPtr;
        final int calcHash = ByteBasedPNameTable.calcHash(n);
        PName pName;
        if ((pName = this._symbols.findSymbol(calcHash, n, 0)) == null) {
            final int[] quadBuffer = this._quadBuffer;
            quadBuffer[0] = n;
            pName = this.addPName(calcHash, quadBuffer, 1, n2);
        }
        return pName;
    }
    
    private final PName findPName(final int n, final int n2, final int n3) {
        --super._inputPtr;
        final int calcHash = ByteBasedPNameTable.calcHash(n, n2);
        PName pName;
        if ((pName = this._symbols.findSymbol(calcHash, n, n2)) == null) {
            final int[] quadBuffer = this._quadBuffer;
            quadBuffer[0] = n;
            quadBuffer[1] = n2;
            pName = this.addPName(calcHash, quadBuffer, 2, n3);
        }
        return pName;
    }
    
    private final PName findPName(final int n, final int n2, final int n3, final int n4, final int[] array) {
        if (n4 > 1) {
            return this.findPName(n, array, n4, n2);
        }
        if (n4 == 0) {
            return this.findPName(n, n2);
        }
        return this.findPName(n3, n, n2);
    }
    
    private final PName findPName(int calcHash, final int[] array, final int n, final int n2) {
        --super._inputPtr;
        int[] growArrayBy = array;
        if (n >= array.length) {
            growArrayBy = DataUtil.growArrayBy(array, array.length);
            this._quadBuffer = growArrayBy;
        }
        final int n3 = n + 1;
        growArrayBy[n] = calcHash;
        calcHash = ByteBasedPNameTable.calcHash(growArrayBy, n3);
        PName pName;
        if ((pName = this._symbols.findSymbol(calcHash, growArrayBy, n3)) == null) {
            pName = this.addPName(calcHash, growArrayBy, n3, n2);
        }
        return pName;
    }
    
    private final int handleCommentOrCdataStart() {
        if (super._inputPtr >= super._inputEnd) {
            this.loadMoreGuaranteed();
        }
        final byte[] inputBuffer = this._inputBuffer;
        final int inputPtr = super._inputPtr;
        final int inputPtr2 = inputPtr + 1;
        super._inputPtr = inputPtr2;
        final byte b = inputBuffer[inputPtr];
        if (b == 45) {
            if (inputPtr2 >= super._inputEnd) {
                this.loadMoreGuaranteed();
            }
            final byte b2 = this._inputBuffer[super._inputPtr++];
            if (b2 != 45) {
                this.reportTreeUnexpChar(this.decodeCharForError(b2), " (expected '-' for COMMENT)");
            }
            if (super._cfgLazyParsing) {
                super._tokenIncomplete = true;
            }
            else {
                this.finishComment();
            }
            return super._currToken = 5;
        }
        if (b == 91) {
            super._currToken = 12;
            for (int i = 0; i < 6; ++i) {
                if (super._inputPtr >= super._inputEnd) {
                    this.loadMoreGuaranteed();
                }
                final byte b3 = this._inputBuffer[super._inputPtr++];
                if (b3 != (byte)"CDATA[".charAt(i)) {
                    final int decodeCharForError = this.decodeCharForError(b3);
                    final StringBuilder sb = new StringBuilder();
                    sb.append(" (expected '");
                    sb.append("CDATA[".charAt(i));
                    sb.append("' for CDATA section)");
                    this.reportTreeUnexpChar(decodeCharForError, sb.toString());
                }
            }
            if (super._cfgLazyParsing) {
                super._tokenIncomplete = true;
            }
            else {
                this.finishCData();
            }
            return 12;
        }
        this.reportTreeUnexpChar(this.decodeCharForError(b), " (expected either '-' for COMMENT or '[CDATA[' for CDATA section)");
        return -1;
    }
    
    private final int handleDtdStart() {
        this.matchAsciiKeyword("DOCTYPE");
        super._tokenName = this.parsePName(this.skipInternalWs(true, "after DOCTYPE keyword, before root name"));
        byte b = this.skipInternalWs(false, null);
        Label_0117: {
            byte b2;
            if (b == 80) {
                this.matchAsciiKeyword("PUBLIC");
                super._publicId = this.parsePublicId(this.skipInternalWs(true, null));
                b2 = this.skipInternalWs(true, null);
            }
            else {
                if (b != 83) {
                    super._systemId = null;
                    super._publicId = null;
                    break Label_0117;
                }
                this.matchAsciiKeyword("SYSTEM");
                b2 = this.skipInternalWs(true, null);
                super._publicId = null;
            }
            super._systemId = this.parseSystemId(b2);
            b = this.skipInternalWs(false, null);
        }
        if (b == 62) {
            super._tokenIncomplete = false;
        }
        else {
            if (b != 91) {
                String s;
                if (super._systemId != null) {
                    s = " (expected '[' for the internal subset, or '>' to end DOCTYPE declaration)";
                }
                else {
                    s = " (expected a 'PUBLIC' or 'SYSTEM' keyword, '[' for the internal subset, or '>' to end DOCTYPE declaration)";
                }
                this.reportTreeUnexpChar(this.decodeCharForError(b), s);
            }
            super._tokenIncomplete = true;
        }
        return super._currToken = 11;
    }
    
    private final int handleEndElementSlow(int i) {
        final int n = i - 1;
        final int n2 = 0;
        int j;
        int n3;
        for (i = 0; i < n; ++i) {
            j = 0;
            n3 = 0;
            while (j < 4) {
                if (super._inputPtr >= super._inputEnd) {
                    this.loadMoreGuaranteed();
                }
                n3 = (n3 << 8 | (this._inputBuffer[super._inputPtr++] & 0xFF));
                ++j;
            }
            if (n3 != super._tokenName.getQuad(i)) {
                this.reportUnexpectedEndTag(super._tokenName.getPrefixedName());
            }
        }
        final int quad = super._tokenName.getQuad(n);
        i = 0;
        int n4 = n2;
        while (true) {
            do {
                if (super._inputPtr >= super._inputEnd) {
                    this.loadMoreGuaranteed();
                }
                n4 = (n4 << 8 | (this._inputBuffer[super._inputPtr++] & 0xFF));
                if (n4 == quad) {
                    if (super._inputPtr >= super._inputEnd) {
                        this.loadMoreGuaranteed();
                    }
                    final byte[] inputBuffer = this._inputBuffer;
                    byte[] inputBuffer2;
                    byte[] inputBuffer3;
                    for (i = super._inputPtr++, i = inputBuffer[i]; i <= 32; i &= 0xFF) {
                        Label_0342: {
                            if (i != 10) {
                                if (i == 13) {
                                    i = super._inputPtr;
                                    if (i < super._inputEnd) {
                                        inputBuffer2 = this._inputBuffer;
                                        super._inputPtr = i + 1;
                                        i = inputBuffer2[i];
                                    }
                                    else {
                                        i = this.loadOne();
                                    }
                                    if (i != 10) {
                                        this.markLF(super._inputPtr - 1);
                                        continue;
                                    }
                                }
                                else {
                                    if (i != 32 && i != 9) {
                                        this.throwInvalidSpace(i);
                                    }
                                    break Label_0342;
                                }
                            }
                            this.markLF();
                        }
                        i = super._inputPtr;
                        if (i < super._inputEnd) {
                            inputBuffer3 = this._inputBuffer;
                            super._inputPtr = i + 1;
                            i = inputBuffer3[i];
                        }
                        else {
                            i = this.loadOne();
                        }
                    }
                    if (i != 62) {
                        this.throwUnexpectedChar(this.decodeCharForError((byte)i), " expected space or closing '>'");
                    }
                    return 2;
                }
            } while (++i <= 3);
            this.reportUnexpectedEndTag(super._tokenName.getPrefixedName());
            continue;
        }
    }
    
    private final int handlePIStart() {
        super._currToken = 3;
        if (super._inputPtr >= super._inputEnd) {
            this.loadMoreGuaranteed();
        }
        final PName pName = this.parsePName(this._inputBuffer[super._inputPtr++]);
        super._tokenName = pName;
        final String localName = pName.getLocalName();
        if (localName.length() == 3 && localName.equalsIgnoreCase("xml") && super._tokenName.getPrefix() == null) {
            this.reportInputProblem(ErrorConsts.ERR_WF_PI_XML_TARGET);
        }
        if (super._inputPtr >= super._inputEnd) {
            this.loadMoreGuaranteed();
        }
        int n = this._inputBuffer[super._inputPtr++] & 0xFF;
        boolean tokenIncomplete;
        if (n <= 32) {
            while (true) {
                Label_0226: {
                    if (n != 10) {
                        if (n == 13) {
                            if (super._inputPtr >= super._inputEnd) {
                                this.loadMoreGuaranteed();
                            }
                            final byte[] inputBuffer = this._inputBuffer;
                            final int inputPtr = super._inputPtr;
                            if (inputBuffer[inputPtr] == 10) {
                                super._inputPtr = inputPtr + 1;
                            }
                        }
                        else {
                            if (n != 32 && n != 9) {
                                this.throwInvalidSpace(n);
                            }
                            break Label_0226;
                        }
                    }
                    this.markLF();
                }
                if (super._inputPtr >= super._inputEnd) {
                    this.loadMoreGuaranteed();
                }
                final byte[] inputBuffer2 = this._inputBuffer;
                final int inputPtr2 = super._inputPtr;
                n = (inputBuffer2[inputPtr2] & 0xFF);
                if (n > 32) {
                    break;
                }
                super._inputPtr = inputPtr2 + 1;
            }
            if (!super._cfgLazyParsing) {
                this.finishPI();
                return 3;
            }
            tokenIncomplete = true;
        }
        else {
            if (n != 63) {
                this.reportMissingPISpace(this.decodeCharForError((byte)n));
            }
            if (super._inputPtr >= super._inputEnd) {
                this.loadMoreGuaranteed();
            }
            final byte b = this._inputBuffer[super._inputPtr++];
            if (b != 62) {
                this.reportMissingPISpace(this.decodeCharForError(b));
            }
            super._textBuilder.resetWithEmpty();
            tokenIncomplete = false;
        }
        super._tokenIncomplete = tokenIncomplete;
        return 3;
    }
    
    private final int handlePrologDeclStart(final boolean b) {
        if (super._inputPtr >= super._inputEnd) {
            this.loadMoreGuaranteed();
        }
        final byte[] inputBuffer = this._inputBuffer;
        final int inputPtr = super._inputPtr;
        final int inputPtr2 = inputPtr + 1;
        super._inputPtr = inputPtr2;
        final byte b2 = inputBuffer[inputPtr];
        byte b3;
        if (b2 == 45) {
            if (inputPtr2 >= super._inputEnd) {
                this.loadMoreGuaranteed();
            }
            if ((b3 = this._inputBuffer[super._inputPtr++]) == 45) {
                if (super._cfgLazyParsing) {
                    super._tokenIncomplete = true;
                }
                else {
                    this.finishComment();
                }
                return super._currToken = 5;
            }
        }
        else if ((b3 = b2) == 68) {
            b3 = b2;
            if (b) {
                this.handleDtdStart();
                if (!super._cfgLazyParsing && super._tokenIncomplete) {
                    this.finishDTD(true);
                    super._tokenIncomplete = false;
                }
                return 11;
            }
        }
        super._tokenIncomplete = true;
        super._currToken = 4;
        this.reportPrologUnexpChar(b, this.decodeCharForError(b3), " (expected '-' for COMMENT)");
        return super._currToken;
    }
    
    private final void matchAsciiKeyword(final String str) {
        for (int length = str.length(), i = 1; i < length; ++i) {
            if (super._inputPtr >= super._inputEnd) {
                this.loadMoreGuaranteed();
            }
            final byte b = this._inputBuffer[super._inputPtr++];
            if (b != (byte)str.charAt(i)) {
                final int decodeCharForError = this.decodeCharForError(b);
                final StringBuilder sb = new StringBuilder();
                sb.append(" (expected '");
                sb.append(str.charAt(i));
                sb.append("' for ");
                sb.append(str);
                sb.append(" keyword)");
                this.reportTreeUnexpChar(decodeCharForError, sb.toString());
            }
        }
    }
    
    @Override
    public void _closeSource() {
        final InputStream in = this._in;
        if (in != null) {
            in.close();
            this._in = null;
        }
    }
    
    public int _nextEntity() {
        super._textBuilder.resetWithEmpty();
        return super._currToken = 9;
    }
    
    @Override
    public void _releaseBuffers() {
        super._releaseBuffers();
        if (this._symbols.maybeDirty()) {
            super._config.updateBBSymbols(this._symbols);
        }
        if (this._in != null) {
            final byte[] inputBuffer = this._inputBuffer;
            if (inputBuffer != null) {
                super._config.freeFullBBuffer(inputBuffer);
                this._inputBuffer = null;
            }
        }
    }
    
    public final PName addPName(final int n, final int[] array, final int n2, final int n3) {
        return this.addUTFPName(this._symbols, this._charTypes, n, array, n2, n3);
    }
    
    public final int checkInTreeIndentation(int i) {
        int n = 32;
        if (i == 13) {
            if (super._inputPtr >= super._inputEnd && !this.loadMore()) {
                super._textBuilder.resetWithIndentation(0, ' ');
                return -1;
            }
            final byte[] inputBuffer = this._inputBuffer;
            i = super._inputPtr;
            if (inputBuffer[i] == 10) {
                super._inputPtr = i + 1;
            }
        }
        this.markLF();
        if (super._inputPtr >= super._inputEnd) {
            this.loadMoreGuaranteed();
        }
        final byte[] inputBuffer2 = this._inputBuffer;
        i = super._inputPtr;
        final byte b = inputBuffer2[i];
        if (b == 32 || b == 9) {
            super._inputPtr = i + 1;
            if (b != 32) {
                n = 8;
            }
            i = 1;
            while (i <= n) {
                if (super._inputPtr >= super._inputEnd) {
                    this.loadMoreGuaranteed();
                }
                final byte[] inputBuffer3 = this._inputBuffer;
                final int inputPtr = super._inputPtr;
                final byte b2 = inputBuffer3[inputPtr];
                if (b2 != b) {
                    if (b2 == 60 && inputPtr + 1 < super._inputEnd && inputBuffer3[inputPtr + 1] != 33) {
                        super._textBuilder.resetWithIndentation(i, (char)b);
                        return -1;
                    }
                    break;
                }
                else {
                    super._inputPtr = inputPtr + 1;
                    ++i;
                }
            }
            final char[] resetWithEmpty = super._textBuilder.resetWithEmpty();
            resetWithEmpty[0] = '\n';
            final char c = (char)b;
            for (int j = 1; j <= i; ++j) {
                resetWithEmpty[j] = c;
            }
            ++i;
            super._textBuilder.setCurrentLength(i);
            return i;
        }
        if (b == 60 && i + 1 < super._inputEnd && inputBuffer2[i + 1] != 33) {
            super._textBuilder.resetWithIndentation(0, ' ');
            return -1;
        }
        super._textBuilder.resetWithEmpty()[0] = '\n';
        super._textBuilder.setCurrentLength(1);
        return 1;
    }
    
    public final int checkPrologIndentation(int i) {
        final int n = 32;
        if (i == 13) {
            if (super._inputPtr >= super._inputEnd && !this.loadMore()) {
                super._textBuilder.resetWithIndentation(0, ' ');
                return -1;
            }
            final byte[] inputBuffer = this._inputBuffer;
            i = super._inputPtr;
            if (inputBuffer[i] == 10) {
                super._inputPtr = i + 1;
            }
        }
        this.markLF();
        if (super._inputPtr >= super._inputEnd && !this.loadMore()) {
            super._textBuilder.resetWithIndentation(0, ' ');
            return -1;
        }
        final byte[] inputBuffer2 = this._inputBuffer;
        i = super._inputPtr;
        final byte b = inputBuffer2[i];
        if (b == 32 || b == 9) {
            super._inputPtr = i + 1;
            if (b == 32) {
                i = n;
            }
            else {
                i = 8;
            }
            int n2 = 1;
            while (true) {
                while (super._inputPtr < super._inputEnd || this.loadMore()) {
                    final byte[] inputBuffer3 = this._inputBuffer;
                    final int inputPtr = super._inputPtr;
                    if (inputBuffer3[inputPtr] != b) {
                        super._textBuilder.resetWithIndentation(n2, (char)b);
                        return -1;
                    }
                    super._inputPtr = inputPtr + 1;
                    final int n3 = n2 + 1;
                    if ((n2 = n3) >= i) {
                        final char[] resetWithEmpty = super._textBuilder.resetWithEmpty();
                        resetWithEmpty[0] = '\n';
                        final char c = (char)b;
                        for (i = 1; i <= n3; ++i) {
                            resetWithEmpty[i] = c;
                        }
                        i = n3 + 1;
                        super._textBuilder.setCurrentLength(i);
                        return i;
                    }
                }
                continue;
            }
        }
        if (b == 60) {
            super._textBuilder.resetWithIndentation(0, ' ');
            return -1;
        }
        super._textBuilder.resetWithEmpty()[0] = '\n';
        super._textBuilder.setCurrentLength(1);
        return 1;
    }
    
    public final int handleCharEntity() {
        if (super._inputPtr >= super._inputEnd) {
            this.loadMoreGuaranteed();
        }
        final byte b = this._inputBuffer[super._inputPtr++];
        int n = 0;
        final int n2 = 0;
        byte b2;
        int n8;
        if ((b2 = b) == 120) {
            int n3 = n2;
            while (true) {
                if (super._inputPtr >= super._inputEnd) {
                    this.loadMoreGuaranteed();
                }
                final byte b3 = this._inputBuffer[super._inputPtr++];
                if (b3 == 59) {
                    break;
                }
                final int n4 = n3 << 4;
                int n6 = 0;
                Label_0134: {
                    int n5;
                    if (b3 <= 57 && b3 >= 48) {
                        n5 = b3 - 48;
                    }
                    else {
                        if (b3 >= 97 && b3 <= 102) {
                            n5 = b3 - 97;
                        }
                        else {
                            if (b3 < 65 || b3 > 70) {
                                this.throwUnexpectedChar(this.decodeCharForError(b3), "; expected a hex digit (0-9a-fA-F)");
                                n6 = n4;
                                break Label_0134;
                            }
                            n5 = b3 - 65;
                        }
                        n5 += 10;
                    }
                    n6 = n4 + n5;
                }
                final int n7 = n3 = n6;
                if (n7 <= 1114111) {
                    continue;
                }
                this.reportEntityOverflow();
                n3 = n7;
            }
            n8 = n3;
        }
        else {
            while (true) {
                n8 = n;
                if (b2 == 59) {
                    break;
                }
                if (b2 <= 57 && b2 >= 48) {
                    final int n9 = n * 10 + (b2 - 48);
                    if ((n = n9) > 1114111) {
                        this.reportEntityOverflow();
                        n = n9;
                    }
                }
                else {
                    this.throwUnexpectedChar(this.decodeCharForError(b2), "; expected a decimal number");
                }
                if (super._inputPtr >= super._inputEnd) {
                    this.loadMoreGuaranteed();
                }
                b2 = this._inputBuffer[super._inputPtr++];
            }
        }
        this.verifyXmlChar(n8);
        return n8;
    }
    
    public final int handleEndElement() {
        --super._depth;
        super._currToken = 2;
        final PName name = super._currElem.getName();
        super._tokenName = name;
        int sizeInQuads = name.sizeInQuads();
        final int inputEnd = super._inputEnd;
        int inputPtr = super._inputPtr;
        if (inputEnd - inputPtr < (sizeInQuads << 2) + 1) {
            return this.handleEndElementSlow(sizeInQuads);
        }
        final byte[] inputBuffer = this._inputBuffer;
        --sizeInQuads;
        for (int i = 0; i < sizeInQuads; ++i) {
            final byte b = inputBuffer[inputPtr];
            final byte b2 = inputBuffer[inputPtr + 1];
            final byte b3 = inputBuffer[inputPtr + 2];
            final byte b4 = inputBuffer[inputPtr + 3];
            inputPtr += 4;
            if ((b << 24 | (b2 & 0xFF) << 16 | (b3 & 0xFF) << 8 | (b4 & 0xFF)) != super._tokenName.getQuad(i)) {
                super._inputPtr = inputPtr;
                this.reportUnexpectedEndTag(super._tokenName.getPrefixedName());
            }
        }
        final int quad = super._tokenName.getQuad(sizeInQuads);
        final int n = inputPtr + 1;
        final int n2 = inputBuffer[inputPtr] & 0xFF;
        int n3 = n;
        if (n2 != quad) {
            final int n4 = n + 1;
            final int n5 = n2 << 8 | (inputBuffer[n] & 0xFF);
            n3 = n4;
            if (n5 != quad) {
                final int n6 = n4 + 1;
                final int n7 = n5 << 8 | (inputBuffer[n4] & 0xFF);
                n3 = n6;
                if (n7 != quad) {
                    final int inputPtr2 = n3 = n6 + 1;
                    if (((inputBuffer[n6] & 0xFF) | n7 << 8) != quad) {
                        super._inputPtr = inputPtr2;
                        this.reportUnexpectedEndTag(super._tokenName.getPrefixedName());
                        n3 = inputPtr2;
                    }
                }
            }
        }
        final int n8 = this._inputBuffer[n3] & 0xFF;
        super._inputPtr = n3 + 1;
        int j;
        byte b5;
        for (j = n8; j <= 32; j = (b5 & 0xFF)) {
            Label_0431: {
                if (j != 10) {
                    if (j == 13) {
                        final int inputPtr3 = super._inputPtr;
                        if (inputPtr3 < super._inputEnd) {
                            final byte[] inputBuffer2 = this._inputBuffer;
                            super._inputPtr = inputPtr3 + 1;
                            b5 = inputBuffer2[inputPtr3];
                        }
                        else {
                            b5 = this.loadOne();
                        }
                        if (b5 != 10) {
                            this.markLF(super._inputPtr - 1);
                            continue;
                        }
                    }
                    else {
                        if (j != 32 && j != 9) {
                            this.throwInvalidSpace(j);
                        }
                        break Label_0431;
                    }
                }
                this.markLF();
            }
            final int inputPtr4 = super._inputPtr;
            if (inputPtr4 < super._inputEnd) {
                final byte[] inputBuffer3 = this._inputBuffer;
                super._inputPtr = inputPtr4 + 1;
                b5 = inputBuffer3[inputPtr4];
            }
            else {
                b5 = this.loadOne();
            }
        }
        if (j != 62) {
            this.throwUnexpectedChar(this.decodeCharForError((byte)j), " expected space or closing '>'");
        }
        return 2;
    }
    
    public abstract int handleEntityInText(final boolean p0);
    
    public abstract int handleStartElement(final byte p0);
    
    public final boolean loadAndRetain(final int n) {
        if (this._in == null) {
            return false;
        }
        final long pastBytesOrChars = super._pastBytesOrChars;
        final int inputPtr = super._inputPtr;
        super._pastBytesOrChars = pastBytesOrChars + inputPtr;
        super._rowStartOffset -= inputPtr;
        final int inputEnd = super._inputEnd - inputPtr;
        final byte[] inputBuffer = this._inputBuffer;
        System.arraycopy(inputBuffer, inputPtr, inputBuffer, 0, inputEnd);
        super._inputPtr = 0;
        super._inputEnd = inputEnd;
        try {
            int read;
            do {
                final byte[] inputBuffer2 = this._inputBuffer;
                final int length = inputBuffer2.length;
                final int inputEnd2 = super._inputEnd;
                final int n2 = length - inputEnd2;
                read = this._in.read(inputBuffer2, inputEnd2, n2);
                if (read < 1) {
                    if (read == 0) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("InputStream returned 0 bytes, even when asked to read up to ");
                        sb.append(n2);
                        this.reportInputProblem(sb.toString());
                    }
                    return false;
                }
            } while ((super._inputEnd += read) < n);
            return true;
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    @Override
    public final boolean loadMore() {
        final long pastBytesOrChars = super._pastBytesOrChars;
        final int inputEnd = super._inputEnd;
        super._pastBytesOrChars = pastBytesOrChars + inputEnd;
        super._rowStartOffset -= inputEnd;
        super._inputPtr = 0;
        final InputStream in = this._in;
        if (in == null) {
            super._inputEnd = 0;
            return false;
        }
        try {
            final byte[] inputBuffer = this._inputBuffer;
            final int read = in.read(inputBuffer, 0, inputBuffer.length);
            if (read < 1) {
                super._inputEnd = 0;
                if (read == 0) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("InputStream returned 0 bytes, even when asked to read up to ");
                    sb.append(this._inputBuffer.length);
                    this.reportInputProblem(sb.toString());
                }
                return false;
            }
            super._inputEnd = read;
            return true;
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    public final byte loadOne() {
        if (!this.loadMore()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unexpected end-of-input when trying to parse ");
            sb.append(ErrorConsts.tokenTypeDesc(super._currToken));
            this.reportInputProblem(sb.toString());
        }
        return this._inputBuffer[super._inputPtr++];
    }
    
    public final byte loadOne(int n) {
        if (!this.loadMore()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unexpected end-of-input when trying to parse ");
            sb.append(ErrorConsts.tokenTypeDesc(n));
            this.reportInputProblem(sb.toString());
        }
        final byte[] inputBuffer = this._inputBuffer;
        n = super._inputPtr++;
        return inputBuffer[n];
    }
    
    public final byte nextByte() {
        if (super._inputPtr >= super._inputEnd && !this.loadMore()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unexpected end-of-input when trying to parse ");
            sb.append(ErrorConsts.tokenTypeDesc(super._currToken));
            this.reportInputProblem(sb.toString());
        }
        return this._inputBuffer[super._inputPtr++];
    }
    
    public final byte nextByte(int n) {
        if (super._inputPtr >= super._inputEnd && !this.loadMore()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unexpected end-of-input when trying to parse ");
            sb.append(ErrorConsts.tokenTypeDesc(n));
            this.reportInputProblem(sb.toString());
        }
        final byte[] inputBuffer = this._inputBuffer;
        n = super._inputPtr++;
        return inputBuffer[n];
    }
    
    @Override
    public final int nextFromProlog(final boolean b) {
        if (super._tokenIncomplete) {
            this.skipToken();
        }
        this.setStartLocation();
        while (super._inputPtr < super._inputEnd || this.loadMore()) {
            final byte[] inputBuffer = this._inputBuffer;
            final int inputPtr = super._inputPtr;
            final int inputPtr2 = inputPtr + 1;
            super._inputPtr = inputPtr2;
            final int n = inputBuffer[inputPtr] & 0xFF;
            if (n == 60) {
                if (inputPtr2 >= super._inputEnd) {
                    this.loadMoreGuaranteed(5);
                }
                final byte b2 = this._inputBuffer[super._inputPtr++];
                if (b2 == 33) {
                    return this.handlePrologDeclStart(b);
                }
                if (b2 == 63) {
                    return this.handlePIStart();
                }
                if (b2 == 47 || !b) {
                    this.reportPrologUnexpElement(b, b2);
                }
                return this.handleStartElement(b2);
            }
            else {
                if (n == 32) {
                    continue;
                }
                if (n != 10) {
                    if (n == 13) {
                        if (inputPtr2 >= super._inputEnd && !this.loadMore()) {
                            this.markLF();
                            this.setStartLocation();
                            return -1;
                        }
                        final byte[] inputBuffer2 = this._inputBuffer;
                        final int inputPtr3 = super._inputPtr;
                        if (inputBuffer2[inputPtr3] == 10) {
                            super._inputPtr = inputPtr3 + 1;
                        }
                    }
                    else {
                        if (n != 9) {
                            this.reportPrologUnexpChar(b, this.decodeCharForError((byte)n), null);
                            continue;
                        }
                        continue;
                    }
                }
                this.markLF();
            }
        }
        this.setStartLocation();
        return -1;
    }
    
    @Override
    public final int nextFromTree() {
        if (super._tokenIncomplete) {
            if (this.skipToken()) {
                return this._nextEntity();
            }
        }
        else {
            final int currToken = super._currToken;
            if (currToken == 1) {
                if (super._isEmptyTag) {
                    --super._depth;
                    return super._currToken = 2;
                }
            }
            else if (currToken == 2) {
                super._currElem = super._currElem.getParent();
                while (true) {
                    final NsDeclaration lastNsDecl = super._lastNsDecl;
                    if (lastNsDecl == null || lastNsDecl.getLevel() < super._depth) {
                        break;
                    }
                    super._lastNsDecl = super._lastNsDecl.unbind();
                }
            }
            else if (super._entityPending) {
                super._entityPending = false;
                return this._nextEntity();
            }
        }
        this.setStartLocation();
        if (super._inputPtr >= super._inputEnd && !this.loadMore()) {
            this.setStartLocation();
            return -1;
        }
        final byte[] inputBuffer = this._inputBuffer;
        int inputPtr = super._inputPtr;
        final byte b = inputBuffer[inputPtr];
        if (b != 60) {
            int tmpChar;
            if (b == 38) {
                super._inputPtr = inputPtr + 1;
                final int handleEntityInText = this.handleEntityInText(false);
                if (handleEntityInText == 0) {
                    return super._currToken = 9;
                }
                tmpChar = -handleEntityInText;
            }
            else {
                tmpChar = (b & 0xFF);
            }
            super._tmpChar = tmpChar;
            if (super._cfgLazyParsing) {
                super._tokenIncomplete = true;
            }
            else {
                this.finishCharacters();
            }
            return super._currToken = 4;
        }
        ++inputPtr;
        byte loadOne;
        if ((super._inputPtr = inputPtr) < super._inputEnd) {
            super._inputPtr = inputPtr + 1;
            loadOne = inputBuffer[inputPtr];
        }
        else {
            loadOne = this.loadOne(5);
        }
        if (loadOne == 33) {
            return this.handleCommentOrCdataStart();
        }
        if (loadOne == 63) {
            return this.handlePIStart();
        }
        if (loadOne == 47) {
            return this.handleEndElement();
        }
        return this.handleStartElement(loadOne);
    }
    
    public final PName parsePName(final byte b) {
        if (super._inputEnd - super._inputPtr < 8) {
            return this.parsePNameSlow(b);
        }
        int n = b & 0xFF;
        if (n < 65) {
            this.throwUnexpectedChar(n, "; expected a name start character");
        }
        final byte[] inputBuffer = this._inputBuffer;
        final int inputPtr = super._inputPtr;
        final int inputPtr2 = inputPtr + 1;
        super._inputPtr = inputPtr2;
        final int n2 = inputBuffer[inputPtr] & 0xFF;
        int n3;
        if (n2 < 65 && (n2 < 45 || n2 > 58 || n2 == 47)) {
            n3 = 1;
        }
        else {
            n = (n << 8 | n2);
            final int inputPtr3 = inputPtr2 + 1;
            super._inputPtr = inputPtr3;
            final int n4 = inputBuffer[inputPtr2] & 0xFF;
            if (n4 < 65 && (n4 < 45 || n4 > 58 || n4 == 47)) {
                n3 = 2;
            }
            else {
                n = (n << 8 | n4);
                final int inputPtr4 = inputPtr3 + 1;
                super._inputPtr = inputPtr4;
                final int n5 = inputBuffer[inputPtr3] & 0xFF;
                if (n5 < 65 && (n5 < 45 || n5 > 58 || n5 == 47)) {
                    n3 = 3;
                }
                else {
                    n = (n << 8 | n5);
                    super._inputPtr = inputPtr4 + 1;
                    final int n6 = inputBuffer[inputPtr4] & 0xFF;
                    if (n6 >= 65 || (n6 >= 45 && n6 <= 58 && n6 != 47)) {
                        return this.parsePNameMedium(n6, n);
                    }
                    n3 = 4;
                }
            }
        }
        return this.findPName(n, n3);
    }
    
    public final PName parsePNameLong(int n, int[] array) {
        int n2 = 2;
        int n5;
        while (true) {
            if (super._inputPtr >= super._inputEnd) {
                this.loadMoreGuaranteed();
            }
            final byte[] inputBuffer = this._inputBuffer;
            final int inputPtr = super._inputPtr;
            final int inputPtr2 = inputPtr + 1;
            super._inputPtr = inputPtr2;
            final int n3 = inputBuffer[inputPtr] & 0xFF;
            if (n3 < 65 && (n3 < 45 || n3 > 58 || n3 == 47)) {
                final int n4 = 1;
                n5 = n;
                n = n4;
                break;
            }
            final int n6 = n << 8 | n3;
            if (inputPtr2 < super._inputEnd) {
                super._inputPtr = inputPtr2 + 1;
                n = inputBuffer[inputPtr2];
            }
            else {
                n = this.loadOne();
            }
            n &= 0xFF;
            if (n < 65 && (n < 45 || n > 58 || n == 47)) {
                return this.findPName(n6, array, n2, 2);
            }
            n5 = (n6 << 8 | n);
            n = super._inputPtr;
            if (n < super._inputEnd) {
                final byte[] inputBuffer2 = this._inputBuffer;
                super._inputPtr = n + 1;
                n = inputBuffer2[n];
            }
            else {
                n = this.loadOne();
            }
            n &= 0xFF;
            if (n < 65 && (n < 45 || n > 58 || n == 47)) {
                n = 3;
                break;
            }
            n5 = (n5 << 8 | n);
            n = super._inputPtr;
            if (n < super._inputEnd) {
                final byte[] inputBuffer3 = this._inputBuffer;
                super._inputPtr = n + 1;
                n = inputBuffer3[n];
            }
            else {
                n = this.loadOne();
            }
            n &= 0xFF;
            if (n < 65 && (n < 45 || n > 58 || n == 47)) {
                n = 4;
                break;
            }
            int[] growArrayBy = array;
            if (n2 >= array.length) {
                growArrayBy = DataUtil.growArrayBy(array, array.length);
                this._quadBuffer = growArrayBy;
            }
            growArrayBy[n2] = n5;
            ++n2;
            array = growArrayBy;
        }
        return this.findPName(n5, array, n2, n);
    }
    
    public PName parsePNameMedium(int inputPtr, final int n) {
        final byte[] inputBuffer = this._inputBuffer;
        final int inputPtr2 = super._inputPtr;
        final int inputPtr3 = inputPtr2 + 1;
        super._inputPtr = inputPtr3;
        final int n2 = inputBuffer[inputPtr2] & 0xFF;
        if (n2 < 65 && (n2 < 45 || n2 > 58 || n2 == 47)) {
            return this.findPName(n, inputPtr, 1);
        }
        final int n3 = inputPtr << 8 | n2;
        inputPtr = inputPtr3 + 1;
        super._inputPtr = inputPtr;
        final int n4 = inputBuffer[inputPtr3] & 0xFF;
        if (n4 < 65 && (n4 < 45 || n4 > 58 || n4 == 47)) {
            return this.findPName(n, n3, 2);
        }
        final int n5 = n3 << 8 | n4;
        final int inputPtr4 = inputPtr + 1;
        super._inputPtr = inputPtr4;
        inputPtr = (inputBuffer[inputPtr] & 0xFF);
        if (inputPtr < 65 && (inputPtr < 45 || inputPtr > 58 || inputPtr == 47)) {
            return this.findPName(n, n5, 3);
        }
        inputPtr |= n5 << 8;
        super._inputPtr = inputPtr4 + 1;
        final int n6 = inputBuffer[inputPtr4] & 0xFF;
        if (n6 < 65 && (n6 < 45 || n6 > 58 || n6 == 47)) {
            return this.findPName(n, inputPtr, 4);
        }
        final int[] quadBuffer = this._quadBuffer;
        quadBuffer[0] = n;
        quadBuffer[1] = inputPtr;
        return this.parsePNameLong(n6, quadBuffer);
    }
    
    public final PName parsePNameSlow(final byte b) {
        int n = b & 0xFF;
        if (n < 65) {
            this.throwUnexpectedChar(n, "; expected a name start character");
        }
        int[] quadBuffer = this._quadBuffer;
        int n2 = 0;
        int n3 = 0;
        while (true) {
            if (super._inputPtr >= super._inputEnd) {
                this.loadMoreGuaranteed();
            }
            final byte[] inputBuffer = this._inputBuffer;
            final int inputPtr = super._inputPtr;
            final int inputPtr2 = inputPtr + 1;
            super._inputPtr = inputPtr2;
            final int n4 = inputBuffer[inputPtr] & 0xFF;
            if (n4 < 65 && (n4 < 45 || n4 > 58 || n4 == 47)) {
                return this.findPName(n, 1, n2, n3, quadBuffer);
            }
            final int n5 = n4 | n << 8;
            byte loadOne;
            if (inputPtr2 < super._inputEnd) {
                super._inputPtr = inputPtr2 + 1;
                loadOne = inputBuffer[inputPtr2];
            }
            else {
                loadOne = this.loadOne();
            }
            final int n6 = loadOne & 0xFF;
            if (n6 < 65 && (n6 < 45 || n6 > 58 || n6 == 47)) {
                return this.findPName(n5, 2, n2, n3, quadBuffer);
            }
            final int n7 = n6 | n5 << 8;
            final int inputPtr3 = super._inputPtr;
            byte loadOne2;
            if (inputPtr3 < super._inputEnd) {
                final byte[] inputBuffer2 = this._inputBuffer;
                super._inputPtr = inputPtr3 + 1;
                loadOne2 = inputBuffer2[inputPtr3];
            }
            else {
                loadOne2 = this.loadOne();
            }
            final int n8 = loadOne2 & 0xFF;
            if (n8 < 65 && (n8 < 45 || n8 > 58 || n8 == 47)) {
                return this.findPName(n7, 3, n2, n3, quadBuffer);
            }
            final int n9 = n7 << 8 | n8;
            final int inputPtr4 = super._inputPtr;
            byte loadOne3;
            if (inputPtr4 < super._inputEnd) {
                final byte[] inputBuffer3 = this._inputBuffer;
                super._inputPtr = inputPtr4 + 1;
                loadOne3 = inputBuffer3[inputPtr4];
            }
            else {
                loadOne3 = this.loadOne();
            }
            n = (loadOne3 & 0xFF);
            if (n < 65 && (n < 45 || n > 58 || n == 47)) {
                return this.findPName(n9, 4, n2, n3, quadBuffer);
            }
            if (n3 == 0) {
                n2 = n9;
            }
            else if (n3 == 1) {
                quadBuffer[0] = n2;
                quadBuffer[1] = n9;
            }
            else {
                int[] growArrayBy = quadBuffer;
                if (n3 >= quadBuffer.length) {
                    growArrayBy = DataUtil.growArrayBy(quadBuffer, quadBuffer.length);
                    this._quadBuffer = growArrayBy;
                }
                growArrayBy[n3] = n9;
                quadBuffer = growArrayBy;
            }
            ++n3;
        }
    }
    
    public abstract String parsePublicId(final byte p0);
    
    public abstract String parseSystemId(final byte p0);
    
    public byte skipInternalWs(final boolean b, final String str) {
        if (super._inputPtr >= super._inputEnd) {
            this.loadMoreGuaranteed();
        }
        byte b3;
        final byte b2 = b3 = this._inputBuffer[super._inputPtr++];
        if ((b2 & 0xFF) > 32) {
            if (!b) {
                return b2;
            }
            final int decodeCharForError = this.decodeCharForError(b2);
            final StringBuilder sb = new StringBuilder();
            sb.append(" (expected white space ");
            sb.append(str);
            sb.append(")");
            this.reportTreeUnexpChar(decodeCharForError, sb.toString());
            b3 = b2;
        }
        byte b4;
        do {
            Label_0202: {
                if (b3 != 10) {
                    if (b3 == 13) {
                        if (super._inputPtr >= super._inputEnd) {
                            this.loadMoreGuaranteed();
                        }
                        final byte[] inputBuffer = this._inputBuffer;
                        final int inputPtr = super._inputPtr;
                        if (inputBuffer[inputPtr] == 10) {
                            super._inputPtr = inputPtr + 1;
                        }
                    }
                    else {
                        if (b3 != 32 && b3 != 9) {
                            this.throwInvalidSpace(b3);
                        }
                        break Label_0202;
                    }
                }
                this.markLF();
            }
            if (super._inputPtr >= super._inputEnd) {
                this.loadMoreGuaranteed();
            }
            b4 = (b3 = this._inputBuffer[super._inputPtr++]);
        } while ((b4 & 0xFF) <= 32);
        return b4;
    }
}

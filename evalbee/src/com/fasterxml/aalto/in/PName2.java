// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.in;

public final class PName2 extends ByteBasedPName
{
    final int mQuad1;
    final int mQuad2;
    
    public PName2(final String s, final String s2, final String s3, final int n, final int mQuad1, final int mQuad2) {
        super(s, s2, s3, n);
        this.mQuad1 = mQuad1;
        this.mQuad2 = mQuad2;
    }
    
    @Override
    public PName createBoundName(final NsBinding namespaceBinding) {
        final PName2 pName2 = new PName2(super._prefixedName, super._prefix, super._localName, super.mHash, this.mQuad1, this.mQuad2);
        pName2._namespaceBinding = namespaceBinding;
        return pName2;
    }
    
    @Override
    public boolean equals(final int n, final int n2) {
        return n == this.mQuad1 && n2 == this.mQuad2;
    }
    
    @Override
    public boolean equals(final int[] array, final int n) {
        boolean b2;
        final boolean b = b2 = false;
        if (n == 2) {
            b2 = b;
            if (array[0] == this.mQuad1) {
                b2 = b;
                if (array[1] == this.mQuad2) {
                    b2 = true;
                }
            }
        }
        return b2;
    }
    
    @Override
    public int getFirstQuad() {
        return this.mQuad1;
    }
    
    @Override
    public int getLastQuad() {
        return this.mQuad2;
    }
    
    @Override
    public int getQuad(int n) {
        if (n == 0) {
            n = this.mQuad1;
        }
        else {
            n = this.mQuad2;
        }
        return n;
    }
    
    @Override
    public boolean hashEquals(final int n, final int n2, final int n3) {
        return n == super.mHash && n2 == this.mQuad1 && n3 == this.mQuad2;
    }
    
    @Override
    public boolean hashEquals(final int n, final int[] array, final int n2) {
        final int mHash = super.mHash;
        boolean b2;
        final boolean b = b2 = false;
        if (n == mHash) {
            b2 = b;
            if (n2 == 2) {
                b2 = b;
                if (array[0] == this.mQuad1) {
                    b2 = b;
                    if (array[1] == this.mQuad2) {
                        b2 = true;
                    }
                }
            }
        }
        return b2;
    }
    
    @Override
    public int sizeInQuads() {
        return 2;
    }
}

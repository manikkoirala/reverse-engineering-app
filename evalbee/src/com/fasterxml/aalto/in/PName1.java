// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.in;

public final class PName1 extends ByteBasedPName
{
    final int mQuad;
    
    public PName1(final String s, final String s2, final String s3, final int n, final int mQuad) {
        super(s, s2, s3, n);
        this.mQuad = mQuad;
    }
    
    @Override
    public PName createBoundName(final NsBinding namespaceBinding) {
        final PName1 pName1 = new PName1(super._prefixedName, super._prefix, super._localName, super.mHash, this.mQuad);
        pName1._namespaceBinding = namespaceBinding;
        return pName1;
    }
    
    @Override
    public boolean equals(final int n, final int n2) {
        return n == this.mQuad && n2 == 0;
    }
    
    @Override
    public boolean equals(final int[] array, final int n) {
        boolean b = false;
        if (n == 1) {
            b = b;
            if (array[0] == this.mQuad) {
                b = true;
            }
        }
        return b;
    }
    
    @Override
    public int getFirstQuad() {
        return this.mQuad;
    }
    
    @Override
    public final int getLastQuad() {
        return this.mQuad;
    }
    
    @Override
    public int getQuad(int mQuad) {
        if (mQuad == 0) {
            mQuad = this.mQuad;
        }
        else {
            mQuad = 0;
        }
        return mQuad;
    }
    
    @Override
    public boolean hashEquals(final int n, final int n2, final int n3) {
        return n == super.mHash && n2 == this.mQuad && n3 == 0;
    }
    
    @Override
    public boolean hashEquals(final int n, final int[] array, final int n2) {
        final int mHash = super.mHash;
        boolean b2;
        final boolean b = b2 = false;
        if (n == mHash) {
            b2 = b;
            if (n2 == 1) {
                b2 = b;
                if (array[0] == this.mQuad) {
                    b2 = true;
                }
            }
        }
        return b2;
    }
    
    @Override
    public int sizeInQuads() {
        return 1;
    }
}

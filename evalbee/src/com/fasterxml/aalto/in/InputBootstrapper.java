// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.in;

import com.fasterxml.aalto.WFCException;
import javax.xml.stream.Location;
import com.fasterxml.aalto.util.XmlConsts;

public abstract class InputBootstrapper implements XmlConsts
{
    public static final String ERR_XMLDECL_END_MARKER = "; expected \"?>\" end marker";
    public static final String ERR_XMLDECL_EXP_ATTRVAL = "; expected a quote character enclosing value for ";
    public static final String ERR_XMLDECL_EXP_EQ = "; expected '=' after ";
    public static final String ERR_XMLDECL_EXP_SPACE = "; expected a white space";
    public static final String ERR_XMLDECL_KW_ENCODING = "; expected keyword 'encoding'";
    public static final String ERR_XMLDECL_KW_STANDALONE = "; expected keyword 'standalone'";
    public static final String ERR_XMLDECL_KW_VERSION = "; expected keyword 'version'";
    final ReaderConfig _config;
    protected int _inputProcessed;
    protected int _inputRow;
    protected int _inputRowStart;
    int mDeclaredXmlVersion;
    String mFoundEncoding;
    final char[] mKeyword;
    String mStandalone;
    
    public InputBootstrapper(final ReaderConfig config) {
        this._inputProcessed = 0;
        this._inputRow = 0;
        this._inputRowStart = 0;
        this.mDeclaredXmlVersion = 0;
        this._config = config;
        this.mKeyword = config.allocSmallCBuffer(60);
    }
    
    private final int getWsOrChar(final int n) {
        final int next = this.getNext();
        if (next == n) {
            return next;
        }
        if (next > 32) {
            final StringBuilder sb = new StringBuilder();
            sb.append("; expected either '");
            sb.append((char)n);
            sb.append("' or white space");
            this.reportUnexpectedChar(next, sb.toString());
        }
        if (next == 10 || next == 13) {
            this.pushback();
        }
        return this.getNextAfterWs(false);
    }
    
    private final int handleEq(final String s) {
        final int nextAfterWs = this.getNextAfterWs(false);
        if (nextAfterWs != 61) {
            final StringBuilder sb = new StringBuilder();
            sb.append("; expected '=' after '");
            sb.append(s);
            sb.append("'");
            this.reportUnexpectedChar(nextAfterWs, sb.toString());
        }
        final int nextAfterWs2 = this.getNextAfterWs(false);
        if (nextAfterWs2 != 34 && nextAfterWs2 != 39) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("; expected a quote character enclosing value for '");
            sb2.append(s);
            sb2.append("'");
            this.reportUnexpectedChar(nextAfterWs2, sb2.toString());
        }
        return nextAfterWs2;
    }
    
    private final String readXmlEncoding() {
        final int checkKeyword = this.checkKeyword("encoding");
        if (checkKeyword != 0) {
            this.reportUnexpectedChar(checkKeyword, "encoding");
        }
        final int quotedValue = this.readQuotedValue(this.mKeyword, this.handleEq("encoding"));
        if (quotedValue == 0) {
            this.reportPseudoAttrProblem("encoding", null, null, null);
        }
        if (quotedValue < 0) {
            return new String(this.mKeyword);
        }
        return new String(this.mKeyword, 0, quotedValue);
    }
    
    private final String readXmlStandalone() {
        final int checkKeyword = this.checkKeyword("standalone");
        if (checkKeyword != 0) {
            this.reportUnexpectedChar(checkKeyword, "standalone");
        }
        final int quotedValue = this.readQuotedValue(this.mKeyword, this.handleEq("standalone"));
        if (quotedValue == 2) {
            final char[] mKeyword = this.mKeyword;
            if (mKeyword[0] == 'n' && mKeyword[1] == 'o') {
                return "no";
            }
        }
        else if (quotedValue == 3) {
            final char[] mKeyword2 = this.mKeyword;
            if (mKeyword2[0] == 'y' && mKeyword2[1] == 'e' && mKeyword2[2] == 's') {
                return "yes";
            }
        }
        String s;
        if (quotedValue < 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("'");
            sb.append(new String(this.mKeyword));
            sb.append("[..]'");
            s = sb.toString();
        }
        else if (quotedValue == 0) {
            s = "<empty>";
        }
        else {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("'");
            sb2.append(new String(this.mKeyword, 0, quotedValue));
            sb2.append("'");
            s = sb2.toString();
        }
        this.reportPseudoAttrProblem("standalone", s, "yes", "no");
        return s;
    }
    
    private final int readXmlVersion() {
        final int checkKeyword = this.checkKeyword("version");
        if (checkKeyword != 0) {
            this.reportUnexpectedChar(checkKeyword, "version");
        }
        final int quotedValue = this.readQuotedValue(this.mKeyword, this.handleEq("version"));
        if (quotedValue == 3) {
            final char[] mKeyword = this.mKeyword;
            if (mKeyword[0] == '1' && mKeyword[1] == '.') {
                final char c = mKeyword[2];
                if (c == '0') {
                    return 256;
                }
                if (c == '1') {
                    return 272;
                }
            }
        }
        String s;
        if (quotedValue < 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("'");
            sb.append(new String(this.mKeyword));
            sb.append("[..]'");
            s = sb.toString();
        }
        else if (quotedValue == 0) {
            s = "<empty>";
        }
        else {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("'");
            sb2.append(new String(this.mKeyword, 0, quotedValue));
            sb2.append("'");
            s = sb2.toString();
        }
        this.reportPseudoAttrProblem("version", s, "1.0", "1.1");
        return 0;
    }
    
    public abstract XmlScanner bootstrap();
    
    public abstract int checkKeyword(final String p0);
    
    public abstract Location getLocation();
    
    public abstract int getNext();
    
    public abstract int getNextAfterWs(final boolean p0);
    
    public abstract void pushback();
    
    public abstract int readQuotedValue(final char[] p0, final int p1);
    
    public void readXmlDeclaration() {
        int n = this.getNextAfterWs(false);
        if (n != 118) {
            this.reportUnexpectedChar(n, "; expected keyword 'version'");
        }
        else {
            this.mDeclaredXmlVersion = this.readXmlVersion();
            n = this.getWsOrChar(63);
        }
        int wsOrChar = n;
        if (n == 101) {
            this.mFoundEncoding = this.readXmlEncoding();
            wsOrChar = this.getWsOrChar(63);
        }
        int wsOrChar2;
        if ((wsOrChar2 = wsOrChar) == 115) {
            this.mStandalone = this.readXmlStandalone();
            wsOrChar2 = this.getWsOrChar(63);
        }
        if (wsOrChar2 != 63) {
            this.reportUnexpectedChar(wsOrChar2, "; expected \"?>\" end marker");
        }
        final int next = this.getNext();
        if (next != 62) {
            this.reportUnexpectedChar(next, "; expected \"?>\" end marker");
        }
    }
    
    public void reportEof() {
        this.reportXmlProblem("Unexpected end-of-input in xml declaration");
    }
    
    public void reportNull() {
        this.reportXmlProblem("Illegal null byte/char in input stream");
    }
    
    public final void reportPseudoAttrProblem(final String s, final String str, String string, final String str2) {
        if (string == null) {
            string = "";
        }
        else {
            final StringBuilder sb = new StringBuilder();
            sb.append("; expected \"");
            sb.append(string);
            sb.append("\" or \"");
            sb.append(str2);
            sb.append("\"");
            string = sb.toString();
        }
        if (str == null || str.length() == 0) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Missing XML pseudo-attribute '");
            sb2.append(s);
            sb2.append("' value");
            sb2.append(string);
            this.reportXmlProblem(sb2.toString());
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("Invalid XML pseudo-attribute '");
        sb3.append(s);
        sb3.append("' value ");
        sb3.append(str);
        sb3.append(string);
        this.reportXmlProblem(sb3.toString());
    }
    
    public void reportUnexpectedChar(final int n, String s) {
        final char c = (char)n;
        if (Character.isISOControl(c)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unexpected character (CTRL-CHAR, code ");
            sb.append(n);
            sb.append(")");
            sb.append(s);
            s = sb.toString();
        }
        else {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Unexpected character '");
            sb2.append(c);
            sb2.append("' (code ");
            sb2.append(n);
            sb2.append(")");
            sb2.append(s);
            s = sb2.toString();
        }
        this.reportXmlProblem(s);
    }
    
    public void reportXmlProblem(final String s) {
        throw new WFCException(s, this.getLocation());
    }
}

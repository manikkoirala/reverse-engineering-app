// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.in;

import java.io.IOException;
import com.fasterxml.aalto.impl.IoStreamException;
import com.fasterxml.aalto.impl.LocationImpl;
import org.codehaus.stax2.XMLStreamLocation2;
import com.fasterxml.aalto.impl.ErrorConsts;
import com.fasterxml.aalto.util.XmlChars;
import com.fasterxml.aalto.util.DataUtil;
import java.io.Reader;
import com.fasterxml.aalto.util.XmlCharTypes;

public final class ReaderScanner extends XmlScanner
{
    private static final XmlCharTypes sCharTypes;
    protected Reader _in;
    protected char[] _inputBuffer;
    protected int _inputEnd;
    protected int _inputPtr;
    protected final CharBasedPNameTable _symbols;
    protected int mTmpChar;
    
    static {
        sCharTypes = InputCharTypes.getLatin1CharTypes();
    }
    
    public ReaderScanner(final ReaderConfig readerConfig, final Reader in) {
        super(readerConfig);
        this.mTmpChar = 0;
        this._in = in;
        this._inputBuffer = readerConfig.allocFullCBuffer(4000);
        this._inputEnd = 0;
        this._inputPtr = 0;
        super._pastBytesOrChars = 0L;
        super._rowStartOffset = 0;
        this._symbols = readerConfig.getCBSymbols();
    }
    
    public ReaderScanner(final ReaderConfig readerConfig, final Reader in, final char[] inputBuffer, final int inputPtr, final int inputEnd) {
        super(readerConfig);
        this.mTmpChar = 0;
        this._in = in;
        this._inputBuffer = inputBuffer;
        this._inputPtr = inputPtr;
        this._inputEnd = inputEnd;
        super._pastBytesOrChars = 0L;
        super._rowStartOffset = 0;
        this._symbols = readerConfig.getCBSymbols();
    }
    
    private char checkSurrogate(final char c) {
        if (c >= '\udc00') {
            this.reportInvalidFirstSurrogate(c);
        }
        if (this._inputPtr >= this._inputEnd) {
            this.loadMoreGuaranteed();
        }
        final char c2 = this._inputBuffer[this._inputPtr++];
        if (c2 < '\udc00' || c2 >= '\ue000') {
            this.reportInvalidSecondSurrogate(c2);
        }
        final int n = (c - '\ud800' << 10) + 65536;
        if (n > 1114111) {
            this.reportInvalidXmlChar(n);
        }
        return c2;
    }
    
    private int checkSurrogateNameChar(final char c, final char c2, final int n) {
        if (c >= '\udc00') {
            this.reportInvalidFirstSurrogate(c);
        }
        if (c2 < '\udc00' || c2 >= '\ue000') {
            this.reportInvalidSecondSurrogate(c2);
        }
        final int n2 = (c - '\ud800' << 10) + 65536;
        if (n2 > 1114111) {
            this.reportInvalidXmlChar(n2);
        }
        this.reportInvalidNameChar(n2, n);
        return n2;
    }
    
    private final int collectValue(int n, final char c, final PName pName) {
        char[] array = super._attrCollector.startNewValue(pName, n);
        final int[] attr_CHARS = ReaderScanner.sCharTypes.ATTR_CHARS;
        int n2 = n;
    Block_15:
        while (true) {
            int n3;
            n = (n3 = this._inputPtr);
            if (n >= this._inputEnd) {
                this.loadMoreGuaranteed();
                n3 = this._inputPtr;
            }
            char[] valueBufferFull = array;
            if (n2 >= array.length) {
                valueBufferFull = super._attrCollector.valueBufferFull();
            }
            final int inputEnd = this._inputEnd;
            final int n4 = valueBufferFull.length - n2 + n3;
            int i = n3;
            int n5 = inputEnd;
            n = n2;
            if (n4 < inputEnd) {
                n5 = n4;
                n = n2;
                i = n3;
            }
            while (i < n5) {
                final char[] inputBuffer = this._inputBuffer;
                final int inputPtr = i + 1;
                final char c2 = inputBuffer[i];
                Label_0167: {
                    if (c2 <= '\u00ff') {
                        if (attr_CHARS[c2] != 0) {
                            break Label_0167;
                        }
                    }
                    else if (c2 >= '\ud800') {
                        break Label_0167;
                    }
                    valueBufferFull[n] = c2;
                    ++n;
                    i = inputPtr;
                    continue;
                }
                this._inputPtr = inputPtr;
                char c3 = '\0';
                Label_0554: {
                    if (c2 <= '\u00ff') {
                        final int n6 = attr_CHARS[c2];
                        Label_0439: {
                            Label_0435: {
                                if (n6 != 1) {
                                    if (n6 != 2) {
                                        if (n6 == 3) {
                                            break Label_0435;
                                        }
                                        if (n6 != 14) {
                                            switch (n6) {
                                                default: {
                                                    array = valueBufferFull;
                                                    c3 = c2;
                                                    n2 = n;
                                                    break Label_0554;
                                                }
                                                case 9: {
                                                    this.throwUnexpectedChar(c2, "'<' not allowed in attribute value");
                                                }
                                                case 10: {
                                                    final int handleEntityInText = this.handleEntityInText(false);
                                                    if (handleEntityInText == 0) {
                                                        this.reportUnexpandedEntityInAttr(pName, false);
                                                    }
                                                    array = valueBufferFull;
                                                    int n7 = handleEntityInText;
                                                    n2 = n;
                                                    if (handleEntityInText >> 16 != 0) {
                                                        final int n8 = handleEntityInText - 65536;
                                                        n2 = n + 1;
                                                        valueBufferFull[n] = (char)(n8 >> 10 | 0xD800);
                                                        n7 = (0xDC00 | (n8 & 0x3FF));
                                                        array = valueBufferFull;
                                                        if (n2 >= valueBufferFull.length) {
                                                            array = super._attrCollector.valueBufferFull();
                                                        }
                                                    }
                                                    c3 = (char)n7;
                                                    break Label_0554;
                                                }
                                                case 8: {
                                                    break Label_0439;
                                                }
                                            }
                                        }
                                        else {
                                            array = valueBufferFull;
                                            c3 = c2;
                                            n2 = n;
                                            if (c2 == c) {
                                                break Block_15;
                                            }
                                            break Label_0554;
                                        }
                                    }
                                }
                                else {
                                    this.handleInvalidXmlChar(c2);
                                }
                                if (this._inputPtr >= this._inputEnd) {
                                    this.loadMoreGuaranteed();
                                }
                                final char[] inputBuffer2 = this._inputBuffer;
                                final int inputPtr2 = this._inputPtr;
                                if (inputBuffer2[inputPtr2] == '\n') {
                                    this._inputPtr = inputPtr2 + 1;
                                }
                            }
                            this.markLF();
                        }
                        c3 = ' ';
                        array = valueBufferFull;
                        n2 = n;
                    }
                    else {
                        array = valueBufferFull;
                        c3 = c2;
                        n2 = n;
                        if (c2 >= '\ud800') {
                            if (c2 < '\ue000') {
                                c3 = this.checkSurrogate(c2);
                                n2 = n + 1;
                                valueBufferFull[n] = c2;
                                array = valueBufferFull;
                                if (n2 >= valueBufferFull.length) {
                                    array = super._attrCollector.valueBufferFull();
                                }
                            }
                            else {
                                array = valueBufferFull;
                                c3 = c2;
                                n2 = n;
                                if (c2 >= '\ufffe') {
                                    c3 = this.handleInvalidXmlChar(c2);
                                    n2 = n;
                                    array = valueBufferFull;
                                }
                            }
                        }
                    }
                }
                array[n2] = c3;
                ++n2;
                continue Block_15;
            }
            this._inputPtr = i;
            array = valueBufferFull;
            n2 = n;
        }
        return n;
    }
    
    private int decodeSurrogate(final char c) {
        if (c >= '\udc00') {
            this.reportInvalidFirstSurrogate(c);
        }
        if (this._inputPtr >= this._inputEnd) {
            this.loadMoreGuaranteed();
        }
        final char c2 = this._inputBuffer[this._inputPtr++];
        if (c2 < '\udc00' || c2 >= '\ue000') {
            this.reportInvalidSecondSurrogate(c2);
        }
        final int n = (c - '\ud800' << 10) + 65536;
        if (n > 1114111) {
            this.reportInvalidXmlChar(n);
        }
        return n;
    }
    
    private final int handleDtdStart() {
        this.matchAsciiKeyword("DOCTYPE");
        super._tokenName = this.parsePName(this.skipInternalWs(true, "after DOCTYPE keyword, before root name"));
        char c = this.skipInternalWs(false, null);
        Label_0117: {
            char c2;
            if (c == 'P') {
                this.matchAsciiKeyword("PUBLIC");
                super._publicId = this.parsePublicId(this.skipInternalWs(true, null));
                c2 = this.skipInternalWs(true, null);
            }
            else {
                if (c != 'S') {
                    super._systemId = null;
                    super._publicId = null;
                    break Label_0117;
                }
                this.matchAsciiKeyword("SYSTEM");
                c2 = this.skipInternalWs(true, null);
                super._publicId = null;
            }
            super._systemId = this.parseSystemId(c2);
            c = this.skipInternalWs(false, null);
        }
        if (c == '>') {
            super._tokenIncomplete = false;
        }
        else {
            if (c != '[') {
                String s;
                if (super._systemId != null) {
                    s = " (expected '[' for the internal subset, or '>' to end DOCTYPE declaration)";
                }
                else {
                    s = " (expected a 'PUBLIC' or 'SYSTEM' keyword, '[' for the internal subset, or '>' to end DOCTYPE declaration)";
                }
                this.reportTreeUnexpChar(c, s);
            }
            super._tokenIncomplete = true;
        }
        return super._currToken = 11;
    }
    
    private void handleNsDeclaration(final PName pName, final char c) {
        char[] nameBuffer = super._nameBuffer;
        int n = 0;
        while (true) {
            if (this._inputPtr >= this._inputEnd) {
                this.loadMoreGuaranteed();
            }
            final char[] inputBuffer = this._inputBuffer;
            final int inputPtr = this._inputPtr;
            final int inputPtr2 = inputPtr + 1;
            this._inputPtr = inputPtr2;
            final char c2 = inputBuffer[inputPtr];
            if (c2 == c) {
                break;
            }
            char[] growArrayBy;
            int n2;
            char c3;
            if (c2 == '&') {
                final int handleEntityInText = this.handleEntityInText(false);
                if (handleEntityInText == 0) {
                    this.reportUnexpandedEntityInAttr(pName, true);
                }
                growArrayBy = nameBuffer;
                n2 = n;
                int n3 = handleEntityInText;
                if (handleEntityInText >> 16 != 0) {
                    growArrayBy = nameBuffer;
                    if (n >= nameBuffer.length) {
                        growArrayBy = DataUtil.growArrayBy(nameBuffer, nameBuffer.length);
                        super._nameBuffer = growArrayBy;
                    }
                    final int n4 = handleEntityInText - 65536;
                    growArrayBy[n] = (char)(n4 >> 10 | 0xD800);
                    n3 = (0xDC00 | (n4 & 0x3FF));
                    n2 = n + 1;
                }
                c3 = (char)n3;
            }
            else if (c2 == '<') {
                this.throwUnexpectedChar(c2, "'<' not allowed in attribute value");
                growArrayBy = nameBuffer;
                n2 = n;
                c3 = c2;
            }
            else {
                growArrayBy = nameBuffer;
                n2 = n;
                if ((c3 = c2) < ' ') {
                    if (c2 == '\n') {
                        this.markLF();
                        growArrayBy = nameBuffer;
                        n2 = n;
                        c3 = c2;
                    }
                    else if (c2 == '\r') {
                        if (inputPtr2 >= this._inputEnd) {
                            this.loadMoreGuaranteed();
                        }
                        final char[] inputBuffer2 = this._inputBuffer;
                        final int inputPtr3 = this._inputPtr;
                        if (inputBuffer2[inputPtr3] == '\n') {
                            this._inputPtr = inputPtr3 + 1;
                        }
                        this.markLF();
                        c3 = '\n';
                        growArrayBy = nameBuffer;
                        n2 = n;
                    }
                    else {
                        growArrayBy = nameBuffer;
                        n2 = n;
                        if ((c3 = c2) != '\t') {
                            this.throwInvalidSpace(c2);
                            c3 = c2;
                            n2 = n;
                            growArrayBy = nameBuffer;
                        }
                    }
                }
            }
            nameBuffer = growArrayBy;
            if (n2 >= growArrayBy.length) {
                nameBuffer = DataUtil.growArrayBy(growArrayBy, growArrayBy.length);
                super._nameBuffer = nameBuffer;
            }
            nameBuffer[n2] = c3;
            n = n2 + 1;
        }
        String canonicalizeURI;
        if (n == 0) {
            canonicalizeURI = "";
        }
        else {
            canonicalizeURI = super._config.canonicalizeURI(nameBuffer, n);
        }
        this.bindNs(pName, canonicalizeURI);
    }
    
    private final void matchAsciiKeyword(final String str) {
        for (int length = str.length(), i = 1; i < length; ++i) {
            if (this._inputPtr >= this._inputEnd) {
                this.loadMoreGuaranteed();
            }
            final char c = this._inputBuffer[this._inputPtr++];
            if (c != str.charAt(i)) {
                final StringBuilder sb = new StringBuilder();
                sb.append(" (expected '");
                sb.append(str.charAt(i));
                sb.append("' for ");
                sb.append(str);
                sb.append(" keyword)");
                this.reportTreeUnexpChar(c, sb.toString());
            }
        }
    }
    
    private void reportInvalidFirstSurrogate(final char i) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid surrogate character (code 0x");
        sb.append(Integer.toHexString(i));
        sb.append("): can not start a surrogate pair");
        this.reportInputProblem(sb.toString());
    }
    
    private void reportInvalidSecondSurrogate(final char i) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid surrogate character (code ");
        sb.append(Integer.toHexString(i));
        sb.append("): is not legal as the second part of a surrogate pair");
        this.reportInputProblem(sb.toString());
    }
    
    @Override
    public void _closeSource() {
        final Reader in = this._in;
        if (in != null) {
            in.close();
            this._in = null;
        }
    }
    
    public int _nextEntity() {
        super._textBuilder.resetWithEmpty();
        return super._currToken = 9;
    }
    
    @Override
    public void _releaseBuffers() {
        super._releaseBuffers();
        if (this._symbols.maybeDirty()) {
            super._config.updateCBSymbols(this._symbols);
        }
        if (this._in != null) {
            final char[] inputBuffer = this._inputBuffer;
            if (inputBuffer != null) {
                super._config.freeFullCBuffer(inputBuffer);
                this._inputBuffer = null;
            }
        }
    }
    
    public final PName addPName(final char[] array, final int n, final int n2) {
        final char c = array[0];
        final int n3 = 1;
        final int n4 = -1;
        int i;
        int n5;
        if (c >= '\ud800' && c < '\ue000') {
            if (n == 1) {
                this.reportInvalidFirstSurrogate(c);
            }
            this.checkSurrogateNameChar(c, array[1], 0);
            i = 2;
            n5 = n4;
        }
        else {
            i = n3;
            n5 = n4;
            if (!XmlChars.is10NameStartChar(c)) {
                this.reportInvalidNameChar(c, 0);
                n5 = n4;
                i = n3;
            }
        }
        while (i < n) {
            final char c2 = array[i];
            int n7;
            if (c2 >= '\ud800' && c2 < '\ue000') {
                final int n6 = i + 1;
                if (n6 >= n) {
                    this.reportInvalidFirstSurrogate(c2);
                }
                this.checkSurrogateNameChar(c2, array[n6], i);
                n7 = n5;
            }
            else if (c2 == ':') {
                if (n5 >= 0) {
                    this.reportMultipleColonsInName();
                }
                n7 = i;
            }
            else {
                n7 = n5;
                if (!XmlChars.is10NameChar(c2)) {
                    this.reportInvalidNameChar(c2, i);
                    n7 = n5;
                }
            }
            ++i;
            n5 = n7;
        }
        return this._symbols.addSymbol(array, 0, n, n2);
    }
    
    public final int checkInTreeIndentation(final char c) {
        int n = 32;
        if (c == '\r') {
            if (this._inputPtr >= this._inputEnd && !this.loadMore()) {
                super._textBuilder.resetWithIndentation(0, ' ');
                return -1;
            }
            final char[] inputBuffer = this._inputBuffer;
            final int inputPtr = this._inputPtr;
            if (inputBuffer[inputPtr] == '\n') {
                this._inputPtr = inputPtr + 1;
            }
        }
        this.markLF();
        if (this._inputPtr >= this._inputEnd) {
            this.loadMoreGuaranteed();
        }
        final char[] inputBuffer2 = this._inputBuffer;
        final int inputPtr2 = this._inputPtr;
        final char c2 = inputBuffer2[inputPtr2];
        if (c2 == ' ' || c2 == '\t') {
            this._inputPtr = inputPtr2 + 1;
            if (c2 != ' ') {
                n = 8;
            }
            int i = 1;
            while (i <= n) {
                if (this._inputPtr >= this._inputEnd) {
                    this.loadMoreGuaranteed();
                }
                final char[] inputBuffer3 = this._inputBuffer;
                final int inputPtr3 = this._inputPtr;
                final char c3 = inputBuffer3[inputPtr3];
                if (c3 != c2) {
                    if (c3 == '<' && inputPtr3 + 1 < this._inputEnd && inputBuffer3[inputPtr3 + 1] != '!') {
                        super._textBuilder.resetWithIndentation(i, c2);
                        return -1;
                    }
                    break;
                }
                else {
                    this._inputPtr = inputPtr3 + 1;
                    ++i;
                }
            }
            final char[] resetWithEmpty = super._textBuilder.resetWithEmpty();
            resetWithEmpty[0] = '\n';
            for (int j = 1; j <= i; ++j) {
                resetWithEmpty[j] = c2;
            }
            ++i;
            super._textBuilder.setCurrentLength(i);
            return i;
        }
        if (c2 == '<' && inputPtr2 + 1 < this._inputEnd && inputBuffer2[inputPtr2 + 1] != '!') {
            super._textBuilder.resetWithIndentation(0, ' ');
            return -1;
        }
        super._textBuilder.resetWithEmpty()[0] = '\n';
        super._textBuilder.setCurrentLength(1);
        return 1;
    }
    
    public final int checkPrologIndentation(final char c) {
        final int n = 32;
        if (c == '\r') {
            if (this._inputPtr >= this._inputEnd && !this.loadMore()) {
                super._textBuilder.resetWithIndentation(0, ' ');
                return -1;
            }
            final char[] inputBuffer = this._inputBuffer;
            final int inputPtr = this._inputPtr;
            if (inputBuffer[inputPtr] == '\n') {
                this._inputPtr = inputPtr + 1;
            }
        }
        this.markLF();
        if (this._inputPtr >= this._inputEnd && !this.loadMore()) {
            super._textBuilder.resetWithIndentation(0, ' ');
            return -1;
        }
        final char[] inputBuffer2 = this._inputBuffer;
        final int inputPtr2 = this._inputPtr;
        final char c2 = inputBuffer2[inputPtr2];
        if (c2 == ' ' || c2 == '\t') {
            this._inputPtr = inputPtr2 + 1;
            int n2;
            if (c2 == ' ') {
                n2 = n;
            }
            else {
                n2 = 8;
            }
            int n3 = 1;
            while (true) {
                while (this._inputPtr < this._inputEnd || this.loadMore()) {
                    final char[] inputBuffer3 = this._inputBuffer;
                    final int inputPtr3 = this._inputPtr;
                    if (inputBuffer3[inputPtr3] != c2) {
                        super._textBuilder.resetWithIndentation(n3, c2);
                        return -1;
                    }
                    this._inputPtr = inputPtr3 + 1;
                    final int n4 = n3 + 1;
                    if ((n3 = n4) >= n2) {
                        final char[] resetWithEmpty = super._textBuilder.resetWithEmpty();
                        resetWithEmpty[0] = '\n';
                        for (int i = 1; i <= n4; ++i) {
                            resetWithEmpty[i] = c2;
                        }
                        final int currentLength = n4 + 1;
                        super._textBuilder.setCurrentLength(currentLength);
                        return currentLength;
                    }
                }
                continue;
            }
        }
        if (c2 == '<') {
            super._textBuilder.resetWithIndentation(0, ' ');
            return -1;
        }
        super._textBuilder.resetWithEmpty()[0] = '\n';
        super._textBuilder.setCurrentLength(1);
        return 1;
    }
    
    @Override
    public final void finishCData() {
        final int[] other_CHARS = ReaderScanner.sCharTypes.OTHER_CHARS;
        final char[] inputBuffer = this._inputBuffer;
        char[] resetWithEmpty = super._textBuilder.resetWithEmpty();
        int n = 0;
        int currentLength = 0;
    Block_18:
        while (true) {
            int n2;
            if ((n2 = this._inputPtr) >= this._inputEnd) {
                this.loadMoreGuaranteed();
                n2 = this._inputPtr;
            }
            char[] array = resetWithEmpty;
            int n3;
            if ((n3 = n) >= resetWithEmpty.length) {
                array = super._textBuilder.finishCurrentSegment();
                n3 = 0;
            }
            final int inputEnd = this._inputEnd;
            final int n4 = array.length - n3 + n2;
            n = n3;
            int i = n2;
            int n5;
            if (n4 < (n5 = inputEnd)) {
                n5 = n4;
                i = n2;
                n = n3;
            }
            while (i < n5) {
                final int inputPtr = i + 1;
                final char c = inputBuffer[i];
                Label_0168: {
                    if (c <= '\u00ff') {
                        if (other_CHARS[c] != 0) {
                            break Label_0168;
                        }
                    }
                    else if (c >= '\ud800') {
                        break Label_0168;
                    }
                    array[n] = c;
                    i = inputPtr;
                    ++n;
                    continue;
                }
                this._inputPtr = inputPtr;
                int n7 = 0;
                char c2 = '\0';
                Label_0624: {
                    if (c <= '\u00ff') {
                        final int n6 = other_CHARS[c];
                        if (n6 != 1) {
                            if (n6 != 2) {
                                if (n6 == 3) {
                                    this.markLF();
                                    resetWithEmpty = array;
                                    n7 = n;
                                    c2 = c;
                                    break Label_0624;
                                }
                                if (n6 != 11) {
                                    resetWithEmpty = array;
                                    n7 = n;
                                    c2 = c;
                                    break Label_0624;
                                }
                                int n8 = 0;
                                char c3;
                                while (true) {
                                    if (this._inputPtr >= this._inputEnd) {
                                        this.loadMoreGuaranteed();
                                    }
                                    final char[] inputBuffer2 = this._inputBuffer;
                                    final int inputPtr2 = this._inputPtr;
                                    c3 = inputBuffer2[inputPtr2];
                                    if (c3 != ']') {
                                        break;
                                    }
                                    this._inputPtr = inputPtr2 + 1;
                                    ++n8;
                                }
                                int n9;
                                if (c3 == '>' && n8 >= 1) {
                                    n9 = 1;
                                }
                                else {
                                    n9 = 0;
                                }
                                resetWithEmpty = array;
                                currentLength = n;
                                int n10 = n8;
                                int n11 = n9;
                                while (true) {
                                    Label_0327: {
                                        if (n9 == 0) {
                                            break Label_0327;
                                        }
                                        n10 = n8 - 1;
                                        n11 = n9;
                                        currentLength = n;
                                        resetWithEmpty = array;
                                    }
                                    if (n10 > 0) {
                                        n = currentLength + 1;
                                        resetWithEmpty[currentLength] = ']';
                                        if (n >= resetWithEmpty.length) {
                                            array = super._textBuilder.finishCurrentSegment();
                                            n = 0;
                                            n8 = n10;
                                            n9 = n11;
                                            continue;
                                        }
                                        array = resetWithEmpty;
                                        n8 = n10;
                                        n9 = n11;
                                        continue;
                                    }
                                    else {
                                        n7 = currentLength;
                                        c2 = c;
                                        if (n11 != 0) {
                                            break Block_18;
                                        }
                                        break Label_0624;
                                    }
                                    break;
                                }
                            }
                        }
                        else {
                            this.handleInvalidXmlChar(c);
                        }
                        if (this._inputPtr >= this._inputEnd) {
                            this.loadMoreGuaranteed();
                        }
                        final int inputPtr3 = this._inputPtr;
                        if (inputBuffer[inputPtr3] == '\n') {
                            this._inputPtr = inputPtr3 + 1;
                        }
                        this.markLF();
                        c2 = '\n';
                        resetWithEmpty = array;
                        n7 = n;
                    }
                    else {
                        resetWithEmpty = array;
                        n7 = n;
                        if ((c2 = c) >= '\ud800') {
                            if (c < '\ue000') {
                                c2 = this.checkSurrogate(c);
                                n7 = n + 1;
                                array[n] = c;
                                if (n7 >= array.length) {
                                    array = super._textBuilder.finishCurrentSegment();
                                    n7 = 0;
                                }
                                resetWithEmpty = array;
                            }
                            else {
                                resetWithEmpty = array;
                                n7 = n;
                                if ((c2 = c) >= '\ufffe') {
                                    c2 = this.handleInvalidXmlChar(c);
                                    n7 = n;
                                    resetWithEmpty = array;
                                }
                            }
                        }
                    }
                }
                resetWithEmpty[n7] = c2;
                n = n7 + 1;
                continue Block_18;
            }
            this._inputPtr = i;
            resetWithEmpty = array;
        }
        ++this._inputPtr;
        super._textBuilder.setCurrentLength(currentLength);
        if (super._cfgCoalescing && !super._entityPending) {
            this.finishCoalescedText();
        }
    }
    
    @Override
    public final void finishCharacters() {
        final int mTmpChar = this.mTmpChar;
        char[] array;
        int checkInTreeIndentation;
        if (mTmpChar < 0) {
            int n = -mTmpChar;
            array = super._textBuilder.resetWithEmpty();
            int n3;
            if (n >> 16 != 0) {
                final int n2 = n - 65536;
                array[0] = (char)(n2 >> 10 | 0xD800);
                n = ((n2 & 0x3FF) | 0xDC00);
                n3 = 1;
            }
            else {
                n3 = 0;
            }
            final int n4 = n3 + 1;
            array[n3] = (char)n;
            checkInTreeIndentation = n4;
        }
        else if (mTmpChar != 13 && mTmpChar != 10) {
            array = super._textBuilder.resetWithEmpty();
            checkInTreeIndentation = 0;
        }
        else {
            ++this._inputPtr;
            checkInTreeIndentation = this.checkInTreeIndentation((char)mTmpChar);
            if (checkInTreeIndentation < 0) {
                return;
            }
            array = super._textBuilder.getBufferWithoutReset();
        }
        final int[] text_CHARS = ReaderScanner.sCharTypes.TEXT_CHARS;
        final char[] inputBuffer = this._inputBuffer;
        char[] finishCurrentSegment = array;
    Label_0656:
        while (true) {
            int n5;
            if ((n5 = this._inputPtr) >= this._inputEnd) {
                this.loadMoreGuaranteed();
                n5 = this._inputPtr;
            }
            char[] array2 = finishCurrentSegment;
            int n6;
            if ((n6 = checkInTreeIndentation) >= finishCurrentSegment.length) {
                array2 = super._textBuilder.finishCurrentSegment();
                n6 = 0;
            }
            final int inputEnd = this._inputEnd;
            final int n7 = array2.length - n6 + n5;
            checkInTreeIndentation = n6;
            int i = n5;
            int n8;
            if (n7 < (n8 = inputEnd)) {
                n8 = n7;
                i = n5;
                checkInTreeIndentation = n6;
            }
            while (i < n8) {
                final int inputPtr = i + 1;
                final char c = inputBuffer[i];
                Label_0304: {
                    if (c <= '\u00ff') {
                        if (text_CHARS[c] != 0) {
                            break Label_0304;
                        }
                    }
                    else if (c >= '\ud800') {
                        break Label_0304;
                    }
                    array2[checkInTreeIndentation] = c;
                    i = inputPtr;
                    ++checkInTreeIndentation;
                    continue;
                }
                this._inputPtr = inputPtr;
                int n10 = 0;
                char c2 = '\0';
                Label_0866: {
                    if (c <= '\u00ff') {
                        final int n9 = text_CHARS[c];
                        if (n9 != 1) {
                            if (n9 != 2) {
                                if (n9 == 3) {
                                    this.markLF();
                                    finishCurrentSegment = array2;
                                    n10 = checkInTreeIndentation;
                                    c2 = c;
                                    break Label_0866;
                                }
                                switch (n9) {
                                    default: {
                                        finishCurrentSegment = array2;
                                        n10 = checkInTreeIndentation;
                                        c2 = c;
                                        break Label_0866;
                                    }
                                    case 11: {
                                        int n11 = 1;
                                        char c3;
                                        while (true) {
                                            if (this._inputPtr >= this._inputEnd) {
                                                this.loadMoreGuaranteed();
                                            }
                                            final int inputPtr2 = this._inputPtr;
                                            c3 = inputBuffer[inputPtr2];
                                            if (c3 != ']') {
                                                break;
                                            }
                                            this._inputPtr = inputPtr2 + 1;
                                            ++n11;
                                        }
                                        finishCurrentSegment = array2;
                                        n10 = checkInTreeIndentation;
                                        int j = n11;
                                        if (c3 == '>') {
                                            finishCurrentSegment = array2;
                                            n10 = checkInTreeIndentation;
                                            if ((j = n11) > 1) {
                                                this.reportIllegalCDataEnd();
                                                j = n11;
                                                n10 = checkInTreeIndentation;
                                                finishCurrentSegment = array2;
                                            }
                                        }
                                        while (j > 1) {
                                            final int n12 = n10 + 1;
                                            finishCurrentSegment[n10] = ']';
                                            if (n12 >= finishCurrentSegment.length) {
                                                finishCurrentSegment = super._textBuilder.finishCurrentSegment();
                                                n10 = 0;
                                            }
                                            else {
                                                n10 = n12;
                                            }
                                            --j;
                                        }
                                        c2 = ']';
                                        break Label_0866;
                                    }
                                    case 10: {
                                        final int handleEntityInText = this.handleEntityInText(false);
                                        if (handleEntityInText == 0) {
                                            super._entityPending = true;
                                            break Label_0656;
                                        }
                                        finishCurrentSegment = array2;
                                        n10 = checkInTreeIndentation;
                                        int n13 = handleEntityInText;
                                        if (handleEntityInText >> 16 != 0) {
                                            final int n14 = handleEntityInText - 65536;
                                            n10 = checkInTreeIndentation + 1;
                                            array2[checkInTreeIndentation] = (char)(n14 >> 10 | 0xD800);
                                            if (n10 >= array2.length) {
                                                array2 = super._textBuilder.finishCurrentSegment();
                                                n10 = 0;
                                            }
                                            n13 = ((n14 & 0x3FF) | 0xDC00);
                                            finishCurrentSegment = array2;
                                        }
                                        c2 = (char)n13;
                                        break Label_0866;
                                    }
                                    case 9: {
                                        --this._inputPtr;
                                        break Label_0656;
                                    }
                                }
                            }
                        }
                        else {
                            this.handleInvalidXmlChar(c);
                        }
                        int n15;
                        if ((n15 = this._inputPtr) >= this._inputEnd) {
                            this.loadMoreGuaranteed();
                            n15 = this._inputPtr;
                        }
                        if (inputBuffer[n15] == '\n') {
                            ++this._inputPtr;
                        }
                        this.markLF();
                        c2 = '\n';
                        finishCurrentSegment = array2;
                        n10 = checkInTreeIndentation;
                    }
                    else {
                        finishCurrentSegment = array2;
                        n10 = checkInTreeIndentation;
                        if ((c2 = c) >= '\ud800') {
                            if (c < '\ue000') {
                                c2 = this.checkSurrogate(c);
                                n10 = checkInTreeIndentation + 1;
                                array2[checkInTreeIndentation] = c;
                                if (n10 >= array2.length) {
                                    array2 = super._textBuilder.finishCurrentSegment();
                                    n10 = 0;
                                }
                                finishCurrentSegment = array2;
                            }
                            else {
                                finishCurrentSegment = array2;
                                n10 = checkInTreeIndentation;
                                if ((c2 = c) >= '\ufffe') {
                                    c2 = this.handleInvalidXmlChar(c);
                                    n10 = checkInTreeIndentation;
                                    finishCurrentSegment = array2;
                                }
                            }
                        }
                    }
                }
                finishCurrentSegment[n10] = c2;
                checkInTreeIndentation = n10 + 1;
                continue Label_0656;
            }
            this._inputPtr = i;
            finishCurrentSegment = array2;
        }
        super._textBuilder.setCurrentLength(checkInTreeIndentation);
        if (super._cfgCoalescing && !super._entityPending) {
            this.finishCoalescedText();
        }
    }
    
    public final void finishCoalescedCData() {
        final int[] other_CHARS = ReaderScanner.sCharTypes.OTHER_CHARS;
        final char[] inputBuffer = this._inputBuffer;
        char[] array = super._textBuilder.getBufferWithoutReset();
        int currentLength = super._textBuilder.getCurrentLength();
        int currentLength2 = 0;
    Block_18:
        while (true) {
            int n;
            if ((n = this._inputPtr) >= this._inputEnd) {
                this.loadMoreGuaranteed();
                n = this._inputPtr;
            }
            final int length = array.length;
            final int n2 = 0;
            int n3;
            if ((n3 = currentLength) >= length) {
                array = super._textBuilder.finishCurrentSegment();
                n3 = 0;
            }
            final int inputEnd = this._inputEnd;
            final int n4 = array.length - n3 + n;
            currentLength = n3;
            int i = n;
            int n5;
            if (n4 < (n5 = inputEnd)) {
                n5 = n4;
                i = n;
                currentLength = n3;
            }
            while (i < n5) {
                final int inputPtr = i + 1;
                final char c = inputBuffer[i];
                Label_0177: {
                    if (c <= '\u00ff') {
                        if (other_CHARS[c] != 0) {
                            break Label_0177;
                        }
                    }
                    else if (c >= '\ud800') {
                        break Label_0177;
                    }
                    array[currentLength] = c;
                    i = inputPtr;
                    ++currentLength;
                    continue;
                }
                this._inputPtr = inputPtr;
                char[] array2 = null;
                int n7 = 0;
                char c2 = '\0';
                Label_0621: {
                    if (c <= '\u00ff') {
                        final int n6 = other_CHARS[c];
                        if (n6 != 1) {
                            if (n6 != 2) {
                                if (n6 == 3) {
                                    this.markLF();
                                    array2 = array;
                                    n7 = currentLength;
                                    c2 = c;
                                    break Label_0621;
                                }
                                if (n6 != 11) {
                                    array2 = array;
                                    n7 = currentLength;
                                    c2 = c;
                                    break Label_0621;
                                }
                                int n8 = 0;
                                char c3;
                                while (true) {
                                    if (this._inputPtr >= this._inputEnd) {
                                        this.loadMoreGuaranteed();
                                    }
                                    final char[] inputBuffer2 = this._inputBuffer;
                                    final int inputPtr2 = this._inputPtr;
                                    c3 = inputBuffer2[inputPtr2];
                                    if (c3 != ']') {
                                        break;
                                    }
                                    this._inputPtr = inputPtr2 + 1;
                                    ++n8;
                                }
                                int n9;
                                if (c3 == '>' && n8 >= 1) {
                                    n9 = 1;
                                }
                                else {
                                    n9 = 0;
                                }
                                array2 = array;
                                currentLength2 = currentLength;
                                int n10 = n8;
                                int n11 = n9;
                                while (true) {
                                    Label_0336: {
                                        if (n9 == 0) {
                                            break Label_0336;
                                        }
                                        n10 = n8 - 1;
                                        n11 = n9;
                                        currentLength2 = currentLength;
                                        array2 = array;
                                    }
                                    if (n10 > 0) {
                                        currentLength = currentLength2 + 1;
                                        array2[currentLength2] = ']';
                                        if (currentLength >= array2.length) {
                                            array = super._textBuilder.finishCurrentSegment();
                                            currentLength = 0;
                                            n8 = n10;
                                            n9 = n11;
                                            continue;
                                        }
                                        array = array2;
                                        n8 = n10;
                                        n9 = n11;
                                        continue;
                                    }
                                    else {
                                        n7 = currentLength2;
                                        c2 = c;
                                        if (n11 != 0) {
                                            break Block_18;
                                        }
                                        break Label_0621;
                                    }
                                    break;
                                }
                            }
                        }
                        else {
                            this.handleInvalidXmlChar(c);
                        }
                        if (this._inputPtr >= this._inputEnd) {
                            this.loadMoreGuaranteed();
                        }
                        final int inputPtr3 = this._inputPtr;
                        if (inputBuffer[inputPtr3] == '\n') {
                            this._inputPtr = inputPtr3 + 1;
                        }
                        this.markLF();
                        c2 = '\n';
                        array2 = array;
                        n7 = currentLength;
                    }
                    else {
                        array2 = array;
                        n7 = currentLength;
                        if ((c2 = c) >= '\ud800') {
                            if (c < '\ue000') {
                                c2 = this.checkSurrogate(c);
                                final int n12 = currentLength + 1;
                                array[currentLength] = c;
                                int n13;
                                if (n12 >= array.length) {
                                    array = super._textBuilder.finishCurrentSegment();
                                    n13 = n2;
                                }
                                else {
                                    n13 = n12;
                                }
                                array2 = array;
                                n7 = n13;
                            }
                            else {
                                array2 = array;
                                n7 = currentLength;
                                if ((c2 = c) >= '\ufffe') {
                                    c2 = this.handleInvalidXmlChar(c);
                                    n7 = currentLength;
                                    array2 = array;
                                }
                            }
                        }
                    }
                }
                array2[n7] = c2;
                currentLength = n7 + 1;
                array = array2;
                continue Block_18;
            }
            this._inputPtr = i;
        }
        ++this._inputPtr;
        super._textBuilder.setCurrentLength(currentLength2);
    }
    
    public final void finishCoalescedCharacters() {
        final int[] text_CHARS = ReaderScanner.sCharTypes.TEXT_CHARS;
        final char[] inputBuffer = this._inputBuffer;
        char[] array = super._textBuilder.getBufferWithoutReset();
        int currentLength = super._textBuilder.getCurrentLength();
    Label_0538:
        while (true) {
            int n;
            if ((n = this._inputPtr) >= this._inputEnd) {
                this.loadMoreGuaranteed();
                n = this._inputPtr;
            }
            final int length = array.length;
            final int n2 = 0;
            final int n3 = 0;
            int n4;
            if ((n4 = currentLength) >= length) {
                array = super._textBuilder.finishCurrentSegment();
                n4 = 0;
            }
            final int inputEnd = this._inputEnd;
            final int n5 = array.length - n4 + n;
            currentLength = n4;
            int i = n;
            int n6;
            if (n5 < (n6 = inputEnd)) {
                n6 = n5;
                i = n;
                currentLength = n4;
            }
            while (i < n6) {
                final int inputPtr = i + 1;
                final char c = inputBuffer[i];
                Label_0180: {
                    if (c <= '\u00ff') {
                        if (text_CHARS[c] != 0) {
                            break Label_0180;
                        }
                    }
                    else if (c >= '\ud800') {
                        break Label_0180;
                    }
                    array[currentLength] = c;
                    i = inputPtr;
                    ++currentLength;
                    continue;
                }
                this._inputPtr = inputPtr;
                char[] finishCurrentSegment = null;
                int n8 = 0;
                char c2 = '\0';
                Label_0736: {
                    if (c <= '\u00ff') {
                        final int n7 = text_CHARS[c];
                        if (n7 != 1) {
                            if (n7 != 2) {
                                if (n7 == 3) {
                                    this.markLF();
                                    finishCurrentSegment = array;
                                    n8 = currentLength;
                                    c2 = c;
                                    break Label_0736;
                                }
                                switch (n7) {
                                    default: {
                                        finishCurrentSegment = array;
                                        n8 = currentLength;
                                        c2 = c;
                                        break Label_0736;
                                    }
                                    case 11: {
                                        int n9 = 1;
                                        char c3;
                                        while (true) {
                                            if (this._inputPtr >= this._inputEnd) {
                                                this.loadMoreGuaranteed();
                                            }
                                            final int inputPtr2 = this._inputPtr;
                                            c3 = inputBuffer[inputPtr2];
                                            if (c3 != ']') {
                                                break;
                                            }
                                            this._inputPtr = inputPtr2 + 1;
                                            ++n9;
                                        }
                                        finishCurrentSegment = array;
                                        n8 = currentLength;
                                        int j = n9;
                                        if (c3 == '>') {
                                            finishCurrentSegment = array;
                                            n8 = currentLength;
                                            if ((j = n9) > 1) {
                                                this.reportIllegalCDataEnd();
                                                j = n9;
                                                n8 = currentLength;
                                                finishCurrentSegment = array;
                                            }
                                        }
                                        while (j > 1) {
                                            final int n10 = n8 + 1;
                                            finishCurrentSegment[n8] = ']';
                                            if (n10 >= finishCurrentSegment.length) {
                                                finishCurrentSegment = super._textBuilder.finishCurrentSegment();
                                                n8 = 0;
                                            }
                                            else {
                                                n8 = n10;
                                            }
                                            --j;
                                        }
                                        c2 = ']';
                                        break Label_0736;
                                    }
                                    case 10: {
                                        final int handleEntityInText = this.handleEntityInText(false);
                                        if (handleEntityInText == 0) {
                                            super._entityPending = true;
                                            break Label_0538;
                                        }
                                        finishCurrentSegment = array;
                                        n8 = currentLength;
                                        int n11 = handleEntityInText;
                                        if (handleEntityInText >> 16 != 0) {
                                            final int n12 = handleEntityInText - 65536;
                                            final int n13 = currentLength + 1;
                                            array[currentLength] = (char)(n12 >> 10 | 0xD800);
                                            int n14;
                                            if (n13 >= array.length) {
                                                array = super._textBuilder.finishCurrentSegment();
                                                n14 = n3;
                                            }
                                            else {
                                                n14 = n13;
                                            }
                                            n11 = (0xDC00 | (n12 & 0x3FF));
                                            n8 = n14;
                                            finishCurrentSegment = array;
                                        }
                                        c2 = (char)n11;
                                        break Label_0736;
                                    }
                                    case 9: {
                                        --this._inputPtr;
                                        break Label_0538;
                                    }
                                }
                            }
                        }
                        else {
                            this.handleInvalidXmlChar(c);
                        }
                        int n15;
                        if ((n15 = this._inputPtr) >= this._inputEnd) {
                            this.loadMoreGuaranteed();
                            n15 = this._inputPtr;
                        }
                        if (inputBuffer[n15] == '\n') {
                            ++this._inputPtr;
                        }
                        this.markLF();
                        c2 = '\n';
                        finishCurrentSegment = array;
                        n8 = currentLength;
                    }
                    else {
                        finishCurrentSegment = array;
                        n8 = currentLength;
                        if ((c2 = c) >= '\ud800') {
                            if (c < '\ue000') {
                                c2 = this.checkSurrogate(c);
                                final int n16 = currentLength + 1;
                                array[currentLength] = c;
                                int n17;
                                if (n16 >= array.length) {
                                    array = super._textBuilder.finishCurrentSegment();
                                    n17 = n2;
                                }
                                else {
                                    n17 = n16;
                                }
                                finishCurrentSegment = array;
                                n8 = n17;
                            }
                            else {
                                finishCurrentSegment = array;
                                n8 = currentLength;
                                if ((c2 = c) >= '\ufffe') {
                                    c2 = this.handleInvalidXmlChar(c);
                                    n8 = currentLength;
                                    finishCurrentSegment = array;
                                }
                            }
                        }
                    }
                }
                finishCurrentSegment[n8] = c2;
                currentLength = n8 + 1;
                array = finishCurrentSegment;
                continue Label_0538;
            }
            this._inputPtr = i;
        }
        super._textBuilder.setCurrentLength(currentLength);
    }
    
    public final void finishCoalescedText() {
        while (this._inputPtr < this._inputEnd || this.loadMore()) {
            final char[] inputBuffer = this._inputBuffer;
            final int inputPtr = this._inputPtr;
            if (inputBuffer[inputPtr] == '<') {
                if (inputPtr + 3 >= this._inputEnd && !this.loadAndRetain(3)) {
                    return;
                }
                final char[] inputBuffer2 = this._inputBuffer;
                final int inputPtr2 = this._inputPtr;
                if (inputBuffer2[inputPtr2 + 1] != '!' || inputBuffer2[inputPtr2 + 2] != '[') {
                    return;
                }
                this._inputPtr = inputPtr2 + 3;
                for (int i = 0; i < 6; ++i) {
                    if (this._inputPtr >= this._inputEnd) {
                        this.loadMoreGuaranteed();
                    }
                    final char c = this._inputBuffer[this._inputPtr++];
                    if (c != "CDATA[".charAt(i)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append(" (expected '");
                        sb.append("CDATA[".charAt(i));
                        sb.append("' for CDATA section)");
                        this.reportTreeUnexpChar(c, sb.toString());
                    }
                }
                this.finishCoalescedCData();
            }
            else {
                this.finishCoalescedCharacters();
                if (super._entityPending) {
                    return;
                }
                continue;
            }
        }
    }
    
    @Override
    public final void finishComment() {
        final int[] other_CHARS = ReaderScanner.sCharTypes.OTHER_CHARS;
        final char[] inputBuffer = this._inputBuffer;
        char[] resetWithEmpty = super._textBuilder.resetWithEmpty();
        int currentLength = 0;
        int inputPtr2 = 0;
    Block_13:
        while (true) {
            int n;
            if ((n = this._inputPtr) >= this._inputEnd) {
                this.loadMoreGuaranteed();
                n = this._inputPtr;
            }
            char[] array = resetWithEmpty;
            int n2;
            if ((n2 = currentLength) >= resetWithEmpty.length) {
                array = super._textBuilder.finishCurrentSegment();
                n2 = 0;
            }
            final int inputEnd = this._inputEnd;
            final int n3 = array.length - n2 + n;
            currentLength = n2;
            int i = n;
            int n4;
            if (n3 < (n4 = inputEnd)) {
                n4 = n3;
                i = n;
                currentLength = n2;
            }
            while (i < n4) {
                final int inputPtr = i + 1;
                final char c = inputBuffer[i];
                Label_0168: {
                    if (c <= '\u00ff') {
                        if (other_CHARS[c] != 0) {
                            break Label_0168;
                        }
                    }
                    else if (c >= '\ud800') {
                        break Label_0168;
                    }
                    array[currentLength] = c;
                    i = inputPtr;
                    ++currentLength;
                    continue;
                }
                this._inputPtr = inputPtr;
                int n6 = 0;
                char c2 = '\0';
                Label_0516: {
                    if (c <= '\u00ff') {
                        final int n5 = other_CHARS[c];
                        if (n5 != 1) {
                            if (n5 != 2) {
                                if (n5 == 3) {
                                    this.markLF();
                                    resetWithEmpty = array;
                                    n6 = currentLength;
                                    c2 = c;
                                    break Label_0516;
                                }
                                if (n5 != 13) {
                                    resetWithEmpty = array;
                                    n6 = currentLength;
                                    c2 = c;
                                    break Label_0516;
                                }
                                if (this._inputPtr >= this._inputEnd) {
                                    this.loadMoreGuaranteed();
                                }
                                final char[] inputBuffer2 = this._inputBuffer;
                                inputPtr2 = this._inputPtr;
                                resetWithEmpty = array;
                                n6 = currentLength;
                                c2 = c;
                                if (inputBuffer2[inputPtr2] == '-') {
                                    break Block_13;
                                }
                                break Label_0516;
                            }
                        }
                        else {
                            this.handleInvalidXmlChar(c);
                        }
                        if (this._inputPtr >= this._inputEnd) {
                            this.loadMoreGuaranteed();
                        }
                        final int inputPtr3 = this._inputPtr;
                        if (inputBuffer[inputPtr3] == '\n') {
                            this._inputPtr = inputPtr3 + 1;
                        }
                        this.markLF();
                        c2 = '\n';
                        resetWithEmpty = array;
                        n6 = currentLength;
                    }
                    else {
                        resetWithEmpty = array;
                        n6 = currentLength;
                        if ((c2 = c) >= '\ud800') {
                            if (c < '\ue000') {
                                c2 = this.checkSurrogate(c);
                                final int n7 = currentLength + 1;
                                array[currentLength] = c;
                                int n8;
                                if (n7 >= array.length) {
                                    array = super._textBuilder.finishCurrentSegment();
                                    n8 = 0;
                                }
                                else {
                                    n8 = n7;
                                }
                                resetWithEmpty = array;
                                n6 = n8;
                            }
                            else {
                                resetWithEmpty = array;
                                n6 = currentLength;
                                if ((c2 = c) >= '\ufffe') {
                                    c2 = this.handleInvalidXmlChar(c);
                                    n6 = currentLength;
                                    resetWithEmpty = array;
                                }
                            }
                        }
                    }
                }
                resetWithEmpty[n6] = c2;
                currentLength = n6 + 1;
                continue Block_13;
            }
            this._inputPtr = i;
            resetWithEmpty = array;
        }
        if ((this._inputPtr = inputPtr2 + 1) >= this._inputEnd) {
            this.loadMoreGuaranteed();
        }
        if (this._inputBuffer[this._inputPtr++] != '>') {
            this.reportDoubleHyphenInComments();
        }
        super._textBuilder.setCurrentLength(currentLength);
    }
    
    @Override
    public final void finishDTD(final boolean b) {
        char[] resetWithEmpty;
        if (b) {
            resetWithEmpty = super._textBuilder.resetWithEmpty();
        }
        else {
            resetWithEmpty = null;
        }
        final int[] dtd_CHARS = ReaderScanner.sCharTypes.DTD_CHARS;
        int n = 0;
        int n3;
        int n2 = n3 = 0;
        int currentLength = 0;
        char[] array = null;
    Block_14:
        while (true) {
            int n4;
            if ((n4 = this._inputPtr) >= this._inputEnd) {
                this.loadMoreGuaranteed();
                n4 = this._inputPtr;
            }
            final int inputEnd = this._inputEnd;
            currentLength = n;
            int i = n4;
            int n5 = inputEnd;
            if ((array = resetWithEmpty) != null) {
                int n6 = n;
                char[] finishCurrentSegment = resetWithEmpty;
                if (n >= resetWithEmpty.length) {
                    finishCurrentSegment = super._textBuilder.finishCurrentSegment();
                    n6 = 0;
                }
                final int n7 = finishCurrentSegment.length - n6 + n4;
                currentLength = n6;
                i = n4;
                n5 = inputEnd;
                array = finishCurrentSegment;
                if (n7 < inputEnd) {
                    n5 = n7;
                    array = finishCurrentSegment;
                    i = n4;
                    currentLength = n6;
                }
            }
            while (i < n5) {
                final char[] inputBuffer = this._inputBuffer;
                final int inputPtr = i + 1;
                final char c = inputBuffer[i];
                Label_0219: {
                    if (c <= '\u00ff') {
                        if (dtd_CHARS[c] != 0) {
                            break Label_0219;
                        }
                    }
                    else if (c >= '\ud800') {
                        break Label_0219;
                    }
                    int n8 = currentLength;
                    if (array != null) {
                        array[currentLength] = c;
                        n8 = currentLength + 1;
                    }
                    i = inputPtr;
                    currentLength = n8;
                    continue;
                }
                this._inputPtr = inputPtr;
                int n10 = 0;
                int n11 = 0;
                int n12 = 0;
                char c2 = '\0';
                char[] finishCurrentSegment2 = null;
                Label_0800: {
                    if (c <= '\u00ff') {
                        final int n9 = dtd_CHARS[c];
                        if (n9 != 1) {
                            if (n9 != 2) {
                                if (n9 == 3) {
                                    this.markLF();
                                    n10 = currentLength;
                                    n11 = n2;
                                    n12 = n3;
                                    c2 = c;
                                    finishCurrentSegment2 = array;
                                    break Label_0800;
                                }
                                switch (n9) {
                                    default: {
                                        n10 = currentLength;
                                        n11 = n2;
                                        n12 = n3;
                                        c2 = c;
                                        finishCurrentSegment2 = array;
                                        break Label_0800;
                                    }
                                    case 11: {
                                        n10 = currentLength;
                                        n11 = n2;
                                        n12 = n3;
                                        c2 = c;
                                        finishCurrentSegment2 = array;
                                        if (n3 != 0) {
                                            break Label_0800;
                                        }
                                        n10 = currentLength;
                                        n11 = n2;
                                        n12 = n3;
                                        c2 = c;
                                        finishCurrentSegment2 = array;
                                        if (n2 == 0) {
                                            break Block_14;
                                        }
                                        break Label_0800;
                                    }
                                    case 10: {
                                        n10 = currentLength;
                                        n11 = n2;
                                        n12 = n3;
                                        c2 = c;
                                        finishCurrentSegment2 = array;
                                        if (n2 == 0) {
                                            n12 = 0;
                                            n10 = currentLength;
                                            n11 = n2;
                                            c2 = c;
                                            finishCurrentSegment2 = array;
                                        }
                                        break Label_0800;
                                    }
                                    case 9: {
                                        n10 = currentLength;
                                        n11 = n2;
                                        n12 = n3;
                                        c2 = c;
                                        finishCurrentSegment2 = array;
                                        if (n3 == 0) {
                                            n12 = 1;
                                            n10 = currentLength;
                                            n11 = n2;
                                            c2 = c;
                                            finishCurrentSegment2 = array;
                                        }
                                        break Label_0800;
                                    }
                                    case 8: {
                                        if (n2 == 0) {
                                            n11 = c;
                                            n10 = currentLength;
                                            n12 = n3;
                                            c2 = c;
                                            finishCurrentSegment2 = array;
                                            break Label_0800;
                                        }
                                        n10 = currentLength;
                                        n11 = n2;
                                        n12 = n3;
                                        c2 = c;
                                        finishCurrentSegment2 = array;
                                        if (n2 == c) {
                                            n11 = 0;
                                            n10 = currentLength;
                                            n12 = n3;
                                            c2 = c;
                                            finishCurrentSegment2 = array;
                                        }
                                        break Label_0800;
                                    }
                                }
                            }
                        }
                        else {
                            this.handleInvalidXmlChar(c);
                        }
                        if (this._inputPtr >= this._inputEnd) {
                            this.loadMoreGuaranteed();
                        }
                        final char[] inputBuffer2 = this._inputBuffer;
                        final int inputPtr2 = this._inputPtr;
                        if (inputBuffer2[inputPtr2] == '\n') {
                            this._inputPtr = inputPtr2 + 1;
                        }
                        this.markLF();
                        c2 = '\n';
                        n10 = currentLength;
                        n11 = n2;
                        n12 = n3;
                        finishCurrentSegment2 = array;
                    }
                    else {
                        n10 = currentLength;
                        n11 = n2;
                        n12 = n3;
                        c2 = c;
                        finishCurrentSegment2 = array;
                        if (c >= '\ud800') {
                            if (c < '\ue000') {
                                c2 = this.checkSurrogate(c);
                                int n13 = currentLength;
                                if ((finishCurrentSegment2 = array) != null) {
                                    n13 = currentLength + 1;
                                    array[currentLength] = c;
                                    if (n13 >= array.length) {
                                        finishCurrentSegment2 = super._textBuilder.finishCurrentSegment();
                                        n13 = 0;
                                    }
                                    else {
                                        finishCurrentSegment2 = array;
                                    }
                                }
                                n10 = n13;
                                n11 = n2;
                                n12 = n3;
                            }
                            else {
                                n10 = currentLength;
                                n11 = n2;
                                n12 = n3;
                                c2 = c;
                                finishCurrentSegment2 = array;
                                if (c >= '\ufffe') {
                                    c2 = this.handleInvalidXmlChar(c);
                                    finishCurrentSegment2 = array;
                                    n12 = n3;
                                    n11 = n2;
                                    n10 = currentLength;
                                }
                            }
                        }
                    }
                }
                n = n10;
                n2 = n11;
                n3 = n12;
                if ((resetWithEmpty = finishCurrentSegment2) != null) {
                    finishCurrentSegment2[n10] = c2;
                    n = n10 + 1;
                    n2 = n11;
                    n3 = n12;
                    resetWithEmpty = finishCurrentSegment2;
                    continue Block_14;
                }
                continue Block_14;
            }
            this._inputPtr = i;
            n = currentLength;
            resetWithEmpty = array;
        }
        if (array != null) {
            super._textBuilder.setCurrentLength(currentLength);
        }
        final char skipInternalWs = this.skipInternalWs(false, null);
        if (skipInternalWs != '>') {
            this.throwUnexpectedChar(skipInternalWs, " expected '>' after the internal subset");
        }
    }
    
    @Override
    public final void finishPI() {
        final int[] other_CHARS = ReaderScanner.sCharTypes.OTHER_CHARS;
        final char[] inputBuffer = this._inputBuffer;
        char[] resetWithEmpty = super._textBuilder.resetWithEmpty();
        int currentLength = 0;
        int inputPtr2 = 0;
    Block_12:
        while (true) {
            int n;
            if ((n = this._inputPtr) >= this._inputEnd) {
                this.loadMoreGuaranteed();
                n = this._inputPtr;
            }
            char[] array = resetWithEmpty;
            int n2;
            if ((n2 = currentLength) >= resetWithEmpty.length) {
                array = super._textBuilder.finishCurrentSegment();
                n2 = 0;
            }
            final int inputEnd = this._inputEnd;
            final int n3 = array.length - n2 + n;
            currentLength = n2;
            int i = n;
            int n4;
            if (n3 < (n4 = inputEnd)) {
                n4 = n3;
                i = n;
                currentLength = n2;
            }
            while (i < n4) {
                final int inputPtr = i + 1;
                final char c = inputBuffer[i];
                Label_0168: {
                    if (c <= '\u00ff') {
                        if (other_CHARS[c] != 0) {
                            break Label_0168;
                        }
                    }
                    else if (c >= '\ud800') {
                        break Label_0168;
                    }
                    array[currentLength] = c;
                    i = inputPtr;
                    ++currentLength;
                    continue;
                }
                this._inputPtr = inputPtr;
                int n6;
                char c2;
                if (c <= '\u00ff') {
                    final int n5 = other_CHARS[c];
                    if (n5 != 2) {
                        if (n5 != 3) {
                            if (n5 != 12) {
                                resetWithEmpty = array;
                                n6 = currentLength;
                                c2 = c;
                            }
                            else {
                                if (this._inputPtr >= this._inputEnd) {
                                    this.loadMoreGuaranteed();
                                }
                                final char[] inputBuffer2 = this._inputBuffer;
                                inputPtr2 = this._inputPtr;
                                resetWithEmpty = array;
                                n6 = currentLength;
                                c2 = c;
                                if (inputBuffer2[inputPtr2] == '>') {
                                    break Block_12;
                                }
                            }
                        }
                        else {
                            this.markLF();
                            resetWithEmpty = array;
                            n6 = currentLength;
                            c2 = c;
                        }
                    }
                    else {
                        if (this._inputPtr >= this._inputEnd) {
                            this.loadMoreGuaranteed();
                        }
                        final int inputPtr3 = this._inputPtr;
                        if (inputBuffer[inputPtr3] == '\n') {
                            this._inputPtr = inputPtr3 + 1;
                        }
                        this.markLF();
                        c2 = '\n';
                        resetWithEmpty = array;
                        n6 = currentLength;
                    }
                }
                else {
                    resetWithEmpty = array;
                    n6 = currentLength;
                    if ((c2 = c) >= '\ud800') {
                        if (c < '\ue000') {
                            c2 = this.checkSurrogate(c);
                            final int n7 = currentLength + 1;
                            array[currentLength] = c;
                            int n8;
                            if (n7 >= array.length) {
                                array = super._textBuilder.finishCurrentSegment();
                                n8 = 0;
                            }
                            else {
                                n8 = n7;
                            }
                            resetWithEmpty = array;
                            n6 = n8;
                        }
                        else {
                            resetWithEmpty = array;
                            n6 = currentLength;
                            if ((c2 = c) >= '\ufffe') {
                                c2 = this.handleInvalidXmlChar(c);
                                n6 = currentLength;
                                resetWithEmpty = array;
                            }
                        }
                    }
                }
                resetWithEmpty[n6] = c2;
                currentLength = n6 + 1;
                continue Block_12;
            }
            this._inputPtr = i;
            resetWithEmpty = array;
        }
        this._inputPtr = inputPtr2 + 1;
        super._textBuilder.setCurrentLength(currentLength);
    }
    
    @Override
    public final void finishSpace() {
        final char c = (char)this.mTmpChar;
        final int n = 0;
        char[] array;
        int checkPrologIndentation;
        if (c != '\r' && c != '\n') {
            array = super._textBuilder.resetWithEmpty();
            array[0] = c;
            checkPrologIndentation = 1;
        }
        else {
            checkPrologIndentation = this.checkPrologIndentation(c);
            if (checkPrologIndentation < 0) {
                return;
            }
            array = super._textBuilder.getBufferWithoutReset();
        }
        int inputPtr = this._inputPtr;
        while (true) {
            int inputPtr2 = inputPtr;
            if (inputPtr >= this._inputEnd) {
                if (!this.loadMore()) {
                    break;
                }
                inputPtr2 = this._inputPtr;
            }
            final char c2 = this._inputBuffer[inputPtr2];
            if (c2 > ' ') {
                inputPtr = inputPtr2;
                break;
            }
            inputPtr = inputPtr2 + 1;
            int n2;
            char c3;
            if (c2 == '\n') {
                this.markLF(inputPtr);
                n2 = inputPtr;
                c3 = c2;
            }
            else if (c2 == '\r') {
                int inputPtr3;
                if ((inputPtr3 = inputPtr) >= this._inputEnd) {
                    if (!this.loadMore()) {
                        if (checkPrologIndentation >= array.length) {
                            array = super._textBuilder.finishCurrentSegment();
                            checkPrologIndentation = n;
                        }
                        final int n3 = checkPrologIndentation + 1;
                        array[checkPrologIndentation] = '\n';
                        checkPrologIndentation = n3;
                        break;
                    }
                    inputPtr3 = this._inputPtr;
                }
                int n4 = inputPtr3;
                if (this._inputBuffer[inputPtr3] == '\n') {
                    n4 = inputPtr3 + 1;
                }
                this.markLF(n4);
                c3 = '\n';
                n2 = n4;
            }
            else {
                n2 = inputPtr;
                if ((c3 = c2) != ' ') {
                    n2 = inputPtr;
                    if ((c3 = c2) != '\t') {
                        this._inputPtr = inputPtr;
                        this.throwInvalidSpace(c2);
                        c3 = c2;
                        n2 = inputPtr;
                    }
                }
            }
            int n5 = checkPrologIndentation;
            char[] finishCurrentSegment = array;
            if (checkPrologIndentation >= array.length) {
                finishCurrentSegment = super._textBuilder.finishCurrentSegment();
                n5 = 0;
            }
            finishCurrentSegment[n5] = c3;
            checkPrologIndentation = n5 + 1;
            array = finishCurrentSegment;
            inputPtr = n2;
        }
        this._inputPtr = inputPtr;
        super._textBuilder.setCurrentLength(checkPrologIndentation);
    }
    
    @Override
    public final void finishToken() {
        super._tokenIncomplete = false;
        final int currToken = super._currToken;
        if (currToken != 3) {
            if (currToken != 4) {
                if (currToken != 5) {
                    if (currToken != 6) {
                        if (currToken != 11) {
                            if (currToken != 12) {
                                ErrorConsts.throwInternalError();
                            }
                            else {
                                this.finishCData();
                            }
                        }
                        else {
                            this.finishDTD(true);
                        }
                    }
                    else {
                        this.finishSpace();
                    }
                }
                else {
                    this.finishComment();
                }
            }
            else {
                this.finishCharacters();
            }
        }
        else {
            this.finishPI();
        }
    }
    
    @Override
    public int getCurrentColumnNr() {
        return this._inputPtr - super._rowStartOffset;
    }
    
    @Override
    public XMLStreamLocation2 getCurrentLocation() {
        final String publicId = super._config.getPublicId();
        final String systemId = super._config.getSystemId();
        final long pastBytesOrChars = super._pastBytesOrChars;
        final int inputPtr = this._inputPtr;
        return (XMLStreamLocation2)LocationImpl.fromZeroBased(publicId, systemId, pastBytesOrChars + inputPtr, super._currRow, inputPtr - super._rowStartOffset);
    }
    
    @Override
    public long getEndingByteOffset() {
        return -1L;
    }
    
    @Override
    public long getEndingCharOffset() {
        if (super._tokenIncomplete) {
            this.finishToken();
        }
        return super._pastBytesOrChars + this._inputPtr;
    }
    
    @Override
    public long getStartingByteOffset() {
        return -1L;
    }
    
    @Override
    public long getStartingCharOffset() {
        return super._startRawOffset;
    }
    
    public final int handleCharEntity() {
        if (this._inputPtr >= this._inputEnd) {
            this.loadMoreGuaranteed();
        }
        final char c = this._inputBuffer[this._inputPtr++];
        int n = 0;
        final int n2 = 0;
        char c2;
        int n8;
        if ((c2 = c) == 'x') {
            int n3 = n2;
            while (true) {
                if (this._inputPtr >= this._inputEnd) {
                    this.loadMoreGuaranteed();
                }
                final char c3 = this._inputBuffer[this._inputPtr++];
                if (c3 == ';') {
                    break;
                }
                final int n4 = n3 << 4;
                int n6 = 0;
                Label_0128: {
                    int n5;
                    if (c3 <= '9' && c3 >= '0') {
                        n5 = c3 - '0';
                    }
                    else {
                        if (c3 >= 'a' && c3 <= 'f') {
                            n5 = c3 - 'a';
                        }
                        else {
                            if (c3 < 'A' || c3 > 'F') {
                                this.throwUnexpectedChar(c3, "; expected a hex digit (0-9a-fA-F)");
                                n6 = n4;
                                break Label_0128;
                            }
                            n5 = c3 - 'A';
                        }
                        n5 += 10;
                    }
                    n6 = n4 + n5;
                }
                final int n7 = n3 = n6;
                if (n7 <= 1114111) {
                    continue;
                }
                this.reportEntityOverflow();
                n3 = n7;
            }
            n8 = n3;
        }
        else {
            while (true) {
                n8 = n;
                if (c2 == ';') {
                    break;
                }
                if (c2 <= '9' && c2 >= '0') {
                    final int n9 = n * 10 + (c2 - '0');
                    if ((n = n9) > 1114111) {
                        this.reportEntityOverflow();
                        n = n9;
                    }
                }
                else {
                    this.throwUnexpectedChar(c2, "; expected a decimal number");
                }
                if (this._inputPtr >= this._inputEnd) {
                    this.loadMoreGuaranteed();
                }
                c2 = this._inputBuffer[this._inputPtr++];
            }
        }
        if (n8 >= 55296) {
            if (n8 < 57344) {
                this.reportInvalidXmlChar(n8);
            }
            if (n8 != 65534 && n8 != 65535) {
                return n8;
            }
        }
        else {
            if (n8 >= 32 || n8 == 10 || n8 == 13 || n8 == 9) {
                return n8;
            }
            if (super._xml11) {
                if (n8 != 0) {
                    return n8;
                }
            }
        }
        this.reportInvalidXmlChar(n8);
        return n8;
    }
    
    public final int handleCommentOrCdataStart() {
        if (this._inputPtr >= this._inputEnd) {
            this.loadMoreGuaranteed();
        }
        final char[] inputBuffer = this._inputBuffer;
        final int inputPtr = this._inputPtr;
        final int inputPtr2 = inputPtr + 1;
        this._inputPtr = inputPtr2;
        final char c = inputBuffer[inputPtr];
        if (c == '-') {
            if (inputPtr2 >= this._inputEnd) {
                this.loadMoreGuaranteed();
            }
            final char c2 = this._inputBuffer[this._inputPtr++];
            if (c2 != '-') {
                this.reportTreeUnexpChar(c2, " (expected '-' for COMMENT)");
            }
            if (super._cfgLazyParsing) {
                super._tokenIncomplete = true;
            }
            else {
                this.finishComment();
            }
            return super._currToken = 5;
        }
        if (c == '[') {
            super._currToken = 12;
            for (int i = 0; i < 6; ++i) {
                if (this._inputPtr >= this._inputEnd) {
                    this.loadMoreGuaranteed();
                }
                final char c3 = this._inputBuffer[this._inputPtr++];
                if (c3 != "CDATA[".charAt(i)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(" (expected '");
                    sb.append("CDATA[".charAt(i));
                    sb.append("' for CDATA section)");
                    this.reportTreeUnexpChar(c3, sb.toString());
                }
            }
            if (super._cfgLazyParsing) {
                super._tokenIncomplete = true;
            }
            else {
                this.finishCData();
            }
            return 12;
        }
        this.reportTreeUnexpChar(c, " (expected either '-' for COMMENT or '[CDATA[' for CDATA section)");
        return -1;
    }
    
    public final int handleEndElement() {
        --super._depth;
        super._currToken = 2;
        final PName name = super._currElem.getName();
        super._tokenName = name;
        final String prefixedName = name.getPrefixedName();
        final int length = prefixedName.length();
        int index = 0;
        do {
            if (this._inputPtr >= this._inputEnd) {
                this.loadMoreGuaranteed();
            }
            if (this._inputBuffer[this._inputPtr++] != prefixedName.charAt(index)) {
                this.reportUnexpectedEndTag(prefixedName);
            }
        } while (++index < length);
        if (this._inputPtr >= this._inputEnd) {
            this.loadMoreGuaranteed();
        }
        final char c = this._inputBuffer[this._inputPtr++];
        char skipInternalWs = '\0';
        Label_0193: {
            if (c <= ' ') {
                skipInternalWs = this.skipInternalWs(false, null);
            }
            else if ((skipInternalWs = c) != '>') {
                if (c != ':') {
                    skipInternalWs = c;
                    if (!XmlChars.is10NameChar(c)) {
                        break Label_0193;
                    }
                }
                this.reportUnexpectedEndTag(prefixedName);
                skipInternalWs = c;
            }
        }
        if (skipInternalWs != '>') {
            this.throwUnexpectedChar(skipInternalWs, " expected space or closing '>'");
        }
        return 2;
    }
    
    public final int handleEntityInText(final boolean b) {
        if (this._inputPtr >= this._inputEnd) {
            this.loadMoreGuaranteed();
        }
        final char[] inputBuffer = this._inputBuffer;
        final int inputPtr = this._inputPtr;
        final int inputPtr2 = inputPtr + 1;
        this._inputPtr = inputPtr2;
        char c = inputBuffer[inputPtr];
        if (c == '#') {
            return this.handleCharEntity();
        }
        String s;
        if (c == 'a') {
            if (inputPtr2 >= this._inputEnd) {
                this.loadMoreGuaranteed();
            }
            final char[] inputBuffer2 = this._inputBuffer;
            final int inputPtr3 = this._inputPtr;
            final int inputPtr4 = inputPtr3 + 1;
            this._inputPtr = inputPtr4;
            c = inputBuffer2[inputPtr3];
            if (c == 'm') {
                if (inputPtr4 >= this._inputEnd) {
                    this.loadMoreGuaranteed();
                }
                final char[] inputBuffer3 = this._inputBuffer;
                final int inputPtr5 = this._inputPtr;
                final int inputPtr6 = inputPtr5 + 1;
                this._inputPtr = inputPtr6;
                c = inputBuffer3[inputPtr5];
                if (c == 'p') {
                    if (inputPtr6 >= this._inputEnd) {
                        this.loadMoreGuaranteed();
                    }
                    c = this._inputBuffer[this._inputPtr++];
                    if (c == ';') {
                        return 38;
                    }
                    s = "amp";
                }
                else {
                    s = "am";
                }
            }
            else if (c == 'p') {
                if (inputPtr4 >= this._inputEnd) {
                    this.loadMoreGuaranteed();
                }
                final char[] inputBuffer4 = this._inputBuffer;
                final int inputPtr7 = this._inputPtr;
                final int inputPtr8 = inputPtr7 + 1;
                this._inputPtr = inputPtr8;
                c = inputBuffer4[inputPtr7];
                if (c == 'o') {
                    if (inputPtr8 >= this._inputEnd) {
                        this.loadMoreGuaranteed();
                    }
                    final char[] inputBuffer5 = this._inputBuffer;
                    final int inputPtr9 = this._inputPtr;
                    final int inputPtr10 = inputPtr9 + 1;
                    this._inputPtr = inputPtr10;
                    c = inputBuffer5[inputPtr9];
                    if (c == 's') {
                        if (inputPtr10 >= this._inputEnd) {
                            this.loadMoreGuaranteed();
                        }
                        c = this._inputBuffer[this._inputPtr++];
                        if (c == ';') {
                            return 39;
                        }
                        s = "apos";
                    }
                    else {
                        s = "apo";
                    }
                }
                else {
                    s = "ap";
                }
            }
            else {
                s = "a";
            }
        }
        else if (c == 'l') {
            if (inputPtr2 >= this._inputEnd) {
                this.loadMoreGuaranteed();
            }
            final char[] inputBuffer6 = this._inputBuffer;
            final int inputPtr11 = this._inputPtr;
            final int inputPtr12 = inputPtr11 + 1;
            this._inputPtr = inputPtr12;
            c = inputBuffer6[inputPtr11];
            if (c == 't') {
                if (inputPtr12 >= this._inputEnd) {
                    this.loadMoreGuaranteed();
                }
                c = this._inputBuffer[this._inputPtr++];
                if (c == ';') {
                    return 60;
                }
                s = "lt";
            }
            else {
                s = "l";
            }
        }
        else if (c == 'g') {
            if (inputPtr2 >= this._inputEnd) {
                this.loadMoreGuaranteed();
            }
            final char[] inputBuffer7 = this._inputBuffer;
            final int inputPtr13 = this._inputPtr;
            final int inputPtr14 = inputPtr13 + 1;
            this._inputPtr = inputPtr14;
            c = inputBuffer7[inputPtr13];
            if (c == 't') {
                if (inputPtr14 >= this._inputEnd) {
                    this.loadMoreGuaranteed();
                }
                c = this._inputBuffer[this._inputPtr++];
                if (c == ';') {
                    return 62;
                }
                s = "gt";
            }
            else {
                s = "g";
            }
        }
        else if (c == 'q') {
            if (inputPtr2 >= this._inputEnd) {
                this.loadMoreGuaranteed();
            }
            final char[] inputBuffer8 = this._inputBuffer;
            final int inputPtr15 = this._inputPtr;
            final int inputPtr16 = inputPtr15 + 1;
            this._inputPtr = inputPtr16;
            c = inputBuffer8[inputPtr15];
            if (c == 'u') {
                if (inputPtr16 >= this._inputEnd) {
                    this.loadMoreGuaranteed();
                }
                final char[] inputBuffer9 = this._inputBuffer;
                final int inputPtr17 = this._inputPtr;
                final int inputPtr18 = inputPtr17 + 1;
                this._inputPtr = inputPtr18;
                c = inputBuffer9[inputPtr17];
                if (c == 'o') {
                    if (inputPtr18 >= this._inputEnd) {
                        this.loadMoreGuaranteed();
                    }
                    final char[] inputBuffer10 = this._inputBuffer;
                    final int inputPtr19 = this._inputPtr;
                    final int inputPtr20 = inputPtr19 + 1;
                    this._inputPtr = inputPtr20;
                    c = inputBuffer10[inputPtr19];
                    if (c == 't') {
                        if (inputPtr20 >= this._inputEnd) {
                            this.loadMoreGuaranteed();
                        }
                        c = this._inputBuffer[this._inputPtr++];
                        if (c == ';') {
                            return 34;
                        }
                        s = "quot";
                    }
                    else {
                        s = "quo";
                    }
                }
                else {
                    s = "qu";
                }
            }
            else {
                s = "q";
            }
        }
        else {
            s = "";
        }
        final int[] name_CHARS = ReaderScanner.sCharTypes.NAME_CHARS;
        final char[] nameBuffer = super._nameBuffer;
        final int length = s.length();
        int index = 0;
        char c2;
        char[] growArrayBy;
        int count;
        while (true) {
            c2 = c;
            growArrayBy = nameBuffer;
            count = index;
            if (index >= length) {
                break;
            }
            nameBuffer[index] = s.charAt(index);
            ++index;
        }
        while (c2 != ';') {
            boolean b2 = true;
            char handleInvalidXmlChar = '\0';
            char[] growArrayBy2 = null;
            int n2 = 0;
            Label_1148: {
                if (c2 <= '\u00ff') {
                    final int n = name_CHARS[c2];
                    if (n != 0 && n != 1 && n != 2) {
                        handleInvalidXmlChar = c2;
                        growArrayBy2 = growArrayBy;
                        n2 = count;
                        if (n == 3) {
                            break Label_1148;
                        }
                        handleInvalidXmlChar = c2;
                    }
                    else {
                        handleInvalidXmlChar = c2;
                        if (count > 0) {
                            handleInvalidXmlChar = c2;
                            growArrayBy2 = growArrayBy;
                            n2 = count;
                            break Label_1148;
                        }
                    }
                }
                else {
                    if (c2 < '\ue000') {
                        final int decodeSurrogate = this.decodeSurrogate(c2);
                        growArrayBy2 = growArrayBy;
                        if (count >= growArrayBy.length) {
                            growArrayBy2 = DataUtil.growArrayBy(growArrayBy, growArrayBy.length);
                            super._nameBuffer = growArrayBy2;
                        }
                        n2 = count + 1;
                        growArrayBy2[count] = c2;
                        handleInvalidXmlChar = this._inputBuffer[this._inputPtr - 1];
                        if (n2 == 0) {
                            b2 = XmlChars.is10NameStartChar(decodeSurrogate);
                        }
                        else {
                            b2 = XmlChars.is10NameChar(decodeSurrogate);
                        }
                        break Label_1148;
                    }
                    handleInvalidXmlChar = c2;
                    growArrayBy2 = growArrayBy;
                    n2 = count;
                    if (c2 < '\ufffe') {
                        break Label_1148;
                    }
                    handleInvalidXmlChar = this.handleInvalidXmlChar(c2);
                }
                b2 = false;
                growArrayBy2 = growArrayBy;
                n2 = count;
            }
            if (!b2) {
                this.reportInvalidNameChar(handleInvalidXmlChar, n2);
            }
            growArrayBy = growArrayBy2;
            if (n2 >= growArrayBy2.length) {
                growArrayBy = DataUtil.growArrayBy(growArrayBy2, growArrayBy2.length);
                super._nameBuffer = growArrayBy;
            }
            growArrayBy[n2] = handleInvalidXmlChar;
            if (this._inputPtr >= this._inputEnd) {
                this.loadMoreGuaranteed();
            }
            c2 = this._inputBuffer[this._inputPtr++];
            count = n2 + 1;
        }
        final String s2 = new String(growArrayBy, 0, count);
        super._tokenName = new PNameC(s2, null, s2, 0);
        if (super._config.willExpandEntities()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("General entity reference (&");
            sb.append(s2);
            sb.append(";) encountered in entity expanding mode: operation not (yet) implemented");
            this.reportInputProblem(sb.toString());
        }
        if (b) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("General entity reference (&");
            sb2.append(s2);
            sb2.append(";) encountered in attribute value, in non-entity-expanding mode: no way to handle it");
            this.reportInputProblem(sb2.toString());
        }
        return 0;
    }
    
    public final int handlePIStart() {
        super._currToken = 3;
        if (this._inputPtr >= this._inputEnd) {
            this.loadMoreGuaranteed();
        }
        final PName pName = this.parsePName(this._inputBuffer[this._inputPtr++]);
        super._tokenName = pName;
        final String localName = pName.getLocalName();
        if (localName.length() == 3 && localName.equalsIgnoreCase("xml") && super._tokenName.getPrefix() == null) {
            this.reportInputProblem(ErrorConsts.ERR_WF_PI_XML_TARGET);
        }
        if (this._inputPtr >= this._inputEnd) {
            this.loadMoreGuaranteed();
        }
        char c = this._inputBuffer[this._inputPtr++];
        boolean tokenIncomplete;
        if (c <= ' ') {
            while (true) {
                Label_0223: {
                    if (c != '\n') {
                        if (c == '\r') {
                            if (this._inputPtr >= this._inputEnd) {
                                this.loadMoreGuaranteed();
                            }
                            final char[] inputBuffer = this._inputBuffer;
                            final int inputPtr = this._inputPtr;
                            if (inputBuffer[inputPtr] == '\n') {
                                this._inputPtr = inputPtr + 1;
                            }
                        }
                        else {
                            if (c != ' ' && c != '\t') {
                                this.throwInvalidSpace(c);
                            }
                            break Label_0223;
                        }
                    }
                    this.markLF();
                }
                if (this._inputPtr >= this._inputEnd) {
                    this.loadMoreGuaranteed();
                }
                final char[] inputBuffer2 = this._inputBuffer;
                final int inputPtr2 = this._inputPtr;
                c = inputBuffer2[inputPtr2];
                if (c > ' ') {
                    break;
                }
                this._inputPtr = inputPtr2 + 1;
            }
            if (!super._cfgLazyParsing) {
                this.finishPI();
                return 3;
            }
            tokenIncomplete = true;
        }
        else {
            if (c != '?') {
                this.reportMissingPISpace(c);
            }
            if (this._inputPtr >= this._inputEnd) {
                this.loadMoreGuaranteed();
            }
            final char c2 = this._inputBuffer[this._inputPtr++];
            if (c2 != '>') {
                this.reportMissingPISpace(c2);
            }
            super._textBuilder.resetWithEmpty();
            tokenIncomplete = false;
        }
        super._tokenIncomplete = tokenIncomplete;
        return 3;
    }
    
    public final int handlePrologDeclStart(final boolean b) {
        if (this._inputPtr >= this._inputEnd) {
            this.loadMoreGuaranteed();
        }
        final char[] inputBuffer = this._inputBuffer;
        final int inputPtr = this._inputPtr;
        final int inputPtr2 = inputPtr + 1;
        this._inputPtr = inputPtr2;
        final char c = inputBuffer[inputPtr];
        char c2;
        if (c == '-') {
            if (inputPtr2 >= this._inputEnd) {
                this.loadMoreGuaranteed();
            }
            if ((c2 = this._inputBuffer[this._inputPtr++]) == '-') {
                if (super._cfgLazyParsing) {
                    super._tokenIncomplete = true;
                }
                else {
                    this.finishComment();
                }
                return super._currToken = 5;
            }
        }
        else if ((c2 = c) == 'D') {
            c2 = c;
            if (b) {
                this.handleDtdStart();
                if (!super._cfgLazyParsing && super._tokenIncomplete) {
                    this.finishDTD(true);
                    super._tokenIncomplete = false;
                }
                return 11;
            }
        }
        super._tokenIncomplete = true;
        super._currToken = 4;
        this.reportPrologUnexpChar(b, c2, " (expected '-' for COMMENT)");
        return super._currToken;
    }
    
    public final int handleStartElement(final char c) {
        super._currToken = 1;
        final int n = 0;
        super._currNsCount = 0;
        PName tokenName = this.parsePName(c);
        final String prefix = tokenName.getPrefix();
        int bound;
        if (prefix == null) {
            bound = 1;
        }
        else {
            tokenName = this.bindName(tokenName, prefix);
            bound = (tokenName.isBound() ? 1 : 0);
        }
        super._tokenName = tokenName;
        super._currElem = new ElementScope(tokenName, super._currElem);
        int collectValue = 0;
    Label_0616_Outer:
        while (true) {
            if (this._inputPtr >= this._inputEnd) {
                this.loadMoreGuaranteed();
            }
            final char c2 = this._inputBuffer[this._inputPtr++];
            char c4;
            if (c2 <= ' ') {
                char c3 = c2;
                do {
                    Label_0219: {
                        if (c3 != '\n') {
                            if (c3 == '\r') {
                                if (this._inputPtr >= this._inputEnd) {
                                    this.loadMoreGuaranteed();
                                }
                                final char[] inputBuffer = this._inputBuffer;
                                final int inputPtr = this._inputPtr;
                                if (inputBuffer[inputPtr] == '\n') {
                                    this._inputPtr = inputPtr + 1;
                                }
                            }
                            else {
                                if (c3 != ' ' && c3 != '\t') {
                                    this.throwInvalidSpace(c3);
                                }
                                break Label_0219;
                            }
                        }
                        this.markLF();
                    }
                    if (this._inputPtr >= this._inputEnd) {
                        this.loadMoreGuaranteed();
                    }
                    c4 = this._inputBuffer[this._inputPtr++];
                } while ((c3 = c4) <= ' ');
            }
            else {
                c4 = c2;
                if (c2 != '/' && (c4 = c2) != '>') {
                    this.throwUnexpectedChar(c2, " expected space, or '>' or \"/>\"");
                    c4 = c2;
                }
            }
            if (c4 == '/') {
                if (this._inputPtr >= this._inputEnd) {
                    this.loadMoreGuaranteed();
                }
                final char c5 = this._inputBuffer[this._inputPtr++];
                if (c5 != '>') {
                    this.throwUnexpectedChar(c5, " expected '>'");
                }
                super._isEmptyTag = true;
                break;
            }
            if (c4 == '>') {
                super._isEmptyTag = false;
                break;
            }
            if (c4 == '<') {
                this.reportInputProblem("Unexpected '<' character in element (missing closing '>'?)");
            }
            final PName pName = this.parsePName(c4);
            final String prefix2 = pName.getPrefix();
            while (true) {
                int bound2 = 0;
                Label_0609: {
                    Label_0565: {
                        PName pName2;
                        if (prefix2 == null) {
                            bound2 = bound;
                            pName2 = pName;
                            if (pName.getLocalName() == "xmlns") {
                                break Label_0565;
                            }
                            break Label_0609;
                        }
                        else {
                            if (prefix2 == "xmlns") {
                                break Label_0565;
                            }
                            final PName bindName = this.bindName(pName, prefix2);
                            bound2 = bound;
                            pName2 = bindName;
                            if (bound != 0) {
                                bound2 = (bindName.isBound() ? 1 : 0);
                                pName2 = bindName;
                            }
                            break Label_0609;
                        }
                        char c6;
                        while (true) {
                            if (this._inputPtr >= this._inputEnd) {
                                this.loadMoreGuaranteed();
                            }
                            final char[] inputBuffer2 = this._inputBuffer;
                            final int inputPtr2 = this._inputPtr;
                            final int inputPtr3 = inputPtr2 + 1;
                            this._inputPtr = inputPtr3;
                            c6 = inputBuffer2[inputPtr2];
                            if (c6 > ' ') {
                                break;
                            }
                            if (c6 != '\n') {
                                if (c6 == '\r') {
                                    if (inputPtr3 >= this._inputEnd) {
                                        this.loadMoreGuaranteed();
                                    }
                                    final char[] inputBuffer3 = this._inputBuffer;
                                    final int inputPtr4 = this._inputPtr;
                                    if (inputBuffer3[inputPtr4] == '\n') {
                                        this._inputPtr = inputPtr4 + 1;
                                    }
                                }
                                else {
                                    if (c6 != ' ' && c6 != '\t') {
                                        this.throwInvalidSpace(c6);
                                        continue Label_0616_Outer;
                                    }
                                    continue Label_0616_Outer;
                                }
                            }
                            this.markLF();
                        }
                        if (c6 != '=') {
                            this.throwUnexpectedChar(c6, " expected '='");
                        }
                        char c7;
                        while (true) {
                            if (this._inputPtr >= this._inputEnd) {
                                this.loadMoreGuaranteed();
                            }
                            final char[] inputBuffer4 = this._inputBuffer;
                            final int inputPtr5 = this._inputPtr;
                            final int inputPtr6 = inputPtr5 + 1;
                            this._inputPtr = inputPtr6;
                            c7 = inputBuffer4[inputPtr5];
                            if (c7 > ' ') {
                                break;
                            }
                            if (c7 != '\n') {
                                if (c7 == '\r') {
                                    if (inputPtr6 >= this._inputEnd) {
                                        this.loadMoreGuaranteed();
                                    }
                                    final char[] inputBuffer5 = this._inputBuffer;
                                    final int inputPtr7 = this._inputPtr;
                                    if (inputBuffer5[inputPtr7] == '\n') {
                                        this._inputPtr = inputPtr7 + 1;
                                    }
                                }
                                else {
                                    if (c7 != ' ' && c7 != '\t') {
                                        this.throwInvalidSpace(c7);
                                        continue Label_0616_Outer;
                                    }
                                    continue Label_0616_Outer;
                                }
                            }
                            this.markLF();
                        }
                        if (c7 != '\"' && c7 != '\'') {
                            this.throwUnexpectedChar(c7, " Expected a quote");
                        }
                        final boolean b;
                        if (b) {
                            this.handleNsDeclaration(pName2, c7);
                            ++super._currNsCount;
                            continue Label_0616_Outer;
                        }
                        collectValue = this.collectValue(collectValue, c7, pName2);
                        continue Label_0616_Outer;
                    }
                    final boolean b = true;
                    PName pName2 = pName;
                    continue;
                }
                final boolean b = false;
                bound = bound2;
                continue;
            }
        }
        int attrCount;
        if ((attrCount = super._attrCollector.finishLastValue(collectValue)) < 0) {
            attrCount = super._attrCollector.getCount();
            this.reportInputProblem(super._attrCollector.getErrorMsg());
        }
        super._attrCount = attrCount;
        ++super._depth;
        if (bound == 0) {
            if (!tokenName.isBound()) {
                this.reportUnboundPrefix(super._tokenName, false);
            }
            for (int attrCount2 = super._attrCount, i = n; i < attrCount2; ++i) {
                final PName name = super._attrCollector.getName(i);
                if (!name.isBound()) {
                    this.reportUnboundPrefix(name, true);
                }
            }
        }
        return 1;
    }
    
    public final boolean loadAndRetain(final int n) {
        if (this._in == null) {
            return false;
        }
        final long pastBytesOrChars = super._pastBytesOrChars;
        final int inputPtr = this._inputPtr;
        super._pastBytesOrChars = pastBytesOrChars + inputPtr;
        super._rowStartOffset -= inputPtr;
        final int inputEnd = this._inputEnd - inputPtr;
        final char[] inputBuffer = this._inputBuffer;
        System.arraycopy(inputBuffer, inputPtr, inputBuffer, 0, inputEnd);
        this._inputPtr = 0;
        this._inputEnd = inputEnd;
        try {
            int read;
            do {
                final char[] inputBuffer2 = this._inputBuffer;
                final int length = inputBuffer2.length;
                final int inputEnd2 = this._inputEnd;
                final int i = length - inputEnd2;
                read = this._in.read(inputBuffer2, inputEnd2, i);
                if (read < 1) {
                    if (read == 0) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Reader returned 0 bytes, even when asked to read up to ");
                        sb.append(i);
                        this.reportInputProblem(sb.toString());
                    }
                    return false;
                }
            } while ((this._inputEnd += read) < n);
            return true;
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    @Override
    public final boolean loadMore() {
        final Reader in = this._in;
        if (in == null) {
            this._inputEnd = 0;
            return false;
        }
        final long pastBytesOrChars = super._pastBytesOrChars;
        final int inputEnd = this._inputEnd;
        super._pastBytesOrChars = pastBytesOrChars + inputEnd;
        super._rowStartOffset -= inputEnd;
        this._inputPtr = 0;
        try {
            final char[] inputBuffer = this._inputBuffer;
            final int read = in.read(inputBuffer, 0, inputBuffer.length);
            if (read < 1) {
                this._inputEnd = 0;
                if (read == 0) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Reader returned 0 bytes, even when asked to read up to ");
                    sb.append(this._inputBuffer.length);
                    this.reportInputProblem(sb.toString());
                }
                return false;
            }
            this._inputEnd = read;
            return true;
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    public final char loadOne() {
        if (!this.loadMore()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unexpected end-of-input when trying to parse ");
            sb.append(ErrorConsts.tokenTypeDesc(super._currToken));
            this.reportInputProblem(sb.toString());
        }
        return this._inputBuffer[this._inputPtr++];
    }
    
    public final char loadOne(int n) {
        if (!this.loadMore()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unexpected end-of-input when trying to parse ");
            sb.append(ErrorConsts.tokenTypeDesc(n));
            this.reportInputProblem(sb.toString());
        }
        final char[] inputBuffer = this._inputBuffer;
        n = this._inputPtr++;
        return inputBuffer[n];
    }
    
    public final void markLF() {
        super._rowStartOffset = this._inputPtr;
        ++super._currRow;
    }
    
    public final void markLF(final int rowStartOffset) {
        super._rowStartOffset = rowStartOffset;
        ++super._currRow;
    }
    
    @Override
    public final int nextFromProlog(final boolean b) {
        if (super._tokenIncomplete) {
            this.skipToken();
        }
        this.setStartLocation();
        while (this._inputPtr < this._inputEnd || this.loadMore()) {
            final char[] inputBuffer = this._inputBuffer;
            final int inputPtr = this._inputPtr;
            final int inputPtr2 = inputPtr + 1;
            this._inputPtr = inputPtr2;
            final int n = inputBuffer[inputPtr] & '\u00ff';
            if (n == 60) {
                if (inputPtr2 >= this._inputEnd) {
                    this.loadMoreGuaranteed(5);
                }
                final char c = this._inputBuffer[this._inputPtr++];
                if (c == '!') {
                    return this.handlePrologDeclStart(b);
                }
                if (c == '?') {
                    return this.handlePIStart();
                }
                if (c == '/' || !b) {
                    this.reportPrologUnexpElement(b, c);
                }
                return this.handleStartElement(c);
            }
            else {
                if (n == 32) {
                    continue;
                }
                if (n != 10) {
                    if (n == 13) {
                        if (inputPtr2 >= this._inputEnd && !this.loadMore()) {
                            this.markLF();
                            this.setStartLocation();
                            return -1;
                        }
                        final char[] inputBuffer2 = this._inputBuffer;
                        final int inputPtr3 = this._inputPtr;
                        if (inputBuffer2[inputPtr3] == '\n') {
                            this._inputPtr = inputPtr3 + 1;
                        }
                    }
                    else {
                        if (n != 9) {
                            this.reportPrologUnexpChar(b, n, null);
                            continue;
                        }
                        continue;
                    }
                }
                this.markLF();
            }
        }
        this.setStartLocation();
        return -1;
    }
    
    @Override
    public final int nextFromTree() {
        if (super._tokenIncomplete) {
            if (this.skipToken()) {
                return this._nextEntity();
            }
        }
        else {
            final int currToken = super._currToken;
            if (currToken == 1) {
                if (super._isEmptyTag) {
                    --super._depth;
                    return super._currToken = 2;
                }
            }
            else if (currToken == 2) {
                super._currElem = super._currElem.getParent();
                while (true) {
                    final NsDeclaration lastNsDecl = super._lastNsDecl;
                    if (lastNsDecl == null || lastNsDecl.getLevel() < super._depth) {
                        break;
                    }
                    super._lastNsDecl = super._lastNsDecl.unbind();
                }
            }
            else if (super._entityPending) {
                super._entityPending = false;
                return this._nextEntity();
            }
        }
        this.setStartLocation();
        if (this._inputPtr >= this._inputEnd && !this.loadMore()) {
            this.setStartLocation();
            return -1;
        }
        final char[] inputBuffer = this._inputBuffer;
        final int inputPtr = this._inputPtr;
        final char mTmpChar = inputBuffer[inputPtr];
        if (mTmpChar != '<') {
            if (mTmpChar == '&') {
                this._inputPtr = inputPtr + 1;
                final int handleEntityInText = this.handleEntityInText(false);
                if (handleEntityInText == 0) {
                    return super._currToken = 9;
                }
                this.mTmpChar = -handleEntityInText;
            }
            else {
                this.mTmpChar = mTmpChar;
            }
            if (super._cfgLazyParsing) {
                super._tokenIncomplete = true;
            }
            else {
                this.finishCharacters();
            }
            return super._currToken = 4;
        }
        final int inputPtr2 = inputPtr + 1;
        char loadOne;
        if ((this._inputPtr = inputPtr2) < this._inputEnd) {
            this._inputPtr = inputPtr2 + 1;
            loadOne = inputBuffer[inputPtr2];
        }
        else {
            loadOne = this.loadOne(5);
        }
        if (loadOne == '!') {
            return this.handleCommentOrCdataStart();
        }
        if (loadOne == '?') {
            return this.handlePIStart();
        }
        if (loadOne == '/') {
            return this.handleEndElement();
        }
        return this.handleStartElement(loadOne);
    }
    
    public PName parsePName(final char c) {
        char[] nameBuffer = super._nameBuffer;
        if (c < 'A') {
            this.throwUnexpectedChar(c, "; expected a name start character");
        }
        nameBuffer[0] = c;
        int n = 1;
        int n2 = c;
        while (true) {
            if (this._inputPtr >= this._inputEnd) {
                this.loadMoreGuaranteed();
            }
            final char[] inputBuffer = this._inputBuffer;
            final int inputPtr = this._inputPtr;
            final char c2 = inputBuffer[inputPtr];
            if (c2 < 'A' && (c2 < '-' || c2 > ':' || c2 == '/')) {
                break;
            }
            this._inputPtr = inputPtr + 1;
            char[] growArrayBy = nameBuffer;
            if (n >= nameBuffer.length) {
                growArrayBy = DataUtil.growArrayBy(nameBuffer, nameBuffer.length);
                super._nameBuffer = growArrayBy;
            }
            growArrayBy[n] = c2;
            n2 = n2 * 31 + c2;
            ++n;
            nameBuffer = growArrayBy;
        }
        PName pName;
        if ((pName = this._symbols.findSymbol(nameBuffer, 0, n, n2)) == null) {
            pName = this.addPName(nameBuffer, n, n2);
        }
        return pName;
    }
    
    public String parsePublicId(final char c) {
        char[] array = super._nameBuffer;
        final int[] pubid_CHARS = XmlCharTypes.PUBID_CHARS;
        int count = 0;
        int n = 0;
        while (true) {
            if (this._inputPtr >= this._inputEnd) {
                this.loadMoreGuaranteed();
            }
            final char c2 = this._inputBuffer[this._inputPtr++];
            if (c2 == c) {
                break;
            }
            if (c2 > '\u00ff' || pubid_CHARS[c2] != 1) {
                this.throwUnexpectedChar(c2, " in public identifier");
            }
            if (c2 <= ' ') {
                n = 1;
            }
            else {
                char[] finishCurrentSegment = array;
                int n2 = count;
                int n3;
                if ((n3 = n) != 0) {
                    finishCurrentSegment = array;
                    int n4;
                    if ((n4 = count) >= array.length) {
                        finishCurrentSegment = super._textBuilder.finishCurrentSegment();
                        n4 = 0;
                    }
                    finishCurrentSegment[n4] = ' ';
                    n2 = n4 + 1;
                    n3 = 0;
                }
                array = finishCurrentSegment;
                if ((count = n2) >= finishCurrentSegment.length) {
                    array = DataUtil.growArrayBy(finishCurrentSegment, finishCurrentSegment.length);
                    super._nameBuffer = array;
                    count = 0;
                }
                array[count] = c2;
                ++count;
                n = n3;
            }
        }
        return new String(array, 0, count);
    }
    
    public String parseSystemId(final char c) {
        char[] nameBuffer = super._nameBuffer;
        final int[] attr_CHARS = ReaderScanner.sCharTypes.ATTR_CHARS;
        int count = 0;
        while (true) {
            if (this._inputPtr >= this._inputEnd) {
                this.loadMoreGuaranteed();
            }
            final char c2 = this._inputBuffer[this._inputPtr++];
            final int n = attr_CHARS[c2];
            char c3 = c2;
            Label_0188: {
                if (n != 0) {
                    if (n != 1) {
                        if (n != 2) {
                            if (n == 3) {
                                this.markLF();
                                c3 = c2;
                                break Label_0188;
                            }
                            if (n != 14) {
                                c3 = c2;
                                break Label_0188;
                            }
                            if ((c3 = c2) == c) {
                                break;
                            }
                            break Label_0188;
                        }
                    }
                    else {
                        this.handleInvalidXmlChar(c2);
                    }
                    if (this._inputPtr >= this._inputEnd) {
                        this.loadMoreGuaranteed();
                    }
                    final char[] inputBuffer = this._inputBuffer;
                    final int inputPtr = this._inputPtr;
                    if (inputBuffer[inputPtr] == '\n') {
                        this._inputPtr = inputPtr + 1;
                    }
                    this.markLF();
                    c3 = '\n';
                }
            }
            char[] growArrayBy = nameBuffer;
            int n2;
            if ((n2 = count) >= nameBuffer.length) {
                growArrayBy = DataUtil.growArrayBy(nameBuffer, nameBuffer.length);
                super._nameBuffer = growArrayBy;
                n2 = 0;
            }
            growArrayBy[n2] = c3;
            count = n2 + 1;
            nameBuffer = growArrayBy;
        }
        return new String(nameBuffer, 0, count);
    }
    
    public final void setStartLocation() {
        final long pastBytesOrChars = super._pastBytesOrChars;
        final int inputPtr = this._inputPtr;
        super._startRawOffset = pastBytesOrChars + inputPtr;
        super._startRow = super._currRow;
        super._startColumn = inputPtr - super._rowStartOffset;
    }
    
    @Override
    public final void skipCData() {
        final int[] other_CHARS = ReaderScanner.sCharTypes.OTHER_CHARS;
        final char[] inputBuffer = this._inputBuffer;
    Block_13:
        while (true) {
            int i;
            int n;
            if ((i = this._inputPtr) >= (n = this._inputEnd)) {
                this.loadMoreGuaranteed();
                i = this._inputPtr;
                n = this._inputEnd;
            }
            while (i < n) {
                final int inputPtr = i + 1;
                final char c = inputBuffer[i];
                Label_0091: {
                    if (c <= '\u00ff') {
                        if (other_CHARS[c] != 0) {
                            break Label_0091;
                        }
                    }
                    else if (c >= '\ud800') {
                        break Label_0091;
                    }
                    i = inputPtr;
                    continue;
                }
                this._inputPtr = inputPtr;
                if (c <= '\u00ff') {
                    final int n2 = other_CHARS[c];
                    if (n2 != 1) {
                        if (n2 != 2) {
                            if (n2 == 3) {
                                this.markLF();
                                continue Block_13;
                            }
                            if (n2 != 11) {
                                continue Block_13;
                            }
                            int n3 = 0;
                            char c2;
                            int n4;
                            int inputPtr3;
                            do {
                                if (this._inputPtr >= this._inputEnd) {
                                    this.loadMoreGuaranteed();
                                }
                                n4 = n3 + 1;
                                final char[] inputBuffer2 = this._inputBuffer;
                                final int inputPtr2 = this._inputPtr;
                                inputPtr3 = inputPtr2 + 1;
                                this._inputPtr = inputPtr3;
                                c2 = inputBuffer2[inputPtr2];
                                n3 = n4;
                            } while (c2 == ']');
                            if (c2 != '>') {
                                this._inputPtr = inputPtr3 - 1;
                                continue Block_13;
                            }
                            if (n4 > 1) {
                                break Block_13;
                            }
                            continue Block_13;
                        }
                    }
                    else {
                        this.handleInvalidXmlChar(c);
                    }
                    int n5;
                    if ((n5 = this._inputPtr) >= this._inputEnd) {
                        this.loadMoreGuaranteed();
                        n5 = this._inputPtr;
                    }
                    int n6 = n5;
                    if (inputBuffer[n5] == '\n') {
                        n6 = n5 + 1;
                        ++this._inputPtr;
                    }
                    this.markLF(n6);
                    continue Block_13;
                }
                if (c < '\ud800') {
                    continue Block_13;
                }
                if (c < '\ue000') {
                    this.checkSurrogate(c);
                    continue Block_13;
                }
                if (c >= '\ufffe') {
                    this.handleInvalidXmlChar(c);
                    continue Block_13;
                }
                continue Block_13;
            }
            this._inputPtr = i;
        }
    }
    
    @Override
    public final boolean skipCharacters() {
        final int[] text_CHARS = ReaderScanner.sCharTypes.TEXT_CHARS;
        final char[] inputBuffer = this._inputBuffer;
    Label_0014:
        while (true) {
            int i;
            int n;
            if ((i = this._inputPtr) >= (n = this._inputEnd)) {
                this.loadMoreGuaranteed();
                i = this._inputPtr;
                n = this._inputEnd;
            }
            while (i < n) {
                final int inputPtr = i + 1;
                final char c = inputBuffer[i];
                Label_0091: {
                    if (c <= '\u00ff') {
                        if (text_CHARS[c] != 0) {
                            break Label_0091;
                        }
                    }
                    else if (c >= '\ud800') {
                        break Label_0091;
                    }
                    i = inputPtr;
                    continue;
                }
                this._inputPtr = inputPtr;
                if (c <= '\u00ff') {
                    final int n2 = text_CHARS[c];
                    while (true) {
                        Label_0257: {
                            if (n2 == 1) {
                                this.handleInvalidXmlChar(c);
                                break Label_0257;
                            }
                            if (n2 == 2) {
                                break Label_0257;
                            }
                            if (n2 != 3) {
                                switch (n2) {
                                    default: {
                                        continue Label_0014;
                                    }
                                    case 11: {
                                        int n3 = 1;
                                        char c2;
                                        while (true) {
                                            if (this._inputPtr >= this._inputEnd) {
                                                this.loadMoreGuaranteed();
                                            }
                                            final int inputPtr2 = this._inputPtr;
                                            c2 = inputBuffer[inputPtr2];
                                            if (c2 != ']') {
                                                break;
                                            }
                                            this._inputPtr = inputPtr2 + 1;
                                            ++n3;
                                        }
                                        if (c2 == '>' && n3 > 1) {
                                            this.reportIllegalCDataEnd();
                                            continue Label_0014;
                                        }
                                        continue Label_0014;
                                    }
                                    case 10: {
                                        if (this.handleEntityInText(false) == 0) {
                                            return true;
                                        }
                                        continue Label_0014;
                                    }
                                    case 9: {
                                        --this._inputPtr;
                                        return false;
                                    }
                                }
                            }
                            this.markLF();
                            continue Label_0014;
                        }
                        if (this._inputPtr >= this._inputEnd) {
                            this.loadMoreGuaranteed();
                        }
                        final int inputPtr3 = this._inputPtr;
                        if (inputBuffer[inputPtr3] == '\n') {
                            this._inputPtr = inputPtr3 + 1;
                        }
                        continue;
                    }
                }
                else {
                    if (c < '\ud800') {
                        continue Label_0014;
                    }
                    if (c < '\ue000') {
                        this.checkSurrogate(c);
                        continue Label_0014;
                    }
                    if (c >= '\ufffe') {
                        this.handleInvalidXmlChar(c);
                        continue Label_0014;
                    }
                    continue Label_0014;
                }
            }
            this._inputPtr = i;
        }
    }
    
    @Override
    public final boolean skipCoalescedText() {
        while (true) {
            final int inputPtr = this._inputPtr;
            final int inputEnd = this._inputEnd;
            int i = 0;
            if (inputPtr >= inputEnd && !this.loadMore()) {
                return false;
            }
            final char[] inputBuffer = this._inputBuffer;
            final int inputPtr2 = this._inputPtr;
            if (inputBuffer[inputPtr2] == '<') {
                if (inputPtr2 + 3 >= this._inputEnd && !this.loadAndRetain(3)) {
                    return false;
                }
                final char[] inputBuffer2 = this._inputBuffer;
                final int inputPtr3 = this._inputPtr;
                if (inputBuffer2[inputPtr3 + 1] != '!' || inputBuffer2[inputPtr3 + 2] != '[') {
                    return false;
                }
                this._inputPtr = inputPtr3 + 3;
                while (i < 6) {
                    if (this._inputPtr >= this._inputEnd) {
                        this.loadMoreGuaranteed();
                    }
                    final char c = this._inputBuffer[this._inputPtr++];
                    if (c != "CDATA[".charAt(i)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append(" (expected '");
                        sb.append("CDATA[".charAt(i));
                        sb.append("' for CDATA section)");
                        this.reportTreeUnexpChar(c, sb.toString());
                    }
                    ++i;
                }
                this.skipCData();
            }
            else {
                if (this.skipCharacters()) {
                    return true;
                }
                continue;
            }
        }
    }
    
    @Override
    public final void skipComment() {
        final int[] other_CHARS = ReaderScanner.sCharTypes.OTHER_CHARS;
        final char[] inputBuffer = this._inputBuffer;
        int inputPtr2 = 0;
    Block_11:
        while (true) {
            int i;
            int n;
            if ((i = this._inputPtr) >= (n = this._inputEnd)) {
                this.loadMoreGuaranteed();
                i = this._inputPtr;
                n = this._inputEnd;
            }
        Label_0215_Outer:
            while (i < n) {
                final int inputPtr = i + 1;
                final char c = inputBuffer[i];
                Label_0087: {
                    if (c <= '\u00ff') {
                        if (other_CHARS[c] != 0) {
                            break Label_0087;
                        }
                    }
                    else if (c >= '\ud800') {
                        break Label_0087;
                    }
                    i = inputPtr;
                    continue Label_0215_Outer;
                }
                this._inputPtr = inputPtr;
                if (c > '\u00ff') {
                    continue Block_11;
                }
                final int n2 = other_CHARS[c];
                while (true) {
                    Label_0228: {
                        if (n2 == 1) {
                            this.handleInvalidXmlChar(c);
                            break Label_0228;
                        }
                        if (n2 == 2) {
                            break Label_0228;
                        }
                        if (n2 != 3) {
                            if (n2 != 13) {
                                continue Block_11;
                            }
                            if (this._inputPtr >= this._inputEnd) {
                                this.loadMoreGuaranteed();
                            }
                            final char[] inputBuffer2 = this._inputBuffer;
                            inputPtr2 = this._inputPtr;
                            if (inputBuffer2[inputPtr2] == '-') {
                                break Block_11;
                            }
                            continue Block_11;
                        }
                        this.markLF();
                        continue Block_11;
                    }
                    if (this._inputPtr >= this._inputEnd) {
                        this.loadMoreGuaranteed();
                    }
                    final int inputPtr3 = this._inputPtr;
                    if (inputBuffer[inputPtr3] == '\n') {
                        this._inputPtr = inputPtr3 + 1;
                    }
                    continue;
                }
            }
            this._inputPtr = i;
        }
        ++inputPtr2;
        if ((this._inputPtr = inputPtr2) >= this._inputEnd) {
            this.loadMoreGuaranteed();
        }
        if (this._inputBuffer[this._inputPtr++] != '>') {
            this.reportDoubleHyphenInComments();
        }
    }
    
    public char skipInternalWs(final boolean b, final String str) {
        if (this._inputPtr >= this._inputEnd) {
            this.loadMoreGuaranteed();
        }
        final char c = this._inputBuffer[this._inputPtr++];
        char c2;
        if ((c2 = c) > ' ') {
            if (!b) {
                return c;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append(" (expected white space ");
            sb.append(str);
            sb.append(")");
            this.reportTreeUnexpChar(c, sb.toString());
            c2 = c;
        }
        char c3;
        do {
            Label_0190: {
                if (c2 != '\n') {
                    if (c2 == '\r') {
                        if (this._inputPtr >= this._inputEnd) {
                            this.loadMoreGuaranteed();
                        }
                        final char[] inputBuffer = this._inputBuffer;
                        final int inputPtr = this._inputPtr;
                        if (inputBuffer[inputPtr] == '\n') {
                            this._inputPtr = inputPtr + 1;
                        }
                    }
                    else {
                        if (c2 != ' ' && c2 != '\t') {
                            this.throwInvalidSpace(c2);
                        }
                        break Label_0190;
                    }
                }
                this.markLF();
            }
            if (this._inputPtr >= this._inputEnd) {
                this.loadMoreGuaranteed();
            }
            c3 = this._inputBuffer[this._inputPtr++];
        } while ((c2 = c3) <= ' ');
        return c3;
    }
    
    @Override
    public final void skipPI() {
        final int[] other_CHARS = ReaderScanner.sCharTypes.OTHER_CHARS;
        final char[] inputBuffer = this._inputBuffer;
        int inputPtr2 = 0;
    Block_10:
        while (true) {
            int i;
            int n;
            if ((i = this._inputPtr) >= (n = this._inputEnd)) {
                this.loadMoreGuaranteed();
                i = this._inputPtr;
                n = this._inputEnd;
            }
            while (i < n) {
                final int inputPtr = i + 1;
                final char c = inputBuffer[i];
                Label_0091: {
                    if (c <= '\u00ff') {
                        if (other_CHARS[c] != 0) {
                            break Label_0091;
                        }
                    }
                    else if (c >= '\ud800') {
                        break Label_0091;
                    }
                    i = inputPtr;
                    continue;
                }
                this._inputPtr = inputPtr;
                if (c <= '\u00ff') {
                    final int n2 = other_CHARS[c];
                    if (n2 != 2) {
                        if (n2 != 3) {
                            if (n2 != 12) {
                                continue Block_10;
                            }
                            if (this._inputPtr >= this._inputEnd) {
                                this.loadMoreGuaranteed();
                            }
                            final char[] inputBuffer2 = this._inputBuffer;
                            inputPtr2 = this._inputPtr;
                            if (inputBuffer2[inputPtr2] == '>') {
                                break Block_10;
                            }
                            continue Block_10;
                        }
                    }
                    else {
                        if (this._inputPtr >= this._inputEnd) {
                            this.loadMoreGuaranteed();
                        }
                        final int inputPtr3 = this._inputPtr;
                        if (inputBuffer[inputPtr3] == '\n') {
                            this._inputPtr = inputPtr3 + 1;
                        }
                    }
                    this.markLF();
                    continue Block_10;
                }
                if (c < '\ud800') {
                    continue Block_10;
                }
                if (c < '\ue000') {
                    this.checkSurrogate(c);
                    continue Block_10;
                }
                if (c >= '\ufffe') {
                    this.handleInvalidXmlChar(c);
                    continue Block_10;
                }
                continue Block_10;
            }
            this._inputPtr = i;
        }
        this._inputPtr = inputPtr2 + 1;
    }
    
    @Override
    public final void skipSpace() {
        int inputPtr = this._inputPtr;
        while (true) {
            int inputPtr2;
            if ((inputPtr2 = inputPtr) >= this._inputEnd) {
                if (!this.loadMore()) {
                    break;
                }
                inputPtr2 = this._inputPtr;
            }
            final char c = this._inputBuffer[inputPtr2];
            if (c > ' ') {
                inputPtr = inputPtr2;
                break;
            }
            ++inputPtr2;
            if (c == '\n') {
                inputPtr = inputPtr2;
            }
            else if (c == '\r') {
                int inputPtr3;
                if ((inputPtr3 = inputPtr2) >= this._inputEnd) {
                    if (!this.loadMore()) {
                        inputPtr = inputPtr2;
                        break;
                    }
                    inputPtr3 = this._inputPtr;
                }
                inputPtr = inputPtr3;
                if (this._inputBuffer[inputPtr3] == '\n') {
                    inputPtr = inputPtr3 + 1;
                }
            }
            else {
                inputPtr = inputPtr2;
                if (c == ' ') {
                    continue;
                }
                inputPtr = inputPtr2;
                if (c != '\t') {
                    this._inputPtr = inputPtr2;
                    this.throwInvalidSpace(c);
                    inputPtr = inputPtr2;
                    continue;
                }
                continue;
            }
            this.markLF(inputPtr);
        }
        this._inputPtr = inputPtr;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.in;

public final class ByteBasedPNameFactory
{
    private static final boolean DO_INTERN = true;
    private static final ByteBasedPNameFactory sInstance;
    
    static {
        sInstance = new ByteBasedPNameFactory();
    }
    
    private ByteBasedPNameFactory() {
    }
    
    public static ByteBasedPNameFactory getInstance() {
        return ByteBasedPNameFactory.sInstance;
    }
    
    public ByteBasedPName constructPName(final int n, String intern, final int endIndex, final int n2, final int n3) {
        if (endIndex < 0) {
            intern = intern.intern();
            if (n3 == 0) {
                return new PName1(intern, null, intern, n, n2);
            }
            return new PName2(intern, null, intern, n, n2, n3);
        }
        else {
            final String substring = intern.substring(0, endIndex);
            final String substring2 = intern.substring(endIndex + 1);
            final String intern2 = substring.intern();
            final String intern3 = substring2.intern();
            if (n3 == 0) {
                return new PName1(intern, intern2, intern3, n, n2);
            }
            return new PName2(intern, intern2, intern3, n, n2, n3);
        }
    }
    
    public ByteBasedPName constructPName(final int n, String s, final int n2, final int[] array, final int n3) {
        if (n3 < 4) {
            if (n2 < 0) {
                s = s.intern();
                if (n3 == 3) {
                    return new PName3(s, null, s, n, array);
                }
                if (n3 == 2) {
                    return new PName2(s, null, s, n, array[0], array[1]);
                }
                return new PName1(s, null, s, n, array[0]);
            }
            else {
                final String substring = s.substring(0, n2);
                final String intern = s.substring(n2 + 1).intern();
                final String intern2 = substring.intern();
                if (n3 == 3) {
                    return new PName3(s, intern2, intern, n, array);
                }
                if (n3 == 2) {
                    return new PName2(s, intern2, intern, n, array[0], array[1]);
                }
                return new PName1(s, intern2, intern, n, array[0]);
            }
        }
        else {
            final int[] array2 = new int[n3];
            for (int i = 0; i < n3; ++i) {
                array2[i] = array[i];
            }
            if (n2 < 0) {
                s = s.intern();
                return new PNameN(s, null, s, n, array2, n3);
            }
            return new PNameN(s, s.substring(0, n2).intern(), s.substring(n2 + 1).intern(), n, array2, n3);
        }
    }
}

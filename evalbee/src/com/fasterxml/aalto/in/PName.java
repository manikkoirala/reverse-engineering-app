// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.in;

import javax.xml.namespace.QName;

public abstract class PName
{
    protected final String _localName;
    protected NsBinding _namespaceBinding;
    protected final String _prefix;
    protected final String _prefixedName;
    
    public PName(final String prefixedName, final String prefix, final String localName) {
        this._namespaceBinding = null;
        this._prefixedName = prefixedName;
        this._prefix = prefix;
        this._localName = localName;
    }
    
    public static int boundHashCode(final String s, final String s2) {
        return s2.hashCode();
    }
    
    public final boolean boundEquals(final PName pName) {
        boolean b2;
        final boolean b = b2 = false;
        if (pName != null) {
            if (pName._localName != this._localName) {
                b2 = b;
            }
            else {
                b2 = b;
                if (pName.getNsUri() == this.getNsUri()) {
                    b2 = true;
                }
            }
        }
        return b2;
    }
    
    public final boolean boundEquals(final String s, String nsUri) {
        final boolean equals = this._localName.equals(nsUri);
        boolean b = false;
        if (!equals) {
            return false;
        }
        nsUri = this.getNsUri();
        if (s != null && s.length() != 0) {
            return s.equals(nsUri);
        }
        if (nsUri == null) {
            b = true;
        }
        return b;
    }
    
    public final int boundHashCode() {
        return this._localName.hashCode();
    }
    
    public final QName constructQName() {
        final String prefix = this._prefix;
        final NsBinding namespaceBinding = this._namespaceBinding;
        String muri;
        if (namespaceBinding == null) {
            muri = null;
        }
        else {
            muri = namespaceBinding.mURI;
        }
        String namespaceURI = muri;
        if (muri == null) {
            namespaceURI = "";
        }
        final String localName = this._localName;
        String prefix2;
        if ((prefix2 = prefix) == null) {
            prefix2 = "";
        }
        return new QName(namespaceURI, localName, prefix2);
    }
    
    public final QName constructQName(final NsBinding nsBinding) {
        final String prefix = this._prefix;
        final String s = "";
        String s2 = prefix;
        if (prefix == null) {
            s2 = "";
        }
        final NsBinding namespaceBinding = this._namespaceBinding;
        if (namespaceBinding != null) {
            final String muri = namespaceBinding.mURI;
            if (muri != null) {
                return new QName(muri, this._localName, s2);
            }
        }
        String muri2 = nsBinding.mURI;
        if (muri2 == null) {
            muri2 = s;
        }
        return new QName(muri2, this._localName, s2);
    }
    
    public abstract PName createBoundName(final NsBinding p0);
    
    @Override
    public final boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (!(o instanceof PName)) {
            return false;
        }
        final PName pName = (PName)o;
        if (pName._prefix != this._prefix || pName._localName != this._localName) {
            b = false;
        }
        return b;
    }
    
    public abstract int getFirstQuad();
    
    public abstract int getLastQuad();
    
    public final String getLocalName() {
        return this._localName;
    }
    
    public final NsBinding getNsBinding() {
        return this._namespaceBinding;
    }
    
    public final String getNsUri() {
        final NsBinding namespaceBinding = this._namespaceBinding;
        String muri;
        if (namespaceBinding == null) {
            muri = null;
        }
        else {
            muri = namespaceBinding.mURI;
        }
        return muri;
    }
    
    public final String getPrefix() {
        return this._prefix;
    }
    
    public final String getPrefixedName() {
        return this._prefixedName;
    }
    
    public abstract int getQuad(final int p0);
    
    public boolean hasPrefix() {
        return this._prefix != null;
    }
    
    public boolean hasPrefixedName(final String anObject) {
        return this._prefixedName.equals(anObject);
    }
    
    @Override
    public int hashCode() {
        return this._prefixedName.hashCode();
    }
    
    public final boolean isBound() {
        final NsBinding namespaceBinding = this._namespaceBinding;
        return namespaceBinding == null || namespaceBinding.mURI != null;
    }
    
    public final boolean needsBinding() {
        return this._prefix != null && this._namespaceBinding == null;
    }
    
    public abstract int sizeInQuads();
    
    @Override
    public final String toString() {
        return this._prefixedName;
    }
    
    public final boolean unboundEquals(final PName pName) {
        return pName._prefixedName == this._prefixedName;
    }
    
    public final int unboundHashCode() {
        return this._prefixedName.hashCode();
    }
}

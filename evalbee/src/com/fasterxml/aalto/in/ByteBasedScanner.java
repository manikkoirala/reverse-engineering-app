// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.in;

import com.fasterxml.aalto.impl.LocationImpl;
import org.codehaus.stax2.XMLStreamLocation2;
import com.fasterxml.aalto.util.DataUtil;
import com.fasterxml.aalto.util.XmlChars;
import com.fasterxml.aalto.util.XmlCharTypes;

public abstract class ByteBasedScanner extends XmlScanner
{
    protected static final byte BYTE_A = 65;
    protected static final byte BYTE_AMP = 38;
    protected static final byte BYTE_APOS = 39;
    protected static final byte BYTE_C = 67;
    protected static final byte BYTE_CR = 13;
    protected static final byte BYTE_D = 68;
    protected static final byte BYTE_EQ = 61;
    protected static final byte BYTE_EXCL = 33;
    protected static final byte BYTE_GT = 62;
    protected static final byte BYTE_HASH = 35;
    protected static final byte BYTE_HYPHEN = 45;
    protected static final byte BYTE_LBRACKET = 91;
    protected static final byte BYTE_LF = 10;
    protected static final byte BYTE_LT = 60;
    protected static final byte BYTE_NULL = 0;
    protected static final byte BYTE_P = 80;
    protected static final byte BYTE_QMARK = 63;
    protected static final byte BYTE_QUOT = 34;
    protected static final byte BYTE_RBRACKET = 93;
    protected static final byte BYTE_S = 83;
    protected static final byte BYTE_SEMICOLON = 59;
    protected static final byte BYTE_SLASH = 47;
    protected static final byte BYTE_SPACE = 32;
    protected static final byte BYTE_T = 84;
    protected static final byte BYTE_TAB = 9;
    protected static final byte BYTE_a = 97;
    protected static final byte BYTE_g = 103;
    protected static final byte BYTE_l = 108;
    protected static final byte BYTE_m = 109;
    protected static final byte BYTE_o = 111;
    protected static final byte BYTE_p = 112;
    protected static final byte BYTE_q = 113;
    protected static final byte BYTE_s = 115;
    protected static final byte BYTE_t = 116;
    protected static final byte BYTE_u = 117;
    protected static final byte BYTE_x = 120;
    protected int _inputEnd;
    protected int _inputPtr;
    protected int _tmpChar;
    
    public ByteBasedScanner(final ReaderConfig readerConfig) {
        super(readerConfig);
        this._tmpChar = 0;
        super._pastBytesOrChars = 0L;
        super._rowStartOffset = 0;
    }
    
    @Override
    public abstract void _closeSource();
    
    public final PName addUTFPName(final ByteBasedPNameTable byteBasedPNameTable, final XmlCharTypes xmlCharTypes, final int n, final int[] array, final int n2, final int n3) {
        final int n4 = (n2 << 2) - 4 + n3;
        int n6;
        if (n3 < 4) {
            final int n5 = n2 - 1;
            n6 = array[n5];
            array[n5] = n6 << (4 - n3 << 3);
        }
        else {
            n6 = 0;
        }
        int n7 = array[0] >>> 24;
        final char[] nameBuffer = super._nameBuffer;
        final int[] name_CHARS = xmlCharTypes.NAME_CHARS;
        final int n8 = name_CHARS[n7];
        int n9 = 0;
        boolean is10NameStartChar = false;
        int i = 0;
        Label_0456: {
            if (n8 != 0 && n8 != 1 && n8 != 2) {
                if (n8 == 3) {
                    n9 = 0;
                    is10NameStartChar = true;
                    i = 1;
                    break Label_0456;
                }
                if (n8 != 4) {
                    int n10;
                    int n11;
                    if ((n7 & 0xE0) == 0xC0) {
                        n10 = (n7 & 0x1F);
                        n11 = 1;
                    }
                    else if ((n7 & 0xF0) == 0xE0) {
                        n10 = (n7 & 0xF);
                        n11 = 2;
                    }
                    else if ((n7 & 0xF8) == 0xF0) {
                        n10 = (n7 & 0x7);
                        n11 = 3;
                    }
                    else {
                        this.reportInvalidInitial(n7);
                        n10 = 1;
                        n11 = 1;
                    }
                    final int n12 = 1 + n11;
                    if (n12 > n4) {
                        this.reportEofInName(nameBuffer, 0);
                    }
                    final int n13 = array[0];
                    final int n14 = n13 >> 16 & 0xFF;
                    if ((n14 & 0xC0) != 0x80) {
                        this.reportInvalidOther(n14);
                    }
                    int n16;
                    final int n15 = n16 = (n10 << 6 | (n14 & 0x3F));
                    if (n11 > 1) {
                        final int n17 = n13 >> 8 & 0xFF;
                        if ((n17 & 0xC0) != 0x80) {
                            this.reportInvalidOther(n17);
                        }
                        final int n18 = n16 = (n15 << 6 | (n17 & 0x3F));
                        if (n11 > 2) {
                            final int n19 = n13 & 0xFF;
                            if ((n19 & 0xC0) != 0x80) {
                                this.reportInvalidOther(n19 & 0xFF);
                            }
                            n16 = (n18 << 6 | (n19 & 0x3F));
                        }
                    }
                    is10NameStartChar = XmlChars.is10NameStartChar(n16);
                    if (n11 > 2) {
                        final int n20 = n16 - 65536;
                        nameBuffer[0] = (char)((n20 >> 10) + 55296);
                        n7 = ((n20 & 0x3FF) | 0xDC00);
                        n9 = 1;
                        i = n12;
                        break Label_0456;
                    }
                    n9 = 0;
                    n7 = n16;
                    i = n12;
                    break Label_0456;
                }
            }
            is10NameStartChar = false;
            n9 = 0;
            i = 1;
        }
        if (!is10NameStartChar) {
            this.reportInvalidNameChar(n7, 0);
        }
        final int n21 = n9 + 1;
        nameBuffer[n9] = (char)n7;
        int n22 = -1;
        char[] growArrayBy = nameBuffer;
        int count = n21;
        while (i < n4) {
            final int n23 = array[i >> 2] >> (3 - (i & 0x3) << 3) & 0xFF;
            final int n24 = i + 1;
            final int n25 = name_CHARS[n23];
            int n32 = 0;
            boolean is10NameChar = false;
            char[] growArrayBy2 = null;
            int n37 = 0;
            Label_1002: {
                Label_0949: {
                    if (n25 != 0) {
                        if (n25 != 1) {
                            if (n25 != 2 && n25 != 3) {
                                if (n25 == 4) {
                                    break Label_0949;
                                }
                                int n26;
                                int n27;
                                if ((n23 & 0xE0) == 0xC0) {
                                    n26 = (n23 & 0x1F);
                                    n27 = 1;
                                }
                                else if ((n23 & 0xF0) == 0xE0) {
                                    n26 = (n23 & 0xF);
                                    n27 = 2;
                                }
                                else if ((n23 & 0xF8) == 0xF0) {
                                    n26 = (n23 & 0x7);
                                    n27 = 3;
                                }
                                else {
                                    this.reportInvalidInitial(n23);
                                    n26 = 1;
                                    n27 = 1;
                                }
                                if (n24 + n27 > n4) {
                                    this.reportEofInName(growArrayBy, count);
                                }
                                final int n28 = array[n24 >> 2] >> (3 - (n24 & 0x3) << 3);
                                int n29 = n24 + 1;
                                if ((n28 & 0xC0) != 0x80) {
                                    this.reportInvalidOther(n28);
                                }
                                int n31;
                                final int n30 = n31 = (n26 << 6 | (n28 & 0x3F));
                                n32 = n29;
                                if (n27 > 1) {
                                    final int n33 = array[n29 >> 2] >> (3 - (n29 & 0x3) << 3);
                                    ++n29;
                                    if ((n33 & 0xC0) != 0x80) {
                                        this.reportInvalidOther(n33);
                                    }
                                    final int n34 = n31 = (n30 << 6 | (n33 & 0x3F));
                                    n32 = n29;
                                    if (n27 > 2) {
                                        final int n35 = array[n29 >> 2] >> (3 - (n29 & 0x3) << 3);
                                        n32 = n29 + 1;
                                        if ((n35 & 0xC0) != 0x80) {
                                            this.reportInvalidOther(n35 & 0xFF);
                                        }
                                        n31 = (n34 << 6 | (n35 & 0x3F));
                                    }
                                }
                                is10NameChar = XmlChars.is10NameChar(n31);
                                if (n27 > 2) {
                                    final int n36 = n31 - 65536;
                                    growArrayBy2 = growArrayBy;
                                    if (count >= growArrayBy.length) {
                                        growArrayBy2 = DataUtil.growArrayBy(growArrayBy, growArrayBy.length);
                                        super._nameBuffer = growArrayBy2;
                                    }
                                    growArrayBy2[count] = (char)((n36 >> 10) + 55296);
                                    n37 = ((n36 & 0x3FF) | 0xDC00);
                                    ++count;
                                    break Label_1002;
                                }
                                n37 = n31;
                                growArrayBy2 = growArrayBy;
                                break Label_1002;
                            }
                        }
                        else {
                            if (n22 >= 0) {
                                this.reportMultipleColonsInName();
                            }
                            n22 = count;
                        }
                        is10NameChar = true;
                        n37 = n23;
                        growArrayBy2 = growArrayBy;
                        n32 = n24;
                        break Label_1002;
                    }
                }
                is10NameChar = false;
                n32 = n24;
                growArrayBy2 = growArrayBy;
                n37 = n23;
            }
            if (!is10NameChar) {
                this.reportInvalidNameChar(n37, count);
            }
            growArrayBy = growArrayBy2;
            if (count >= growArrayBy2.length) {
                growArrayBy = DataUtil.growArrayBy(growArrayBy2, growArrayBy2.length);
                super._nameBuffer = growArrayBy;
            }
            growArrayBy[count] = (char)n37;
            ++count;
            i = n32;
        }
        final String s = new String(growArrayBy, 0, count);
        if (n3 < 4) {
            array[n2 - 1] = n6;
        }
        return byteBasedPNameTable.addSymbol(n, s, n22, array, n2);
    }
    
    public abstract int decodeCharForError(final byte p0);
    
    @Override
    public int getCurrentColumnNr() {
        return this._inputPtr - super._rowStartOffset;
    }
    
    @Override
    public XMLStreamLocation2 getCurrentLocation() {
        final String publicId = super._config.getPublicId();
        final String systemId = super._config.getSystemId();
        final long pastBytesOrChars = super._pastBytesOrChars;
        final int inputPtr = this._inputPtr;
        return (XMLStreamLocation2)LocationImpl.fromZeroBased(publicId, systemId, pastBytesOrChars + inputPtr, super._currRow, inputPtr - super._rowStartOffset);
    }
    
    @Override
    public long getEndingByteOffset() {
        if (super._tokenIncomplete) {
            this.finishToken();
        }
        return super._pastBytesOrChars + this._inputPtr;
    }
    
    @Override
    public long getEndingCharOffset() {
        return -1L;
    }
    
    @Override
    public long getStartingByteOffset() {
        return super._startRawOffset;
    }
    
    @Override
    public long getStartingCharOffset() {
        return -1L;
    }
    
    public final void markLF() {
        super._rowStartOffset = this._inputPtr;
        ++super._currRow;
    }
    
    public final void markLF(final int rowStartOffset) {
        super._rowStartOffset = rowStartOffset;
        ++super._currRow;
    }
    
    public void reportInvalidInitial(final int i) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid UTF-8 start byte 0x");
        sb.append(Integer.toHexString(i));
        this.reportInputProblem(sb.toString());
    }
    
    public void reportInvalidOther(final int i) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid UTF-8 middle byte 0x");
        sb.append(Integer.toHexString(i));
        this.reportInputProblem(sb.toString());
    }
    
    public final void setStartLocation() {
        final long pastBytesOrChars = super._pastBytesOrChars;
        final int inputPtr = this._inputPtr;
        super._startRawOffset = pastBytesOrChars + inputPtr;
        super._startRow = super._currRow;
        super._startColumn = inputPtr - super._rowStartOffset;
    }
}

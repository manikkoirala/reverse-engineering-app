// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.in;

import com.fasterxml.aalto.WFCException;
import com.fasterxml.aalto.util.IllegalCharHandler;
import com.fasterxml.aalto.util.XmlChars;
import java.io.Writer;
import com.fasterxml.aalto.impl.LocationImpl;
import com.fasterxml.aalto.util.EmptyIterator;
import java.util.ArrayList;
import com.fasterxml.aalto.util.SingletonIterator;
import java.util.Iterator;
import com.fasterxml.aalto.impl.ErrorConsts;
import org.codehaus.stax2.XMLStreamLocation2;
import javax.xml.namespace.QName;
import org.xml.sax.Attributes;
import org.xml.sax.ext.LexicalHandler;
import org.xml.sax.ContentHandler;
import javax.xml.stream.Location;
import org.codehaus.stax2.typed.TypedXMLStreamException;
import org.codehaus.stax2.typed.TypedArrayDecoder;
import org.codehaus.stax2.typed.TypedValueDecoder;
import org.codehaus.stax2.ri.typed.CharArrayBase64Decoder;
import org.codehaus.stax2.typed.Base64Variant;
import java.io.IOException;
import com.fasterxml.aalto.impl.IoStreamException;
import com.fasterxml.aalto.util.DataUtil;
import com.fasterxml.aalto.util.TextBuilder;
import javax.xml.namespace.NamespaceContext;
import javax.xml.stream.XMLStreamConstants;
import com.fasterxml.aalto.util.XmlConsts;

public abstract class XmlScanner implements XmlConsts, XMLStreamConstants, NamespaceContext
{
    private static final int BIND_CACHE_MASK = 63;
    private static final int BIND_CACHE_SIZE = 64;
    private static final int BIND_MISSES_TO_ACTIVATE_CACHE = 10;
    protected static final int INT_0 = 48;
    protected static final int INT_9 = 57;
    protected static final int INT_A = 65;
    protected static final int INT_AMP = 38;
    protected static final int INT_APOS = 39;
    protected static final int INT_COLON = 58;
    protected static final int INT_CR = 13;
    protected static final int INT_EQ = 61;
    protected static final int INT_EXCL = 33;
    protected static final int INT_F = 70;
    protected static final int INT_GT = 62;
    protected static final int INT_HYPHEN = 45;
    protected static final int INT_LBRACKET = 91;
    protected static final int INT_LF = 10;
    protected static final int INT_LT = 60;
    protected static final int INT_NULL = 0;
    protected static final int INT_QMARK = 63;
    protected static final int INT_QUOTE = 34;
    protected static final int INT_RBRACKET = 93;
    protected static final int INT_SLASH = 47;
    protected static final int INT_SPACE = 32;
    protected static final int INT_TAB = 9;
    protected static final int INT_a = 97;
    protected static final int INT_f = 102;
    protected static final int INT_z = 122;
    protected static final int MAX_UNICODE_CHAR = 1114111;
    public static final int TOKEN_EOI = -1;
    protected final String CDATA_STR;
    protected final AttributeCollector _attrCollector;
    protected int _attrCount;
    protected final boolean _cfgCoalescing;
    protected boolean _cfgLazyParsing;
    protected final ReaderConfig _config;
    protected ElementScope _currElem;
    protected int _currNsCount;
    protected int _currRow;
    protected int _currToken;
    protected NsBinding _defaultNs;
    protected int _depth;
    protected boolean _entityPending;
    protected boolean _isEmptyTag;
    protected FixedNsContext _lastNsContext;
    protected NsDeclaration _lastNsDecl;
    protected char[] _nameBuffer;
    protected int _nsBindMisses;
    protected PName[] _nsBindingCache;
    protected int _nsBindingCount;
    protected NsBinding[] _nsBindings;
    protected long _pastBytesOrChars;
    protected String _publicId;
    protected int _rowStartOffset;
    protected long _startColumn;
    protected long _startRawOffset;
    protected long _startRow;
    protected String _systemId;
    protected final TextBuilder _textBuilder;
    protected boolean _tokenIncomplete;
    protected PName _tokenName;
    protected final boolean _xml11;
    
    public XmlScanner(final ReaderConfig config) {
        this.CDATA_STR = "CDATA[";
        this._currToken = 7;
        this._tokenIncomplete = false;
        this._depth = 0;
        this._entityPending = false;
        this._nameBuffer = null;
        this._tokenName = null;
        this._isEmptyTag = false;
        this._lastNsDecl = null;
        this._currNsCount = 0;
        this._defaultNs = NsBinding.createDefaultNs();
        this._nsBindingCount = 0;
        this._nsBindingCache = null;
        this._nsBindMisses = 0;
        this._lastNsContext = FixedNsContext.EMPTY_CONTEXT;
        this._attrCount = 0;
        this._startRow = -1L;
        this._startColumn = -1L;
        this._config = config;
        this._cfgCoalescing = config.willCoalesceText();
        this._cfgLazyParsing = config.willParseLazily();
        this._xml11 = config.isXml11();
        this._textBuilder = TextBuilder.createRecyclableBuffer(config);
        this._attrCollector = new AttributeCollector(config);
        this._nameBuffer = config.allocSmallCBuffer(60);
        this._currRow = 0;
    }
    
    private NsDeclaration findCurrNsDecl(final int n) {
        NsDeclaration nsDeclaration = this._lastNsDecl;
        int depth = this._depth;
        int n2;
        if (this._currToken == 1) {
            n2 = this._currNsCount - 1 - n;
            --depth;
        }
        else {
            n2 = n;
        }
        while (nsDeclaration != null && nsDeclaration.getLevel() == depth) {
            if (n2 == 0) {
                return nsDeclaration;
            }
            --n2;
            nsDeclaration = nsDeclaration.getPrev();
        }
        this.reportInvalidNsIndex(n);
        return null;
    }
    
    public abstract void _closeSource();
    
    public void _releaseBuffers() {
        this._textBuilder.recycle(true);
        final char[] nameBuffer = this._nameBuffer;
        if (nameBuffer != null) {
            this._nameBuffer = null;
            this._config.freeSmallCBuffer(nameBuffer);
        }
    }
    
    public final PName bindName(PName boundName, final String s) {
        final PName[] nsBindingCache = this._nsBindingCache;
        if (nsBindingCache != null) {
            final PName pName = nsBindingCache[boundName.unboundHashCode() & 0x3F];
            if (pName != null && pName.unboundEquals(boundName)) {
                return pName;
            }
        }
        for (int nsBindingCount = this._nsBindingCount, i = 0; i < nsBindingCount; ++i) {
            final NsBinding[] nsBindings = this._nsBindings;
            final NsBinding nsBinding = nsBindings[i];
            if (nsBinding.mPrefix == s) {
                if (i > 0) {
                    final int n = i - 1;
                    nsBindings[i] = nsBindings[n];
                    nsBindings[n] = nsBinding;
                }
                boundName = boundName.createBoundName(nsBinding);
                if (this._nsBindingCache == null) {
                    if (++this._nsBindMisses < 10) {
                        return boundName;
                    }
                    this._nsBindingCache = new PName[64];
                }
                return this._nsBindingCache[boundName.unboundHashCode() & 0x3F] = boundName;
            }
        }
        if (s == "xml") {
            return boundName.createBoundName(NsBinding.XML_BINDING);
        }
        ++this._nsBindMisses;
        final NsBinding nsBinding2 = new NsBinding(s);
        final int nsBindingCount2 = this._nsBindingCount;
        if (nsBindingCount2 == 0) {
            this._nsBindings = new NsBinding[16];
        }
        else {
            final NsBinding[] nsBindings2 = this._nsBindings;
            if (nsBindingCount2 >= nsBindings2.length) {
                this._nsBindings = (NsBinding[])DataUtil.growAnyArrayBy(nsBindings2, nsBindings2.length);
            }
        }
        final NsBinding[] nsBindings3 = this._nsBindings;
        final int nsBindingCount3 = this._nsBindingCount;
        nsBindings3[nsBindingCount3] = nsBinding2;
        this._nsBindingCount = nsBindingCount3 + 1;
        return boundName.createBoundName(nsBinding2);
    }
    
    public final void bindNs(final PName pName, final String s) {
        final String prefix = pName.getPrefix();
        String s2;
        NsBinding nsBinding;
        if (prefix == null) {
            final NsBinding defaultNs = this._defaultNs;
            s2 = prefix;
            nsBinding = defaultNs;
        }
        else {
            final String localName = pName.getLocalName();
            final NsBinding orCreateBinding = this.findOrCreateBinding(localName);
            s2 = localName;
            nsBinding = orCreateBinding;
            if (orCreateBinding.isImmutable()) {
                this.checkImmutableBinding(localName, s);
                nsBinding = orCreateBinding;
                s2 = localName;
            }
        }
        Label_0119: {
            if (!nsBinding.isImmutable()) {
                String s3 = "http://www.w3.org/XML/1998/namespace";
                String s4;
                if (s == "http://www.w3.org/XML/1998/namespace") {
                    s4 = "xml";
                }
                else {
                    s3 = "http://www.w3.org/2000/xmlns/";
                    if (s != "http://www.w3.org/2000/xmlns/") {
                        break Label_0119;
                    }
                    s4 = "xmlns";
                }
                this.reportIllegalNsDecl(s4, s3);
            }
        }
        final NsDeclaration lastNsDecl = this._lastNsDecl;
        if (lastNsDecl != null && lastNsDecl.alreadyDeclared(s2, this._depth)) {
            this.reportDuplicateNsDecl(s2);
        }
        this._lastNsDecl = new NsDeclaration(nsBinding, s, this._lastNsDecl, this._depth);
    }
    
    public final void checkImmutableBinding(final String s, final String s2) {
        if (s != "xml" || !s2.equals("http://www.w3.org/XML/1998/namespace")) {
            this.reportIllegalNsDecl(s);
        }
    }
    
    public final void close(final boolean b) {
        this._releaseBuffers();
        if (!b) {
            if (!this._config.willAutoCloseInput()) {
                return;
            }
        }
        try {
            this._closeSource();
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    public final byte[] decodeAttrBinaryValue(final int n, final Base64Variant base64Variant, final CharArrayBase64Decoder charArrayBase64Decoder) {
        return this._attrCollector.decodeBinaryValue(n, base64Variant, charArrayBase64Decoder, this);
    }
    
    public final void decodeAttrValue(final int n, final TypedValueDecoder typedValueDecoder) {
        this._attrCollector.decodeValue(n, typedValueDecoder);
    }
    
    public final int decodeAttrValues(final int n, final TypedArrayDecoder typedArrayDecoder) {
        return this._attrCollector.decodeValues(n, typedArrayDecoder, this);
    }
    
    public final int decodeElements(final TypedArrayDecoder typedArrayDecoder, final boolean b) {
        if (this._tokenIncomplete) {
            this.finishToken();
        }
        try {
            return this._textBuilder.decodeElements(typedArrayDecoder, b);
        }
        catch (final TypedXMLStreamException ex) {
            throw new TypedXMLStreamException(ex.getLexical(), ((Throwable)ex).getMessage(), (Location)this.getCurrentLocation(), (IllegalArgumentException)((Throwable)ex).getCause());
        }
    }
    
    public final int findAttrIndex(final String s, final String s2) {
        if (this._attrCount < 1) {
            return -1;
        }
        return this._attrCollector.findIndex(s, s2);
    }
    
    public final NsBinding findOrCreateBinding(final String s) {
        for (int nsBindingCount = this._nsBindingCount, i = 0; i < nsBindingCount; ++i) {
            final NsBinding[] nsBindings = this._nsBindings;
            final NsBinding nsBinding = nsBindings[i];
            if (nsBinding.mPrefix == s) {
                if (i > 0) {
                    final int n = i - 1;
                    nsBindings[i] = nsBindings[n];
                    nsBindings[n] = nsBinding;
                }
                return nsBinding;
            }
        }
        if (s == "xml") {
            return NsBinding.XML_BINDING;
        }
        if (s == "xmlns") {
            return NsBinding.XMLNS_BINDING;
        }
        final NsBinding nsBinding2 = new NsBinding(s);
        final int nsBindingCount2 = this._nsBindingCount;
        if (nsBindingCount2 == 0) {
            this._nsBindings = new NsBinding[16];
        }
        else {
            final NsBinding[] nsBindings2 = this._nsBindings;
            if (nsBindingCount2 >= nsBindings2.length) {
                this._nsBindings = (NsBinding[])DataUtil.growAnyArrayBy(nsBindings2, nsBindings2.length);
            }
        }
        final NsBinding[] nsBindings3 = this._nsBindings;
        final int nsBindingCount3 = this._nsBindingCount;
        nsBindings3[nsBindingCount3] = nsBinding2;
        this._nsBindingCount = nsBindingCount3 + 1;
        return nsBinding2;
    }
    
    public abstract void finishCData();
    
    public abstract void finishCharacters();
    
    public abstract void finishComment();
    
    public abstract void finishDTD(final boolean p0);
    
    public abstract void finishPI();
    
    public abstract void finishSpace();
    
    public abstract void finishToken();
    
    public void fireSaxCharacterEvents(final ContentHandler contentHandler) {
        if (contentHandler != null) {
            if (this._tokenIncomplete) {
                this.finishToken();
            }
            this._textBuilder.fireSaxCharacterEvents(contentHandler);
        }
    }
    
    public void fireSaxCommentEvent(final LexicalHandler lexicalHandler) {
        if (lexicalHandler != null) {
            if (this._tokenIncomplete) {
                this.finishToken();
            }
            this._textBuilder.fireSaxCommentEvent(lexicalHandler);
        }
    }
    
    public void fireSaxEndElement(final ContentHandler contentHandler) {
        if (contentHandler != null) {
            final PName name = this.getName();
            String nsUri;
            if ((nsUri = name.getNsUri()) == null) {
                nsUri = "";
            }
            contentHandler.endElement(nsUri, name.getLocalName(), name.getPrefixedName());
            NsDeclaration nsDeclaration = this._lastNsDecl;
            for (int depth = this._depth; nsDeclaration != null && nsDeclaration.getLevel() == depth; nsDeclaration = nsDeclaration.getPrev()) {
                String prefix;
                if ((prefix = nsDeclaration.getPrefix()) == null) {
                    prefix = "";
                }
                contentHandler.endPrefixMapping(prefix);
            }
        }
    }
    
    public void fireSaxPIEvent(final ContentHandler contentHandler) {
        if (contentHandler != null) {
            if (this._tokenIncomplete) {
                this.finishToken();
            }
            contentHandler.processingInstruction(this._tokenName.getLocalName(), this.getText());
        }
    }
    
    public void fireSaxSpaceEvents(final ContentHandler contentHandler) {
        if (contentHandler != null) {
            if (this._tokenIncomplete) {
                this.finishToken();
            }
            this._textBuilder.fireSaxSpaceEvents(contentHandler);
        }
    }
    
    public void fireSaxStartElement(final ContentHandler contentHandler, final Attributes attributes) {
        if (contentHandler != null) {
            NsDeclaration nsDeclaration = this._lastNsDecl;
            final int depth = this._depth;
            String s;
            while (true) {
                s = "";
                if (nsDeclaration == null || nsDeclaration.getLevel() != depth - 1) {
                    break;
                }
                final String prefix = nsDeclaration.getPrefix();
                final String currNsURI = nsDeclaration.getCurrNsURI();
                if (prefix != null) {
                    s = prefix;
                }
                contentHandler.startPrefixMapping(s, currNsURI);
                nsDeclaration = nsDeclaration.getPrev();
            }
            final PName name = this.getName();
            final String nsUri = name.getNsUri();
            if (nsUri != null) {
                s = nsUri;
            }
            contentHandler.startElement(s, name.getLocalName(), name.getPrefixedName(), attributes);
        }
    }
    
    public AttributeCollector getAttrCollector() {
        return this._attrCollector;
    }
    
    public final int getAttrCount() {
        return this._attrCount;
    }
    
    public final String getAttrLocalName(final int n) {
        return this._attrCollector.getName(n).getLocalName();
    }
    
    public final String getAttrNsURI(final int n) {
        return this._attrCollector.getName(n).getNsUri();
    }
    
    public final String getAttrPrefix(final int n) {
        return this._attrCollector.getName(n).getPrefix();
    }
    
    public final String getAttrPrefixedName(final int n) {
        return this._attrCollector.getName(n).getPrefixedName();
    }
    
    public final QName getAttrQName(final int n) {
        return this._attrCollector.getQName(n);
    }
    
    public final String getAttrType(final int n) {
        return "CDATA";
    }
    
    public final String getAttrValue(final int n) {
        return this._attrCollector.getValue(n);
    }
    
    public final String getAttrValue(final String s, final String s2) {
        if (this._attrCount < 1) {
            return null;
        }
        return this._attrCollector.getValue(s, s2);
    }
    
    public ReaderConfig getConfig() {
        return this._config;
    }
    
    public abstract int getCurrentColumnNr();
    
    public final int getCurrentLineNr() {
        return this._currRow + 1;
    }
    
    public abstract XMLStreamLocation2 getCurrentLocation();
    
    public final String getDTDPublicId() {
        return this._publicId;
    }
    
    public final String getDTDSystemId() {
        return this._systemId;
    }
    
    public final int getDepth() {
        return this._depth;
    }
    
    public XMLStreamLocation2 getEndLocation() {
        if (this._tokenIncomplete) {
            this.finishToken();
        }
        return this.getCurrentLocation();
    }
    
    public abstract long getEndingByteOffset();
    
    public abstract long getEndingCharOffset();
    
    public final String getInputPublicId() {
        return this._config.getPublicId();
    }
    
    public final String getInputSystemId() {
        return this._config.getSystemId();
    }
    
    public final PName getName() {
        return this._tokenName;
    }
    
    public final String getNamespacePrefix(final int n) {
        return this.findCurrNsDecl(n).getBinding().mPrefix;
    }
    
    public final String getNamespaceURI() {
        String s;
        if ((s = this._tokenName.getNsUri()) == null) {
            s = this._defaultNs.mURI;
        }
        return s;
    }
    
    public final String getNamespaceURI(final int n) {
        return this.findCurrNsDecl(n).getBinding().mURI;
    }
    
    @Override
    public String getNamespaceURI(String muri) {
        if (muri == null) {
            throw new IllegalArgumentException(ErrorConsts.ERR_NULL_ARG);
        }
        if (muri.length() == 0) {
            if ((muri = this._defaultNs.mURI) == null) {
                muri = "";
            }
            return muri;
        }
        if (muri.equals("xml")) {
            return "http://www.w3.org/XML/1998/namespace";
        }
        if (muri.equals("xmlns")) {
            return "http://www.w3.org/2000/xmlns/";
        }
        for (NsDeclaration nsDeclaration = this._lastNsDecl; nsDeclaration != null; nsDeclaration = nsDeclaration.getPrev()) {
            if (nsDeclaration.hasPrefix(muri)) {
                return nsDeclaration.getCurrNsURI();
            }
        }
        return null;
    }
    
    public final NamespaceContext getNonTransientNamespaceContext() {
        return this._lastNsContext = this._lastNsContext.reuseOrCreate(this._lastNsDecl);
    }
    
    public final int getNsCount() {
        if (this._currToken == 1) {
            return this._currNsCount;
        }
        final NsDeclaration lastNsDecl = this._lastNsDecl;
        int countDeclsOnLevel;
        if (lastNsDecl == null) {
            countDeclsOnLevel = 0;
        }
        else {
            countDeclsOnLevel = lastNsDecl.countDeclsOnLevel(this._depth);
        }
        return countDeclsOnLevel;
    }
    
    @Override
    public String getPrefix(final String s) {
        if (s == null) {
            throw new IllegalArgumentException(ErrorConsts.ERR_NULL_ARG);
        }
        if (s.equals("http://www.w3.org/XML/1998/namespace")) {
            return "xml";
        }
        if (s.equals("http://www.w3.org/2000/xmlns/")) {
            return "xmlns";
        }
        if (s.equals(this._defaultNs.mURI)) {
            return "";
        }
    Label_0111:
        for (NsDeclaration nsDeclaration = this._lastNsDecl; nsDeclaration != null; nsDeclaration = nsDeclaration.getPrev()) {
            if (nsDeclaration.hasNsURI(s)) {
                final String prefix = nsDeclaration.getPrefix();
                if (prefix != null) {
                    for (NsDeclaration nsDeclaration2 = this._lastNsDecl; nsDeclaration2 != nsDeclaration; nsDeclaration2 = nsDeclaration2.getPrev()) {
                        if (nsDeclaration2.hasPrefix(prefix)) {
                            continue Label_0111;
                        }
                    }
                    return prefix;
                }
            }
        }
        return null;
    }
    
    @Override
    public Iterator<String> getPrefixes(final String s) {
        if (s == null) {
            throw new IllegalArgumentException(ErrorConsts.ERR_NULL_ARG);
        }
        if (s.equals("http://www.w3.org/XML/1998/namespace")) {
            return new SingletonIterator("xml");
        }
        if (s.equals("http://www.w3.org/2000/xmlns/")) {
            return new SingletonIterator("xmlns");
        }
        ArrayList<String> list;
        if (s.equals(this._defaultNs.mURI)) {
            list = new ArrayList<String>();
            list.add("");
        }
        else {
            list = null;
        }
        ArrayList<String> list2;
    Label_0171:
        for (NsDeclaration nsDeclaration = this._lastNsDecl; nsDeclaration != null; nsDeclaration = nsDeclaration.getPrev(), list = list2) {
            list2 = list;
            if (nsDeclaration.hasNsURI(s)) {
                final String prefix = nsDeclaration.getPrefix();
                list2 = list;
                if (prefix != null) {
                    for (NsDeclaration nsDeclaration2 = this._lastNsDecl; nsDeclaration2 != nsDeclaration; nsDeclaration2 = nsDeclaration2.getPrev()) {
                        if (nsDeclaration2.hasPrefix(prefix)) {
                            list2 = list;
                            continue Label_0171;
                        }
                    }
                    if ((list2 = list) == null) {
                        list2 = new ArrayList<String>();
                    }
                    list2.add(prefix);
                }
            }
        }
        if (list == null) {
            return (Iterator<String>)EmptyIterator.getInstance();
        }
        if (list.size() == 1) {
            return new SingletonIterator(list.get(0));
        }
        return list.iterator();
    }
    
    public final QName getQName() {
        return this._tokenName.constructQName(this._defaultNs);
    }
    
    public final XMLStreamLocation2 getStartLocation() {
        return (XMLStreamLocation2)LocationImpl.fromZeroBased(this._config.getPublicId(), this._config.getSystemId(), this._startRawOffset, (int)this._startRow, (int)this._startColumn);
    }
    
    public abstract long getStartingByteOffset();
    
    public abstract long getStartingCharOffset();
    
    public final int getText(final Writer writer, final boolean b) {
        if (this._tokenIncomplete) {
            this.finishToken();
        }
        try {
            return this._textBuilder.rawContentsTo(writer);
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    public final String getText() {
        if (this._tokenIncomplete) {
            this.finishToken();
        }
        return this._textBuilder.contentsAsString();
    }
    
    public final int getTextCharacters(final int n, final char[] array, final int n2, final int n3) {
        if (this._tokenIncomplete) {
            this.finishToken();
        }
        return this._textBuilder.contentsToArray(n, array, n2, n3);
    }
    
    public final char[] getTextCharacters() {
        if (this._tokenIncomplete) {
            this.finishToken();
        }
        return this._textBuilder.getTextBuffer();
    }
    
    public final int getTextLength() {
        if (this._tokenIncomplete) {
            this.finishToken();
        }
        return this._textBuilder.size();
    }
    
    public char handleInvalidXmlChar(final int n) {
        final IllegalCharHandler illegalCharHandler = this._config.getIllegalCharHandler();
        if (illegalCharHandler != null) {
            return illegalCharHandler.convertIllegalChar(n);
        }
        final char c = (char)n;
        if (c == '\0') {
            this.throwNullChar();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Illegal XML character (");
        sb.append(XmlChars.getCharDesc(c));
        sb.append(")");
        String s;
        final String str = s = sb.toString();
        if (this._xml11) {
            s = str;
            if (n < 32) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(str);
                sb2.append(" [note: in XML 1.1, it could be included via entity expansion]");
                s = sb2.toString();
            }
        }
        this.reportInputProblem(s);
        return c;
    }
    
    public final boolean hasEmptyStack() {
        return this._depth == 0;
    }
    
    public final boolean isAttrSpecified(final int n) {
        return true;
    }
    
    public final boolean isEmptyTag() {
        return this._isEmptyTag;
    }
    
    public final boolean isTextWhitespace() {
        if (this._tokenIncomplete) {
            this.finishToken();
        }
        return this._textBuilder.isAllWhitespace();
    }
    
    public abstract boolean loadMore();
    
    public final void loadMoreGuaranteed() {
        if (!this.loadMore()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unexpected end-of-input when trying to parse ");
            sb.append(ErrorConsts.tokenTypeDesc(this._currToken));
            this.reportInputProblem(sb.toString());
        }
    }
    
    public final void loadMoreGuaranteed(final int n) {
        if (!this.loadMore()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unexpected end-of-input when trying to parse ");
            sb.append(ErrorConsts.tokenTypeDesc(n));
            this.reportInputProblem(sb.toString());
        }
    }
    
    public abstract int nextFromProlog(final boolean p0);
    
    public abstract int nextFromTree();
    
    public void reportDoubleHyphenInComments() {
        this.reportInputProblem("String '--' not allowed in comment (missing '>'?)");
    }
    
    public void reportDuplicateNsDecl(String string) {
        if (string == null) {
            string = "Duplicate namespace declaration for the default namespace";
        }
        else {
            final StringBuilder sb = new StringBuilder();
            sb.append("Duplicate namespace declaration for prefix '");
            sb.append(string);
            sb.append("'");
            string = sb.toString();
        }
        this.reportInputProblem(string);
    }
    
    public void reportEntityOverflow() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Illegal character entity: value higher than max allowed (0x");
        sb.append(Integer.toHexString(1114111));
        sb.append(")");
        this.reportInputProblem(sb.toString());
    }
    
    public void reportEofInName(final char[] array, final int n) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Unexpected end-of-input in name (parsing ");
        sb.append(ErrorConsts.tokenTypeDesc(this._currToken));
        sb.append(")");
        this.reportInputProblem(sb.toString());
    }
    
    public void reportIllegalCDataEnd() {
        this.reportInputProblem("String ']]>' not allowed in textual content, except as the end marker of CDATA section");
    }
    
    public void reportIllegalNsDecl(final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Illegal namespace declaration: can not re-bind prefix '");
        sb.append(str);
        sb.append("'");
        this.reportInputProblem(sb.toString());
    }
    
    public void reportIllegalNsDecl(final String str, final String str2) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Illegal namespace declaration: can not bind URI '");
        sb.append(str2);
        sb.append("' to prefix other than '");
        sb.append(str);
        sb.append("'");
        this.reportInputProblem(sb.toString());
    }
    
    public void reportInputProblem(final String s) {
        throw new WFCException(s, (Location)this.getCurrentLocation());
    }
    
    public void reportInvalidNameChar(final int n, final int n2) {
        if (n == 58) {
            this.reportInputProblem("Invalid colon in name: at most one colon allowed in element/attribute names, and none in PI target or entity names");
        }
        if (n2 == 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid name start character (0x");
            sb.append(Integer.toHexString(n));
            sb.append(")");
            this.reportInputProblem(sb.toString());
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Invalid name character (0x");
        sb2.append(Integer.toHexString(n));
        sb2.append(")");
        this.reportInputProblem(sb2.toString());
    }
    
    public void reportInvalidNsIndex(final int i) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Illegal namespace declaration index, ");
        sb.append(i);
        sb.append(", current START_ELEMENT/END_ELEMENT has ");
        sb.append(this.getNsCount());
        sb.append(" declarations");
        throw new IndexOutOfBoundsException(sb.toString());
    }
    
    public void reportInvalidXmlChar(final int n) {
        if (n == 0) {
            this.reportInputProblem("Invalid null character");
        }
        if (n < 32) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid white space character (0x");
            sb.append(Integer.toHexString(n));
            sb.append(")");
            this.reportInputProblem(sb.toString());
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Invalid xml content character (0x");
        sb2.append(Integer.toHexString(n));
        sb2.append(")");
        this.reportInputProblem(sb2.toString());
    }
    
    public void reportMissingPISpace(final int n) {
        this.throwUnexpectedChar(n, ": expected either white space, or closing '?>'");
    }
    
    public void reportMultipleColonsInName() {
        this.reportInputProblem("Multiple colons not allowed in names");
    }
    
    public void reportPrologProblem(final boolean b, final String str) {
        String str2;
        if (b) {
            str2 = ErrorConsts.SUFFIX_IN_PROLOG;
        }
        else {
            str2 = ErrorConsts.SUFFIX_IN_EPILOG;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(str2);
        sb.append(": ");
        sb.append(str);
        this.reportInputProblem(sb.toString());
    }
    
    public void reportPrologUnexpChar(final boolean b, final int n, String string) {
        String s;
        if (b) {
            s = ErrorConsts.SUFFIX_IN_PROLOG;
        }
        else {
            s = ErrorConsts.SUFFIX_IN_EPILOG;
        }
        if (string == null) {
            string = s;
            if (n == 38) {
                final StringBuilder sb = new StringBuilder();
                sb.append(s);
                sb.append("; no entities allowed");
                this.throwUnexpectedChar(n, sb.toString());
                string = s;
            }
        }
        else {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(s);
            sb2.append(string);
            string = sb2.toString();
        }
        this.throwUnexpectedChar(n, string);
    }
    
    public void reportPrologUnexpElement(final boolean b, final int n) {
        int n2 = n;
        if (n < 0) {
            n2 = (n & 0x7FFFF);
        }
        if (n2 == 47) {
            if (b) {
                this.reportInputProblem("Unexpected end element in prolog: malformed XML document, expected root element");
            }
            this.reportInputProblem("Unexpected end element in epilog: malformed XML document (unbalanced start/end tags?)");
        }
        if (n2 < 32) {
            String str;
            if (b) {
                str = ErrorConsts.SUFFIX_IN_PROLOG;
            }
            else {
                str = ErrorConsts.SUFFIX_IN_EPILOG;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Unrecognized directive ");
            sb.append(str);
            this.throwUnexpectedChar(n2, sb.toString());
        }
        this.reportInputProblem("Second root element in content: malformed XML document, only one allowed");
    }
    
    public void reportTreeUnexpChar(final int n, final String str) {
        String str2 = ErrorConsts.SUFFIX_IN_TREE;
        if (str != null) {
            final StringBuilder sb = new StringBuilder();
            sb.append(str2);
            sb.append(str);
            str2 = sb.toString();
        }
        this.throwUnexpectedChar(n, str2);
    }
    
    public void reportUnboundPrefix(final PName pName, final boolean b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Unbound namespace prefix '");
        sb.append(pName.getPrefix());
        sb.append("' (for ");
        String str;
        if (b) {
            str = "attribute";
        }
        else {
            str = "element";
        }
        sb.append(str);
        sb.append(" name '");
        sb.append(pName.getPrefixedName());
        sb.append("')");
        this.reportInputProblem(sb.toString());
    }
    
    public void reportUnexpandedEntityInAttr(final PName pName, final boolean b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Unexpanded ENTITY_REFERENCE (");
        sb.append(this._tokenName);
        sb.append(") in ");
        String str;
        if (b) {
            str = "namespace declaration";
        }
        else {
            str = "attribute value";
        }
        sb.append(str);
        this.reportInputProblem(sb.toString());
    }
    
    public void reportUnexpectedEndTag(final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Unexpected end tag: expected </");
        sb.append(str);
        sb.append(">");
        this.reportInputProblem(sb.toString());
    }
    
    public final void resetForDecoding(final Base64Variant base64Variant, final CharArrayBase64Decoder charArrayBase64Decoder, final boolean b) {
        if (this._tokenIncomplete) {
            this.finishToken();
        }
        this._textBuilder.resetForBinaryDecode(base64Variant, charArrayBase64Decoder, b);
    }
    
    public abstract void skipCData();
    
    public abstract boolean skipCharacters();
    
    public abstract boolean skipCoalescedText();
    
    public abstract void skipComment();
    
    public abstract void skipPI();
    
    public abstract void skipSpace();
    
    public final boolean skipToken() {
        this._tokenIncomplete = false;
        final int currToken = this._currToken;
        if (currToken != 3) {
            if (currToken != 4) {
                if (currToken != 5) {
                    if (currToken != 6) {
                        if (currToken != 11) {
                            if (currToken != 12) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("Internal error, unexpected incomplete token type ");
                                sb.append(ErrorConsts.tokenTypeDesc(this._currToken));
                                throw new Error(sb.toString());
                            }
                            this.skipCData();
                            if (this._cfgCoalescing) {
                                this.skipCoalescedText();
                                if (this._entityPending) {
                                    this._currToken = 9;
                                    return true;
                                }
                            }
                        }
                        else {
                            this.finishDTD(false);
                        }
                    }
                    else {
                        this.skipSpace();
                    }
                }
                else {
                    this.skipComment();
                }
            }
            else {
                if (this.skipCharacters()) {
                    this._currToken = 9;
                    return true;
                }
                if (this._cfgCoalescing && this.skipCoalescedText()) {
                    this._currToken = 9;
                    return true;
                }
            }
        }
        else {
            this.skipPI();
        }
        return false;
    }
    
    public void throwInvalidSpace(final int n) {
        final char c = (char)n;
        if (c == '\0') {
            this.throwNullChar();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Illegal character (");
        sb.append(XmlChars.getCharDesc(c));
        sb.append(")");
        String s;
        final String str = s = sb.toString();
        if (this._xml11) {
            s = str;
            if (n < 32) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(str);
                sb2.append(" [note: in XML 1.1, it could be included via entity expansion]");
                s = sb2.toString();
            }
        }
        this.reportInputProblem(s);
    }
    
    public void throwNullChar() {
        this.reportInputProblem("Illegal character (NULL, unicode 0) encountered: not valid in any content");
    }
    
    public void throwUnexpectedChar(int n, final String str) {
        if (n < 32 && n != 13 && n != 10 && n != 9) {
            this.throwInvalidSpace(n);
        }
        n = (char)n;
        final StringBuilder sb = new StringBuilder();
        sb.append("Unexpected character ");
        sb.append(XmlChars.getCharDesc(n));
        sb.append(str);
        this.reportInputProblem(sb.toString());
    }
    
    public final void verifyXmlChar(final int n) {
        if (n >= 55296) {
            if (n < 57344) {
                this.reportInvalidXmlChar(n);
            }
            if (n != 65534 && n != 65535) {
                return;
            }
        }
        else {
            if (n >= 32 || n == 10 || n == 13 || n == 9) {
                return;
            }
            if (this._xml11) {
                if (n != 0) {
                    return;
                }
            }
        }
        this.reportInvalidXmlChar(n);
    }
}

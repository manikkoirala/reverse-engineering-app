// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.in;

import javax.xml.namespace.QName;
import com.fasterxml.aalto.util.DataUtil;
import org.codehaus.stax2.typed.TypedValueDecoder;
import java.util.List;
import org.codehaus.stax2.ri.typed.CharArrayBase64Decoder;
import org.codehaus.stax2.typed.Base64Variant;
import java.text.MessageFormat;
import com.fasterxml.aalto.impl.ErrorConsts;
import javax.xml.stream.Location;
import org.codehaus.stax2.typed.TypedXMLStreamException;
import org.codehaus.stax2.ri.typed.ValueDecoderFactory$BaseArrayDecoder;
import org.codehaus.stax2.typed.TypedArrayDecoder;

public final class AttributeCollector
{
    private static final int DEFAULT_BUFFER_LENGTH = 120;
    private static final int DEFAULT_ENTRY_COUNT = 12;
    private static final int INT_SPACE = 32;
    private String _allAttrValues;
    private int _attrCount;
    protected int[] _attrMap;
    final ReaderConfig _config;
    private String _errorMsg;
    protected int _hashAreaSize;
    private PName[] _names;
    private boolean _needToResetValues;
    protected int _spillAreaEnd;
    private char[] _valueBuffer;
    private int[] _valueOffsets;
    
    public AttributeCollector(final ReaderConfig config) {
        this._names = null;
        this._valueBuffer = null;
        this._attrMap = null;
        this._valueOffsets = null;
        this._needToResetValues = true;
        this._errorMsg = null;
        this._allAttrValues = null;
        this._config = config;
        this._attrCount = 0;
    }
    
    private final boolean checkExpand(final TypedArrayDecoder typedArrayDecoder) {
        if (typedArrayDecoder instanceof ValueDecoderFactory$BaseArrayDecoder) {
            ((ValueDecoderFactory$BaseArrayDecoder)typedArrayDecoder).expand();
            return true;
        }
        return false;
    }
    
    private final int decodeValues(final TypedArrayDecoder typedArrayDecoder, final char[] value, int offset, final int n, final XmlScanner xmlScanner) {
        int n2 = 0;
        int n3 = 0;
    Label_0172:
        while (true) {
            n3 = n2;
            if (offset < n) {
                while (value[offset] <= ' ') {
                    if (++offset >= n) {
                        n3 = n2;
                        break Label_0172;
                    }
                }
                int n4;
                for (n4 = offset + 1; n4 < n && value[n4] > ' '; ++n4) {}
                n3 = n4 + 1;
                ++n2;
                try {
                    if (!typedArrayDecoder.decodeValue(value, offset, n4) || this.checkExpand(typedArrayDecoder)) {
                        final int n5 = offset;
                        offset = n3;
                        n3 = n5;
                        continue;
                    }
                    n3 = n2;
                }
                catch (final IllegalArgumentException ex) {
                    throw new TypedXMLStreamException(new String(value, offset, n3 - offset), ex.getMessage(), (Location)xmlScanner.getCurrentLocation(), ex);
                }
                break;
            }
            break;
        }
        return n3;
    }
    
    private static final boolean isSpace(final char c) {
        return c <= ' ';
    }
    
    private void noteDupAttr(final int i, final int j) {
        this._errorMsg = MessageFormat.format(ErrorConsts.ERR_WF_DUP_ATTRS, this._names[i].toString(), i, this._names[j].toString(), j);
    }
    
    public char[] continueValue() {
        return this._valueBuffer;
    }
    
    public byte[] decodeBinaryValue(int n, final Base64Variant base64Variant, final CharArrayBase64Decoder charArrayBase64Decoder, final XmlScanner xmlScanner) {
        if (n >= 0 && n < this._attrCount) {
            int n2;
            if (n == 0) {
                final int[] valueOffsets = this._valueOffsets;
                n = 0;
                n2 = valueOffsets[0];
            }
            else {
                final int[] valueOffsets2 = this._valueOffsets;
                final int n3 = valueOffsets2[n - 1];
                final int n4 = valueOffsets2[n];
                n = n3;
                n2 = n4;
            }
            charArrayBase64Decoder.init(base64Variant, true, this._valueBuffer, n, n2, (List)null);
            try {
                return charArrayBase64Decoder.decodeCompletely();
            }
            catch (final IllegalArgumentException ex) {
                throw new TypedXMLStreamException(new String(this._valueBuffer, n, n2 - n), ex.getMessage(), (Location)xmlScanner.getCurrentLocation(), ex);
            }
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid index ");
        sb.append(n);
        sb.append("; current element has only ");
        sb.append(this._attrCount);
        sb.append(" attributes");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public final void decodeValue(int i, final TypedValueDecoder typedValueDecoder) {
        if (i >= 0 && i < this._attrCount) {
            int n;
            if (i == 0) {
                n = this._valueOffsets[0];
                i = 0;
            }
            else {
                final int[] valueOffsets = this._valueOffsets;
                final int n2 = valueOffsets[i - 1];
                n = valueOffsets[i];
                i = n2;
            }
            final char[] valueBuffer = this._valueBuffer;
            while (i < n) {
                if (!isSpace(valueBuffer[i])) {
                    while (--n > i && isSpace(valueBuffer[n])) {}
                    typedValueDecoder.decode(valueBuffer, i, n + 1);
                    return;
                }
                ++i;
            }
            typedValueDecoder.handleEmptyValue();
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid index ");
        sb.append(i);
        sb.append("; current element has only ");
        sb.append(this._attrCount);
        sb.append(" attributes");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public final int decodeValues(int i, final TypedArrayDecoder typedArrayDecoder, final XmlScanner xmlScanner) {
        if (i >= 0 && i < this._attrCount) {
            int n;
            if (i == 0) {
                i = this._valueOffsets[0];
                n = 0;
            }
            else {
                final int[] valueOffsets = this._valueOffsets;
                n = valueOffsets[i - 1];
                i = valueOffsets[i];
            }
            return this.decodeValues(typedArrayDecoder, this._valueBuffer, n, i, xmlScanner);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid index ");
        sb.append(i);
        sb.append("; current element has only ");
        sb.append(this._attrCount);
        sb.append(" attributes");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public int findIndex(final String s, final String s2) {
        int i = this._hashAreaSize;
        if (i < 1) {
            for (int attrCount = this._attrCount, j = 0; j < attrCount; ++j) {
                if (this._names[j].boundEquals(s, s2)) {
                    return j;
                }
            }
            return -1;
        }
        final int boundHashCode = PName.boundHashCode(s, s2);
        int n = this._attrMap[i - 1 & boundHashCode];
        if (n > 0) {
            --n;
            if (this._names[n].boundEquals(s, s2)) {
                return n;
            }
            while (i < this._spillAreaEnd) {
                final int[] attrMap = this._attrMap;
                if (attrMap[i] == boundHashCode) {
                    final int n2 = attrMap[i + 1];
                    if (this._names[n2].boundEquals(s, s2)) {
                        return n2;
                    }
                }
                i += 2;
            }
        }
        return -1;
    }
    
    public final int finishLastValue(final int n) {
        if (this._needToResetValues) {
            return 0;
        }
        this._needToResetValues = true;
        final int attrCount = this._attrCount;
        this._valueOffsets[attrCount - 1] = n;
        if (attrCount < 3) {
            this._hashAreaSize = 0;
            if (attrCount == 2) {
                final PName[] names = this._names;
                if (names[0].boundEquals(names[1])) {
                    this.noteDupAttr(0, 1);
                    return -1;
                }
            }
            return attrCount;
        }
        return this.finishLastValue2();
    }
    
    public final int finishLastValue2() {
        final int attrCount = this._attrCount;
        final PName[] names = this._names;
        final int[] attrMap = this._attrMap;
        int i;
        for (i = 8; i < (attrCount >> 2) + attrCount; i += i) {}
        this._hashAreaSize = i;
        final int n = (i >> 4) + i;
        final int n2 = 0;
        int[] attrMap2;
        if (attrMap != null && attrMap.length >= n) {
            attrMap[6] = (attrMap[7] = 0);
            attrMap[4] = (attrMap[5] = 0);
            attrMap[2] = (attrMap[3] = 0);
            attrMap[attrMap[1] = 0] = 0;
            int n3 = 8;
            while (true) {
                attrMap2 = attrMap;
                if (n3 >= i) {
                    break;
                }
                attrMap[n3] = 0;
                ++n3;
            }
        }
        else {
            attrMap2 = new int[n];
        }
        int spillAreaEnd = i;
        int[] growArrayBy;
        for (int j = n2; j < attrCount; ++j, attrMap2 = growArrayBy) {
            final PName pName = names[j];
            final int boundHashCode = pName.boundHashCode();
            final int n4 = boundHashCode & i - 1;
            int n5 = attrMap2[n4];
            if (n5 == 0) {
                attrMap2[n4] = j + 1;
                growArrayBy = attrMap2;
            }
            else {
                --n5;
                if (names[n5].boundEquals(pName) && this._errorMsg == null) {
                    this.noteDupAttr(n5, j);
                }
                final int n6 = spillAreaEnd + 1;
                growArrayBy = attrMap2;
                if (n6 >= attrMap2.length) {
                    growArrayBy = DataUtil.growArrayBy(attrMap2, 8);
                }
                for (int k = i; k < spillAreaEnd; k += 2) {
                    if (growArrayBy[k] == boundHashCode) {
                        final int n7 = growArrayBy[k + 1];
                        if (names[n7].boundEquals(pName)) {
                            if (this._errorMsg == null) {
                                this.noteDupAttr(n7, j);
                                break;
                            }
                            break;
                        }
                    }
                }
                growArrayBy[spillAreaEnd] = boundHashCode;
                spillAreaEnd = n6 + 1;
                growArrayBy[n6] = j;
            }
        }
        this._spillAreaEnd = spillAreaEnd;
        this._attrMap = attrMap2;
        int n8;
        if (this._errorMsg == null) {
            n8 = attrCount;
        }
        else {
            n8 = -1;
        }
        return n8;
    }
    
    public final int getCount() {
        return this._attrCount;
    }
    
    public String getErrorMsg() {
        return this._errorMsg;
    }
    
    public final PName getName(final int n) {
        return this._names[n];
    }
    
    public final QName getQName(final int n) {
        return this._names[n].constructQName();
    }
    
    public String getValue(int n) {
        final int attrCount = this._attrCount;
        final String allAttrValues = this._allAttrValues;
        String s = "";
        if (allAttrValues == null) {
            final int count = this._valueOffsets[attrCount - 1];
            String allAttrValues2;
            if (count == 0) {
                allAttrValues2 = "";
            }
            else {
                allAttrValues2 = new String(this._valueBuffer, 0, count);
            }
            this._allAttrValues = allAttrValues2;
        }
        if (n != 0) {
            final int[] valueOffsets = this._valueOffsets;
            final int beginIndex = valueOffsets[n - 1];
            n = valueOffsets[n];
            if (beginIndex != n) {
                s = this._allAttrValues.substring(beginIndex, n);
            }
            return s;
        }
        if (attrCount == 1) {
            return this._allAttrValues;
        }
        n = this._valueOffsets[0];
        if (n != 0) {
            s = this._allAttrValues.substring(0, n);
        }
        return s;
    }
    
    public String getValue(String value, final String s) {
        final int index = this.findIndex(value, s);
        if (index >= 0) {
            value = this.getValue(index);
        }
        else {
            value = null;
        }
        return value;
    }
    
    public char[] startNewValue(final PName pName, int n) {
        final boolean needToResetValues = this._needToResetValues;
        final int n2 = 0;
        int i = 0;
        if (needToResetValues) {
            this._needToResetValues = false;
            this._attrCount = 0;
            this._allAttrValues = null;
            n = n2;
            if (this._valueBuffer == null) {
                this._names = new PName[12];
                this._valueBuffer = new char[120];
                this._valueOffsets = new int[12];
                n = n2;
            }
        }
        else {
            final int attrCount = this._attrCount;
            final int[] valueOffsets = this._valueOffsets;
            if (attrCount >= valueOffsets.length) {
                final PName[] names = this._names;
                final int length = valueOffsets.length;
                final int n3 = length + length;
                this._valueOffsets = new int[n3];
                this._names = new PName[n3];
                while (i < length) {
                    this._valueOffsets[i] = valueOffsets[i];
                    this._names[i] = names[i];
                    ++i;
                }
            }
            if (attrCount > 0) {
                this._valueOffsets[attrCount - 1] = n;
            }
            n = attrCount;
        }
        this._names[n] = pName;
        ++this._attrCount;
        return this._valueBuffer;
    }
    
    public char[] valueBufferFull() {
        final char[] valueBuffer = this._valueBuffer;
        return this._valueBuffer = DataUtil.growArrayBy(valueBuffer, valueBuffer.length);
    }
}

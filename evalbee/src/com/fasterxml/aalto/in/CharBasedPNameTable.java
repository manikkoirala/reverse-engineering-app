// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.in;

import com.fasterxml.aalto.util.NameTable;

public class CharBasedPNameTable extends NameTable
{
    protected static final float DEFAULT_FILL_FACTOR = 0.75f;
    static final int MIN_HASH_SIZE = 16;
    protected Bucket[] _buckets;
    protected boolean _dirty;
    protected int _indexMask;
    protected int _size;
    protected int _sizeThreshold;
    protected PNameC[] _symbols;
    
    public CharBasedPNameTable(final int i) {
        this._dirty = true;
        if (i >= 1) {
            int j;
            for (j = 16; j < i; j += j) {}
            this._symbols = new PNameC[j];
            this._buckets = new Bucket[j >> 1];
            this._indexMask = j - 1;
            this._size = 0;
            this._sizeThreshold = j * 3 + 3 >> 2;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Can not use negative/zero initial size: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public CharBasedPNameTable(final CharBasedPNameTable charBasedPNameTable) {
        this._symbols = charBasedPNameTable._symbols;
        this._buckets = charBasedPNameTable._buckets;
        this._size = charBasedPNameTable._size;
        this._sizeThreshold = charBasedPNameTable._sizeThreshold;
        this._indexMask = charBasedPNameTable._indexMask;
        this._dirty = false;
    }
    
    private void copyArrays() {
        final PNameC[] symbols = this._symbols;
        final int length = symbols.length;
        System.arraycopy(symbols, 0, this._symbols = new PNameC[length], 0, length);
        final Bucket[] buckets = this._buckets;
        final int length2 = buckets.length;
        System.arraycopy(buckets, 0, this._buckets = new Bucket[length2], 0, length2);
        this._dirty = true;
    }
    
    private void rehash() {
        final PNameC[] symbols = this._symbols;
        final int length = symbols.length;
        final int n = length + length;
        final Bucket[] buckets = this._buckets;
        this._symbols = new PNameC[n];
        this._buckets = new Bucket[n >> 1];
        this._indexMask = n - 1;
        final int sizeThreshold = this._sizeThreshold;
        this._sizeThreshold = sizeThreshold + sizeThreshold;
        final int n2 = 0;
        int i = 0;
        int n3 = 0;
        while (i < length) {
            final PNameC pNameC = symbols[i];
            int n4 = n3;
            if (pNameC != null) {
                n4 = n3 + 1;
                final int n5 = pNameC.getCustomHash() & this._indexMask;
                final PNameC[] symbols2 = this._symbols;
                if (symbols2[n5] == null) {
                    symbols2[n5] = pNameC;
                }
                else {
                    final int n6 = n5 >> 1;
                    final Bucket[] buckets2 = this._buckets;
                    buckets2[n6] = new Bucket(pNameC, buckets2[n6]);
                }
            }
            ++i;
            n3 = n4;
        }
        int j = n3;
        for (int k = n2; k < length >> 1; ++k) {
            for (Bucket next = buckets[k]; next != null; next = next.getNext()) {
                ++j;
                final PNameC symbol = next.getSymbol();
                final int n7 = symbol.getCustomHash() & this._indexMask;
                final PNameC[] symbols3 = this._symbols;
                if (symbols3[n7] == null) {
                    symbols3[n7] = symbol;
                }
                else {
                    final int n8 = n7 >> 1;
                    final Bucket[] buckets3 = this._buckets;
                    buckets3[n8] = new Bucket(symbol, buckets3[n8]);
                }
            }
        }
        if (j == this._size) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Internal error on SymbolTable.rehash(): had ");
        sb.append(this._size);
        sb.append(" entries; now have ");
        sb.append(j);
        sb.append(".");
        throw new Error(sb.toString());
    }
    
    public PNameC addSymbol(final char[] value, int offset, int count, int n) {
        final PNameC construct = PNameC.construct(new String(value, offset, count).intern(), n);
        offset = (this._indexMask & n);
        Label_0101: {
            if (this._symbols[offset] != null) {
                final int size = this._size;
                final int sizeThreshold = this._sizeThreshold;
                final int n2 = count = 0;
                if (size < sizeThreshold) {
                    break Label_0101;
                }
                this.rehash();
                n &= this._indexMask;
                count = n2;
                offset = n;
                if (this._symbols[n] != null) {
                    break Label_0101;
                }
                offset = n;
            }
            count = 1;
        }
        if (!this._dirty) {
            this.copyArrays();
        }
        ++this._size;
        if (count != 0) {
            this._symbols[offset] = construct;
        }
        else {
            offset >>= 1;
            final Bucket[] buckets = this._buckets;
            buckets[offset] = new Bucket(construct, buckets[offset]);
        }
        return construct;
    }
    
    public PNameC findSymbol(final char[] array, final int n, final int n2, final int n3) {
        final int n4 = this._indexMask & n3;
        final PNameC pNameC = this._symbols[n4];
        if (pNameC != null) {
            if (pNameC.equalsPName(array, n, n2, n3)) {
                return pNameC;
            }
            final Bucket bucket = this._buckets[n4 >> 1];
            if (bucket != null) {
                final PNameC find = bucket.find(array, n, n2, n3);
                if (find != null) {
                    return find;
                }
            }
        }
        return null;
    }
    
    @Override
    public boolean maybeDirty() {
        return this._dirty;
    }
    
    public void mergeFromChild(final CharBasedPNameTable charBasedPNameTable) {
        synchronized (this) {
            if (charBasedPNameTable.size() <= this.size()) {
                return;
            }
            this._symbols = charBasedPNameTable._symbols;
            this._buckets = charBasedPNameTable._buckets;
            this._size = charBasedPNameTable._size;
            this._sizeThreshold = charBasedPNameTable._sizeThreshold;
            this._indexMask = charBasedPNameTable._indexMask;
            this._dirty = false;
            charBasedPNameTable._dirty = false;
        }
    }
    
    @Override
    public int size() {
        return this._size;
    }
    
    public static final class Bucket
    {
        private final Bucket mNext;
        private final PNameC mSymbol;
        
        public Bucket(final PNameC mSymbol, final Bucket mNext) {
            this.mSymbol = mSymbol;
            this.mNext = mNext;
        }
        
        public PNameC find(final char[] array, final int n, final int n2, final int n3) {
            Bucket next = this;
            do {
                final PNameC mSymbol = next.mSymbol;
                if (mSymbol.equalsPName(array, n, n2, n3)) {
                    return mSymbol;
                }
            } while ((next = next.getNext()) != null);
            return null;
        }
        
        public Bucket getNext() {
            return this.mNext;
        }
        
        public PNameC getSymbol() {
            return this.mSymbol;
        }
    }
}

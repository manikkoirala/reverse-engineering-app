// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.in;

public final class NsDeclaration
{
    private final NsBinding mBinding;
    private final int mLevel;
    private final NsDeclaration mPrevDeclaration;
    private final String mPreviousURI;
    
    public NsDeclaration(final NsBinding mBinding, final String muri, final NsDeclaration mPrevDeclaration, final int mLevel) {
        this.mBinding = mBinding;
        this.mPrevDeclaration = mPrevDeclaration;
        this.mLevel = mLevel;
        this.mPreviousURI = mBinding.mURI;
        mBinding.mURI = muri;
    }
    
    public boolean alreadyDeclared(final String s, final int n) {
        if (this.mLevel >= n) {
            if (s == this.mBinding.mPrefix) {
                return true;
            }
            for (NsDeclaration nsDeclaration = this.mPrevDeclaration; nsDeclaration != null && nsDeclaration.mLevel >= n; nsDeclaration = nsDeclaration.mPrevDeclaration) {
                if (s == nsDeclaration.mBinding.mPrefix) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public int countDeclsOnLevel(final int n) {
        int n3;
        if (this.mLevel == n) {
            NsDeclaration nsDeclaration = this.mPrevDeclaration;
            int n2 = 1;
            while (true) {
                n3 = n2;
                if (nsDeclaration == null) {
                    break;
                }
                n3 = n2;
                if (nsDeclaration.mLevel != n) {
                    break;
                }
                ++n2;
                nsDeclaration = nsDeclaration.mPrevDeclaration;
            }
        }
        else {
            n3 = 0;
        }
        return n3;
    }
    
    public NsBinding getBinding() {
        return this.mBinding;
    }
    
    public String getCurrNsURI() {
        return this.mBinding.mURI;
    }
    
    public int getLevel() {
        return this.mLevel;
    }
    
    public String getPrefix() {
        return this.mBinding.mPrefix;
    }
    
    public NsDeclaration getPrev() {
        return this.mPrevDeclaration;
    }
    
    public boolean hasNsURI(final String s) {
        return s.equals(this.mBinding.mURI);
    }
    
    public boolean hasPrefix(final String s) {
        return s.equals(this.mBinding.mPrefix);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("[NS-DECL, prefix = <");
        sb.append(this.mBinding.mPrefix);
        sb.append(">, current URI <");
        sb.append(this.mBinding.mURI);
        sb.append(">, level ");
        sb.append(this.mLevel);
        sb.append(", prev URI <");
        sb.append(this.mPreviousURI);
        sb.append(">]");
        return sb.toString();
    }
    
    public NsDeclaration unbind() {
        this.mBinding.mURI = this.mPreviousURI;
        return this.mPrevDeclaration;
    }
}

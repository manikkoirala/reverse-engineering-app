// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.in;

public final class PName3 extends ByteBasedPName
{
    final int mQuad1;
    final int mQuad2;
    final int mQuad3;
    
    public PName3(final String s, final String s2, final String s3, final int n, final int mQuad1, final int mQuad2, final int mQuad3) {
        super(s, s2, s3, n);
        this.mQuad1 = mQuad1;
        this.mQuad2 = mQuad2;
        this.mQuad3 = mQuad3;
    }
    
    public PName3(final String s, final String s2, final String s3, final int n, final int[] array) {
        super(s, s2, s3, n);
        this.mQuad1 = array[0];
        this.mQuad2 = array[1];
        this.mQuad3 = array[2];
    }
    
    @Override
    public PName createBoundName(final NsBinding namespaceBinding) {
        final PName3 pName3 = new PName3(super._prefixedName, super._prefix, super._localName, super.mHash, this.mQuad1, this.mQuad2, this.mQuad3);
        pName3._namespaceBinding = namespaceBinding;
        return pName3;
    }
    
    @Override
    public boolean equals(final int n, final int n2) {
        return false;
    }
    
    @Override
    public boolean equals(final int[] array, final int n) {
        boolean b2;
        final boolean b = b2 = false;
        if (n == 3) {
            b2 = b;
            if (array[0] == this.mQuad1) {
                b2 = b;
                if (array[1] == this.mQuad2) {
                    b2 = b;
                    if (array[2] == this.mQuad3) {
                        b2 = true;
                    }
                }
            }
        }
        return b2;
    }
    
    @Override
    public int getFirstQuad() {
        return this.mQuad1;
    }
    
    @Override
    public int getLastQuad() {
        return this.mQuad3;
    }
    
    @Override
    public int getQuad(int n) {
        if (n < 2) {
            if (n == 0) {
                n = this.mQuad1;
            }
            else {
                n = this.mQuad2;
            }
            return n;
        }
        return this.mQuad3;
    }
    
    @Override
    public boolean hashEquals(final int n, final int n2, final int n3) {
        return false;
    }
    
    @Override
    public boolean hashEquals(final int n, final int[] array, final int n2) {
        final int mHash = super.mHash;
        boolean b2;
        final boolean b = b2 = false;
        if (n == mHash) {
            b2 = b;
            if (n2 == 3) {
                b2 = b;
                if (array[0] == this.mQuad1) {
                    b2 = b;
                    if (array[1] == this.mQuad2) {
                        b2 = b;
                        if (array[2] == this.mQuad3) {
                            b2 = true;
                        }
                    }
                }
            }
        }
        return b2;
    }
    
    @Override
    public int sizeInQuads() {
        return 3;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.graphbuilder.math;

public class ExpressionParseException extends RuntimeException
{
    private String descrip;
    private int index;
    
    public ExpressionParseException(final String descrip, final int index) {
        this.descrip = descrip;
        this.index = index;
    }
    
    public String getDescription() {
        return this.descrip;
    }
    
    public int getIndex() {
        return this.index;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("(");
        sb.append(this.index);
        sb.append(") ");
        sb.append(this.descrip);
        return sb.toString();
    }
}

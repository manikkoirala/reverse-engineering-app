// 
// Decompiled by Procyon v0.6.0
// 

package com.graphbuilder.curve;

import com.graphbuilder.math.ExpressionParseException;

public class ControlStringParseException extends RuntimeException
{
    private String descrip;
    private ExpressionParseException epe;
    private int fromIndex;
    private int toIndex;
    
    public ControlStringParseException(final String descrip) {
        this.fromIndex = -1;
        this.toIndex = -1;
        this.epe = null;
        this.descrip = descrip;
    }
    
    public ControlStringParseException(final String descrip, final int n) {
        this.epe = null;
        this.descrip = descrip;
        this.fromIndex = n;
        this.toIndex = n;
    }
    
    public ControlStringParseException(final String descrip, final int fromIndex, final int toIndex) {
        this.epe = null;
        this.descrip = descrip;
        this.fromIndex = fromIndex;
        this.toIndex = toIndex;
    }
    
    public ControlStringParseException(final String descrip, final int fromIndex, final int toIndex, final ExpressionParseException epe) {
        this.descrip = descrip;
        this.fromIndex = fromIndex;
        this.toIndex = toIndex;
        this.epe = epe;
    }
    
    public String getDescription() {
        return this.descrip;
    }
    
    public ExpressionParseException getExpressionParseException() {
        return this.epe;
    }
    
    public int getFromIndex() {
        return this.fromIndex;
    }
    
    public int getToIndex() {
        return this.toIndex;
    }
    
    @Override
    public String toString() {
        String string;
        if (this.epe != null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("\n");
            sb.append(this.epe.toString());
            string = sb.toString();
        }
        else {
            string = "";
        }
        final int fromIndex = this.fromIndex;
        StringBuilder sb2;
        if (fromIndex == -1 && this.toIndex == -1) {
            sb2 = new StringBuilder();
            sb2.append(this.descrip);
        }
        else {
            if (fromIndex == this.toIndex) {
                sb2 = new StringBuilder();
                sb2.append(this.descrip);
                sb2.append(" : [");
            }
            else {
                sb2 = new StringBuilder();
                sb2.append(this.descrip);
                sb2.append(" : [");
                sb2.append(this.fromIndex);
                sb2.append(", ");
            }
            sb2.append(this.toIndex);
            sb2.append("]");
        }
        sb2.append(string);
        return sb2.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.gson;

import java.math.BigDecimal;
import com.google.gson.stream.MalformedJsonException;
import com.google.gson.internal.LazilyParsedNumber;

public enum ToNumberPolicy implements vw1
{
    private static final ToNumberPolicy[] $VALUES;
    
    BIG_DECIMAL {
        @Override
        public BigDecimal readNumber(final rh0 rh0) {
            final String i0 = rh0.i0();
            try {
                return new BigDecimal(i0);
            }
            catch (final NumberFormatException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Cannot parse ");
                sb.append(i0);
                sb.append("; at path ");
                sb.append(rh0.j());
                throw new JsonParseException(sb.toString(), ex);
            }
        }
    }, 
    DOUBLE {
        @Override
        public Double readNumber(final rh0 rh0) {
            return rh0.E();
        }
    }, 
    LAZILY_PARSED_NUMBER {
        @Override
        public Number readNumber(final rh0 rh0) {
            return new LazilyParsedNumber(rh0.i0());
        }
    }, 
    LONG_OR_DOUBLE {
        @Override
        public Number readNumber(final rh0 rh0) {
            final String i0 = rh0.i0();
            try {
                return Long.parseLong(i0);
            }
            catch (final NumberFormatException ex) {
                try {
                    final Double value = Double.valueOf(i0);
                    if ((!value.isInfinite() && !value.isNaN()) || rh0.o()) {
                        return value;
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("JSON forbids NaN and infinities: ");
                    sb.append(value);
                    sb.append("; at path ");
                    sb.append(rh0.j());
                    throw new MalformedJsonException(sb.toString());
                }
                catch (final NumberFormatException ex2) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Cannot parse ");
                    sb2.append(i0);
                    sb2.append("; at path ");
                    sb2.append(rh0.j());
                    throw new JsonParseException(sb2.toString(), ex2);
                }
            }
        }
    };
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.gson;

public enum LongSerializationPolicy
{
    private static final LongSerializationPolicy[] $VALUES;
    
    DEFAULT {
        @Override
        public nh0 serialize(final Long n) {
            if (n == null) {
                return oh0.a;
            }
            return new qh0(n);
        }
    }, 
    STRING {
        @Override
        public nh0 serialize(final Long n) {
            if (n == null) {
                return oh0.a;
            }
            return new qh0(n.toString());
        }
    };
    
    public abstract nh0 serialize(final Long p0);
}

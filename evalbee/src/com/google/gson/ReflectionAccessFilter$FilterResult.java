// 
// Decompiled by Procyon v0.6.0
// 

package com.google.gson;

public enum ReflectionAccessFilter$FilterResult
{
    private static final ReflectionAccessFilter$FilterResult[] $VALUES;
    
    ALLOW, 
    BLOCK_ALL, 
    BLOCK_INACCESSIBLE, 
    INDECISIVE;
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.gson.reflect;

import java.util.Map;
import java.util.HashMap;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.TypeVariable;
import java.util.Objects;
import com.google.gson.internal.$Gson$Types;
import java.lang.reflect.Type;

public class TypeToken<T>
{
    private final int hashCode;
    private final Class<? super T> rawType;
    private final Type type;
    
    public TypeToken() {
        final Type typeTokenTypeArgument = this.getTypeTokenTypeArgument();
        this.type = typeTokenTypeArgument;
        this.rawType = $Gson$Types.k(typeTokenTypeArgument);
        this.hashCode = typeTokenTypeArgument.hashCode();
    }
    
    private TypeToken(Type b) {
        Objects.requireNonNull(b);
        b = $Gson$Types.b(b);
        this.type = b;
        this.rawType = $Gson$Types.k(b);
        this.hashCode = b.hashCode();
    }
    
    private static AssertionError buildUnexpectedTypeError(final Type type, final Class<?>... array) {
        final StringBuilder sb = new StringBuilder("Unexpected type. Expected one of: ");
        for (int length = array.length, i = 0; i < length; ++i) {
            sb.append(array[i].getName());
            sb.append(", ");
        }
        sb.append("but got: ");
        sb.append(type.getClass().getName());
        sb.append(", for type token: ");
        sb.append(type.toString());
        sb.append('.');
        return new AssertionError((Object)sb.toString());
    }
    
    public static <T> TypeToken<T> get(final Class<T> clazz) {
        return new TypeToken<T>(clazz);
    }
    
    public static TypeToken<?> get(final Type type) {
        return new TypeToken<Object>(type);
    }
    
    public static TypeToken<?> getArray(final Type type) {
        return new TypeToken<Object>($Gson$Types.a(type));
    }
    
    public static TypeToken<?> getParameterized(final Type obj, final Type... obj2) {
        Objects.requireNonNull(obj);
        Objects.requireNonNull(obj2);
        if (!(obj instanceof Class)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("rawType must be of type Class, but was ");
            sb.append(obj);
            throw new IllegalArgumentException(sb.toString());
        }
        final Class clazz = (Class)obj;
        final TypeVariable[] typeParameters = clazz.getTypeParameters();
        final int length = typeParameters.length;
        final int length2 = obj2.length;
        if (length2 == length) {
            for (int i = 0; i < length; ++i) {
                final Type obj3 = obj2[i];
                final Class k = $Gson$Types.k(obj3);
                final TypeVariable obj4 = typeParameters[i];
                final Type[] bounds = obj4.getBounds();
                for (int length3 = bounds.length, j = 0; j < length3; ++j) {
                    if (!$Gson$Types.k(bounds[j]).isAssignableFrom(k)) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Type argument ");
                        sb2.append(obj3);
                        sb2.append(" does not satisfy bounds for type variable ");
                        sb2.append(obj4);
                        sb2.append(" declared by ");
                        sb2.append(obj);
                        throw new IllegalArgumentException(sb2.toString());
                    }
                }
            }
            return new TypeToken<Object>($Gson$Types.n(null, obj, obj2));
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(clazz.getName());
        sb3.append(" requires ");
        sb3.append(length);
        sb3.append(" type arguments, but got ");
        sb3.append(length2);
        throw new IllegalArgumentException(sb3.toString());
    }
    
    private Type getTypeTokenTypeArgument() {
        final Type genericSuperclass = this.getClass().getGenericSuperclass();
        if (genericSuperclass instanceof ParameterizedType) {
            final ParameterizedType parameterizedType = (ParameterizedType)genericSuperclass;
            if (parameterizedType.getRawType() == TypeToken.class) {
                return $Gson$Types.b(parameterizedType.getActualTypeArguments()[0]);
            }
        }
        else if (genericSuperclass == TypeToken.class) {
            throw new IllegalStateException("TypeToken must be created with a type argument: new TypeToken<...>() {}; When using code shrinkers (ProGuard, R8, ...) make sure that generic signatures are preserved.");
        }
        throw new IllegalStateException("Must only create direct subclasses of TypeToken");
    }
    
    private static boolean isAssignableFrom(final Type type, final GenericArrayType genericArrayType) {
        final Type genericComponentType = genericArrayType.getGenericComponentType();
        if (genericComponentType instanceof ParameterizedType) {
            Type genericComponentType2;
            if (type instanceof GenericArrayType) {
                genericComponentType2 = ((GenericArrayType)type).getGenericComponentType();
            }
            else {
                genericComponentType2 = type;
                if (type instanceof Class) {
                    Class componentType = (Class)type;
                    while (true) {
                        genericComponentType2 = componentType;
                        if (!componentType.isArray()) {
                            break;
                        }
                        componentType = componentType.getComponentType();
                    }
                }
            }
            return isAssignableFrom(genericComponentType2, (ParameterizedType)genericComponentType, new HashMap<String, Type>());
        }
        return true;
    }
    
    private static boolean isAssignableFrom(final Type obj, final ParameterizedType parameterizedType, final Map<String, Type> map) {
        final int n = 0;
        if (obj == null) {
            return false;
        }
        if (parameterizedType.equals(obj)) {
            return true;
        }
        final Class k = $Gson$Types.k(obj);
        ParameterizedType parameterizedType2;
        if (obj instanceof ParameterizedType) {
            parameterizedType2 = (ParameterizedType)obj;
        }
        else {
            parameterizedType2 = null;
        }
        if (parameterizedType2 != null) {
            final Type[] actualTypeArguments = parameterizedType2.getActualTypeArguments();
            final TypeVariable[] typeParameters = k.getTypeParameters();
            for (int i = 0; i < actualTypeArguments.length; ++i) {
                Type type = actualTypeArguments[i];
                final TypeVariable typeVariable = typeParameters[i];
                while (type instanceof TypeVariable) {
                    type = map.get(((TypeVariable)type).getName());
                }
                map.put(typeVariable.getName(), type);
            }
            if (typeEquals(parameterizedType2, parameterizedType, map)) {
                return true;
            }
        }
        final Type[] genericInterfaces = k.getGenericInterfaces();
        for (int length = genericInterfaces.length, j = n; j < length; ++j) {
            if (isAssignableFrom(genericInterfaces[j], parameterizedType, new HashMap<String, Type>(map))) {
                return true;
            }
        }
        return isAssignableFrom(k.getGenericSuperclass(), parameterizedType, new HashMap<String, Type>(map));
    }
    
    private static boolean matches(final Type obj, final Type type, final Map<String, Type> map) {
        return type.equals(obj) || (obj instanceof TypeVariable && type.equals(map.get(((TypeVariable)obj).getName())));
    }
    
    private static boolean typeEquals(final ParameterizedType parameterizedType, final ParameterizedType parameterizedType2, final Map<String, Type> map) {
        if (parameterizedType.getRawType().equals(parameterizedType2.getRawType())) {
            final Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
            final Type[] actualTypeArguments2 = parameterizedType2.getActualTypeArguments();
            for (int i = 0; i < actualTypeArguments.length; ++i) {
                if (!matches(actualTypeArguments[i], actualTypeArguments2[i], map)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    @Override
    public final boolean equals(final Object o) {
        return o instanceof TypeToken && $Gson$Types.f(this.type, ((TypeToken)o).type);
    }
    
    public final Class<? super T> getRawType() {
        return this.rawType;
    }
    
    public final Type getType() {
        return this.type;
    }
    
    @Override
    public final int hashCode() {
        return this.hashCode;
    }
    
    @Deprecated
    public boolean isAssignableFrom(final TypeToken<?> typeToken) {
        return this.isAssignableFrom(typeToken.getType());
    }
    
    @Deprecated
    public boolean isAssignableFrom(final Class<?> clazz) {
        return this.isAssignableFrom((Type)clazz);
    }
    
    @Deprecated
    public boolean isAssignableFrom(final Type obj) {
        final boolean b = false;
        if (obj == null) {
            return false;
        }
        if (this.type.equals(obj)) {
            return true;
        }
        final Type type = this.type;
        if (type instanceof Class) {
            return this.rawType.isAssignableFrom($Gson$Types.k(obj));
        }
        if (type instanceof ParameterizedType) {
            return isAssignableFrom(obj, (ParameterizedType)type, new HashMap<String, Type>());
        }
        if (type instanceof GenericArrayType) {
            boolean b2 = b;
            if (this.rawType.isAssignableFrom($Gson$Types.k(obj))) {
                b2 = b;
                if (isAssignableFrom(obj, (GenericArrayType)this.type)) {
                    b2 = true;
                }
            }
            return b2;
        }
        throw buildUnexpectedTypeError(type, Class.class, ParameterizedType.class, GenericArrayType.class);
    }
    
    @Override
    public final String toString() {
        return $Gson$Types.t(this.type);
    }
}

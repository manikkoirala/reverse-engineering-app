// 
// Decompiled by Procyon v0.6.0
// 

package com.google.gson.internal;

import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;
import java.util.Iterator;
import java.util.AbstractSet;
import java.util.Set;
import java.util.Map;
import java.util.LinkedHashMap;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.util.Objects;
import java.util.Comparator;
import java.io.Serializable;
import java.util.AbstractMap;

public final class LinkedTreeMap<K, V> extends AbstractMap<K, V> implements Serializable
{
    static final boolean $assertionsDisabled = false;
    private static final Comparator<Comparable> NATURAL_ORDER;
    private final boolean allowNullValues;
    private final Comparator<? super K> comparator;
    private b entrySet;
    final e header;
    private c keySet;
    int modCount;
    e root;
    int size;
    
    static {
        NATURAL_ORDER = new Comparator() {
            public int a(final Comparable comparable, final Comparable comparable2) {
                return comparable.compareTo(comparable2);
            }
        };
    }
    
    public LinkedTreeMap() {
        this((Comparator)LinkedTreeMap.NATURAL_ORDER, true);
    }
    
    public LinkedTreeMap(Comparator<? super K> natural_ORDER, final boolean allowNullValues) {
        this.size = 0;
        this.modCount = 0;
        if (natural_ORDER == null) {
            natural_ORDER = LinkedTreeMap.NATURAL_ORDER;
        }
        this.comparator = (Comparator<? super K>)natural_ORDER;
        this.allowNullValues = allowNullValues;
        this.header = new e(allowNullValues);
    }
    
    public LinkedTreeMap(final boolean b) {
        this((Comparator)LinkedTreeMap.NATURAL_ORDER, b);
    }
    
    private boolean equal(final Object a, final Object b) {
        return Objects.equals(a, b);
    }
    
    private void readObject(final ObjectInputStream objectInputStream) {
        throw new InvalidObjectException("Deserialization is unsupported");
    }
    
    private void rebalance(e a, final boolean b) {
        while (a != null) {
            final e b2 = a.b;
            final e c = a.c;
            final int n = 0;
            final int n2 = 0;
            int i;
            if (b2 != null) {
                i = b2.i;
            }
            else {
                i = 0;
            }
            int j;
            if (c != null) {
                j = c.i;
            }
            else {
                j = 0;
            }
            final int n3 = i - j;
            if (n3 == -2) {
                final e b3 = c.b;
                final e c2 = c.c;
                int k;
                if (c2 != null) {
                    k = c2.i;
                }
                else {
                    k = 0;
                }
                int l = n2;
                if (b3 != null) {
                    l = b3.i;
                }
                final int n4 = l - k;
                if (n4 != -1) {
                    if (n4 != 0 || b) {
                        this.rotateRight(c);
                    }
                }
                this.rotateLeft(a);
                if (b) {
                    break;
                }
            }
            else if (n3 == 2) {
                final e b4 = b2.b;
                final e c3 = b2.c;
                int m;
                if (c3 != null) {
                    m = c3.i;
                }
                else {
                    m = 0;
                }
                int i2 = n;
                if (b4 != null) {
                    i2 = b4.i;
                }
                final int n5 = i2 - m;
                if (n5 != 1) {
                    if (n5 != 0 || b) {
                        this.rotateLeft(b2);
                    }
                }
                this.rotateRight(a);
                if (b) {
                    break;
                }
            }
            else if (n3 == 0) {
                a.i = i + 1;
                if (b) {
                    break;
                }
            }
            else {
                a.i = Math.max(i, j) + 1;
                if (!b) {
                    break;
                }
            }
            a = a.a;
        }
    }
    
    private void replaceInParent(final e e, final e root) {
        final e a = e.a;
        e.a = null;
        if (root != null) {
            root.a = a;
        }
        if (a != null) {
            if (a.b == e) {
                a.b = root;
            }
            else {
                a.c = root;
            }
        }
        else {
            this.root = root;
        }
    }
    
    private void rotateLeft(final e e) {
        final e b = e.b;
        final e c = e.c;
        final e b2 = c.b;
        final e c2 = c.c;
        e.c = b2;
        if (b2 != null) {
            b2.a = e;
        }
        this.replaceInParent(e, c);
        c.b = e;
        e.a = c;
        final int n = 0;
        int i;
        if (b != null) {
            i = b.i;
        }
        else {
            i = 0;
        }
        int j;
        if (b2 != null) {
            j = b2.i;
        }
        else {
            j = 0;
        }
        final int n2 = Math.max(i, j) + 1;
        e.i = n2;
        int k = n;
        if (c2 != null) {
            k = c2.i;
        }
        c.i = Math.max(n2, k) + 1;
    }
    
    private void rotateRight(final e e) {
        final e b = e.b;
        final e c = e.c;
        final e b2 = b.b;
        final e c2 = b.c;
        e.b = c2;
        if (c2 != null) {
            c2.a = e;
        }
        this.replaceInParent(e, b);
        b.c = e;
        e.a = b;
        final int n = 0;
        int i;
        if (c != null) {
            i = c.i;
        }
        else {
            i = 0;
        }
        int j;
        if (c2 != null) {
            j = c2.i;
        }
        else {
            j = 0;
        }
        final int n2 = Math.max(i, j) + 1;
        e.i = n2;
        int k = n;
        if (b2 != null) {
            k = b2.i;
        }
        b.i = Math.max(n2, k) + 1;
    }
    
    private Object writeReplace() {
        return new LinkedHashMap(this);
    }
    
    @Override
    public void clear() {
        this.root = null;
        this.size = 0;
        ++this.modCount;
        final e header = this.header;
        header.e = header;
        header.d = header;
    }
    
    @Override
    public boolean containsKey(final Object o) {
        return this.findByObject(o) != null;
    }
    
    @Override
    public Set<Entry<K, V>> entrySet() {
        b entrySet = this.entrySet;
        if (entrySet == null) {
            entrySet = new b();
            this.entrySet = entrySet;
        }
        return entrySet;
    }
    
    public e find(final K k, final boolean b) {
        final Comparator<? super K> comparator = this.comparator;
        e root = this.root;
        int n;
        if (root != null) {
            Comparable comparable;
            if (comparator == LinkedTreeMap.NATURAL_ORDER) {
                comparable = (Comparable)k;
            }
            else {
                comparable = null;
            }
            while (true) {
                final Object f = root.f;
                if (comparable != null) {
                    n = comparable.compareTo(f);
                }
                else {
                    n = comparator.compare(k, f);
                }
                if (n == 0) {
                    return root;
                }
                e e;
                if (n < 0) {
                    e = root.b;
                }
                else {
                    e = root.c;
                }
                if (e == null) {
                    break;
                }
                root = e;
            }
        }
        else {
            n = 0;
        }
        if (!b) {
            return null;
        }
        final e header = this.header;
        e c;
        if (root == null) {
            if (comparator == LinkedTreeMap.NATURAL_ORDER && !(k instanceof Comparable)) {
                final StringBuilder sb = new StringBuilder();
                sb.append(k.getClass().getName());
                sb.append(" is not Comparable");
                throw new ClassCastException(sb.toString());
            }
            c = new e(this.allowNullValues, root, k, header, header.e);
            this.root = c;
        }
        else {
            c = new e(this.allowNullValues, root, k, header, header.e);
            if (n < 0) {
                root.b = c;
            }
            else {
                root.c = c;
            }
            this.rebalance(root, true);
        }
        ++this.size;
        ++this.modCount;
        return c;
    }
    
    public e findByEntry(final Entry<?, ?> entry) {
        final e byObject = this.findByObject(entry.getKey());
        e e;
        if (byObject != null && this.equal(byObject.h, entry.getValue())) {
            e = byObject;
        }
        else {
            e = null;
        }
        return e;
    }
    
    public e findByObject(final Object o) {
        e find = null;
        if (o == null) {
            return find;
        }
        try {
            find = this.find(o, false);
            return find;
        }
        catch (final ClassCastException ex) {
            find = find;
            return find;
        }
    }
    
    @Override
    public V get(Object h) {
        final e byObject = this.findByObject(h);
        if (byObject != null) {
            h = byObject.h;
        }
        else {
            h = null;
        }
        return (V)h;
    }
    
    @Override
    public Set<K> keySet() {
        c keySet = this.keySet;
        if (keySet == null) {
            keySet = new c();
            this.keySet = keySet;
        }
        return keySet;
    }
    
    @Override
    public V put(final K k, final V h) {
        if (k == null) {
            throw new NullPointerException("key == null");
        }
        if (h == null && !this.allowNullValues) {
            throw new NullPointerException("value == null");
        }
        final e find = this.find(k, true);
        final Object h2 = find.h;
        find.h = h;
        return (V)h2;
    }
    
    @Override
    public V remove(Object h) {
        final e removeInternalByKey = this.removeInternalByKey(h);
        if (removeInternalByKey != null) {
            h = removeInternalByKey.h;
        }
        else {
            h = null;
        }
        return (V)h;
    }
    
    public void removeInternal(final e e, final boolean b) {
        if (b) {
            final e e2 = e.e;
            e2.d = e.d;
            e.d.e = e2;
        }
        final e b2 = e.b;
        final e c = e.c;
        final e a = e.a;
        int i = 0;
        if (b2 != null && c != null) {
            e e3;
            if (b2.i > c.i) {
                e3 = b2.b();
            }
            else {
                e3 = c.a();
            }
            this.removeInternal(e3, false);
            final e b3 = e.b;
            int j;
            if (b3 != null) {
                j = b3.i;
                e3.b = b3;
                b3.a = e3;
                e.b = null;
            }
            else {
                j = 0;
            }
            final e c2 = e.c;
            if (c2 != null) {
                i = c2.i;
                e3.c = c2;
                c2.a = e3;
                e.c = null;
            }
            e3.i = Math.max(j, i) + 1;
            this.replaceInParent(e, e3);
            return;
        }
        if (b2 != null) {
            this.replaceInParent(e, b2);
            e.b = null;
        }
        else if (c != null) {
            this.replaceInParent(e, c);
            e.c = null;
        }
        else {
            this.replaceInParent(e, null);
        }
        this.rebalance(a, false);
        --this.size;
        ++this.modCount;
    }
    
    public e removeInternalByKey(final Object o) {
        final e byObject = this.findByObject(o);
        if (byObject != null) {
            this.removeInternal(byObject, true);
        }
        return byObject;
    }
    
    @Override
    public int size() {
        return this.size;
    }
    
    public class b extends AbstractSet
    {
        public final LinkedTreeMap a;
        
        public b(final LinkedTreeMap a) {
            this.a = a;
        }
        
        @Override
        public void clear() {
            this.a.clear();
        }
        
        @Override
        public boolean contains(final Object o) {
            return o instanceof Entry && this.a.findByEntry((Entry<?, ?>)o) != null;
        }
        
        @Override
        public Iterator iterator() {
            return new d(this) {
                public final b e;
                
                public Entry c() {
                    return ((d)this).b();
                }
            };
        }
        
        @Override
        public boolean remove(final Object o) {
            if (!(o instanceof Entry)) {
                return false;
            }
            final e byEntry = this.a.findByEntry((Entry<?, ?>)o);
            if (byEntry == null) {
                return false;
            }
            this.a.removeInternal(byEntry, true);
            return true;
        }
        
        @Override
        public int size() {
            return this.a.size;
        }
    }
    
    public abstract class d implements Iterator
    {
        public e a;
        public e b;
        public int c;
        public final LinkedTreeMap d;
        
        public d(final LinkedTreeMap d) {
            this.d = d;
            this.a = d.header.d;
            this.b = null;
            this.c = d.modCount;
        }
        
        public final e b() {
            final e a = this.a;
            final LinkedTreeMap d = this.d;
            if (a == d.header) {
                throw new NoSuchElementException();
            }
            if (d.modCount == this.c) {
                this.a = a.d;
                return this.b = a;
            }
            throw new ConcurrentModificationException();
        }
        
        @Override
        public final boolean hasNext() {
            return this.a != this.d.header;
        }
        
        @Override
        public final void remove() {
            final e b = this.b;
            if (b != null) {
                this.d.removeInternal(b, true);
                this.b = null;
                this.c = this.d.modCount;
                return;
            }
            throw new IllegalStateException();
        }
    }
    
    public final class c extends AbstractSet
    {
        public final LinkedTreeMap a;
        
        public c(final LinkedTreeMap a) {
            this.a = a;
        }
        
        @Override
        public void clear() {
            this.a.clear();
        }
        
        @Override
        public boolean contains(final Object o) {
            return this.a.containsKey(o);
        }
        
        @Override
        public Iterator iterator() {
            return new d(this) {
                public final c e;
                
                @Override
                public Object next() {
                    return ((d)this).b().f;
                }
            };
        }
        
        @Override
        public boolean remove(final Object o) {
            return this.a.removeInternalByKey(o) != null;
        }
        
        @Override
        public int size() {
            return this.a.size;
        }
    }
    
    public static final class e implements Entry
    {
        public e a;
        public e b;
        public e c;
        public e d;
        public e e;
        public final Object f;
        public final boolean g;
        public Object h;
        public int i;
        
        public e(final boolean g) {
            this.f = null;
            this.g = g;
            this.e = this;
            this.d = this;
        }
        
        public e(final boolean g, final e a, final Object f, final e d, final e e) {
            this.a = a;
            this.f = f;
            this.g = g;
            this.i = 1;
            this.d = d;
            this.e = e;
            e.d = this;
            d.e = this;
        }
        
        public e a() {
            e b = this.b;
            e e = this;
            while (b != null) {
                final e b2 = b.b;
                e = b;
                b = b2;
            }
            return e;
        }
        
        public e b() {
            e c = this.c;
            e e = this;
            while (c != null) {
                final e c2 = c.c;
                e = c;
                c = c2;
            }
            return e;
        }
        
        @Override
        public boolean equals(Object value) {
            final boolean b = value instanceof Entry;
            boolean b3;
            final boolean b2 = b3 = false;
            if (b) {
                final Entry entry = (Entry)value;
                final Object f = this.f;
                if (f == null) {
                    b3 = b2;
                    if (entry.getKey() != null) {
                        return b3;
                    }
                }
                else {
                    b3 = b2;
                    if (!f.equals(entry.getKey())) {
                        return b3;
                    }
                }
                final Object h = this.h;
                value = entry.getValue();
                if (h == null) {
                    b3 = b2;
                    if (value != null) {
                        return b3;
                    }
                }
                else {
                    b3 = b2;
                    if (!h.equals(value)) {
                        return b3;
                    }
                }
                b3 = true;
            }
            return b3;
        }
        
        @Override
        public Object getKey() {
            return this.f;
        }
        
        @Override
        public Object getValue() {
            return this.h;
        }
        
        @Override
        public int hashCode() {
            final Object f = this.f;
            int hashCode = 0;
            int hashCode2;
            if (f == null) {
                hashCode2 = 0;
            }
            else {
                hashCode2 = f.hashCode();
            }
            final Object h = this.h;
            if (h != null) {
                hashCode = h.hashCode();
            }
            return hashCode2 ^ hashCode;
        }
        
        @Override
        public Object setValue(final Object h) {
            if (h == null && !this.g) {
                throw new NullPointerException("value == null");
            }
            final Object h2 = this.h;
            this.h = h;
            return h2;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.f);
            sb.append("=");
            sb.append(this.h);
            return sb.toString();
        }
    }
}

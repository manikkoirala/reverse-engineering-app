// 
// Decompiled by Procyon v0.6.0
// 

package com.google.gson.internal;

import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.NoSuchElementException;
import java.lang.reflect.Array;
import java.util.Map;
import java.util.Properties;
import java.util.Collection;
import java.util.Arrays;
import java.util.Objects;
import java.lang.reflect.TypeVariable;
import java.io.Serializable;
import java.lang.reflect.WildcardType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Type;

public abstract class $Gson$Types
{
    public static final Type[] a;
    
    static {
        a = new Type[0];
    }
    
    public static GenericArrayType a(final Type type) {
        return new GenericArrayTypeImpl(type);
    }
    
    public static Type b(final Type type) {
        if (type instanceof Class) {
            Serializable s;
            final Class clazz = (Class)(s = (Class)type);
            if (clazz.isArray()) {
                s = new GenericArrayTypeImpl(b(clazz.getComponentType()));
            }
            return (Type)s;
        }
        if (type instanceof ParameterizedType) {
            final ParameterizedType parameterizedType = (ParameterizedType)type;
            return new ParameterizedTypeImpl(parameterizedType.getOwnerType(), parameterizedType.getRawType(), parameterizedType.getActualTypeArguments());
        }
        if (type instanceof GenericArrayType) {
            return new GenericArrayTypeImpl(((GenericArrayType)type).getGenericComponentType());
        }
        if (type instanceof WildcardType) {
            final WildcardType wildcardType = (WildcardType)type;
            return new WildcardTypeImpl(wildcardType.getUpperBounds(), wildcardType.getLowerBounds());
        }
        return type;
    }
    
    public static void c(final Type type) {
        a.a(!(type instanceof Class) || !((Class)type).isPrimitive());
    }
    
    public static Class d(final TypeVariable typeVariable) {
        final Class genericDeclaration = typeVariable.getGenericDeclaration();
        Class clazz;
        if (genericDeclaration instanceof Class) {
            clazz = genericDeclaration;
        }
        else {
            clazz = null;
        }
        return clazz;
    }
    
    public static boolean e(final Object a, final Object b) {
        return Objects.equals(a, b);
    }
    
    public static boolean f(final Type type, final Type obj) {
        final boolean b = true;
        boolean b2 = true;
        final boolean b3 = true;
        if (type == obj) {
            return true;
        }
        if (type instanceof Class) {
            return type.equals(obj);
        }
        if (type instanceof ParameterizedType) {
            if (!(obj instanceof ParameterizedType)) {
                return false;
            }
            final ParameterizedType parameterizedType = (ParameterizedType)type;
            final ParameterizedType parameterizedType2 = (ParameterizedType)obj;
            return e(parameterizedType.getOwnerType(), parameterizedType2.getOwnerType()) && parameterizedType.getRawType().equals(parameterizedType2.getRawType()) && Arrays.equals(parameterizedType.getActualTypeArguments(), parameterizedType2.getActualTypeArguments()) && b3;
        }
        else {
            if (type instanceof GenericArrayType) {
                return obj instanceof GenericArrayType && f(((GenericArrayType)type).getGenericComponentType(), ((GenericArrayType)obj).getGenericComponentType());
            }
            if (type instanceof WildcardType) {
                if (!(obj instanceof WildcardType)) {
                    return false;
                }
                final WildcardType wildcardType = (WildcardType)type;
                final WildcardType wildcardType2 = (WildcardType)obj;
                return Arrays.equals(wildcardType.getUpperBounds(), wildcardType2.getUpperBounds()) && Arrays.equals(wildcardType.getLowerBounds(), wildcardType2.getLowerBounds()) && b;
            }
            else {
                if (!(type instanceof TypeVariable)) {
                    return false;
                }
                if (!(obj instanceof TypeVariable)) {
                    return false;
                }
                final TypeVariable typeVariable = (TypeVariable)type;
                final TypeVariable typeVariable2 = (TypeVariable)obj;
                if (typeVariable.getGenericDeclaration() != typeVariable2.getGenericDeclaration() || !typeVariable.getName().equals(typeVariable2.getName())) {
                    b2 = false;
                }
                return b2;
            }
        }
    }
    
    public static Type g(Type type) {
        if (type instanceof GenericArrayType) {
            type = ((GenericArrayType)type).getGenericComponentType();
        }
        else {
            type = ((Class)type).getComponentType();
        }
        return type;
    }
    
    public static Type h(Type l, final Class clazz) {
        l = l(l, clazz, Collection.class);
        if (l instanceof ParameterizedType) {
            return ((ParameterizedType)l).getActualTypeArguments()[0];
        }
        return Object.class;
    }
    
    public static Type i(final Type type, Class clazz, final Class clazz2) {
        if (clazz2 == clazz) {
            return type;
        }
        if (clazz2.isInterface()) {
            final Class[] interfaces = clazz.getInterfaces();
            for (int length = interfaces.length, i = 0; i < length; ++i) {
                final Class clazz3 = interfaces[i];
                if (clazz3 == clazz2) {
                    return clazz.getGenericInterfaces()[i];
                }
                if (clazz2.isAssignableFrom(clazz3)) {
                    return i(clazz.getGenericInterfaces()[i], interfaces[i], clazz2);
                }
            }
        }
        if (!clazz.isInterface()) {
            while (clazz != Object.class) {
                final Class superclass = clazz.getSuperclass();
                if (superclass == clazz2) {
                    return clazz.getGenericSuperclass();
                }
                if (clazz2.isAssignableFrom(superclass)) {
                    return i(clazz.getGenericSuperclass(), superclass, clazz2);
                }
                clazz = superclass;
            }
        }
        return clazz2;
    }
    
    public static Type[] j(Type l, final Class clazz) {
        if (l == Properties.class) {
            return new Type[] { String.class, String.class };
        }
        l = l(l, clazz, Map.class);
        if (l instanceof ParameterizedType) {
            return ((ParameterizedType)l).getActualTypeArguments();
        }
        return new Type[] { Object.class, Object.class };
    }
    
    public static Class k(Type rawType) {
        if (rawType instanceof Class) {
            return (Class)rawType;
        }
        if (rawType instanceof ParameterizedType) {
            rawType = ((ParameterizedType)rawType).getRawType();
            a.a(rawType instanceof Class);
            return (Class)rawType;
        }
        if (rawType instanceof GenericArrayType) {
            return Array.newInstance(k(((GenericArrayType)rawType).getGenericComponentType()), 0).getClass();
        }
        if (rawType instanceof TypeVariable) {
            return Object.class;
        }
        if (rawType instanceof WildcardType) {
            return k(((WildcardType)rawType).getUpperBounds()[0]);
        }
        String name;
        if (rawType == null) {
            name = "null";
        }
        else {
            name = rawType.getClass().getName();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Expected a Class, ParameterizedType, or GenericArrayType, but <");
        sb.append(rawType);
        sb.append("> is of type ");
        sb.append(name);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static Type l(final Type type, final Class clazz, final Class clazz2) {
        Type type2 = type;
        if (type instanceof WildcardType) {
            type2 = ((WildcardType)type).getUpperBounds()[0];
        }
        a.a(clazz2.isAssignableFrom(clazz));
        return o(type2, clazz, i(type2, clazz, clazz2));
    }
    
    public static int m(final Object[] array, final Object o) {
        for (int length = array.length, i = 0; i < length; ++i) {
            if (o.equals(array[i])) {
                return i;
            }
        }
        throw new NoSuchElementException();
    }
    
    public static ParameterizedType n(final Type type, final Type type2, final Type... array) {
        return new ParameterizedTypeImpl(type, type2, array);
    }
    
    public static Type o(final Type type, final Class clazz, final Type type2) {
        return p(type, clazz, type2, new HashMap());
    }
    
    public static Type p(Type type, final Class clazz, Type o, final Map map) {
        TypeVariable typeVariable = null;
        Object o2 = o;
    Label_0162_Outer:
        while (true) {
            while (o2 instanceof TypeVariable) {
                final TypeVariable typeVariable2 = (TypeVariable)o2;
                o = map.get(typeVariable2);
                if (o != null) {
                    if (o != Void.TYPE) {
                        o2 = o;
                    }
                    return (Type)o2;
                }
                map.put((Class<?>)typeVariable2, Void.TYPE);
                TypeVariable typeVariable3;
                if ((typeVariable3 = typeVariable) == null) {
                    typeVariable3 = typeVariable2;
                }
                final Type q = q(type, clazz, typeVariable2);
                typeVariable = typeVariable3;
                if ((o2 = q) == typeVariable2) {
                    final TypeVariable typeVariable4 = typeVariable3;
                    o = q;
                    if (typeVariable4 != null) {
                        map.put(typeVariable4, o);
                    }
                    return (Type)o;
                }
            }
        Label_0167_Outer:
            while (true) {
                while (true) {
                    Label_0176: {
                        if (!(o2 instanceof Class)) {
                            break Label_0176;
                        }
                        o = o2;
                        if (!((Class)o).isArray()) {
                            break Label_0176;
                        }
                        final Class<?> componentType = ((Class)o).getComponentType();
                        if (e(componentType, type = p(type, clazz, componentType, map))) {
                            final TypeVariable typeVariable4 = typeVariable;
                            continue Label_0162_Outer;
                        }
                        final Object o3 = a(type);
                        final TypeVariable typeVariable4 = typeVariable;
                        o = o3;
                        continue Label_0162_Outer;
                    }
                    if (o2 instanceof GenericArrayType) {
                        o = o2;
                        final Type genericComponentType = ((GenericArrayType)o).getGenericComponentType();
                        if (e(genericComponentType, type = p(type, clazz, genericComponentType, map))) {
                            final TypeVariable typeVariable4 = typeVariable;
                            continue Label_0162_Outer;
                        }
                        continue Label_0167_Outer;
                    }
                    else {
                        final boolean b = o2 instanceof ParameterizedType;
                        int i = 0;
                        if (b) {
                            final ParameterizedType parameterizedType = (ParameterizedType)o2;
                            o = parameterizedType.getOwnerType();
                            final Type p4 = p(type, clazz, (Type)o, map);
                            int n = (e(p4, o) ^ true) ? 1 : 0;
                            Type[] actualTypeArguments;
                            int n2;
                            Type[] array;
                            for (actualTypeArguments = parameterizedType.getActualTypeArguments(); i < actualTypeArguments.length; ++i, n = n2, actualTypeArguments = array) {
                                final Type p5 = p(type, clazz, actualTypeArguments[i], map);
                                n2 = n;
                                array = actualTypeArguments;
                                if (!e(p5, actualTypeArguments[i])) {
                                    n2 = n;
                                    array = actualTypeArguments;
                                    if (n == 0) {
                                        array = actualTypeArguments.clone();
                                        n2 = 1;
                                    }
                                    array[i] = p5;
                                }
                            }
                            final TypeVariable typeVariable4 = typeVariable;
                            o = parameterizedType;
                            if (n != 0) {
                                final Object o3 = n(p4, parameterizedType.getRawType(), actualTypeArguments);
                                continue;
                            }
                            continue Label_0162_Outer;
                        }
                        else {
                            TypeVariable typeVariable4 = typeVariable;
                            o = o2;
                            if (!(o2 instanceof WildcardType)) {
                                continue Label_0162_Outer;
                            }
                            final WildcardType wildcardType = (WildcardType)o2;
                            final Type[] lowerBounds = wildcardType.getLowerBounds();
                            final Type[] upperBounds = wildcardType.getUpperBounds();
                            if (lowerBounds.length == 1) {
                                type = p(type, clazz, lowerBounds[0], map);
                                typeVariable4 = typeVariable;
                                o = wildcardType;
                                if (type != lowerBounds[0]) {
                                    o = s(type);
                                    typeVariable4 = typeVariable;
                                }
                                continue Label_0162_Outer;
                            }
                            else {
                                typeVariable4 = typeVariable;
                                o = wildcardType;
                                if (upperBounds.length != 1) {
                                    continue Label_0162_Outer;
                                }
                                type = p(type, clazz, upperBounds[0], map);
                                typeVariable4 = typeVariable;
                                o = wildcardType;
                                if (type != upperBounds[0]) {
                                    o = r(type);
                                    typeVariable4 = typeVariable;
                                }
                                continue Label_0162_Outer;
                            }
                        }
                    }
                    break;
                }
                break;
            }
            break;
        }
    }
    
    public static Type q(Type i, final Class clazz, final TypeVariable typeVariable) {
        final Class d = d(typeVariable);
        if (d == null) {
            return typeVariable;
        }
        i = i(i, clazz, d);
        if (i instanceof ParameterizedType) {
            return ((ParameterizedType)i).getActualTypeArguments()[m(d.getTypeParameters(), typeVariable)];
        }
        return typeVariable;
    }
    
    public static WildcardType r(final Type type) {
        Type[] upperBounds;
        if (type instanceof WildcardType) {
            upperBounds = ((WildcardType)type).getUpperBounds();
        }
        else {
            upperBounds = new Type[] { type };
        }
        return new WildcardTypeImpl(upperBounds, $Gson$Types.a);
    }
    
    public static WildcardType s(final Type type) {
        Type[] lowerBounds;
        if (type instanceof WildcardType) {
            lowerBounds = ((WildcardType)type).getLowerBounds();
        }
        else {
            lowerBounds = new Type[] { type };
        }
        return new WildcardTypeImpl(new Type[] { Object.class }, lowerBounds);
    }
    
    public static String t(final Type type) {
        String s;
        if (type instanceof Class) {
            s = ((Class)type).getName();
        }
        else {
            s = type.toString();
        }
        return s;
    }
    
    public static final class GenericArrayTypeImpl implements GenericArrayType, Serializable
    {
        private static final long serialVersionUID = 0L;
        private final Type componentType;
        
        public GenericArrayTypeImpl(final Type obj) {
            Objects.requireNonNull(obj);
            this.componentType = $Gson$Types.b(obj);
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof GenericArrayType && $Gson$Types.f(this, (Type)o);
        }
        
        @Override
        public Type getGenericComponentType() {
            return this.componentType;
        }
        
        @Override
        public int hashCode() {
            return this.componentType.hashCode();
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append($Gson$Types.t(this.componentType));
            sb.append("[]");
            return sb.toString();
        }
    }
    
    public static final class ParameterizedTypeImpl implements ParameterizedType, Serializable
    {
        private static final long serialVersionUID = 0L;
        private final Type ownerType;
        private final Type rawType;
        private final Type[] typeArguments;
        
        public ParameterizedTypeImpl(Type b, final Type obj, final Type... array) {
            Objects.requireNonNull(obj);
            final boolean b2 = obj instanceof Class;
            final int n = 0;
            if (b2) {
                final Class clazz = (Class)obj;
                final boolean static1 = Modifier.isStatic(clazz.getModifiers());
                final boolean b3 = true;
                final boolean b4 = static1 || clazz.getEnclosingClass() == null;
                boolean b5 = b3;
                if (b == null) {
                    b5 = (b4 && b3);
                }
                a.a(b5);
            }
            if (b == null) {
                b = null;
            }
            else {
                b = $Gson$Types.b(b);
            }
            this.ownerType = b;
            this.rawType = $Gson$Types.b(obj);
            final Type[] typeArguments = array.clone();
            this.typeArguments = typeArguments;
            for (int length = typeArguments.length, i = n; i < length; ++i) {
                Objects.requireNonNull(this.typeArguments[i]);
                $Gson$Types.c(this.typeArguments[i]);
                final Type[] typeArguments2 = this.typeArguments;
                typeArguments2[i] = $Gson$Types.b(typeArguments2[i]);
            }
        }
        
        private static int hashCodeOrZero(final Object o) {
            int hashCode;
            if (o != null) {
                hashCode = o.hashCode();
            }
            else {
                hashCode = 0;
            }
            return hashCode;
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof ParameterizedType && $Gson$Types.f(this, (Type)o);
        }
        
        @Override
        public Type[] getActualTypeArguments() {
            return this.typeArguments.clone();
        }
        
        @Override
        public Type getOwnerType() {
            return this.ownerType;
        }
        
        @Override
        public Type getRawType() {
            return this.rawType;
        }
        
        @Override
        public int hashCode() {
            return Arrays.hashCode(this.typeArguments) ^ this.rawType.hashCode() ^ hashCodeOrZero(this.ownerType);
        }
        
        @Override
        public String toString() {
            final int length = this.typeArguments.length;
            if (length == 0) {
                return $Gson$Types.t(this.rawType);
            }
            final StringBuilder sb = new StringBuilder((length + 1) * 30);
            sb.append($Gson$Types.t(this.rawType));
            sb.append("<");
            sb.append($Gson$Types.t(this.typeArguments[0]));
            for (int i = 1; i < length; ++i) {
                sb.append(", ");
                sb.append($Gson$Types.t(this.typeArguments[i]));
            }
            sb.append(">");
            return sb.toString();
        }
    }
    
    public static final class WildcardTypeImpl implements WildcardType, Serializable
    {
        private static final long serialVersionUID = 0L;
        private final Type lowerBound;
        private final Type upperBound;
        
        public WildcardTypeImpl(final Type[] array, final Type[] array2) {
            final int length = array2.length;
            final boolean b = true;
            a.a(length <= 1);
            a.a(array.length == 1);
            if (array2.length == 1) {
                Objects.requireNonNull(array2[0]);
                $Gson$Types.c(array2[0]);
                a.a(array[0] == Object.class && b);
                this.lowerBound = $Gson$Types.b(array2[0]);
                this.upperBound = Object.class;
            }
            else {
                Objects.requireNonNull(array[0]);
                $Gson$Types.c(array[0]);
                this.lowerBound = null;
                this.upperBound = $Gson$Types.b(array[0]);
            }
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof WildcardType && $Gson$Types.f(this, (Type)o);
        }
        
        @Override
        public Type[] getLowerBounds() {
            final Type lowerBound = this.lowerBound;
            Type[] a;
            if (lowerBound != null) {
                a = new Type[] { lowerBound };
            }
            else {
                a = $Gson$Types.a;
            }
            return a;
        }
        
        @Override
        public Type[] getUpperBounds() {
            return new Type[] { this.upperBound };
        }
        
        @Override
        public int hashCode() {
            final Type lowerBound = this.lowerBound;
            int n;
            if (lowerBound != null) {
                n = lowerBound.hashCode() + 31;
            }
            else {
                n = 1;
            }
            return n ^ this.upperBound.hashCode() + 31;
        }
        
        @Override
        public String toString() {
            StringBuilder sb;
            Type type;
            if (this.lowerBound != null) {
                sb = new StringBuilder();
                sb.append("? super ");
                type = this.lowerBound;
            }
            else {
                if (this.upperBound == Object.class) {
                    return "?";
                }
                sb = new StringBuilder();
                sb.append("? extends ");
                type = this.upperBound;
            }
            sb.append($Gson$Types.t(type));
            return sb.toString();
        }
    }
}

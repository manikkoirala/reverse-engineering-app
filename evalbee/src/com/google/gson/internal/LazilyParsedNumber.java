// 
// Decompiled by Procyon v0.6.0
// 

package com.google.gson.internal;

import java.math.BigDecimal;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;

public final class LazilyParsedNumber extends Number
{
    private final String value;
    
    public LazilyParsedNumber(final String value) {
        this.value = value;
    }
    
    private void readObject(final ObjectInputStream objectInputStream) {
        throw new InvalidObjectException("Deserialization is unsupported");
    }
    
    private Object writeReplace() {
        return new BigDecimal(this.value);
    }
    
    @Override
    public double doubleValue() {
        return Double.parseDouble(this.value);
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = true;
        if (this == o) {
            return true;
        }
        if (o instanceof LazilyParsedNumber) {
            final LazilyParsedNumber lazilyParsedNumber = (LazilyParsedNumber)o;
            final String value = this.value;
            final String value2 = lazilyParsedNumber.value;
            boolean b2 = b;
            if (value != value2) {
                b2 = (value.equals(value2) && b);
            }
            return b2;
        }
        return false;
    }
    
    @Override
    public float floatValue() {
        return Float.parseFloat(this.value);
    }
    
    @Override
    public int hashCode() {
        return this.value.hashCode();
    }
    
    @Override
    public int intValue() {
        try {
            return Integer.parseInt(this.value);
        }
        catch (final NumberFormatException ex) {
            try {
                return (int)Long.parseLong(this.value);
            }
            catch (final NumberFormatException ex2) {
                return new BigDecimal(this.value).intValue();
            }
        }
    }
    
    @Override
    public long longValue() {
        try {
            return Long.parseLong(this.value);
        }
        catch (final NumberFormatException ex) {
            return new BigDecimal(this.value).longValue();
        }
    }
    
    @Override
    public String toString() {
        return this.value;
    }
}

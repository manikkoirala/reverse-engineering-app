// 
// Decompiled by Procyon v0.6.0
// 

package com.google.gson;

import java.util.Locale;
import java.lang.reflect.Field;

public enum FieldNamingPolicy implements r00
{
    private static final FieldNamingPolicy[] $VALUES;
    
    IDENTITY {
        @Override
        public String translateName(final Field field) {
            return field.getName();
        }
    }, 
    LOWER_CASE_WITH_DASHES {
        @Override
        public String translateName(final Field field) {
            return FieldNamingPolicy.separateCamelCase(field.getName(), '-').toLowerCase(Locale.ENGLISH);
        }
    }, 
    LOWER_CASE_WITH_DOTS {
        @Override
        public String translateName(final Field field) {
            return FieldNamingPolicy.separateCamelCase(field.getName(), '.').toLowerCase(Locale.ENGLISH);
        }
    }, 
    LOWER_CASE_WITH_UNDERSCORES {
        @Override
        public String translateName(final Field field) {
            return FieldNamingPolicy.separateCamelCase(field.getName(), '_').toLowerCase(Locale.ENGLISH);
        }
    }, 
    UPPER_CAMEL_CASE {
        @Override
        public String translateName(final Field field) {
            return FieldNamingPolicy.upperCaseFirstLetter(field.getName());
        }
    }, 
    UPPER_CAMEL_CASE_WITH_SPACES {
        @Override
        public String translateName(final Field field) {
            return FieldNamingPolicy.upperCaseFirstLetter(FieldNamingPolicy.separateCamelCase(field.getName(), ' '));
        }
    }, 
    UPPER_CASE_WITH_UNDERSCORES {
        @Override
        public String translateName(final Field field) {
            return FieldNamingPolicy.separateCamelCase(field.getName(), '_').toUpperCase(Locale.ENGLISH);
        }
    };
    
    public static String separateCamelCase(final String s, final char c) {
        final StringBuilder sb = new StringBuilder();
        for (int length = s.length(), i = 0; i < length; ++i) {
            final char char1 = s.charAt(i);
            if (Character.isUpperCase(char1) && sb.length() != 0) {
                sb.append(c);
            }
            sb.append(char1);
        }
        return sb.toString();
    }
    
    public static String upperCaseFirstLetter(final String s) {
        final int length = s.length();
        int i = 0;
        while (i < length) {
            final char char1 = s.charAt(i);
            if (Character.isLetter(char1)) {
                if (Character.isUpperCase(char1)) {
                    return s;
                }
                final char upperCase = Character.toUpperCase(char1);
                if (i == 0) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(upperCase);
                    sb.append(s.substring(1));
                    return sb.toString();
                }
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(s.substring(0, i));
                sb2.append(upperCase);
                sb2.append(s.substring(i + 1));
                return sb2.toString();
            }
            else {
                ++i;
            }
        }
        return s;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.logging.type;

import com.google.protobuf.t;

public enum LogSeverity implements a
{
    private static final LogSeverity[] $VALUES;
    
    ALERT(700);
    
    public static final int ALERT_VALUE = 700;
    
    CRITICAL(600);
    
    public static final int CRITICAL_VALUE = 600;
    
    DEBUG(100);
    
    public static final int DEBUG_VALUE = 100;
    
    DEFAULT(0);
    
    public static final int DEFAULT_VALUE = 0;
    
    EMERGENCY(800);
    
    public static final int EMERGENCY_VALUE = 800;
    
    ERROR(500);
    
    public static final int ERROR_VALUE = 500;
    
    INFO(200);
    
    public static final int INFO_VALUE = 200;
    
    NOTICE(300);
    
    public static final int NOTICE_VALUE = 300;
    
    UNRECOGNIZED(-1), 
    WARNING(400);
    
    public static final int WARNING_VALUE = 400;
    private static final t.b internalValueMap;
    private final int value;
    
    static {
        internalValueMap = new t.b() {};
    }
    
    private LogSeverity(final int value) {
        this.value = value;
    }
    
    public static LogSeverity forNumber(final int n) {
        if (n == 0) {
            return LogSeverity.DEFAULT;
        }
        if (n == 100) {
            return LogSeverity.DEBUG;
        }
        if (n == 200) {
            return LogSeverity.INFO;
        }
        if (n == 300) {
            return LogSeverity.NOTICE;
        }
        if (n == 400) {
            return LogSeverity.WARNING;
        }
        if (n == 500) {
            return LogSeverity.ERROR;
        }
        if (n == 600) {
            return LogSeverity.CRITICAL;
        }
        if (n == 700) {
            return LogSeverity.ALERT;
        }
        if (n != 800) {
            return null;
        }
        return LogSeverity.EMERGENCY;
    }
    
    public static t.b internalGetValueMap() {
        return LogSeverity.internalValueMap;
    }
    
    public static c internalGetVerifier() {
        return b.a;
    }
    
    @Deprecated
    public static LogSeverity valueOf(final int n) {
        return forNumber(n);
    }
    
    @Override
    public final int getNumber() {
        if (this != LogSeverity.UNRECOGNIZED) {
            return this.value;
        }
        throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
    }
    
    public static final class b implements c
    {
        public static final c a;
        
        static {
            a = new b();
        }
        
        @Override
        public boolean a(final int n) {
            return LogSeverity.forNumber(n) != null;
        }
    }
}

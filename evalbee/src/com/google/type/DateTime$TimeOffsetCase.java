// 
// Decompiled by Procyon v0.6.0
// 

package com.google.type;

public enum DateTime$TimeOffsetCase
{
    private static final DateTime$TimeOffsetCase[] $VALUES;
    
    TIMEOFFSET_NOT_SET(0), 
    TIME_ZONE(9), 
    UTC_OFFSET(8);
    
    private final int value;
    
    private DateTime$TimeOffsetCase(final int value) {
        this.value = value;
    }
    
    public static DateTime$TimeOffsetCase forNumber(final int n) {
        if (n == 0) {
            return DateTime$TimeOffsetCase.TIMEOFFSET_NOT_SET;
        }
        if (n == 8) {
            return DateTime$TimeOffsetCase.UTC_OFFSET;
        }
        if (n != 9) {
            return null;
        }
        return DateTime$TimeOffsetCase.TIME_ZONE;
    }
    
    @Deprecated
    public static DateTime$TimeOffsetCase valueOf(final int n) {
        return forNumber(n);
    }
    
    public int getNumber() {
        return this.value;
    }
}

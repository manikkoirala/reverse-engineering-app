// 
// Decompiled by Procyon v0.6.0
// 

package com.google.type;

import com.google.protobuf.t;

public enum DayOfWeek implements a
{
    private static final DayOfWeek[] $VALUES;
    
    DAY_OF_WEEK_UNSPECIFIED(0);
    
    public static final int DAY_OF_WEEK_UNSPECIFIED_VALUE = 0;
    
    FRIDAY(5);
    
    public static final int FRIDAY_VALUE = 5;
    
    MONDAY(1);
    
    public static final int MONDAY_VALUE = 1;
    
    SATURDAY(6);
    
    public static final int SATURDAY_VALUE = 6;
    
    SUNDAY(7);
    
    public static final int SUNDAY_VALUE = 7;
    
    THURSDAY(4);
    
    public static final int THURSDAY_VALUE = 4;
    
    TUESDAY(2);
    
    public static final int TUESDAY_VALUE = 2;
    
    UNRECOGNIZED(-1), 
    WEDNESDAY(3);
    
    public static final int WEDNESDAY_VALUE = 3;
    private static final t.b internalValueMap;
    private final int value;
    
    static {
        internalValueMap = new t.b() {};
    }
    
    private DayOfWeek(final int value) {
        this.value = value;
    }
    
    public static DayOfWeek forNumber(final int n) {
        switch (n) {
            default: {
                return null;
            }
            case 7: {
                return DayOfWeek.SUNDAY;
            }
            case 6: {
                return DayOfWeek.SATURDAY;
            }
            case 5: {
                return DayOfWeek.FRIDAY;
            }
            case 4: {
                return DayOfWeek.THURSDAY;
            }
            case 3: {
                return DayOfWeek.WEDNESDAY;
            }
            case 2: {
                return DayOfWeek.TUESDAY;
            }
            case 1: {
                return DayOfWeek.MONDAY;
            }
            case 0: {
                return DayOfWeek.DAY_OF_WEEK_UNSPECIFIED;
            }
        }
    }
    
    public static t.b internalGetValueMap() {
        return DayOfWeek.internalValueMap;
    }
    
    public static c internalGetVerifier() {
        return b.a;
    }
    
    @Deprecated
    public static DayOfWeek valueOf(final int n) {
        return forNumber(n);
    }
    
    @Override
    public final int getNumber() {
        if (this != DayOfWeek.UNRECOGNIZED) {
            return this.value;
        }
        throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
    }
    
    public static final class b implements c
    {
        public static final c a;
        
        static {
            a = new b();
        }
        
        @Override
        public boolean a(final int n) {
            return DayOfWeek.forNumber(n) != null;
        }
    }
}

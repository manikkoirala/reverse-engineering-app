// 
// Decompiled by Procyon v0.6.0
// 

package com.google.type;

import com.google.protobuf.t;

public enum CalendarPeriod implements a
{
    private static final CalendarPeriod[] $VALUES;
    
    CALENDAR_PERIOD_UNSPECIFIED(0);
    
    public static final int CALENDAR_PERIOD_UNSPECIFIED_VALUE = 0;
    
    DAY(1);
    
    public static final int DAY_VALUE = 1;
    
    FORTNIGHT(3);
    
    public static final int FORTNIGHT_VALUE = 3;
    
    HALF(6);
    
    public static final int HALF_VALUE = 6;
    
    MONTH(4);
    
    public static final int MONTH_VALUE = 4;
    
    QUARTER(5);
    
    public static final int QUARTER_VALUE = 5;
    
    UNRECOGNIZED(-1), 
    WEEK(2);
    
    public static final int WEEK_VALUE = 2;
    
    YEAR(7);
    
    public static final int YEAR_VALUE = 7;
    private static final t.b internalValueMap;
    private final int value;
    
    static {
        internalValueMap = new t.b() {};
    }
    
    private CalendarPeriod(final int value) {
        this.value = value;
    }
    
    public static CalendarPeriod forNumber(final int n) {
        switch (n) {
            default: {
                return null;
            }
            case 7: {
                return CalendarPeriod.YEAR;
            }
            case 6: {
                return CalendarPeriod.HALF;
            }
            case 5: {
                return CalendarPeriod.QUARTER;
            }
            case 4: {
                return CalendarPeriod.MONTH;
            }
            case 3: {
                return CalendarPeriod.FORTNIGHT;
            }
            case 2: {
                return CalendarPeriod.WEEK;
            }
            case 1: {
                return CalendarPeriod.DAY;
            }
            case 0: {
                return CalendarPeriod.CALENDAR_PERIOD_UNSPECIFIED;
            }
        }
    }
    
    public static t.b internalGetValueMap() {
        return CalendarPeriod.internalValueMap;
    }
    
    public static c internalGetVerifier() {
        return b.a;
    }
    
    @Deprecated
    public static CalendarPeriod valueOf(final int n) {
        return forNumber(n);
    }
    
    @Override
    public final int getNumber() {
        if (this != CalendarPeriod.UNRECOGNIZED) {
            return this.value;
        }
        throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
    }
    
    public static final class b implements c
    {
        public static final c a;
        
        static {
            a = new b();
        }
        
        @Override
        public boolean a(final int n) {
            return CalendarPeriod.forNumber(n) != null;
        }
    }
}

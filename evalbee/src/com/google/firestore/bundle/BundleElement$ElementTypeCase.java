// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.bundle;

public enum BundleElement$ElementTypeCase
{
    private static final BundleElement$ElementTypeCase[] $VALUES;
    
    DOCUMENT(4), 
    DOCUMENT_METADATA(3), 
    ELEMENTTYPE_NOT_SET(0), 
    METADATA(1), 
    NAMED_QUERY(2);
    
    private final int value;
    
    private BundleElement$ElementTypeCase(final int value) {
        this.value = value;
    }
    
    public static BundleElement$ElementTypeCase forNumber(final int n) {
        if (n == 0) {
            return BundleElement$ElementTypeCase.ELEMENTTYPE_NOT_SET;
        }
        if (n == 1) {
            return BundleElement$ElementTypeCase.METADATA;
        }
        if (n == 2) {
            return BundleElement$ElementTypeCase.NAMED_QUERY;
        }
        if (n == 3) {
            return BundleElement$ElementTypeCase.DOCUMENT_METADATA;
        }
        if (n != 4) {
            return null;
        }
        return BundleElement$ElementTypeCase.DOCUMENT;
    }
    
    @Deprecated
    public static BundleElement$ElementTypeCase valueOf(final int n) {
        return forNumber(n);
    }
    
    public int getNumber() {
        return this.value;
    }
}

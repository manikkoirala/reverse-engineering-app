// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.bundle;

public enum BundledQuery$QueryTypeCase
{
    private static final BundledQuery$QueryTypeCase[] $VALUES;
    
    QUERYTYPE_NOT_SET(0), 
    STRUCTURED_QUERY(2);
    
    private final int value;
    
    private BundledQuery$QueryTypeCase(final int value) {
        this.value = value;
    }
    
    public static BundledQuery$QueryTypeCase forNumber(final int n) {
        if (n == 0) {
            return BundledQuery$QueryTypeCase.QUERYTYPE_NOT_SET;
        }
        if (n != 2) {
            return null;
        }
        return BundledQuery$QueryTypeCase.STRUCTURED_QUERY;
    }
    
    @Deprecated
    public static BundledQuery$QueryTypeCase valueOf(final int n) {
        return forNumber(n);
    }
    
    public int getNumber() {
        return this.value;
    }
}

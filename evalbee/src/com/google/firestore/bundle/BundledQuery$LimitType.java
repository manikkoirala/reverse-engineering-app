// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.bundle;

import com.google.protobuf.t;

public enum BundledQuery$LimitType implements a
{
    private static final BundledQuery$LimitType[] $VALUES;
    
    FIRST(0);
    
    public static final int FIRST_VALUE = 0;
    
    LAST(1);
    
    public static final int LAST_VALUE = 1;
    
    UNRECOGNIZED(-1);
    
    private static final t.b internalValueMap;
    private final int value;
    
    static {
        internalValueMap = new t.b() {};
    }
    
    private BundledQuery$LimitType(final int value) {
        this.value = value;
    }
    
    public static BundledQuery$LimitType forNumber(final int n) {
        if (n == 0) {
            return BundledQuery$LimitType.FIRST;
        }
        if (n != 1) {
            return null;
        }
        return BundledQuery$LimitType.LAST;
    }
    
    public static t.b internalGetValueMap() {
        return BundledQuery$LimitType.internalValueMap;
    }
    
    public static c internalGetVerifier() {
        return b.a;
    }
    
    @Deprecated
    public static BundledQuery$LimitType valueOf(final int n) {
        return forNumber(n);
    }
    
    @Override
    public final int getNumber() {
        if (this != BundledQuery$LimitType.UNRECOGNIZED) {
            return this.value;
        }
        throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
    }
    
    public static final class b implements c
    {
        public static final c a;
        
        static {
            a = new b();
        }
        
        @Override
        public boolean a(final int n) {
            return BundledQuery$LimitType.forNumber(n) != null;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

public enum TransactionOptions$ReadOnly$ConsistencySelectorCase
{
    private static final TransactionOptions$ReadOnly$ConsistencySelectorCase[] $VALUES;
    
    CONSISTENCYSELECTOR_NOT_SET(0), 
    READ_TIME(2);
    
    private final int value;
    
    private TransactionOptions$ReadOnly$ConsistencySelectorCase(final int value) {
        this.value = value;
    }
    
    public static TransactionOptions$ReadOnly$ConsistencySelectorCase forNumber(final int n) {
        if (n == 0) {
            return TransactionOptions$ReadOnly$ConsistencySelectorCase.CONSISTENCYSELECTOR_NOT_SET;
        }
        if (n != 2) {
            return null;
        }
        return TransactionOptions$ReadOnly$ConsistencySelectorCase.READ_TIME;
    }
    
    @Deprecated
    public static TransactionOptions$ReadOnly$ConsistencySelectorCase valueOf(final int n) {
        return forNumber(n);
    }
    
    public int getNumber() {
        return this.value;
    }
}

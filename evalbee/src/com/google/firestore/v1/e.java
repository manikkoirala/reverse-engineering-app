// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

import com.google.protobuf.WireFormat;
import com.google.protobuf.x;
import com.google.protobuf.a0;
import java.util.Collections;
import java.util.Map;
import com.google.protobuf.MapFieldLite;
import com.google.protobuf.j0;
import com.google.protobuf.GeneratedMessageLite;

public final class e extends GeneratedMessageLite implements xv0
{
    public static final int CREATE_TIME_FIELD_NUMBER = 3;
    private static final e DEFAULT_INSTANCE;
    public static final int FIELDS_FIELD_NUMBER = 2;
    public static final int NAME_FIELD_NUMBER = 1;
    private static volatile b31 PARSER;
    public static final int UPDATE_TIME_FIELD_NUMBER = 4;
    private j0 createTime_;
    private MapFieldLite<String, Value> fields_;
    private String name_;
    private j0 updateTime_;
    
    static {
        GeneratedMessageLite.V(e.class, DEFAULT_INSTANCE = new e());
    }
    
    public e() {
        this.fields_ = MapFieldLite.emptyMapField();
        this.name_ = "";
    }
    
    public static /* synthetic */ e Z() {
        return e.DEFAULT_INSTANCE;
    }
    
    public static e d0() {
        return e.DEFAULT_INSTANCE;
    }
    
    public static b k0() {
        return (b)e.DEFAULT_INSTANCE.u();
    }
    
    public Map e0() {
        return Collections.unmodifiableMap((Map<?, ?>)this.i0());
    }
    
    public final Map f0() {
        return this.j0();
    }
    
    public String g0() {
        return this.name_;
    }
    
    public j0 h0() {
        j0 j0;
        if ((j0 = this.updateTime_) == null) {
            j0 = com.google.protobuf.j0.c0();
        }
        return j0;
    }
    
    public final MapFieldLite i0() {
        return this.fields_;
    }
    
    public final MapFieldLite j0() {
        if (!this.fields_.isMutable()) {
            this.fields_ = this.fields_.mutableCopy();
        }
        return this.fields_;
    }
    
    public final void l0(final String name_) {
        name_.getClass();
        this.name_ = name_;
    }
    
    public final void m0(final j0 updateTime_) {
        updateTime_.getClass();
        this.updateTime_ = updateTime_;
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (e$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = e.PARSER) == null) {
                    synchronized (e.class) {
                        if (e.PARSER == null) {
                            e.PARSER = new GeneratedMessageLite.b(e.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return e.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(e.DEFAULT_INSTANCE, "\u0000\u0004\u0000\u0000\u0001\u0004\u0004\u0001\u0000\u0000\u0001\u0208\u00022\u0003\t\u0004\t", new Object[] { "name_", "fields_", c.a, "createTime_", "updateTime_" });
            }
            case 2: {
                return new b((e$a)null);
            }
            case 1: {
                return new e();
            }
        }
    }
    
    public static final class b extends a implements xv0
    {
        public b() {
            super(e.Z());
        }
        
        public b A(final Map map) {
            ((a)this).s();
            ((e)super.b).f0().putAll(map);
            return this;
        }
        
        public b B(final String s) {
            ((a)this).s();
            ((e)super.b).l0(s);
            return this;
        }
        
        public b C(final j0 j0) {
            ((a)this).s();
            ((e)super.b).m0(j0);
            return this;
        }
    }
    
    public abstract static final class c
    {
        public static final x a;
        
        static {
            a = x.d(WireFormat.FieldType.STRING, "", WireFormat.FieldType.MESSAGE, Value.o0());
        }
    }
}

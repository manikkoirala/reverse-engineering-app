// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

import java.util.List;
import com.google.protobuf.a;
import com.google.protobuf.a0;
import com.google.protobuf.r;
import com.google.protobuf.t;
import com.google.protobuf.GeneratedMessageLite;

public final class StructuredQuery extends GeneratedMessageLite implements xv0
{
    private static final StructuredQuery DEFAULT_INSTANCE;
    public static final int END_AT_FIELD_NUMBER = 8;
    public static final int FROM_FIELD_NUMBER = 2;
    public static final int LIMIT_FIELD_NUMBER = 5;
    public static final int OFFSET_FIELD_NUMBER = 6;
    public static final int ORDER_BY_FIELD_NUMBER = 4;
    private static volatile b31 PARSER;
    public static final int SELECT_FIELD_NUMBER = 1;
    public static final int START_AT_FIELD_NUMBER = 7;
    public static final int WHERE_FIELD_NUMBER = 3;
    private com.google.firestore.v1.d endAt_;
    private t.e from_;
    private r limit_;
    private int offset_;
    private t.e orderBy_;
    private f select_;
    private com.google.firestore.v1.d startAt_;
    private Filter where_;
    
    static {
        GeneratedMessageLite.V(StructuredQuery.class, DEFAULT_INSTANCE = new StructuredQuery());
    }
    
    public StructuredQuery() {
        this.from_ = GeneratedMessageLite.A();
        this.orderBy_ = GeneratedMessageLite.A();
    }
    
    public static /* synthetic */ StructuredQuery Z() {
        return StructuredQuery.DEFAULT_INSTANCE;
    }
    
    public static StructuredQuery k0() {
        return StructuredQuery.DEFAULT_INSTANCE;
    }
    
    public static b x0() {
        return (b)StructuredQuery.DEFAULT_INSTANCE.u();
    }
    
    public final void A0(final com.google.firestore.v1.d startAt_) {
        startAt_.getClass();
        this.startAt_ = startAt_;
    }
    
    public final void B0(final Filter where_) {
        where_.getClass();
        this.where_ = where_;
    }
    
    public final void g0(final c c) {
        c.getClass();
        this.i0();
        this.from_.add(c);
    }
    
    public final void h0(final e e) {
        e.getClass();
        this.j0();
        this.orderBy_.add(e);
    }
    
    public final void i0() {
        final t.e from_ = this.from_;
        if (!from_.h()) {
            this.from_ = GeneratedMessageLite.L(from_);
        }
    }
    
    public final void j0() {
        final t.e orderBy_ = this.orderBy_;
        if (!orderBy_.h()) {
            this.orderBy_ = GeneratedMessageLite.L(orderBy_);
        }
    }
    
    public com.google.firestore.v1.d l0() {
        com.google.firestore.v1.d d;
        if ((d = this.endAt_) == null) {
            d = com.google.firestore.v1.d.f0();
        }
        return d;
    }
    
    public c m0(final int n) {
        return this.from_.get(n);
    }
    
    public int n0() {
        return this.from_.size();
    }
    
    public r o0() {
        r r;
        if ((r = this.limit_) == null) {
            r = com.google.protobuf.r.b0();
        }
        return r;
    }
    
    public e p0(final int n) {
        return this.orderBy_.get(n);
    }
    
    public int q0() {
        return this.orderBy_.size();
    }
    
    public com.google.firestore.v1.d r0() {
        com.google.firestore.v1.d d;
        if ((d = this.startAt_) == null) {
            d = com.google.firestore.v1.d.f0();
        }
        return d;
    }
    
    public Filter s0() {
        Filter filter;
        if ((filter = this.where_) == null) {
            filter = Filter.e0();
        }
        return filter;
    }
    
    public boolean t0() {
        return this.endAt_ != null;
    }
    
    public boolean u0() {
        return this.limit_ != null;
    }
    
    public boolean v0() {
        return this.startAt_ != null;
    }
    
    public boolean w0() {
        return this.where_ != null;
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (StructuredQuery$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = StructuredQuery.PARSER) == null) {
                    synchronized (StructuredQuery.class) {
                        if (StructuredQuery.PARSER == null) {
                            StructuredQuery.PARSER = new GeneratedMessageLite.b(StructuredQuery.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return StructuredQuery.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(StructuredQuery.DEFAULT_INSTANCE, "\u0000\b\u0000\u0000\u0001\b\b\u0000\u0002\u0000\u0001\t\u0002\u001b\u0003\t\u0004\u001b\u0005\t\u0006\u0004\u0007\t\b\t", new Object[] { "select_", "from_", c.class, "where_", "orderBy_", e.class, "limit_", "offset_", "startAt_", "endAt_" });
            }
            case 2: {
                return new b((StructuredQuery$a)null);
            }
            case 1: {
                return new StructuredQuery();
            }
        }
    }
    
    public final void y0(final com.google.firestore.v1.d endAt_) {
        endAt_.getClass();
        this.endAt_ = endAt_;
    }
    
    public final void z0(final r limit_) {
        limit_.getClass();
        this.limit_ = limit_;
    }
    
    public static final class CompositeFilter extends GeneratedMessageLite implements xv0
    {
        private static final CompositeFilter DEFAULT_INSTANCE;
        public static final int FILTERS_FIELD_NUMBER = 2;
        public static final int OP_FIELD_NUMBER = 1;
        private static volatile b31 PARSER;
        private t.e filters_;
        private int op_;
        
        static {
            GeneratedMessageLite.V(CompositeFilter.class, DEFAULT_INSTANCE = new CompositeFilter());
        }
        
        public CompositeFilter() {
            this.filters_ = GeneratedMessageLite.A();
        }
        
        public static /* synthetic */ CompositeFilter Z() {
            return CompositeFilter.DEFAULT_INSTANCE;
        }
        
        public static CompositeFilter e0() {
            return CompositeFilter.DEFAULT_INSTANCE;
        }
        
        public static a h0() {
            return (a)CompositeFilter.DEFAULT_INSTANCE.u();
        }
        
        public final void c0(final Iterable iterable) {
            this.d0();
            com.google.protobuf.a.j(iterable, this.filters_);
        }
        
        public final void d0() {
            final t.e filters_ = this.filters_;
            if (!filters_.h()) {
                this.filters_ = GeneratedMessageLite.L(filters_);
            }
        }
        
        public List f0() {
            return this.filters_;
        }
        
        public Operator g0() {
            Operator operator;
            if ((operator = Operator.forNumber(this.op_)) == null) {
                operator = Operator.UNRECOGNIZED;
            }
            return operator;
        }
        
        public final void i0(final Operator operator) {
            this.op_ = operator.getNumber();
        }
        
        @Override
        public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
            switch (StructuredQuery$a.a[methodToInvoke.ordinal()]) {
                default: {
                    throw new UnsupportedOperationException();
                }
                case 7: {
                    return null;
                }
                case 6: {
                    return 1;
                }
                case 5: {
                    final b31 parser;
                    if ((parser = CompositeFilter.PARSER) == null) {
                        synchronized (CompositeFilter.class) {
                            if (CompositeFilter.PARSER == null) {
                                CompositeFilter.PARSER = new GeneratedMessageLite.b(CompositeFilter.DEFAULT_INSTANCE);
                            }
                        }
                    }
                    return parser;
                }
                case 4: {
                    return CompositeFilter.DEFAULT_INSTANCE;
                }
                case 3: {
                    return GeneratedMessageLite.N(CompositeFilter.DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0001\u0000\u0001\f\u0002\u001b", new Object[] { "op_", "filters_", Filter.class });
                }
                case 2: {
                    return new a((StructuredQuery$a)null);
                }
                case 1: {
                    return new CompositeFilter();
                }
            }
        }
        
        public enum Operator implements t.a
        {
            private static final Operator[] $VALUES;
            
            AND(1);
            
            public static final int AND_VALUE = 1;
            
            OPERATOR_UNSPECIFIED(0);
            
            public static final int OPERATOR_UNSPECIFIED_VALUE = 0;
            
            OR(2);
            
            public static final int OR_VALUE = 2;
            
            UNRECOGNIZED(-1);
            
            private static final t.b internalValueMap;
            private final int value;
            
            static {
                internalValueMap = new t.b() {};
            }
            
            private Operator(final int value) {
                this.value = value;
            }
            
            public static Operator forNumber(final int n) {
                if (n == 0) {
                    return Operator.OPERATOR_UNSPECIFIED;
                }
                if (n == 1) {
                    return Operator.AND;
                }
                if (n != 2) {
                    return null;
                }
                return Operator.OR;
            }
            
            public static t.b internalGetValueMap() {
                return Operator.internalValueMap;
            }
            
            public static t.c internalGetVerifier() {
                return b.a;
            }
            
            @Deprecated
            public static Operator valueOf(final int n) {
                return forNumber(n);
            }
            
            @Override
            public final int getNumber() {
                if (this != Operator.UNRECOGNIZED) {
                    return this.value;
                }
                throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
            }
            
            public static final class b implements t.c
            {
                public static final t.c a;
                
                static {
                    a = new b();
                }
                
                @Override
                public boolean a(final int n) {
                    return Operator.forNumber(n) != null;
                }
            }
        }
        
        public static final class a extends GeneratedMessageLite.a implements xv0
        {
            public a() {
                super(CompositeFilter.Z());
            }
            
            public a A(final Iterable iterable) {
                ((GeneratedMessageLite.a)this).s();
                ((CompositeFilter)super.b).c0(iterable);
                return this;
            }
            
            public a B(final Operator operator) {
                ((GeneratedMessageLite.a)this).s();
                ((CompositeFilter)super.b).i0(operator);
                return this;
            }
        }
    }
    
    public enum Direction implements t.a
    {
        private static final Direction[] $VALUES;
        
        ASCENDING(1);
        
        public static final int ASCENDING_VALUE = 1;
        
        DESCENDING(2);
        
        public static final int DESCENDING_VALUE = 2;
        
        DIRECTION_UNSPECIFIED(0);
        
        public static final int DIRECTION_UNSPECIFIED_VALUE = 0;
        
        UNRECOGNIZED(-1);
        
        private static final t.b internalValueMap;
        private final int value;
        
        static {
            internalValueMap = new t.b() {};
        }
        
        private Direction(final int value) {
            this.value = value;
        }
        
        public static Direction forNumber(final int n) {
            if (n == 0) {
                return Direction.DIRECTION_UNSPECIFIED;
            }
            if (n == 1) {
                return Direction.ASCENDING;
            }
            if (n != 2) {
                return null;
            }
            return Direction.DESCENDING;
        }
        
        public static t.b internalGetValueMap() {
            return Direction.internalValueMap;
        }
        
        public static t.c internalGetVerifier() {
            return b.a;
        }
        
        @Deprecated
        public static Direction valueOf(final int n) {
            return forNumber(n);
        }
        
        @Override
        public final int getNumber() {
            if (this != Direction.UNRECOGNIZED) {
                return this.value;
            }
            throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
        }
        
        public static final class b implements t.c
        {
            public static final t.c a;
            
            static {
                a = new b();
            }
            
            @Override
            public boolean a(final int n) {
                return Direction.forNumber(n) != null;
            }
        }
    }
    
    public static final class FieldFilter extends GeneratedMessageLite implements xv0
    {
        private static final FieldFilter DEFAULT_INSTANCE;
        public static final int FIELD_FIELD_NUMBER = 1;
        public static final int OP_FIELD_NUMBER = 2;
        private static volatile b31 PARSER;
        public static final int VALUE_FIELD_NUMBER = 3;
        private d field_;
        private int op_;
        private Value value_;
        
        static {
            GeneratedMessageLite.V(FieldFilter.class, DEFAULT_INSTANCE = new FieldFilter());
        }
        
        public static /* synthetic */ FieldFilter Z() {
            return FieldFilter.DEFAULT_INSTANCE;
        }
        
        public static FieldFilter d0() {
            return FieldFilter.DEFAULT_INSTANCE;
        }
        
        public static a h0() {
            return (a)FieldFilter.DEFAULT_INSTANCE.u();
        }
        
        public d e0() {
            xv0 xv0;
            if ((xv0 = this.field_) == null) {
                xv0 = d.b0();
            }
            return (d)xv0;
        }
        
        public Operator f0() {
            Operator operator;
            if ((operator = Operator.forNumber(this.op_)) == null) {
                operator = Operator.UNRECOGNIZED;
            }
            return operator;
        }
        
        public Value g0() {
            Value value;
            if ((value = this.value_) == null) {
                value = Value.o0();
            }
            return value;
        }
        
        public final void i0(final d field_) {
            field_.getClass();
            this.field_ = field_;
        }
        
        public final void j0(final Operator operator) {
            this.op_ = operator.getNumber();
        }
        
        public final void k0(final Value value_) {
            value_.getClass();
            this.value_ = value_;
        }
        
        @Override
        public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
            switch (StructuredQuery$a.a[methodToInvoke.ordinal()]) {
                default: {
                    throw new UnsupportedOperationException();
                }
                case 7: {
                    return null;
                }
                case 6: {
                    return 1;
                }
                case 5: {
                    final b31 parser;
                    if ((parser = FieldFilter.PARSER) == null) {
                        synchronized (FieldFilter.class) {
                            if (FieldFilter.PARSER == null) {
                                FieldFilter.PARSER = new GeneratedMessageLite.b(FieldFilter.DEFAULT_INSTANCE);
                            }
                        }
                    }
                    return parser;
                }
                case 4: {
                    return FieldFilter.DEFAULT_INSTANCE;
                }
                case 3: {
                    return GeneratedMessageLite.N(FieldFilter.DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001\t\u0002\f\u0003\t", new Object[] { "field_", "op_", "value_" });
                }
                case 2: {
                    return new a((StructuredQuery$a)null);
                }
                case 1: {
                    return new FieldFilter();
                }
            }
        }
        
        public enum Operator implements t.a
        {
            private static final Operator[] $VALUES;
            
            ARRAY_CONTAINS(7), 
            ARRAY_CONTAINS_ANY(9);
            
            public static final int ARRAY_CONTAINS_ANY_VALUE = 9;
            public static final int ARRAY_CONTAINS_VALUE = 7;
            
            EQUAL(5);
            
            public static final int EQUAL_VALUE = 5;
            
            GREATER_THAN(3), 
            GREATER_THAN_OR_EQUAL(4);
            
            public static final int GREATER_THAN_OR_EQUAL_VALUE = 4;
            public static final int GREATER_THAN_VALUE = 3;
            
            IN(8);
            
            public static final int IN_VALUE = 8;
            
            LESS_THAN(1), 
            LESS_THAN_OR_EQUAL(2);
            
            public static final int LESS_THAN_OR_EQUAL_VALUE = 2;
            public static final int LESS_THAN_VALUE = 1;
            
            NOT_EQUAL(6);
            
            public static final int NOT_EQUAL_VALUE = 6;
            
            NOT_IN(10);
            
            public static final int NOT_IN_VALUE = 10;
            
            OPERATOR_UNSPECIFIED(0);
            
            public static final int OPERATOR_UNSPECIFIED_VALUE = 0;
            
            UNRECOGNIZED(-1);
            
            private static final t.b internalValueMap;
            private final int value;
            
            static {
                internalValueMap = new t.b() {};
            }
            
            private Operator(final int value) {
                this.value = value;
            }
            
            public static Operator forNumber(final int n) {
                switch (n) {
                    default: {
                        return null;
                    }
                    case 10: {
                        return Operator.NOT_IN;
                    }
                    case 9: {
                        return Operator.ARRAY_CONTAINS_ANY;
                    }
                    case 8: {
                        return Operator.IN;
                    }
                    case 7: {
                        return Operator.ARRAY_CONTAINS;
                    }
                    case 6: {
                        return Operator.NOT_EQUAL;
                    }
                    case 5: {
                        return Operator.EQUAL;
                    }
                    case 4: {
                        return Operator.GREATER_THAN_OR_EQUAL;
                    }
                    case 3: {
                        return Operator.GREATER_THAN;
                    }
                    case 2: {
                        return Operator.LESS_THAN_OR_EQUAL;
                    }
                    case 1: {
                        return Operator.LESS_THAN;
                    }
                    case 0: {
                        return Operator.OPERATOR_UNSPECIFIED;
                    }
                }
            }
            
            public static t.b internalGetValueMap() {
                return Operator.internalValueMap;
            }
            
            public static t.c internalGetVerifier() {
                return b.a;
            }
            
            @Deprecated
            public static Operator valueOf(final int n) {
                return forNumber(n);
            }
            
            @Override
            public final int getNumber() {
                if (this != Operator.UNRECOGNIZED) {
                    return this.value;
                }
                throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
            }
            
            public static final class b implements t.c
            {
                public static final t.c a;
                
                static {
                    a = new b();
                }
                
                @Override
                public boolean a(final int n) {
                    return Operator.forNumber(n) != null;
                }
            }
        }
        
        public static final class a extends GeneratedMessageLite.a implements xv0
        {
            public a() {
                super(FieldFilter.Z());
            }
            
            public a A(final d d) {
                ((GeneratedMessageLite.a)this).s();
                ((FieldFilter)super.b).i0(d);
                return this;
            }
            
            public a B(final Operator operator) {
                ((GeneratedMessageLite.a)this).s();
                ((FieldFilter)super.b).j0(operator);
                return this;
            }
            
            public a C(final Value value) {
                ((GeneratedMessageLite.a)this).s();
                ((FieldFilter)super.b).k0(value);
                return this;
            }
        }
    }
    
    public static final class Filter extends GeneratedMessageLite implements xv0
    {
        public static final int COMPOSITE_FILTER_FIELD_NUMBER = 1;
        private static final Filter DEFAULT_INSTANCE;
        public static final int FIELD_FILTER_FIELD_NUMBER = 2;
        private static volatile b31 PARSER;
        public static final int UNARY_FILTER_FIELD_NUMBER = 3;
        private int filterTypeCase_;
        private Object filterType_;
        
        static {
            GeneratedMessageLite.V(Filter.class, DEFAULT_INSTANCE = new Filter());
        }
        
        public Filter() {
            this.filterTypeCase_ = 0;
        }
        
        public static /* synthetic */ Filter b0() {
            return Filter.DEFAULT_INSTANCE;
        }
        
        public static Filter e0() {
            return Filter.DEFAULT_INSTANCE;
        }
        
        public static a i0() {
            return (a)Filter.DEFAULT_INSTANCE.u();
        }
        
        public CompositeFilter d0() {
            if (this.filterTypeCase_ == 1) {
                return (CompositeFilter)this.filterType_;
            }
            return CompositeFilter.e0();
        }
        
        public FieldFilter f0() {
            if (this.filterTypeCase_ == 2) {
                return (FieldFilter)this.filterType_;
            }
            return FieldFilter.d0();
        }
        
        public FilterTypeCase g0() {
            return FilterTypeCase.forNumber(this.filterTypeCase_);
        }
        
        public UnaryFilter h0() {
            if (this.filterTypeCase_ == 3) {
                return (UnaryFilter)this.filterType_;
            }
            return UnaryFilter.c0();
        }
        
        public final void j0(final CompositeFilter filterType_) {
            filterType_.getClass();
            this.filterType_ = filterType_;
            this.filterTypeCase_ = 1;
        }
        
        public final void k0(final FieldFilter filterType_) {
            filterType_.getClass();
            this.filterType_ = filterType_;
            this.filterTypeCase_ = 2;
        }
        
        public final void l0(final UnaryFilter filterType_) {
            filterType_.getClass();
            this.filterType_ = filterType_;
            this.filterTypeCase_ = 3;
        }
        
        @Override
        public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
            switch (StructuredQuery$a.a[methodToInvoke.ordinal()]) {
                default: {
                    throw new UnsupportedOperationException();
                }
                case 7: {
                    return null;
                }
                case 6: {
                    return 1;
                }
                case 5: {
                    final b31 parser;
                    if ((parser = Filter.PARSER) == null) {
                        synchronized (Filter.class) {
                            if (Filter.PARSER == null) {
                                Filter.PARSER = new GeneratedMessageLite.b(Filter.DEFAULT_INSTANCE);
                            }
                        }
                    }
                    return parser;
                }
                case 4: {
                    return Filter.DEFAULT_INSTANCE;
                }
                case 3: {
                    return GeneratedMessageLite.N(Filter.DEFAULT_INSTANCE, "\u0000\u0003\u0001\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001<\u0000\u0002<\u0000\u0003<\u0000", new Object[] { "filterType_", "filterTypeCase_", CompositeFilter.class, FieldFilter.class, UnaryFilter.class });
                }
                case 2: {
                    return new a((StructuredQuery$a)null);
                }
                case 1: {
                    return new Filter();
                }
            }
        }
        
        public enum FilterTypeCase
        {
            private static final FilterTypeCase[] $VALUES;
            
            COMPOSITE_FILTER(1), 
            FIELD_FILTER(2), 
            FILTERTYPE_NOT_SET(0), 
            UNARY_FILTER(3);
            
            private final int value;
            
            private FilterTypeCase(final int value) {
                this.value = value;
            }
            
            public static FilterTypeCase forNumber(final int n) {
                if (n == 0) {
                    return FilterTypeCase.FILTERTYPE_NOT_SET;
                }
                if (n == 1) {
                    return FilterTypeCase.COMPOSITE_FILTER;
                }
                if (n == 2) {
                    return FilterTypeCase.FIELD_FILTER;
                }
                if (n != 3) {
                    return null;
                }
                return FilterTypeCase.UNARY_FILTER;
            }
            
            @Deprecated
            public static FilterTypeCase valueOf(final int n) {
                return forNumber(n);
            }
            
            public int getNumber() {
                return this.value;
            }
        }
        
        public static final class a extends GeneratedMessageLite.a implements xv0
        {
            public a() {
                super(Filter.b0());
            }
            
            public a A(final CompositeFilter.a a) {
                ((GeneratedMessageLite.a)this).s();
                ((Filter)super.b).j0((CompositeFilter)((GeneratedMessageLite.a)a).p());
                return this;
            }
            
            public a B(final FieldFilter.a a) {
                ((GeneratedMessageLite.a)this).s();
                ((Filter)super.b).k0((FieldFilter)((GeneratedMessageLite.a)a).p());
                return this;
            }
            
            public a C(final UnaryFilter.a a) {
                ((GeneratedMessageLite.a)this).s();
                ((Filter)super.b).l0((UnaryFilter)((GeneratedMessageLite.a)a).p());
                return this;
            }
        }
    }
    
    public static final class UnaryFilter extends GeneratedMessageLite implements xv0
    {
        private static final UnaryFilter DEFAULT_INSTANCE;
        public static final int FIELD_FIELD_NUMBER = 2;
        public static final int OP_FIELD_NUMBER = 1;
        private static volatile b31 PARSER;
        private int op_;
        private int operandTypeCase_;
        private Object operandType_;
        
        static {
            GeneratedMessageLite.V(UnaryFilter.class, DEFAULT_INSTANCE = new UnaryFilter());
        }
        
        public UnaryFilter() {
            this.operandTypeCase_ = 0;
        }
        
        public static /* synthetic */ UnaryFilter Z() {
            return UnaryFilter.DEFAULT_INSTANCE;
        }
        
        public static UnaryFilter c0() {
            return UnaryFilter.DEFAULT_INSTANCE;
        }
        
        public static a f0() {
            return (a)UnaryFilter.DEFAULT_INSTANCE.u();
        }
        
        public d d0() {
            if (this.operandTypeCase_ == 2) {
                return (d)this.operandType_;
            }
            return d.b0();
        }
        
        public Operator e0() {
            Operator operator;
            if ((operator = Operator.forNumber(this.op_)) == null) {
                operator = Operator.UNRECOGNIZED;
            }
            return operator;
        }
        
        public final void g0(final d operandType_) {
            operandType_.getClass();
            this.operandType_ = operandType_;
            this.operandTypeCase_ = 2;
        }
        
        public final void h0(final Operator operator) {
            this.op_ = operator.getNumber();
        }
        
        @Override
        public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
            switch (StructuredQuery$a.a[methodToInvoke.ordinal()]) {
                default: {
                    throw new UnsupportedOperationException();
                }
                case 7: {
                    return null;
                }
                case 6: {
                    return 1;
                }
                case 5: {
                    final b31 parser;
                    if ((parser = UnaryFilter.PARSER) == null) {
                        synchronized (UnaryFilter.class) {
                            if (UnaryFilter.PARSER == null) {
                                UnaryFilter.PARSER = new GeneratedMessageLite.b(UnaryFilter.DEFAULT_INSTANCE);
                            }
                        }
                    }
                    return parser;
                }
                case 4: {
                    return UnaryFilter.DEFAULT_INSTANCE;
                }
                case 3: {
                    return GeneratedMessageLite.N(UnaryFilter.DEFAULT_INSTANCE, "\u0000\u0002\u0001\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\f\u0002<\u0000", new Object[] { "operandType_", "operandTypeCase_", "op_", d.class });
                }
                case 2: {
                    return new a((StructuredQuery$a)null);
                }
                case 1: {
                    return new UnaryFilter();
                }
            }
        }
        
        public enum OperandTypeCase
        {
            private static final OperandTypeCase[] $VALUES;
            
            FIELD(2), 
            OPERANDTYPE_NOT_SET(0);
            
            private final int value;
            
            private OperandTypeCase(final int value) {
                this.value = value;
            }
            
            public static OperandTypeCase forNumber(final int n) {
                if (n == 0) {
                    return OperandTypeCase.OPERANDTYPE_NOT_SET;
                }
                if (n != 2) {
                    return null;
                }
                return OperandTypeCase.FIELD;
            }
            
            @Deprecated
            public static OperandTypeCase valueOf(final int n) {
                return forNumber(n);
            }
            
            public int getNumber() {
                return this.value;
            }
        }
        
        public enum Operator implements t.a
        {
            private static final Operator[] $VALUES;
            
            IS_NAN(2);
            
            public static final int IS_NAN_VALUE = 2;
            
            IS_NOT_NAN(4);
            
            public static final int IS_NOT_NAN_VALUE = 4;
            
            IS_NOT_NULL(5);
            
            public static final int IS_NOT_NULL_VALUE = 5;
            
            IS_NULL(3);
            
            public static final int IS_NULL_VALUE = 3;
            
            OPERATOR_UNSPECIFIED(0);
            
            public static final int OPERATOR_UNSPECIFIED_VALUE = 0;
            
            UNRECOGNIZED(-1);
            
            private static final t.b internalValueMap;
            private final int value;
            
            static {
                internalValueMap = new t.b() {};
            }
            
            private Operator(final int value) {
                this.value = value;
            }
            
            public static Operator forNumber(final int n) {
                if (n == 0) {
                    return Operator.OPERATOR_UNSPECIFIED;
                }
                if (n == 2) {
                    return Operator.IS_NAN;
                }
                if (n == 3) {
                    return Operator.IS_NULL;
                }
                if (n == 4) {
                    return Operator.IS_NOT_NAN;
                }
                if (n != 5) {
                    return null;
                }
                return Operator.IS_NOT_NULL;
            }
            
            public static t.b internalGetValueMap() {
                return Operator.internalValueMap;
            }
            
            public static t.c internalGetVerifier() {
                return b.a;
            }
            
            @Deprecated
            public static Operator valueOf(final int n) {
                return forNumber(n);
            }
            
            @Override
            public final int getNumber() {
                if (this != Operator.UNRECOGNIZED) {
                    return this.value;
                }
                throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
            }
            
            public static final class b implements t.c
            {
                public static final t.c a;
                
                static {
                    a = new b();
                }
                
                @Override
                public boolean a(final int n) {
                    return Operator.forNumber(n) != null;
                }
            }
        }
        
        public static final class a extends GeneratedMessageLite.a implements xv0
        {
            public a() {
                super(UnaryFilter.Z());
            }
            
            public a A(final d d) {
                ((GeneratedMessageLite.a)this).s();
                ((UnaryFilter)super.b).g0(d);
                return this;
            }
            
            public a B(final Operator operator) {
                ((GeneratedMessageLite.a)this).s();
                ((UnaryFilter)super.b).h0(operator);
                return this;
            }
        }
    }
    
    public static final class b extends GeneratedMessageLite.a implements xv0
    {
        public b() {
            super(StructuredQuery.Z());
        }
        
        public b A(final StructuredQuery.c.a a) {
            ((GeneratedMessageLite.a)this).s();
            ((StructuredQuery)super.b).g0((StructuredQuery.c)((GeneratedMessageLite.a)a).p());
            return this;
        }
        
        public b B(final e e) {
            ((GeneratedMessageLite.a)this).s();
            ((StructuredQuery)super.b).h0(e);
            return this;
        }
        
        public b C(final com.google.firestore.v1.d.b b) {
            ((GeneratedMessageLite.a)this).s();
            ((StructuredQuery)super.b).y0((com.google.firestore.v1.d)((GeneratedMessageLite.a)b).p());
            return this;
        }
        
        public b D(final r.b b) {
            ((GeneratedMessageLite.a)this).s();
            ((StructuredQuery)super.b).z0((r)((GeneratedMessageLite.a)b).p());
            return this;
        }
        
        public b E(final com.google.firestore.v1.d.b b) {
            ((GeneratedMessageLite.a)this).s();
            ((StructuredQuery)super.b).A0((com.google.firestore.v1.d)((GeneratedMessageLite.a)b).p());
            return this;
        }
        
        public b F(final Filter filter) {
            ((GeneratedMessageLite.a)this).s();
            ((StructuredQuery)super.b).B0(filter);
            return this;
        }
    }
    
    public static final class c extends GeneratedMessageLite implements xv0
    {
        public static final int ALL_DESCENDANTS_FIELD_NUMBER = 3;
        public static final int COLLECTION_ID_FIELD_NUMBER = 2;
        private static final c DEFAULT_INSTANCE;
        private static volatile b31 PARSER;
        private boolean allDescendants_;
        private String collectionId_;
        
        static {
            GeneratedMessageLite.V(c.class, DEFAULT_INSTANCE = new c());
        }
        
        public c() {
            this.collectionId_ = "";
        }
        
        public static /* synthetic */ c Z() {
            return c.DEFAULT_INSTANCE;
        }
        
        public static a e0() {
            return (a)c.DEFAULT_INSTANCE.u();
        }
        
        public boolean c0() {
            return this.allDescendants_;
        }
        
        public String d0() {
            return this.collectionId_;
        }
        
        public final void f0(final boolean allDescendants_) {
            this.allDescendants_ = allDescendants_;
        }
        
        public final void g0(final String collectionId_) {
            collectionId_.getClass();
            this.collectionId_ = collectionId_;
        }
        
        @Override
        public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
            switch (StructuredQuery$a.a[methodToInvoke.ordinal()]) {
                default: {
                    throw new UnsupportedOperationException();
                }
                case 7: {
                    return null;
                }
                case 6: {
                    return 1;
                }
                case 5: {
                    final b31 parser;
                    if ((parser = c.PARSER) == null) {
                        synchronized (c.class) {
                            if (c.PARSER == null) {
                                c.PARSER = new GeneratedMessageLite.b(c.DEFAULT_INSTANCE);
                            }
                        }
                    }
                    return parser;
                }
                case 4: {
                    return c.DEFAULT_INSTANCE;
                }
                case 3: {
                    return GeneratedMessageLite.N(c.DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0002\u0003\u0002\u0000\u0000\u0000\u0002\u0208\u0003\u0007", new Object[] { "collectionId_", "allDescendants_" });
                }
                case 2: {
                    return new a((StructuredQuery$a)null);
                }
                case 1: {
                    return new c();
                }
            }
        }
        
        public static final class a extends GeneratedMessageLite.a implements xv0
        {
            public a() {
                super(StructuredQuery.c.Z());
            }
            
            public a A(final boolean b) {
                ((GeneratedMessageLite.a)this).s();
                ((StructuredQuery.c)super.b).f0(b);
                return this;
            }
            
            public a B(final String s) {
                ((GeneratedMessageLite.a)this).s();
                ((StructuredQuery.c)super.b).g0(s);
                return this;
            }
        }
    }
    
    public static final class d extends GeneratedMessageLite implements xv0
    {
        private static final d DEFAULT_INSTANCE;
        public static final int FIELD_PATH_FIELD_NUMBER = 2;
        private static volatile b31 PARSER;
        private String fieldPath_;
        
        static {
            GeneratedMessageLite.V(d.class, DEFAULT_INSTANCE = new d());
        }
        
        public d() {
            this.fieldPath_ = "";
        }
        
        public static /* synthetic */ d Z() {
            return d.DEFAULT_INSTANCE;
        }
        
        public static d b0() {
            return d.DEFAULT_INSTANCE;
        }
        
        public static a d0() {
            return (a)d.DEFAULT_INSTANCE.u();
        }
        
        public String c0() {
            return this.fieldPath_;
        }
        
        public final void e0(final String fieldPath_) {
            fieldPath_.getClass();
            this.fieldPath_ = fieldPath_;
        }
        
        @Override
        public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
            switch (StructuredQuery$a.a[methodToInvoke.ordinal()]) {
                default: {
                    throw new UnsupportedOperationException();
                }
                case 7: {
                    return null;
                }
                case 6: {
                    return 1;
                }
                case 5: {
                    final b31 parser;
                    if ((parser = d.PARSER) == null) {
                        synchronized (d.class) {
                            if (d.PARSER == null) {
                                d.PARSER = new GeneratedMessageLite.b(d.DEFAULT_INSTANCE);
                            }
                        }
                    }
                    return parser;
                }
                case 4: {
                    return d.DEFAULT_INSTANCE;
                }
                case 3: {
                    return GeneratedMessageLite.N(d.DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0002\u0002\u0001\u0000\u0000\u0000\u0002\u0208", new Object[] { "fieldPath_" });
                }
                case 2: {
                    return new a((StructuredQuery$a)null);
                }
                case 1: {
                    return new d();
                }
            }
        }
        
        public static final class a extends GeneratedMessageLite.a implements xv0
        {
            public a() {
                super(d.Z());
            }
            
            public a A(final String s) {
                ((GeneratedMessageLite.a)this).s();
                ((d)super.b).e0(s);
                return this;
            }
        }
    }
    
    public static final class e extends GeneratedMessageLite implements xv0
    {
        private static final e DEFAULT_INSTANCE;
        public static final int DIRECTION_FIELD_NUMBER = 2;
        public static final int FIELD_FIELD_NUMBER = 1;
        private static volatile b31 PARSER;
        private int direction_;
        private d field_;
        
        static {
            GeneratedMessageLite.V(e.class, DEFAULT_INSTANCE = new e());
        }
        
        public static /* synthetic */ e Z() {
            return e.DEFAULT_INSTANCE;
        }
        
        public static a e0() {
            return (a)e.DEFAULT_INSTANCE.u();
        }
        
        public Direction c0() {
            Direction direction;
            if ((direction = Direction.forNumber(this.direction_)) == null) {
                direction = Direction.UNRECOGNIZED;
            }
            return direction;
        }
        
        public d d0() {
            xv0 xv0;
            if ((xv0 = this.field_) == null) {
                xv0 = d.b0();
            }
            return (d)xv0;
        }
        
        public final void f0(final Direction direction) {
            this.direction_ = direction.getNumber();
        }
        
        public final void g0(final d field_) {
            field_.getClass();
            this.field_ = field_;
        }
        
        @Override
        public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
            switch (StructuredQuery$a.a[methodToInvoke.ordinal()]) {
                default: {
                    throw new UnsupportedOperationException();
                }
                case 7: {
                    return null;
                }
                case 6: {
                    return 1;
                }
                case 5: {
                    final b31 parser;
                    if ((parser = e.PARSER) == null) {
                        synchronized (e.class) {
                            if (e.PARSER == null) {
                                e.PARSER = new GeneratedMessageLite.b(e.DEFAULT_INSTANCE);
                            }
                        }
                    }
                    return parser;
                }
                case 4: {
                    return e.DEFAULT_INSTANCE;
                }
                case 3: {
                    return GeneratedMessageLite.N(e.DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\t\u0002\f", new Object[] { "field_", "direction_" });
                }
                case 2: {
                    return new a((StructuredQuery$a)null);
                }
                case 1: {
                    return new e();
                }
            }
        }
        
        public static final class a extends GeneratedMessageLite.a implements xv0
        {
            public a() {
                super(e.Z());
            }
            
            public a A(final Direction direction) {
                ((GeneratedMessageLite.a)this).s();
                ((e)super.b).f0(direction);
                return this;
            }
            
            public a B(final d d) {
                ((GeneratedMessageLite.a)this).s();
                ((e)super.b).g0(d);
                return this;
            }
        }
    }
    
    public static final class f extends GeneratedMessageLite implements xv0
    {
        private static final f DEFAULT_INSTANCE;
        public static final int FIELDS_FIELD_NUMBER = 2;
        private static volatile b31 PARSER;
        private t.e fields_;
        
        static {
            GeneratedMessageLite.V(f.class, DEFAULT_INSTANCE = new f());
        }
        
        public f() {
            this.fields_ = GeneratedMessageLite.A();
        }
        
        public static /* synthetic */ f Z() {
            return f.DEFAULT_INSTANCE;
        }
        
        @Override
        public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
            switch (StructuredQuery$a.a[methodToInvoke.ordinal()]) {
                default: {
                    throw new UnsupportedOperationException();
                }
                case 7: {
                    return null;
                }
                case 6: {
                    return 1;
                }
                case 5: {
                    final b31 parser;
                    if ((parser = f.PARSER) == null) {
                        synchronized (f.class) {
                            if (f.PARSER == null) {
                                f.PARSER = new GeneratedMessageLite.b(f.DEFAULT_INSTANCE);
                            }
                        }
                    }
                    return parser;
                }
                case 4: {
                    return f.DEFAULT_INSTANCE;
                }
                case 3: {
                    return GeneratedMessageLite.N(f.DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0002\u0002\u0001\u0000\u0001\u0000\u0002\u001b", new Object[] { "fields_", d.class });
                }
                case 2: {
                    return new a((StructuredQuery$a)null);
                }
                case 1: {
                    return new f();
                }
            }
        }
        
        public static final class a extends GeneratedMessageLite.a implements xv0
        {
            public a() {
                super(f.Z());
            }
        }
    }
}

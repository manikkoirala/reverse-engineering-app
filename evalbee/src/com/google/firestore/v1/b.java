// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

import com.google.protobuf.a0;
import com.google.protobuf.ByteString;
import com.google.protobuf.GeneratedMessageLite;

public final class b extends GeneratedMessageLite implements xv0
{
    public static final int BITMAP_FIELD_NUMBER = 1;
    private static final b DEFAULT_INSTANCE;
    public static final int PADDING_FIELD_NUMBER = 2;
    private static volatile b31 PARSER;
    private ByteString bitmap_;
    private int padding_;
    
    static {
        GeneratedMessageLite.V(b.class, DEFAULT_INSTANCE = new b());
    }
    
    public b() {
        this.bitmap_ = ByteString.EMPTY;
    }
    
    public static /* synthetic */ b Z() {
        return b.DEFAULT_INSTANCE;
    }
    
    public static b b0() {
        return b.DEFAULT_INSTANCE;
    }
    
    public ByteString a0() {
        return this.bitmap_;
    }
    
    public int c0() {
        return this.padding_;
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (b$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = b.PARSER) == null) {
                    synchronized (b.class) {
                        if (b.PARSER == null) {
                            b.PARSER = new GeneratedMessageLite.b(b.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return b.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(b.DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\n\u0002\u0004", new Object[] { "bitmap_", "padding_" });
            }
            case 2: {
                return new b((b$a)null);
            }
            case 1: {
                return new b();
            }
        }
    }
    
    public static final class b extends a implements xv0
    {
        public b() {
            super(com.google.firestore.v1.b.Z());
        }
    }
}

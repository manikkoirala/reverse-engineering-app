// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

import com.google.protobuf.a0;
import com.google.protobuf.t;
import com.google.protobuf.ByteString;
import com.google.protobuf.j0;
import com.google.protobuf.GeneratedMessageLite;

public final class m extends GeneratedMessageLite implements xv0
{
    public static final int COMMIT_TIME_FIELD_NUMBER = 4;
    private static final m DEFAULT_INSTANCE;
    private static volatile b31 PARSER;
    public static final int STREAM_ID_FIELD_NUMBER = 1;
    public static final int STREAM_TOKEN_FIELD_NUMBER = 2;
    public static final int WRITE_RESULTS_FIELD_NUMBER = 3;
    private j0 commitTime_;
    private String streamId_;
    private ByteString streamToken_;
    private t.e writeResults_;
    
    static {
        GeneratedMessageLite.V(m.class, DEFAULT_INSTANCE = new m());
    }
    
    public m() {
        this.streamId_ = "";
        this.streamToken_ = ByteString.EMPTY;
        this.writeResults_ = GeneratedMessageLite.A();
    }
    
    public static /* synthetic */ m Z() {
        return m.DEFAULT_INSTANCE;
    }
    
    public static m b0() {
        return m.DEFAULT_INSTANCE;
    }
    
    public j0 a0() {
        j0 j0;
        if ((j0 = this.commitTime_) == null) {
            j0 = com.google.protobuf.j0.c0();
        }
        return j0;
    }
    
    public ByteString c0() {
        return this.streamToken_;
    }
    
    public n d0(final int n) {
        return this.writeResults_.get(n);
    }
    
    public int e0() {
        return this.writeResults_.size();
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (m$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = m.PARSER) == null) {
                    synchronized (m.class) {
                        if (m.PARSER == null) {
                            m.PARSER = new GeneratedMessageLite.b(m.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return m.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(m.DEFAULT_INSTANCE, "\u0000\u0004\u0000\u0000\u0001\u0004\u0004\u0000\u0001\u0000\u0001\u0208\u0002\n\u0003\u001b\u0004\t", new Object[] { "streamId_", "streamToken_", "writeResults_", n.class, "commitTime_" });
            }
            case 2: {
                return new b((m$a)null);
            }
            case 1: {
                return new m();
            }
        }
    }
    
    public static final class b extends a implements xv0
    {
        public b() {
            super(m.Z());
        }
    }
}

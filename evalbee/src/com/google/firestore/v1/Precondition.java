// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

import com.google.protobuf.a0;
import com.google.protobuf.j0;
import com.google.protobuf.GeneratedMessageLite;

public final class Precondition extends GeneratedMessageLite implements xv0
{
    private static final Precondition DEFAULT_INSTANCE;
    public static final int EXISTS_FIELD_NUMBER = 1;
    private static volatile b31 PARSER;
    public static final int UPDATE_TIME_FIELD_NUMBER = 2;
    private int conditionTypeCase_;
    private Object conditionType_;
    
    static {
        GeneratedMessageLite.V(Precondition.class, DEFAULT_INSTANCE = new Precondition());
    }
    
    public Precondition() {
        this.conditionTypeCase_ = 0;
    }
    
    public static /* synthetic */ Precondition Z() {
        return Precondition.DEFAULT_INSTANCE;
    }
    
    public static Precondition d0() {
        return Precondition.DEFAULT_INSTANCE;
    }
    
    public static b g0() {
        return (b)Precondition.DEFAULT_INSTANCE.u();
    }
    
    public ConditionTypeCase c0() {
        return ConditionTypeCase.forNumber(this.conditionTypeCase_);
    }
    
    public boolean e0() {
        return this.conditionTypeCase_ == 1 && (boolean)this.conditionType_;
    }
    
    public j0 f0() {
        if (this.conditionTypeCase_ == 2) {
            return (j0)this.conditionType_;
        }
        return j0.c0();
    }
    
    public final void h0(final boolean b) {
        this.conditionTypeCase_ = 1;
        this.conditionType_ = b;
    }
    
    public final void i0(final j0 conditionType_) {
        conditionType_.getClass();
        this.conditionType_ = conditionType_;
        this.conditionTypeCase_ = 2;
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (Precondition$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = Precondition.PARSER) == null) {
                    synchronized (Precondition.class) {
                        if (Precondition.PARSER == null) {
                            Precondition.PARSER = new GeneratedMessageLite.b(Precondition.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return Precondition.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(Precondition.DEFAULT_INSTANCE, "\u0000\u0002\u0001\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001:\u0000\u0002<\u0000", new Object[] { "conditionType_", "conditionTypeCase_", j0.class });
            }
            case 2: {
                return new b((Precondition$a)null);
            }
            case 1: {
                return new Precondition();
            }
        }
    }
    
    public enum ConditionTypeCase
    {
        private static final ConditionTypeCase[] $VALUES;
        
        CONDITIONTYPE_NOT_SET(0), 
        EXISTS(1), 
        UPDATE_TIME(2);
        
        private final int value;
        
        private ConditionTypeCase(final int value) {
            this.value = value;
        }
        
        public static ConditionTypeCase forNumber(final int n) {
            if (n == 0) {
                return ConditionTypeCase.CONDITIONTYPE_NOT_SET;
            }
            if (n == 1) {
                return ConditionTypeCase.EXISTS;
            }
            if (n != 2) {
                return null;
            }
            return ConditionTypeCase.UPDATE_TIME;
        }
        
        @Deprecated
        public static ConditionTypeCase valueOf(final int n) {
            return forNumber(n);
        }
        
        public int getNumber() {
            return this.value;
        }
    }
    
    public static final class b extends a implements xv0
    {
        public b() {
            super(Precondition.Z());
        }
        
        public b A(final boolean b) {
            ((a)this).s();
            ((Precondition)super.b).h0(b);
            return this;
        }
        
        public b B(final j0 j0) {
            ((a)this).s();
            ((Precondition)super.b).i0(j0);
            return this;
        }
    }
}

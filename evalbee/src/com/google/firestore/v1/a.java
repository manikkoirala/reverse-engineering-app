// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

import java.util.Collections;
import com.google.protobuf.a0;
import java.util.List;
import com.google.protobuf.t;
import com.google.protobuf.GeneratedMessageLite;

public final class a extends GeneratedMessageLite implements w8
{
    private static final a DEFAULT_INSTANCE;
    private static volatile b31 PARSER;
    public static final int VALUES_FIELD_NUMBER = 1;
    private t.e values_;
    
    static {
        GeneratedMessageLite.V(a.class, DEFAULT_INSTANCE = new a());
    }
    
    public a() {
        this.values_ = GeneratedMessageLite.A();
    }
    
    public static /* synthetic */ a Z() {
        return a.DEFAULT_INSTANCE;
    }
    
    public static a g0() {
        return a.DEFAULT_INSTANCE;
    }
    
    public static b j0() {
        return (b)a.DEFAULT_INSTANCE.u();
    }
    
    public final void d0(final Iterable iterable) {
        this.f0();
        com.google.protobuf.a.j(iterable, this.values_);
    }
    
    public final void e0(final Value value) {
        value.getClass();
        this.f0();
        this.values_.add(value);
    }
    
    public final void f0() {
        final t.e values_ = this.values_;
        if (!values_.h()) {
            this.values_ = GeneratedMessageLite.L(values_);
        }
    }
    
    @Override
    public List h() {
        return this.values_;
    }
    
    public Value h0(final int n) {
        return this.values_.get(n);
    }
    
    public int i0() {
        return this.values_.size();
    }
    
    public final void k0(final int n) {
        this.f0();
        this.values_.remove(n);
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (a$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = a.PARSER) == null) {
                    synchronized (a.class) {
                        if (a.PARSER == null) {
                            a.PARSER = new GeneratedMessageLite.b(a.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return a.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(a.DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b", new Object[] { "values_", Value.class });
            }
            case 2: {
                return new b((a$a)null);
            }
            case 1: {
                return new a();
            }
        }
    }
    
    public static final class b extends GeneratedMessageLite.a implements w8
    {
        public b() {
            super(a.Z());
        }
        
        public b A(final Iterable iterable) {
            ((GeneratedMessageLite.a)this).s();
            ((a)super.b).d0(iterable);
            return this;
        }
        
        public b B(final Value value) {
            ((GeneratedMessageLite.a)this).s();
            ((a)super.b).e0(value);
            return this;
        }
        
        public Value C(final int n) {
            return ((a)super.b).h0(n);
        }
        
        public int D() {
            return ((a)super.b).i0();
        }
        
        public b E(final int n) {
            ((GeneratedMessageLite.a)this).s();
            ((a)super.b).k0(n);
            return this;
        }
        
        @Override
        public List h() {
            return Collections.unmodifiableList((List<?>)((a)super.b).h());
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

import com.google.protobuf.a0;
import com.google.protobuf.GeneratedMessageLite;

public final class j extends GeneratedMessageLite implements xv0
{
    public static final int COUNT_FIELD_NUMBER = 2;
    private static final j DEFAULT_INSTANCE;
    private static volatile b31 PARSER;
    public static final int TARGET_ID_FIELD_NUMBER = 1;
    public static final int UNCHANGED_NAMES_FIELD_NUMBER = 3;
    private int count_;
    private int targetId_;
    private com.google.firestore.v1.c unchangedNames_;
    
    static {
        GeneratedMessageLite.V(j.class, DEFAULT_INSTANCE = new j());
    }
    
    public static /* synthetic */ j Z() {
        return j.DEFAULT_INSTANCE;
    }
    
    public static j b0() {
        return j.DEFAULT_INSTANCE;
    }
    
    public int a0() {
        return this.count_;
    }
    
    public int c0() {
        return this.targetId_;
    }
    
    public com.google.firestore.v1.c d0() {
        com.google.firestore.v1.c c;
        if ((c = this.unchangedNames_) == null) {
            c = com.google.firestore.v1.c.b0();
        }
        return c;
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (j$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = j.PARSER) == null) {
                    synchronized (j.class) {
                        if (j.PARSER == null) {
                            j.PARSER = new GeneratedMessageLite.b(j.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return j.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(j.DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001\u0004\u0002\u0004\u0003\t", new Object[] { "targetId_", "count_", "unchangedNames_" });
            }
            case 2: {
                return new b((j$a)null);
            }
            case 1: {
                return new j();
            }
        }
    }
    
    public static final class b extends a implements xv0
    {
        public b() {
            super(j.Z());
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

public enum ListDocumentsRequest$ConsistencySelectorCase
{
    private static final ListDocumentsRequest$ConsistencySelectorCase[] $VALUES;
    
    CONSISTENCYSELECTOR_NOT_SET(0), 
    READ_TIME(10), 
    TRANSACTION(8);
    
    private final int value;
    
    private ListDocumentsRequest$ConsistencySelectorCase(final int value) {
        this.value = value;
    }
    
    public static ListDocumentsRequest$ConsistencySelectorCase forNumber(final int n) {
        if (n == 0) {
            return ListDocumentsRequest$ConsistencySelectorCase.CONSISTENCYSELECTOR_NOT_SET;
        }
        if (n == 8) {
            return ListDocumentsRequest$ConsistencySelectorCase.TRANSACTION;
        }
        if (n != 10) {
            return null;
        }
        return ListDocumentsRequest$ConsistencySelectorCase.READ_TIME;
    }
    
    @Deprecated
    public static ListDocumentsRequest$ConsistencySelectorCase valueOf(final int n) {
        return forNumber(n);
    }
    
    public int getNumber() {
        return this.value;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

public enum RunQueryRequest$ConsistencySelectorCase
{
    private static final RunQueryRequest$ConsistencySelectorCase[] $VALUES;
    
    CONSISTENCYSELECTOR_NOT_SET(0), 
    NEW_TRANSACTION(6), 
    READ_TIME(7), 
    TRANSACTION(5);
    
    private final int value;
    
    private RunQueryRequest$ConsistencySelectorCase(final int value) {
        this.value = value;
    }
    
    public static RunQueryRequest$ConsistencySelectorCase forNumber(final int n) {
        if (n == 0) {
            return RunQueryRequest$ConsistencySelectorCase.CONSISTENCYSELECTOR_NOT_SET;
        }
        if (n == 5) {
            return RunQueryRequest$ConsistencySelectorCase.TRANSACTION;
        }
        if (n == 6) {
            return RunQueryRequest$ConsistencySelectorCase.NEW_TRANSACTION;
        }
        if (n != 7) {
            return null;
        }
        return RunQueryRequest$ConsistencySelectorCase.READ_TIME;
    }
    
    @Deprecated
    public static RunQueryRequest$ConsistencySelectorCase valueOf(final int n) {
        return forNumber(n);
    }
    
    public int getNumber() {
        return this.value;
    }
}

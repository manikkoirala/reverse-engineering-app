// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

import com.google.protobuf.a0;
import java.util.List;
import com.google.protobuf.t;
import com.google.protobuf.GeneratedMessageLite;

public final class f extends GeneratedMessageLite implements xv0
{
    private static final f DEFAULT_INSTANCE;
    public static final int DOCUMENT_FIELD_NUMBER = 1;
    private static volatile b31 PARSER;
    public static final int REMOVED_TARGET_IDS_FIELD_NUMBER = 6;
    public static final int TARGET_IDS_FIELD_NUMBER = 5;
    private e document_;
    private int removedTargetIdsMemoizedSerializedSize;
    private t.d removedTargetIds_;
    private int targetIdsMemoizedSerializedSize;
    private t.d targetIds_;
    
    static {
        GeneratedMessageLite.V(f.class, DEFAULT_INSTANCE = new f());
    }
    
    public f() {
        this.targetIdsMemoizedSerializedSize = -1;
        this.removedTargetIdsMemoizedSerializedSize = -1;
        this.targetIds_ = GeneratedMessageLite.z();
        this.removedTargetIds_ = GeneratedMessageLite.z();
    }
    
    public static /* synthetic */ f Z() {
        return f.DEFAULT_INSTANCE;
    }
    
    public static f a0() {
        return f.DEFAULT_INSTANCE;
    }
    
    public e b0() {
        e e;
        if ((e = this.document_) == null) {
            e = com.google.firestore.v1.e.d0();
        }
        return e;
    }
    
    public List c0() {
        return this.removedTargetIds_;
    }
    
    public List d0() {
        return this.targetIds_;
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (f$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = f.PARSER) == null) {
                    synchronized (f.class) {
                        if (f.PARSER == null) {
                            f.PARSER = new GeneratedMessageLite.b(f.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return f.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(f.DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0006\u0003\u0000\u0002\u0000\u0001\t\u0005'\u0006'", new Object[] { "document_", "targetIds_", "removedTargetIds_" });
            }
            case 2: {
                return new b((f$a)null);
            }
            case 1: {
                return new f();
            }
        }
    }
    
    public static final class b extends a implements xv0
    {
        public b() {
            super(f.Z());
        }
    }
}

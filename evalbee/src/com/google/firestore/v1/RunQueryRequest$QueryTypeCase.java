// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

public enum RunQueryRequest$QueryTypeCase
{
    private static final RunQueryRequest$QueryTypeCase[] $VALUES;
    
    QUERYTYPE_NOT_SET(0), 
    STRUCTURED_QUERY(2);
    
    private final int value;
    
    private RunQueryRequest$QueryTypeCase(final int value) {
        this.value = value;
    }
    
    public static RunQueryRequest$QueryTypeCase forNumber(final int n) {
        if (n == 0) {
            return RunQueryRequest$QueryTypeCase.QUERYTYPE_NOT_SET;
        }
        if (n != 2) {
            return null;
        }
        return RunQueryRequest$QueryTypeCase.STRUCTURED_QUERY;
    }
    
    @Deprecated
    public static RunQueryRequest$QueryTypeCase valueOf(final int n) {
        return forNumber(n);
    }
    
    public int getNumber() {
        return this.value;
    }
}

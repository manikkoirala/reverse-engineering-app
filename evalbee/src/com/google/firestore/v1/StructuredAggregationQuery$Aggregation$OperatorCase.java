// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

public enum StructuredAggregationQuery$Aggregation$OperatorCase
{
    private static final StructuredAggregationQuery$Aggregation$OperatorCase[] $VALUES;
    
    AVG(3), 
    COUNT(1), 
    OPERATOR_NOT_SET(0), 
    SUM(2);
    
    private final int value;
    
    private StructuredAggregationQuery$Aggregation$OperatorCase(final int value) {
        this.value = value;
    }
    
    public static StructuredAggregationQuery$Aggregation$OperatorCase forNumber(final int n) {
        if (n == 0) {
            return StructuredAggregationQuery$Aggregation$OperatorCase.OPERATOR_NOT_SET;
        }
        if (n == 1) {
            return StructuredAggregationQuery$Aggregation$OperatorCase.COUNT;
        }
        if (n == 2) {
            return StructuredAggregationQuery$Aggregation$OperatorCase.SUM;
        }
        if (n != 3) {
            return null;
        }
        return StructuredAggregationQuery$Aggregation$OperatorCase.AVG;
    }
    
    @Deprecated
    public static StructuredAggregationQuery$Aggregation$OperatorCase valueOf(final int n) {
        return forNumber(n);
    }
    
    public int getNumber() {
        return this.value;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

import com.google.protobuf.a0;
import java.util.List;
import com.google.protobuf.t;
import com.google.protobuf.GeneratedMessageLite;

public final class Write extends GeneratedMessageLite implements xv0
{
    public static final int CURRENT_DOCUMENT_FIELD_NUMBER = 4;
    private static final Write DEFAULT_INSTANCE;
    public static final int DELETE_FIELD_NUMBER = 2;
    private static volatile b31 PARSER;
    public static final int TRANSFORM_FIELD_NUMBER = 6;
    public static final int UPDATE_FIELD_NUMBER = 1;
    public static final int UPDATE_MASK_FIELD_NUMBER = 3;
    public static final int UPDATE_TRANSFORMS_FIELD_NUMBER = 7;
    public static final int VERIFY_FIELD_NUMBER = 5;
    private Precondition currentDocument_;
    private int operationCase_;
    private Object operation_;
    private h updateMask_;
    private t.e updateTransforms_;
    
    static {
        GeneratedMessageLite.V(Write.class, DEFAULT_INSTANCE = new Write());
    }
    
    public Write() {
        this.operationCase_ = 0;
        this.updateTransforms_ = GeneratedMessageLite.A();
    }
    
    public static /* synthetic */ Write Z() {
        return Write.DEFAULT_INSTANCE;
    }
    
    public static b u0() {
        return (b)Write.DEFAULT_INSTANCE.u();
    }
    
    public static b v0(final Write write) {
        return (b)Write.DEFAULT_INSTANCE.v(write);
    }
    
    public static Write w0(final byte[] array) {
        return (Write)GeneratedMessageLite.R(Write.DEFAULT_INSTANCE, array);
    }
    
    public final void A0(final h updateMask_) {
        updateMask_.getClass();
        this.updateMask_ = updateMask_;
    }
    
    public final void B0(final String operation_) {
        operation_.getClass();
        this.operationCase_ = 5;
        this.operation_ = operation_;
    }
    
    public final void g0(final DocumentTransform.FieldTransform fieldTransform) {
        fieldTransform.getClass();
        this.h0();
        this.updateTransforms_.add(fieldTransform);
    }
    
    public final void h0() {
        final t.e updateTransforms_ = this.updateTransforms_;
        if (!updateTransforms_.h()) {
            this.updateTransforms_ = GeneratedMessageLite.L(updateTransforms_);
        }
    }
    
    public Precondition i0() {
        Precondition precondition;
        if ((precondition = this.currentDocument_) == null) {
            precondition = Precondition.d0();
        }
        return precondition;
    }
    
    public String j0() {
        String s;
        if (this.operationCase_ == 2) {
            s = (String)this.operation_;
        }
        else {
            s = "";
        }
        return s;
    }
    
    public OperationCase k0() {
        return OperationCase.forNumber(this.operationCase_);
    }
    
    public DocumentTransform l0() {
        if (this.operationCase_ == 6) {
            return (DocumentTransform)this.operation_;
        }
        return DocumentTransform.a0();
    }
    
    public e m0() {
        if (this.operationCase_ == 1) {
            return (e)this.operation_;
        }
        return e.d0();
    }
    
    public h n0() {
        h h;
        if ((h = this.updateMask_) == null) {
            h = com.google.firestore.v1.h.d0();
        }
        return h;
    }
    
    public List o0() {
        return this.updateTransforms_;
    }
    
    public String p0() {
        String s;
        if (this.operationCase_ == 5) {
            s = (String)this.operation_;
        }
        else {
            s = "";
        }
        return s;
    }
    
    public boolean q0() {
        return this.currentDocument_ != null;
    }
    
    public boolean r0() {
        return this.operationCase_ == 6;
    }
    
    public boolean s0() {
        final int operationCase_ = this.operationCase_;
        boolean b = true;
        if (operationCase_ != 1) {
            b = false;
        }
        return b;
    }
    
    public boolean t0() {
        return this.updateMask_ != null;
    }
    
    public final void x0(final Precondition currentDocument_) {
        currentDocument_.getClass();
        this.currentDocument_ = currentDocument_;
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (Write$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = Write.PARSER) == null) {
                    synchronized (Write.class) {
                        if (Write.PARSER == null) {
                            Write.PARSER = new GeneratedMessageLite.b(Write.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return Write.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(Write.DEFAULT_INSTANCE, "\u0000\u0007\u0001\u0000\u0001\u0007\u0007\u0000\u0001\u0000\u0001<\u0000\u0002\u023b\u0000\u0003\t\u0004\t\u0005\u023b\u0000\u0006<\u0000\u0007\u001b", new Object[] { "operation_", "operationCase_", e.class, "updateMask_", "currentDocument_", DocumentTransform.class, "updateTransforms_", DocumentTransform.FieldTransform.class });
            }
            case 2: {
                return new b((Write$a)null);
            }
            case 1: {
                return new Write();
            }
        }
    }
    
    public final void y0(final String operation_) {
        operation_.getClass();
        this.operationCase_ = 2;
        this.operation_ = operation_;
    }
    
    public final void z0(final e operation_) {
        operation_.getClass();
        this.operation_ = operation_;
        this.operationCase_ = 1;
    }
    
    public enum OperationCase
    {
        private static final OperationCase[] $VALUES;
        
        DELETE(2), 
        OPERATION_NOT_SET(0), 
        TRANSFORM(6), 
        UPDATE(1), 
        VERIFY(5);
        
        private final int value;
        
        private OperationCase(final int value) {
            this.value = value;
        }
        
        public static OperationCase forNumber(final int n) {
            if (n == 0) {
                return OperationCase.OPERATION_NOT_SET;
            }
            if (n == 1) {
                return OperationCase.UPDATE;
            }
            if (n == 2) {
                return OperationCase.DELETE;
            }
            if (n == 5) {
                return OperationCase.VERIFY;
            }
            if (n != 6) {
                return null;
            }
            return OperationCase.TRANSFORM;
        }
        
        @Deprecated
        public static OperationCase valueOf(final int n) {
            return forNumber(n);
        }
        
        public int getNumber() {
            return this.value;
        }
    }
    
    public static final class b extends a implements xv0
    {
        public b() {
            super(Write.Z());
        }
        
        public b A(final DocumentTransform.FieldTransform fieldTransform) {
            ((a)this).s();
            ((Write)super.b).g0(fieldTransform);
            return this;
        }
        
        public b B(final Precondition precondition) {
            ((a)this).s();
            ((Write)super.b).x0(precondition);
            return this;
        }
        
        public b C(final String s) {
            ((a)this).s();
            ((Write)super.b).y0(s);
            return this;
        }
        
        public b D(final e e) {
            ((a)this).s();
            ((Write)super.b).z0(e);
            return this;
        }
        
        public b E(final h h) {
            ((a)this).s();
            ((Write)super.b).A0(h);
            return this;
        }
        
        public b F(final String s) {
            ((a)this).s();
            ((Write)super.b).B0(s);
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

public enum StructuredAggregationQuery$QueryTypeCase
{
    private static final StructuredAggregationQuery$QueryTypeCase[] $VALUES;
    
    QUERYTYPE_NOT_SET(0), 
    STRUCTURED_QUERY(1);
    
    private final int value;
    
    private StructuredAggregationQuery$QueryTypeCase(final int value) {
        this.value = value;
    }
    
    public static StructuredAggregationQuery$QueryTypeCase forNumber(final int n) {
        if (n == 0) {
            return StructuredAggregationQuery$QueryTypeCase.QUERYTYPE_NOT_SET;
        }
        if (n != 1) {
            return null;
        }
        return StructuredAggregationQuery$QueryTypeCase.STRUCTURED_QUERY;
    }
    
    @Deprecated
    public static StructuredAggregationQuery$QueryTypeCase valueOf(final int n) {
        return forNumber(n);
    }
    
    public int getNumber() {
        return this.value;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

import com.google.protobuf.WireFormat;
import com.google.protobuf.x;
import com.google.protobuf.a0;
import java.util.Collections;
import java.util.Map;
import com.google.protobuf.MapFieldLite;
import com.google.protobuf.GeneratedMessageLite;

public final class k extends GeneratedMessageLite implements xv0
{
    private static final k DEFAULT_INSTANCE;
    public static final int FIELDS_FIELD_NUMBER = 1;
    private static volatile b31 PARSER;
    private MapFieldLite<String, Value> fields_;
    
    static {
        GeneratedMessageLite.V(k.class, DEFAULT_INSTANCE = new k());
    }
    
    public k() {
        this.fields_ = MapFieldLite.emptyMapField();
    }
    
    public static /* synthetic */ k Z() {
        return k.DEFAULT_INSTANCE;
    }
    
    public static k b0() {
        return k.DEFAULT_INSTANCE;
    }
    
    public static b j0() {
        return (b)k.DEFAULT_INSTANCE.u();
    }
    
    public int c0() {
        return this.h0().size();
    }
    
    public Map d0() {
        return Collections.unmodifiableMap((Map<?, ?>)this.h0());
    }
    
    public Value e0(final String s, Value value) {
        s.getClass();
        final MapFieldLite h0 = this.h0();
        if (h0.containsKey(s)) {
            value = (Value)h0.get(s);
        }
        return value;
    }
    
    public Value f0(final String s) {
        s.getClass();
        final MapFieldLite h0 = this.h0();
        if (h0.containsKey(s)) {
            return (Value)h0.get(s);
        }
        throw new IllegalArgumentException();
    }
    
    public final Map g0() {
        return this.i0();
    }
    
    public final MapFieldLite h0() {
        return this.fields_;
    }
    
    public final MapFieldLite i0() {
        if (!this.fields_.isMutable()) {
            this.fields_ = this.fields_.mutableCopy();
        }
        return this.fields_;
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (k$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = k.PARSER) == null) {
                    synchronized (k.class) {
                        if (k.PARSER == null) {
                            k.PARSER = new GeneratedMessageLite.b(k.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return k.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(k.DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0001\u0000\u0000\u00012", new Object[] { "fields_", c.a });
            }
            case 2: {
                return new b((k$a)null);
            }
            case 1: {
                return new k();
            }
        }
    }
    
    public static final class b extends a implements xv0
    {
        public b() {
            super(k.Z());
        }
        
        public boolean A(final String s) {
            s.getClass();
            return ((k)super.b).d0().containsKey(s);
        }
        
        public b B(final Map map) {
            ((a)this).s();
            ((k)super.b).g0().putAll(map);
            return this;
        }
        
        public b C(final String s, final Value value) {
            s.getClass();
            value.getClass();
            ((a)this).s();
            ((k)super.b).g0().put(s, value);
            return this;
        }
        
        public b D(final String s) {
            s.getClass();
            ((a)this).s();
            ((k)super.b).g0().remove(s);
            return this;
        }
    }
    
    public abstract static final class c
    {
        public static final x a;
        
        static {
            a = x.d(WireFormat.FieldType.STRING, "", WireFormat.FieldType.MESSAGE, Value.o0());
        }
    }
}

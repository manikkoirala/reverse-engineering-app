// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

import com.google.protobuf.a0;
import com.google.protobuf.t;
import com.google.protobuf.GeneratedMessageLite;

public final class h extends GeneratedMessageLite implements xv0
{
    private static final h DEFAULT_INSTANCE;
    public static final int FIELD_PATHS_FIELD_NUMBER = 1;
    private static volatile b31 PARSER;
    private t.e fieldPaths_;
    
    static {
        GeneratedMessageLite.V(h.class, DEFAULT_INSTANCE = new h());
    }
    
    public h() {
        this.fieldPaths_ = GeneratedMessageLite.A();
    }
    
    public static /* synthetic */ h Z() {
        return h.DEFAULT_INSTANCE;
    }
    
    public static h d0() {
        return h.DEFAULT_INSTANCE;
    }
    
    public static b g0() {
        return (b)h.DEFAULT_INSTANCE.u();
    }
    
    public final void b0(final String s) {
        s.getClass();
        this.c0();
        this.fieldPaths_.add(s);
    }
    
    public final void c0() {
        final t.e fieldPaths_ = this.fieldPaths_;
        if (!fieldPaths_.h()) {
            this.fieldPaths_ = GeneratedMessageLite.L(fieldPaths_);
        }
    }
    
    public String e0(final int n) {
        return this.fieldPaths_.get(n);
    }
    
    public int f0() {
        return this.fieldPaths_.size();
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (h$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = h.PARSER) == null) {
                    synchronized (h.class) {
                        if (h.PARSER == null) {
                            h.PARSER = new GeneratedMessageLite.b(h.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return h.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(h.DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u021a", new Object[] { "fieldPaths_" });
            }
            case 2: {
                return new b((h$a)null);
            }
            case 1: {
                return new h();
            }
        }
    }
    
    public static final class b extends a implements xv0
    {
        public b() {
            super(h.Z());
        }
        
        public b A(final String s) {
            ((a)this).s();
            ((h)super.b).b0(s);
            return this;
        }
    }
}

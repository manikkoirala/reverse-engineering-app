// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

import com.google.protobuf.a0;
import com.google.protobuf.j0;
import com.google.protobuf.t;
import com.google.protobuf.GeneratedMessageLite;

public final class n extends GeneratedMessageLite implements xv0
{
    private static final n DEFAULT_INSTANCE;
    private static volatile b31 PARSER;
    public static final int TRANSFORM_RESULTS_FIELD_NUMBER = 2;
    public static final int UPDATE_TIME_FIELD_NUMBER = 1;
    private t.e transformResults_;
    private j0 updateTime_;
    
    static {
        GeneratedMessageLite.V(n.class, DEFAULT_INSTANCE = new n());
    }
    
    public n() {
        this.transformResults_ = GeneratedMessageLite.A();
    }
    
    public static /* synthetic */ n Z() {
        return n.DEFAULT_INSTANCE;
    }
    
    public Value a0(final int n) {
        return this.transformResults_.get(n);
    }
    
    public int b0() {
        return this.transformResults_.size();
    }
    
    public j0 c0() {
        j0 j0;
        if ((j0 = this.updateTime_) == null) {
            j0 = com.google.protobuf.j0.c0();
        }
        return j0;
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (n$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = n.PARSER) == null) {
                    synchronized (n.class) {
                        if (n.PARSER == null) {
                            n.PARSER = new GeneratedMessageLite.b(n.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return n.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(n.DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0001\u0000\u0001\t\u0002\u001b", new Object[] { "updateTime_", "transformResults_", Value.class });
            }
            case 2: {
                return new b((n$a)null);
            }
            case 1: {
                return new n();
            }
        }
    }
    
    public static final class b extends a implements xv0
    {
        public b() {
            super(n.Z());
        }
    }
}

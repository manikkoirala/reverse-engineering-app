// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

public enum TransactionOptions$ModeCase
{
    private static final TransactionOptions$ModeCase[] $VALUES;
    
    MODE_NOT_SET(0), 
    READ_ONLY(2), 
    READ_WRITE(3);
    
    private final int value;
    
    private TransactionOptions$ModeCase(final int value) {
        this.value = value;
    }
    
    public static TransactionOptions$ModeCase forNumber(final int n) {
        if (n == 0) {
            return TransactionOptions$ModeCase.MODE_NOT_SET;
        }
        if (n == 2) {
            return TransactionOptions$ModeCase.READ_ONLY;
        }
        if (n != 3) {
            return null;
        }
        return TransactionOptions$ModeCase.READ_WRITE;
    }
    
    @Deprecated
    public static TransactionOptions$ModeCase valueOf(final int n) {
        return forNumber(n);
    }
    
    public int getNumber() {
        return this.value;
    }
}

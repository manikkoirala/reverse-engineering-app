// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

public enum BatchGetDocumentsRequest$ConsistencySelectorCase
{
    private static final BatchGetDocumentsRequest$ConsistencySelectorCase[] $VALUES;
    
    CONSISTENCYSELECTOR_NOT_SET(0), 
    NEW_TRANSACTION(5), 
    READ_TIME(7), 
    TRANSACTION(4);
    
    private final int value;
    
    private BatchGetDocumentsRequest$ConsistencySelectorCase(final int value) {
        this.value = value;
    }
    
    public static BatchGetDocumentsRequest$ConsistencySelectorCase forNumber(final int n) {
        if (n == 0) {
            return BatchGetDocumentsRequest$ConsistencySelectorCase.CONSISTENCYSELECTOR_NOT_SET;
        }
        if (n == 7) {
            return BatchGetDocumentsRequest$ConsistencySelectorCase.READ_TIME;
        }
        if (n == 4) {
            return BatchGetDocumentsRequest$ConsistencySelectorCase.TRANSACTION;
        }
        if (n != 5) {
            return null;
        }
        return BatchGetDocumentsRequest$ConsistencySelectorCase.NEW_TRANSACTION;
    }
    
    @Deprecated
    public static BatchGetDocumentsRequest$ConsistencySelectorCase valueOf(final int n) {
        return forNumber(n);
    }
    
    public int getNumber() {
        return this.value;
    }
}

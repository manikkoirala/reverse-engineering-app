// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

import com.google.protobuf.t;
import com.google.protobuf.a0;
import com.google.protobuf.j0;
import com.google.protobuf.ByteString;
import com.google.protobuf.r;
import com.google.protobuf.GeneratedMessageLite;

public final class Target extends GeneratedMessageLite implements xv0
{
    private static final Target DEFAULT_INSTANCE;
    public static final int DOCUMENTS_FIELD_NUMBER = 3;
    public static final int EXPECTED_COUNT_FIELD_NUMBER = 12;
    public static final int ONCE_FIELD_NUMBER = 6;
    private static volatile b31 PARSER;
    public static final int QUERY_FIELD_NUMBER = 2;
    public static final int READ_TIME_FIELD_NUMBER = 11;
    public static final int RESUME_TOKEN_FIELD_NUMBER = 4;
    public static final int TARGET_ID_FIELD_NUMBER = 5;
    private r expectedCount_;
    private boolean once_;
    private int resumeTypeCase_;
    private Object resumeType_;
    private int targetId_;
    private int targetTypeCase_;
    private Object targetType_;
    
    static {
        GeneratedMessageLite.V(Target.class, DEFAULT_INSTANCE = new Target());
    }
    
    public Target() {
        this.targetTypeCase_ = 0;
        this.resumeTypeCase_ = 0;
    }
    
    public static /* synthetic */ Target Z() {
        return Target.DEFAULT_INSTANCE;
    }
    
    public static b g0() {
        return (b)Target.DEFAULT_INSTANCE.u();
    }
    
    public final void h0(final c targetType_) {
        targetType_.getClass();
        this.targetType_ = targetType_;
        this.targetTypeCase_ = 3;
    }
    
    public final void i0(final r expectedCount_) {
        expectedCount_.getClass();
        this.expectedCount_ = expectedCount_;
    }
    
    public final void j0(final QueryTarget targetType_) {
        targetType_.getClass();
        this.targetType_ = targetType_;
        this.targetTypeCase_ = 2;
    }
    
    public final void k0(final j0 resumeType_) {
        resumeType_.getClass();
        this.resumeType_ = resumeType_;
        this.resumeTypeCase_ = 11;
    }
    
    public final void l0(final ByteString resumeType_) {
        resumeType_.getClass();
        this.resumeTypeCase_ = 4;
        this.resumeType_ = resumeType_;
    }
    
    public final void m0(final int targetId_) {
        this.targetId_ = targetId_;
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (Target$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = Target.PARSER) == null) {
                    synchronized (Target.class) {
                        if (Target.PARSER == null) {
                            Target.PARSER = new GeneratedMessageLite.b(Target.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return Target.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(Target.DEFAULT_INSTANCE, "\u0000\u0007\u0002\u0000\u0002\f\u0007\u0000\u0000\u0000\u0002<\u0000\u0003<\u0000\u0004=\u0001\u0005\u0004\u0006\u0007\u000b<\u0001\f\t", new Object[] { "targetType_", "targetTypeCase_", "resumeType_", "resumeTypeCase_", QueryTarget.class, c.class, "targetId_", "once_", j0.class, "expectedCount_" });
            }
            case 2: {
                return new b((Target$a)null);
            }
            case 1: {
                return new Target();
            }
        }
    }
    
    public static final class QueryTarget extends GeneratedMessageLite implements xv0
    {
        private static final QueryTarget DEFAULT_INSTANCE;
        public static final int PARENT_FIELD_NUMBER = 1;
        private static volatile b31 PARSER;
        public static final int STRUCTURED_QUERY_FIELD_NUMBER = 2;
        private String parent_;
        private int queryTypeCase_;
        private Object queryType_;
        
        static {
            GeneratedMessageLite.V(QueryTarget.class, DEFAULT_INSTANCE = new QueryTarget());
        }
        
        public QueryTarget() {
            this.queryTypeCase_ = 0;
            this.parent_ = "";
        }
        
        public static /* synthetic */ QueryTarget a0() {
            return QueryTarget.DEFAULT_INSTANCE;
        }
        
        public static QueryTarget c0() {
            return QueryTarget.DEFAULT_INSTANCE;
        }
        
        public static a f0() {
            return (a)QueryTarget.DEFAULT_INSTANCE.u();
        }
        
        public String d0() {
            return this.parent_;
        }
        
        public StructuredQuery e0() {
            if (this.queryTypeCase_ == 2) {
                return (StructuredQuery)this.queryType_;
            }
            return StructuredQuery.k0();
        }
        
        public final void g0(final String parent_) {
            parent_.getClass();
            this.parent_ = parent_;
        }
        
        public final void h0(final StructuredQuery queryType_) {
            queryType_.getClass();
            this.queryType_ = queryType_;
            this.queryTypeCase_ = 2;
        }
        
        @Override
        public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
            switch (Target$a.a[methodToInvoke.ordinal()]) {
                default: {
                    throw new UnsupportedOperationException();
                }
                case 7: {
                    return null;
                }
                case 6: {
                    return 1;
                }
                case 5: {
                    final b31 parser;
                    if ((parser = QueryTarget.PARSER) == null) {
                        synchronized (QueryTarget.class) {
                            if (QueryTarget.PARSER == null) {
                                QueryTarget.PARSER = new GeneratedMessageLite.b(QueryTarget.DEFAULT_INSTANCE);
                            }
                        }
                    }
                    return parser;
                }
                case 4: {
                    return QueryTarget.DEFAULT_INSTANCE;
                }
                case 3: {
                    return GeneratedMessageLite.N(QueryTarget.DEFAULT_INSTANCE, "\u0000\u0002\u0001\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0208\u0002<\u0000", new Object[] { "queryType_", "queryTypeCase_", "parent_", StructuredQuery.class });
                }
                case 2: {
                    return new a((Target$a)null);
                }
                case 1: {
                    return new QueryTarget();
                }
            }
        }
        
        public enum QueryTypeCase
        {
            private static final QueryTypeCase[] $VALUES;
            
            QUERYTYPE_NOT_SET(0), 
            STRUCTURED_QUERY(2);
            
            private final int value;
            
            private QueryTypeCase(final int value) {
                this.value = value;
            }
            
            public static QueryTypeCase forNumber(final int n) {
                if (n == 0) {
                    return QueryTypeCase.QUERYTYPE_NOT_SET;
                }
                if (n != 2) {
                    return null;
                }
                return QueryTypeCase.STRUCTURED_QUERY;
            }
            
            @Deprecated
            public static QueryTypeCase valueOf(final int n) {
                return forNumber(n);
            }
            
            public int getNumber() {
                return this.value;
            }
        }
        
        public static final class a extends GeneratedMessageLite.a implements xv0
        {
            public a() {
                super(QueryTarget.a0());
            }
            
            public a A(final String s) {
                ((GeneratedMessageLite.a)this).s();
                ((QueryTarget)super.b).g0(s);
                return this;
            }
            
            public a B(final StructuredQuery.b b) {
                ((GeneratedMessageLite.a)this).s();
                ((QueryTarget)super.b).h0((StructuredQuery)((GeneratedMessageLite.a)b).p());
                return this;
            }
        }
    }
    
    public enum ResumeTypeCase
    {
        private static final ResumeTypeCase[] $VALUES;
        
        READ_TIME(11), 
        RESUMETYPE_NOT_SET(0), 
        RESUME_TOKEN(4);
        
        private final int value;
        
        private ResumeTypeCase(final int value) {
            this.value = value;
        }
        
        public static ResumeTypeCase forNumber(final int n) {
            if (n == 0) {
                return ResumeTypeCase.RESUMETYPE_NOT_SET;
            }
            if (n == 4) {
                return ResumeTypeCase.RESUME_TOKEN;
            }
            if (n != 11) {
                return null;
            }
            return ResumeTypeCase.READ_TIME;
        }
        
        @Deprecated
        public static ResumeTypeCase valueOf(final int n) {
            return forNumber(n);
        }
        
        public int getNumber() {
            return this.value;
        }
    }
    
    public enum TargetTypeCase
    {
        private static final TargetTypeCase[] $VALUES;
        
        DOCUMENTS(3), 
        QUERY(2), 
        TARGETTYPE_NOT_SET(0);
        
        private final int value;
        
        private TargetTypeCase(final int value) {
            this.value = value;
        }
        
        public static TargetTypeCase forNumber(final int n) {
            if (n == 0) {
                return TargetTypeCase.TARGETTYPE_NOT_SET;
            }
            if (n == 2) {
                return TargetTypeCase.QUERY;
            }
            if (n != 3) {
                return null;
            }
            return TargetTypeCase.DOCUMENTS;
        }
        
        @Deprecated
        public static TargetTypeCase valueOf(final int n) {
            return forNumber(n);
        }
        
        public int getNumber() {
            return this.value;
        }
    }
    
    public static final class b extends GeneratedMessageLite.a implements xv0
    {
        public b() {
            super(Target.Z());
        }
        
        public b A(final Target.c c) {
            ((GeneratedMessageLite.a)this).s();
            ((Target)super.b).h0(c);
            return this;
        }
        
        public b B(final r.b b) {
            ((GeneratedMessageLite.a)this).s();
            ((Target)super.b).i0((r)((GeneratedMessageLite.a)b).p());
            return this;
        }
        
        public b C(final QueryTarget queryTarget) {
            ((GeneratedMessageLite.a)this).s();
            ((Target)super.b).j0(queryTarget);
            return this;
        }
        
        public b D(final j0 j0) {
            ((GeneratedMessageLite.a)this).s();
            ((Target)super.b).k0(j0);
            return this;
        }
        
        public b E(final ByteString byteString) {
            ((GeneratedMessageLite.a)this).s();
            ((Target)super.b).l0(byteString);
            return this;
        }
        
        public b F(final int n) {
            ((GeneratedMessageLite.a)this).s();
            ((Target)super.b).m0(n);
            return this;
        }
    }
    
    public static final class c extends GeneratedMessageLite implements xv0
    {
        private static final c DEFAULT_INSTANCE;
        public static final int DOCUMENTS_FIELD_NUMBER = 2;
        private static volatile b31 PARSER;
        private t.e documents_;
        
        static {
            GeneratedMessageLite.V(c.class, DEFAULT_INSTANCE = new c());
        }
        
        public c() {
            this.documents_ = GeneratedMessageLite.A();
        }
        
        public static /* synthetic */ c Z() {
            return c.DEFAULT_INSTANCE;
        }
        
        public static c d0() {
            return c.DEFAULT_INSTANCE;
        }
        
        public static a g0() {
            return (a)c.DEFAULT_INSTANCE.u();
        }
        
        public final void b0(final String s) {
            s.getClass();
            this.c0();
            this.documents_.add(s);
        }
        
        public final void c0() {
            final t.e documents_ = this.documents_;
            if (!documents_.h()) {
                this.documents_ = GeneratedMessageLite.L(documents_);
            }
        }
        
        public String e0(final int n) {
            return this.documents_.get(n);
        }
        
        public int f0() {
            return this.documents_.size();
        }
        
        @Override
        public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
            switch (Target$a.a[methodToInvoke.ordinal()]) {
                default: {
                    throw new UnsupportedOperationException();
                }
                case 7: {
                    return null;
                }
                case 6: {
                    return 1;
                }
                case 5: {
                    final b31 parser;
                    if ((parser = c.PARSER) == null) {
                        synchronized (c.class) {
                            if (c.PARSER == null) {
                                c.PARSER = new GeneratedMessageLite.b(c.DEFAULT_INSTANCE);
                            }
                        }
                    }
                    return parser;
                }
                case 4: {
                    return c.DEFAULT_INSTANCE;
                }
                case 3: {
                    return GeneratedMessageLite.N(c.DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0002\u0002\u0001\u0000\u0001\u0000\u0002\u021a", new Object[] { "documents_" });
                }
                case 2: {
                    return new a((Target$a)null);
                }
                case 1: {
                    return new c();
                }
            }
        }
        
        public static final class a extends GeneratedMessageLite.a implements xv0
        {
            public a() {
                super(Target.c.Z());
            }
            
            public a A(final String s) {
                ((GeneratedMessageLite.a)this).s();
                ((Target.c)super.b).b0(s);
                return this;
            }
        }
    }
}

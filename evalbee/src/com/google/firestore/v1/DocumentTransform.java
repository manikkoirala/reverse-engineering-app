// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

import com.google.protobuf.a0;
import java.util.List;
import com.google.protobuf.t;
import com.google.protobuf.GeneratedMessageLite;

public final class DocumentTransform extends GeneratedMessageLite implements xv0
{
    private static final DocumentTransform DEFAULT_INSTANCE;
    public static final int DOCUMENT_FIELD_NUMBER = 1;
    public static final int FIELD_TRANSFORMS_FIELD_NUMBER = 2;
    private static volatile b31 PARSER;
    private String document_;
    private t.e fieldTransforms_;
    
    static {
        GeneratedMessageLite.V(DocumentTransform.class, DEFAULT_INSTANCE = new DocumentTransform());
    }
    
    public DocumentTransform() {
        this.document_ = "";
        this.fieldTransforms_ = GeneratedMessageLite.A();
    }
    
    public static /* synthetic */ DocumentTransform Z() {
        return DocumentTransform.DEFAULT_INSTANCE;
    }
    
    public static DocumentTransform a0() {
        return DocumentTransform.DEFAULT_INSTANCE;
    }
    
    public List b0() {
        return this.fieldTransforms_;
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (DocumentTransform$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = DocumentTransform.PARSER) == null) {
                    synchronized (DocumentTransform.class) {
                        if (DocumentTransform.PARSER == null) {
                            DocumentTransform.PARSER = new GeneratedMessageLite.b(DocumentTransform.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return DocumentTransform.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(DocumentTransform.DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0001\u0000\u0001\u0208\u0002\u001b", new Object[] { "document_", "fieldTransforms_", FieldTransform.class });
            }
            case 2: {
                return new b((DocumentTransform$a)null);
            }
            case 1: {
                return new DocumentTransform();
            }
        }
    }
    
    public static final class FieldTransform extends GeneratedMessageLite implements xv0
    {
        public static final int APPEND_MISSING_ELEMENTS_FIELD_NUMBER = 6;
        private static final FieldTransform DEFAULT_INSTANCE;
        public static final int FIELD_PATH_FIELD_NUMBER = 1;
        public static final int INCREMENT_FIELD_NUMBER = 3;
        public static final int MAXIMUM_FIELD_NUMBER = 4;
        public static final int MINIMUM_FIELD_NUMBER = 5;
        private static volatile b31 PARSER;
        public static final int REMOVE_ALL_FROM_ARRAY_FIELD_NUMBER = 7;
        public static final int SET_TO_SERVER_VALUE_FIELD_NUMBER = 2;
        private String fieldPath_;
        private int transformTypeCase_;
        private Object transformType_;
        
        static {
            GeneratedMessageLite.V(FieldTransform.class, DEFAULT_INSTANCE = new FieldTransform());
        }
        
        public FieldTransform() {
            this.transformTypeCase_ = 0;
            this.fieldPath_ = "";
        }
        
        public static /* synthetic */ FieldTransform Z() {
            return FieldTransform.DEFAULT_INSTANCE;
        }
        
        public static a l0() {
            return (a)FieldTransform.DEFAULT_INSTANCE.u();
        }
        
        public com.google.firestore.v1.a f0() {
            if (this.transformTypeCase_ == 6) {
                return (com.google.firestore.v1.a)this.transformType_;
            }
            return com.google.firestore.v1.a.g0();
        }
        
        public String g0() {
            return this.fieldPath_;
        }
        
        public Value h0() {
            if (this.transformTypeCase_ == 3) {
                return (Value)this.transformType_;
            }
            return Value.o0();
        }
        
        public com.google.firestore.v1.a i0() {
            if (this.transformTypeCase_ == 7) {
                return (com.google.firestore.v1.a)this.transformType_;
            }
            return com.google.firestore.v1.a.g0();
        }
        
        public ServerValue j0() {
            if (this.transformTypeCase_ == 2) {
                ServerValue serverValue;
                if ((serverValue = ServerValue.forNumber((int)this.transformType_)) == null) {
                    serverValue = ServerValue.UNRECOGNIZED;
                }
                return serverValue;
            }
            return ServerValue.SERVER_VALUE_UNSPECIFIED;
        }
        
        public TransformTypeCase k0() {
            return TransformTypeCase.forNumber(this.transformTypeCase_);
        }
        
        public final void m0(final com.google.firestore.v1.a transformType_) {
            transformType_.getClass();
            this.transformType_ = transformType_;
            this.transformTypeCase_ = 6;
        }
        
        public final void n0(final String fieldPath_) {
            fieldPath_.getClass();
            this.fieldPath_ = fieldPath_;
        }
        
        public final void o0(final Value transformType_) {
            transformType_.getClass();
            this.transformType_ = transformType_;
            this.transformTypeCase_ = 3;
        }
        
        public final void p0(final com.google.firestore.v1.a transformType_) {
            transformType_.getClass();
            this.transformType_ = transformType_;
            this.transformTypeCase_ = 7;
        }
        
        public final void q0(final ServerValue serverValue) {
            this.transformType_ = serverValue.getNumber();
            this.transformTypeCase_ = 2;
        }
        
        @Override
        public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
            switch (DocumentTransform$a.a[methodToInvoke.ordinal()]) {
                default: {
                    throw new UnsupportedOperationException();
                }
                case 7: {
                    return null;
                }
                case 6: {
                    return 1;
                }
                case 5: {
                    final b31 parser;
                    if ((parser = FieldTransform.PARSER) == null) {
                        synchronized (FieldTransform.class) {
                            if (FieldTransform.PARSER == null) {
                                FieldTransform.PARSER = new GeneratedMessageLite.b(FieldTransform.DEFAULT_INSTANCE);
                            }
                        }
                    }
                    return parser;
                }
                case 4: {
                    return FieldTransform.DEFAULT_INSTANCE;
                }
                case 3: {
                    return GeneratedMessageLite.N(FieldTransform.DEFAULT_INSTANCE, "\u0000\u0007\u0001\u0000\u0001\u0007\u0007\u0000\u0000\u0000\u0001\u0208\u0002?\u0000\u0003<\u0000\u0004<\u0000\u0005<\u0000\u0006<\u0000\u0007<\u0000", new Object[] { "transformType_", "transformTypeCase_", "fieldPath_", Value.class, Value.class, Value.class, com.google.firestore.v1.a.class, com.google.firestore.v1.a.class });
                }
                case 2: {
                    return new a((DocumentTransform$a)null);
                }
                case 1: {
                    return new FieldTransform();
                }
            }
        }
        
        public enum ServerValue implements t.a
        {
            private static final ServerValue[] $VALUES;
            
            REQUEST_TIME(1);
            
            public static final int REQUEST_TIME_VALUE = 1;
            
            SERVER_VALUE_UNSPECIFIED(0);
            
            public static final int SERVER_VALUE_UNSPECIFIED_VALUE = 0;
            
            UNRECOGNIZED(-1);
            
            private static final t.b internalValueMap;
            private final int value;
            
            static {
                internalValueMap = new t.b() {};
            }
            
            private ServerValue(final int value) {
                this.value = value;
            }
            
            public static ServerValue forNumber(final int n) {
                if (n == 0) {
                    return ServerValue.SERVER_VALUE_UNSPECIFIED;
                }
                if (n != 1) {
                    return null;
                }
                return ServerValue.REQUEST_TIME;
            }
            
            public static t.b internalGetValueMap() {
                return ServerValue.internalValueMap;
            }
            
            public static t.c internalGetVerifier() {
                return b.a;
            }
            
            @Deprecated
            public static ServerValue valueOf(final int n) {
                return forNumber(n);
            }
            
            @Override
            public final int getNumber() {
                if (this != ServerValue.UNRECOGNIZED) {
                    return this.value;
                }
                throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
            }
            
            public static final class b implements t.c
            {
                public static final t.c a;
                
                static {
                    a = new b();
                }
                
                @Override
                public boolean a(final int n) {
                    return ServerValue.forNumber(n) != null;
                }
            }
        }
        
        public enum TransformTypeCase
        {
            private static final TransformTypeCase[] $VALUES;
            
            APPEND_MISSING_ELEMENTS(6), 
            INCREMENT(3), 
            MAXIMUM(4), 
            MINIMUM(5), 
            REMOVE_ALL_FROM_ARRAY(7), 
            SET_TO_SERVER_VALUE(2), 
            TRANSFORMTYPE_NOT_SET(0);
            
            private final int value;
            
            private TransformTypeCase(final int value) {
                this.value = value;
            }
            
            public static TransformTypeCase forNumber(final int n) {
                if (n == 0) {
                    return TransformTypeCase.TRANSFORMTYPE_NOT_SET;
                }
                switch (n) {
                    default: {
                        return null;
                    }
                    case 7: {
                        return TransformTypeCase.REMOVE_ALL_FROM_ARRAY;
                    }
                    case 6: {
                        return TransformTypeCase.APPEND_MISSING_ELEMENTS;
                    }
                    case 5: {
                        return TransformTypeCase.MINIMUM;
                    }
                    case 4: {
                        return TransformTypeCase.MAXIMUM;
                    }
                    case 3: {
                        return TransformTypeCase.INCREMENT;
                    }
                    case 2: {
                        return TransformTypeCase.SET_TO_SERVER_VALUE;
                    }
                }
            }
            
            @Deprecated
            public static TransformTypeCase valueOf(final int n) {
                return forNumber(n);
            }
            
            public int getNumber() {
                return this.value;
            }
        }
        
        public static final class a extends GeneratedMessageLite.a implements xv0
        {
            public a() {
                super(FieldTransform.Z());
            }
            
            public a A(final com.google.firestore.v1.a.b b) {
                ((GeneratedMessageLite.a)this).s();
                ((FieldTransform)super.b).m0((com.google.firestore.v1.a)((GeneratedMessageLite.a)b).p());
                return this;
            }
            
            public a B(final String s) {
                ((GeneratedMessageLite.a)this).s();
                ((FieldTransform)super.b).n0(s);
                return this;
            }
            
            public a C(final Value value) {
                ((GeneratedMessageLite.a)this).s();
                ((FieldTransform)super.b).o0(value);
                return this;
            }
            
            public a D(final com.google.firestore.v1.a.b b) {
                ((GeneratedMessageLite.a)this).s();
                ((FieldTransform)super.b).p0((com.google.firestore.v1.a)((GeneratedMessageLite.a)b).p());
                return this;
            }
            
            public a E(final ServerValue serverValue) {
                ((GeneratedMessageLite.a)this).s();
                ((FieldTransform)super.b).q0(serverValue);
                return this;
            }
        }
    }
    
    public static final class b extends GeneratedMessageLite.a implements xv0
    {
        public b() {
            super(DocumentTransform.Z());
        }
    }
}

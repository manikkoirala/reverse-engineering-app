// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

import com.google.protobuf.a0;
import java.util.List;
import com.google.protobuf.t;
import com.google.protobuf.ByteString;
import com.google.protobuf.j0;
import com.google.protobuf.GeneratedMessageLite;

public final class TargetChange extends GeneratedMessageLite implements xv0
{
    public static final int CAUSE_FIELD_NUMBER = 3;
    private static final TargetChange DEFAULT_INSTANCE;
    private static volatile b31 PARSER;
    public static final int READ_TIME_FIELD_NUMBER = 6;
    public static final int RESUME_TOKEN_FIELD_NUMBER = 4;
    public static final int TARGET_CHANGE_TYPE_FIELD_NUMBER = 1;
    public static final int TARGET_IDS_FIELD_NUMBER = 2;
    private bq1 cause_;
    private j0 readTime_;
    private ByteString resumeToken_;
    private int targetChangeType_;
    private int targetIdsMemoizedSerializedSize;
    private t.d targetIds_;
    
    static {
        GeneratedMessageLite.V(TargetChange.class, DEFAULT_INSTANCE = new TargetChange());
    }
    
    public TargetChange() {
        this.targetIdsMemoizedSerializedSize = -1;
        this.targetIds_ = GeneratedMessageLite.z();
        this.resumeToken_ = ByteString.EMPTY;
    }
    
    public static /* synthetic */ TargetChange Z() {
        return TargetChange.DEFAULT_INSTANCE;
    }
    
    public static TargetChange b0() {
        return TargetChange.DEFAULT_INSTANCE;
    }
    
    public bq1 a0() {
        bq1 bq1;
        if ((bq1 = this.cause_) == null) {
            bq1 = bq1.b0();
        }
        return bq1;
    }
    
    public j0 c0() {
        j0 j0;
        if ((j0 = this.readTime_) == null) {
            j0 = com.google.protobuf.j0.c0();
        }
        return j0;
    }
    
    public ByteString d0() {
        return this.resumeToken_;
    }
    
    public TargetChangeType e0() {
        TargetChangeType targetChangeType;
        if ((targetChangeType = TargetChangeType.forNumber(this.targetChangeType_)) == null) {
            targetChangeType = TargetChangeType.UNRECOGNIZED;
        }
        return targetChangeType;
    }
    
    public int f0() {
        return this.targetIds_.size();
    }
    
    public List g0() {
        return this.targetIds_;
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (TargetChange$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = TargetChange.PARSER) == null) {
                    synchronized (TargetChange.class) {
                        if (TargetChange.PARSER == null) {
                            TargetChange.PARSER = new GeneratedMessageLite.b(TargetChange.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return TargetChange.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(TargetChange.DEFAULT_INSTANCE, "\u0000\u0005\u0000\u0000\u0001\u0006\u0005\u0000\u0001\u0000\u0001\f\u0002'\u0003\t\u0004\n\u0006\t", new Object[] { "targetChangeType_", "targetIds_", "cause_", "resumeToken_", "readTime_" });
            }
            case 2: {
                return new b((TargetChange$a)null);
            }
            case 1: {
                return new TargetChange();
            }
        }
    }
    
    public enum TargetChangeType implements t.a
    {
        private static final TargetChangeType[] $VALUES;
        
        ADD(1);
        
        public static final int ADD_VALUE = 1;
        
        CURRENT(3);
        
        public static final int CURRENT_VALUE = 3;
        
        NO_CHANGE(0);
        
        public static final int NO_CHANGE_VALUE = 0;
        
        REMOVE(2);
        
        public static final int REMOVE_VALUE = 2;
        
        RESET(4);
        
        public static final int RESET_VALUE = 4;
        
        UNRECOGNIZED(-1);
        
        private static final t.b internalValueMap;
        private final int value;
        
        static {
            internalValueMap = new t.b() {};
        }
        
        private TargetChangeType(final int value) {
            this.value = value;
        }
        
        public static TargetChangeType forNumber(final int n) {
            if (n == 0) {
                return TargetChangeType.NO_CHANGE;
            }
            if (n == 1) {
                return TargetChangeType.ADD;
            }
            if (n == 2) {
                return TargetChangeType.REMOVE;
            }
            if (n == 3) {
                return TargetChangeType.CURRENT;
            }
            if (n != 4) {
                return null;
            }
            return TargetChangeType.RESET;
        }
        
        public static t.b internalGetValueMap() {
            return TargetChangeType.internalValueMap;
        }
        
        public static t.c internalGetVerifier() {
            return b.a;
        }
        
        @Deprecated
        public static TargetChangeType valueOf(final int n) {
            return forNumber(n);
        }
        
        @Override
        public final int getNumber() {
            if (this != TargetChangeType.UNRECOGNIZED) {
                return this.value;
            }
            throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
        }
        
        public static final class b implements t.c
        {
            public static final t.c a;
            
            static {
                a = new b();
            }
            
            @Override
            public boolean a(final int n) {
                return TargetChangeType.forNumber(n) != null;
            }
        }
    }
    
    public static final class b extends a implements xv0
    {
        public b() {
            super(TargetChange.Z());
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

import com.google.protobuf.a0;
import java.util.List;
import com.google.protobuf.t;
import com.google.protobuf.j0;
import com.google.protobuf.GeneratedMessageLite;

public final class i extends GeneratedMessageLite implements xv0
{
    private static final i DEFAULT_INSTANCE;
    public static final int DOCUMENT_FIELD_NUMBER = 1;
    private static volatile b31 PARSER;
    public static final int READ_TIME_FIELD_NUMBER = 4;
    public static final int REMOVED_TARGET_IDS_FIELD_NUMBER = 2;
    private String document_;
    private j0 readTime_;
    private int removedTargetIdsMemoizedSerializedSize;
    private t.d removedTargetIds_;
    
    static {
        GeneratedMessageLite.V(i.class, DEFAULT_INSTANCE = new i());
    }
    
    public i() {
        this.removedTargetIdsMemoizedSerializedSize = -1;
        this.document_ = "";
        this.removedTargetIds_ = GeneratedMessageLite.z();
    }
    
    public static /* synthetic */ i Z() {
        return i.DEFAULT_INSTANCE;
    }
    
    public static i a0() {
        return i.DEFAULT_INSTANCE;
    }
    
    public String b0() {
        return this.document_;
    }
    
    public List c0() {
        return this.removedTargetIds_;
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (i$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = i.PARSER) == null) {
                    synchronized (i.class) {
                        if (i.PARSER == null) {
                            i.PARSER = new GeneratedMessageLite.b(i.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return i.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(i.DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0004\u0003\u0000\u0001\u0000\u0001\u0208\u0002'\u0004\t", new Object[] { "document_", "removedTargetIds_", "readTime_" });
            }
            case 2: {
                return new b((i$a)null);
            }
            case 1: {
                return new i();
            }
        }
    }
    
    public static final class b extends a implements xv0
    {
        public b() {
            super(i.Z());
        }
    }
}

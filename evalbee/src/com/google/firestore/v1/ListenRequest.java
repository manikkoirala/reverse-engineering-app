// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

import com.google.protobuf.WireFormat;
import com.google.protobuf.x;
import com.google.protobuf.a0;
import java.util.Map;
import com.google.protobuf.MapFieldLite;
import com.google.protobuf.GeneratedMessageLite;

public final class ListenRequest extends GeneratedMessageLite implements xv0
{
    public static final int ADD_TARGET_FIELD_NUMBER = 2;
    public static final int DATABASE_FIELD_NUMBER = 1;
    private static final ListenRequest DEFAULT_INSTANCE;
    public static final int LABELS_FIELD_NUMBER = 4;
    private static volatile b31 PARSER;
    public static final int REMOVE_TARGET_FIELD_NUMBER = 3;
    private String database_;
    private MapFieldLite<String, String> labels_;
    private int targetChangeCase_;
    private Object targetChange_;
    
    static {
        GeneratedMessageLite.V(ListenRequest.class, DEFAULT_INSTANCE = new ListenRequest());
    }
    
    public ListenRequest() {
        this.targetChangeCase_ = 0;
        this.labels_ = MapFieldLite.emptyMapField();
        this.database_ = "";
    }
    
    public static /* synthetic */ ListenRequest Z() {
        return ListenRequest.DEFAULT_INSTANCE;
    }
    
    public static ListenRequest e0() {
        return ListenRequest.DEFAULT_INSTANCE;
    }
    
    public static b h0() {
        return (b)ListenRequest.DEFAULT_INSTANCE.u();
    }
    
    public final Map f0() {
        return this.g0();
    }
    
    public final MapFieldLite g0() {
        if (!this.labels_.isMutable()) {
            this.labels_ = this.labels_.mutableCopy();
        }
        return this.labels_;
    }
    
    public final void i0(final Target targetChange_) {
        targetChange_.getClass();
        this.targetChange_ = targetChange_;
        this.targetChangeCase_ = 2;
    }
    
    public final void j0(final String database_) {
        database_.getClass();
        this.database_ = database_;
    }
    
    public final void k0(final int i) {
        this.targetChangeCase_ = 3;
        this.targetChange_ = i;
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (ListenRequest$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = ListenRequest.PARSER) == null) {
                    synchronized (ListenRequest.class) {
                        if (ListenRequest.PARSER == null) {
                            ListenRequest.PARSER = new GeneratedMessageLite.b(ListenRequest.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return ListenRequest.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(ListenRequest.DEFAULT_INSTANCE, "\u0000\u0004\u0001\u0000\u0001\u0004\u0004\u0001\u0000\u0000\u0001\u0208\u0002<\u0000\u00037\u0000\u00042", new Object[] { "targetChange_", "targetChangeCase_", "database_", Target.class, "labels_", c.a });
            }
            case 2: {
                return new b((ListenRequest$a)null);
            }
            case 1: {
                return new ListenRequest();
            }
        }
    }
    
    public enum TargetChangeCase
    {
        private static final TargetChangeCase[] $VALUES;
        
        ADD_TARGET(2), 
        REMOVE_TARGET(3), 
        TARGETCHANGE_NOT_SET(0);
        
        private final int value;
        
        private TargetChangeCase(final int value) {
            this.value = value;
        }
        
        public static TargetChangeCase forNumber(final int n) {
            if (n == 0) {
                return TargetChangeCase.TARGETCHANGE_NOT_SET;
            }
            if (n == 2) {
                return TargetChangeCase.ADD_TARGET;
            }
            if (n != 3) {
                return null;
            }
            return TargetChangeCase.REMOVE_TARGET;
        }
        
        @Deprecated
        public static TargetChangeCase valueOf(final int n) {
            return forNumber(n);
        }
        
        public int getNumber() {
            return this.value;
        }
    }
    
    public static final class b extends a implements xv0
    {
        public b() {
            super(ListenRequest.Z());
        }
        
        public b A(final Map map) {
            ((a)this).s();
            ((ListenRequest)super.b).f0().putAll(map);
            return this;
        }
        
        public b B(final Target target) {
            ((a)this).s();
            ((ListenRequest)super.b).i0(target);
            return this;
        }
        
        public b C(final String s) {
            ((a)this).s();
            ((ListenRequest)super.b).j0(s);
            return this;
        }
        
        public b D(final int n) {
            ((a)this).s();
            ((ListenRequest)super.b).k0(n);
            return this;
        }
    }
    
    public abstract static final class c
    {
        public static final x a;
        
        static {
            final WireFormat.FieldType string = WireFormat.FieldType.STRING;
            a = x.d(string, "", string, "");
        }
    }
}

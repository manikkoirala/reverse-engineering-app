// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

public enum BatchGetDocumentsResponse$ResultCase
{
    private static final BatchGetDocumentsResponse$ResultCase[] $VALUES;
    
    FOUND(1), 
    MISSING(2), 
    RESULT_NOT_SET(0);
    
    private final int value;
    
    private BatchGetDocumentsResponse$ResultCase(final int value) {
        this.value = value;
    }
    
    public static BatchGetDocumentsResponse$ResultCase forNumber(final int n) {
        if (n == 0) {
            return BatchGetDocumentsResponse$ResultCase.RESULT_NOT_SET;
        }
        if (n == 1) {
            return BatchGetDocumentsResponse$ResultCase.FOUND;
        }
        if (n != 2) {
            return null;
        }
        return BatchGetDocumentsResponse$ResultCase.MISSING;
    }
    
    @Deprecated
    public static BatchGetDocumentsResponse$ResultCase valueOf(final int n) {
        return forNumber(n);
    }
    
    public int getNumber() {
        return this.value;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

import com.google.protobuf.a0;
import com.google.protobuf.GeneratedMessageLite;

public final class c extends GeneratedMessageLite implements xv0
{
    public static final int BITS_FIELD_NUMBER = 1;
    private static final c DEFAULT_INSTANCE;
    public static final int HASH_COUNT_FIELD_NUMBER = 2;
    private static volatile b31 PARSER;
    private com.google.firestore.v1.b bits_;
    private int hashCount_;
    
    static {
        GeneratedMessageLite.V(c.class, DEFAULT_INSTANCE = new c());
    }
    
    public static /* synthetic */ c Z() {
        return c.DEFAULT_INSTANCE;
    }
    
    public static c b0() {
        return c.DEFAULT_INSTANCE;
    }
    
    public com.google.firestore.v1.b a0() {
        com.google.firestore.v1.b b;
        if ((b = this.bits_) == null) {
            b = com.google.firestore.v1.b.b0();
        }
        return b;
    }
    
    public int c0() {
        return this.hashCount_;
    }
    
    public boolean d0() {
        return this.bits_ != null;
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (c$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = c.PARSER) == null) {
                    synchronized (c.class) {
                        if (c.PARSER == null) {
                            c.PARSER = new GeneratedMessageLite.b(c.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return c.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(c.DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\t\u0002\u0004", new Object[] { "bits_", "hashCount_" });
            }
            case 2: {
                return new b((c$a)null);
            }
            case 1: {
                return new c();
            }
        }
    }
    
    public static final class b extends a implements xv0
    {
        public b() {
            super(c.Z());
        }
    }
}

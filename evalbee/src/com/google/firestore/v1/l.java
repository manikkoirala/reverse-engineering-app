// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

import com.google.protobuf.WireFormat;
import com.google.protobuf.x;
import com.google.protobuf.a0;
import com.google.protobuf.t;
import com.google.protobuf.ByteString;
import com.google.protobuf.MapFieldLite;
import com.google.protobuf.GeneratedMessageLite;

public final class l extends GeneratedMessageLite implements xv0
{
    public static final int DATABASE_FIELD_NUMBER = 1;
    private static final l DEFAULT_INSTANCE;
    public static final int LABELS_FIELD_NUMBER = 5;
    private static volatile b31 PARSER;
    public static final int STREAM_ID_FIELD_NUMBER = 2;
    public static final int STREAM_TOKEN_FIELD_NUMBER = 4;
    public static final int WRITES_FIELD_NUMBER = 3;
    private String database_;
    private MapFieldLite<String, String> labels_;
    private String streamId_;
    private ByteString streamToken_;
    private t.e writes_;
    
    static {
        GeneratedMessageLite.V(l.class, DEFAULT_INSTANCE = new l());
    }
    
    public l() {
        this.labels_ = MapFieldLite.emptyMapField();
        this.database_ = "";
        this.streamId_ = "";
        this.writes_ = GeneratedMessageLite.A();
        this.streamToken_ = ByteString.EMPTY;
    }
    
    public static /* synthetic */ l Z() {
        return l.DEFAULT_INSTANCE;
    }
    
    public static l f0() {
        return l.DEFAULT_INSTANCE;
    }
    
    public static b g0() {
        return (b)l.DEFAULT_INSTANCE.u();
    }
    
    public final void d0(final Write write) {
        write.getClass();
        this.e0();
        this.writes_.add(write);
    }
    
    public final void e0() {
        final t.e writes_ = this.writes_;
        if (!writes_.h()) {
            this.writes_ = GeneratedMessageLite.L(writes_);
        }
    }
    
    public final void h0(final String database_) {
        database_.getClass();
        this.database_ = database_;
    }
    
    public final void i0(final ByteString streamToken_) {
        streamToken_.getClass();
        this.streamToken_ = streamToken_;
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (l$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = l.PARSER) == null) {
                    synchronized (l.class) {
                        if (l.PARSER == null) {
                            l.PARSER = new GeneratedMessageLite.b(l.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return l.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(l.DEFAULT_INSTANCE, "\u0000\u0005\u0000\u0000\u0001\u0005\u0005\u0001\u0001\u0000\u0001\u0208\u0002\u0208\u0003\u001b\u0004\n\u00052", new Object[] { "database_", "streamId_", "writes_", Write.class, "streamToken_", "labels_", c.a });
            }
            case 2: {
                return new b((l$a)null);
            }
            case 1: {
                return new l();
            }
        }
    }
    
    public static final class b extends a implements xv0
    {
        public b() {
            super(l.Z());
        }
        
        public b A(final Write write) {
            ((a)this).s();
            ((l)super.b).d0(write);
            return this;
        }
        
        public b B(final String s) {
            ((a)this).s();
            ((l)super.b).h0(s);
            return this;
        }
        
        public b C(final ByteString byteString) {
            ((a)this).s();
            ((l)super.b).i0(byteString);
            return this;
        }
    }
    
    public abstract static final class c
    {
        public static final x a;
        
        static {
            final WireFormat.FieldType string = WireFormat.FieldType.STRING;
            a = x.d(string, "", string, "");
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

public enum GetDocumentRequest$ConsistencySelectorCase
{
    private static final GetDocumentRequest$ConsistencySelectorCase[] $VALUES;
    
    CONSISTENCYSELECTOR_NOT_SET(0), 
    READ_TIME(5), 
    TRANSACTION(3);
    
    private final int value;
    
    private GetDocumentRequest$ConsistencySelectorCase(final int value) {
        this.value = value;
    }
    
    public static GetDocumentRequest$ConsistencySelectorCase forNumber(final int n) {
        if (n == 0) {
            return GetDocumentRequest$ConsistencySelectorCase.CONSISTENCYSELECTOR_NOT_SET;
        }
        if (n == 3) {
            return GetDocumentRequest$ConsistencySelectorCase.TRANSACTION;
        }
        if (n != 5) {
            return null;
        }
        return GetDocumentRequest$ConsistencySelectorCase.READ_TIME;
    }
    
    @Deprecated
    public static GetDocumentRequest$ConsistencySelectorCase valueOf(final int n) {
        return forNumber(n);
    }
    
    public int getNumber() {
        return this.value;
    }
}

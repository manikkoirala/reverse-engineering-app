// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

import com.google.protobuf.a0;
import java.util.List;
import com.google.protobuf.t;
import com.google.protobuf.j0;
import com.google.protobuf.GeneratedMessageLite;

public final class g extends GeneratedMessageLite implements xv0
{
    private static final g DEFAULT_INSTANCE;
    public static final int DOCUMENT_FIELD_NUMBER = 1;
    private static volatile b31 PARSER;
    public static final int READ_TIME_FIELD_NUMBER = 4;
    public static final int REMOVED_TARGET_IDS_FIELD_NUMBER = 6;
    private String document_;
    private j0 readTime_;
    private int removedTargetIdsMemoizedSerializedSize;
    private t.d removedTargetIds_;
    
    static {
        GeneratedMessageLite.V(g.class, DEFAULT_INSTANCE = new g());
    }
    
    public g() {
        this.removedTargetIdsMemoizedSerializedSize = -1;
        this.document_ = "";
        this.removedTargetIds_ = GeneratedMessageLite.z();
    }
    
    public static /* synthetic */ g Z() {
        return g.DEFAULT_INSTANCE;
    }
    
    public static g a0() {
        return g.DEFAULT_INSTANCE;
    }
    
    public String b0() {
        return this.document_;
    }
    
    public j0 c0() {
        j0 j0;
        if ((j0 = this.readTime_) == null) {
            j0 = com.google.protobuf.j0.c0();
        }
        return j0;
    }
    
    public List d0() {
        return this.removedTargetIds_;
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (g$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = g.PARSER) == null) {
                    synchronized (g.class) {
                        if (g.PARSER == null) {
                            g.PARSER = new GeneratedMessageLite.b(g.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return g.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(g.DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0006\u0003\u0000\u0001\u0000\u0001\u0208\u0004\t\u0006'", new Object[] { "document_", "readTime_", "removedTargetIds_" });
            }
            case 2: {
                return new b((g$a)null);
            }
            case 1: {
                return new g();
            }
        }
    }
    
    public static final class b extends a implements xv0
    {
        public b() {
            super(g.Z());
        }
    }
}

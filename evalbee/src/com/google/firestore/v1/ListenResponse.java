// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

import com.google.protobuf.a0;
import com.google.protobuf.GeneratedMessageLite;

public final class ListenResponse extends GeneratedMessageLite implements xv0
{
    private static final ListenResponse DEFAULT_INSTANCE;
    public static final int DOCUMENT_CHANGE_FIELD_NUMBER = 3;
    public static final int DOCUMENT_DELETE_FIELD_NUMBER = 4;
    public static final int DOCUMENT_REMOVE_FIELD_NUMBER = 6;
    public static final int FILTER_FIELD_NUMBER = 5;
    private static volatile b31 PARSER;
    public static final int TARGET_CHANGE_FIELD_NUMBER = 2;
    private int responseTypeCase_;
    private Object responseType_;
    
    static {
        GeneratedMessageLite.V(ListenResponse.class, DEFAULT_INSTANCE = new ListenResponse());
    }
    
    public ListenResponse() {
        this.responseTypeCase_ = 0;
    }
    
    public static /* synthetic */ ListenResponse Z() {
        return ListenResponse.DEFAULT_INSTANCE;
    }
    
    public static ListenResponse a0() {
        return ListenResponse.DEFAULT_INSTANCE;
    }
    
    public f b0() {
        if (this.responseTypeCase_ == 3) {
            return (f)this.responseType_;
        }
        return f.a0();
    }
    
    public g c0() {
        if (this.responseTypeCase_ == 4) {
            return (g)this.responseType_;
        }
        return g.a0();
    }
    
    public i d0() {
        if (this.responseTypeCase_ == 6) {
            return (i)this.responseType_;
        }
        return i.a0();
    }
    
    public j e0() {
        if (this.responseTypeCase_ == 5) {
            return (j)this.responseType_;
        }
        return j.b0();
    }
    
    public ResponseTypeCase f0() {
        return ResponseTypeCase.forNumber(this.responseTypeCase_);
    }
    
    public TargetChange g0() {
        if (this.responseTypeCase_ == 2) {
            return (TargetChange)this.responseType_;
        }
        return TargetChange.b0();
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (ListenResponse$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = ListenResponse.PARSER) == null) {
                    synchronized (ListenResponse.class) {
                        if (ListenResponse.PARSER == null) {
                            ListenResponse.PARSER = new GeneratedMessageLite.b(ListenResponse.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return ListenResponse.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(ListenResponse.DEFAULT_INSTANCE, "\u0000\u0005\u0001\u0000\u0002\u0006\u0005\u0000\u0000\u0000\u0002<\u0000\u0003<\u0000\u0004<\u0000\u0005<\u0000\u0006<\u0000", new Object[] { "responseType_", "responseTypeCase_", TargetChange.class, f.class, g.class, j.class, i.class });
            }
            case 2: {
                return new b((ListenResponse$a)null);
            }
            case 1: {
                return new ListenResponse();
            }
        }
    }
    
    public enum ResponseTypeCase
    {
        private static final ResponseTypeCase[] $VALUES;
        
        DOCUMENT_CHANGE(3), 
        DOCUMENT_DELETE(4), 
        DOCUMENT_REMOVE(6), 
        FILTER(5), 
        RESPONSETYPE_NOT_SET(0), 
        TARGET_CHANGE(2);
        
        private final int value;
        
        private ResponseTypeCase(final int value) {
            this.value = value;
        }
        
        public static ResponseTypeCase forNumber(final int n) {
            if (n == 0) {
                return ResponseTypeCase.RESPONSETYPE_NOT_SET;
            }
            if (n == 2) {
                return ResponseTypeCase.TARGET_CHANGE;
            }
            if (n == 3) {
                return ResponseTypeCase.DOCUMENT_CHANGE;
            }
            if (n == 4) {
                return ResponseTypeCase.DOCUMENT_DELETE;
            }
            if (n == 5) {
                return ResponseTypeCase.FILTER;
            }
            if (n != 6) {
                return null;
            }
            return ResponseTypeCase.DOCUMENT_REMOVE;
        }
        
        @Deprecated
        public static ResponseTypeCase valueOf(final int n) {
            return forNumber(n);
        }
        
        public int getNumber() {
            return this.value;
        }
    }
    
    public static final class b extends a implements xv0
    {
        public b() {
            super(ListenResponse.Z());
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

public enum RunAggregationQueryRequest$QueryTypeCase
{
    private static final RunAggregationQueryRequest$QueryTypeCase[] $VALUES;
    
    QUERYTYPE_NOT_SET(0), 
    STRUCTURED_AGGREGATION_QUERY(2);
    
    private final int value;
    
    private RunAggregationQueryRequest$QueryTypeCase(final int value) {
        this.value = value;
    }
    
    public static RunAggregationQueryRequest$QueryTypeCase forNumber(final int n) {
        if (n == 0) {
            return RunAggregationQueryRequest$QueryTypeCase.QUERYTYPE_NOT_SET;
        }
        if (n != 2) {
            return null;
        }
        return RunAggregationQueryRequest$QueryTypeCase.STRUCTURED_AGGREGATION_QUERY;
    }
    
    @Deprecated
    public static RunAggregationQueryRequest$QueryTypeCase valueOf(final int n) {
        return forNumber(n);
    }
    
    public int getNumber() {
        return this.value;
    }
}

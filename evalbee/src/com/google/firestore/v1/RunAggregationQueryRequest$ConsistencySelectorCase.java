// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

public enum RunAggregationQueryRequest$ConsistencySelectorCase
{
    private static final RunAggregationQueryRequest$ConsistencySelectorCase[] $VALUES;
    
    CONSISTENCYSELECTOR_NOT_SET(0), 
    NEW_TRANSACTION(5), 
    READ_TIME(6), 
    TRANSACTION(4);
    
    private final int value;
    
    private RunAggregationQueryRequest$ConsistencySelectorCase(final int value) {
        this.value = value;
    }
    
    public static RunAggregationQueryRequest$ConsistencySelectorCase forNumber(final int n) {
        if (n == 0) {
            return RunAggregationQueryRequest$ConsistencySelectorCase.CONSISTENCYSELECTOR_NOT_SET;
        }
        if (n == 4) {
            return RunAggregationQueryRequest$ConsistencySelectorCase.TRANSACTION;
        }
        if (n == 5) {
            return RunAggregationQueryRequest$ConsistencySelectorCase.NEW_TRANSACTION;
        }
        if (n != 6) {
            return null;
        }
        return RunAggregationQueryRequest$ConsistencySelectorCase.READ_TIME;
    }
    
    @Deprecated
    public static RunAggregationQueryRequest$ConsistencySelectorCase valueOf(final int n) {
        return forNumber(n);
    }
    
    public int getNumber() {
        return this.value;
    }
}

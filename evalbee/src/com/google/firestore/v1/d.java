// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

import com.google.protobuf.a0;
import java.util.List;
import com.google.protobuf.a;
import com.google.protobuf.t;
import com.google.protobuf.GeneratedMessageLite;

public final class d extends GeneratedMessageLite implements xv0
{
    public static final int BEFORE_FIELD_NUMBER = 2;
    private static final d DEFAULT_INSTANCE;
    private static volatile b31 PARSER;
    public static final int VALUES_FIELD_NUMBER = 1;
    private boolean before_;
    private t.e values_;
    
    static {
        GeneratedMessageLite.V(d.class, DEFAULT_INSTANCE = new d());
    }
    
    public d() {
        this.values_ = GeneratedMessageLite.A();
    }
    
    public static /* synthetic */ d Z() {
        return d.DEFAULT_INSTANCE;
    }
    
    public static d f0() {
        return d.DEFAULT_INSTANCE;
    }
    
    public static b g0() {
        return (b)d.DEFAULT_INSTANCE.u();
    }
    
    public final void c0(final Iterable iterable) {
        this.d0();
        com.google.protobuf.a.j(iterable, this.values_);
    }
    
    public final void d0() {
        final t.e values_ = this.values_;
        if (!values_.h()) {
            this.values_ = GeneratedMessageLite.L(values_);
        }
    }
    
    public boolean e0() {
        return this.before_;
    }
    
    public List h() {
        return this.values_;
    }
    
    public final void h0(final boolean before_) {
        this.before_ = before_;
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (d$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = d.PARSER) == null) {
                    synchronized (d.class) {
                        if (d.PARSER == null) {
                            d.PARSER = new GeneratedMessageLite.b(d.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return d.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(d.DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0001\u0000\u0001\u001b\u0002\u0007", new Object[] { "values_", Value.class, "before_" });
            }
            case 2: {
                return new b((d$a)null);
            }
            case 1: {
                return new d();
            }
        }
    }
    
    public static final class b extends a implements xv0
    {
        public b() {
            super(d.Z());
        }
        
        public b A(final Iterable iterable) {
            ((a)this).s();
            ((d)super.b).c0(iterable);
            return this;
        }
        
        public b B(final boolean b) {
            ((a)this).s();
            ((d)super.b).h0(b);
            return this;
        }
    }
}

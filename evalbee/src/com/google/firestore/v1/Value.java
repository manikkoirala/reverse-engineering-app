// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.v1;

import com.google.protobuf.a0;
import com.google.protobuf.NullValue;
import com.google.protobuf.ByteString;
import com.google.protobuf.j0;
import com.google.protobuf.GeneratedMessageLite;

public final class Value extends GeneratedMessageLite implements xv0
{
    public static final int ARRAY_VALUE_FIELD_NUMBER = 9;
    public static final int BOOLEAN_VALUE_FIELD_NUMBER = 1;
    public static final int BYTES_VALUE_FIELD_NUMBER = 18;
    private static final Value DEFAULT_INSTANCE;
    public static final int DOUBLE_VALUE_FIELD_NUMBER = 3;
    public static final int GEO_POINT_VALUE_FIELD_NUMBER = 8;
    public static final int INTEGER_VALUE_FIELD_NUMBER = 2;
    public static final int MAP_VALUE_FIELD_NUMBER = 6;
    public static final int NULL_VALUE_FIELD_NUMBER = 11;
    private static volatile b31 PARSER;
    public static final int REFERENCE_VALUE_FIELD_NUMBER = 5;
    public static final int STRING_VALUE_FIELD_NUMBER = 17;
    public static final int TIMESTAMP_VALUE_FIELD_NUMBER = 10;
    private int valueTypeCase_;
    private Object valueType_;
    
    static {
        GeneratedMessageLite.V(Value.class, DEFAULT_INSTANCE = new Value());
    }
    
    public Value() {
        this.valueTypeCase_ = 0;
    }
    
    public static /* synthetic */ Value Z() {
        return Value.DEFAULT_INSTANCE;
    }
    
    public static Value o0() {
        return Value.DEFAULT_INSTANCE;
    }
    
    public static b x0() {
        return (b)Value.DEFAULT_INSTANCE.u();
    }
    
    public final void A0(final ByteString valueType_) {
        valueType_.getClass();
        this.valueTypeCase_ = 18;
        this.valueType_ = valueType_;
    }
    
    public final void B0(final double d) {
        this.valueTypeCase_ = 3;
        this.valueType_ = d;
    }
    
    public final void C0(final ui0 valueType_) {
        valueType_.getClass();
        this.valueType_ = valueType_;
        this.valueTypeCase_ = 8;
    }
    
    public final void D0(final long l) {
        this.valueTypeCase_ = 2;
        this.valueType_ = l;
    }
    
    public final void E0(final k valueType_) {
        valueType_.getClass();
        this.valueType_ = valueType_;
        this.valueTypeCase_ = 6;
    }
    
    public final void F0(final NullValue nullValue) {
        this.valueType_ = nullValue.getNumber();
        this.valueTypeCase_ = 11;
    }
    
    public final void G0(final String valueType_) {
        valueType_.getClass();
        this.valueTypeCase_ = 5;
        this.valueType_ = valueType_;
    }
    
    public final void H0(final String valueType_) {
        valueType_.getClass();
        this.valueTypeCase_ = 17;
        this.valueType_ = valueType_;
    }
    
    public final void I0(final j0 valueType_) {
        valueType_.getClass();
        this.valueType_ = valueType_;
        this.valueTypeCase_ = 10;
    }
    
    public com.google.firestore.v1.a l0() {
        if (this.valueTypeCase_ == 9) {
            return (com.google.firestore.v1.a)this.valueType_;
        }
        return com.google.firestore.v1.a.g0();
    }
    
    public boolean m0() {
        return this.valueTypeCase_ == 1 && (boolean)this.valueType_;
    }
    
    public ByteString n0() {
        if (this.valueTypeCase_ == 18) {
            return (ByteString)this.valueType_;
        }
        return ByteString.EMPTY;
    }
    
    public double p0() {
        if (this.valueTypeCase_ == 3) {
            return (double)this.valueType_;
        }
        return 0.0;
    }
    
    public ui0 q0() {
        if (this.valueTypeCase_ == 8) {
            return (ui0)this.valueType_;
        }
        return ui0.c0();
    }
    
    public long r0() {
        if (this.valueTypeCase_ == 2) {
            return (long)this.valueType_;
        }
        return 0L;
    }
    
    public k s0() {
        if (this.valueTypeCase_ == 6) {
            return (k)this.valueType_;
        }
        return k.b0();
    }
    
    public String t0() {
        String s;
        if (this.valueTypeCase_ == 5) {
            s = (String)this.valueType_;
        }
        else {
            s = "";
        }
        return s;
    }
    
    public String u0() {
        String s;
        if (this.valueTypeCase_ == 17) {
            s = (String)this.valueType_;
        }
        else {
            s = "";
        }
        return s;
    }
    
    public j0 v0() {
        if (this.valueTypeCase_ == 10) {
            return (j0)this.valueType_;
        }
        return j0.c0();
    }
    
    public ValueTypeCase w0() {
        return ValueTypeCase.forNumber(this.valueTypeCase_);
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (Value$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = Value.PARSER) == null) {
                    synchronized (Value.class) {
                        if (Value.PARSER == null) {
                            Value.PARSER = new GeneratedMessageLite.b(Value.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return Value.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(Value.DEFAULT_INSTANCE, "\u0000\u000b\u0001\u0000\u0001\u0012\u000b\u0000\u0000\u0000\u0001:\u0000\u00025\u0000\u00033\u0000\u0005\u023b\u0000\u0006<\u0000\b<\u0000\t<\u0000\n<\u0000\u000b?\u0000\u0011\u023b\u0000\u0012=\u0000", new Object[] { "valueType_", "valueTypeCase_", k.class, ui0.class, com.google.firestore.v1.a.class, j0.class });
            }
            case 2: {
                return new b((Value$a)null);
            }
            case 1: {
                return new Value();
            }
        }
    }
    
    public final void y0(final com.google.firestore.v1.a valueType_) {
        valueType_.getClass();
        this.valueType_ = valueType_;
        this.valueTypeCase_ = 9;
    }
    
    public final void z0(final boolean b) {
        this.valueTypeCase_ = 1;
        this.valueType_ = b;
    }
    
    public enum ValueTypeCase
    {
        private static final ValueTypeCase[] $VALUES;
        
        ARRAY_VALUE(9), 
        BOOLEAN_VALUE(1), 
        BYTES_VALUE(18), 
        DOUBLE_VALUE(3), 
        GEO_POINT_VALUE(8), 
        INTEGER_VALUE(2), 
        MAP_VALUE(6), 
        NULL_VALUE(11), 
        REFERENCE_VALUE(5), 
        STRING_VALUE(17), 
        TIMESTAMP_VALUE(10), 
        VALUETYPE_NOT_SET(0);
        
        private final int value;
        
        private ValueTypeCase(final int value) {
            this.value = value;
        }
        
        public static ValueTypeCase forNumber(final int n) {
            if (n == 0) {
                return ValueTypeCase.VALUETYPE_NOT_SET;
            }
            if (n == 1) {
                return ValueTypeCase.BOOLEAN_VALUE;
            }
            if (n == 2) {
                return ValueTypeCase.INTEGER_VALUE;
            }
            if (n == 3) {
                return ValueTypeCase.DOUBLE_VALUE;
            }
            if (n == 5) {
                return ValueTypeCase.REFERENCE_VALUE;
            }
            if (n == 6) {
                return ValueTypeCase.MAP_VALUE;
            }
            if (n == 17) {
                return ValueTypeCase.STRING_VALUE;
            }
            if (n == 18) {
                return ValueTypeCase.BYTES_VALUE;
            }
            switch (n) {
                default: {
                    return null;
                }
                case 11: {
                    return ValueTypeCase.NULL_VALUE;
                }
                case 10: {
                    return ValueTypeCase.TIMESTAMP_VALUE;
                }
                case 9: {
                    return ValueTypeCase.ARRAY_VALUE;
                }
                case 8: {
                    return ValueTypeCase.GEO_POINT_VALUE;
                }
            }
        }
        
        @Deprecated
        public static ValueTypeCase valueOf(final int n) {
            return forNumber(n);
        }
        
        public int getNumber() {
            return this.value;
        }
    }
    
    public static final class b extends a implements xv0
    {
        public b() {
            super(Value.Z());
        }
        
        public b A(final com.google.firestore.v1.a.b b) {
            ((a)this).s();
            ((Value)super.b).y0((com.google.firestore.v1.a)((a)b).p());
            return this;
        }
        
        public b B(final com.google.firestore.v1.a a) {
            ((a)this).s();
            ((Value)super.b).y0(a);
            return this;
        }
        
        public b C(final boolean b) {
            ((a)this).s();
            ((Value)super.b).z0(b);
            return this;
        }
        
        public b D(final ByteString byteString) {
            ((a)this).s();
            ((Value)super.b).A0(byteString);
            return this;
        }
        
        public b E(final double n) {
            ((a)this).s();
            ((Value)super.b).B0(n);
            return this;
        }
        
        public b F(final ui0.b b) {
            ((a)this).s();
            ((Value)super.b).C0((ui0)((a)b).p());
            return this;
        }
        
        public b G(final long n) {
            ((a)this).s();
            ((Value)super.b).D0(n);
            return this;
        }
        
        public b H(final k.b b) {
            ((a)this).s();
            ((Value)super.b).E0((k)((a)b).p());
            return this;
        }
        
        public b I(final k k) {
            ((a)this).s();
            ((Value)super.b).E0(k);
            return this;
        }
        
        public b J(final NullValue nullValue) {
            ((a)this).s();
            ((Value)super.b).F0(nullValue);
            return this;
        }
        
        public b K(final String s) {
            ((a)this).s();
            ((Value)super.b).G0(s);
            return this;
        }
        
        public b L(final String s) {
            ((a)this).s();
            ((Value)super.b).H0(s);
            return this;
        }
        
        public b M(final j0.b b) {
            ((a)this).s();
            ((Value)super.b).I0((j0)((a)b).p());
            return this;
        }
    }
}

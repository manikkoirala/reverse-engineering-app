// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firestore.admin.v1;

import com.google.protobuf.a0;
import java.util.List;
import com.google.protobuf.t;
import com.google.protobuf.GeneratedMessageLite;

public final class Index extends GeneratedMessageLite implements xv0
{
    private static final Index DEFAULT_INSTANCE;
    public static final int FIELDS_FIELD_NUMBER = 3;
    public static final int NAME_FIELD_NUMBER = 1;
    private static volatile b31 PARSER;
    public static final int QUERY_SCOPE_FIELD_NUMBER = 2;
    public static final int STATE_FIELD_NUMBER = 4;
    private t.e fields_;
    private String name_;
    private int queryScope_;
    private int state_;
    
    static {
        GeneratedMessageLite.V(Index.class, DEFAULT_INSTANCE = new Index());
    }
    
    public Index() {
        this.name_ = "";
        this.fields_ = GeneratedMessageLite.A();
    }
    
    public static /* synthetic */ Index Z() {
        return Index.DEFAULT_INSTANCE;
    }
    
    public static b f0() {
        return (b)Index.DEFAULT_INSTANCE.u();
    }
    
    public static Index g0(final byte[] array) {
        return (Index)GeneratedMessageLite.R(Index.DEFAULT_INSTANCE, array);
    }
    
    public final void c0(final IndexField indexField) {
        indexField.getClass();
        this.d0();
        this.fields_.add(indexField);
    }
    
    public final void d0() {
        final t.e fields_ = this.fields_;
        if (!fields_.h()) {
            this.fields_ = GeneratedMessageLite.L(fields_);
        }
    }
    
    public List e0() {
        return this.fields_;
    }
    
    public final void h0(final QueryScope queryScope) {
        this.queryScope_ = queryScope.getNumber();
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (Index$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = Index.PARSER) == null) {
                    synchronized (Index.class) {
                        if (Index.PARSER == null) {
                            Index.PARSER = new GeneratedMessageLite.b(Index.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return Index.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(Index.DEFAULT_INSTANCE, "\u0000\u0004\u0000\u0000\u0001\u0004\u0004\u0000\u0001\u0000\u0001\u0208\u0002\f\u0003\u001b\u0004\f", new Object[] { "name_", "queryScope_", "fields_", IndexField.class, "state_" });
            }
            case 2: {
                return new b((Index$a)null);
            }
            case 1: {
                return new Index();
            }
        }
    }
    
    public static final class IndexField extends GeneratedMessageLite implements xv0
    {
        public static final int ARRAY_CONFIG_FIELD_NUMBER = 3;
        private static final IndexField DEFAULT_INSTANCE;
        public static final int FIELD_PATH_FIELD_NUMBER = 1;
        public static final int ORDER_FIELD_NUMBER = 2;
        private static volatile b31 PARSER;
        private String fieldPath_;
        private int valueModeCase_;
        private Object valueMode_;
        
        static {
            GeneratedMessageLite.V(IndexField.class, DEFAULT_INSTANCE = new IndexField());
        }
        
        public IndexField() {
            this.valueModeCase_ = 0;
            this.fieldPath_ = "";
        }
        
        public static /* synthetic */ IndexField Z() {
            return IndexField.DEFAULT_INSTANCE;
        }
        
        public static a g0() {
            return (a)IndexField.DEFAULT_INSTANCE.u();
        }
        
        public String d0() {
            return this.fieldPath_;
        }
        
        public Order e0() {
            if (this.valueModeCase_ == 2) {
                Order order;
                if ((order = Order.forNumber((int)this.valueMode_)) == null) {
                    order = Order.UNRECOGNIZED;
                }
                return order;
            }
            return Order.ORDER_UNSPECIFIED;
        }
        
        public ValueModeCase f0() {
            return ValueModeCase.forNumber(this.valueModeCase_);
        }
        
        public final void h0(final ArrayConfig arrayConfig) {
            this.valueMode_ = arrayConfig.getNumber();
            this.valueModeCase_ = 3;
        }
        
        public final void i0(final String fieldPath_) {
            fieldPath_.getClass();
            this.fieldPath_ = fieldPath_;
        }
        
        public final void j0(final Order order) {
            this.valueMode_ = order.getNumber();
            this.valueModeCase_ = 2;
        }
        
        @Override
        public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
            switch (Index$a.a[methodToInvoke.ordinal()]) {
                default: {
                    throw new UnsupportedOperationException();
                }
                case 7: {
                    return null;
                }
                case 6: {
                    return 1;
                }
                case 5: {
                    final b31 parser;
                    if ((parser = IndexField.PARSER) == null) {
                        synchronized (IndexField.class) {
                            if (IndexField.PARSER == null) {
                                IndexField.PARSER = new GeneratedMessageLite.b(IndexField.DEFAULT_INSTANCE);
                            }
                        }
                    }
                    return parser;
                }
                case 4: {
                    return IndexField.DEFAULT_INSTANCE;
                }
                case 3: {
                    return GeneratedMessageLite.N(IndexField.DEFAULT_INSTANCE, "\u0000\u0003\u0001\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001\u0208\u0002?\u0000\u0003?\u0000", new Object[] { "valueMode_", "valueModeCase_", "fieldPath_" });
                }
                case 2: {
                    return new a((Index$a)null);
                }
                case 1: {
                    return new IndexField();
                }
            }
        }
        
        public enum ArrayConfig implements t.a
        {
            private static final ArrayConfig[] $VALUES;
            
            ARRAY_CONFIG_UNSPECIFIED(0);
            
            public static final int ARRAY_CONFIG_UNSPECIFIED_VALUE = 0;
            
            CONTAINS(1);
            
            public static final int CONTAINS_VALUE = 1;
            
            UNRECOGNIZED(-1);
            
            private static final t.b internalValueMap;
            private final int value;
            
            static {
                internalValueMap = new t.b() {};
            }
            
            private ArrayConfig(final int value) {
                this.value = value;
            }
            
            public static ArrayConfig forNumber(final int n) {
                if (n == 0) {
                    return ArrayConfig.ARRAY_CONFIG_UNSPECIFIED;
                }
                if (n != 1) {
                    return null;
                }
                return ArrayConfig.CONTAINS;
            }
            
            public static t.b internalGetValueMap() {
                return ArrayConfig.internalValueMap;
            }
            
            public static t.c internalGetVerifier() {
                return b.a;
            }
            
            @Deprecated
            public static ArrayConfig valueOf(final int n) {
                return forNumber(n);
            }
            
            @Override
            public final int getNumber() {
                if (this != ArrayConfig.UNRECOGNIZED) {
                    return this.value;
                }
                throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
            }
            
            public static final class b implements t.c
            {
                public static final t.c a;
                
                static {
                    a = new b();
                }
                
                @Override
                public boolean a(final int n) {
                    return ArrayConfig.forNumber(n) != null;
                }
            }
        }
        
        public enum Order implements t.a
        {
            private static final Order[] $VALUES;
            
            ASCENDING(1);
            
            public static final int ASCENDING_VALUE = 1;
            
            DESCENDING(2);
            
            public static final int DESCENDING_VALUE = 2;
            
            ORDER_UNSPECIFIED(0);
            
            public static final int ORDER_UNSPECIFIED_VALUE = 0;
            
            UNRECOGNIZED(-1);
            
            private static final t.b internalValueMap;
            private final int value;
            
            static {
                internalValueMap = new t.b() {};
            }
            
            private Order(final int value) {
                this.value = value;
            }
            
            public static Order forNumber(final int n) {
                if (n == 0) {
                    return Order.ORDER_UNSPECIFIED;
                }
                if (n == 1) {
                    return Order.ASCENDING;
                }
                if (n != 2) {
                    return null;
                }
                return Order.DESCENDING;
            }
            
            public static t.b internalGetValueMap() {
                return Order.internalValueMap;
            }
            
            public static t.c internalGetVerifier() {
                return b.a;
            }
            
            @Deprecated
            public static Order valueOf(final int n) {
                return forNumber(n);
            }
            
            @Override
            public final int getNumber() {
                if (this != Order.UNRECOGNIZED) {
                    return this.value;
                }
                throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
            }
            
            public static final class b implements t.c
            {
                public static final t.c a;
                
                static {
                    a = new b();
                }
                
                @Override
                public boolean a(final int n) {
                    return Order.forNumber(n) != null;
                }
            }
        }
        
        public enum ValueModeCase
        {
            private static final ValueModeCase[] $VALUES;
            
            ARRAY_CONFIG(3), 
            ORDER(2), 
            VALUEMODE_NOT_SET(0);
            
            private final int value;
            
            private ValueModeCase(final int value) {
                this.value = value;
            }
            
            public static ValueModeCase forNumber(final int n) {
                if (n == 0) {
                    return ValueModeCase.VALUEMODE_NOT_SET;
                }
                if (n == 2) {
                    return ValueModeCase.ORDER;
                }
                if (n != 3) {
                    return null;
                }
                return ValueModeCase.ARRAY_CONFIG;
            }
            
            @Deprecated
            public static ValueModeCase valueOf(final int n) {
                return forNumber(n);
            }
            
            public int getNumber() {
                return this.value;
            }
        }
        
        public static final class a extends GeneratedMessageLite.a implements xv0
        {
            public a() {
                super(IndexField.Z());
            }
            
            public a A(final ArrayConfig arrayConfig) {
                ((GeneratedMessageLite.a)this).s();
                ((IndexField)super.b).h0(arrayConfig);
                return this;
            }
            
            public a B(final String s) {
                ((GeneratedMessageLite.a)this).s();
                ((IndexField)super.b).i0(s);
                return this;
            }
            
            public a C(final Order order) {
                ((GeneratedMessageLite.a)this).s();
                ((IndexField)super.b).j0(order);
                return this;
            }
        }
    }
    
    public enum QueryScope implements t.a
    {
        private static final QueryScope[] $VALUES;
        
        COLLECTION(1), 
        COLLECTION_GROUP(2);
        
        public static final int COLLECTION_GROUP_VALUE = 2;
        public static final int COLLECTION_VALUE = 1;
        
        QUERY_SCOPE_UNSPECIFIED(0);
        
        public static final int QUERY_SCOPE_UNSPECIFIED_VALUE = 0;
        
        UNRECOGNIZED(-1);
        
        private static final t.b internalValueMap;
        private final int value;
        
        static {
            internalValueMap = new t.b() {};
        }
        
        private QueryScope(final int value) {
            this.value = value;
        }
        
        public static QueryScope forNumber(final int n) {
            if (n == 0) {
                return QueryScope.QUERY_SCOPE_UNSPECIFIED;
            }
            if (n == 1) {
                return QueryScope.COLLECTION;
            }
            if (n != 2) {
                return null;
            }
            return QueryScope.COLLECTION_GROUP;
        }
        
        public static t.b internalGetValueMap() {
            return QueryScope.internalValueMap;
        }
        
        public static t.c internalGetVerifier() {
            return b.a;
        }
        
        @Deprecated
        public static QueryScope valueOf(final int n) {
            return forNumber(n);
        }
        
        @Override
        public final int getNumber() {
            if (this != QueryScope.UNRECOGNIZED) {
                return this.value;
            }
            throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
        }
        
        public static final class b implements t.c
        {
            public static final t.c a;
            
            static {
                a = new b();
            }
            
            @Override
            public boolean a(final int n) {
                return QueryScope.forNumber(n) != null;
            }
        }
    }
    
    public enum State implements t.a
    {
        private static final State[] $VALUES;
        
        CREATING(1);
        
        public static final int CREATING_VALUE = 1;
        
        NEEDS_REPAIR(3);
        
        public static final int NEEDS_REPAIR_VALUE = 3;
        
        READY(2);
        
        public static final int READY_VALUE = 2;
        
        STATE_UNSPECIFIED(0);
        
        public static final int STATE_UNSPECIFIED_VALUE = 0;
        
        UNRECOGNIZED(-1);
        
        private static final t.b internalValueMap;
        private final int value;
        
        static {
            internalValueMap = new t.b() {};
        }
        
        private State(final int value) {
            this.value = value;
        }
        
        public static State forNumber(final int n) {
            if (n == 0) {
                return State.STATE_UNSPECIFIED;
            }
            if (n == 1) {
                return State.CREATING;
            }
            if (n == 2) {
                return State.READY;
            }
            if (n != 3) {
                return null;
            }
            return State.NEEDS_REPAIR;
        }
        
        public static t.b internalGetValueMap() {
            return State.internalValueMap;
        }
        
        public static t.c internalGetVerifier() {
            return b.a;
        }
        
        @Deprecated
        public static State valueOf(final int n) {
            return forNumber(n);
        }
        
        @Override
        public final int getNumber() {
            if (this != State.UNRECOGNIZED) {
                return this.value;
            }
            throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
        }
        
        public static final class b implements t.c
        {
            public static final t.c a;
            
            static {
                a = new b();
            }
            
            @Override
            public boolean a(final int n) {
                return State.forNumber(n) != null;
            }
        }
    }
    
    public static final class b extends GeneratedMessageLite.a implements xv0
    {
        public b() {
            super(Index.Z());
        }
        
        public b A(final IndexField.a a) {
            ((GeneratedMessageLite.a)this).s();
            ((Index)super.b).c0((IndexField)((GeneratedMessageLite.a)a).p());
            return this;
        }
        
        public b B(final QueryScope queryScope) {
            ((GeneratedMessageLite.a)this).s();
            ((Index)super.b).h0(queryScope);
            return this;
        }
    }
}

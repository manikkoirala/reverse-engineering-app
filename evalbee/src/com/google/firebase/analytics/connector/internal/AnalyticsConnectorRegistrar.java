// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.analytics.connector.internal;

import java.util.Arrays;
import java.util.List;
import android.content.Context;
import com.google.android.gms.common.annotation.KeepForSdk;
import androidx.annotation.Keep;
import com.google.firebase.components.ComponentRegistrar;

@Keep
@KeepForSdk
public class AnalyticsConnectorRegistrar implements ComponentRegistrar
{
    @Keep
    @KeepForSdk
    @Override
    public List<zi> getComponents() {
        return Arrays.asList(zi.e(g4.class).b(os.k(r10.class)).b(os.k(Context.class)).b(os.k(bs1.class)).f(ge2.a).e().d(), mj0.b("fire-analytics", "21.5.0"));
    }
}

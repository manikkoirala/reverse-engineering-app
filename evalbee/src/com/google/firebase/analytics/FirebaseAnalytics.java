// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.analytics;

import android.app.Activity;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import com.google.android.gms.tasks.Tasks;
import java.util.concurrent.TimeUnit;
import com.google.firebase.installations.a;
import com.google.android.gms.measurement.internal.zzjz;
import android.os.Bundle;
import androidx.annotation.Keep;
import android.content.Context;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.measurement.zzdf;

public final class FirebaseAnalytics
{
    public static volatile FirebaseAnalytics b;
    public final zzdf a;
    
    public FirebaseAnalytics(final zzdf a) {
        Preconditions.checkNotNull(a);
        this.a = a;
    }
    
    @Keep
    public static FirebaseAnalytics getInstance(final Context context) {
        if (FirebaseAnalytics.b == null) {
            synchronized (FirebaseAnalytics.class) {
                if (FirebaseAnalytics.b == null) {
                    FirebaseAnalytics.b = new FirebaseAnalytics(zzdf.zza(context));
                }
            }
        }
        return FirebaseAnalytics.b;
    }
    
    @Keep
    public static zzjz getScionFrontendApiImplementation(final Context context, final Bundle bundle) {
        final zzdf zza = zzdf.zza(context, (String)null, (String)null, (String)null, bundle);
        if (zza == null) {
            return null;
        }
        return (zzjz)new ie2(zza);
    }
    
    public final void a(final String s, final Bundle bundle) {
        this.a.zza(s, bundle);
    }
    
    @Keep
    public final String getFirebaseInstanceId() {
        try {
            return (String)Tasks.await(com.google.firebase.installations.a.p().getId(), 30000L, TimeUnit.MILLISECONDS);
        }
        catch (final InterruptedException cause) {
            throw new IllegalStateException(cause);
        }
        catch (final TimeoutException ex) {
            throw new IllegalThreadStateException("Firebase Installations getId Task has timed out.");
        }
        catch (final ExecutionException ex2) {
            throw new IllegalStateException(ex2.getCause());
        }
    }
    
    @Deprecated
    @Keep
    public final void setCurrentScreen(final Activity activity, final String s, final String s2) {
        this.a.zza(activity, s, s2);
    }
    
    public enum ConsentStatus
    {
        DENIED, 
        GRANTED;
        
        private static final ConsentStatus[] zza;
    }
    
    public enum ConsentType
    {
        AD_PERSONALIZATION, 
        AD_STORAGE, 
        AD_USER_DATA, 
        ANALYTICS_STORAGE;
        
        private static final ConsentType[] zza;
    }
}

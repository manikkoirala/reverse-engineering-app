// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.installations;

import java.util.Arrays;
import java.util.List;
import com.google.firebase.concurrent.FirebaseExecutors;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import androidx.annotation.Keep;
import com.google.firebase.components.ComponentRegistrar;

@Keep
public class FirebaseInstallationsRegistrar implements ComponentRegistrar
{
    private static final String LIBRARY_NAME = "fire-installations";
    
    @Override
    public List<zi> getComponents() {
        return Arrays.asList(zi.e(r20.class).h("fire-installations").b(os.k(r10.class)).b(os.i(wc0.class)).b(os.j(da1.a(za.class, ExecutorService.class))).b(os.j(da1.a(fc.class, Executor.class))).f(new s20()).d(), vc0.a(), mj0.b("fire-installations", "17.2.0"));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.installations.remote;

public abstract class TokenResult
{
    public static a a() {
        return new b.b().d(0L);
    }
    
    public abstract ResponseCode b();
    
    public abstract String c();
    
    public abstract long d();
    
    public enum ResponseCode
    {
        private static final ResponseCode[] $VALUES;
        
        AUTH_ERROR, 
        BAD_CONFIG, 
        OK;
    }
    
    public abstract static class a
    {
        public abstract TokenResult a();
        
        public abstract a b(final ResponseCode p0);
        
        public abstract a c(final String p0);
        
        public abstract a d(final long p0);
    }
}

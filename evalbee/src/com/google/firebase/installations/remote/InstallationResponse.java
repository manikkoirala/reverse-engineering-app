// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.installations.remote;

public abstract class InstallationResponse
{
    public static a a() {
        return (a)new com.google.firebase.installations.remote.a.b();
    }
    
    public abstract TokenResult b();
    
    public abstract String c();
    
    public abstract String d();
    
    public abstract ResponseCode e();
    
    public abstract String f();
    
    public enum ResponseCode
    {
        private static final ResponseCode[] $VALUES;
        
        BAD_CONFIG, 
        OK;
    }
    
    public abstract static class a
    {
        public abstract InstallationResponse a();
        
        public abstract a b(final TokenResult p0);
        
        public abstract a c(final String p0);
        
        public abstract a d(final String p0);
        
        public abstract a e(final ResponseCode p0);
        
        public abstract a f(final String p0);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.installations.remote;

public final class a extends InstallationResponse
{
    public final String a;
    public final String b;
    public final String c;
    public final TokenResult d;
    public final ResponseCode e;
    
    public a(final String a, final String b, final String c, final TokenResult d, final ResponseCode e) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
    }
    
    @Override
    public TokenResult b() {
        return this.d;
    }
    
    @Override
    public String c() {
        return this.b;
    }
    
    @Override
    public String d() {
        return this.c;
    }
    
    @Override
    public ResponseCode e() {
        return this.e;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof InstallationResponse) {
            final InstallationResponse installationResponse = (InstallationResponse)o;
            final String a = this.a;
            if (a == null) {
                if (installationResponse.f() != null) {
                    return false;
                }
            }
            else if (!a.equals(installationResponse.f())) {
                return false;
            }
            final String b2 = this.b;
            if (b2 == null) {
                if (installationResponse.c() != null) {
                    return false;
                }
            }
            else if (!b2.equals(installationResponse.c())) {
                return false;
            }
            final String c = this.c;
            if (c == null) {
                if (installationResponse.d() != null) {
                    return false;
                }
            }
            else if (!c.equals(installationResponse.d())) {
                return false;
            }
            final TokenResult d = this.d;
            if (d == null) {
                if (installationResponse.b() != null) {
                    return false;
                }
            }
            else if (!d.equals(installationResponse.b())) {
                return false;
            }
            final ResponseCode e = this.e;
            final ResponseCode e2 = installationResponse.e();
            if (e == null) {
                if (e2 == null) {
                    return b;
                }
            }
            else if (e.equals(e2)) {
                return b;
            }
            b = false;
            return b;
        }
        return false;
    }
    
    @Override
    public String f() {
        return this.a;
    }
    
    @Override
    public int hashCode() {
        final String a = this.a;
        int hashCode = 0;
        int hashCode2;
        if (a == null) {
            hashCode2 = 0;
        }
        else {
            hashCode2 = a.hashCode();
        }
        final String b = this.b;
        int hashCode3;
        if (b == null) {
            hashCode3 = 0;
        }
        else {
            hashCode3 = b.hashCode();
        }
        final String c = this.c;
        int hashCode4;
        if (c == null) {
            hashCode4 = 0;
        }
        else {
            hashCode4 = c.hashCode();
        }
        final TokenResult d = this.d;
        int hashCode5;
        if (d == null) {
            hashCode5 = 0;
        }
        else {
            hashCode5 = d.hashCode();
        }
        final ResponseCode e = this.e;
        if (e != null) {
            hashCode = e.hashCode();
        }
        return ((((hashCode2 ^ 0xF4243) * 1000003 ^ hashCode3) * 1000003 ^ hashCode4) * 1000003 ^ hashCode5) * 1000003 ^ hashCode;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("InstallationResponse{uri=");
        sb.append(this.a);
        sb.append(", fid=");
        sb.append(this.b);
        sb.append(", refreshToken=");
        sb.append(this.c);
        sb.append(", authToken=");
        sb.append(this.d);
        sb.append(", responseCode=");
        sb.append(this.e);
        sb.append("}");
        return sb.toString();
    }
    
    public static final class b extends InstallationResponse.a
    {
        public String a;
        public String b;
        public String c;
        public TokenResult d;
        public ResponseCode e;
        
        @Override
        public InstallationResponse a() {
            return new a(this.a, this.b, this.c, this.d, this.e, null);
        }
        
        @Override
        public InstallationResponse.a b(final TokenResult d) {
            this.d = d;
            return this;
        }
        
        @Override
        public InstallationResponse.a c(final String b) {
            this.b = b;
            return this;
        }
        
        @Override
        public InstallationResponse.a d(final String c) {
            this.c = c;
            return this;
        }
        
        @Override
        public InstallationResponse.a e(final ResponseCode e) {
            this.e = e;
            return this;
        }
        
        @Override
        public InstallationResponse.a f(final String a) {
            this.a = a;
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.installations.remote;

import java.io.InputStream;
import java.io.Reader;
import android.util.JsonReader;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutionException;
import com.google.android.gms.tasks.Tasks;
import java.net.MalformedURLException;
import android.content.pm.PackageManager$NameNotFoundException;
import com.google.android.gms.common.util.Hex;
import com.google.android.gms.common.util.AndroidUtilsLight;
import java.net.URL;
import java.io.IOException;
import com.google.firebase.installations.FirebaseInstallationsException;
import android.net.TrafficStats;
import java.net.URLConnection;
import com.google.android.gms.common.internal.Preconditions;
import java.net.HttpURLConnection;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;
import android.text.TextUtils;
import android.content.Context;
import java.nio.charset.Charset;
import java.util.regex.Pattern;

public class c
{
    public static final Pattern d;
    public static final Charset e;
    public final Context a;
    public final r91 b;
    public final de1 c;
    
    static {
        d = Pattern.compile("[0-9]+s");
        e = Charset.forName("UTF-8");
    }
    
    public c(final Context a, final r91 b) {
        this.a = a;
        this.b = b;
        this.c = new de1();
    }
    
    public static String a(String string, final String s, final String s2) {
        if (TextUtils.isEmpty((CharSequence)string)) {
            string = "";
        }
        else {
            final StringBuilder sb = new StringBuilder();
            sb.append(", ");
            sb.append(string);
            string = sb.toString();
        }
        return String.format("Firebase options used while communicating with Firebase server APIs: %s, %s%s", s, s2, string);
    }
    
    public static JSONObject b(final String s, final String s2) {
        try {
            final JSONObject jsonObject = new JSONObject();
            jsonObject.put("fid", (Object)s);
            jsonObject.put("appId", (Object)s2);
            jsonObject.put("authVersion", (Object)"FIS_v2");
            jsonObject.put("sdkVersion", (Object)"a:17.2.0");
            return jsonObject;
        }
        catch (final JSONException cause) {
            throw new IllegalStateException((Throwable)cause);
        }
    }
    
    public static JSONObject c() {
        try {
            final JSONObject jsonObject = new JSONObject();
            jsonObject.put("sdkVersion", (Object)"a:17.2.0");
            final JSONObject jsonObject2 = new JSONObject();
            jsonObject2.put("installation", (Object)jsonObject);
            return jsonObject2;
        }
        catch (final JSONException cause) {
            throw new IllegalStateException((Throwable)cause);
        }
    }
    
    public static byte[] h(final JSONObject jsonObject) {
        return jsonObject.toString().getBytes("UTF-8");
    }
    
    public static boolean i(final int n) {
        return n >= 200 && n < 300;
    }
    
    public static void j() {
        Log.e("Firebase-Installations", "Firebase Installations can not communicate with Firebase server APIs due to invalid configuration. Please update your Firebase initialization process and set valid Firebase options (API key, Project ID, Application ID) when initializing Firebase.");
    }
    
    public static void k(final HttpURLConnection httpURLConnection, final String s, final String s2, final String s3) {
        final String o = o(httpURLConnection);
        if (!TextUtils.isEmpty((CharSequence)o)) {
            Log.w("Firebase-Installations", o);
            Log.w("Firebase-Installations", a(s, s2, s3));
        }
    }
    
    public static long m(final String input) {
        Preconditions.checkArgument(c.d.matcher(input).matches(), (Object)"Invalid Expiration Timestamp.");
        long long1;
        if (input != null && input.length() != 0) {
            long1 = Long.parseLong(input.substring(0, input.length() - 1));
        }
        else {
            long1 = 0L;
        }
        return long1;
    }
    
    public static String o(final HttpURLConnection p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokevirtual   java/net/HttpURLConnection.getErrorStream:()Ljava/io/InputStream;
        //     4: astore_1       
        //     5: aload_1        
        //     6: ifnonnull       11
        //     9: aconst_null    
        //    10: areturn        
        //    11: new             Ljava/io/BufferedReader;
        //    14: dup            
        //    15: new             Ljava/io/InputStreamReader;
        //    18: dup            
        //    19: aload_1        
        //    20: getstatic       com/google/firebase/installations/remote/c.e:Ljava/nio/charset/Charset;
        //    23: invokespecial   java/io/InputStreamReader.<init>:(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V
        //    26: invokespecial   java/io/BufferedReader.<init>:(Ljava/io/Reader;)V
        //    29: astore_1       
        //    30: new             Ljava/lang/StringBuilder;
        //    33: astore_2       
        //    34: aload_2        
        //    35: invokespecial   java/lang/StringBuilder.<init>:()V
        //    38: aload_1        
        //    39: invokevirtual   java/io/BufferedReader.readLine:()Ljava/lang/String;
        //    42: astore_3       
        //    43: aload_3        
        //    44: ifnull          63
        //    47: aload_2        
        //    48: aload_3        
        //    49: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    52: pop            
        //    53: aload_2        
        //    54: bipush          10
        //    56: invokevirtual   java/lang/StringBuilder.append:(C)Ljava/lang/StringBuilder;
        //    59: pop            
        //    60: goto            38
        //    63: ldc             "Error when communicating with the Firebase Installations server API. HTTP response: [%d %s: %s]"
        //    65: iconst_3       
        //    66: anewarray       Ljava/lang/Object;
        //    69: dup            
        //    70: iconst_0       
        //    71: aload_0        
        //    72: invokevirtual   java/net/HttpURLConnection.getResponseCode:()I
        //    75: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //    78: aastore        
        //    79: dup            
        //    80: iconst_1       
        //    81: aload_0        
        //    82: invokevirtual   java/net/HttpURLConnection.getResponseMessage:()Ljava/lang/String;
        //    85: aastore        
        //    86: dup            
        //    87: iconst_2       
        //    88: aload_2        
        //    89: aastore        
        //    90: invokestatic    java/lang/String.format:(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
        //    93: astore_0       
        //    94: aload_1        
        //    95: invokevirtual   java/io/BufferedReader.close:()V
        //    98: aload_0        
        //    99: areturn        
        //   100: astore_0       
        //   101: aload_1        
        //   102: invokevirtual   java/io/BufferedReader.close:()V
        //   105: aload_0        
        //   106: athrow         
        //   107: astore_0       
        //   108: aload_1        
        //   109: invokevirtual   java/io/BufferedReader.close:()V
        //   112: aconst_null    
        //   113: areturn        
        //   114: astore_1       
        //   115: goto            98
        //   118: astore_1       
        //   119: goto            105
        //   122: astore_0       
        //   123: goto            112
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  30     38     107    114    Ljava/io/IOException;
        //  30     38     100    107    Any
        //  38     43     107    114    Ljava/io/IOException;
        //  38     43     100    107    Any
        //  47     60     107    114    Ljava/io/IOException;
        //  47     60     100    107    Any
        //  63     94     107    114    Ljava/io/IOException;
        //  63     94     100    107    Any
        //  94     98     114    118    Ljava/io/IOException;
        //  101    105    118    122    Ljava/io/IOException;
        //  108    112    122    126    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 74 out of bounds for length 74
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:248)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:372)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:459)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static void s(final URLConnection p0, final byte[] p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokevirtual   java/net/URLConnection.getOutputStream:()Ljava/io/OutputStream;
        //     4: astore_0       
        //     5: aload_0        
        //     6: ifnull          43
        //     9: new             Ljava/util/zip/GZIPOutputStream;
        //    12: dup            
        //    13: aload_0        
        //    14: invokespecial   java/util/zip/GZIPOutputStream.<init>:(Ljava/io/OutputStream;)V
        //    17: astore_2       
        //    18: aload_2        
        //    19: aload_1        
        //    20: invokevirtual   java/io/OutputStream.write:([B)V
        //    23: aload_2        
        //    24: invokevirtual   java/io/OutputStream.close:()V
        //    27: aload_0        
        //    28: invokevirtual   java/io/OutputStream.close:()V
        //    31: return         
        //    32: astore_1       
        //    33: aload_2        
        //    34: invokevirtual   java/io/OutputStream.close:()V
        //    37: aload_0        
        //    38: invokevirtual   java/io/OutputStream.close:()V
        //    41: aload_1        
        //    42: athrow         
        //    43: new             Ljava/io/IOException;
        //    46: dup            
        //    47: ldc             "Cannot send request to FIS servers. No OutputStream available."
        //    49: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
        //    52: athrow         
        //    53: astore_0       
        //    54: goto            31
        //    57: astore_0       
        //    58: goto            41
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  18     23     32     43     Any
        //  23     31     53     57     Ljava/io/IOException;
        //  33     41     57     61     Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 34 out of bounds for length 34
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:248)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:372)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:459)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public InstallationResponse d(final String s, final String s2, final String s3, final String s4, final String value) {
        if (!this.c.b()) {
            goto Label_0252;
        }
        final URL g = this.g(String.format("projects/%s/installations", s3));
        if (0 > 1) {
            goto Label_0238;
        }
        TrafficStats.setThreadStatsTag(32769);
        final HttpURLConnection l = this.l(g, s);
        try {
            l.setRequestMethod("POST");
            l.setDoOutput(true);
            if (value != null) {
                l.addRequestProperty("x-goog-fis-android-iid-migration-auth", value);
            }
            this.q(l, s2, s4);
            final int responseCode = l.getResponseCode();
            this.c.f(responseCode);
            InstallationResponse installationResponse;
            if (i(responseCode)) {
                installationResponse = this.n(l);
            }
            else {
                k(l, s4, s, s3);
                if (responseCode == 429) {
                    throw new FirebaseInstallationsException("Firebase servers have received too many requests from this client in a short period of time. Please try again later.", FirebaseInstallationsException.Status.TOO_MANY_REQUESTS);
                }
                if (responseCode >= 500 && responseCode < 600) {
                    goto Label_0224;
                }
                j();
                installationResponse = InstallationResponse.a().e(InstallationResponse.ResponseCode.BAD_CONFIG).a();
            }
            return installationResponse;
        }
        catch (final AssertionError | IOException ex) {
            goto Label_0224;
        }
        finally {
            l.disconnect();
            TrafficStats.clearThreadStatsTag();
        }
    }
    
    public TokenResult e(final String s, final String s2, final String s3, final String str) {
        if (!this.c.b()) {
            goto Label_0301;
        }
        final URL g = this.g(String.format("projects/%s/installations/%s/authTokens:generate", s3, s2));
        if (0 > 1) {
            goto Label_0287;
        }
        TrafficStats.setThreadStatsTag(32771);
        final HttpURLConnection l = this.l(g, s);
        try {
            l.setRequestMethod("POST");
            final StringBuilder sb = new StringBuilder();
            sb.append("FIS_v2 ");
            sb.append(str);
            l.addRequestProperty("Authorization", sb.toString());
            l.setDoOutput(true);
            this.r(l);
            final int responseCode = l.getResponseCode();
            this.c.f(responseCode);
            TokenResult tokenResult;
            if (i(responseCode)) {
                tokenResult = this.p(l);
            }
            else {
                k(l, null, s, s3);
                TokenResult.a a;
                if (responseCode != 401 && responseCode != 404) {
                    if (responseCode == 429) {
                        throw new FirebaseInstallationsException("Firebase servers have received too many requests from this client in a short period of time. Please try again later.", FirebaseInstallationsException.Status.TOO_MANY_REQUESTS);
                    }
                    if (responseCode >= 500 && responseCode < 600) {
                        goto Label_0273;
                    }
                    j();
                    a = TokenResult.a().b(TokenResult.ResponseCode.BAD_CONFIG);
                }
                else {
                    a = TokenResult.a().b(TokenResult.ResponseCode.AUTH_ERROR);
                }
                tokenResult = a.a();
            }
            return tokenResult;
        }
        catch (final AssertionError | IOException ex) {
            goto Label_0273;
        }
        finally {
            l.disconnect();
            TrafficStats.clearThreadStatsTag();
        }
    }
    
    public final String f() {
        try {
            final Context a = this.a;
            final byte[] packageCertificateHashBytes = AndroidUtilsLight.getPackageCertificateHashBytes(a, a.getPackageName());
            if (packageCertificateHashBytes == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Could not get fingerprint hash for package: ");
                sb.append(this.a.getPackageName());
                Log.e("ContentValues", sb.toString());
                return null;
            }
            return Hex.bytesToStringUppercase(packageCertificateHashBytes, false);
        }
        catch (final PackageManager$NameNotFoundException ex) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("No such package: ");
            sb2.append(this.a.getPackageName());
            Log.e("ContentValues", sb2.toString(), (Throwable)ex);
            return null;
        }
    }
    
    public final URL g(final String s) {
        try {
            return new URL(String.format("https://%s/%s/%s", "firebaseinstallations.googleapis.com", "v1", s));
        }
        catch (final MalformedURLException ex) {
            throw new FirebaseInstallationsException(ex.getMessage(), FirebaseInstallationsException.Status.UNAVAILABLE);
        }
    }
    
    public final HttpURLConnection l(URL ex, final String value) {
        try {
            final HttpURLConnection httpURLConnection = (HttpURLConnection)((URL)ex).openConnection();
            httpURLConnection.setConnectTimeout(10000);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setReadTimeout(10000);
            httpURLConnection.addRequestProperty("Content-Type", "application/json");
            httpURLConnection.addRequestProperty("Accept", "application/json");
            httpURLConnection.addRequestProperty("Content-Encoding", "gzip");
            httpURLConnection.addRequestProperty("Cache-Control", "no-cache");
            httpURLConnection.addRequestProperty("X-Android-Package", this.a.getPackageName());
            ex = (InterruptedException)this.b.get();
            Label_0142: {
                if (ex != null) {
                    try {
                        httpURLConnection.addRequestProperty("x-firebase-client", (String)Tasks.await(((wc0)ex).b()));
                        break Label_0142;
                    }
                    catch (final InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                    catch (final ExecutionException ex2) {}
                    Log.w("ContentValues", "Failed to get heartbeats header", (Throwable)ex);
                }
            }
            httpURLConnection.addRequestProperty("X-Android-Cert", this.f());
            httpURLConnection.addRequestProperty("x-goog-api-key", value);
            return httpURLConnection;
        }
        catch (final IOException ex3) {
            throw new FirebaseInstallationsException("Firebase Installations Service is unavailable. Please try again later.", FirebaseInstallationsException.Status.UNAVAILABLE);
        }
    }
    
    public final InstallationResponse n(final HttpURLConnection httpURLConnection) {
        final InputStream inputStream = httpURLConnection.getInputStream();
        final JsonReader jsonReader = new JsonReader((Reader)new InputStreamReader(inputStream, com.google.firebase.installations.remote.c.e));
        final TokenResult.a a = TokenResult.a();
        final InstallationResponse.a a2 = InstallationResponse.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            final String nextName = jsonReader.nextName();
            if (nextName.equals("name")) {
                a2.f(jsonReader.nextString());
            }
            else if (nextName.equals("fid")) {
                a2.c(jsonReader.nextString());
            }
            else if (nextName.equals("refreshToken")) {
                a2.d(jsonReader.nextString());
            }
            else if (nextName.equals("authToken")) {
                jsonReader.beginObject();
                while (jsonReader.hasNext()) {
                    final String nextName2 = jsonReader.nextName();
                    if (nextName2.equals("token")) {
                        a.c(jsonReader.nextString());
                    }
                    else if (nextName2.equals("expiresIn")) {
                        a.d(m(jsonReader.nextString()));
                    }
                    else {
                        jsonReader.skipValue();
                    }
                }
                a2.b(a.a());
                jsonReader.endObject();
            }
            else {
                jsonReader.skipValue();
            }
        }
        jsonReader.endObject();
        jsonReader.close();
        inputStream.close();
        return a2.e(InstallationResponse.ResponseCode.OK).a();
    }
    
    public final TokenResult p(final HttpURLConnection httpURLConnection) {
        final InputStream inputStream = httpURLConnection.getInputStream();
        final JsonReader jsonReader = new JsonReader((Reader)new InputStreamReader(inputStream, com.google.firebase.installations.remote.c.e));
        final TokenResult.a a = TokenResult.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            final String nextName = jsonReader.nextName();
            if (nextName.equals("token")) {
                a.c(jsonReader.nextString());
            }
            else if (nextName.equals("expiresIn")) {
                a.d(m(jsonReader.nextString()));
            }
            else {
                jsonReader.skipValue();
            }
        }
        jsonReader.endObject();
        jsonReader.close();
        inputStream.close();
        return a.b(TokenResult.ResponseCode.OK).a();
    }
    
    public final void q(final HttpURLConnection httpURLConnection, final String s, final String s2) {
        s(httpURLConnection, h(b(s, s2)));
    }
    
    public final void r(final HttpURLConnection httpURLConnection) {
        s(httpURLConnection, h(c()));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.installations;

import com.google.firebase.FirebaseException;

public class FirebaseInstallationsException extends FirebaseException
{
    private final Status status;
    
    public FirebaseInstallationsException(final Status status) {
        this.status = status;
    }
    
    public FirebaseInstallationsException(final String s, final Status status) {
        super(s);
        this.status = status;
    }
    
    public FirebaseInstallationsException(final String s, final Status status, final Throwable t) {
        super(s, t);
        this.status = status;
    }
    
    public Status getStatus() {
        return this.status;
    }
    
    public enum Status
    {
        private static final Status[] $VALUES;
        
        BAD_CONFIG, 
        TOO_MANY_REQUESTS, 
        UNAVAILABLE;
    }
}

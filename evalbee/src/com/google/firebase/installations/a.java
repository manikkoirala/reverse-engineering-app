// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.installations;

import com.google.firebase.installations.remote.TokenResult;
import java.io.IOException;
import com.google.android.gms.tasks.Tasks;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Task;
import java.util.Iterator;
import com.google.firebase.installations.remote.InstallationResponse;
import android.text.TextUtils;
import com.google.firebase.installations.local.b;
import com.google.android.gms.common.internal.Preconditions;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import com.google.firebase.installations.local.PersistedInstallation;
import com.google.firebase.installations.remote.c;
import java.util.concurrent.ThreadFactory;

public class a implements r20
{
    public static final Object m;
    public static final ThreadFactory n;
    public final r10 a;
    public final c b;
    public final PersistedInstallation c;
    public final u22 d;
    public final yi0 e;
    public final dc1 f;
    public final Object g;
    public final ExecutorService h;
    public final Executor i;
    public String j;
    public Set k;
    public final List l;
    
    static {
        m = new Object();
        n = new ThreadFactory() {
            public final AtomicInteger a = new AtomicInteger(1);
            
            @Override
            public Thread newThread(final Runnable target) {
                return new Thread(target, String.format("firebase-installations-executor-%d", this.a.getAndIncrement()));
            }
        };
    }
    
    public a(final ExecutorService h, final Executor i, final r10 a, final c b, final PersistedInstallation c, final u22 d, final yi0 e, final dc1 f) {
        this.g = new Object();
        this.k = new HashSet();
        this.l = new ArrayList();
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.h = h;
        this.i = i;
    }
    
    public a(final r10 r10, final r91 r11, final ExecutorService executorService, final Executor executor) {
        this(executorService, executor, r10, new c(r10.l(), r11), new PersistedInstallation(r10), u22.c(), new yi0(new o20(r10)), new dc1());
    }
    
    public static a p() {
        return q(r10.m());
    }
    
    public static a q(final r10 r10) {
        Preconditions.checkArgument(r10 != null, (Object)"Null is not a valid value of FirebaseApp.");
        return (a)r10.j(r20.class);
    }
    
    public final String A(final b b) {
        if ((!this.a.o().equals("CHIME_ANDROID_SDK") && !this.a.w()) || !b.m()) {
            return this.f.a();
        }
        String s;
        if (TextUtils.isEmpty((CharSequence)(s = this.o().f()))) {
            s = this.f.a();
        }
        return s;
    }
    
    public final b B(final b b) {
        String i;
        if (b.d() != null && b.d().length() == 11) {
            i = this.o().i();
        }
        else {
            i = null;
        }
        final InstallationResponse d = this.b.d(this.l(), b.d(), this.t(), this.m(), i);
        final int n = a$b.a[d.e().ordinal()];
        if (n == 1) {
            return b.s(d.c(), d.d(), this.d.b(), d.b().c(), d.b().d());
        }
        if (n == 2) {
            return b.q("BAD CONFIG");
        }
        throw new FirebaseInstallationsException("Firebase Installations Service is unavailable. Please try again later.", FirebaseInstallationsException.Status.UNAVAILABLE);
    }
    
    public final void C(final Exception ex) {
        synchronized (this.g) {
            final Iterator iterator = this.l.iterator();
            while (iterator.hasNext()) {
                if (((yp1)iterator.next()).a(ex)) {
                    iterator.remove();
                }
            }
        }
    }
    
    public final void D(final b b) {
        synchronized (this.g) {
            final Iterator iterator = this.l.iterator();
            while (iterator.hasNext()) {
                if (((yp1)iterator.next()).b(b)) {
                    iterator.remove();
                }
            }
        }
    }
    
    public final void E(final String j) {
        synchronized (this) {
            this.j = j;
        }
    }
    
    public final void F(final b b, final b b2) {
        synchronized (this) {
            if (this.k.size() != 0 && !TextUtils.equals((CharSequence)b.d(), (CharSequence)b2.d())) {
                final Iterator iterator = this.k.iterator();
                if (iterator.hasNext()) {
                    zu0.a(iterator.next());
                    b2.d();
                    throw null;
                }
            }
        }
    }
    
    @Override
    public Task a(final boolean b) {
        this.z();
        final Task f = this.f();
        this.h.execute(new q20(this, b));
        return f;
    }
    
    public final Task f() {
        final TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        this.h(new la0(this.d, taskCompletionSource));
        return taskCompletionSource.getTask();
    }
    
    public final Task g() {
        final TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        this.h(new na0(taskCompletionSource));
        return taskCompletionSource.getTask();
    }
    
    @Override
    public Task getId() {
        this.z();
        final String n = this.n();
        if (n != null) {
            return Tasks.forResult((Object)n);
        }
        final Task g = this.g();
        this.h.execute(new n20(this));
        return g;
    }
    
    public final void h(final yp1 yp1) {
        synchronized (this.g) {
            this.l.add(yp1);
        }
    }
    
    public final void i(final boolean b) {
        final b r = this.r();
        try {
            b b2;
            if (!r.i() && !r.l()) {
                if (!b && !this.d.f(r)) {
                    return;
                }
                b2 = this.k(r);
            }
            else {
                b2 = this.B(r);
            }
            this.u(b2);
            this.F(r, b2);
            if (b2.k()) {
                this.E(b2.d());
            }
            Exception ex;
            if (b2.i()) {
                ex = new FirebaseInstallationsException(FirebaseInstallationsException.Status.BAD_CONFIG);
            }
            else {
                if (!b2.j()) {
                    this.D(b2);
                    return;
                }
                ex = new IOException("Installation ID could not be validated with the Firebase servers (maybe it was deleted). Firebase Installations will need to create a new Installation ID and auth token. Please retry your last request.");
            }
            this.C(ex);
        }
        catch (final FirebaseInstallationsException ex2) {
            this.C(ex2);
        }
    }
    
    public final void j(final boolean b) {
        b b2 = this.s();
        if (b) {
            b2 = b2.p();
        }
        this.D(b2);
        this.i.execute(new p20(this, b));
    }
    
    public final b k(final b b) {
        final TokenResult e = this.b.e(this.l(), b.d(), this.t(), b.f());
        final int n = a$b.b[e.b().ordinal()];
        if (n == 1) {
            return b.o(e.c(), e.d(), this.d.b());
        }
        if (n == 2) {
            return b.q("BAD CONFIG");
        }
        if (n == 3) {
            this.E(null);
            return b.r();
        }
        throw new FirebaseInstallationsException("Firebase Installations Service is unavailable. Please try again later.", FirebaseInstallationsException.Status.UNAVAILABLE);
    }
    
    public String l() {
        return this.a.p().b();
    }
    
    public String m() {
        return this.a.p().c();
    }
    
    public final String n() {
        synchronized (this) {
            return this.j;
        }
    }
    
    public final ge0 o() {
        return (ge0)this.e.get();
    }
    
    public final b r() {
        synchronized (com.google.firebase.installations.a.m) {
            final fo a = fo.a(this.a.l(), "generatefid.lock");
            try {
                return this.c.d();
            }
            finally {
                if (a != null) {
                    a.b();
                }
            }
        }
    }
    
    public final b s() {
        synchronized (com.google.firebase.installations.a.m) {
            final fo a = fo.a(this.a.l(), "generatefid.lock");
            try {
                b b2;
                final b b = b2 = this.c.d();
                if (b.j()) {
                    b2 = this.c.b(b.t(this.A(b)));
                }
                return b2;
            }
            finally {
                if (a != null) {
                    a.b();
                }
            }
        }
    }
    
    public String t() {
        return this.a.p().e();
    }
    
    public final void u(final b b) {
        synchronized (com.google.firebase.installations.a.m) {
            final fo a = fo.a(this.a.l(), "generatefid.lock");
            try {
                this.c.b(b);
            }
            finally {
                if (a != null) {
                    a.b();
                }
            }
        }
    }
    
    public final void z() {
        Preconditions.checkNotEmpty(this.m(), "Please set your Application ID. A valid Firebase App ID is required to communicate with Firebase server APIs: It identifies your application with Firebase.Please refer to https://firebase.google.com/support/privacy/init-options.");
        Preconditions.checkNotEmpty(this.t(), "Please set your Project ID. A valid Firebase Project ID is required to communicate with Firebase server APIs: It identifies your application with Firebase.Please refer to https://firebase.google.com/support/privacy/init-options.");
        Preconditions.checkNotEmpty(this.l(), "Please set a valid API key. A Firebase API key is required to communicate with Firebase server APIs: It authenticates your project with Google.Please refer to https://firebase.google.com/support/privacy/init-options.");
        Preconditions.checkArgument(u22.h(this.m()), (Object)"Please set your Application ID. A valid Firebase App ID is required to communicate with Firebase server APIs: It identifies your application with Firebase.Please refer to https://firebase.google.com/support/privacy/init-options.");
        Preconditions.checkArgument(u22.g(this.l()), (Object)"Please set a valid API key. A Firebase API key is required to communicate with Firebase server APIs: It authenticates your project with Google.Please refer to https://firebase.google.com/support/privacy/init-options.");
    }
}

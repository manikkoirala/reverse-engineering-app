// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.installations.local;

public final class a extends com.google.firebase.installations.local.b
{
    public final String b;
    public final PersistedInstallation.RegistrationStatus c;
    public final String d;
    public final String e;
    public final long f;
    public final long g;
    public final String h;
    
    public a(final String b, final PersistedInstallation.RegistrationStatus c, final String d, final String e, final long f, final long g, final String h) {
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = h;
    }
    
    @Override
    public String b() {
        return this.d;
    }
    
    @Override
    public long c() {
        return this.f;
    }
    
    @Override
    public String d() {
        return this.b;
    }
    
    @Override
    public String e() {
        return this.h;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof com.google.firebase.installations.local.b) {
            final com.google.firebase.installations.local.b b2 = (com.google.firebase.installations.local.b)o;
            final String b3 = this.b;
            if (b3 == null) {
                if (b2.d() != null) {
                    return false;
                }
            }
            else if (!b3.equals(b2.d())) {
                return false;
            }
            if (this.c.equals(b2.g())) {
                final String d = this.d;
                if (d == null) {
                    if (b2.b() != null) {
                        return false;
                    }
                }
                else if (!d.equals(b2.b())) {
                    return false;
                }
                final String e = this.e;
                if (e == null) {
                    if (b2.f() != null) {
                        return false;
                    }
                }
                else if (!e.equals(b2.f())) {
                    return false;
                }
                if (this.f == b2.c() && this.g == b2.h()) {
                    final String h = this.h;
                    final String e2 = b2.e();
                    if (h == null) {
                        if (e2 == null) {
                            return b;
                        }
                    }
                    else if (h.equals(e2)) {
                        return b;
                    }
                }
            }
            b = false;
            return b;
        }
        return false;
    }
    
    @Override
    public String f() {
        return this.e;
    }
    
    @Override
    public PersistedInstallation.RegistrationStatus g() {
        return this.c;
    }
    
    @Override
    public long h() {
        return this.g;
    }
    
    @Override
    public int hashCode() {
        final String b = this.b;
        int hashCode = 0;
        int hashCode2;
        if (b == null) {
            hashCode2 = 0;
        }
        else {
            hashCode2 = b.hashCode();
        }
        final int hashCode3 = this.c.hashCode();
        final String d = this.d;
        int hashCode4;
        if (d == null) {
            hashCode4 = 0;
        }
        else {
            hashCode4 = d.hashCode();
        }
        final String e = this.e;
        int hashCode5;
        if (e == null) {
            hashCode5 = 0;
        }
        else {
            hashCode5 = e.hashCode();
        }
        final long f = this.f;
        final int n = (int)(f ^ f >>> 32);
        final long g = this.g;
        final int n2 = (int)(g ^ g >>> 32);
        final String h = this.h;
        if (h != null) {
            hashCode = h.hashCode();
        }
        return ((((((hashCode2 ^ 0xF4243) * 1000003 ^ hashCode3) * 1000003 ^ hashCode4) * 1000003 ^ hashCode5) * 1000003 ^ n) * 1000003 ^ n2) * 1000003 ^ hashCode;
    }
    
    @Override
    public com.google.firebase.installations.local.b.a n() {
        return new b(this, null);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("PersistedInstallationEntry{firebaseInstallationId=");
        sb.append(this.b);
        sb.append(", registrationStatus=");
        sb.append(this.c);
        sb.append(", authToken=");
        sb.append(this.d);
        sb.append(", refreshToken=");
        sb.append(this.e);
        sb.append(", expiresInSecs=");
        sb.append(this.f);
        sb.append(", tokenCreationEpochInSecs=");
        sb.append(this.g);
        sb.append(", fisError=");
        sb.append(this.h);
        sb.append("}");
        return sb.toString();
    }
    
    public static final class b extends com.google.firebase.installations.local.b.a
    {
        public String a;
        public PersistedInstallation.RegistrationStatus b;
        public String c;
        public String d;
        public Long e;
        public Long f;
        public String g;
        
        public b() {
        }
        
        public b(final com.google.firebase.installations.local.b b) {
            this.a = b.d();
            this.b = b.g();
            this.c = b.b();
            this.d = b.f();
            this.e = b.c();
            this.f = b.h();
            this.g = b.e();
        }
        
        @Override
        public com.google.firebase.installations.local.b a() {
            final PersistedInstallation.RegistrationStatus b = this.b;
            String string = "";
            if (b == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("");
                sb.append(" registrationStatus");
                string = sb.toString();
            }
            String string2 = string;
            if (this.e == null) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(string);
                sb2.append(" expiresInSecs");
                string2 = sb2.toString();
            }
            String string3 = string2;
            if (this.f == null) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(string2);
                sb3.append(" tokenCreationEpochInSecs");
                string3 = sb3.toString();
            }
            if (string3.isEmpty()) {
                return new a(this.a, this.b, this.c, this.d, this.e, this.f, this.g, null);
            }
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("Missing required properties:");
            sb4.append(string3);
            throw new IllegalStateException(sb4.toString());
        }
        
        @Override
        public com.google.firebase.installations.local.b.a b(final String c) {
            this.c = c;
            return this;
        }
        
        @Override
        public com.google.firebase.installations.local.b.a c(final long l) {
            this.e = l;
            return this;
        }
        
        @Override
        public com.google.firebase.installations.local.b.a d(final String a) {
            this.a = a;
            return this;
        }
        
        @Override
        public com.google.firebase.installations.local.b.a e(final String g) {
            this.g = g;
            return this;
        }
        
        @Override
        public com.google.firebase.installations.local.b.a f(final String d) {
            this.d = d;
            return this;
        }
        
        @Override
        public com.google.firebase.installations.local.b.a g(final PersistedInstallation.RegistrationStatus b) {
            if (b != null) {
                this.b = b;
                return this;
            }
            throw new NullPointerException("Null registrationStatus");
        }
        
        @Override
        public com.google.firebase.installations.local.b.a h(final long l) {
            this.f = l;
            return this;
        }
    }
}

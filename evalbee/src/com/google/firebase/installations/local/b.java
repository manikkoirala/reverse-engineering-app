// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.installations.local;

public abstract class b
{
    public static b a;
    
    static {
        b.a = a().a();
    }
    
    public static a a() {
        return new com.google.firebase.installations.local.a.b().h(0L).g(PersistedInstallation.RegistrationStatus.ATTEMPT_MIGRATION).c(0L);
    }
    
    public abstract String b();
    
    public abstract long c();
    
    public abstract String d();
    
    public abstract String e();
    
    public abstract String f();
    
    public abstract PersistedInstallation.RegistrationStatus g();
    
    public abstract long h();
    
    public boolean i() {
        return this.g() == PersistedInstallation.RegistrationStatus.REGISTER_ERROR;
    }
    
    public boolean j() {
        return this.g() == PersistedInstallation.RegistrationStatus.NOT_GENERATED || this.g() == PersistedInstallation.RegistrationStatus.ATTEMPT_MIGRATION;
    }
    
    public boolean k() {
        return this.g() == PersistedInstallation.RegistrationStatus.REGISTERED;
    }
    
    public boolean l() {
        return this.g() == PersistedInstallation.RegistrationStatus.UNREGISTERED;
    }
    
    public boolean m() {
        return this.g() == PersistedInstallation.RegistrationStatus.ATTEMPT_MIGRATION;
    }
    
    public abstract a n();
    
    public b o(final String s, final long n, final long n2) {
        return this.n().b(s).c(n).h(n2).a();
    }
    
    public b p() {
        return this.n().b(null).a();
    }
    
    public b q(final String s) {
        return this.n().e(s).g(PersistedInstallation.RegistrationStatus.REGISTER_ERROR).a();
    }
    
    public b r() {
        return this.n().g(PersistedInstallation.RegistrationStatus.NOT_GENERATED).a();
    }
    
    public b s(final String s, final String s2, final long n, final String s3, final long n2) {
        return this.n().d(s).g(PersistedInstallation.RegistrationStatus.REGISTERED).b(s3).f(s2).c(n2).h(n).a();
    }
    
    public b t(final String s) {
        return this.n().d(s).g(PersistedInstallation.RegistrationStatus.UNREGISTERED).a();
    }
    
    public abstract static class a
    {
        public abstract b a();
        
        public abstract a b(final String p0);
        
        public abstract a c(final long p0);
        
        public abstract a d(final String p0);
        
        public abstract a e(final String p0);
        
        public abstract a f(final String p0);
        
        public abstract a g(final PersistedInstallation.RegistrationStatus p0);
        
        public abstract a h(final long p0);
    }
}

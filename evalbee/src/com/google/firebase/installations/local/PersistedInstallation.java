// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.installations.local;

import java.io.FileInputStream;
import java.io.ByteArrayOutputStream;
import org.json.JSONException;
import java.io.IOException;
import java.io.FileOutputStream;
import org.json.JSONObject;
import java.io.File;

public class PersistedInstallation
{
    public File a;
    public final r10 b;
    
    public PersistedInstallation(final r10 b) {
        this.b = b;
    }
    
    public final File a() {
        if (this.a == null) {
            synchronized (this) {
                if (this.a == null) {
                    final File filesDir = this.b.l().getFilesDir();
                    final StringBuilder sb = new StringBuilder();
                    sb.append("PersistedInstallation.");
                    sb.append(this.b.q());
                    sb.append(".json");
                    this.a = new File(filesDir, sb.toString());
                }
            }
        }
        return this.a;
    }
    
    public b b(final b b) {
        try {
            final JSONObject jsonObject = new JSONObject();
            jsonObject.put("Fid", (Object)b.d());
            jsonObject.put("Status", b.g().ordinal());
            jsonObject.put("AuthToken", (Object)b.b());
            jsonObject.put("RefreshToken", (Object)b.f());
            jsonObject.put("TokenCreationEpochInSecs", b.h());
            jsonObject.put("ExpiresInSecs", b.c());
            jsonObject.put("FisError", (Object)b.e());
            final File tempFile = File.createTempFile("PersistedInstallation", "tmp", this.b.l().getFilesDir());
            final FileOutputStream fileOutputStream = new FileOutputStream(tempFile);
            fileOutputStream.write(jsonObject.toString().getBytes("UTF-8"));
            fileOutputStream.close();
            if (tempFile.renameTo(this.a())) {
                return b;
            }
            throw new IOException("unable to rename the tmpfile to PersistedInstallation");
        }
        catch (final JSONException | IOException ex) {
            return b;
        }
    }
    
    public final JSONObject c() {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final byte[] array = new byte[16384];
        try {
            final FileInputStream fileInputStream = new FileInputStream(this.a());
            try {
                while (true) {
                    final int read = fileInputStream.read(array, 0, 16384);
                    if (read < 0) {
                        break;
                    }
                    byteArrayOutputStream.write(array, 0, read);
                }
                final JSONObject jsonObject = new JSONObject(byteArrayOutputStream.toString());
                fileInputStream.close();
                return jsonObject;
            }
            finally {
                try {
                    fileInputStream.close();
                }
                finally {
                    final Throwable t;
                    final Throwable exception;
                    t.addSuppressed(exception);
                }
            }
        }
        catch (final IOException | JSONException ex) {
            return new JSONObject();
        }
    }
    
    public b d() {
        final JSONObject c = this.c();
        return com.google.firebase.installations.local.b.a().d(c.optString("Fid", (String)null)).g(RegistrationStatus.values()[c.optInt("Status", RegistrationStatus.ATTEMPT_MIGRATION.ordinal())]).b(c.optString("AuthToken", (String)null)).f(c.optString("RefreshToken", (String)null)).h(c.optLong("TokenCreationEpochInSecs", 0L)).c(c.optLong("ExpiresInSecs", 0L)).e(c.optString("FisError", (String)null)).a();
    }
    
    public enum RegistrationStatus
    {
        private static final RegistrationStatus[] $VALUES;
        
        ATTEMPT_MIGRATION, 
        NOT_GENERATED, 
        REGISTERED, 
        REGISTER_ERROR, 
        UNREGISTERED;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.dynamiclinks.internal;

import java.util.Arrays;
import java.util.List;
import com.google.android.gms.common.annotation.KeepForSdk;
import androidx.annotation.Keep;
import com.google.firebase.components.ComponentRegistrar;

@Keep
@KeepForSdk
public final class FirebaseDynamicLinkRegistrar implements ComponentRegistrar
{
    private static final String LIBRARY_NAME = "fire-dl";
    
    @Keep
    @Override
    public List<zi> getComponents() {
        return Arrays.asList(zi.e(l20.class).h("fire-dl").b(os.k(r10.class)).b(os.i(g4.class)).f(new k20()).d(), mj0.b("fire-dl", "21.2.0"));
    }
}

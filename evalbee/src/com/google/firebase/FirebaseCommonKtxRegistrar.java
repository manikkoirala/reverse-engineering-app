// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase;

import org.jetbrains.annotations.NotNull;
import java.util.concurrent.Executor;
import kotlinx.coroutines.CoroutineDispatcher;
import java.util.List;
import androidx.annotation.Keep;
import com.google.firebase.components.ComponentRegistrar;

@Keep
public final class FirebaseCommonKtxRegistrar implements ComponentRegistrar
{
    @NotNull
    @Override
    public List<zi> getComponents() {
        final zi d = zi.c(da1.a(za.class, CoroutineDispatcher.class)).b(os.j(da1.a(za.class, Executor.class))).f(FirebaseCommonKtxRegistrar$a.a).d();
        fg0.d((Object)d, "builder(Qualified.qualif\u2026cher()\n    }\n    .build()");
        final zi d2 = zi.c(da1.a(sj0.class, CoroutineDispatcher.class)).b(os.j(da1.a(sj0.class, Executor.class))).f(FirebaseCommonKtxRegistrar$b.a).d();
        fg0.d((Object)d2, "builder(Qualified.qualif\u2026cher()\n    }\n    .build()");
        final zi d3 = zi.c(da1.a(fc.class, CoroutineDispatcher.class)).b(os.j(da1.a(fc.class, Executor.class))).f(FirebaseCommonKtxRegistrar$c.a).d();
        fg0.d((Object)d3, "builder(Qualified.qualif\u2026cher()\n    }\n    .build()");
        final zi d4 = zi.c(da1.a(n02.class, CoroutineDispatcher.class)).b(os.j(da1.a(n02.class, Executor.class))).f(FirebaseCommonKtxRegistrar$d.a).d();
        fg0.d((Object)d4, "builder(Qualified.qualif\u2026cher()\n    }\n    .build()");
        return nh.j((Object[])new zi[] { d, d2, d3, d4 });
    }
}

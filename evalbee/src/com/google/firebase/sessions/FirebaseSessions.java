// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.sessions;

import android.content.Context;
import kotlinx.coroutines.CoroutineStart;
import java.util.Iterator;
import com.google.firebase.sessions.api.SessionSubscriber;
import java.util.Collection;
import java.util.Map;
import com.google.firebase.sessions.api.FirebaseSessionsDependencies;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.f;
import android.app.Application$ActivityLifecycleCallbacks;
import android.app.Application;
import android.util.Log;
import kotlin.coroutines.CoroutineContext;
import com.google.firebase.sessions.settings.SessionsSettings;

public final class FirebaseSessions
{
    public static final a c;
    public final r10 a;
    public final SessionsSettings b;
    
    static {
        c = new a(null);
    }
    
    public FirebaseSessions(final r10 a, final SessionsSettings b, final CoroutineContext coroutineContext) {
        fg0.e((Object)a, "firebaseApp");
        fg0.e((Object)b, "settings");
        fg0.e((Object)coroutineContext, "backgroundDispatcher");
        this.a = a;
        this.b = b;
        Log.d("FirebaseSessions", "Initializing Firebase Sessions SDK.");
        final Context applicationContext = a.l().getApplicationContext();
        if (applicationContext instanceof Application) {
            ((Application)applicationContext).registerActivityLifecycleCallbacks((Application$ActivityLifecycleCallbacks)om1.a);
            ad.d(f.a(coroutineContext), (CoroutineContext)null, (CoroutineStart)null, (q90)new q90(this, coroutineContext, null) {
                final CoroutineContext $backgroundDispatcher;
                int label;
                final FirebaseSessions this$0;
                
                private static final void invokeSuspend$lambda-1(final String s, final e30 e30) {
                    Log.w("FirebaseSessions", "FirebaseApp instance deleted. Sessions library will stop collecting data.");
                    om1.a.a(null);
                }
                
                @NotNull
                public final vl create(@Nullable final Object o, @NotNull final vl vl) {
                    return (vl)new q90(this.this$0, this.$backgroundDispatcher, vl) {
                        final CoroutineContext $backgroundDispatcher;
                        int label;
                        final FirebaseSessions this$0;
                        
                        private static final void invokeSuspend$lambda-1(final String s, final e30 e30) {
                            Log.w("FirebaseSessions", "FirebaseApp instance deleted. Sessions library will stop collecting data.");
                            om1.a.a(null);
                        }
                    };
                }
                
                @Nullable
                public final Object invoke(@NotNull final lm lm, @Nullable final vl vl) {
                    return ((FirebaseSessions$1)this.create(lm, vl)).invokeSuspend(u02.a);
                }
                
                @Nullable
                public final Object invokeSuspend(@NotNull Object c) {
                    final Object d = gg0.d();
                    final int label = this.label;
                    final boolean b = true;
                    String s = null;
                    Label_0209: {
                        Label_0193: {
                            if (label != 0) {
                                if (label != 1) {
                                    if (label == 2) {
                                        xe1.b(c);
                                        break Label_0193;
                                    }
                                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                                }
                                else {
                                    xe1.b(c);
                                }
                            }
                            else {
                                xe1.b(c);
                                final FirebaseSessionsDependencies a = FirebaseSessionsDependencies.a;
                                this.label = 1;
                                if ((c = a.c((vl)this)) == d) {
                                    return d;
                                }
                            }
                            final Iterable iterable = ((Map)c).values();
                            int n = 0;
                            Label_0157: {
                                if (iterable instanceof Collection && ((Collection)iterable).isEmpty()) {
                                    n = (b ? 1 : 0);
                                }
                                else {
                                    final Iterator iterator = iterable.iterator();
                                    do {
                                        n = (b ? 1 : 0);
                                        if (iterator.hasNext()) {
                                            continue;
                                        }
                                        break Label_0157;
                                    } while (!((SessionSubscriber)iterator.next()).a());
                                    n = 0;
                                }
                            }
                            if (n != 0) {
                                s = "No Sessions subscribers. Not listening to lifecycle events.";
                                break Label_0209;
                            }
                            final SessionsSettings b2 = FirebaseSessions.b(this.this$0);
                            this.label = 2;
                            if (b2.g((vl)this) == d) {
                                return d;
                            }
                        }
                        if (FirebaseSessions.b(this.this$0).d()) {
                            c = new SessionLifecycleClient(this.$backgroundDispatcher);
                            ((SessionLifecycleClient)c).i();
                            om1.a.a((SessionLifecycleClient)c);
                            FirebaseSessions.a(this.this$0).h(new g30());
                            return u02.a;
                        }
                        s = "Sessions SDK disabled. Not listening to lifecycle events.";
                    }
                    Log.d("FirebaseSessions", s);
                    return u02.a;
                }
            }, 3, (Object)null);
        }
        else {
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed to register lifecycle callbacks, unexpected context ");
            sb.append(((Application)applicationContext).getClass());
            sb.append('.');
            Log.e("FirebaseSessions", sb.toString());
        }
    }
    
    public static final /* synthetic */ r10 a(final FirebaseSessions firebaseSessions) {
        return firebaseSessions.a;
    }
    
    public static final /* synthetic */ SessionsSettings b(final FirebaseSessions firebaseSessions) {
        return firebaseSessions.b;
    }
    
    public static final class a
    {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.sessions.settings;

import kotlin.text.Regex;
import kotlin.time.DurationUnit;
import java.util.Map;
import com.google.android.gms.tasks.Task;
import kotlin.collections.b;
import kotlin.Pair;
import android.os.Build$VERSION;
import java.util.Arrays;
import android.os.Build;
import kotlinx.coroutines.tasks.TasksKt;
import android.util.Log;
import kotlinx.coroutines.sync.MutexKt;
import kotlin.coroutines.CoroutineContext;

public final class RemoteSettings implements ym1
{
    public static final a g;
    public final CoroutineContext a;
    public final r20 b;
    public final y7 c;
    public final bo d;
    public final SettingsCache e;
    public final by0 f;
    
    static {
        g = new a(null);
    }
    
    public RemoteSettings(final CoroutineContext a, final r20 b, final y7 c, final bo d, final jp jp) {
        fg0.e((Object)a, "backgroundDispatcher");
        fg0.e((Object)b, "firebaseInstallationsApi");
        fg0.e((Object)c, "appInfo");
        fg0.e((Object)d, "configsFetcher");
        fg0.e((Object)jp, "dataStore");
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = new SettingsCache(jp);
        this.f = MutexKt.b(false, 1, (Object)null);
    }
    
    @Override
    public Object a(vl vl) {
        Object o = null;
        Label_0049: {
            if (vl instanceof RemoteSettings$updateSettings.RemoteSettings$updateSettings$1) {
                o = vl;
                final int label = ((RemoteSettings$updateSettings.RemoteSettings$updateSettings$1)o).label;
                if ((label & Integer.MIN_VALUE) != 0x0) {
                    ((RemoteSettings$updateSettings.RemoteSettings$updateSettings$1)o).label = label + Integer.MIN_VALUE;
                    break Label_0049;
                }
            }
            o = new RemoteSettings$updateSettings.RemoteSettings$updateSettings$1(this, vl);
        }
        Object o2 = ((RemoteSettings$updateSettings.RemoteSettings$updateSettings$1)o).result;
        final Object d = gg0.d();
        final int label2 = ((RemoteSettings$updateSettings.RemoteSettings$updateSettings$1)o).label;
        Object f = null;
        Label_0688: {
            RemoteSettings l$0 = null;
            Label_0366: {
                if (label2 != 0) {
                    Label_0152: {
                        if (label2 != 1) {
                            Label_0706: {
                                if (label2 != 2) {
                                    if (label2 == 3) {
                                        vl = (vl)((RemoteSettings$updateSettings.RemoteSettings$updateSettings$1)o).L$0;
                                        try {
                                            xe1.b(o2);
                                            break Label_0688;
                                        }
                                        finally {
                                            break Label_0706;
                                        }
                                    }
                                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                                }
                                final Object o3 = ((RemoteSettings$updateSettings.RemoteSettings$updateSettings$1)o).L$1;
                                l$0 = (RemoteSettings)((RemoteSettings$updateSettings.RemoteSettings$updateSettings$1)o).L$0;
                                vl = (vl)o3;
                                try {
                                    xe1.b(o2);
                                    break Label_0366;
                                }
                                finally {
                                    break Label_0706;
                                }
                                break Label_0152;
                            }
                            ((by0)vl).c((Object)null);
                        }
                    }
                    f = ((RemoteSettings$updateSettings.RemoteSettings$updateSettings$1)o).L$1;
                    l$0 = (RemoteSettings)((RemoteSettings$updateSettings.RemoteSettings$updateSettings$1)o).L$0;
                    xe1.b(o2);
                }
                else {
                    xe1.b(o2);
                    if (!this.f.a() && !this.e.d()) {
                        return u02.a;
                    }
                    f = this.f;
                    ((RemoteSettings$updateSettings.RemoteSettings$updateSettings$1)o).L$0 = this;
                    ((RemoteSettings$updateSettings.RemoteSettings$updateSettings$1)o).L$1 = f;
                    ((RemoteSettings$updateSettings.RemoteSettings$updateSettings$1)o).label = 1;
                    if (((by0)f).d((Object)null, (vl)o) == d) {
                        return d;
                    }
                    l$0 = this;
                }
                if (!l$0.e.d()) {
                    Log.d("SessionConfigFetcher", "Remote settings cache not expired. Using cached values.");
                    final u02 a = u02.a;
                    ((by0)f).c((Object)null);
                    return a;
                }
                final Task id = l$0.b.getId();
                fg0.d((Object)id, "firebaseInstallationsApi.id");
                ((RemoteSettings$updateSettings.RemoteSettings$updateSettings$1)o).L$0 = l$0;
                ((RemoteSettings$updateSettings.RemoteSettings$updateSettings$1)o).L$1 = f;
                ((RemoteSettings$updateSettings.RemoteSettings$updateSettings$1)o).label = 2;
                if ((o2 = TasksKt.a(id, (vl)o)) == d) {
                    return d;
                }
            }
            final String s = (String)o2;
            if (s == null) {
                Log.w("SessionConfigFetcher", "Error getting Firebase Installation ID. Skipping this Session Event.");
                final u02 a2 = u02.a;
                ((by0)f).c((Object)null);
                return a2;
            }
            final Pair a3 = dz1.a((Object)"X-Crashlytics-Installation-ID", (Object)s);
            final fr1 a4 = fr1.a;
            final String format = String.format("%s/%s", Arrays.copyOf(new Object[] { Build.MANUFACTURER, Build.MODEL }, 2));
            fg0.d((Object)format, "format(format, *args)");
            final Pair a5 = dz1.a((Object)"X-Crashlytics-Device-Model", (Object)l$0.f(format));
            final String incremental = Build$VERSION.INCREMENTAL;
            fg0.d((Object)incremental, "INCREMENTAL");
            final Pair a6 = dz1.a((Object)"X-Crashlytics-OS-Build-Version", (Object)l$0.f(incremental));
            final String release = Build$VERSION.RELEASE;
            fg0.d((Object)release, "RELEASE");
            final Map j = kotlin.collections.b.j(new Pair[] { a3, a5, a6, dz1.a((Object)"X-Crashlytics-OS-Display-Version", (Object)l$0.f(release)), dz1.a((Object)"X-Crashlytics-API-Client-Version", (Object)l$0.c.f()) });
            Log.d("SessionConfigFetcher", "Fetching settings from server.");
            final bo d2 = l$0.d;
            final RemoteSettings$updateSettings$2.RemoteSettings$updateSettings$2$1 remoteSettings$updateSettings$2$1 = new RemoteSettings$updateSettings$2.RemoteSettings$updateSettings$2$1(l$0, (vl)null);
            final RemoteSettings$updateSettings$2.RemoteSettings$updateSettings$2$2 remoteSettings$updateSettings$2$2 = new RemoteSettings$updateSettings$2.RemoteSettings$updateSettings$2$2((vl)null);
            ((RemoteSettings$updateSettings.RemoteSettings$updateSettings$1)o).L$0 = f;
            ((RemoteSettings$updateSettings.RemoteSettings$updateSettings$1)o).L$1 = null;
            ((RemoteSettings$updateSettings.RemoteSettings$updateSettings$1)o).label = 3;
            if (d2.a(j, (q90)remoteSettings$updateSettings$2$1, (q90)remoteSettings$updateSettings$2$2, (vl)o) == d) {
                return d;
            }
        }
        final u02 a7 = u02.a;
        ((by0)f).c((Object)null);
        return u02.a;
    }
    
    @Override
    public Double b() {
        return this.e.g();
    }
    
    @Override
    public Boolean c() {
        return this.e.h();
    }
    
    @Override
    public lv d() {
        final Integer f = this.e.f();
        lv d;
        if (f != null) {
            final lv$a b = lv.b;
            d = lv.d(nv.h((int)f, DurationUnit.SECONDS));
        }
        else {
            d = null;
        }
        return d;
    }
    
    public final String f(final String s) {
        return new Regex("/").replace((CharSequence)s, "");
    }
    
    public static final class a
    {
    }
}

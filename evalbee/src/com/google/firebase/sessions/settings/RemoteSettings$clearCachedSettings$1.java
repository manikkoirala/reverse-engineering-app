// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.sessions.settings;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import kotlin.coroutines.jvm.internal.SuspendLambda;

@xp(c = "com.google.firebase.sessions.settings.RemoteSettings$clearCachedSettings$1", f = "RemoteSettings.kt", l = { 151 }, m = "invokeSuspend")
final class RemoteSettings$clearCachedSettings$1 extends SuspendLambda implements q90
{
    int label;
    final RemoteSettings this$0;
    
    public RemoteSettings$clearCachedSettings$1(final RemoteSettings this$0, final vl vl) {
        this.this$0 = this$0;
        super(2, vl);
    }
    
    @NotNull
    public final vl create(@Nullable final Object o, @NotNull final vl vl) {
        return (vl)new RemoteSettings$clearCachedSettings$1(this.this$0, vl);
    }
    
    @Nullable
    public final Object invoke(@NotNull final lm lm, @Nullable final vl vl) {
        return ((RemoteSettings$clearCachedSettings$1)this.create(lm, vl)).invokeSuspend(u02.a);
    }
    
    @Nullable
    public final Object invokeSuspend(@NotNull final Object o) {
        final Object d = gg0.d();
        final int label = this.label;
        if (label != 0) {
            if (label != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            xe1.b(o);
        }
        else {
            xe1.b(o);
            final SettingsCache e = RemoteSettings.e(this.this$0);
            this.label = 1;
            if (e.e((vl)this) == d) {
                return d;
            }
        }
        return u02.a;
    }
}

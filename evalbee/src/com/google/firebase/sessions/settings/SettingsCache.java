// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.sessions.settings;

import android.util.Log;
import java.io.IOException;
import androidx.datastore.preferences.core.PreferencesKt;
import kotlin.coroutines.CoroutineContext;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import kotlin.coroutines.jvm.internal.SuspendLambda;

public final class SettingsCache
{
    public static final a c;
    public static final s71.a d;
    public static final s71.a e;
    public static final s71.a f;
    public static final s71.a g;
    public static final s71.a h;
    public final jp a;
    public tl1 b;
    
    static {
        c = new a(null);
        d = u71.a("firebase_sessions_enabled");
        e = u71.b("firebase_sessions_sampling_rate");
        f = u71.d("firebase_sessions_restart_timeout");
        g = u71.d("firebase_sessions_cache_duration");
        h = u71.e("firebase_sessions_cache_updated_time");
    }
    
    public SettingsCache(final jp a) {
        fg0.e((Object)a, "dataStore");
        this.a = a;
        ad.f((CoroutineContext)null, (q90)new q90(this, null) {
            Object L$0;
            int label;
            final SettingsCache this$0;
            
            @NotNull
            public final vl create(@Nullable final Object o, @NotNull final vl vl) {
                return (vl)new q90(this.this$0, vl) {
                    Object L$0;
                    int label;
                    final SettingsCache this$0;
                };
            }
            
            @Nullable
            public final Object invoke(@NotNull final lm lm, @Nullable final vl vl) {
                return ((SettingsCache$1)this.create(lm, vl)).invokeSuspend(u02.a);
            }
            
            @Nullable
            public final Object invokeSuspend(@NotNull final Object o) {
                final Object d = gg0.d();
                final int label = this.label;
                SettingsCache settingsCache;
                Object o2;
                if (label != 0) {
                    if (label != 1) {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    settingsCache = (SettingsCache)this.L$0;
                    xe1.b(o);
                    o2 = o;
                }
                else {
                    xe1.b(o);
                    final SettingsCache this$0 = this.this$0;
                    final y40 data = SettingsCache.a(this$0).getData();
                    this.L$0 = this$0;
                    this.label = 1;
                    o2 = d50.o(data, (vl)this);
                    if (o2 == d) {
                        return d;
                    }
                    settingsCache = this$0;
                }
                settingsCache.m(((s71)o2).d());
                return u02.a;
            }
        }, 1, (Object)null);
    }
    
    public static final /* synthetic */ jp a(final SettingsCache settingsCache) {
        return settingsCache.a;
    }
    
    public final boolean d() {
        final tl1 b = this.b;
        final tl1 tl1 = null;
        tl1 tl2 = b;
        if (b == null) {
            fg0.t("sessionConfigs");
            tl2 = null;
        }
        final Long b2 = tl2.b();
        tl1 b3 = this.b;
        if (b3 == null) {
            fg0.t("sessionConfigs");
            b3 = tl1;
        }
        final Integer a = b3.a();
        return b2 == null || a == null || (System.currentTimeMillis() - b2) / 1000 >= a;
    }
    
    public final Object e(final vl vl) {
        final Object a = PreferencesKt.a(this.a, (q90)new SettingsCache$removeConfigs.SettingsCache$removeConfigs$2(this, (vl)null), vl);
        if (a == gg0.d()) {
            return a;
        }
        return u02.a;
    }
    
    public final Integer f() {
        tl1 b;
        if ((b = this.b) == null) {
            fg0.t("sessionConfigs");
            b = null;
        }
        return b.d();
    }
    
    public final Double g() {
        tl1 b;
        if ((b = this.b) == null) {
            fg0.t("sessionConfigs");
            b = null;
        }
        return b.e();
    }
    
    public final Boolean h() {
        tl1 b;
        if ((b = this.b) == null) {
            fg0.t("sessionConfigs");
            b = null;
        }
        return b.c();
    }
    
    public final Object i(final s71.a a, Object o, final vl vl) {
        SettingsCache$updateConfigValue.SettingsCache$updateConfigValue$1 settingsCache$updateConfigValue$2 = null;
        Label_0054: {
            if (vl instanceof SettingsCache$updateConfigValue.SettingsCache$updateConfigValue$1) {
                final SettingsCache$updateConfigValue.SettingsCache$updateConfigValue$1 settingsCache$updateConfigValue$1 = (SettingsCache$updateConfigValue.SettingsCache$updateConfigValue$1)vl;
                final int label = settingsCache$updateConfigValue$1.label;
                if ((label & Integer.MIN_VALUE) != 0x0) {
                    settingsCache$updateConfigValue$1.label = label + Integer.MIN_VALUE;
                    settingsCache$updateConfigValue$2 = settingsCache$updateConfigValue$1;
                    break Label_0054;
                }
            }
            settingsCache$updateConfigValue$2 = new SettingsCache$updateConfigValue.SettingsCache$updateConfigValue$1(this, vl);
        }
        final Object result = settingsCache$updateConfigValue$2.result;
        final Object d = gg0.d();
        final int label2 = settingsCache$updateConfigValue$2.label;
        if (label2 != 0) {
            if (label2 == 1) {
                Label_0152: {
                    try {
                        xe1.b(result);
                        return u02.a;
                    }
                    catch (final IOException obj) {
                        break Label_0152;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                o = new StringBuilder();
                ((StringBuilder)o).append("Failed to update cache config value: ");
                final IOException obj;
                ((StringBuilder)o).append(obj);
                Log.w("SettingsCache", ((StringBuilder)o).toString());
                return u02.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        xe1.b(result);
        final jp a2 = this.a;
        final SettingsCache$updateConfigValue.SettingsCache$updateConfigValue$2 settingsCache$updateConfigValue$3 = new SettingsCache$updateConfigValue.SettingsCache$updateConfigValue$2(o, a, this, (vl)null);
        settingsCache$updateConfigValue$2.label = 1;
        if (PreferencesKt.a(a2, (q90)settingsCache$updateConfigValue$3, (vl)settingsCache$updateConfigValue$2) == d) {
            return d;
        }
        return u02.a;
    }
    
    public final Object j(final Double n, final vl vl) {
        final Object i = this.i(SettingsCache.e, n, vl);
        if (i == gg0.d()) {
            return i;
        }
        return u02.a;
    }
    
    public final Object k(final Integer n, final vl vl) {
        final Object i = this.i(SettingsCache.g, n, vl);
        if (i == gg0.d()) {
            return i;
        }
        return u02.a;
    }
    
    public final Object l(final Long n, final vl vl) {
        final Object i = this.i(SettingsCache.h, n, vl);
        if (i == gg0.d()) {
            return i;
        }
        return u02.a;
    }
    
    public final void m(final s71 s71) {
        this.b = new tl1((Boolean)s71.b(SettingsCache.d), (Double)s71.b(SettingsCache.e), (Integer)s71.b(SettingsCache.f), (Integer)s71.b(SettingsCache.g), (Long)s71.b(SettingsCache.h));
    }
    
    public final Object n(final Integer n, final vl vl) {
        final Object i = this.i(SettingsCache.f, n, vl);
        if (i == gg0.d()) {
            return i;
        }
        return u02.a;
    }
    
    public final Object o(final Boolean b, final vl vl) {
        final Object i = this.i(SettingsCache.d, b, vl);
        if (i == gg0.d()) {
            return i;
        }
        return u02.a;
    }
    
    public static final class a
    {
    }
}

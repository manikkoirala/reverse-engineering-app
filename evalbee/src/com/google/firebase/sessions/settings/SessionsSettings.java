// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.sessions.settings;

import kotlin.jvm.internal.PropertyReference2;
import kotlin.jvm.internal.PropertyReference2Impl;
import kotlin.time.DurationUnit;
import kotlin.coroutines.CoroutineContext;
import android.content.Context;
import androidx.datastore.preferences.PreferenceDataStoreDelegateKt;

public final class SessionsSettings
{
    public static final a c;
    public static final pc1 d;
    public final ym1 a;
    public final ym1 b;
    
    static {
        c = new a(null);
        d = PreferenceDataStoreDelegateKt.b(ul1.a.b(), null, null, null, 14, null);
    }
    
    public SessionsSettings(final Context context, final CoroutineContext coroutineContext, final CoroutineContext coroutineContext2, final r20 r20, final y7 y7) {
        this(new yk0(context), new RemoteSettings(coroutineContext2, r20, y7, new RemoteSettingsFetcher(y7, coroutineContext, null, 4, null), SessionsSettings.c.b(context)));
    }
    
    public SessionsSettings(final r10 r10, final CoroutineContext coroutineContext, final CoroutineContext coroutineContext2, final r20 r11) {
        fg0.e((Object)r10, "firebaseApp");
        fg0.e((Object)coroutineContext, "blockingDispatcher");
        fg0.e((Object)coroutineContext2, "backgroundDispatcher");
        fg0.e((Object)r11, "firebaseInstallationsApi");
        final Context l = r10.l();
        fg0.d((Object)l, "firebaseApp.applicationContext");
        this(l, coroutineContext, coroutineContext2, r11, yl1.a.b(r10));
    }
    
    public SessionsSettings(final ym1 a, final ym1 b) {
        fg0.e((Object)a, "localOverrideSettings");
        fg0.e((Object)b, "remoteSettings");
        this.a = a;
        this.b = b;
    }
    
    public static final /* synthetic */ pc1 a() {
        return SessionsSettings.d;
    }
    
    public final double b() {
        final Double b = this.a.b();
        if (b != null) {
            final double doubleValue = b.doubleValue();
            if (this.e(doubleValue)) {
                return doubleValue;
            }
        }
        final Double b2 = this.b.b();
        if (b2 != null) {
            final double doubleValue2 = b2.doubleValue();
            if (this.e(doubleValue2)) {
                return doubleValue2;
            }
        }
        return 1.0;
    }
    
    public final long c() {
        final lv d = this.a.d();
        if (d != null) {
            final long f = d.F();
            if (this.f(f)) {
                return f;
            }
        }
        final lv d2 = this.b.d();
        if (d2 != null) {
            final long f2 = d2.F();
            if (this.f(f2)) {
                return f2;
            }
        }
        final lv$a b = lv.b;
        return nv.h(30, DurationUnit.MINUTES);
    }
    
    public final boolean d() {
        final Boolean c = this.a.c();
        if (c != null) {
            return c;
        }
        final Boolean c2 = this.b.c();
        return c2 == null || c2;
    }
    
    public final boolean e(final double n) {
        boolean b = false;
        if (0.0 <= n) {
            b = b;
            if (n <= 1.0) {
                b = true;
            }
        }
        return b;
    }
    
    public final boolean f(final long n) {
        return lv.B(n) && lv.w(n);
    }
    
    public final Object g(final vl vl) {
        Object o = null;
        Label_0047: {
            if (vl instanceof SessionsSettings$updateSettings.SessionsSettings$updateSettings$1) {
                final SessionsSettings$updateSettings.SessionsSettings$updateSettings$1 sessionsSettings$updateSettings$1 = (SessionsSettings$updateSettings.SessionsSettings$updateSettings$1)vl;
                final int label = sessionsSettings$updateSettings$1.label;
                if ((label & Integer.MIN_VALUE) != 0x0) {
                    sessionsSettings$updateSettings$1.label = label + Integer.MIN_VALUE;
                    o = sessionsSettings$updateSettings$1;
                    break Label_0047;
                }
            }
            o = new SessionsSettings$updateSettings.SessionsSettings$updateSettings$1(this, vl);
        }
        final Object result = ((SessionsSettings$updateSettings.SessionsSettings$updateSettings$1)o).result;
        final Object d = gg0.d();
        final int label2 = ((SessionsSettings$updateSettings.SessionsSettings$updateSettings$1)o).label;
        SessionsSettings sessionsSettings;
        if (label2 != 0) {
            if (label2 != 1) {
                if (label2 == 2) {
                    xe1.b(result);
                    return u02.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            else {
                sessionsSettings = (SessionsSettings)((SessionsSettings$updateSettings.SessionsSettings$updateSettings$1)o).L$0;
                xe1.b(result);
            }
        }
        else {
            xe1.b(result);
            final ym1 a = this.a;
            ((SessionsSettings$updateSettings.SessionsSettings$updateSettings$1)o).L$0 = this;
            ((SessionsSettings$updateSettings.SessionsSettings$updateSettings$1)o).label = 1;
            if (a.a((vl)o) == d) {
                return d;
            }
            sessionsSettings = this;
        }
        final ym1 b = sessionsSettings.b;
        ((SessionsSettings$updateSettings.SessionsSettings$updateSettings$1)o).L$0 = null;
        ((SessionsSettings$updateSettings.SessionsSettings$updateSettings$1)o).label = 2;
        if (b.a((vl)o) == d) {
            return d;
        }
        return u02.a;
    }
    
    public static final class a
    {
        public static final gi0[] a;
        
        static {
            a = new gi0[] { (gi0)yc1.i((PropertyReference2)new PropertyReference2Impl((Class)a.class, "dataStore", "getDataStore(Landroid/content/Context;)Landroidx/datastore/core/DataStore;", 0)) };
        }
        
        public final jp b(final Context context) {
            return (jp)SessionsSettings.a().a((Object)context, SessionsSettings.a.a[0]);
        }
        
        public final SessionsSettings c() {
            final Object j = u20.a(o10.a).j(SessionsSettings.class);
            fg0.d(j, "Firebase.app[SessionsSettings::class.java]");
            return (SessionsSettings)j;
        }
    }
}

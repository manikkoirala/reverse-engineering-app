// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.sessions.settings;

import android.net.Uri$Builder;
import java.util.Map;
import java.net.URL;
import kotlin.coroutines.CoroutineContext;

public final class RemoteSettingsFetcher implements bo
{
    public static final a d;
    public final y7 a;
    public final CoroutineContext b;
    public final String c;
    
    static {
        d = new a(null);
    }
    
    public RemoteSettingsFetcher(final y7 a, final CoroutineContext b, final String c) {
        fg0.e((Object)a, "appInfo");
        fg0.e((Object)b, "blockingDispatcher");
        fg0.e((Object)c, "baseUrl");
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    @Override
    public Object a(final Map map, final q90 q90, final q90 q91, final vl vl) {
        final Object g = ad.g(this.b, (q90)new RemoteSettingsFetcher$doConfigFetch.RemoteSettingsFetcher$doConfigFetch$2(this, map, q90, q91, (vl)null), vl);
        if (g == gg0.d()) {
            return g;
        }
        return u02.a;
    }
    
    public final URL c() {
        return new URL(new Uri$Builder().scheme("https").authority(this.c).appendPath("spi").appendPath("v2").appendPath("platforms").appendPath("android").appendPath("gmp").appendPath(this.a.b()).appendPath("settings").appendQueryParameter("build_version", this.a.a().a()).appendQueryParameter("display_version", this.a.a().f()).build().toString());
    }
    
    public static final class a
    {
    }
}

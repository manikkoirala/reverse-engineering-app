// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.sessions;

import java.util.List;
import android.content.Context;
import kotlin.coroutines.CoroutineContext;
import com.google.firebase.sessions.settings.SessionsSettings;
import com.google.android.datatransport.TransportFactory;
import kotlinx.coroutines.CoroutineDispatcher;
import org.jetbrains.annotations.NotNull;
import androidx.annotation.Keep;
import com.google.firebase.components.ComponentRegistrar;

@Keep
public final class FirebaseSessionsRegistrar implements ComponentRegistrar
{
    @NotNull
    private static final a Companion;
    @Deprecated
    @NotNull
    private static final String LIBRARY_NAME = "fire-sessions";
    @Deprecated
    private static final da1 backgroundDispatcher;
    @Deprecated
    private static final da1 blockingDispatcher;
    @Deprecated
    private static final da1 firebaseApp;
    @Deprecated
    private static final da1 firebaseInstallationsApi;
    @Deprecated
    private static final da1 sessionFirelogPublisher;
    @Deprecated
    private static final da1 sessionGenerator;
    @Deprecated
    private static final da1 sessionsSettings;
    @Deprecated
    private static final da1 transportFactory;
    
    static {
        Companion = new a(null);
        firebaseApp = da1.b(r10.class);
        firebaseInstallationsApi = da1.b(r20.class);
        backgroundDispatcher = da1.a(za.class, CoroutineDispatcher.class);
        blockingDispatcher = da1.a(fc.class, CoroutineDispatcher.class);
        transportFactory = da1.b(TransportFactory.class);
        sessionFirelogPublisher = da1.b(zl1.class);
        sessionGenerator = da1.b(SessionGenerator.class);
        sessionsSettings = da1.b(SessionsSettings.class);
    }
    
    private static final FirebaseSessions getComponents$lambda-0(final gj gj) {
        final Object d = gj.d(FirebaseSessionsRegistrar.firebaseApp);
        fg0.d(d, "container[firebaseApp]");
        final r10 r10 = (r10)d;
        final Object d2 = gj.d(FirebaseSessionsRegistrar.sessionsSettings);
        fg0.d(d2, "container[sessionsSettings]");
        final SessionsSettings sessionsSettings = (SessionsSettings)d2;
        final Object d3 = gj.d(FirebaseSessionsRegistrar.backgroundDispatcher);
        fg0.d(d3, "container[backgroundDispatcher]");
        return new FirebaseSessions(r10, sessionsSettings, (CoroutineContext)d3);
    }
    
    private static final SessionGenerator getComponents$lambda-1(final gj gj) {
        return new SessionGenerator(v52.a, null, 2, null);
    }
    
    private static final zl1 getComponents$lambda-2(final gj gj) {
        final Object d = gj.d(FirebaseSessionsRegistrar.firebaseApp);
        fg0.d(d, "container[firebaseApp]");
        final r10 r10 = (r10)d;
        final Object d2 = gj.d(FirebaseSessionsRegistrar.firebaseInstallationsApi);
        fg0.d(d2, "container[firebaseInstallationsApi]");
        final r20 r11 = (r20)d2;
        final Object d3 = gj.d(FirebaseSessionsRegistrar.sessionsSettings);
        fg0.d(d3, "container[sessionsSettings]");
        final SessionsSettings sessionsSettings = (SessionsSettings)d3;
        final r91 g = gj.g(FirebaseSessionsRegistrar.transportFactory);
        fg0.d((Object)g, "container.getProvider(transportFactory)");
        final ox ox = new ox(g);
        final Object d4 = gj.d(FirebaseSessionsRegistrar.backgroundDispatcher);
        fg0.d(d4, "container[backgroundDispatcher]");
        return new SessionFirelogPublisherImpl(r10, r11, sessionsSettings, ox, (CoroutineContext)d4);
    }
    
    private static final SessionsSettings getComponents$lambda-3(final gj gj) {
        final Object d = gj.d(FirebaseSessionsRegistrar.firebaseApp);
        fg0.d(d, "container[firebaseApp]");
        final r10 r10 = (r10)d;
        final Object d2 = gj.d(FirebaseSessionsRegistrar.blockingDispatcher);
        fg0.d(d2, "container[blockingDispatcher]");
        final CoroutineContext coroutineContext = (CoroutineContext)d2;
        final Object d3 = gj.d(FirebaseSessionsRegistrar.backgroundDispatcher);
        fg0.d(d3, "container[backgroundDispatcher]");
        final CoroutineContext coroutineContext2 = (CoroutineContext)d3;
        final Object d4 = gj.d(FirebaseSessionsRegistrar.firebaseInstallationsApi);
        fg0.d(d4, "container[firebaseInstallationsApi]");
        return new SessionsSettings(r10, coroutineContext, coroutineContext2, (r20)d4);
    }
    
    private static final vl1 getComponents$lambda-4(final gj gj) {
        final Context l = ((r10)gj.d(FirebaseSessionsRegistrar.firebaseApp)).l();
        fg0.d((Object)l, "container[firebaseApp].applicationContext");
        final Object d = gj.d(FirebaseSessionsRegistrar.backgroundDispatcher);
        fg0.d(d, "container[backgroundDispatcher]");
        return new SessionDatastoreImpl(l, (CoroutineContext)d);
    }
    
    private static final cm1 getComponents$lambda-5(final gj gj) {
        final Object d = gj.d(FirebaseSessionsRegistrar.firebaseApp);
        fg0.d(d, "container[firebaseApp]");
        return new dm1((r10)d);
    }
    
    @NotNull
    @Override
    public List<zi> getComponents() {
        final zi.b h = zi.e(FirebaseSessions.class).h("fire-sessions");
        final da1 firebaseApp = FirebaseSessionsRegistrar.firebaseApp;
        final zi.b b = h.b(os.j(firebaseApp));
        final da1 sessionsSettings = FirebaseSessionsRegistrar.sessionsSettings;
        final zi.b b2 = b.b(os.j(sessionsSettings));
        final da1 backgroundDispatcher = FirebaseSessionsRegistrar.backgroundDispatcher;
        final zi d = b2.b(os.j(backgroundDispatcher)).f(new i30()).e().d();
        final zi d2 = zi.e(SessionGenerator.class).h("session-generator").f(new j30()).d();
        final zi.b b3 = zi.e(zl1.class).h("session-publisher").b(os.j(firebaseApp));
        final da1 firebaseInstallationsApi = FirebaseSessionsRegistrar.firebaseInstallationsApi;
        return nh.j((Object[])new zi[] { d, d2, b3.b(os.j(firebaseInstallationsApi)).b(os.j(sessionsSettings)).b(os.l(FirebaseSessionsRegistrar.transportFactory)).b(os.j(backgroundDispatcher)).f(new k30()).d(), zi.e(SessionsSettings.class).h("sessions-settings").b(os.j(firebaseApp)).b(os.j(FirebaseSessionsRegistrar.blockingDispatcher)).b(os.j(backgroundDispatcher)).b(os.j(firebaseInstallationsApi)).f(new l30()).d(), zi.e(vl1.class).h("sessions-datastore").b(os.j(firebaseApp)).b(os.j(backgroundDispatcher)).f(new m30()).d(), zi.e(cm1.class).h("sessions-service-binder").b(os.j(firebaseApp)).f(new n30()).d(), mj0.b("fire-sessions", "1.2.0") });
    }
    
    public static final class a
    {
    }
}

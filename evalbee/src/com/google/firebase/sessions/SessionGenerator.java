// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.sessions;

import java.util.Locale;
import java.util.UUID;

public final class SessionGenerator
{
    public static final a f;
    public final kw1 a;
    public final a90 b;
    public final String c;
    public int d;
    public wl1 e;
    
    static {
        f = new a(null);
    }
    
    public SessionGenerator(final kw1 a, final a90 b) {
        fg0.e((Object)a, "timeProvider");
        fg0.e((Object)b, "uuidGenerator");
        this.a = a;
        this.b = b;
        this.c = this.b();
        this.d = -1;
    }
    
    public final wl1 a() {
        final int d = this.d + 1;
        this.d = d;
        String s;
        if (d == 0) {
            s = this.c;
        }
        else {
            s = this.b();
        }
        this.e = new wl1(s, this.c, this.d, this.a.a());
        return this.c();
    }
    
    public final String b() {
        final String string = ((UUID)this.b.invoke()).toString();
        fg0.d((Object)string, "uuidGenerator().toString()");
        final String lowerCase = qr1.r(string, "-", "", false, 4, (Object)null).toLowerCase(Locale.ROOT);
        fg0.d((Object)lowerCase, "this as java.lang.String).toLowerCase(Locale.ROOT)");
        return lowerCase;
    }
    
    public final wl1 c() {
        final wl1 e = this.e;
        if (e != null) {
            return e;
        }
        fg0.t("currentSession");
        return null;
    }
    
    public static final class a
    {
        public final SessionGenerator a() {
            final Object j = u20.a(o10.a).j(SessionGenerator.class);
            fg0.d(j, "Firebase.app[SessionGenerator::class.java]");
            return (SessionGenerator)j;
        }
    }
}

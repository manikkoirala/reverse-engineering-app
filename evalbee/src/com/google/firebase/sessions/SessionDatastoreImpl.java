// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.sessions;

import kotlin.jvm.internal.PropertyReference2;
import kotlin.jvm.internal.PropertyReference2Impl;
import kotlinx.coroutines.CoroutineStart;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.f;
import androidx.datastore.preferences.PreferenceDataStoreDelegateKt;
import java.util.concurrent.atomic.AtomicReference;
import kotlin.coroutines.CoroutineContext;
import android.content.Context;

public final class SessionDatastoreImpl implements vl1
{
    public static final a f;
    public static final pc1 g;
    public final Context b;
    public final CoroutineContext c;
    public final AtomicReference d;
    public final y40 e;
    
    static {
        f = new a(null);
        g = PreferenceDataStoreDelegateKt.b(ul1.a.a(), null, null, null, 14, null);
    }
    
    public SessionDatastoreImpl(final Context b, final CoroutineContext c) {
        fg0.e((Object)b, "context");
        fg0.e((Object)c, "backgroundDispatcher");
        this.b = b;
        this.c = c;
        this.d = new AtomicReference();
        this.e = (y40)new SessionDatastoreImpl$special$$inlined$map.SessionDatastoreImpl$special$$inlined$map$1(d50.d(SessionDatastoreImpl.f.b(b).getData(), (s90)new SessionDatastoreImpl$firebaseSessionDataFlow.SessionDatastoreImpl$firebaseSessionDataFlow$1((vl)null)), this);
        ad.d(kotlinx.coroutines.f.a(c), (CoroutineContext)null, (CoroutineStart)null, (q90)new q90(this, null) {
            int label;
            final SessionDatastoreImpl this$0;
            
            @NotNull
            public final vl create(@Nullable final Object o, @NotNull final vl vl) {
                return (vl)new q90(this.this$0, vl) {
                    int label;
                    final SessionDatastoreImpl this$0;
                };
            }
            
            @Nullable
            public final Object invoke(@NotNull final lm lm, @Nullable final vl vl) {
                return ((SessionDatastoreImpl$1)this.create(lm, vl)).invokeSuspend(u02.a);
            }
            
            @Nullable
            public final Object invokeSuspend(@NotNull final Object o) {
                final Object d = gg0.d();
                final int label = this.label;
                if (label != 0) {
                    if (label != 1) {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    xe1.b(o);
                }
                else {
                    xe1.b(o);
                    final y40 g = SessionDatastoreImpl.g(this.this$0);
                    final z40 z40 = (z40)new z40(this.this$0) {
                        public final SessionDatastoreImpl a;
                        
                        public final Object a(final h30 newValue, final vl vl) {
                            SessionDatastoreImpl.e(this.a).set(newValue);
                            return u02.a;
                        }
                    };
                    this.label = 1;
                    if (g.a((z40)z40, (vl)this) == d) {
                        return d;
                    }
                }
                return u02.a;
            }
        }, 3, (Object)null);
    }
    
    public static final /* synthetic */ AtomicReference e(final SessionDatastoreImpl sessionDatastoreImpl) {
        return sessionDatastoreImpl.d;
    }
    
    public static final /* synthetic */ pc1 f() {
        return SessionDatastoreImpl.g;
    }
    
    public static final /* synthetic */ y40 g(final SessionDatastoreImpl sessionDatastoreImpl) {
        return sessionDatastoreImpl.e;
    }
    
    @Override
    public void a(final String s) {
        fg0.e((Object)s, "sessionId");
        ad.d(kotlinx.coroutines.f.a(this.c), (CoroutineContext)null, (CoroutineStart)null, (q90)new SessionDatastoreImpl$updateSessionId.SessionDatastoreImpl$updateSessionId$1(this, s, (vl)null), 3, (Object)null);
    }
    
    @Override
    public String b() {
        final h30 h30 = this.d.get();
        String a;
        if (h30 != null) {
            a = h30.a();
        }
        else {
            a = null;
        }
        return a;
    }
    
    public final h30 i(final s71 s71) {
        return new h30((String)s71.b(SessionDatastoreImpl.b.a.a()));
    }
    
    public static final class a
    {
        public static final gi0[] a;
        
        static {
            a = new gi0[] { (gi0)yc1.i((PropertyReference2)new PropertyReference2Impl((Class)a.class, "dataStore", "getDataStore(Landroid/content/Context;)Landroidx/datastore/core/DataStore;", 0)) };
        }
        
        public final jp b(final Context context) {
            return (jp)SessionDatastoreImpl.f().a((Object)context, SessionDatastoreImpl.a.a[0]);
        }
    }
    
    public static final class b
    {
        public static final b a;
        public static final s71.a b;
        
        static {
            a = new b();
            b = u71.f("session_id");
        }
        
        public final s71.a a() {
            return SessionDatastoreImpl.b.b;
        }
    }
}

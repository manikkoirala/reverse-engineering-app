// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.sessions;

import android.os.BaseBundle;
import android.os.Bundle;
import android.os.Looper;
import android.os.RemoteException;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.f;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import android.os.Handler;
import kotlinx.coroutines.n;
import android.os.Message;
import java.util.List;
import android.util.Log;
import android.os.IBinder;
import android.content.ComponentName;
import android.content.ServiceConnection;
import java.util.concurrent.LinkedBlockingDeque;
import android.os.Messenger;
import kotlin.coroutines.CoroutineContext;

public final class SessionLifecycleClient
{
    public static final a f;
    public final CoroutineContext a;
    public Messenger b;
    public boolean c;
    public final LinkedBlockingDeque d;
    public final SessionLifecycleClient$b e;
    
    static {
        f = new a(null);
    }
    
    public SessionLifecycleClient(final CoroutineContext a) {
        fg0.e((Object)a, "backgroundDispatcher");
        this.a = a;
        this.d = new LinkedBlockingDeque(20);
        this.e = new ServiceConnection(this) {
            public final SessionLifecycleClient a;
            
            public void onServiceConnected(final ComponentName componentName, final IBinder binder) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Connected to SessionLifecycleService. Queue size ");
                sb.append(SessionLifecycleClient.c(this.a).size());
                Log.d("SessionLifecycleClient", sb.toString());
                SessionLifecycleClient.f(this.a, new Messenger(binder));
                SessionLifecycleClient.g(this.a, true);
                final SessionLifecycleClient a = this.a;
                a.o(a.j());
            }
            
            public void onServiceDisconnected(final ComponentName componentName) {
                Log.d("SessionLifecycleClient", "Disconnected from SessionLifecycleService");
                SessionLifecycleClient.f(this.a, null);
                SessionLifecycleClient.g(this.a, false);
            }
        };
    }
    
    public static final /* synthetic */ LinkedBlockingDeque c(final SessionLifecycleClient sessionLifecycleClient) {
        return sessionLifecycleClient.d;
    }
    
    public static final /* synthetic */ void f(final SessionLifecycleClient sessionLifecycleClient, final Messenger b) {
        sessionLifecycleClient.b = b;
    }
    
    public static final /* synthetic */ void g(final SessionLifecycleClient sessionLifecycleClient, final boolean c) {
        sessionLifecycleClient.c = c;
    }
    
    public final void h() {
        this.n(2);
    }
    
    public final void i() {
        cm1.a.a().a(new Messenger((Handler)new ClientUpdateHandler(this.a)), (ServiceConnection)this.e);
    }
    
    public final List j() {
        final ArrayList c = new ArrayList();
        this.d.drainTo(c);
        return c;
    }
    
    public final void k() {
        this.n(1);
    }
    
    public final Message l(final List list, final int n) {
        final Iterable iterable = list;
        final ArrayList list2 = new ArrayList();
        for (final Object next : iterable) {
            if (((Message)next).what == n) {
                list2.add(next);
            }
        }
        final Iterator iterator2 = list2.iterator();
        Object next2;
        if (!iterator2.hasNext()) {
            next2 = null;
        }
        else {
            next2 = iterator2.next();
            if (iterator2.hasNext()) {
                long when = ((Message)next2).getWhen();
                Message message = (Message)next2;
                do {
                    final Object next3 = iterator2.next();
                    final long when2 = ((Message)next3).getWhen();
                    long n2 = when;
                    next2 = message;
                    if (when < when2) {
                        next2 = next3;
                        n2 = when2;
                    }
                    when = n2;
                    message = (Message)next2;
                } while (iterator2.hasNext());
            }
        }
        return (Message)next2;
    }
    
    public final void m(final Message e) {
        StringBuilder sb2;
        if (this.d.offer(e)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Queued message ");
            sb.append(e.what);
            sb.append(". Queue size ");
            sb.append(this.d.size());
            sb2 = sb;
        }
        else {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Failed to enqueue message ");
            sb3.append(e.what);
            sb3.append(". Dropping.");
            sb2 = sb3;
        }
        Log.d("SessionLifecycleClient", sb2.toString());
    }
    
    public final void n(final int n) {
        final List j = this.j();
        final Message obtain = Message.obtain((Handler)null, n, 0, 0);
        fg0.d((Object)obtain, "obtain(null, messageCode, 0, 0)");
        j.add(obtain);
        this.o(j);
    }
    
    public final n o(final List list) {
        return ad.d(kotlinx.coroutines.f.a(this.a), (CoroutineContext)null, (CoroutineStart)null, (q90)new SessionLifecycleClient$sendLifecycleEvents.SessionLifecycleClient$sendLifecycleEvents$1(this, list, (vl)null), 3, (Object)null);
    }
    
    public final void p(final Message message) {
        if (this.b != null) {
            try {
                final StringBuilder sb = new StringBuilder();
                sb.append("Sending lifecycle ");
                sb.append(message.what);
                sb.append(" to service");
                Log.d("SessionLifecycleClient", sb.toString());
                final Messenger b = this.b;
                if (b != null) {
                    b.send(message);
                }
                return;
            }
            catch (final RemoteException ex) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Unable to deliver message: ");
                sb2.append(message.what);
                Log.w("SessionLifecycleClient", sb2.toString(), (Throwable)ex);
            }
        }
        this.m(message);
    }
    
    public static final class ClientUpdateHandler extends Handler
    {
        public final CoroutineContext a;
        
        public ClientUpdateHandler(final CoroutineContext a) {
            fg0.e((Object)a, "backgroundDispatcher");
            super(Looper.getMainLooper());
            this.a = a;
        }
        
        public final void a(final String str) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Session update received: ");
            sb.append(str);
            Log.d("SessionLifecycleClient", sb.toString());
            ad.d(kotlinx.coroutines.f.a(this.a), (CoroutineContext)null, (CoroutineStart)null, (q90)new SessionLifecycleClient$ClientUpdateHandler$handleSessionUpdate.SessionLifecycleClient$ClientUpdateHandler$handleSessionUpdate$1(str, (vl)null), 3, (Object)null);
        }
        
        public void handleMessage(final Message obj) {
            fg0.e((Object)obj, "msg");
            if (obj.what == 3) {
                final Bundle data = obj.getData();
                String string;
                if (data == null || (string = ((BaseBundle)data).getString("SessionUpdateExtra")) == null) {
                    string = "";
                }
                this.a(string);
            }
            else {
                final StringBuilder sb = new StringBuilder();
                sb.append("Received unexpected event from the SessionLifecycleService: ");
                sb.append(obj);
                Log.w("SessionLifecycleClient", sb.toString());
                super.handleMessage(obj);
            }
        }
    }
    
    public static final class a
    {
    }
}

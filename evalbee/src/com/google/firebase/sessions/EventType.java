// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.sessions;

public enum EventType implements p01
{
    private static final EventType[] $VALUES;
    
    EVENT_TYPE_UNKNOWN(0), 
    SESSION_START(1);
    
    private final int number;
    
    private static final /* synthetic */ EventType[] $values() {
        return new EventType[] { EventType.EVENT_TYPE_UNKNOWN, EventType.SESSION_START };
    }
    
    static {
        $VALUES = $values();
    }
    
    private EventType(final int number) {
        this.number = number;
    }
    
    @Override
    public int getNumber() {
        return this.number;
    }
}

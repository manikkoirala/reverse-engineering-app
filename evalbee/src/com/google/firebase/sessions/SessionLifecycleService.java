// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.sessions;

import android.os.BaseBundle;
import android.os.DeadObjectException;
import android.os.Bundle;
import com.google.firebase.sessions.settings.SessionsSettings;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import android.os.Looper;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.os.IBinder;
import android.os.Build$VERSION;
import android.content.Intent;
import android.os.Messenger;
import android.os.HandlerThread;
import android.app.Service;

public final class SessionLifecycleService extends Service
{
    public static final a d;
    public final HandlerThread a;
    public b b;
    public Messenger c;
    
    static {
        d = new a(null);
    }
    
    public SessionLifecycleService() {
        this.a = new HandlerThread("FirebaseSessions_HandlerThread");
    }
    
    public final Messenger a(final Intent intent) {
        Object o;
        if (Build$VERSION.SDK_INT >= 33) {
            o = bm1.a(intent, "ClientCallbackMessenger", (Class)Messenger.class);
        }
        else {
            o = intent.getParcelableExtra("ClientCallbackMessenger");
        }
        return (Messenger)o;
    }
    
    public IBinder onBind(final Intent intent) {
        final IBinder binder = null;
        if (intent == null) {
            Log.d("SessionLifecycleService", "Service bound with null intent. Ignoring.");
            return null;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Service bound to new client on process ");
        sb.append(intent.getAction());
        Log.d("SessionLifecycleService", sb.toString());
        final Messenger a = this.a(intent);
        if (a != null) {
            final Message obtain = Message.obtain((Handler)null, 4, 0, 0);
            obtain.replyTo = a;
            final b b = this.b;
            if (b != null) {
                b.sendMessage(obtain);
            }
        }
        final Messenger c = this.c;
        IBinder binder2 = binder;
        if (c != null) {
            binder2 = c.getBinder();
        }
        return binder2;
    }
    
    public void onCreate() {
        super.onCreate();
        ((Thread)this.a).start();
        final Looper looper = this.a.getLooper();
        fg0.d((Object)looper, "handlerThread.looper");
        this.b = new b(looper);
        this.c = new Messenger((Handler)this.b);
    }
    
    public void onDestroy() {
        super.onDestroy();
        this.a.quit();
    }
    
    public static final class a
    {
    }
    
    public static final class b extends Handler
    {
        public boolean a;
        public long b;
        public final ArrayList c;
        
        public b(final Looper looper) {
            fg0.e((Object)looper, "looper");
            super(looper);
            this.c = new ArrayList();
        }
        
        public final void a() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Broadcasting new session: ");
            final SessionGenerator.a f = SessionGenerator.f;
            sb.append(f.a().c());
            Log.d("SessionLifecycleService", sb.toString());
            zl1.a.a().a(f.a().c());
            for (final Messenger messenger : new ArrayList(this.c)) {
                fg0.d((Object)messenger, "it");
                this.f(messenger);
            }
        }
        
        public final void b(final Message message) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Activity backgrounding at ");
            sb.append(message.getWhen());
            Log.d("SessionLifecycleService", sb.toString());
            this.b = message.getWhen();
        }
        
        public final void c(final Message message) {
            this.c.add(message.replyTo);
            final Messenger replyTo = message.replyTo;
            fg0.d((Object)replyTo, "msg.replyTo");
            this.f(replyTo);
            final StringBuilder sb = new StringBuilder();
            sb.append("Client ");
            sb.append(message.replyTo);
            sb.append(" bound at ");
            sb.append(message.getWhen());
            sb.append(". Clients: ");
            sb.append(this.c.size());
            Log.d("SessionLifecycleService", sb.toString());
        }
        
        public final void d(final Message message) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Activity foregrounding at ");
            sb.append(message.getWhen());
            sb.append('.');
            Log.d("SessionLifecycleService", sb.toString());
            Label_0090: {
                if (!this.a) {
                    Log.d("SessionLifecycleService", "Cold start detected.");
                    this.a = true;
                }
                else {
                    if (!this.e(message.getWhen())) {
                        break Label_0090;
                    }
                    Log.d("SessionLifecycleService", "Session too long in background. Creating new session.");
                }
                this.g();
            }
            this.b = message.getWhen();
        }
        
        public final boolean e(final long n) {
            return n - this.b > lv.n(SessionsSettings.c.c().c());
        }
        
        public final void f(final Messenger messenger) {
            String str;
            if (this.a) {
                str = SessionGenerator.f.a().c().b();
            }
            else {
                str = vl1.a.a().b();
                final StringBuilder sb = new StringBuilder();
                sb.append("App has not yet foregrounded. Using previously stored session: ");
                sb.append(str);
                Log.d("SessionLifecycleService", sb.toString());
                if (str == null) {
                    return;
                }
            }
            this.h(messenger, str);
        }
        
        public final void g() {
            final SessionGenerator.a f = SessionGenerator.f;
            f.a().a();
            final StringBuilder sb = new StringBuilder();
            sb.append("Generated new session ");
            sb.append(f.a().c().b());
            Log.d("SessionLifecycleService", sb.toString());
            this.a();
            vl1.a.a().a(f.a().c().b());
        }
        
        public final void h(final Messenger o, final String s) {
            try {
                final Bundle data = new Bundle();
                ((BaseBundle)data).putString("SessionUpdateExtra", s);
                final Message obtain = Message.obtain((Handler)null, 3, 0, 0);
                obtain.setData(data);
                o.send(obtain);
            }
            catch (final Exception ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unable to push new session to ");
                sb.append(o);
                sb.append('.');
                Log.w("SessionLifecycleService", sb.toString(), (Throwable)ex);
            }
            catch (final DeadObjectException ex2) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Removing dead client from list: ");
                sb2.append(o);
                Log.d("SessionLifecycleService", sb2.toString());
                this.c.remove(o);
            }
        }
        
        public void handleMessage(final Message obj) {
            fg0.e((Object)obj, "msg");
            if (this.b > obj.getWhen()) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Ignoring old message from ");
                sb.append(obj.getWhen());
                sb.append(" which is older than ");
                sb.append(this.b);
                sb.append('.');
                Log.d("SessionLifecycleService", sb.toString());
                return;
            }
            final int what = obj.what;
            if (what != 1) {
                if (what != 2) {
                    if (what != 4) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Received unexpected event from the SessionLifecycleClient: ");
                        sb2.append(obj);
                        Log.w("SessionLifecycleService", sb2.toString());
                        super.handleMessage(obj);
                    }
                    else {
                        this.c(obj);
                    }
                }
                else {
                    this.b(obj);
                }
            }
            else {
                this.d(obj);
            }
        }
    }
}

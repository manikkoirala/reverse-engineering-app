// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.sessions;

public enum LogEnvironment implements p01
{
    private static final LogEnvironment[] $VALUES;
    
    LOG_ENVIRONMENT_AUTOPUSH(1), 
    LOG_ENVIRONMENT_PROD(3), 
    LOG_ENVIRONMENT_STAGING(2), 
    LOG_ENVIRONMENT_UNKNOWN(0);
    
    private final int number;
    
    private static final /* synthetic */ LogEnvironment[] $values() {
        return new LogEnvironment[] { LogEnvironment.LOG_ENVIRONMENT_UNKNOWN, LogEnvironment.LOG_ENVIRONMENT_AUTOPUSH, LogEnvironment.LOG_ENVIRONMENT_STAGING, LogEnvironment.LOG_ENVIRONMENT_PROD };
    }
    
    static {
        $VALUES = $values();
    }
    
    private LogEnvironment(final int number) {
        this.number = number;
    }
    
    @Override
    public int getNumber() {
        return this.number;
    }
}

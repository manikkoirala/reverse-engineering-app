// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.sessions;

import com.google.android.gms.tasks.Task;
import kotlinx.coroutines.tasks.TasksKt;
import android.util.Log;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.f;
import kotlin.coroutines.CoroutineContext;
import com.google.firebase.sessions.settings.SessionsSettings;

public final class SessionFirelogPublisherImpl implements zl1
{
    public static final a g;
    public static final double h;
    public final r10 b;
    public final r20 c;
    public final SessionsSettings d;
    public final px e;
    public final CoroutineContext f;
    
    static {
        g = new a(null);
        h = Math.random();
    }
    
    public SessionFirelogPublisherImpl(final r10 b, final r20 c, final SessionsSettings d, final px e, final CoroutineContext f) {
        fg0.e((Object)b, "firebaseApp");
        fg0.e((Object)c, "firebaseInstallations");
        fg0.e((Object)d, "sessionSettings");
        fg0.e((Object)e, "eventGDTLogger");
        fg0.e((Object)f, "backgroundDispatcher");
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
    }
    
    @Override
    public void a(final wl1 wl1) {
        fg0.e((Object)wl1, "sessionDetails");
        ad.d(kotlinx.coroutines.f.a(this.f), (CoroutineContext)null, (CoroutineStart)null, (q90)new SessionFirelogPublisherImpl$logSession.SessionFirelogPublisherImpl$logSession$1(this, wl1, (vl)null), 3, (Object)null);
    }
    
    public final void g(final xl1 xl1) {
        try {
            this.e.a(xl1);
            final StringBuilder sb = new StringBuilder();
            sb.append("Successfully logged Session Start event: ");
            sb.append(xl1.c().e());
            Log.d("SessionFirelogPublisher", sb.toString());
        }
        catch (final RuntimeException ex) {
            Log.e("SessionFirelogPublisher", "Error logging Session Start event to DataTransport: ", (Throwable)ex);
        }
    }
    
    public final Object h(final vl vl) {
        Object o = null;
        Label_0047: {
            if (vl instanceof SessionFirelogPublisherImpl$getFirebaseInstallationId.SessionFirelogPublisherImpl$getFirebaseInstallationId$1) {
                final SessionFirelogPublisherImpl$getFirebaseInstallationId.SessionFirelogPublisherImpl$getFirebaseInstallationId$1 sessionFirelogPublisherImpl$getFirebaseInstallationId$1 = (SessionFirelogPublisherImpl$getFirebaseInstallationId.SessionFirelogPublisherImpl$getFirebaseInstallationId$1)vl;
                final int label = sessionFirelogPublisherImpl$getFirebaseInstallationId$1.label;
                if ((label & Integer.MIN_VALUE) != 0x0) {
                    sessionFirelogPublisherImpl$getFirebaseInstallationId$1.label = label + Integer.MIN_VALUE;
                    o = sessionFirelogPublisherImpl$getFirebaseInstallationId$1;
                    break Label_0047;
                }
            }
            o = new SessionFirelogPublisherImpl$getFirebaseInstallationId.SessionFirelogPublisherImpl$getFirebaseInstallationId$1(this, vl);
        }
        final Object result = ((SessionFirelogPublisherImpl$getFirebaseInstallationId.SessionFirelogPublisherImpl$getFirebaseInstallationId$1)o).result;
        final Object d = gg0.d();
        final int label2 = ((SessionFirelogPublisherImpl$getFirebaseInstallationId.SessionFirelogPublisherImpl$getFirebaseInstallationId$1)o).label;
        if (label2 != 0) {
            if (label2 == 1) {
                Label_0144: {
                    try {
                        xe1.b(result);
                        return result;
                    }
                    catch (final Exception ex) {
                        break Label_0144;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                final Exception ex;
                Log.e("SessionFirelogPublisher", "Error getting Firebase Installation ID. Using an empty ID", (Throwable)ex);
                return "";
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        xe1.b(result);
        final Task id = this.c.getId();
        fg0.d((Object)id, "firebaseInstallations.id");
        ((SessionFirelogPublisherImpl$getFirebaseInstallationId.SessionFirelogPublisherImpl$getFirebaseInstallationId$1)o).label = 1;
        Object a;
        if ((a = TasksKt.a(id, (vl)o)) == d) {
            return d;
        }
        return (String)a;
    }
    
    public final boolean i() {
        return SessionFirelogPublisherImpl.h <= this.d.b();
    }
    
    public final Object j(final vl vl) {
        Object o = null;
        Label_0047: {
            if (vl instanceof SessionFirelogPublisherImpl$shouldLogSession.SessionFirelogPublisherImpl$shouldLogSession$1) {
                final SessionFirelogPublisherImpl$shouldLogSession.SessionFirelogPublisherImpl$shouldLogSession$1 sessionFirelogPublisherImpl$shouldLogSession$1 = (SessionFirelogPublisherImpl$shouldLogSession.SessionFirelogPublisherImpl$shouldLogSession$1)vl;
                final int label = sessionFirelogPublisherImpl$shouldLogSession$1.label;
                if ((label & Integer.MIN_VALUE) != 0x0) {
                    sessionFirelogPublisherImpl$shouldLogSession$1.label = label + Integer.MIN_VALUE;
                    o = sessionFirelogPublisherImpl$shouldLogSession$1;
                    break Label_0047;
                }
            }
            o = new SessionFirelogPublisherImpl$shouldLogSession.SessionFirelogPublisherImpl$shouldLogSession$1(this, vl);
        }
        final Object result = ((SessionFirelogPublisherImpl$shouldLogSession.SessionFirelogPublisherImpl$shouldLogSession$1)o).result;
        final Object d = gg0.d();
        final int label2 = ((SessionFirelogPublisherImpl$shouldLogSession.SessionFirelogPublisherImpl$shouldLogSession$1)o).label;
        SessionFirelogPublisherImpl sessionFirelogPublisherImpl;
        if (label2 != 0) {
            if (label2 != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            sessionFirelogPublisherImpl = (SessionFirelogPublisherImpl)((SessionFirelogPublisherImpl$shouldLogSession.SessionFirelogPublisherImpl$shouldLogSession$1)o).L$0;
            xe1.b(result);
        }
        else {
            xe1.b(result);
            Log.d("SessionFirelogPublisher", "Data Collection is enabled for at least one Subscriber");
            final SessionsSettings d2 = this.d;
            ((SessionFirelogPublisherImpl$shouldLogSession.SessionFirelogPublisherImpl$shouldLogSession$1)o).L$0 = this;
            ((SessionFirelogPublisherImpl$shouldLogSession.SessionFirelogPublisherImpl$shouldLogSession$1)o).label = 1;
            if (d2.g((vl)o) == d) {
                return d;
            }
            sessionFirelogPublisherImpl = this;
        }
        String s;
        if (!sessionFirelogPublisherImpl.d.d()) {
            s = "Sessions SDK disabled. Events will not be sent.";
        }
        else {
            if (sessionFirelogPublisherImpl.i()) {
                return pc.a(true);
            }
            s = "Sessions SDK has dropped this session due to sampling.";
        }
        Log.d("SessionFirelogPublisher", s);
        return pc.a(false);
    }
    
    public static final class a
    {
    }
}

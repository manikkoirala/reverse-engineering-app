// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.sessions.api;

import java.util.Iterator;
import kotlinx.coroutines.sync.MutexKt;
import android.util.Log;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public final class FirebaseSessionsDependencies
{
    public static final FirebaseSessionsDependencies a;
    public static final Map b;
    
    static {
        a = new FirebaseSessionsDependencies();
        b = Collections.synchronizedMap(new LinkedHashMap<Object, Object>());
    }
    
    public static final void e(final SessionSubscriber sessionSubscriber) {
        fg0.e((Object)sessionSubscriber, "subscriber");
        final SessionSubscriber.Name b = sessionSubscriber.b();
        final a b2 = FirebaseSessionsDependencies.a.b(b);
        if (b2.b() != null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Subscriber ");
            sb.append(b);
            sb.append(" already registered.");
            Log.d("SessionsDependencies", sb.toString());
            return;
        }
        b2.c(sessionSubscriber);
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Subscriber ");
        sb2.append(b);
        sb2.append(" registered.");
        Log.d("SessionsDependencies", sb2.toString());
        by0$a.a(b2.a(), (Object)null, 1, (Object)null);
    }
    
    public final void a(final SessionSubscriber.Name name) {
        fg0.e((Object)name, "subscriberName");
        if (name != SessionSubscriber.Name.PERFORMANCE) {
            final Map b = FirebaseSessionsDependencies.b;
            StringBuilder sb;
            String str;
            if (b.containsKey(name)) {
                sb = new StringBuilder();
                sb.append("Dependency ");
                sb.append(name);
                str = " already added.";
            }
            else {
                fg0.d((Object)b, "dependencies");
                b.put(name, new a(MutexKt.a(true), null, 2, null));
                sb = new StringBuilder();
                sb.append("Dependency to ");
                sb.append(name);
                str = " added.";
            }
            sb.append(str);
            Log.d("SessionsDependencies", sb.toString());
            return;
        }
        throw new IllegalArgumentException("Incompatible versions of Firebase Perf and Firebase Sessions.\nA safe combination would be:\n  firebase-sessions:1.1.0\n  firebase-crashlytics:18.5.0\n  firebase-perf:20.5.0\nFor more information contact Firebase Support.");
    }
    
    public final a b(final SessionSubscriber.Name obj) {
        final Map b = FirebaseSessionsDependencies.b;
        fg0.d((Object)b, "dependencies");
        final Object value = b.get(obj);
        if (value != null) {
            fg0.d(value, "dependencies.getOrElse(s\u2026load time.\"\n      )\n    }");
            return (a)value;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Cannot get dependency ");
        sb.append(obj);
        sb.append(". Dependencies should be added at class load time.");
        throw new IllegalStateException(sb.toString());
    }
    
    public final Object c(final vl vl) {
        FirebaseSessionsDependencies$getRegisteredSubscribers.FirebaseSessionsDependencies$getRegisteredSubscribers$1 firebaseSessionsDependencies$getRegisteredSubscribers$2 = null;
        Label_0047: {
            if (vl instanceof FirebaseSessionsDependencies$getRegisteredSubscribers.FirebaseSessionsDependencies$getRegisteredSubscribers$1) {
                final FirebaseSessionsDependencies$getRegisteredSubscribers.FirebaseSessionsDependencies$getRegisteredSubscribers$1 firebaseSessionsDependencies$getRegisteredSubscribers$1 = (FirebaseSessionsDependencies$getRegisteredSubscribers.FirebaseSessionsDependencies$getRegisteredSubscribers$1)vl;
                final int label = firebaseSessionsDependencies$getRegisteredSubscribers$1.label;
                if ((label & Integer.MIN_VALUE) != 0x0) {
                    firebaseSessionsDependencies$getRegisteredSubscribers$1.label = label + Integer.MIN_VALUE;
                    firebaseSessionsDependencies$getRegisteredSubscribers$2 = firebaseSessionsDependencies$getRegisteredSubscribers$1;
                    break Label_0047;
                }
            }
            firebaseSessionsDependencies$getRegisteredSubscribers$2 = new FirebaseSessionsDependencies$getRegisteredSubscribers.FirebaseSessionsDependencies$getRegisteredSubscribers$1(this, vl);
        }
        final Object result = firebaseSessionsDependencies$getRegisteredSubscribers$2.result;
        final Object d = gg0.d();
        final int label2 = firebaseSessionsDependencies$getRegisteredSubscribers$2.label;
        Object o = null;
        Label_0321: {
            Iterator iterator;
            vl vl2;
            if (label2 != 0) {
                if (label2 == 1) {
                    final Object l$5 = firebaseSessionsDependencies$getRegisteredSubscribers$2.L$5;
                    o = firebaseSessionsDependencies$getRegisteredSubscribers$2.L$4;
                    final by0 a = (by0)firebaseSessionsDependencies$getRegisteredSubscribers$2.L$3;
                    final SessionSubscriber.Name l$6 = (SessionSubscriber.Name)firebaseSessionsDependencies$getRegisteredSubscribers$2.L$2;
                    iterator = (Iterator)firebaseSessionsDependencies$getRegisteredSubscribers$2.L$1;
                    final Map map = (Map)firebaseSessionsDependencies$getRegisteredSubscribers$2.L$0;
                    xe1.b(result);
                    vl2 = (vl)firebaseSessionsDependencies$getRegisteredSubscribers$2;
                    final Object o2 = map;
                    break Label_0321;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            else {
                xe1.b(result);
                final Map b = FirebaseSessionsDependencies.b;
                fg0.d((Object)b, "dependencies");
                o = new LinkedHashMap<Object, SessionSubscriber>(en0.e(b.size()));
                iterator = b.entrySet().iterator();
                vl2 = (vl)firebaseSessionsDependencies$getRegisteredSubscribers$2;
            }
            while (true) {
                if (!iterator.hasNext()) {
                    return o;
                }
                final Map.Entry<Object, V> entry = iterator.next();
                final Object l$5 = entry.getKey();
                final SessionSubscriber.Name l$6 = entry.getKey();
                final by0 a = ((a)entry.getValue()).a();
                ((FirebaseSessionsDependencies$getRegisteredSubscribers.FirebaseSessionsDependencies$getRegisteredSubscribers$1)vl2).L$0 = o;
                ((FirebaseSessionsDependencies$getRegisteredSubscribers.FirebaseSessionsDependencies$getRegisteredSubscribers$1)vl2).L$1 = iterator;
                ((FirebaseSessionsDependencies$getRegisteredSubscribers.FirebaseSessionsDependencies$getRegisteredSubscribers$1)vl2).L$2 = l$6;
                ((FirebaseSessionsDependencies$getRegisteredSubscribers.FirebaseSessionsDependencies$getRegisteredSubscribers$1)vl2).L$3 = a;
                ((FirebaseSessionsDependencies$getRegisteredSubscribers.FirebaseSessionsDependencies$getRegisteredSubscribers$1)vl2).L$4 = o;
                ((FirebaseSessionsDependencies$getRegisteredSubscribers.FirebaseSessionsDependencies$getRegisteredSubscribers$1)vl2).L$5 = l$5;
                ((FirebaseSessionsDependencies$getRegisteredSubscribers.FirebaseSessionsDependencies$getRegisteredSubscribers$1)vl2).label = 1;
                if (a.d((Object)null, vl2) == d) {
                    return d;
                }
                final Object o2 = o;
                try {
                    final SessionSubscriber d2 = FirebaseSessionsDependencies.a.d(l$6);
                    a.c((Object)null);
                    ((Map<Object, SessionSubscriber>)o).put(l$5, d2);
                    o = o2;
                    continue;
                }
                finally {
                    a.c((Object)null);
                }
                break;
            }
        }
        return o;
    }
    
    public final SessionSubscriber d(final SessionSubscriber.Name obj) {
        fg0.e((Object)obj, "subscriberName");
        final SessionSubscriber b = this.b(obj).b();
        if (b != null) {
            return b;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Subscriber ");
        sb.append(obj);
        sb.append(" has not been registered.");
        throw new IllegalStateException(sb.toString());
    }
    
    public static final class a
    {
        public final by0 a;
        public SessionSubscriber b;
        
        public a(final by0 a, final SessionSubscriber b) {
            fg0.e((Object)a, "mutex");
            this.a = a;
            this.b = b;
        }
        
        public final by0 a() {
            return this.a;
        }
        
        public final SessionSubscriber b() {
            return this.b;
        }
        
        public final void c(final SessionSubscriber b) {
            this.b = b;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof a)) {
                return false;
            }
            final a a = (a)o;
            return fg0.a((Object)this.a, (Object)a.a) && fg0.a((Object)this.b, (Object)a.b);
        }
        
        @Override
        public int hashCode() {
            final int hashCode = this.a.hashCode();
            final SessionSubscriber b = this.b;
            int hashCode2;
            if (b == null) {
                hashCode2 = 0;
            }
            else {
                hashCode2 = b.hashCode();
            }
            return hashCode * 31 + hashCode2;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Dependency(mutex=");
            sb.append(this.a);
            sb.append(", subscriber=");
            sb.append(this.b);
            sb.append(')');
            return sb.toString();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.sessions.api;

public interface SessionSubscriber
{
    boolean a();
    
    Name b();
    
    void c(final a p0);
    
    public enum Name
    {
        private static final Name[] $VALUES;
        
        CRASHLYTICS, 
        MATT_SAYS_HI, 
        PERFORMANCE;
        
        private static final /* synthetic */ Name[] $values() {
            return new Name[] { Name.CRASHLYTICS, Name.PERFORMANCE, Name.MATT_SAYS_HI };
        }
        
        static {
            $VALUES = $values();
        }
    }
    
    public static final class a
    {
        public final String a;
        
        public a(final String a) {
            fg0.e((Object)a, "sessionId");
            this.a = a;
        }
        
        public final String a() {
            return this.a;
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || (o instanceof a && fg0.a((Object)this.a, (Object)((a)o).a));
        }
        
        @Override
        public int hashCode() {
            return this.a.hashCode();
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("SessionDetails(sessionId=");
            sb.append(this.a);
            sb.append(')');
            return sb.toString();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.sessions;

public enum DataCollectionState implements p01
{
    private static final DataCollectionState[] $VALUES;
    
    COLLECTION_DISABLED(3), 
    COLLECTION_DISABLED_REMOTE(4), 
    COLLECTION_ENABLED(2), 
    COLLECTION_SAMPLED(5), 
    COLLECTION_SDK_NOT_INSTALLED(1), 
    COLLECTION_UNKNOWN(0);
    
    private final int number;
    
    private static final /* synthetic */ DataCollectionState[] $values() {
        return new DataCollectionState[] { DataCollectionState.COLLECTION_UNKNOWN, DataCollectionState.COLLECTION_SDK_NOT_INSTALLED, DataCollectionState.COLLECTION_ENABLED, DataCollectionState.COLLECTION_DISABLED, DataCollectionState.COLLECTION_DISABLED_REMOTE, DataCollectionState.COLLECTION_SAMPLED };
    }
    
    static {
        $VALUES = $values();
    }
    
    private DataCollectionState(final int number) {
        this.number = number;
    }
    
    @Override
    public int getNumber() {
        return this.number;
    }
}

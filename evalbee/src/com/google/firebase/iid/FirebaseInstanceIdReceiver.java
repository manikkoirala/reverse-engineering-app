// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.iid;

import com.google.firebase.messaging.b;
import android.util.Log;
import java.util.concurrent.ExecutionException;
import com.google.android.gms.tasks.Tasks;
import com.google.android.gms.cloudmessaging.CloudMessage;
import android.content.Intent;
import android.os.Bundle;
import android.content.Context;
import com.google.android.gms.cloudmessaging.CloudMessagingReceiver;

public final class FirebaseInstanceIdReceiver extends CloudMessagingReceiver
{
    public static Intent a(final Context context, final String s, final Bundle bundle) {
        return new Intent(s).putExtras(bundle);
    }
    
    @Override
    public int onMessageReceive(final Context ex, final CloudMessage cloudMessage) {
        try {
            return (int)Tasks.await(new i00((Context)ex).k(cloudMessage.getIntent()));
        }
        catch (final InterruptedException ex) {}
        catch (final ExecutionException ex2) {}
        Log.e("FirebaseMessaging", "Failed to send message to service.", (Throwable)ex);
        return 500;
    }
    
    @Override
    public void onNotificationDismissed(final Context context, final Bundle bundle) {
        final Intent a = a(context, "com.google.firebase.messaging.NOTIFICATION_DISMISS", bundle);
        if (b.A(a)) {
            b.s(a);
        }
    }
}

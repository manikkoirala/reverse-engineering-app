// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.provider;

import android.database.Cursor;
import android.util.Log;
import android.content.ContentValues;
import android.net.Uri;
import android.content.Context;
import com.google.android.gms.common.internal.Preconditions;
import android.content.pm.ProviderInfo;
import java.util.concurrent.atomic.AtomicBoolean;
import android.content.ContentProvider;

public class FirebaseInitProvider extends ContentProvider
{
    public static tp1 a;
    public static AtomicBoolean b;
    
    static {
        FirebaseInitProvider.a = tp1.e();
        FirebaseInitProvider.b = new AtomicBoolean(false);
    }
    
    public static void a(final ProviderInfo providerInfo) {
        Preconditions.checkNotNull(providerInfo, "FirebaseInitProvider ProviderInfo cannot be null.");
        if (!"com.google.firebase.firebaseinitprovider".equals(providerInfo.authority)) {
            return;
        }
        throw new IllegalStateException("Incorrect provider authority in manifest. Most likely due to a missing applicationId variable in application's build.gradle.");
    }
    
    public static tp1 b() {
        return FirebaseInitProvider.a;
    }
    
    public static boolean c() {
        return FirebaseInitProvider.b.get();
    }
    
    public void attachInfo(final Context context, final ProviderInfo providerInfo) {
        a(providerInfo);
        super.attachInfo(context, providerInfo);
    }
    
    public int delete(final Uri uri, final String s, final String[] array) {
        return 0;
    }
    
    public String getType(final Uri uri) {
        return null;
    }
    
    public Uri insert(final Uri uri, final ContentValues contentValues) {
        return null;
    }
    
    public boolean onCreate() {
        try {
            FirebaseInitProvider.b.set(true);
            String s;
            if (r10.s(this.getContext()) == null) {
                s = "FirebaseApp initialization unsuccessful";
            }
            else {
                s = "FirebaseApp initialization successful";
            }
            Log.i("FirebaseInitProvider", s);
            return false;
        }
        finally {
            FirebaseInitProvider.b.set(false);
        }
    }
    
    public Cursor query(final Uri uri, final String[] array, final String s, final String[] array2, final String s2) {
        return null;
    }
    
    public int update(final Uri uri, final ContentValues contentValues, final String s, final String[] array) {
        return 0;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.auth;

import com.google.android.gms.tasks.Tasks;
import com.google.android.gms.internal.firebase_auth_api.zzacf;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.Task;
import android.text.TextUtils;
import androidx.annotation.Keep;
import android.util.Log;
import java.util.concurrent.ScheduledExecutorService;
import com.google.android.gms.internal.firebase-auth-api.zzafn;
import com.google.android.gms.common.internal.Preconditions;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import com.google.android.recaptcha.RecaptchaAction;
import com.google.android.gms.internal.firebase-auth-api.zzaai;
import java.util.List;

public class FirebaseAuth implements zf0
{
    public r10 a;
    public final List b;
    public final List c;
    public List d;
    public zzaai e;
    public r30 f;
    public pa2 g;
    public final Object h;
    public String i;
    public final Object j;
    public String k;
    public ud2 l;
    public final RecaptchaAction m;
    public final RecaptchaAction n;
    public final RecaptchaAction o;
    public final be2 p;
    public final ne2 q;
    public final nc2 r;
    public final r91 s;
    public final r91 t;
    public zd2 u;
    public final Executor v;
    public final Executor w;
    public final Executor x;
    public String y;
    
    public FirebaseAuth(final r10 r10, final zzaai zzaai, final be2 be2, final ne2 ne2, final nc2 nc2, final r91 s, final r91 t, final Executor executor, final Executor v, final Executor w, final Executor x) {
        this.b = new CopyOnWriteArrayList();
        this.c = new CopyOnWriteArrayList();
        this.d = new CopyOnWriteArrayList();
        this.h = new Object();
        this.j = new Object();
        this.m = RecaptchaAction.custom("getOobCode");
        this.n = RecaptchaAction.custom("signInWithPassword");
        this.o = RecaptchaAction.custom("signUpPassword");
        this.a = Preconditions.checkNotNull(r10);
        this.e = (zzaai)Preconditions.checkNotNull((com.google.android.gms.internal.firebase_auth_api.zzaai)zzaai);
        final be2 p11 = Preconditions.checkNotNull(be2);
        this.p = p11;
        this.g = new pa2();
        final ne2 q = Preconditions.checkNotNull(ne2);
        this.q = q;
        this.r = Preconditions.checkNotNull(nc2);
        this.s = s;
        this.t = t;
        this.v = v;
        this.w = w;
        this.x = x;
        final r30 a = p11.a();
        this.f = a;
        if (a != null) {
            final zzafn b = p11.b(a);
            if (b != null) {
                y(this, this.f, b, false, false);
            }
        }
        q.b(this);
    }
    
    public FirebaseAuth(final r10 r10, final r91 r11, final r91 r12, final Executor executor, final Executor executor2, final Executor executor3, final ScheduledExecutorService scheduledExecutorService, final Executor executor4) {
        this(r10, (zzaai)new com.google.android.gms.internal.firebase_auth_api.zzaai(r10, executor2, scheduledExecutorService), new be2(r10.l(), r10.q()), ne2.c(), nc2.a(), r11, r12, executor, executor2, executor3, executor4);
    }
    
    public static void B(final FirebaseAuth firebaseAuth, final r30 r30) {
        String string;
        if (r30 != null) {
            final String o = r30.O();
            final StringBuilder sb = new StringBuilder("Notifying id token listeners about user ( ");
            sb.append(o);
            sb.append(" ).");
            string = sb.toString();
        }
        else {
            string = "Notifying id token listeners about a sign-out event.";
        }
        Log.d("FirebaseAuth", string);
        String zzd;
        if (r30 != null) {
            zzd = r30.zzd();
        }
        else {
            zzd = null;
        }
        firebaseAuth.x.execute(new uj2(firebaseAuth, new cg0(zzd)));
    }
    
    public static zd2 O(final FirebaseAuth firebaseAuth) {
        if (firebaseAuth.u == null) {
            firebaseAuth.u = new zd2(Preconditions.checkNotNull(firebaseAuth.a));
        }
        return firebaseAuth.u;
    }
    
    @Keep
    public static FirebaseAuth getInstance() {
        return (FirebaseAuth)r10.m().j(FirebaseAuth.class);
    }
    
    @Keep
    public static FirebaseAuth getInstance(final r10 r10) {
        return (FirebaseAuth)r10.j(FirebaseAuth.class);
    }
    
    public static void x(final FirebaseAuth firebaseAuth, final r30 r30) {
        String string;
        if (r30 != null) {
            final String o = r30.O();
            final StringBuilder sb = new StringBuilder("Notifying auth state listeners about user ( ");
            sb.append(o);
            sb.append(" ).");
            string = sb.toString();
        }
        else {
            string = "Notifying auth state listeners about a sign-out event.";
        }
        Log.d("FirebaseAuth", string);
        firebaseAuth.x.execute(new jk2(firebaseAuth));
    }
    
    public static void y(final FirebaseAuth firebaseAuth, r30 f, final zzafn zzafn, final boolean b, final boolean b2) {
        Preconditions.checkNotNull(f);
        Preconditions.checkNotNull(zzafn);
        final r30 f2 = firebaseAuth.f;
        boolean b3 = false;
        boolean b4 = true;
        final boolean b5 = f2 != null && f.O().equals(firebaseAuth.f.O());
        if (!b5 && b2) {
            return;
        }
        final r30 f3 = firebaseAuth.f;
        if (f3 == null) {
            b3 = true;
        }
        else {
            final boolean equals = ((com.google.android.gms.internal.firebase_auth_api.zzafn)f3.r0()).zzc().equals(((com.google.android.gms.internal.firebase_auth_api.zzafn)zzafn).zzc());
            b4 = (!b5 || (equals ^ true));
            if (!b5) {
                b3 = true;
            }
        }
        Preconditions.checkNotNull(f);
        if (firebaseAuth.f != null && f.O().equals(firebaseAuth.a())) {
            firebaseAuth.f.m0(f.J());
            if (!f.R()) {
                firebaseAuth.f.p0();
            }
            firebaseAuth.f.q0(f.H().a());
        }
        else {
            firebaseAuth.f = f;
        }
        if (b) {
            firebaseAuth.p.f(firebaseAuth.f);
        }
        if (b4) {
            final r30 f4 = firebaseAuth.f;
            if (f4 != null) {
                f4.o0(zzafn);
            }
            B(firebaseAuth, firebaseAuth.f);
        }
        if (b3) {
            x(firebaseAuth, firebaseAuth.f);
        }
        if (b) {
            firebaseAuth.p.d(f, zzafn);
        }
        f = firebaseAuth.f;
        if (f != null) {
            O(firebaseAuth).d(f.r0());
        }
    }
    
    public final ud2 A() {
        synchronized (this) {
            return this.l;
        }
    }
    
    public final boolean C(final String s) {
        final a2 c = a2.c(s);
        return c != null && !TextUtils.equals((CharSequence)this.k, (CharSequence)c.d());
    }
    
    public final r91 D() {
        return this.s;
    }
    
    public final Task F(final r30 r30, v9 h) {
        Preconditions.checkNotNull(r30);
        Preconditions.checkNotNull(h);
        h = h.H();
        if (h instanceof fw) {
            final fw fw = (fw)h;
            if ("password".equals(fw.E())) {
                return this.u(fw.zzc(), Preconditions.checkNotEmpty(fw.zzd()), r30.K(), r30, true);
            }
            if (this.C(Preconditions.checkNotEmpty(fw.zze()))) {
                return Tasks.forException((Exception)zzacf.zza(new Status(17072)));
            }
            return this.p(fw, r30, true);
        }
        else {
            if (h instanceof e51) {
                return ((com.google.android.gms.internal.firebase_auth_api.zzaai)this.e).zzb(this.a, r30, (e51)h, this.k, (le2)new b());
            }
            return ((com.google.android.gms.internal.firebase_auth_api.zzaai)this.e).zzc(this.a, r30, h, r30.K(), (le2)new b());
        }
    }
    
    public final r91 G() {
        return this.t;
    }
    
    public final Executor H() {
        return this.v;
    }
    
    public final void L() {
        Preconditions.checkNotNull(this.p);
        final r30 f = this.f;
        if (f != null) {
            final be2 p = this.p;
            Preconditions.checkNotNull(f);
            p.e(String.format("com.google.firebase.auth.GET_TOKEN_RESPONSE.%s", f.O()));
            this.f = null;
        }
        this.p.e("com.google.firebase.auth.FIREBASE_USER");
        B(this, null);
        x(this, null);
    }
    
    public final zd2 N() {
        synchronized (this) {
            return O(this);
        }
    }
    
    @Override
    public String a() {
        final r30 f = this.f;
        if (f == null) {
            return null;
        }
        return f.O();
    }
    
    @Override
    public void b(final ee0 ee0) {
        Preconditions.checkNotNull(ee0);
        this.c.add(ee0);
        this.N().c(this.c.size());
    }
    
    @Override
    public Task c(final boolean b) {
        return this.s(this.f, b);
    }
    
    public r10 d() {
        return this.a;
    }
    
    public r30 e() {
        return this.f;
    }
    
    public String f() {
        return this.y;
    }
    
    public String g() {
        synchronized (this.h) {
            return this.i;
        }
    }
    
    public String h() {
        synchronized (this.j) {
            return this.k;
        }
    }
    
    public boolean i(final String s) {
        return fw.K(s);
    }
    
    public Task j(final String s, final z1 z1) {
        Preconditions.checkNotEmpty(s);
        Preconditions.checkNotNull(z1);
        if (z1.i()) {
            final String i = this.i;
            if (i != null) {
                z1.Z(i);
            }
            return new qi2(this, s, z1).b(this, this.k, this.m, "EMAIL_PASSWORD_PROVIDER");
        }
        throw new IllegalArgumentException("You must set canHandleCodeInApp in your ActionCodeSettings to true for Email-Link Sign-in.");
    }
    
    public void k(final String k) {
        Preconditions.checkNotEmpty(k);
        synchronized (this.j) {
            this.k = k;
        }
    }
    
    public Task l(v9 h) {
        Preconditions.checkNotNull(h);
        h = h.H();
        if (h instanceof fw) {
            final fw fw = (fw)h;
            if (!fw.zzf()) {
                return this.u(fw.zzc(), Preconditions.checkNotNull(fw.zzd()), this.k, null, false);
            }
            if (this.C(Preconditions.checkNotEmpty(fw.zze()))) {
                return Tasks.forException((Exception)zzacf.zza(new Status(17072)));
            }
            return this.p(fw, null, false);
        }
        else {
            if (h instanceof e51) {
                return ((com.google.android.gms.internal.firebase_auth_api.zzaai)this.e).zza(this.a, (e51)h, this.k, (wf2)new a());
            }
            return ((com.google.android.gms.internal.firebase_auth_api.zzaai)this.e).zza(this.a, h, this.k, (wf2)new a());
        }
    }
    
    public Task m(final String s, final String s2) {
        return this.l(gw.a(s, s2));
    }
    
    public void n() {
        this.L();
        final zd2 u = this.u;
        if (u != null) {
            u.b();
        }
    }
    
    public final Task p(final fw fw, final r30 r30, final boolean b) {
        return new com.google.firebase.auth.b(this, b, r30, fw).b(this, this.k, this.m, "EMAIL_PASSWORD_PROVIDER");
    }
    
    public final Task q(final r30 r30, final v9 v9) {
        Preconditions.checkNotNull(v9);
        Preconditions.checkNotNull(r30);
        if (v9 instanceof fw) {
            return new c(this, r30, (fw)v9.H()).b(this, r30.K(), this.o, "EMAIL_PASSWORD_PROVIDER");
        }
        return ((com.google.android.gms.internal.firebase_auth_api.zzaai)this.e).zza(this.a, r30, v9.H(), (String)null, (le2)new b());
    }
    
    public final Task r(final r30 r30, final g22 g22) {
        Preconditions.checkNotNull(r30);
        Preconditions.checkNotNull(g22);
        return ((com.google.android.gms.internal.firebase_auth_api.zzaai)this.e).zza(this.a, r30, g22, (le2)new b());
    }
    
    public final Task s(final r30 r30, final boolean b) {
        if (r30 == null) {
            return Tasks.forException((Exception)zzacf.zza(new Status(17495)));
        }
        final zzafn r31 = r30.r0();
        if (((com.google.android.gms.internal.firebase_auth_api.zzafn)r31).zzg() && !b) {
            return Tasks.forResult((Object)sc2.a(((com.google.android.gms.internal.firebase_auth_api.zzafn)r31).zzc()));
        }
        return ((com.google.android.gms.internal.firebase_auth_api.zzaai)this.e).zza(this.a, r30, ((com.google.android.gms.internal.firebase_auth_api.zzafn)r31).zzd(), (le2)new ik2(this));
    }
    
    public final Task t(final String s) {
        return ((com.google.android.gms.internal.firebase_auth_api.zzaai)this.e).zza(this.k, s);
    }
    
    public final Task u(final String s, final String s2, final String s3, final r30 r30, final boolean b) {
        return new com.google.firebase.auth.a(this, s, b, r30, s2, s3).b(this, s3, this.n, "EMAIL_PASSWORD_PROVIDER");
    }
    
    public final void v(final r30 r30, final zzafn zzafn, final boolean b) {
        this.w(r30, zzafn, true, false);
    }
    
    public final void w(final r30 r30, final zzafn zzafn, final boolean b, final boolean b2) {
        y(this, r30, zzafn, true, b2);
    }
    
    public final void z(final ud2 l) {
        synchronized (this) {
            this.l = l;
        }
    }
    
    public class a implements wf2
    {
        public final FirebaseAuth a;
        
        public a(final FirebaseAuth a) {
            this.a = a;
        }
        
        @Override
        public final void a(final zzafn zzafn, final r30 r30) {
            Preconditions.checkNotNull(zzafn);
            Preconditions.checkNotNull(r30);
            r30.o0(zzafn);
            this.a.v(r30, zzafn, true);
        }
    }
    
    public final class b implements zb2, wf2
    {
        public final FirebaseAuth a;
        
        public b(final FirebaseAuth a) {
            this.a = a;
        }
        
        @Override
        public final void a(final zzafn zzafn, final r30 r30) {
            Preconditions.checkNotNull(zzafn);
            Preconditions.checkNotNull(r30);
            r30.o0(zzafn);
            this.a.w(r30, zzafn, true, true);
        }
        
        @Override
        public final void zza(final Status status) {
            if (status.getStatusCode() == 17011 || status.getStatusCode() == 17021 || status.getStatusCode() == 17005 || status.getStatusCode() == 17091) {
                this.a.n();
            }
        }
    }
}

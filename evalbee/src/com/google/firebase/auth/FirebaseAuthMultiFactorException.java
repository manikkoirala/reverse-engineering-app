// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.auth;

public class FirebaseAuthMultiFactorException extends FirebaseAuthException
{
    private hx0 zza;
    
    public FirebaseAuthMultiFactorException(final String s, final String s2, final hx0 zza) {
        super(s, s2);
        this.zza = zza;
    }
    
    public hx0 getResolver() {
        return this.zza;
    }
}

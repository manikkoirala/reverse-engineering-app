// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.auth;

import com.google.android.gms.internal.firebase_auth_api.zzaai;
import android.util.Log;
import android.text.TextUtils;
import com.google.android.gms.tasks.Task;

public final class c extends ld2
{
    public final r30 a;
    public final fw b;
    public final FirebaseAuth c;
    
    public c(final FirebaseAuth c, final r30 a, final fw b) {
        this.c = c;
        this.a = a;
        this.b = b;
    }
    
    @Override
    public final Task c(final String s) {
        String s2;
        if (TextUtils.isEmpty((CharSequence)s)) {
            s2 = "Linking email account with empty reCAPTCHA token";
        }
        else {
            s2 = "Got reCAPTCHA token for linking email account";
        }
        Log.i("FirebaseAuth", s2);
        return ((zzaai)FirebaseAuth.E(this.c)).zza(FirebaseAuth.o(this.c), this.a, (v9)this.b, s, (le2)this.c.new b());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.auth.internal;

import android.os.AsyncTask;
import android.os.BaseBundle;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Tasks;
import com.google.android.gms.tasks.Continuation;
import java.io.IOException;
import com.google.android.gms.internal.firebase_auth_api.zzb;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager$NameNotFoundException;
import com.google.android.gms.internal.firebase_auth_api.zzacj;
import com.google.android.gms.internal.firebase_auth_api.zzaci;
import com.google.android.gms.internal.firebase_auth_api.zzaed;
import java.util.Locale;
import com.google.android.gms.common.util.Hex;
import com.google.android.gms.common.util.AndroidUtilsLight;
import com.google.android.gms.common.util.DefaultClock;
import android.os.Bundle;
import android.content.Context;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.firebase_auth_api.zzacw;
import android.text.TextUtils;
import com.google.firebase.auth.FirebaseAuth;
import java.util.UUID;
import android.content.Intent;
import android.net.Uri$Builder;
import android.util.Log;
import com.google.android.gms.tasks.Task;
import android.net.Uri;
import com.google.android.gms.internal.firebase_auth_api.zzack;
import androidx.fragment.app.e;

public class RecaptchaActivity extends e implements zzack
{
    public static final String b = "RecaptchaActivity";
    public static long c;
    public static final ne2 d;
    public boolean a;
    
    static {
        d = ne2.c();
    }
    
    public RecaptchaActivity() {
        this.a = false;
    }
    
    public final Uri$Builder m(final Uri$Builder uri$Builder, final Intent intent, final String s, final String s2) {
        final String stringExtra = intent.getStringExtra("com.google.firebase.auth.KEY_API_KEY");
        final String string = UUID.randomUUID().toString();
        final String stringExtra2 = intent.getStringExtra("com.google.firebase.auth.internal.CLIENT_VERSION");
        final String stringExtra3 = intent.getStringExtra("com.google.firebase.auth.internal.FIREBASE_APP_NAME");
        final r10 n = r10.n(stringExtra3);
        final FirebaseAuth instance = FirebaseAuth.getInstance(n);
        hg2.c().d(((Context)this).getApplicationContext(), s, string, "com.google.firebase.auth.internal.ACTION_SHOW_RECAPTCHA", stringExtra3);
        final String a = oi2.c(((Context)this).getApplicationContext(), n.q()).a();
        if (TextUtils.isEmpty((CharSequence)a)) {
            Log.e(RecaptchaActivity.b, "Could not generate an encryption key for reCAPTCHA - cancelling flow.");
            this.o(ob2.a("Failed to generate/retrieve public encryption key for reCAPTCHA flow."));
            return null;
        }
        String s3;
        if (!TextUtils.isEmpty((CharSequence)instance.g())) {
            s3 = instance.g();
        }
        else {
            s3 = zzacw.zza();
        }
        final Uri$Builder appendQueryParameter = uri$Builder.appendQueryParameter("apiKey", stringExtra).appendQueryParameter("authType", "verifyApp").appendQueryParameter("apn", s).appendQueryParameter("hl", s3).appendQueryParameter("eventId", string);
        final StringBuilder sb = new StringBuilder("X");
        sb.append(stringExtra2);
        appendQueryParameter.appendQueryParameter("v", sb.toString()).appendQueryParameter("eid", "p").appendQueryParameter("appName", stringExtra3).appendQueryParameter("sha1Cert", s2).appendQueryParameter("publicKey", a);
        return uri$Builder;
    }
    
    public final void o(final Status status) {
        RecaptchaActivity.c = 0L;
        this.a = false;
        final Intent intent = new Intent();
        je2.b(intent, status);
        intent.setAction("com.google.firebase.auth.ACTION_RECEIVE_FIREBASE_AUTH_INTENT");
        this.q(intent);
        RecaptchaActivity.d.a((Context)this);
        this.finish();
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final String action = this.getIntent().getAction();
        if (!"com.google.firebase.auth.internal.ACTION_SHOW_RECAPTCHA".equals(action) && !"android.intent.action.VIEW".equals(action)) {
            final String b = RecaptchaActivity.b;
            final StringBuilder sb = new StringBuilder("Could not do operation - unknown action: ");
            sb.append(action);
            Log.e(b, sb.toString());
            this.r();
            return;
        }
        final long currentTimeMillis = DefaultClock.getInstance().currentTimeMillis();
        if (currentTimeMillis - RecaptchaActivity.c < 30000L) {
            Log.e(RecaptchaActivity.b, "Could not start operation - already in progress");
            return;
        }
        RecaptchaActivity.c = currentTimeMillis;
        if (bundle != null) {
            this.a = ((BaseBundle)bundle).getBoolean("com.google.firebase.auth.internal.KEY_ALREADY_STARTED_RECAPTCHA_FLOW");
        }
    }
    
    @Override
    public void onNewIntent(final Intent intent) {
        super.onNewIntent(intent);
        this.setIntent(intent);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        if ("android.intent.action.VIEW".equals(this.getIntent().getAction())) {
            final Intent intent = this.getIntent();
            if (intent.hasExtra("firebaseError")) {
                this.o(je2.a(intent.getStringExtra("firebaseError")));
                return;
            }
            if (intent.hasExtra("link") && intent.hasExtra("eventId")) {
                final String stringExtra = intent.getStringExtra("link");
                final String g = hg2.c().g(((Context)this).getApplicationContext(), ((Context)this).getPackageName(), intent.getStringExtra("eventId"));
                if (TextUtils.isEmpty((CharSequence)g)) {
                    Log.e(RecaptchaActivity.b, "Failed to find registration for this event - failing to prevent session injection.");
                    this.o(ob2.a("Failed to find registration for this reCAPTCHA event"));
                }
                String b = stringExtra;
                if (intent.getBooleanExtra("encryptionEnabled", true)) {
                    b = oi2.c(((Context)this).getApplicationContext(), r10.n(g).q()).b(stringExtra);
                }
                final String queryParameter = Uri.parse(b).getQueryParameter("recaptchaToken");
                RecaptchaActivity.c = 0L;
                this.a = false;
                final Intent intent2 = new Intent();
                intent2.putExtra("com.google.firebase.auth.internal.RECAPTCHA_TOKEN", queryParameter);
                intent2.putExtra("com.google.firebase.auth.internal.OPERATION", "com.google.firebase.auth.internal.ACTION_SHOW_RECAPTCHA");
                intent2.setAction("com.google.firebase.auth.ACTION_RECEIVE_FIREBASE_AUTH_INTENT");
                if (!this.q(intent2)) {
                    ed2.d(((Context)this).getApplicationContext(), queryParameter, "com.google.firebase.auth.internal.ACTION_SHOW_RECAPTCHA");
                }
                else {
                    RecaptchaActivity.d.a((Context)this);
                }
                this.finish();
                return;
            }
            this.r();
        }
        else {
            if (!this.a) {
                final Intent intent3 = this.getIntent();
                final String packageName = ((Context)this).getPackageName();
                try {
                    final String lowerCase = Hex.bytesToStringUppercase(AndroidUtilsLight.getPackageCertificateHashBytes((Context)this, packageName)).toLowerCase(Locale.US);
                    final r10 n = r10.n(intent3.getStringExtra("com.google.firebase.auth.internal.FIREBASE_APP_NAME"));
                    final FirebaseAuth instance = FirebaseAuth.getInstance(n);
                    if (!zzaed.zza(n)) {
                        ((AsyncTask)new zzaci(packageName, lowerCase, intent3, n, (com.google.android.gms.internal.firebase-auth-api.zzack)this)).executeOnExecutor(instance.H(), (Object[])new Void[0]);
                    }
                    else {
                        this.zza(this.m(Uri.parse(zzaed.zza(n.p().b())).buildUpon(), this.getIntent(), packageName, lowerCase).build(), packageName, instance.D());
                    }
                }
                catch (final PackageManager$NameNotFoundException obj) {
                    final String b2 = RecaptchaActivity.b;
                    final String value = String.valueOf(obj);
                    final StringBuilder sb = new StringBuilder("Could not get package signature: ");
                    sb.append(packageName);
                    sb.append(" ");
                    sb.append(value);
                    Log.e(b2, sb.toString());
                    zzacj.zzb((com.google.android.gms.internal.firebase-auth-api.zzack)this, packageName);
                }
                this.a = true;
                return;
            }
            this.r();
        }
    }
    
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        ((BaseBundle)bundle).putBoolean("com.google.firebase.auth.internal.KEY_ALREADY_STARTED_RECAPTCHA_FLOW", this.a);
    }
    
    public final boolean q(final Intent intent) {
        return uk0.b((Context)this).c(intent);
    }
    
    public final void r() {
        RecaptchaActivity.c = 0L;
        this.a = false;
        final Intent intent = new Intent();
        intent.putExtra("com.google.firebase.auth.internal.EXTRA_CANCELED", true);
        intent.setAction("com.google.firebase.auth.ACTION_RECEIVE_FIREBASE_AUTH_INTENT");
        this.q(intent);
        RecaptchaActivity.d.a((Context)this);
        this.finish();
    }
    
    public final Context zza() {
        return ((Context)this).getApplicationContext();
    }
    
    public final Uri$Builder zza(final Intent intent, final String s, final String s2) {
        return this.m(new Uri$Builder().scheme("https").appendPath("__").appendPath("auth").appendPath("handler"), intent, s, s2);
    }
    
    public final String zza(final String s) {
        return zzaed.zzb(s);
    }
    
    public final HttpURLConnection zza(final URL url) {
        try {
            return (HttpURLConnection)((zzb)zzb.zza()).zza(url, "client-firebase-auth-api");
        }
        catch (final IOException ex) {
            zzack.zza.e("Error generating connection", new Object[0]);
            return null;
        }
    }
    
    public final void zza(final Uri uri, final String s, final r91 r91) {
        final dg0 dg0 = (dg0)r91.get();
        Task task;
        if (dg0 != null) {
            task = dg0.a(false).continueWith((Continuation)new gd2(uri));
        }
        else {
            task = Tasks.forResult((Object)uri);
        }
        task.addOnCompleteListener((OnCompleteListener)new jd2(this, s));
    }
    
    public final void zza(final String s, final Status status) {
        if (status == null) {
            this.r();
            return;
        }
        this.o(status);
    }
}

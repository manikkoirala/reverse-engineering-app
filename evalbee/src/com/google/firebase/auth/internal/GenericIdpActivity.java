// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.auth.internal;

import android.os.AsyncTask;
import android.os.BaseBundle;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Tasks;
import com.google.android.gms.tasks.Continuation;
import java.io.IOException;
import com.google.android.gms.internal.firebase_auth_api.zzb;
import java.net.HttpURLConnection;
import java.net.URL;
import com.google.android.gms.common.api.Status;
import android.content.pm.PackageManager$NameNotFoundException;
import com.google.android.gms.internal.firebase_auth_api.zzaci;
import com.google.android.gms.internal.firebase_auth_api.zzaed;
import com.google.firebase.auth.FirebaseAuth;
import java.util.Locale;
import com.google.android.gms.common.util.Hex;
import com.google.android.gms.common.util.AndroidUtilsLight;
import com.google.android.gms.common.internal.safeparcel.SafeParcelableSerializer;
import com.google.android.gms.internal.firebase_auth_api.zzagt;
import android.content.Context;
import com.google.android.gms.common.util.DefaultClock;
import java.util.ArrayList;
import com.google.android.gms.internal.firebase_auth_api.zzacj;
import java.util.UUID;
import android.content.Intent;
import java.util.Iterator;
import org.json.JSONException;
import android.text.TextUtils;
import org.json.JSONObject;
import android.os.Bundle;
import android.net.Uri$Builder;
import android.util.Log;
import com.google.android.gms.tasks.Task;
import android.net.Uri;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.internal.firebase_auth_api.zzack;
import androidx.fragment.app.e;

@KeepName
public class GenericIdpActivity extends e implements zzack
{
    public static long b;
    public static final ne2 c;
    public boolean a;
    
    static {
        c = ne2.c();
    }
    
    public GenericIdpActivity() {
        this.a = false;
    }
    
    public static String o(final Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        final JSONObject jsonObject = new JSONObject();
        try {
            for (final String s : ((BaseBundle)bundle).keySet()) {
                final String string = ((BaseBundle)bundle).getString(s);
                if (!TextUtils.isEmpty((CharSequence)string)) {
                    jsonObject.put(s, (Object)string);
                }
            }
        }
        catch (final JSONException ex) {
            Log.e("GenericIdpActivity", "Unexpected JSON exception when serializing developer specified custom params");
        }
        return jsonObject.toString();
    }
    
    public final Uri$Builder m(final Uri$Builder uri$Builder, final Intent intent, final String s, final String s2) {
        final String stringExtra = intent.getStringExtra("com.google.firebase.auth.KEY_API_KEY");
        final String stringExtra2 = intent.getStringExtra("com.google.firebase.auth.KEY_PROVIDER_ID");
        final String stringExtra3 = intent.getStringExtra("com.google.firebase.auth.KEY_TENANT_ID");
        final String stringExtra4 = intent.getStringExtra("com.google.firebase.auth.KEY_FIREBASE_APP_NAME");
        final ArrayList stringArrayListExtra = intent.getStringArrayListExtra("com.google.firebase.auth.KEY_PROVIDER_SCOPES");
        String join;
        if (stringArrayListExtra != null && !stringArrayListExtra.isEmpty()) {
            join = TextUtils.join((CharSequence)",", (Iterable)stringArrayListExtra);
        }
        else {
            join = null;
        }
        final String o = o(intent.getBundleExtra("com.google.firebase.auth.KEY_PROVIDER_CUSTOM_PARAMS"));
        final String string = UUID.randomUUID().toString();
        final String zza = zzacj.zza((com.google.android.gms.internal.firebase-auth-api.zzack)this, UUID.randomUUID().toString());
        final String action = intent.getAction();
        final String stringExtra5 = intent.getStringExtra("com.google.firebase.auth.internal.CLIENT_VERSION");
        hg2.c().e(((Context)this).getApplicationContext(), s, string, zza, action, stringExtra2, stringExtra3, stringExtra4);
        final String a = oi2.c(((Context)this).getApplicationContext(), r10.n(stringExtra4).q()).a();
        if (TextUtils.isEmpty((CharSequence)a)) {
            Log.e("GenericIdpActivity", "Could not generate an encryption key for Generic IDP - cancelling flow.");
            this.p(ob2.a("Failed to generate/retrieve public encryption key for Generic IDP flow."));
            return null;
        }
        if (zza == null) {
            return null;
        }
        final Uri$Builder appendQueryParameter = uri$Builder.appendQueryParameter("eid", "p");
        final StringBuilder sb = new StringBuilder("X");
        sb.append(stringExtra5);
        appendQueryParameter.appendQueryParameter("v", sb.toString()).appendQueryParameter("authType", "signInWithRedirect").appendQueryParameter("apiKey", stringExtra).appendQueryParameter("providerId", stringExtra2).appendQueryParameter("sessionId", zza).appendQueryParameter("eventId", string).appendQueryParameter("apn", s).appendQueryParameter("sha1Cert", s2).appendQueryParameter("publicKey", a);
        if (!TextUtils.isEmpty((CharSequence)join)) {
            uri$Builder.appendQueryParameter("scopes", join);
        }
        if (!TextUtils.isEmpty((CharSequence)o)) {
            uri$Builder.appendQueryParameter("customParameters", o);
        }
        if (!TextUtils.isEmpty((CharSequence)stringExtra3)) {
            uri$Builder.appendQueryParameter("tid", stringExtra3);
        }
        return uri$Builder;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final String action = this.getIntent().getAction();
        if (!"com.google.firebase.auth.internal.NONGMSCORE_SIGN_IN".equals(action) && !"com.google.firebase.auth.internal.NONGMSCORE_LINK".equals(action) && !"com.google.firebase.auth.internal.NONGMSCORE_REAUTHENTICATE".equals(action) && !"android.intent.action.VIEW".equals(action)) {
            final StringBuilder sb = new StringBuilder("Could not do operation - unknown action: ");
            sb.append(action);
            Log.e("GenericIdpActivity", sb.toString());
            this.r();
            return;
        }
        final long currentTimeMillis = DefaultClock.getInstance().currentTimeMillis();
        if (currentTimeMillis - GenericIdpActivity.b < 30000L) {
            Log.e("GenericIdpActivity", "Could not start operation - already in progress");
            return;
        }
        GenericIdpActivity.b = currentTimeMillis;
        if (bundle != null) {
            this.a = ((BaseBundle)bundle).getBoolean("com.google.firebase.auth.internal.KEY_STARTED_SIGN_IN");
        }
    }
    
    @Override
    public void onNewIntent(final Intent intent) {
        super.onNewIntent(intent);
        this.setIntent(intent);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        if ("android.intent.action.VIEW".equals(this.getIntent().getAction())) {
            final Intent intent = this.getIntent();
            if (intent.hasExtra("firebaseError")) {
                this.p(je2.a(intent.getStringExtra("firebaseError")));
                return;
            }
            if (!intent.hasExtra("link") || !intent.hasExtra("eventId")) {
                this.r();
                return;
            }
            final String stringExtra = intent.getStringExtra("link");
            final String stringExtra2 = intent.getStringExtra("eventId");
            final String packageName = ((Context)this).getPackageName();
            final boolean booleanExtra = intent.getBooleanExtra("encryptionEnabled", true);
            final gg2 b = hg2.c().b((Context)this, packageName, stringExtra2);
            if (b == null) {
                this.r();
            }
            String b2 = stringExtra;
            if (booleanExtra) {
                b2 = oi2.c(((Context)this).getApplicationContext(), r10.n(b.a()).q()).b(stringExtra);
            }
            final zzagt zzagt = new zzagt(b, b2);
            final String e = b.e();
            final String b3 = b.b();
            zzagt.zzb(e);
            if (!"com.google.firebase.auth.internal.NONGMSCORE_SIGN_IN".equals(b3) && !"com.google.firebase.auth.internal.NONGMSCORE_LINK".equals(b3) && !"com.google.firebase.auth.internal.NONGMSCORE_REAUTHENTICATE".equals(b3)) {
                final StringBuilder sb = new StringBuilder("unsupported operation: ");
                sb.append(b3);
                Log.e("GenericIdpActivity", sb.toString());
                this.r();
                return;
            }
            GenericIdpActivity.b = 0L;
            this.a = false;
            final Intent intent2 = new Intent();
            SafeParcelableSerializer.serializeToIntentExtra(zzagt, intent2, "com.google.firebase.auth.internal.VERIFY_ASSERTION_REQUEST");
            intent2.putExtra("com.google.firebase.auth.internal.OPERATION", b3);
            intent2.setAction("com.google.firebase.auth.ACTION_RECEIVE_FIREBASE_AUTH_INTENT");
            if (!this.q(intent2)) {
                ed2.c(((Context)this).getApplicationContext(), (com.google.android.gms.internal.firebase-auth-api.zzagt)zzagt, b3, e);
            }
            else {
                GenericIdpActivity.c.a((Context)this);
            }
            this.finish();
        }
        else {
            if (!this.a) {
                final String packageName2 = ((Context)this).getPackageName();
                try {
                    final String lowerCase = Hex.bytesToStringUppercase(AndroidUtilsLight.getPackageCertificateHashBytes((Context)this, packageName2)).toLowerCase(Locale.US);
                    final r10 n = r10.n(this.getIntent().getStringExtra("com.google.firebase.auth.KEY_FIREBASE_APP_NAME"));
                    final FirebaseAuth instance = FirebaseAuth.getInstance(n);
                    if (!zzaed.zza(n)) {
                        ((AsyncTask)new zzaci(packageName2, lowerCase, this.getIntent(), n, (com.google.android.gms.internal.firebase-auth-api.zzack)this)).executeOnExecutor(instance.H(), (Object[])new Void[0]);
                    }
                    else {
                        this.zza(this.m(Uri.parse(zzaed.zza(n.p().b())).buildUpon(), this.getIntent(), packageName2, lowerCase).build(), packageName2, instance.D());
                    }
                }
                catch (final PackageManager$NameNotFoundException obj) {
                    final String value = String.valueOf(obj);
                    final StringBuilder sb2 = new StringBuilder("Could not get package signature: ");
                    sb2.append(packageName2);
                    sb2.append(" ");
                    sb2.append(value);
                    Log.e("GenericIdpActivity", sb2.toString());
                    zzacj.zzb((com.google.android.gms.internal.firebase-auth-api.zzack)this, packageName2);
                }
                this.a = true;
                return;
            }
            this.r();
        }
    }
    
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        ((BaseBundle)bundle).putBoolean("com.google.firebase.auth.internal.KEY_STARTED_SIGN_IN", this.a);
    }
    
    public final void p(final Status status) {
        GenericIdpActivity.b = 0L;
        this.a = false;
        final Intent intent = new Intent();
        je2.b(intent, status);
        intent.setAction("com.google.firebase.auth.ACTION_RECEIVE_FIREBASE_AUTH_INTENT");
        if (!this.q(intent)) {
            ed2.b(((Context)this).getApplicationContext(), status);
        }
        else {
            GenericIdpActivity.c.a((Context)this);
        }
        this.finish();
    }
    
    public final boolean q(final Intent intent) {
        return uk0.b((Context)this).c(intent);
    }
    
    public final void r() {
        GenericIdpActivity.b = 0L;
        this.a = false;
        final Intent intent = new Intent();
        intent.putExtra("com.google.firebase.auth.internal.EXTRA_CANCELED", true);
        intent.setAction("com.google.firebase.auth.ACTION_RECEIVE_FIREBASE_AUTH_INTENT");
        if (!this.q(intent)) {
            ed2.b((Context)this, ob2.a("WEB_CONTEXT_CANCELED"));
        }
        else {
            GenericIdpActivity.c.a((Context)this);
        }
        this.finish();
    }
    
    public final Context zza() {
        return ((Context)this).getApplicationContext();
    }
    
    public final Uri$Builder zza(final Intent intent, final String s, final String s2) {
        return this.m(new Uri$Builder().scheme("https").appendPath("__").appendPath("auth").appendPath("handler"), intent, s, s2);
    }
    
    public final String zza(final String s) {
        return zzaed.zzb(s);
    }
    
    public final HttpURLConnection zza(final URL url) {
        try {
            return (HttpURLConnection)((zzb)zzb.zza()).zza(url, "client-firebase-auth-api");
        }
        catch (final IOException ex) {
            Log.e("GenericIdpActivity", "Error generating URL connection");
            return null;
        }
    }
    
    public final void zza(final Uri uri, final String s, final r91 r91) {
        final dg0 dg0 = (dg0)r91.get();
        Task task;
        if (dg0 != null) {
            task = dg0.a(false).continueWith((Continuation)new uc2(uri));
        }
        else {
            task = Tasks.forResult((Object)uri);
        }
        task.addOnCompleteListener((OnCompleteListener)new qc2(this, s));
    }
    
    public final void zza(final String s, final Status status) {
        if (status == null) {
            this.r();
            return;
        }
        this.p(status);
    }
}

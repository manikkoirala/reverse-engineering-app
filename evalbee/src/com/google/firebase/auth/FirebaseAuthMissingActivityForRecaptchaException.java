// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.auth;

public class FirebaseAuthMissingActivityForRecaptchaException extends FirebaseAuthException
{
    public FirebaseAuthMissingActivityForRecaptchaException() {
        super("ERROR_MISSING_ACTIVITY", "App verification failed - a valid Activity is required to complete the Recaptcha flow");
    }
}

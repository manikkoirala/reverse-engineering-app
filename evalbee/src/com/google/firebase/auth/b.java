// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.auth;

import com.google.android.gms.internal.firebase_auth_api.zzaai;
import com.google.android.gms.common.internal.Preconditions;
import android.util.Log;
import android.text.TextUtils;
import com.google.android.gms.tasks.Task;

public final class b extends ld2
{
    public final boolean a;
    public final r30 b;
    public final fw c;
    public final FirebaseAuth d;
    
    public b(final FirebaseAuth d, final boolean a, final r30 b, final fw c) {
        this.d = d;
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    @Override
    public final Task c(final String s) {
        String s2;
        if (TextUtils.isEmpty((CharSequence)s)) {
            s2 = "Email link login/reauth with empty reCAPTCHA token";
        }
        else {
            s2 = "Got reCAPTCHA token for login/reauth with email link";
        }
        Log.i("FirebaseAuth", s2);
        if (this.a) {
            return ((zzaai)FirebaseAuth.E(this.d)).zzb(FirebaseAuth.o(this.d), (r30)Preconditions.checkNotNull(this.b), this.c, s, (le2)this.d.new b());
        }
        return ((zzaai)FirebaseAuth.E(this.d)).zza(FirebaseAuth.o(this.d), this.c, s, (wf2)this.d.new a());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.auth;

import com.google.android.gms.internal.firebase_auth_api.zzaai;
import com.google.android.gms.common.internal.Preconditions;
import android.util.Log;
import android.text.TextUtils;
import com.google.android.gms.tasks.Task;

public final class a extends ld2
{
    public final String a;
    public final boolean b;
    public final r30 c;
    public final String d;
    public final String e;
    public final FirebaseAuth f;
    
    public a(final FirebaseAuth f, final String a, final boolean b, final r30 c, final String d, final String e) {
        this.f = f;
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
    }
    
    @Override
    public final Task c(final String s) {
        StringBuilder sb;
        String a2;
        if (TextUtils.isEmpty((CharSequence)s)) {
            final String a = this.a;
            sb = new StringBuilder("Logging in as ");
            sb.append(a);
            a2 = " with empty reCAPTCHA token";
        }
        else {
            a2 = this.a;
            sb = new StringBuilder("Got reCAPTCHA token for login with email ");
        }
        sb.append(a2);
        Log.i("FirebaseAuth", sb.toString());
        if (this.b) {
            return ((zzaai)FirebaseAuth.E(this.f)).zzb(FirebaseAuth.o(this.f), (r30)Preconditions.checkNotNull(this.c), this.a, this.d, this.e, s, (le2)this.f.new b());
        }
        return ((zzaai)FirebaseAuth.E(this.f)).zzb(FirebaseAuth.o(this.f), this.a, this.d, this.e, s, (wf2)this.f.new a());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.auth;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.Executor;
import com.google.android.gms.common.annotation.KeepForSdk;
import androidx.annotation.Keep;
import com.google.firebase.components.ComponentRegistrar;

@Keep
@KeepForSdk
public class FirebaseAuthRegistrar implements ComponentRegistrar
{
    @Keep
    @Override
    public List<zi> getComponents() {
        final da1 a = da1.a(za.class, Executor.class);
        final da1 a2 = da1.a(fc.class, Executor.class);
        final da1 a3 = da1.a(sj0.class, Executor.class);
        final da1 a4 = da1.a(sj0.class, ScheduledExecutorService.class);
        final da1 a5 = da1.a(n02.class, Executor.class);
        return Arrays.asList(zi.f(FirebaseAuth.class, zf0.class).b(os.k(r10.class)).b(os.m(wc0.class)).b(os.j(a)).b(os.j(a2)).b(os.j(a3)).b(os.j(a4)).b(os.j(a5)).b(os.i(dg0.class)).f(new ya2(a, a2, a3, a4, a5)).d(), vc0.a(), mj0.b("fire-auth", "22.3.0"));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.heartbeatinfo;

public interface HeartBeatInfo
{
    HeartBeat a(final String p0);
    
    public enum HeartBeat
    {
        private static final HeartBeat[] $VALUES;
        
        COMBINED(3), 
        GLOBAL(2), 
        NONE(0), 
        SDK(1);
        
        private final int code;
        
        private HeartBeat(final int code) {
            this.code = code;
        }
        
        public int getCode() {
            return this.code;
        }
    }
}

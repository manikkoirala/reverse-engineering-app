// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.heartbeatinfo;

import java.util.concurrent.Callable;
import com.google.android.gms.tasks.Tasks;
import com.google.android.gms.tasks.Task;
import java.util.List;
import java.util.zip.GZIPOutputStream;
import java.io.OutputStream;
import android.util.Base64OutputStream;
import java.io.ByteArrayOutputStream;
import java.util.Collection;
import org.json.JSONObject;
import org.json.JSONArray;
import java.util.concurrent.Executor;
import java.util.Set;
import android.content.Context;

public class a implements wc0, HeartBeatInfo
{
    public final r91 a;
    public final Context b;
    public final r91 c;
    public final Set d;
    public final Executor e;
    
    public a(final Context context, final String s, final Set set, final r91 r91, final Executor executor) {
        this(new tq(context, s), set, executor, r91, context);
    }
    
    public a(final r91 a, final Set d, final Executor e, final r91 c, final Context b) {
        this.a = a;
        this.d = d;
        this.e = e;
        this.c = c;
        this.b = b;
    }
    
    public static zi g() {
        final da1 a = da1.a(za.class, Executor.class);
        return zi.f(a.class, wc0.class, HeartBeatInfo.class).b(os.k(Context.class)).b(os.k(r10.class)).b(os.n(uc0.class)).b(os.m(v12.class)).b(os.j(a)).f(new qq(a)).d();
    }
    
    @Override
    public HeartBeat a(final String s) {
        synchronized (this) {
            final long currentTimeMillis = System.currentTimeMillis();
            final dd0 dd0 = (dd0)this.a.get();
            if (dd0.i(currentTimeMillis)) {
                dd0.g();
                return HeartBeat.GLOBAL;
            }
            return HeartBeat.NONE;
        }
    }
    
    @Override
    public Task b() {
        if (d22.a(this.b) ^ true) {
            return Tasks.forResult((Object)"");
        }
        return Tasks.call(this.e, (Callable)new rq(this));
    }
    
    public Task l() {
        if (this.d.size() <= 0) {
            return Tasks.forResult((Object)null);
        }
        if (d22.a(this.b) ^ true) {
            return Tasks.forResult((Object)null);
        }
        return Tasks.call(this.e, (Callable)new sq(this));
    }
}

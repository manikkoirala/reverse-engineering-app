// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.encoders;

public final class EncodingException extends RuntimeException
{
    public EncodingException(final String message) {
        super(message);
    }
    
    public EncodingException(final String message, final Exception cause) {
        super(message, cause);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.encoders.proto;

public final class a
{
    public int a;
    public Protobuf.IntEncoding b;
    
    public a() {
        this.b = Protobuf.IntEncoding.DEFAULT;
    }
    
    public static a b() {
        return new a();
    }
    
    public Protobuf a() {
        return new a(this.a, this.b);
    }
    
    public a c(final int a) {
        this.a = a;
        return this;
    }
    
    public static final class a implements Protobuf
    {
        public final int a;
        public final IntEncoding b;
        
        public a(final int a, final IntEncoding b) {
            this.a = a;
            this.b = b;
        }
        
        @Override
        public Class annotationType() {
            return Protobuf.class;
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (!(o instanceof Protobuf)) {
                return false;
            }
            final Protobuf protobuf = (Protobuf)o;
            if (this.a != protobuf.tag() || !this.b.equals(protobuf.intEncoding())) {
                b = false;
            }
            return b;
        }
        
        @Override
        public int hashCode() {
            return (0xDE0D66 ^ this.a) + (this.b.hashCode() ^ 0x79AD669E);
        }
        
        @Override
        public IntEncoding intEncoding() {
            return this.b;
        }
        
        @Override
        public int tag() {
            return this.a;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("@com.google.firebase.encoders.proto.Protobuf");
            sb.append('(');
            sb.append("tag=");
            sb.append(this.a);
            sb.append("intEncoding=");
            sb.append(this.b);
            sb.append(')');
            return sb.toString();
        }
    }
}

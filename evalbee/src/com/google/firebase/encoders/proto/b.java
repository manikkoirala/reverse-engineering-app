// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.encoders.proto;

import java.util.Iterator;
import java.util.Collection;
import com.google.firebase.encoders.EncodingException;
import java.nio.ByteOrder;
import java.nio.ByteBuffer;
import java.lang.annotation.Annotation;
import java.util.Map;
import java.io.OutputStream;
import java.nio.charset.Charset;

public final class b implements x01
{
    public static final Charset f;
    public static final n00 g;
    public static final n00 h;
    public static final w01 i;
    public OutputStream a;
    public final Map b;
    public final Map c;
    public final w01 d;
    public final p91 e;
    
    static {
        f = Charset.forName("UTF-8");
        g = n00.a("key").b(a.b().c(1).a()).a();
        h = n00.a("value").b(a.b().c(2).a()).a();
        i = new m91();
    }
    
    public b(final OutputStream a, final Map b, final Map c, final w01 d) {
        this.e = new p91(this);
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
    
    public static ByteBuffer p(final int capacity) {
        return ByteBuffer.allocate(capacity).order(ByteOrder.LITTLE_ENDIAN);
    }
    
    public static Protobuf u(final n00 n00) {
        final Protobuf protobuf = (Protobuf)n00.c(Protobuf.class);
        if (protobuf != null) {
            return protobuf;
        }
        throw new EncodingException("Field has no @Protobuf config");
    }
    
    public static int v(final n00 n00) {
        final Protobuf protobuf = (Protobuf)n00.c(Protobuf.class);
        if (protobuf != null) {
            return protobuf.tag();
        }
        throw new EncodingException("Field has no @Protobuf config");
    }
    
    @Override
    public x01 d(final n00 n00, final double n2) {
        return this.g(n00, n2, true);
    }
    
    @Override
    public x01 f(final n00 n00, final Object o) {
        return this.i(n00, o, true);
    }
    
    public x01 g(final n00 n00, final double n2, final boolean b) {
        if (b && n2 == 0.0) {
            return this;
        }
        this.x(v(n00) << 3 | 0x1);
        this.a.write(p(8).putDouble(n2).array());
        return this;
    }
    
    public x01 h(final n00 n00, final float n2, final boolean b) {
        if (b && n2 == 0.0f) {
            return this;
        }
        this.x(v(n00) << 3 | 0x5);
        this.a.write(p(4).putFloat(n2).array());
        return this;
    }
    
    public x01 i(final n00 n00, final Object o, final boolean b) {
        if (o == null) {
            return this;
        }
        if (o instanceof CharSequence) {
            final CharSequence charSequence = (CharSequence)o;
            if (b && charSequence.length() == 0) {
                return this;
            }
            this.x(v(n00) << 3 | 0x2);
            final byte[] bytes = charSequence.toString().getBytes(b.f);
            this.x(bytes.length);
            this.a.write(bytes);
            return this;
        }
        else {
            if (o instanceof Collection) {
                final Iterator iterator = ((Collection)o).iterator();
                while (iterator.hasNext()) {
                    this.i(n00, iterator.next(), false);
                }
                return this;
            }
            if (o instanceof Map) {
                final Iterator iterator2 = ((Map)o).entrySet().iterator();
                while (iterator2.hasNext()) {
                    this.r(b.i, n00, iterator2.next(), false);
                }
                return this;
            }
            if (o instanceof Double) {
                return this.g(n00, (double)o, b);
            }
            if (o instanceof Float) {
                return this.h(n00, (float)o, b);
            }
            if (o instanceof Number) {
                return this.m(n00, ((Number)o).longValue(), b);
            }
            if (o instanceof Boolean) {
                return this.o(n00, (boolean)o, b);
            }
            if (o instanceof byte[]) {
                final byte[] b2 = (byte[])o;
                if (b && b2.length == 0) {
                    return this;
                }
                this.x(v(n00) << 3 | 0x2);
                this.x(b2.length);
                this.a.write(b2);
                return this;
            }
            else {
                final w01 w01 = this.b.get(o.getClass());
                if (w01 != null) {
                    return this.r(w01, n00, o, b);
                }
                final x22 x22 = this.c.get(o.getClass());
                if (x22 != null) {
                    return this.s(x22, n00, o, b);
                }
                if (o instanceof j91) {
                    return this.j(n00, ((j91)o).getNumber());
                }
                if (o instanceof Enum) {
                    return this.j(n00, ((Enum)o).ordinal());
                }
                return this.r(this.d, n00, o, b);
            }
        }
    }
    
    public b j(final n00 n00, final int n2) {
        return this.k(n00, n2, true);
    }
    
    public b k(final n00 n00, final int n2, final boolean b) {
        if (b && n2 == 0) {
            return this;
        }
        final Protobuf u = u(n00);
        final int n3 = b$a.a[u.intEncoding().ordinal()];
        if (n3 != 1) {
            if (n3 != 2) {
                if (n3 == 3) {
                    this.x(u.tag() << 3 | 0x5);
                    this.a.write(p(4).putInt(n2).array());
                }
            }
            else {
                this.x(u.tag() << 3);
                this.x(n2 << 1 ^ n2 >> 31);
            }
        }
        else {
            this.x(u.tag() << 3);
            this.x(n2);
        }
        return this;
    }
    
    public b l(final n00 n00, final long n2) {
        return this.m(n00, n2, true);
    }
    
    public b m(final n00 n00, final long n2, final boolean b) {
        if (b && n2 == 0L) {
            return this;
        }
        final Protobuf u = u(n00);
        final int n3 = b$a.a[u.intEncoding().ordinal()];
        if (n3 != 1) {
            if (n3 != 2) {
                if (n3 == 3) {
                    this.x(u.tag() << 3 | 0x1);
                    this.a.write(p(8).putLong(n2).array());
                }
            }
            else {
                this.x(u.tag() << 3);
                this.y(n2 >> 63 ^ n2 << 1);
            }
        }
        else {
            this.x(u.tag() << 3);
            this.y(n2);
        }
        return this;
    }
    
    public b n(final n00 n00, final boolean b) {
        return this.o(n00, b, true);
    }
    
    public b o(final n00 n00, final boolean b, final boolean b2) {
        return this.k(n00, b ? 1 : 0, b2);
    }
    
    public final long q(final w01 w01, final Object o) {
        final ij0 a = new ij0();
        try {
            final OutputStream a2 = this.a;
            this.a = a;
            try {
                w01.encode(o, this);
                this.a = a2;
                final long a3 = a.a();
                a.close();
                return a3;
            }
            finally {
                this.a = a2;
            }
        }
        finally {
            try {
                a.close();
            }
            finally {
                final Throwable exception;
                ((Throwable)w01).addSuppressed(exception);
            }
        }
    }
    
    public final b r(final w01 w01, final n00 n00, final Object o, final boolean b) {
        final long q = this.q(w01, o);
        if (b && q == 0L) {
            return this;
        }
        this.x(v(n00) << 3 | 0x2);
        this.y(q);
        w01.encode(o, this);
        return this;
    }
    
    public final b s(final x22 x22, final n00 n00, final Object o, final boolean b) {
        this.e.c(n00, b);
        x22.encode(o, this.e);
        return this;
    }
    
    public b t(final Object o) {
        if (o == null) {
            return this;
        }
        final w01 w01 = this.b.get(o.getClass());
        if (w01 != null) {
            w01.encode(o, this);
            return this;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("No encoder for ");
        sb.append(o.getClass());
        throw new EncodingException(sb.toString());
    }
    
    public final void x(int n) {
        while ((n & 0xFFFFFF80) != 0x0L) {
            this.a.write((n & 0x7F) | 0x80);
            n >>>= 7;
        }
        this.a.write(n & 0x7F);
    }
    
    public final void y(long n) {
        while ((0xFFFFFFFFFFFFFF80L & n) != 0x0L) {
            this.a.write(((int)n & 0x7F) | 0x80);
            n >>>= 7;
        }
        this.a.write((int)n & 0x7F);
    }
}

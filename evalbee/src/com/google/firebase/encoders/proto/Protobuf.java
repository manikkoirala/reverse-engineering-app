// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.encoders.proto;

public @interface Protobuf {
    IntEncoding intEncoding() default IntEncoding.DEFAULT;
    
    int tag();
    
    public enum IntEncoding
    {
        private static final IntEncoding[] $VALUES;
        
        DEFAULT, 
        FIXED, 
        SIGNED;
    }
}

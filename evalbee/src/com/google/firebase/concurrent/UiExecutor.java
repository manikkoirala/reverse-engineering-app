// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.concurrent;

import android.os.Looper;
import android.os.Handler;
import java.util.concurrent.Executor;

public enum UiExecutor implements Executor
{
    private static final UiExecutor[] $VALUES;
    private static final Handler HANDLER;
    
    INSTANCE;
    
    static {
        HANDLER = new Handler(Looper.getMainLooper());
    }
    
    @Override
    public void execute(final Runnable runnable) {
        UiExecutor.HANDLER.post(runnable);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.concurrent;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import android.os.Build$VERSION;
import android.os.StrictMode$ThreadPolicy$Builder;
import android.os.StrictMode$ThreadPolicy;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;
import com.google.firebase.components.ComponentRegistrar;

public class ExecutorsRegistrar implements ComponentRegistrar
{
    public static final yi0 a;
    public static final yi0 b;
    public static final yi0 c;
    public static final yi0 d;
    
    static {
        a = new yi0(new zy());
        b = new yi0(new az());
        c = new yi0(new bz());
        d = new yi0(new cz());
    }
    
    public static StrictMode$ThreadPolicy i() {
        final StrictMode$ThreadPolicy$Builder detectNetwork = new StrictMode$ThreadPolicy$Builder().detectNetwork();
        final int sdk_INT = Build$VERSION.SDK_INT;
        detectNetwork.detectResourceMismatches();
        if (sdk_INT >= 26) {
            yy.a(detectNetwork);
        }
        return detectNetwork.penaltyLog().build();
    }
    
    public static ThreadFactory j(final String s, final int n) {
        return new xo(s, n, null);
    }
    
    public static ThreadFactory k(final String s, final int n, final StrictMode$ThreadPolicy strictMode$ThreadPolicy) {
        return new xo(s, n, strictMode$ThreadPolicy);
    }
    
    public static StrictMode$ThreadPolicy t() {
        return new StrictMode$ThreadPolicy$Builder().detectAll().penaltyLog().build();
    }
    
    public static ScheduledExecutorService u(final ExecutorService executorService) {
        return new cs(executorService, (ScheduledExecutorService)ExecutorsRegistrar.d.get());
    }
    
    @Override
    public List getComponents() {
        return Arrays.asList(zi.d(da1.a(za.class, ScheduledExecutorService.class), da1.a(za.class, ExecutorService.class), da1.a(za.class, Executor.class)).f(new dz()).d(), zi.d(da1.a(fc.class, ScheduledExecutorService.class), da1.a(fc.class, ExecutorService.class), da1.a(fc.class, Executor.class)).f(new ez()).d(), zi.d(da1.a(sj0.class, ScheduledExecutorService.class), da1.a(sj0.class, ExecutorService.class), da1.a(sj0.class, Executor.class)).f(new fz()).d(), zi.c(da1.a(n02.class, Executor.class)).f(new gz()).d());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.concurrent;

import java.util.concurrent.RejectedExecutionException;
import com.google.android.gms.common.internal.Preconditions;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.logging.Logger;
import java.util.concurrent.Executor;

public final class SequentialExecutor implements Executor
{
    public static final Logger f;
    public final Executor a;
    public final Deque b;
    public WorkerRunningState c;
    public long d;
    public final b e;
    
    static {
        f = Logger.getLogger(SequentialExecutor.class.getName());
    }
    
    public SequentialExecutor(final Executor executor) {
        this.b = new ArrayDeque();
        this.c = WorkerRunningState.IDLE;
        this.d = 0L;
        this.e = new b(null);
        this.a = Preconditions.checkNotNull(executor);
    }
    
    public static /* synthetic */ Deque a(final SequentialExecutor sequentialExecutor) {
        return sequentialExecutor.b;
    }
    
    public static /* synthetic */ WorkerRunningState b(final SequentialExecutor sequentialExecutor) {
        return sequentialExecutor.c;
    }
    
    public static /* synthetic */ WorkerRunningState c(final SequentialExecutor sequentialExecutor, final WorkerRunningState c) {
        return sequentialExecutor.c = c;
    }
    
    public static /* synthetic */ long d(final SequentialExecutor sequentialExecutor) {
        final long d = sequentialExecutor.d;
        sequentialExecutor.d = 1L + d;
        return d;
    }
    
    @Override
    public void execute(Runnable queuing) {
        Preconditions.checkNotNull(queuing);
        synchronized (this.b) {
            final WorkerRunningState c = this.c;
            if (c != WorkerRunningState.RUNNING) {
                final WorkerRunningState queued = WorkerRunningState.QUEUED;
                if (c != queued) {
                    final long d = this.d;
                    final Runnable runnable = new Runnable(this, queuing) {
                        public final Runnable a;
                        public final SequentialExecutor b;
                        
                        @Override
                        public void run() {
                            this.a.run();
                        }
                        
                        @Override
                        public String toString() {
                            return this.a.toString();
                        }
                    };
                    this.b.add(runnable);
                    queuing = (Error)WorkerRunningState.QUEUING;
                    this.c = (WorkerRunningState)queuing;
                    monitorexit(this.b);
                    final int n = 1;
                    boolean b = true;
                    try {
                        this.a.execute(this.e);
                        if (this.c == queuing) {
                            b = false;
                        }
                        if (b) {
                            return;
                        }
                        synchronized (this.b) {
                            if (this.d == d && this.c == queuing) {
                                this.c = queued;
                            }
                            return;
                        }
                    }
                    catch (final Error queuing) {}
                    catch (final RuntimeException ex) {}
                    final Deque b2 = this.b;
                    synchronized (this.b) {
                        final WorkerRunningState c2 = this.c;
                        if ((c2 == WorkerRunningState.IDLE || c2 == WorkerRunningState.QUEUING) && this.b.removeLastOccurrence(runnable)) {
                            final int n2 = n;
                        }
                        else {
                            final int n2 = 0;
                        }
                        int n2;
                        if (queuing instanceof RejectedExecutionException && n2 == 0) {
                            return;
                        }
                        throw queuing;
                    }
                }
            }
            this.b.add(queuing);
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SequentialExecutor@");
        sb.append(System.identityHashCode(this));
        sb.append("{");
        sb.append(this.a);
        sb.append("}");
        return sb.toString();
    }
    
    public enum WorkerRunningState
    {
        private static final WorkerRunningState[] $VALUES;
        
        IDLE, 
        QUEUED, 
        QUEUING, 
        RUNNING;
    }
    
    public final class b implements Runnable
    {
        public Runnable a;
        public final SequentialExecutor b;
        
        public b(final SequentialExecutor b) {
            this.b = b;
        }
        
        public final void a() {
            int n = 0;
            int n2 = 0;
            while (true) {
                int n3 = n2;
                try {
                    final Deque a = SequentialExecutor.a(this.b);
                    n3 = n2;
                    monitorenter(a);
                    int n4 = n;
                    Label_0083: {
                        if (n != 0) {
                            break Label_0083;
                        }
                        try {
                            final WorkerRunningState b = SequentialExecutor.b(this.b);
                            final WorkerRunningState running = WorkerRunningState.RUNNING;
                            if (b == running) {
                                monitorexit(a);
                                return;
                            }
                            SequentialExecutor.d(this.b);
                            SequentialExecutor.c(this.b, running);
                            n4 = 1;
                            if ((this.a = SequentialExecutor.a(this.b).poll()) == null) {
                                SequentialExecutor.c(this.b, WorkerRunningState.IDLE);
                                monitorexit(a);
                                return;
                            }
                            monitorexit(a);
                            n3 = n2;
                            n2 |= (Thread.interrupted() ? 1 : 0);
                            try {
                                try {
                                    this.a.run();
                                    n3 = n2;
                                    this.a = null;
                                    n = n4;
                                }
                                finally {
                                    n3 = n2;
                                    this.a = null;
                                    n3 = n2;
                                }
                            }
                            catch (final RuntimeException ex) {}
                        }
                        finally {
                            monitorexit(a);
                            n3 = n2;
                        }
                    }
                }
                finally {
                    if (n3 != 0) {
                        Thread.currentThread().interrupt();
                    }
                }
            }
        }
        
        @Override
        public void run() {
            try {
                this.a();
            }
            catch (final Error error) {
                synchronized (SequentialExecutor.a(this.b)) {
                    SequentialExecutor.c(this.b, WorkerRunningState.IDLE);
                    monitorexit(SequentialExecutor.a(this.b));
                    throw error;
                }
            }
        }
        
        @Override
        public String toString() {
            final Runnable a = this.a;
            if (a != null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("SequentialExecutorWorker{running=");
                sb.append(a);
                sb.append("}");
                return sb.toString();
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("SequentialExecutorWorker{state=");
            sb2.append(SequentialExecutor.b(this.b));
            sb2.append("}");
            return sb2.toString();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.concurrent;

import java.util.concurrent.Executor;

public abstract class FirebaseExecutors
{
    public static Executor a(final Executor executor, final int n) {
        return new uj0(executor, n);
    }
    
    public static Executor b(final Executor executor) {
        return new SequentialExecutor(executor);
    }
    
    public enum DirectExecutor implements Executor
    {
        private static final DirectExecutor[] $VALUES;
        
        INSTANCE;
        
        @Override
        public void execute(final Runnable runnable) {
            runnable.run();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.appcheck.internal;

import android.content.SharedPreferences;
import com.google.android.gms.common.internal.Preconditions;
import android.content.Context;

public class StorageHelper
{
    public static final yl0 b;
    public yi0 a;
    
    static {
        b = new yl0(StorageHelper.class.getSimpleName());
    }
    
    public StorageHelper(final Context context, final String s) {
        Preconditions.checkNotNull(context);
        Preconditions.checkNotEmpty(s);
        this.a = new yi0(new eq1(context, String.format("com.google.firebase.appcheck.store.%s", s)));
    }
    
    public void b() {
        ((SharedPreferences)this.a.get()).edit().remove("com.google.firebase.appcheck.APP_CHECK_TOKEN").remove("com.google.firebase.appcheck.TOKEN_TYPE").apply();
    }
    
    public s5 d() {
        final String string = ((SharedPreferences)this.a.get()).getString("com.google.firebase.appcheck.TOKEN_TYPE", (String)null);
        final String string2 = ((SharedPreferences)this.a.get()).getString("com.google.firebase.appcheck.APP_CHECK_TOKEN", (String)null);
        if (string != null) {
            if (string2 != null) {
                try {
                    final int n = StorageHelper$a.a[TokenType.valueOf(string).ordinal()];
                    if (n == 1) {
                        return eq.d(string2);
                    }
                    if (n != 2) {
                        StorageHelper.b.d("Reached unreachable section in #retrieveAppCheckToken()");
                        return null;
                    }
                    return eq.c(string2);
                }
                catch (final IllegalArgumentException ex) {
                    final yl0 b = StorageHelper.b;
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Failed to parse TokenType of stored token  with type [");
                    sb.append(string);
                    sb.append("] with exception: ");
                    sb.append(ex.getMessage());
                    b.d(sb.toString());
                    this.b();
                }
            }
        }
        return null;
    }
    
    public enum TokenType
    {
        private static final TokenType[] $VALUES;
        
        DEFAULT_APP_CHECK_TOKEN, 
        UNKNOWN_APP_CHECK_TOKEN;
    }
}

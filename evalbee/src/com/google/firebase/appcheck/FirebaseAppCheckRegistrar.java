// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.appcheck;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.Executor;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.firebase.components.ComponentRegistrar;

@KeepForSdk
public class FirebaseAppCheckRegistrar implements ComponentRegistrar
{
    @Override
    public List getComponents() {
        final da1 a = da1.a(n02.class, Executor.class);
        final da1 a2 = da1.a(sj0.class, Executor.class);
        final da1 a3 = da1.a(za.class, Executor.class);
        final da1 a4 = da1.a(fc.class, ScheduledExecutorService.class);
        return Arrays.asList(zi.f(s10.class, dg0.class).h("fire-app-check").b(os.k(r10.class)).b(os.j(a)).b(os.j(a2)).b(os.j(a3)).b(os.j(a4)).b(os.i(wc0.class)).f(new t10(a, a2, a3, a4)).c().d(), vc0.a(), mj0.b("fire-app-check", "17.1.1"));
    }
}

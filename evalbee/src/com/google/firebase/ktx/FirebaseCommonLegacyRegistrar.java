// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.ktx;

import org.jetbrains.annotations.NotNull;
import java.util.List;
import androidx.annotation.Keep;
import com.google.firebase.components.ComponentRegistrar;

@Keep
public final class FirebaseCommonLegacyRegistrar implements ComponentRegistrar
{
    @NotNull
    @Override
    public List<zi> getComponents() {
        return mh.e((Object)mj0.b("fire-core-ktx", "20.4.2"));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase;

import com.google.android.gms.common.internal.Preconditions;

public class FirebaseException extends Exception
{
    @Deprecated
    public FirebaseException() {
    }
    
    public FirebaseException(final String message) {
        Preconditions.checkNotEmpty(message, "Detail message must not be empty");
        super(message);
    }
    
    public FirebaseException(final String message, final Throwable cause) {
        Preconditions.checkNotEmpty(message, "Detail message must not be empty");
        super(message, cause);
    }
}

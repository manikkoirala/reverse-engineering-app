// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.components;

public class InvalidRegistrarException extends RuntimeException
{
    public InvalidRegistrarException(final String message) {
        super(message);
    }
    
    public InvalidRegistrarException(final String message, final Throwable cause) {
        super(message, cause);
    }
}

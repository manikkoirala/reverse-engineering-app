// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.database.collection;

public class d extends g
{
    public int e;
    
    public d(final Object o, final Object o2, final LLRBNode llrbNode, final LLRBNode llrbNode2) {
        super(o, o2, llrbNode, llrbNode2);
        this.e = -1;
    }
    
    @Override
    public boolean a() {
        return false;
    }
    
    @Override
    public g g(Object value, final Object o, LLRBNode right, final LLRBNode llrbNode) {
        Object key = value;
        if (value == null) {
            key = this.getKey();
        }
        if ((value = o) == null) {
            value = this.getValue();
        }
        LLRBNode left;
        if ((left = right) == null) {
            left = this.getLeft();
        }
        if ((right = llrbNode) == null) {
            right = this.getRight();
        }
        return new d(key, value, left, right);
    }
    
    @Override
    public Color i() {
        return Color.BLACK;
    }
    
    @Override
    public void p(final LLRBNode llrbNode) {
        if (this.e == -1) {
            super.p(llrbNode);
            return;
        }
        throw new IllegalStateException("Can't set left after using size");
    }
    
    @Override
    public int size() {
        if (this.e == -1) {
            this.e = this.getLeft().size() + 1 + this.getRight().size();
        }
        return this.e;
    }
}

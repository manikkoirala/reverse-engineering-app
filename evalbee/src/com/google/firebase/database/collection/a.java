// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.database.collection;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Collections;
import java.util.Map;
import java.util.List;
import java.util.Comparator;

public class a extends b
{
    public final Object[] a;
    public final Object[] b;
    public final Comparator c;
    
    public a(final Comparator c) {
        this.a = new Object[0];
        this.b = new Object[0];
        this.c = c;
    }
    
    public a(final Comparator c, final Object[] a, final Object[] b) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public static /* synthetic */ Object[] o(final a a) {
        return a.a;
    }
    
    public static /* synthetic */ Object[] p(final a a) {
        return a.b;
    }
    
    public static Object[] r(final Object[] array, final int n, final Object o) {
        final int n2 = array.length + 1;
        final Object[] array2 = new Object[n2];
        System.arraycopy(array, 0, array2, 0, n);
        array2[n] = o;
        System.arraycopy(array, n, array2, n + 1, n2 - n - 1);
        return array2;
    }
    
    public static a s(final List list, final Map map, final b.a.a a, final Comparator c) {
        Collections.sort((List<Object>)list, c);
        final int size = list.size();
        final Object[] array = new Object[size];
        final Object[] array2 = new Object[size];
        final Iterator iterator = list.iterator();
        int n = 0;
        while (iterator.hasNext()) {
            final Object next = iterator.next();
            array[n] = next;
            array2[n] = map.get(a.a(next));
            ++n;
        }
        return new a(c, array, array2);
    }
    
    public static Object[] w(final Object[] array, final int n) {
        final int n2 = array.length - 1;
        final Object[] array2 = new Object[n2];
        System.arraycopy(array, 0, array2, 0, n);
        System.arraycopy(array, n + 1, array2, n, n2 - n);
        return array2;
    }
    
    public static Object[] y(final Object[] array, final int n, final Object o) {
        final int length = array.length;
        final Object[] array2 = new Object[length];
        System.arraycopy(array, 0, array2, 0, length);
        array2[n] = o;
        return array2;
    }
    
    @Override
    public boolean a(final Object o) {
        return this.t(o) != -1;
    }
    
    @Override
    public Object b(Object o) {
        final int t = this.t(o);
        if (t != -1) {
            o = this.b[t];
        }
        else {
            o = null;
        }
        return o;
    }
    
    @Override
    public Comparator c() {
        return this.c;
    }
    
    @Override
    public Object g() {
        final Object[] a = this.a;
        Object o;
        if (a.length > 0) {
            o = a[a.length - 1];
        }
        else {
            o = null;
        }
        return o;
    }
    
    @Override
    public Object i() {
        final Object[] a = this.a;
        Object o;
        if (a.length > 0) {
            o = a[0];
        }
        else {
            o = null;
        }
        return o;
    }
    
    @Override
    public boolean isEmpty() {
        return this.a.length == 0;
    }
    
    @Override
    public Iterator iterator() {
        return this.v(0, false);
    }
    
    @Override
    public b l(final Object o, final Object o2) {
        final int t = this.t(o);
        if (t != -1) {
            final Object[] a = this.a;
            if (a[t] == o && this.b[t] == o2) {
                return this;
            }
            return new a(this.c, y(a, t, o), y(this.b, t, o2));
        }
        else {
            if (this.a.length > 25) {
                final HashMap hashMap = new HashMap(this.a.length + 1);
                int n = 0;
                while (true) {
                    final Object[] a2 = this.a;
                    if (n >= a2.length) {
                        break;
                    }
                    hashMap.put(a2[n], this.b[n]);
                    ++n;
                }
                hashMap.put(o, o2);
                return h.p(hashMap, this.c);
            }
            final int u = this.u(o);
            return new a(this.c, r(this.a, u, o), r(this.b, u, o2));
        }
    }
    
    @Override
    public Iterator m(final Object o) {
        return this.v(this.u(o), false);
    }
    
    @Override
    public b n(final Object o) {
        final int t = this.t(o);
        if (t == -1) {
            return this;
        }
        return new a(this.c, w(this.a, t), w(this.b, t));
    }
    
    @Override
    public int size() {
        return this.a.length;
    }
    
    public final int t(final Object o) {
        final Object[] a = this.a;
        final int length = a.length;
        int i = 0;
        int n = 0;
        while (i < length) {
            if (this.c.compare(o, a[i]) == 0) {
                return n;
            }
            ++n;
            ++i;
        }
        return -1;
    }
    
    public final int u(final Object o) {
        int n = 0;
        while (true) {
            final Object[] a = this.a;
            if (n >= a.length || this.c.compare(a[n], o) >= 0) {
                break;
            }
            ++n;
        }
        return n;
    }
    
    public final Iterator v(final int n, final boolean b) {
        return new Iterator(this, n, b) {
            public int a = n;
            public final int b;
            public final boolean c;
            public final a d;
            
            public Map.Entry b() {
                final Object key = com.google.firebase.database.collection.a.o(this.d)[this.a];
                final Object[] p = com.google.firebase.database.collection.a.p(this.d);
                int a = this.a;
                final Object value = p[a];
                if (this.c) {
                    --a;
                }
                else {
                    ++a;
                }
                this.a = a;
                return (Map.Entry)new AbstractMap.SimpleImmutableEntry(key, value);
            }
            
            @Override
            public boolean hasNext() {
                final boolean c = this.c;
                boolean b = true;
                if (c) {
                    if (this.a >= 0) {
                        return b;
                    }
                }
                else if (this.a < com.google.firebase.database.collection.a.o(this.d).length) {
                    return b;
                }
                b = false;
                return b;
            }
            
            @Override
            public void remove() {
                throw new UnsupportedOperationException("Can't remove elements from ImmutableSortedMap");
            }
        };
    }
}

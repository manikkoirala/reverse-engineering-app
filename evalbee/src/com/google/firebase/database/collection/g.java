// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.database.collection;

import java.util.Comparator;

public abstract class g implements LLRBNode
{
    public final Object a;
    public final Object b;
    public LLRBNode c;
    public final LLRBNode d;
    
    public g(final Object a, final Object b, final LLRBNode llrbNode, final LLRBNode llrbNode2) {
        this.a = a;
        this.b = b;
        LLRBNode e = llrbNode;
        if (llrbNode == null) {
            e = com.google.firebase.database.collection.e.e();
        }
        this.c = e;
        LLRBNode e2;
        if ((e2 = llrbNode2) == null) {
            e2 = com.google.firebase.database.collection.e.e();
        }
        this.d = e2;
    }
    
    public static Color l(final LLRBNode llrbNode) {
        Color color;
        if (llrbNode.a()) {
            color = Color.BLACK;
        }
        else {
            color = Color.RED;
        }
        return color;
    }
    
    @Override
    public LLRBNode b(final Object o, final Object o2, final Comparator comparator) {
        final int compare = comparator.compare(o, this.a);
        g g;
        if (compare < 0) {
            g = this.g(null, null, this.c.b(o, o2, comparator), null);
        }
        else if (compare == 0) {
            g = this.g(o, o2, null, null);
        }
        else {
            g = this.g(null, null, null, this.d.b(o, o2, comparator));
        }
        return g.h();
    }
    
    @Override
    public LLRBNode c(final Object o, final Comparator comparator) {
        g g;
        if (comparator.compare(o, this.a) < 0) {
            g j;
            if (!this.c.isEmpty() && !this.c.a() && !((g)this.c).c.a()) {
                j = this.j();
            }
            else {
                j = this;
            }
            g = j.g(null, null, j.c.c(o, comparator), null);
        }
        else {
            g o2;
            if (this.c.a()) {
                o2 = this.o();
            }
            else {
                o2 = this;
            }
            g k = o2;
            if (!o2.d.isEmpty()) {
                k = o2;
                if (!o2.d.a()) {
                    k = o2;
                    if (!((g)o2.d).c.a()) {
                        k = o2.k();
                    }
                }
            }
            g g2 = k;
            if (comparator.compare(o, k.a) == 0) {
                if (k.d.isEmpty()) {
                    return e.e();
                }
                final LLRBNode min = k.d.getMin();
                g2 = k.g(min.getKey(), min.getValue(), null, ((g)k.d).m());
            }
            g = g2.g(null, null, null, g2.d.c(o, comparator));
        }
        return g.h();
    }
    
    public final g e() {
        final LLRBNode c = this.c;
        final LLRBNode d = c.d(null, null, l(c), null, null);
        final LLRBNode d2 = this.d;
        return this.f(null, null, l(this), d, d2.d(null, null, l(d2), null, null));
    }
    
    public g f(Object b, final Object o, final Color color, LLRBNode d, final LLRBNode llrbNode) {
        Object a = b;
        if (b == null) {
            a = this.a;
        }
        if ((b = o) == null) {
            b = this.b;
        }
        LLRBNode c;
        if ((c = d) == null) {
            c = this.c;
        }
        if ((d = llrbNode) == null) {
            d = this.d;
        }
        if (color == Color.RED) {
            return new f(a, b, c, d);
        }
        return new d(a, b, c, d);
    }
    
    public abstract g g(final Object p0, final Object p1, final LLRBNode p2, final LLRBNode p3);
    
    @Override
    public Object getKey() {
        return this.a;
    }
    
    @Override
    public LLRBNode getLeft() {
        return this.c;
    }
    
    @Override
    public LLRBNode getMax() {
        if (this.d.isEmpty()) {
            return this;
        }
        return this.d.getMax();
    }
    
    @Override
    public LLRBNode getMin() {
        if (this.c.isEmpty()) {
            return this;
        }
        return this.c.getMin();
    }
    
    @Override
    public LLRBNode getRight() {
        return this.d;
    }
    
    @Override
    public Object getValue() {
        return this.b;
    }
    
    public final g h() {
        g n;
        if (this.d.a() && !this.c.a()) {
            n = this.n();
        }
        else {
            n = this;
        }
        g o = n;
        if (n.c.a()) {
            o = n;
            if (((g)n.c).c.a()) {
                o = n.o();
            }
        }
        g e = o;
        if (o.c.a()) {
            e = o;
            if (o.d.a()) {
                e = o.e();
            }
        }
        return e;
    }
    
    public abstract Color i();
    
    @Override
    public boolean isEmpty() {
        return false;
    }
    
    public final g j() {
        g g2;
        final g g = g2 = this.e();
        if (g.getRight().getLeft().a()) {
            g2 = g.g(null, null, null, ((g)g.getRight()).o()).n().e();
        }
        return g2;
    }
    
    public final g k() {
        g g2;
        final g g = g2 = this.e();
        if (g.getLeft().getLeft().a()) {
            g2 = g.o().e();
        }
        return g2;
    }
    
    public final LLRBNode m() {
        if (this.c.isEmpty()) {
            return e.e();
        }
        g j;
        if (!this.getLeft().a() && !this.getLeft().getLeft().a()) {
            j = this.j();
        }
        else {
            j = this;
        }
        return j.g(null, null, ((g)j.c).m(), null).h();
    }
    
    public final g n() {
        return (g)this.d.d(null, null, this.i(), this.f(null, null, Color.RED, null, ((g)this.d).c), null);
    }
    
    public final g o() {
        return (g)this.c.d(null, null, this.i(), null, this.f(null, null, Color.RED, ((g)this.c).d, null));
    }
    
    public void p(final LLRBNode c) {
        this.c = c;
    }
}

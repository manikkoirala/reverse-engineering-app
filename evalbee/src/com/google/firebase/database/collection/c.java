// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.database.collection;

import java.util.Iterator;
import java.util.Map;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class c implements Iterable
{
    public final b a;
    
    public c(final b a) {
        this.a = a;
    }
    
    public c(final List list, final Comparator comparator) {
        this.a = b.a.b(list, Collections.emptyMap(), b.a.d(), comparator);
    }
    
    public Object a() {
        return this.a.g();
    }
    
    public Object b() {
        return this.a.i();
    }
    
    public c c(final Object o) {
        return new c(this.a.l(o, null));
    }
    
    public boolean contains(final Object o) {
        return this.a.a(o);
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof c && this.a.equals(((c)o).a));
    }
    
    public Iterator g(final Object o) {
        return new a(this.a.m(o));
    }
    
    @Override
    public int hashCode() {
        return this.a.hashCode();
    }
    
    public c i(final Object o) {
        final b n = this.a.n(o);
        c c;
        if (n == this.a) {
            c = this;
        }
        else {
            c = new c(n);
        }
        return c;
    }
    
    public boolean isEmpty() {
        return this.a.isEmpty();
    }
    
    @Override
    public Iterator iterator() {
        return new a(this.a.iterator());
    }
    
    public c l(c c) {
        c c2;
        if (this.size() < c.size()) {
            c2 = this;
        }
        else {
            c2 = c;
            c = this;
        }
        final Iterator iterator = c2.iterator();
        while (iterator.hasNext()) {
            c = c.c(iterator.next());
        }
        return c;
    }
    
    public int size() {
        return this.a.size();
    }
    
    public static class a implements Iterator
    {
        public final Iterator a;
        
        public a(final Iterator a) {
            this.a = a;
        }
        
        @Override
        public boolean hasNext() {
            return this.a.hasNext();
        }
        
        @Override
        public Object next() {
            return this.a.next().getKey();
        }
        
        @Override
        public void remove() {
            this.a.remove();
        }
    }
}

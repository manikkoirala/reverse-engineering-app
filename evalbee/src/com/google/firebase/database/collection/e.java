// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.database.collection;

import java.util.Comparator;

public class e implements LLRBNode
{
    public static final e a;
    
    static {
        a = new e();
    }
    
    public static e e() {
        return e.a;
    }
    
    @Override
    public boolean a() {
        return false;
    }
    
    @Override
    public LLRBNode b(final Object o, final Object o2, final Comparator comparator) {
        return new f(o, o2);
    }
    
    @Override
    public LLRBNode c(final Object o, final Comparator comparator) {
        return this;
    }
    
    @Override
    public LLRBNode d(final Object o, final Object o2, final Color color, final LLRBNode llrbNode, final LLRBNode llrbNode2) {
        return this;
    }
    
    @Override
    public Object getKey() {
        return null;
    }
    
    @Override
    public LLRBNode getLeft() {
        return this;
    }
    
    @Override
    public LLRBNode getMax() {
        return this;
    }
    
    @Override
    public LLRBNode getMin() {
        return this;
    }
    
    @Override
    public LLRBNode getRight() {
        return this;
    }
    
    @Override
    public Object getValue() {
        return null;
    }
    
    @Override
    public boolean isEmpty() {
        return true;
    }
    
    @Override
    public int size() {
        return 0;
    }
}

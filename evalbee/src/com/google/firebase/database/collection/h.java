// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.database.collection;

import java.util.Collections;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.Comparator;

public class h extends com.google.firebase.database.collection.b
{
    public LLRBNode a;
    public Comparator b;
    
    public h(final LLRBNode a, final Comparator b) {
        this.a = a;
        this.b = b;
    }
    
    public static h o(final List list, final Map map, final com.google.firebase.database.collection.b.a.a a, final Comparator comparator) {
        return b.b(list, map, a, comparator);
    }
    
    public static h p(final Map map, final Comparator comparator) {
        return b.b(new ArrayList(map.keySet()), map, com.google.firebase.database.collection.b.a.d(), comparator);
    }
    
    @Override
    public boolean a(final Object o) {
        return this.r(o) != null;
    }
    
    @Override
    public Object b(Object value) {
        final LLRBNode r = this.r(value);
        if (r != null) {
            value = r.getValue();
        }
        else {
            value = null;
        }
        return value;
    }
    
    @Override
    public Comparator c() {
        return this.b;
    }
    
    @Override
    public Object g() {
        return this.a.getMax().getKey();
    }
    
    @Override
    public Object i() {
        return this.a.getMin().getKey();
    }
    
    @Override
    public boolean isEmpty() {
        return this.a.isEmpty();
    }
    
    @Override
    public Iterator iterator() {
        return new le0(this.a, null, this.b, false);
    }
    
    @Override
    public com.google.firebase.database.collection.b l(final Object o, final Object o2) {
        return new h(this.a.b(o, o2, this.b).d(null, null, LLRBNode.Color.BLACK, null, null), this.b);
    }
    
    @Override
    public Iterator m(final Object o) {
        return new le0(this.a, o, this.b, false);
    }
    
    @Override
    public com.google.firebase.database.collection.b n(final Object o) {
        if (!this.a(o)) {
            return this;
        }
        return new h(this.a.c(o, this.b).d(null, null, LLRBNode.Color.BLACK, null, null), this.b);
    }
    
    public final LLRBNode r(final Object o) {
        LLRBNode llrbNode = this.a;
        while (!llrbNode.isEmpty()) {
            final int compare = this.b.compare(o, llrbNode.getKey());
            if (compare < 0) {
                llrbNode = llrbNode.getLeft();
            }
            else {
                if (compare == 0) {
                    return llrbNode;
                }
                llrbNode = llrbNode.getRight();
            }
        }
        return null;
    }
    
    @Override
    public int size() {
        return this.a.size();
    }
    
    public static class b
    {
        public final List a;
        public final Map b;
        public final com.google.firebase.database.collection.b.a.a c;
        public g d;
        public g e;
        
        public b(final List a, final Map b, final com.google.firebase.database.collection.b.a.a c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
        
        public static h b(final List list, final Map map, final com.google.firebase.database.collection.b.a.a a, final Comparator c) {
            final h.b b = new h.b(list, map, a);
            Collections.sort((List<Object>)list, c);
            final Iterator iterator = new a(list.size()).iterator();
            int size = list.size();
            while (iterator.hasNext()) {
                final h.b.b b2 = iterator.next();
                final int b3 = b2.b;
                size -= b3;
                if (b2.a) {
                    b.c(LLRBNode.Color.BLACK, b3, size);
                }
                else {
                    b.c(LLRBNode.Color.BLACK, b3, size);
                    final int b4 = b2.b;
                    size -= b4;
                    b.c(LLRBNode.Color.RED, b4, size);
                }
            }
            LLRBNode llrbNode;
            if ((llrbNode = b.d) == null) {
                llrbNode = e.e();
            }
            return new h(llrbNode, c, null);
        }
        
        public final LLRBNode a(final int n, int n2) {
            if (n2 == 0) {
                return com.google.firebase.database.collection.e.e();
            }
            if (n2 == 1) {
                final Object value = this.a.get(n);
                return new d(value, this.d(value), null, null);
            }
            final int n3 = n2 / 2;
            n2 = n + n3;
            final LLRBNode a = this.a(n, n3);
            final LLRBNode a2 = this.a(n2 + 1, n3);
            final Object value2 = this.a.get(n2);
            return new d(value2, this.d(value2), a, a2);
        }
        
        public final void c(final LLRBNode.Color color, final int n, final int n2) {
            final LLRBNode a = this.a(n2 + 1, n - 1);
            final Object value = this.a.get(n2);
            g g;
            if (color == LLRBNode.Color.RED) {
                g = new f(value, this.d(value), null, a);
            }
            else {
                g = new d(value, this.d(value), null, a);
            }
            if (this.d == null) {
                this.d = g;
            }
            else {
                this.e.p(g);
            }
            this.e = g;
        }
        
        public final Object d(final Object o) {
            return this.b.get(this.c.a(o));
        }
        
        public static class a implements Iterable
        {
            public long a;
            public final int b;
            
            public a(int n) {
                final int b = (int)Math.floor(Math.log(++n) / Math.log(2.0));
                this.b = b;
                this.a = ((long)Math.pow(2.0, b) - 1L & (long)n);
            }
            
            public static /* synthetic */ int a(final a a) {
                return a.b;
            }
            
            public static /* synthetic */ long b(final a a) {
                return a.a;
            }
            
            @Override
            public Iterator iterator() {
                return new Iterator(this) {
                    public int a = h.b.a.a(b) - 1;
                    public final a b;
                    
                    public h.b.b b() {
                        final long b = h.b.a.b(this.b);
                        final long n = 1 << this.a;
                        final h.b.b b2 = new h.b.b();
                        b2.a = ((b & n) == 0x0L);
                        b2.b = (int)Math.pow(2.0, this.a);
                        --this.a;
                        return b2;
                    }
                    
                    @Override
                    public boolean hasNext() {
                        return this.a >= 0;
                    }
                    
                    @Override
                    public void remove() {
                    }
                };
            }
        }
        
        public static class b
        {
            public boolean a;
            public int b;
        }
    }
}

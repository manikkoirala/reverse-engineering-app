// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.database.collection;

import java.util.Comparator;

public interface LLRBNode
{
    boolean a();
    
    LLRBNode b(final Object p0, final Object p1, final Comparator p2);
    
    LLRBNode c(final Object p0, final Comparator p1);
    
    LLRBNode d(final Object p0, final Object p1, final Color p2, final LLRBNode p3, final LLRBNode p4);
    
    Object getKey();
    
    LLRBNode getLeft();
    
    LLRBNode getMax();
    
    LLRBNode getMin();
    
    LLRBNode getRight();
    
    Object getValue();
    
    boolean isEmpty();
    
    int size();
    
    public enum Color
    {
        private static final Color[] $VALUES;
        
        BLACK, 
        RED;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.database.collection;

public class f extends g
{
    public f(final Object o, final Object o2) {
        super(o, o2, e.e(), e.e());
    }
    
    public f(final Object o, final Object o2, final LLRBNode llrbNode, final LLRBNode llrbNode2) {
        super(o, o2, llrbNode, llrbNode2);
    }
    
    @Override
    public boolean a() {
        return true;
    }
    
    @Override
    public g g(Object value, final Object o, LLRBNode right, final LLRBNode llrbNode) {
        Object key = value;
        if (value == null) {
            key = this.getKey();
        }
        if ((value = o) == null) {
            value = this.getValue();
        }
        LLRBNode left;
        if ((left = right) == null) {
            left = this.getLeft();
        }
        if ((right = llrbNode) == null) {
            right = this.getRight();
        }
        return new f(key, value, left, right);
    }
    
    @Override
    public Color i() {
        return Color.RED;
    }
    
    @Override
    public int size() {
        return this.getLeft().size() + 1 + this.getRight().size();
    }
}

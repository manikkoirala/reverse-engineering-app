// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.database.collection;

import java.util.List;
import java.util.Iterator;
import java.util.Map;
import java.util.Comparator;

public abstract class b implements Iterable
{
    public abstract boolean a(final Object p0);
    
    public abstract Object b(final Object p0);
    
    public abstract Comparator c();
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof b)) {
            return false;
        }
        final b b = (b)o;
        if (!this.c().equals(b.c())) {
            return false;
        }
        if (this.size() != b.size()) {
            return false;
        }
        final Iterator iterator = this.iterator();
        final Iterator iterator2 = b.iterator();
        while (iterator.hasNext()) {
            if (!iterator.next().equals(iterator2.next())) {
                return false;
            }
        }
        return true;
    }
    
    public abstract Object g();
    
    @Override
    public int hashCode() {
        int hashCode = this.c().hashCode();
        final Iterator iterator = this.iterator();
        while (iterator.hasNext()) {
            hashCode = hashCode * 31 + ((Map.Entry)iterator.next()).hashCode();
        }
        return hashCode;
    }
    
    public abstract Object i();
    
    public abstract boolean isEmpty();
    
    @Override
    public abstract Iterator iterator();
    
    public abstract b l(final Object p0, final Object p1);
    
    public abstract Iterator m(final Object p0);
    
    public abstract b n(final Object p0);
    
    public abstract int size();
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getSimpleName());
        sb.append("{");
        final Iterator iterator = this.iterator();
        int n = 1;
        while (iterator.hasNext()) {
            final Map.Entry<Object, V> entry = iterator.next();
            if (n != 0) {
                n = 0;
            }
            else {
                sb.append(", ");
            }
            sb.append("(");
            sb.append(entry.getKey());
            sb.append("=>");
            sb.append(entry.getValue());
            sb.append(")");
        }
        sb.append("};");
        return sb.toString();
    }
    
    public abstract static class a
    {
        public static final b.a.a a;
        
        static {
            a = (b.a.a)new ke0();
        }
        
        public static b b(final List list, final Map map, final b.a.a a, final Comparator comparator) {
            if (list.size() < 25) {
                return a.s(list, map, a, comparator);
            }
            return h.o(list, map, a, comparator);
        }
        
        public static b c(final Comparator comparator) {
            return new a(comparator);
        }
        
        public static b.a.a d() {
            return b.a.a;
        }
        
        public interface a
        {
            Object a(final Object p0);
        }
    }
}

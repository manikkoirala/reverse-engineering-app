// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.storage;

import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.api.Status;
import android.util.Log;
import com.google.firebase.FirebaseException;

public class StorageException extends FirebaseException
{
    static final boolean $assertionsDisabled = false;
    public static final int ERROR_BUCKET_NOT_FOUND = -13011;
    public static final int ERROR_CANCELED = -13040;
    public static final int ERROR_INVALID_CHECKSUM = -13031;
    public static final int ERROR_NOT_AUTHENTICATED = -13020;
    public static final int ERROR_NOT_AUTHORIZED = -13021;
    public static final int ERROR_OBJECT_NOT_FOUND = -13010;
    public static final int ERROR_PROJECT_NOT_FOUND = -13012;
    public static final int ERROR_QUOTA_EXCEEDED = -13013;
    public static final int ERROR_RETRY_LIMIT_EXCEEDED = -13030;
    public static final int ERROR_UNKNOWN = -13000;
    private static final int NETWORK_UNAVAILABLE = -2;
    private static final String TAG = "StorageException";
    private Throwable cause;
    private final int errorCode;
    private final int httpResultCode;
    
    public StorageException(final int n, Throwable cause, final int n2) {
        super(getErrorMessageForCode(n));
        this.cause = cause;
        this.errorCode = n;
        this.httpResultCode = n2;
        final StringBuilder sb = new StringBuilder();
        sb.append("StorageException has occurred.\n");
        sb.append(getErrorMessageForCode(n));
        sb.append("\n Code: ");
        sb.append(n);
        sb.append(" HttpResult: ");
        sb.append(n2);
        Log.e("StorageException", sb.toString());
        cause = this.cause;
        if (cause != null) {
            Log.e("StorageException", cause.getMessage(), this.cause);
        }
    }
    
    private static int calculateErrorCode(final Status status) {
        if (status.isCanceled()) {
            return -13040;
        }
        if (status.equals(Status.RESULT_TIMEOUT)) {
            return -13030;
        }
        return -13000;
    }
    
    private static int calculateErrorCode(final Throwable t, final int n) {
        if (t instanceof CancelException) {
            return -13040;
        }
        if (n == -2) {
            return -13030;
        }
        if (n == 401) {
            return -13020;
        }
        if (n == 409) {
            return -13031;
        }
        if (n == 403) {
            return -13021;
        }
        if (n != 404) {
            return -13000;
        }
        return -13010;
    }
    
    public static StorageException fromErrorStatus(final Status status) {
        Preconditions.checkNotNull(status);
        Preconditions.checkArgument(status.isSuccess() ^ true);
        return new StorageException(calculateErrorCode(status), null, 0);
    }
    
    public static StorageException fromException(final Throwable t) {
        return fromExceptionAndHttpCode(t, 0);
    }
    
    public static StorageException fromExceptionAndHttpCode(final Throwable t, final int n) {
        if (t instanceof StorageException) {
            return (StorageException)t;
        }
        if (isResultSuccess(n) && t == null) {
            return null;
        }
        return new StorageException(calculateErrorCode(t, n), t, n);
    }
    
    public static String getErrorMessageForCode(final int n) {
        if (n == -13040) {
            return "The operation was cancelled.";
        }
        if (n == -13031) {
            return "Object has a checksum which does not match. Please retry the operation.";
        }
        if (n == -13030) {
            return "The operation retry limit has been exceeded.";
        }
        if (n == -13021) {
            return "User does not have permission to access this object.";
        }
        if (n == -13020) {
            return "User is not authenticated, please authenticate using Firebase Authentication and try again.";
        }
        switch (n) {
            default: {
                return "An unknown error occurred, please check the HTTP result code and inner exception for server response.";
            }
            case -13010: {
                return "Object does not exist at location.";
            }
            case -13011: {
                return "Bucket does not exist.";
            }
            case -13012: {
                return "Project does not exist.";
            }
            case -13013: {
                return "Quota for bucket exceeded, please view quota on www.firebase.google.com/storage.";
            }
        }
    }
    
    private static boolean isResultSuccess(final int n) {
        return n == 0 || (n >= 200 && n < 300);
    }
    
    @Override
    public Throwable getCause() {
        synchronized (this) {
            final Throwable cause = this.cause;
            monitorexit(this);
            Throwable t = cause;
            if (cause == this) {
                t = null;
            }
            return t;
        }
    }
    
    public int getErrorCode() {
        return this.errorCode;
    }
    
    public int getHttpResultCode() {
        return this.httpResultCode;
    }
    
    public boolean getIsRecoverableException() {
        return this.getErrorCode() == -13030;
    }
}

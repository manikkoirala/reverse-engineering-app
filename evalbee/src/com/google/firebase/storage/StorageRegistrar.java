// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.storage;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executor;
import androidx.annotation.Keep;
import com.google.firebase.components.ComponentRegistrar;

@Keep
public class StorageRegistrar implements ComponentRegistrar
{
    private static final String LIBRARY_NAME = "fire-gcs";
    da1 blockingExecutor;
    da1 uiExecutor;
    
    public StorageRegistrar() {
        this.blockingExecutor = da1.a(fc.class, Executor.class);
        this.uiExecutor = da1.a(n02.class, Executor.class);
    }
    
    @Override
    public List<zi> getComponents() {
        return Arrays.asList(zi.e(p30.class).h("fire-gcs").b(os.k(r10.class)).b(os.j(this.blockingExecutor)).b(os.j(this.uiExecutor)).b(os.i(zf0.class)).b(os.i(dg0.class)).f(new lq1(this)).d(), mj0.b("fire-gcs", "20.3.0"));
    }
}

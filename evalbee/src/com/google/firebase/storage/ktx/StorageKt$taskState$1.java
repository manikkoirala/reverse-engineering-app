// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.storage.ktx;

import kotlinx.coroutines.channels.ProduceKt;
import kotlin.jvm.internal.Lambda;
import com.google.android.gms.tasks.OnCompleteListener;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import kotlinx.coroutines.f;
import com.google.android.gms.tasks.Task;
import kotlin.coroutines.jvm.internal.SuspendLambda;

@xp(c = "com.google.firebase.storage.ktx.StorageKt$taskState$1", f = "Storage.kt", l = { 350 }, m = "invokeSuspend")
final class StorageKt$taskState$1 extends SuspendLambda implements q90
{
    final zq1 $this_taskState;
    private Object L$0;
    int label;
    
    public StorageKt$taskState$1(final zq1 $this_taskState, final vl vl) {
        this.$this_taskState = $this_taskState;
        super(2, vl);
    }
    
    private static final void invokeSuspend$lambda-1(final s81 s81, final zq1.b b) {
        br1.a().d(new e(s81, b));
    }
    
    private static final void invokeSuspend$lambda-1$lambda-0(final s81 s81, final zq1.b b) {
        eg.w((zk1)s81, (Object)new qu1.a(b));
    }
    
    private static final void invokeSuspend$lambda-3(final s81 s81, final zq1.b b) {
        br1.a().d(new a(s81, b));
    }
    
    private static final void invokeSuspend$lambda-3$lambda-2(final s81 s81, final zq1.b b) {
        eg.w((zk1)s81, (Object)new qu1.b(b));
    }
    
    private static final void invokeSuspend$lambda-4(final s81 s81, final Task task) {
        if (task.isSuccessful()) {
            zk1$a.a((zk1)s81, (Throwable)null, 1, (Object)null);
        }
        else {
            f.c((lm)s81, "Error getting the TaskState", (Throwable)task.getException());
        }
    }
    
    @NotNull
    public final vl create(@Nullable final Object l$0, @NotNull final vl vl) {
        final StorageKt$taskState$1 storageKt$taskState$1 = new StorageKt$taskState$1(this.$this_taskState, vl);
        storageKt$taskState$1.L$0 = l$0;
        return (vl)storageKt$taskState$1;
    }
    
    @Nullable
    public final Object invoke(@NotNull final s81 s81, @Nullable final vl vl) {
        return ((StorageKt$taskState$1)this.create(s81, vl)).invokeSuspend(u02.a);
    }
    
    @Nullable
    public final Object invokeSuspend(@NotNull final Object o) {
        final Object d = gg0.d();
        final int label = this.label;
        if (label != 0) {
            if (label != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            xe1.b(o);
        }
        else {
            xe1.b(o);
            final s81 s81 = (s81)this.L$0;
            final b b = new b(s81);
            final c c = new c(s81);
            final d d2 = new d(s81);
            this.$this_taskState.s(b);
            this.$this_taskState.r(c);
            this.$this_taskState.m((OnCompleteListener)d2);
            final a90 a90 = (a90)new a90(this.$this_taskState, b, c, d2) {
                final OnCompleteListener<zq1.b> $completionListener;
                final n11 $pauseListener;
                final o11 $progressListener;
                final zq1 $this_taskState;
                
                public final void invoke() {
                    this.$this_taskState.a0(this.$progressListener);
                    this.$this_taskState.Z(this.$pauseListener);
                    this.$this_taskState.Y(this.$completionListener);
                }
            };
            this.label = 1;
            if (ProduceKt.a(s81, (a90)a90, (vl)this) == d) {
                return d;
            }
        }
        return u02.a;
    }
}

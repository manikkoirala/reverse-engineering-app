// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.storage;

import java.io.IOException;

class CancelException extends IOException
{
    public CancelException() {
        super("The operation was canceled.");
    }
}

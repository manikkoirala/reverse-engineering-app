// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase;

import android.os.Build;
import com.google.firebase.heartbeatinfo.a;
import java.util.ArrayList;
import java.util.List;
import android.os.Build$VERSION;
import android.content.pm.ApplicationInfo;
import android.content.Context;
import com.google.firebase.components.ComponentRegistrar;

public class FirebaseCommonRegistrar implements ComponentRegistrar
{
    public static String i(final String s) {
        return s.replace(' ', '_').replace('/', '_');
    }
    
    @Override
    public List getComponents() {
        final ArrayList list = new ArrayList();
        list.add(ir.c());
        list.add(a.g());
        list.add(mj0.b("fire-android", String.valueOf(Build$VERSION.SDK_INT)));
        list.add(mj0.b("fire-core", "20.4.2"));
        list.add(mj0.b("device-name", i(Build.PRODUCT)));
        list.add(mj0.b("device-model", i(Build.DEVICE)));
        list.add(mj0.b("device-brand", i(Build.BRAND)));
        list.add(mj0.c("android-target-sdk", (mj0.a)new f20()));
        list.add(mj0.c("android-min-sdk", (mj0.a)new g20()));
        list.add(mj0.c("android-platform", (mj0.a)new h20()));
        list.add(mj0.c("android-installer", (mj0.a)new i20()));
        final String a = pi0.a();
        if (a != null) {
            list.add(mj0.b("kotlin", a));
        }
        return list;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.ktx;

import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import kotlin.coroutines.jvm.internal.ContinuationImpl;

public final class FirestoreKt$dataObjects$$inlined$map$2$1 extends ContinuationImpl
{
    int label;
    Object result;
    final p40 this$0;
    
    public FirestoreKt$dataObjects$$inlined$map$2$1(final p40 p2, final vl vl) {
        super(vl);
    }
    
    @Nullable
    public final Object invokeSuspend(@NotNull final Object result) {
        this.result = result;
        this.label |= Integer.MIN_VALUE;
        throw null;
    }
}

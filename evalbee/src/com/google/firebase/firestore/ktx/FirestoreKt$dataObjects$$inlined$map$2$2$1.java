// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.ktx;

import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import kotlin.coroutines.jvm.internal.ContinuationImpl;

@xp(c = "com.google.firebase.firestore.ktx.FirestoreKt$dataObjects$$inlined$map$2$2", f = "Firestore.kt", l = { 224 }, m = "emit")
public final class FirestoreKt$dataObjects$$inlined$map$2$2$1 extends ContinuationImpl
{
    Object L$0;
    int label;
    Object result;
    final n40 this$0;
    
    public FirestoreKt$dataObjects$$inlined$map$2$2$1(final n40 n40, final vl vl) {
        super(vl);
    }
    
    @Nullable
    public final Object invokeSuspend(@NotNull final Object result) {
        this.result = result;
        this.label |= Integer.MIN_VALUE;
        throw null;
    }
}

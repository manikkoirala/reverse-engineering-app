// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore;

import java.util.Arrays;
import java.util.List;
import com.google.firebase.heartbeatinfo.HeartBeatInfo;
import android.content.Context;
import androidx.annotation.Keep;
import com.google.firebase.components.ComponentRegistrar;

@Keep
public class FirestoreRegistrar implements ComponentRegistrar
{
    private static final String LIBRARY_NAME = "fire-fst";
    
    @Keep
    @Override
    public List<zi> getComponents() {
        return Arrays.asList(zi.e(e.class).h("fire-fst").b(os.k(r10.class)).b(os.k(Context.class)).b(os.i(HeartBeatInfo.class)).b(os.i(v12.class)).b(os.a(zf0.class)).b(os.a(dg0.class)).b(os.h(e30.class)).f(new r40()).d(), mj0.b("fire-fst", "24.10.0"));
    }
}

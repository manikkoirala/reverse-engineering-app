// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.core;

public class LimboDocumentChange
{
    public final Type a;
    public final du b;
    
    public LimboDocumentChange(final Type a, final du b) {
        this.a = a;
        this.b = b;
    }
    
    public du a() {
        return this.b;
    }
    
    public Type b() {
        return this.a;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof LimboDocumentChange;
        final boolean b2 = false;
        if (!b) {
            return false;
        }
        final LimboDocumentChange limboDocumentChange = (LimboDocumentChange)o;
        boolean b3 = b2;
        if (this.a.equals(limboDocumentChange.b())) {
            b3 = b2;
            if (this.b.equals(limboDocumentChange.a())) {
                b3 = true;
            }
        }
        return b3;
    }
    
    @Override
    public int hashCode() {
        return (2077 + this.a.hashCode()) * 31 + this.b.hashCode();
    }
    
    public enum Type
    {
        private static final Type[] $VALUES;
        
        ADDED, 
        REMOVED;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.core;

import com.google.firebase.firestore.local.QueryPurpose;
import com.google.firebase.firestore.util.Logger;
import com.google.protobuf.ByteString;
import com.google.firebase.firestore.FirebaseFirestoreException;
import io.grpc.Status$Code;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.database.collection.b;
import java.util.Set;
import com.google.firebase.firestore.model.MutableDocument;
import java.util.Collections;
import io.grpc.Status;
import com.google.firebase.database.collection.c;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import com.google.firebase.firestore.local.a;
import com.google.firebase.firestore.remote.g;

public class p implements g.c
{
    public static final String o = "p";
    public final a a;
    public final g b;
    public final Map c;
    public final Map d;
    public final int e;
    public final LinkedHashSet f;
    public final Map g;
    public final Map h;
    public final xc1 i;
    public final Map j;
    public final Map k;
    public final bu1 l;
    public u12 m;
    public c n;
    
    public p(final a a, final g b, final u12 m, final int e) {
        this.a = a;
        this.b = b;
        this.e = e;
        this.c = new HashMap();
        this.d = new HashMap();
        this.f = new LinkedHashSet();
        this.g = new HashMap();
        this.h = new HashMap();
        this.i = new xc1();
        this.j = new HashMap();
        this.l = bu1.a();
        this.m = m;
        this.k = new HashMap();
    }
    
    @Override
    public void a(final OnlineState onlineState) {
        this.h("handleOnlineStateChange");
        final ArrayList list = new ArrayList();
        final Iterator iterator = this.c.entrySet().iterator();
        while (iterator.hasNext()) {
            final m32 e = ((Map.Entry<K, ka1>)iterator.next()).getValue().c().e(onlineState);
            g9.d(e.a().isEmpty(), "OnlineState should not affect limbo documents.", new Object[0]);
            if (e.b() != null) {
                list.add(e.b());
            }
        }
        this.n.c(list);
        this.n.a(onlineState);
    }
    
    @Override
    public com.google.firebase.database.collection.c b(final int i) {
        final b b = this.h.get(i);
        if (b != null && p.b.a(b)) {
            return du.e().c(p.b.c(b));
        }
        com.google.firebase.database.collection.c e;
        com.google.firebase.database.collection.c l = e = du.e();
        if (this.d.containsKey(i)) {
            final Iterator iterator = this.d.get(i).iterator();
            while (true) {
                e = l;
                if (!iterator.hasNext()) {
                    break;
                }
                final Query query = (Query)iterator.next();
                if (!this.c.containsKey(query)) {
                    continue;
                }
                l = l.l(((ka1)this.c.get(query)).c().k());
            }
        }
        return e;
    }
    
    @Override
    public void c(final int n, final Status status) {
        this.h("handleRejectedListen");
        final b b = this.h.get(n);
        du c;
        if (b != null) {
            c = p.b.c(b);
        }
        else {
            c = null;
        }
        if (c != null) {
            this.g.remove(c);
            this.h.remove(n);
            this.q();
            final qo1 b2 = qo1.b;
            this.d(new jd1(b2, Collections.emptyMap(), Collections.emptyMap(), Collections.singletonMap(c, MutableDocument.r(c, b2)), Collections.singleton(c)));
        }
        else {
            this.a.P(n);
            this.r(n, status);
        }
    }
    
    @Override
    public void d(final jd1 jd1) {
        this.h("handleRemoteEvent");
        for (final Map.Entry<Integer, V> entry : jd1.d().entrySet()) {
            final Integer n = entry.getKey();
            final zt1 zt1 = (zt1)entry.getValue();
            final b b = this.h.get(n);
            if (b != null) {
                g9.d(zt1.b().size() + zt1.c().size() + zt1.d().size() <= 1, "Limbo resolution for single document contains multiple changes.", new Object[0]);
                if (zt1.b().size() > 0) {
                    p.b.b(b, true);
                }
                else if (zt1.c().size() > 0) {
                    g9.d(p.b.a(b), "Received change for limbo target document without add.", new Object[0]);
                }
                else {
                    if (zt1.d().size() <= 0) {
                        continue;
                    }
                    g9.d(p.b.a(b), "Received remove for limbo target document without add.", new Object[0]);
                    p.b.b(b, false);
                }
            }
        }
        this.i(this.a.n(jd1), jd1);
    }
    
    @Override
    public void e(final yx0 yx0) {
        this.h("handleSuccessfulWrite");
        this.p(yx0.b().e(), null);
        this.t(yx0.b().e());
        this.i(this.a.l(yx0), null);
    }
    
    @Override
    public void f(final int n, final Status status) {
        this.h("handleRejectedWrite");
        final com.google.firebase.database.collection.b o = this.a.O(n);
        if (!o.isEmpty()) {
            this.o(status, "Write failed at %s", ((du)o.i()).m());
        }
        this.p(n, status);
        this.t(n);
        this.i(o, null);
    }
    
    public final void g(final int i, final TaskCompletionSource taskCompletionSource) {
        Map map;
        if ((map = this.j.get(this.m)) == null) {
            map = new HashMap();
            this.j.put(this.m, map);
        }
        map.put(i, taskCompletionSource);
    }
    
    public final void h(final String s) {
        g9.d(this.n != null, "Trying to call %s before setting callback", s);
    }
    
    public final void i(final com.google.firebase.database.collection.b b, final jd1 jd1) {
        final ArrayList list = new ArrayList();
        final ArrayList list2 = new ArrayList();
        final Iterator iterator = this.c.entrySet().iterator();
        while (iterator.hasNext()) {
            final ka1 ka1 = ((Map.Entry<K, ka1>)iterator.next()).getValue();
            final r c = ka1.c();
            final r.b h = c.h(b);
            final boolean b2 = h.b();
            final boolean b3 = false;
            r.b i = h;
            if (b2) {
                i = c.i(this.a.q(ka1.a(), false).a(), h);
            }
            zt1 zt1;
            if (jd1 == null) {
                zt1 = null;
            }
            else {
                zt1 = jd1.d().get(ka1.b());
            }
            boolean b4 = b3;
            if (jd1 != null) {
                b4 = b3;
                if (jd1.e().get(ka1.b()) != null) {
                    b4 = true;
                }
            }
            final m32 d = ka1.c().d(i, zt1, b4);
            this.x(d.a(), ka1.b());
            if (d.b() != null) {
                list.add(d.b());
                list2.add(ml0.a(ka1.b(), d.b()));
            }
        }
        this.n.c(list);
        this.a.L(list2);
    }
    
    public final boolean j(final Status status) {
        final Status$Code code = status.getCode();
        String description;
        if (status.getDescription() != null) {
            description = status.getDescription();
        }
        else {
            description = "";
        }
        return (code == Status$Code.FAILED_PRECONDITION && description.contains("requires an index")) || code == Status$Code.PERMISSION_DENIED;
    }
    
    public final void k() {
        final Iterator iterator = this.k.entrySet().iterator();
        while (iterator.hasNext()) {
            final Iterator iterator2 = ((Map.Entry<K, List>)iterator.next()).getValue().iterator();
            while (iterator2.hasNext()) {
                ((TaskCompletionSource)iterator2.next()).setException((Exception)new FirebaseFirestoreException("'waitForPendingWrites' task is cancelled due to User change.", FirebaseFirestoreException.Code.CANCELLED));
            }
        }
        this.k.clear();
    }
    
    public void l(final u12 m) {
        final boolean equals = this.m.equals(m);
        this.m = m;
        if (equals ^ true) {
            this.k();
            this.i(this.a.y(m), null);
        }
        this.b.t();
    }
    
    public final ViewSnapshot m(final Query query, final int i, final ByteString byteString) {
        final ja1 q = this.a.q(query, true);
        Enum<ViewSnapshot.SyncState> enum1 = ViewSnapshot.SyncState.NONE;
        final Object value = this.d.get(i);
        boolean b = false;
        if (value != null) {
            enum1 = this.c.get(this.d.get(i).get(0)).c().j();
        }
        if (enum1 == ViewSnapshot.SyncState.SYNCED) {
            b = true;
        }
        final zt1 a = zt1.a(b, byteString);
        final r r = new r(query, q.b());
        final m32 c = r.c(r.h(q.a()), a);
        this.x(c.a(), i);
        this.c.put(query, new ka1(query, i, r));
        if (!this.d.containsKey(i)) {
            this.d.put(i, new ArrayList(1));
        }
        this.d.get(i).add(query);
        return c.b();
    }
    
    public int n(final Query query) {
        this.h("listen");
        g9.d(this.c.containsKey(query) ^ true, "We already listen to query: %s", query);
        final au1 m = this.a.m(query.x());
        this.n.c(Collections.singletonList(this.m(query, m.h(), m.d())));
        this.b.E(m);
        return m.h();
    }
    
    public final void o(final Status status, final String format, final Object... args) {
        if (this.j(status)) {
            Logger.d("Firestore", "%s: %s", String.format(format, args), status);
        }
    }
    
    public final void p(final int i, final Status status) {
        final Map map = this.j.get(this.m);
        if (map != null) {
            final Integer value = i;
            final TaskCompletionSource taskCompletionSource = (TaskCompletionSource)map.get(value);
            if (taskCompletionSource != null) {
                if (status != null) {
                    taskCompletionSource.setException((Exception)o22.r(status));
                }
                else {
                    taskCompletionSource.setResult((Object)null);
                }
                map.remove(value);
            }
        }
    }
    
    public final void q() {
        while (!this.f.isEmpty() && this.g.size() < this.e) {
            final Iterator iterator = this.f.iterator();
            final du du = (du)iterator.next();
            iterator.remove();
            final int c = this.l.c();
            this.h.put(c, new b(du));
            this.g.put(du, c);
            this.b.E(new au1(Query.b(du.m()).x(), c, -1L, QueryPurpose.LIMBO_RESOLUTION));
        }
    }
    
    public final void r(final int n, final Status status) {
        for (final Query query : this.d.get(n)) {
            this.c.remove(query);
            if (!status.isOk()) {
                this.n.b(query, status);
                this.o(status, "Listen for %s failed", query);
            }
        }
        this.d.remove(n);
        final com.google.firebase.database.collection.c d = this.i.d(n);
        this.i.h(n);
        for (final du du : d) {
            if (!this.i.c(du)) {
                this.s(du);
            }
        }
    }
    
    public final void s(final du o) {
        this.f.remove(o);
        final Integer n = this.g.get(o);
        if (n != null) {
            this.b.P(n);
            this.g.remove(o);
            this.h.remove(n);
            this.q();
        }
    }
    
    public final void t(final int i) {
        if (this.k.containsKey(i)) {
            final Iterator iterator = this.k.get(i).iterator();
            while (iterator.hasNext()) {
                ((TaskCompletionSource)iterator.next()).setResult((Object)null);
            }
            this.k.remove(i);
        }
    }
    
    public void u(final c n) {
        this.n = n;
    }
    
    public void v(final Query query) {
        this.h("stopListening");
        final ka1 ka1 = this.c.get(query);
        g9.d(ka1 != null, "Trying to stop listening to a query not found", new Object[0]);
        this.c.remove(query);
        final int b = ka1.b();
        final List list = this.d.get(b);
        list.remove(query);
        if (list.isEmpty()) {
            this.a.P(b);
            this.b.P(b);
            this.r(b, Status.OK);
        }
    }
    
    public final void w(final LimboDocumentChange limboDocumentChange) {
        final du a = limboDocumentChange.a();
        if (!this.g.containsKey(a) && !this.f.contains(a)) {
            Logger.a(p.o, "New document in limbo: %s", a);
            this.f.add(a);
            this.q();
        }
    }
    
    public final void x(final List list, final int n) {
        for (final LimboDocumentChange limboDocumentChange : list) {
            final int n2 = p$a.a[limboDocumentChange.b().ordinal()];
            if (n2 != 1) {
                if (n2 != 2) {
                    throw g9.a("Unknown limbo change type: %s", limboDocumentChange.b());
                }
                Logger.a(p.o, "Document no longer in limbo: %s", limboDocumentChange.a());
                final du a = limboDocumentChange.a();
                this.i.e(a, n);
                if (this.i.c(a)) {
                    continue;
                }
                this.s(a);
            }
            else {
                this.i.a(limboDocumentChange.a(), n);
                this.w(limboDocumentChange);
            }
        }
    }
    
    public void y(final List list, final TaskCompletionSource taskCompletionSource) {
        this.h("writeMutations");
        final wk0 v = this.a.V(list);
        this.g(v.b(), taskCompletionSource);
        this.i(v.c(), null);
        this.b.s();
    }
    
    public static class b
    {
        public final du a;
        public boolean b;
        
        public b(final du a) {
            this.a = a;
        }
        
        public static /* synthetic */ boolean a(final b b) {
            return b.b;
        }
        
        public static /* synthetic */ boolean b(final b b, final boolean b2) {
            return b.b = b2;
        }
        
        public static /* synthetic */ du c(final b b) {
            return b.a;
        }
    }
    
    public interface c
    {
        void a(final OnlineState p0);
        
        void b(final Query p0, final Status p1);
        
        void c(final List p0);
    }
}

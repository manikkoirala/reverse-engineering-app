// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.core;

import java.util.ArrayList;
import com.google.firebase.firestore.FirebaseFirestoreException;
import java.util.List;
import io.grpc.Status;
import java.util.Iterator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Map;

public final class f implements c
{
    public final p a;
    public final Map b;
    public final Set c;
    public OnlineState d;
    
    public f(final p a) {
        this.c = new HashSet();
        this.d = OnlineState.UNKNOWN;
        this.a = a;
        this.b = new HashMap();
        a.u((p.c)this);
    }
    
    @Override
    public void a(final OnlineState d) {
        this.d = d;
        final Iterator iterator = this.b.values().iterator();
        int n = 0;
        while (iterator.hasNext()) {
            final Iterator iterator2 = f.b.a((b)iterator.next()).iterator();
            int n2 = n;
            while (true) {
                n = n2;
                if (!iterator2.hasNext()) {
                    break;
                }
                if (!((n)iterator2.next()).c(d)) {
                    continue;
                }
                n2 = 1;
            }
        }
        if (n != 0) {
            this.e();
        }
    }
    
    @Override
    public void b(final Query query, final Status status) {
        final b b = this.b.get(query);
        if (b != null) {
            final Iterator iterator = f.b.a(b).iterator();
            while (iterator.hasNext()) {
                ((n)iterator.next()).b(o22.r(status));
            }
        }
        this.b.remove(query);
    }
    
    @Override
    public void c(final List list) {
        final Iterator iterator = list.iterator();
        boolean b = false;
        while (iterator.hasNext()) {
            final ViewSnapshot viewSnapshot = (ViewSnapshot)iterator.next();
            final b b2 = this.b.get(viewSnapshot.h());
            if (b2 != null) {
                final Iterator iterator2 = f.b.a(b2).iterator();
                while (iterator2.hasNext()) {
                    if (((n)iterator2.next()).d(viewSnapshot)) {
                        b = true;
                    }
                }
                f.b.c(b2, viewSnapshot);
            }
        }
        if (b) {
            this.e();
        }
    }
    
    public int d(final n n) {
        final Query a = n.a();
        b b = this.b.get(a);
        final boolean b2 = b == null;
        if (b2) {
            b = new b();
            this.b.put(a, b);
        }
        f.b.a(b).add(n);
        g9.d(true ^ n.c(this.d), "onOnlineStateChanged() shouldn't raise an event for brand-new listeners.", new Object[0]);
        if (f.b.b(b) != null && n.d(f.b.b(b))) {
            this.e();
        }
        if (b2) {
            f.b.e(b, this.a.n(a));
        }
        return f.b.d(b);
    }
    
    public final void e() {
        final Iterator iterator = this.c.iterator();
        while (iterator.hasNext()) {
            ((rx)iterator.next()).a(null, null);
        }
    }
    
    public void f(final n n) {
        final Query a = n.a();
        final b b = this.b.get(a);
        boolean empty;
        if (b != null) {
            f.b.a(b).remove(n);
            empty = f.b.a(b).isEmpty();
        }
        else {
            empty = false;
        }
        if (empty) {
            this.b.remove(a);
            this.a.v(a);
        }
    }
    
    public static class a
    {
        public boolean a;
        public boolean b;
        public boolean c;
    }
    
    public static class b
    {
        public final List a;
        public ViewSnapshot b;
        public int c;
        
        public b() {
            this.a = new ArrayList();
        }
        
        public static /* synthetic */ List a(final b b) {
            return b.a;
        }
        
        public static /* synthetic */ ViewSnapshot b(final b b) {
            return b.b;
        }
        
        public static /* synthetic */ ViewSnapshot c(final b b, final ViewSnapshot b2) {
            return b.b = b2;
        }
        
        public static /* synthetic */ int d(final b b) {
            return b.c;
        }
        
        public static /* synthetic */ int e(final b b, final int c) {
            return b.c = c;
        }
    }
}

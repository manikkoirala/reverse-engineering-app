// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.core;

import java.util.Iterator;
import com.google.firestore.v1.Value;
import java.util.List;

public final class c
{
    public final boolean a;
    public final List b;
    
    public c(final List b, final boolean a) {
        this.b = b;
        this.a = a;
    }
    
    public final int a(final List list, final zt zt) {
        g9.d(this.b.size() <= list.size(), "Bound has more components than query's orderBy", new Object[0]);
        int i = 0;
        int n = 0;
        while (i < this.b.size()) {
            final OrderBy orderBy = list.get(i);
            final Value value = this.b.get(i);
            int n2;
            if (orderBy.b.equals(s00.b)) {
                g9.d(a32.B(value), "Bound has a non-key value where the key path is being used %s", value);
                n2 = du.f(value.t0()).c(zt.getKey());
            }
            else {
                final Value j = zt.j(orderBy.c());
                g9.d(j != null, "Field should exist since document matched the orderBy already.", new Object[0]);
                n2 = a32.i(value, j);
            }
            int n3 = n2;
            if (orderBy.b().equals(OrderBy.Direction.DESCENDING)) {
                n3 = n2 * -1;
            }
            n = n3;
            if (n != 0) {
                break;
            }
            ++i;
        }
        return n;
    }
    
    public List b() {
        return this.b;
    }
    
    public boolean c() {
        return this.a;
    }
    
    public String d() {
        final StringBuilder sb = new StringBuilder();
        final Iterator iterator = this.b.iterator();
        int n = 1;
        while (iterator.hasNext()) {
            final Value value = (Value)iterator.next();
            if (n == 0) {
                sb.append(",");
            }
            sb.append(a32.b(value));
            n = 0;
        }
        return sb.toString();
    }
    
    public boolean e(final List list, final zt zt) {
        final int a = this.a(list, zt);
        final boolean a2 = this.a;
        boolean b = true;
        if (a2) {
            if (a >= 0) {
                return b;
            }
        }
        else if (a > 0) {
            return b;
        }
        b = false;
        return b;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && c.class == o.getClass()) {
            final c c = (c)o;
            if (this.a != c.a || !this.b.equals(c.b)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    public boolean f(final List list, final zt zt) {
        final int a = this.a(list, zt);
        final boolean a2 = this.a;
        boolean b = true;
        if (a2) {
            if (a <= 0) {
                return b;
            }
        }
        else if (a < 0) {
            return b;
        }
        b = false;
        return b;
    }
    
    @Override
    public int hashCode() {
        return (this.a ? 1 : 0) * 31 + this.b.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Bound(inclusive=");
        sb.append(this.a);
        sb.append(", position=");
        for (int i = 0; i < this.b.size(); ++i) {
            if (i > 0) {
                sb.append(" and ");
            }
            sb.append(a32.b((Value)this.b.get(i)));
        }
        sb.append(")");
        return sb.toString();
    }
}

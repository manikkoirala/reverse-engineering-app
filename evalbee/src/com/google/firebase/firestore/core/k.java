// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.core;

import java.util.Collection;
import java.util.ArrayList;
import com.google.firestore.v1.Value;
import java.util.List;

public class k extends FieldFilter
{
    public final List d;
    
    public k(final s00 s00, final Value value) {
        final Operator not_IN = Operator.NOT_IN;
        super(s00, not_IN, value);
        (this.d = new ArrayList()).addAll(j.k(not_IN, value));
    }
    
    @Override
    public boolean d(final zt zt) {
        return this.d.contains(zt.getKey()) ^ true;
    }
}

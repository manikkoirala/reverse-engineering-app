// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.core;

import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

public class e
{
    public final TreeMap a;
    
    public e() {
        this.a = new TreeMap();
    }
    
    public void a(DocumentViewChange documentViewChange) {
        final du key = documentViewChange.b().getKey();
        final DocumentViewChange documentViewChange2 = this.a.get(key);
        if (documentViewChange2 == null) {
            this.a.put(key, documentViewChange);
            return;
        }
        final DocumentViewChange.Type c = documentViewChange2.c();
        final DocumentViewChange.Type c2 = documentViewChange.c();
        final DocumentViewChange.Type added = DocumentViewChange.Type.ADDED;
        Label_0072: {
            if (c2 == added || c != DocumentViewChange.Type.METADATA) {
                if (c2 == DocumentViewChange.Type.METADATA && c != DocumentViewChange.Type.REMOVED) {
                    documentViewChange = DocumentViewChange.a(c, documentViewChange.b());
                }
                else {
                    final DocumentViewChange.Type modified = DocumentViewChange.Type.MODIFIED;
                    if (c2 != modified || c != modified) {
                        if (c2 == modified && c == added) {
                            documentViewChange = DocumentViewChange.a(added, documentViewChange.b());
                            break Label_0072;
                        }
                        final DocumentViewChange.Type removed = DocumentViewChange.Type.REMOVED;
                        if (c2 == removed && c == added) {
                            this.a.remove(key);
                            return;
                        }
                        if (c2 == removed && c == modified) {
                            documentViewChange = DocumentViewChange.a(removed, documentViewChange2.b());
                            break Label_0072;
                        }
                        if (c2 != added || c != removed) {
                            throw g9.a("Unsupported combination of changes %s after %s", c2, c);
                        }
                    }
                    documentViewChange = DocumentViewChange.a(modified, documentViewChange.b());
                }
            }
        }
        this.a.put(key, documentViewChange);
    }
    
    public List b() {
        return new ArrayList(this.a.values());
    }
}

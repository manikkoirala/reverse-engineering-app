// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.core;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Collection;
import java.util.ArrayList;
import com.google.firestore.v1.Value;
import android.util.Pair;
import java.util.Iterator;
import java.util.Collections;
import com.google.firebase.firestore.model.FieldIndex;
import java.util.List;

public final class q
{
    public String a;
    public final List b;
    public final List c;
    public final ke1 d;
    public final String e;
    public final long f;
    public final c g;
    public final c h;
    
    public q(final ke1 d, final String e, final List c, final List b, final long f, final c g, final c h) {
        this.d = d;
        this.e = e;
        this.b = b;
        this.c = c;
        this.f = f;
        this.g = g;
        this.h = h;
    }
    
    public List a(final FieldIndex fieldIndex) {
        final FieldIndex.Segment c = fieldIndex.c();
        if (c == null) {
            return null;
        }
        for (final FieldFilter fieldFilter : this.g(c.d())) {
            final int n = q$a.a[fieldFilter.g().ordinal()];
            if (n == 1) {
                return fieldFilter.h().l0().h();
            }
            if (n != 2) {
                continue;
            }
            return Collections.singletonList(fieldFilter.h());
        }
        return null;
    }
    
    public final Pair b(final FieldIndex.Segment segment, final c c) {
        Value c2 = a32.c;
        final Iterator iterator = this.g(segment.d()).iterator();
        int n = 1;
        int n2;
        while (true) {
            final boolean hasNext = iterator.hasNext();
            n2 = 0;
            int n3 = 0;
            if (!hasNext) {
                break;
            }
            final FieldFilter fieldFilter = (FieldFilter)iterator.next();
            Value value2;
            final Value value = value2 = a32.c;
            Label_0124: {
                switch (q$a.a[fieldFilter.g().ordinal()]) {
                    default: {
                        value2 = value;
                        break Label_0124;
                    }
                    case 3:
                    case 4:
                    case 9: {
                        value2 = fieldFilter.h();
                        break Label_0124;
                    }
                    case 7:
                    case 8: {
                        value2 = a32.r(fieldFilter.h().w0());
                    }
                    case 5:
                    case 6: {
                        n3 = 1;
                        break;
                    }
                    case 10: {
                        value2 = fieldFilter.h();
                        break;
                    }
                }
            }
            if (a32.C(c2, (boolean)(n != 0), value2, (boolean)(n3 != 0)) >= 0) {
                continue;
            }
            n = n3;
            c2 = value2;
        }
        Value value3 = c2;
        int c3 = n;
        if (c != null) {
            while (true) {
                value3 = c2;
                c3 = n;
                if (n2 >= this.b.size()) {
                    break;
                }
                if (((OrderBy)this.b.get(n2)).c().equals(segment.d())) {
                    final Value value4 = c.b().get(n2);
                    value3 = c2;
                    c3 = n;
                    if (a32.C(c2, (boolean)(n != 0), value4, c.c()) < 0) {
                        c3 = (c.c() ? 1 : 0);
                        value3 = value4;
                        break;
                    }
                    break;
                }
                else {
                    ++n2;
                }
            }
        }
        return new Pair((Object)value3, (Object)(boolean)(c3 != 0));
    }
    
    public String c() {
        final String a = this.a;
        if (a != null) {
            return a;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(this.n().d());
        if (this.e != null) {
            sb.append("|cg:");
            sb.append(this.e);
        }
        sb.append("|f:");
        final Iterator iterator = this.h().iterator();
        while (iterator.hasNext()) {
            sb.append(((i10)iterator.next()).a());
        }
        sb.append("|ob:");
        for (final OrderBy orderBy : this.m()) {
            sb.append(orderBy.c().d());
            String str;
            if (orderBy.b().equals(OrderBy.Direction.ASCENDING)) {
                str = "asc";
            }
            else {
                str = "desc";
            }
            sb.append(str);
        }
        if (this.r()) {
            sb.append("|l:");
            sb.append(this.j());
        }
        final c g = this.g;
        final String s = "b:";
        if (g != null) {
            sb.append("|lb:");
            String str2;
            if (this.g.c()) {
                str2 = "b:";
            }
            else {
                str2 = "a:";
            }
            sb.append(str2);
            sb.append(this.g.d());
        }
        if (this.h != null) {
            sb.append("|ub:");
            String str3 = s;
            if (this.h.c()) {
                str3 = "a:";
            }
            sb.append(str3);
            sb.append(this.h.d());
        }
        return this.a = sb.toString();
    }
    
    public String d() {
        return this.e;
    }
    
    public final Pair e(final FieldIndex.Segment segment, final c c) {
        Value e = a32.e;
        final Iterator iterator = this.g(segment.d()).iterator();
        int n = 1;
        int n2;
        while (true) {
            final boolean hasNext = iterator.hasNext();
            n2 = 0;
            int n3 = 0;
            if (!hasNext) {
                break;
            }
            final FieldFilter fieldFilter = (FieldFilter)iterator.next();
            Value value2;
            final Value value = value2 = a32.e;
            switch (q$a.a[fieldFilter.g().ordinal()]) {
                case 3:
                case 4:
                case 8: {
                    value2 = fieldFilter.h();
                }
                default: {
                    value2 = value;
                }
                case 5:
                case 6: {
                    n3 = 1;
                    break;
                }
                case 9:
                case 10: {
                    value2 = a32.s(fieldFilter.h().w0());
                    break;
                }
                case 7: {
                    value2 = fieldFilter.h();
                    break;
                }
            }
            if (a32.H(e, (boolean)(n != 0), value2, (boolean)(n3 != 0)) <= 0) {
                continue;
            }
            n = n3;
            e = value2;
        }
        Value value3 = e;
        int c2 = n;
        if (c != null) {
            while (true) {
                value3 = e;
                c2 = n;
                if (n2 >= this.b.size()) {
                    break;
                }
                if (((OrderBy)this.b.get(n2)).c().equals(segment.d())) {
                    final Value value4 = c.b().get(n2);
                    value3 = e;
                    c2 = n;
                    if (a32.H(e, (boolean)(n != 0), value4, c.c()) > 0) {
                        c2 = (c.c() ? 1 : 0);
                        value3 = value4;
                        break;
                    }
                    break;
                }
                else {
                    ++n2;
                }
            }
        }
        return new Pair((Object)value3, (Object)(boolean)(c2 != 0));
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean equals = true;
        if (this == o) {
            return true;
        }
        if (o == null || q.class != o.getClass()) {
            return false;
        }
        final q q = (q)o;
        final String e = this.e;
        Label_0062: {
            if (e != null) {
                if (e.equals(q.e)) {
                    break Label_0062;
                }
            }
            else if (q.e == null) {
                break Label_0062;
            }
            return false;
        }
        if (this.f != q.f) {
            return false;
        }
        if (!this.b.equals(q.b)) {
            return false;
        }
        if (!this.c.equals(q.c)) {
            return false;
        }
        if (!this.d.equals(q.d)) {
            return false;
        }
        final c g = this.g;
        Label_0160: {
            if (g != null) {
                if (g.equals(q.g)) {
                    break Label_0160;
                }
            }
            else if (q.g == null) {
                break Label_0160;
            }
            return false;
        }
        final c h = this.h;
        final c h2 = q.h;
        if (h != null) {
            equals = h.equals(h2);
        }
        else if (h2 != null) {
            equals = false;
        }
        return equals;
    }
    
    public c f() {
        return this.h;
    }
    
    public final List g(final s00 s00) {
        final ArrayList list = new ArrayList();
        for (final i10 i10 : this.c) {
            if (i10 instanceof FieldFilter) {
                final FieldFilter fieldFilter = (FieldFilter)i10;
                if (!fieldFilter.f().equals(s00)) {
                    continue;
                }
                list.add(fieldFilter);
            }
        }
        return list;
    }
    
    public List h() {
        return this.c;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.b.hashCode();
        final String e = this.e;
        int hashCode2 = 0;
        int hashCode3;
        if (e != null) {
            hashCode3 = e.hashCode();
        }
        else {
            hashCode3 = 0;
        }
        final int hashCode4 = this.c.hashCode();
        final int hashCode5 = this.d.hashCode();
        final long f = this.f;
        final int n = (int)(f ^ f >>> 32);
        final c g = this.g;
        int hashCode6;
        if (g != null) {
            hashCode6 = g.hashCode();
        }
        else {
            hashCode6 = 0;
        }
        final c h = this.h;
        if (h != null) {
            hashCode2 = h.hashCode();
        }
        return (((((hashCode * 31 + hashCode3) * 31 + hashCode4) * 31 + hashCode5) * 31 + n) * 31 + hashCode6) * 31 + hashCode2;
    }
    
    public OrderBy.Direction i() {
        final List b = this.b;
        return ((OrderBy)b.get(b.size() - 1)).b();
    }
    
    public long j() {
        return this.f;
    }
    
    public c k(final FieldIndex fieldIndex) {
        final ArrayList list = new ArrayList();
        final Iterator iterator = fieldIndex.e().iterator();
        boolean b = true;
        while (iterator.hasNext()) {
            final FieldIndex.Segment segment = (FieldIndex.Segment)iterator.next();
            Pair pair;
            if (segment.e().equals(FieldIndex.Segment.Kind.ASCENDING)) {
                pair = this.b(segment, this.g);
            }
            else {
                pair = this.e(segment, this.g);
            }
            list.add(pair.first);
            b &= (boolean)pair.second;
        }
        return new c(list, b);
    }
    
    public Collection l(final FieldIndex fieldIndex) {
        final LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (final FieldIndex.Segment segment : fieldIndex.e()) {
            for (final FieldFilter fieldFilter : this.g(segment.d())) {
                final int n = q$a.a[fieldFilter.g().ordinal()];
                if (n != 3 && n != 4) {
                    if (n != 5 && n != 6) {
                        continue;
                    }
                    linkedHashMap.put(segment.d(), fieldFilter.h());
                    return linkedHashMap.values();
                }
                else {
                    linkedHashMap.put(segment.d(), fieldFilter.h());
                }
            }
        }
        return null;
    }
    
    public List m() {
        return this.b;
    }
    
    public ke1 n() {
        return this.d;
    }
    
    public int o() {
        final HashSet set = new HashSet();
        final Iterator iterator = this.c.iterator();
        int n = 0;
        while (iterator.hasNext()) {
            final Iterator iterator2 = ((i10)iterator.next()).c().iterator();
            int n2 = n;
            while (true) {
                n = n2;
                if (!iterator2.hasNext()) {
                    break;
                }
                final FieldFilter fieldFilter = (FieldFilter)iterator2.next();
                if (fieldFilter.f().s()) {
                    continue;
                }
                if (!fieldFilter.g().equals(FieldFilter.Operator.ARRAY_CONTAINS) && !fieldFilter.g().equals(FieldFilter.Operator.ARRAY_CONTAINS_ANY)) {
                    set.add(fieldFilter.f());
                }
                else {
                    n2 = 1;
                }
            }
        }
        for (final OrderBy orderBy : this.b) {
            if (!orderBy.c().s()) {
                set.add(orderBy.c());
            }
        }
        return set.size() + n;
    }
    
    public c p() {
        return this.g;
    }
    
    public c q(final FieldIndex fieldIndex) {
        final ArrayList list = new ArrayList();
        final Iterator iterator = fieldIndex.e().iterator();
        boolean b = true;
        while (iterator.hasNext()) {
            final FieldIndex.Segment segment = (FieldIndex.Segment)iterator.next();
            Pair pair;
            if (segment.e().equals(FieldIndex.Segment.Kind.ASCENDING)) {
                pair = this.e(segment, this.h);
            }
            else {
                pair = this.b(segment, this.h);
            }
            list.add(pair.first);
            b &= (boolean)pair.second;
        }
        return new c(list, b);
    }
    
    public boolean r() {
        return this.f != -1L;
    }
    
    public boolean s() {
        return du.o(this.d) && this.e == null && this.c.isEmpty();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Query(");
        sb.append(this.d.d());
        if (this.e != null) {
            sb.append(" collectionGroup=");
            sb.append(this.e);
        }
        final boolean empty = this.c.isEmpty();
        final int n = 0;
        if (!empty) {
            sb.append(" where ");
            for (int i = 0; i < this.c.size(); ++i) {
                if (i > 0) {
                    sb.append(" and ");
                }
                sb.append(this.c.get(i));
            }
        }
        if (!this.b.isEmpty()) {
            sb.append(" order by ");
            for (int j = n; j < this.b.size(); ++j) {
                if (j > 0) {
                    sb.append(", ");
                }
                sb.append(this.b.get(j));
            }
        }
        sb.append(")");
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.core;

import java.util.Iterator;
import com.google.firestore.v1.Value;

public class a extends FieldFilter
{
    public a(final s00 s00, final Value value) {
        super(s00, Operator.ARRAY_CONTAINS_ANY, value);
        g9.d(a32.t(value), "ArrayContainsAnyFilter expects an ArrayValue", new Object[0]);
    }
    
    @Override
    public boolean d(final zt zt) {
        final Value j = zt.j(this.f());
        if (!a32.t(j)) {
            return false;
        }
        final Iterator iterator = j.l0().h().iterator();
        while (iterator.hasNext()) {
            if (a32.p(this.h().l0(), (Value)iterator.next())) {
                return true;
            }
        }
        return false;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.core;

import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import com.google.firestore.v1.Value;
import java.util.List;

public class j extends FieldFilter
{
    public final List d;
    
    public j(final s00 s00, final Value value) {
        final Operator in = Operator.IN;
        super(s00, in, value);
        (this.d = new ArrayList()).addAll(k(in, value));
    }
    
    public static List k(final Operator operator, final Value value) {
        g9.d(operator == Operator.IN || operator == Operator.NOT_IN, "extractDocumentKeysFromArrayValue requires IN or NOT_IN operators", new Object[0]);
        g9.d(a32.t(value), "KeyFieldInFilter/KeyFieldNotInFilter expects an ArrayValue", new Object[0]);
        final ArrayList list = new ArrayList();
        for (final Value value2 : value.l0().h()) {
            final boolean b = a32.B(value2);
            final StringBuilder sb = new StringBuilder();
            sb.append("Comparing on key with ");
            sb.append(operator.toString());
            sb.append(", but an array value was not a ReferenceValue");
            g9.d(b, sb.toString(), new Object[0]);
            list.add(du.f(value2.t0()));
        }
        return list;
    }
    
    @Override
    public boolean d(final zt zt) {
        return this.d.contains(zt.getKey());
    }
}

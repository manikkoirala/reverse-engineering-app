// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.core;

import com.google.firestore.v1.Value;

public class b extends FieldFilter
{
    public b(final s00 s00, final Value value) {
        super(s00, Operator.ARRAY_CONTAINS, value);
    }
    
    @Override
    public boolean d(final zt zt) {
        final Value j = zt.j(this.f());
        return a32.t(j) && a32.p(j.l0(), this.h());
    }
}

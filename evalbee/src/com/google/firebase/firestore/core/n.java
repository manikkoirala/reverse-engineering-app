// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.core;

import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import com.google.firebase.firestore.FirebaseFirestoreException;

public class n
{
    public final Query a;
    public final f.a b;
    public final rx c;
    public boolean d;
    public OnlineState e;
    public ViewSnapshot f;
    
    public n(final Query a, final f.a b, final rx c) {
        this.d = false;
        this.e = OnlineState.UNKNOWN;
        this.a = a;
        this.c = c;
        this.b = b;
    }
    
    public Query a() {
        return this.a;
    }
    
    public void b(final FirebaseFirestoreException ex) {
        this.c.a(null, ex);
    }
    
    public boolean c(final OnlineState e) {
        this.e = e;
        final ViewSnapshot f = this.f;
        boolean b;
        if (f != null && !this.d && this.g(f, e)) {
            this.e(this.f);
            b = true;
        }
        else {
            b = false;
        }
        return b;
    }
    
    public boolean d(final ViewSnapshot viewSnapshot) {
        final boolean empty = viewSnapshot.d().isEmpty();
        final boolean b = false;
        g9.d(!empty || viewSnapshot.a(), "We got a new snapshot with no changes?", new Object[0]);
        ViewSnapshot f = viewSnapshot;
        if (!this.b.a) {
            final ArrayList list = new ArrayList();
            for (final DocumentViewChange documentViewChange : viewSnapshot.d()) {
                if (documentViewChange.c() != DocumentViewChange.Type.METADATA) {
                    list.add(documentViewChange);
                }
            }
            f = new ViewSnapshot(viewSnapshot.h(), viewSnapshot.e(), viewSnapshot.g(), list, viewSnapshot.k(), viewSnapshot.f(), viewSnapshot.a(), true, viewSnapshot.i());
        }
        boolean b2 = false;
        Label_0218: {
            if (!this.d) {
                b2 = b;
                if (!this.g(f, this.e)) {
                    break Label_0218;
                }
                this.e(f);
            }
            else {
                b2 = b;
                if (!this.f(f)) {
                    break Label_0218;
                }
                this.c.a(f, null);
            }
            b2 = true;
        }
        this.f = f;
        return b2;
    }
    
    public final void e(ViewSnapshot c) {
        g9.d(this.d ^ true, "Trying to raise initial event for second time", new Object[0]);
        c = ViewSnapshot.c(c.h(), c.e(), c.f(), c.k(), c.b(), c.i());
        this.d = true;
        this.c.a(c, null);
    }
    
    public final boolean f(final ViewSnapshot viewSnapshot) {
        final boolean empty = viewSnapshot.d().isEmpty();
        boolean b = true;
        if (!empty) {
            return true;
        }
        final ViewSnapshot f = this.f;
        if (f == null || f.j() == viewSnapshot.j()) {
            b = false;
        }
        return (viewSnapshot.a() || b) && this.b.b;
    }
    
    public final boolean g(final ViewSnapshot viewSnapshot, final OnlineState onlineState) {
        final boolean d = this.d;
        final boolean b = true;
        g9.d(d ^ true, "Determining whether to raise first event but already had first event.", new Object[0]);
        if (!viewSnapshot.k()) {
            return true;
        }
        final OnlineState offline = OnlineState.OFFLINE;
        final boolean equals = onlineState.equals(offline);
        if (this.b.c && (equals ^ true)) {
            g9.d(viewSnapshot.k(), "Waiting for sync, but snapshot is not from cache", new Object[0]);
            return false;
        }
        boolean b2 = b;
        if (viewSnapshot.e().isEmpty()) {
            b2 = b;
            if (!viewSnapshot.i()) {
                b2 = (onlineState.equals(offline) && b);
            }
        }
        return b2;
    }
}

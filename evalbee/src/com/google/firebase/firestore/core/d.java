// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.core;

import com.google.firebase.firestore.b;
import com.google.firebase.firestore.util.AsyncQueue;
import android.content.Context;
import com.google.firebase.firestore.remote.ConnectivityMonitor;
import com.google.firebase.firestore.remote.g;
import com.google.firebase.firestore.local.a;

public abstract class d
{
    public d51 a;
    public com.google.firebase.firestore.local.a b;
    public p c;
    public g d;
    public f e;
    public ConnectivityMonitor f;
    public we0 g;
    public hj1 h;
    
    public abstract ConnectivityMonitor a(final a p0);
    
    public abstract f b(final a p0);
    
    public abstract hj1 c(final a p0);
    
    public abstract we0 d(final a p0);
    
    public abstract com.google.firebase.firestore.local.a e(final a p0);
    
    public abstract d51 f(final a p0);
    
    public abstract g g(final a p0);
    
    public abstract p h(final a p0);
    
    public ConnectivityMonitor i() {
        return (ConnectivityMonitor)g9.e(this.f, "connectivityMonitor not initialized yet", new Object[0]);
    }
    
    public f j() {
        return (f)g9.e(this.e, "eventManager not initialized yet", new Object[0]);
    }
    
    public hj1 k() {
        return this.h;
    }
    
    public we0 l() {
        return this.g;
    }
    
    public com.google.firebase.firestore.local.a m() {
        return (com.google.firebase.firestore.local.a)g9.e(this.b, "localStore not initialized yet", new Object[0]);
    }
    
    public d51 n() {
        return (d51)g9.e(this.a, "persistence not initialized yet", new Object[0]);
    }
    
    public g o() {
        return (g)g9.e(this.d, "remoteStore not initialized yet", new Object[0]);
    }
    
    public p p() {
        return (p)g9.e(this.c, "syncEngine not initialized yet", new Object[0]);
    }
    
    public void q(final a a) {
        (this.a = this.f(a)).l();
        this.b = this.e(a);
        this.f = this.a(a);
        this.d = this.g(a);
        this.c = this.h(a);
        this.e = this.b(a);
        this.b.S();
        this.d.M();
        this.h = this.c(a);
        this.g = this.d(a);
    }
    
    public static class a
    {
        public final Context a;
        public final AsyncQueue b;
        public final rp c;
        public final com.google.firebase.firestore.remote.d d;
        public final u12 e;
        public final int f;
        public final b g;
        
        public a(final Context a, final AsyncQueue b, final rp c, final com.google.firebase.firestore.remote.d d, final u12 e, final int f, final b g) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
            this.f = f;
            this.g = g;
        }
        
        public AsyncQueue a() {
            return this.b;
        }
        
        public Context b() {
            return this.a;
        }
        
        public rp c() {
            return this.c;
        }
        
        public com.google.firebase.firestore.remote.d d() {
            return this.d;
        }
        
        public u12 e() {
            return this.e;
        }
        
        public int f() {
            return this.f;
        }
        
        public b g() {
            return this.g;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.core;

import com.google.firestore.v1.Value;

public class h extends FieldFilter
{
    public h(final s00 s00, final Value value) {
        super(s00, Operator.IN, value);
        g9.d(a32.t(value), "InFilter expects an ArrayValue", new Object[0]);
    }
    
    @Override
    public boolean d(final zt zt) {
        final Value j = zt.j(this.f());
        return j != null && a32.p(this.h().l0(), j);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.core;

public class DocumentViewChange
{
    public final Type a;
    public final zt b;
    
    public DocumentViewChange(final Type a, final zt b) {
        this.a = a;
        this.b = b;
    }
    
    public static DocumentViewChange a(final Type type, final zt zt) {
        return new DocumentViewChange(type, zt);
    }
    
    public zt b() {
        return this.b;
    }
    
    public Type c() {
        return this.a;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof DocumentViewChange;
        final boolean b2 = false;
        if (!b) {
            return false;
        }
        final DocumentViewChange documentViewChange = (DocumentViewChange)o;
        boolean b3 = b2;
        if (this.a.equals(documentViewChange.a)) {
            b3 = b2;
            if (this.b.equals(documentViewChange.b)) {
                b3 = true;
            }
        }
        return b3;
    }
    
    @Override
    public int hashCode() {
        return ((1891 + this.a.hashCode()) * 31 + this.b.getKey().hashCode()) * 31 + this.b.getData().hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("DocumentViewChange(");
        sb.append(this.b);
        sb.append(",");
        sb.append(this.a);
        sb.append(")");
        return sb.toString();
    }
    
    public enum Type
    {
        private static final Type[] $VALUES;
        
        ADDED, 
        METADATA, 
        MODIFIED, 
        REMOVED;
    }
}

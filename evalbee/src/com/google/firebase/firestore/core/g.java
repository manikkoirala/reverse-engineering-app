// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.core;

import com.google.firebase.firestore.remote.d;
import com.google.android.gms.tasks.Continuation;
import java.util.concurrent.Callable;
import com.google.firebase.firestore.util.Logger;
import java.util.concurrent.ExecutionException;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.firestore.FirebaseFirestoreException;
import java.util.List;
import com.google.android.gms.tasks.Task;
import java.util.concurrent.atomic.AtomicBoolean;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.firestore.b;
import android.content.Context;
import com.google.firebase.firestore.local.a;
import com.google.firebase.firestore.util.AsyncQueue;

public final class g
{
    public final rp a;
    public final eo b;
    public final eo c;
    public final AsyncQueue d;
    public final jd e;
    public final fc0 f;
    public d51 g;
    public a h;
    public com.google.firebase.firestore.remote.g i;
    public p j;
    public f k;
    public hj1 l;
    public hj1 m;
    
    public g(final Context context, final rp a, final b b, final eo b2, final eo c, final AsyncQueue d, final fc0 f) {
        this.a = a;
        this.b = b2;
        this.c = c;
        this.d = d;
        this.f = f;
        this.e = new jd(new com.google.firebase.firestore.remote.f(a.a()));
        final TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        final AtomicBoolean atomicBoolean = new AtomicBoolean(false);
        d.i(new z30(this, taskCompletionSource, context, b));
        b2.c(new a40(this, atomicBoolean, taskCompletionSource, d));
        c.c(new b40());
    }
    
    public Task j(final du du) {
        this.x();
        return this.d.g(new c40(this, du)).continueWith((Continuation)new d40());
    }
    
    public final void k(final Context context, final u12 u12, final b b) {
        Logger.a("FirestoreClient", "Initializing. user=%s", u12.a());
        final com.google.firebase.firestore.core.d.a a = new com.google.firebase.firestore.core.d.a(context, this.d, this.a, new d(this.a, this.d, this.b, this.c, context, this.f), u12, 100, b);
        l l;
        if (b.d()) {
            l = new o();
        }
        else {
            l = new l();
        }
        l.q(a);
        this.g = l.n();
        this.m = l.k();
        this.h = l.m();
        this.i = l.o();
        this.j = l.p();
        this.k = l.j();
        final we0 i = l.l();
        final hj1 m = this.m;
        if (m != null) {
            m.start();
        }
        if (i != null) {
            (this.l = i.f()).start();
        }
    }
    
    public boolean l() {
        return this.d.k();
    }
    
    public n v(final Query query, final f.a a, final rx rx) {
        this.x();
        final n n = new n(query, a, rx);
        this.d.i(new f40(this, n));
        return n;
    }
    
    public void w(final n n) {
        if (this.l()) {
            return;
        }
        this.d.i(new g40(this, n));
    }
    
    public final void x() {
        if (!this.l()) {
            return;
        }
        throw new IllegalStateException("The client has already been terminated");
    }
    
    public Task y(final List list) {
        this.x();
        final TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        this.d.i(new y30(this, list, taskCompletionSource));
        return taskCompletionSource.getTask();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.core;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import com.google.firestore.v1.Value;

public class FieldFilter extends i10
{
    public final Operator a;
    public final Value b;
    public final s00 c;
    
    public FieldFilter(final s00 c, final Operator a, final Value b) {
        this.c = c;
        this.a = a;
        this.b = b;
    }
    
    public static FieldFilter e(final s00 s00, final Operator operator, final Value value) {
        if (s00.s()) {
            if (operator == Operator.IN) {
                return new j(s00, value);
            }
            if (operator == Operator.NOT_IN) {
                return new k(s00, value);
            }
            final boolean b = operator != Operator.ARRAY_CONTAINS && operator != Operator.ARRAY_CONTAINS_ANY;
            final StringBuilder sb = new StringBuilder();
            sb.append(operator.toString());
            sb.append("queries don't make sense on document keys");
            g9.d(b, sb.toString(), new Object[0]);
            return new i(s00, operator, value);
        }
        else {
            if (operator == Operator.ARRAY_CONTAINS) {
                return new b(s00, value);
            }
            if (operator == Operator.IN) {
                return new h(s00, value);
            }
            if (operator == Operator.ARRAY_CONTAINS_ANY) {
                return new a(s00, value);
            }
            if (operator == Operator.NOT_IN) {
                return new m(s00, value);
            }
            return new FieldFilter(s00, operator, value);
        }
    }
    
    @Override
    public String a() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.f().d());
        sb.append(this.g().toString());
        sb.append(a32.b(this.h()));
        return sb.toString();
    }
    
    @Override
    public List b() {
        return Collections.singletonList(this);
    }
    
    @Override
    public List c() {
        return Collections.singletonList(this);
    }
    
    @Override
    public boolean d(final zt zt) {
        final Value j = zt.j(this.c);
        final Operator a = this.a;
        final Operator not_EQUAL = Operator.NOT_EQUAL;
        final boolean b = true;
        boolean b2 = true;
        if (a == not_EQUAL) {
            if (j == null || !this.j(a32.i(j, this.b))) {
                b2 = false;
            }
            return b2;
        }
        return j != null && a32.G(j) == a32.G(this.b) && this.j(a32.i(j, this.b)) && b;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b2;
        final boolean b = b2 = false;
        if (o != null) {
            if (!(o instanceof FieldFilter)) {
                b2 = b;
            }
            else {
                final FieldFilter fieldFilter = (FieldFilter)o;
                b2 = b;
                if (this.a == fieldFilter.a) {
                    b2 = b;
                    if (this.c.equals(fieldFilter.c)) {
                        b2 = b;
                        if (this.b.equals(fieldFilter.b)) {
                            b2 = true;
                        }
                    }
                }
            }
        }
        return b2;
    }
    
    public s00 f() {
        return this.c;
    }
    
    public Operator g() {
        return this.a;
    }
    
    public Value h() {
        return this.b;
    }
    
    @Override
    public int hashCode() {
        return ((1147 + this.a.hashCode()) * 31 + this.c.hashCode()) * 31 + this.b.hashCode();
    }
    
    public boolean i() {
        return Arrays.asList(Operator.LESS_THAN, Operator.LESS_THAN_OR_EQUAL, Operator.GREATER_THAN, Operator.GREATER_THAN_OR_EQUAL, Operator.NOT_EQUAL, Operator.NOT_IN).contains(this.a);
    }
    
    public boolean j(final int n) {
        final int n2 = FieldFilter$a.a[this.a.ordinal()];
        final boolean b = true;
        final boolean b2 = true;
        final boolean b3 = true;
        boolean b4 = true;
        final boolean b5 = true;
        final boolean b6 = true;
        switch (n2) {
            default: {
                throw g9.a("Unknown FieldFilter operator: %s", this.a);
            }
            case 6: {
                return n >= 0 && b6;
            }
            case 5: {
                return n > 0 && b;
            }
            case 4: {
                return n != 0 && b2;
            }
            case 3: {
                return n == 0 && b3;
            }
            case 2: {
                if (n > 0) {
                    b4 = false;
                }
                return b4;
            }
            case 1: {
                return n < 0 && b5;
            }
        }
    }
    
    @Override
    public String toString() {
        return this.a();
    }
    
    public enum Operator
    {
        private static final Operator[] $VALUES;
        
        ARRAY_CONTAINS("array_contains"), 
        ARRAY_CONTAINS_ANY("array_contains_any"), 
        EQUAL("=="), 
        GREATER_THAN(">"), 
        GREATER_THAN_OR_EQUAL(">="), 
        IN("in"), 
        LESS_THAN("<"), 
        LESS_THAN_OR_EQUAL("<="), 
        NOT_EQUAL("!="), 
        NOT_IN("not_in");
        
        private final String text;
        
        private Operator(final String text) {
            this.text = text;
        }
        
        @Override
        public String toString() {
            return this.text;
        }
    }
}

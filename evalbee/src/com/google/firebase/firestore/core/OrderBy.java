// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.core;

import com.google.firestore.v1.Value;

public class OrderBy
{
    public final Direction a;
    public final s00 b;
    
    public OrderBy(final Direction a, final s00 b) {
        this.a = a;
        this.b = b;
    }
    
    public static OrderBy d(final Direction direction, final s00 s00) {
        return new OrderBy(direction, s00);
    }
    
    public int a(final zt zt, final zt zt2) {
        int n;
        int n2;
        if (this.b.equals(s00.b)) {
            n = this.a.getComparisonModifier();
            n2 = zt.getKey().c(zt2.getKey());
        }
        else {
            final Value j = zt.j(this.b);
            final Value i = zt2.j(this.b);
            g9.d(j != null && i != null, "Trying to compare documents on fields that don't exist.", new Object[0]);
            n = this.a.getComparisonModifier();
            n2 = a32.i(j, i);
        }
        return n * n2;
    }
    
    public Direction b() {
        return this.a;
    }
    
    public s00 c() {
        return this.b;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b2;
        final boolean b = b2 = false;
        if (o != null) {
            if (!(o instanceof OrderBy)) {
                b2 = b;
            }
            else {
                final OrderBy orderBy = (OrderBy)o;
                b2 = b;
                if (this.a == orderBy.a) {
                    b2 = b;
                    if (this.b.equals(orderBy.b)) {
                        b2 = true;
                    }
                }
            }
        }
        return b2;
    }
    
    @Override
    public int hashCode() {
        return (899 + this.a.hashCode()) * 31 + this.b.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        String str;
        if (this.a == Direction.ASCENDING) {
            str = "";
        }
        else {
            str = "-";
        }
        sb.append(str);
        sb.append(this.b.d());
        return sb.toString();
    }
    
    public enum Direction
    {
        private static final Direction[] $VALUES;
        
        ASCENDING(1), 
        DESCENDING(-1);
        
        private final int comparisonModifier;
        
        private Direction(final int comparisonModifier) {
            this.comparisonModifier = comparisonModifier;
        }
        
        public int getComparisonModifier() {
            return this.comparisonModifier;
        }
    }
}

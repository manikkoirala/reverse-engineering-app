// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.core;

import java.util.Collections;
import java.util.Iterator;
import android.text.TextUtils;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;

public class CompositeFilter extends i10
{
    public final List a;
    public final Operator b;
    public List c;
    
    public CompositeFilter(final List c, final Operator b) {
        this.a = new ArrayList(c);
        this.b = b;
    }
    
    @Override
    public String a() {
        final StringBuilder sb = new StringBuilder();
        if (this.i()) {
            final Iterator iterator = this.a.iterator();
            while (iterator.hasNext()) {
                sb.append(((i10)iterator.next()).a());
            }
            return sb.toString();
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(this.b.toString());
        sb2.append("(");
        sb.append(sb2.toString());
        sb.append(TextUtils.join((CharSequence)",", (Iterable)this.a));
        sb.append(")");
        return sb.toString();
    }
    
    @Override
    public List b() {
        return Collections.unmodifiableList((List<?>)this.a);
    }
    
    @Override
    public List c() {
        final List c = this.c;
        if (c != null) {
            return Collections.unmodifiableList((List<?>)c);
        }
        this.c = new ArrayList();
        final Iterator iterator = this.a.iterator();
        while (iterator.hasNext()) {
            this.c.addAll(((i10)iterator.next()).c());
        }
        return Collections.unmodifiableList((List<?>)this.c);
    }
    
    @Override
    public boolean d(final zt zt) {
        if (this.f()) {
            final Iterator iterator = this.a.iterator();
            while (iterator.hasNext()) {
                if (!((i10)iterator.next()).d(zt)) {
                    return false;
                }
            }
            return true;
        }
        final Iterator iterator2 = this.a.iterator();
        while (iterator2.hasNext()) {
            if (((i10)iterator2.next()).d(zt)) {
                return true;
            }
        }
        return false;
    }
    
    public Operator e() {
        return this.b;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b2;
        final boolean b = b2 = false;
        if (o != null) {
            if (!(o instanceof CompositeFilter)) {
                b2 = b;
            }
            else {
                final CompositeFilter compositeFilter = (CompositeFilter)o;
                b2 = b;
                if (this.b == compositeFilter.b) {
                    b2 = b;
                    if (this.a.equals(compositeFilter.a)) {
                        b2 = true;
                    }
                }
            }
        }
        return b2;
    }
    
    public boolean f() {
        return this.b == Operator.AND;
    }
    
    public boolean g() {
        return this.b == Operator.OR;
    }
    
    public boolean h() {
        final Iterator iterator = this.a.iterator();
        while (iterator.hasNext()) {
            if (((i10)iterator.next()) instanceof CompositeFilter) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public int hashCode() {
        return (1147 + this.b.hashCode()) * 31 + this.a.hashCode();
    }
    
    public boolean i() {
        return this.h() && this.f();
    }
    
    public CompositeFilter j(final List list) {
        final ArrayList list2 = new ArrayList(this.a);
        list2.addAll(list);
        return new CompositeFilter(list2, this.b);
    }
    
    @Override
    public String toString() {
        return this.a();
    }
    
    public enum Operator
    {
        private static final Operator[] $VALUES;
        
        AND("and"), 
        OR("or");
        
        private final String text;
        
        private Operator(final String text) {
            this.text = text;
        }
        
        @Override
        public String toString() {
            return this.text;
        }
    }
}

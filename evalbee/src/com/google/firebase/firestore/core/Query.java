// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.core;

import java.util.HashSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.SortedSet;
import java.util.Comparator;
import java.util.Collections;
import java.util.List;

public final class Query
{
    public static final OrderBy k;
    public static final OrderBy l;
    public final List a;
    public List b;
    public q c;
    public final List d;
    public final ke1 e;
    public final String f;
    public final long g;
    public final LimitType h;
    public final c i;
    public final c j;
    
    static {
        final OrderBy.Direction ascending = OrderBy.Direction.ASCENDING;
        final s00 b = s00.b;
        k = OrderBy.d(ascending, b);
        l = OrderBy.d(OrderBy.Direction.DESCENDING, b);
    }
    
    public Query(final ke1 ke1, final String s) {
        this(ke1, s, Collections.emptyList(), Collections.emptyList(), -1L, LimitType.LIMIT_TO_FIRST, null, null);
    }
    
    public Query(final ke1 e, final String f, final List d, final List a, final long g, final LimitType h, final c i, final c j) {
        this.e = e;
        this.f = f;
        this.a = a;
        this.d = d;
        this.g = g;
        this.h = h;
        this.i = i;
        this.j = j;
    }
    
    public static Query b(final ke1 ke1) {
        return new Query(ke1, null);
    }
    
    public Query a(final ke1 ke1) {
        return new Query(ke1, null, this.d, this.a, this.g, this.h, this.i, this.j);
    }
    
    public Comparator c() {
        return new a(this.k());
    }
    
    public String d() {
        return this.f;
    }
    
    public c e() {
        return this.j;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o != null && Query.class == o.getClass()) {
            final Query query = (Query)o;
            return this.h == query.h && this.x().equals(query.x());
        }
        return false;
    }
    
    public List f() {
        return this.a;
    }
    
    public List g() {
        return this.d;
    }
    
    public SortedSet h() {
        final TreeSet set = new TreeSet();
        final Iterator iterator = this.g().iterator();
        while (iterator.hasNext()) {
            for (final FieldFilter fieldFilter : ((i10)iterator.next()).c()) {
                if (fieldFilter.i()) {
                    set.add(fieldFilter.f());
                }
            }
        }
        return set;
    }
    
    @Override
    public int hashCode() {
        return this.x().hashCode() * 31 + this.h.hashCode();
    }
    
    public long i() {
        return this.g;
    }
    
    public LimitType j() {
        return this.h;
    }
    
    public List k() {
        synchronized (this) {
            if (this.b == null) {
                final ArrayList list = new ArrayList();
                final HashSet<String> set = new HashSet<String>();
                for (final OrderBy orderBy : this.a) {
                    list.add(orderBy);
                    set.add(orderBy.b.d());
                }
                OrderBy.Direction direction;
                if (this.a.size() > 0) {
                    final List a = this.a;
                    direction = a.get(a.size() - 1).b();
                }
                else {
                    direction = OrderBy.Direction.ASCENDING;
                }
                for (final s00 s00 : this.h()) {
                    if (!set.contains(s00.d()) && !s00.s()) {
                        list.add(OrderBy.d(direction, s00));
                    }
                }
                if (!set.contains(s00.b.d())) {
                    OrderBy orderBy2;
                    if (direction.equals(OrderBy.Direction.ASCENDING)) {
                        orderBy2 = Query.k;
                    }
                    else {
                        orderBy2 = Query.l;
                    }
                    list.add(orderBy2);
                }
                this.b = Collections.unmodifiableList((List<?>)list);
            }
            return this.b;
        }
    }
    
    public ke1 l() {
        return this.e;
    }
    
    public c m() {
        return this.i;
    }
    
    public boolean n() {
        return this.g != -1L;
    }
    
    public boolean o() {
        return this.f != null;
    }
    
    public boolean p() {
        return du.o(this.e) && this.f == null && this.d.isEmpty();
    }
    
    public Query q(final long n) {
        return new Query(this.e, this.f, this.d, this.a, n, LimitType.LIMIT_TO_FIRST, this.i, this.j);
    }
    
    public boolean r(final zt zt) {
        return zt.d() && this.w(zt) && this.v(zt) && this.u(zt) && this.t(zt);
    }
    
    public boolean s() {
        final boolean empty = this.d.isEmpty();
        boolean b2;
        final boolean b = b2 = false;
        if (empty) {
            b2 = b;
            if (this.g == -1L) {
                b2 = b;
                if (this.i == null) {
                    b2 = b;
                    if (this.j == null) {
                        if (!this.f().isEmpty()) {
                            b2 = b;
                            if (this.f().size() != 1) {
                                return b2;
                            }
                            b2 = b;
                            if (!this.f().get(0).b.s()) {
                                return b2;
                            }
                        }
                        b2 = true;
                    }
                }
            }
        }
        return b2;
    }
    
    public final boolean t(final zt zt) {
        final c i = this.i;
        if (i != null && !i.f(this.k(), zt)) {
            return false;
        }
        final c j = this.j;
        return j == null || j.e(this.k(), zt);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Query(target=");
        sb.append(this.x().toString());
        sb.append(";limitType=");
        sb.append(this.h.toString());
        sb.append(")");
        return sb.toString();
    }
    
    public final boolean u(final zt zt) {
        final Iterator iterator = this.d.iterator();
        while (iterator.hasNext()) {
            if (!((i10)iterator.next()).d(zt)) {
                return false;
            }
        }
        return true;
    }
    
    public final boolean v(final zt zt) {
        for (final OrderBy orderBy : this.k()) {
            if (!orderBy.c().equals(s00.b) && zt.j(orderBy.b) == null) {
                return false;
            }
        }
        return true;
    }
    
    public final boolean w(final zt zt) {
        final ke1 m = zt.getKey().m();
        final String f = this.f;
        final boolean b = false;
        final boolean b2 = false;
        if (f != null) {
            boolean b3 = b2;
            if (zt.getKey().n(this.f)) {
                b3 = b2;
                if (this.e.k(m)) {
                    b3 = true;
                }
            }
            return b3;
        }
        if (du.o(this.e)) {
            return this.e.equals(m);
        }
        boolean b4 = b;
        if (this.e.k(m)) {
            b4 = b;
            if (this.e.l() == m.l() - 1) {
                b4 = true;
            }
        }
        return b4;
    }
    
    public q x() {
        synchronized (this) {
            if (this.c == null) {
                this.c = this.y(this.k());
            }
            return this.c;
        }
    }
    
    public final q y(final List list) {
        synchronized (this) {
            if (this.h == LimitType.LIMIT_TO_FIRST) {
                return new q(this.l(), this.d(), this.g(), list, this.g, this.m(), this.e());
            }
            final ArrayList<OrderBy> list2 = new ArrayList<OrderBy>();
            for (final OrderBy orderBy : list) {
                OrderBy.Direction direction;
                if (orderBy.b() == (direction = OrderBy.Direction.DESCENDING)) {
                    direction = OrderBy.Direction.ASCENDING;
                }
                list2.add(OrderBy.d(direction, orderBy.c()));
            }
            final c j = this.j;
            c c = null;
            c c2;
            if (j != null) {
                c2 = new c(j.b(), this.j.c());
            }
            else {
                c2 = null;
            }
            final c i = this.i;
            if (i != null) {
                c = new c(i.b(), this.i.c());
            }
            return new q(this.l(), this.d(), this.g(), list2, this.g, c2, c);
        }
    }
    
    public enum LimitType
    {
        private static final LimitType[] $VALUES;
        
        LIMIT_TO_FIRST, 
        LIMIT_TO_LAST;
    }
    
    public static class a implements Comparator
    {
        public final List a;
        
        public a(final List a) {
            final Iterator iterator = a.iterator();
            int n = 0;
        Label_0012:
            while (true) {
                n = 0;
                while (iterator.hasNext()) {
                    final OrderBy orderBy = (OrderBy)iterator.next();
                    if (n == 0 && !orderBy.c().equals(s00.b)) {
                        continue Label_0012;
                    }
                    n = 1;
                }
                break;
            }
            if (n != 0) {
                this.a = a;
                return;
            }
            throw new IllegalArgumentException("QueryComparator needs to have a key ordering");
        }
        
        public int a(final zt zt, final zt zt2) {
            final Iterator iterator = this.a.iterator();
            while (iterator.hasNext()) {
                final int a = ((OrderBy)iterator.next()).a(zt, zt2);
                if (a != 0) {
                    return a;
                }
            }
            return 0;
        }
    }
}

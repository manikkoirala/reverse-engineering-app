// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.core;

public enum UserData$Source
{
    private static final UserData$Source[] $VALUES;
    
    Argument, 
    ArrayArgument, 
    MergeSet, 
    Set, 
    Update;
}

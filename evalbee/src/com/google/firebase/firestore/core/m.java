// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.core;

import com.google.firestore.v1.Value;

public class m extends FieldFilter
{
    public m(final s00 s00, final Value value) {
        super(s00, Operator.NOT_IN, value);
        g9.d(a32.t(value), "NotInFilter expects an ArrayValue", new Object[0]);
    }
    
    @Override
    public boolean d(final zt zt) {
        final boolean p = a32.p(this.h().l0(), a32.b);
        final boolean b = false;
        if (p) {
            return false;
        }
        final Value j = zt.j(this.f());
        boolean b2 = b;
        if (j != null) {
            b2 = b;
            if (!a32.p(this.h().l0(), j)) {
                b2 = true;
            }
        }
        return b2;
    }
}

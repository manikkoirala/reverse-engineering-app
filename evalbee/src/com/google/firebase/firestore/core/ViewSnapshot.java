// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.core;

import java.util.Iterator;
import java.util.ArrayList;
import com.google.firebase.database.collection.c;
import java.util.List;

public class ViewSnapshot
{
    public final Query a;
    public final mu b;
    public final mu c;
    public final List d;
    public final boolean e;
    public final c f;
    public final boolean g;
    public boolean h;
    public boolean i;
    
    public ViewSnapshot(final Query a, final mu b, final mu c, final List d, final boolean e, final c f, final boolean g, final boolean h, final boolean i) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = h;
        this.i = i;
    }
    
    public static ViewSnapshot c(final Query query, final mu mu, final c c, final boolean b, final boolean b2, final boolean b3) {
        final ArrayList list = new ArrayList();
        final Iterator iterator = mu.iterator();
        while (iterator.hasNext()) {
            list.add(DocumentViewChange.a(DocumentViewChange.Type.ADDED, (zt)iterator.next()));
        }
        return new ViewSnapshot(query, mu, mu.c(query.c()), list, b, c, true, b2, b3);
    }
    
    public boolean a() {
        return this.g;
    }
    
    public boolean b() {
        return this.h;
    }
    
    public List d() {
        return this.d;
    }
    
    public mu e() {
        return this.b;
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ViewSnapshot)) {
            return false;
        }
        final ViewSnapshot viewSnapshot = (ViewSnapshot)o;
        return this.e == viewSnapshot.e && this.g == viewSnapshot.g && this.h == viewSnapshot.h && this.a.equals(viewSnapshot.a) && this.f.equals(viewSnapshot.f) && this.b.equals(viewSnapshot.b) && this.c.equals(viewSnapshot.c) && this.i == viewSnapshot.i && this.d.equals(viewSnapshot.d);
    }
    
    public c f() {
        return this.f;
    }
    
    public mu g() {
        return this.c;
    }
    
    public Query h() {
        return this.a;
    }
    
    @Override
    public int hashCode() {
        return (((((((this.a.hashCode() * 31 + this.b.hashCode()) * 31 + this.c.hashCode()) * 31 + this.d.hashCode()) * 31 + this.f.hashCode()) * 31 + (this.e ? 1 : 0)) * 31 + (this.g ? 1 : 0)) * 31 + (this.h ? 1 : 0)) * 31 + (this.i ? 1 : 0);
    }
    
    public boolean i() {
        return this.i;
    }
    
    public boolean j() {
        return this.f.isEmpty() ^ true;
    }
    
    public boolean k() {
        return this.e;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ViewSnapshot(");
        sb.append(this.a);
        sb.append(", ");
        sb.append(this.b);
        sb.append(", ");
        sb.append(this.c);
        sb.append(", ");
        sb.append(this.d);
        sb.append(", isFromCache=");
        sb.append(this.e);
        sb.append(", mutatedKeys=");
        sb.append(this.f.size());
        sb.append(", didSyncStateChange=");
        sb.append(this.g);
        sb.append(", excludesMetadataChanges=");
        sb.append(this.h);
        sb.append(", hasCachedResults=");
        sb.append(this.i);
        sb.append(")");
        return sb.toString();
    }
    
    public enum SyncState
    {
        private static final SyncState[] $VALUES;
        
        LOCAL, 
        NONE, 
        SYNCED;
    }
}

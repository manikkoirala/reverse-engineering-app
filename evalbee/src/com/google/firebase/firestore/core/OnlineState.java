// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.core;

public enum OnlineState
{
    private static final OnlineState[] $VALUES;
    
    OFFLINE, 
    ONLINE, 
    UNKNOWN;
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.core;

import java.util.ArrayList;
import java.util.Map;
import com.google.firebase.database.collection.b;
import java.util.Iterator;
import java.util.Comparator;
import java.util.List;
import java.util.Collections;
import com.google.firebase.database.collection.c;

public class r
{
    public final Query a;
    public ViewSnapshot.SyncState b;
    public boolean c;
    public mu d;
    public c e;
    public c f;
    public c g;
    
    public r(final Query a, final c e) {
        this.a = a;
        this.b = ViewSnapshot.SyncState.NONE;
        this.d = mu.c(a.c());
        this.e = e;
        this.f = du.e();
        this.g = du.e();
    }
    
    public static int g(final DocumentViewChange documentViewChange) {
        final int n = r$a.a[documentViewChange.c().ordinal()];
        int n2 = 1;
        if (n != 1) {
            n2 = 2;
            if (n != 2) {
                n2 = n2;
                if (n != 3) {
                    if (n == 4) {
                        return 0;
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unknown change type: ");
                    sb.append(documentViewChange.c());
                    throw new IllegalArgumentException(sb.toString());
                }
            }
        }
        return n2;
    }
    
    public m32 b(final b b) {
        return this.c(b, null);
    }
    
    public m32 c(final b b, final zt1 zt1) {
        return this.d(b, zt1, false);
    }
    
    public m32 d(final b b, final zt1 zt1, final boolean b2) {
        g9.d(r.b.a(b) ^ true, "Cannot apply changes that need a refill", new Object[0]);
        final mu d = this.d;
        this.d = b.a;
        this.g = b.d;
        final List b3 = b.b.b();
        Collections.sort((List<Object>)b3, new l32(this));
        this.f(zt1);
        List<Object> list;
        if (b2) {
            list = Collections.emptyList();
        }
        else {
            list = this.o();
        }
        ViewSnapshot.SyncState b4;
        if (this.f.size() == 0 && this.c && !b2) {
            b4 = ViewSnapshot.SyncState.SYNCED;
        }
        else {
            b4 = ViewSnapshot.SyncState.LOCAL;
        }
        final boolean b5 = b4 != this.b;
        this.b = b4;
        ViewSnapshot viewSnapshot;
        if (b3.size() == 0 && !b5) {
            viewSnapshot = null;
        }
        else {
            viewSnapshot = new ViewSnapshot(this.a, b.a, d, b3, b4 == ViewSnapshot.SyncState.LOCAL, b.d, b5, false, zt1 != null && !zt1.e().isEmpty());
        }
        return new m32(viewSnapshot, list);
    }
    
    public m32 e(final OnlineState onlineState) {
        if (this.c && onlineState == OnlineState.OFFLINE) {
            this.c = false;
            return this.b(new b(this.d, new e(), this.g, false, null));
        }
        return new m32(null, Collections.emptyList());
    }
    
    public final void f(final zt1 zt1) {
        if (zt1 != null) {
            final Iterator iterator = zt1.b().iterator();
            while (iterator.hasNext()) {
                this.e = this.e.c(iterator.next());
            }
            for (final du du : zt1.c()) {
                g9.d(this.e.contains(du), "Modified document %s not found in view.", du);
            }
            final Iterator iterator3 = zt1.d().iterator();
            while (iterator3.hasNext()) {
                this.e = this.e.i(iterator3.next());
            }
            this.c = zt1.f();
        }
    }
    
    public b h(final com.google.firebase.database.collection.b b) {
        return this.i(b, null);
    }
    
    public b i(final com.google.firebase.database.collection.b b, final b b2) {
        e b3;
        if (b2 != null) {
            b3 = b2.b;
        }
        else {
            b3 = new e();
        }
        mu mu;
        if (b2 != null) {
            mu = b2.a;
        }
        else {
            mu = this.d;
        }
        c c;
        if (b2 != null) {
            c = b2.d;
        }
        else {
            c = this.g;
        }
        zt l;
        if (this.a.j().equals(Query.LimitType.LIMIT_TO_FIRST) && mu.size() == this.a.i()) {
            l = mu.l();
        }
        else {
            l = null;
        }
        zt i;
        if (this.a.j().equals(Query.LimitType.LIMIT_TO_LAST) && mu.size() == this.a.i()) {
            i = mu.i();
        }
        else {
            i = null;
        }
        final Iterator iterator = b.iterator();
        mu n = mu;
        boolean b4 = false;
        c j = c;
        while (iterator.hasNext()) {
            final Map.Entry<du, V> entry = iterator.next();
            final du du = entry.getKey();
            final zt g = mu.g(du);
            zt zt;
            if (this.a.r((zt)entry.getValue())) {
                zt = (zt)entry.getValue();
            }
            else {
                zt = null;
            }
            final boolean b5 = g != null && this.g.contains(g.getKey());
            final boolean b6 = zt != null && (zt.g() || (this.g.contains(zt.getKey()) && zt.f()));
            boolean b8 = false;
            Label_0541: {
                Label_0539: {
                    while (true) {
                        Label_0533: {
                            DocumentViewChange.Type type;
                            if (g != null && zt != null) {
                                if (!g.getData().equals(zt.getData())) {
                                    if (this.n(g, zt)) {
                                        break Label_0539;
                                    }
                                    b3.a(DocumentViewChange.a(DocumentViewChange.Type.MODIFIED, zt));
                                    if (l != null && this.a.c().compare(zt, l) > 0) {
                                        break Label_0533;
                                    }
                                    boolean b7 = b4;
                                    if (i == null) {
                                        break Label_0487;
                                    }
                                    b7 = b4;
                                    if (this.a.c().compare(zt, i) < 0) {
                                        break Label_0533;
                                    }
                                    break Label_0487;
                                }
                                else {
                                    if (b5 == b6) {
                                        break Label_0539;
                                    }
                                    type = DocumentViewChange.Type.METADATA;
                                }
                            }
                            else if (g == null && zt != null) {
                                type = DocumentViewChange.Type.ADDED;
                            }
                            else {
                                if (g == null || zt != null) {
                                    break Label_0539;
                                }
                                b3.a(DocumentViewChange.a(DocumentViewChange.Type.REMOVED, g));
                                if (l != null) {
                                    break Label_0533;
                                }
                                final boolean b7 = b4;
                                if (i != null) {
                                    break Label_0533;
                                }
                                break Label_0487;
                            }
                            b3.a(DocumentViewChange.a(type, zt));
                            boolean b7 = b4;
                            b8 = true;
                            b4 = b7;
                            break Label_0541;
                        }
                        boolean b7 = true;
                        continue;
                    }
                }
                b8 = false;
            }
            c c2 = j;
            mu mu2 = n;
            if (b8) {
                mu mu3;
                c c3;
                if (zt != null) {
                    mu3 = n.b(zt);
                    if (zt.g()) {
                        c3 = j.c(zt.getKey());
                    }
                    else {
                        c3 = j.i(zt.getKey());
                    }
                }
                else {
                    mu3 = n.n(du);
                    c3 = j.i(du);
                }
                mu2 = mu3;
                c2 = c3;
            }
            j = c2;
            n = mu2;
        }
        c c4 = j;
        mu mu4 = n;
        if (this.a.n()) {
            long n2 = n.size();
            long k = this.a.i();
            while (true) {
                n2 -= k;
                c4 = j;
                mu4 = n;
                if (n2 <= 0L) {
                    break;
                }
                zt zt2;
                if (this.a.j().equals(Query.LimitType.LIMIT_TO_FIRST)) {
                    zt2 = n.l();
                }
                else {
                    zt2 = n.i();
                }
                n = n.n(zt2.getKey());
                j = j.i(zt2.getKey());
                b3.a(DocumentViewChange.a(DocumentViewChange.Type.REMOVED, zt2));
                k = 1L;
            }
        }
        g9.d(!b4 || b2 == null, "View was refilled using docs that themselves needed refilling.", new Object[0]);
        return new b(mu4, b3, c4, b4, null);
    }
    
    public ViewSnapshot.SyncState j() {
        return this.b;
    }
    
    public c k() {
        return this.e;
    }
    
    public final boolean m(final du du) {
        if (this.e.contains(du)) {
            return false;
        }
        final zt g = this.d.g(du);
        return g != null && !g.g();
    }
    
    public final boolean n(final zt zt, final zt zt2) {
        return zt.g() && zt2.f() && !zt2.g();
    }
    
    public final List o() {
        if (!this.c) {
            return Collections.emptyList();
        }
        final c f = this.f;
        this.f = du.e();
        for (final zt zt : this.d) {
            if (this.m(zt.getKey())) {
                this.f = this.f.c(zt.getKey());
            }
        }
        final ArrayList list = new ArrayList(f.size() + this.f.size());
        for (final du du : f) {
            if (!this.f.contains(du)) {
                list.add((Object)new LimboDocumentChange(LimboDocumentChange.Type.REMOVED, du));
            }
        }
        for (final du du2 : this.f) {
            if (!f.contains(du2)) {
                list.add((Object)new LimboDocumentChange(LimboDocumentChange.Type.ADDED, du2));
            }
        }
        return list;
    }
    
    public static class b
    {
        public final mu a;
        public final e b;
        public final boolean c;
        public final c d;
        
        public b(final mu a, final e b, final c d, final boolean c) {
            this.a = a;
            this.b = b;
            this.d = d;
            this.c = c;
        }
        
        public static /* synthetic */ boolean a(final b b) {
            return b.c;
        }
        
        public boolean b() {
            return this.c;
        }
    }
}

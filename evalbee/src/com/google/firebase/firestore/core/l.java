// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.core;

import io.grpc.Status;
import com.google.firebase.database.collection.c;
import com.google.firebase.firestore.remote.g;
import com.google.firebase.firestore.local.e;
import com.google.firebase.firestore.local.b;
import com.google.firebase.firestore.local.a;
import com.google.firebase.firestore.remote.ConnectivityMonitor;

public class l extends d
{
    @Override
    public f b(final a a) {
        return new f(this.p());
    }
    
    @Override
    public hj1 c(final a a) {
        return null;
    }
    
    @Override
    public we0 d(final a a) {
        return null;
    }
    
    @Override
    public com.google.firebase.firestore.local.a e(final a a) {
        return new com.google.firebase.firestore.local.a(this.n(), new com.google.firebase.firestore.local.f(), a.e());
    }
    
    @Override
    public d51 f(final a a) {
        if (this.s(a.g())) {
            return com.google.firebase.firestore.local.e.n(com.google.firebase.firestore.local.b.b.a(a.g().b()), new zk0(new com.google.firebase.firestore.remote.f(a.c().a())));
        }
        return com.google.firebase.firestore.local.e.m();
    }
    
    @Override
    public g g(final a a) {
        return new g((g.c)new b(null), this.m(), a.d(), a.a(), this.i());
    }
    
    @Override
    public p h(final a a) {
        return new p(this.m(), this.o(), a.e(), a.f());
    }
    
    public com.google.firebase.firestore.remote.a r(final a a) {
        return new com.google.firebase.firestore.remote.a(a.b());
    }
    
    public final boolean s(final com.google.firebase.firestore.b b) {
        b.a();
        return false;
    }
    
    public class b implements c
    {
        public final l a;
        
        public b(final l a) {
            this.a = a;
        }
        
        @Override
        public void a(final OnlineState onlineState) {
            this.a.p().a(onlineState);
        }
        
        @Override
        public com.google.firebase.database.collection.c b(final int n) {
            return this.a.p().b(n);
        }
        
        @Override
        public void c(final int n, final Status status) {
            this.a.p().c(n, status);
        }
        
        @Override
        public void d(final jd1 jd1) {
            this.a.p().d(jd1);
        }
        
        @Override
        public void e(final yx0 yx0) {
            this.a.p().e(yx0);
        }
        
        @Override
        public void f(final int n, final Status status) {
            this.a.p().f(n, status);
        }
    }
}

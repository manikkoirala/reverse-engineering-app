// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.core;

import com.google.firestore.v1.Value;

public class i extends FieldFilter
{
    public final du d;
    
    public i(final s00 s00, final Operator operator, final Value value) {
        super(s00, operator, value);
        g9.d(a32.B(value), "KeyFieldFilter expects a ReferenceValue", new Object[0]);
        this.d = du.f(this.h().t0());
    }
    
    @Override
    public boolean d(final zt zt) {
        return this.j(zt.getKey().c(this.d));
    }
}

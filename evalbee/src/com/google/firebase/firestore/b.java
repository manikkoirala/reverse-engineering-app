// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore;

public final class b
{
    public final String a;
    public final boolean b;
    public final boolean c;
    public final long d;
    
    public b(final b b) {
        this.a = com.google.firebase.firestore.b.b.b(b);
        this.b = com.google.firebase.firestore.b.b.c(b);
        this.c = com.google.firebase.firestore.b.b.d(b);
        this.d = com.google.firebase.firestore.b.b.e(b);
        com.google.firebase.firestore.b.b.a(b);
    }
    
    public vk0 a() {
        return null;
    }
    
    public long b() {
        return this.d;
    }
    
    public String c() {
        return this.a;
    }
    
    public boolean d() {
        return this.c;
    }
    
    public boolean e() {
        return this.b;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o != null && b.class == o.getClass()) {
            final b b = (b)o;
            return this.b == b.b && this.c == b.c && this.d == b.d && this.a.equals(b.a);
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.a.hashCode();
        final int b = this.b ? 1 : 0;
        final int c = this.c ? 1 : 0;
        final long d = this.d;
        return (((hashCode * 31 + b) * 31 + c) * 31 + (int)(d ^ d >>> 32)) * 31 + 0;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("FirebaseFirestoreSettings{host=");
        sb.append(this.a);
        sb.append(", sslEnabled=");
        sb.append(this.b);
        sb.append(", persistenceEnabled=");
        sb.append(this.c);
        sb.append(", cacheSizeBytes=");
        sb.append(this.d);
        sb.append(", cacheSettings=");
        sb.append((Object)null);
        if (sb.toString() == null) {
            return "null";
        }
        throw null;
    }
    
    public static final class b
    {
        public String a;
        public boolean b;
        public boolean c;
        public long d;
        public boolean e;
        
        public b() {
            this.e = false;
            this.a = "firestore.googleapis.com";
            this.b = true;
            this.c = true;
            this.d = 104857600L;
        }
        
        public static /* synthetic */ vk0 a(final b b) {
            b.getClass();
            return null;
        }
        
        public static /* synthetic */ String b(final b b) {
            return b.a;
        }
        
        public static /* synthetic */ boolean c(final b b) {
            return b.b;
        }
        
        public static /* synthetic */ boolean d(final b b) {
            return b.c;
        }
        
        public static /* synthetic */ long e(final b b) {
            return b.d;
        }
        
        public com.google.firebase.firestore.b f() {
            if (!this.b && this.a.equals("firestore.googleapis.com")) {
                throw new IllegalStateException("You can't set the 'sslEnabled' setting unless you also set a non-default 'host'.");
            }
            return new com.google.firebase.firestore.b(this, null);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.model;

public final class c extends b
{
    public final long a;
    public final a b;
    
    public c(final long a, final a b) {
        this.a = a;
        if (b != null) {
            this.b = b;
            return;
        }
        throw new NullPointerException("Null offset");
    }
    
    @Override
    public a c() {
        return this.b;
    }
    
    @Override
    public long d() {
        return this.a;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof b) {
            final b b2 = (b)o;
            if (this.a != b2.d() || !this.b.equals(b2.c())) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        final long a = this.a;
        return ((int)(a ^ a >>> 32) ^ 0xF4243) * 1000003 ^ this.b.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("IndexState{sequenceNumber=");
        sb.append(this.a);
        sb.append(", offset=");
        sb.append(this.b);
        sb.append("}");
        return sb.toString();
    }
}

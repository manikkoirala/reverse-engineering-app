// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.model;

import com.google.firebase.firestore.core.OrderBy;
import java.util.HashSet;
import java.util.Iterator;
import com.google.firebase.firestore.core.FieldFilter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeSet;
import com.google.firebase.firestore.core.q;
import java.util.List;
import java.util.SortedSet;

public class e
{
    public final String a;
    public final SortedSet b;
    public final List c;
    public final List d;
    
    public e(final q q) {
        String a;
        if (q.d() != null) {
            a = q.d();
        }
        else {
            a = q.n().g();
        }
        this.a = a;
        this.d = q.m();
        this.b = new TreeSet(new cu1());
        this.c = new ArrayList();
        for (final FieldFilter fieldFilter : q.h()) {
            if (fieldFilter.i()) {
                this.b.add(fieldFilter);
            }
            else {
                this.c.add(fieldFilter);
            }
        }
    }
    
    public FieldIndex b() {
        if (this.d()) {
            return null;
        }
        final HashSet set = new HashSet();
        final ArrayList list = new ArrayList();
        for (final FieldFilter fieldFilter : this.c) {
            if (fieldFilter.f().s()) {
                continue;
            }
            s00 s00;
            FieldIndex.Segment.Kind kind;
            if (fieldFilter.g().equals(FieldFilter.Operator.ARRAY_CONTAINS) || fieldFilter.g().equals(FieldFilter.Operator.ARRAY_CONTAINS_ANY)) {
                s00 = fieldFilter.f();
                kind = FieldIndex.Segment.Kind.CONTAINS;
            }
            else {
                if (set.contains(fieldFilter.f())) {
                    continue;
                }
                set.add(fieldFilter.f());
                s00 = fieldFilter.f();
                kind = FieldIndex.Segment.Kind.ASCENDING;
            }
            list.add(FieldIndex.Segment.c(s00, kind));
        }
        for (final OrderBy orderBy : this.d) {
            if (orderBy.c().s()) {
                continue;
            }
            if (set.contains(orderBy.c())) {
                continue;
            }
            set.add(orderBy.c());
            final s00 c = orderBy.c();
            FieldIndex.Segment.Kind kind2;
            if (orderBy.b() == OrderBy.Direction.ASCENDING) {
                kind2 = FieldIndex.Segment.Kind.ASCENDING;
            }
            else {
                kind2 = FieldIndex.Segment.Kind.DESCENDING;
            }
            list.add(FieldIndex.Segment.c(c, kind2));
        }
        return FieldIndex.b(-1, this.a, list, FieldIndex.a);
    }
    
    public final boolean c(final FieldIndex.Segment segment) {
        final Iterator iterator = this.c.iterator();
        while (iterator.hasNext()) {
            if (this.f((FieldFilter)iterator.next(), segment)) {
                return true;
            }
        }
        return false;
    }
    
    public boolean d() {
        final int size = this.b.size();
        boolean b = true;
        if (size <= 1) {
            b = false;
        }
        return b;
    }
    
    public final boolean f(final FieldFilter fieldFilter, final FieldIndex.Segment segment) {
        boolean b2;
        final boolean b = b2 = false;
        if (fieldFilter != null) {
            if (!fieldFilter.f().equals(segment.d())) {
                b2 = b;
            }
            else {
                final boolean b3 = fieldFilter.g().equals(FieldFilter.Operator.ARRAY_CONTAINS) || fieldFilter.g().equals(FieldFilter.Operator.ARRAY_CONTAINS_ANY);
                b2 = b;
                if (segment.e().equals(FieldIndex.Segment.Kind.CONTAINS) == b3) {
                    b2 = true;
                }
            }
        }
        return b2;
    }
    
    public final boolean g(final OrderBy orderBy, final FieldIndex.Segment segment) {
        final boolean equals = orderBy.c().equals(segment.d());
        final boolean b = false;
        if (!equals) {
            return false;
        }
        if (!segment.e().equals(FieldIndex.Segment.Kind.ASCENDING) || !orderBy.b().equals(OrderBy.Direction.ASCENDING)) {
            boolean b2 = b;
            if (!segment.e().equals(FieldIndex.Segment.Kind.DESCENDING)) {
                return b2;
            }
            b2 = b;
            if (!orderBy.b().equals(OrderBy.Direction.DESCENDING)) {
                return b2;
            }
        }
        return true;
    }
    
    public boolean h(final FieldIndex fieldIndex) {
        g9.d(fieldIndex.d().equals(this.a), "Collection IDs do not match", new Object[0]);
        if (this.d()) {
            return false;
        }
        final FieldIndex.Segment c = fieldIndex.c();
        if (c != null && !this.c(c)) {
            return false;
        }
        final Iterator iterator = this.d.iterator();
        final List e = fieldIndex.e();
        final HashSet set = new HashSet();
        int n;
        for (n = 0; n < e.size() && this.c((FieldIndex.Segment)e.get(n)); ++n) {
            set.add(((FieldIndex.Segment)e.get(n)).d().d());
        }
        if (n == e.size()) {
            return true;
        }
        int n2 = n;
        while (true) {
            Label_0244: {
                if (this.b.size() <= 0) {
                    break Label_0244;
                }
                final FieldFilter fieldFilter = this.b.first();
                n2 = n;
                if (!set.contains(fieldFilter.f().d())) {
                    final FieldIndex.Segment segment = e.get(n);
                    if (this.f(fieldFilter, segment)) {
                        n2 = n;
                        if (this.g((OrderBy)iterator.next(), segment)) {
                            break Label_0241;
                        }
                    }
                    return false;
                }
                ++n2;
            }
            if (n2 >= e.size()) {
                return true;
            }
            final FieldIndex.Segment segment2 = e.get(n2);
            if (!iterator.hasNext() || !this.g((OrderBy)iterator.next(), segment2)) {
                return false;
            }
            continue;
        }
    }
}

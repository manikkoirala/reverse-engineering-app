// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.model;

import com.google.firestore.v1.Value;

public final class MutableDocument implements zt
{
    public final du b;
    public DocumentType c;
    public qo1 d;
    public qo1 e;
    public a11 f;
    public DocumentState g;
    
    public MutableDocument(final du b) {
        this.b = b;
        this.e = qo1.b;
    }
    
    public MutableDocument(final du b, final DocumentType c, final qo1 d, final qo1 e, final a11 f, final DocumentState g) {
        this.b = b;
        this.d = d;
        this.e = e;
        this.c = c;
        this.g = g;
        this.f = f;
    }
    
    public static MutableDocument p(final du du, final qo1 qo1, final a11 a11) {
        return new MutableDocument(du).l(qo1, a11);
    }
    
    public static MutableDocument q(final du du) {
        final DocumentType invalid = DocumentType.INVALID;
        final qo1 b = qo1.b;
        return new MutableDocument(du, invalid, b, b, new a11(), DocumentState.SYNCED);
    }
    
    public static MutableDocument r(final du du, final qo1 qo1) {
        return new MutableDocument(du).m(qo1);
    }
    
    public static MutableDocument s(final du du, final qo1 qo1) {
        return new MutableDocument(du).n(qo1);
    }
    
    @Override
    public MutableDocument a() {
        return new MutableDocument(this.b, this.c, this.d, this.e, this.f.d(), this.g);
    }
    
    @Override
    public boolean b() {
        return this.g() || this.f();
    }
    
    @Override
    public boolean c() {
        return this.c.equals(DocumentType.NO_DOCUMENT);
    }
    
    @Override
    public boolean d() {
        return this.c.equals(DocumentType.FOUND_DOCUMENT);
    }
    
    @Override
    public qo1 e() {
        return this.e;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o != null && MutableDocument.class == o.getClass()) {
            final MutableDocument mutableDocument = (MutableDocument)o;
            return this.b.equals(mutableDocument.b) && this.d.equals(mutableDocument.d) && this.c.equals(mutableDocument.c) && this.g.equals(mutableDocument.g) && this.f.equals(mutableDocument.f);
        }
        return false;
    }
    
    @Override
    public boolean f() {
        return this.g.equals(DocumentState.HAS_COMMITTED_MUTATIONS);
    }
    
    @Override
    public boolean g() {
        return this.g.equals(DocumentState.HAS_LOCAL_MUTATIONS);
    }
    
    @Override
    public a11 getData() {
        return this.f;
    }
    
    @Override
    public du getKey() {
        return this.b;
    }
    
    @Override
    public qo1 getVersion() {
        return this.d;
    }
    
    @Override
    public int hashCode() {
        return this.b.hashCode();
    }
    
    @Override
    public boolean i() {
        return this.c.equals(DocumentType.UNKNOWN_DOCUMENT);
    }
    
    @Override
    public Value j(final s00 s00) {
        return this.getData().j(s00);
    }
    
    public MutableDocument l(final qo1 d, final a11 f) {
        this.d = d;
        this.c = DocumentType.FOUND_DOCUMENT;
        this.f = f;
        this.g = DocumentState.SYNCED;
        return this;
    }
    
    public MutableDocument m(final qo1 d) {
        this.d = d;
        this.c = DocumentType.NO_DOCUMENT;
        this.f = new a11();
        this.g = DocumentState.SYNCED;
        return this;
    }
    
    public MutableDocument n(final qo1 d) {
        this.d = d;
        this.c = DocumentType.UNKNOWN_DOCUMENT;
        this.f = new a11();
        this.g = DocumentState.HAS_COMMITTED_MUTATIONS;
        return this;
    }
    
    public boolean o() {
        return this.c.equals(DocumentType.INVALID) ^ true;
    }
    
    public MutableDocument t() {
        this.g = DocumentState.HAS_COMMITTED_MUTATIONS;
        return this;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Document{key=");
        sb.append(this.b);
        sb.append(", version=");
        sb.append(this.d);
        sb.append(", readTime=");
        sb.append(this.e);
        sb.append(", type=");
        sb.append(this.c);
        sb.append(", documentState=");
        sb.append(this.g);
        sb.append(", value=");
        sb.append(this.f);
        sb.append('}');
        return sb.toString();
    }
    
    public MutableDocument u() {
        this.g = DocumentState.HAS_LOCAL_MUTATIONS;
        this.d = qo1.b;
        return this;
    }
    
    public MutableDocument v(final qo1 e) {
        this.e = e;
        return this;
    }
    
    public enum DocumentState
    {
        private static final DocumentState[] $VALUES;
        
        HAS_COMMITTED_MUTATIONS, 
        HAS_LOCAL_MUTATIONS, 
        SYNCED;
    }
    
    public enum DocumentType
    {
        private static final DocumentType[] $VALUES;
        
        FOUND_DOCUMENT, 
        INVALID, 
        NO_DOCUMENT, 
        UNKNOWN_DOCUMENT;
    }
}

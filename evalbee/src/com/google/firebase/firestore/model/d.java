// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.model;

public final class d extends Segment
{
    public final s00 a;
    public final Kind b;
    
    public d(final s00 a, final Kind b) {
        if (a == null) {
            throw new NullPointerException("Null fieldPath");
        }
        this.a = a;
        if (b != null) {
            this.b = b;
            return;
        }
        throw new NullPointerException("Null kind");
    }
    
    @Override
    public s00 d() {
        return this.a;
    }
    
    @Override
    public Kind e() {
        return this.b;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof Segment) {
            final Segment segment = (Segment)o;
            if (!this.a.equals(segment.d()) || !this.b.equals(segment.e())) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return (this.a.hashCode() ^ 0xF4243) * 1000003 ^ this.b.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Segment{fieldPath=");
        sb.append(this.a);
        sb.append(", kind=");
        sb.append(this.b);
        sb.append("}");
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Comparator;

public abstract class FieldIndex
{
    public static b a;
    public static final Comparator b;
    
    static {
        FieldIndex.a = FieldIndex.b.b(0L, FieldIndex.a.a);
        b = new o00();
    }
    
    public static FieldIndex b(final int n, final String s, final List list, final b b) {
        return new com.google.firebase.firestore.model.a(n, s, list, b);
    }
    
    public Segment c() {
        for (final Segment segment : this.h()) {
            if (segment.e().equals(Kind.CONTAINS)) {
                return segment;
            }
        }
        return null;
    }
    
    public abstract String d();
    
    public List e() {
        final ArrayList list = new ArrayList();
        for (final Segment segment : this.h()) {
            if (!segment.e().equals(Kind.CONTAINS)) {
                list.add(segment);
            }
        }
        return list;
    }
    
    public abstract int f();
    
    public abstract b g();
    
    public abstract List h();
    
    public abstract static class Segment implements Comparable
    {
        public static Segment c(final s00 s00, final Kind kind) {
            return (Segment)new d(s00, kind);
        }
        
        public int a(final Segment segment) {
            final int e = this.d().e(segment.d());
            if (e != 0) {
                return e;
            }
            return this.e().compareTo(segment.e());
        }
        
        public abstract s00 d();
        
        public abstract Kind e();
        
        public enum Kind
        {
            private static final Kind[] $VALUES;
            
            ASCENDING, 
            CONTAINS, 
            DESCENDING;
        }
    }
    
    public abstract static class a implements Comparable
    {
        public static final a a;
        public static final Comparator b;
        
        static {
            a = d(qo1.b, du.d(), -1);
            b = new p00();
        }
        
        public static a d(final qo1 qo1, final du du, final int n) {
            return (a)new com.google.firebase.firestore.model.b(qo1, du, n);
        }
        
        public static a e(final qo1 qo1, final int n) {
            final long e = qo1.c().e();
            final int n2 = qo1.c().d() + 1;
            pw1 pw1;
            if (n2 == 1.0E9) {
                pw1 = new pw1(e + 1L, 0);
            }
            else {
                pw1 = new pw1(e, n2);
            }
            return d(new qo1(pw1), du.d(), n);
        }
        
        public static a f(final zt zt) {
            return d(zt.e(), zt.getKey(), -1);
        }
        
        public int c(final a a) {
            final int a2 = this.j().a(a.j());
            if (a2 != 0) {
                return a2;
            }
            final int c = this.g().c(a.g());
            if (c != 0) {
                return c;
            }
            return Integer.compare(this.h(), a.h());
        }
        
        public abstract du g();
        
        public abstract int h();
        
        public abstract qo1 j();
    }
    
    public abstract static class b
    {
        public static b a(final long n, final qo1 qo1, final du du, final int n2) {
            return b(n, FieldIndex.a.d(qo1, du, n2));
        }
        
        public static b b(final long n, final a a) {
            return (b)new c(n, a);
        }
        
        public abstract a c();
        
        public abstract long d();
    }
}

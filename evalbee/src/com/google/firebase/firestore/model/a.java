// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.model;

import java.util.List;

public final class a extends FieldIndex
{
    public final int c;
    public final String d;
    public final List e;
    public final b f;
    
    public a(final int c, final String d, final List e, final b f) {
        this.c = c;
        if (d == null) {
            throw new NullPointerException("Null collectionGroup");
        }
        this.d = d;
        if (e == null) {
            throw new NullPointerException("Null segments");
        }
        this.e = e;
        if (f != null) {
            this.f = f;
            return;
        }
        throw new NullPointerException("Null indexState");
    }
    
    @Override
    public String d() {
        return this.d;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof FieldIndex) {
            final FieldIndex fieldIndex = (FieldIndex)o;
            if (this.c != fieldIndex.f() || !this.d.equals(fieldIndex.d()) || !this.e.equals(fieldIndex.h()) || !this.f.equals(fieldIndex.g())) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int f() {
        return this.c;
    }
    
    @Override
    public b g() {
        return this.f;
    }
    
    @Override
    public List h() {
        return this.e;
    }
    
    @Override
    public int hashCode() {
        return (((this.c ^ 0xF4243) * 1000003 ^ this.d.hashCode()) * 1000003 ^ this.e.hashCode()) * 1000003 ^ this.f.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("FieldIndex{indexId=");
        sb.append(this.c);
        sb.append(", collectionGroup=");
        sb.append(this.d);
        sb.append(", segments=");
        sb.append(this.e);
        sb.append(", indexState=");
        sb.append(this.f);
        sb.append("}");
        return sb.toString();
    }
}

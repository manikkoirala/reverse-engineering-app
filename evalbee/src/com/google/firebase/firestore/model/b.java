// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.model;

public final class b extends a
{
    public final qo1 c;
    public final du d;
    public final int e;
    
    public b(final qo1 c, final du d, final int e) {
        if (c == null) {
            throw new NullPointerException("Null readTime");
        }
        this.c = c;
        if (d != null) {
            this.d = d;
            this.e = e;
            return;
        }
        throw new NullPointerException("Null documentKey");
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof a) {
            final a a = (a)o;
            if (!this.c.equals(a.j()) || !this.d.equals(a.g()) || this.e != a.h()) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public du g() {
        return this.d;
    }
    
    @Override
    public int h() {
        return this.e;
    }
    
    @Override
    public int hashCode() {
        return ((this.c.hashCode() ^ 0xF4243) * 1000003 ^ this.d.hashCode()) * 1000003 ^ this.e;
    }
    
    @Override
    public qo1 j() {
        return this.c;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("IndexOffset{readTime=");
        sb.append(this.c);
        sb.append(", documentKey=");
        sb.append(this.d);
        sb.append(", largestBatchId=");
        sb.append(this.e);
        sb.append("}");
        return sb.toString();
    }
}

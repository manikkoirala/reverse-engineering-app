// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore;

import androidx.annotation.Keep;
import com.google.firebase.firestore.core.g;
import com.google.firebase.firestore.util.AsyncQueue;
import android.content.Context;

public class FirebaseFirestore
{
    public final Context a;
    public final qp b;
    public final String c;
    public final eo d;
    public final eo e;
    public final AsyncQueue f;
    public final r10 g;
    public final b22 h;
    public final a i;
    public b j;
    public volatile g k;
    public final fc0 l;
    
    public FirebaseFirestore(final Context context, final qp qp, final String s, final eo eo, final eo eo2, final AsyncQueue asyncQueue, final r10 g, final a i, final fc0 l) {
        this.a = (Context)k71.b(context);
        this.b = (qp)k71.b(k71.b(qp));
        this.h = new b22(qp);
        this.c = (String)k71.b(s);
        this.d = (eo)k71.b(eo);
        this.e = (eo)k71.b(eo2);
        this.f = (AsyncQueue)k71.b(asyncQueue);
        this.g = g;
        this.i = i;
        this.l = l;
        this.j = new b.b().f();
    }
    
    public static r10 e() {
        final r10 m = r10.m();
        if (m != null) {
            return m;
        }
        throw new IllegalStateException("You must call FirebaseApp.initializeApp first.");
    }
    
    public static FirebaseFirestore f() {
        return g(e(), "(default)");
    }
    
    public static FirebaseFirestore g(final r10 r10, final String s) {
        k71.c(r10, "Provided FirebaseApp must not be null.");
        k71.c(s, "Provided database name must not be null.");
        final e e = (e)r10.j(e.class);
        k71.c(e, "Firestore component is not present.");
        return e.a(s);
    }
    
    public static FirebaseFirestore i(final Context context, final r10 r10, final jr jr, final jr jr2, final String s, final a a, final fc0 fc0) {
        final String e = r10.p().e();
        if (e != null) {
            return new FirebaseFirestore(context, qp.c(e, s), r10.o(), new c20(jr), new x10(jr2), new AsyncQueue(), r10, a, fc0);
        }
        throw new IllegalArgumentException("FirebaseOptions.getProjectId() cannot be null");
    }
    
    @Keep
    public static void setClientLanguage(final String s) {
        w30.h(s);
    }
    
    public ih a(final String s) {
        k71.c(s, "Provided collection path must not be null.");
        this.b();
        return new ih(ke1.q(s), this);
    }
    
    public final void b() {
        if (this.k != null) {
            return;
        }
        synchronized (this.b) {
            if (this.k != null) {
                return;
            }
            this.k = new g(this.a, new rp(this.b, this.c, this.j.c(), this.j.e()), this.j, this.d, this.e, this.f, this.l);
        }
    }
    
    public g c() {
        return this.k;
    }
    
    public qp d() {
        return this.b;
    }
    
    public b22 h() {
        return this.h;
    }
    
    public interface a
    {
    }
}

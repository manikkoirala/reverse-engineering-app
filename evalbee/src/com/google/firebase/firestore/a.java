// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore;

import java.util.Map;
import java.util.List;
import java.util.Collections;
import com.google.android.gms.tasks.Continuation;
import com.google.firebase.firestore.core.Query;
import android.app.Activity;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutionException;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.firestore.core.f;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.firestore.core.ViewSnapshot;

public class a
{
    public final du a;
    public final FirebaseFirestore b;
    
    public a(final du du, final FirebaseFirestore b) {
        this.a = (du)k71.b(du);
        this.b = b;
    }
    
    public static a g(final ke1 ke1, final FirebaseFirestore firebaseFirestore) {
        if (ke1.l() % 2 == 0) {
            return new a(du.g(ke1), firebaseFirestore);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid document reference. Document references must have an even number of segments, but ");
        sb.append(ke1.d());
        sb.append(" has ");
        sb.append(ke1.l());
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static f.a m(final MetadataChanges metadataChanges) {
        final f.a a = new f.a();
        final MetadataChanges include = MetadataChanges.INCLUDE;
        final boolean b = true;
        a.a = (metadataChanges == include);
        a.b = (metadataChanges == include && b);
        a.c = false;
        return a;
    }
    
    public mk0 d(final Executor executor, final MetadataChanges metadataChanges, final rx rx) {
        k71.c(executor, "Provided executor must not be null.");
        k71.c(metadataChanges, "Provided MetadataChanges value must not be null.");
        k71.c(rx, "Provided EventListener must not be null.");
        return this.e(executor, m(metadataChanges), null, rx);
    }
    
    public final mk0 e(final Executor executor, final f.a a, final Activity activity, final rx rx) {
        final j9 j9 = new j9(executor, new ju(this, rx));
        return w2.c(activity, new nk0(this.b.c(), this.b.c().v(this.f(), a, j9), j9));
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof a)) {
            return false;
        }
        final a a = (a)o;
        if (!this.a.equals(a.a) || !this.b.equals(a.b)) {
            b = false;
        }
        return b;
    }
    
    public final Query f() {
        return Query.b(this.a.m());
    }
    
    public Task h() {
        return this.i(Source.DEFAULT);
    }
    
    @Override
    public int hashCode() {
        return this.a.hashCode() * 31 + this.b.hashCode();
    }
    
    public Task i(final Source source) {
        if (source == Source.CACHE) {
            return this.b.c().j(this.a).continueWith(wy.b, (Continuation)new gu(this));
        }
        return this.l(source);
    }
    
    public FirebaseFirestore j() {
        return this.b;
    }
    
    public String k() {
        return this.a.m().d();
    }
    
    public final Task l(final Source source) {
        final TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        final TaskCompletionSource taskCompletionSource2 = new TaskCompletionSource();
        final f.a a = new f.a();
        a.a = true;
        a.b = true;
        a.c = true;
        taskCompletionSource2.setResult((Object)this.e(wy.b, a, null, new iu(taskCompletionSource, taskCompletionSource2, source)));
        return taskCompletionSource.getTask();
    }
    
    public Task q(final Object o, final rm1 rm1) {
        k71.c(o, "Provided data must not be null.");
        k71.c(rm1, "Provided options must not be null.");
        z12 z12;
        if (rm1.b()) {
            z12 = this.b.h().f(o, rm1.a());
        }
        else {
            z12 = this.b.h().i(o);
        }
        return this.b.c().y(Collections.singletonList(z12.a(this.a, h71.c))).continueWith(wy.b, o22.A());
    }
    
    public final Task r(final a22 a22) {
        return this.b.c().y(Collections.singletonList(a22.a(this.a, h71.a(true)))).continueWith(wy.b, o22.A());
    }
    
    public Task s(final Map map) {
        return this.r(this.b.h().k(map));
    }
}

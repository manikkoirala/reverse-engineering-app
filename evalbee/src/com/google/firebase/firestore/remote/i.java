// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.remote;

import com.google.firestore.v1.ListenResponse;
import java.util.Map;
import com.google.protobuf.GeneratedMessageLite;
import com.google.firestore.v1.ListenRequest;
import com.google.firebase.firestore.util.AsyncQueue;
import com.google.protobuf.ByteString;

public class i extends g0
{
    public static final ByteString t;
    public final f s;
    
    static {
        t = ByteString.EMPTY;
    }
    
    public i(final w30 w30, final AsyncQueue asyncQueue, final f s, final a a) {
        super(w30, h40.a(), asyncQueue, AsyncQueue.TimerId.LISTEN_STREAM_CONNECTION_BACKOFF, AsyncQueue.TimerId.LISTEN_STREAM_IDLE, AsyncQueue.TimerId.HEALTH_CHECK_TIMEOUT, a);
        this.s = s;
    }
    
    public void A(final au1 au1) {
        g9.d(this.m(), "Watching queries requires an open stream", new Object[0]);
        final ListenRequest.b b = ListenRequest.h0().C(this.s.a()).B(this.s.R(au1));
        final Map k = this.s.K(au1);
        if (k != null) {
            b.A(k);
        }
        this.x(((GeneratedMessageLite.a)b).p());
    }
    
    public void y(final ListenResponse listenResponse) {
        super.l.f();
        ((a)super.m).e(this.s.w(listenResponse), this.s.x(listenResponse));
    }
    
    public void z(final int n) {
        g9.d(this.m(), "Unwatching targets requires an open stream", new Object[0]);
        this.x(((GeneratedMessageLite.a)ListenRequest.h0().C(this.s.a()).D(n)).p());
    }
    
    public interface a extends cr1
    {
        void e(final qo1 p0, final WatchChange p1);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.remote;

import com.google.firestore.v1.c;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.CopyOnWriteArrayList;

public final class h
{
    public static final h b;
    public final CopyOnWriteArrayList a;
    
    static {
        b = new h();
    }
    
    public h() {
        this.a = new CopyOnWriteArrayList();
    }
    
    public static h a() {
        return h.b;
    }
    
    public void b(final b b) {
        final Iterator iterator = this.a.iterator();
        while (iterator.hasNext()) {
            zu0.a(((AtomicReference<Object>)iterator.next()).get());
        }
    }
    
    public abstract static class a
    {
        public static a d(final BloomFilter bloomFilter, final boolean b, final int n, final int n2, final int n3) {
            return (a)new com.google.firebase.firestore.remote.b(bloomFilter, b, n, n2, n3);
        }
        
        public static a e(final BloomFilter bloomFilter, final WatchChangeAggregator.BloomFilterApplicationStatus bloomFilterApplicationStatus, final hz hz) {
            final c b = hz.b();
            if (b == null) {
                return null;
            }
            return d(bloomFilter, bloomFilterApplicationStatus == WatchChangeAggregator.BloomFilterApplicationStatus.SUCCESS, b.c0(), b.a0().a0().size(), b.a0().c0());
        }
        
        public abstract boolean a();
        
        public abstract int b();
        
        public abstract BloomFilter c();
        
        public abstract int f();
        
        public abstract int g();
    }
    
    public abstract static class b
    {
        public static b b(final int n, final int n2, final String s, final String s2, final a a) {
            return (b)new com.google.firebase.firestore.remote.c(n, n2, s, s2, a);
        }
        
        public static b e(final int n, final hz hz, final qp qp, final BloomFilter bloomFilter, final WatchChangeAggregator.BloomFilterApplicationStatus bloomFilterApplicationStatus) {
            return b(n, hz.a(), qp.f(), qp.e(), a.e(bloomFilter, bloomFilterApplicationStatus, hz));
        }
        
        public abstract a a();
        
        public abstract String c();
        
        public abstract int d();
        
        public abstract int f();
        
        public abstract String g();
    }
}

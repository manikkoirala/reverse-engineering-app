// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.remote;

import com.google.firebase.database.collection.c;
import com.google.firebase.firestore.local.QueryPurpose;
import com.google.protobuf.ByteString;
import java.util.Iterator;
import com.google.firebase.firestore.util.Logger;
import com.google.firebase.firestore.core.OnlineState;
import java.util.List;
import io.grpc.Status;
import java.util.Objects;
import java.util.ArrayDeque;
import java.util.HashMap;
import com.google.firebase.firestore.util.AsyncQueue;
import java.util.Deque;
import java.util.Map;
import com.google.firebase.firestore.local.a;

public final class g implements b
{
    public final c a;
    public final a b;
    public final d c;
    public final ConnectivityMonitor d;
    public final Map e;
    public final e f;
    public boolean g;
    public final i h;
    public final j i;
    public WatchChangeAggregator j;
    public final Deque k;
    
    public g(final c c, final a b, final d c2, final AsyncQueue asyncQueue, final ConnectivityMonitor d) {
        this.g = false;
        this.a = c;
        this.b = b;
        this.c = c2;
        this.d = d;
        this.e = new HashMap();
        this.k = new ArrayDeque();
        Objects.requireNonNull(c);
        this.f = new e(asyncQueue, (e.a)new md1(c));
        this.h = c2.a(new i.a(this) {
            public final g a;
            
            @Override
            public void a(final Status status) {
                this.a.v(status);
            }
            
            @Override
            public void b() {
                this.a.w();
            }
            
            @Override
            public void e(final qo1 qo1, final WatchChange watchChange) {
                this.a.u(qo1, watchChange);
            }
        });
        this.i = c2.b(new j.a(this) {
            public final g a;
            
            @Override
            public void a(final Status status) {
                this.a.z(status);
            }
            
            @Override
            public void b() {
                g.i(this.a).C();
            }
            
            @Override
            public void c() {
                this.a.A();
            }
            
            @Override
            public void d(final qo1 qo1, final List list) {
                this.a.B(qo1, list);
            }
        });
        d.a(new nd1(this, asyncQueue));
    }
    
    public static /* synthetic */ j i(final g g) {
        return g.i;
    }
    
    public final void A() {
        this.b.Q(this.i.y());
        final Iterator iterator = this.k.iterator();
        while (iterator.hasNext()) {
            this.i.D(((xx0)iterator.next()).h());
        }
    }
    
    public final void B(final qo1 qo1, final List list) {
        this.a.e(yx0.a(this.k.poll(), qo1, list, this.i.y()));
        this.s();
    }
    
    public void E(final au1 au1) {
        final Integer value = au1.h();
        if (this.e.containsKey(value)) {
            return;
        }
        this.e.put(value, au1);
        if (this.K()) {
            this.N();
        }
        else if (this.h.m()) {
            this.J(au1);
        }
    }
    
    public final void F(final WatchChange.d d) {
        g9.d(d.a() != null, "Processing target error without a cause", new Object[0]);
        for (final Integer n : d.d()) {
            if (this.e.containsKey(n)) {
                this.e.remove(n);
                this.j.q(n);
                this.a.c(n, d.a());
            }
        }
    }
    
    public final void G(final qo1 qo1) {
        g9.d(qo1.equals(qo1.b) ^ true, "Can't raise event for unknown SnapshotVersion", new Object[0]);
        final jd1 c = this.j.c(qo1);
        for (final Map.Entry<K, zt1> entry : c.d().entrySet()) {
            final zt1 zt1 = entry.getValue();
            if (!zt1.e().isEmpty()) {
                final int intValue = (int)entry.getKey();
                final au1 au1 = this.e.get(intValue);
                if (au1 == null) {
                    continue;
                }
                this.e.put(intValue, au1.k(zt1.e(), qo1));
            }
        }
        for (final Map.Entry<Integer, V> entry2 : c.e().entrySet()) {
            final int intValue2 = entry2.getKey();
            final au1 au2 = this.e.get(intValue2);
            if (au2 != null) {
                this.e.put(intValue2, au2.k(ByteString.EMPTY, au2.f()));
                this.I(intValue2);
                this.J(new au1(au2.g(), intValue2, au2.e(), (QueryPurpose)entry2.getValue()));
            }
        }
        this.a.d(c);
    }
    
    public final void H() {
        this.g = false;
        this.q();
        this.f.i(OnlineState.UNKNOWN);
        this.i.l();
        this.h.l();
        this.r();
    }
    
    public final void I(final int n) {
        this.j.o(n);
        this.h.z(n);
    }
    
    public final void J(final au1 au1) {
        this.j.o(au1.h());
        au1 i = null;
        Label_0055: {
            if (au1.d().isEmpty()) {
                i = au1;
                if (au1.f().a(qo1.b) <= 0) {
                    break Label_0055;
                }
            }
            i = au1.i(this.b(au1.h()).size());
        }
        this.h.A(i);
    }
    
    public final boolean K() {
        return this.o() && !this.h.n() && !this.e.isEmpty();
    }
    
    public final boolean L() {
        return this.o() && !this.i.n() && !this.k.isEmpty();
    }
    
    public void M() {
        this.r();
    }
    
    public final void N() {
        g9.d(this.K(), "startWatchStream() called when shouldStartWatchStream() is false.", new Object[0]);
        this.j = new WatchChangeAggregator((WatchChangeAggregator.b)this);
        this.h.u();
        this.f.e();
    }
    
    public final void O() {
        g9.d(this.L(), "startWriteStream() called when shouldStartWriteStream() is false.", new Object[0]);
        this.i.u();
    }
    
    public void P(final int n) {
        g9.d(this.e.remove(n) != null, "stopListening called on target no currently watched: %d", n);
        if (this.h.m()) {
            this.I(n);
        }
        if (this.e.isEmpty()) {
            if (this.h.m()) {
                this.h.q();
            }
            else if (this.o()) {
                this.f.i(OnlineState.UNKNOWN);
            }
        }
    }
    
    @Override
    public qp a() {
        return this.c.c().a();
    }
    
    @Override
    public com.google.firebase.database.collection.c b(final int n) {
        return this.a.b(n);
    }
    
    @Override
    public au1 c(final int i) {
        return this.e.get(i);
    }
    
    public final void m(final xx0 xx0) {
        g9.d(this.n(), "addToWritePipeline called when pipeline is full", new Object[0]);
        this.k.add(xx0);
        if (this.i.m() && this.i.z()) {
            this.i.D(xx0.h());
        }
    }
    
    public final boolean n() {
        return this.o() && this.k.size() < 10;
    }
    
    public boolean o() {
        return this.g;
    }
    
    public final void p() {
        this.j = null;
    }
    
    public final void q() {
        this.h.v();
        this.i.v();
        if (!this.k.isEmpty()) {
            Logger.a("RemoteStore", "Stopping write stream with %d pending writes", this.k.size());
            this.k.clear();
        }
        this.p();
    }
    
    public void r() {
        this.g = true;
        if (this.o()) {
            this.i.B(this.b.u());
            if (this.K()) {
                this.N();
            }
            else {
                this.f.i(OnlineState.UNKNOWN);
            }
            this.s();
        }
    }
    
    public void s() {
        while (true) {
            xx0 w = null;
            Label_0080: {
                if (!this.k.isEmpty()) {
                    w = this.k.getLast();
                    break Label_0080;
                }
                final int e = -1;
                if (this.n()) {
                    w = this.b.w(e);
                    if (w != null) {
                        this.m(w);
                        break Label_0080;
                    }
                    if (this.k.size() == 0) {
                        this.i.q();
                    }
                }
                if (this.L()) {
                    this.O();
                }
                return;
            }
            final int e = w.e();
            continue;
        }
    }
    
    public void t() {
        if (this.o()) {
            Logger.a("RemoteStore", "Restarting streams for new credential.", new Object[0]);
            this.H();
        }
    }
    
    public final void u(final qo1 qo1, final WatchChange watchChange) {
        this.f.i(OnlineState.ONLINE);
        g9.d(this.h != null && this.j != null, "WatchStream and WatchStreamAggregator should both be non-null", new Object[0]);
        final boolean b = watchChange instanceof WatchChange.d;
        WatchChange.d d;
        if (b) {
            d = (WatchChange.d)watchChange;
        }
        else {
            d = null;
        }
        if (d != null && d.b().equals(WatchChange.WatchTargetChangeType.Removed) && d.a() != null) {
            this.F(d);
        }
        else {
            if (watchChange instanceof WatchChange.b) {
                this.j.i((WatchChange.b)watchChange);
            }
            else if (watchChange instanceof WatchChange.c) {
                this.j.j((WatchChange.c)watchChange);
            }
            else {
                g9.d(b, "Expected watchChange to be an instance of WatchTargetChange", new Object[0]);
                this.j.k((WatchChange.d)watchChange);
            }
            if (!qo1.equals(qo1.b) && qo1.a(this.b.t()) >= 0) {
                this.G(qo1);
            }
        }
    }
    
    public final void v(final Status status) {
        if (status.isOk()) {
            g9.d(this.K() ^ true, "Watch stream was stopped gracefully while still needed.", new Object[0]);
        }
        this.p();
        if (this.K()) {
            this.f.d(status);
            this.N();
        }
        else {
            this.f.i(OnlineState.UNKNOWN);
        }
    }
    
    public final void w() {
        final Iterator iterator = this.e.values().iterator();
        while (iterator.hasNext()) {
            this.J((au1)iterator.next());
        }
    }
    
    public final void x(final Status status) {
        g9.d(status.isOk() ^ true, "Handling write error with status OK.", new Object[0]);
        if (com.google.firebase.firestore.remote.d.h(status)) {
            final xx0 xx0 = this.k.poll();
            this.i.l();
            this.a.f(xx0.e(), status);
            this.s();
        }
    }
    
    public final void y(final Status status) {
        g9.d(status.isOk() ^ true, "Handling write error with status OK.", new Object[0]);
        if (com.google.firebase.firestore.remote.d.g(status)) {
            Logger.a("RemoteStore", "RemoteStore error before completed handshake; resetting stream token %s: %s", o22.y(this.i.y()), status);
            final j i = this.i;
            final ByteString v = com.google.firebase.firestore.remote.j.v;
            i.B(v);
            this.b.Q(v);
        }
    }
    
    public final void z(final Status status) {
        if (status.isOk()) {
            g9.d(this.L() ^ true, "Write stream was stopped gracefully while still needed.", new Object[0]);
        }
        if (!status.isOk() && !this.k.isEmpty()) {
            if (this.i.z()) {
                this.x(status);
            }
            else {
                this.y(status);
            }
        }
        if (this.L()) {
            this.O();
        }
    }
    
    public interface c
    {
        void a(final OnlineState p0);
        
        com.google.firebase.database.collection.c b(final int p0);
        
        void c(final int p0, final Status p1);
        
        void d(final jd1 p0);
        
        void e(final yx0 p0);
        
        void f(final int p0, final Status p1);
    }
}

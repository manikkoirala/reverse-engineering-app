// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.remote;

public enum Stream$State
{
    private static final Stream$State[] $VALUES;
    
    Backoff, 
    Error, 
    Healthy, 
    Initial, 
    Open, 
    Starting;
}

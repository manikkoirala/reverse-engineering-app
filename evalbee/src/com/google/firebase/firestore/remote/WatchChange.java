// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.remote;

import io.grpc.Status;
import com.google.protobuf.ByteString;
import com.google.firebase.firestore.model.MutableDocument;
import java.util.List;

public abstract class WatchChange
{
    public enum WatchTargetChangeType
    {
        private static final WatchTargetChangeType[] $VALUES;
        
        Added, 
        Current, 
        NoChange, 
        Removed, 
        Reset;
    }
    
    public static final class b extends WatchChange
    {
        public final List a;
        public final List b;
        public final du c;
        public final MutableDocument d;
        
        public b(final List a, final List b, final du c, final MutableDocument d) {
            super(null);
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
        }
        
        public du a() {
            return this.c;
        }
        
        public MutableDocument b() {
            return this.d;
        }
        
        public List c() {
            return this.b;
        }
        
        public List d() {
            return this.a;
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean equals = true;
            if (this == o) {
                return true;
            }
            if (o == null || b.class != o.getClass()) {
                return false;
            }
            final b b = (b)o;
            if (!this.a.equals(b.a)) {
                return false;
            }
            if (!this.b.equals(b.b)) {
                return false;
            }
            if (!this.c.equals(b.c)) {
                return false;
            }
            final MutableDocument d = this.d;
            final MutableDocument d2 = b.d;
            if (d != null) {
                equals = d.equals(d2);
            }
            else if (d2 != null) {
                equals = false;
            }
            return equals;
        }
        
        @Override
        public int hashCode() {
            final int hashCode = this.a.hashCode();
            final int hashCode2 = this.b.hashCode();
            final int hashCode3 = this.c.hashCode();
            final MutableDocument d = this.d;
            int hashCode4;
            if (d != null) {
                hashCode4 = d.hashCode();
            }
            else {
                hashCode4 = 0;
            }
            return ((hashCode * 31 + hashCode2) * 31 + hashCode3) * 31 + hashCode4;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("DocumentChange{updatedTargetIds=");
            sb.append(this.a);
            sb.append(", removedTargetIds=");
            sb.append(this.b);
            sb.append(", key=");
            sb.append(this.c);
            sb.append(", newDocument=");
            sb.append(this.d);
            sb.append('}');
            return sb.toString();
        }
    }
    
    public static final class c extends WatchChange
    {
        public final int a;
        public final hz b;
        
        public c(final int a, final hz b) {
            super(null);
            this.a = a;
            this.b = b;
        }
        
        public hz a() {
            return this.b;
        }
        
        public int b() {
            return this.a;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("ExistenceFilterWatchChange{targetId=");
            sb.append(this.a);
            sb.append(", existenceFilter=");
            sb.append(this.b);
            sb.append('}');
            return sb.toString();
        }
    }
    
    public static final class d extends WatchChange
    {
        public final WatchTargetChangeType a;
        public final List b;
        public final ByteString c;
        public final Status d;
        
        public d(final WatchTargetChangeType a, final List b, final ByteString c, final Status d) {
            super(null);
            g9.d(d == null || a == WatchTargetChangeType.Removed, "Got cause for a target change that was not a removal", new Object[0]);
            this.a = a;
            this.b = b;
            this.c = c;
            if (d != null && !d.isOk()) {
                this.d = d;
            }
            else {
                this.d = null;
            }
        }
        
        public Status a() {
            return this.d;
        }
        
        public WatchTargetChangeType b() {
            return this.a;
        }
        
        public ByteString c() {
            return this.c;
        }
        
        public List d() {
            return this.b;
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            final boolean b2 = true;
            if (this == o) {
                return true;
            }
            if (o == null || d.class != o.getClass()) {
                return false;
            }
            final d d = (d)o;
            if (this.a != d.a) {
                return false;
            }
            if (!this.b.equals(d.b)) {
                return false;
            }
            if (!this.c.equals(d.c)) {
                return false;
            }
            final Status d2 = this.d;
            if (d2 != null) {
                return d.d != null && d2.getCode().equals(d.d.getCode()) && b2;
            }
            if (d.d != null) {
                b = false;
            }
            return b;
        }
        
        @Override
        public int hashCode() {
            final int hashCode = this.a.hashCode();
            final int hashCode2 = this.b.hashCode();
            final int hashCode3 = this.c.hashCode();
            final Status d = this.d;
            int hashCode4;
            if (d != null) {
                hashCode4 = d.getCode().hashCode();
            }
            else {
                hashCode4 = 0;
            }
            return ((hashCode * 31 + hashCode2) * 31 + hashCode3) * 31 + hashCode4;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("WatchTargetChange{changeType=");
            sb.append(this.a);
            sb.append(", targetIds=");
            sb.append(this.b);
            sb.append('}');
            return sb.toString();
        }
    }
}

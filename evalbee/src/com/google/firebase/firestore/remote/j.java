// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.remote;

import java.util.Collections;
import java.util.Iterator;
import com.google.firestore.v1.l;
import com.google.protobuf.GeneratedMessageLite;
import java.util.List;
import java.util.ArrayList;
import com.google.firestore.v1.m;
import com.google.firebase.firestore.util.AsyncQueue;
import com.google.protobuf.ByteString;

public class j extends g0
{
    public static final ByteString v;
    public final f s;
    public boolean t;
    public ByteString u;
    
    static {
        v = ByteString.EMPTY;
    }
    
    public j(final w30 w30, final AsyncQueue asyncQueue, final f s, final a a) {
        super(w30, h40.b(), asyncQueue, AsyncQueue.TimerId.WRITE_STREAM_CONNECTION_BACKOFF, AsyncQueue.TimerId.WRITE_STREAM_IDLE, AsyncQueue.TimerId.HEALTH_CHECK_TIMEOUT, a);
        this.t = false;
        this.u = j.v;
        this.s = s;
    }
    
    public void A(final m m) {
        this.u = m.c0();
        if (!this.t) {
            this.t = true;
            ((a)super.m).c();
        }
        else {
            super.l.f();
            final qo1 v = this.s.v(m.a0());
            final int e0 = m.e0();
            final ArrayList list = new ArrayList(e0);
            for (int i = 0; i < e0; ++i) {
                list.add((Object)this.s.m(m.d0(i), v));
            }
            ((a)super.m).d(v, list);
        }
    }
    
    public void B(final ByteString byteString) {
        this.u = (ByteString)k71.b(byteString);
    }
    
    public void C() {
        g9.d(this.m(), "Writing handshake requires an opened stream", new Object[0]);
        g9.d(this.t ^ true, "Handshake already completed", new Object[0]);
        this.x(((GeneratedMessageLite.a)com.google.firestore.v1.l.g0().B(this.s.a())).p());
    }
    
    public void D(final List list) {
        g9.d(this.m(), "Writing mutations requires an opened stream", new Object[0]);
        g9.d(this.t, "Handshake must be complete before writing mutations", new Object[0]);
        final l.b g0 = com.google.firestore.v1.l.g0();
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            g0.A(this.s.L((wx0)iterator.next()));
        }
        g0.C(this.u);
        this.x(((GeneratedMessageLite.a)g0).p());
    }
    
    @Override
    public void u() {
        this.t = false;
        super.u();
    }
    
    @Override
    public void w() {
        if (this.t) {
            this.D(Collections.emptyList());
        }
    }
    
    public ByteString y() {
        return this.u;
    }
    
    public boolean z() {
        return this.t;
    }
    
    public interface a extends cr1
    {
        void c();
        
        void d(final qo1 p0, final List p1);
    }
}

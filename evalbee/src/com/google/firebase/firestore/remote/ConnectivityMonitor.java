// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.remote;

public interface ConnectivityMonitor
{
    void a(final cl p0);
    
    public enum NetworkStatus
    {
        private static final NetworkStatus[] $VALUES;
        
        REACHABLE, 
        UNREACHABLE;
    }
}

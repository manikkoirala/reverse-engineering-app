// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.remote;

public final class b extends a
{
    public final BloomFilter a;
    public final boolean b;
    public final int c;
    public final int d;
    public final int e;
    
    public b(final BloomFilter a, final boolean b, final int c, final int d, final int e) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
    }
    
    @Override
    public boolean a() {
        return this.b;
    }
    
    @Override
    public int b() {
        return this.d;
    }
    
    @Override
    public BloomFilter c() {
        return this.a;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof a) {
            final a a = (a)o;
            final BloomFilter a2 = this.a;
            if (a2 == null) {
                if (a.c() != null) {
                    return false;
                }
            }
            else if (!a2.equals(a.c())) {
                return false;
            }
            if (this.b == a.a() && this.c == a.f() && this.d == a.b() && this.e == a.g()) {
                return b;
            }
            b = false;
            return b;
        }
        return false;
    }
    
    @Override
    public int f() {
        return this.c;
    }
    
    @Override
    public int g() {
        return this.e;
    }
    
    @Override
    public int hashCode() {
        final BloomFilter a = this.a;
        int hashCode;
        if (a == null) {
            hashCode = 0;
        }
        else {
            hashCode = a.hashCode();
        }
        int n;
        if (this.b) {
            n = 1231;
        }
        else {
            n = 1237;
        }
        return ((((hashCode ^ 0xF4243) * 1000003 ^ n) * 1000003 ^ this.c) * 1000003 ^ this.d) * 1000003 ^ this.e;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ExistenceFilterBloomFilterInfo{bloomFilter=");
        sb.append(this.a);
        sb.append(", applied=");
        sb.append(this.b);
        sb.append(", hashCount=");
        sb.append(this.c);
        sb.append(", bitmapLength=");
        sb.append(this.d);
        sb.append(", padding=");
        sb.append(this.e);
        sb.append("}");
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.remote;

import com.google.firebase.firestore.util.Logger;
import io.grpc.Status;
import java.util.Locale;
import com.google.firebase.firestore.util.AsyncQueue;
import com.google.firebase.firestore.core.OnlineState;

public class e
{
    public OnlineState a;
    public int b;
    public AsyncQueue.b c;
    public boolean d;
    public final AsyncQueue e;
    public final a f;
    
    public e(final AsyncQueue e, final a f) {
        this.e = e;
        this.f = f;
        this.a = OnlineState.UNKNOWN;
        this.d = true;
    }
    
    public final void b() {
        final AsyncQueue.b c = this.c;
        if (c != null) {
            c.c();
            this.c = null;
        }
    }
    
    public OnlineState c() {
        return this.a;
    }
    
    public void d(final Status status) {
        final OnlineState a = this.a;
        final OnlineState online = OnlineState.ONLINE;
        final boolean b = true;
        if (a == online) {
            this.h(OnlineState.UNKNOWN);
            g9.d(this.b == 0, "watchStreamFailures must be 0", new Object[0]);
            g9.d(this.c == null && b, "onlineStateTimer must be null", new Object[0]);
        }
        else if (++this.b >= 1) {
            this.b();
            this.g(String.format(Locale.ENGLISH, "Connection failed %d times. Most recent error: %s", 1, status));
            this.h(OnlineState.OFFLINE);
        }
    }
    
    public void e() {
        if (this.b == 0) {
            this.h(OnlineState.UNKNOWN);
            g9.d(this.c == null, "onlineStateTimer shouldn't be started yet", new Object[0]);
            this.c = this.e.h(AsyncQueue.TimerId.ONLINE_STATE_TIMEOUT, 10000L, new u11(this));
        }
    }
    
    public final void g(String format) {
        format = String.format("Could not reach Cloud Firestore backend. %s\nThis typically indicates that your device does not have a healthy Internet connection at the moment. The client will operate in offline mode until it is able to successfully connect to the backend.", format);
        final boolean d = this.d;
        final Object[] array = { format };
        if (d) {
            Logger.d("OnlineStateTracker", "%s", array);
            this.d = false;
        }
        else {
            Logger.a("OnlineStateTracker", "%s", array);
        }
    }
    
    public final void h(final OnlineState a) {
        if (a != this.a) {
            this.a = a;
            this.f.a(a);
        }
    }
    
    public void i(final OnlineState onlineState) {
        this.b();
        this.b = 0;
        if (onlineState == OnlineState.ONLINE) {
            this.d = false;
        }
        this.h(onlineState);
    }
    
    public interface a
    {
        void a(final OnlineState p0);
    }
}

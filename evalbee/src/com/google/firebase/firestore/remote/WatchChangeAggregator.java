// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.remote;

import com.google.protobuf.ByteString;
import com.google.firestore.v1.c;
import com.google.firebase.firestore.util.Logger;
import com.google.firebase.firestore.core.q;
import java.util.List;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Collections;
import com.google.firebase.firestore.local.QueryPurpose;
import java.util.Set;
import java.util.HashSet;
import com.google.firebase.firestore.core.DocumentViewChange;
import com.google.firebase.firestore.model.MutableDocument;
import java.util.HashMap;
import java.util.Map;

public class WatchChangeAggregator
{
    public final b a;
    public final Map b;
    public Map c;
    public Map d;
    public Map e;
    
    public WatchChangeAggregator(final b a) {
        this.b = new HashMap();
        this.c = new HashMap();
        this.d = new HashMap();
        this.e = new HashMap();
        this.a = a;
    }
    
    public final void a(final int i, final MutableDocument mutableDocument) {
        if (!this.l(i)) {
            return;
        }
        DocumentViewChange.Type type;
        if (this.s(i, mutableDocument.getKey())) {
            type = DocumentViewChange.Type.MODIFIED;
        }
        else {
            type = DocumentViewChange.Type.ADDED;
        }
        this.e(i).a(mutableDocument.getKey(), type);
        this.c.put(mutableDocument.getKey(), mutableDocument);
        this.d(mutableDocument.getKey()).add(i);
    }
    
    public final BloomFilterApplicationStatus b(final BloomFilter bloomFilter, final WatchChange.c c, final int n) {
        BloomFilterApplicationStatus bloomFilterApplicationStatus;
        if (c.a().a() == n - this.f(bloomFilter, c.b())) {
            bloomFilterApplicationStatus = BloomFilterApplicationStatus.SUCCESS;
        }
        else {
            bloomFilterApplicationStatus = BloomFilterApplicationStatus.FALSE_POSITIVE;
        }
        return bloomFilterApplicationStatus;
    }
    
    public jd1 c(final qo1 qo1) {
        final HashMap m = new HashMap();
        for (final Map.Entry<Integer, V> entry : this.b.entrySet()) {
            final int intValue = entry.getKey();
            final du1 du1 = (du1)entry.getValue();
            final au1 n = this.n(intValue);
            if (n != null) {
                if (du1.d() && n.g().s()) {
                    final du g = du.g(n.g().n());
                    if (this.c.get(g) == null && !this.s(intValue, g)) {
                        this.p(intValue, g, MutableDocument.r(g, qo1));
                    }
                }
                if (!du1.c()) {
                    continue;
                }
                m.put(intValue, du1.j());
                du1.b();
            }
        }
        final HashSet s = new HashSet();
    Label_0210:
        for (final Map.Entry<du, V> entry2 : this.d.entrySet()) {
            final du du2 = entry2.getKey();
            final Iterator iterator3 = ((Set)entry2.getValue()).iterator();
            while (true) {
                while (iterator3.hasNext()) {
                    final au1 n2 = this.n((int)iterator3.next());
                    if (n2 != null && !n2.c().equals(QueryPurpose.LIMBO_RESOLUTION)) {
                        final boolean b = false;
                        if (b) {
                            s.add(du2);
                            continue Label_0210;
                        }
                        continue Label_0210;
                    }
                }
                final boolean b = true;
                continue;
            }
        }
        final Iterator iterator4 = this.c.values().iterator();
        while (iterator4.hasNext()) {
            ((MutableDocument)iterator4.next()).v(qo1);
        }
        final jd1 jd1 = new jd1(qo1, Collections.unmodifiableMap((Map<?, ?>)m), Collections.unmodifiableMap((Map<?, ?>)this.e), Collections.unmodifiableMap((Map<?, ?>)this.c), Collections.unmodifiableSet((Set<?>)s));
        this.c = new HashMap();
        this.d = new HashMap();
        this.e = new HashMap();
        return jd1;
    }
    
    public final Set d(final du du) {
        Set set;
        if ((set = this.d.get(du)) == null) {
            set = new HashSet();
            this.d.put(du, set);
        }
        return set;
    }
    
    public final du1 e(final int n) {
        du1 du1;
        if ((du1 = this.b.get(n)) == null) {
            du1 = new du1();
            this.b.put(n, du1);
        }
        return du1;
    }
    
    public final int f(final BloomFilter bloomFilter, final int n) {
        final Iterator iterator = this.a.b(n).iterator();
        int n2 = 0;
        while (iterator.hasNext()) {
            final du du = iterator.next();
            final qp a = this.a.a();
            final StringBuilder sb = new StringBuilder();
            sb.append("projects/");
            sb.append(a.f());
            sb.append("/databases/");
            sb.append(a.e());
            sb.append("/documents/");
            sb.append(du.m().d());
            if (!bloomFilter.h(sb.toString())) {
                this.p(n, du, null);
                ++n2;
            }
        }
        return n2;
    }
    
    public final int g(final int n) {
        final zt1 j = this.e(n).j();
        return this.a.b(n).size() + j.b().size() - j.d().size();
    }
    
    public final Collection h(final WatchChange.d d) {
        final List d2 = d.d();
        if (!d2.isEmpty()) {
            return d2;
        }
        final ArrayList list = new ArrayList();
        for (final Integer n : this.b.keySet()) {
            if (this.l(n)) {
                list.add(n);
            }
        }
        return list;
    }
    
    public void i(final WatchChange.b b) {
        final MutableDocument b2 = b.b();
        final du a = b.a();
        for (final int intValue : b.d()) {
            if (b2 != null && b2.d()) {
                this.a(intValue, b2);
            }
            else {
                this.p(intValue, a, b2);
            }
        }
        final Iterator iterator2 = b.c().iterator();
        while (iterator2.hasNext()) {
            this.p((int)iterator2.next(), a, b.b());
        }
    }
    
    public void j(final WatchChange.c c) {
        final int b = c.b();
        final int a = c.a().a();
        final au1 n = this.n(b);
        if (n != null) {
            final q g = n.g();
            if (g.s()) {
                if (a == 0) {
                    final du g2 = du.g(g.n());
                    this.p(b, g2, MutableDocument.r(g2, qo1.b));
                }
                else {
                    boolean b2 = true;
                    if (a != 1) {
                        b2 = false;
                    }
                    g9.d(b2, "Single document existence filter with count: %d", a);
                }
            }
            else {
                final int g3 = this.g(b);
                if (g3 != a) {
                    final BloomFilter m = this.m(c);
                    BloomFilterApplicationStatus bloomFilterApplicationStatus;
                    if (m != null) {
                        bloomFilterApplicationStatus = this.b(m, c, g3);
                    }
                    else {
                        bloomFilterApplicationStatus = BloomFilterApplicationStatus.SKIPPED;
                    }
                    if (bloomFilterApplicationStatus != BloomFilterApplicationStatus.SUCCESS) {
                        this.r(b);
                        QueryPurpose queryPurpose;
                        if (bloomFilterApplicationStatus == BloomFilterApplicationStatus.FALSE_POSITIVE) {
                            queryPurpose = QueryPurpose.EXISTENCE_FILTER_MISMATCH_BLOOM;
                        }
                        else {
                            queryPurpose = QueryPurpose.EXISTENCE_FILTER_MISMATCH;
                        }
                        this.e.put(b, queryPurpose);
                    }
                    h.a().b(h.b.e(g3, c.a(), this.a.a(), m, bloomFilterApplicationStatus));
                }
            }
        }
    }
    
    public void k(final WatchChange.d d) {
        for (final int intValue : this.h(d)) {
            final du1 e = this.e(intValue);
            final int n = WatchChangeAggregator$a.a[d.b().ordinal()];
            boolean b = true;
            if (n != 1) {
                if (n != 2) {
                    if (n == 3) {
                        e.h();
                        if (!e.e()) {
                            this.q(intValue);
                        }
                        if (d.a() != null) {
                            b = false;
                        }
                        g9.d(b, "WatchChangeAggregator does not handle errored targets", new Object[0]);
                        continue;
                    }
                    if (n != 4) {
                        if (n != 5) {
                            throw g9.a("Unknown target watch change state: %s", d.b());
                        }
                        if (!this.l(intValue)) {
                            continue;
                        }
                        this.r(intValue);
                    }
                    else {
                        if (!this.l(intValue)) {
                            continue;
                        }
                        e.f();
                    }
                }
                else {
                    e.h();
                    if (!e.e()) {
                        e.b();
                    }
                }
            }
            else if (!this.l(intValue)) {
                continue;
            }
            e.k(d.c());
        }
    }
    
    public final boolean l(final int n) {
        return this.n(n) != null;
    }
    
    public final BloomFilter m(final WatchChange.c c) {
        final c b = c.a().b();
        if (b != null) {
            if (b.d0()) {
                final ByteString a0 = b.a0().a0();
                try {
                    final BloomFilter a2 = BloomFilter.a(a0, b.a0().c0(), b.c0());
                    if (a2.c() == 0) {
                        return null;
                    }
                    return a2;
                }
                catch (final BloomFilter.BloomFilterCreateException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Applying bloom filter failed: (");
                    sb.append(ex.getMessage());
                    sb.append("); ignoring the bloom filter and falling back to full re-query.");
                    Logger.d("WatchChangeAggregator", sb.toString(), new Object[0]);
                }
            }
        }
        return null;
    }
    
    public final au1 n(final int i) {
        final du1 du1 = this.b.get(i);
        au1 c;
        if (du1 != null && du1.e()) {
            c = null;
        }
        else {
            c = this.a.c(i);
        }
        return c;
    }
    
    public void o(final int n) {
        this.e(n).g();
    }
    
    public final void p(final int i, final du du, final MutableDocument mutableDocument) {
        if (!this.l(i)) {
            return;
        }
        final du1 e = this.e(i);
        if (this.s(i, du)) {
            e.a(du, DocumentViewChange.Type.REMOVED);
        }
        else {
            e.i(du);
        }
        this.d(du).add(i);
        if (mutableDocument != null) {
            this.c.put(du, mutableDocument);
        }
    }
    
    public void q(final int i) {
        this.b.remove(i);
    }
    
    public final void r(final int i) {
        g9.d(this.b.get(i) != null && !this.b.get(i).e(), "Should only reset active targets", new Object[0]);
        this.b.put(i, new du1());
        final Iterator iterator = this.a.b(i).iterator();
        while (iterator.hasNext()) {
            this.p(i, (du)iterator.next(), null);
        }
    }
    
    public final boolean s(final int n, final du du) {
        return this.a.b(n).contains(du);
    }
    
    public enum BloomFilterApplicationStatus
    {
        private static final BloomFilterApplicationStatus[] $VALUES;
        
        FALSE_POSITIVE, 
        SKIPPED, 
        SUCCESS;
    }
    
    public interface b
    {
        qp a();
        
        com.google.firebase.database.collection.c b(final int p0);
        
        au1 c(final int p0);
    }
}

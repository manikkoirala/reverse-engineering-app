// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.remote;

import android.util.Base64;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.MessageDigest;
import com.google.protobuf.ByteString;

public final class BloomFilter
{
    public final int a;
    public final ByteString b;
    public final int c;
    public final MessageDigest d;
    
    public BloomFilter(final ByteString b, final int n, final int c) {
        if (n < 0 || n >= 8) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid padding: ");
            sb.append(n);
            throw new IllegalArgumentException(sb.toString());
        }
        if (c < 0) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Invalid hash count: ");
            sb2.append(c);
            throw new IllegalArgumentException(sb2.toString());
        }
        if (b.size() > 0 && c == 0) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Invalid hash count: ");
            sb3.append(c);
            throw new IllegalArgumentException(sb3.toString());
        }
        if (b.size() == 0 && n != 0) {
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("Expected padding of 0 when bitmap length is 0, but got ");
            sb4.append(n);
            throw new IllegalArgumentException(sb4.toString());
        }
        this.b = b;
        this.c = c;
        this.a = b.size() * 8 - n;
        this.d = b();
    }
    
    public static BloomFilter a(final ByteString byteString, final int n, final int n2) {
        if (n < 0 || n >= 8) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid padding: ");
            sb.append(n);
            throw new BloomFilterCreateException(sb.toString());
        }
        if (n2 < 0) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Invalid hash count: ");
            sb2.append(n2);
            throw new BloomFilterCreateException(sb2.toString());
        }
        if (byteString.size() > 0 && n2 == 0) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Invalid hash count: ");
            sb3.append(n2);
            throw new BloomFilterCreateException(sb3.toString());
        }
        if (byteString.size() == 0 && n != 0) {
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("Expected padding of 0 when bitmap length is 0, but got ");
            sb4.append(n);
            throw new BloomFilterCreateException(sb4.toString());
        }
        return new BloomFilter(byteString, n, n2);
    }
    
    public static MessageDigest b() {
        try {
            return MessageDigest.getInstance("MD5");
        }
        catch (final NoSuchAlgorithmException cause) {
            throw new RuntimeException("Missing MD5 MessageDigest provider: ", cause);
        }
    }
    
    public static long e(final byte[] array, final int n) {
        long n2 = 0L;
        for (int i = 0; i < 8; ++i) {
            n2 |= ((long)array[n + i] & 0xFFL) << i * 8;
        }
        return n2;
    }
    
    public static long i(long n, long n2) {
        n -= ((n >>> 1) / n2 << 1) * n2;
        if (n < n2) {
            n2 = 0L;
        }
        return n - n2;
    }
    
    public int c() {
        return this.a;
    }
    
    public final int d(final long n, final long n2, final int n3) {
        return (int)i(n + n2 * n3, this.a);
    }
    
    public final boolean f(final int n) {
        final byte byte1 = this.b.byteAt(n / 8);
        boolean b = true;
        if ((1 << n % 8 & byte1) == 0x0) {
            b = false;
        }
        return b;
    }
    
    public final byte[] g(final String s) {
        return this.d.digest(s.getBytes(StandardCharsets.UTF_8));
    }
    
    public boolean h(final String s) {
        if (this.a == 0) {
            return false;
        }
        final byte[] g = this.g(s);
        if (g.length == 16) {
            final long e = e(g, 0);
            final long e2 = e(g, 8);
            for (int i = 0; i < this.c; ++i) {
                if (!this.f(this.d(e, e2, i))) {
                    return false;
                }
            }
            return true;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid md5 hash array length: ");
        sb.append(g.length);
        sb.append(" (expected 16)");
        throw new RuntimeException(sb.toString());
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("BloomFilter{hashCount=");
        sb.append(this.c);
        sb.append(", size=");
        sb.append(this.a);
        sb.append(", bitmap=\"");
        sb.append(Base64.encodeToString(this.b.toByteArray(), 2));
        sb.append("\"}");
        return sb.toString();
    }
    
    public static final class BloomFilterCreateException extends Exception
    {
        public BloomFilterCreateException(final String message) {
            super(message);
        }
    }
}

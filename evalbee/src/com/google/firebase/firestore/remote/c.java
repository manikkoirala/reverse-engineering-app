// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.remote;

public final class c extends b
{
    public final int a;
    public final int b;
    public final String c;
    public final String d;
    public final a e;
    
    public c(final int a, final int b, final String c, final String d, final a e) {
        this.a = a;
        this.b = b;
        if (c == null) {
            throw new NullPointerException("Null projectId");
        }
        this.c = c;
        if (d != null) {
            this.d = d;
            this.e = e;
            return;
        }
        throw new NullPointerException("Null databaseId");
    }
    
    @Override
    public a a() {
        return this.e;
    }
    
    @Override
    public String c() {
        return this.d;
    }
    
    @Override
    public int d() {
        return this.b;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof b) {
            final b b2 = (b)o;
            if (this.a == b2.f() && this.b == b2.d() && this.c.equals(b2.g()) && this.d.equals(b2.c())) {
                final a e = this.e;
                final a a = b2.a();
                if (e == null) {
                    if (a == null) {
                        return b;
                    }
                }
                else if (e.equals(a)) {
                    return b;
                }
            }
            b = false;
            return b;
        }
        return false;
    }
    
    @Override
    public int f() {
        return this.a;
    }
    
    @Override
    public String g() {
        return this.c;
    }
    
    @Override
    public int hashCode() {
        final int a = this.a;
        final int b = this.b;
        final int hashCode = this.c.hashCode();
        final int hashCode2 = this.d.hashCode();
        final a e = this.e;
        int hashCode3;
        if (e == null) {
            hashCode3 = 0;
        }
        else {
            hashCode3 = e.hashCode();
        }
        return ((((a ^ 0xF4243) * 1000003 ^ b) * 1000003 ^ hashCode) * 1000003 ^ hashCode2) * 1000003 ^ hashCode3;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ExistenceFilterMismatchInfo{localCacheCount=");
        sb.append(this.a);
        sb.append(", existenceFilterCount=");
        sb.append(this.b);
        sb.append(", projectId=");
        sb.append(this.c);
        sb.append(", databaseId=");
        sb.append(this.d);
        sb.append(", bloomFilter=");
        sb.append(this.e);
        sb.append("}");
        return sb.toString();
    }
}

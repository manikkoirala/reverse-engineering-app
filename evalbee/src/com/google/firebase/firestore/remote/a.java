// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.remote;

import android.content.Intent;
import android.net.Network;
import com.google.firebase.firestore.util.Logger;
import java.util.Iterator;
import android.net.NetworkInfo;
import android.content.IntentFilter;
import android.content.ComponentCallbacks;
import android.content.res.Configuration;
import android.content.ComponentCallbacks2;
import android.os.Bundle;
import android.app.Activity;
import android.app.Application$ActivityLifecycleCallbacks;
import java.util.concurrent.atomic.AtomicBoolean;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.net.ConnectivityManager$NetworkCallback;
import java.util.ArrayList;
import java.util.List;
import android.net.ConnectivityManager;
import android.content.Context;

public final class a implements ConnectivityMonitor
{
    public final Context a;
    public final ConnectivityManager b;
    public Runnable c;
    public final List d;
    
    public a(final Context a) {
        this.d = new ArrayList();
        g9.d(a != null, "Context must be non-null", new Object[0]);
        this.a = a;
        this.b = (ConnectivityManager)a.getSystemService("connectivity");
        this.f();
        this.g();
    }
    
    @Override
    public void a(final cl cl) {
        synchronized (this.d) {
            this.d.add(cl);
        }
    }
    
    public final void f() {
        final Application application = (Application)this.a.getApplicationContext();
        final AtomicBoolean atomicBoolean = new AtomicBoolean();
        application.registerActivityLifecycleCallbacks((Application$ActivityLifecycleCallbacks)new Application$ActivityLifecycleCallbacks(this, atomicBoolean) {
            public final AtomicBoolean a;
            public final a b;
            
            public void onActivityCreated(final Activity activity, final Bundle bundle) {
                if (this.a.compareAndSet(true, false)) {
                    this.b.l();
                }
            }
            
            public void onActivityDestroyed(final Activity activity) {
            }
            
            public void onActivityPaused(final Activity activity) {
            }
            
            public void onActivityResumed(final Activity activity) {
                if (this.a.compareAndSet(true, false)) {
                    this.b.l();
                }
            }
            
            public void onActivitySaveInstanceState(final Activity activity, final Bundle bundle) {
            }
            
            public void onActivityStarted(final Activity activity) {
                if (this.a.compareAndSet(true, false)) {
                    this.b.l();
                }
            }
            
            public void onActivityStopped(final Activity activity) {
            }
        });
        application.registerComponentCallbacks((ComponentCallbacks)new ComponentCallbacks2(this, atomicBoolean) {
            public final AtomicBoolean a;
            public final a b;
            
            public void onConfigurationChanged(final Configuration configuration) {
            }
            
            public void onLowMemory() {
            }
            
            public void onTrimMemory(final int n) {
                if (n == 20) {
                    this.a.set(true);
                }
            }
        });
    }
    
    public final void g() {
        Runnable c2;
        if (this.b != null) {
            final c c = new c(null);
            this.b.registerDefaultNetworkCallback((ConnectivityManager$NetworkCallback)c);
            c2 = new s4(this, c);
        }
        else {
            final d d = new d(null);
            this.a.registerReceiver((BroadcastReceiver)d, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
            c2 = new t4(this, d);
        }
        this.c = c2;
    }
    
    public final boolean h() {
        final NetworkInfo activeNetworkInfo = ((ConnectivityManager)this.a.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    
    public final void k(final boolean b) {
        synchronized (this.d) {
            for (final cl cl : this.d) {
                NetworkStatus networkStatus;
                if (b) {
                    networkStatus = NetworkStatus.REACHABLE;
                }
                else {
                    networkStatus = NetworkStatus.UNREACHABLE;
                }
                cl.accept(networkStatus);
            }
        }
    }
    
    public void l() {
        Logger.a("AndroidConnectivityMonitor", "App has entered the foreground.", new Object[0]);
        if (this.h()) {
            this.k(true);
        }
    }
    
    public class c extends ConnectivityManager$NetworkCallback
    {
        public final a a;
        
        public c(final a a) {
            this.a = a;
        }
        
        public void onAvailable(final Network network) {
            this.a.k(true);
        }
        
        public void onLost(final Network network) {
            this.a.k(false);
        }
    }
    
    public class d extends BroadcastReceiver
    {
        public boolean a;
        public final a b;
        
        public d(final a b) {
            this.b = b;
            this.a = false;
        }
        
        public void onReceive(final Context context, final Intent intent) {
            final boolean e = this.b.h();
            Label_0063: {
                a a;
                boolean b;
                if (this.b.h() && !this.a) {
                    a = this.b;
                    b = true;
                }
                else {
                    if (e || !this.a) {
                        break Label_0063;
                    }
                    a = this.b;
                    b = false;
                }
                a.k(b);
            }
            this.a = e;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.remote;

import io.grpc.Status$Code;
import com.google.firebase.firestore.FirebaseFirestoreException;
import javax.net.ssl.SSLHandshakeException;
import io.grpc.Status;
import android.content.Context;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import com.google.firebase.firestore.util.AsyncQueue;
import java.util.Set;

public class d
{
    public static final Set e;
    public final rp a;
    public final f b;
    public final AsyncQueue c;
    public final w30 d;
    
    static {
        e = new HashSet(Arrays.asList("date", "x-google-backends", "x-google-netmon-label", "x-google-service", "x-google-gfe-request-trace"));
    }
    
    public d(final rp a, final AsyncQueue c, final eo eo, final eo eo2, final Context context, final fc0 fc0) {
        this.a = a;
        this.c = c;
        this.b = new f(a.a());
        this.d = this.d(a, c, eo, eo2, context, fc0);
    }
    
    public static boolean e(final Status status) {
        status.getCode();
        final Throwable cause = status.getCause();
        if (cause instanceof SSLHandshakeException) {
            cause.getMessage().contains("no ciphers available");
        }
        return false;
    }
    
    public static boolean f(final FirebaseFirestoreException.Code obj) {
        switch (d$a.a[obj.ordinal()]) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unknown gRPC status code: ");
                sb.append(obj);
                throw new IllegalArgumentException(sb.toString());
            }
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17: {
                return true;
            }
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8: {
                return false;
            }
            case 1: {
                throw new IllegalArgumentException("Treated status OK as error");
            }
        }
    }
    
    public static boolean g(final Status status) {
        return f(FirebaseFirestoreException.Code.fromValue(status.getCode().value()));
    }
    
    public static boolean h(final Status status) {
        return g(status) && !status.getCode().equals(Status$Code.ABORTED);
    }
    
    public i a(final i.a a) {
        return new i(this.d, this.c, this.b, a);
    }
    
    public j b(final j.a a) {
        return new j(this.d, this.c, this.b, a);
    }
    
    public rp c() {
        return this.a;
    }
    
    public w30 d(final rp rp, final AsyncQueue asyncQueue, final eo eo, final eo eo2, final Context context, final fc0 fc0) {
        return new w30(asyncQueue, context, eo, eo2, rp, fc0);
    }
}

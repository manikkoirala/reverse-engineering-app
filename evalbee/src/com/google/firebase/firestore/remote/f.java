// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.remote;

import com.google.protobuf.GeneratedMessageLite;
import com.google.firestore.v1.TargetChange;
import com.google.firestore.v1.g;
import com.google.firestore.v1.i;
import com.google.firestore.v1.j;
import com.google.firebase.firestore.model.MutableDocument;
import com.google.firestore.v1.ListenResponse;
import com.google.firestore.v1.Value;
import com.google.firebase.firestore.core.c;
import com.google.firestore.v1.n;
import java.util.Collections;
import com.google.firebase.firestore.core.Query;
import java.util.Set;
import java.util.HashSet;
import java.util.ArrayList;
import io.grpc.Status;
import com.google.protobuf.j0;
import com.google.firestore.v1.d;
import com.google.protobuf.r;
import com.google.firestore.v1.Precondition;
import com.google.firebase.firestore.core.OrderBy;
import com.google.firestore.v1.Write;
import java.util.HashMap;
import java.util.Map;
import com.google.firebase.firestore.local.QueryPurpose;
import com.google.firebase.firestore.core.CompositeFilter;
import com.google.firestore.v1.a;
import com.google.firestore.v1.DocumentTransform;
import com.google.firestore.v1.StructuredQuery;
import com.google.firebase.firestore.core.FieldFilter;
import com.google.firestore.v1.Target;
import com.google.firebase.firestore.core.q;
import java.util.Iterator;
import com.google.firestore.v1.h;
import com.google.firestore.v1.e;
import java.util.List;
import java.util.Arrays;

public final class f
{
    public final qp a;
    public final String b;
    
    public f(final qp a) {
        this.a = a;
        this.b = V(a).d();
    }
    
    public static ke1 V(final qp qp) {
        return ke1.p(Arrays.asList("projects", qp.f(), "databases", qp.e()));
    }
    
    public static ke1 W(final ke1 ke1) {
        g9.d(ke1.l() > 4 && ke1.h(4).equals("documents"), "Tried to deserialize invalid key %s", ke1);
        return (ke1)ke1.m(5);
    }
    
    public static boolean Y(final ke1 ke1) {
        final int l = ke1.l();
        boolean b2;
        final boolean b = b2 = false;
        if (l >= 4) {
            b2 = b;
            if (ke1.h(0).equals("projects")) {
                b2 = b;
                if (ke1.h(2).equals("databases")) {
                    b2 = true;
                }
            }
        }
        return b2;
    }
    
    public e A(final du du, final a11 a11) {
        final e.b k0 = e.k0();
        k0.B(this.I(du));
        k0.A(a11.l());
        return (e)((GeneratedMessageLite.a)k0).p();
    }
    
    public final h B(final q00 q00) {
        final h.b g0 = h.g0();
        final Iterator iterator = q00.c().iterator();
        while (iterator.hasNext()) {
            g0.A(((s00)iterator.next()).d());
        }
        return (h)((GeneratedMessageLite.a)g0).p();
    }
    
    public Target.c C(final q q) {
        final Target.c.a g0 = Target.c.g0();
        g0.A(this.O(q.n()));
        return (Target.c)((GeneratedMessageLite.a)g0).p();
    }
    
    public final StructuredQuery.FieldFilter.Operator D(final FieldFilter.Operator operator) {
        switch (f$a.i[operator.ordinal()]) {
            default: {
                throw g9.a("Unknown operator %d", operator);
            }
            case 10: {
                return StructuredQuery.FieldFilter.Operator.NOT_IN;
            }
            case 9: {
                return StructuredQuery.FieldFilter.Operator.ARRAY_CONTAINS_ANY;
            }
            case 8: {
                return StructuredQuery.FieldFilter.Operator.IN;
            }
            case 7: {
                return StructuredQuery.FieldFilter.Operator.ARRAY_CONTAINS;
            }
            case 6: {
                return StructuredQuery.FieldFilter.Operator.GREATER_THAN_OR_EQUAL;
            }
            case 5: {
                return StructuredQuery.FieldFilter.Operator.GREATER_THAN;
            }
            case 4: {
                return StructuredQuery.FieldFilter.Operator.NOT_EQUAL;
            }
            case 3: {
                return StructuredQuery.FieldFilter.Operator.EQUAL;
            }
            case 2: {
                return StructuredQuery.FieldFilter.Operator.LESS_THAN_OR_EQUAL;
            }
            case 1: {
                return StructuredQuery.FieldFilter.Operator.LESS_THAN;
            }
        }
    }
    
    public final StructuredQuery.d E(final s00 s00) {
        return (StructuredQuery.d)((GeneratedMessageLite.a)StructuredQuery.d.d0().A(s00.d())).p();
    }
    
    public final DocumentTransform.FieldTransform F(final u00 u00) {
        final oy1 b = u00.b();
        DocumentTransform.FieldTransform.a a;
        if (b instanceof pl1) {
            a = DocumentTransform.FieldTransform.l0().B(u00.a().d()).E(DocumentTransform.FieldTransform.ServerValue.REQUEST_TIME);
        }
        else if (b instanceof u8.b) {
            a = DocumentTransform.FieldTransform.l0().B(u00.a().d()).A(com.google.firestore.v1.a.j0().A(((u8.b)b).f()));
        }
        else if (b instanceof u8.a) {
            a = DocumentTransform.FieldTransform.l0().B(u00.a().d()).D(com.google.firestore.v1.a.j0().A(((u8.a)b).f()));
        }
        else {
            if (!(b instanceof q01)) {
                throw g9.a("Unknown transform: %s", b);
            }
            a = DocumentTransform.FieldTransform.l0().B(u00.a().d()).C(((q01)b).d());
        }
        return (DocumentTransform.FieldTransform)((GeneratedMessageLite.a)a).p();
    }
    
    public StructuredQuery.Filter G(final i10 i10) {
        if (i10 instanceof FieldFilter) {
            return this.T((FieldFilter)i10);
        }
        if (i10 instanceof CompositeFilter) {
            return this.y((CompositeFilter)i10);
        }
        throw g9.a("Unrecognized filter type %s", i10.toString());
    }
    
    public final StructuredQuery.Filter H(final List list) {
        return this.G(new CompositeFilter(list, CompositeFilter.Operator.AND));
    }
    
    public String I(final du du) {
        return this.Q(this.a, du.m());
    }
    
    public final String J(final QueryPurpose queryPurpose) {
        final int n = f$a.d[queryPurpose.ordinal()];
        if (n == 1) {
            return null;
        }
        if (n == 2) {
            return "existence-filter-mismatch";
        }
        if (n == 3) {
            return "existence-filter-mismatch-bloom";
        }
        if (n == 4) {
            return "limbo-document";
        }
        throw g9.a("Unrecognized query purpose: %s", queryPurpose);
    }
    
    public Map K(final au1 au1) {
        final String j = this.J(au1.c());
        if (j == null) {
            return null;
        }
        final HashMap hashMap = new HashMap(1);
        hashMap.put("goog-listen-tags", j);
        return hashMap;
    }
    
    public Write L(final wx0 wx0) {
        final Write.b u0 = Write.u0();
        if (wx0 instanceof qm1) {
            u0.D(this.A(wx0.g(), ((qm1)wx0).o()));
        }
        else if (wx0 instanceof e31) {
            u0.D(this.A(wx0.g(), ((e31)wx0).q()));
            u0.E(this.B(wx0.e()));
        }
        else if (wx0 instanceof hs) {
            u0.C(this.I(wx0.g()));
        }
        else {
            if (!(wx0 instanceof h32)) {
                throw g9.a("unknown mutation type %s", wx0.getClass());
            }
            u0.F(this.I(wx0.g()));
        }
        final Iterator iterator = wx0.f().iterator();
        while (iterator.hasNext()) {
            u0.A(this.F((u00)iterator.next()));
        }
        if (!wx0.h().d()) {
            u0.B(this.N(wx0.h()));
        }
        return (Write)((GeneratedMessageLite.a)u0).p();
    }
    
    public final StructuredQuery.e M(final OrderBy orderBy) {
        final StructuredQuery.e.a e0 = StructuredQuery.e.e0();
        StructuredQuery.Direction direction;
        if (orderBy.b().equals(OrderBy.Direction.ASCENDING)) {
            direction = StructuredQuery.Direction.ASCENDING;
        }
        else {
            direction = StructuredQuery.Direction.DESCENDING;
        }
        e0.A(direction);
        e0.B(this.E(orderBy.c()));
        return (StructuredQuery.e)((GeneratedMessageLite.a)e0).p();
    }
    
    public final Precondition N(final h71 h71) {
        g9.d(h71.d() ^ true, "Can't serialize an empty precondition", new Object[0]);
        final Precondition.b g0 = Precondition.g0();
        Precondition.b b;
        if (h71.c() != null) {
            b = g0.B(this.U(h71.c()));
        }
        else {
            if (h71.b() == null) {
                throw g9.a("Unknown Precondition", new Object[0]);
            }
            b = g0.A(h71.b());
        }
        return (Precondition)((GeneratedMessageLite.a)b).p();
    }
    
    public final String O(final ke1 ke1) {
        return this.Q(this.a, ke1);
    }
    
    public Target.QueryTarget P(final q q) {
        final Target.QueryTarget.a f0 = Target.QueryTarget.f0();
        final StructuredQuery.b x0 = StructuredQuery.x0();
        final ke1 n = q.n();
        if (q.d() != null) {
            g9.d(n.l() % 2 == 0, "Collection Group queries should be within a document path or root.", new Object[0]);
            f0.A(this.O(n));
            final StructuredQuery.c.a e0 = StructuredQuery.c.e0();
            e0.B(q.d());
            e0.A(true);
            x0.A(e0);
        }
        else {
            g9.d(n.l() % 2 != 0, "Document queries with filters are not supported.", new Object[0]);
            f0.A(this.O((ke1)n.n()));
            final StructuredQuery.c.a e2 = StructuredQuery.c.e0();
            e2.B(n.g());
            x0.A(e2);
        }
        if (q.h().size() > 0) {
            x0.F(this.H(q.h()));
        }
        final Iterator iterator = q.m().iterator();
        while (iterator.hasNext()) {
            x0.B(this.M((OrderBy)iterator.next()));
        }
        if (q.r()) {
            x0.D(r.d0().A((int)q.j()));
        }
        if (q.p() != null) {
            final d.b g0 = d.g0();
            g0.A(q.p().b());
            g0.B(q.p().c());
            x0.E(g0);
        }
        if (q.f() != null) {
            final d.b g2 = d.g0();
            g2.A(q.f().b());
            g2.B(q.f().c() ^ true);
            x0.C(g2);
        }
        f0.B(x0);
        return (Target.QueryTarget)((GeneratedMessageLite.a)f0).p();
    }
    
    public final String Q(final qp qp, final ke1 ke1) {
        return ((ke1)V(qp).c("documents").a(ke1)).d();
    }
    
    public Target R(final au1 au1) {
        final Target.b g0 = Target.g0();
        final q g2 = au1.g();
        if (g2.s()) {
            g0.A(this.C(g2));
        }
        else {
            g0.C(this.P(g2));
        }
        g0.F(au1.h());
        if (au1.d().isEmpty() && au1.f().a(qo1.b) > 0) {
            g0.D(this.S(au1.f().c()));
        }
        else {
            g0.E(au1.d());
        }
        if (au1.a() != null && (!au1.d().isEmpty() || au1.f().a(qo1.b) > 0)) {
            g0.B(r.d0().A(au1.a()));
        }
        return (Target)((GeneratedMessageLite.a)g0).p();
    }
    
    public j0 S(final pw1 pw1) {
        final j0.b f0 = j0.f0();
        f0.B(pw1.e());
        f0.A(pw1.d());
        return (j0)((GeneratedMessageLite.a)f0).p();
    }
    
    public StructuredQuery.Filter T(final FieldFilter fieldFilter) {
        final FieldFilter.Operator g = fieldFilter.g();
        final FieldFilter.Operator equal = FieldFilter.Operator.EQUAL;
        Label_0124: {
            if (g != equal && fieldFilter.g() != FieldFilter.Operator.NOT_EQUAL) {
                break Label_0124;
            }
            final StructuredQuery.UnaryFilter.a f0 = StructuredQuery.UnaryFilter.f0();
            f0.A(this.E(fieldFilter.f()));
            StructuredQuery.UnaryFilter.Operator operator;
            if (a32.y(fieldFilter.h())) {
                if (fieldFilter.g() == equal) {
                    operator = StructuredQuery.UnaryFilter.Operator.IS_NAN;
                }
                else {
                    operator = StructuredQuery.UnaryFilter.Operator.IS_NOT_NAN;
                }
            }
            else {
                if (!a32.z(fieldFilter.h())) {
                    break Label_0124;
                }
                if (fieldFilter.g() == equal) {
                    operator = StructuredQuery.UnaryFilter.Operator.IS_NULL;
                }
                else {
                    operator = StructuredQuery.UnaryFilter.Operator.IS_NOT_NULL;
                }
            }
            f0.B(operator);
            final StructuredQuery.Filter.a a = StructuredQuery.Filter.i0().C(f0);
            return (StructuredQuery.Filter)((GeneratedMessageLite.a)a).p();
        }
        final StructuredQuery.FieldFilter.a h0 = StructuredQuery.FieldFilter.h0();
        h0.A(this.E(fieldFilter.f()));
        h0.B(this.D(fieldFilter.g()));
        h0.C(fieldFilter.h());
        final StructuredQuery.Filter.a a = StructuredQuery.Filter.i0().B(h0);
        return (StructuredQuery.Filter)((GeneratedMessageLite.a)a).p();
    }
    
    public j0 U(final qo1 qo1) {
        return this.S(qo1.c());
    }
    
    public final Status X(final bq1 bq1) {
        return Status.fromCodeValue(bq1.a0()).withDescription(bq1.c0());
    }
    
    public String a() {
        return this.b;
    }
    
    public CompositeFilter b(final StructuredQuery.CompositeFilter compositeFilter) {
        final ArrayList list = new ArrayList();
        final Iterator iterator = compositeFilter.f0().iterator();
        while (iterator.hasNext()) {
            list.add(this.i((StructuredQuery.Filter)iterator.next()));
        }
        return new CompositeFilter(list, this.c(compositeFilter.g0()));
    }
    
    public CompositeFilter.Operator c(final StructuredQuery.CompositeFilter.Operator operator) {
        final int n = f$a.f[operator.ordinal()];
        if (n == 1) {
            return CompositeFilter.Operator.AND;
        }
        if (n == 2) {
            return CompositeFilter.Operator.OR;
        }
        throw g9.a("Only AND and OR composite filter types are supported.", new Object[0]);
    }
    
    public final q00 d(final h h) {
        final int f0 = h.f0();
        final HashSet set = new HashSet(f0);
        for (int i = 0; i < f0; ++i) {
            set.add((Object)s00.q(h.e0(i)));
        }
        return q00.b(set);
    }
    
    public q e(final Target.c c) {
        final int f0 = c.f0();
        boolean b = true;
        if (f0 != 1) {
            b = false;
        }
        g9.d(b, "DocumentsTarget contained other than 1 document %d", f0);
        return Query.b(this.p(c.e0(0))).x();
    }
    
    public FieldFilter f(final StructuredQuery.FieldFilter fieldFilter) {
        return FieldFilter.e(s00.q(fieldFilter.e0().c0()), this.g(fieldFilter.f0()), fieldFilter.g0());
    }
    
    public final FieldFilter.Operator g(final StructuredQuery.FieldFilter.Operator operator) {
        switch (f$a.j[operator.ordinal()]) {
            default: {
                throw g9.a("Unhandled FieldFilter.operator %d", operator);
            }
            case 10: {
                return FieldFilter.Operator.NOT_IN;
            }
            case 9: {
                return FieldFilter.Operator.ARRAY_CONTAINS_ANY;
            }
            case 8: {
                return FieldFilter.Operator.IN;
            }
            case 7: {
                return FieldFilter.Operator.ARRAY_CONTAINS;
            }
            case 6: {
                return FieldFilter.Operator.GREATER_THAN;
            }
            case 5: {
                return FieldFilter.Operator.GREATER_THAN_OR_EQUAL;
            }
            case 4: {
                return FieldFilter.Operator.NOT_EQUAL;
            }
            case 3: {
                return FieldFilter.Operator.EQUAL;
            }
            case 2: {
                return FieldFilter.Operator.LESS_THAN_OR_EQUAL;
            }
            case 1: {
                return FieldFilter.Operator.LESS_THAN;
            }
        }
    }
    
    public final u00 h(final DocumentTransform.FieldTransform fieldTransform) {
        final int n = f$a.c[fieldTransform.k0().ordinal()];
        boolean b = true;
        if (n == 1) {
            if (fieldTransform.j0() != DocumentTransform.FieldTransform.ServerValue.REQUEST_TIME) {
                b = false;
            }
            g9.d(b, "Unknown transform setToServerValue: %s", fieldTransform.j0());
            return new u00(s00.q(fieldTransform.g0()), pl1.d());
        }
        if (n == 2) {
            return new u00(s00.q(fieldTransform.g0()), new u8.b(fieldTransform.f0().h()));
        }
        if (n == 3) {
            return new u00(s00.q(fieldTransform.g0()), new u8.a(fieldTransform.i0().h()));
        }
        if (n == 4) {
            return new u00(s00.q(fieldTransform.g0()), new q01(fieldTransform.h0()));
        }
        throw g9.a("Unknown FieldTransform proto: %s", fieldTransform);
    }
    
    public i10 i(final StructuredQuery.Filter filter) {
        final int n = f$a.g[filter.g0().ordinal()];
        if (n == 1) {
            return this.b(filter.d0());
        }
        if (n == 2) {
            return this.f(filter.f0());
        }
        if (n == 3) {
            return this.u(filter.h0());
        }
        throw g9.a("Unrecognized Filter.filterType %d", filter.g0());
    }
    
    public final List j(final StructuredQuery.Filter filter) {
        final i10 i = this.i(filter);
        if (i instanceof CompositeFilter) {
            final CompositeFilter compositeFilter = (CompositeFilter)i;
            if (compositeFilter.i()) {
                return compositeFilter.b();
            }
        }
        return Collections.singletonList(i);
    }
    
    public du k(final String s) {
        final ke1 s2 = this.s(s);
        g9.d(s2.h(1).equals(this.a.f()), "Tried to deserialize key from different project.", new Object[0]);
        g9.d(s2.h(3).equals(this.a.e()), "Tried to deserialize key from different database.", new Object[0]);
        return du.g(W(s2));
    }
    
    public wx0 l(final Write write) {
        h71 h71;
        if (write.q0()) {
            h71 = this.o(write.i0());
        }
        else {
            h71 = h71.c;
        }
        final ArrayList list = new ArrayList();
        final Iterator iterator = write.o0().iterator();
        while (iterator.hasNext()) {
            list.add(this.h((DocumentTransform.FieldTransform)iterator.next()));
        }
        final int n = f$a.a[write.k0().ordinal()];
        if (n != 1) {
            if (n == 2) {
                return new hs(this.k(write.j0()), h71);
            }
            if (n == 3) {
                return new h32(this.k(write.p0()), h71);
            }
            throw g9.a("Unknown mutation operation: %d", write.k0());
        }
        else {
            if (write.t0()) {
                return new e31(this.k(write.m0().g0()), a11.i(write.m0().e0()), this.d(write.n0()), h71, list);
            }
            return new qm1(this.k(write.m0().g0()), a11.i(write.m0().e0()), h71, list);
        }
    }
    
    public ay0 m(final n n, qo1 qo1) {
        final qo1 v = this.v(n.c0());
        if (!qo1.b.equals(v)) {
            qo1 = v;
        }
        final int b0 = n.b0();
        final ArrayList list = new ArrayList(b0);
        for (int i = 0; i < b0; ++i) {
            list.add((Object)n.a0(i));
        }
        return new ay0(qo1, list);
    }
    
    public final OrderBy n(final StructuredQuery.e e) {
        final s00 q = s00.q(e.d0().c0());
        final int n = f$a.k[e.c0().ordinal()];
        OrderBy.Direction direction;
        if (n != 1) {
            if (n != 2) {
                throw g9.a("Unrecognized direction %d", e.c0());
            }
            direction = OrderBy.Direction.DESCENDING;
        }
        else {
            direction = OrderBy.Direction.ASCENDING;
        }
        return OrderBy.d(direction, q);
    }
    
    public final h71 o(final Precondition precondition) {
        final int n = f$a.b[precondition.c0().ordinal()];
        if (n == 1) {
            return h71.f(this.v(precondition.f0()));
        }
        if (n == 2) {
            return h71.a(precondition.e0());
        }
        if (n == 3) {
            return h71.c;
        }
        throw g9.a("Unknown precondition", new Object[0]);
    }
    
    public final ke1 p(final String s) {
        final ke1 s2 = this.s(s);
        if (s2.l() == 4) {
            return ke1.b;
        }
        return W(s2);
    }
    
    public q q(final Target.QueryTarget queryTarget) {
        return this.r(queryTarget.d0(), queryTarget.e0());
    }
    
    public q r(final String s, final StructuredQuery structuredQuery) {
        final ke1 p2 = this.p(s);
        final int n0 = structuredQuery.n0();
        int i = 0;
        c c = null;
        ke1 ke1 = p2;
        String s2 = null;
        Label_0100: {
            if (n0 > 0) {
                g9.d(n0 == 1, "StructuredQuery.from with more than one collection is not supported.", new Object[0]);
                final StructuredQuery.c m0 = structuredQuery.m0(0);
                final boolean c2 = m0.c0();
                final String d0 = m0.d0();
                if (c2) {
                    ke1 = p2;
                    s2 = d0;
                    break Label_0100;
                }
                ke1 = (ke1)p2.c(d0);
            }
            s2 = null;
        }
        List<Object> list;
        if (structuredQuery.w0()) {
            list = this.j(structuredQuery.s0());
        }
        else {
            list = Collections.emptyList();
        }
        final int q0 = structuredQuery.q0();
        List<OrderBy> emptyList;
        if (q0 > 0) {
            emptyList = new ArrayList<OrderBy>(q0);
            while (i < q0) {
                emptyList.add(this.n(structuredQuery.p0(i)));
                ++i;
            }
        }
        else {
            emptyList = Collections.emptyList();
        }
        long n2;
        if (structuredQuery.u0()) {
            n2 = structuredQuery.o0().c0();
        }
        else {
            n2 = -1L;
        }
        c c3;
        if (structuredQuery.v0()) {
            c3 = new c(structuredQuery.r0().h(), structuredQuery.r0().e0());
        }
        else {
            c3 = null;
        }
        if (structuredQuery.t0()) {
            c = new c(structuredQuery.l0().h(), structuredQuery.l0().e0() ^ true);
        }
        return new q(ke1, s2, list, emptyList, n2, c3, c);
    }
    
    public final ke1 s(final String s) {
        final ke1 q = ke1.q(s);
        g9.d(Y(q), "Tried to deserialize invalid key %s", q);
        return q;
    }
    
    public pw1 t(final j0 j0) {
        return new pw1(j0.e0(), j0.d0());
    }
    
    public final i10 u(final StructuredQuery.UnaryFilter unaryFilter) {
        final s00 q = s00.q(unaryFilter.d0().c0());
        final int n = f$a.h[unaryFilter.e0().ordinal()];
        FieldFilter.Operator operator = null;
        Label_0082: {
            if (n == 1) {
                operator = FieldFilter.Operator.EQUAL;
                break Label_0082;
            }
            if (n != 2) {
                if (n == 3) {
                    operator = FieldFilter.Operator.NOT_EQUAL;
                    break Label_0082;
                }
                if (n != 4) {
                    throw g9.a("Unrecognized UnaryFilter.operator %d", unaryFilter.e0());
                }
                operator = FieldFilter.Operator.NOT_EQUAL;
            }
            else {
                operator = FieldFilter.Operator.EQUAL;
            }
            final Value value = a32.b;
            return FieldFilter.e(q, operator, value);
        }
        final Value value = a32.a;
        return FieldFilter.e(q, operator, value);
    }
    
    public qo1 v(final j0 j0) {
        if (j0.e0() == 0L && j0.d0() == 0) {
            return qo1.b;
        }
        return new qo1(this.t(j0));
    }
    
    public qo1 w(final ListenResponse listenResponse) {
        if (listenResponse.f0() != ListenResponse.ResponseTypeCase.TARGET_CHANGE) {
            return qo1.b;
        }
        if (listenResponse.g0().f0() != 0) {
            return qo1.b;
        }
        return this.v(listenResponse.g0().c0());
    }
    
    public WatchChange x(final ListenResponse listenResponse) {
        final int n = f$a.m[listenResponse.f0().ordinal()];
        Status x = null;
        WatchChange watchChange;
        if (n != 1) {
            if (n != 2) {
                if (n != 3) {
                    if (n != 4) {
                        if (n != 5) {
                            throw new IllegalArgumentException("Unknown change type set");
                        }
                        final j e0 = listenResponse.e0();
                        watchChange = new WatchChange.c(e0.c0(), new hz(e0.a0(), e0.d0()));
                    }
                    else {
                        final i d0 = listenResponse.d0();
                        watchChange = new WatchChange.b(Collections.emptyList(), d0.c0(), this.k(d0.b0()), null);
                    }
                }
                else {
                    final g c0 = listenResponse.c0();
                    final List d2 = c0.d0();
                    final MutableDocument r = MutableDocument.r(this.k(c0.b0()), this.v(c0.c0()));
                    watchChange = new WatchChange.b(Collections.emptyList(), d2, r.getKey(), r);
                }
            }
            else {
                final com.google.firestore.v1.f b0 = listenResponse.b0();
                final List d3 = b0.d0();
                final List c2 = b0.c0();
                final du k = this.k(b0.b0().g0());
                final qo1 v = this.v(b0.b0().h0());
                g9.d(v.equals(qo1.b) ^ true, "Got a document change without an update time", new Object[0]);
                final MutableDocument p = MutableDocument.p(k, v, a11.i(b0.b0().e0()));
                watchChange = new WatchChange.b(d3, c2, p.getKey(), p);
            }
        }
        else {
            final TargetChange g0 = listenResponse.g0();
            final int n2 = f$a.l[g0.e0().ordinal()];
            WatchChange.WatchTargetChangeType watchTargetChangeType;
            if (n2 != 1) {
                if (n2 != 2) {
                    if (n2 != 3) {
                        if (n2 != 4) {
                            if (n2 != 5) {
                                throw new IllegalArgumentException("Unknown target change type");
                            }
                            watchTargetChangeType = WatchChange.WatchTargetChangeType.Reset;
                        }
                        else {
                            watchTargetChangeType = WatchChange.WatchTargetChangeType.Current;
                        }
                    }
                    else {
                        watchTargetChangeType = WatchChange.WatchTargetChangeType.Removed;
                        x = this.X(g0.a0());
                    }
                }
                else {
                    watchTargetChangeType = WatchChange.WatchTargetChangeType.Added;
                }
            }
            else {
                watchTargetChangeType = WatchChange.WatchTargetChangeType.NoChange;
            }
            watchChange = new WatchChange.d(watchTargetChangeType, g0.g0(), g0.d0(), x);
        }
        return watchChange;
    }
    
    public StructuredQuery.Filter y(final CompositeFilter compositeFilter) {
        final ArrayList list = new ArrayList(compositeFilter.b().size());
        final Iterator iterator = compositeFilter.b().iterator();
        while (iterator.hasNext()) {
            list.add(this.G((i10)iterator.next()));
        }
        Object o;
        if (list.size() == 1) {
            o = list.get(0);
        }
        else {
            final StructuredQuery.CompositeFilter.a h0 = StructuredQuery.CompositeFilter.h0();
            h0.B(this.z(compositeFilter.e()));
            h0.A(list);
            o = ((GeneratedMessageLite.a)StructuredQuery.Filter.i0().A(h0)).p();
        }
        return (StructuredQuery.Filter)o;
    }
    
    public StructuredQuery.CompositeFilter.Operator z(final CompositeFilter.Operator operator) {
        final int n = f$a.e[operator.ordinal()];
        if (n == 1) {
            return StructuredQuery.CompositeFilter.Operator.AND;
        }
        if (n == 2) {
            return StructuredQuery.CompositeFilter.Operator.OR;
        }
        throw g9.a("Unrecognized composite filter type.", new Object[0]);
    }
}

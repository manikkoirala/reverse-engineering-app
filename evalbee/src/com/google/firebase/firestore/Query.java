// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore;

import android.app.Activity;
import java.util.concurrent.Executor;
import com.google.firebase.firestore.core.f;
import com.google.firebase.firestore.core.ViewSnapshot;

public abstract class Query
{
    public final com.google.firebase.firestore.core.Query a;
    public final FirebaseFirestore b;
    
    public Query(final com.google.firebase.firestore.core.Query query, final FirebaseFirestore firebaseFirestore) {
        this.a = (com.google.firebase.firestore.core.Query)k71.b(query);
        this.b = (FirebaseFirestore)k71.b(firebaseFirestore);
    }
    
    public static f.a d(final MetadataChanges metadataChanges) {
        final f.a a = new f.a();
        final MetadataChanges include = MetadataChanges.INCLUDE;
        final boolean b = true;
        a.a = (metadataChanges == include);
        a.b = (metadataChanges == include && b);
        a.c = false;
        return a;
    }
    
    public mk0 b(final Executor executor, final MetadataChanges metadataChanges, final rx rx) {
        k71.c(executor, "Provided executor must not be null.");
        k71.c(metadataChanges, "Provided MetadataChanges value must not be null.");
        k71.c(rx, "Provided EventListener must not be null.");
        return this.c(executor, d(metadataChanges), null, rx);
    }
    
    public final mk0 c(final Executor executor, final f.a a, final Activity activity, final rx rx) {
        this.f();
        final j9 j9 = new j9(executor, new fa1(this, rx));
        return w2.c(activity, new nk0(this.b.c(), this.b.c().v(this.a, a, j9), j9));
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof Query)) {
            return false;
        }
        final Query query = (Query)o;
        if (!this.a.equals(query.a) || !this.b.equals(query.b)) {
            b = false;
        }
        return b;
    }
    
    public final void f() {
        if (this.a.j().equals(com.google.firebase.firestore.core.Query.LimitType.LIMIT_TO_LAST) && this.a.f().isEmpty()) {
            throw new IllegalStateException("limitToLast() queries require specifying at least one orderBy() clause");
        }
    }
    
    @Override
    public int hashCode() {
        return this.a.hashCode() * 31 + this.b.hashCode();
    }
    
    public enum Direction
    {
        private static final Direction[] $VALUES;
        
        ASCENDING, 
        DESCENDING;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore;

import android.util.SparseArray;
import com.google.firebase.FirebaseException;

public class FirebaseFirestoreException extends FirebaseException
{
    private final Code code;
    
    public FirebaseFirestoreException(final String s, final Code code) {
        super(s);
        k71.c(s, "Provided message must not be null.");
        g9.d(code != Code.OK, "A FirebaseFirestoreException should never be thrown for OK", new Object[0]);
        this.code = (Code)k71.c(code, "Provided code must not be null.");
    }
    
    public FirebaseFirestoreException(final String s, final Code code, final Throwable t) {
        super(s, t);
        k71.c(s, "Provided message must not be null.");
        g9.d(code != Code.OK, "A FirebaseFirestoreException should never be thrown for OK", new Object[0]);
        this.code = (Code)k71.c(code, "Provided code must not be null.");
    }
    
    public Code getCode() {
        return this.code;
    }
    
    public enum Code
    {
        private static final Code[] $VALUES;
        
        ABORTED(10), 
        ALREADY_EXISTS(6), 
        CANCELLED(1), 
        DATA_LOSS(15), 
        DEADLINE_EXCEEDED(4), 
        FAILED_PRECONDITION(9), 
        INTERNAL(13), 
        INVALID_ARGUMENT(3), 
        NOT_FOUND(5), 
        OK(0), 
        OUT_OF_RANGE(11), 
        PERMISSION_DENIED(7), 
        RESOURCE_EXHAUSTED(8);
        
        private static final SparseArray<Code> STATUS_LIST;
        
        UNAUTHENTICATED(16), 
        UNAVAILABLE(14), 
        UNIMPLEMENTED(12), 
        UNKNOWN(2);
        
        private final int value;
        
        static {
            STATUS_LIST = buildStatusList();
        }
        
        private Code(final int value) {
            this.value = value;
        }
        
        private static SparseArray<Code> buildStatusList() {
            final SparseArray sparseArray = new SparseArray();
            for (final Code code : values()) {
                final Code obj = (Code)sparseArray.get(code.value());
                if (obj != null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Code value duplication between ");
                    sb.append(obj);
                    sb.append("&");
                    sb.append(code.name());
                    throw new IllegalStateException(sb.toString());
                }
                sparseArray.put(code.value(), (Object)code);
            }
            return (SparseArray<Code>)sparseArray;
        }
        
        public static Code fromValue(final int n) {
            return (Code)Code.STATUS_LIST.get(n, (Object)Code.UNKNOWN);
        }
        
        public int value() {
            return this.value;
        }
    }
}

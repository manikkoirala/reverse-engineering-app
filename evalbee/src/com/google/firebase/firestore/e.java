// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore;

import java.util.HashMap;
import android.content.Context;
import java.util.Map;

public class e implements y10, a
{
    public final Map a;
    public final r10 b;
    public final Context c;
    public final jr d;
    public final jr e;
    public final fc0 f;
    
    public e(final Context c, final r10 b, final jr d, final jr e, final fc0 f) {
        this.a = new HashMap();
        this.c = c;
        this.b = b;
        this.d = d;
        this.e = e;
        this.f = f;
        b.h(this);
    }
    
    public FirebaseFirestore a(final String s) {
        synchronized (this) {
            FirebaseFirestore i;
            if ((i = this.a.get(s)) == null) {
                i = FirebaseFirestore.i(this.c, this.b, this.d, this.e, s, (FirebaseFirestore.a)this, this.f);
                this.a.put(s, i);
            }
            return i;
        }
    }
}

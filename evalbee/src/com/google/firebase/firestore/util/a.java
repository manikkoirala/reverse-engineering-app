// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.util;

import java.util.Date;

public class a
{
    public final AsyncQueue a;
    public final AsyncQueue.TimerId b;
    public final long c;
    public final double d;
    public final long e;
    public long f;
    public long g;
    public long h;
    public AsyncQueue.b i;
    
    public a(final AsyncQueue a, final AsyncQueue.TimerId b, final long c, final double d, final long n) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = n;
        this.f = n;
        this.h = new Date().getTime();
        this.f();
    }
    
    public void b(final Runnable runnable) {
        this.c();
        final long l = this.g + this.d();
        final long max = Math.max(0L, new Date().getTime() - this.h);
        final long max2 = Math.max(0L, l - max);
        if (this.g > 0L) {
            Logger.a(this.getClass().getSimpleName(), "Backing off for %d ms (base delay: %d ms, delay with jitter: %d ms, last attempt: %d ms ago)", max2, this.g, l, max);
        }
        this.i = this.a.h(this.b, max2, new lz(this, runnable));
        final long g = (long)(this.g * this.d);
        this.g = g;
        long g2 = this.c;
        Label_0183: {
            if (g >= g2) {
                g2 = this.f;
                if (g <= g2) {
                    break Label_0183;
                }
            }
            this.g = g2;
        }
        this.f = this.e;
    }
    
    public void c() {
        final AsyncQueue.b i = this.i;
        if (i != null) {
            i.c();
            this.i = null;
        }
    }
    
    public final long d() {
        return (long)((Math.random() - 0.5) * this.g);
    }
    
    public void f() {
        this.g = 0L;
    }
    
    public void g() {
        this.g = this.f;
    }
    
    public void h(final long f) {
        this.f = f;
    }
}

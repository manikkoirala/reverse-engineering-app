// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.util;

import android.util.Log;

public abstract class Logger
{
    public static Level a;
    
    static {
        Logger.a = Level.WARN;
    }
    
    public static void a(final String s, final String s2, final Object... array) {
        b(Level.DEBUG, s, s2, array);
    }
    
    public static void b(final Level level, String string, final String format, final Object... args) {
        if (level.ordinal() >= Logger.a.ordinal()) {
            final StringBuilder sb = new StringBuilder();
            sb.append(String.format("(%s) [%s]: ", "24.10.0", string));
            sb.append(String.format(format, args));
            string = sb.toString();
            final int n = Logger$a.a[level.ordinal()];
            if (n != 1) {
                if (n == 2) {
                    Log.w("Firestore", string);
                    return;
                }
                if (n == 3) {
                    throw new IllegalStateException("Trying to log something on level NONE");
                }
            }
            else {
                Log.i("Firestore", string);
            }
        }
    }
    
    public static boolean c() {
        return Logger.a.ordinal() >= Level.DEBUG.ordinal();
    }
    
    public static void d(final String s, final String s2, final Object... array) {
        b(Level.WARN, s, s2, array);
    }
    
    public enum Level
    {
        private static final Level[] $VALUES;
        
        DEBUG, 
        NONE, 
        WARN;
    }
}

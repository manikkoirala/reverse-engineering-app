// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.util;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.RejectedExecutionException;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ScheduledFuture;
import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;
import java.util.concurrent.Callable;
import com.google.android.gms.tasks.Task;
import java.util.ArrayList;

public class AsyncQueue
{
    public final c a;
    public final ArrayList b;
    public final ArrayList c;
    
    public AsyncQueue() {
        this.c = new ArrayList();
        this.b = new ArrayList();
        this.a = new c();
    }
    
    public static /* synthetic */ c c(final AsyncQueue asyncQueue) {
        return asyncQueue.a;
    }
    
    public final b e(final TimerId timerId, final long n, final Runnable runnable) {
        final b b = new b(timerId, System.currentTimeMillis() + n, runnable, null);
        b.f(n);
        return b;
    }
    
    public Task f(final Runnable runnable) {
        return this.g(new k9(runnable));
    }
    
    public Task g(final Callable callable) {
        return this.a.i(callable);
    }
    
    public b h(final TimerId o, long n, final Runnable runnable) {
        if (this.c.contains(o)) {
            n = 0L;
        }
        final b e = this.e(o, n, runnable);
        this.b.add(e);
        return e;
    }
    
    public void i(final Runnable runnable) {
        this.f(runnable);
    }
    
    public Executor j() {
        return this.a;
    }
    
    public boolean k() {
        return this.a.j();
    }
    
    public void n(final Throwable t) {
        this.a.m();
        new Handler(Looper.getMainLooper()).post((Runnable)new l9(t));
    }
    
    public final void o(final b o) {
        g9.d(this.b.remove(o), "Delayed task not found.", new Object[0]);
    }
    
    public void p() {
        final Thread currentThread = Thread.currentThread();
        if (AsyncQueue.c.d(this.a) == currentThread) {
            return;
        }
        throw g9.a("We are running on the wrong thread. Expected to be on the AsyncQueue thread %s/%d but was %s/%d", AsyncQueue.c.d(this.a).getName(), AsyncQueue.c.d(this.a).getId(), currentThread.getName(), currentThread.getId());
    }
    
    public enum TimerId
    {
        private static final TimerId[] $VALUES;
        
        ALL, 
        CONNECTIVITY_ATTEMPT_TIMER, 
        GARBAGE_COLLECTION, 
        HEALTH_CHECK_TIMEOUT, 
        INDEX_BACKFILL, 
        LISTEN_STREAM_CONNECTION_BACKOFF, 
        LISTEN_STREAM_IDLE, 
        ONLINE_STATE_TIMEOUT, 
        RETRY_TRANSACTION, 
        WRITE_STREAM_CONNECTION_BACKOFF, 
        WRITE_STREAM_IDLE;
    }
    
    public class b
    {
        public final TimerId a;
        public final long b;
        public final Runnable c;
        public ScheduledFuture d;
        public final AsyncQueue e;
        
        public b(final AsyncQueue e, final TimerId a, final long b, final Runnable c) {
            this.e = e;
            this.a = a;
            this.b = b;
            this.c = c;
        }
        
        public void c() {
            this.e.p();
            final ScheduledFuture d = this.d;
            if (d != null) {
                d.cancel(false);
                this.e();
            }
        }
        
        public final void d() {
            this.e.p();
            if (this.d != null) {
                this.e();
                this.c.run();
            }
        }
        
        public final void e() {
            g9.d(this.d != null, "Caller should have verified scheduledFuture is non-null.", new Object[0]);
            this.d = null;
            this.e.o(this);
        }
        
        public final void f(final long n) {
            this.d = AsyncQueue.c(this.e).schedule(new m9(this), n, TimeUnit.MILLISECONDS);
        }
    }
    
    public class c implements Executor
    {
        public final ScheduledThreadPoolExecutor a;
        public boolean b;
        public final Thread c;
        public final AsyncQueue d;
        
        public c(final AsyncQueue d) {
            this.d = d;
            final b b = new b(null);
            final Thread thread = Executors.defaultThreadFactory().newThread(b);
            (this.c = thread).setName("FirestoreWorker");
            thread.setDaemon(true);
            thread.setUncaughtExceptionHandler((Thread.UncaughtExceptionHandler)new n9(this));
            (this.a = new ScheduledThreadPoolExecutor(this, 1, b, d) {
                public final AsyncQueue a;
                public final c b;
                
                public void afterExecute(final Runnable r, final Throwable t) {
                    super.afterExecute(r, t);
                    if (t != null || !(r instanceof Future)) {
                        goto Label_0067;
                    }
                    final Future future = (Future)r;
                    try {
                        if (future.isDone()) {
                            future.get();
                            goto Label_0067;
                        }
                        goto Label_0067;
                    }
                    catch (final InterruptedException ex) {
                        Thread.currentThread().interrupt();
                        goto Label_0067;
                    }
                    catch (final ExecutionException ex2) {
                        ex2.getCause();
                    }
                    catch (final CancellationException ex3) {
                        goto Label_0067;
                    }
                }
            }).setKeepAliveTime(3L, TimeUnit.SECONDS);
            this.b = false;
        }
        
        public static /* synthetic */ Thread d(final c c) {
            return c.c;
        }
        
        @Override
        public void execute(final Runnable command) {
            synchronized (this) {
                if (!this.b) {
                    this.a.execute(command);
                }
            }
        }
        
        public final Task i(final Callable callable) {
            final TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
            try {
                this.execute(new o9(taskCompletionSource, callable));
            }
            catch (final RejectedExecutionException ex) {
                Logger.d(AsyncQueue.class.getSimpleName(), "Refused to enqueue task after panic", new Object[0]);
            }
            return taskCompletionSource.getTask();
        }
        
        public final boolean j() {
            synchronized (this) {
                return this.b;
            }
        }
        
        public final void m() {
            this.a.shutdownNow();
        }
        
        public final ScheduledFuture schedule(final Runnable command, final long delay, final TimeUnit unit) {
            synchronized (this) {
                if (!this.b) {
                    return this.a.schedule(command, delay, unit);
                }
                return null;
            }
        }
        
        public class b implements Runnable, ThreadFactory
        {
            public final CountDownLatch a;
            public Runnable b;
            public final c c;
            
            public b(final c c) {
                this.c = c;
                this.a = new CountDownLatch(1);
            }
            
            @Override
            public Thread newThread(final Runnable b) {
                g9.d(this.b == null, "Only one thread may be created in an AsyncQueue.", new Object[0]);
                this.b = b;
                this.a.countDown();
                return AsyncQueue.c.d(this.c);
            }
            
            @Override
            public void run() {
                try {
                    this.a.await();
                }
                catch (final InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
                this.b.run();
            }
        }
    }
}

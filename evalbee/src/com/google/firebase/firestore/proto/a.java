// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.proto;

import com.google.protobuf.a0;
import com.google.protobuf.j0;
import com.google.protobuf.GeneratedMessageLite;

public final class a extends GeneratedMessageLite implements xv0
{
    private static final a DEFAULT_INSTANCE;
    public static final int NAME_FIELD_NUMBER = 1;
    private static volatile b31 PARSER;
    public static final int READ_TIME_FIELD_NUMBER = 2;
    private String name_;
    private j0 readTime_;
    
    static {
        GeneratedMessageLite.V(a.class, DEFAULT_INSTANCE = new a());
    }
    
    public a() {
        this.name_ = "";
    }
    
    public static /* synthetic */ a Z() {
        return a.DEFAULT_INSTANCE;
    }
    
    public static a c0() {
        return a.DEFAULT_INSTANCE;
    }
    
    public static b f0() {
        return (b)a.DEFAULT_INSTANCE.u();
    }
    
    public String d0() {
        return this.name_;
    }
    
    public j0 e0() {
        j0 j0;
        if ((j0 = this.readTime_) == null) {
            j0 = com.google.protobuf.j0.c0();
        }
        return j0;
    }
    
    public final void g0(final String name_) {
        name_.getClass();
        this.name_ = name_;
    }
    
    public final void h0(final j0 readTime_) {
        readTime_.getClass();
        this.readTime_ = readTime_;
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (a$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = a.PARSER) == null) {
                    synchronized (a.class) {
                        if (a.PARSER == null) {
                            a.PARSER = new GeneratedMessageLite.b(a.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return a.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(a.DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0208\u0002\t", new Object[] { "name_", "readTime_" });
            }
            case 2: {
                return new b((a$a)null);
            }
            case 1: {
                return new a();
            }
        }
    }
    
    public static final class b extends GeneratedMessageLite.a implements xv0
    {
        public b() {
            super(a.Z());
        }
        
        public b A(final String s) {
            ((GeneratedMessageLite.a)this).s();
            ((a)super.b).g0(s);
            return this;
        }
        
        public b B(final j0 j0) {
            ((GeneratedMessageLite.a)this).s();
            ((a)super.b).h0(j0);
            return this;
        }
    }
}

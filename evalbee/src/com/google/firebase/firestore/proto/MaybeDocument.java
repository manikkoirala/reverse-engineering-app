// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.proto;

import com.google.protobuf.a0;
import com.google.firestore.v1.e;
import com.google.protobuf.GeneratedMessageLite;

public final class MaybeDocument extends GeneratedMessageLite implements xv0
{
    private static final MaybeDocument DEFAULT_INSTANCE;
    public static final int DOCUMENT_FIELD_NUMBER = 2;
    public static final int HAS_COMMITTED_MUTATIONS_FIELD_NUMBER = 4;
    public static final int NO_DOCUMENT_FIELD_NUMBER = 1;
    private static volatile b31 PARSER;
    public static final int UNKNOWN_DOCUMENT_FIELD_NUMBER = 3;
    private int documentTypeCase_;
    private Object documentType_;
    private boolean hasCommittedMutations_;
    
    static {
        GeneratedMessageLite.V(MaybeDocument.class, DEFAULT_INSTANCE = new MaybeDocument());
    }
    
    public MaybeDocument() {
        this.documentTypeCase_ = 0;
    }
    
    public static /* synthetic */ MaybeDocument Z() {
        return MaybeDocument.DEFAULT_INSTANCE;
    }
    
    public static b j0() {
        return (b)MaybeDocument.DEFAULT_INSTANCE.u();
    }
    
    public static MaybeDocument k0(final byte[] array) {
        return (MaybeDocument)GeneratedMessageLite.R(MaybeDocument.DEFAULT_INSTANCE, array);
    }
    
    public e e0() {
        if (this.documentTypeCase_ == 2) {
            return (e)this.documentType_;
        }
        return e.d0();
    }
    
    public DocumentTypeCase f0() {
        return DocumentTypeCase.forNumber(this.documentTypeCase_);
    }
    
    public boolean g0() {
        return this.hasCommittedMutations_;
    }
    
    public com.google.firebase.firestore.proto.a h0() {
        if (this.documentTypeCase_ == 1) {
            return (com.google.firebase.firestore.proto.a)this.documentType_;
        }
        return com.google.firebase.firestore.proto.a.c0();
    }
    
    public com.google.firebase.firestore.proto.b i0() {
        if (this.documentTypeCase_ == 3) {
            return (com.google.firebase.firestore.proto.b)this.documentType_;
        }
        return com.google.firebase.firestore.proto.b.c0();
    }
    
    public final void l0(final e documentType_) {
        documentType_.getClass();
        this.documentType_ = documentType_;
        this.documentTypeCase_ = 2;
    }
    
    public final void m0(final boolean hasCommittedMutations_) {
        this.hasCommittedMutations_ = hasCommittedMutations_;
    }
    
    public final void n0(final com.google.firebase.firestore.proto.a documentType_) {
        documentType_.getClass();
        this.documentType_ = documentType_;
        this.documentTypeCase_ = 1;
    }
    
    public final void o0(final com.google.firebase.firestore.proto.b documentType_) {
        documentType_.getClass();
        this.documentType_ = documentType_;
        this.documentTypeCase_ = 3;
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (MaybeDocument$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = MaybeDocument.PARSER) == null) {
                    synchronized (MaybeDocument.class) {
                        if (MaybeDocument.PARSER == null) {
                            MaybeDocument.PARSER = new GeneratedMessageLite.b(MaybeDocument.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return MaybeDocument.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(MaybeDocument.DEFAULT_INSTANCE, "\u0000\u0004\u0001\u0000\u0001\u0004\u0004\u0000\u0000\u0000\u0001<\u0000\u0002<\u0000\u0003<\u0000\u0004\u0007", new Object[] { "documentType_", "documentTypeCase_", com.google.firebase.firestore.proto.a.class, e.class, com.google.firebase.firestore.proto.b.class, "hasCommittedMutations_" });
            }
            case 2: {
                return new b((MaybeDocument$a)null);
            }
            case 1: {
                return new MaybeDocument();
            }
        }
    }
    
    public enum DocumentTypeCase
    {
        private static final DocumentTypeCase[] $VALUES;
        
        DOCUMENT(2), 
        DOCUMENTTYPE_NOT_SET(0), 
        NO_DOCUMENT(1), 
        UNKNOWN_DOCUMENT(3);
        
        private final int value;
        
        private DocumentTypeCase(final int value) {
            this.value = value;
        }
        
        public static DocumentTypeCase forNumber(final int n) {
            if (n == 0) {
                return DocumentTypeCase.DOCUMENTTYPE_NOT_SET;
            }
            if (n == 1) {
                return DocumentTypeCase.NO_DOCUMENT;
            }
            if (n == 2) {
                return DocumentTypeCase.DOCUMENT;
            }
            if (n != 3) {
                return null;
            }
            return DocumentTypeCase.UNKNOWN_DOCUMENT;
        }
        
        @Deprecated
        public static DocumentTypeCase valueOf(final int n) {
            return forNumber(n);
        }
        
        public int getNumber() {
            return this.value;
        }
    }
    
    public static final class b extends a implements xv0
    {
        public b() {
            super(MaybeDocument.Z());
        }
        
        public b A(final e e) {
            ((a)this).s();
            ((MaybeDocument)super.b).l0(e);
            return this;
        }
        
        public b B(final boolean b) {
            ((a)this).s();
            ((MaybeDocument)super.b).m0(b);
            return this;
        }
        
        public b C(final com.google.firebase.firestore.proto.a a) {
            ((a)this).s();
            ((MaybeDocument)super.b).n0(a);
            return this;
        }
        
        public b D(final com.google.firebase.firestore.proto.b b) {
            ((a)this).s();
            ((MaybeDocument)super.b).o0(b);
            return this;
        }
    }
}

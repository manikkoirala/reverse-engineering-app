// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.proto;

import com.google.protobuf.a0;
import com.google.protobuf.j0;
import com.google.protobuf.GeneratedMessageLite;

public final class b extends GeneratedMessageLite implements xv0
{
    private static final b DEFAULT_INSTANCE;
    public static final int NAME_FIELD_NUMBER = 1;
    private static volatile b31 PARSER;
    public static final int VERSION_FIELD_NUMBER = 2;
    private String name_;
    private j0 version_;
    
    static {
        GeneratedMessageLite.V(b.class, DEFAULT_INSTANCE = new b());
    }
    
    public b() {
        this.name_ = "";
    }
    
    public static /* synthetic */ b Z() {
        return b.DEFAULT_INSTANCE;
    }
    
    public static b c0() {
        return b.DEFAULT_INSTANCE;
    }
    
    public static b f0() {
        return (b)b.DEFAULT_INSTANCE.u();
    }
    
    public String d0() {
        return this.name_;
    }
    
    public j0 e0() {
        j0 j0;
        if ((j0 = this.version_) == null) {
            j0 = com.google.protobuf.j0.c0();
        }
        return j0;
    }
    
    public final void g0(final String name_) {
        name_.getClass();
        this.name_ = name_;
    }
    
    public final void h0(final j0 version_) {
        version_.getClass();
        this.version_ = version_;
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (b$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = b.PARSER) == null) {
                    synchronized (b.class) {
                        if (b.PARSER == null) {
                            b.PARSER = new GeneratedMessageLite.b(b.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return b.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(b.DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0208\u0002\t", new Object[] { "name_", "version_" });
            }
            case 2: {
                return new b((b$a)null);
            }
            case 1: {
                return new b();
            }
        }
    }
    
    public static final class b extends a implements xv0
    {
        public b() {
            super(com.google.firebase.firestore.proto.b.Z());
        }
        
        public b A(final String s) {
            ((a)this).s();
            ((com.google.firebase.firestore.proto.b)super.b).g0(s);
            return this;
        }
        
        public b B(final j0 j0) {
            ((a)this).s();
            ((com.google.firebase.firestore.proto.b)super.b).h0(j0);
            return this;
        }
    }
}

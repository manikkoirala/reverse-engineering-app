// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.proto;

import com.google.protobuf.a0;
import com.google.protobuf.ByteString;
import com.google.protobuf.j0;
import com.google.protobuf.GeneratedMessageLite;

public final class Target extends GeneratedMessageLite implements xv0
{
    private static final Target DEFAULT_INSTANCE;
    public static final int DOCUMENTS_FIELD_NUMBER = 6;
    public static final int LAST_LIMBO_FREE_SNAPSHOT_VERSION_FIELD_NUMBER = 7;
    public static final int LAST_LISTEN_SEQUENCE_NUMBER_FIELD_NUMBER = 4;
    private static volatile b31 PARSER;
    public static final int QUERY_FIELD_NUMBER = 5;
    public static final int RESUME_TOKEN_FIELD_NUMBER = 3;
    public static final int SNAPSHOT_VERSION_FIELD_NUMBER = 2;
    public static final int TARGET_ID_FIELD_NUMBER = 1;
    private j0 lastLimboFreeSnapshotVersion_;
    private long lastListenSequenceNumber_;
    private ByteString resumeToken_;
    private j0 snapshotVersion_;
    private int targetId_;
    private int targetTypeCase_;
    private Object targetType_;
    
    static {
        GeneratedMessageLite.V(Target.class, DEFAULT_INSTANCE = new Target());
    }
    
    public Target() {
        this.targetTypeCase_ = 0;
        this.resumeToken_ = ByteString.EMPTY;
    }
    
    public static /* synthetic */ Target Z() {
        return Target.DEFAULT_INSTANCE;
    }
    
    public static b r0() {
        return (b)Target.DEFAULT_INSTANCE.u();
    }
    
    public static Target s0(final byte[] array) {
        return (Target)GeneratedMessageLite.R(Target.DEFAULT_INSTANCE, array);
    }
    
    public final void i0() {
        this.lastLimboFreeSnapshotVersion_ = null;
    }
    
    public com.google.firestore.v1.Target.c j0() {
        if (this.targetTypeCase_ == 6) {
            return (com.google.firestore.v1.Target.c)this.targetType_;
        }
        return com.google.firestore.v1.Target.c.d0();
    }
    
    public j0 k0() {
        j0 j0;
        if ((j0 = this.lastLimboFreeSnapshotVersion_) == null) {
            j0 = com.google.protobuf.j0.c0();
        }
        return j0;
    }
    
    public long l0() {
        return this.lastListenSequenceNumber_;
    }
    
    public com.google.firestore.v1.Target.QueryTarget m0() {
        if (this.targetTypeCase_ == 5) {
            return (com.google.firestore.v1.Target.QueryTarget)this.targetType_;
        }
        return com.google.firestore.v1.Target.QueryTarget.c0();
    }
    
    public ByteString n0() {
        return this.resumeToken_;
    }
    
    public j0 o0() {
        j0 j0;
        if ((j0 = this.snapshotVersion_) == null) {
            j0 = com.google.protobuf.j0.c0();
        }
        return j0;
    }
    
    public int p0() {
        return this.targetId_;
    }
    
    public TargetTypeCase q0() {
        return TargetTypeCase.forNumber(this.targetTypeCase_);
    }
    
    public final void t0(final com.google.firestore.v1.Target.c targetType_) {
        targetType_.getClass();
        this.targetType_ = targetType_;
        this.targetTypeCase_ = 6;
    }
    
    public final void u0(final j0 lastLimboFreeSnapshotVersion_) {
        lastLimboFreeSnapshotVersion_.getClass();
        this.lastLimboFreeSnapshotVersion_ = lastLimboFreeSnapshotVersion_;
    }
    
    public final void v0(final long lastListenSequenceNumber_) {
        this.lastListenSequenceNumber_ = lastListenSequenceNumber_;
    }
    
    public final void w0(final com.google.firestore.v1.Target.QueryTarget targetType_) {
        targetType_.getClass();
        this.targetType_ = targetType_;
        this.targetTypeCase_ = 5;
    }
    
    public final void x0(final ByteString resumeToken_) {
        resumeToken_.getClass();
        this.resumeToken_ = resumeToken_;
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (Target$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = Target.PARSER) == null) {
                    synchronized (Target.class) {
                        if (Target.PARSER == null) {
                            Target.PARSER = new GeneratedMessageLite.b(Target.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return Target.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(Target.DEFAULT_INSTANCE, "\u0000\u0007\u0001\u0000\u0001\u0007\u0007\u0000\u0000\u0000\u0001\u0004\u0002\t\u0003\n\u0004\u0002\u0005<\u0000\u0006<\u0000\u0007\t", new Object[] { "targetType_", "targetTypeCase_", "targetId_", "snapshotVersion_", "resumeToken_", "lastListenSequenceNumber_", com.google.firestore.v1.Target.QueryTarget.class, com.google.firestore.v1.Target.c.class, "lastLimboFreeSnapshotVersion_" });
            }
            case 2: {
                return new b((Target$a)null);
            }
            case 1: {
                return new Target();
            }
        }
    }
    
    public final void y0(final j0 snapshotVersion_) {
        snapshotVersion_.getClass();
        this.snapshotVersion_ = snapshotVersion_;
    }
    
    public final void z0(final int targetId_) {
        this.targetId_ = targetId_;
    }
    
    public enum TargetTypeCase
    {
        private static final TargetTypeCase[] $VALUES;
        
        DOCUMENTS(6), 
        QUERY(5), 
        TARGETTYPE_NOT_SET(0);
        
        private final int value;
        
        private TargetTypeCase(final int value) {
            this.value = value;
        }
        
        public static TargetTypeCase forNumber(final int n) {
            if (n == 0) {
                return TargetTypeCase.TARGETTYPE_NOT_SET;
            }
            if (n == 5) {
                return TargetTypeCase.QUERY;
            }
            if (n != 6) {
                return null;
            }
            return TargetTypeCase.DOCUMENTS;
        }
        
        @Deprecated
        public static TargetTypeCase valueOf(final int n) {
            return forNumber(n);
        }
        
        public int getNumber() {
            return this.value;
        }
    }
    
    public static final class b extends a implements xv0
    {
        public b() {
            super(Target.Z());
        }
        
        public b A() {
            ((a)this).s();
            ((Target)super.b).i0();
            return this;
        }
        
        public b B(final com.google.firestore.v1.Target.c c) {
            ((a)this).s();
            ((Target)super.b).t0(c);
            return this;
        }
        
        public b C(final j0 j0) {
            ((a)this).s();
            ((Target)super.b).u0(j0);
            return this;
        }
        
        public b D(final long n) {
            ((a)this).s();
            ((Target)super.b).v0(n);
            return this;
        }
        
        public b E(final com.google.firestore.v1.Target.QueryTarget queryTarget) {
            ((a)this).s();
            ((Target)super.b).w0(queryTarget);
            return this;
        }
        
        public b F(final ByteString byteString) {
            ((a)this).s();
            ((Target)super.b).x0(byteString);
            return this;
        }
        
        public b G(final j0 j0) {
            ((a)this).s();
            ((Target)super.b).y0(j0);
            return this;
        }
        
        public b H(final int n) {
            ((a)this).s();
            ((Target)super.b).z0(n);
            return this;
        }
    }
}

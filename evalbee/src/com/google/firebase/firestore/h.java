// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore;

import com.google.protobuf.j0;
import com.google.firebase.firestore.util.Logger;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import com.google.firestore.v1.Value;
import java.util.ArrayList;
import java.util.List;
import com.google.firestore.v1.a;

public class h
{
    public final FirebaseFirestore a;
    public final DocumentSnapshot.ServerTimestampBehavior b;
    
    public h(final FirebaseFirestore a, final DocumentSnapshot.ServerTimestampBehavior b) {
        this.a = a;
        this.b = b;
    }
    
    public final List a(final a a) {
        final ArrayList list = new ArrayList(a.i0());
        final Iterator iterator = a.h().iterator();
        while (iterator.hasNext()) {
            list.add(this.f((Value)iterator.next()));
        }
        return list;
    }
    
    public Map b(final Map map) {
        final HashMap hashMap = new HashMap();
        for (final Map.Entry<String, V> entry : map.entrySet()) {
            hashMap.put(entry.getKey(), this.f((Value)entry.getValue()));
        }
        return hashMap;
    }
    
    public final Object c(final Value value) {
        final qp d = qp.d(value.t0());
        final du f = du.f(value.t0());
        final qp d2 = this.a.d();
        if (!d.equals(d2)) {
            Logger.d("DocumentSnapshot", "Document %s contains a document reference within a different database (%s/%s) which is not supported. It will be treated as a reference in the current database (%s/%s) instead.", f.m(), d.f(), d.e(), d2.f(), d2.e());
        }
        return new com.google.firebase.firestore.a(f, this.a);
    }
    
    public final Object d(Value b) {
        final int n = h$a.a[this.b.ordinal()];
        if (n != 1) {
            if (n != 2) {
                return null;
            }
            return this.e(ql1.a(b));
        }
        else {
            b = ql1.b(b);
            if (b == null) {
                return null;
            }
            return this.f(b);
        }
    }
    
    public final Object e(final j0 j0) {
        return new pw1(j0.e0(), j0.d0());
    }
    
    public Object f(final Value value) {
        switch (a32.G(value)) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unknown value type: ");
                sb.append(value.w0());
                throw g9.a(sb.toString(), new Object[0]);
            }
            case 10: {
                return this.b(value.s0().d0());
            }
            case 9: {
                return this.a(value.l0());
            }
            case 8: {
                return new ia0(value.q0().d0(), value.q0().e0());
            }
            case 7: {
                return this.c(value);
            }
            case 6: {
                return ec.c(value.n0());
            }
            case 5: {
                return value.u0();
            }
            case 4: {
                return this.d(value);
            }
            case 3: {
                return this.e(value.v0());
            }
            case 2: {
                Number n;
                if (value.w0().equals(Value.ValueTypeCase.INTEGER_VALUE)) {
                    n = value.r0();
                }
                else {
                    n = value.p0();
                }
                return n;
            }
            case 1: {
                return value.m0();
            }
            case 0: {
                return null;
            }
        }
    }
}

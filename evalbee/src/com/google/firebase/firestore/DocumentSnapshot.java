// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore;

import java.util.Map;

public class DocumentSnapshot
{
    public final FirebaseFirestore a;
    public final du b;
    public final zt c;
    public final po1 d;
    
    public DocumentSnapshot(final FirebaseFirestore firebaseFirestore, final du du, final zt c, final boolean b, final boolean b2) {
        this.a = (FirebaseFirestore)k71.b(firebaseFirestore);
        this.b = (du)k71.b(du);
        this.c = c;
        this.d = new po1(b2, b);
    }
    
    public static DocumentSnapshot b(final FirebaseFirestore firebaseFirestore, final zt zt, final boolean b, final boolean b2) {
        return new DocumentSnapshot(firebaseFirestore, zt.getKey(), zt, b, b2);
    }
    
    public static DocumentSnapshot c(final FirebaseFirestore firebaseFirestore, final du du, final boolean b) {
        return new DocumentSnapshot(firebaseFirestore, du, null, b, false);
    }
    
    public boolean a() {
        return this.c != null;
    }
    
    public Map d() {
        return this.e(ServerTimestampBehavior.DEFAULT);
    }
    
    public Map e(final ServerTimestampBehavior serverTimestampBehavior) {
        k71.c(serverTimestampBehavior, "Provided serverTimestampBehavior value must not be null.");
        final h h = new h(this.a, serverTimestampBehavior);
        final zt c = this.c;
        Map b;
        if (c == null) {
            b = null;
        }
        else {
            b = h.b(c.getData().l());
        }
        return b;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof DocumentSnapshot)) {
            return false;
        }
        final DocumentSnapshot documentSnapshot = (DocumentSnapshot)o;
        if (this.a.equals(documentSnapshot.a) && this.b.equals(documentSnapshot.b)) {
            final zt c = this.c;
            if (c == null) {
                if (documentSnapshot.c != null) {
                    return false;
                }
            }
            else if (!c.equals(documentSnapshot.c)) {
                return false;
            }
            if (this.d.equals(documentSnapshot.d)) {
                return b;
            }
        }
        b = false;
        return b;
    }
    
    public po1 f() {
        return this.d;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.a.hashCode();
        final int hashCode2 = this.b.hashCode();
        final zt c = this.c;
        int hashCode3 = 0;
        int hashCode4;
        if (c != null) {
            hashCode4 = c.getKey().hashCode();
        }
        else {
            hashCode4 = 0;
        }
        final zt c2 = this.c;
        if (c2 != null) {
            hashCode3 = c2.getData().hashCode();
        }
        return (((hashCode * 31 + hashCode2) * 31 + hashCode4) * 31 + hashCode3) * 31 + this.d.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("DocumentSnapshot{key=");
        sb.append(this.b);
        sb.append(", metadata=");
        sb.append(this.d);
        sb.append(", doc=");
        sb.append(this.c);
        sb.append('}');
        return sb.toString();
    }
    
    public enum ServerTimestampBehavior
    {
        private static final ServerTimestampBehavior[] $VALUES;
        static final ServerTimestampBehavior DEFAULT;
        
        ESTIMATE, 
        NONE, 
        PREVIOUS;
        
        static {
            final ServerTimestampBehavior default1;
            DEFAULT = default1;
        }
    }
}

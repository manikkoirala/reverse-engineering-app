// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore;

import java.util.Map;

public class f extends DocumentSnapshot
{
    public f(final FirebaseFirestore firebaseFirestore, final du du, final zt zt, final boolean b, final boolean b2) {
        super(firebaseFirestore, du, zt, b, b2);
    }
    
    public static f g(final FirebaseFirestore firebaseFirestore, final zt zt, final boolean b, final boolean b2) {
        return new f(firebaseFirestore, zt.getKey(), zt, b, b2);
    }
    
    @Override
    public Map d() {
        final Map d = super.d();
        g9.d(d != null, "Data in a QueryDocumentSnapshot should be non-null", new Object[0]);
        return d;
    }
    
    @Override
    public Map e(final ServerTimestampBehavior serverTimestampBehavior) {
        k71.c(serverTimestampBehavior, "Provided serverTimestampBehavior value must not be null.");
        final Map e = super.e(serverTimestampBehavior);
        g9.d(e != null, "Data in a QueryDocumentSnapshot should be non-null", new Object[0]);
        return e;
    }
}

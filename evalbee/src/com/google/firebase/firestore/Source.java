// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore;

public enum Source
{
    private static final Source[] $VALUES;
    
    CACHE, 
    DEFAULT, 
    SERVER;
}

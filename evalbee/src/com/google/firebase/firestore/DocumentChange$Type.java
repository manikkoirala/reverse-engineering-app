// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore;

public enum DocumentChange$Type
{
    private static final DocumentChange$Type[] $VALUES;
    
    ADDED, 
    MODIFIED, 
    REMOVED;
}

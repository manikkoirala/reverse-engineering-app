// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore;

import org.jetbrains.annotations.NotNull;
import java.util.List;
import androidx.annotation.Keep;
import com.google.firebase.components.ComponentRegistrar;

@Keep
public final class FirebaseFirestoreKtxRegistrar implements ComponentRegistrar
{
    @NotNull
    @Override
    public List<zi> getComponents() {
        return nh.g();
    }
}

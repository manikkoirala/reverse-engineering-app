// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore;

import java.util.Iterator;
import com.google.firebase.firestore.core.ViewSnapshot;

public class g implements Iterable
{
    public final Query a;
    public final ViewSnapshot b;
    public final FirebaseFirestore c;
    public final po1 d;
    
    public g(final Query query, final ViewSnapshot viewSnapshot, final FirebaseFirestore firebaseFirestore) {
        this.a = (Query)k71.b(query);
        this.b = (ViewSnapshot)k71.b(viewSnapshot);
        this.c = (FirebaseFirestore)k71.b(firebaseFirestore);
        this.d = new po1(viewSnapshot.j(), viewSnapshot.k());
    }
    
    public final f b(final zt zt) {
        return f.g(this.c, zt, this.b.k(), this.b.f().contains(zt.getKey()));
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof g)) {
            return false;
        }
        final g g = (g)o;
        if (!this.c.equals(g.c) || !this.a.equals(g.a) || !this.b.equals(g.b) || !this.d.equals(g.d)) {
            b = false;
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        return ((this.c.hashCode() * 31 + this.a.hashCode()) * 31 + this.b.hashCode()) * 31 + this.d.hashCode();
    }
    
    @Override
    public Iterator iterator() {
        return new a(this.b.e().iterator());
    }
    
    public class a implements Iterator
    {
        public final Iterator a;
        public final g b;
        
        public a(final g b, final Iterator a) {
            this.b = b;
            this.a = a;
        }
        
        public f b() {
            return this.b.b(this.a.next());
        }
        
        @Override
        public boolean hasNext() {
            return this.a.hasNext();
        }
        
        @Override
        public void remove() {
            throw new UnsupportedOperationException("QuerySnapshot does not support remove().");
        }
    }
}

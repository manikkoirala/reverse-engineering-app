// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.local;

import java.util.Arrays;
import com.google.firebase.firestore.core.Query;
import java.util.Collection;
import com.google.firebase.firestore.util.Logger;
import java.util.ArrayList;
import com.google.firebase.firestore.model.MutableDocument;
import java.util.HashSet;
import com.google.firebase.database.collection.c;
import java.util.List;
import java.util.Set;
import java.util.Iterator;
import com.google.protobuf.ByteString;
import com.google.firebase.firestore.core.q;
import com.google.firebase.database.collection.b;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.Map;
import android.util.SparseArray;

public final class a
{
    public static final long n;
    public final d51 a;
    public IndexManager b;
    public zx0 c;
    public eu d;
    public final id1 e;
    public xk0 f;
    public final f g;
    public final xc1 h;
    public final yt1 i;
    public final gd j;
    public final SparseArray k;
    public final Map l;
    public final bu1 m;
    
    static {
        n = TimeUnit.MINUTES.toSeconds(5L);
    }
    
    public a(final d51 a, final f g, final u12 u12) {
        g9.d(a.i(), "LocalStore was passed an unstarted persistence implementation", new Object[0]);
        this.a = a;
        this.g = g;
        final yt1 h = a.h();
        this.i = h;
        this.j = a.a();
        this.m = bu1.b(h.d());
        this.e = a.g();
        final xc1 h2 = new xc1();
        this.h = h2;
        this.k = new SparseArray();
        this.l = new HashMap();
        a.f().o(h2);
        this.z(u12);
    }
    
    public static boolean R(final au1 au1, final au1 au2, final zt1 zt1) {
        final boolean empty = au1.d().isEmpty();
        boolean b = true;
        if (empty) {
            return true;
        }
        final long e = au2.f().c().e();
        final long e2 = au1.f().c().e();
        final long n = a.n;
        if (e - e2 >= n) {
            return true;
        }
        if (au2.b().c().e() - au1.b().c().e() >= n) {
            return true;
        }
        if (zt1 == null) {
            return false;
        }
        if (zt1.b().size() + zt1.c().size() + zt1.d().size() <= 0) {
            b = false;
        }
        return b;
    }
    
    public void L(final List list) {
        this.a.k("notifyLocalViewChanges", new al0(this, list));
    }
    
    public final c M(final Map map) {
        final HashMap hashMap = new HashMap();
        final ArrayList list = new ArrayList();
        final HashSet set = new HashSet();
        final Map all = this.e.getAll(map.keySet());
        for (final Map.Entry<du, V> entry : map.entrySet()) {
            final du du = entry.getKey();
            final MutableDocument mutableDocument = (MutableDocument)entry.getValue();
            final MutableDocument mutableDocument2 = all.get(du);
            if (mutableDocument.d() != mutableDocument2.d()) {
                set.add(du);
            }
            if (mutableDocument.c() && mutableDocument.getVersion().equals(qo1.b)) {
                list.add(mutableDocument.getKey());
            }
            else {
                if (mutableDocument2.o() && mutableDocument.getVersion().a(mutableDocument2.getVersion()) <= 0 && (mutableDocument.getVersion().a(mutableDocument2.getVersion()) != 0 || !mutableDocument2.b())) {
                    Logger.a("LocalStore", "Ignoring outdated watch update for %s.Current version: %s  Watch version: %s", du, mutableDocument2.getVersion(), mutableDocument.getVersion());
                    continue;
                }
                g9.d(qo1.b.equals(mutableDocument.e()) ^ true, "Cannot add a document when the remote version is zero", new Object[0]);
                this.e.c(mutableDocument, mutableDocument.e());
            }
            hashMap.put(du, mutableDocument);
        }
        this.e.removeAll(list);
        return new c(hashMap, set, null);
    }
    
    public zt N(final du du) {
        return this.f.c(du);
    }
    
    public com.google.firebase.database.collection.b O(final int n) {
        return (com.google.firebase.database.collection.b)this.a.j("Reject batch", new fl0(this, n));
    }
    
    public void P(final int n) {
        this.a.k("Release target", new il0(this, n));
    }
    
    public void Q(final ByteString byteString) {
        this.a.k("Set stream token", new hl0(this, byteString));
    }
    
    public void S() {
        this.a.e().run();
        this.T();
        this.U();
    }
    
    public final void T() {
        this.a.k("Start IndexManager", new cl0(this));
    }
    
    public final void U() {
        this.a.k("Start MutationQueue", new dl0(this));
    }
    
    public wk0 V(final List list) {
        final pw1 f = pw1.f();
        final HashSet set = new HashSet();
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            set.add(((wx0)iterator.next()).g());
        }
        return (wk0)this.a.j("Locally write mutations", new el0(this, set, list, f));
    }
    
    public com.google.firebase.database.collection.b l(final yx0 yx0) {
        return (com.google.firebase.database.collection.b)this.a.j("Acknowledge batch", new kl0(this, yx0));
    }
    
    public au1 m(final q q) {
        au1 au1 = this.i.c(q);
        int i;
        if (au1 != null) {
            i = au1.h();
        }
        else {
            final b b = new b(null);
            this.a.k("Allocate target", new jl0(this, b, q));
            i = b.b;
            au1 = b.a;
        }
        if (this.k.get(i) == null) {
            this.k.put(i, (Object)au1);
            this.l.put(q, i);
        }
        return au1;
    }
    
    public com.google.firebase.database.collection.b n(final jd1 jd1) {
        return (com.google.firebase.database.collection.b)this.a.j("Apply remote event", new bl0(this, jd1, jd1.c()));
    }
    
    public final void o(final yx0 yx0) {
        final xx0 b = yx0.b();
        for (final du du : b.f()) {
            final MutableDocument d = this.e.d(du);
            final qo1 qo1 = (qo1)yx0.d().b(du);
            g9.d(qo1 != null, "docVersions should contain every doc in the write.", new Object[0]);
            if (d.getVersion().a(qo1) < 0) {
                b.c(d, yx0);
                if (!d.o()) {
                    continue;
                }
                this.e.c(d, yx0.c());
            }
        }
        this.c.c(b);
    }
    
    public com.google.firebase.firestore.local.b.c p(final com.google.firebase.firestore.local.b b) {
        return (com.google.firebase.firestore.local.b.c)this.a.j("Collect garbage", new gl0(this, b));
    }
    
    public ja1 q(final Query query, final boolean b) {
        final au1 x = this.x(query.x());
        qo1 b2 = qo1.b;
        com.google.firebase.database.collection.c c = du.e();
        qo1 b3;
        if (x != null) {
            b3 = x.b();
            c = this.i.h(x.h());
        }
        else {
            b3 = b2;
        }
        final f g = this.g;
        if (b) {
            b2 = b3;
        }
        return new ja1(g.e(query, b2, c), c);
    }
    
    public IndexManager r() {
        return this.b;
    }
    
    public final Set s(final yx0 yx0) {
        final HashSet set = new HashSet();
        for (int i = 0; i < yx0.e().size(); ++i) {
            if (!((ay0)yx0.e().get(i)).a().isEmpty()) {
                set.add(((wx0)yx0.b().h().get(i)).g());
            }
        }
        return set;
    }
    
    public qo1 t() {
        return this.i.i();
    }
    
    public ByteString u() {
        return this.c.g();
    }
    
    public xk0 v() {
        return this.f;
    }
    
    public xx0 w(final int n) {
        return this.c.e(n);
    }
    
    public au1 x(final q q) {
        final Integer n = this.l.get(q);
        if (n != null) {
            return (au1)this.k.get((int)n);
        }
        return this.i.c(q);
    }
    
    public com.google.firebase.database.collection.b y(final u12 u12) {
        final List j = this.c.j();
        this.z(u12);
        this.T();
        this.U();
        final List i = this.c.j();
        com.google.firebase.database.collection.c e = du.e();
        final Iterator<List> iterator = Arrays.asList(j, i).iterator();
        while (iterator.hasNext()) {
            final Iterator iterator2 = iterator.next().iterator();
            com.google.firebase.database.collection.c c = e;
            while (true) {
                e = c;
                if (!iterator2.hasNext()) {
                    break;
                }
                final Iterator iterator3 = ((xx0)iterator2.next()).h().iterator();
                com.google.firebase.database.collection.c c2 = c;
                while (true) {
                    c = c2;
                    if (!iterator3.hasNext()) {
                        break;
                    }
                    c2 = c2.c(((wx0)iterator3.next()).g());
                }
            }
        }
        return this.f.d(e);
    }
    
    public final void z(final u12 u12) {
        final IndexManager c = this.a.c(u12);
        this.b = c;
        this.c = this.a.d(u12, c);
        final eu b = this.a.b(u12);
        this.d = b;
        this.f = new xk0(this.e, this.c, b, this.b);
        this.e.e(this.b);
        this.g.f(this.f, this.b);
    }
    
    public static class b
    {
        public au1 a;
        public int b;
    }
    
    public static class c
    {
        public final Map a;
        public final Set b;
        
        public c(final Map a, final Set b) {
            this.a = a;
            this.b = b;
        }
    }
}

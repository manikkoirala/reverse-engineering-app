// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.local;

import com.google.firebase.firestore.core.OrderBy;
import android.text.TextUtils;
import android.util.Pair;
import com.google.firebase.database.collection.b;
import com.google.firebase.firestore.util.Logger;
import com.google.firebase.firestore.core.FieldFilter;
import com.google.firebase.firestore.core.CompositeFilter;
import java.util.Collections;
import com.google.firebase.firestore.model.e;
import java.util.TreeSet;
import java.util.Arrays;
import java.util.Iterator;
import com.google.firestore.v1.Value;
import java.util.Collection;
import com.google.firebase.firestore.core.q;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.firestore.admin.v1.Index;
import com.google.firebase.firestore.model.FieldIndex;
import java.util.SortedSet;
import java.util.List;
import android.database.Cursor;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.HashMap;
import java.util.Queue;
import java.util.Map;

public final class i implements IndexManager
{
    public static final String k = "i";
    public static final byte[] l;
    public final m a;
    public final zk0 b;
    public final String c;
    public final Map d;
    public final c.a e;
    public final Map f;
    public final Queue g;
    public boolean h;
    public int i;
    public long j;
    
    static {
        l = new byte[0];
    }
    
    public i(final m a, final zk0 b, final u12 u12) {
        this.d = new HashMap();
        this.e = new c.a();
        this.f = new HashMap();
        this.g = new PriorityQueue(10, new kg1());
        this.h = false;
        this.i = -1;
        this.j = -1L;
        this.a = a;
        this.b = b;
        String a2;
        if (u12.b()) {
            a2 = u12.a();
        }
        else {
            a2 = "";
        }
        this.c = a2;
    }
    
    public final Object[] A(final FieldIndex fieldIndex, final q q, final Collection collection) {
        if (collection == null) {
            return null;
        }
        final ArrayList list = new ArrayList();
        list.add(new xe0());
        final Iterator iterator = collection.iterator();
        final Iterator iterator2 = fieldIndex.e().iterator();
        ArrayList list2 = list;
        while (iterator2.hasNext()) {
            final FieldIndex.Segment segment = (FieldIndex.Segment)iterator2.next();
            final Value value = (Value)iterator.next();
            final Iterator iterator3 = list2.iterator();
            List b = list2;
            while (true) {
                list2 = (ArrayList)b;
                if (!iterator3.hasNext()) {
                    break;
                }
                final xe0 xe0 = (xe0)iterator3.next();
                if (this.K(q, segment.d()) && a32.t(value)) {
                    b = this.B(b, segment, value);
                }
                else {
                    i40.a.e(value, xe0.b(segment.e()));
                }
            }
        }
        return this.E(list2);
    }
    
    public final List B(final List c, final FieldIndex.Segment segment, final Value value) {
        final ArrayList list = new ArrayList(c);
        final ArrayList list2 = new ArrayList();
        for (final Value value2 : value.l0().h()) {
            for (final xe0 xe0 : list) {
                final xe0 xe2 = new xe0();
                xe2.d(xe0.c());
                i40.a.e(value2, xe2.b(segment.e()));
                list2.add(xe2);
            }
        }
        return list2;
    }
    
    public final Object[] C(int i, int length, final List list, final Object[] array, final Object[] array2, final Object[] array3) {
        int size;
        if (list != null) {
            size = list.size();
        }
        else {
            size = 1;
        }
        final int n = i / size;
        final int n2 = 0;
        int length2;
        if (array3 != null) {
            length2 = array3.length;
        }
        else {
            length2 = 0;
        }
        final Object[] array4 = new Object[i * 5 + length2];
        int j = 0;
        int n3 = 0;
        while (j < i) {
            final int n4 = n3 + 1;
            array4[n3] = length;
            final int n5 = n4 + 1;
            array4[n4] = this.c;
            final int n6 = n5 + 1;
            byte[] array5;
            if (list != null) {
                array5 = this.z(list.get(j / n));
            }
            else {
                array5 = i.l;
            }
            array4[n5] = array5;
            final int n7 = n6 + 1;
            final int n8 = j % n;
            array4[n6] = array[n8];
            array4[n7] = array2[n8];
            ++j;
            n3 = n7 + 1;
        }
        if (array3 != null) {
            for (length = array3.length, i = n2; i < length; ++i, ++n3) {
                array4[n3] = array3[i];
            }
        }
        return array4;
    }
    
    public final Object[] D(final q q, final int n, final List list, final Object[] array, final String str, final Object[] array2, final String str2, final Object[] array3) {
        int size;
        if (list != null) {
            size = list.size();
        }
        else {
            size = 1;
        }
        final int n2 = Math.max(array.length, array2.length) * size;
        final StringBuilder sb = new StringBuilder();
        sb.append("SELECT document_key, directional_value FROM index_entries ");
        sb.append("WHERE index_id = ? AND uid = ? ");
        sb.append("AND array_value = ? ");
        sb.append("AND directional_value ");
        sb.append(str);
        sb.append(" ? ");
        sb.append("AND directional_value ");
        sb.append(str2);
        sb.append(" ? ");
        final StringBuilder x = o22.x(sb, n2, " UNION ");
        StringBuilder sb2;
        if (array3 != null) {
            sb2 = new StringBuilder("SELECT document_key, directional_value FROM (");
            sb2.append((CharSequence)x);
            sb2.append(") WHERE directional_value NOT IN (");
            sb2.append((CharSequence)o22.x("?", array3.length, ", "));
            sb2.append(")");
        }
        else {
            sb2 = x;
        }
        final Object[] c = this.C(n2, n, list, array, array2, array3);
        final ArrayList list2 = new ArrayList();
        list2.add(sb2.toString());
        list2.addAll(Arrays.asList(c));
        return list2.toArray();
    }
    
    public final Object[] E(final List list) {
        final Object[] array = new Object[list.size()];
        for (int i = 0; i < list.size(); ++i) {
            array[i] = ((xe0)list.get(i)).c();
        }
        return array;
    }
    
    public final SortedSet F(final du du, final FieldIndex fieldIndex) {
        final TreeSet set = new TreeSet();
        this.a.C("SELECT array_value, directional_value FROM index_entries WHERE index_id = ? AND document_key = ? AND uid = ?").b(fieldIndex.f(), du.toString(), this.c).e(new og1(set, fieldIndex, du));
        return set;
    }
    
    public final FieldIndex G(final q q) {
        g9.d(this.h, "IndexManager not started", new Object[0]);
        final e e = new e(q);
        String s;
        if (q.d() != null) {
            s = q.d();
        }
        else {
            s = q.n().g();
        }
        final Collection h = this.H(s);
        final boolean empty = h.isEmpty();
        FieldIndex fieldIndex = null;
        if (empty) {
            return null;
        }
        for (final FieldIndex fieldIndex2 : h) {
            if (e.h(fieldIndex2) && (fieldIndex == null || fieldIndex2.h().size() > fieldIndex.h().size())) {
                fieldIndex = fieldIndex2;
            }
        }
        return fieldIndex;
    }
    
    public Collection H(final String s) {
        g9.d(this.h, "IndexManager not started", new Object[0]);
        final Map map = this.f.get(s);
        Collection<Object> collection;
        if (map == null) {
            collection = Collections.emptyList();
        }
        else {
            collection = map.values();
        }
        return collection;
    }
    
    public final FieldIndex.a I(final Collection collection) {
        g9.d(collection.isEmpty() ^ true, "Found empty index group when looking for least recent index offset.", new Object[0]);
        final Iterator iterator = collection.iterator();
        Comparable c = ((FieldIndex)iterator.next()).g().c();
        int b = ((FieldIndex.a)c).h();
        while (iterator.hasNext()) {
            final FieldIndex.a c2 = ((FieldIndex)iterator.next()).g().c();
            FieldIndex.a a = (FieldIndex.a)c;
            if (c2.c((FieldIndex.a)c) < 0) {
                a = c2;
            }
            b = Math.max(c2.h(), b);
            c = a;
        }
        return FieldIndex.a.d(((FieldIndex.a)c).j(), ((FieldIndex.a)c).g(), b);
    }
    
    public final List J(final q q) {
        if (this.d.containsKey(q)) {
            return this.d.get(q);
        }
        final ArrayList list = new ArrayList();
        if (q.h().isEmpty()) {
            list.add(q);
        }
        else {
            final Iterator iterator = am0.i(new CompositeFilter(q.h(), CompositeFilter.Operator.AND)).iterator();
            while (iterator.hasNext()) {
                list.add(new q(q.n(), q.d(), ((i10)iterator.next()).b(), q.m(), q.j(), q.p(), q.f()));
            }
        }
        this.d.put(q, list);
        return list;
    }
    
    public final boolean K(final q q, final s00 s00) {
        for (final i10 i10 : q.h()) {
            if (i10 instanceof FieldFilter) {
                final FieldFilter fieldFilter = (FieldFilter)i10;
                if (!fieldFilter.f().equals(s00)) {
                    continue;
                }
                final FieldFilter.Operator g = fieldFilter.g();
                if (g.equals(FieldFilter.Operator.IN) || g.equals(FieldFilter.Operator.NOT_IN)) {
                    return true;
                }
                continue;
            }
        }
        return false;
    }
    
    public final void T(final FieldIndex fieldIndex) {
        Map map;
        if ((map = this.f.get(fieldIndex.d())) == null) {
            map = new HashMap();
            this.f.put(fieldIndex.d(), map);
        }
        final FieldIndex fieldIndex2 = (FieldIndex)map.get(fieldIndex.f());
        if (fieldIndex2 != null) {
            this.g.remove(fieldIndex2);
        }
        map.put(fieldIndex.f(), fieldIndex);
        this.g.add(fieldIndex);
        this.i = Math.max(this.i, fieldIndex.f());
        this.j = Math.max(this.j, fieldIndex.g().d());
    }
    
    public final void U(final zt zt, final SortedSet set, final SortedSet set2) {
        Logger.a(com.google.firebase.firestore.local.i.k, "Updating index entries for document '%s'", zt.getKey());
        o22.q(set, set2, new mg1(this, zt), new ng1(this, zt));
    }
    
    @Override
    public void a(ke1 ke1) {
        g9.d(this.h, "IndexManager not started", new Object[0]);
        final int l = ke1.l();
        boolean b = true;
        if (l % 2 != 1) {
            b = false;
        }
        g9.d(b, "Expected a collection path.", new Object[0]);
        if (this.e.a(ke1)) {
            final String g = ke1.g();
            ke1 = (ke1)ke1.n();
            this.a.t("INSERT OR REPLACE INTO collection_parents (collection_id, parent) VALUES (?, ?)", g, xw.c(ke1));
        }
    }
    
    @Override
    public void b(final b b) {
        g9.d(this.h, "IndexManager not started", new Object[0]);
        for (final Map.Entry<du, V> entry : b) {
            for (final FieldIndex fieldIndex : this.H(entry.getKey().j())) {
                final SortedSet f = this.F(entry.getKey(), fieldIndex);
                final SortedSet u = this.u((zt)entry.getValue(), fieldIndex);
                if (!f.equals(u)) {
                    this.U((zt)entry.getValue(), f, u);
                }
            }
        }
    }
    
    @Override
    public void c(final q q) {
        g9.d(this.h, "IndexManager not started", new Object[0]);
        for (final q q2 : this.J(q)) {
            final IndexType i = this.i(q2);
            if (i == IndexType.NONE || i == IndexType.PARTIAL) {
                final FieldIndex b = new e(q2).b();
                if (b == null) {
                    continue;
                }
                this.s(b);
            }
        }
    }
    
    @Override
    public FieldIndex.a d(final q q) {
        final ArrayList list = new ArrayList();
        final Iterator iterator = this.J(q).iterator();
        while (iterator.hasNext()) {
            final FieldIndex g = this.G((q)iterator.next());
            if (g != null) {
                list.add(g);
            }
        }
        return this.I(list);
    }
    
    @Override
    public FieldIndex.a e(final String s) {
        final Collection h = this.H(s);
        g9.d(h.isEmpty() ^ true, "minOffset was called for collection without indexes", new Object[0]);
        return this.I(h);
    }
    
    @Override
    public void f(final String s, final FieldIndex.a a) {
        g9.d(this.h, "IndexManager not started", new Object[0]);
        ++this.j;
        for (final FieldIndex fieldIndex : this.H(s)) {
            final FieldIndex b = FieldIndex.b(fieldIndex.f(), fieldIndex.d(), fieldIndex.h(), FieldIndex.b.b(this.j, a));
            this.a.t("REPLACE INTO index_state (index_id, uid,  sequence_number, read_time_seconds, read_time_nanos, document_key, largest_batch_id) VALUES(?, ?, ?, ?, ?, ?, ?)", fieldIndex.f(), this.c, this.j, a.j().c().e(), a.j().c().d(), xw.c(a.g().m()), a.h());
            this.T(b);
        }
    }
    
    @Override
    public List g(final String s) {
        g9.d(this.h, "IndexManager not started", new Object[0]);
        final ArrayList list = new ArrayList();
        this.a.C("SELECT parent FROM collection_parents WHERE collection_id = ?").b(s).e(new lg1(list));
        return list;
    }
    
    @Override
    public String h() {
        g9.d(this.h, "IndexManager not started", new Object[0]);
        final FieldIndex fieldIndex = this.g.peek();
        String d;
        if (fieldIndex != null) {
            d = fieldIndex.d();
        }
        else {
            d = null;
        }
        return d;
    }
    
    @Override
    public IndexType i(final q q) {
        IndexType indexType = IndexType.FULL;
        final List j = this.J(q);
        final Iterator iterator = j.iterator();
        Enum<IndexType> none;
        while (true) {
            none = indexType;
            if (!iterator.hasNext()) {
                break;
            }
            final q q2 = (q)iterator.next();
            final FieldIndex g = this.G(q2);
            if (g == null) {
                none = IndexType.NONE;
                break;
            }
            if (g.h().size() >= q2.o()) {
                continue;
            }
            indexType = IndexType.PARTIAL;
        }
        if (q.r() && j.size() > 1 && none == IndexType.FULL) {
            return IndexType.PARTIAL;
        }
        return (IndexType)none;
    }
    
    @Override
    public List j(final q q) {
        g9.d(this.h, "IndexManager not started", new Object[0]);
        final ArrayList list = new ArrayList();
        final ArrayList list2 = new ArrayList();
        final ArrayList list3 = new ArrayList();
        for (final q q2 : this.J(q)) {
            final FieldIndex g = this.G(q2);
            if (g == null) {
                return null;
            }
            list3.add(Pair.create((Object)q2, (Object)g));
        }
        final Iterator iterator2 = list3.iterator();
        boolean b;
        while (true) {
            final boolean hasNext = iterator2.hasNext();
            b = true;
            if (!hasNext) {
                break;
            }
            final Pair pair = (Pair)iterator2.next();
            final q q3 = (q)pair.first;
            final FieldIndex fieldIndex = (FieldIndex)pair.second;
            final List a = q3.a(fieldIndex);
            final Collection l = q3.l(fieldIndex);
            final com.google.firebase.firestore.core.c k = q3.k(fieldIndex);
            final com.google.firebase.firestore.core.c q4 = q3.q(fieldIndex);
            if (Logger.c()) {
                Logger.a(com.google.firebase.firestore.local.i.k, "Using index '%s' to execute '%s' (Arrays: %s, Lower bound: %s, Upper bound: %s)", fieldIndex, q3, a, k, q4);
            }
            final Object[] w = this.w(fieldIndex, q3, k);
            String s;
            if (k.c()) {
                s = ">=";
            }
            else {
                s = ">";
            }
            final Object[] w2 = this.w(fieldIndex, q3, q4);
            String s2;
            if (q4.c()) {
                s2 = "<=";
            }
            else {
                s2 = "<";
            }
            final Object[] d = this.D(q3, fieldIndex.f(), a, w, s, w2, s2, this.A(fieldIndex, q3, l));
            list.add(String.valueOf(d[0]));
            list2.addAll(Arrays.asList(d).subList(1, d.length));
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(TextUtils.join((CharSequence)" UNION ", (Iterable)list));
        sb.append("ORDER BY directional_value, document_key ");
        String str;
        if (q.i().equals(OrderBy.Direction.ASCENDING)) {
            str = "asc ";
        }
        else {
            str = "desc ";
        }
        sb.append(str);
        final String string = sb.toString();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("SELECT DISTINCT document_key FROM (");
        sb2.append(string);
        sb2.append(")");
        String str2 = sb2.toString();
        if (q.r()) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(str2);
            sb3.append(" LIMIT ");
            sb3.append(q.j());
            str2 = sb3.toString();
        }
        if (list2.size() >= 1000) {
            b = false;
        }
        g9.d(b, "Cannot perform query with more than 999 bind elements", new Object[0]);
        final m.d b2 = this.a.C(str2).b(list2.toArray());
        final ArrayList list4 = new ArrayList();
        b2.e(new jg1(list4));
        Logger.a(com.google.firebase.firestore.local.i.k, "Index scan returned %s documents", list4.size());
        return list4;
    }
    
    public void s(FieldIndex b) {
        g9.d(this.h, "IndexManager not started", new Object[0]);
        final int i = this.i + 1;
        b = FieldIndex.b(i, b.d(), b.h(), b.g());
        this.a.t("INSERT INTO index_configuration (index_id, collection_group, index_proto) VALUES(?, ?, ?)", i, b.d(), this.y(b));
        this.T(b);
    }
    
    @Override
    public void start() {
        final HashMap hashMap = new HashMap();
        this.a.C("SELECT index_id, sequence_number, read_time_seconds, read_time_nanos, document_key, largest_batch_id FROM index_state WHERE uid = ?").b(this.c).e(new pg1(hashMap));
        this.a.C("SELECT index_id, collection_group, index_proto FROM index_configuration").e(new qg1(this, hashMap));
        this.h = true;
    }
    
    public final void t(final zt zt, final ye0 ye0) {
        this.a.t("INSERT INTO index_entries (index_id, uid, array_value, directional_value, document_key) VALUES(?, ?, ?, ?, ?)", ye0.g(), this.c, ye0.d(), ye0.e(), zt.getKey().toString());
    }
    
    public final SortedSet u(final zt zt, final FieldIndex fieldIndex) {
        final TreeSet set = new TreeSet();
        final byte[] x = this.x(fieldIndex, zt);
        if (x == null) {
            return set;
        }
        final FieldIndex.Segment c = fieldIndex.c();
        if (c != null) {
            final Value j = zt.j(c.d());
            if (a32.t(j)) {
                final Iterator iterator = j.l0().h().iterator();
                while (iterator.hasNext()) {
                    set.add(ye0.c(fieldIndex.f(), zt.getKey(), this.z((Value)iterator.next()), x));
                }
            }
        }
        else {
            set.add(ye0.c(fieldIndex.f(), zt.getKey(), new byte[0], x));
        }
        return set;
    }
    
    public final void v(final zt zt, final ye0 ye0) {
        this.a.t("DELETE FROM index_entries WHERE index_id = ? AND uid = ? AND array_value = ? AND directional_value = ? AND document_key = ?", ye0.g(), this.c, ye0.d(), ye0.e(), zt.getKey().toString());
    }
    
    public final Object[] w(final FieldIndex fieldIndex, final q q, final com.google.firebase.firestore.core.c c) {
        return this.A(fieldIndex, q, c.b());
    }
    
    public final byte[] x(final FieldIndex fieldIndex, final zt zt) {
        final xe0 xe0 = new xe0();
        for (final FieldIndex.Segment segment : fieldIndex.e()) {
            final Value j = zt.j(segment.d());
            if (j == null) {
                return null;
            }
            i40.a.e(j, xe0.b(segment.e()));
        }
        return xe0.c();
    }
    
    public final byte[] y(final FieldIndex fieldIndex) {
        return this.b.j(fieldIndex.h()).toByteArray();
    }
    
    public final byte[] z(final Value value) {
        final xe0 xe0 = new xe0();
        i40.a.e(value, xe0.b(FieldIndex.Segment.Kind.ASCENDING));
        return xe0.c();
    }
}

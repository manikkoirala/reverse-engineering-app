// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.local;

import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteQuery;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase$CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import android.database.sqlite.SQLiteDatabaseLockedException;
import com.google.firebase.firestore.util.Logger;
import android.database.sqlite.SQLiteStatement;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import android.database.sqlite.SQLiteProgram;
import android.database.Cursor;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteTransactionListener;

public final class m extends d51
{
    public final c c;
    public final zk0 d;
    public final p e;
    public final g f;
    public final n g;
    public final j h;
    public final SQLiteTransactionListener i;
    public SQLiteDatabase j;
    public boolean k;
    
    public m(final Context context, final String s, final qp qp, final zk0 zk0, final com.google.firebase.firestore.local.b.b b) {
        this(zk0, b, new c(context, zk0, r(s, qp), null));
    }
    
    public m(final zk0 d, final com.google.firebase.firestore.local.b.b b, final c c) {
        this.i = (SQLiteTransactionListener)new SQLiteTransactionListener(this) {
            public final m a;
            
            public void onBegin() {
                m.o(this.a).f();
            }
            
            public void onCommit() {
                m.o(this.a).e();
            }
            
            public void onRollback() {
            }
        };
        this.c = c;
        this.d = d;
        this.e = new p(this, d);
        this.f = new g(this, d);
        this.g = new n(this, d);
        this.h = new j(this, b);
    }
    
    public static /* synthetic */ j o(final m m) {
        return m.h;
    }
    
    public static void q(final SQLiteProgram sqLiteProgram, final Object[] array) {
        for (int i = 0; i < array.length; ++i) {
            final Object o = array[i];
            if (o == null) {
                sqLiteProgram.bindNull(i + 1);
            }
            else if (o instanceof String) {
                sqLiteProgram.bindString(i + 1, (String)o);
            }
            else {
                int n;
                long longValue;
                if (o instanceof Integer) {
                    n = i + 1;
                    longValue = (int)o;
                }
                else if (o instanceof Long) {
                    n = i + 1;
                    longValue = (long)o;
                }
                else {
                    if (o instanceof Double) {
                        sqLiteProgram.bindDouble(i + 1, (double)o);
                        continue;
                    }
                    if (o instanceof byte[]) {
                        sqLiteProgram.bindBlob(i + 1, (byte[])o);
                        continue;
                    }
                    throw g9.a("Unknown argument %s of type %s", o, ((byte[])o).getClass());
                }
                sqLiteProgram.bindLong(n, longValue);
            }
        }
    }
    
    public static String r(String string, final qp qp) {
        try {
            final StringBuilder sb = new StringBuilder();
            sb.append("firestore.");
            sb.append(URLEncoder.encode(string, "utf-8"));
            sb.append(".");
            sb.append(URLEncoder.encode(qp.f(), "utf-8"));
            sb.append(".");
            sb.append(URLEncoder.encode(qp.e(), "utf-8"));
            string = sb.toString();
            return string;
        }
        catch (final UnsupportedEncodingException detailMessage) {
            throw new AssertionError((Object)detailMessage);
        }
    }
    
    public SQLiteStatement B(final String s) {
        return this.j.compileStatement(s);
    }
    
    public d C(final String s) {
        return new d(this.j, s);
    }
    
    @Override
    public gd a() {
        return this.f;
    }
    
    @Override
    public eu b(final u12 u12) {
        return new h(this, this.d, u12);
    }
    
    @Override
    public IndexManager c(final u12 u12) {
        return new i(this, this.d, u12);
    }
    
    @Override
    public zx0 d(final u12 u12, final IndexManager indexManager) {
        return new k(this, this.d, u12, indexManager);
    }
    
    @Override
    public l21 e() {
        return new l(this);
    }
    
    @Override
    public id1 g() {
        return this.g;
    }
    
    @Override
    public boolean i() {
        return this.k;
    }
    
    @Override
    public Object j(final String s, final hs1 hs1) {
        Logger.a(d51.a, "Starting transaction: %s", s);
        this.j.beginTransactionWithListener(this.i);
        try {
            final Object value = hs1.get();
            this.j.setTransactionSuccessful();
            return value;
        }
        finally {
            this.j.endTransaction();
        }
    }
    
    @Override
    public void k(final String s, final Runnable runnable) {
        Logger.a(d51.a, "Starting transaction: %s", s);
        this.j.beginTransactionWithListener(this.i);
        try {
            runnable.run();
            this.j.setTransactionSuccessful();
        }
        finally {
            this.j.endTransaction();
        }
    }
    
    @Override
    public void l() {
        g9.d(this.k ^ true, "SQLitePersistence double-started!", new Object[0]);
        this.k = true;
        try {
            this.j = this.c.getWritableDatabase();
            this.e.B();
            this.h.z(this.e.q());
        }
        catch (final SQLiteDatabaseLockedException cause) {
            throw new RuntimeException("Failed to gain exclusive lock to the Cloud Firestore client's offline persistence. This generally means you are using Cloud Firestore from multiple processes in your app. Keep in mind that multi-process Android apps execute the code in your Application class in all processes, so you may need to avoid initializing Cloud Firestore in your Application class. If you are intentionally using Cloud Firestore from multiple processes, you can only enable offline persistence (that is, call setPersistenceEnabled(true)) in one of them.", (Throwable)cause);
        }
    }
    
    public int s(final SQLiteStatement sqLiteStatement, final Object... array) {
        ((SQLiteProgram)sqLiteStatement).clearBindings();
        q((SQLiteProgram)sqLiteStatement, array);
        return sqLiteStatement.executeUpdateDelete();
    }
    
    public void t(final String s, final Object... array) {
        this.j.execSQL(s, array);
    }
    
    public long u() {
        return this.v() * this.w();
    }
    
    public final long v() {
        return (long)this.C("PRAGMA page_count").d(new hh1());
    }
    
    public final long w() {
        return (long)this.C("PRAGMA page_size").d(new gh1());
    }
    
    public j x() {
        return this.h;
    }
    
    public p y() {
        return this.e;
    }
    
    public static class b
    {
        public final m a;
        public final String b;
        public final String c;
        public final List d;
        public int e;
        public final Iterator f;
        
        public b(final m a, final String b, final List list, final String c) {
            this.e = 0;
            this.a = a;
            this.b = b;
            this.d = Collections.emptyList();
            this.c = c;
            this.f = list.iterator();
        }
        
        public b(final m a, final String b, final List d, final List list, final String c) {
            this.e = 0;
            this.a = a;
            this.b = b;
            this.d = d;
            this.c = c;
            this.f = list.iterator();
        }
        
        public void a() {
            ++this.e;
            final Object[] b = this.b();
            final m a = this.a;
            final StringBuilder sb = new StringBuilder();
            sb.append(this.b);
            sb.append((Object)o22.x("?", b.length, ", "));
            sb.append(this.c);
            a.t(sb.toString(), b);
        }
        
        public final Object[] b() {
            final ArrayList list = new ArrayList(this.d);
            for (int n = 0; this.f.hasNext() && n < 900 - this.d.size(); ++n) {
                list.add(this.f.next());
            }
            return list.toArray();
        }
        
        public int c() {
            return this.e;
        }
        
        public boolean d() {
            return this.f.hasNext();
        }
        
        public d e() {
            ++this.e;
            final Object[] b = this.b();
            final m a = this.a;
            final StringBuilder sb = new StringBuilder();
            sb.append(this.b);
            sb.append((Object)o22.x("?", b.length, ", "));
            sb.append(this.c);
            return a.C(sb.toString()).b(b);
        }
    }
    
    public static class c extends SQLiteOpenHelper
    {
        public final zk0 a;
        public boolean b;
        
        public c(final Context context, final zk0 zk0, final String s) {
            this(context, zk0, s, 16);
        }
        
        public c(final Context context, final zk0 a, final String s, final int n) {
            super(context, s, (SQLiteDatabase$CursorFactory)null, n);
            this.a = a;
        }
        
        public final void a(final SQLiteDatabase sqLiteDatabase) {
            if (!this.b) {
                this.onConfigure(sqLiteDatabase);
            }
        }
        
        public void onConfigure(final SQLiteDatabase sqLiteDatabase) {
            this.b = true;
            sqLiteDatabase.rawQuery("PRAGMA locking_mode = EXCLUSIVE", new String[0]).close();
        }
        
        public void onCreate(final SQLiteDatabase sqLiteDatabase) {
            this.a(sqLiteDatabase);
            new o(sqLiteDatabase, this.a).j0(0);
        }
        
        public void onDowngrade(final SQLiteDatabase sqLiteDatabase, final int n, final int n2) {
            this.a(sqLiteDatabase);
        }
        
        public void onOpen(final SQLiteDatabase sqLiteDatabase) {
            this.a(sqLiteDatabase);
        }
        
        public void onUpgrade(final SQLiteDatabase sqLiteDatabase, final int n, final int n2) {
            this.a(sqLiteDatabase);
            new o(sqLiteDatabase, this.a).j0(n);
        }
    }
    
    public static class d
    {
        public final SQLiteDatabase a;
        public final String b;
        public SQLiteDatabase$CursorFactory c;
        
        public d(final SQLiteDatabase a, final String b) {
            this.a = a;
            this.b = b;
        }
        
        public d b(final Object... array) {
            this.c = (SQLiteDatabase$CursorFactory)new ih1(array);
            return this;
        }
        
        public int c(final cl cl) {
            final Cursor h = this.h();
            try {
                if (h.moveToFirst()) {
                    cl.accept(h);
                    h.close();
                    return 1;
                }
                h.close();
                return 0;
            }
            finally {
                if (h != null) {
                    try {
                        h.close();
                    }
                    finally {
                        final Throwable exception;
                        ((Throwable)cl).addSuppressed(exception);
                    }
                }
            }
        }
        
        public Object d(final r90 r90) {
            final Cursor h = this.h();
            try {
                if (h.moveToFirst()) {
                    final Object apply = r90.apply(h);
                    h.close();
                    return apply;
                }
                h.close();
                return null;
            }
            finally {
                if (h != null) {
                    try {
                        h.close();
                    }
                    finally {
                        final Throwable exception;
                        ((Throwable)r90).addSuppressed(exception);
                    }
                }
            }
        }
        
        public int e(final cl cl) {
            final Cursor h = this.h();
            int n = 0;
            try {
                while (h.moveToNext()) {
                    ++n;
                    cl.accept(h);
                }
                h.close();
                return n;
            }
            finally {
                if (h != null) {
                    try {
                        h.close();
                    }
                    finally {
                        final Throwable exception;
                        ((Throwable)cl).addSuppressed(exception);
                    }
                }
            }
        }
        
        public boolean f() {
            final Cursor h = this.h();
            try {
                final boolean moveToFirst = h.moveToFirst();
                h.close();
                return moveToFirst ^ true;
            }
            finally {
                if (h != null) {
                    try {
                        h.close();
                    }
                    finally {
                        final Throwable t;
                        final Throwable exception;
                        t.addSuppressed(exception);
                    }
                }
            }
        }
        
        public final Cursor h() {
            final SQLiteDatabase$CursorFactory c = this.c;
            if (c != null) {
                return this.a.rawQueryWithFactory(c, this.b, (String[])null, (String)null);
            }
            return this.a.rawQuery(this.b, (String[])null);
        }
    }
}

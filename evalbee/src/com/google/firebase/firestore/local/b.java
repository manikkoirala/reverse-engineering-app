// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.local;

import java.util.PriorityQueue;
import java.util.Comparator;
import java.util.Locale;
import com.google.firebase.firestore.util.AsyncQueue;
import com.google.firebase.firestore.util.Logger;
import android.util.SparseArray;
import java.util.concurrent.TimeUnit;

public class b
{
    public static final long c;
    public static final long d;
    public final km0 a;
    public final b b;
    
    static {
        final TimeUnit minutes = TimeUnit.MINUTES;
        c = minutes.toMillis(1L);
        d = minutes.toMillis(5L);
    }
    
    public b(final km0 a, final b b) {
        this.a = a;
        this.b = b;
    }
    
    public static /* synthetic */ b b(final b b) {
        return b.b;
    }
    
    public static /* synthetic */ long c() {
        return b.d;
    }
    
    public static /* synthetic */ long d() {
        return b.c;
    }
    
    public int e(final int n) {
        return (int)(n / 100.0f * this.a.p());
    }
    
    public c f(final SparseArray sparseArray) {
        if (this.b.a == -1L) {
            Logger.a("LruGarbageCollector", "Garbage collection skipped; disabled", new Object[0]);
        }
        else {
            final long g = this.g();
            if (g >= this.b.a) {
                return this.m(sparseArray);
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Garbage collection skipped; Cache size ");
            sb.append(g);
            sb.append(" is lower than threshold ");
            sb.append(this.b.a);
            Logger.a("LruGarbageCollector", sb.toString(), new Object[0]);
        }
        return com.google.firebase.firestore.local.b.c.a();
    }
    
    public long g() {
        return this.a.j();
    }
    
    public long h(final int n) {
        if (n == 0) {
            return -1L;
        }
        final d d = new d(n);
        this.a.i(new lm0(d));
        this.a.k(new mm0(d));
        return d.c();
    }
    
    public a j(final AsyncQueue asyncQueue, final com.google.firebase.firestore.local.a a) {
        return new a(asyncQueue, a);
    }
    
    public int k(final long n) {
        return this.a.m(n);
    }
    
    public int l(final long n, final SparseArray sparseArray) {
        return this.a.n(n, sparseArray);
    }
    
    public final c m(final SparseArray sparseArray) {
        final long currentTimeMillis = System.currentTimeMillis();
        int j;
        final int i = j = this.e(this.b.b);
        if (i > this.b.c) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Capping sequence numbers to collect down to the maximum of ");
            sb.append(this.b.c);
            sb.append(" from ");
            sb.append(i);
            Logger.a("LruGarbageCollector", sb.toString(), new Object[0]);
            j = this.b.c;
        }
        final long currentTimeMillis2 = System.currentTimeMillis();
        final long h = this.h(j);
        final long currentTimeMillis3 = System.currentTimeMillis();
        final int l = this.l(h, sparseArray);
        final long currentTimeMillis4 = System.currentTimeMillis();
        final int k = this.k(h);
        final long currentTimeMillis5 = System.currentTimeMillis();
        if (Logger.c()) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("LRU Garbage Collection:\n");
            sb2.append("\tCounted targets in ");
            sb2.append(currentTimeMillis2 - currentTimeMillis);
            sb2.append("ms\n");
            final String string = sb2.toString();
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(string);
            final Locale root = Locale.ROOT;
            sb3.append(String.format(root, "\tDetermined least recently used %d sequence numbers in %dms\n", j, currentTimeMillis3 - currentTimeMillis2));
            final String string2 = sb3.toString();
            final StringBuilder sb4 = new StringBuilder();
            sb4.append(string2);
            sb4.append(String.format(root, "\tRemoved %d targets in %dms\n", l, currentTimeMillis4 - currentTimeMillis3));
            final String string3 = sb4.toString();
            final StringBuilder sb5 = new StringBuilder();
            sb5.append(string3);
            sb5.append(String.format(root, "\tRemoved %d documents in %dms\n", k, currentTimeMillis5 - currentTimeMillis4));
            final String string4 = sb5.toString();
            final StringBuilder sb6 = new StringBuilder();
            sb6.append(string4);
            sb6.append(String.format(root, "Total Duration: %dms", currentTimeMillis5 - currentTimeMillis));
            Logger.a("LruGarbageCollector", sb6.toString(), new Object[0]);
        }
        return new c(true, j, l, k);
    }
    
    public class a implements hj1
    {
        public final AsyncQueue a;
        public final com.google.firebase.firestore.local.a b;
        public boolean c;
        public AsyncQueue.b d;
        public final b e;
        
        public a(final b e, final AsyncQueue a, final com.google.firebase.firestore.local.a b) {
            this.e = e;
            this.c = false;
            this.a = a;
            this.b = b;
        }
        
        public final void c() {
            long n;
            if (this.c) {
                n = com.google.firebase.firestore.local.b.c();
            }
            else {
                n = com.google.firebase.firestore.local.b.d();
            }
            this.d = this.a.h(AsyncQueue.TimerId.GARBAGE_COLLECTION, n, new nm0(this));
        }
        
        @Override
        public void start() {
            if (com.google.firebase.firestore.local.b.b(this.e).a != -1L) {
                this.c();
            }
        }
    }
    
    public static class b
    {
        public long a;
        public int b;
        public final int c;
        
        public b(final long a, final int b, final int c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
        
        public static b a(final long n) {
            return new b(n, 10, 1000);
        }
    }
    
    public static class c
    {
        public final boolean a;
        public final int b;
        public final int c;
        public final int d;
        
        public c(final boolean a, final int b, final int c, final int d) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
        }
        
        public static c a() {
            return new c(false, 0, 0, 0);
        }
    }
    
    public static class d
    {
        public static final Comparator c;
        public final PriorityQueue a;
        public final int b;
        
        static {
            c = new om0();
        }
        
        public d(final int n) {
            this.b = n;
            this.a = new PriorityQueue(n, d.c);
        }
        
        public void b(final Long e) {
            if (this.a.size() >= this.b) {
                if (e >= this.a.peek()) {
                    return;
                }
                this.a.poll();
            }
            this.a.add(e);
        }
        
        public long c() {
            return this.a.peek();
        }
    }
}

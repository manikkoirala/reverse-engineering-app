// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.local;

import com.google.protobuf.InvalidProtocolBufferException;
import android.database.sqlite.SQLiteStatement;
import java.util.Comparator;
import java.util.Collections;
import java.util.HashSet;
import java.util.Arrays;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Set;
import java.util.List;
import android.database.Cursor;
import com.google.firebase.firestore.remote.j;
import com.google.protobuf.ByteString;

public final class k implements zx0
{
    public final m a;
    public final zk0 b;
    public final IndexManager c;
    public final String d;
    public int e;
    public ByteString f;
    
    public k(final m a, final zk0 b, final u12 u12, final IndexManager c) {
        this.a = a;
        this.b = b;
        String a2;
        if (u12.b()) {
            a2 = u12.a();
        }
        else {
            a2 = "";
        }
        this.d = a2;
        this.f = j.v;
        this.c = c;
    }
    
    public final void E() {
        final ArrayList list = new ArrayList();
        this.a.C("SELECT uid FROM mutation_queues").e(new zg1(list));
        this.e = 0;
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            this.a.C("SELECT MAX(batch_id) FROM mutations WHERE uid = ?").b(iterator.next()).e(new ah1(this));
        }
        ++this.e;
    }
    
    public final void F() {
        this.a.t("INSERT OR REPLACE INTO mutation_queues (uid, last_acknowledged_batch_id, last_stream_token) VALUES (?, ?, ?)", this.d, -1, this.f.toByteArray());
    }
    
    @Override
    public void a() {
        if (!this.u()) {
            return;
        }
        final ArrayList list = new ArrayList();
        this.a.C("SELECT path FROM document_mutations WHERE uid = ?").b(this.d).e(new ch1(list));
        g9.d(list.isEmpty(), "Document leak -- detected dangling mutation references when queue is empty. Dangling keys: %s", list);
    }
    
    @Override
    public List b(final Iterable iterable) {
        final ArrayList list = new ArrayList();
        final Iterator iterator = iterable.iterator();
        while (iterator.hasNext()) {
            list.add(xw.c(((du)iterator.next()).m()));
        }
        final m.b b = new m.b(this.a, "SELECT DISTINCT dm.batch_id, SUBSTR(m.mutations, 1, ?) FROM document_mutations dm, mutations m WHERE dm.uid = ? AND dm.path IN (", Arrays.asList(1000000, this.d), list, ") AND dm.uid = m.uid AND dm.batch_id = m.batch_id ORDER BY dm.batch_id");
        final ArrayList list2 = new ArrayList();
        final HashSet set = new HashSet();
        while (b.d()) {
            b.e().e(new ug1(this, set, list2));
        }
        if (b.c() > 1) {
            Collections.sort((List<Object>)list2, new vg1());
        }
        return list2;
    }
    
    @Override
    public void c(final xx0 xx0) {
        final SQLiteStatement b = this.a.B("DELETE FROM mutations WHERE uid = ? AND batch_id = ?");
        final SQLiteStatement b2 = this.a.B("DELETE FROM document_mutations WHERE uid = ? AND path = ? AND batch_id = ?");
        final int e = xx0.e();
        g9.d(this.a.s(b, this.d, e) != 0, "Mutation batch (%s, %d) did not exist", this.d, xx0.e());
        final Iterator iterator = xx0.h().iterator();
        while (iterator.hasNext()) {
            final du g = ((wx0)iterator.next()).g();
            this.a.s(b2, this.d, xw.c(g.m()), e);
            this.a.x().d(g);
        }
    }
    
    @Override
    public xx0 d(final pw1 pw1, final List list, final List list2) {
        final int n = this.e++;
        final xx0 xx0 = new xx0(n, pw1, list, list2);
        this.a.t("INSERT INTO mutations (uid, batch_id, mutations) VALUES (?, ?, ?)", this.d, n, this.b.m(xx0).toByteArray());
        final HashSet set = new HashSet();
        final SQLiteStatement b = this.a.B("INSERT INTO document_mutations (uid, path, batch_id) VALUES (?, ?, ?)");
        final Iterator iterator = list2.iterator();
        while (iterator.hasNext()) {
            final du g = ((wx0)iterator.next()).g();
            if (!set.add(g)) {
                continue;
            }
            this.a.s(b, this.d, xw.c(g.m()), n);
            this.c.a(g.k());
        }
        return xx0;
    }
    
    @Override
    public xx0 e(final int n) {
        return (xx0)this.a.C("SELECT batch_id, SUBSTR(mutations, 1, ?) FROM mutations WHERE uid = ? AND batch_id >= ? ORDER BY batch_id ASC LIMIT 1").b(1000000, this.d, n + 1).d(new wg1(this));
    }
    
    @Override
    public xx0 f(final int i) {
        return (xx0)this.a.C("SELECT SUBSTR(mutations, 1, ?) FROM mutations WHERE uid = ? AND batch_id = ?").b(1000000, this.d, i).d(new bh1(this, i));
    }
    
    @Override
    public ByteString g() {
        return this.f;
    }
    
    @Override
    public void h(final xx0 xx0, final ByteString byteString) {
        this.f = (ByteString)k71.b(byteString);
        this.F();
    }
    
    @Override
    public void i(final ByteString byteString) {
        this.f = (ByteString)k71.b(byteString);
        this.F();
    }
    
    @Override
    public List j() {
        final ArrayList list = new ArrayList();
        this.a.C("SELECT batch_id, SUBSTR(mutations, 1, ?) FROM mutations WHERE uid = ? ORDER BY batch_id ASC").b(1000000, this.d).e(new xg1(this, list));
        return list;
    }
    
    @Override
    public void start() {
        this.E();
        if (this.a.C("SELECT last_stream_token FROM mutation_queues WHERE uid = ?").b(this.d).c(new yg1(this)) == 0) {
            this.F();
        }
    }
    
    public final xx0 t(final int i, final byte[] array) {
        try {
            if (array.length < 1000000) {
                return this.b.e(ea2.q0(array));
            }
            final a a = new a(array);
            while (k.a.b(a)) {
                this.a.C("SELECT SUBSTR(mutations, ?, ?) FROM mutations WHERE uid = ? AND batch_id = ?").b(a.d() * 1000000 + 1, 1000000, this.d, i).c(a);
            }
            return this.b.e(ea2.p0(a.e()));
        }
        catch (final InvalidProtocolBufferException ex) {
            throw g9.a("MutationBatch failed to parse: %s", ex);
        }
    }
    
    public boolean u() {
        return this.a.C("SELECT batch_id FROM mutations WHERE uid = ? LIMIT 1").b(this.d).f();
    }
    
    public static class a implements cl
    {
        public final ArrayList a;
        public boolean b;
        
        public a(final byte[] array) {
            this.a = new ArrayList();
            this.b = true;
            this.c(array);
        }
        
        public static /* synthetic */ boolean b(final a a) {
            return a.b;
        }
        
        public void a(final Cursor cursor) {
            final byte[] blob = cursor.getBlob(0);
            this.c(blob);
            if (blob.length < 1000000) {
                this.b = false;
            }
        }
        
        public final void c(final byte[] array) {
            this.a.add(ByteString.copyFrom(array));
        }
        
        public int d() {
            return this.a.size();
        }
        
        public ByteString e() {
            return ByteString.copyFrom(this.a);
        }
    }
}

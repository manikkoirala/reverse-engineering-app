// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.local;

import java.util.HashMap;
import java.util.Map;

public final class e extends d51
{
    public final Map c;
    public final Map d;
    public final c e;
    public final iv0 f;
    public final bv0 g;
    public final hv0 h;
    public wc1 i;
    public boolean j;
    
    public e() {
        this.c = new HashMap();
        this.e = new c();
        this.f = new iv0(this);
        this.g = new bv0();
        this.h = new hv0();
        this.d = new HashMap();
    }
    
    public static e m() {
        final e e = new e();
        e.s(new dv0(e));
        return e;
    }
    
    public static e n(final b.b b, final zk0 zk0) {
        final e e = new e();
        e.s(new d(e, b, zk0));
        return e;
    }
    
    @Override
    public gd a() {
        return this.g;
    }
    
    @Override
    public eu b(final u12 u12) {
        cv0 cv0;
        if ((cv0 = this.d.get(u12)) == null) {
            cv0 = new cv0();
            this.d.put(u12, cv0);
        }
        return cv0;
    }
    
    @Override
    public zx0 d(final u12 u12, final IndexManager indexManager) {
        fv0 fv0;
        if ((fv0 = this.c.get(u12)) == null) {
            fv0 = new fv0(this, u12);
            this.c.put(u12, fv0);
        }
        return fv0;
    }
    
    @Override
    public l21 e() {
        return new gv0();
    }
    
    @Override
    public wc1 f() {
        return this.i;
    }
    
    @Override
    public boolean i() {
        return this.j;
    }
    
    @Override
    public Object j(final String s, final hs1 hs1) {
        this.i.f();
        try {
            return hs1.get();
        }
        finally {
            this.i.e();
        }
    }
    
    @Override
    public void k(final String s, final Runnable runnable) {
        this.i.f();
        try {
            runnable.run();
        }
        finally {
            this.i.e();
        }
    }
    
    @Override
    public void l() {
        g9.d(this.j ^ true, "MemoryPersistence double-started!", new Object[0]);
        this.j = true;
    }
    
    public c o(final u12 u12) {
        return this.e;
    }
    
    public Iterable p() {
        return this.c.values();
    }
    
    public hv0 q() {
        return this.h;
    }
    
    public iv0 r() {
        return this.f;
    }
    
    public final void s(final wc1 i) {
        this.i = i;
    }
}

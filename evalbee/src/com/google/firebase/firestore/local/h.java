// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.local;

import java.util.Arrays;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.firestore.v1.Write;
import java.util.List;
import java.util.ArrayList;
import java.util.SortedSet;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;
import android.database.Cursor;

public class h implements eu
{
    public final m a;
    public final zk0 b;
    public final String c;
    
    public h(final m a, final zk0 b, final u12 u12) {
        this.a = a;
        this.b = b;
        String a2;
        if (u12.b()) {
            a2 = u12.a();
        }
        else {
            a2 = "";
        }
        this.c = a2;
    }
    
    @Override
    public k21 a(final du du) {
        return (k21)this.a.C("SELECT overlay_mutation, largest_batch_id FROM document_overlays WHERE uid = ? AND collection_path = ? AND document_id = ?").b(this.c, xw.c(du.m().n()), du.m().g()).d(new dg1(this));
    }
    
    @Override
    public void b(final int i) {
        this.a.t("DELETE FROM document_overlays WHERE uid = ? AND largest_batch_id = ?", this.c, i);
    }
    
    @Override
    public Map c(final ke1 ke1, final int i) {
        final HashMap hashMap = new HashMap();
        final cb cb = new cb();
        this.a.C("SELECT overlay_mutation, largest_batch_id FROM document_overlays WHERE uid = ? AND collection_path = ? AND largest_batch_id > ?").b(this.c, xw.c(ke1), i).e(new fg1(this, cb, hashMap));
        cb.b();
        return hashMap;
    }
    
    @Override
    public Map d(final String s, final int i, final int j) {
        final HashMap hashMap = new HashMap();
        final String[] array = { null };
        final String[] array2 = { null };
        final int[] array3 = { 0 };
        final cb cb = new cb();
        this.a.C("SELECT overlay_mutation, largest_batch_id, collection_path, document_id  FROM document_overlays WHERE uid = ? AND collection_group = ? AND largest_batch_id > ? ORDER BY largest_batch_id, collection_path, document_id LIMIT ?").b(this.c, s, i, j).e(new hg1(this, array3, array, array2, cb, hashMap));
        if (array[0] == null) {
            return hashMap;
        }
        final m.d c = this.a.C("SELECT overlay_mutation, largest_batch_id FROM document_overlays WHERE uid = ? AND collection_group = ? AND (collection_path > ? OR (collection_path = ? AND document_id > ?)) AND largest_batch_id = ?");
        final String c2 = this.c;
        final String s2 = array[0];
        c.b(c2, s, s2, s2, array2[0], array3[0]).e(new ig1(this, cb, hashMap));
        cb.b();
        return hashMap;
    }
    
    @Override
    public void e(final int n, final Map map) {
        for (final Map.Entry<du, V> entry : map.entrySet()) {
            final du du = entry.getKey();
            this.v(n, du, (wx0)k71.d(entry.getValue(), "null value for key: %s", du));
        }
    }
    
    @Override
    public Map f(final SortedSet set) {
        g9.d(set.comparator() == null, "getOverlays() requires natural order", new Object[0]);
        final HashMap hashMap = new HashMap();
        final cb cb = new cb();
        ke1 b = ke1.b;
        final ArrayList list = new ArrayList();
        for (final du du : set) {
            ke1 k = b;
            if (!b.equals(du.k())) {
                this.u(hashMap, cb, b, list);
                k = du.k();
                list.clear();
            }
            list.add(du.l());
            b = k;
        }
        this.u(hashMap, cb, b, list);
        cb.b();
        return hashMap;
    }
    
    public final k21 m(final byte[] array, final int n) {
        try {
            return k21.a(n, this.b.d(Write.w0(array)));
        }
        catch (final InvalidProtocolBufferException ex) {
            throw g9.a("Overlay failed to parse: %s", ex);
        }
    }
    
    public final void t(cb b, final Map map, final Cursor cursor) {
        final byte[] blob = cursor.getBlob(0);
        final int int1 = cursor.getInt(1);
        if (cursor.isLast()) {
            b = (cb)wy.b;
        }
        b.execute(new gg1(this, blob, int1, map));
    }
    
    public final void u(final Map map, final cb cb, final ke1 ke1, final List list) {
        if (list.isEmpty()) {
            return;
        }
        final m.b b = new m.b(this.a, "SELECT overlay_mutation, largest_batch_id FROM document_overlays WHERE uid = ? AND collection_path = ? AND document_id IN (", Arrays.asList(this.c, xw.c(ke1)), list, ")");
        while (b.d()) {
            b.e().e(new eg1(this, cb, map));
        }
    }
    
    public final void v(final int i, final du du, final wx0 wx0) {
        this.a.t("INSERT OR REPLACE INTO document_overlays (uid, collection_group, collection_path, document_id, largest_batch_id, overlay_mutation) VALUES (?, ?, ?, ?, ?, ?)", this.c, du.j(), xw.c(du.m().n()), du.m().g(), i, this.b.l(wx0).toByteArray());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.local;

import android.util.SparseArray;
import java.util.Collection;
import java.util.ArrayList;
import android.database.Cursor;
import java.util.List;

public class j implements wc1, km0
{
    public final m a;
    public hk0 b;
    public long c;
    public final b d;
    public xc1 e;
    
    public j(final m a, final b.b b) {
        this.c = -1L;
        this.a = a;
        this.d = new b(this, b);
    }
    
    public final void A(final du du) {
        this.a.t("INSERT OR REPLACE INTO target_documents (target_id, path, sequence_number) VALUES (0, ?, ?)", xw.c(du.m()), this.l());
    }
    
    @Override
    public void a(final du du) {
        this.A(du);
    }
    
    @Override
    public b b() {
        return this.d;
    }
    
    @Override
    public void c(final du du) {
        this.A(du);
    }
    
    @Override
    public void d(final du du) {
        this.A(du);
    }
    
    @Override
    public void e() {
        g9.d(this.c != -1L, "Committing a transaction without having started one", new Object[0]);
        this.c = -1L;
    }
    
    @Override
    public void f() {
        g9.d(this.c == -1L, "Starting a transaction without committing the previous one", new Object[0]);
        this.c = this.b.a();
    }
    
    @Override
    public void g(au1 l) {
        l = l.l(this.l());
        this.a.y().a(l);
    }
    
    @Override
    public void h(final du du) {
        this.A(du);
    }
    
    @Override
    public void i(final cl cl) {
        this.a.y().p(cl);
    }
    
    @Override
    public long j() {
        return this.a.u();
    }
    
    @Override
    public void k(final cl cl) {
        this.a.C("select sequence_number from target_documents group by path having COUNT(*) = 1 AND target_id = 0").e(new tg1(cl));
    }
    
    @Override
    public long l() {
        g9.d(this.c != -1L, "Attempting to get a sequence number outside of a transaction", new Object[0]);
        return this.c;
    }
    
    @Override
    public int m(final long l) {
        final int[] array = { 0 };
        final ArrayList list = new ArrayList();
    Label_0014:
        while (true) {
            for (int i = 1; i != 0; i = 0) {
                if (this.a.C("select path from target_documents group by path having COUNT(*) = 1 AND target_id = 0 AND sequence_number <= ? LIMIT ?").b(l, 100).e(new sg1(this, array, list)) == 100) {
                    continue Label_0014;
                }
            }
            break;
        }
        this.a.g().removeAll(list);
        return array[0];
    }
    
    @Override
    public int n(final long n, final SparseArray sparseArray) {
        return this.a.y().y(n, sparseArray);
    }
    
    @Override
    public void o(final xc1 e) {
        this.e = e;
    }
    
    @Override
    public long p() {
        return this.a.y().r() + (long)this.a.C("SELECT COUNT(*) FROM (SELECT sequence_number FROM target_documents GROUP BY path HAVING COUNT(*) = 1 AND target_id = 0)").d(new rg1());
    }
    
    public final boolean t(final du du) {
        return this.e.c(du) || this.x(du);
    }
    
    public final boolean x(final du du) {
        return this.a.C("SELECT 1 FROM document_mutations WHERE path = ?").b(xw.c(du.m())).f() ^ true;
    }
    
    public final void y(final du du) {
        this.a.t("DELETE FROM target_documents WHERE path = ? AND target_id = 0", xw.c(du.m()));
    }
    
    public void z(final long n) {
        this.b = new hk0(n);
    }
}

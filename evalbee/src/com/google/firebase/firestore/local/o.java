// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.local;

import android.database.sqlite.SQLiteProgram;
import android.content.ContentValues;
import com.google.firebase.firestore.util.Logger;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;
import android.database.DatabaseUtils;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.firebase.firestore.proto.Target;
import com.google.protobuf.GeneratedMessageLite;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import android.database.sqlite.SQLiteDatabase;

public class o
{
    public final SQLiteDatabase a;
    public final zk0 b;
    
    public o(final SQLiteDatabase a, final zk0 b) {
        this.a = a;
        this.b = b;
    }
    
    public final void A() {
        this.N(new String[] { "document_overlays" }, new yh1(this));
    }
    
    public final void B() {
        this.N(new String[] { "mutation_queues", "mutations", "document_mutations" }, new ph1(this));
    }
    
    public final void C() {
        this.N(new String[] { "remote_documents" }, new ai1(this));
    }
    
    public final void D() {
        this.N(new String[] { "targets", "target_globals", "target_documents" }, new nh1(this));
    }
    
    public final void E() {
        this.N(new String[] { "collection_parents" }, new qh1(this));
        final rh1 rh1 = new rh1(new c.a(), this.a.compileStatement("INSERT OR REPLACE INTO collection_parents (collection_id, parent) VALUES (?, ?)"));
        new m.d(this.a, "SELECT path FROM remote_documents").e(new sh1(rh1));
        new m.d(this.a, "SELECT path FROM document_mutations").e(new th1(rh1));
    }
    
    public final void F() {
        new m.d(this.a, "SELECT target_id, target_proto FROM targets").e(new di1(this));
    }
    
    public final void G() {
        if (this.m0("targets")) {
            this.a.execSQL("DROP TABLE targets");
        }
        if (this.m0("target_globals")) {
            this.a.execSQL("DROP TABLE target_globals");
        }
        if (this.m0("target_documents")) {
            this.a.execSQL("DROP TABLE target_documents");
        }
    }
    
    public final void H() {
        final m.d b = new m.d(this.a, "SELECT path FROM remote_documents WHERE path_length IS NULL LIMIT ?").b(100);
        final SQLiteStatement compileStatement = this.a.compileStatement("UPDATE remote_documents SET path_length = ? WHERE path = ?");
        final boolean[] array = { false };
        do {
            array[0] = false;
            b.e(new uh1(array, compileStatement));
        } while (array[0]);
    }
    
    public final void I() {
        this.a.execSQL("UPDATE remote_documents SET read_time_seconds = 0, read_time_nanos = 0 WHERE read_time_seconds IS NULL");
    }
    
    public final void J() {
        final Long n = (Long)new m.d(this.a, "SELECT highest_listen_sequence_number FROM target_globals LIMIT 1").d(new ei1());
        g9.d(n != null, "Missing highest sequence number", new Object[0]);
        final long longValue = n;
        final SQLiteStatement compileStatement = this.a.compileStatement("INSERT INTO target_documents (target_id, path, sequence_number) VALUES (0, ?, ?)");
        final m.d b = new m.d(this.a, "SELECT RD.path FROM remote_documents AS RD WHERE NOT EXISTS (SELECT TD.path FROM target_documents AS TD WHERE RD.path = TD.path AND TD.target_id = 0) LIMIT ?").b(100);
        final boolean[] array = { false };
        do {
            array[0] = false;
            b.e(new oh1(array, compileStatement, longValue));
        } while (array[0]);
    }
    
    public final void K() {
        if (DatabaseUtils.queryNumEntries(this.a, "target_globals") != 1L) {
            this.a.execSQL("INSERT INTO target_globals (highest_target_id, highest_listen_sequence_number, last_remote_snapshot_version_seconds, last_remote_snapshot_version_nanos) VALUES (?, ?, ?, ?)", (Object[])new String[] { "0", "0", "0", "0" });
        }
    }
    
    public List L(final String str) {
        final ArrayList list = new ArrayList();
        Cursor rawQuery;
        final Cursor cursor = rawQuery = null;
        try {
            final SQLiteDatabase a = this.a;
            rawQuery = cursor;
            rawQuery = cursor;
            final StringBuilder sb = new StringBuilder();
            rawQuery = cursor;
            sb.append("PRAGMA table_info(");
            rawQuery = cursor;
            sb.append(str);
            rawQuery = cursor;
            sb.append(")");
            rawQuery = cursor;
            final Cursor cursor2 = rawQuery = a.rawQuery(sb.toString(), (String[])null);
            final int columnIndex = cursor2.getColumnIndex("name");
            while (true) {
                rawQuery = cursor2;
                if (!cursor2.moveToNext()) {
                    break;
                }
                rawQuery = cursor2;
                list.add(cursor2.getString(columnIndex));
            }
            cursor2.close();
            return list;
        }
        finally {
            if (rawQuery != null) {
                rawQuery.close();
            }
        }
    }
    
    public final boolean M() {
        final boolean l0 = this.l0("remote_documents", "read_time_seconds");
        final boolean l2 = this.l0("remote_documents", "read_time_nanos");
        final boolean b = true;
        g9.d(l0 == l2, "Table contained just one of read_time_seconds or read_time_nanos", new Object[0]);
        return l0 && l2 && b;
    }
    
    public final void N(final String[] array, final Runnable runnable) {
        final StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(TextUtils.join((CharSequence)", ", (Object[])array));
        sb.append("]");
        final String string = sb.toString();
        int i = 0;
        int n = 0;
        while (i < array.length) {
            final String s = array[i];
            final boolean m0 = this.m0(s);
            boolean b;
            if (i == 0) {
                b = m0;
            }
            else if (m0 != (b = (n != 0))) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Expected all of ");
                sb2.append(string);
                sb2.append(" to either exist or not, but ");
                final String string2 = sb2.toString();
                StringBuilder sb3;
                String str;
                if (n != 0) {
                    sb3 = new StringBuilder();
                    sb3.append(string2);
                    sb3.append(array[0]);
                    sb3.append(" exists and ");
                    sb3.append(s);
                    str = " does not";
                }
                else {
                    sb3 = new StringBuilder();
                    sb3.append(string2);
                    sb3.append(array[0]);
                    sb3.append(" does not exist and ");
                    sb3.append(s);
                    str = " does";
                }
                sb3.append(str);
                throw new IllegalStateException(sb3.toString());
            }
            ++i;
            n = (b ? 1 : 0);
        }
        if (n == 0) {
            runnable.run();
        }
        else {
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("Skipping migration because all of ");
            sb4.append(string);
            sb4.append(" already exist");
            Logger.a("SQLiteSchema", sb4.toString(), new Object[0]);
        }
    }
    
    public final void g0() {
        new m.d(this.a, "SELECT uid, last_acknowledged_batch_id FROM mutation_queues").e(new bi1(this));
    }
    
    public final void h0(final String s, final int n) {
        final SQLiteStatement compileStatement = this.a.compileStatement("DELETE FROM mutations WHERE uid = ? AND batch_id = ?");
        boolean b = true;
        ((SQLiteProgram)compileStatement).bindString(1, s);
        ((SQLiteProgram)compileStatement).bindLong(2, (long)n);
        if (compileStatement.executeUpdateDelete() == 0) {
            b = false;
        }
        g9.d(b, "Mutation batch (%s, %d) did not exist", s, n);
        this.a.execSQL("DELETE FROM document_mutations WHERE uid = ? AND batch_id = ?", new Object[] { s, n });
    }
    
    public final void i0() {
        new m.d(this.a, "SELECT target_id, target_proto FROM targets").e(new wh1(this));
    }
    
    public void j0(final int n) {
        this.k0(n, 16);
    }
    
    public void k0(final int i, final int j) {
        final long currentTimeMillis = System.currentTimeMillis();
        if (i < 1 && j >= 1) {
            this.B();
            this.D();
            this.C();
        }
        if (i < 3 && j >= 3 && i != 0) {
            this.G();
            this.D();
        }
        if (i < 4 && j >= 4) {
            this.K();
            this.w();
        }
        if (i < 5 && j >= 5) {
            this.v();
        }
        if (i < 6 && j >= 6) {
            this.g0();
        }
        if (i < 7 && j >= 7) {
            this.J();
        }
        if (i < 8 && j >= 8) {
            this.E();
        }
        if (i < 9 && j >= 9) {
            if (!this.M()) {
                this.u();
            }
            else {
                this.F();
            }
        }
        if (i == 9 && j >= 10) {
            this.F();
        }
        if (i < 11 && j >= 11) {
            this.i0();
        }
        if (i < 12 && j >= 12) {
            this.x();
        }
        if (i < 13 && j >= 13) {
            this.s();
            this.H();
        }
        if (i < 14 && j >= 14) {
            this.A();
            this.y();
            this.t(d51.b);
        }
        if (i < 15 && j >= 15) {
            this.I();
        }
        if (i < 16 && j >= 16) {
            this.z();
        }
        Logger.a("SQLiteSchema", "Migration from version %s to %s took %s milliseconds", i, j, System.currentTimeMillis() - currentTimeMillis);
    }
    
    public final boolean l0(final String s, final String s2) {
        return this.L(s).indexOf(s2) != -1;
    }
    
    public final boolean m0(final String s) {
        return new m.d(this.a, "SELECT 1=1 FROM sqlite_master WHERE tbl_name = ?").b(s).f() ^ true;
    }
    
    public final void s() {
        if (!this.l0("remote_documents", "path_length")) {
            this.a.execSQL("ALTER TABLE remote_documents ADD COLUMN path_length INTEGER");
        }
    }
    
    public final void t(final String s) {
        this.a.execSQL("INSERT OR IGNORE INTO data_migrations (migration_name) VALUES (?)", (Object[])new String[] { s });
    }
    
    public final void u() {
        this.a.execSQL("ALTER TABLE remote_documents ADD COLUMN read_time_seconds INTEGER");
        this.a.execSQL("ALTER TABLE remote_documents ADD COLUMN read_time_nanos INTEGER");
    }
    
    public final void v() {
        if (!this.l0("target_documents", "sequence_number")) {
            this.a.execSQL("ALTER TABLE target_documents ADD COLUMN sequence_number INTEGER");
        }
    }
    
    public final void w() {
        if (!this.l0("target_globals", "target_count")) {
            this.a.execSQL("ALTER TABLE target_globals ADD COLUMN target_count INTEGER");
        }
        final long queryNumEntries = DatabaseUtils.queryNumEntries(this.a, "targets");
        final ContentValues contentValues = new ContentValues();
        contentValues.put("target_count", Long.valueOf(queryNumEntries));
        this.a.update("target_globals", contentValues, (String)null, (String[])null);
    }
    
    public final void x() {
        this.N(new String[] { "bundles", "named_queries" }, new ci1(this));
    }
    
    public final void y() {
        this.N(new String[] { "data_migrations" }, new xh1(this));
    }
    
    public final void z() {
        this.N(new String[] { "index_configuration", "index_state", "index_entries" }, new zh1(this));
    }
}

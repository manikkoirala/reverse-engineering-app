// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.local;

import com.google.firebase.firestore.core.q;
import com.google.firebase.firestore.util.Logger;
import java.util.Map;
import java.util.List;
import java.util.Collections;
import com.google.firebase.database.collection.c;
import java.util.Iterator;
import com.google.firebase.database.collection.b;
import com.google.firebase.firestore.model.FieldIndex;
import com.google.firebase.firestore.core.Query;

public class f
{
    public xk0 a;
    public IndexManager b;
    public boolean c;
    public boolean d;
    public int e;
    public double f;
    
    public f() {
        this.d = false;
        this.e = 100;
        this.f = 2.0;
    }
    
    public final b a(final Iterable iterable, final Query query, final FieldIndex.a a) {
        final b h = this.a.h(query, a);
        final Iterator iterator = iterable.iterator();
        b l = h;
        while (iterator.hasNext()) {
            final zt zt = (zt)iterator.next();
            l = l.l(zt.getKey(), zt);
        }
        return l;
    }
    
    public final c b(final Query query, final b b) {
        final c c = new c(Collections.emptyList(), query.c());
        final Iterator iterator = b.iterator();
        c c2 = c;
        while (iterator.hasNext()) {
            final zt zt = iterator.next().getValue();
            if (query.r(zt)) {
                c2 = c2.c(zt);
            }
        }
        return c2;
    }
    
    public final void c(final Query query, final ga1 ga1, final int i) {
        if (ga1.a() < this.e) {
            Logger.a("QueryEngine", "SDK will not create cache indexes for query: %s, since it only creates cache indexes for collection contains more than or equal to %s documents.", query.toString(), this.e);
            return;
        }
        Logger.a("QueryEngine", "Query: %s, scans %s local documents and returns %s documents as results.", query.toString(), ga1.a(), i);
        if (ga1.a() > this.f * i) {
            this.b.c(query.x());
            Logger.a("QueryEngine", "The SDK decides to create cache indexes for query: %s, as using cache indexes may help improve performance.", query.toString());
        }
    }
    
    public final b d(final Query query, final ga1 ga1) {
        if (Logger.c()) {
            Logger.a("QueryEngine", "Using full collection scan to execute query: %s", query.toString());
        }
        return this.a.i(query, FieldIndex.a.a, ga1);
    }
    
    public b e(final Query query, final qo1 qo1, final c c) {
        g9.d(this.c, "initialize() not called", new Object[0]);
        final b h = this.h(query);
        if (h != null) {
            return h;
        }
        final b i = this.i(query, c, qo1);
        if (i != null) {
            return i;
        }
        final ga1 ga1 = new ga1();
        final b d = this.d(query, ga1);
        if (d != null && this.d) {
            this.c(query, ga1, d.size());
        }
        return d;
    }
    
    public void f(final xk0 a, final IndexManager b) {
        this.a = a;
        this.b = b;
        this.c = true;
    }
    
    public final boolean g(final Query query, final int n, final c c, final qo1 qo1) {
        final boolean n2 = query.n();
        boolean b = false;
        if (!n2) {
            return false;
        }
        if (n != c.size()) {
            return true;
        }
        Object o;
        if (query.j() == Query.LimitType.LIMIT_TO_FIRST) {
            o = c.a();
        }
        else {
            o = c.b();
        }
        final zt zt = (zt)o;
        if (zt == null) {
            return false;
        }
        if (zt.b() || zt.getVersion().a(qo1) > 0) {
            b = true;
        }
        return b;
    }
    
    public final b h(final Query query) {
        if (query.s()) {
            return null;
        }
        final q x = query.x();
        final IndexManager.IndexType i = this.b.i(x);
        if (i.equals(IndexManager.IndexType.NONE)) {
            return null;
        }
        if (!query.n() || !i.equals(IndexManager.IndexType.PARTIAL)) {
            final List j = this.b.j(x);
            g9.d(j != null, "index manager must return results for partial and full indexes.", new Object[0]);
            final b d = this.a.d(j);
            final FieldIndex.a d2 = this.b.d(x);
            final c b = this.b(query, d);
            if (!this.g(query, j.size(), b, d2.j())) {
                return this.a(b, query, d2);
            }
        }
        return this.h(query.q(-1L));
    }
    
    public final b i(final Query query, final c c, final qo1 qo1) {
        if (query.s()) {
            return null;
        }
        if (qo1.equals(qo1.b)) {
            return null;
        }
        final c b = this.b(query, this.a.d(c));
        if (this.g(query, c.size(), b, qo1)) {
            return null;
        }
        if (Logger.c()) {
            Logger.a("QueryEngine", "Re-using previous result from %s to execute query: %s", qo1.toString(), query.toString());
        }
        return this.a(b, query, FieldIndex.a.e(qo1, -1));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.local;

import java.util.List;
import com.google.firebase.firestore.model.FieldIndex;
import com.google.firebase.firestore.core.q;
import com.google.firebase.database.collection.b;

public interface IndexManager
{
    void a(final ke1 p0);
    
    void b(final b p0);
    
    void c(final q p0);
    
    FieldIndex.a d(final q p0);
    
    FieldIndex.a e(final String p0);
    
    void f(final String p0, final FieldIndex.a p1);
    
    List g(final String p0);
    
    String h();
    
    IndexType i(final q p0);
    
    List j(final q p0);
    
    void start();
    
    public enum IndexType
    {
        private static final IndexType[] $VALUES;
        
        FULL, 
        NONE, 
        PARTIAL;
    }
}

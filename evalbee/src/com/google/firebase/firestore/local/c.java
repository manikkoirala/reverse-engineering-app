// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.local;

import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.HashMap;
import java.util.List;
import com.google.firebase.firestore.model.FieldIndex;
import com.google.firebase.firestore.core.q;
import com.google.firebase.database.collection.b;

public class c implements IndexManager
{
    public final a a;
    
    public c() {
        this.a = new a();
    }
    
    @Override
    public void a(final ke1 ke1) {
        this.a.a(ke1);
    }
    
    @Override
    public void b(final b b) {
    }
    
    @Override
    public void c(final q q) {
    }
    
    @Override
    public FieldIndex.a d(final q q) {
        return FieldIndex.a.a;
    }
    
    @Override
    public FieldIndex.a e(final String s) {
        return FieldIndex.a.a;
    }
    
    @Override
    public void f(final String s, final FieldIndex.a a) {
    }
    
    @Override
    public List g(final String s) {
        return this.a.b(s);
    }
    
    @Override
    public String h() {
        return null;
    }
    
    @Override
    public IndexType i(final q q) {
        return IndexType.NONE;
    }
    
    @Override
    public List j(final q q) {
        return null;
    }
    
    @Override
    public void start() {
    }
    
    public static class a
    {
        public final HashMap a;
        
        public a() {
            this.a = new HashMap();
        }
        
        public boolean a(final ke1 ke1) {
            final int l = ke1.l();
            boolean b = true;
            if (l % 2 != 1) {
                b = false;
            }
            g9.d(b, "Expected a collection path.", new Object[0]);
            final String g = ke1.g();
            final ke1 e = (ke1)ke1.n();
            HashSet value;
            if ((value = this.a.get(g)) == null) {
                value = new HashSet();
                this.a.put(g, value);
            }
            return value.add(e);
        }
        
        public List b(final String key) {
            final HashSet c = this.a.get(key);
            List<Object> emptyList;
            if (c != null) {
                emptyList = new ArrayList<Object>(c);
            }
            else {
                emptyList = Collections.emptyList();
            }
            return emptyList;
        }
    }
}

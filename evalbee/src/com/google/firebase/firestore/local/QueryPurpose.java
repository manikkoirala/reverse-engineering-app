// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.local;

public enum QueryPurpose
{
    private static final QueryPurpose[] $VALUES;
    
    EXISTENCE_FILTER_MISMATCH, 
    EXISTENCE_FILTER_MISMATCH_BLOOM, 
    LIMBO_RESOLUTION, 
    LISTEN;
}

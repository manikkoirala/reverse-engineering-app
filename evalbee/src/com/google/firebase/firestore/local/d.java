// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.local;

import android.util.SparseArray;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;

public class d implements wc1, km0
{
    public final e a;
    public final zk0 b;
    public final Map c;
    public xc1 d;
    public final b e;
    public final hk0 f;
    public long g;
    
    public d(final e a, final b.b b, final zk0 b2) {
        this.a = a;
        this.b = b2;
        this.c = new HashMap();
        this.f = new hk0(a.r().m());
        this.g = -1L;
        this.e = new b(this, b);
    }
    
    @Override
    public void a(final du du) {
        this.c.put(du, this.l());
    }
    
    @Override
    public b b() {
        return this.e;
    }
    
    @Override
    public void c(final du du) {
        this.c.put(du, this.l());
    }
    
    @Override
    public void d(final du du) {
        this.c.put(du, this.l());
    }
    
    @Override
    public void e() {
        g9.d(this.g != -1L, "Committing a transaction without having started one", new Object[0]);
        this.g = -1L;
    }
    
    @Override
    public void f() {
        g9.d(this.g == -1L, "Starting a transaction without committing the previous one", new Object[0]);
        this.g = this.f.a();
    }
    
    @Override
    public void g(au1 l) {
        l = l.l(this.l());
        this.a.r().a(l);
    }
    
    @Override
    public void h(final du du) {
        this.c.put(du, this.l());
    }
    
    @Override
    public void i(final cl cl) {
        this.a.r().k(cl);
    }
    
    @Override
    public long j() {
        long n = this.a.r().l(this.b) + 0L + this.a.q().g(this.b);
        final Iterator iterator = this.a.p().iterator();
        while (iterator.hasNext()) {
            n += ((fv0)iterator.next()).l(this.b);
        }
        return n;
    }
    
    @Override
    public void k(final cl cl) {
        for (final Map.Entry<du, V> entry : this.c.entrySet()) {
            if (!this.r(entry.getKey(), (long)entry.getValue())) {
                cl.accept(entry.getValue());
            }
        }
    }
    
    @Override
    public long l() {
        g9.d(this.g != -1L, "Attempting to get a sequence number outside of a transaction", new Object[0]);
        return this.g;
    }
    
    @Override
    public int m(final long n) {
        final hv0 q = this.a.q();
        final ArrayList list = new ArrayList();
        final Iterator iterator = q.h().iterator();
        while (iterator.hasNext()) {
            final du key = ((zt)iterator.next()).getKey();
            if (!this.r(key, n)) {
                list.add(key);
                this.c.remove(key);
            }
        }
        q.removeAll(list);
        return list.size();
    }
    
    @Override
    public int n(final long n, final SparseArray sparseArray) {
        return this.a.r().p(n, sparseArray);
    }
    
    @Override
    public void o(final xc1 d) {
        this.d = d;
    }
    
    @Override
    public long p() {
        final long n = this.a.r().n();
        final long[] array = { 0L };
        this.k(new ev0(array));
        return n + array[0];
    }
    
    public final boolean r(final du du, final long n) {
        final boolean t = this.t(du);
        boolean b = true;
        if (t) {
            return true;
        }
        if (this.d.c(du)) {
            return true;
        }
        if (this.a.r().j(du)) {
            return true;
        }
        final Long n2 = this.c.get(du);
        if (n2 == null || n2 <= n) {
            b = false;
        }
        return b;
    }
    
    public final boolean t(final du du) {
        final Iterator iterator = this.a.p().iterator();
        while (iterator.hasNext()) {
            if (((fv0)iterator.next()).k(du)) {
                return true;
            }
        }
        return false;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.local;

import com.google.firebase.database.collection.b;
import java.util.Collection;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.firebase.firestore.proto.MaybeDocument;
import java.util.Iterator;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import com.google.firebase.firestore.model.FieldIndex;
import com.google.firebase.firestore.model.MutableDocument;
import java.util.Set;
import com.google.firebase.firestore.core.Query;
import android.database.Cursor;
import java.util.Map;

public final class n implements id1
{
    public final m a;
    public final zk0 b;
    public IndexManager c;
    
    public n(final m a, final zk0 b) {
        this.a = a;
        this.b = b;
    }
    
    @Override
    public Map a(final Query query, final FieldIndex.a a, final Set set, final ga1 ga1) {
        return this.l(Collections.singletonList(query.l()), a, Integer.MAX_VALUE, new jh1(query, set), ga1);
    }
    
    @Override
    public Map b(final String s, final FieldIndex.a a, final int n) {
        final List g = this.c.g(s);
        final ArrayList list = new ArrayList(g.size());
        final Iterator iterator = g.iterator();
        while (iterator.hasNext()) {
            list.add((Object)((ke1)iterator.next()).c(s));
        }
        if (list.isEmpty()) {
            return Collections.emptyMap();
        }
        if (list.size() * 9 < 900) {
            return this.k(list, a, n, null);
        }
        final HashMap hashMap = new HashMap();
        int b;
        for (int i = 0; i < list.size(); i = b) {
            final int size = list.size();
            b = i + 100;
            hashMap.putAll(this.k(list.subList(i, Math.min(size, b)), a, n, null));
        }
        return o22.s(hashMap, n, FieldIndex.a.b);
    }
    
    @Override
    public void c(final MutableDocument mutableDocument, final qo1 qo1) {
        g9.d(qo1.equals(qo1.b) ^ true, "Cannot add document to the RemoteDocumentCache with a read time of zero", new Object[0]);
        final du key = mutableDocument.getKey();
        final pw1 c = qo1.c();
        this.a.t("INSERT OR REPLACE INTO remote_documents (path, path_length, read_time_seconds, read_time_nanos, contents) VALUES (?, ?, ?, ?, ?)", xw.c(key.m()), key.m().l(), c.e(), c.d(), this.b.k(mutableDocument).toByteArray());
        this.c.a(mutableDocument.getKey().k());
    }
    
    @Override
    public MutableDocument d(final du o) {
        return this.getAll(Collections.singletonList(o)).get(o);
    }
    
    @Override
    public void e(final IndexManager c) {
        this.c = c;
    }
    
    @Override
    public Map getAll(final Iterable iterable) {
        final HashMap hashMap = new HashMap();
        final ArrayList list = new ArrayList();
        for (final du du : iterable) {
            list.add(xw.c(du.m()));
            hashMap.put(du, MutableDocument.q(du));
        }
        final m.b b = new m.b(this.a, "SELECT contents, read_time_seconds, read_time_nanos FROM remote_documents WHERE path IN (", list, ") ORDER BY path");
        final cb cb = new cb();
        while (b.d()) {
            b.e().e(new kh1(this, cb, hashMap));
        }
        cb.b();
        return hashMap;
    }
    
    public final MutableDocument j(final byte[] array, final int n, final int n2) {
        try {
            return this.b.c(MaybeDocument.k0(array)).v(new qo1(new pw1(n, n2)));
        }
        catch (final InvalidProtocolBufferException ex) {
            throw g9.a("MaybeDocument failed to parse: %s", ex);
        }
    }
    
    public final Map k(final List list, final FieldIndex.a a, final int n, final r90 r90) {
        return this.l(list, a, n, r90, null);
    }
    
    public final Map l(final List list, final FieldIndex.a a, final int i, final r90 r90, final ga1 ga1) {
        final pw1 c = a.j().c();
        final du g = a.g();
        final StringBuilder x = o22.x("SELECT contents, read_time_seconds, read_time_nanos, path FROM remote_documents WHERE path >= ? AND path < ? AND path_length = ? AND (read_time_seconds > ? OR ( read_time_seconds = ? AND read_time_nanos > ?) OR ( read_time_seconds = ? AND read_time_nanos = ? and path > ?)) ", list.size(), " UNION ");
        x.append("ORDER BY read_time_seconds, read_time_nanos, path LIMIT ?");
        final Object[] array = new Object[list.size() * 9 + 1];
        final Iterator iterator = list.iterator();
        int n = 0;
        while (iterator.hasNext()) {
            final ke1 ke1 = (ke1)iterator.next();
            final String c2 = xw.c(ke1);
            final int n2 = n + 1;
            array[n] = c2;
            final int n3 = n2 + 1;
            array[n2] = xw.f(c2);
            final int n4 = n3 + 1;
            array[n3] = ke1.l() + 1;
            final int n5 = n4 + 1;
            array[n4] = c.e();
            final int n6 = n5 + 1;
            array[n5] = c.e();
            final int n7 = n6 + 1;
            array[n6] = c.d();
            final int n8 = n7 + 1;
            array[n7] = c.e();
            n = n8 + 1;
            array[n8] = c.d();
            array[n] = xw.c(g.m());
            ++n;
        }
        array[n] = i;
        final cb cb = new cb();
        final HashMap hashMap = new HashMap();
        this.a.C(x.toString()).b(array).e(new lh1(this, cb, hashMap, r90, ga1));
        cb.b();
        return hashMap;
    }
    
    public final void q(cb b, final Map map, final Cursor cursor, final r90 r90) {
        final byte[] blob = cursor.getBlob(0);
        final int int1 = cursor.getInt(1);
        final int int2 = cursor.getInt(2);
        if (cursor.isLast()) {
            b = (cb)wy.b;
        }
        b.execute(new mh1(this, blob, int1, int2, r90, map));
    }
    
    @Override
    public void removeAll(final Collection collection) {
        if (collection.isEmpty()) {
            return;
        }
        final ArrayList list = new ArrayList();
        final b a = au.a();
        final Iterator iterator = collection.iterator();
        b l = a;
        while (iterator.hasNext()) {
            final du du = (du)iterator.next();
            list.add(xw.c(du.m()));
            l = l.l(du, MutableDocument.r(du, qo1.b));
        }
        final m.b b = new m.b(this.a, "DELETE FROM remote_documents WHERE path IN (", list, ")");
        while (b.d()) {
            b.a();
        }
        this.c.b(l);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.local;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.firebase.firestore.proto.Target;
import java.util.Iterator;
import android.database.sqlite.SQLiteStatement;
import com.google.firebase.database.collection.c;
import android.util.SparseArray;
import com.google.firebase.firestore.core.q;
import android.database.Cursor;

public final class p implements yt1
{
    public final m a;
    public final zk0 b;
    public int c;
    public long d;
    public qo1 e;
    public long f;
    
    public p(final m a, final zk0 b) {
        this.e = qo1.b;
        this.a = a;
        this.b = b;
    }
    
    public final void A(final au1 au1) {
        final int h = au1.h();
        final String c = au1.g().c();
        final pw1 c2 = au1.f().c();
        this.a.t("INSERT OR REPLACE INTO targets (target_id, canonical_id, snapshot_version_seconds, snapshot_version_nanos, resume_token, last_listen_sequence_number, target_proto) VALUES (?, ?, ?, ?, ?, ?, ?)", h, c, c2.e(), c2.d(), au1.d().toByteArray(), au1.e(), this.b.o(au1).toByteArray());
    }
    
    public void B() {
        final int c = this.a.C("SELECT highest_target_id, highest_listen_sequence_number, last_remote_snapshot_version_seconds, last_remote_snapshot_version_nanos, target_count FROM target_globals LIMIT 1").c(new gi1(this));
        boolean b = true;
        if (c != 1) {
            b = false;
        }
        g9.d(b, "Missing target_globals entry", new Object[0]);
    }
    
    public final boolean C(final au1 au1) {
        final int h = au1.h();
        final int c = this.c;
        final boolean b = true;
        boolean b2;
        if (h > c) {
            this.c = au1.h();
            b2 = true;
        }
        else {
            b2 = false;
        }
        if (au1.e() > this.d) {
            this.d = au1.e();
            b2 = b;
        }
        return b2;
    }
    
    public final void D() {
        this.a.t("UPDATE target_globals SET highest_target_id = ?, highest_listen_sequence_number = ?, last_remote_snapshot_version_seconds = ?, last_remote_snapshot_version_nanos = ?, target_count = ?", this.c, this.d, this.e.c().e(), this.e.c().d(), this.f);
    }
    
    @Override
    public void a(final au1 au1) {
        this.A(au1);
        if (this.C(au1)) {
            this.D();
        }
    }
    
    @Override
    public void b(final com.google.firebase.database.collection.c c, final int i) {
        final SQLiteStatement b = this.a.B("DELETE FROM target_documents WHERE target_id = ? AND path = ?");
        final j x = this.a.x();
        for (final du du : c) {
            this.a.s(b, i, xw.c(du.m()));
            x.a(du);
        }
    }
    
    @Override
    public au1 c(final q q) {
        final String c = q.c();
        final c c2 = new c(null);
        this.a.C("SELECT target_proto FROM targets WHERE canonical_id = ?").b(c).e(new fi1(this, q, c2));
        return c2.a;
    }
    
    @Override
    public int d() {
        return this.c;
    }
    
    @Override
    public void e(final au1 au1) {
        this.A(au1);
        this.C(au1);
        ++this.f;
        this.D();
    }
    
    @Override
    public void f(final com.google.firebase.database.collection.c c, final int i) {
        final SQLiteStatement b = this.a.B("INSERT OR IGNORE INTO target_documents (target_id, path) VALUES (?, ?)");
        final j x = this.a.x();
        for (final du du : c) {
            this.a.s(b, i, xw.c(du.m()));
            x.c(du);
        }
    }
    
    @Override
    public void g(final qo1 e) {
        this.e = e;
        this.D();
    }
    
    @Override
    public com.google.firebase.database.collection.c h(final int i) {
        final b b = new b(null);
        this.a.C("SELECT path FROM target_documents WHERE target_id = ?").b(i).e(new hi1(b));
        return b.a;
    }
    
    @Override
    public qo1 i() {
        return this.e;
    }
    
    public final au1 o(final byte[] array) {
        try {
            return this.b.g(Target.s0(array));
        }
        catch (final InvalidProtocolBufferException ex) {
            throw g9.a("TargetData failed to parse: %s", ex);
        }
    }
    
    public void p(final cl cl) {
        this.a.C("SELECT target_proto FROM targets").e(new ji1(this, cl));
    }
    
    public long q() {
        return this.d;
    }
    
    public long r() {
        return this.f;
    }
    
    public void x(final int i) {
        this.a.t("DELETE FROM target_documents WHERE target_id = ?", i);
    }
    
    public int y(final long l, final SparseArray sparseArray) {
        final int[] array = { 0 };
        this.a.C("SELECT target_id FROM targets WHERE last_listen_sequence_number <= ?").b(l).e(new ii1(this, sparseArray, array));
        this.D();
        return array[0];
    }
    
    public final void z(final int i) {
        this.x(i);
        this.a.t("DELETE FROM targets WHERE target_id = ?", i);
        --this.f;
    }
    
    public static class b
    {
        public com.google.firebase.database.collection.c a;
        
        public b() {
            this.a = du.e();
        }
    }
    
    public static class c
    {
        public au1 a;
    }
}

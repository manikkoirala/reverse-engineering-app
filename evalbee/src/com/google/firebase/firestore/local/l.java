// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore.local;

import java.util.Iterator;
import java.util.Collection;
import java.util.HashSet;
import android.database.Cursor;
import java.util.Set;

public class l implements l21
{
    public final m a;
    
    public l(final m a) {
        this.a = a;
    }
    
    public final void d() {
        this.a.k("build overlays", new dh1(this));
    }
    
    public final Set e() {
        final HashSet set = new HashSet();
        this.a.C("SELECT DISTINCT uid FROM mutation_queues").e(new fh1(set));
        return set;
    }
    
    public boolean f() {
        final Boolean[] array = { Boolean.FALSE };
        this.a.C("SELECT migration_name FROM data_migrations").e(new eh1(array));
        return array[0];
    }
    
    public final void j() {
        this.a.t("DELETE FROM data_migrations WHERE migration_name = ?", d51.b);
    }
    
    @Override
    public void run() {
        this.d();
    }
}

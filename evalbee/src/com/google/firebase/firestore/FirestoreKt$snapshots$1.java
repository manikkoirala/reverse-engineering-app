// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.firestore;

import kotlinx.coroutines.channels.ProduceKt;
import kotlin.jvm.internal.Lambda;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import kotlinx.coroutines.f;
import kotlin.coroutines.jvm.internal.SuspendLambda;

@xp(c = "com.google.firebase.firestore.FirestoreKt$snapshots$1", f = "Firestore.kt", l = { 243 }, m = "invokeSuspend")
final class FirestoreKt$snapshots$1 extends SuspendLambda implements q90
{
    final MetadataChanges $metadataChanges;
    final a $this_snapshots;
    private Object L$0;
    int label;
    
    public FirestoreKt$snapshots$1(final a $this_snapshots, final MetadataChanges $metadataChanges, final vl vl) {
        this.$this_snapshots = $this_snapshots;
        this.$metadataChanges = $metadataChanges;
        super(2, vl);
    }
    
    private static final void invokeSuspend$lambda-0(final s81 s81, final DocumentSnapshot documentSnapshot, final FirebaseFirestoreException ex) {
        if (ex != null) {
            f.c((lm)s81, "Error getting DocumentReference snapshot", (Throwable)ex);
        }
        else if (documentSnapshot != null) {
            eg.w((zk1)s81, (Object)documentSnapshot);
        }
    }
    
    @NotNull
    public final vl create(@Nullable final Object l$0, @NotNull final vl vl) {
        final FirestoreKt$snapshots$1 firestoreKt$snapshots$1 = new FirestoreKt$snapshots$1(this.$this_snapshots, this.$metadataChanges, vl);
        firestoreKt$snapshots$1.L$0 = l$0;
        return (vl)firestoreKt$snapshots$1;
    }
    
    @Nullable
    public final Object invoke(@NotNull final s81 s81, @Nullable final vl vl) {
        return ((FirestoreKt$snapshots$1)this.create(s81, vl)).invokeSuspend(u02.a);
    }
    
    @Nullable
    public final Object invokeSuspend(@NotNull final Object o) {
        final Object d = gg0.d();
        final int label = this.label;
        if (label != 0) {
            if (label != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            xe1.b(o);
        }
        else {
            xe1.b(o);
            final s81 s81 = (s81)this.L$0;
            final mk0 d2 = this.$this_snapshots.d(wy.c, this.$metadataChanges, new c(s81));
            fg0.d((Object)d2, "addSnapshotListener(BACK\u2026apshot)\n        }\n      }");
            final a90 a90 = (a90)new a90(d2) {
                final mk0 $registration;
                
                public final void invoke() {
                    this.$registration.remove();
                }
            };
            this.label = 1;
            if (ProduceKt.a(s81, (a90)a90, (vl)this) == d) {
                return d;
            }
        }
        return u02.a;
    }
}

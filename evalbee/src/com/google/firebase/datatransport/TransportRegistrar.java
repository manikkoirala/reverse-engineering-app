// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.datatransport;

import java.util.Arrays;
import java.util.List;
import com.google.android.datatransport.runtime.Destination;
import com.google.android.datatransport.cct.CCTDestination;
import com.google.android.datatransport.runtime.TransportRuntime;
import android.content.Context;
import com.google.android.datatransport.TransportFactory;
import androidx.annotation.Keep;
import com.google.firebase.components.ComponentRegistrar;

@Keep
public class TransportRegistrar implements ComponentRegistrar
{
    private static final String LIBRARY_NAME = "fire-transport";
    
    @Override
    public List<zi> getComponents() {
        return Arrays.asList(zi.e(TransportFactory.class).h("fire-transport").b(os.k(Context.class)).f(new zy1()).d(), mj0.b("fire-transport", "18.1.8"));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.messaging;

import android.os.BaseBundle;
import android.os.Bundle;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.SharedPreferences;
import android.content.pm.PackageManager$NameNotFoundException;
import android.content.Intent;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import com.google.android.gms.common.util.concurrent.NamedThreadFactory;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import com.google.android.gms.tasks.Tasks;
import com.google.android.gms.tasks.SuccessContinuation;
import androidx.annotation.Keep;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.OnSuccessListener;
import android.util.Log;
import android.app.Application;
import java.util.concurrent.TimeUnit;
import android.app.Application$ActivityLifecycleCallbacks;
import com.google.android.gms.tasks.Task;
import java.util.concurrent.Executor;
import android.content.Context;
import java.util.concurrent.ScheduledExecutorService;
import com.google.android.datatransport.TransportFactory;

public class FirebaseMessaging
{
    public static final long n;
    public static f o;
    public static TransportFactory p;
    public static ScheduledExecutorService q;
    public final r10 a;
    public final r20 b;
    public final Context c;
    public final ib0 d;
    public final e e;
    public final a f;
    public final Executor g;
    public final Executor h;
    public final Executor i;
    public final Task j;
    public final cw0 k;
    public boolean l;
    public final Application$ActivityLifecycleCallbacks m;
    
    static {
        n = TimeUnit.HOURS.toSeconds(8L);
    }
    
    public FirebaseMessaging(final r10 a, final t20 t20, final r20 b, final TransportFactory p10, final bs1 bs1, final cw0 k, final ib0 d, final Executor h, final Executor g, final Executor i) {
        this.l = false;
        FirebaseMessaging.p = p10;
        this.a = a;
        this.b = b;
        this.f = new a(bs1);
        final Context l = a.l();
        this.c = l;
        final l00 m = new l00();
        this.m = (Application$ActivityLifecycleCallbacks)m;
        this.k = k;
        this.h = h;
        this.d = d;
        this.e = new e(h);
        this.g = g;
        this.i = i;
        final Context j = a.l();
        if (j instanceof Application) {
            ((Application)j).registerActivityLifecycleCallbacks((Application$ActivityLifecycleCallbacks)m);
        }
        else {
            final StringBuilder sb = new StringBuilder();
            sb.append("Context ");
            sb.append(j);
            sb.append(" was not an application, can't register for lifecycle callbacks. Some notification events may be dropped as a result.");
            Log.w("FirebaseMessaging", sb.toString());
        }
        if (t20 != null) {
            t20.a((t20.a)new v20(this));
        }
        g.execute(new w20(this));
        (this.j = cy1.e(this, k, d, l, j00.g())).addOnSuccessListener(g, (OnSuccessListener)new x20(this));
        g.execute(new y20(this));
    }
    
    public FirebaseMessaging(final r10 r10, final t20 t20, final r91 r11, final r91 r12, final r20 r13, final TransportFactory transportFactory, final bs1 bs1) {
        this(r10, t20, r11, r12, r13, transportFactory, bs1, new cw0(r10.l()));
    }
    
    public FirebaseMessaging(final r10 r10, final t20 t20, final r91 r11, final r91 r12, final r20 r13, final TransportFactory transportFactory, final bs1 bs1, final cw0 cw0) {
        this(r10, t20, r13, transportFactory, bs1, cw0, new ib0(r10, cw0, r11, r12, r13), j00.f(), j00.c(), j00.b());
    }
    
    public static /* synthetic */ r10 g(final FirebaseMessaging firebaseMessaging) {
        return firebaseMessaging.a;
    }
    
    @Keep
    public static FirebaseMessaging getInstance(final r10 r10) {
        synchronized (FirebaseMessaging.class) {
            final FirebaseMessaging firebaseMessaging = (FirebaseMessaging)r10.j(FirebaseMessaging.class);
            Preconditions.checkNotNull(firebaseMessaging, "Firebase Messaging component is not present");
            return firebaseMessaging;
        }
    }
    
    public static FirebaseMessaging l() {
        synchronized (FirebaseMessaging.class) {
            return getInstance(r10.m());
        }
    }
    
    public static f m(final Context context) {
        synchronized (FirebaseMessaging.class) {
            if (FirebaseMessaging.o == null) {
                FirebaseMessaging.o = new f(context);
            }
            return FirebaseMessaging.o;
        }
    }
    
    public static TransportFactory q() {
        return FirebaseMessaging.p;
    }
    
    public void A(final boolean l) {
        synchronized (this) {
            this.l = l;
        }
    }
    
    public final void B() {
        synchronized (this) {
            if (!this.l) {
                this.D(0L);
            }
        }
    }
    
    public final void C() {
        if (this.E(this.p())) {
            this.B();
        }
    }
    
    public void D(final long n) {
        synchronized (this) {
            this.j(new at1(this, Math.min(Math.max(30L, 2L * n), FirebaseMessaging.n)), n);
            this.l = true;
        }
    }
    
    public boolean E(final f.a a) {
        return a == null || a.b(this.k.a());
    }
    
    public String i() {
        final f.a p = this.p();
        if (!this.E(p)) {
            return p.a;
        }
        final String c = cw0.c(this.a);
        Object b = this.e.b(c, (e.a)new a30(this, c, p));
        try {
            b = Tasks.await((Task)b);
            return (String)b;
        }
        catch (final InterruptedException b) {}
        catch (final ExecutionException ex) {}
        throw new IOException((Throwable)b);
    }
    
    public void j(final Runnable runnable, final long n) {
        synchronized (FirebaseMessaging.class) {
            if (FirebaseMessaging.q == null) {
                FirebaseMessaging.q = new ScheduledThreadPoolExecutor(1, new NamedThreadFactory("TAG"));
            }
            FirebaseMessaging.q.schedule(runnable, n, TimeUnit.SECONDS);
        }
    }
    
    public Context k() {
        return this.c;
    }
    
    public final String n() {
        String q;
        if ("[DEFAULT]".equals(this.a.o())) {
            q = "";
        }
        else {
            q = this.a.q();
        }
        return q;
    }
    
    public Task o() {
        final TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        this.g.execute(new z20(this, taskCompletionSource));
        return taskCompletionSource.getTask();
    }
    
    public f.a p() {
        return m(this.c).d(this.n(), cw0.c(this.a));
    }
    
    public final void r(final String s) {
        if ("[DEFAULT]".equals(this.a.o())) {
            if (Log.isLoggable("FirebaseMessaging", 3)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invoking onNewToken for app: ");
                sb.append(this.a.o());
                Log.d("FirebaseMessaging", sb.toString());
            }
            final Intent intent = new Intent("com.google.firebase.messaging.NEW_TOKEN");
            intent.putExtra("token", s);
            new i00(this.c).k(intent);
        }
    }
    
    public boolean s() {
        return this.f.c();
    }
    
    public boolean t() {
        return this.k.g();
    }
    
    public class a
    {
        public final bs1 a;
        public boolean b;
        public qx c;
        public Boolean d;
        public final FirebaseMessaging e;
        
        public a(final FirebaseMessaging e, final bs1 a) {
            this.e = e;
            this.a = a;
        }
        
        public void b() {
            synchronized (this) {
                if (this.b) {
                    return;
                }
                if ((this.d = this.e()) == null) {
                    final c30 c = new c30(this);
                    this.c = c;
                    this.a.b(fp.class, c);
                }
                this.b = true;
            }
        }
        
        public boolean c() {
            synchronized (this) {
                this.b();
                final Boolean d = this.d;
                boolean b;
                if (d != null) {
                    b = d;
                }
                else {
                    b = FirebaseMessaging.g(this.e).v();
                }
                return b;
            }
        }
        
        public final Boolean e() {
            final Context l = FirebaseMessaging.g(this.e).l();
            final SharedPreferences sharedPreferences = l.getSharedPreferences("com.google.firebase.messaging", 0);
            if (sharedPreferences.contains("auto_init")) {
                return sharedPreferences.getBoolean("auto_init", false);
            }
            try {
                final PackageManager packageManager = l.getPackageManager();
                if (packageManager != null) {
                    final ApplicationInfo applicationInfo = packageManager.getApplicationInfo(l.getPackageName(), 128);
                    if (applicationInfo != null) {
                        final Bundle metaData = applicationInfo.metaData;
                        if (metaData != null && ((BaseBundle)metaData).containsKey("firebase_messaging_auto_init_enabled")) {
                            return ((BaseBundle)applicationInfo.metaData).getBoolean("firebase_messaging_auto_init_enabled");
                        }
                    }
                }
                return null;
            }
            catch (final PackageManager$NameNotFoundException ex) {
                return null;
            }
        }
    }
}

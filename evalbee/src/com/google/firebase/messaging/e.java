// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.messaging;

import com.google.android.gms.tasks.Continuation;
import android.util.Log;
import com.google.android.gms.tasks.Task;
import java.util.Map;
import java.util.concurrent.Executor;

public class e
{
    public final Executor a;
    public final Map b;
    
    public e(final Executor a) {
        this.b = new r8();
        this.a = a;
    }
    
    public Task b(final String s, final a a) {
        synchronized (this) {
            final Task task = this.b.get(s);
            if (task != null) {
                if (Log.isLoggable("FirebaseMessaging", 3)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Joining ongoing request for: ");
                    sb.append(s);
                    Log.d("FirebaseMessaging", sb.toString());
                }
                return task;
            }
            if (Log.isLoggable("FirebaseMessaging", 3)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Making new request for: ");
                sb2.append(s);
                Log.d("FirebaseMessaging", sb2.toString());
            }
            final Task continueWithTask = a.start().continueWithTask(this.a, (Continuation)new be1(this, s));
            this.b.put(s, continueWithTask);
            return continueWithTask;
        }
    }
    
    public interface a
    {
        Task start();
    }
}

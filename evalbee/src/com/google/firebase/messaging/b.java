// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.messaging;

import android.os.BaseBundle;
import com.google.android.datatransport.Event;
import com.google.android.datatransport.ProductData;
import com.google.android.datatransport.Transformer;
import com.google.android.datatransport.Encoding;
import com.google.android.datatransport.TransportFactory;
import android.util.Log;
import java.util.concurrent.ExecutionException;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.installations.a;
import android.text.TextUtils;
import com.google.firebase.messaging.reporting.MessagingClientEvent;
import android.os.Bundle;
import android.content.Intent;

public abstract class b
{
    public static boolean A(final Intent intent) {
        return intent != null && !r(intent) && B(intent.getExtras());
    }
    
    public static boolean B(final Bundle bundle) {
        return bundle != null && "1".equals(((BaseBundle)bundle).getString("google.c.a.e"));
    }
    
    public static boolean a() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: pop            
        //     4: invokestatic    r10.m:()Lr10;
        //     7: invokevirtual   r10.l:()Landroid/content/Context;
        //    10: astore_1       
        //    11: aload_1        
        //    12: ldc             "com.google.firebase.messaging"
        //    14: iconst_0       
        //    15: invokevirtual   android/content/Context.getSharedPreferences:(Ljava/lang/String;I)Landroid/content/SharedPreferences;
        //    18: astore_2       
        //    19: aload_2        
        //    20: ldc             "export_to_big_query"
        //    22: invokeinterface android/content/SharedPreferences.contains:(Ljava/lang/String;)Z
        //    27: ifeq            40
        //    30: aload_2        
        //    31: ldc             "export_to_big_query"
        //    33: iconst_0       
        //    34: invokeinterface android/content/SharedPreferences.getBoolean:(Ljava/lang/String;Z)Z
        //    39: ireturn        
        //    40: aload_1        
        //    41: invokevirtual   android/content/Context.getPackageManager:()Landroid/content/pm/PackageManager;
        //    44: astore_2       
        //    45: aload_2        
        //    46: ifnull          96
        //    49: aload_2        
        //    50: aload_1        
        //    51: invokevirtual   android/content/Context.getPackageName:()Ljava/lang/String;
        //    54: sipush          128
        //    57: invokevirtual   android/content/pm/PackageManager.getApplicationInfo:(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
        //    60: astore_1       
        //    61: aload_1        
        //    62: ifnull          96
        //    65: aload_1        
        //    66: getfield        android/content/pm/ApplicationInfo.metaData:Landroid/os/Bundle;
        //    69: astore_2       
        //    70: aload_2        
        //    71: ifnull          96
        //    74: aload_2        
        //    75: ldc             "delivery_metrics_exported_to_big_query_enabled"
        //    77: invokevirtual   android/os/BaseBundle.containsKey:(Ljava/lang/String;)Z
        //    80: ifeq            96
        //    83: aload_1        
        //    84: getfield        android/content/pm/ApplicationInfo.metaData:Landroid/os/Bundle;
        //    87: ldc             "delivery_metrics_exported_to_big_query_enabled"
        //    89: iconst_0       
        //    90: invokevirtual   android/os/BaseBundle.getBoolean:(Ljava/lang/String;Z)Z
        //    93: istore_0       
        //    94: iload_0        
        //    95: ireturn        
        //    96: iconst_0       
        //    97: ireturn        
        //    98: astore_1       
        //    99: ldc             "FirebaseMessaging"
        //   101: ldc             "FirebaseApp has not being initialized. Device might be in direct boot mode. Skip exporting delivery metrics to Big Query"
        //   103: invokestatic    android/util/Log.i:(Ljava/lang/String;Ljava/lang/String;)I
        //   106: pop            
        //   107: iconst_0       
        //   108: ireturn        
        //   109: astore_1       
        //   110: goto            96
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                                     
        //  -----  -----  -----  -----  ---------------------------------------------------------
        //  0      4      98     109    Ljava/lang/IllegalStateException;
        //  40     45     109    113    Landroid/content/pm/PackageManager$NameNotFoundException;
        //  49     61     109    113    Landroid/content/pm/PackageManager$NameNotFoundException;
        //  65     70     109    113    Landroid/content/pm/PackageManager$NameNotFoundException;
        //  74     94     109    113    Landroid/content/pm/PackageManager$NameNotFoundException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0040:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static MessagingClientEvent b(final MessagingClientEvent.Event event, final Intent intent) {
        if (intent == null) {
            return null;
        }
        Bundle bundle;
        if ((bundle = intent.getExtras()) == null) {
            bundle = Bundle.EMPTY;
        }
        final MessagingClientEvent.a h = MessagingClientEvent.p().m(p(bundle)).e(event).f(f(bundle)).i(m()).k(MessagingClientEvent.SDKPlatform.ANDROID).h(k(bundle));
        final String h2 = h(bundle);
        if (h2 != null) {
            h.g(h2);
        }
        final String o = o(bundle);
        if (o != null) {
            h.l(o);
        }
        final String c = c(bundle);
        if (c != null) {
            h.c(c);
        }
        final String i = i(bundle);
        if (i != null) {
            h.b(i);
        }
        final String e = e(bundle);
        if (e != null) {
            h.d(e);
        }
        final long n = n(bundle);
        if (n > 0L) {
            h.j(n);
        }
        return h.a();
    }
    
    public static String c(final Bundle bundle) {
        return ((BaseBundle)bundle).getString("collapse_key");
    }
    
    public static String d(final Bundle bundle) {
        return ((BaseBundle)bundle).getString("google.c.a.c_id");
    }
    
    public static String e(final Bundle bundle) {
        return ((BaseBundle)bundle).getString("google.c.a.c_l");
    }
    
    public static String f(Bundle string) {
        string = (InterruptedException)((BaseBundle)string).getString("google.to");
        if (!TextUtils.isEmpty((CharSequence)string)) {
            return (String)string;
        }
        try {
            string = (InterruptedException)Tasks.await(a.q(r10.m()).getId());
            return (String)string;
        }
        catch (final InterruptedException string) {}
        catch (final ExecutionException ex) {}
        throw new RuntimeException(string);
    }
    
    public static String g(final Bundle bundle) {
        return ((BaseBundle)bundle).getString("google.c.a.m_c");
    }
    
    public static String h(final Bundle bundle) {
        String s;
        if ((s = ((BaseBundle)bundle).getString("google.message_id")) == null) {
            s = ((BaseBundle)bundle).getString("message_id");
        }
        return s;
    }
    
    public static String i(final Bundle bundle) {
        return ((BaseBundle)bundle).getString("google.c.a.m_l");
    }
    
    public static String j(final Bundle bundle) {
        return ((BaseBundle)bundle).getString("google.c.a.ts");
    }
    
    public static MessagingClientEvent.MessageType k(final Bundle bundle) {
        MessagingClientEvent.MessageType messageType;
        if (bundle != null && c.t(bundle)) {
            messageType = MessagingClientEvent.MessageType.DISPLAY_NOTIFICATION;
        }
        else {
            messageType = MessagingClientEvent.MessageType.DATA_MESSAGE;
        }
        return messageType;
    }
    
    public static String l(final Bundle bundle) {
        String s;
        if (bundle != null && c.t(bundle)) {
            s = "display";
        }
        else {
            s = "data";
        }
        return s;
    }
    
    public static String m() {
        return r10.m().l().getPackageName();
    }
    
    public static long n(Bundle m) {
        if (((BaseBundle)m).containsKey("google.c.sender.id")) {
            try {
                return Long.parseLong(((BaseBundle)m).getString("google.c.sender.id"));
            }
            catch (final NumberFormatException ex) {
                Log.w("FirebaseMessaging", "error parsing project number", (Throwable)ex);
            }
        }
        m = (Bundle)r10.m();
        final String d = ((r10)m).p().d();
        if (d != null) {
            try {
                return Long.parseLong(d);
            }
            catch (final NumberFormatException ex2) {
                Log.w("FirebaseMessaging", "error parsing sender ID", (Throwable)ex2);
            }
        }
        final String c = ((r10)m).p().c();
        if (!c.startsWith("1:")) {
            try {
                return Long.parseLong(c);
            }
            catch (final NumberFormatException ex3) {
                Log.w("FirebaseMessaging", "error parsing app ID", (Throwable)ex3);
                return 0L;
            }
        }
        final String[] split = c.split(":");
        if (split.length < 2) {
            return 0L;
        }
        final String s = split[1];
        if (s.isEmpty()) {
            return 0L;
        }
        return Long.parseLong(s);
    }
    
    public static String o(final Bundle bundle) {
        String string = ((BaseBundle)bundle).getString("from");
        if (string == null || !string.startsWith("/topics/")) {
            string = null;
        }
        return string;
    }
    
    public static int p(Bundle value) {
        value = (Bundle)((BaseBundle)value).get("google.ttl");
        if (value instanceof Integer) {
            return (int)value;
        }
        if (value instanceof String) {
            try {
                return Integer.parseInt((String)value);
            }
            catch (final NumberFormatException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid TTL: ");
                sb.append(value);
                Log.w("FirebaseMessaging", sb.toString());
            }
        }
        return 0;
    }
    
    public static String q(final Bundle bundle) {
        if (((BaseBundle)bundle).containsKey("google.c.a.udt")) {
            return ((BaseBundle)bundle).getString("google.c.a.udt");
        }
        return null;
    }
    
    public static boolean r(final Intent intent) {
        return "com.google.firebase.messaging.RECEIVE_DIRECT_BOOT".equals(intent.getAction());
    }
    
    public static void s(final Intent intent) {
        x("_nd", intent.getExtras());
    }
    
    public static void t(final Intent intent) {
        x("_nf", intent.getExtras());
    }
    
    public static void u(final Bundle bundle) {
        y(bundle);
        x("_no", bundle);
    }
    
    public static void v(final Intent intent) {
        if (A(intent)) {
            x("_nr", intent.getExtras());
        }
        if (z(intent)) {
            w(MessagingClientEvent.Event.MESSAGE_DELIVERED, intent, FirebaseMessaging.q());
        }
    }
    
    public static void w(final MessagingClientEvent.Event event, final Intent intent, final TransportFactory transportFactory) {
        if (transportFactory == null) {
            Log.e("FirebaseMessaging", "TransportFactory is null. Skip exporting message delivery metrics to Big Query");
            return;
        }
        final MessagingClientEvent b = b(event, intent);
        if (b == null) {
            return;
        }
        try {
            transportFactory.getTransport("FCM_CLIENT_EVENT_LOGGING", aw0.class, Encoding.of("proto"), new zv0()).send(Event.ofData(aw0.b().b(b).a(), ProductData.withProductId(intent.getIntExtra("google.product_id", 111881503))));
        }
        catch (final RuntimeException ex) {
            Log.w("FirebaseMessaging", "Failed to send big query analytics payload.", (Throwable)ex);
        }
    }
    
    public static void x(final String str, Bundle obj) {
        try {
            r10.m();
            Bundle bundle = obj;
            if (obj == null) {
                bundle = new Bundle();
            }
            obj = new Bundle();
            final String d = d(bundle);
            if (d != null) {
                ((BaseBundle)obj).putString("_nmid", d);
            }
            final String e = e(bundle);
            if (e != null) {
                ((BaseBundle)obj).putString("_nmn", e);
            }
            final String i = i(bundle);
            if (!TextUtils.isEmpty((CharSequence)i)) {
                ((BaseBundle)obj).putString("label", i);
            }
            final String g = g(bundle);
            if (!TextUtils.isEmpty((CharSequence)g)) {
                ((BaseBundle)obj).putString("message_channel", g);
            }
            final String o = o(bundle);
            if (o != null) {
                ((BaseBundle)obj).putString("_nt", o);
            }
            final String j = j(bundle);
            if (j != null) {
                try {
                    ((BaseBundle)obj).putInt("_nmt", Integer.parseInt(j));
                }
                catch (final NumberFormatException ex) {
                    Log.w("FirebaseMessaging", "Error while parsing timestamp in GCM event", (Throwable)ex);
                }
            }
            final String q = q(bundle);
            if (q != null) {
                try {
                    ((BaseBundle)obj).putInt("_ndt", Integer.parseInt(q));
                }
                catch (final NumberFormatException ex2) {
                    Log.w("FirebaseMessaging", "Error while parsing use_device_time in GCM event", (Throwable)ex2);
                }
            }
            final String l = l(bundle);
            if ("_nr".equals(str) || "_nf".equals(str)) {
                ((BaseBundle)obj).putString("_nmc", l);
            }
            if (Log.isLoggable("FirebaseMessaging", 3)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Logging to scion event=");
                sb.append(str);
                sb.append(" scionPayload=");
                sb.append(obj);
                Log.d("FirebaseMessaging", sb.toString());
            }
            final g4 g2 = (g4)r10.m().j(g4.class);
            if (g2 != null) {
                g2.a("fcm", str, obj);
            }
            else {
                Log.w("FirebaseMessaging", "Unable to log event: analytics library is missing");
            }
        }
        catch (final IllegalStateException ex3) {
            Log.e("FirebaseMessaging", "Default FirebaseApp has not been initialized. Skip logging event to GA.");
        }
    }
    
    public static void y(final Bundle bundle) {
        if (bundle == null) {
            return;
        }
        if ("1".equals(((BaseBundle)bundle).getString("google.c.a.tc"))) {
            final g4 g4 = (g4)r10.m().j(g4.class);
            if (Log.isLoggable("FirebaseMessaging", 3)) {
                Log.d("FirebaseMessaging", "Received event with track-conversion=true. Setting user property and reengagement event");
            }
            if (g4 != null) {
                final String string = ((BaseBundle)bundle).getString("google.c.a.c_id");
                g4.b("fcm", "_ln", string);
                final Bundle bundle2 = new Bundle();
                ((BaseBundle)bundle2).putString("source", "Firebase");
                ((BaseBundle)bundle2).putString("medium", "notification");
                ((BaseBundle)bundle2).putString("campaign", string);
                g4.a("fcm", "_cmp", bundle2);
            }
            else {
                Log.w("FirebaseMessaging", "Unable to set user property for conversion tracking:  analytics library is missing");
            }
        }
        else if (Log.isLoggable("FirebaseMessaging", 3)) {
            Log.d("FirebaseMessaging", "Received event with track-conversion=false. Do not set user property");
        }
    }
    
    public static boolean z(final Intent intent) {
        return intent != null && !r(intent) && a();
    }
}

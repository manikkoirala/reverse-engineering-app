// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.messaging.threads;

public enum ThreadPriority
{
    private static final ThreadPriority[] $VALUES;
    
    HIGH_SPEED, 
    LOW_POWER;
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.messaging.reporting;

public final class MessagingClientEvent
{
    public static final MessagingClientEvent p;
    public final long a;
    public final String b;
    public final String c;
    public final MessageType d;
    public final SDKPlatform e;
    public final String f;
    public final String g;
    public final int h;
    public final int i;
    public final String j;
    public final long k;
    public final Event l;
    public final String m;
    public final long n;
    public final String o;
    
    static {
        p = new a().a();
    }
    
    public MessagingClientEvent(final long a, final String b, final String c, final MessageType d, final SDKPlatform e, final String f, final String g, final int h, final int i, final String j, final long k, final Event l, final String m, final long n, final String o) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = h;
        this.i = i;
        this.j = j;
        this.k = k;
        this.l = l;
        this.m = m;
        this.n = n;
        this.o = o;
    }
    
    public static a p() {
        return new a();
    }
    
    public String a() {
        return this.m;
    }
    
    public long b() {
        return this.k;
    }
    
    public long c() {
        return this.n;
    }
    
    public String d() {
        return this.g;
    }
    
    public String e() {
        return this.o;
    }
    
    public Event f() {
        return this.l;
    }
    
    public String g() {
        return this.c;
    }
    
    public String h() {
        return this.b;
    }
    
    public MessageType i() {
        return this.d;
    }
    
    public String j() {
        return this.f;
    }
    
    public int k() {
        return this.h;
    }
    
    public long l() {
        return this.a;
    }
    
    public SDKPlatform m() {
        return this.e;
    }
    
    public String n() {
        return this.j;
    }
    
    public int o() {
        return this.i;
    }
    
    public enum Event implements j91
    {
        private static final Event[] $VALUES;
        
        MESSAGE_DELIVERED(1), 
        MESSAGE_OPEN(2), 
        UNKNOWN_EVENT(0);
        
        private final int number_;
        
        private Event(final int number_) {
            this.number_ = number_;
        }
        
        @Override
        public int getNumber() {
            return this.number_;
        }
    }
    
    public enum MessageType implements j91
    {
        private static final MessageType[] $VALUES;
        
        DATA_MESSAGE(1), 
        DISPLAY_NOTIFICATION(3), 
        TOPIC(2), 
        UNKNOWN(0);
        
        private final int number_;
        
        private MessageType(final int number_) {
            this.number_ = number_;
        }
        
        @Override
        public int getNumber() {
            return this.number_;
        }
    }
    
    public enum SDKPlatform implements j91
    {
        private static final SDKPlatform[] $VALUES;
        
        ANDROID(1), 
        IOS(2), 
        UNKNOWN_OS(0), 
        WEB(3);
        
        private final int number_;
        
        private SDKPlatform(final int number_) {
            this.number_ = number_;
        }
        
        @Override
        public int getNumber() {
            return this.number_;
        }
    }
    
    public static final class a
    {
        public long a;
        public String b;
        public String c;
        public MessageType d;
        public SDKPlatform e;
        public String f;
        public String g;
        public int h;
        public int i;
        public String j;
        public long k;
        public Event l;
        public String m;
        public long n;
        public String o;
        
        public a() {
            this.a = 0L;
            this.b = "";
            this.c = "";
            this.d = MessageType.UNKNOWN;
            this.e = SDKPlatform.UNKNOWN_OS;
            this.f = "";
            this.g = "";
            this.h = 0;
            this.i = 0;
            this.j = "";
            this.k = 0L;
            this.l = Event.UNKNOWN_EVENT;
            this.m = "";
            this.n = 0L;
            this.o = "";
        }
        
        public MessagingClientEvent a() {
            return new MessagingClientEvent(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.o);
        }
        
        public a b(final String m) {
            this.m = m;
            return this;
        }
        
        public a c(final String g) {
            this.g = g;
            return this;
        }
        
        public a d(final String o) {
            this.o = o;
            return this;
        }
        
        public a e(final Event l) {
            this.l = l;
            return this;
        }
        
        public a f(final String c) {
            this.c = c;
            return this;
        }
        
        public a g(final String b) {
            this.b = b;
            return this;
        }
        
        public a h(final MessageType d) {
            this.d = d;
            return this;
        }
        
        public a i(final String f) {
            this.f = f;
            return this;
        }
        
        public a j(final long a) {
            this.a = a;
            return this;
        }
        
        public a k(final SDKPlatform e) {
            this.e = e;
            return this;
        }
        
        public a l(final String j) {
            this.j = j;
            return this;
        }
        
        public a m(final int i) {
            this.i = i;
            return this;
        }
    }
}

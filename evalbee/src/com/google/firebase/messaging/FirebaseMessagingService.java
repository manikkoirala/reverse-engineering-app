// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.messaging;

import com.google.android.gms.cloudmessaging.CloudMessage;
import java.util.concurrent.ExecutorService;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.content.Intent;
import java.util.ArrayDeque;
import com.google.android.gms.cloudmessaging.Rpc;
import java.util.Queue;

public class FirebaseMessagingService extends ex
{
    public static final Queue g;
    public Rpc f;
    
    static {
        g = new ArrayDeque(10);
    }
    
    @Override
    public Intent e(final Intent intent) {
        return sl1.b().c();
    }
    
    @Override
    public void f(final Intent intent) {
        final String action = intent.getAction();
        if (!"com.google.android.c2dm.intent.RECEIVE".equals(action) && !"com.google.firebase.messaging.RECEIVE_DIRECT_BOOT".equals(action)) {
            if ("com.google.firebase.messaging.NEW_TOKEN".equals(action)) {
                this.t(intent.getStringExtra("token"));
            }
            else {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unknown intent action: ");
                sb.append(intent.getAction());
                Log.d("FirebaseMessaging", sb.toString());
            }
        }
        else {
            this.p(intent);
        }
    }
    
    public final boolean l(final String str) {
        if (TextUtils.isEmpty((CharSequence)str)) {
            return false;
        }
        final Queue g = FirebaseMessagingService.g;
        if (g.contains(str)) {
            if (Log.isLoggable("FirebaseMessaging", 3)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Received duplicate message: ");
                sb.append(str);
                Log.d("FirebaseMessaging", sb.toString());
            }
            return true;
        }
        if (g.size() >= 10) {
            g.remove();
        }
        g.add(str);
        return false;
    }
    
    public final void m(final Intent intent) {
        Bundle extras;
        if ((extras = intent.getExtras()) == null) {
            extras = new Bundle();
        }
        extras.remove("androidx.content.wakelockid");
        if (com.google.firebase.messaging.c.t(extras)) {
            final c c = new c(extras);
            final ExecutorService e = j00.e();
            final st st = new st((Context)this, c, e);
            try {
                final boolean a = st.a();
                e.shutdown();
                if (a) {
                    return;
                }
                if (com.google.firebase.messaging.b.A(intent)) {
                    com.google.firebase.messaging.b.t(intent);
                }
            }
            finally {
                e.shutdown();
            }
        }
        this.r(new d(extras));
    }
    
    public final String n(final Intent intent) {
        String s;
        if ((s = intent.getStringExtra("google.message_id")) == null) {
            s = intent.getStringExtra("message_id");
        }
        return s;
    }
    
    public final Rpc o(final Context context) {
        if (this.f == null) {
            this.f = new Rpc(context.getApplicationContext());
        }
        return this.f;
    }
    
    public final void p(final Intent intent) {
        if (!this.l(intent.getStringExtra("google.message_id"))) {
            this.v(intent);
        }
        this.o((Context)this).messageHandled(new CloudMessage(intent));
    }
    
    public void q() {
    }
    
    public void r(final d d) {
    }
    
    public void s(final String s) {
    }
    
    public void t(final String s) {
    }
    
    public void u(final String s, final Exception ex) {
    }
    
    public final void v(final Intent intent) {
        String stringExtra;
        if ((stringExtra = intent.getStringExtra("message_type")) == null) {
            stringExtra = "gcm";
        }
        final int hashCode = stringExtra.hashCode();
        int n = -1;
        switch (hashCode) {
            case 814800675: {
                if (!stringExtra.equals("send_event")) {
                    break;
                }
                n = 3;
                break;
            }
            case 814694033: {
                if (!stringExtra.equals("send_error")) {
                    break;
                }
                n = 2;
                break;
            }
            case 102161: {
                if (!stringExtra.equals("gcm")) {
                    break;
                }
                n = 1;
                break;
            }
            case -2062414158: {
                if (!stringExtra.equals("deleted_messages")) {
                    break;
                }
                n = 0;
                break;
            }
        }
        switch (n) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("Received message with unknown type: ");
                sb.append(stringExtra);
                Log.w("FirebaseMessaging", sb.toString());
                break;
            }
            case 3: {
                this.s(intent.getStringExtra("google.message_id"));
                break;
            }
            case 2: {
                this.u(this.n(intent), new SendException(intent.getStringExtra("error")));
                break;
            }
            case 1: {
                com.google.firebase.messaging.b.v(intent);
                this.m(intent);
                break;
            }
            case 0: {
                this.q();
                break;
            }
        }
    }
}

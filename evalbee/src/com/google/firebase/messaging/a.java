// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.messaging;

import android.os.BaseBundle;
import java.util.Iterator;
import android.os.Bundle;
import java.util.concurrent.TimeUnit;

public abstract class a
{
    public static final long a;
    
    static {
        a = TimeUnit.MINUTES.toMillis(3L);
    }
    
    public abstract static final class a
    {
        public static r8 a(final Bundle bundle) {
            final r8 r8 = new r8();
            for (final String s : ((BaseBundle)bundle).keySet()) {
                final Object value = ((BaseBundle)bundle).get(s);
                if (value instanceof String) {
                    final String s2 = (String)value;
                    if (s.startsWith("google.") || s.startsWith("gcm.") || s.equals("from") || s.equals("message_type") || s.equals("collapse_key")) {
                        continue;
                    }
                    r8.put(s, s2);
                }
            }
            return r8;
        }
    }
}

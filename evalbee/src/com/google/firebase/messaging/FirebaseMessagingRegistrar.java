// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.messaging;

import java.util.Arrays;
import java.util.List;
import com.google.android.datatransport.TransportFactory;
import com.google.firebase.heartbeatinfo.HeartBeatInfo;
import com.google.android.gms.common.annotation.KeepForSdk;
import androidx.annotation.Keep;
import com.google.firebase.components.ComponentRegistrar;

@Keep
@KeepForSdk
public class FirebaseMessagingRegistrar implements ComponentRegistrar
{
    private static final String LIBRARY_NAME = "fire-fcm";
    
    @Keep
    @Override
    public List<zi> getComponents() {
        return Arrays.asList(zi.e(FirebaseMessaging.class).h("fire-fcm").b(os.k(r10.class)).b(os.h(t20.class)).b(os.i(v12.class)).b(os.i(HeartBeatInfo.class)).b(os.h(TransportFactory.class)).b(os.k(r20.class)).b(os.k(bs1.class)).f(new d30()).c().d(), mj0.b("fire-fcm", "23.4.0"));
    }
}

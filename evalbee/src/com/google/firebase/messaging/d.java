// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.messaging;

import android.net.Uri;
import android.os.Parcel;
import java.util.Map;
import android.os.Bundle;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

public final class d extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<d> CREATOR;
    public Bundle a;
    public Map b;
    public b c;
    
    static {
        CREATOR = (Parcelable$Creator)new ld1();
    }
    
    public d(final Bundle a) {
        this.a = a;
    }
    
    public Map getData() {
        if (this.b == null) {
            this.b = com.google.firebase.messaging.a.a.a(this.a);
        }
        return this.b;
    }
    
    public b i() {
        if (this.c == null && com.google.firebase.messaging.c.t(this.a)) {
            this.c = new b(new c(this.a), null);
        }
        return this.c;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        ld1.c(this, parcel, n);
    }
    
    public static class b
    {
        public final String a;
        public final String b;
        public final String[] c;
        public final String d;
        public final String e;
        public final String[] f;
        public final String g;
        public final String h;
        public final String i;
        public final String j;
        public final String k;
        public final String l;
        public final String m;
        public final Uri n;
        public final String o;
        public final Integer p;
        public final Integer q;
        public final Integer r;
        public final int[] s;
        public final Long t;
        public final boolean u;
        public final boolean v;
        public final boolean w;
        public final boolean x;
        public final boolean y;
        public final long[] z;
        
        public b(final c c) {
            this.a = c.p("gcm.n.title");
            this.b = c.h("gcm.n.title");
            this.c = b(c, "gcm.n.title");
            this.d = c.p("gcm.n.body");
            this.e = c.h("gcm.n.body");
            this.f = b(c, "gcm.n.body");
            this.g = c.p("gcm.n.icon");
            this.i = c.o();
            this.j = c.p("gcm.n.tag");
            this.k = c.p("gcm.n.color");
            this.l = c.p("gcm.n.click_action");
            this.m = c.p("gcm.n.android_channel_id");
            this.n = c.f();
            this.h = c.p("gcm.n.image");
            this.o = c.p("gcm.n.ticker");
            this.p = c.b("gcm.n.notification_priority");
            this.q = c.b("gcm.n.visibility");
            this.r = c.b("gcm.n.notification_count");
            this.u = c.a("gcm.n.sticky");
            this.v = c.a("gcm.n.local_only");
            this.w = c.a("gcm.n.default_sound");
            this.x = c.a("gcm.n.default_vibrate_timings");
            this.y = c.a("gcm.n.default_light_settings");
            this.t = c.j("gcm.n.event_time");
            this.s = c.e();
            this.z = c.q();
        }
        
        public static String[] b(final c c, final String s) {
            final Object[] g = c.g(s);
            if (g == null) {
                return null;
            }
            final String[] array = new String[g.length];
            for (int i = 0; i < g.length; ++i) {
                array[i] = String.valueOf(g[i]);
            }
            return array;
        }
        
        public String a() {
            return this.d;
        }
        
        public String c() {
            return this.a;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.crashlytics.ktx;

import java.util.List;
import org.jetbrains.annotations.NotNull;
import androidx.annotation.Keep;
import com.google.firebase.components.ComponentRegistrar;

@Keep
public final class FirebaseCrashlyticsKtxRegistrar implements ComponentRegistrar
{
    @NotNull
    public static final a Companion;
    
    static {
        Companion = new a(null);
    }
    
    @NotNull
    @Override
    public List<zi> getComponents() {
        return nh.g();
    }
    
    public static final class a
    {
    }
}

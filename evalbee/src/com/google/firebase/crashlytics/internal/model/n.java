// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.crashlytics.internal.model;

import java.util.List;

public final class n extends CrashlyticsReport.e.d.a.b
{
    public final List a;
    public final c b;
    public final CrashlyticsReport.a c;
    public final d d;
    public final List e;
    
    public n(final List a, final c b, final CrashlyticsReport.a c, final d d, final List e) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
    }
    
    @Override
    public CrashlyticsReport.a b() {
        return this.c;
    }
    
    @Override
    public List c() {
        return this.e;
    }
    
    @Override
    public c d() {
        return this.b;
    }
    
    @Override
    public d e() {
        return this.d;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof CrashlyticsReport.e.d.a.b) {
            final CrashlyticsReport.e.d.a.b b2 = (CrashlyticsReport.e.d.a.b)o;
            final List a = this.a;
            if (a == null) {
                if (b2.f() != null) {
                    return false;
                }
            }
            else if (!a.equals(b2.f())) {
                return false;
            }
            final c b3 = this.b;
            if (b3 == null) {
                if (b2.d() != null) {
                    return false;
                }
            }
            else if (!b3.equals(b2.d())) {
                return false;
            }
            final CrashlyticsReport.a c = this.c;
            if (c == null) {
                if (b2.b() != null) {
                    return false;
                }
            }
            else if (!c.equals(b2.b())) {
                return false;
            }
            if (this.d.equals(b2.e()) && this.e.equals(b2.c())) {
                return b;
            }
            b = false;
            return b;
        }
        return false;
    }
    
    @Override
    public List f() {
        return this.a;
    }
    
    @Override
    public int hashCode() {
        final List a = this.a;
        int hashCode = 0;
        int hashCode2;
        if (a == null) {
            hashCode2 = 0;
        }
        else {
            hashCode2 = a.hashCode();
        }
        final c b = this.b;
        int hashCode3;
        if (b == null) {
            hashCode3 = 0;
        }
        else {
            hashCode3 = b.hashCode();
        }
        final CrashlyticsReport.a c = this.c;
        if (c != null) {
            hashCode = c.hashCode();
        }
        return ((((hashCode2 ^ 0xF4243) * 1000003 ^ hashCode3) * 1000003 ^ hashCode) * 1000003 ^ this.d.hashCode()) * 1000003 ^ this.e.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Execution{threads=");
        sb.append(this.a);
        sb.append(", exception=");
        sb.append(this.b);
        sb.append(", appExitInfo=");
        sb.append(this.c);
        sb.append(", signal=");
        sb.append(this.d);
        sb.append(", binaries=");
        sb.append(this.e);
        sb.append("}");
        return sb.toString();
    }
    
    public static final class b extends CrashlyticsReport.e.d.a.b.b
    {
        public List a;
        public c b;
        public CrashlyticsReport.a c;
        public d d;
        public List e;
        
        @Override
        public CrashlyticsReport.e.d.a.b a() {
            final d d = this.d;
            String string = "";
            if (d == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("");
                sb.append(" signal");
                string = sb.toString();
            }
            String string2 = string;
            if (this.e == null) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(string);
                sb2.append(" binaries");
                string2 = sb2.toString();
            }
            if (string2.isEmpty()) {
                return new n(this.a, this.b, this.c, this.d, this.e, null);
            }
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Missing required properties:");
            sb3.append(string2);
            throw new IllegalStateException(sb3.toString());
        }
        
        @Override
        public CrashlyticsReport.e.d.a.b.b b(final CrashlyticsReport.a c) {
            this.c = c;
            return this;
        }
        
        @Override
        public CrashlyticsReport.e.d.a.b.b c(final List e) {
            if (e != null) {
                this.e = e;
                return this;
            }
            throw new NullPointerException("Null binaries");
        }
        
        @Override
        public CrashlyticsReport.e.d.a.b.b d(final c b) {
            this.b = b;
            return this;
        }
        
        @Override
        public CrashlyticsReport.e.d.a.b.b e(final d d) {
            if (d != null) {
                this.d = d;
                return this;
            }
            throw new NullPointerException("Null signal");
        }
        
        @Override
        public CrashlyticsReport.e.d.a.b.b f(final List a) {
            this.a = a;
            return this;
        }
    }
}

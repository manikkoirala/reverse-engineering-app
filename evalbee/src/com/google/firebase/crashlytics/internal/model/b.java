// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.crashlytics.internal.model;

public final class b extends CrashlyticsReport
{
    public final String b;
    public final String c;
    public final int d;
    public final String e;
    public final String f;
    public final String g;
    public final String h;
    public final String i;
    public final CrashlyticsReport.e j;
    public final CrashlyticsReport.d k;
    public final CrashlyticsReport.a l;
    
    public b(final String b, final String c, final int d, final String e, final String f, final String g, final String h, final String i, final CrashlyticsReport.e j, final CrashlyticsReport.d k, final CrashlyticsReport.a l) {
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = h;
        this.i = i;
        this.j = j;
        this.k = k;
        this.l = l;
    }
    
    @Override
    public CrashlyticsReport.a c() {
        return this.l;
    }
    
    @Override
    public String d() {
        return this.g;
    }
    
    @Override
    public String e() {
        return this.h;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof CrashlyticsReport) {
            final CrashlyticsReport crashlyticsReport = (CrashlyticsReport)o;
            if (this.b.equals(crashlyticsReport.l()) && this.c.equals(crashlyticsReport.h()) && this.d == crashlyticsReport.k() && this.e.equals(crashlyticsReport.i())) {
                final String f = this.f;
                if (f == null) {
                    if (crashlyticsReport.g() != null) {
                        return false;
                    }
                }
                else if (!f.equals(crashlyticsReport.g())) {
                    return false;
                }
                final String g = this.g;
                if (g == null) {
                    if (crashlyticsReport.d() != null) {
                        return false;
                    }
                }
                else if (!g.equals(crashlyticsReport.d())) {
                    return false;
                }
                if (this.h.equals(crashlyticsReport.e()) && this.i.equals(crashlyticsReport.f())) {
                    final CrashlyticsReport.e j = this.j;
                    if (j == null) {
                        if (crashlyticsReport.m() != null) {
                            return false;
                        }
                    }
                    else if (!j.equals(crashlyticsReport.m())) {
                        return false;
                    }
                    final CrashlyticsReport.d k = this.k;
                    if (k == null) {
                        if (crashlyticsReport.j() != null) {
                            return false;
                        }
                    }
                    else if (!k.equals(crashlyticsReport.j())) {
                        return false;
                    }
                    final CrashlyticsReport.a l = this.l;
                    final CrashlyticsReport.a c = crashlyticsReport.c();
                    if (l == null) {
                        if (c == null) {
                            return b;
                        }
                    }
                    else if (l.equals(c)) {
                        return b;
                    }
                }
            }
            b = false;
            return b;
        }
        return false;
    }
    
    @Override
    public String f() {
        return this.i;
    }
    
    @Override
    public String g() {
        return this.f;
    }
    
    @Override
    public String h() {
        return this.c;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.b.hashCode();
        final int hashCode2 = this.c.hashCode();
        final int d = this.d;
        final int hashCode3 = this.e.hashCode();
        final String f = this.f;
        int hashCode4 = 0;
        int hashCode5;
        if (f == null) {
            hashCode5 = 0;
        }
        else {
            hashCode5 = f.hashCode();
        }
        final String g = this.g;
        int hashCode6;
        if (g == null) {
            hashCode6 = 0;
        }
        else {
            hashCode6 = g.hashCode();
        }
        final int hashCode7 = this.h.hashCode();
        final int hashCode8 = this.i.hashCode();
        final CrashlyticsReport.e j = this.j;
        int hashCode9;
        if (j == null) {
            hashCode9 = 0;
        }
        else {
            hashCode9 = j.hashCode();
        }
        final CrashlyticsReport.d k = this.k;
        int hashCode10;
        if (k == null) {
            hashCode10 = 0;
        }
        else {
            hashCode10 = k.hashCode();
        }
        final CrashlyticsReport.a l = this.l;
        if (l != null) {
            hashCode4 = l.hashCode();
        }
        return ((((((((((hashCode ^ 0xF4243) * 1000003 ^ hashCode2) * 1000003 ^ d) * 1000003 ^ hashCode3) * 1000003 ^ hashCode5) * 1000003 ^ hashCode6) * 1000003 ^ hashCode7) * 1000003 ^ hashCode8) * 1000003 ^ hashCode9) * 1000003 ^ hashCode10) * 1000003 ^ hashCode4;
    }
    
    @Override
    public String i() {
        return this.e;
    }
    
    @Override
    public CrashlyticsReport.d j() {
        return this.k;
    }
    
    @Override
    public int k() {
        return this.d;
    }
    
    @Override
    public String l() {
        return this.b;
    }
    
    @Override
    public CrashlyticsReport.e m() {
        return this.j;
    }
    
    @Override
    public CrashlyticsReport.b n() {
        return new b(this, null);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("CrashlyticsReport{sdkVersion=");
        sb.append(this.b);
        sb.append(", gmpAppId=");
        sb.append(this.c);
        sb.append(", platform=");
        sb.append(this.d);
        sb.append(", installationUuid=");
        sb.append(this.e);
        sb.append(", firebaseInstallationId=");
        sb.append(this.f);
        sb.append(", appQualitySessionId=");
        sb.append(this.g);
        sb.append(", buildVersion=");
        sb.append(this.h);
        sb.append(", displayVersion=");
        sb.append(this.i);
        sb.append(", session=");
        sb.append(this.j);
        sb.append(", ndkPayload=");
        sb.append(this.k);
        sb.append(", appExitInfo=");
        sb.append(this.l);
        sb.append("}");
        return sb.toString();
    }
    
    public static final class b extends CrashlyticsReport.b
    {
        public String a;
        public String b;
        public Integer c;
        public String d;
        public String e;
        public String f;
        public String g;
        public String h;
        public CrashlyticsReport.e i;
        public CrashlyticsReport.d j;
        public CrashlyticsReport.a k;
        
        public b() {
        }
        
        public b(final CrashlyticsReport crashlyticsReport) {
            this.a = crashlyticsReport.l();
            this.b = crashlyticsReport.h();
            this.c = crashlyticsReport.k();
            this.d = crashlyticsReport.i();
            this.e = crashlyticsReport.g();
            this.f = crashlyticsReport.d();
            this.g = crashlyticsReport.e();
            this.h = crashlyticsReport.f();
            this.i = crashlyticsReport.m();
            this.j = crashlyticsReport.j();
            this.k = crashlyticsReport.c();
        }
        
        @Override
        public CrashlyticsReport a() {
            final String a = this.a;
            String string = "";
            if (a == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("");
                sb.append(" sdkVersion");
                string = sb.toString();
            }
            String string2 = string;
            if (this.b == null) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(string);
                sb2.append(" gmpAppId");
                string2 = sb2.toString();
            }
            String string3 = string2;
            if (this.c == null) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(string2);
                sb3.append(" platform");
                string3 = sb3.toString();
            }
            String string4 = string3;
            if (this.d == null) {
                final StringBuilder sb4 = new StringBuilder();
                sb4.append(string3);
                sb4.append(" installationUuid");
                string4 = sb4.toString();
            }
            String string5 = string4;
            if (this.g == null) {
                final StringBuilder sb5 = new StringBuilder();
                sb5.append(string4);
                sb5.append(" buildVersion");
                string5 = sb5.toString();
            }
            String string6 = string5;
            if (this.h == null) {
                final StringBuilder sb6 = new StringBuilder();
                sb6.append(string5);
                sb6.append(" displayVersion");
                string6 = sb6.toString();
            }
            if (string6.isEmpty()) {
                return new com.google.firebase.crashlytics.internal.model.b(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, null);
            }
            final StringBuilder sb7 = new StringBuilder();
            sb7.append("Missing required properties:");
            sb7.append(string6);
            throw new IllegalStateException(sb7.toString());
        }
        
        @Override
        public CrashlyticsReport.b b(final CrashlyticsReport.a k) {
            this.k = k;
            return this;
        }
        
        @Override
        public CrashlyticsReport.b c(final String f) {
            this.f = f;
            return this;
        }
        
        @Override
        public CrashlyticsReport.b d(final String g) {
            if (g != null) {
                this.g = g;
                return this;
            }
            throw new NullPointerException("Null buildVersion");
        }
        
        @Override
        public CrashlyticsReport.b e(final String h) {
            if (h != null) {
                this.h = h;
                return this;
            }
            throw new NullPointerException("Null displayVersion");
        }
        
        @Override
        public CrashlyticsReport.b f(final String e) {
            this.e = e;
            return this;
        }
        
        @Override
        public CrashlyticsReport.b g(final String b) {
            if (b != null) {
                this.b = b;
                return this;
            }
            throw new NullPointerException("Null gmpAppId");
        }
        
        @Override
        public CrashlyticsReport.b h(final String d) {
            if (d != null) {
                this.d = d;
                return this;
            }
            throw new NullPointerException("Null installationUuid");
        }
        
        @Override
        public CrashlyticsReport.b i(final CrashlyticsReport.d j) {
            this.j = j;
            return this;
        }
        
        @Override
        public CrashlyticsReport.b j(final int i) {
            this.c = i;
            return this;
        }
        
        @Override
        public CrashlyticsReport.b k(final String a) {
            if (a != null) {
                this.a = a;
                return this;
            }
            throw new NullPointerException("Null sdkVersion");
        }
        
        @Override
        public CrashlyticsReport.b l(final CrashlyticsReport.e i) {
            this.i = i;
            return this;
        }
    }
}

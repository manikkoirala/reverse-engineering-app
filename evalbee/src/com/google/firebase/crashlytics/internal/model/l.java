// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.crashlytics.internal.model;

public final class l extends CrashlyticsReport.e.d
{
    public final long a;
    public final String b;
    public final CrashlyticsReport.e.d.a c;
    public final CrashlyticsReport.e.d.c d;
    public final CrashlyticsReport.e.d.d e;
    public final f f;
    
    public l(final long a, final String b, final CrashlyticsReport.e.d.a c, final CrashlyticsReport.e.d.c d, final CrashlyticsReport.e.d.d e, final f f) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
    }
    
    @Override
    public CrashlyticsReport.e.d.a b() {
        return this.c;
    }
    
    @Override
    public CrashlyticsReport.e.d.c c() {
        return this.d;
    }
    
    @Override
    public CrashlyticsReport.e.d.d d() {
        return this.e;
    }
    
    @Override
    public f e() {
        return this.f;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof CrashlyticsReport.e.d) {
            final CrashlyticsReport.e.d d = (CrashlyticsReport.e.d)o;
            if (this.a == d.f() && this.b.equals(d.g()) && this.c.equals(d.b()) && this.d.equals(d.c())) {
                final CrashlyticsReport.e.d.d e = this.e;
                if (e == null) {
                    if (d.d() != null) {
                        return false;
                    }
                }
                else if (!e.equals(d.d())) {
                    return false;
                }
                final f f = this.f;
                final f e2 = d.e();
                if (f == null) {
                    if (e2 == null) {
                        return b;
                    }
                }
                else if (f.equals(e2)) {
                    return b;
                }
            }
            b = false;
            return b;
        }
        return false;
    }
    
    @Override
    public long f() {
        return this.a;
    }
    
    @Override
    public String g() {
        return this.b;
    }
    
    @Override
    public CrashlyticsReport.e.d.b h() {
        return new b(this, null);
    }
    
    @Override
    public int hashCode() {
        final long a = this.a;
        final int n = (int)(a ^ a >>> 32);
        final int hashCode = this.b.hashCode();
        final int hashCode2 = this.c.hashCode();
        final int hashCode3 = this.d.hashCode();
        final CrashlyticsReport.e.d.d e = this.e;
        int hashCode4 = 0;
        int hashCode5;
        if (e == null) {
            hashCode5 = 0;
        }
        else {
            hashCode5 = e.hashCode();
        }
        final f f = this.f;
        if (f != null) {
            hashCode4 = f.hashCode();
        }
        return (((((n ^ 0xF4243) * 1000003 ^ hashCode) * 1000003 ^ hashCode2) * 1000003 ^ hashCode3) * 1000003 ^ hashCode5) * 1000003 ^ hashCode4;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Event{timestamp=");
        sb.append(this.a);
        sb.append(", type=");
        sb.append(this.b);
        sb.append(", app=");
        sb.append(this.c);
        sb.append(", device=");
        sb.append(this.d);
        sb.append(", log=");
        sb.append(this.e);
        sb.append(", rollouts=");
        sb.append(this.f);
        sb.append("}");
        return sb.toString();
    }
    
    public static final class b extends CrashlyticsReport.e.d.b
    {
        public Long a;
        public String b;
        public CrashlyticsReport.e.d.a c;
        public CrashlyticsReport.e.d.c d;
        public CrashlyticsReport.e.d.d e;
        public f f;
        
        public b() {
        }
        
        public b(final CrashlyticsReport.e.d d) {
            this.a = d.f();
            this.b = d.g();
            this.c = d.b();
            this.d = d.c();
            this.e = d.d();
            this.f = d.e();
        }
        
        @Override
        public CrashlyticsReport.e.d a() {
            final Long a = this.a;
            String string = "";
            if (a == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("");
                sb.append(" timestamp");
                string = sb.toString();
            }
            String string2 = string;
            if (this.b == null) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(string);
                sb2.append(" type");
                string2 = sb2.toString();
            }
            String string3 = string2;
            if (this.c == null) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(string2);
                sb3.append(" app");
                string3 = sb3.toString();
            }
            String string4 = string3;
            if (this.d == null) {
                final StringBuilder sb4 = new StringBuilder();
                sb4.append(string3);
                sb4.append(" device");
                string4 = sb4.toString();
            }
            if (string4.isEmpty()) {
                return new l(this.a, this.b, this.c, this.d, this.e, this.f, null);
            }
            final StringBuilder sb5 = new StringBuilder();
            sb5.append("Missing required properties:");
            sb5.append(string4);
            throw new IllegalStateException(sb5.toString());
        }
        
        @Override
        public CrashlyticsReport.e.d.b b(final CrashlyticsReport.e.d.a c) {
            if (c != null) {
                this.c = c;
                return this;
            }
            throw new NullPointerException("Null app");
        }
        
        @Override
        public CrashlyticsReport.e.d.b c(final CrashlyticsReport.e.d.c d) {
            if (d != null) {
                this.d = d;
                return this;
            }
            throw new NullPointerException("Null device");
        }
        
        @Override
        public CrashlyticsReport.e.d.b d(final CrashlyticsReport.e.d.d e) {
            this.e = e;
            return this;
        }
        
        @Override
        public CrashlyticsReport.e.d.b e(final f f) {
            this.f = f;
            return this;
        }
        
        @Override
        public CrashlyticsReport.e.d.b f(final long l) {
            this.a = l;
            return this;
        }
        
        @Override
        public CrashlyticsReport.e.d.b g(final String b) {
            if (b != null) {
                this.b = b;
                return this;
            }
            throw new NullPointerException("Null type");
        }
    }
}

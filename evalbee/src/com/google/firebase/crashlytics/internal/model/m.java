// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.crashlytics.internal.model;

import java.util.List;

public final class m extends CrashlyticsReport.e.d.a
{
    public final CrashlyticsReport.e.d.a.b a;
    public final List b;
    public final List c;
    public final Boolean d;
    public final CrashlyticsReport.e.d.a.c e;
    public final List f;
    public final int g;
    
    public m(final CrashlyticsReport.e.d.a.b a, final List b, final List c, final Boolean d, final CrashlyticsReport.e.d.a.c e, final List f, final int g) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
    }
    
    @Override
    public List b() {
        return this.f;
    }
    
    @Override
    public Boolean c() {
        return this.d;
    }
    
    @Override
    public CrashlyticsReport.e.d.a.c d() {
        return this.e;
    }
    
    @Override
    public List e() {
        return this.b;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof CrashlyticsReport.e.d.a) {
            final CrashlyticsReport.e.d.a a = (CrashlyticsReport.e.d.a)o;
            if (this.a.equals(a.f())) {
                final List b2 = this.b;
                if (b2 == null) {
                    if (a.e() != null) {
                        return false;
                    }
                }
                else if (!b2.equals(a.e())) {
                    return false;
                }
                final List c = this.c;
                if (c == null) {
                    if (a.g() != null) {
                        return false;
                    }
                }
                else if (!c.equals(a.g())) {
                    return false;
                }
                final Boolean d = this.d;
                if (d == null) {
                    if (a.c() != null) {
                        return false;
                    }
                }
                else if (!d.equals(a.c())) {
                    return false;
                }
                final CrashlyticsReport.e.d.a.c e = this.e;
                if (e == null) {
                    if (a.d() != null) {
                        return false;
                    }
                }
                else if (!e.equals(a.d())) {
                    return false;
                }
                final List f = this.f;
                if (f == null) {
                    if (a.b() != null) {
                        return false;
                    }
                }
                else if (!f.equals(a.b())) {
                    return false;
                }
                if (this.g == a.h()) {
                    return b;
                }
            }
            b = false;
            return b;
        }
        return false;
    }
    
    @Override
    public CrashlyticsReport.e.d.a.b f() {
        return this.a;
    }
    
    @Override
    public List g() {
        return this.c;
    }
    
    @Override
    public int h() {
        return this.g;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.a.hashCode();
        final List b = this.b;
        int hashCode2 = 0;
        int hashCode3;
        if (b == null) {
            hashCode3 = 0;
        }
        else {
            hashCode3 = b.hashCode();
        }
        final List c = this.c;
        int hashCode4;
        if (c == null) {
            hashCode4 = 0;
        }
        else {
            hashCode4 = c.hashCode();
        }
        final Boolean d = this.d;
        int hashCode5;
        if (d == null) {
            hashCode5 = 0;
        }
        else {
            hashCode5 = d.hashCode();
        }
        final CrashlyticsReport.e.d.a.c e = this.e;
        int hashCode6;
        if (e == null) {
            hashCode6 = 0;
        }
        else {
            hashCode6 = e.hashCode();
        }
        final List f = this.f;
        if (f != null) {
            hashCode2 = f.hashCode();
        }
        return ((((((hashCode ^ 0xF4243) * 1000003 ^ hashCode3) * 1000003 ^ hashCode4) * 1000003 ^ hashCode5) * 1000003 ^ hashCode6) * 1000003 ^ hashCode2) * 1000003 ^ this.g;
    }
    
    @Override
    public CrashlyticsReport.e.d.a.a i() {
        return new b(this, null);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Application{execution=");
        sb.append(this.a);
        sb.append(", customAttributes=");
        sb.append(this.b);
        sb.append(", internalKeys=");
        sb.append(this.c);
        sb.append(", background=");
        sb.append(this.d);
        sb.append(", currentProcessDetails=");
        sb.append(this.e);
        sb.append(", appProcessDetails=");
        sb.append(this.f);
        sb.append(", uiOrientation=");
        sb.append(this.g);
        sb.append("}");
        return sb.toString();
    }
    
    public static final class b extends CrashlyticsReport.e.d.a.a
    {
        public CrashlyticsReport.e.d.a.b a;
        public List b;
        public List c;
        public Boolean d;
        public CrashlyticsReport.e.d.a.c e;
        public List f;
        public Integer g;
        
        public b() {
        }
        
        public b(final CrashlyticsReport.e.d.a a) {
            this.a = a.f();
            this.b = a.e();
            this.c = a.g();
            this.d = a.c();
            this.e = a.d();
            this.f = a.b();
            this.g = a.h();
        }
        
        @Override
        public CrashlyticsReport.e.d.a a() {
            final CrashlyticsReport.e.d.a.b a = this.a;
            String string = "";
            if (a == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("");
                sb.append(" execution");
                string = sb.toString();
            }
            String string2 = string;
            if (this.g == null) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(string);
                sb2.append(" uiOrientation");
                string2 = sb2.toString();
            }
            if (string2.isEmpty()) {
                return new m(this.a, this.b, this.c, this.d, this.e, this.f, this.g, null);
            }
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Missing required properties:");
            sb3.append(string2);
            throw new IllegalStateException(sb3.toString());
        }
        
        @Override
        public CrashlyticsReport.e.d.a.a b(final List f) {
            this.f = f;
            return this;
        }
        
        @Override
        public CrashlyticsReport.e.d.a.a c(final Boolean d) {
            this.d = d;
            return this;
        }
        
        @Override
        public CrashlyticsReport.e.d.a.a d(final CrashlyticsReport.e.d.a.c e) {
            this.e = e;
            return this;
        }
        
        @Override
        public CrashlyticsReport.e.d.a.a e(final List b) {
            this.b = b;
            return this;
        }
        
        @Override
        public CrashlyticsReport.e.d.a.a f(final CrashlyticsReport.e.d.a.b a) {
            if (a != null) {
                this.a = a;
                return this;
            }
            throw new NullPointerException("Null execution");
        }
        
        @Override
        public CrashlyticsReport.e.d.a.a g(final List c) {
            this.c = c;
            return this;
        }
        
        @Override
        public CrashlyticsReport.e.d.a.a h(final int i) {
            this.g = i;
            return this;
        }
    }
}

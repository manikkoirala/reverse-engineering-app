// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.crashlytics.internal.model;

public final class w extends e
{
    public final e.b a;
    public final String b;
    public final String c;
    public final long d;
    
    public w(final e.b a, final String b, final String c, final long d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
    
    @Override
    public String b() {
        return this.b;
    }
    
    @Override
    public String c() {
        return this.c;
    }
    
    @Override
    public e.b d() {
        return this.a;
    }
    
    @Override
    public long e() {
        return this.d;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof e) {
            final e e = (e)o;
            if (!this.a.equals(e.d()) || !this.b.equals(e.b()) || !this.c.equals(e.c()) || this.d != e.e()) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.a.hashCode();
        final int hashCode2 = this.b.hashCode();
        final int hashCode3 = this.c.hashCode();
        final long d = this.d;
        return (((hashCode ^ 0xF4243) * 1000003 ^ hashCode2) * 1000003 ^ hashCode3) * 1000003 ^ (int)(d ^ d >>> 32);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("RolloutAssignment{rolloutVariant=");
        sb.append(this.a);
        sb.append(", parameterKey=");
        sb.append(this.b);
        sb.append(", parameterValue=");
        sb.append(this.c);
        sb.append(", templateVersion=");
        sb.append(this.d);
        sb.append("}");
        return sb.toString();
    }
    
    public static final class b extends e.a
    {
        public e.b a;
        public String b;
        public String c;
        public Long d;
        
        @Override
        public e a() {
            final e.b a = this.a;
            String string = "";
            if (a == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("");
                sb.append(" rolloutVariant");
                string = sb.toString();
            }
            String string2 = string;
            if (this.b == null) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(string);
                sb2.append(" parameterKey");
                string2 = sb2.toString();
            }
            String string3 = string2;
            if (this.c == null) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(string2);
                sb3.append(" parameterValue");
                string3 = sb3.toString();
            }
            String string4 = string3;
            if (this.d == null) {
                final StringBuilder sb4 = new StringBuilder();
                sb4.append(string3);
                sb4.append(" templateVersion");
                string4 = sb4.toString();
            }
            if (string4.isEmpty()) {
                return new w(this.a, this.b, this.c, this.d, null);
            }
            final StringBuilder sb5 = new StringBuilder();
            sb5.append("Missing required properties:");
            sb5.append(string4);
            throw new IllegalStateException(sb5.toString());
        }
        
        @Override
        public e.a b(final String b) {
            if (b != null) {
                this.b = b;
                return this;
            }
            throw new NullPointerException("Null parameterKey");
        }
        
        @Override
        public e.a c(final String c) {
            if (c != null) {
                this.c = c;
                return this;
            }
            throw new NullPointerException("Null parameterValue");
        }
        
        @Override
        public e.a d(final e.b a) {
            if (a != null) {
                this.a = a;
                return this;
            }
            throw new NullPointerException("Null rolloutVariant");
        }
        
        @Override
        public e.a e(final long l) {
            this.d = l;
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.crashlytics.internal.model;

import java.util.List;

public final class y extends f
{
    public final List a;
    
    public y(final List a) {
        this.a = a;
    }
    
    @Override
    public List b() {
        return this.a;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof f && this.a.equals(((f)o).b()));
    }
    
    @Override
    public int hashCode() {
        return this.a.hashCode() ^ 0xF4243;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("RolloutsState{rolloutAssignments=");
        sb.append(this.a);
        sb.append("}");
        return sb.toString();
    }
    
    public static final class b extends a
    {
        public List a;
        
        @Override
        public f a() {
            final List a = this.a;
            String string = "";
            if (a == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("");
                sb.append(" rolloutAssignments");
                string = sb.toString();
            }
            if (string.isEmpty()) {
                return new y(this.a, null);
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Missing required properties:");
            sb2.append(string);
            throw new IllegalStateException(sb2.toString());
        }
        
        @Override
        public a b(final List a) {
            if (a != null) {
                this.a = a;
                return this;
            }
            throw new NullPointerException("Null rolloutAssignments");
        }
    }
}

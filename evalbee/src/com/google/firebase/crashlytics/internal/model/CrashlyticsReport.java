// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.crashlytics.internal.model;

import java.util.List;
import java.nio.charset.Charset;

public abstract class CrashlyticsReport
{
    public static final Charset a;
    
    static {
        a = Charset.forName("UTF-8");
    }
    
    public static /* synthetic */ Charset a() {
        return CrashlyticsReport.a;
    }
    
    public static b b() {
        return (b)new com.google.firebase.crashlytics.internal.model.b.b();
    }
    
    public abstract a c();
    
    public abstract String d();
    
    public abstract String e();
    
    public abstract String f();
    
    public abstract String g();
    
    public abstract String h();
    
    public abstract String i();
    
    public abstract d j();
    
    public abstract int k();
    
    public abstract String l();
    
    public abstract e m();
    
    public abstract b n();
    
    public CrashlyticsReport o(final String s) {
        final b c = this.n().c(s);
        if (this.m() != null) {
            c.l(this.m().p(s));
        }
        return c.a();
    }
    
    public CrashlyticsReport p(final a a) {
        CrashlyticsReport a2;
        if (a == null) {
            a2 = this;
        }
        else {
            a2 = this.n().b(a).a();
        }
        return a2;
    }
    
    public CrashlyticsReport q(final List list) {
        if (this.m() != null) {
            return this.n().l(this.m().q(list)).a();
        }
        throw new IllegalStateException("Reports without sessions cannot have events added to them.");
    }
    
    public CrashlyticsReport r(final String s) {
        return this.n().f(s).a();
    }
    
    public CrashlyticsReport s(final d d) {
        return this.n().l(null).i(d).a();
    }
    
    public CrashlyticsReport t(final long n, final boolean b, final String s) {
        final b n2 = this.n();
        if (this.m() != null) {
            n2.l(this.m().r(n, b, s));
        }
        return n2.a();
    }
    
    public enum Type
    {
        private static final Type[] $VALUES;
        
        INCOMPLETE, 
        JAVA, 
        NATIVE;
    }
    
    public abstract static class a
    {
        public static b a() {
            return (b)new com.google.firebase.crashlytics.internal.model.c.b();
        }
        
        public abstract List b();
        
        public abstract int c();
        
        public abstract int d();
        
        public abstract String e();
        
        public abstract long f();
        
        public abstract int g();
        
        public abstract long h();
        
        public abstract long i();
        
        public abstract String j();
        
        public abstract static class a
        {
            public static CrashlyticsReport.a.a.a a() {
                return (CrashlyticsReport.a.a.a)new com.google.firebase.crashlytics.internal.model.d.b();
            }
            
            public abstract String b();
            
            public abstract String c();
            
            public abstract String d();
            
            public abstract static class a
            {
                public abstract CrashlyticsReport.a.a a();
                
                public abstract a b(final String p0);
                
                public abstract a c(final String p0);
                
                public abstract a d(final String p0);
            }
        }
        
        public abstract static class b
        {
            public abstract CrashlyticsReport.a a();
            
            public abstract b b(final List p0);
            
            public abstract b c(final int p0);
            
            public abstract b d(final int p0);
            
            public abstract b e(final String p0);
            
            public abstract b f(final long p0);
            
            public abstract b g(final int p0);
            
            public abstract b h(final long p0);
            
            public abstract b i(final long p0);
            
            public abstract b j(final String p0);
        }
    }
    
    public abstract static class b
    {
        public abstract CrashlyticsReport a();
        
        public abstract b b(final CrashlyticsReport.a p0);
        
        public abstract b c(final String p0);
        
        public abstract b d(final String p0);
        
        public abstract b e(final String p0);
        
        public abstract b f(final String p0);
        
        public abstract b g(final String p0);
        
        public abstract b h(final String p0);
        
        public abstract b i(final CrashlyticsReport.d p0);
        
        public abstract b j(final int p0);
        
        public abstract b k(final String p0);
        
        public abstract b l(final CrashlyticsReport.e p0);
    }
    
    public abstract static class c
    {
        public static a a() {
            return (a)new e.b();
        }
        
        public abstract String b();
        
        public abstract String c();
        
        public abstract static class a
        {
            public abstract c a();
            
            public abstract a b(final String p0);
            
            public abstract a c(final String p0);
        }
    }
    
    public abstract static class d
    {
        public static a a() {
            return (a)new com.google.firebase.crashlytics.internal.model.f.b();
        }
        
        public abstract List b();
        
        public abstract String c();
        
        public abstract static class a
        {
            public abstract d a();
            
            public abstract a b(final List p0);
            
            public abstract a c(final String p0);
        }
        
        public abstract static class b
        {
            public static a a() {
                return (a)new g.b();
            }
            
            public abstract byte[] b();
            
            public abstract String c();
            
            public abstract static class a
            {
                public abstract b a();
                
                public abstract a b(final byte[] p0);
                
                public abstract a c(final String p0);
            }
        }
    }
    
    public abstract static class e
    {
        public static b a() {
            return new h.b().d(false);
        }
        
        public abstract a b();
        
        public abstract String c();
        
        public abstract c d();
        
        public abstract Long e();
        
        public abstract List f();
        
        public abstract String g();
        
        public abstract int h();
        
        public abstract String i();
        
        public byte[] j() {
            return this.i().getBytes(CrashlyticsReport.a());
        }
        
        public abstract CrashlyticsReport.e.e k();
        
        public abstract long l();
        
        public abstract f m();
        
        public abstract boolean n();
        
        public abstract b o();
        
        public CrashlyticsReport.e p(final String s) {
            return this.o().c(s).a();
        }
        
        public CrashlyticsReport.e q(final List list) {
            return this.o().g(list).a();
        }
        
        public CrashlyticsReport.e r(final long l, final boolean b, final String s) {
            final b o = this.o();
            o.f(l);
            o.d(b);
            if (s != null) {
                o.n(f.a().b(s).a());
            }
            return o.a();
        }
        
        public abstract static class a
        {
            public static CrashlyticsReport.e.a.a a() {
                return (CrashlyticsReport.e.a.a)new i.b();
            }
            
            public abstract String b();
            
            public abstract String c();
            
            public abstract String d();
            
            public abstract String e();
            
            public abstract String f();
            
            public abstract b g();
            
            public abstract String h();
            
            public abstract static class a
            {
                public abstract CrashlyticsReport.e.a a();
                
                public abstract a b(final String p0);
                
                public abstract a c(final String p0);
                
                public abstract a d(final String p0);
                
                public abstract a e(final String p0);
                
                public abstract a f(final String p0);
                
                public abstract a g(final String p0);
            }
            
            public abstract static class b
            {
            }
        }
        
        public abstract static class b
        {
            public abstract CrashlyticsReport.e a();
            
            public abstract b b(final CrashlyticsReport.e.a p0);
            
            public abstract b c(final String p0);
            
            public abstract b d(final boolean p0);
            
            public abstract b e(final CrashlyticsReport.e.c p0);
            
            public abstract b f(final Long p0);
            
            public abstract b g(final List p0);
            
            public abstract b h(final String p0);
            
            public abstract b i(final int p0);
            
            public abstract b j(final String p0);
            
            public b k(final byte[] bytes) {
                return this.j(new String(bytes, CrashlyticsReport.a()));
            }
            
            public abstract b l(final CrashlyticsReport.e.e p0);
            
            public abstract b m(final long p0);
            
            public abstract b n(final CrashlyticsReport.e.f p0);
        }
        
        public abstract static class c
        {
            public static a a() {
                return (a)new k.b();
            }
            
            public abstract int b();
            
            public abstract int c();
            
            public abstract long d();
            
            public abstract String e();
            
            public abstract String f();
            
            public abstract String g();
            
            public abstract long h();
            
            public abstract int i();
            
            public abstract boolean j();
            
            public abstract static class a
            {
                public abstract c a();
                
                public abstract a b(final int p0);
                
                public abstract a c(final int p0);
                
                public abstract a d(final long p0);
                
                public abstract a e(final String p0);
                
                public abstract a f(final String p0);
                
                public abstract a g(final String p0);
                
                public abstract a h(final long p0);
                
                public abstract a i(final boolean p0);
                
                public abstract a j(final int p0);
            }
        }
        
        public abstract static class d
        {
            public static b a() {
                return (b)new l.b();
            }
            
            public abstract a b();
            
            public abstract c c();
            
            public abstract CrashlyticsReport.e.d.d d();
            
            public abstract f e();
            
            public abstract long f();
            
            public abstract String g();
            
            public abstract b h();
            
            public abstract static class a
            {
                public static CrashlyticsReport.e.d.a.a a() {
                    return (CrashlyticsReport.e.d.a.a)new m.b();
                }
                
                public abstract List b();
                
                public abstract Boolean c();
                
                public abstract c d();
                
                public abstract List e();
                
                public abstract b f();
                
                public abstract List g();
                
                public abstract int h();
                
                public abstract CrashlyticsReport.e.d.a.a i();
                
                public abstract static class a
                {
                    public abstract CrashlyticsReport.e.d.a a();
                    
                    public abstract a b(final List p0);
                    
                    public abstract a c(final Boolean p0);
                    
                    public abstract a d(final CrashlyticsReport.e.d.a.c p0);
                    
                    public abstract a e(final List p0);
                    
                    public abstract a f(final CrashlyticsReport.e.d.a.b p0);
                    
                    public abstract a g(final List p0);
                    
                    public abstract a h(final int p0);
                }
                
                public abstract static class b
                {
                    public static CrashlyticsReport.e.d.a.b.b a() {
                        return (CrashlyticsReport.e.d.a.b.b)new n.b();
                    }
                    
                    public abstract CrashlyticsReport.a b();
                    
                    public abstract List c();
                    
                    public abstract c d();
                    
                    public abstract d e();
                    
                    public abstract List f();
                    
                    public abstract static class a
                    {
                        public static CrashlyticsReport.e.d.a.b.a.a a() {
                            return (CrashlyticsReport.e.d.a.b.a.a)new o.b();
                        }
                        
                        public abstract long b();
                        
                        public abstract String c();
                        
                        public abstract long d();
                        
                        public abstract String e();
                        
                        public byte[] f() {
                            final String e = this.e();
                            byte[] bytes;
                            if (e != null) {
                                bytes = e.getBytes(CrashlyticsReport.a());
                            }
                            else {
                                bytes = null;
                            }
                            return bytes;
                        }
                        
                        public abstract static class a
                        {
                            public abstract CrashlyticsReport.e.d.a.b.a a();
                            
                            public abstract a b(final long p0);
                            
                            public abstract a c(final String p0);
                            
                            public abstract a d(final long p0);
                            
                            public abstract a e(final String p0);
                            
                            public a f(final byte[] bytes) {
                                return this.e(new String(bytes, CrashlyticsReport.a()));
                            }
                        }
                    }
                    
                    public abstract static class b
                    {
                        public abstract CrashlyticsReport.e.d.a.b a();
                        
                        public abstract b b(final CrashlyticsReport.a p0);
                        
                        public abstract b c(final List p0);
                        
                        public abstract b d(final c p0);
                        
                        public abstract b e(final d p0);
                        
                        public abstract b f(final List p0);
                    }
                    
                    public abstract static class c
                    {
                        public static a a() {
                            return (a)new p.b();
                        }
                        
                        public abstract c b();
                        
                        public abstract List c();
                        
                        public abstract int d();
                        
                        public abstract String e();
                        
                        public abstract String f();
                        
                        public abstract static class a
                        {
                            public abstract c a();
                            
                            public abstract a b(final c p0);
                            
                            public abstract a c(final List p0);
                            
                            public abstract a d(final int p0);
                            
                            public abstract a e(final String p0);
                            
                            public abstract a f(final String p0);
                        }
                    }
                    
                    public abstract static class d
                    {
                        public static a a() {
                            return (a)new q.b();
                        }
                        
                        public abstract long b();
                        
                        public abstract String c();
                        
                        public abstract String d();
                        
                        public abstract static class a
                        {
                            public abstract d a();
                            
                            public abstract a b(final long p0);
                            
                            public abstract a c(final String p0);
                            
                            public abstract a d(final String p0);
                        }
                    }
                    
                    public abstract static class e
                    {
                        public static a a() {
                            return (a)new r.b();
                        }
                        
                        public abstract List b();
                        
                        public abstract int c();
                        
                        public abstract String d();
                        
                        public abstract static class a
                        {
                            public abstract e a();
                            
                            public abstract a b(final List p0);
                            
                            public abstract a c(final int p0);
                            
                            public abstract a d(final String p0);
                        }
                        
                        public abstract static class b
                        {
                            public static a a() {
                                return (a)new s.b();
                            }
                            
                            public abstract String b();
                            
                            public abstract int c();
                            
                            public abstract long d();
                            
                            public abstract long e();
                            
                            public abstract String f();
                            
                            public abstract static class a
                            {
                                public abstract b a();
                                
                                public abstract a b(final String p0);
                                
                                public abstract a c(final int p0);
                                
                                public abstract a d(final long p0);
                                
                                public abstract a e(final long p0);
                                
                                public abstract a f(final String p0);
                            }
                        }
                    }
                }
                
                public abstract static class c
                {
                    public static a a() {
                        return (a)new t.b();
                    }
                    
                    public abstract int b();
                    
                    public abstract int c();
                    
                    public abstract String d();
                    
                    public abstract boolean e();
                    
                    public abstract static class a
                    {
                        public abstract c a();
                        
                        public abstract a b(final boolean p0);
                        
                        public abstract a c(final int p0);
                        
                        public abstract a d(final int p0);
                        
                        public abstract a e(final String p0);
                    }
                }
            }
            
            public abstract static class b
            {
                public abstract CrashlyticsReport.e.d a();
                
                public abstract b b(final CrashlyticsReport.e.d.a p0);
                
                public abstract b c(final CrashlyticsReport.e.d.c p0);
                
                public abstract b d(final CrashlyticsReport.e.d.d p0);
                
                public abstract b e(final f p0);
                
                public abstract b f(final long p0);
                
                public abstract b g(final String p0);
            }
            
            public abstract static class c
            {
                public static a a() {
                    return (a)new u.b();
                }
                
                public abstract Double b();
                
                public abstract int c();
                
                public abstract long d();
                
                public abstract int e();
                
                public abstract long f();
                
                public abstract boolean g();
                
                public abstract static class a
                {
                    public abstract c a();
                    
                    public abstract a b(final Double p0);
                    
                    public abstract a c(final int p0);
                    
                    public abstract a d(final long p0);
                    
                    public abstract a e(final int p0);
                    
                    public abstract a f(final boolean p0);
                    
                    public abstract a g(final long p0);
                }
            }
            
            public abstract static class d
            {
                public static a a() {
                    return (a)new v.b();
                }
                
                public abstract String b();
                
                public abstract static class a
                {
                    public abstract d a();
                    
                    public abstract a b(final String p0);
                }
            }
            
            public abstract static class e
            {
                public static a a() {
                    return (a)new w.b();
                }
                
                public abstract String b();
                
                public abstract String c();
                
                public abstract b d();
                
                public abstract long e();
                
                public abstract static class a
                {
                    public abstract e a();
                    
                    public abstract a b(final String p0);
                    
                    public abstract a c(final String p0);
                    
                    public abstract a d(final b p0);
                    
                    public abstract a e(final long p0);
                }
                
                public abstract static class b
                {
                    public static a a() {
                        return (a)new x.b();
                    }
                    
                    public abstract String b();
                    
                    public abstract String c();
                    
                    public abstract static class a
                    {
                        public abstract b a();
                        
                        public abstract a b(final String p0);
                        
                        public abstract a c(final String p0);
                    }
                }
            }
            
            public abstract static class f
            {
                public static a a() {
                    return (a)new y.b();
                }
                
                public abstract List b();
                
                public abstract static class a
                {
                    public abstract f a();
                    
                    public abstract a b(final List p0);
                }
            }
        }
        
        public abstract static class e
        {
            public static a a() {
                return (a)new z.b();
            }
            
            public abstract String b();
            
            public abstract int c();
            
            public abstract String d();
            
            public abstract boolean e();
            
            public abstract static class a
            {
                public abstract e a();
                
                public abstract a b(final String p0);
                
                public abstract a c(final boolean p0);
                
                public abstract a d(final int p0);
                
                public abstract a e(final String p0);
            }
        }
        
        public abstract static class f
        {
            public static a a() {
                return (a)new a0.b();
            }
            
            public abstract String b();
            
            public abstract static class a
            {
                public abstract f a();
                
                public abstract a b(final String p0);
            }
        }
    }
}

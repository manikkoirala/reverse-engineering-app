// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.crashlytics.internal.model;

public final class a implements jk
{
    public static final jk a;
    
    static {
        a = new a();
    }
    
    @Override
    public void configure(final zw zw) {
        final d a = d.a;
        zw.a(CrashlyticsReport.class, a);
        zw.a(com.google.firebase.crashlytics.internal.model.b.class, a);
        final j a2 = j.a;
        zw.a(CrashlyticsReport.e.class, a2);
        zw.a(com.google.firebase.crashlytics.internal.model.h.class, a2);
        final g a3 = g.a;
        zw.a(CrashlyticsReport.e.a.class, a3);
        zw.a(com.google.firebase.crashlytics.internal.model.i.class, a3);
        final h a4 = h.a;
        zw.a(CrashlyticsReport.e.a.b.class, a4);
        zw.a(com.google.firebase.crashlytics.internal.model.j.class, a4);
        final z a5 = z.a;
        zw.a(CrashlyticsReport.e.f.class, a5);
        zw.a(a0.class, a5);
        final y a6 = y.a;
        zw.a(CrashlyticsReport.e.e.class, a6);
        zw.a(com.google.firebase.crashlytics.internal.model.z.class, a6);
        final i a7 = i.a;
        zw.a(CrashlyticsReport.e.c.class, a7);
        zw.a(com.google.firebase.crashlytics.internal.model.k.class, a7);
        final t a8 = t.a;
        zw.a(CrashlyticsReport.e.d.class, a8);
        zw.a(com.google.firebase.crashlytics.internal.model.l.class, a8);
        final k a9 = k.a;
        zw.a(CrashlyticsReport.e.d.a.class, a9);
        zw.a(com.google.firebase.crashlytics.internal.model.m.class, a9);
        final m a10 = m.a;
        zw.a(CrashlyticsReport.e.d.a.b.class, a10);
        zw.a(com.google.firebase.crashlytics.internal.model.n.class, a10);
        final p a11 = p.a;
        zw.a(CrashlyticsReport.e.d.a.b.e.class, a11);
        zw.a(com.google.firebase.crashlytics.internal.model.r.class, a11);
        final q a12 = q.a;
        zw.a(CrashlyticsReport.e.d.a.b.e.b.class, a12);
        zw.a(com.google.firebase.crashlytics.internal.model.s.class, a12);
        final n a13 = n.a;
        zw.a(CrashlyticsReport.e.d.a.b.c.class, a13);
        zw.a(com.google.firebase.crashlytics.internal.model.p.class, a13);
        final b a14 = b.a;
        zw.a(CrashlyticsReport.a.class, a14);
        zw.a(com.google.firebase.crashlytics.internal.model.c.class, a14);
        final a a15 = com.google.firebase.crashlytics.internal.model.a.a.a;
        zw.a(CrashlyticsReport.a.a.class, a15);
        zw.a(com.google.firebase.crashlytics.internal.model.d.class, a15);
        final o a16 = o.a;
        zw.a(CrashlyticsReport.e.d.a.b.d.class, a16);
        zw.a(com.google.firebase.crashlytics.internal.model.q.class, a16);
        final l a17 = l.a;
        zw.a(CrashlyticsReport.e.d.a.b.a.class, a17);
        zw.a(com.google.firebase.crashlytics.internal.model.o.class, a17);
        final c a18 = c.a;
        zw.a(CrashlyticsReport.c.class, a18);
        zw.a(com.google.firebase.crashlytics.internal.model.e.class, a18);
        final r a19 = r.a;
        zw.a(CrashlyticsReport.e.d.a.c.class, a19);
        zw.a(com.google.firebase.crashlytics.internal.model.t.class, a19);
        final s a20 = s.a;
        zw.a(CrashlyticsReport.e.d.c.class, a20);
        zw.a(com.google.firebase.crashlytics.internal.model.u.class, a20);
        final u a21 = u.a;
        zw.a(CrashlyticsReport.e.d.d.class, a21);
        zw.a(com.google.firebase.crashlytics.internal.model.v.class, a21);
        final x a22 = x.a;
        zw.a(CrashlyticsReport.e.d.f.class, a22);
        zw.a(com.google.firebase.crashlytics.internal.model.y.class, a22);
        final v a23 = v.a;
        zw.a(CrashlyticsReport.e.d.e.class, a23);
        zw.a(com.google.firebase.crashlytics.internal.model.w.class, a23);
        final w a24 = w.a;
        zw.a(CrashlyticsReport.e.d.e.b.class, a24);
        zw.a(com.google.firebase.crashlytics.internal.model.x.class, a24);
        final e a25 = e.a;
        zw.a(CrashlyticsReport.d.class, a25);
        zw.a(com.google.firebase.crashlytics.internal.model.f.class, a25);
        final f a26 = f.a;
        zw.a(CrashlyticsReport.d.b.class, a26);
        zw.a(com.google.firebase.crashlytics.internal.model.g.class, a26);
    }
    
    public static final class a implements w01
    {
        public static final a a;
        public static final n00 b;
        public static final n00 c;
        public static final n00 d;
        
        static {
            a = new a();
            b = n00.d("arch");
            c = n00.d("libraryName");
            d = n00.d("buildId");
        }
        
        public void a(final CrashlyticsReport.a.a a, final x01 x01) {
            x01.f(a.b, a.b());
            x01.f(a.c, a.d());
            x01.f(a.d, a.c());
        }
    }
    
    public static final class b implements w01
    {
        public static final b a;
        public static final n00 b;
        public static final n00 c;
        public static final n00 d;
        public static final n00 e;
        public static final n00 f;
        public static final n00 g;
        public static final n00 h;
        public static final n00 i;
        public static final n00 j;
        
        static {
            a = new b();
            b = n00.d("pid");
            c = n00.d("processName");
            d = n00.d("reasonCode");
            e = n00.d("importance");
            f = n00.d("pss");
            g = n00.d("rss");
            h = n00.d("timestamp");
            i = n00.d("traceFile");
            j = n00.d("buildIdMappingForArch");
        }
        
        public void a(final CrashlyticsReport.a a, final x01 x01) {
            x01.c(com.google.firebase.crashlytics.internal.model.a.b.b, a.d());
            x01.f(com.google.firebase.crashlytics.internal.model.a.b.c, a.e());
            x01.c(com.google.firebase.crashlytics.internal.model.a.b.d, a.g());
            x01.c(com.google.firebase.crashlytics.internal.model.a.b.e, a.c());
            x01.e(com.google.firebase.crashlytics.internal.model.a.b.f, a.f());
            x01.e(com.google.firebase.crashlytics.internal.model.a.b.g, a.h());
            x01.e(com.google.firebase.crashlytics.internal.model.a.b.h, a.i());
            x01.f(com.google.firebase.crashlytics.internal.model.a.b.i, a.j());
            x01.f(com.google.firebase.crashlytics.internal.model.a.b.j, a.b());
        }
    }
    
    public static final class c implements w01
    {
        public static final c a;
        public static final n00 b;
        public static final n00 c;
        
        static {
            a = new c();
            b = n00.d("key");
            c = n00.d("value");
        }
        
        public void a(final CrashlyticsReport.c c, final x01 x01) {
            x01.f(com.google.firebase.crashlytics.internal.model.a.c.b, c.b());
            x01.f(com.google.firebase.crashlytics.internal.model.a.c.c, c.c());
        }
    }
    
    public static final class d implements w01
    {
        public static final d a;
        public static final n00 b;
        public static final n00 c;
        public static final n00 d;
        public static final n00 e;
        public static final n00 f;
        public static final n00 g;
        public static final n00 h;
        public static final n00 i;
        public static final n00 j;
        public static final n00 k;
        public static final n00 l;
        
        static {
            a = new d();
            b = n00.d("sdkVersion");
            c = n00.d("gmpAppId");
            d = n00.d("platform");
            e = n00.d("installationUuid");
            f = n00.d("firebaseInstallationId");
            g = n00.d("appQualitySessionId");
            h = n00.d("buildVersion");
            i = n00.d("displayVersion");
            j = n00.d("session");
            k = n00.d("ndkPayload");
            l = n00.d("appExitInfo");
        }
        
        public void a(final CrashlyticsReport crashlyticsReport, final x01 x01) {
            x01.f(com.google.firebase.crashlytics.internal.model.a.d.b, crashlyticsReport.l());
            x01.f(com.google.firebase.crashlytics.internal.model.a.d.c, crashlyticsReport.h());
            x01.c(com.google.firebase.crashlytics.internal.model.a.d.d, crashlyticsReport.k());
            x01.f(com.google.firebase.crashlytics.internal.model.a.d.e, crashlyticsReport.i());
            x01.f(com.google.firebase.crashlytics.internal.model.a.d.f, crashlyticsReport.g());
            x01.f(com.google.firebase.crashlytics.internal.model.a.d.g, crashlyticsReport.d());
            x01.f(com.google.firebase.crashlytics.internal.model.a.d.h, crashlyticsReport.e());
            x01.f(com.google.firebase.crashlytics.internal.model.a.d.i, crashlyticsReport.f());
            x01.f(com.google.firebase.crashlytics.internal.model.a.d.j, crashlyticsReport.m());
            x01.f(com.google.firebase.crashlytics.internal.model.a.d.k, crashlyticsReport.j());
            x01.f(com.google.firebase.crashlytics.internal.model.a.d.l, crashlyticsReport.c());
        }
    }
    
    public static final class e implements w01
    {
        public static final e a;
        public static final n00 b;
        public static final n00 c;
        
        static {
            a = new e();
            b = n00.d("files");
            c = n00.d("orgId");
        }
        
        public void a(final CrashlyticsReport.d d, final x01 x01) {
            x01.f(e.b, d.b());
            x01.f(e.c, d.c());
        }
    }
    
    public static final class f implements w01
    {
        public static final f a;
        public static final n00 b;
        public static final n00 c;
        
        static {
            a = new f();
            b = n00.d("filename");
            c = n00.d("contents");
        }
        
        public void a(final CrashlyticsReport.d.b b, final x01 x01) {
            x01.f(f.b, b.c());
            x01.f(f.c, b.b());
        }
    }
    
    public static final class g implements w01
    {
        public static final g a;
        public static final n00 b;
        public static final n00 c;
        public static final n00 d;
        public static final n00 e;
        public static final n00 f;
        public static final n00 g;
        public static final n00 h;
        
        static {
            a = new g();
            b = n00.d("identifier");
            c = n00.d("version");
            d = n00.d("displayVersion");
            e = n00.d("organization");
            f = n00.d("installationUuid");
            g = n00.d("developmentPlatform");
            h = n00.d("developmentPlatformVersion");
        }
        
        public void a(final CrashlyticsReport.e.a a, final x01 x01) {
            x01.f(com.google.firebase.crashlytics.internal.model.a.g.b, a.e());
            x01.f(com.google.firebase.crashlytics.internal.model.a.g.c, a.h());
            x01.f(com.google.firebase.crashlytics.internal.model.a.g.d, a.d());
            final n00 e = com.google.firebase.crashlytics.internal.model.a.g.e;
            a.g();
            x01.f(e, null);
            x01.f(com.google.firebase.crashlytics.internal.model.a.g.f, a.f());
            x01.f(com.google.firebase.crashlytics.internal.model.a.g.g, a.b());
            x01.f(com.google.firebase.crashlytics.internal.model.a.g.h, a.c());
        }
    }
    
    public static final class h implements w01
    {
        public static final h a;
        public static final n00 b;
        
        static {
            a = new h();
            b = n00.d("clsId");
        }
        
        public void a(final CrashlyticsReport.e.a.b b, final x01 x01) {
            throw null;
        }
    }
    
    public static final class i implements w01
    {
        public static final i a;
        public static final n00 b;
        public static final n00 c;
        public static final n00 d;
        public static final n00 e;
        public static final n00 f;
        public static final n00 g;
        public static final n00 h;
        public static final n00 i;
        public static final n00 j;
        
        static {
            a = new i();
            b = n00.d("arch");
            c = n00.d("model");
            d = n00.d("cores");
            e = n00.d("ram");
            f = n00.d("diskSpace");
            g = n00.d("simulator");
            h = n00.d("state");
            i = n00.d("manufacturer");
            j = n00.d("modelClass");
        }
        
        public void a(final CrashlyticsReport.e.c c, final x01 x01) {
            x01.c(com.google.firebase.crashlytics.internal.model.a.i.b, c.b());
            x01.f(com.google.firebase.crashlytics.internal.model.a.i.c, c.f());
            x01.c(com.google.firebase.crashlytics.internal.model.a.i.d, c.c());
            x01.e(com.google.firebase.crashlytics.internal.model.a.i.e, c.h());
            x01.e(com.google.firebase.crashlytics.internal.model.a.i.f, c.d());
            x01.b(com.google.firebase.crashlytics.internal.model.a.i.g, c.j());
            x01.c(com.google.firebase.crashlytics.internal.model.a.i.h, c.i());
            x01.f(com.google.firebase.crashlytics.internal.model.a.i.i, c.e());
            x01.f(com.google.firebase.crashlytics.internal.model.a.i.j, c.g());
        }
    }
    
    public static final class j implements w01
    {
        public static final j a;
        public static final n00 b;
        public static final n00 c;
        public static final n00 d;
        public static final n00 e;
        public static final n00 f;
        public static final n00 g;
        public static final n00 h;
        public static final n00 i;
        public static final n00 j;
        public static final n00 k;
        public static final n00 l;
        public static final n00 m;
        
        static {
            a = new j();
            b = n00.d("generator");
            c = n00.d("identifier");
            d = n00.d("appQualitySessionId");
            e = n00.d("startedAt");
            f = n00.d("endedAt");
            g = n00.d("crashed");
            h = n00.d("app");
            i = n00.d("user");
            j = n00.d("os");
            k = n00.d("device");
            l = n00.d("events");
            m = n00.d("generatorType");
        }
        
        public void a(final CrashlyticsReport.e e, final x01 x01) {
            x01.f(com.google.firebase.crashlytics.internal.model.a.j.b, e.g());
            x01.f(com.google.firebase.crashlytics.internal.model.a.j.c, e.j());
            x01.f(com.google.firebase.crashlytics.internal.model.a.j.d, e.c());
            x01.e(com.google.firebase.crashlytics.internal.model.a.j.e, e.l());
            x01.f(com.google.firebase.crashlytics.internal.model.a.j.f, e.e());
            x01.b(com.google.firebase.crashlytics.internal.model.a.j.g, e.n());
            x01.f(com.google.firebase.crashlytics.internal.model.a.j.h, e.b());
            x01.f(com.google.firebase.crashlytics.internal.model.a.j.i, e.m());
            x01.f(com.google.firebase.crashlytics.internal.model.a.j.j, e.k());
            x01.f(com.google.firebase.crashlytics.internal.model.a.j.k, e.d());
            x01.f(com.google.firebase.crashlytics.internal.model.a.j.l, e.f());
            x01.c(com.google.firebase.crashlytics.internal.model.a.j.m, e.h());
        }
    }
    
    public static final class k implements w01
    {
        public static final k a;
        public static final n00 b;
        public static final n00 c;
        public static final n00 d;
        public static final n00 e;
        public static final n00 f;
        public static final n00 g;
        public static final n00 h;
        
        static {
            a = new k();
            b = n00.d("execution");
            c = n00.d("customAttributes");
            d = n00.d("internalKeys");
            e = n00.d("background");
            f = n00.d("currentProcessDetails");
            g = n00.d("appProcessDetails");
            h = n00.d("uiOrientation");
        }
        
        public void a(final CrashlyticsReport.e.d.a a, final x01 x01) {
            x01.f(k.b, a.f());
            x01.f(k.c, a.e());
            x01.f(k.d, a.g());
            x01.f(k.e, a.c());
            x01.f(k.f, a.d());
            x01.f(k.g, a.b());
            x01.c(k.h, a.h());
        }
    }
    
    public static final class l implements w01
    {
        public static final l a;
        public static final n00 b;
        public static final n00 c;
        public static final n00 d;
        public static final n00 e;
        
        static {
            a = new l();
            b = n00.d("baseAddress");
            c = n00.d("size");
            d = n00.d("name");
            e = n00.d("uuid");
        }
        
        public void a(final CrashlyticsReport.e.d.a.b.a a, final x01 x01) {
            x01.e(l.b, a.b());
            x01.e(l.c, a.d());
            x01.f(l.d, a.c());
            x01.f(l.e, a.f());
        }
    }
    
    public static final class m implements w01
    {
        public static final m a;
        public static final n00 b;
        public static final n00 c;
        public static final n00 d;
        public static final n00 e;
        public static final n00 f;
        
        static {
            a = new m();
            b = n00.d("threads");
            c = n00.d("exception");
            d = n00.d("appExitInfo");
            e = n00.d("signal");
            f = n00.d("binaries");
        }
        
        public void a(final CrashlyticsReport.e.d.a.b b, final x01 x01) {
            x01.f(m.b, b.f());
            x01.f(m.c, b.d());
            x01.f(m.d, b.b());
            x01.f(m.e, b.e());
            x01.f(m.f, b.c());
        }
    }
    
    public static final class n implements w01
    {
        public static final n a;
        public static final n00 b;
        public static final n00 c;
        public static final n00 d;
        public static final n00 e;
        public static final n00 f;
        
        static {
            a = new n();
            b = n00.d("type");
            c = n00.d("reason");
            d = n00.d("frames");
            e = n00.d("causedBy");
            f = n00.d("overflowCount");
        }
        
        public void a(final CrashlyticsReport.e.d.a.b.c c, final x01 x01) {
            x01.f(n.b, c.f());
            x01.f(n.c, c.e());
            x01.f(n.d, c.c());
            x01.f(n.e, c.b());
            x01.c(n.f, c.d());
        }
    }
    
    public static final class o implements w01
    {
        public static final o a;
        public static final n00 b;
        public static final n00 c;
        public static final n00 d;
        
        static {
            a = new o();
            b = n00.d("name");
            c = n00.d("code");
            d = n00.d("address");
        }
        
        public void a(final CrashlyticsReport.e.d.a.b.d d, final x01 x01) {
            x01.f(o.b, d.d());
            x01.f(o.c, d.c());
            x01.e(o.d, d.b());
        }
    }
    
    public static final class p implements w01
    {
        public static final p a;
        public static final n00 b;
        public static final n00 c;
        public static final n00 d;
        
        static {
            a = new p();
            b = n00.d("name");
            c = n00.d("importance");
            d = n00.d("frames");
        }
        
        public void a(final CrashlyticsReport.e.d.a.b.e e, final x01 x01) {
            x01.f(p.b, e.d());
            x01.c(p.c, e.c());
            x01.f(p.d, e.b());
        }
    }
    
    public static final class q implements w01
    {
        public static final q a;
        public static final n00 b;
        public static final n00 c;
        public static final n00 d;
        public static final n00 e;
        public static final n00 f;
        
        static {
            a = new q();
            b = n00.d("pc");
            c = n00.d("symbol");
            d = n00.d("file");
            e = n00.d("offset");
            f = n00.d("importance");
        }
        
        public void a(final CrashlyticsReport.e.d.a.b.e.b b, final x01 x01) {
            x01.e(q.b, b.e());
            x01.f(q.c, b.f());
            x01.f(q.d, b.b());
            x01.e(q.e, b.d());
            x01.c(q.f, b.c());
        }
    }
    
    public static final class r implements w01
    {
        public static final r a;
        public static final n00 b;
        public static final n00 c;
        public static final n00 d;
        public static final n00 e;
        
        static {
            a = new r();
            b = n00.d("processName");
            c = n00.d("pid");
            d = n00.d("importance");
            e = n00.d("defaultProcess");
        }
        
        public void a(final CrashlyticsReport.e.d.a.c c, final x01 x01) {
            x01.f(r.b, c.d());
            x01.c(r.c, c.c());
            x01.c(r.d, c.b());
            x01.b(r.e, c.e());
        }
    }
    
    public static final class s implements w01
    {
        public static final s a;
        public static final n00 b;
        public static final n00 c;
        public static final n00 d;
        public static final n00 e;
        public static final n00 f;
        public static final n00 g;
        
        static {
            a = new s();
            b = n00.d("batteryLevel");
            c = n00.d("batteryVelocity");
            d = n00.d("proximityOn");
            e = n00.d("orientation");
            f = n00.d("ramUsed");
            g = n00.d("diskUsed");
        }
        
        public void a(final CrashlyticsReport.e.d.c c, final x01 x01) {
            x01.f(s.b, c.b());
            x01.c(s.c, c.c());
            x01.b(s.d, c.g());
            x01.c(s.e, c.e());
            x01.e(s.f, c.f());
            x01.e(s.g, c.d());
        }
    }
    
    public static final class t implements w01
    {
        public static final t a;
        public static final n00 b;
        public static final n00 c;
        public static final n00 d;
        public static final n00 e;
        public static final n00 f;
        public static final n00 g;
        
        static {
            a = new t();
            b = n00.d("timestamp");
            c = n00.d("type");
            d = n00.d("app");
            e = n00.d("device");
            f = n00.d("log");
            g = n00.d("rollouts");
        }
        
        public void a(final CrashlyticsReport.e.d d, final x01 x01) {
            x01.e(t.b, d.f());
            x01.f(t.c, d.g());
            x01.f(t.d, d.b());
            x01.f(t.e, d.c());
            x01.f(t.f, d.d());
            x01.f(t.g, d.e());
        }
    }
    
    public static final class u implements w01
    {
        public static final u a;
        public static final n00 b;
        
        static {
            a = new u();
            b = n00.d("content");
        }
        
        public void a(final CrashlyticsReport.e.d.d d, final x01 x01) {
            x01.f(u.b, d.b());
        }
    }
    
    public static final class v implements w01
    {
        public static final v a;
        public static final n00 b;
        public static final n00 c;
        public static final n00 d;
        public static final n00 e;
        
        static {
            a = new v();
            b = n00.d("rolloutVariant");
            c = n00.d("parameterKey");
            d = n00.d("parameterValue");
            e = n00.d("templateVersion");
        }
        
        public void a(final CrashlyticsReport.e.d.e e, final x01 x01) {
            x01.f(v.b, e.d());
            x01.f(v.c, e.b());
            x01.f(v.d, e.c());
            x01.e(v.e, e.e());
        }
    }
    
    public static final class w implements w01
    {
        public static final w a;
        public static final n00 b;
        public static final n00 c;
        
        static {
            a = new w();
            b = n00.d("rolloutId");
            c = n00.d("variantId");
        }
        
        public void a(final CrashlyticsReport.e.d.e.b b, final x01 x01) {
            x01.f(w.b, b.b());
            x01.f(w.c, b.c());
        }
    }
    
    public static final class x implements w01
    {
        public static final x a;
        public static final n00 b;
        
        static {
            a = new x();
            b = n00.d("assignments");
        }
        
        public void a(final CrashlyticsReport.e.d.f f, final x01 x01) {
            x01.f(x.b, f.b());
        }
    }
    
    public static final class y implements w01
    {
        public static final y a;
        public static final n00 b;
        public static final n00 c;
        public static final n00 d;
        public static final n00 e;
        
        static {
            a = new y();
            b = n00.d("platform");
            c = n00.d("version");
            d = n00.d("buildVersion");
            e = n00.d("jailbroken");
        }
        
        public void a(final CrashlyticsReport.e.e e, final x01 x01) {
            x01.c(y.b, e.c());
            x01.f(y.c, e.d());
            x01.f(y.d, e.b());
            x01.b(y.e, e.e());
        }
    }
    
    public static final class z implements w01
    {
        public static final z a;
        public static final n00 b;
        
        static {
            a = new z();
            b = n00.d("identifier");
        }
        
        public void a(final CrashlyticsReport.e.f f, final x01 x01) {
            x01.f(z.b, f.b());
        }
    }
}

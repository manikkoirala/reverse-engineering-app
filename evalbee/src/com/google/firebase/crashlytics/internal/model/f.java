// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.crashlytics.internal.model;

import java.util.List;

public final class f extends d
{
    public final List a;
    public final String b;
    
    public f(final List a, final String b) {
        this.a = a;
        this.b = b;
    }
    
    @Override
    public List b() {
        return this.a;
    }
    
    @Override
    public String c() {
        return this.b;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof d) {
            final d d = (d)o;
            if (this.a.equals(d.b())) {
                final String b2 = this.b;
                final String c = d.c();
                if (b2 == null) {
                    if (c == null) {
                        return b;
                    }
                }
                else if (b2.equals(c)) {
                    return b;
                }
            }
            b = false;
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.a.hashCode();
        final String b = this.b;
        int hashCode2;
        if (b == null) {
            hashCode2 = 0;
        }
        else {
            hashCode2 = b.hashCode();
        }
        return (hashCode ^ 0xF4243) * 1000003 ^ hashCode2;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("FilesPayload{files=");
        sb.append(this.a);
        sb.append(", orgId=");
        sb.append(this.b);
        sb.append("}");
        return sb.toString();
    }
    
    public static final class b extends d.a
    {
        public List a;
        public String b;
        
        @Override
        public d a() {
            final List a = this.a;
            String string = "";
            if (a == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("");
                sb.append(" files");
                string = sb.toString();
            }
            if (string.isEmpty()) {
                return new f(this.a, this.b, null);
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Missing required properties:");
            sb2.append(string);
            throw new IllegalStateException(sb2.toString());
        }
        
        @Override
        public d.a b(final List a) {
            if (a != null) {
                this.a = a;
                return this;
            }
            throw new NullPointerException("Null files");
        }
        
        @Override
        public d.a c(final String b) {
            this.b = b;
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.crashlytics.internal.model;

import java.util.List;

public final class h extends CrashlyticsReport.e
{
    public final String a;
    public final String b;
    public final String c;
    public final long d;
    public final Long e;
    public final boolean f;
    public final CrashlyticsReport.e.a g;
    public final CrashlyticsReport.e.f h;
    public final CrashlyticsReport.e.e i;
    public final CrashlyticsReport.e.c j;
    public final List k;
    public final int l;
    
    public h(final String a, final String b, final String c, final long d, final Long e, final boolean f, final CrashlyticsReport.e.a g, final CrashlyticsReport.e.f h, final CrashlyticsReport.e.e i, final CrashlyticsReport.e.c j, final List k, final int l) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = h;
        this.i = i;
        this.j = j;
        this.k = k;
        this.l = l;
    }
    
    @Override
    public CrashlyticsReport.e.a b() {
        return this.g;
    }
    
    @Override
    public String c() {
        return this.c;
    }
    
    @Override
    public CrashlyticsReport.e.c d() {
        return this.j;
    }
    
    @Override
    public Long e() {
        return this.e;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof CrashlyticsReport.e) {
            final CrashlyticsReport.e e = (CrashlyticsReport.e)o;
            if (this.a.equals(e.g()) && this.b.equals(e.i())) {
                final String c = this.c;
                if (c == null) {
                    if (e.c() != null) {
                        return false;
                    }
                }
                else if (!c.equals(e.c())) {
                    return false;
                }
                if (this.d == e.l()) {
                    final Long e2 = this.e;
                    if (e2 == null) {
                        if (e.e() != null) {
                            return false;
                        }
                    }
                    else if (!e2.equals(e.e())) {
                        return false;
                    }
                    if (this.f == e.n() && this.g.equals(e.b())) {
                        final CrashlyticsReport.e.f h = this.h;
                        if (h == null) {
                            if (e.m() != null) {
                                return false;
                            }
                        }
                        else if (!h.equals(e.m())) {
                            return false;
                        }
                        final CrashlyticsReport.e.e i = this.i;
                        if (i == null) {
                            if (e.k() != null) {
                                return false;
                            }
                        }
                        else if (!i.equals(e.k())) {
                            return false;
                        }
                        final CrashlyticsReport.e.c j = this.j;
                        if (j == null) {
                            if (e.d() != null) {
                                return false;
                            }
                        }
                        else if (!j.equals(e.d())) {
                            return false;
                        }
                        final List k = this.k;
                        if (k == null) {
                            if (e.f() != null) {
                                return false;
                            }
                        }
                        else if (!k.equals(e.f())) {
                            return false;
                        }
                        if (this.l == e.h()) {
                            return b;
                        }
                    }
                }
            }
            b = false;
            return b;
        }
        return false;
    }
    
    @Override
    public List f() {
        return this.k;
    }
    
    @Override
    public String g() {
        return this.a;
    }
    
    @Override
    public int h() {
        return this.l;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.a.hashCode();
        final int hashCode2 = this.b.hashCode();
        final String c = this.c;
        int hashCode3 = 0;
        int hashCode4;
        if (c == null) {
            hashCode4 = 0;
        }
        else {
            hashCode4 = c.hashCode();
        }
        final long d = this.d;
        final int n = (int)(d ^ d >>> 32);
        final Long e = this.e;
        int hashCode5;
        if (e == null) {
            hashCode5 = 0;
        }
        else {
            hashCode5 = e.hashCode();
        }
        int n2;
        if (this.f) {
            n2 = 1231;
        }
        else {
            n2 = 1237;
        }
        final int hashCode6 = this.g.hashCode();
        final CrashlyticsReport.e.f h = this.h;
        int hashCode7;
        if (h == null) {
            hashCode7 = 0;
        }
        else {
            hashCode7 = h.hashCode();
        }
        final CrashlyticsReport.e.e i = this.i;
        int hashCode8;
        if (i == null) {
            hashCode8 = 0;
        }
        else {
            hashCode8 = i.hashCode();
        }
        final CrashlyticsReport.e.c j = this.j;
        int hashCode9;
        if (j == null) {
            hashCode9 = 0;
        }
        else {
            hashCode9 = j.hashCode();
        }
        final List k = this.k;
        if (k != null) {
            hashCode3 = k.hashCode();
        }
        return (((((((((((hashCode ^ 0xF4243) * 1000003 ^ hashCode2) * 1000003 ^ hashCode4) * 1000003 ^ n) * 1000003 ^ hashCode5) * 1000003 ^ n2) * 1000003 ^ hashCode6) * 1000003 ^ hashCode7) * 1000003 ^ hashCode8) * 1000003 ^ hashCode9) * 1000003 ^ hashCode3) * 1000003 ^ this.l;
    }
    
    @Override
    public String i() {
        return this.b;
    }
    
    @Override
    public CrashlyticsReport.e.e k() {
        return this.i;
    }
    
    @Override
    public long l() {
        return this.d;
    }
    
    @Override
    public CrashlyticsReport.e.f m() {
        return this.h;
    }
    
    @Override
    public boolean n() {
        return this.f;
    }
    
    @Override
    public CrashlyticsReport.e.b o() {
        return new b(this, null);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Session{generator=");
        sb.append(this.a);
        sb.append(", identifier=");
        sb.append(this.b);
        sb.append(", appQualitySessionId=");
        sb.append(this.c);
        sb.append(", startedAt=");
        sb.append(this.d);
        sb.append(", endedAt=");
        sb.append(this.e);
        sb.append(", crashed=");
        sb.append(this.f);
        sb.append(", app=");
        sb.append(this.g);
        sb.append(", user=");
        sb.append(this.h);
        sb.append(", os=");
        sb.append(this.i);
        sb.append(", device=");
        sb.append(this.j);
        sb.append(", events=");
        sb.append(this.k);
        sb.append(", generatorType=");
        sb.append(this.l);
        sb.append("}");
        return sb.toString();
    }
    
    public static final class b extends CrashlyticsReport.e.b
    {
        public String a;
        public String b;
        public String c;
        public Long d;
        public Long e;
        public Boolean f;
        public CrashlyticsReport.e.a g;
        public CrashlyticsReport.e.f h;
        public CrashlyticsReport.e.e i;
        public CrashlyticsReport.e.c j;
        public List k;
        public Integer l;
        
        public b() {
        }
        
        public b(final CrashlyticsReport.e e) {
            this.a = e.g();
            this.b = e.i();
            this.c = e.c();
            this.d = e.l();
            this.e = e.e();
            this.f = e.n();
            this.g = e.b();
            this.h = e.m();
            this.i = e.k();
            this.j = e.d();
            this.k = e.f();
            this.l = e.h();
        }
        
        @Override
        public CrashlyticsReport.e a() {
            final String a = this.a;
            String string = "";
            if (a == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("");
                sb.append(" generator");
                string = sb.toString();
            }
            String string2 = string;
            if (this.b == null) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(string);
                sb2.append(" identifier");
                string2 = sb2.toString();
            }
            String string3 = string2;
            if (this.d == null) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(string2);
                sb3.append(" startedAt");
                string3 = sb3.toString();
            }
            String string4 = string3;
            if (this.f == null) {
                final StringBuilder sb4 = new StringBuilder();
                sb4.append(string3);
                sb4.append(" crashed");
                string4 = sb4.toString();
            }
            String string5 = string4;
            if (this.g == null) {
                final StringBuilder sb5 = new StringBuilder();
                sb5.append(string4);
                sb5.append(" app");
                string5 = sb5.toString();
            }
            String string6 = string5;
            if (this.l == null) {
                final StringBuilder sb6 = new StringBuilder();
                sb6.append(string5);
                sb6.append(" generatorType");
                string6 = sb6.toString();
            }
            if (string6.isEmpty()) {
                return new h(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, null);
            }
            final StringBuilder sb7 = new StringBuilder();
            sb7.append("Missing required properties:");
            sb7.append(string6);
            throw new IllegalStateException(sb7.toString());
        }
        
        @Override
        public CrashlyticsReport.e.b b(final CrashlyticsReport.e.a g) {
            if (g != null) {
                this.g = g;
                return this;
            }
            throw new NullPointerException("Null app");
        }
        
        @Override
        public CrashlyticsReport.e.b c(final String c) {
            this.c = c;
            return this;
        }
        
        @Override
        public CrashlyticsReport.e.b d(final boolean b) {
            this.f = b;
            return this;
        }
        
        @Override
        public CrashlyticsReport.e.b e(final CrashlyticsReport.e.c j) {
            this.j = j;
            return this;
        }
        
        @Override
        public CrashlyticsReport.e.b f(final Long e) {
            this.e = e;
            return this;
        }
        
        @Override
        public CrashlyticsReport.e.b g(final List k) {
            this.k = k;
            return this;
        }
        
        @Override
        public CrashlyticsReport.e.b h(final String a) {
            if (a != null) {
                this.a = a;
                return this;
            }
            throw new NullPointerException("Null generator");
        }
        
        @Override
        public CrashlyticsReport.e.b i(final int i) {
            this.l = i;
            return this;
        }
        
        @Override
        public CrashlyticsReport.e.b j(final String b) {
            if (b != null) {
                this.b = b;
                return this;
            }
            throw new NullPointerException("Null identifier");
        }
        
        @Override
        public CrashlyticsReport.e.b l(final CrashlyticsReport.e.e i) {
            this.i = i;
            return this;
        }
        
        @Override
        public CrashlyticsReport.e.b m(final long l) {
            this.d = l;
            return this;
        }
        
        @Override
        public CrashlyticsReport.e.b n(final CrashlyticsReport.e.f h) {
            this.h = h;
            return this;
        }
    }
}

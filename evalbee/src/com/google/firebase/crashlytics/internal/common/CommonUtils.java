// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.crashlytics.internal.common;

import android.text.TextUtils;
import java.util.HashMap;
import java.util.Map;
import java.io.File;
import android.os.Build;
import android.os.Debug;
import java.security.NoSuchAlgorithmException;
import java.security.MessageDigest;
import android.content.SharedPreferences;
import android.content.res.Resources$NotFoundException;
import android.hardware.SensorManager;
import android.content.res.Resources;
import java.util.Iterator;
import java.util.List;
import java.util.Collections;
import java.util.Locale;
import java.util.ArrayList;
import java.io.IOException;
import java.io.Closeable;
import android.net.NetworkInfo;
import android.net.ConnectivityManager;
import android.os.StatFs;
import android.app.ActivityManager;
import android.app.ActivityManager$MemoryInfo;
import android.content.Context;
import java.util.Scanner;
import java.io.InputStream;

public abstract class CommonUtils
{
    public static final char[] a;
    
    static {
        a = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
    }
    
    public static String A(final InputStream source) {
        final Scanner useDelimiter = new Scanner(source).useDelimiter("\\A");
        String next;
        if (useDelimiter.hasNext()) {
            next = useDelimiter.next();
        }
        else {
            next = "";
        }
        return next;
    }
    
    public static long a(final Context context) {
        final ActivityManager$MemoryInfo activityManager$MemoryInfo = new ActivityManager$MemoryInfo();
        ((ActivityManager)context.getSystemService("activity")).getMemoryInfo(activityManager$MemoryInfo);
        return activityManager$MemoryInfo.availMem;
    }
    
    public static long b(final Context context) {
        synchronized (CommonUtils.class) {
            final ActivityManager$MemoryInfo activityManager$MemoryInfo = new ActivityManager$MemoryInfo();
            ((ActivityManager)context.getSystemService("activity")).getMemoryInfo(activityManager$MemoryInfo);
            return activityManager$MemoryInfo.totalMem;
        }
    }
    
    public static long c(final String s) {
        final StatFs statFs = new StatFs(s);
        final long n = statFs.getBlockSize();
        return statFs.getBlockCount() * n - n * statFs.getAvailableBlocks();
    }
    
    public static boolean d(final Context context) {
        final boolean e = e(context, "android.permission.ACCESS_NETWORK_STATE");
        boolean b = true;
        if (e) {
            final NetworkInfo activeNetworkInfo = ((ConnectivityManager)context.getSystemService("connectivity")).getActiveNetworkInfo();
            b = (activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting() && b);
        }
        return b;
    }
    
    public static boolean e(final Context context, final String s) {
        return context.checkCallingOrSelfPermission(s) == 0;
    }
    
    public static void f(final Closeable closeable, final String s) {
        if (closeable != null) {
            try {
                closeable.close();
            }
            catch (final IOException ex) {
                zl0.f().e(s, ex);
            }
        }
    }
    
    public static void g(final Closeable closeable) {
        if (closeable == null) {
            goto Label_0016;
        }
        try {
            closeable.close();
            goto Label_0016;
        }
        catch (final RuntimeException ex) {
            throw ex;
        }
        catch (final Exception ex2) {
            goto Label_0016;
        }
    }
    
    public static String h(final String... array) {
        String z;
        final String s = z = null;
        if (array != null) {
            if (array.length == 0) {
                z = s;
            }
            else {
                final ArrayList list = new ArrayList();
                for (final String s2 : array) {
                    if (s2 != null) {
                        list.add(s2.replace("-", "").toLowerCase(Locale.US));
                    }
                }
                Collections.sort((List<Comparable>)list);
                final StringBuilder sb = new StringBuilder();
                final Iterator iterator = list.iterator();
                while (iterator.hasNext()) {
                    sb.append((String)iterator.next());
                }
                final String string = sb.toString();
                z = s;
                if (string.length() > 0) {
                    z = z(string);
                }
            }
        }
        return z;
    }
    
    public static boolean i(final Context context, final String s, final boolean b) {
        if (context != null) {
            final Resources resources = context.getResources();
            if (resources != null) {
                final int p3 = p(context, s, "bool");
                if (p3 > 0) {
                    return resources.getBoolean(p3);
                }
                final int p4 = p(context, s, "string");
                if (p4 > 0) {
                    return Boolean.parseBoolean(context.getString(p4));
                }
            }
        }
        return b;
    }
    
    public static List j(final Context context) {
        final ArrayList list = new ArrayList();
        final int p = p(context, "com.google.firebase.crashlytics.build_ids_lib", "array");
        final int p2 = p(context, "com.google.firebase.crashlytics.build_ids_arch", "array");
        final int p3 = p(context, "com.google.firebase.crashlytics.build_ids_build_id", "array");
        if (p == 0 || p2 == 0 || p3 == 0) {
            zl0.f().b(String.format("Could not find resources: %d %d %d", p, p2, p3));
            return list;
        }
        final String[] stringArray = context.getResources().getStringArray(p);
        final String[] stringArray2 = context.getResources().getStringArray(p2);
        final String[] stringArray3 = context.getResources().getStringArray(p3);
        if (stringArray.length == stringArray3.length && stringArray2.length == stringArray3.length) {
            for (int i = 0; i < stringArray3.length; ++i) {
                list.add(new zc(stringArray[i], stringArray2[i], stringArray3[i]));
            }
            return list;
        }
        zl0.f().b(String.format("Lengths did not match: %d %d %d", stringArray.length, stringArray2.length, stringArray3.length));
        return list;
    }
    
    public static int k() {
        return Architecture.getValue().ordinal();
    }
    
    public static int l() {
        int w;
        final boolean b = (w = (w() ? 1 : 0)) != 0;
        if (x()) {
            w = ((b ? 1 : 0) | 0x2);
        }
        int n = w;
        if (v()) {
            n = (w | 0x4);
        }
        return n;
    }
    
    public static String m(final Context context) {
        int n;
        if ((n = p(context, "com.google.firebase.crashlytics.mapping_file_id", "string")) == 0) {
            n = p(context, "com.crashlytics.android.build_id", "string");
        }
        String string;
        if (n != 0) {
            string = context.getResources().getString(n);
        }
        else {
            string = null;
        }
        return string;
    }
    
    public static boolean n(final Context context) {
        final boolean w = w();
        boolean b = false;
        if (w) {
            return false;
        }
        if (((SensorManager)context.getSystemService("sensor")).getDefaultSensor(8) != null) {
            b = true;
        }
        return b;
    }
    
    public static String o(final Context context) {
        final int icon = context.getApplicationContext().getApplicationInfo().icon;
        Label_0044: {
            if (icon <= 0) {
                break Label_0044;
            }
            try {
                String s;
                if ("android".equals(s = context.getResources().getResourcePackageName(icon))) {
                    s = context.getPackageName();
                }
                return s;
                s = context.getPackageName();
                return s;
            }
            catch (final Resources$NotFoundException ex) {
                return context.getPackageName();
            }
        }
    }
    
    public static int p(final Context context, final String s, final String s2) {
        return context.getResources().getIdentifier(s, s2, o(context));
    }
    
    public static SharedPreferences q(final Context context) {
        return context.getSharedPreferences("com.google.firebase.crashlytics", 0);
    }
    
    public static String r(final String s, final String s2) {
        return s(s.getBytes(), s2);
    }
    
    public static String s(final byte[] input, final String s) {
        try {
            final MessageDigest instance = MessageDigest.getInstance(s);
            instance.update(input);
            return t(instance.digest());
        }
        catch (final NoSuchAlgorithmException ex) {
            final zl0 f = zl0.f();
            final StringBuilder sb = new StringBuilder();
            sb.append("Could not create hashing algorithm: ");
            sb.append(s);
            sb.append(", returning empty string.");
            f.e(sb.toString(), ex);
            return "";
        }
    }
    
    public static String t(final byte[] array) {
        final char[] value = new char[array.length * 2];
        for (int i = 0; i < array.length; ++i) {
            final int n = array[i] & 0xFF;
            final int n2 = i * 2;
            final char[] a = CommonUtils.a;
            value[n2] = a[n >>> 4];
            value[n2 + 1] = a[n & 0xF];
        }
        return new String(value);
    }
    
    public static boolean u(final Context context) {
        return (context.getApplicationInfo().flags & 0x2) != 0x0;
    }
    
    public static boolean v() {
        return Debug.isDebuggerConnected() || Debug.waitingForDebugger();
    }
    
    public static boolean w() {
        if (!Build.PRODUCT.contains("sdk")) {
            final String hardware = Build.HARDWARE;
            if (!hardware.contains("goldfish")) {
                if (!hardware.contains("ranchu")) {
                    return false;
                }
            }
        }
        return true;
    }
    
    public static boolean x() {
        final boolean w = w();
        final String tags = Build.TAGS;
        if (!w && tags != null && tags.contains("test-keys")) {
            return true;
        }
        if (new File("/system/app/Superuser.apk").exists()) {
            return true;
        }
        final File file = new File("/system/xbin/su");
        return !w && file.exists();
    }
    
    public static boolean y(final String s, final String anObject) {
        if (s == null) {
            return anObject == null;
        }
        return s.equals(anObject);
    }
    
    public static String z(final String s) {
        return r(s, "SHA-1");
    }
    
    public enum Architecture
    {
        private static final Architecture[] $VALUES;
        
        ARM64, 
        ARMV6, 
        ARMV7, 
        ARMV7S, 
        ARM_UNKNOWN, 
        PPC, 
        PPC64, 
        UNKNOWN, 
        X86_32, 
        X86_64;
        
        private static final Map<String, Architecture> matcher;
        
        static {
            final Architecture architecture;
            final Architecture architecture2;
            final Architecture architecture3;
            final Architecture architecture4;
            final Architecture architecture5;
            final Architecture architecture6;
            final Architecture architecture7;
            final Architecture architecture8;
            final Architecture architecture9;
            final Architecture architecture10;
            $VALUES = new Architecture[] { architecture, architecture2, architecture3, architecture4, architecture5, architecture6, architecture7, architecture8, architecture9, architecture10 };
            final HashMap matcher2 = new HashMap(4);
            (matcher = matcher2).put("armeabi-v7a", architecture7);
            matcher2.put("armeabi", architecture6);
            matcher2.put("arm64-v8a", architecture10);
            matcher2.put("x86", architecture);
        }
        
        public static Architecture getValue() {
            final String cpu_ABI = Build.CPU_ABI;
            if (TextUtils.isEmpty((CharSequence)cpu_ABI)) {
                zl0.f().i("Architecture#getValue()::Build.CPU_ABI returned null or empty");
                return Architecture.UNKNOWN;
            }
            Architecture unknown;
            if ((unknown = Architecture.matcher.get(cpu_ABI.toLowerCase(Locale.US))) == null) {
                unknown = Architecture.UNKNOWN;
            }
            return unknown;
        }
    }
}

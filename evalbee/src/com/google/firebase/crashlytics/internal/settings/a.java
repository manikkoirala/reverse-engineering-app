// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.crashlytics.internal.settings;

import android.content.SharedPreferences$Editor;
import com.google.android.gms.tasks.SuccessContinuation;
import com.google.android.gms.tasks.Tasks;
import java.util.concurrent.Executor;
import com.google.android.gms.tasks.Task;
import java.util.Locale;
import com.google.firebase.crashlytics.internal.common.DeliveryMechanism;
import com.google.firebase.crashlytics.internal.common.CommonUtils;
import org.json.JSONObject;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.concurrent.atomic.AtomicReference;
import android.content.Context;

public class a implements zm1
{
    public final Context a;
    public final an1 b;
    public final wm1 c;
    public final io d;
    public final qe e;
    public final bn1 f;
    public final dp g;
    public final AtomicReference h;
    public final AtomicReference i;
    
    public a(final Context a, final an1 b, final io d, final wm1 c, final qe e, final bn1 f, final dp g) {
        final AtomicReference h = new AtomicReference();
        this.h = h;
        this.i = new AtomicReference((V)new TaskCompletionSource());
        this.a = a;
        this.b = b;
        this.d = d;
        this.c = c;
        this.e = e;
        this.f = f;
        this.g = g;
        h.set(br.b(d));
    }
    
    public static /* synthetic */ an1 c(final a a) {
        return a.b;
    }
    
    public static /* synthetic */ bn1 d(final a a) {
        return a.f;
    }
    
    public static /* synthetic */ wm1 e(final a a) {
        return a.c;
    }
    
    public static /* synthetic */ qe f(final a a) {
        return a.e;
    }
    
    public static /* synthetic */ AtomicReference i(final a a) {
        return a.h;
    }
    
    public static /* synthetic */ AtomicReference j(final a a) {
        return a.i;
    }
    
    public static a l(final Context context, final String s, final de0 de0, final jd0 jd0, final String s2, final String s3, final z00 z00, final dp dp) {
        final String g = de0.g();
        final et1 et1 = new et1();
        return new a(context, new an1(s, de0.h(), de0.i(), de0.j(), de0, CommonUtils.h(CommonUtils.m(context), s, s3, s2), s3, s2, DeliveryMechanism.determineFrom(g).getId()), et1, new wm1(et1), new qe(z00), new cr(String.format(Locale.US, "https://firebase-settings.crashlytics.com/spi/v2/platforms/android/gmp/%s/settings", s), jd0), dp);
    }
    
    @Override
    public vm1 a() {
        return this.h.get();
    }
    
    @Override
    public Task b() {
        return this.i.get().getTask();
    }
    
    public boolean k() {
        return this.n().equals(this.b.f) ^ true;
    }
    
    public final vm1 m(final SettingsCacheBehavior settingsCacheBehavior) {
        final vm1 vm1 = null;
        final vm1 vm2 = null;
        vm1 b = vm1;
        Label_0156: {
            try {
                if (SettingsCacheBehavior.SKIP_CACHE_LOOKUP.equals(settingsCacheBehavior)) {
                    return b;
                }
                final JSONObject b2 = this.e.b();
                if (b2 != null) {
                    b = this.c.b(b2);
                    if (b != null) {
                        this.q(b2, "Loaded cached settings: ");
                        final long a = this.d.a();
                        if (!SettingsCacheBehavior.IGNORE_CACHE_EXPIRATION.equals(settingsCacheBehavior)) {
                            if (b.a(a)) {
                                zl0.f().i("Cached settings have expired.");
                                b = vm1;
                                return b;
                            }
                        }
                        try {
                            zl0.f().i("Returning cached settings.");
                            return b;
                        }
                        catch (final Exception ex) {
                            break Label_0156;
                        }
                    }
                    zl0.f().e("Failed to parse cached settings data.", null);
                    b = vm1;
                    return b;
                }
                zl0.f().b("No cached settings data found.");
                b = vm1;
                return b;
            }
            catch (final Exception ex) {
                b = vm2;
            }
        }
        final Exception ex;
        zl0.f().e("Failed to get cached settings", ex);
        return b;
    }
    
    public final String n() {
        return CommonUtils.q(this.a).getString("existing_instance_identifier", "");
    }
    
    public Task o(final SettingsCacheBehavior settingsCacheBehavior, final Executor executor) {
        if (!this.k()) {
            final vm1 m = this.m(settingsCacheBehavior);
            if (m != null) {
                this.h.set(m);
                this.i.get().trySetResult((Object)m);
                return Tasks.forResult((Object)null);
            }
        }
        final vm1 i = this.m(SettingsCacheBehavior.IGNORE_CACHE_EXPIRATION);
        if (i != null) {
            this.h.set(i);
            this.i.get().trySetResult((Object)i);
        }
        return this.g.i(executor).onSuccessTask(executor, (SuccessContinuation)new SuccessContinuation(this) {
            public final a a;
            
            public Task a(final Void void1) {
                final JSONObject a = com.google.firebase.crashlytics.internal.settings.a.d(this.a).a(com.google.firebase.crashlytics.internal.settings.a.c(this.a), true);
                if (a != null) {
                    final vm1 b = com.google.firebase.crashlytics.internal.settings.a.e(this.a).b(a);
                    com.google.firebase.crashlytics.internal.settings.a.f(this.a).c(b.c, a);
                    this.a.q(a, "Loaded settings: ");
                    final a a2 = this.a;
                    a2.r(com.google.firebase.crashlytics.internal.settings.a.c(a2).f);
                    com.google.firebase.crashlytics.internal.settings.a.i(this.a).set(b);
                    com.google.firebase.crashlytics.internal.settings.a.j(this.a).get().trySetResult((Object)b);
                }
                return Tasks.forResult((Object)null);
            }
        });
    }
    
    public Task p(final Executor executor) {
        return this.o(SettingsCacheBehavior.USE_CACHE, executor);
    }
    
    public final void q(final JSONObject jsonObject, final String str) {
        final zl0 f = zl0.f();
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(jsonObject.toString());
        f.b(sb.toString());
    }
    
    public final boolean r(final String s) {
        final SharedPreferences$Editor edit = CommonUtils.q(this.a).edit();
        edit.putString("existing_instance_identifier", s);
        edit.apply();
        return true;
    }
}

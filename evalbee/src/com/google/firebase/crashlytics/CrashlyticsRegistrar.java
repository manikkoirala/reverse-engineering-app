// 
// Decompiled by Procyon v0.6.0
// 

package com.google.firebase.crashlytics;

import java.util.Arrays;
import java.util.List;
import com.google.firebase.sessions.api.SessionSubscriber;
import com.google.firebase.sessions.api.FirebaseSessionsDependencies;
import com.google.firebase.components.ComponentRegistrar;

public class CrashlyticsRegistrar implements ComponentRegistrar
{
    static {
        FirebaseSessionsDependencies.a.a(SessionSubscriber.Name.CRASHLYTICS);
    }
    
    public final j20 b(final gj gj) {
        return j20.a((r10)gj.a(r10.class), (r20)gj.a(r20.class), gj.i(dn.class), gj.i(g4.class), gj.i(f30.class));
    }
    
    @Override
    public List getComponents() {
        return Arrays.asList(zi.e(j20.class).h("fire-cls").b(os.k(r10.class)).b(os.k(r20.class)).b(os.a(dn.class)).b(os.a(g4.class)).b(os.a(f30.class)).f(new in(this)).e().d(), mj0.b("fire-cls", "18.6.0"));
    }
}

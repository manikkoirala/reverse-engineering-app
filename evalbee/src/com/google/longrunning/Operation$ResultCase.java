// 
// Decompiled by Procyon v0.6.0
// 

package com.google.longrunning;

public enum Operation$ResultCase
{
    private static final Operation$ResultCase[] $VALUES;
    
    ERROR(4), 
    RESPONSE(5), 
    RESULT_NOT_SET(0);
    
    private final int value;
    
    private Operation$ResultCase(final int value) {
        this.value = value;
    }
    
    public static Operation$ResultCase forNumber(final int n) {
        if (n == 0) {
            return Operation$ResultCase.RESULT_NOT_SET;
        }
        if (n == 4) {
            return Operation$ResultCase.ERROR;
        }
        if (n != 5) {
            return null;
        }
        return Operation$ResultCase.RESPONSE;
    }
    
    @Deprecated
    public static Operation$ResultCase valueOf(final int n) {
        return forNumber(n);
    }
    
    public int getNumber() {
        return this.value;
    }
}

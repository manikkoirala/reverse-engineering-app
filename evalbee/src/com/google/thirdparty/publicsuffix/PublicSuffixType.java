// 
// Decompiled by Procyon v0.6.0
// 

package com.google.thirdparty.publicsuffix;

public enum PublicSuffixType
{
    private static final PublicSuffixType[] $VALUES;
    
    PRIVATE(':', ','), 
    REGISTRY('!', '?');
    
    private final char innerNodeCode;
    private final char leafNodeCode;
    
    private static /* synthetic */ PublicSuffixType[] $values() {
        return new PublicSuffixType[] { PublicSuffixType.PRIVATE, PublicSuffixType.REGISTRY };
    }
    
    static {
        $VALUES = $values();
    }
    
    private PublicSuffixType(final char innerNodeCode, final char leafNodeCode) {
        this.innerNodeCode = innerNodeCode;
        this.leafNodeCode = leafNodeCode;
    }
    
    public static PublicSuffixType fromCode(final char c) {
        for (final PublicSuffixType publicSuffixType : values()) {
            if (publicSuffixType.getInnerNodeCode() == c || publicSuffixType.getLeafNodeCode() == c) {
                return publicSuffixType;
            }
        }
        final StringBuilder sb = new StringBuilder(38);
        sb.append("No enum corresponding to given code: ");
        sb.append(c);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public char getInnerNodeCode() {
        return this.innerNodeCode;
    }
    
    public char getLeafNodeCode() {
        return this.leafNodeCode;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.ads;

public enum AdRequest$ErrorCode
{
    INTERNAL_ERROR("INTERNAL_ERROR", 3, "There was an internal error."), 
    INVALID_REQUEST("INVALID_REQUEST", 0, "Invalid Ad request."), 
    NETWORK_ERROR("NETWORK_ERROR", 2, "A network error occurred."), 
    NO_FILL("NO_FILL", 1, "Ad request successful, but no ad returned due to lack of ad inventory.");
    
    private static final AdRequest$ErrorCode[] zza;
    private final String zzb;
    
    private AdRequest$ErrorCode(final String name, final int ordinal, final String zzb) {
        this.zzb = zzb;
    }
    
    @Override
    public String toString() {
        return this.zzb;
    }
}

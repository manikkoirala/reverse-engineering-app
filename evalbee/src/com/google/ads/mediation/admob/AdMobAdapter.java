// 
// Decompiled by Procyon v0.6.0
// 

package com.google.ads.mediation.admob;

import android.os.BaseBundle;
import android.text.TextUtils;
import android.os.Bundle;
import androidx.annotation.Keep;
import com.google.ads.mediation.AbstractAdViewAdapter;

@Keep
public final class AdMobAdapter extends AbstractAdViewAdapter
{
    static final String AD_JSON_PARAMETER = "adJson";
    static final String AD_PARAMETER = "_ad";
    static final String HOUSE_ADS_PARAMETER = "mad_hac";
    public static final String NEW_BUNDLE = "_newBundle";
    
    @Override
    public Bundle buildExtrasBundle(Bundle o, final Bundle bundle) {
        Object o2 = o;
        if (o == null) {
            o2 = new Bundle();
        }
        o = o2;
        if (((BaseBundle)o2).getBoolean("_newBundle")) {
            o = new Bundle((Bundle)o2);
        }
        ((BaseBundle)o).putInt("gw", 1);
        ((BaseBundle)o).putString("mad_hac", ((BaseBundle)bundle).getString("mad_hac"));
        if (!TextUtils.isEmpty((CharSequence)((BaseBundle)bundle).getString("adJson"))) {
            ((BaseBundle)o).putString("_ad", ((BaseBundle)bundle).getString("adJson"));
        }
        ((BaseBundle)o).putBoolean("_noRefresh", true);
        return (Bundle)o;
    }
}

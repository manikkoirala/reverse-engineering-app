// 
// Decompiled by Procyon v0.6.0
// 

package com.google.ads.mediation;

import android.os.BaseBundle;
import android.app.Activity;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.mediation.NativeMediationAdRequest;
import com.google.android.gms.ads.mediation.MediationNativeListener;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.gms.ads.mediation.MediationInterstitialListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.mediation.MediationBannerListener;
import com.google.android.gms.ads.internal.client.zzdq;
import android.view.View;
import java.util.Iterator;
import java.util.Set;
import java.util.Date;
import com.google.android.gms.ads.mediation.MediationExtrasReceiver;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.internal.ads.zzcbg;
import com.google.android.gms.ads.internal.client.zzay;
import com.google.android.gms.ads.AdRequest;
import android.os.Bundle;
import com.google.android.gms.ads.mediation.MediationAdRequest;
import android.content.Context;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.mediation.zza;
import com.google.android.gms.ads.mediation.OnImmersiveModeUpdatedListener;
import com.google.android.gms.ads.mediation.MediationNativeAdapter;
import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.mediation.MediationBannerAdapter;

public abstract class AbstractAdViewAdapter implements MediationBannerAdapter, MediationInterstitialAdapter, MediationNativeAdapter, OnImmersiveModeUpdatedListener, zza
{
    public static final String AD_UNIT_ID_PARAMETER = "pubid";
    private AdLoader adLoader;
    protected AdView mAdView;
    protected InterstitialAd mInterstitialAd;
    
    public AdRequest buildAdRequest(final Context context, final MediationAdRequest mediationAdRequest, final Bundle bundle, final Bundle bundle2) {
        final AdRequest.Builder builder = new AdRequest.Builder();
        final Date birthday = mediationAdRequest.getBirthday();
        if (birthday != null) {
            builder.zzb(birthday);
        }
        final int gender = mediationAdRequest.getGender();
        if (gender != 0) {
            builder.zzc(gender);
        }
        final Set<String> keywords = mediationAdRequest.getKeywords();
        if (keywords != null) {
            final Iterator<String> iterator = keywords.iterator();
            while (iterator.hasNext()) {
                builder.addKeyword(iterator.next());
            }
        }
        if (mediationAdRequest.isTesting()) {
            zzay.zzb();
            builder.zza(zzcbg.zzy(context));
        }
        if (mediationAdRequest.taggedForChildDirectedTreatment() != -1) {
            final int taggedForChildDirectedTreatment = mediationAdRequest.taggedForChildDirectedTreatment();
            boolean b = true;
            if (taggedForChildDirectedTreatment != 1) {
                b = false;
            }
            builder.zze(b);
        }
        builder.zzd(mediationAdRequest.isDesignedForFamilies());
        builder.addNetworkExtrasBundle(AdMobAdapter.class, this.buildExtrasBundle(bundle, bundle2));
        return builder.build();
    }
    
    public abstract Bundle buildExtrasBundle(final Bundle p0, final Bundle p1);
    
    public String getAdUnitId(final Bundle bundle) {
        return ((BaseBundle)bundle).getString("pubid");
    }
    
    @Override
    public View getBannerView() {
        return (View)this.mAdView;
    }
    
    public InterstitialAd getInterstitialAd() {
        return this.mInterstitialAd;
    }
    
    @Override
    public zzdq getVideoController() {
        final AdView mAdView = this.mAdView;
        if (mAdView != null) {
            return mAdView.zza().zza();
        }
        return null;
    }
    
    public AdLoader.Builder newAdLoader(final Context context, final String s) {
        return new AdLoader.Builder(context, s);
    }
    
    @Override
    public void onDestroy() {
        final AdView mAdView = this.mAdView;
        if (mAdView != null) {
            mAdView.destroy();
            this.mAdView = null;
        }
        if (this.mInterstitialAd != null) {
            this.mInterstitialAd = null;
        }
        if (this.adLoader != null) {
            this.adLoader = null;
        }
    }
    
    @Override
    public void onImmersiveModeUpdated(final boolean immersiveMode) {
        final InterstitialAd mInterstitialAd = this.mInterstitialAd;
        if (mInterstitialAd != null) {
            mInterstitialAd.setImmersiveMode(immersiveMode);
        }
    }
    
    @Override
    public void onPause() {
        final AdView mAdView = this.mAdView;
        if (mAdView != null) {
            mAdView.pause();
        }
    }
    
    @Override
    public void onResume() {
        final AdView mAdView = this.mAdView;
        if (mAdView != null) {
            mAdView.resume();
        }
    }
    
    @Override
    public void requestBannerAd(final Context context, final MediationBannerListener mediationBannerListener, final Bundle bundle, final AdSize adSize, final MediationAdRequest mediationAdRequest, final Bundle bundle2) {
        (this.mAdView = new AdView(context)).setAdSize(new AdSize(adSize.getWidth(), adSize.getHeight()));
        this.mAdView.setAdUnitId(this.getAdUnitId(bundle));
        this.mAdView.setAdListener(new oc2(this, mediationBannerListener));
        this.mAdView.loadAd(this.buildAdRequest(context, mediationAdRequest, bundle2, bundle));
    }
    
    @Override
    public void requestInterstitialAd(final Context context, final MediationInterstitialListener mediationInterstitialListener, final Bundle bundle, final MediationAdRequest mediationAdRequest, final Bundle bundle2) {
        InterstitialAd.load(context, this.getAdUnitId(bundle), this.buildAdRequest(context, mediationAdRequest, bundle2, bundle), new a(this, mediationInterstitialListener));
    }
    
    @Override
    public void requestNativeAd(final Context context, final MediationNativeListener mediationNativeListener, final Bundle bundle, final NativeMediationAdRequest nativeMediationAdRequest, final Bundle bundle2) {
        final hf2 hf2 = new hf2(this, mediationNativeListener);
        final AdLoader.Builder withAdListener = this.newAdLoader(context, ((BaseBundle)bundle).getString("pubid")).withAdListener(hf2);
        withAdListener.withNativeAdOptions(nativeMediationAdRequest.getNativeAdOptions());
        withAdListener.withNativeAdOptions(nativeMediationAdRequest.getNativeAdRequestOptions());
        if (nativeMediationAdRequest.isUnifiedNativeAdRequested()) {
            withAdListener.forUnifiedNativeAd(hf2);
        }
        if (nativeMediationAdRequest.zzb()) {
            for (final String s : nativeMediationAdRequest.zza().keySet()) {
                NativeCustomTemplateAd.OnCustomClickListener onCustomClickListener;
                if (!(boolean)nativeMediationAdRequest.zza().get(s)) {
                    onCustomClickListener = null;
                }
                else {
                    onCustomClickListener = hf2;
                }
                withAdListener.forCustomTemplateAd(s, hf2, onCustomClickListener);
            }
        }
        (this.adLoader = withAdListener.build()).loadAd(this.buildAdRequest(context, nativeMediationAdRequest, bundle2, bundle));
    }
    
    @Override
    public void showInterstitial() {
        final InterstitialAd mInterstitialAd = this.mInterstitialAd;
        if (mInterstitialAd != null) {
            mInterstitialAd.show(null);
        }
    }
}

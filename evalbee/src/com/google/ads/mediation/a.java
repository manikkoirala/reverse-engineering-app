// 
// Decompiled by Procyon v0.6.0
// 

package com.google.ads.mediation;

import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.mediation.MediationInterstitialListener;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;

public final class a extends InterstitialAdLoadCallback
{
    public final AbstractAdViewAdapter a;
    public final MediationInterstitialListener b;
    
    public a(final AbstractAdViewAdapter a, final MediationInterstitialListener b) {
        this.a = a;
        this.b = b;
    }
    
    @Override
    public final void onAdFailedToLoad(final LoadAdError loadAdError) {
        this.b.onAdFailedToLoad(this.a, loadAdError);
    }
}

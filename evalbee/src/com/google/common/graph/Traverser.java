// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.graph;

import java.util.Deque;

public abstract class Traverser
{
    public enum InsertionOrder
    {
        private static final InsertionOrder[] $VALUES;
        
        BACK {
            @Override
            public <T> void insertInto(final Deque<T> deque, final T t) {
                deque.addLast(t);
            }
        }, 
        FRONT {
            @Override
            public <T> void insertInto(final Deque<T> deque, final T t) {
                deque.addFirst(t);
            }
        };
        
        private static /* synthetic */ InsertionOrder[] $values() {
            return new InsertionOrder[] { InsertionOrder.FRONT, InsertionOrder.BACK };
        }
        
        static {
            $VALUES = $values();
        }
        
        public abstract <T> void insertInto(final Deque<T> p0, final T p1);
    }
}

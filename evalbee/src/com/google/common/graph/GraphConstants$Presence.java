// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.graph;

enum GraphConstants$Presence
{
    private static final GraphConstants$Presence[] $VALUES;
    
    EDGE_EXISTS;
    
    private static /* synthetic */ GraphConstants$Presence[] $values() {
        return new GraphConstants$Presence[] { GraphConstants$Presence.EDGE_EXISTS };
    }
    
    static {
        $VALUES = $values();
    }
}

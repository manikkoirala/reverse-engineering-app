// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.graph;

public enum ElementOrder$Type
{
    private static final ElementOrder$Type[] $VALUES;
    
    INSERTION, 
    SORTED, 
    STABLE, 
    UNORDERED;
    
    private static /* synthetic */ ElementOrder$Type[] $values() {
        return new ElementOrder$Type[] { ElementOrder$Type.UNORDERED, ElementOrder$Type.STABLE, ElementOrder$Type.INSERTION, ElementOrder$Type.SORTED };
    }
    
    static {
        $VALUES = $values();
    }
}

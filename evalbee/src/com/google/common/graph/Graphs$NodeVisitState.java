// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.graph;

enum Graphs$NodeVisitState
{
    private static final Graphs$NodeVisitState[] $VALUES;
    
    COMPLETE, 
    PENDING;
    
    private static /* synthetic */ Graphs$NodeVisitState[] $values() {
        return new Graphs$NodeVisitState[] { Graphs$NodeVisitState.PENDING, Graphs$NodeVisitState.COMPLETE };
    }
    
    static {
        $VALUES = $values();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.util.concurrent;

import java.util.concurrent.Semaphore;

class Striped$PaddedSemaphore extends Semaphore
{
    long unused1;
    long unused2;
    long unused3;
    
    public Striped$PaddedSemaphore(final int permits) {
        super(permits, false);
    }
}

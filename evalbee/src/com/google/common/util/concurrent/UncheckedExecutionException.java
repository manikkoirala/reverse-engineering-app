// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.util.concurrent;

public class UncheckedExecutionException extends RuntimeException
{
    private static final long serialVersionUID = 0L;
    
    public UncheckedExecutionException() {
    }
    
    public UncheckedExecutionException(final String message) {
        super(message);
    }
    
    public UncheckedExecutionException(final String message, final Throwable cause) {
        super(message, cause);
    }
    
    public UncheckedExecutionException(final Throwable cause) {
        super(cause);
    }
}

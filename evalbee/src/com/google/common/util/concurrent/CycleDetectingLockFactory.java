// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.util.concurrent;

import java.util.Objects;
import java.util.logging.Level;
import com.google.common.collect.ImmutableSet;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import com.google.common.collect.Lists;
import java.util.ArrayList;
import com.google.common.collect.MapMaker;
import java.util.logging.Logger;
import java.util.concurrent.ConcurrentMap;

public abstract class CycleDetectingLockFactory
{
    public static final ConcurrentMap a;
    public static final Logger b;
    public static final ThreadLocal c;
    
    static {
        a = new MapMaker().l().i();
        b = Logger.getLogger(CycleDetectingLockFactory.class.getName());
        c = new ThreadLocal() {
            public ArrayList a() {
                return Lists.l(3);
            }
        };
    }
    
    public static /* synthetic */ Logger a() {
        return CycleDetectingLockFactory.b;
    }
    
    public static /* synthetic */ void b(final CycleDetectingLockFactory cycleDetectingLockFactory, final b b) {
        throw null;
    }
    
    public static void d(final b b) {
        if (!b.isAcquiredByCurrentThread()) {
            final ArrayList list = CycleDetectingLockFactory.c.get();
            b.getLockGraphNode();
            for (int i = list.size() - 1; i >= 0; --i) {
                if (list.get(i) == null) {
                    list.remove(i);
                    break;
                }
            }
        }
    }
    
    public final class CycleDetectingReentrantLock extends ReentrantLock implements b
    {
        private final c lockGraphNode;
        final CycleDetectingLockFactory this$0;
        
        private CycleDetectingReentrantLock(final CycleDetectingLockFactory cycleDetectingLockFactory, final c c, final boolean fair) {
            super(fair);
            zu0.a(i71.r(c));
        }
        
        @Override
        public c getLockGraphNode() {
            return null;
        }
        
        @Override
        public boolean isAcquiredByCurrentThread() {
            return this.isHeldByCurrentThread();
        }
        
        @Override
        public void lock() {
            CycleDetectingLockFactory.b(null, (b)this);
            try {
                super.lock();
            }
            finally {
                d((b)this);
            }
        }
        
        @Override
        public void lockInterruptibly() {
            CycleDetectingLockFactory.b(null, (b)this);
            try {
                super.lockInterruptibly();
            }
            finally {
                d((b)this);
            }
        }
        
        @Override
        public boolean tryLock() {
            CycleDetectingLockFactory.b(null, (b)this);
            try {
                return super.tryLock();
            }
            finally {
                d((b)this);
            }
        }
        
        @Override
        public boolean tryLock(final long timeout, final TimeUnit unit) {
            CycleDetectingLockFactory.b(null, (b)this);
            try {
                return super.tryLock(timeout, unit);
            }
            finally {
                d((b)this);
            }
        }
        
        @Override
        public void unlock() {
            try {
                super.unlock();
            }
            finally {
                d((b)this);
            }
        }
    }
    
    public class CycleDetectingReentrantReadLock extends ReadLock
    {
        final CycleDetectingReentrantReadWriteLock readWriteLock;
        final CycleDetectingLockFactory this$0;
        
        public CycleDetectingReentrantReadLock(final CycleDetectingLockFactory cycleDetectingLockFactory, final CycleDetectingReentrantReadWriteLock cycleDetectingReentrantReadWriteLock) {
            super(cycleDetectingReentrantReadWriteLock);
            this.readWriteLock = cycleDetectingReentrantReadWriteLock;
        }
        
        @Override
        public void lock() {
            CycleDetectingLockFactory.b(null, (b)this.readWriteLock);
            try {
                super.lock();
            }
            finally {
                d((b)this.readWriteLock);
            }
        }
        
        @Override
        public void lockInterruptibly() {
            CycleDetectingLockFactory.b(null, (b)this.readWriteLock);
            try {
                super.lockInterruptibly();
            }
            finally {
                d((b)this.readWriteLock);
            }
        }
        
        @Override
        public boolean tryLock() {
            CycleDetectingLockFactory.b(null, (b)this.readWriteLock);
            try {
                return super.tryLock();
            }
            finally {
                d((b)this.readWriteLock);
            }
        }
        
        @Override
        public boolean tryLock(final long timeout, final TimeUnit unit) {
            CycleDetectingLockFactory.b(null, (b)this.readWriteLock);
            try {
                return super.tryLock(timeout, unit);
            }
            finally {
                d((b)this.readWriteLock);
            }
        }
        
        @Override
        public void unlock() {
            try {
                super.unlock();
            }
            finally {
                d((b)this.readWriteLock);
            }
        }
    }
    
    public final class CycleDetectingReentrantReadWriteLock extends ReentrantReadWriteLock implements b
    {
        private final c lockGraphNode;
        private final CycleDetectingReentrantReadLock readLock;
        private final CycleDetectingReentrantWriteLock writeLock;
        
        private CycleDetectingReentrantReadWriteLock(final CycleDetectingLockFactory cycleDetectingLockFactory, final c c, final boolean fair) {
            super(fair);
            this.readLock = cycleDetectingLockFactory.new CycleDetectingReentrantReadLock(this);
            this.writeLock = cycleDetectingLockFactory.new CycleDetectingReentrantWriteLock(this);
            zu0.a(i71.r(c));
        }
        
        @Override
        public c getLockGraphNode() {
            return null;
        }
        
        @Override
        public boolean isAcquiredByCurrentThread() {
            return this.isWriteLockedByCurrentThread() || this.getReadHoldCount() > 0;
        }
        
        @Override
        public ReadLock readLock() {
            return this.readLock;
        }
        
        @Override
        public WriteLock writeLock() {
            return this.writeLock;
        }
    }
    
    public class CycleDetectingReentrantWriteLock extends WriteLock
    {
        final CycleDetectingReentrantReadWriteLock readWriteLock;
        final CycleDetectingLockFactory this$0;
        
        public CycleDetectingReentrantWriteLock(final CycleDetectingLockFactory cycleDetectingLockFactory, final CycleDetectingReentrantReadWriteLock cycleDetectingReentrantReadWriteLock) {
            super(cycleDetectingReentrantReadWriteLock);
            this.readWriteLock = cycleDetectingReentrantReadWriteLock;
        }
        
        @Override
        public void lock() {
            CycleDetectingLockFactory.b(null, (b)this.readWriteLock);
            try {
                super.lock();
            }
            finally {
                d((b)this.readWriteLock);
            }
        }
        
        @Override
        public void lockInterruptibly() {
            CycleDetectingLockFactory.b(null, (b)this.readWriteLock);
            try {
                super.lockInterruptibly();
            }
            finally {
                d((b)this.readWriteLock);
            }
        }
        
        @Override
        public boolean tryLock() {
            CycleDetectingLockFactory.b(null, (b)this.readWriteLock);
            try {
                return super.tryLock();
            }
            finally {
                d((b)this.readWriteLock);
            }
        }
        
        @Override
        public boolean tryLock(final long timeout, final TimeUnit unit) {
            CycleDetectingLockFactory.b(null, (b)this.readWriteLock);
            try {
                return super.tryLock(timeout, unit);
            }
            finally {
                d((b)this.readWriteLock);
            }
        }
        
        @Override
        public void unlock() {
            try {
                super.unlock();
            }
            finally {
                d((b)this.readWriteLock);
            }
        }
    }
    
    public static class ExampleStackTrace extends IllegalStateException
    {
        static final StackTraceElement[] EMPTY_STACK_TRACE;
        static final ImmutableSet<String> EXCLUDED_CLASS_NAMES;
        
        static {
            EMPTY_STACK_TRACE = new StackTraceElement[0];
            EXCLUDED_CLASS_NAMES = ImmutableSet.of(CycleDetectingLockFactory.class.getName(), ExampleStackTrace.class.getName(), c.class.getName());
        }
        
        public ExampleStackTrace(final c c, final c c2) {
            throw null;
        }
    }
    
    public enum Policies
    {
        private static final Policies[] $VALUES;
        
        DISABLED {
            @Override
            public void handlePotentialDeadlock(final PotentialDeadlockException ex) {
            }
        }, 
        THROW {
            @Override
            public void handlePotentialDeadlock(final PotentialDeadlockException ex) {
                throw ex;
            }
        }, 
        WARN {
            @Override
            public void handlePotentialDeadlock(final PotentialDeadlockException thrown) {
                CycleDetectingLockFactory.a().log(Level.SEVERE, "Detected potential deadlock", thrown);
            }
        };
        
        private static /* synthetic */ Policies[] $values() {
            return new Policies[] { Policies.THROW, Policies.WARN, Policies.DISABLED };
        }
        
        static {
            $VALUES = $values();
        }
    }
    
    public static final class PotentialDeadlockException extends ExampleStackTrace
    {
        private final ExampleStackTrace conflictingStackTrace;
        
        private PotentialDeadlockException(final c c, final c c2, final ExampleStackTrace conflictingStackTrace) {
            super(c, c2);
            this.initCause(this.conflictingStackTrace = conflictingStackTrace);
        }
        
        public ExampleStackTrace getConflictingStackTrace() {
            return this.conflictingStackTrace;
        }
        
        @Override
        public String getMessage() {
            final String message = super.getMessage();
            Objects.requireNonNull(message);
            final StringBuilder sb = new StringBuilder(message);
            for (Throwable t = this.conflictingStackTrace; t != null; t = t.getCause()) {
                sb.append(", ");
                sb.append(t.getMessage());
            }
            return sb.toString();
        }
    }
    
    public interface b
    {
        c getLockGraphNode();
        
        boolean isAcquiredByCurrentThread();
    }
    
    public abstract static class c
    {
    }
}

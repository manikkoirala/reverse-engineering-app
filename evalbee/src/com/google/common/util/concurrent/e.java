// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.util.concurrent;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.Executor;

public abstract class e extends jc0
{
    public static void a(final ik0 ik0, final fa0 fa0, final Executor executor) {
        i71.r(fa0);
        ik0.addListener(new a(ik0, fa0), executor);
    }
    
    public static Object b(final Future future) {
        i71.B(future.isDone(), "Future was expected to be done: %s", future);
        return t02.a(future);
    }
    
    public static ik0 c(final Throwable t) {
        i71.r(t);
        return new f.a(t);
    }
    
    public static ik0 d(final Object o) {
        if (o == null) {
            return f.b;
        }
        return new f(o);
    }
    
    public static ik0 e(final ik0 ik0, final m90 m90, final Executor executor) {
        return com.google.common.util.concurrent.a.n(ik0, m90, executor);
    }
    
    public static final class a implements Runnable
    {
        public final Future a;
        public final fa0 b;
        
        public a(final Future a, final fa0 b) {
            this.a = a;
            this.b = b;
        }
        
        @Override
        public void run() {
            Object o = this.a;
            if (o instanceof ag0) {
                o = bg0.a((ag0)o);
                if (o != null) {
                    this.b.onFailure((Throwable)o);
                    return;
                }
            }
            try {
                o = e.b(this.a);
                this.b.onSuccess(o);
            }
            catch (final Error o) {
                goto Label_0059;
            }
            catch (final RuntimeException ex) {}
            catch (final ExecutionException ex2) {
                this.b.onFailure(ex2.getCause());
            }
        }
        
        @Override
        public String toString() {
            return com.google.common.base.a.c(this).k(this.b).toString();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.util.concurrent;

import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.util.concurrent.atomic.AtomicLong;
import java.io.Serializable;

public class AtomicDouble extends Number implements Serializable
{
    private static final long serialVersionUID = 0L;
    private transient AtomicLong value;
    
    public AtomicDouble() {
        this(0.0);
    }
    
    public AtomicDouble(final double n) {
        this.value = new AtomicLong(Double.doubleToRawLongBits(n));
    }
    
    private void readObject(final ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        this.value = new AtomicLong();
        this.set(objectInputStream.readDouble());
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeDouble(this.get());
    }
    
    public final double addAndGet(final double n) {
        long value;
        double n2;
        do {
            value = this.value.get();
            n2 = Double.longBitsToDouble(value) + n;
        } while (!this.value.compareAndSet(value, Double.doubleToRawLongBits(n2)));
        return n2;
    }
    
    public final boolean compareAndSet(final double n, final double n2) {
        return this.value.compareAndSet(Double.doubleToRawLongBits(n), Double.doubleToRawLongBits(n2));
    }
    
    @Override
    public double doubleValue() {
        return this.get();
    }
    
    @Override
    public float floatValue() {
        return (float)this.get();
    }
    
    public final double get() {
        return Double.longBitsToDouble(this.value.get());
    }
    
    public final double getAndAdd(final double n) {
        long value;
        double longBitsToDouble;
        do {
            value = this.value.get();
            longBitsToDouble = Double.longBitsToDouble(value);
        } while (!this.value.compareAndSet(value, Double.doubleToRawLongBits(longBitsToDouble + n)));
        return longBitsToDouble;
    }
    
    public final double getAndSet(final double n) {
        return Double.longBitsToDouble(this.value.getAndSet(Double.doubleToRawLongBits(n)));
    }
    
    @Override
    public int intValue() {
        return (int)this.get();
    }
    
    public final void lazySet(final double n) {
        this.value.lazySet(Double.doubleToRawLongBits(n));
    }
    
    @Override
    public long longValue() {
        return (long)this.get();
    }
    
    public final void set(final double n) {
        this.value.set(Double.doubleToRawLongBits(n));
    }
    
    @Override
    public String toString() {
        return Double.toString(this.get());
    }
    
    public final boolean weakCompareAndSet(final double n, final double n2) {
        return this.value.weakCompareAndSet(Double.doubleToRawLongBits(n), Double.doubleToRawLongBits(n2));
    }
}

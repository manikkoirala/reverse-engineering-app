// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.util.concurrent;

import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;

public abstract class CombinedFuture extends AggregateFuture
{
    public static /* synthetic */ CombinedFutureInterruptibleTask n(final CombinedFuture combinedFuture, final CombinedFutureInterruptibleTask combinedFutureInterruptibleTask) {
        throw null;
    }
    
    public final class AsyncCallableInterruptibleTask extends CombinedFutureInterruptibleTask<ik0>
    {
        private final h9 callable;
        final CombinedFuture this$0;
        
        public AsyncCallableInterruptibleTask(final CombinedFuture combinedFuture, final h9 h9, final Executor executor) {
            combinedFuture.super(executor);
            zu0.a(i71.r(h9));
        }
        
        @Override
        public ik0 runInterruptibly() {
            throw null;
        }
        
        public void setValue(final ik0 ik0) {
            throw null;
        }
        
        @Override
        public String toPendingString() {
            throw null;
        }
    }
    
    public final class CallableInterruptibleTask extends CombinedFutureInterruptibleTask<Object>
    {
        private final Callable<Object> callable;
        final CombinedFuture this$0;
        
        public CallableInterruptibleTask(final CombinedFuture combinedFuture, final Callable<Object> callable, final Executor executor) {
            combinedFuture.super(executor);
            this.callable = (Callable)i71.r(callable);
        }
        
        @Override
        public Object runInterruptibly() {
            return this.callable.call();
        }
        
        @Override
        public void setValue(final Object o) {
            throw null;
        }
        
        @Override
        public String toPendingString() {
            return this.callable.toString();
        }
    }
    
    public abstract class CombinedFutureInterruptibleTask<T> extends InterruptibleTask<T>
    {
        private final Executor listenerExecutor;
        final CombinedFuture this$0;
        
        public CombinedFutureInterruptibleTask(final CombinedFuture combinedFuture, final Executor executor) {
            this.listenerExecutor = (Executor)i71.r(executor);
        }
        
        @Override
        public final void afterRanInterruptiblyFailure(final Throwable t) {
            CombinedFuture.n(null, null);
            if (!(t instanceof ExecutionException)) {
                final boolean b = t instanceof CancellationException;
                throw null;
            }
            t.getCause();
            throw null;
        }
        
        @Override
        public final void afterRanInterruptiblySuccess(final T value) {
            CombinedFuture.n(null, null);
            this.setValue(value);
        }
        
        public final void execute() {
            try {
                this.listenerExecutor.execute(this);
            }
            catch (final RejectedExecutionException ex) {
                throw null;
            }
        }
        
        @Override
        public final boolean isDone() {
            throw null;
        }
        
        public abstract void setValue(final T p0);
    }
}

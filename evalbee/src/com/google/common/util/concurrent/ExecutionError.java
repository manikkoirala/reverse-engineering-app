// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.util.concurrent;

public class ExecutionError extends Error
{
    private static final long serialVersionUID = 0L;
    
    public ExecutionError() {
    }
    
    public ExecutionError(final Error cause) {
        super(cause);
    }
    
    public ExecutionError(final String message) {
        super(message);
    }
    
    public ExecutionError(final String message, final Error cause) {
        super(message, cause);
    }
}

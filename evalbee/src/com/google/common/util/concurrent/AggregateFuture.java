// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.util.concurrent;

import java.util.logging.Logger;

public abstract class AggregateFuture extends b
{
    public static final Logger e;
    
    static {
        e = Logger.getLogger(AggregateFuture.class.getName());
    }
    
    public enum ReleaseResourcesReason
    {
        private static final ReleaseResourcesReason[] $VALUES;
        
        ALL_INPUT_FUTURES_PROCESSED, 
        OUTPUT_FUTURE_DONE;
        
        private static /* synthetic */ ReleaseResourcesReason[] $values() {
            return new ReleaseResourcesReason[] { ReleaseResourcesReason.OUTPUT_FUTURE_DONE, ReleaseResourcesReason.ALL_INPUT_FUTURES_PROCESSED };
        }
        
        static {
            $VALUES = $values();
        }
    }
}

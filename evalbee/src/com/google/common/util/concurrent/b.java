// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.util.concurrent;

import java.util.logging.Level;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.Set;
import java.util.logging.Logger;

public abstract class b extends i
{
    public static final b c;
    public static final Logger d;
    public volatile Set a;
    public volatile int b;
    
    static {
        d = Logger.getLogger(b.class.getName());
        d c3;
        try {
            final c c2 = new c(AtomicReferenceFieldUpdater.newUpdater(b.class, Set.class, "a"), AtomicIntegerFieldUpdater.newUpdater(b.class, "b"));
        }
        finally {
            c3 = new d(null);
        }
        c = (b)c3;
        final Throwable thrown;
        if (thrown != null) {
            b.d.log(Level.SEVERE, "SafeAtomicHelper is broken!", thrown);
        }
    }
    
    public abstract static class b
    {
    }
    
    public static final class c extends b
    {
        public final AtomicReferenceFieldUpdater a;
        public final AtomicIntegerFieldUpdater b;
        
        public c(final AtomicReferenceFieldUpdater a, final AtomicIntegerFieldUpdater b) {
            super(null);
            this.a = a;
            this.b = b;
        }
    }
    
    public static final class d extends b
    {
        public d() {
            super(null);
        }
    }
}

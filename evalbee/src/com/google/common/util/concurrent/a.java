// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.util.concurrent;

import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.Executor;

public abstract class a extends a implements Runnable
{
    public ik0 a;
    public Object b;
    
    public a(final ik0 ik0, final Object o) {
        this.a = (ik0)i71.r(ik0);
        this.b = i71.r(o);
    }
    
    public static ik0 n(final ik0 ik0, final m90 m90, final Executor executor) {
        i71.r(m90);
        final a a = new a(ik0, m90);
        ik0.addListener(a, com.google.common.util.concurrent.g.b(executor, a));
        return a;
    }
    
    @Override
    public final void afterDone() {
        this.maybePropagateCancellationTo(this.a);
        this.a = null;
        this.b = null;
    }
    
    public abstract Object o(final Object p0, final Object p1);
    
    public abstract void p(final Object p0);
    
    @Override
    public String pendingToString() {
        final ik0 a = this.a;
        final Object b = this.b;
        final String pendingToString = super.pendingToString();
        String string;
        if (a != null) {
            final String value = String.valueOf(a);
            final StringBuilder sb = new StringBuilder(value.length() + 16);
            sb.append("inputFuture=[");
            sb.append(value);
            sb.append("], ");
            string = sb.toString();
        }
        else {
            string = "";
        }
        if (b != null) {
            final String value2 = String.valueOf(b);
            final StringBuilder sb2 = new StringBuilder(String.valueOf(string).length() + 11 + value2.length());
            sb2.append(string);
            sb2.append("function=[");
            sb2.append(value2);
            sb2.append("]");
            return sb2.toString();
        }
        if (pendingToString != null) {
            final String value3 = String.valueOf(string);
            String concat;
            if (pendingToString.length() != 0) {
                concat = value3.concat(pendingToString);
            }
            else {
                concat = new String(value3);
            }
            return concat;
        }
        return null;
    }
    
    @Override
    public final void run() {
        final ik0 a = this.a;
        final Object b = this.b;
        final boolean cancelled = ((a)this).isCancelled();
        boolean b2 = true;
        final boolean b3 = a == null;
        if (b != null) {
            b2 = false;
        }
        if (cancelled | b3 | b2) {
            return;
        }
        this.a = null;
        if (a.isCancelled()) {
            this.setFuture(a);
            return;
        }
        try {
            final Object b4 = com.google.common.util.concurrent.e.b(a);
            try {
                final Object o = this.o(b, b4);
                this.b = null;
                this.p(o);
            }
            finally {
                try {
                    final Throwable exception;
                    this.setException(exception);
                }
                finally {
                    this.b = null;
                }
            }
        }
        catch (final Error exception2) {
            this.setException(exception2);
        }
        catch (final RuntimeException exception3) {
            this.setException(exception3);
        }
        catch (final ExecutionException ex) {
            this.setException(ex.getCause());
        }
        catch (final CancellationException ex2) {
            ((a)this).cancel(false);
        }
    }
    
    public static final class a extends com.google.common.util.concurrent.a
    {
        public a(final ik0 ik0, final m90 m90) {
            super(ik0, m90);
        }
        
        @Override
        public void p(final Object o) {
            this.set(o);
        }
        
        public Object q(final m90 m90, final Object o) {
            return m90.apply(o);
        }
    }
}

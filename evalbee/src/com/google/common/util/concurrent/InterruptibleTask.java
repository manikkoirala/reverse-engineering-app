// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.util.concurrent;

import java.util.concurrent.locks.AbstractOwnableSynchronizer;
import java.util.concurrent.locks.LockSupport;
import java.util.concurrent.atomic.AtomicReference;

abstract class InterruptibleTask<T> extends AtomicReference<Runnable> implements Runnable
{
    private static final Runnable DONE;
    private static final int MAX_BUSY_WAIT_SPINS = 1000;
    private static final Runnable PARKED;
    
    static {
        DONE = new b(null);
        PARKED = new b(null);
    }
    
    public InterruptibleTask() {
    }
    
    private void waitForInterrupt(final Thread thread) {
        Runnable expectedValue = this.get();
        Object blocker = null;
        int n = 0;
        int n2 = 0;
        while (true) {
            final boolean b = expectedValue instanceof Blocker;
            if (!b && expectedValue != InterruptibleTask.PARKED) {
                break;
            }
            if (b) {
                blocker = expectedValue;
            }
            final int n3 = n2 + 1;
            int n4 = 0;
            Label_0133: {
                if (n3 > 1000) {
                    final Runnable parked = InterruptibleTask.PARKED;
                    if (expectedValue != parked) {
                        n4 = n;
                        if (!this.compareAndSet(expectedValue, parked)) {
                            break Label_0133;
                        }
                    }
                    final boolean b2 = Thread.interrupted() || n != 0;
                    LockSupport.park(blocker);
                    n4 = (b2 ? 1 : 0);
                }
                else {
                    Thread.yield();
                    n4 = n;
                }
            }
            expectedValue = this.get();
            n = n4;
            n2 = n3;
        }
        if (n != 0) {
            thread.interrupt();
        }
    }
    
    public abstract void afterRanInterruptiblyFailure(final Throwable p0);
    
    public abstract void afterRanInterruptiblySuccess(final T p0);
    
    public final void interruptTask() {
        final Runnable expectedValue = this.get();
        if (expectedValue instanceof Thread) {
            final Blocker newValue = new Blocker(this, null);
            newValue.setOwner(Thread.currentThread());
            if (this.compareAndSet(expectedValue, newValue)) {
                try {
                    ((Thread)expectedValue).interrupt();
                }
                finally {
                    if (this.getAndSet(InterruptibleTask.DONE) == InterruptibleTask.PARKED) {
                        LockSupport.unpark((Thread)expectedValue);
                    }
                }
            }
        }
    }
    
    public abstract boolean isDone();
    
    @Override
    public final void run() {
        final Thread currentThread = Thread.currentThread();
        final Throwable t = null;
        if (!this.compareAndSet(null, currentThread)) {
            return;
        }
        final boolean b = this.isDone() ^ true;
        if (b) {
            try {
                this.runInterruptibly();
            }
            finally {
                if (!this.compareAndSet(currentThread, InterruptibleTask.DONE)) {
                    this.waitForInterrupt(currentThread);
                }
                if (b) {
                    this.afterRanInterruptiblyFailure(t);
                }
                return;
            }
        }
        if (!this.compareAndSet(currentThread, InterruptibleTask.DONE)) {
            this.waitForInterrupt(currentThread);
        }
        if (b) {
            this.afterRanInterruptiblySuccess(l01.a(t));
        }
    }
    
    public abstract T runInterruptibly();
    
    public abstract String toPendingString();
    
    @Override
    public final String toString() {
        final Runnable runnable = this.get();
        String string;
        if (runnable == InterruptibleTask.DONE) {
            string = "running=[DONE]";
        }
        else if (runnable instanceof Blocker) {
            string = "running=[INTERRUPTED]";
        }
        else if (runnable instanceof Thread) {
            final String name = ((Thread)runnable).getName();
            final StringBuilder sb = new StringBuilder(String.valueOf(name).length() + 21);
            sb.append("running=[RUNNING ON ");
            sb.append(name);
            sb.append("]");
            string = sb.toString();
        }
        else {
            string = "running=[NOT STARTED YET]";
        }
        final String pendingString = this.toPendingString();
        final StringBuilder sb2 = new StringBuilder(String.valueOf(string).length() + 2 + String.valueOf(pendingString).length());
        sb2.append(string);
        sb2.append(", ");
        sb2.append(pendingString);
        return sb2.toString();
    }
    
    public static final class Blocker extends AbstractOwnableSynchronizer implements Runnable
    {
        private final InterruptibleTask<?> task;
        
        private Blocker(final InterruptibleTask<?> task) {
            this.task = task;
        }
        
        private void setOwner(final Thread exclusiveOwnerThread) {
            super.setExclusiveOwnerThread(exclusiveOwnerThread);
        }
        
        @Override
        public void run() {
        }
        
        @Override
        public String toString() {
            return this.task.toString();
        }
    }
    
    public static final class b implements Runnable
    {
        @Override
        public void run() {
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.util.concurrent;

public enum Service$State
{
    private static final Service$State[] $VALUES;
    
    FAILED, 
    NEW, 
    RUNNING, 
    STARTING, 
    STOPPING, 
    TERMINATED;
    
    private static /* synthetic */ Service$State[] $values() {
        return new Service$State[] { Service$State.NEW, Service$State.STARTING, Service$State.RUNNING, Service$State.STOPPING, Service$State.TERMINATED, Service$State.FAILED };
    }
    
    static {
        $VALUES = $values();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.util.concurrent;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArraySet;
import java.lang.ref.WeakReference;
import java.util.Set;

enum FuturesGetChecked$GetCheckedTypeValidatorHolder$WeakSetValidator
{
    private static final FuturesGetChecked$GetCheckedTypeValidatorHolder$WeakSetValidator[] $VALUES;
    
    INSTANCE;
    
    private static final Set<WeakReference<Class<? extends Exception>>> validClasses;
    
    private static /* synthetic */ FuturesGetChecked$GetCheckedTypeValidatorHolder$WeakSetValidator[] $values() {
        return new FuturesGetChecked$GetCheckedTypeValidatorHolder$WeakSetValidator[] { FuturesGetChecked$GetCheckedTypeValidatorHolder$WeakSetValidator.INSTANCE };
    }
    
    static {
        $VALUES = $values();
        validClasses = new CopyOnWriteArraySet<WeakReference<Class<? extends Exception>>>();
    }
    
    public void validateClass(final Class<? extends Exception> referent) {
        final Iterator<WeakReference<Class<? extends Exception>>> iterator = FuturesGetChecked$GetCheckedTypeValidatorHolder$WeakSetValidator.validClasses.iterator();
        while (iterator.hasNext()) {
            if (referent.equals(((WeakReference<Object>)iterator.next()).get())) {
                return;
            }
        }
        ga0.a(referent);
        final Set<WeakReference<Class<? extends Exception>>> validClasses = FuturesGetChecked$GetCheckedTypeValidatorHolder$WeakSetValidator.validClasses;
        if (validClasses.size() > 1000) {
            validClasses.clear();
        }
        validClasses.add(new WeakReference<Class<? extends Exception>>(referent));
    }
}

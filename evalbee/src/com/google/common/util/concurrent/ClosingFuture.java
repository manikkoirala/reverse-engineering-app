// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.util.concurrent;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.IdentityHashMap;
import java.util.concurrent.RejectedExecutionException;
import java.util.logging.Level;
import java.io.IOException;
import java.util.concurrent.Executor;
import java.io.Closeable;
import java.util.logging.Logger;

public abstract class ClosingFuture
{
    public static final Logger a;
    
    static {
        a = Logger.getLogger(ClosingFuture.class.getName());
    }
    
    public static /* synthetic */ com.google.common.util.concurrent.d a(final ClosingFuture closingFuture) {
        throw null;
    }
    
    public static /* synthetic */ void b(final ClosingFuture closingFuture, final CloseableList list) {
        throw null;
    }
    
    public static /* synthetic */ Logger c() {
        return ClosingFuture.a;
    }
    
    public static void e(final Closeable closeable, final Executor executor) {
        if (closeable == null) {
            return;
        }
        try {
            executor.execute(new Runnable(closeable) {
                public final Closeable a;
                
                @Override
                public void run() {
                    try {
                        this.a.close();
                        return;
                    }
                    catch (final RuntimeException thrown) {}
                    catch (final IOException ex) {}
                    final RuntimeException thrown;
                    ClosingFuture.c().log(Level.WARNING, "thrown by close()", thrown);
                }
            });
        }
        catch (final RejectedExecutionException thrown) {
            final Logger a = ClosingFuture.a;
            final Level warning = Level.WARNING;
            if (a.isLoggable(warning)) {
                a.log(warning, String.format("while submitting close to %s; will close inline", executor), thrown);
            }
            e(closeable, g.a());
        }
    }
    
    public static final class CloseableList extends IdentityHashMap<Closeable, Executor> implements Closeable
    {
        private volatile boolean closed;
        private final d closer;
        private volatile CountDownLatch whenClosed;
        
        private CloseableList() {
            this.closer = new d(this);
        }
        
        public void add(final Closeable key, final Executor value) {
            i71.r(value);
            if (key == null) {
                return;
            }
            synchronized (this) {
                if (!this.closed) {
                    this.put(key, value);
                    return;
                }
                monitorexit(this);
                e(key, value);
            }
        }
        
        public <V, U> com.google.common.util.concurrent.d applyAsyncClosingFunction(final b b, final V v) {
            final CloseableList list = new CloseableList();
            try {
                b.a(list.closer, v);
                ClosingFuture.b(null, list);
                return ClosingFuture.a(null);
            }
            finally {
                this.add(list, g.a());
            }
        }
        
        public <V, U> ik0 applyClosingFunction(final c c, final V v) {
            final CloseableList list = new CloseableList();
            try {
                return e.d(c.a(list.closer, v));
            }
            finally {
                this.add(list, g.a());
            }
        }
        
        @Override
        public void close() {
            if (this.closed) {
                return;
            }
            synchronized (this) {
                if (this.closed) {
                    return;
                }
                this.closed = true;
                monitorexit(this);
                for (final Map.Entry entry : this.entrySet()) {
                    e((Closeable)entry.getKey(), (Executor)entry.getValue());
                }
                this.clear();
                if (this.whenClosed != null) {
                    this.whenClosed.countDown();
                }
            }
        }
        
        public CountDownLatch whenClosedCountDown() {
            final boolean closed = this.closed;
            boolean b = false;
            if (closed) {
                return new CountDownLatch(0);
            }
            synchronized (this) {
                if (this.closed) {
                    return new CountDownLatch(0);
                }
                if (this.whenClosed == null) {
                    b = true;
                }
                i71.x(b);
                return this.whenClosed = new CountDownLatch(1);
            }
        }
    }
    
    public enum State
    {
        private static final State[] $VALUES;
        
        CLOSED, 
        CLOSING, 
        OPEN, 
        SUBSUMED, 
        WILL_CLOSE, 
        WILL_CREATE_VALUE_AND_CLOSER;
        
        private static /* synthetic */ State[] $values() {
            return new State[] { State.OPEN, State.SUBSUMED, State.WILL_CLOSE, State.CLOSING, State.CLOSED, State.WILL_CREATE_VALUE_AND_CLOSER };
        }
        
        static {
            $VALUES = $values();
        }
    }
    
    public interface b
    {
        ClosingFuture a(final d p0, final Object p1);
    }
    
    public interface c
    {
        Object a(final d p0, final Object p1);
    }
    
    public static final class d
    {
        public final CloseableList a;
        
        public d(final CloseableList a) {
            this.a = a;
        }
    }
}

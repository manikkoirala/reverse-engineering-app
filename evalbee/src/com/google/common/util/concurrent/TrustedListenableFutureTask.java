// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.util.concurrent;

import java.util.concurrent.Callable;
import java.util.concurrent.RunnableFuture;

public abstract class TrustedListenableFutureTask extends a implements RunnableFuture
{
    public final class TrustedFutureInterruptibleAsyncTask extends InterruptibleTask<ik0>
    {
        private final h9 callable;
        final TrustedListenableFutureTask this$0;
        
        public TrustedFutureInterruptibleAsyncTask(final TrustedListenableFutureTask trustedListenableFutureTask, final h9 h9) {
            zu0.a(i71.r(h9));
        }
        
        @Override
        public void afterRanInterruptiblyFailure(final Throwable t) {
            throw null;
        }
        
        @Override
        public void afterRanInterruptiblySuccess(final ik0 ik0) {
            throw null;
        }
        
        @Override
        public final boolean isDone() {
            throw null;
        }
        
        @Override
        public ik0 runInterruptibly() {
            throw null;
        }
        
        @Override
        public String toPendingString() {
            throw null;
        }
    }
    
    public final class TrustedFutureInterruptibleTask extends InterruptibleTask<Object>
    {
        private final Callable<Object> callable;
        final TrustedListenableFutureTask this$0;
        
        public TrustedFutureInterruptibleTask(final TrustedListenableFutureTask trustedListenableFutureTask, final Callable<Object> callable) {
            this.callable = (Callable)i71.r(callable);
        }
        
        @Override
        public void afterRanInterruptiblyFailure(final Throwable t) {
            throw null;
        }
        
        @Override
        public void afterRanInterruptiblySuccess(final Object o) {
            throw null;
        }
        
        @Override
        public final boolean isDone() {
            throw null;
        }
        
        @Override
        public Object runInterruptibly() {
            return this.callable.call();
        }
        
        @Override
        public String toPendingString() {
            return this.callable.toString();
        }
    }
}

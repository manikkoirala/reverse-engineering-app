// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.util.concurrent;

import java.security.PrivilegedActionException;
import java.security.AccessController;
import java.lang.reflect.Field;
import java.security.PrivilegedExceptionAction;
import sun.misc.Unsafe;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeoutException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;
import java.util.concurrent.Executor;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.logging.Logger;

public abstract class AbstractFuture extends ag0 implements ik0
{
    private static final b ATOMIC_HELPER;
    static final boolean GENERATE_CANCELLATION_CAUSES;
    private static final Object NULL;
    private static final long SPIN_THRESHOLD_NANOS = 1000L;
    private static final Logger log;
    private volatile d listeners;
    private volatile Object value;
    private volatile k waiters;
    
    static {
        boolean boolean1;
        try {
            boolean1 = Boolean.parseBoolean(System.getProperty("guava.concurrent.generate_cancellation_cause", "false"));
        }
        catch (final SecurityException ex) {
            boolean1 = false;
        }
        GENERATE_CANCELLATION_CAUSES = boolean1;
        log = Logger.getLogger(AbstractFuture.class.getName());
        g atomic_HELPER = null;
        try {
            final j j = new j(null);
        }
        finally {
            try {
                final e e = new e(AtomicReferenceFieldUpdater.newUpdater(k.class, Thread.class, "a"), AtomicReferenceFieldUpdater.newUpdater(k.class, k.class, "b"), AtomicReferenceFieldUpdater.newUpdater(AbstractFuture.class, k.class, "waiters"), AtomicReferenceFieldUpdater.newUpdater(AbstractFuture.class, d.class, "listeners"), AtomicReferenceFieldUpdater.newUpdater(AbstractFuture.class, Object.class, "value"));
            }
            finally {
                atomic_HELPER = new g(null);
            }
        }
        ATOMIC_HELPER = (b)atomic_HELPER;
        final Throwable thrown;
        if (thrown != null) {
            final Logger log2 = AbstractFuture.log;
            final Level severe = Level.SEVERE;
            final Throwable thrown2;
            log2.log(severe, "UnsafeAtomicHelper is broken!", thrown2);
            log2.log(severe, "SafeAtomicHelper is broken!", thrown);
        }
        NULL = new Object();
    }
    
    private void a(final StringBuilder sb) {
        String str = "]";
        try {
            final Object k = k(this);
            sb.append("SUCCESS, result=[");
            this.c(sb, k);
            sb.append("]");
            return;
        }
        catch (final RuntimeException ex) {
            sb.append("UNKNOWN, cause=[");
            sb.append(ex.getClass());
            str = " thrown from get()]";
        }
        catch (final CancellationException ex2) {
            str = "CANCELLED";
        }
        catch (final ExecutionException ex3) {
            sb.append("FAILURE, cause=[");
            sb.append(ex3.getCause());
        }
        sb.append(str);
    }
    
    public static /* synthetic */ b access$200() {
        return AbstractFuture.ATOMIC_HELPER;
    }
    
    public static /* synthetic */ Object access$300(final AbstractFuture abstractFuture) {
        return abstractFuture.value;
    }
    
    public static /* synthetic */ Object access$302(final AbstractFuture abstractFuture, final Object value) {
        return abstractFuture.value = value;
    }
    
    public static /* synthetic */ d access$700(final AbstractFuture abstractFuture) {
        return abstractFuture.listeners;
    }
    
    public static /* synthetic */ d access$702(final AbstractFuture abstractFuture, final d listeners) {
        return abstractFuture.listeners = listeners;
    }
    
    public static /* synthetic */ k access$800(final AbstractFuture abstractFuture) {
        return abstractFuture.waiters;
    }
    
    public static /* synthetic */ k access$802(final AbstractFuture abstractFuture, final k waiters) {
        return abstractFuture.waiters = waiters;
    }
    
    public static CancellationException e(final String message, final Throwable cause) {
        final CancellationException ex = new CancellationException(message);
        ex.initCause(cause);
        return ex;
    }
    
    public static void g(AbstractFuture abstractFuture) {
        d d = null;
    Label_0002:
        while (true) {
            abstractFuture.l();
            abstractFuture.afterDone();
            d c;
            for (d f = abstractFuture.f(d); f != null; f = c) {
                c = f.c;
                final Runnable a = f.a;
                Objects.requireNonNull(a);
                final Runnable runnable = a;
                if (runnable instanceof f) {
                    final f f2 = (f)runnable;
                    final AbstractFuture a2 = f2.a;
                    if (a2.value == f2 && AbstractFuture.ATOMIC_HELPER.b(a2, f2, j(f2.b))) {
                        d = c;
                        abstractFuture = a2;
                        continue Label_0002;
                    }
                }
                else {
                    final Executor b = f.b;
                    Objects.requireNonNull(b);
                    h(runnable, b);
                }
            }
            break;
        }
    }
    
    public static void h(final Runnable obj, final Executor obj2) {
        try {
            obj2.execute(obj);
        }
        catch (final RuntimeException thrown) {
            final Logger log = AbstractFuture.log;
            final Level severe = Level.SEVERE;
            final String value = String.valueOf(obj);
            final String value2 = String.valueOf(obj2);
            final StringBuilder sb = new StringBuilder(value.length() + 57 + value2.length());
            sb.append("RuntimeException while executing runnable ");
            sb.append(value);
            sb.append(" with executor ");
            sb.append(value2);
            log.log(severe, sb.toString(), thrown);
        }
    }
    
    private Object i(final Object o) {
        if (o instanceof c) {
            throw e("Task was cancelled.", ((c)o).b);
        }
        if (!(o instanceof Failure)) {
            Object b;
            if ((b = o) == AbstractFuture.NULL) {
                b = l01.b();
            }
            return b;
        }
        throw new ExecutionException(((Failure)o).a);
    }
    
    public static Object j(final ik0 obj) {
        if (obj instanceof h) {
            Object obj2;
            final Object o = obj2 = ((AbstractFuture)obj).value;
            if (o instanceof c) {
                final c c = (c)o;
                obj2 = o;
                if (c.a) {
                    if (c.b != null) {
                        obj2 = new c(false, c.b);
                    }
                    else {
                        obj2 = AbstractFuture.c.d;
                    }
                }
            }
            Objects.requireNonNull(obj2);
            return obj2;
        }
        if (obj instanceof ag0) {
            final Throwable a = bg0.a((ag0)obj);
            if (a != null) {
                return new Failure(a);
            }
        }
        final boolean cancelled = obj.isCancelled();
        if ((AbstractFuture.GENERATE_CANCELLATION_CAUSES ^ true) & cancelled) {
            final c d = c.d;
            Objects.requireNonNull(d);
            return d;
        }
        try {
            final Object k = k(obj);
            if (cancelled) {
                final String value = String.valueOf(obj);
                final StringBuilder sb = new StringBuilder(value.length() + 84);
                sb.append("get() did not throw CancellationException, despite reporting isCancelled() == true: ");
                sb.append(value);
                return new c(false, new IllegalArgumentException(sb.toString()));
            }
            Object null;
            if ((null = k) == null) {
                null = AbstractFuture.NULL;
            }
            return null;
        }
        catch (final CancellationException cause) {
            if (!cancelled) {
                final String value2 = String.valueOf(obj);
                final StringBuilder sb2 = new StringBuilder(value2.length() + 77);
                sb2.append("get() threw CancellationException, despite reporting isCancelled() == false: ");
                sb2.append(value2);
                return new Failure(new IllegalArgumentException(sb2.toString(), cause));
            }
            return new c(false, cause);
        }
        catch (final ExecutionException cause2) {
            if (cancelled) {
                final String value3 = String.valueOf(obj);
                final StringBuilder sb3 = new StringBuilder(value3.length() + 84);
                sb3.append("get() did not throw CancellationException, despite reporting isCancelled() == true: ");
                sb3.append(value3);
                return new c(false, new IllegalArgumentException(sb3.toString(), cause2));
            }
            return new Failure(cause2.getCause());
        }
        finally {
            final Throwable t;
            return new Failure(t);
        }
    }
    
    public static Object k(final Future future) {
        boolean b = false;
        try {
            return future.get();
        }
        catch (final InterruptedException ex) {
            b = true;
            return future.get();
        }
        finally {
            if (b) {
                Thread.currentThread().interrupt();
            }
        }
    }
    
    private void l() {
        for (Object o = AbstractFuture.ATOMIC_HELPER.e(this, k.c); o != null; o = ((k)o).b) {
            ((k)o).b();
        }
    }
    
    @Override
    public void addListener(final Runnable runnable, final Executor executor) {
        i71.s(runnable, "Runnable was null.");
        i71.s(executor, "Executor was null.");
        if (!this.isDone()) {
            d c = this.listeners;
            if (c != d.d) {
                final d d = new d(runnable, executor);
                do {
                    d.c = c;
                    if (AbstractFuture.ATOMIC_HELPER.a(this, c, d)) {
                        return;
                    }
                } while ((c = this.listeners) != AbstractFuture.d.d);
            }
        }
        h(runnable, executor);
    }
    
    public void afterDone() {
    }
    
    public final void b(final StringBuilder sb) {
        final int length = sb.length();
        sb.append("PENDING");
        Object str = this.value;
        Label_0137: {
            if (str instanceof f) {
                sb.append(", setFuture=[");
                this.d(sb, ((f)str).b);
            }
            else {
                Label_0116: {
                    try {
                        str = ir1.a(this.pendingToString());
                        break Label_0116;
                    }
                    catch (final StackOverflowError str) {}
                    catch (final RuntimeException ex) {}
                    final String value = String.valueOf(((f)str).getClass());
                    final StringBuilder sb2 = new StringBuilder(value.length() + 38);
                    sb2.append("Exception thrown from implementation: ");
                    sb2.append(value);
                    str = sb2.toString();
                }
                if (str == null) {
                    break Label_0137;
                }
                sb.append(", info=[");
                sb.append((String)str);
            }
            sb.append("]");
        }
        if (this.isDone()) {
            sb.delete(length, sb.length());
            this.a(sb);
        }
    }
    
    public final void c(final StringBuilder sb, final Object o) {
        String hexString;
        if (o == null) {
            hexString = "null";
        }
        else if (o == this) {
            hexString = "this future";
        }
        else {
            sb.append(o.getClass().getName());
            sb.append("@");
            hexString = Integer.toHexString(System.identityHashCode(o));
        }
        sb.append(hexString);
    }
    
    @Override
    public boolean cancel(final boolean b) {
        final Object value = this.value;
        final boolean b2 = true;
        boolean b3;
        if (value == null | value instanceof f) {
            c c;
            if (AbstractFuture.GENERATE_CANCELLATION_CAUSES) {
                c = new c(b, new CancellationException("Future.cancel() was called."));
            }
            else {
                c obj;
                if (b) {
                    obj = AbstractFuture.c.c;
                }
                else {
                    obj = AbstractFuture.c.d;
                }
                Objects.requireNonNull(obj);
                c = obj;
            }
            AbstractFuture abstractFuture = this;
            b3 = false;
            Object o = value;
            while (true) {
                if (AbstractFuture.ATOMIC_HELPER.b(abstractFuture, o, c)) {
                    if (b) {
                        abstractFuture.interruptTask();
                    }
                    g(abstractFuture);
                    b3 = b2;
                    if (!(o instanceof f)) {
                        break;
                    }
                    final ik0 b4 = ((f)o).b;
                    if (!(b4 instanceof h)) {
                        b4.cancel(b);
                        b3 = b2;
                        break;
                    }
                    abstractFuture = (AbstractFuture)b4;
                    o = abstractFuture.value;
                    final boolean b5 = o == null;
                    b3 = b2;
                    if (!(b5 | o instanceof f)) {
                        break;
                    }
                    b3 = true;
                }
                else {
                    if (!((o = abstractFuture.value) instanceof f)) {
                        break;
                    }
                    continue;
                }
            }
        }
        else {
            b3 = false;
        }
        return b3;
    }
    
    public final void d(final StringBuilder sb, final Object obj) {
        while (true) {
            if (obj == this) {
                try {
                    sb.append("this future");
                    return;
                    sb.append(obj);
                    return;
                }
                catch (final StackOverflowError obj) {}
                catch (final RuntimeException ex) {}
                sb.append("Exception thrown from implementation: ");
                sb.append(obj.getClass());
                return;
            }
            continue;
        }
    }
    
    public final d f(d d) {
        final Object d2 = AbstractFuture.ATOMIC_HELPER.d(this, AbstractFuture.d.d);
        d c = d;
        d c2;
        for (d = (d)d2; d != null; d = c2) {
            c2 = d.c;
            d.c = c;
            c = d;
        }
        return c;
    }
    
    @Override
    public Object get() {
        if (Thread.interrupted()) {
            throw new InterruptedException();
        }
        final Object value = this.value;
        if (value != null & (value instanceof f ^ true)) {
            return this.i(value);
        }
        k k = this.waiters;
        if (k != AbstractFuture.k.c) {
            final k i = new k();
            do {
                i.a(k);
                if (AbstractFuture.ATOMIC_HELPER.c(this, k, i)) {
                    Object value2;
                    do {
                        LockSupport.park(this);
                        if (Thread.interrupted()) {
                            this.m(i);
                            throw new InterruptedException();
                        }
                        value2 = this.value;
                    } while (!(value2 != null & (value2 instanceof f ^ true)));
                    return this.i(value2);
                }
            } while ((k = this.waiters) != AbstractFuture.k.c);
        }
        final Object value3 = this.value;
        Objects.requireNonNull(value3);
        return this.i(value3);
    }
    
    @Override
    public Object get(long convert, final TimeUnit timeUnit) {
        long nanos = timeUnit.toNanos(convert);
        if (Thread.interrupted()) {
            throw new InterruptedException();
        }
        final Object value = this.value;
        if (value != null & (value instanceof f ^ true)) {
            return this.i(value);
        }
        long n;
        if (nanos > 0L) {
            n = System.nanoTime() + nanos;
        }
        else {
            n = 0L;
        }
        long n2 = nanos;
        Label_0261: {
            if (nanos >= 1000L) {
                k k = this.waiters;
                if (k != AbstractFuture.k.c) {
                    final k i = new k();
                    do {
                        i.a(k);
                        if (AbstractFuture.ATOMIC_HELPER.c(this, k, i)) {
                            do {
                                j21.a(this, nanos);
                                if (Thread.interrupted()) {
                                    this.m(i);
                                    throw new InterruptedException();
                                }
                                final Object value2 = this.value;
                                if (value2 != null & (value2 instanceof f ^ true)) {
                                    return this.i(value2);
                                }
                                n2 = (nanos = n - System.nanoTime());
                            } while (n2 >= 1000L);
                            this.m(i);
                            break Label_0261;
                        }
                    } while ((k = this.waiters) != AbstractFuture.k.c);
                }
                final Object value3 = this.value;
                Objects.requireNonNull(value3);
                return this.i(value3);
            }
        }
        while (n2 > 0L) {
            final Object value4 = this.value;
            if (value4 != null & (value4 instanceof f ^ true)) {
                return this.i(value4);
            }
            if (Thread.interrupted()) {
                throw new InterruptedException();
            }
            n2 = n - System.nanoTime();
        }
        final String string = this.toString();
        final String string2 = timeUnit.toString();
        final Locale root = Locale.ROOT;
        final String lowerCase = string2.toLowerCase(root);
        final String lowerCase2 = timeUnit.toString().toLowerCase(root);
        final StringBuilder sb = new StringBuilder(String.valueOf(lowerCase2).length() + 28);
        sb.append("Waited ");
        sb.append(convert);
        sb.append(" ");
        sb.append(lowerCase2);
        String str;
        final String obj = str = sb.toString();
        if (n2 + 1000L < 0L) {
            final String concat = String.valueOf(obj).concat(" (plus ");
            final long sourceDuration = -n2;
            convert = timeUnit.convert(sourceDuration, TimeUnit.NANOSECONDS);
            final long lng = sourceDuration - timeUnit.toNanos(convert);
            final long n3 = lcmp(convert, 0L);
            final boolean b = n3 == 0 || lng > 1000L;
            String concat2 = concat;
            if (n3 > 0) {
                final String value5 = String.valueOf(concat);
                final StringBuilder sb2 = new StringBuilder(value5.length() + 21 + String.valueOf(lowerCase).length());
                sb2.append(value5);
                sb2.append(convert);
                sb2.append(" ");
                sb2.append(lowerCase);
                String s = sb2.toString();
                if (b) {
                    s = String.valueOf(s).concat(",");
                }
                concat2 = String.valueOf(s).concat(" ");
            }
            String string3 = concat2;
            if (b) {
                final String value6 = String.valueOf(concat2);
                final StringBuilder sb3 = new StringBuilder(value6.length() + 33);
                sb3.append(value6);
                sb3.append(lng);
                sb3.append(" nanoseconds ");
                string3 = sb3.toString();
            }
            str = String.valueOf(string3).concat("delay)");
        }
        if (this.isDone()) {
            throw new TimeoutException(String.valueOf(str).concat(" but future completed as timeout expired"));
        }
        final StringBuilder sb4 = new StringBuilder(String.valueOf(str).length() + 5 + String.valueOf(string).length());
        sb4.append(str);
        sb4.append(" for ");
        sb4.append(string);
        throw new TimeoutException(sb4.toString());
    }
    
    public void interruptTask() {
    }
    
    @Override
    public boolean isCancelled() {
        return this.value instanceof c;
    }
    
    @Override
    public boolean isDone() {
        final Object value = this.value;
        return (value instanceof f ^ true) & value != null;
    }
    
    public final void m(k waiters) {
        waiters.a = null;
    Label_0005:
        while (true) {
            waiters = this.waiters;
            if (waiters == k.c) {
                return;
            }
            k k = null;
            while (waiters != null) {
                final k b = waiters.b;
                k i;
                if (waiters.a != null) {
                    i = waiters;
                }
                else if (k != null) {
                    k.b = b;
                    i = k;
                    if (k.a == null) {
                        continue Label_0005;
                    }
                }
                else {
                    i = k;
                    if (!AbstractFuture.ATOMIC_HELPER.c(this, waiters, b)) {
                        continue Label_0005;
                    }
                }
                waiters = b;
                k = i;
            }
        }
    }
    
    public final void maybePropagateCancellationTo(final Future<?> future) {
        if (future != null & this.isCancelled()) {
            future.cancel(this.wasInterrupted());
        }
    }
    
    public String pendingToString() {
        if (this instanceof ScheduledFuture) {
            final long delay = ((ScheduledFuture)this).getDelay(TimeUnit.MILLISECONDS);
            final StringBuilder sb = new StringBuilder(41);
            sb.append("remaining delay=[");
            sb.append(delay);
            sb.append(" ms]");
            return sb.toString();
        }
        return null;
    }
    
    public boolean set(final Object o) {
        Object null = o;
        if (o == null) {
            null = AbstractFuture.NULL;
        }
        if (AbstractFuture.ATOMIC_HELPER.b(this, null, null)) {
            g(this);
            return true;
        }
        return false;
    }
    
    public boolean setException(final Throwable t) {
        if (AbstractFuture.ATOMIC_HELPER.b(this, null, new Failure((Throwable)i71.r(t)))) {
            g(this);
            return true;
        }
        return false;
    }
    
    public boolean setFuture(final ik0 ik0) {
        i71.r(ik0);
        Object o;
        if ((o = this.value) == null) {
            if (ik0.isDone()) {
                if (AbstractFuture.ATOMIC_HELPER.b(this, null, j(ik0))) {
                    g(this);
                    return true;
                }
                return false;
            }
            else {
                final f f = new f(this, ik0);
                if (AbstractFuture.ATOMIC_HELPER.b(this, null, f)) {
                    try {
                        ik0.addListener(f, DirectExecutor.INSTANCE);
                    }
                    finally {
                        Failure b = null;
                        try {
                            final Throwable t;
                            final Failure failure = new Failure(t);
                        }
                        finally {
                            b = Failure.b;
                        }
                        AbstractFuture.ATOMIC_HELPER.b(this, f, b);
                    }
                    return true;
                }
                o = this.value;
            }
        }
        if (o instanceof c) {
            ik0.cancel(((c)o).a);
        }
        return false;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        String str;
        if (this.getClass().getName().startsWith("com.google.common.util.concurrent.")) {
            str = this.getClass().getSimpleName();
        }
        else {
            str = this.getClass().getName();
        }
        sb.append(str);
        sb.append('@');
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append("[status=");
        if (this.isCancelled()) {
            sb.append("CANCELLED");
        }
        else if (this.isDone()) {
            this.a(sb);
        }
        else {
            this.b(sb);
        }
        sb.append("]");
        return sb.toString();
    }
    
    @Override
    public final Throwable tryInternalFastPathGetFailure() {
        if (this instanceof h) {
            final Object value = this.value;
            if (value instanceof Failure) {
                return ((Failure)value).a;
            }
        }
        return null;
    }
    
    public final boolean wasInterrupted() {
        final Object value = this.value;
        return value instanceof c && ((c)value).a;
    }
    
    public static final class Failure
    {
        public static final Failure b;
        public final Throwable a;
        
        static {
            b = new Failure(new Throwable("Failure occurred while trying to finish a future.") {
                @Override
                public Throwable fillInStackTrace() {
                    monitorenter(this);
                    monitorexit(this);
                    return this;
                }
            });
        }
        
        public Failure(final Throwable t) {
            this.a = (Throwable)i71.r(t);
        }
    }
    
    public abstract static class b
    {
        public abstract boolean a(final AbstractFuture p0, final d p1, final d p2);
        
        public abstract boolean b(final AbstractFuture p0, final Object p1, final Object p2);
        
        public abstract boolean c(final AbstractFuture p0, final k p1, final k p2);
        
        public abstract d d(final AbstractFuture p0, final d p1);
        
        public abstract k e(final AbstractFuture p0, final k p1);
        
        public abstract void f(final k p0, final k p1);
        
        public abstract void g(final k p0, final Thread p1);
    }
    
    public static final class c
    {
        public static final c c;
        public static final c d;
        public final boolean a;
        public final Throwable b;
        
        static {
            if (AbstractFuture.GENERATE_CANCELLATION_CAUSES) {
                d = null;
                c = null;
            }
            else {
                d = new c(false, null);
                c = new c(true, null);
            }
        }
        
        public c(final boolean a, final Throwable b) {
            this.a = a;
            this.b = b;
        }
    }
    
    public static final class d
    {
        public static final d d;
        public final Runnable a;
        public final Executor b;
        public d c;
        
        static {
            d = new d();
        }
        
        public d() {
            this.a = null;
            this.b = null;
        }
        
        public d(final Runnable a, final Executor b) {
            this.a = a;
            this.b = b;
        }
    }
    
    public static final class e extends b
    {
        public final AtomicReferenceFieldUpdater a;
        public final AtomicReferenceFieldUpdater b;
        public final AtomicReferenceFieldUpdater c;
        public final AtomicReferenceFieldUpdater d;
        public final AtomicReferenceFieldUpdater e;
        
        public e(final AtomicReferenceFieldUpdater a, final AtomicReferenceFieldUpdater b, final AtomicReferenceFieldUpdater c, final AtomicReferenceFieldUpdater d, final AtomicReferenceFieldUpdater e) {
            super(null);
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
        }
        
        @Override
        public boolean a(final AbstractFuture abstractFuture, final d d, final d d2) {
            return z.a(this.d, abstractFuture, d, d2);
        }
        
        @Override
        public boolean b(final AbstractFuture abstractFuture, final Object o, final Object o2) {
            return z.a(this.e, abstractFuture, o, o2);
        }
        
        @Override
        public boolean c(final AbstractFuture abstractFuture, final k k, final k i) {
            return z.a(this.c, abstractFuture, k, i);
        }
        
        @Override
        public d d(final AbstractFuture obj, final d newValue) {
            return this.d.getAndSet(obj, newValue);
        }
        
        @Override
        public k e(final AbstractFuture obj, final k newValue) {
            return this.c.getAndSet(obj, newValue);
        }
        
        @Override
        public void f(final k k, final k i) {
            this.b.lazySet(k, i);
        }
        
        @Override
        public void g(final k k, final Thread thread) {
            this.a.lazySet(k, thread);
        }
    }
    
    public static final class f implements Runnable
    {
        public final AbstractFuture a;
        public final ik0 b;
        
        public f(final AbstractFuture a, final ik0 b) {
            this.a = a;
            this.b = b;
        }
        
        @Override
        public void run() {
            if (AbstractFuture.access$300(this.a) != this) {
                return;
            }
            if (AbstractFuture.access$200().b(this.a, this, j(this.b))) {
                g(this.a);
            }
        }
    }
    
    public static final class g extends b
    {
        public g() {
            super(null);
        }
        
        @Override
        public boolean a(final AbstractFuture abstractFuture, final d d, final d d2) {
            synchronized (abstractFuture) {
                if (AbstractFuture.access$700(abstractFuture) == d) {
                    AbstractFuture.access$702(abstractFuture, d2);
                    return true;
                }
                return false;
            }
        }
        
        @Override
        public boolean b(final AbstractFuture abstractFuture, final Object o, final Object o2) {
            synchronized (abstractFuture) {
                if (AbstractFuture.access$300(abstractFuture) == o) {
                    AbstractFuture.access$302(abstractFuture, o2);
                    return true;
                }
                return false;
            }
        }
        
        @Override
        public boolean c(final AbstractFuture abstractFuture, final k k, final k i) {
            synchronized (abstractFuture) {
                if (AbstractFuture.access$800(abstractFuture) == k) {
                    AbstractFuture.access$802(abstractFuture, i);
                    return true;
                }
                return false;
            }
        }
        
        @Override
        public d d(final AbstractFuture abstractFuture, final d d) {
            synchronized (abstractFuture) {
                final d access$700 = AbstractFuture.access$700(abstractFuture);
                if (access$700 != d) {
                    AbstractFuture.access$702(abstractFuture, d);
                }
                return access$700;
            }
        }
        
        @Override
        public k e(final AbstractFuture abstractFuture, final k k) {
            synchronized (abstractFuture) {
                final k access$800 = AbstractFuture.access$800(abstractFuture);
                if (access$800 != k) {
                    AbstractFuture.access$802(abstractFuture, k);
                }
                return access$800;
            }
        }
        
        @Override
        public void f(final k k, final k b) {
            k.b = b;
        }
        
        @Override
        public void g(final k k, final Thread a) {
            k.a = a;
        }
    }
    
    public interface h extends ik0
    {
    }
    
    public abstract static class i extends AbstractFuture implements h
    {
        @Override
        public final void addListener(final Runnable runnable, final Executor executor) {
            super.addListener(runnable, executor);
        }
        
        @Override
        public final boolean cancel(final boolean b) {
            return super.cancel(b);
        }
        
        @Override
        public final Object get() {
            return super.get();
        }
        
        @Override
        public final Object get(final long n, final TimeUnit timeUnit) {
            return super.get(n, timeUnit);
        }
        
        @Override
        public final boolean isCancelled() {
            return super.isCancelled();
        }
        
        @Override
        public final boolean isDone() {
            return super.isDone();
        }
    }
    
    public static final class j extends b
    {
        public static final Unsafe a;
        public static final long b;
        public static final long c;
        public static final long d;
        public static final long e;
        public static final long f;
        
        static {
            Label_0024: {
                try {
                    final Unsafe unsafe = Unsafe.getUnsafe();
                    break Label_0024;
                }
                catch (final SecurityException ex) {
                    final PrivilegedExceptionAction privilegedExceptionAction = new(com.google.common.util.concurrent.AbstractFuture$j$acom.google.common.util.concurrent.AbstractFuture.j.AbstractFuture$j$a.class)();
                    final PrivilegedExceptionAction privilegedExceptionAction3;
                    final PrivilegedExceptionAction privilegedExceptionAction2 = privilegedExceptionAction3 = privilegedExceptionAction;
                    new PrivilegedExceptionAction() {
                        public Unsafe a() {
                            for (final Field field : Unsafe.class.getDeclaredFields()) {
                                field.setAccessible(true);
                                final Object value = field.get(null);
                                if (Unsafe.class.isInstance(value)) {
                                    return Unsafe.class.cast(value);
                                }
                            }
                            throw new NoSuchFieldError("the Unsafe");
                        }
                    };
                    final PrivilegedExceptionAction privilegedExceptionAction4 = privilegedExceptionAction2;
                    final Unsafe unsafe2 = AccessController.doPrivileged((PrivilegedExceptionAction<Unsafe>)privilegedExceptionAction4);
                    final Unsafe unsafe;
                    final Unsafe unsafe3 = unsafe = unsafe2;
                }
                try {
                    final PrivilegedExceptionAction privilegedExceptionAction = new(com.google.common.util.concurrent.AbstractFuture$j$acom.google.common.util.concurrent.AbstractFuture.j.AbstractFuture$j$a.class)();
                    final PrivilegedExceptionAction privilegedExceptionAction3;
                    final PrivilegedExceptionAction privilegedExceptionAction2 = privilegedExceptionAction3 = privilegedExceptionAction;
                    new PrivilegedExceptionAction() {
                        public Unsafe a() {
                            for (final Field field : Unsafe.class.getDeclaredFields()) {
                                field.setAccessible(true);
                                final Object value = field.get(null);
                                if (Unsafe.class.isInstance(value)) {
                                    return Unsafe.class.cast(value);
                                }
                            }
                            throw new NoSuchFieldError("the Unsafe");
                        }
                    };
                    final PrivilegedExceptionAction privilegedExceptionAction4 = privilegedExceptionAction2;
                    final Unsafe unsafe2 = AccessController.doPrivileged((PrivilegedExceptionAction<Unsafe>)privilegedExceptionAction4);
                    final Unsafe unsafe = unsafe2;
                    try {
                        c = unsafe.objectFieldOffset(AbstractFuture.class.getDeclaredField("waiters"));
                        b = unsafe.objectFieldOffset(AbstractFuture.class.getDeclaredField("listeners"));
                        d = unsafe.objectFieldOffset(AbstractFuture.class.getDeclaredField("value"));
                        e = unsafe.objectFieldOffset(k.class.getDeclaredField("a"));
                        f = unsafe.objectFieldOffset(k.class.getDeclaredField("b"));
                        a = unsafe;
                    }
                    catch (final Exception cause) {
                        gw1.f(cause);
                        throw new RuntimeException(cause);
                    }
                }
                catch (final PrivilegedActionException ex2) {
                    throw new RuntimeException("Could not initialize intrinsics", ex2.getCause());
                }
            }
        }
        
        public j() {
            super(null);
        }
        
        @Override
        public boolean a(final AbstractFuture abstractFuture, final d d, final d d2) {
            return rf2.a(j.a, (Object)abstractFuture, j.b, (Object)d, (Object)d2);
        }
        
        @Override
        public boolean b(final AbstractFuture abstractFuture, final Object o, final Object o2) {
            return rf2.a(j.a, (Object)abstractFuture, j.d, o, o2);
        }
        
        @Override
        public boolean c(final AbstractFuture abstractFuture, final k k, final k i) {
            return rf2.a(j.a, (Object)abstractFuture, j.c, (Object)k, (Object)i);
        }
        
        @Override
        public d d(final AbstractFuture abstractFuture, final d d) {
            d access$700;
            do {
                access$700 = AbstractFuture.access$700(abstractFuture);
                if (d == access$700) {
                    return access$700;
                }
            } while (!this.a(abstractFuture, access$700, d));
            return access$700;
        }
        
        @Override
        public k e(final AbstractFuture abstractFuture, final k k) {
            k access$800;
            do {
                access$800 = AbstractFuture.access$800(abstractFuture);
                if (k == access$800) {
                    return access$800;
                }
            } while (!this.c(abstractFuture, access$800, k));
            return access$800;
        }
        
        @Override
        public void f(final k o, final k x) {
            j.a.putObject(o, j.f, x);
        }
        
        @Override
        public void g(final k o, final Thread x) {
            j.a.putObject(o, j.e, x);
        }
    }
    
    public static final class k
    {
        public static final k c;
        public volatile Thread a;
        public volatile k b;
        
        static {
            c = new k(false);
        }
        
        public k() {
            AbstractFuture.access$200().g(this, Thread.currentThread());
        }
        
        public k(final boolean b) {
        }
        
        public void a(final k k) {
            AbstractFuture.access$200().f(this, k);
        }
        
        public void b() {
            final Thread a = this.a;
            if (a != null) {
                this.a = null;
                LockSupport.unpark(a);
            }
        }
    }
}

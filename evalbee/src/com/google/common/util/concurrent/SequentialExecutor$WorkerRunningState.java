// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.util.concurrent;

enum SequentialExecutor$WorkerRunningState
{
    private static final SequentialExecutor$WorkerRunningState[] $VALUES;
    
    IDLE, 
    QUEUED, 
    QUEUING, 
    RUNNING;
    
    private static /* synthetic */ SequentialExecutor$WorkerRunningState[] $values() {
        return new SequentialExecutor$WorkerRunningState[] { SequentialExecutor$WorkerRunningState.IDLE, SequentialExecutor$WorkerRunningState.QUEUING, SequentialExecutor$WorkerRunningState.QUEUED, SequentialExecutor$WorkerRunningState.RUNNING };
    }
    
    static {
        $VALUES = $values();
    }
}

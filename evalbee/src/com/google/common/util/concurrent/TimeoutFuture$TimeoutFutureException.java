// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.util.concurrent;

import java.util.concurrent.TimeoutException;

final class TimeoutFuture$TimeoutFutureException extends TimeoutException
{
    private TimeoutFuture$TimeoutFutureException(final String message) {
        super(message);
    }
    
    @Override
    public Throwable fillInStackTrace() {
        synchronized (this) {
            this.setStackTrace(new StackTraceElement[0]);
            return this;
        }
    }
}

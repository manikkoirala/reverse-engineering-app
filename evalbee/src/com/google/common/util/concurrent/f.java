// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.util.concurrent;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.concurrent.Executor;
import java.util.logging.Logger;

public class f implements ik0
{
    public static final ik0 b;
    public static final Logger c;
    public final Object a;
    
    static {
        b = new f(null);
        c = Logger.getLogger(f.class.getName());
    }
    
    public f(final Object a) {
        this.a = a;
    }
    
    @Override
    public void addListener(final Runnable obj, final Executor obj2) {
        i71.s(obj, "Runnable was null.");
        i71.s(obj2, "Executor was null.");
        try {
            obj2.execute(obj);
        }
        catch (final RuntimeException thrown) {
            final Logger c = f.c;
            final Level severe = Level.SEVERE;
            final String value = String.valueOf(obj);
            final String value2 = String.valueOf(obj2);
            final StringBuilder sb = new StringBuilder(value.length() + 57 + value2.length());
            sb.append("RuntimeException while executing runnable ");
            sb.append(value);
            sb.append(" with executor ");
            sb.append(value2);
            c.log(severe, sb.toString(), thrown);
        }
    }
    
    @Override
    public boolean cancel(final boolean b) {
        return false;
    }
    
    @Override
    public Object get() {
        return this.a;
    }
    
    @Override
    public Object get(final long n, final TimeUnit timeUnit) {
        i71.r(timeUnit);
        return this.get();
    }
    
    @Override
    public boolean isCancelled() {
        return false;
    }
    
    @Override
    public boolean isDone() {
        return true;
    }
    
    @Override
    public String toString() {
        final String string = super.toString();
        final String value = String.valueOf(this.a);
        final StringBuilder sb = new StringBuilder(String.valueOf(string).length() + 27 + value.length());
        sb.append(string);
        sb.append("[status=SUCCESS, result=[");
        sb.append(value);
        sb.append("]]");
        return sb.toString();
    }
    
    public static final class a extends i
    {
        public a(final Throwable exception) {
            this.setException(exception);
        }
    }
}

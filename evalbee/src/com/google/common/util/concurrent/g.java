// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.util.concurrent;

import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.Executor;

public abstract class g
{
    public static Executor a() {
        return DirectExecutor.INSTANCE;
    }
    
    public static Executor b(final Executor executor, final AbstractFuture abstractFuture) {
        i71.r(executor);
        i71.r(abstractFuture);
        if (executor == a()) {
            return executor;
        }
        return new Executor(executor, abstractFuture) {
            public final Executor a;
            public final AbstractFuture b;
            
            @Override
            public void execute(final Runnable runnable) {
                try {
                    this.a.execute(runnable);
                }
                catch (final RejectedExecutionException exception) {
                    this.b.setException(exception);
                }
            }
        };
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.util.concurrent;

import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicReference;

public abstract class ExecutionSequencer
{
    public enum RunningState
    {
        private static final RunningState[] $VALUES;
        
        CANCELLED, 
        NOT_RUN, 
        STARTED;
        
        private static /* synthetic */ RunningState[] $values() {
            return new RunningState[] { RunningState.NOT_RUN, RunningState.CANCELLED, RunningState.STARTED };
        }
        
        static {
            $VALUES = $values();
        }
    }
    
    public static final class TaskNonReentrantExecutor extends AtomicReference<RunningState> implements Executor, Runnable
    {
        Executor delegate;
        ExecutionSequencer sequencer;
        Thread submitting;
        Runnable task;
        
        private TaskNonReentrantExecutor(final Executor delegate, final ExecutionSequencer executionSequencer) {
            super(RunningState.NOT_RUN);
            this.delegate = delegate;
        }
        
        private boolean trySetCancelled() {
            return this.compareAndSet(RunningState.NOT_RUN, RunningState.CANCELLED);
        }
        
        private boolean trySetStarted() {
            return this.compareAndSet(RunningState.NOT_RUN, RunningState.STARTED);
        }
        
        @Override
        public void execute(final Runnable runnable) {
            if (this.get() == RunningState.CANCELLED) {
                this.delegate = null;
                return;
            }
            this.submitting = Thread.currentThread();
            try {
                throw null;
            }
            finally {
                this.submitting = null;
            }
        }
        
        @Override
        public void run() {
            final Thread currentThread = Thread.currentThread();
            if (currentThread != this.submitting) {
                final Runnable task = this.task;
                Objects.requireNonNull(task);
                final Runnable runnable = task;
                this.task = null;
                runnable.run();
                return;
            }
            new a(null).a = currentThread;
            throw null;
        }
    }
    
    public static final class a
    {
        public Thread a;
    }
}

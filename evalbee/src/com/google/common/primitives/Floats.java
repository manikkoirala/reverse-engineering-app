// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.primitives;

import java.util.Comparator;
import com.google.common.base.Converter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.io.Serializable;
import java.util.RandomAccess;
import java.util.AbstractList;

public abstract class Floats extends w40
{
    public static int c(final float f) {
        return Float.valueOf(f).hashCode();
    }
    
    public static int d(final float[] array, final float n, int i, final int n2) {
        while (i < n2) {
            if (array[i] == n) {
                return i;
            }
            ++i;
        }
        return -1;
    }
    
    public static int e(final float[] array, final float n, final int n2, int i) {
        --i;
        while (i >= n2) {
            if (array[i] == n) {
                return i;
            }
            --i;
        }
        return -1;
    }
    
    public static class FloatArrayAsList extends AbstractList<Float> implements RandomAccess, Serializable
    {
        private static final long serialVersionUID = 0L;
        final float[] array;
        final int end;
        final int start;
        
        public FloatArrayAsList(final float[] array) {
            this(array, 0, array.length);
        }
        
        public FloatArrayAsList(final float[] array, final int start, final int end) {
            this.array = array;
            this.start = start;
            this.end = end;
        }
        
        @Override
        public boolean contains(final Object o) {
            return o instanceof Float && d(this.array, (float)o, this.start, this.end) != -1;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof FloatArrayAsList)) {
                return super.equals(o);
            }
            final FloatArrayAsList list = (FloatArrayAsList)o;
            final int size = this.size();
            if (list.size() != size) {
                return false;
            }
            for (int i = 0; i < size; ++i) {
                if (this.array[this.start + i] != list.array[list.start + i]) {
                    return false;
                }
            }
            return true;
        }
        
        @Override
        public Float get(final int n) {
            i71.p(n, this.size());
            return this.array[this.start + n];
        }
        
        @Override
        public int hashCode() {
            int i = this.start;
            int n = 1;
            while (i < this.end) {
                n = n * 31 + Floats.c(this.array[i]);
                ++i;
            }
            return n;
        }
        
        @Override
        public int indexOf(final Object o) {
            if (o instanceof Float) {
                final int a = d(this.array, (float)o, this.start, this.end);
                if (a >= 0) {
                    return a - this.start;
                }
            }
            return -1;
        }
        
        @Override
        public boolean isEmpty() {
            return false;
        }
        
        @Override
        public int lastIndexOf(final Object o) {
            if (o instanceof Float) {
                final int b = e(this.array, (float)o, this.start, this.end);
                if (b >= 0) {
                    return b - this.start;
                }
            }
            return -1;
        }
        
        @Override
        public Float set(final int n, final Float n2) {
            i71.p(n, this.size());
            final float[] array = this.array;
            final int start = this.start;
            final float f = array[start + n];
            array[start + n] = (float)i71.r(n2);
            return f;
        }
        
        @Override
        public int size() {
            return this.end - this.start;
        }
        
        @Override
        public List<Float> subList(final int n, final int n2) {
            i71.w(n, n2, this.size());
            if (n == n2) {
                return Collections.emptyList();
            }
            final float[] array = this.array;
            final int start = this.start;
            return new FloatArrayAsList(array, n + start, start + n2);
        }
        
        public float[] toFloatArray() {
            return Arrays.copyOfRange(this.array, this.start, this.end);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder(this.size() * 12);
            sb.append('[');
            sb.append(this.array[this.start]);
            int start = this.start;
            while (++start < this.end) {
                sb.append(", ");
                sb.append(this.array[start]);
            }
            sb.append(']');
            return sb.toString();
        }
    }
    
    public static final class FloatConverter extends Converter implements Serializable
    {
        static final FloatConverter INSTANCE;
        private static final long serialVersionUID = 1L;
        
        static {
            INSTANCE = new FloatConverter();
        }
        
        private FloatConverter() {
        }
        
        private Object readResolve() {
            return FloatConverter.INSTANCE;
        }
        
        public String doBackward(final Float n) {
            return n.toString();
        }
        
        public Float doForward(final String s) {
            return Float.valueOf(s);
        }
        
        @Override
        public String toString() {
            return "Floats.stringConverter()";
        }
    }
    
    public enum LexicographicalComparator implements Comparator<float[]>
    {
        private static final LexicographicalComparator[] $VALUES;
        
        INSTANCE;
        
        private static /* synthetic */ LexicographicalComparator[] $values() {
            return new LexicographicalComparator[] { LexicographicalComparator.INSTANCE };
        }
        
        static {
            $VALUES = $values();
        }
        
        @Override
        public int compare(final float[] array, final float[] array2) {
            for (int min = Math.min(array.length, array2.length), i = 0; i < min; ++i) {
                final int compare = Float.compare(array[i], array2[i]);
                if (compare != 0) {
                    return compare;
                }
            }
            return array.length - array2.length;
        }
        
        @Override
        public String toString() {
            return "Floats.lexicographicalComparator()";
        }
    }
}

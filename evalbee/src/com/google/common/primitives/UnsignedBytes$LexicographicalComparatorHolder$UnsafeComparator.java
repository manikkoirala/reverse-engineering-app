// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.primitives;

import java.security.PrivilegedActionException;
import java.security.AccessController;
import java.lang.reflect.Field;
import java.security.PrivilegedExceptionAction;
import java.nio.ByteOrder;
import sun.misc.Unsafe;
import java.util.Comparator;

enum UnsignedBytes$LexicographicalComparatorHolder$UnsafeComparator implements Comparator<byte[]>
{
    private static final UnsignedBytes$LexicographicalComparatorHolder$UnsafeComparator[] $VALUES;
    static final boolean BIG_ENDIAN;
    static final int BYTE_ARRAY_BASE_OFFSET;
    
    INSTANCE;
    
    static final Unsafe theUnsafe;
    
    private static /* synthetic */ UnsignedBytes$LexicographicalComparatorHolder$UnsafeComparator[] $values() {
        return new UnsignedBytes$LexicographicalComparatorHolder$UnsafeComparator[] { UnsignedBytes$LexicographicalComparatorHolder$UnsafeComparator.INSTANCE };
    }
    
    static {
        $VALUES = $values();
        BIG_ENDIAN = ByteOrder.nativeOrder().equals(ByteOrder.BIG_ENDIAN);
        final Unsafe unsafe = theUnsafe = getUnsafe();
        final int n = BYTE_ARRAY_BASE_OFFSET = unsafe.arrayBaseOffset(byte[].class);
        if ("64".equals(System.getProperty("sun.arch.data.model")) && n % 8 == 0 && unsafe.arrayIndexScale(byte[].class) == 1) {
            return;
        }
        throw new Error();
    }
    
    private static Unsafe getUnsafe() {
        try {
            return Unsafe.getUnsafe();
        }
        catch (final SecurityException ex) {
            try {
                return AccessController.doPrivileged((PrivilegedExceptionAction<Unsafe>)new PrivilegedExceptionAction() {
                    public Unsafe a() {
                        for (final Field field : Unsafe.class.getDeclaredFields()) {
                            field.setAccessible(true);
                            final Object value = field.get(null);
                            if (Unsafe.class.isInstance(value)) {
                                return Unsafe.class.cast(value);
                            }
                        }
                        throw new NoSuchFieldError("the Unsafe");
                    }
                });
            }
            catch (final PrivilegedActionException ex2) {
                throw new RuntimeException("Could not initialize intrinsics", ex2.getCause());
            }
        }
    }
    
    @Override
    public int compare(final byte[] o, final byte[] o2) {
        final int min = Math.min(o.length, o2.length);
        int n = 0;
        int i;
        while (true) {
            i = n;
            if (n >= (min & 0xFFFFFFF8)) {
                break;
            }
            final Unsafe theUnsafe = UnsignedBytes$LexicographicalComparatorHolder$UnsafeComparator.theUnsafe;
            final int byte_ARRAY_BASE_OFFSET = UnsignedBytes$LexicographicalComparatorHolder$UnsafeComparator.BYTE_ARRAY_BASE_OFFSET;
            final long n2 = byte_ARRAY_BASE_OFFSET;
            final long n3 = n;
            final long long1 = theUnsafe.getLong(o, n2 + n3);
            final long long2 = theUnsafe.getLong(o2, byte_ARRAY_BASE_OFFSET + n3);
            if (long1 != long2) {
                if (UnsignedBytes$LexicographicalComparatorHolder$UnsafeComparator.BIG_ENDIAN) {
                    return UnsignedLongs.a(long1, long2);
                }
                final int n4 = Long.numberOfTrailingZeros(long1 ^ long2) & 0xFFFFFFF8;
                return (int)(long1 >>> n4 & 0xFFL) - (int)(long2 >>> n4 & 0xFFL);
            }
            else {
                n += 8;
            }
        }
        while (i < min) {
            final int b = f12.b(o[i], o2[i]);
            if (b != 0) {
                return b;
            }
            ++i;
        }
        return o.length - o2.length;
    }
    
    @Override
    public String toString() {
        return "UnsignedBytes.lexicographicalComparator() (sun.misc.Unsafe version)";
    }
}

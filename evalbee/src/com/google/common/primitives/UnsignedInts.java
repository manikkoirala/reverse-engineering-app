// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.primitives;

import java.util.Comparator;

public abstract class UnsignedInts
{
    public static int a(final int n, final int n2) {
        return Ints.d(c(n), c(n2));
    }
    
    public static int b(final int n, final int n2) {
        return (int)(f(n) / f(n2));
    }
    
    public static int c(final int n) {
        return n ^ Integer.MIN_VALUE;
    }
    
    public static int d(final String str, final int n) {
        i71.r(str);
        final long long1 = Long.parseLong(str, n);
        if ((0xFFFFFFFFL & long1) == long1) {
            return (int)long1;
        }
        final StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 69);
        sb.append("Input ");
        sb.append(str);
        sb.append(" in base ");
        sb.append(n);
        sb.append(" is not in the range of an unsigned integer");
        throw new NumberFormatException(sb.toString());
    }
    
    public static int e(final int n, final int n2) {
        return (int)(f(n) % f(n2));
    }
    
    public static long f(final int n) {
        return (long)n & 0xFFFFFFFFL;
    }
    
    public static String g(final int n, final int radix) {
        return Long.toString((long)n & 0xFFFFFFFFL, radix);
    }
    
    public enum LexicographicalComparator implements Comparator<int[]>
    {
        private static final LexicographicalComparator[] $VALUES;
        
        INSTANCE;
        
        private static /* synthetic */ LexicographicalComparator[] $values() {
            return new LexicographicalComparator[] { LexicographicalComparator.INSTANCE };
        }
        
        static {
            $VALUES = $values();
        }
        
        @Override
        public int compare(final int[] array, final int[] array2) {
            for (int min = Math.min(array.length, array2.length), i = 0; i < min; ++i) {
                final int n = array[i];
                final int n2 = array2[i];
                if (n != n2) {
                    return UnsignedInts.a(n, n2);
                }
            }
            return array.length - array2.length;
        }
        
        @Override
        public String toString() {
            return "UnsignedInts.lexicographicalComparator()";
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.primitives;

import java.util.Comparator;
import com.google.common.base.Converter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.io.Serializable;
import java.util.RandomAccess;
import java.util.AbstractList;
import java.util.Collection;

public abstract class Ints extends hg0
{
    public static int c(final long n) {
        final int n2 = (int)n;
        i71.j(n2 == n, "Out of range: %s", n);
        return n2;
    }
    
    public static int d(int n, final int n2) {
        if (n < n2) {
            n = -1;
        }
        else if (n > n2) {
            n = 1;
        }
        else {
            n = 0;
        }
        return n;
    }
    
    public static int e(final int a, final int b, final int b2) {
        i71.i(b <= b2, "min (%s) must be less than or equal to max (%s)", b, b2);
        return Math.min(Math.max(a, b), b2);
    }
    
    public static int f(final byte b, final byte b2, final byte b3, final byte b4) {
        return b << 24 | (b2 & 0xFF) << 16 | (b3 & 0xFF) << 8 | (b4 & 0xFF);
    }
    
    public static int g(final int n) {
        return n;
    }
    
    public static int h(final int[] array, final int n, int i, final int n2) {
        while (i < n2) {
            if (array[i] == n) {
                return i;
            }
            ++i;
        }
        return -1;
    }
    
    public static int i(final int[] array, final int n, final int n2, int i) {
        --i;
        while (i >= n2) {
            if (array[i] == n) {
                return i;
            }
            --i;
        }
        return -1;
    }
    
    public static int j(final int... array) {
        final int length = array.length;
        int i = 1;
        i71.d(length > 0);
        int n = array[0];
        while (i < array.length) {
            final int n2 = array[i];
            int n3;
            if (n2 < (n3 = n)) {
                n3 = n2;
            }
            ++i;
            n = n3;
        }
        return n;
    }
    
    public static int k(final long n) {
        if (n > 2147483647L) {
            return Integer.MAX_VALUE;
        }
        if (n < -2147483648L) {
            return Integer.MIN_VALUE;
        }
        return (int)n;
    }
    
    public static int[] l(final Collection collection) {
        if (collection instanceof IntArrayAsList) {
            return ((IntArrayAsList)collection).toIntArray();
        }
        final Object[] array = collection.toArray();
        final int length = array.length;
        final int[] array2 = new int[length];
        for (int i = 0; i < length; ++i) {
            array2[i] = ((Number)i71.r(array[i])).intValue();
        }
        return array2;
    }
    
    public static class IntArrayAsList extends AbstractList<Integer> implements RandomAccess, Serializable
    {
        private static final long serialVersionUID = 0L;
        final int[] array;
        final int end;
        final int start;
        
        public IntArrayAsList(final int[] array) {
            this(array, 0, array.length);
        }
        
        public IntArrayAsList(final int[] array, final int start, final int end) {
            this.array = array;
            this.start = start;
            this.end = end;
        }
        
        @Override
        public boolean contains(final Object o) {
            return o instanceof Integer && h(this.array, (int)o, this.start, this.end) != -1;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof IntArrayAsList)) {
                return super.equals(o);
            }
            final IntArrayAsList list = (IntArrayAsList)o;
            final int size = this.size();
            if (list.size() != size) {
                return false;
            }
            for (int i = 0; i < size; ++i) {
                if (this.array[this.start + i] != list.array[list.start + i]) {
                    return false;
                }
            }
            return true;
        }
        
        @Override
        public Integer get(final int n) {
            i71.p(n, this.size());
            return this.array[this.start + n];
        }
        
        @Override
        public int hashCode() {
            int i = this.start;
            int n = 1;
            while (i < this.end) {
                n = n * 31 + Ints.g(this.array[i]);
                ++i;
            }
            return n;
        }
        
        @Override
        public int indexOf(final Object o) {
            if (o instanceof Integer) {
                final int a = h(this.array, (int)o, this.start, this.end);
                if (a >= 0) {
                    return a - this.start;
                }
            }
            return -1;
        }
        
        @Override
        public boolean isEmpty() {
            return false;
        }
        
        @Override
        public int lastIndexOf(final Object o) {
            if (o instanceof Integer) {
                final int b = i(this.array, (int)o, this.start, this.end);
                if (b >= 0) {
                    return b - this.start;
                }
            }
            return -1;
        }
        
        @Override
        public Integer set(final int n, final Integer n2) {
            i71.p(n, this.size());
            final int[] array = this.array;
            final int start = this.start;
            final int i = array[start + n];
            array[start + n] = (int)i71.r(n2);
            return i;
        }
        
        @Override
        public int size() {
            return this.end - this.start;
        }
        
        @Override
        public List<Integer> subList(final int n, final int n2) {
            i71.w(n, n2, this.size());
            if (n == n2) {
                return Collections.emptyList();
            }
            final int[] array = this.array;
            final int start = this.start;
            return new IntArrayAsList(array, n + start, start + n2);
        }
        
        public int[] toIntArray() {
            return Arrays.copyOfRange(this.array, this.start, this.end);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder(this.size() * 5);
            sb.append('[');
            sb.append(this.array[this.start]);
            int start = this.start;
            while (++start < this.end) {
                sb.append(", ");
                sb.append(this.array[start]);
            }
            sb.append(']');
            return sb.toString();
        }
    }
    
    public static final class IntConverter extends Converter implements Serializable
    {
        static final IntConverter INSTANCE;
        private static final long serialVersionUID = 1L;
        
        static {
            INSTANCE = new IntConverter();
        }
        
        private IntConverter() {
        }
        
        private Object readResolve() {
            return IntConverter.INSTANCE;
        }
        
        public String doBackward(final Integer n) {
            return n.toString();
        }
        
        public Integer doForward(final String nm) {
            return Integer.decode(nm);
        }
        
        @Override
        public String toString() {
            return "Ints.stringConverter()";
        }
    }
    
    public enum LexicographicalComparator implements Comparator<int[]>
    {
        private static final LexicographicalComparator[] $VALUES;
        
        INSTANCE;
        
        private static /* synthetic */ LexicographicalComparator[] $values() {
            return new LexicographicalComparator[] { LexicographicalComparator.INSTANCE };
        }
        
        static {
            $VALUES = $values();
        }
        
        @Override
        public int compare(final int[] array, final int[] array2) {
            for (int min = Math.min(array.length, array2.length), i = 0; i < min; ++i) {
                final int d = Ints.d(array[i], array2[i]);
                if (d != 0) {
                    return d;
                }
            }
            return array.length - array2.length;
        }
        
        @Override
        public String toString() {
            return "Ints.lexicographicalComparator()";
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.primitives;

import java.util.Comparator;

enum UnsignedBytes$LexicographicalComparatorHolder$PureJavaComparator implements Comparator<byte[]>
{
    private static final UnsignedBytes$LexicographicalComparatorHolder$PureJavaComparator[] $VALUES;
    
    INSTANCE;
    
    private static /* synthetic */ UnsignedBytes$LexicographicalComparatorHolder$PureJavaComparator[] $values() {
        return new UnsignedBytes$LexicographicalComparatorHolder$PureJavaComparator[] { UnsignedBytes$LexicographicalComparatorHolder$PureJavaComparator.INSTANCE };
    }
    
    static {
        $VALUES = $values();
    }
    
    @Override
    public int compare(final byte[] array, final byte[] array2) {
        for (int min = Math.min(array.length, array2.length), i = 0; i < min; ++i) {
            final int b = f12.b(array[i], array2[i]);
            if (b != 0) {
                return b;
            }
        }
        return array.length - array2.length;
    }
    
    @Override
    public String toString() {
        return "UnsignedBytes.lexicographicalComparator() (pure Java version)";
    }
}

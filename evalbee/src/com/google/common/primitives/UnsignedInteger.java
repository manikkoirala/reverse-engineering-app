// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.primitives;

import java.math.BigInteger;

public final class UnsignedInteger extends Number implements Comparable<UnsignedInteger>
{
    public static final UnsignedInteger MAX_VALUE;
    public static final UnsignedInteger ONE;
    public static final UnsignedInteger ZERO;
    private final int value;
    
    static {
        ZERO = fromIntBits(0);
        ONE = fromIntBits(1);
        MAX_VALUE = fromIntBits(-1);
    }
    
    private UnsignedInteger(final int n) {
        this.value = (n & -1);
    }
    
    public static UnsignedInteger fromIntBits(final int n) {
        return new UnsignedInteger(n);
    }
    
    public static UnsignedInteger valueOf(final long n) {
        i71.j((0xFFFFFFFFL & n) == n, "value (%s) is outside the range for an unsigned integer value", n);
        return fromIntBits((int)n);
    }
    
    public static UnsignedInteger valueOf(final String s) {
        return valueOf(s, 10);
    }
    
    public static UnsignedInteger valueOf(final String s, final int n) {
        return fromIntBits(UnsignedInts.d(s, n));
    }
    
    public static UnsignedInteger valueOf(final BigInteger bigInteger) {
        i71.r(bigInteger);
        i71.m(bigInteger.signum() >= 0 && bigInteger.bitLength() <= 32, "value (%s) is outside the range for an unsigned integer value", bigInteger);
        return fromIntBits(bigInteger.intValue());
    }
    
    public BigInteger bigIntegerValue() {
        return BigInteger.valueOf(this.longValue());
    }
    
    @Override
    public int compareTo(final UnsignedInteger unsignedInteger) {
        i71.r(unsignedInteger);
        return UnsignedInts.a(this.value, unsignedInteger.value);
    }
    
    public UnsignedInteger dividedBy(final UnsignedInteger unsignedInteger) {
        return fromIntBits(UnsignedInts.b(this.value, ((UnsignedInteger)i71.r(unsignedInteger)).value));
    }
    
    @Override
    public double doubleValue() {
        return (double)this.longValue();
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof UnsignedInteger;
        boolean b2 = false;
        if (b) {
            final UnsignedInteger unsignedInteger = (UnsignedInteger)o;
            b2 = b2;
            if (this.value == unsignedInteger.value) {
                b2 = true;
            }
        }
        return b2;
    }
    
    @Override
    public float floatValue() {
        return (float)this.longValue();
    }
    
    @Override
    public int hashCode() {
        return this.value;
    }
    
    @Override
    public int intValue() {
        return this.value;
    }
    
    @Override
    public long longValue() {
        return UnsignedInts.f(this.value);
    }
    
    public UnsignedInteger minus(final UnsignedInteger unsignedInteger) {
        return fromIntBits(this.value - ((UnsignedInteger)i71.r(unsignedInteger)).value);
    }
    
    public UnsignedInteger mod(final UnsignedInteger unsignedInteger) {
        return fromIntBits(UnsignedInts.e(this.value, ((UnsignedInteger)i71.r(unsignedInteger)).value));
    }
    
    public UnsignedInteger plus(final UnsignedInteger unsignedInteger) {
        return fromIntBits(this.value + ((UnsignedInteger)i71.r(unsignedInteger)).value);
    }
    
    public UnsignedInteger times(final UnsignedInteger unsignedInteger) {
        return fromIntBits(this.value * ((UnsignedInteger)i71.r(unsignedInteger)).value);
    }
    
    @Override
    public String toString() {
        return this.toString(10);
    }
    
    public String toString(final int n) {
        return UnsignedInts.g(this.value, n);
    }
}

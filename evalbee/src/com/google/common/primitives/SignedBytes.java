// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.primitives;

import java.util.Comparator;

public abstract class SignedBytes
{
    public static byte a(final long n) {
        final byte b = (byte)n;
        i71.j(b == n, "Out of range: %s", n);
        return b;
    }
    
    public static int b(final byte b, final byte b2) {
        return b - b2;
    }
    
    public enum LexicographicalComparator implements Comparator<byte[]>
    {
        private static final LexicographicalComparator[] $VALUES;
        
        INSTANCE;
        
        private static /* synthetic */ LexicographicalComparator[] $values() {
            return new LexicographicalComparator[] { LexicographicalComparator.INSTANCE };
        }
        
        static {
            $VALUES = $values();
        }
        
        @Override
        public int compare(final byte[] array, final byte[] array2) {
            for (int min = Math.min(array.length, array2.length), i = 0; i < min; ++i) {
                final int b = SignedBytes.b(array[i], array2[i]);
                if (b != 0) {
                    return b;
                }
            }
            return array.length - array2.length;
        }
        
        @Override
        public String toString() {
            return "SignedBytes.lexicographicalComparator()";
        }
    }
}

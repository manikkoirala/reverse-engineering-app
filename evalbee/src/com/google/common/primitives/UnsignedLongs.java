// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.primitives;

import java.math.BigInteger;
import java.util.Comparator;

public abstract class UnsignedLongs
{
    public static int a(final long n, final long n2) {
        return Longs.c(c(n), c(n2));
    }
    
    public static long b(final long n, final long n2) {
        if (n2 < 0L) {
            if (a(n, n2) < 0) {
                return 0L;
            }
            return 1L;
        }
        else {
            if (n >= 0L) {
                return n / n2;
            }
            boolean b = true;
            final long n3 = (n >>> 1) / n2 << 1;
            if (a(n - n3 * n2, n2) < 0) {
                b = false;
            }
            return n3 + (b ? 1 : 0);
        }
    }
    
    public static long c(final long n) {
        return n ^ Long.MIN_VALUE;
    }
    
    public static long d(String concat, final int n) {
        i71.r(concat);
        if (((String)concat).length() == 0) {
            throw new NumberFormatException("empty string");
        }
        if (n >= 2 && n <= 36) {
            final int n2 = a.c[n];
            long n3 = 0L;
            for (int i = 0; i < ((String)concat).length(); ++i) {
                final int digit = Character.digit(((String)concat).charAt(i), n);
                if (digit == -1) {
                    throw new NumberFormatException((String)concat);
                }
                if (i > n2 - 1 && a.a(n3, digit, n)) {
                    if (((String)concat).length() != 0) {
                        concat = "Too large for unsigned long: ".concat((String)concat);
                    }
                    else {
                        concat = new String("Too large for unsigned long: ");
                    }
                    throw new NumberFormatException((String)concat);
                }
                n3 = n3 * n + digit;
            }
            return n3;
        }
        final StringBuilder sb = new StringBuilder(26);
        sb.append("illegal radix: ");
        sb.append(n);
        throw new NumberFormatException(sb.toString());
    }
    
    public static long e(long n, long n2) {
        if (n2 < 0L) {
            if (a(n, n2) < 0) {
                return n;
            }
            return n - n2;
        }
        else {
            if (n >= 0L) {
                return n % n2;
            }
            n -= ((n >>> 1) / n2 << 1) * n2;
            if (a(n, n2) < 0) {
                n2 = 0L;
            }
            return n - n2;
        }
    }
    
    public static String f(final long n) {
        return g(n, 10);
    }
    
    public static String g(long i, final int radix) {
        i71.h(radix >= 2 && radix <= 36, "radix (%s) must be between Character.MIN_RADIX and Character.MAX_RADIX", radix);
        final long n = lcmp(i, 0L);
        if (n == 0) {
            return "0";
        }
        if (n > 0) {
            return Long.toString(i, radix);
        }
        int n2 = 64;
        final char[] value = new char[64];
        final int n3 = radix - 1;
        int offset;
        if ((radix & n3) == 0x0) {
            final int numberOfTrailingZeros = Integer.numberOfTrailingZeros(radix);
            long n4;
            int n5;
            do {
                n5 = n2 - 1;
                value[n5] = Character.forDigit((int)i & n3, radix);
                n4 = i >>> numberOfTrailingZeros;
                n2 = n5;
                i = n4;
            } while (n4 != 0L);
            offset = n5;
        }
        else {
            long b;
            if ((radix & 0x1) == 0x0) {
                b = (i >>> 1) / (radix >>> 1);
            }
            else {
                b = b(i, radix);
            }
            final long n6 = radix;
            final char forDigit = Character.forDigit((int)(i - b * n6), radix);
            offset = 63;
            value[63] = forDigit;
            while (b > 0L) {
                --offset;
                value[offset] = Character.forDigit((int)(b % n6), radix);
                b /= n6;
            }
        }
        return new String(value, offset, 64 - offset);
    }
    
    public enum LexicographicalComparator implements Comparator<long[]>
    {
        private static final LexicographicalComparator[] $VALUES;
        
        INSTANCE;
        
        private static /* synthetic */ LexicographicalComparator[] $values() {
            return new LexicographicalComparator[] { LexicographicalComparator.INSTANCE };
        }
        
        static {
            $VALUES = $values();
        }
        
        @Override
        public int compare(final long[] array, final long[] array2) {
            for (int min = Math.min(array.length, array2.length), i = 0; i < min; ++i) {
                final long n = array[i];
                final long n2 = array2[i];
                if (n != n2) {
                    return UnsignedLongs.a(n, n2);
                }
            }
            return array.length - array2.length;
        }
        
        @Override
        public String toString() {
            return "UnsignedLongs.lexicographicalComparator()";
        }
    }
    
    public abstract static final class a
    {
        public static final long[] a;
        public static final int[] b;
        public static final int[] c;
        
        static {
            a = new long[37];
            b = new int[37];
            c = new int[37];
            final BigInteger bigInteger = new BigInteger("10000000000000000", 16);
            for (int i = 2; i <= 36; ++i) {
                final long[] a2 = UnsignedLongs.a.a;
                final long n = i;
                a2[i] = UnsignedLongs.b(-1L, n);
                UnsignedLongs.a.b[i] = (int)UnsignedLongs.e(-1L, n);
                UnsignedLongs.a.c[i] = bigInteger.toString(i).length() - 1;
            }
        }
        
        public static boolean a(final long n, final int n2, final int n3) {
            boolean b = true;
            if (n >= 0L) {
                final long n4 = UnsignedLongs.a.a[n3];
                if (n < n4) {
                    return false;
                }
                if (n > n4) {
                    return true;
                }
                b = (n2 > UnsignedLongs.a.b[n3] && b);
            }
            return b;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.primitives;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.io.Serializable;
import java.util.RandomAccess;
import java.util.AbstractList;

public abstract class Bytes
{
    public static int c(final byte b) {
        return b;
    }
    
    public static int d(final byte[] array, final byte b, int i, final int n) {
        while (i < n) {
            if (array[i] == b) {
                return i;
            }
            ++i;
        }
        return -1;
    }
    
    public static int e(final byte[] array, final byte b, final int n, int i) {
        --i;
        while (i >= n) {
            if (array[i] == b) {
                return i;
            }
            --i;
        }
        return -1;
    }
    
    public static class ByteArrayAsList extends AbstractList<Byte> implements RandomAccess, Serializable
    {
        private static final long serialVersionUID = 0L;
        final byte[] array;
        final int end;
        final int start;
        
        public ByteArrayAsList(final byte[] array) {
            this(array, 0, array.length);
        }
        
        public ByteArrayAsList(final byte[] array, final int start, final int end) {
            this.array = array;
            this.start = start;
            this.end = end;
        }
        
        @Override
        public boolean contains(final Object o) {
            return o instanceof Byte && d(this.array, (byte)o, this.start, this.end) != -1;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof ByteArrayAsList)) {
                return super.equals(o);
            }
            final ByteArrayAsList list = (ByteArrayAsList)o;
            final int size = this.size();
            if (list.size() != size) {
                return false;
            }
            for (int i = 0; i < size; ++i) {
                if (this.array[this.start + i] != list.array[list.start + i]) {
                    return false;
                }
            }
            return true;
        }
        
        @Override
        public Byte get(final int n) {
            i71.p(n, this.size());
            return this.array[this.start + n];
        }
        
        @Override
        public int hashCode() {
            int i = this.start;
            int n = 1;
            while (i < this.end) {
                n = n * 31 + Bytes.c(this.array[i]);
                ++i;
            }
            return n;
        }
        
        @Override
        public int indexOf(final Object o) {
            if (o instanceof Byte) {
                final int a = d(this.array, (byte)o, this.start, this.end);
                if (a >= 0) {
                    return a - this.start;
                }
            }
            return -1;
        }
        
        @Override
        public boolean isEmpty() {
            return false;
        }
        
        @Override
        public int lastIndexOf(final Object o) {
            if (o instanceof Byte) {
                final int b = e(this.array, (byte)o, this.start, this.end);
                if (b >= 0) {
                    return b - this.start;
                }
            }
            return -1;
        }
        
        @Override
        public Byte set(final int n, final Byte b) {
            i71.p(n, this.size());
            final byte[] array = this.array;
            final int start = this.start;
            final byte b2 = array[start + n];
            array[start + n] = (byte)i71.r(b);
            return b2;
        }
        
        @Override
        public int size() {
            return this.end - this.start;
        }
        
        @Override
        public List<Byte> subList(final int n, final int n2) {
            i71.w(n, n2, this.size());
            if (n == n2) {
                return Collections.emptyList();
            }
            final byte[] array = this.array;
            final int start = this.start;
            return new ByteArrayAsList(array, n + start, start + n2);
        }
        
        public byte[] toByteArray() {
            return Arrays.copyOfRange(this.array, this.start, this.end);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder(this.size() * 5);
            sb.append('[');
            sb.append(this.array[this.start]);
            int start = this.start;
            while (++start < this.end) {
                sb.append(", ");
                sb.append(this.array[start]);
            }
            sb.append(']');
            return sb.toString();
        }
    }
}

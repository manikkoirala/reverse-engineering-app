// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.primitives;

import java.util.Iterator;
import java.util.RandomAccess;
import java.util.AbstractList;
import java.util.List;
import java.util.Arrays;
import java.util.Collection;
import java.io.Serializable;

public final class ImmutableDoubleArray implements Serializable
{
    private static final ImmutableDoubleArray EMPTY;
    private final double[] array;
    private final int end;
    private final transient int start;
    
    static {
        EMPTY = new ImmutableDoubleArray(new double[0]);
    }
    
    private ImmutableDoubleArray(final double[] array) {
        this(array, 0, array.length);
    }
    
    private ImmutableDoubleArray(final double[] array, final int start, final int end) {
        this.array = array;
        this.start = start;
        this.end = end;
    }
    
    public static /* synthetic */ double[] access$000(final ImmutableDoubleArray immutableDoubleArray) {
        return immutableDoubleArray.array;
    }
    
    public static /* synthetic */ int access$100(final ImmutableDoubleArray immutableDoubleArray) {
        return immutableDoubleArray.start;
    }
    
    public static /* synthetic */ ImmutableDoubleArray access$200() {
        return ImmutableDoubleArray.EMPTY;
    }
    
    private static boolean areEqual(final double value, final double value2) {
        return Double.doubleToLongBits(value) == Double.doubleToLongBits(value2);
    }
    
    public static b builder() {
        return new b(10);
    }
    
    public static b builder(final int n) {
        i71.h(n >= 0, "Invalid initialCapacity: %s", n);
        return new b(n);
    }
    
    public static ImmutableDoubleArray copyOf(final Iterable<Double> iterable) {
        if (iterable instanceof Collection) {
            return copyOf((Collection<Double>)iterable);
        }
        return builder().b(iterable).d();
    }
    
    public static ImmutableDoubleArray copyOf(final Collection<Double> collection) {
        ImmutableDoubleArray empty;
        if (collection.isEmpty()) {
            empty = ImmutableDoubleArray.EMPTY;
        }
        else {
            empty = new ImmutableDoubleArray(Doubles.h(collection));
        }
        return empty;
    }
    
    public static ImmutableDoubleArray copyOf(final double[] original) {
        ImmutableDoubleArray empty;
        if (original.length == 0) {
            empty = ImmutableDoubleArray.EMPTY;
        }
        else {
            empty = new ImmutableDoubleArray(Arrays.copyOf(original, original.length));
        }
        return empty;
    }
    
    private boolean isPartialView() {
        return this.start > 0 || this.end < this.array.length;
    }
    
    public static ImmutableDoubleArray of() {
        return ImmutableDoubleArray.EMPTY;
    }
    
    public static ImmutableDoubleArray of(final double n) {
        return new ImmutableDoubleArray(new double[] { n });
    }
    
    public static ImmutableDoubleArray of(final double n, final double n2) {
        return new ImmutableDoubleArray(new double[] { n, n2 });
    }
    
    public static ImmutableDoubleArray of(final double n, final double n2, final double n3) {
        return new ImmutableDoubleArray(new double[] { n, n2, n3 });
    }
    
    public static ImmutableDoubleArray of(final double n, final double n2, final double n3, final double n4) {
        return new ImmutableDoubleArray(new double[] { n, n2, n3, n4 });
    }
    
    public static ImmutableDoubleArray of(final double n, final double n2, final double n3, final double n4, final double n5) {
        return new ImmutableDoubleArray(new double[] { n, n2, n3, n4, n5 });
    }
    
    public static ImmutableDoubleArray of(final double n, final double n2, final double n3, final double n4, final double n5, final double n6) {
        return new ImmutableDoubleArray(new double[] { n, n2, n3, n4, n5, n6 });
    }
    
    public static ImmutableDoubleArray of(final double n, final double... array) {
        i71.e(array.length <= 2147483646, "the total number of elements must fit in an int");
        final double[] array2 = new double[array.length + 1];
        array2[0] = n;
        System.arraycopy(array, 0, array2, 1, array.length);
        return new ImmutableDoubleArray(array2);
    }
    
    public List<Double> asList() {
        return new AsList(this, null);
    }
    
    public boolean contains(final double n) {
        return this.indexOf(n) >= 0;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof ImmutableDoubleArray)) {
            return false;
        }
        final ImmutableDoubleArray immutableDoubleArray = (ImmutableDoubleArray)o;
        if (this.length() != immutableDoubleArray.length()) {
            return false;
        }
        for (int i = 0; i < this.length(); ++i) {
            if (!areEqual(this.get(i), immutableDoubleArray.get(i))) {
                return false;
            }
        }
        return true;
    }
    
    public double get(final int n) {
        i71.p(n, this.length());
        return this.array[this.start + n];
    }
    
    @Override
    public int hashCode() {
        int i = this.start;
        int n = 1;
        while (i < this.end) {
            n = n * 31 + Doubles.d(this.array[i]);
            ++i;
        }
        return n;
    }
    
    public int indexOf(final double n) {
        for (int i = this.start; i < this.end; ++i) {
            if (areEqual(this.array[i], n)) {
                return i - this.start;
            }
        }
        return -1;
    }
    
    public boolean isEmpty() {
        return this.end == this.start;
    }
    
    public int lastIndexOf(final double n) {
        for (int i = this.end - 1; i >= this.start; --i) {
            if (areEqual(this.array[i], n)) {
                return i - this.start;
            }
        }
        return -1;
    }
    
    public int length() {
        return this.end - this.start;
    }
    
    public Object readResolve() {
        ImmutableDoubleArray empty;
        if (this.isEmpty()) {
            empty = ImmutableDoubleArray.EMPTY;
        }
        else {
            empty = this;
        }
        return empty;
    }
    
    public ImmutableDoubleArray subArray(final int n, final int n2) {
        i71.w(n, n2, this.length());
        ImmutableDoubleArray empty;
        if (n == n2) {
            empty = ImmutableDoubleArray.EMPTY;
        }
        else {
            final double[] array = this.array;
            final int start = this.start;
            empty = new ImmutableDoubleArray(array, n + start, start + n2);
        }
        return empty;
    }
    
    public double[] toArray() {
        return Arrays.copyOfRange(this.array, this.start, this.end);
    }
    
    @Override
    public String toString() {
        if (this.isEmpty()) {
            return "[]";
        }
        final StringBuilder sb = new StringBuilder(this.length() * 5);
        sb.append('[');
        sb.append(this.array[this.start]);
        int start = this.start;
        while (++start < this.end) {
            sb.append(", ");
            sb.append(this.array[start]);
        }
        sb.append(']');
        return sb.toString();
    }
    
    public ImmutableDoubleArray trimmed() {
        ImmutableDoubleArray immutableDoubleArray;
        if (this.isPartialView()) {
            immutableDoubleArray = new ImmutableDoubleArray(this.toArray());
        }
        else {
            immutableDoubleArray = this;
        }
        return immutableDoubleArray;
    }
    
    public Object writeReplace() {
        return this.trimmed();
    }
    
    public static class AsList extends AbstractList<Double> implements RandomAccess, Serializable
    {
        private final ImmutableDoubleArray parent;
        
        private AsList(final ImmutableDoubleArray parent) {
            this.parent = parent;
        }
        
        @Override
        public boolean contains(final Object o) {
            return this.indexOf(o) >= 0;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (o instanceof AsList) {
                return this.parent.equals(((AsList)o).parent);
            }
            if (!(o instanceof List)) {
                return false;
            }
            final List list = (List)o;
            if (this.size() != list.size()) {
                return false;
            }
            int access$100 = ImmutableDoubleArray.access$100(this.parent);
            for (final Object next : list) {
                if (!(next instanceof Double) || !areEqual(ImmutableDoubleArray.access$000(this.parent)[access$100], (double)next)) {
                    return false;
                }
                ++access$100;
            }
            return true;
        }
        
        @Override
        public Double get(final int n) {
            return this.parent.get(n);
        }
        
        @Override
        public int hashCode() {
            return this.parent.hashCode();
        }
        
        @Override
        public int indexOf(final Object o) {
            int index;
            if (o instanceof Double) {
                index = this.parent.indexOf((double)o);
            }
            else {
                index = -1;
            }
            return index;
        }
        
        @Override
        public int lastIndexOf(final Object o) {
            int lastIndex;
            if (o instanceof Double) {
                lastIndex = this.parent.lastIndexOf((double)o);
            }
            else {
                lastIndex = -1;
            }
            return lastIndex;
        }
        
        @Override
        public int size() {
            return this.parent.length();
        }
        
        @Override
        public List<Double> subList(final int n, final int n2) {
            return this.parent.subArray(n, n2).asList();
        }
        
        @Override
        public String toString() {
            return this.parent.toString();
        }
    }
    
    public static final class b
    {
        public double[] a;
        public int b;
        
        public b(final int n) {
            this.b = 0;
            this.a = new double[n];
        }
        
        public static int f(int n, int n2) {
            if (n2 >= 0) {
                if ((n = n + (n >> 1) + 1) < n2) {
                    n = Integer.highestOneBit(n2 - 1) << 1;
                }
                if ((n2 = n) < 0) {
                    n2 = Integer.MAX_VALUE;
                }
                return n2;
            }
            throw new AssertionError((Object)"cannot store more than MAX_VALUE elements");
        }
        
        public b a(final double n) {
            this.e(1);
            final double[] a = this.a;
            final int b = this.b;
            a[b] = n;
            this.b = b + 1;
            return this;
        }
        
        public b b(final Iterable iterable) {
            if (iterable instanceof Collection) {
                return this.c((Collection)iterable);
            }
            final Iterator iterator = iterable.iterator();
            while (iterator.hasNext()) {
                this.a((double)iterator.next());
            }
            return this;
        }
        
        public b c(final Collection collection) {
            this.e(collection.size());
            final Iterator iterator = collection.iterator();
            while (iterator.hasNext()) {
                this.a[this.b++] = (double)iterator.next();
            }
            return this;
        }
        
        public ImmutableDoubleArray d() {
            ImmutableDoubleArray access$200;
            if (this.b == 0) {
                access$200 = ImmutableDoubleArray.access$200();
            }
            else {
                access$200 = new ImmutableDoubleArray(this.a, 0, this.b, null);
            }
            return access$200;
        }
        
        public final void e(int n) {
            n += this.b;
            final double[] a = this.a;
            if (n > a.length) {
                this.a = Arrays.copyOf(a, f(a.length, n));
            }
        }
    }
}

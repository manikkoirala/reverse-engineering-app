// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.primitives;

import java.util.Comparator;
import com.google.common.base.Converter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.io.Serializable;
import java.util.RandomAccess;
import java.util.AbstractList;
import java.util.Collection;
import java.util.regex.Pattern;

public abstract class Doubles extends ru
{
    public static final Pattern a;
    
    static {
        a = c();
    }
    
    public static Pattern c() {
        final String concat = "(?:\\d+#(?:\\.\\d*#)?|\\.\\d+#)".concat("(?:[eE][+-]?\\d+#)?[fFdD]?");
        final StringBuilder sb = new StringBuilder("(?:[0-9a-fA-F]+#(?:\\.[0-9a-fA-F]*#)?|\\.[0-9a-fA-F]+#)".length() + 25);
        sb.append("0[xX]");
        sb.append("(?:[0-9a-fA-F]+#(?:\\.[0-9a-fA-F]*#)?|\\.[0-9a-fA-F]+#)");
        sb.append("[pP][+-]?\\d+#[fFdD]?");
        final String string = sb.toString();
        final StringBuilder sb2 = new StringBuilder(String.valueOf(concat).length() + 23 + String.valueOf(string).length());
        sb2.append("[+-]?(?:NaN|Infinity|");
        sb2.append(concat);
        sb2.append("|");
        sb2.append(string);
        sb2.append(")");
        return Pattern.compile(sb2.toString().replace("#", "+"));
    }
    
    public static int d(final double d) {
        return Double.valueOf(d).hashCode();
    }
    
    public static int e(final double[] array, final double n, int i, final int n2) {
        while (i < n2) {
            if (array[i] == n) {
                return i;
            }
            ++i;
        }
        return -1;
    }
    
    public static boolean f(final double n) {
        return Double.NEGATIVE_INFINITY < n && n < Double.POSITIVE_INFINITY;
    }
    
    public static int g(final double[] array, final double n, final int n2, int i) {
        --i;
        while (i >= n2) {
            if (array[i] == n) {
                return i;
            }
            --i;
        }
        return -1;
    }
    
    public static double[] h(final Collection collection) {
        if (collection instanceof DoubleArrayAsList) {
            return ((DoubleArrayAsList)collection).toDoubleArray();
        }
        final Object[] array = collection.toArray();
        final int length = array.length;
        final double[] array2 = new double[length];
        for (int i = 0; i < length; ++i) {
            array2[i] = ((Number)i71.r(array[i])).doubleValue();
        }
        return array2;
    }
    
    public static class DoubleArrayAsList extends AbstractList<Double> implements RandomAccess, Serializable
    {
        private static final long serialVersionUID = 0L;
        final double[] array;
        final int end;
        final int start;
        
        public DoubleArrayAsList(final double[] array) {
            this(array, 0, array.length);
        }
        
        public DoubleArrayAsList(final double[] array, final int start, final int end) {
            this.array = array;
            this.start = start;
            this.end = end;
        }
        
        @Override
        public boolean contains(final Object o) {
            return o instanceof Double && e(this.array, (double)o, this.start, this.end) != -1;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof DoubleArrayAsList)) {
                return super.equals(o);
            }
            final DoubleArrayAsList list = (DoubleArrayAsList)o;
            final int size = this.size();
            if (list.size() != size) {
                return false;
            }
            for (int i = 0; i < size; ++i) {
                if (this.array[this.start + i] != list.array[list.start + i]) {
                    return false;
                }
            }
            return true;
        }
        
        @Override
        public Double get(final int n) {
            i71.p(n, this.size());
            return this.array[this.start + n];
        }
        
        @Override
        public int hashCode() {
            int i = this.start;
            int n = 1;
            while (i < this.end) {
                n = n * 31 + Doubles.d(this.array[i]);
                ++i;
            }
            return n;
        }
        
        @Override
        public int indexOf(final Object o) {
            if (o instanceof Double) {
                final int a = e(this.array, (double)o, this.start, this.end);
                if (a >= 0) {
                    return a - this.start;
                }
            }
            return -1;
        }
        
        @Override
        public boolean isEmpty() {
            return false;
        }
        
        @Override
        public int lastIndexOf(final Object o) {
            if (o instanceof Double) {
                final int b = g(this.array, (double)o, this.start, this.end);
                if (b >= 0) {
                    return b - this.start;
                }
            }
            return -1;
        }
        
        @Override
        public Double set(final int n, final Double n2) {
            i71.p(n, this.size());
            final double[] array = this.array;
            final int start = this.start;
            final double d = array[start + n];
            array[start + n] = (double)i71.r(n2);
            return d;
        }
        
        @Override
        public int size() {
            return this.end - this.start;
        }
        
        @Override
        public List<Double> subList(final int n, final int n2) {
            i71.w(n, n2, this.size());
            if (n == n2) {
                return Collections.emptyList();
            }
            final double[] array = this.array;
            final int start = this.start;
            return new DoubleArrayAsList(array, n + start, start + n2);
        }
        
        public double[] toDoubleArray() {
            return Arrays.copyOfRange(this.array, this.start, this.end);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder(this.size() * 12);
            sb.append('[');
            sb.append(this.array[this.start]);
            int start = this.start;
            while (++start < this.end) {
                sb.append(", ");
                sb.append(this.array[start]);
            }
            sb.append(']');
            return sb.toString();
        }
    }
    
    public static final class DoubleConverter extends Converter implements Serializable
    {
        static final DoubleConverter INSTANCE;
        private static final long serialVersionUID = 1L;
        
        static {
            INSTANCE = new DoubleConverter();
        }
        
        private DoubleConverter() {
        }
        
        private Object readResolve() {
            return DoubleConverter.INSTANCE;
        }
        
        public String doBackward(final Double n) {
            return n.toString();
        }
        
        public Double doForward(final String s) {
            return Double.valueOf(s);
        }
        
        @Override
        public String toString() {
            return "Doubles.stringConverter()";
        }
    }
    
    public enum LexicographicalComparator implements Comparator<double[]>
    {
        private static final LexicographicalComparator[] $VALUES;
        
        INSTANCE;
        
        private static /* synthetic */ LexicographicalComparator[] $values() {
            return new LexicographicalComparator[] { LexicographicalComparator.INSTANCE };
        }
        
        static {
            $VALUES = $values();
        }
        
        @Override
        public int compare(final double[] array, final double[] array2) {
            for (int min = Math.min(array.length, array2.length), i = 0; i < min; ++i) {
                final int compare = Double.compare(array[i], array2[i]);
                if (compare != 0) {
                    return compare;
                }
            }
            return array.length - array2.length;
        }
        
        @Override
        public String toString() {
            return "Doubles.lexicographicalComparator()";
        }
    }
}

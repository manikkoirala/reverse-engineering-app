// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.primitives;

import java.util.Iterator;
import java.util.RandomAccess;
import java.util.AbstractList;
import java.util.List;
import java.util.Arrays;
import java.util.Collection;
import java.io.Serializable;

public final class ImmutableLongArray implements Serializable
{
    private static final ImmutableLongArray EMPTY;
    private final long[] array;
    private final int end;
    private final transient int start;
    
    static {
        EMPTY = new ImmutableLongArray(new long[0]);
    }
    
    private ImmutableLongArray(final long[] array) {
        this(array, 0, array.length);
    }
    
    private ImmutableLongArray(final long[] array, final int start, final int end) {
        this.array = array;
        this.start = start;
        this.end = end;
    }
    
    public static /* synthetic */ long[] access$000(final ImmutableLongArray immutableLongArray) {
        return immutableLongArray.array;
    }
    
    public static /* synthetic */ int access$100(final ImmutableLongArray immutableLongArray) {
        return immutableLongArray.start;
    }
    
    public static /* synthetic */ ImmutableLongArray access$200() {
        return ImmutableLongArray.EMPTY;
    }
    
    public static b builder() {
        return new b(10);
    }
    
    public static b builder(final int n) {
        i71.h(n >= 0, "Invalid initialCapacity: %s", n);
        return new b(n);
    }
    
    public static ImmutableLongArray copyOf(final Iterable<Long> iterable) {
        if (iterable instanceof Collection) {
            return copyOf((Collection<Long>)iterable);
        }
        return builder().b(iterable).d();
    }
    
    public static ImmutableLongArray copyOf(final Collection<Long> collection) {
        ImmutableLongArray empty;
        if (collection.isEmpty()) {
            empty = ImmutableLongArray.EMPTY;
        }
        else {
            empty = new ImmutableLongArray(Longs.h(collection));
        }
        return empty;
    }
    
    public static ImmutableLongArray copyOf(final long[] original) {
        ImmutableLongArray empty;
        if (original.length == 0) {
            empty = ImmutableLongArray.EMPTY;
        }
        else {
            empty = new ImmutableLongArray(Arrays.copyOf(original, original.length));
        }
        return empty;
    }
    
    private boolean isPartialView() {
        return this.start > 0 || this.end < this.array.length;
    }
    
    public static ImmutableLongArray of() {
        return ImmutableLongArray.EMPTY;
    }
    
    public static ImmutableLongArray of(final long n) {
        return new ImmutableLongArray(new long[] { n });
    }
    
    public static ImmutableLongArray of(final long n, final long n2) {
        return new ImmutableLongArray(new long[] { n, n2 });
    }
    
    public static ImmutableLongArray of(final long n, final long n2, final long n3) {
        return new ImmutableLongArray(new long[] { n, n2, n3 });
    }
    
    public static ImmutableLongArray of(final long n, final long n2, final long n3, final long n4) {
        return new ImmutableLongArray(new long[] { n, n2, n3, n4 });
    }
    
    public static ImmutableLongArray of(final long n, final long n2, final long n3, final long n4, final long n5) {
        return new ImmutableLongArray(new long[] { n, n2, n3, n4, n5 });
    }
    
    public static ImmutableLongArray of(final long n, final long n2, final long n3, final long n4, final long n5, final long n6) {
        return new ImmutableLongArray(new long[] { n, n2, n3, n4, n5, n6 });
    }
    
    public static ImmutableLongArray of(final long n, final long... array) {
        i71.e(array.length <= 2147483646, "the total number of elements must fit in an int");
        final long[] array2 = new long[array.length + 1];
        array2[0] = n;
        System.arraycopy(array, 0, array2, 1, array.length);
        return new ImmutableLongArray(array2);
    }
    
    public List<Long> asList() {
        return new AsList(this, null);
    }
    
    public boolean contains(final long n) {
        return this.indexOf(n) >= 0;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof ImmutableLongArray)) {
            return false;
        }
        final ImmutableLongArray immutableLongArray = (ImmutableLongArray)o;
        if (this.length() != immutableLongArray.length()) {
            return false;
        }
        for (int i = 0; i < this.length(); ++i) {
            if (this.get(i) != immutableLongArray.get(i)) {
                return false;
            }
        }
        return true;
    }
    
    public long get(final int n) {
        i71.p(n, this.length());
        return this.array[this.start + n];
    }
    
    @Override
    public int hashCode() {
        int i = this.start;
        int n = 1;
        while (i < this.end) {
            n = n * 31 + Longs.e(this.array[i]);
            ++i;
        }
        return n;
    }
    
    public int indexOf(final long n) {
        for (int i = this.start; i < this.end; ++i) {
            if (this.array[i] == n) {
                return i - this.start;
            }
        }
        return -1;
    }
    
    public boolean isEmpty() {
        return this.end == this.start;
    }
    
    public int lastIndexOf(final long n) {
        int n2 = this.end - 1;
        while (true) {
            final int start = this.start;
            if (n2 < start) {
                return -1;
            }
            if (this.array[n2] == n) {
                return n2 - start;
            }
            --n2;
        }
    }
    
    public int length() {
        return this.end - this.start;
    }
    
    public Object readResolve() {
        ImmutableLongArray empty;
        if (this.isEmpty()) {
            empty = ImmutableLongArray.EMPTY;
        }
        else {
            empty = this;
        }
        return empty;
    }
    
    public ImmutableLongArray subArray(final int n, final int n2) {
        i71.w(n, n2, this.length());
        ImmutableLongArray empty;
        if (n == n2) {
            empty = ImmutableLongArray.EMPTY;
        }
        else {
            final long[] array = this.array;
            final int start = this.start;
            empty = new ImmutableLongArray(array, n + start, start + n2);
        }
        return empty;
    }
    
    public long[] toArray() {
        return Arrays.copyOfRange(this.array, this.start, this.end);
    }
    
    @Override
    public String toString() {
        if (this.isEmpty()) {
            return "[]";
        }
        final StringBuilder sb = new StringBuilder(this.length() * 5);
        sb.append('[');
        sb.append(this.array[this.start]);
        int start = this.start;
        while (++start < this.end) {
            sb.append(", ");
            sb.append(this.array[start]);
        }
        sb.append(']');
        return sb.toString();
    }
    
    public ImmutableLongArray trimmed() {
        ImmutableLongArray immutableLongArray;
        if (this.isPartialView()) {
            immutableLongArray = new ImmutableLongArray(this.toArray());
        }
        else {
            immutableLongArray = this;
        }
        return immutableLongArray;
    }
    
    public Object writeReplace() {
        return this.trimmed();
    }
    
    public static class AsList extends AbstractList<Long> implements RandomAccess, Serializable
    {
        private final ImmutableLongArray parent;
        
        private AsList(final ImmutableLongArray parent) {
            this.parent = parent;
        }
        
        @Override
        public boolean contains(final Object o) {
            return this.indexOf(o) >= 0;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (o instanceof AsList) {
                return this.parent.equals(((AsList)o).parent);
            }
            if (!(o instanceof List)) {
                return false;
            }
            final List list = (List)o;
            if (this.size() != list.size()) {
                return false;
            }
            int access$100 = ImmutableLongArray.access$100(this.parent);
            for (final Object next : list) {
                if (!(next instanceof Long) || ImmutableLongArray.access$000(this.parent)[access$100] != (long)next) {
                    return false;
                }
                ++access$100;
            }
            return true;
        }
        
        @Override
        public Long get(final int n) {
            return this.parent.get(n);
        }
        
        @Override
        public int hashCode() {
            return this.parent.hashCode();
        }
        
        @Override
        public int indexOf(final Object o) {
            int index;
            if (o instanceof Long) {
                index = this.parent.indexOf((long)o);
            }
            else {
                index = -1;
            }
            return index;
        }
        
        @Override
        public int lastIndexOf(final Object o) {
            int lastIndex;
            if (o instanceof Long) {
                lastIndex = this.parent.lastIndexOf((long)o);
            }
            else {
                lastIndex = -1;
            }
            return lastIndex;
        }
        
        @Override
        public int size() {
            return this.parent.length();
        }
        
        @Override
        public List<Long> subList(final int n, final int n2) {
            return this.parent.subArray(n, n2).asList();
        }
        
        @Override
        public String toString() {
            return this.parent.toString();
        }
    }
    
    public static final class b
    {
        public long[] a;
        public int b;
        
        public b(final int n) {
            this.b = 0;
            this.a = new long[n];
        }
        
        public static int f(int n, int n2) {
            if (n2 >= 0) {
                if ((n = n + (n >> 1) + 1) < n2) {
                    n = Integer.highestOneBit(n2 - 1) << 1;
                }
                if ((n2 = n) < 0) {
                    n2 = Integer.MAX_VALUE;
                }
                return n2;
            }
            throw new AssertionError((Object)"cannot store more than MAX_VALUE elements");
        }
        
        public b a(final long n) {
            this.e(1);
            final long[] a = this.a;
            final int b = this.b;
            a[b] = n;
            this.b = b + 1;
            return this;
        }
        
        public b b(final Iterable iterable) {
            if (iterable instanceof Collection) {
                return this.c((Collection)iterable);
            }
            final Iterator iterator = iterable.iterator();
            while (iterator.hasNext()) {
                this.a((long)iterator.next());
            }
            return this;
        }
        
        public b c(final Collection collection) {
            this.e(collection.size());
            final Iterator iterator = collection.iterator();
            while (iterator.hasNext()) {
                this.a[this.b++] = (long)iterator.next();
            }
            return this;
        }
        
        public ImmutableLongArray d() {
            ImmutableLongArray access$200;
            if (this.b == 0) {
                access$200 = ImmutableLongArray.access$200();
            }
            else {
                access$200 = new ImmutableLongArray(this.a, 0, this.b, null);
            }
            return access$200;
        }
        
        public final void e(int n) {
            n += this.b;
            final long[] a = this.a;
            if (n > a.length) {
                this.a = Arrays.copyOf(a, f(a.length, n));
            }
        }
    }
}

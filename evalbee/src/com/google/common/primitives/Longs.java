// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.primitives;

import com.google.common.base.Converter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.io.Serializable;
import java.util.RandomAccess;
import java.util.AbstractList;
import java.util.Comparator;
import java.util.Collection;

public abstract class Longs
{
    public static int c(final long n, final long n2) {
        final long n3 = lcmp(n, n2);
        int n4;
        if (n3 < 0) {
            n4 = -1;
        }
        else if (n3 > 0) {
            n4 = 1;
        }
        else {
            n4 = 0;
        }
        return n4;
    }
    
    public static long d(final byte b, final byte b2, final byte b3, final byte b4, final byte b5, final byte b6, final byte b7, final byte b8) {
        return ((long)b2 & 0xFFL) << 48 | ((long)b & 0xFFL) << 56 | ((long)b3 & 0xFFL) << 40 | ((long)b4 & 0xFFL) << 32 | ((long)b5 & 0xFFL) << 24 | ((long)b6 & 0xFFL) << 16 | ((long)b7 & 0xFFL) << 8 | ((long)b8 & 0xFFL);
    }
    
    public static int e(final long n) {
        return (int)(n ^ n >>> 32);
    }
    
    public static int f(final long[] array, final long n, int i, final int n2) {
        while (i < n2) {
            if (array[i] == n) {
                return i;
            }
            ++i;
        }
        return -1;
    }
    
    public static int g(final long[] array, final long n, final int n2, int i) {
        --i;
        while (i >= n2) {
            if (array[i] == n) {
                return i;
            }
            --i;
        }
        return -1;
    }
    
    public static long[] h(final Collection collection) {
        if (collection instanceof LongArrayAsList) {
            return ((LongArrayAsList)collection).toLongArray();
        }
        final Object[] array = collection.toArray();
        final int length = array.length;
        final long[] array2 = new long[length];
        for (int i = 0; i < length; ++i) {
            array2[i] = ((Number)i71.r(array[i])).longValue();
        }
        return array2;
    }
    
    public enum LexicographicalComparator implements Comparator<long[]>
    {
        private static final LexicographicalComparator[] $VALUES;
        
        INSTANCE;
        
        private static /* synthetic */ LexicographicalComparator[] $values() {
            return new LexicographicalComparator[] { LexicographicalComparator.INSTANCE };
        }
        
        static {
            $VALUES = $values();
        }
        
        @Override
        public int compare(final long[] array, final long[] array2) {
            for (int min = Math.min(array.length, array2.length), i = 0; i < min; ++i) {
                final int c = Longs.c(array[i], array2[i]);
                if (c != 0) {
                    return c;
                }
            }
            return array.length - array2.length;
        }
        
        @Override
        public String toString() {
            return "Longs.lexicographicalComparator()";
        }
    }
    
    public static class LongArrayAsList extends AbstractList<Long> implements RandomAccess, Serializable
    {
        private static final long serialVersionUID = 0L;
        final long[] array;
        final int end;
        final int start;
        
        public LongArrayAsList(final long[] array) {
            this(array, 0, array.length);
        }
        
        public LongArrayAsList(final long[] array, final int start, final int end) {
            this.array = array;
            this.start = start;
            this.end = end;
        }
        
        @Override
        public boolean contains(final Object o) {
            return o instanceof Long && f(this.array, (long)o, this.start, this.end) != -1;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof LongArrayAsList)) {
                return super.equals(o);
            }
            final LongArrayAsList list = (LongArrayAsList)o;
            final int size = this.size();
            if (list.size() != size) {
                return false;
            }
            for (int i = 0; i < size; ++i) {
                if (this.array[this.start + i] != list.array[list.start + i]) {
                    return false;
                }
            }
            return true;
        }
        
        @Override
        public Long get(final int n) {
            i71.p(n, this.size());
            return this.array[this.start + n];
        }
        
        @Override
        public int hashCode() {
            int i = this.start;
            int n = 1;
            while (i < this.end) {
                n = n * 31 + Longs.e(this.array[i]);
                ++i;
            }
            return n;
        }
        
        @Override
        public int indexOf(final Object o) {
            if (o instanceof Long) {
                final int a = f(this.array, (long)o, this.start, this.end);
                if (a >= 0) {
                    return a - this.start;
                }
            }
            return -1;
        }
        
        @Override
        public boolean isEmpty() {
            return false;
        }
        
        @Override
        public int lastIndexOf(final Object o) {
            if (o instanceof Long) {
                final int b = g(this.array, (long)o, this.start, this.end);
                if (b >= 0) {
                    return b - this.start;
                }
            }
            return -1;
        }
        
        @Override
        public Long set(final int n, final Long n2) {
            i71.p(n, this.size());
            final long[] array = this.array;
            final int start = this.start;
            final long l = array[start + n];
            array[start + n] = (long)i71.r(n2);
            return l;
        }
        
        @Override
        public int size() {
            return this.end - this.start;
        }
        
        @Override
        public List<Long> subList(final int n, final int n2) {
            i71.w(n, n2, this.size());
            if (n == n2) {
                return Collections.emptyList();
            }
            final long[] array = this.array;
            final int start = this.start;
            return new LongArrayAsList(array, n + start, start + n2);
        }
        
        public long[] toLongArray() {
            return Arrays.copyOfRange(this.array, this.start, this.end);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder(this.size() * 10);
            sb.append('[');
            sb.append(this.array[this.start]);
            int start = this.start;
            while (++start < this.end) {
                sb.append(", ");
                sb.append(this.array[start]);
            }
            sb.append(']');
            return sb.toString();
        }
    }
    
    public static final class LongConverter extends Converter implements Serializable
    {
        static final LongConverter INSTANCE;
        private static final long serialVersionUID = 1L;
        
        static {
            INSTANCE = new LongConverter();
        }
        
        private LongConverter() {
        }
        
        private Object readResolve() {
            return LongConverter.INSTANCE;
        }
        
        public String doBackward(final Long n) {
            return n.toString();
        }
        
        public Long doForward(final String nm) {
            return Long.decode(nm);
        }
        
        @Override
        public String toString() {
            return "Longs.stringConverter()";
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.primitives;

import java.util.Iterator;
import java.util.RandomAccess;
import java.util.AbstractList;
import java.util.List;
import java.util.Arrays;
import java.util.Collection;
import java.io.Serializable;

public final class ImmutableIntArray implements Serializable
{
    private static final ImmutableIntArray EMPTY;
    private final int[] array;
    private final int end;
    private final transient int start;
    
    static {
        EMPTY = new ImmutableIntArray(new int[0]);
    }
    
    private ImmutableIntArray(final int[] array) {
        this(array, 0, array.length);
    }
    
    private ImmutableIntArray(final int[] array, final int start, final int end) {
        this.array = array;
        this.start = start;
        this.end = end;
    }
    
    public static /* synthetic */ int[] access$000(final ImmutableIntArray immutableIntArray) {
        return immutableIntArray.array;
    }
    
    public static /* synthetic */ int access$100(final ImmutableIntArray immutableIntArray) {
        return immutableIntArray.start;
    }
    
    public static /* synthetic */ ImmutableIntArray access$200() {
        return ImmutableIntArray.EMPTY;
    }
    
    public static b builder() {
        return new b(10);
    }
    
    public static b builder(final int n) {
        i71.h(n >= 0, "Invalid initialCapacity: %s", n);
        return new b(n);
    }
    
    public static ImmutableIntArray copyOf(final Iterable<Integer> iterable) {
        if (iterable instanceof Collection) {
            return copyOf((Collection<Integer>)iterable);
        }
        return builder().b(iterable).d();
    }
    
    public static ImmutableIntArray copyOf(final Collection<Integer> collection) {
        ImmutableIntArray empty;
        if (collection.isEmpty()) {
            empty = ImmutableIntArray.EMPTY;
        }
        else {
            empty = new ImmutableIntArray(Ints.l(collection));
        }
        return empty;
    }
    
    public static ImmutableIntArray copyOf(final int[] original) {
        ImmutableIntArray empty;
        if (original.length == 0) {
            empty = ImmutableIntArray.EMPTY;
        }
        else {
            empty = new ImmutableIntArray(Arrays.copyOf(original, original.length));
        }
        return empty;
    }
    
    private boolean isPartialView() {
        return this.start > 0 || this.end < this.array.length;
    }
    
    public static ImmutableIntArray of() {
        return ImmutableIntArray.EMPTY;
    }
    
    public static ImmutableIntArray of(final int n) {
        return new ImmutableIntArray(new int[] { n });
    }
    
    public static ImmutableIntArray of(final int n, final int n2) {
        return new ImmutableIntArray(new int[] { n, n2 });
    }
    
    public static ImmutableIntArray of(final int n, final int n2, final int n3) {
        return new ImmutableIntArray(new int[] { n, n2, n3 });
    }
    
    public static ImmutableIntArray of(final int n, final int n2, final int n3, final int n4) {
        return new ImmutableIntArray(new int[] { n, n2, n3, n4 });
    }
    
    public static ImmutableIntArray of(final int n, final int n2, final int n3, final int n4, final int n5) {
        return new ImmutableIntArray(new int[] { n, n2, n3, n4, n5 });
    }
    
    public static ImmutableIntArray of(final int n, final int n2, final int n3, final int n4, final int n5, final int n6) {
        return new ImmutableIntArray(new int[] { n, n2, n3, n4, n5, n6 });
    }
    
    public static ImmutableIntArray of(final int n, final int... array) {
        i71.e(array.length <= 2147483646, "the total number of elements must fit in an int");
        final int[] array2 = new int[array.length + 1];
        array2[0] = n;
        System.arraycopy(array, 0, array2, 1, array.length);
        return new ImmutableIntArray(array2);
    }
    
    public List<Integer> asList() {
        return new AsList(this, null);
    }
    
    public boolean contains(final int n) {
        return this.indexOf(n) >= 0;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof ImmutableIntArray)) {
            return false;
        }
        final ImmutableIntArray immutableIntArray = (ImmutableIntArray)o;
        if (this.length() != immutableIntArray.length()) {
            return false;
        }
        for (int i = 0; i < this.length(); ++i) {
            if (this.get(i) != immutableIntArray.get(i)) {
                return false;
            }
        }
        return true;
    }
    
    public int get(final int n) {
        i71.p(n, this.length());
        return this.array[this.start + n];
    }
    
    @Override
    public int hashCode() {
        int i = this.start;
        int n = 1;
        while (i < this.end) {
            n = n * 31 + Ints.g(this.array[i]);
            ++i;
        }
        return n;
    }
    
    public int indexOf(final int n) {
        for (int i = this.start; i < this.end; ++i) {
            if (this.array[i] == n) {
                return i - this.start;
            }
        }
        return -1;
    }
    
    public boolean isEmpty() {
        return this.end == this.start;
    }
    
    public int lastIndexOf(final int n) {
        int n2 = this.end - 1;
        while (true) {
            final int start = this.start;
            if (n2 < start) {
                return -1;
            }
            if (this.array[n2] == n) {
                return n2 - start;
            }
            --n2;
        }
    }
    
    public int length() {
        return this.end - this.start;
    }
    
    public Object readResolve() {
        ImmutableIntArray empty;
        if (this.isEmpty()) {
            empty = ImmutableIntArray.EMPTY;
        }
        else {
            empty = this;
        }
        return empty;
    }
    
    public ImmutableIntArray subArray(final int n, final int n2) {
        i71.w(n, n2, this.length());
        ImmutableIntArray empty;
        if (n == n2) {
            empty = ImmutableIntArray.EMPTY;
        }
        else {
            final int[] array = this.array;
            final int start = this.start;
            empty = new ImmutableIntArray(array, n + start, start + n2);
        }
        return empty;
    }
    
    public int[] toArray() {
        return Arrays.copyOfRange(this.array, this.start, this.end);
    }
    
    @Override
    public String toString() {
        if (this.isEmpty()) {
            return "[]";
        }
        final StringBuilder sb = new StringBuilder(this.length() * 5);
        sb.append('[');
        sb.append(this.array[this.start]);
        int start = this.start;
        while (++start < this.end) {
            sb.append(", ");
            sb.append(this.array[start]);
        }
        sb.append(']');
        return sb.toString();
    }
    
    public ImmutableIntArray trimmed() {
        ImmutableIntArray immutableIntArray;
        if (this.isPartialView()) {
            immutableIntArray = new ImmutableIntArray(this.toArray());
        }
        else {
            immutableIntArray = this;
        }
        return immutableIntArray;
    }
    
    public Object writeReplace() {
        return this.trimmed();
    }
    
    public static class AsList extends AbstractList<Integer> implements RandomAccess, Serializable
    {
        private final ImmutableIntArray parent;
        
        private AsList(final ImmutableIntArray parent) {
            this.parent = parent;
        }
        
        @Override
        public boolean contains(final Object o) {
            return this.indexOf(o) >= 0;
        }
        
        @Override
        public boolean equals(Object next) {
            if (next instanceof AsList) {
                return this.parent.equals(((AsList)next).parent);
            }
            if (!(next instanceof List)) {
                return false;
            }
            final List list = (List)next;
            if (this.size() != list.size()) {
                return false;
            }
            int access$100 = ImmutableIntArray.access$100(this.parent);
            final Iterator iterator = list.iterator();
            while (iterator.hasNext()) {
                next = iterator.next();
                if (!(next instanceof Integer) || ImmutableIntArray.access$000(this.parent)[access$100] != (int)next) {
                    return false;
                }
                ++access$100;
            }
            return true;
        }
        
        @Override
        public Integer get(final int n) {
            return this.parent.get(n);
        }
        
        @Override
        public int hashCode() {
            return this.parent.hashCode();
        }
        
        @Override
        public int indexOf(final Object o) {
            int index;
            if (o instanceof Integer) {
                index = this.parent.indexOf((int)o);
            }
            else {
                index = -1;
            }
            return index;
        }
        
        @Override
        public int lastIndexOf(final Object o) {
            int lastIndex;
            if (o instanceof Integer) {
                lastIndex = this.parent.lastIndexOf((int)o);
            }
            else {
                lastIndex = -1;
            }
            return lastIndex;
        }
        
        @Override
        public int size() {
            return this.parent.length();
        }
        
        @Override
        public List<Integer> subList(final int n, final int n2) {
            return this.parent.subArray(n, n2).asList();
        }
        
        @Override
        public String toString() {
            return this.parent.toString();
        }
    }
    
    public static final class b
    {
        public int[] a;
        public int b;
        
        public b(final int n) {
            this.b = 0;
            this.a = new int[n];
        }
        
        public static int f(int n, int n2) {
            if (n2 >= 0) {
                if ((n = n + (n >> 1) + 1) < n2) {
                    n = Integer.highestOneBit(n2 - 1) << 1;
                }
                if ((n2 = n) < 0) {
                    n2 = Integer.MAX_VALUE;
                }
                return n2;
            }
            throw new AssertionError((Object)"cannot store more than MAX_VALUE elements");
        }
        
        public b a(final int n) {
            this.e(1);
            final int[] a = this.a;
            final int b = this.b;
            a[b] = n;
            this.b = b + 1;
            return this;
        }
        
        public b b(final Iterable iterable) {
            if (iterable instanceof Collection) {
                return this.c((Collection)iterable);
            }
            final Iterator iterator = iterable.iterator();
            while (iterator.hasNext()) {
                this.a((int)iterator.next());
            }
            return this;
        }
        
        public b c(final Collection collection) {
            this.e(collection.size());
            final Iterator iterator = collection.iterator();
            while (iterator.hasNext()) {
                this.a[this.b++] = (int)iterator.next();
            }
            return this;
        }
        
        public ImmutableIntArray d() {
            ImmutableIntArray access$200;
            if (this.b == 0) {
                access$200 = ImmutableIntArray.access$200();
            }
            else {
                access$200 = new ImmutableIntArray(this.a, 0, this.b, null);
            }
            return access$200;
        }
        
        public final void e(int n) {
            n += this.b;
            final int[] a = this.a;
            if (n > a.length) {
                this.a = Arrays.copyOf(a, f(a.length, n));
            }
        }
    }
}

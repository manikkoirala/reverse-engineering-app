// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.primitives;

import com.google.common.base.Converter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.io.Serializable;
import java.util.RandomAccess;
import java.util.AbstractList;
import java.util.Comparator;

public abstract class Shorts extends vn1
{
    public static int c(final short n, final short n2) {
        return n - n2;
    }
    
    public static int d(final short n) {
        return n;
    }
    
    public static int e(final short[] array, final short n, int i, final int n2) {
        while (i < n2) {
            if (array[i] == n) {
                return i;
            }
            ++i;
        }
        return -1;
    }
    
    public static int f(final short[] array, final short n, final int n2, int i) {
        --i;
        while (i >= n2) {
            if (array[i] == n) {
                return i;
            }
            --i;
        }
        return -1;
    }
    
    public enum LexicographicalComparator implements Comparator<short[]>
    {
        private static final LexicographicalComparator[] $VALUES;
        
        INSTANCE;
        
        private static /* synthetic */ LexicographicalComparator[] $values() {
            return new LexicographicalComparator[] { LexicographicalComparator.INSTANCE };
        }
        
        static {
            $VALUES = $values();
        }
        
        @Override
        public int compare(final short[] array, final short[] array2) {
            for (int min = Math.min(array.length, array2.length), i = 0; i < min; ++i) {
                final int c = Shorts.c(array[i], array2[i]);
                if (c != 0) {
                    return c;
                }
            }
            return array.length - array2.length;
        }
        
        @Override
        public String toString() {
            return "Shorts.lexicographicalComparator()";
        }
    }
    
    public static class ShortArrayAsList extends AbstractList<Short> implements RandomAccess, Serializable
    {
        private static final long serialVersionUID = 0L;
        final short[] array;
        final int end;
        final int start;
        
        public ShortArrayAsList(final short[] array) {
            this(array, 0, array.length);
        }
        
        public ShortArrayAsList(final short[] array, final int start, final int end) {
            this.array = array;
            this.start = start;
            this.end = end;
        }
        
        @Override
        public boolean contains(final Object o) {
            return o instanceof Short && e(this.array, (short)o, this.start, this.end) != -1;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof ShortArrayAsList)) {
                return super.equals(o);
            }
            final ShortArrayAsList list = (ShortArrayAsList)o;
            final int size = this.size();
            if (list.size() != size) {
                return false;
            }
            for (int i = 0; i < size; ++i) {
                if (this.array[this.start + i] != list.array[list.start + i]) {
                    return false;
                }
            }
            return true;
        }
        
        @Override
        public Short get(final int n) {
            i71.p(n, this.size());
            return this.array[this.start + n];
        }
        
        @Override
        public int hashCode() {
            int i = this.start;
            int n = 1;
            while (i < this.end) {
                n = n * 31 + Shorts.d(this.array[i]);
                ++i;
            }
            return n;
        }
        
        @Override
        public int indexOf(final Object o) {
            if (o instanceof Short) {
                final int a = e(this.array, (short)o, this.start, this.end);
                if (a >= 0) {
                    return a - this.start;
                }
            }
            return -1;
        }
        
        @Override
        public boolean isEmpty() {
            return false;
        }
        
        @Override
        public int lastIndexOf(final Object o) {
            if (o instanceof Short) {
                final int b = f(this.array, (short)o, this.start, this.end);
                if (b >= 0) {
                    return b - this.start;
                }
            }
            return -1;
        }
        
        @Override
        public Short set(final int n, final Short n2) {
            i71.p(n, this.size());
            final short[] array = this.array;
            final int start = this.start;
            final short s = array[start + n];
            array[start + n] = (short)i71.r(n2);
            return s;
        }
        
        @Override
        public int size() {
            return this.end - this.start;
        }
        
        @Override
        public List<Short> subList(final int n, final int n2) {
            i71.w(n, n2, this.size());
            if (n == n2) {
                return Collections.emptyList();
            }
            final short[] array = this.array;
            final int start = this.start;
            return new ShortArrayAsList(array, n + start, start + n2);
        }
        
        public short[] toShortArray() {
            return Arrays.copyOfRange(this.array, this.start, this.end);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder(this.size() * 6);
            sb.append('[');
            sb.append(this.array[this.start]);
            int start = this.start;
            while (++start < this.end) {
                sb.append(", ");
                sb.append(this.array[start]);
            }
            sb.append(']');
            return sb.toString();
        }
    }
    
    public static final class ShortConverter extends Converter implements Serializable
    {
        static final ShortConverter INSTANCE;
        private static final long serialVersionUID = 1L;
        
        static {
            INSTANCE = new ShortConverter();
        }
        
        private ShortConverter() {
        }
        
        private Object readResolve() {
            return ShortConverter.INSTANCE;
        }
        
        public String doBackward(final Short n) {
            return n.toString();
        }
        
        public Short doForward(final String nm) {
            return Short.decode(nm);
        }
        
        @Override
        public String toString() {
            return "Shorts.stringConverter()";
        }
    }
}

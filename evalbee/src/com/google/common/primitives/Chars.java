// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.primitives;

import java.util.Comparator;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.io.Serializable;
import java.util.RandomAccess;
import java.util.AbstractList;

public abstract class Chars
{
    public static int c(final char c, final char c2) {
        return c - c2;
    }
    
    public static int d(final char c) {
        return c;
    }
    
    public static int e(final char[] array, final char c, int i, final int n) {
        while (i < n) {
            if (array[i] == c) {
                return i;
            }
            ++i;
        }
        return -1;
    }
    
    public static int f(final char[] array, final char c, final int n, int i) {
        --i;
        while (i >= n) {
            if (array[i] == c) {
                return i;
            }
            --i;
        }
        return -1;
    }
    
    public static class CharArrayAsList extends AbstractList<Character> implements RandomAccess, Serializable
    {
        private static final long serialVersionUID = 0L;
        final char[] array;
        final int end;
        final int start;
        
        public CharArrayAsList(final char[] array) {
            this(array, 0, array.length);
        }
        
        public CharArrayAsList(final char[] array, final int start, final int end) {
            this.array = array;
            this.start = start;
            this.end = end;
        }
        
        @Override
        public boolean contains(final Object o) {
            return o instanceof Character && e(this.array, (char)o, this.start, this.end) != -1;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof CharArrayAsList)) {
                return super.equals(o);
            }
            final CharArrayAsList list = (CharArrayAsList)o;
            final int size = this.size();
            if (list.size() != size) {
                return false;
            }
            for (int i = 0; i < size; ++i) {
                if (this.array[this.start + i] != list.array[list.start + i]) {
                    return false;
                }
            }
            return true;
        }
        
        @Override
        public Character get(final int n) {
            i71.p(n, this.size());
            return this.array[this.start + n];
        }
        
        @Override
        public int hashCode() {
            int i = this.start;
            int n = 1;
            while (i < this.end) {
                n = n * 31 + Chars.d(this.array[i]);
                ++i;
            }
            return n;
        }
        
        @Override
        public int indexOf(final Object o) {
            if (o instanceof Character) {
                final int a = e(this.array, (char)o, this.start, this.end);
                if (a >= 0) {
                    return a - this.start;
                }
            }
            return -1;
        }
        
        @Override
        public boolean isEmpty() {
            return false;
        }
        
        @Override
        public int lastIndexOf(final Object o) {
            if (o instanceof Character) {
                final int b = f(this.array, (char)o, this.start, this.end);
                if (b >= 0) {
                    return b - this.start;
                }
            }
            return -1;
        }
        
        @Override
        public Character set(final int n, final Character c) {
            i71.p(n, this.size());
            final char[] array = this.array;
            final int start = this.start;
            final char c2 = array[start + n];
            array[start + n] = (char)i71.r(c);
            return c2;
        }
        
        @Override
        public int size() {
            return this.end - this.start;
        }
        
        @Override
        public List<Character> subList(final int n, final int n2) {
            i71.w(n, n2, this.size());
            if (n == n2) {
                return Collections.emptyList();
            }
            final char[] array = this.array;
            final int start = this.start;
            return new CharArrayAsList(array, n + start, start + n2);
        }
        
        public char[] toCharArray() {
            return Arrays.copyOfRange(this.array, this.start, this.end);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder(this.size() * 3);
            sb.append('[');
            sb.append(this.array[this.start]);
            int start = this.start;
            while (++start < this.end) {
                sb.append(", ");
                sb.append(this.array[start]);
            }
            sb.append(']');
            return sb.toString();
        }
    }
    
    public enum LexicographicalComparator implements Comparator<char[]>
    {
        private static final LexicographicalComparator[] $VALUES;
        
        INSTANCE;
        
        private static /* synthetic */ LexicographicalComparator[] $values() {
            return new LexicographicalComparator[] { LexicographicalComparator.INSTANCE };
        }
        
        static {
            $VALUES = $values();
        }
        
        @Override
        public int compare(final char[] array, final char[] array2) {
            for (int min = Math.min(array.length, array2.length), i = 0; i < min; ++i) {
                final int c = Chars.c(array[i], array2[i]);
                if (c != 0) {
                    return c;
                }
            }
            return array.length - array2.length;
        }
        
        @Override
        public String toString() {
            return "Chars.lexicographicalComparator()";
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.primitives;

import java.util.Comparator;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.io.Serializable;
import java.util.RandomAccess;
import java.util.AbstractList;

public abstract class Booleans
{
    public static int c(final boolean b, final boolean b2) {
        int n;
        if (b == b2) {
            n = 0;
        }
        else if (b) {
            n = 1;
        }
        else {
            n = -1;
        }
        return n;
    }
    
    public static int d(final boolean b) {
        int n;
        if (b) {
            n = 1231;
        }
        else {
            n = 1237;
        }
        return n;
    }
    
    public static int e(final boolean[] array, final boolean b, int i, final int n) {
        while (i < n) {
            if (array[i] == b) {
                return i;
            }
            ++i;
        }
        return -1;
    }
    
    public static int f(final boolean[] array, final boolean b, final int n, int i) {
        --i;
        while (i >= n) {
            if (array[i] == b) {
                return i;
            }
            --i;
        }
        return -1;
    }
    
    public static class BooleanArrayAsList extends AbstractList<Boolean> implements RandomAccess, Serializable
    {
        private static final long serialVersionUID = 0L;
        final boolean[] array;
        final int end;
        final int start;
        
        public BooleanArrayAsList(final boolean[] array) {
            this(array, 0, array.length);
        }
        
        public BooleanArrayAsList(final boolean[] array, final int start, final int end) {
            this.array = array;
            this.start = start;
            this.end = end;
        }
        
        @Override
        public boolean contains(final Object o) {
            return o instanceof Boolean && e(this.array, (boolean)o, this.start, this.end) != -1;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof BooleanArrayAsList)) {
                return super.equals(o);
            }
            final BooleanArrayAsList list = (BooleanArrayAsList)o;
            final int size = this.size();
            if (list.size() != size) {
                return false;
            }
            for (int i = 0; i < size; ++i) {
                if (this.array[this.start + i] != list.array[list.start + i]) {
                    return false;
                }
            }
            return true;
        }
        
        @Override
        public Boolean get(final int n) {
            i71.p(n, this.size());
            return this.array[this.start + n];
        }
        
        @Override
        public int hashCode() {
            int i = this.start;
            int n = 1;
            while (i < this.end) {
                n = n * 31 + Booleans.d(this.array[i]);
                ++i;
            }
            return n;
        }
        
        @Override
        public int indexOf(final Object o) {
            if (o instanceof Boolean) {
                final int a = e(this.array, (boolean)o, this.start, this.end);
                if (a >= 0) {
                    return a - this.start;
                }
            }
            return -1;
        }
        
        @Override
        public boolean isEmpty() {
            return false;
        }
        
        @Override
        public int lastIndexOf(final Object o) {
            if (o instanceof Boolean) {
                final int b = f(this.array, (boolean)o, this.start, this.end);
                if (b >= 0) {
                    return b - this.start;
                }
            }
            return -1;
        }
        
        @Override
        public Boolean set(final int n, final Boolean b) {
            i71.p(n, this.size());
            final boolean[] array = this.array;
            final int start = this.start;
            final boolean b2 = array[start + n];
            array[start + n] = (boolean)i71.r(b);
            return b2;
        }
        
        @Override
        public int size() {
            return this.end - this.start;
        }
        
        @Override
        public List<Boolean> subList(final int n, final int n2) {
            i71.w(n, n2, this.size());
            if (n == n2) {
                return Collections.emptyList();
            }
            final boolean[] array = this.array;
            final int start = this.start;
            return new BooleanArrayAsList(array, n + start, start + n2);
        }
        
        public boolean[] toBooleanArray() {
            return Arrays.copyOfRange(this.array, this.start, this.end);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder(this.size() * 7);
            String str;
            if (this.array[this.start]) {
                str = "[true";
            }
            else {
                str = "[false";
            }
            sb.append(str);
            int start = this.start;
            while (++start < this.end) {
                String str2;
                if (this.array[start]) {
                    str2 = ", true";
                }
                else {
                    str2 = ", false";
                }
                sb.append(str2);
            }
            sb.append(']');
            return sb.toString();
        }
    }
    
    public enum BooleanComparator implements Comparator<Boolean>
    {
        private static final BooleanComparator[] $VALUES;
        
        FALSE_FIRST(-1, "Booleans.falseFirst()"), 
        TRUE_FIRST(1, "Booleans.trueFirst()");
        
        private final String toString;
        private final int trueValue;
        
        private static /* synthetic */ BooleanComparator[] $values() {
            return new BooleanComparator[] { BooleanComparator.TRUE_FIRST, BooleanComparator.FALSE_FIRST };
        }
        
        static {
            $VALUES = $values();
        }
        
        private BooleanComparator(final int trueValue, final String toString) {
            this.trueValue = trueValue;
            this.toString = toString;
        }
        
        @Override
        public int compare(final Boolean b, final Boolean b2) {
            final boolean booleanValue = b;
            int trueValue = 0;
            int trueValue2;
            if (booleanValue) {
                trueValue2 = this.trueValue;
            }
            else {
                trueValue2 = 0;
            }
            if (b2) {
                trueValue = this.trueValue;
            }
            return trueValue - trueValue2;
        }
        
        @Override
        public String toString() {
            return this.toString;
        }
    }
    
    public enum LexicographicalComparator implements Comparator<boolean[]>
    {
        private static final LexicographicalComparator[] $VALUES;
        
        INSTANCE;
        
        private static /* synthetic */ LexicographicalComparator[] $values() {
            return new LexicographicalComparator[] { LexicographicalComparator.INSTANCE };
        }
        
        static {
            $VALUES = $values();
        }
        
        @Override
        public int compare(final boolean[] array, final boolean[] array2) {
            for (int min = Math.min(array.length, array2.length), i = 0; i < min; ++i) {
                final int c = Booleans.c(array[i], array2[i]);
                if (c != 0) {
                    return c;
                }
            }
            return array.length - array2.length;
        }
        
        @Override
        public String toString() {
            return "Booleans.lexicographicalComparator()";
        }
    }
}

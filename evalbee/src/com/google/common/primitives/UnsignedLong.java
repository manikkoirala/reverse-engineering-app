// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.primitives;

import java.math.BigInteger;
import java.io.Serializable;

public final class UnsignedLong extends Number implements Comparable<UnsignedLong>, Serializable
{
    public static final UnsignedLong MAX_VALUE;
    public static final UnsignedLong ONE;
    private static final long UNSIGNED_MASK = Long.MAX_VALUE;
    public static final UnsignedLong ZERO;
    private final long value;
    
    static {
        ZERO = new UnsignedLong(0L);
        ONE = new UnsignedLong(1L);
        MAX_VALUE = new UnsignedLong(-1L);
    }
    
    private UnsignedLong(final long value) {
        this.value = value;
    }
    
    public static UnsignedLong fromLongBits(final long n) {
        return new UnsignedLong(n);
    }
    
    public static UnsignedLong valueOf(final long n) {
        i71.j(n >= 0L, "value (%s) is outside the range for an unsigned long value", n);
        return fromLongBits(n);
    }
    
    public static UnsignedLong valueOf(final String s) {
        return valueOf(s, 10);
    }
    
    public static UnsignedLong valueOf(final String s, final int n) {
        return fromLongBits(UnsignedLongs.d(s, n));
    }
    
    public static UnsignedLong valueOf(final BigInteger bigInteger) {
        i71.r(bigInteger);
        i71.m(bigInteger.signum() >= 0 && bigInteger.bitLength() <= 64, "value (%s) is outside the range for an unsigned long value", bigInteger);
        return fromLongBits(bigInteger.longValue());
    }
    
    public BigInteger bigIntegerValue() {
        BigInteger bigInteger = BigInteger.valueOf(this.value & Long.MAX_VALUE);
        if (this.value < 0L) {
            bigInteger = bigInteger.setBit(63);
        }
        return bigInteger;
    }
    
    @Override
    public int compareTo(final UnsignedLong unsignedLong) {
        i71.r(unsignedLong);
        return UnsignedLongs.a(this.value, unsignedLong.value);
    }
    
    public UnsignedLong dividedBy(final UnsignedLong unsignedLong) {
        return fromLongBits(UnsignedLongs.b(this.value, ((UnsignedLong)i71.r(unsignedLong)).value));
    }
    
    @Override
    public double doubleValue() {
        final long value = this.value;
        if (value >= 0L) {
            return (double)value;
        }
        return ((value & 0x1L) | value >>> 1) * 2.0;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof UnsignedLong;
        boolean b2 = false;
        if (b) {
            final UnsignedLong unsignedLong = (UnsignedLong)o;
            b2 = b2;
            if (this.value == unsignedLong.value) {
                b2 = true;
            }
        }
        return b2;
    }
    
    @Override
    public float floatValue() {
        final long value = this.value;
        if (value >= 0L) {
            return (float)value;
        }
        return ((value & 0x1L) | value >>> 1) * 2.0f;
    }
    
    @Override
    public int hashCode() {
        return Longs.e(this.value);
    }
    
    @Override
    public int intValue() {
        return (int)this.value;
    }
    
    @Override
    public long longValue() {
        return this.value;
    }
    
    public UnsignedLong minus(final UnsignedLong unsignedLong) {
        return fromLongBits(this.value - ((UnsignedLong)i71.r(unsignedLong)).value);
    }
    
    public UnsignedLong mod(final UnsignedLong unsignedLong) {
        return fromLongBits(UnsignedLongs.e(this.value, ((UnsignedLong)i71.r(unsignedLong)).value));
    }
    
    public UnsignedLong plus(final UnsignedLong unsignedLong) {
        return fromLongBits(this.value + ((UnsignedLong)i71.r(unsignedLong)).value);
    }
    
    public UnsignedLong times(final UnsignedLong unsignedLong) {
        return fromLongBits(this.value * ((UnsignedLong)i71.r(unsignedLong)).value);
    }
    
    @Override
    public String toString() {
        return UnsignedLongs.f(this.value);
    }
    
    public String toString(final int n) {
        return UnsignedLongs.g(this.value, n);
    }
}

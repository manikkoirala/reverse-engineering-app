// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.io;

import java.util.Arrays;
import java.math.RoundingMode;
import java.io.IOException;

public abstract class BaseEncoding
{
    public static final BaseEncoding a;
    public static final BaseEncoding b;
    public static final BaseEncoding c;
    public static final BaseEncoding d;
    public static final BaseEncoding e;
    
    static {
        final Character value = '=';
        a = new c("base64()", "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", value);
        b = new c("base64Url()", "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_", value);
        c = new d("base32()", "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567", value);
        d = new d("base32Hex()", "0123456789ABCDEFGHIJKLMNOPQRSTUV", value);
        e = new b("base16()", "0123456789ABCDEF");
    }
    
    public static BaseEncoding a() {
        return BaseEncoding.a;
    }
    
    public static byte[] h(final byte[] array, final int n) {
        if (n == array.length) {
            return array;
        }
        final byte[] array2 = new byte[n];
        System.arraycopy(array, 0, array2, 0, n);
        return array2;
    }
    
    public final byte[] b(final CharSequence charSequence) {
        try {
            return this.c(charSequence);
        }
        catch (final DecodingException cause) {
            throw new IllegalArgumentException(cause);
        }
    }
    
    public final byte[] c(final CharSequence charSequence) {
        final CharSequence l = this.l(charSequence);
        final byte[] array = new byte[this.i(l.length())];
        return h(array, this.d(array, l));
    }
    
    public abstract int d(final byte[] p0, final CharSequence p1);
    
    public String e(final byte[] array) {
        return this.f(array, 0, array.length);
    }
    
    public final String f(final byte[] array, final int n, final int n2) {
        i71.w(n, n + n2, array.length);
        final StringBuilder sb = new StringBuilder(this.j(n2));
        try {
            this.g(sb, array, n, n2);
            return sb.toString();
        }
        catch (final IOException detailMessage) {
            throw new AssertionError((Object)detailMessage);
        }
    }
    
    public abstract void g(final Appendable p0, final byte[] p1, final int p2, final int p3);
    
    public abstract int i(final int p0);
    
    public abstract int j(final int p0);
    
    public abstract BaseEncoding k();
    
    public abstract CharSequence l(final CharSequence p0);
    
    public static final class DecodingException extends IOException
    {
        public DecodingException(final String message) {
            super(message);
        }
        
        public DecodingException(final Throwable cause) {
            super(cause);
        }
    }
    
    public static final class a
    {
        public final String a;
        public final char[] b;
        public final int c;
        public final int d;
        public final int e;
        public final int f;
        public final byte[] g;
        public final boolean[] h;
        
        public a(String concat, final char[] value) {
            this.a = (String)i71.r(concat);
            this.b = (char[])i71.r(value);
            try {
                final int f = tf0.f(value.length, RoundingMode.UNNECESSARY);
                this.d = f;
                final int min = Math.min(8, Integer.lowestOneBit(f));
                try {
                    this.e = 8 / min;
                    this.f = f / min;
                    this.c = value.length - 1;
                    final byte[] array = new byte[128];
                    Arrays.fill(array, (byte)(-1));
                    final int n = 0;
                    for (int i = 0; i < value.length; ++i) {
                        final char c = value[i];
                        i71.f(c < '\u0080', "Non-ASCII character: %s", c);
                        i71.f(array[c] == -1, "Duplicate character: %s", c);
                        array[c] = (byte)i;
                    }
                    this.g = array;
                    final boolean[] h = new boolean[this.e];
                    for (int j = n; j < this.f; ++j) {
                        h[tf0.c(j * 8, this.d, RoundingMode.CEILING)] = true;
                    }
                    this.h = h;
                }
                catch (final ArithmeticException cause) {
                    concat = new String(value);
                    if (concat.length() != 0) {
                        concat = "Illegal alphabet ".concat(concat);
                    }
                    else {
                        concat = new String("Illegal alphabet ");
                    }
                    throw new IllegalArgumentException(concat, cause);
                }
            }
            catch (final ArithmeticException cause2) {
                final int length = value.length;
                final StringBuilder sb = new StringBuilder(35);
                sb.append("Illegal alphabet length ");
                sb.append(length);
                throw new IllegalArgumentException(sb.toString(), cause2);
            }
        }
        
        public static /* synthetic */ char[] a(final a a) {
            return a.b;
        }
        
        public int b(final char i) {
            if (i > '\u007f') {
                final String value = String.valueOf(Integer.toHexString(i));
                String concat;
                if (value.length() != 0) {
                    concat = "Unrecognized character: 0x".concat(value);
                }
                else {
                    concat = new String("Unrecognized character: 0x");
                }
                throw new DecodingException(concat);
            }
            final byte b = this.g[i];
            if (b != -1) {
                return b;
            }
            if (i > ' ' && i != '\u007f') {
                final StringBuilder sb = new StringBuilder(25);
                sb.append("Unrecognized character: ");
                sb.append(i);
                throw new DecodingException(sb.toString());
            }
            final String value2 = String.valueOf(Integer.toHexString(i));
            String concat2;
            if (value2.length() != 0) {
                concat2 = "Unrecognized character: 0x".concat(value2);
            }
            else {
                concat2 = new String("Unrecognized character: 0x");
            }
            throw new DecodingException(concat2);
        }
        
        public char c(final int n) {
            return this.b[n];
        }
        
        public boolean d(final int n) {
            return this.h[n % this.e];
        }
        
        public boolean e(final char c) {
            final byte[] g = this.g;
            return c < g.length && g[c] != -1;
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof a && Arrays.equals(this.b, ((a)o).b);
        }
        
        @Override
        public int hashCode() {
            return Arrays.hashCode(this.b);
        }
        
        @Override
        public String toString() {
            return this.a;
        }
    }
    
    public static final class b extends d
    {
        public final char[] h;
        
        public b(final a a) {
            super(a, null);
            this.h = new char[512];
            final int length = BaseEncoding.a.a(a).length;
            int i = 0;
            i71.d(length == 16);
            while (i < 256) {
                this.h[i] = a.c(i >>> 4);
                this.h[i | 0x100] = a.c(i & 0xF);
                ++i;
            }
        }
        
        public b(final String s, final String s2) {
            this(new a(s, s2.toCharArray()));
        }
        
        @Override
        public int d(final byte[] array, final CharSequence charSequence) {
            i71.r(array);
            if (charSequence.length() % 2 != 1) {
                int i;
                int n;
                for (i = 0, n = 0; i < charSequence.length(); i += 2, ++n) {
                    array[n] = (byte)(super.f.b(charSequence.charAt(i)) << 4 | super.f.b(charSequence.charAt(i + 1)));
                }
                return n;
            }
            final int length = charSequence.length();
            final StringBuilder sb = new StringBuilder(32);
            sb.append("Invalid input length ");
            sb.append(length);
            throw new DecodingException(sb.toString());
        }
        
        @Override
        public void g(final Appendable appendable, final byte[] array, final int n, final int n2) {
            i71.r(appendable);
            i71.w(n, n + n2, array.length);
            for (int i = 0; i < n2; ++i) {
                final int n3 = array[n + i] & 0xFF;
                appendable.append(this.h[n3]);
                appendable.append(this.h[n3 | 0x100]);
            }
        }
        
        @Override
        public BaseEncoding n(final a a, final Character c) {
            return new b(a);
        }
    }
    
    public static final class c extends d
    {
        public c(final a a, final Character c) {
            super(a, c);
            i71.d(BaseEncoding.a.a(a).length == 64);
        }
        
        public c(final String s, final String s2, final Character c) {
            this(new a(s, s2.toCharArray()), c);
        }
        
        @Override
        public int d(final byte[] array, CharSequence l) {
            i71.r(array);
            l = ((d)this).l(l);
            if (super.f.d(l.length())) {
                int i = 0;
                int n = 0;
                while (i < l.length()) {
                    final a f = super.f;
                    final int n2 = i + 1;
                    final int b = f.b(l.charAt(i));
                    final a f2 = super.f;
                    final int n3 = n2 + 1;
                    final int n4 = b << 18 | f2.b(l.charAt(n2)) << 12;
                    final int n5 = n + 1;
                    array[n] = (byte)(n4 >>> 16);
                    int n6 = n5;
                    int n7;
                    if ((n7 = n3) < l.length()) {
                        final a f3 = super.f;
                        i = n3 + 1;
                        final int n8 = n4 | f3.b(l.charAt(n3)) << 6;
                        n = n5 + 1;
                        array[n5] = (byte)(n8 >>> 8 & 0xFF);
                        if (i >= l.length()) {
                            continue;
                        }
                        final a f4 = super.f;
                        final int n9 = i + 1;
                        final int b2 = f4.b(l.charAt(i));
                        n6 = n + 1;
                        array[n] = (byte)((n8 | b2) & 0xFF);
                        n7 = n9;
                    }
                    final int n10 = n6;
                    i = n7;
                    n = n10;
                }
                return n;
            }
            final int length = l.length();
            final StringBuilder sb = new StringBuilder(32);
            sb.append("Invalid input length ");
            sb.append(length);
            throw new DecodingException(sb.toString());
        }
        
        @Override
        public void g(final Appendable appendable, final byte[] array, int n, int i) {
            i71.r(appendable);
            final int n2 = n + i;
            i71.w(n, n2, array.length);
            while (i >= 3) {
                final int n3 = n + 1;
                final byte b = array[n];
                n = n3 + 1;
                final int n4 = (b & 0xFF) << 16 | (array[n3] & 0xFF) << 8 | (array[n] & 0xFF);
                appendable.append(super.f.c(n4 >>> 18));
                appendable.append(super.f.c(n4 >>> 12 & 0x3F));
                appendable.append(super.f.c(n4 >>> 6 & 0x3F));
                appendable.append(super.f.c(n4 & 0x3F));
                i -= 3;
                ++n;
            }
            if (n < n2) {
                ((d)this).m(appendable, array, n, n2 - n);
            }
        }
        
        @Override
        public BaseEncoding n(final a a, final Character c) {
            return new c(a, c);
        }
    }
    
    public static class d extends BaseEncoding
    {
        public final a f;
        public final Character g;
        
        public d(final a a, final Character g) {
            this.f = (a)i71.r(a);
            i71.m(g == null || !a.e(g), "Padding character %s was already in alphabet", g);
            this.g = g;
        }
        
        public d(final String s, final String s2, final Character c) {
            this(new a(s, s2.toCharArray()), c);
        }
        
        @Override
        public int d(final byte[] array, final CharSequence charSequence) {
            i71.r(array);
            final CharSequence l = this.l(charSequence);
            if (this.f.d(l.length())) {
                int i = 0;
                int n = 0;
                while (i < l.length()) {
                    long n2 = 0L;
                    int n3 = 0;
                    int n4 = 0;
                    a f;
                    while (true) {
                        f = this.f;
                        if (n3 >= f.e) {
                            break;
                        }
                        final long n5 = n2 <<= f.d;
                        int n6 = n4;
                        if (i + n3 < l.length()) {
                            n2 = (n5 | (long)this.f.b(l.charAt(n4 + i)));
                            n6 = n4 + 1;
                        }
                        ++n3;
                        n4 = n6;
                    }
                    for (int f2 = f.f, d = f.d, j = (f2 - 1) * 8; j >= f2 * 8 - n4 * d; j -= 8, ++n) {
                        array[n] = (byte)(n2 >>> j & 0xFFL);
                    }
                    i += this.f.e;
                }
                return n;
            }
            final int length = l.length();
            final StringBuilder sb = new StringBuilder(32);
            sb.append("Invalid input length ");
            sb.append(length);
            throw new DecodingException(sb.toString());
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof d;
            boolean b3;
            final boolean b2 = b3 = false;
            if (b) {
                final d d = (d)o;
                b3 = b2;
                if (this.f.equals(d.f)) {
                    b3 = b2;
                    if (b11.a(this.g, d.g)) {
                        b3 = true;
                    }
                }
            }
            return b3;
        }
        
        @Override
        public void g(final Appendable appendable, final byte[] array, final int n, final int n2) {
            i71.r(appendable);
            i71.w(n, n + n2, array.length);
            for (int i = 0; i < n2; i += this.f.f) {
                this.m(appendable, array, n + i, Math.min(this.f.f, n2 - i));
            }
        }
        
        @Override
        public int hashCode() {
            return this.f.hashCode() ^ b11.b(this.g);
        }
        
        @Override
        public int i(final int n) {
            return (int)((this.f.d * (long)n + 7L) / 8L);
        }
        
        @Override
        public int j(final int n) {
            final a f = this.f;
            return f.e * tf0.c(n, f.f, RoundingMode.CEILING);
        }
        
        @Override
        public BaseEncoding k() {
            BaseEncoding n;
            if (this.g == null) {
                n = this;
            }
            else {
                n = this.n(this.f, null);
            }
            return n;
        }
        
        @Override
        public CharSequence l(final CharSequence charSequence) {
            i71.r(charSequence);
            final Character g = this.g;
            if (g == null) {
                return charSequence;
            }
            char charValue;
            int n;
            for (charValue = g, n = charSequence.length() - 1; n >= 0 && charSequence.charAt(n) == charValue; --n) {}
            return charSequence.subSequence(0, n + 1);
        }
        
        public void m(final Appendable appendable, final byte[] array, int i, final int n) {
            i71.r(appendable);
            i71.w(i, i + n, array.length);
            final int f = this.f.f;
            final int n2 = 0;
            i71.d(n <= f);
            long n3 = 0L;
            for (int j = 0; j < n; ++j) {
                n3 = (n3 | (long)(array[i + j] & 0xFF)) << 8;
            }
            final int d = this.f.d;
            int n4;
            a f2;
            for (i = n2; i < n * 8; i += this.f.d) {
                n4 = (int)(n3 >>> (n + 1) * 8 - d - i);
                f2 = this.f;
                appendable.append(f2.c(n4 & f2.c));
            }
            if (this.g != null) {
                while (i < this.f.f * 8) {
                    appendable.append(this.g);
                    i += this.f.d;
                }
            }
        }
        
        public BaseEncoding n(final a a, final Character c) {
            return new d(a, c);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("BaseEncoding.");
            sb.append(this.f.toString());
            if (8 % this.f.d != 0) {
                String str;
                if (this.g == null) {
                    str = ".omitPadding()";
                }
                else {
                    sb.append(".withPadChar('");
                    sb.append(this.g);
                    str = "')";
                }
                sb.append(str);
            }
            return sb.toString();
        }
    }
}

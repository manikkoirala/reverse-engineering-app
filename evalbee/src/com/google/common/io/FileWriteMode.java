// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.io;

public enum FileWriteMode
{
    private static final FileWriteMode[] $VALUES;
    
    APPEND;
    
    private static /* synthetic */ FileWriteMode[] $values() {
        return new FileWriteMode[] { FileWriteMode.APPEND };
    }
    
    static {
        $VALUES = $values();
    }
}

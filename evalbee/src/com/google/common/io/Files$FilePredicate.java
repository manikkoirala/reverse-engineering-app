// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.io;

import java.io.File;

enum Files$FilePredicate implements m71
{
    private static final Files$FilePredicate[] $VALUES;
    
    IS_DIRECTORY {
        public boolean apply(final File file) {
            return file.isDirectory();
        }
        
        @Override
        public String toString() {
            return "Files.isDirectory()";
        }
    }, 
    IS_FILE {
        public boolean apply(final File file) {
            return file.isFile();
        }
        
        @Override
        public String toString() {
            return "Files.isFile()";
        }
    };
    
    private static /* synthetic */ Files$FilePredicate[] $values() {
        return new Files$FilePredicate[] { Files$FilePredicate.IS_DIRECTORY, Files$FilePredicate.IS_FILE };
    }
    
    static {
        $VALUES = $values();
    }
}

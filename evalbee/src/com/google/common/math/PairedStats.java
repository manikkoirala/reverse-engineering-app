// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.math;

import java.nio.ByteOrder;
import java.nio.ByteBuffer;
import java.io.Serializable;

public final class PairedStats implements Serializable
{
    private static final int BYTES = 88;
    private static final long serialVersionUID = 0L;
    private final double sumOfProductsOfDeltas;
    private final Stats xStats;
    private final Stats yStats;
    
    public PairedStats(final Stats xStats, final Stats yStats, final double sumOfProductsOfDeltas) {
        this.xStats = xStats;
        this.yStats = yStats;
        this.sumOfProductsOfDeltas = sumOfProductsOfDeltas;
    }
    
    private static double ensureInUnitRange(final double n) {
        if (n >= 1.0) {
            return 1.0;
        }
        if (n <= -1.0) {
            return -1.0;
        }
        return n;
    }
    
    private static double ensurePositive(final double n) {
        if (n > 0.0) {
            return n;
        }
        return Double.MIN_VALUE;
    }
    
    public static PairedStats fromByteArray(final byte[] array) {
        i71.r(array);
        i71.i(array.length == 88, "Expected PairedStats.BYTES = %s, got %s", 88, array.length);
        final ByteBuffer order = ByteBuffer.wrap(array).order(ByteOrder.LITTLE_ENDIAN);
        return new PairedStats(Stats.readFrom(order), Stats.readFrom(order), order.getDouble());
    }
    
    public long count() {
        return this.xStats.count();
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = false;
        if (o == null) {
            return false;
        }
        if (PairedStats.class != o.getClass()) {
            return false;
        }
        final PairedStats pairedStats = (PairedStats)o;
        boolean b2 = b;
        if (this.xStats.equals(pairedStats.xStats)) {
            b2 = b;
            if (this.yStats.equals(pairedStats.yStats)) {
                b2 = b;
                if (Double.doubleToLongBits(this.sumOfProductsOfDeltas) == Double.doubleToLongBits(pairedStats.sumOfProductsOfDeltas)) {
                    b2 = true;
                }
            }
        }
        return b2;
    }
    
    @Override
    public int hashCode() {
        return b11.b(this.xStats, this.yStats, this.sumOfProductsOfDeltas);
    }
    
    public a leastSquaresFit() {
        final long count = this.count();
        final boolean b = true;
        i71.x(count > 1L);
        if (Double.isNaN(this.sumOfProductsOfDeltas)) {
            return a.a();
        }
        final double sumOfSquaresOfDeltas = this.xStats.sumOfSquaresOfDeltas();
        if (sumOfSquaresOfDeltas <= 0.0) {
            i71.x(this.yStats.sumOfSquaresOfDeltas() > 0.0 && b);
            return a.d(this.xStats.mean());
        }
        if (this.yStats.sumOfSquaresOfDeltas() > 0.0) {
            return a.c(this.xStats.mean(), this.yStats.mean()).a(this.sumOfProductsOfDeltas / sumOfSquaresOfDeltas);
        }
        return a.b(this.yStats.mean());
    }
    
    public double pearsonsCorrelationCoefficient() {
        final long count = this.count();
        final boolean b = true;
        i71.x(count > 1L);
        if (Double.isNaN(this.sumOfProductsOfDeltas)) {
            return Double.NaN;
        }
        final double sumOfSquaresOfDeltas = this.xStats().sumOfSquaresOfDeltas();
        final double sumOfSquaresOfDeltas2 = this.yStats().sumOfSquaresOfDeltas();
        i71.x(sumOfSquaresOfDeltas > 0.0);
        i71.x(sumOfSquaresOfDeltas2 > 0.0 && b);
        return ensureInUnitRange(this.sumOfProductsOfDeltas / Math.sqrt(ensurePositive(sumOfSquaresOfDeltas * sumOfSquaresOfDeltas2)));
    }
    
    public double populationCovariance() {
        i71.x(this.count() != 0L);
        return this.sumOfProductsOfDeltas / this.count();
    }
    
    public double sampleCovariance() {
        i71.x(this.count() > 1L);
        return this.sumOfProductsOfDeltas / (this.count() - 1L);
    }
    
    public double sumOfProductsOfDeltas() {
        return this.sumOfProductsOfDeltas;
    }
    
    public byte[] toByteArray() {
        final ByteBuffer order = ByteBuffer.allocate(88).order(ByteOrder.LITTLE_ENDIAN);
        this.xStats.writeTo(order);
        this.yStats.writeTo(order);
        order.putDouble(this.sumOfProductsOfDeltas);
        return order.array();
    }
    
    @Override
    public String toString() {
        com.google.common.base.a.b b;
        if (this.count() > 0L) {
            b = com.google.common.base.a.c(this).d("xStats", this.xStats).d("yStats", this.yStats).a("populationCovariance", this.populationCovariance());
        }
        else {
            b = com.google.common.base.a.c(this).d("xStats", this.xStats).d("yStats", this.yStats);
        }
        return b.toString();
    }
    
    public Stats xStats() {
        return this.xStats;
    }
    
    public Stats yStats() {
        return this.yStats;
    }
}

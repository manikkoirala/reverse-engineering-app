// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.math;

import com.google.common.base.a;
import com.google.common.primitives.Doubles;
import java.util.Iterator;
import java.nio.ByteOrder;
import java.nio.ByteBuffer;
import java.io.Serializable;

public final class Stats implements Serializable
{
    static final int BYTES = 40;
    private static final long serialVersionUID = 0L;
    private final long count;
    private final double max;
    private final double mean;
    private final double min;
    private final double sumOfSquaresOfDeltas;
    
    public Stats(final long count, final double mean, final double sumOfSquaresOfDeltas, final double min, final double max) {
        this.count = count;
        this.mean = mean;
        this.sumOfSquaresOfDeltas = sumOfSquaresOfDeltas;
        this.min = min;
        this.max = max;
    }
    
    public static Stats fromByteArray(final byte[] array) {
        i71.r(array);
        i71.i(array.length == 40, "Expected Stats.BYTES = %s remaining , got %s", 40, array.length);
        return readFrom(ByteBuffer.wrap(array).order(ByteOrder.LITTLE_ENDIAN));
    }
    
    public static double meanOf(final Iterable<? extends Number> iterable) {
        return meanOf(iterable.iterator());
    }
    
    public static double meanOf(final Iterator<? extends Number> iterator) {
        i71.d(iterator.hasNext());
        double n = iterator.next().doubleValue();
        long n2 = 1L;
        while (iterator.hasNext()) {
            final double doubleValue = iterator.next().doubleValue();
            ++n2;
            if (Doubles.f(doubleValue) && Doubles.f(n)) {
                n += (doubleValue - n) / n2;
            }
            else {
                n = aq1.g(n, doubleValue);
            }
        }
        return n;
    }
    
    public static double meanOf(final double... array) {
        final int length = array.length;
        int i = 1;
        i71.d(length > 0);
        double g = array[0];
        while (i < array.length) {
            final double n = array[i];
            if (Doubles.f(n) && Doubles.f(g)) {
                g += (n - g) / (i + 1);
            }
            else {
                g = aq1.g(g, n);
            }
            ++i;
        }
        return g;
    }
    
    public static double meanOf(final int... array) {
        final int length = array.length;
        int i = 1;
        i71.d(length > 0);
        double g = array[0];
        while (i < array.length) {
            final double n = array[i];
            if (Doubles.f(n) && Doubles.f(g)) {
                g += (n - g) / (i + 1);
            }
            else {
                g = aq1.g(g, n);
            }
            ++i;
        }
        return g;
    }
    
    public static double meanOf(final long... array) {
        final int length = array.length;
        int i = 1;
        i71.d(length > 0);
        double g = (double)array[0];
        while (i < array.length) {
            final double n = (double)array[i];
            if (Doubles.f(n) && Doubles.f(g)) {
                g += (n - g) / (i + 1);
            }
            else {
                g = aq1.g(g, n);
            }
            ++i;
        }
        return g;
    }
    
    public static Stats of(final Iterable<? extends Number> iterable) {
        final aq1 aq1 = new aq1();
        aq1.b(iterable);
        return aq1.h();
    }
    
    public static Stats of(final Iterator<? extends Number> iterator) {
        final aq1 aq1 = new aq1();
        aq1.c(iterator);
        return aq1.h();
    }
    
    public static Stats of(final double... array) {
        final aq1 aq1 = new aq1();
        aq1.d(array);
        return aq1.h();
    }
    
    public static Stats of(final int... array) {
        final aq1 aq1 = new aq1();
        aq1.e(array);
        return aq1.h();
    }
    
    public static Stats of(final long... array) {
        final aq1 aq1 = new aq1();
        aq1.f(array);
        return aq1.h();
    }
    
    public static Stats readFrom(final ByteBuffer byteBuffer) {
        i71.r(byteBuffer);
        i71.i(byteBuffer.remaining() >= 40, "Expected at least Stats.BYTES = %s remaining , got %s", 40, byteBuffer.remaining());
        return new Stats(byteBuffer.getLong(), byteBuffer.getDouble(), byteBuffer.getDouble(), byteBuffer.getDouble(), byteBuffer.getDouble());
    }
    
    public long count() {
        return this.count;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = false;
        if (o == null) {
            return false;
        }
        if (Stats.class != o.getClass()) {
            return false;
        }
        final Stats stats = (Stats)o;
        boolean b2 = b;
        if (this.count == stats.count) {
            b2 = b;
            if (Double.doubleToLongBits(this.mean) == Double.doubleToLongBits(stats.mean)) {
                b2 = b;
                if (Double.doubleToLongBits(this.sumOfSquaresOfDeltas) == Double.doubleToLongBits(stats.sumOfSquaresOfDeltas)) {
                    b2 = b;
                    if (Double.doubleToLongBits(this.min) == Double.doubleToLongBits(stats.min)) {
                        b2 = b;
                        if (Double.doubleToLongBits(this.max) == Double.doubleToLongBits(stats.max)) {
                            b2 = true;
                        }
                    }
                }
            }
        }
        return b2;
    }
    
    @Override
    public int hashCode() {
        return b11.b(this.count, this.mean, this.sumOfSquaresOfDeltas, this.min, this.max);
    }
    
    public double max() {
        i71.x(this.count != 0L);
        return this.max;
    }
    
    public double mean() {
        i71.x(this.count != 0L);
        return this.mean;
    }
    
    public double min() {
        i71.x(this.count != 0L);
        return this.min;
    }
    
    public double populationStandardDeviation() {
        return Math.sqrt(this.populationVariance());
    }
    
    public double populationVariance() {
        i71.x(this.count > 0L);
        if (Double.isNaN(this.sumOfSquaresOfDeltas)) {
            return Double.NaN;
        }
        if (this.count == 1L) {
            return 0.0;
        }
        return qu.a(this.sumOfSquaresOfDeltas) / this.count();
    }
    
    public double sampleStandardDeviation() {
        return Math.sqrt(this.sampleVariance());
    }
    
    public double sampleVariance() {
        i71.x(this.count > 1L);
        if (Double.isNaN(this.sumOfSquaresOfDeltas)) {
            return Double.NaN;
        }
        return qu.a(this.sumOfSquaresOfDeltas) / (this.count - 1L);
    }
    
    public double sum() {
        return this.mean * this.count;
    }
    
    public double sumOfSquaresOfDeltas() {
        return this.sumOfSquaresOfDeltas;
    }
    
    public byte[] toByteArray() {
        final ByteBuffer order = ByteBuffer.allocate(40).order(ByteOrder.LITTLE_ENDIAN);
        this.writeTo(order);
        return order.array();
    }
    
    @Override
    public String toString() {
        a.b b;
        if (this.count() > 0L) {
            b = a.c(this).c("count", this.count).a("mean", this.mean).a("populationStandardDeviation", this.populationStandardDeviation()).a("min", this.min).a("max", this.max);
        }
        else {
            b = a.c(this).c("count", this.count);
        }
        return b.toString();
    }
    
    public void writeTo(final ByteBuffer byteBuffer) {
        i71.r(byteBuffer);
        i71.i(byteBuffer.remaining() >= 40, "Expected at least Stats.BYTES = %s remaining , got %s", 40, byteBuffer.remaining());
        byteBuffer.putLong(this.count).putDouble(this.mean).putDouble(this.sumOfSquaresOfDeltas).putDouble(this.min).putDouble(this.max);
    }
}

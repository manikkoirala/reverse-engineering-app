// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.math;

public abstract class a
{
    public static a a() {
        return c.a;
    }
    
    public static a b(final double n) {
        i71.d(qu.c(n));
        return new d(0.0, n);
    }
    
    public static b c(final double n, final double n2) {
        i71.d(qu.c(n) && qu.c(n2));
        return new b(n, n2, null);
    }
    
    public static a d(final double n) {
        i71.d(qu.c(n));
        return new e(n);
    }
    
    public static final class b
    {
        public final double a;
        public final double b;
        
        public b(final double a, final double b) {
            this.a = a;
            this.b = b;
        }
        
        public a a(final double v) {
            i71.d(Double.isNaN(v) ^ true);
            if (qu.c(v)) {
                return new d(v, this.b - this.a * v);
            }
            return new e(this.a);
        }
    }
    
    public static final class c extends a
    {
        public static final c a;
        
        static {
            a = new c();
        }
        
        @Override
        public String toString() {
            return "NaN";
        }
    }
    
    public static final class d extends a
    {
        public final double a;
        public final double b;
        public a c;
        
        public d(final double a, final double b) {
            this.a = a;
            this.b = b;
            this.c = null;
        }
        
        @Override
        public String toString() {
            return String.format("y = %g * x + %g", this.a, this.b);
        }
    }
    
    public static final class e extends a
    {
        public final double a;
        public a b;
        
        public e(final double a) {
            this.a = a;
            this.b = null;
        }
        
        @Override
        public String toString() {
            return String.format("x = %g", this.a);
        }
    }
}

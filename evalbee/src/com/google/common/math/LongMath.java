// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.math;

import com.google.common.primitives.UnsignedLongs;
import java.math.RoundingMode;

public abstract class LongMath
{
    public static final byte[] a;
    public static final long[] b;
    public static final long[] c;
    public static final long[] d;
    public static final int[] e;
    public static final int[] f;
    public static final long[][] g;
    
    static {
        a = new byte[] { 19, 18, 18, 18, 18, 17, 17, 17, 16, 16, 16, 15, 15, 15, 15, 14, 14, 14, 13, 13, 13, 12, 12, 12, 12, 11, 11, 11, 10, 10, 10, 9, 9, 9, 9, 8, 8, 8, 7, 7, 7, 6, 6, 6, 6, 5, 5, 5, 4, 4, 4, 3, 3, 3, 3, 2, 2, 2, 1, 1, 1, 0, 0, 0 };
        b = new long[] { 1L, 10L, 100L, 1000L, 10000L, 100000L, 1000000L, 10000000L, 100000000L, 1000000000L, 10000000000L, 100000000000L, 1000000000000L, 10000000000000L, 100000000000000L, 1000000000000000L, 10000000000000000L, 100000000000000000L, 1000000000000000000L };
        c = new long[] { 3L, 31L, 316L, 3162L, 31622L, 316227L, 3162277L, 31622776L, 316227766L, 3162277660L, 31622776601L, 316227766016L, 3162277660168L, 31622776601683L, 316227766016837L, 3162277660168379L, 31622776601683793L, 316227766016837933L, 3162277660168379331L };
        d = new long[] { 1L, 1L, 2L, 6L, 24L, 120L, 720L, 5040L, 40320L, 362880L, 3628800L, 39916800L, 479001600L, 6227020800L, 87178291200L, 1307674368000L, 20922789888000L, 355687428096000L, 6402373705728000L, 121645100408832000L, 2432902008176640000L };
        e = new int[] { Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE, 3810779, 121977, 16175, 4337, 1733, 887, 534, 361, 265, 206, 169, 143, 125, 111, 101, 94, 88, 83, 79, 76, 74, 72, 70, 69, 68, 67, 67, 66, 66, 66, 66 };
        f = new int[] { Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE, 2642246, 86251, 11724, 3218, 1313, 684, 419, 287, 214, 169, 139, 119, 105, 95, 87, 81, 76, 73, 70, 68, 66, 64, 63, 62, 62, 61, 61, 61 };
        g = new long[][] { { 291830L, 126401071349994536L }, { 885594168L, 725270293939359937L, 3569819667048198375L }, { 273919523040L, 15L, 7363882082L, 992620450144556L }, { 47636622961200L, 2L, 2570940L, 211991001L, 3749873356L }, { 7999252175582850L, 2L, 4130806001517L, 149795463772692060L, 186635894390467037L, 3967304179347715805L }, { 585226005592931976L, 2L, 123635709730000L, 9233062284813009L, 43835965440333360L, 761179012939631437L, 1263739024124850375L }, { Long.MAX_VALUE, 2L, 325L, 9375L, 28178L, 450775L, 9780504L, 1795265022L } };
    }
    
    public static long a(final long n, final long n2) {
        final long n3 = n + n2;
        boolean b = true;
        final boolean b2 = (n ^ n2) < 0L;
        if ((n ^ n3) < 0L) {
            b = false;
        }
        ju0.c(b2 | b, "checkedAdd", n, n2);
        return n3;
    }
    
    public static long b(final long i, final long j) {
        final int n = Long.numberOfLeadingZeros(i) + Long.numberOfLeadingZeros(~i) + Long.numberOfLeadingZeros(j) + Long.numberOfLeadingZeros(~j);
        if (n > 65) {
            return i * j;
        }
        ju0.c(n >= 64, "checkedMultiply", i, j);
        final long n2 = lcmp(i, 0L);
        ju0.c(n2 >= 0 | j != Long.MIN_VALUE, "checkedMultiply", i, j);
        final long n3 = i * j;
        ju0.c(n2 == 0 || n3 / i == j, "checkedMultiply", i, j);
        return n3;
    }
    
    public static long c(long abs, final long a, final RoundingMode roundingMode) {
        i71.r(roundingMode);
        final long n = abs / a;
        final long a2 = abs - a * n;
        final long n2 = lcmp(a2, 0L);
        if (n2 == 0) {
            return n;
        }
        final int n3 = (int)((abs ^ a) >> 63);
        final boolean b = true;
        boolean b2 = true;
        final int n4 = n3 | 0x1;
        int n5 = b ? 1 : 0;
        Label_0231: {
            switch (LongMath$a.a[roundingMode.ordinal()]) {
                default: {
                    throw new AssertionError();
                }
                case 6:
                case 7:
                case 8: {
                    abs = Math.abs(a2);
                    final long n6 = lcmp(abs - (Math.abs(a) - abs), 0L);
                    if (n6 == 0) {
                        n5 = (b ? 1 : 0);
                        if (roundingMode == RoundingMode.HALF_UP) {
                            break Label_0231;
                        }
                        if (roundingMode == RoundingMode.HALF_EVEN && (0x1L & n) != 0x0L) {
                            n5 = (b ? 1 : 0);
                            break Label_0231;
                        }
                        break Label_0231;
                    }
                    else {
                        if (n6 > 0) {
                            n5 = (b ? 1 : 0);
                            break Label_0231;
                        }
                        break Label_0231;
                    }
                    break;
                }
                case 5: {
                    if (n4 > 0) {
                        n5 = (b ? 1 : 0);
                        break Label_0231;
                    }
                    break Label_0231;
                }
                case 3: {
                    if (n4 < 0) {
                        n5 = (b ? 1 : 0);
                        break Label_0231;
                    }
                    break Label_0231;
                }
                case 1: {
                    if (n2 != 0) {
                        b2 = false;
                    }
                    ju0.e(b2);
                }
                case 2: {
                    n5 = 0;
                }
                case 4: {
                    abs = n;
                    if (n5 != 0) {
                        abs = n + n4;
                    }
                    return abs;
                }
            }
        }
    }
    
    public enum MillerRabinTester
    {
        private static final MillerRabinTester[] $VALUES;
        
        LARGE {
            private long plusMod(final long n, final long n2, final long n3) {
                long n4 = n + n2;
                if (n >= n3 - n2) {
                    n4 -= n3;
                }
                return n4;
            }
            
            private long times2ToThe32Mod(long i, final long n) {
                int a = 32;
                int j;
                long e;
                do {
                    final int min = Math.min(a, Long.numberOfLeadingZeros(i));
                    e = UnsignedLongs.e(i << min, n);
                    j = (a -= min);
                    i = e;
                } while (j > 0);
                return e;
            }
            
            @Override
            public long mulMod(long e, long n, final long n2) {
                final long n3 = e >>> 32;
                final long n4 = n >>> 32;
                final long n5 = e & 0xFFFFFFFFL;
                final long n6 = n & 0xFFFFFFFFL;
                n = (e = this.times2ToThe32Mod(n3 * n4, n2) + n3 * n6);
                if (n < 0L) {
                    e = UnsignedLongs.e(n, n2);
                }
                return this.plusMod(this.times2ToThe32Mod(e + n4 * n5, n2), UnsignedLongs.e(n5 * n6, n2), n2);
            }
            
            @Override
            public long squareMod(long e, final long n) {
                final long n2 = e >>> 32;
                final long n3 = e & 0xFFFFFFFFL;
                final long times2ToThe32Mod = this.times2ToThe32Mod(n2 * n2, n);
                final long n4 = e = n2 * n3 * 2L;
                if (n4 < 0L) {
                    e = UnsignedLongs.e(n4, n);
                }
                return this.plusMod(this.times2ToThe32Mod(times2ToThe32Mod + e, n), UnsignedLongs.e(n3 * n3, n), n);
            }
        }, 
        SMALL {
            @Override
            public long mulMod(final long n, final long n2, final long n3) {
                return n * n2 % n3;
            }
            
            @Override
            public long squareMod(final long n, final long n2) {
                return n * n % n2;
            }
        };
        
        private static /* synthetic */ MillerRabinTester[] $values() {
            return new MillerRabinTester[] { MillerRabinTester.SMALL, MillerRabinTester.LARGE };
        }
        
        static {
            $VALUES = $values();
        }
        
        private long powMod(long squareMod, long n, final long n2) {
            long n3 = 1L;
            while (n != 0L) {
                long mulMod = n3;
                if ((n & 0x1L) != 0x0L) {
                    mulMod = this.mulMod(n3, squareMod, n2);
                }
                squareMod = this.squareMod(squareMod, n2);
                n >>= 1;
                n3 = mulMod;
            }
            return n3;
        }
        
        public static boolean test(final long n, final long n2) {
            MillerRabinTester millerRabinTester;
            if (n2 <= 3037000499L) {
                millerRabinTester = MillerRabinTester.SMALL;
            }
            else {
                millerRabinTester = MillerRabinTester.LARGE;
            }
            return millerRabinTester.testWitness(n, n2);
        }
        
        private boolean testWitness(long n, final long n2) {
            final long i = n2 - 1L;
            final int numberOfTrailingZeros = Long.numberOfTrailingZeros(i);
            n %= n2;
            if (n == 0L) {
                return true;
            }
            n = this.powMod(n, i >> numberOfTrailingZeros, n2);
            if (n == 1L) {
                return true;
            }
            int n3 = 0;
            while (n != i) {
                if (++n3 == numberOfTrailingZeros) {
                    return false;
                }
                n = this.squareMod(n, n2);
            }
            return true;
        }
        
        public abstract long mulMod(final long p0, final long p1, final long p2);
        
        public abstract long squareMod(final long p0, final long p1);
    }
}

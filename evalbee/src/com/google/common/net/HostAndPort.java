// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.net;

import java.io.Serializable;

public final class HostAndPort implements Serializable
{
    private static final int NO_PORT = -1;
    private static final long serialVersionUID = 0L;
    private final boolean hasBracketlessColons;
    private final String host;
    private final int port;
    
    private HostAndPort(final String host, final int port, final boolean hasBracketlessColons) {
        this.host = host;
        this.port = port;
        this.hasBracketlessColons = hasBracketlessColons;
    }
    
    public static HostAndPort fromHost(final String s) {
        final HostAndPort fromString = fromString(s);
        i71.m(fromString.hasPort() ^ true, "Host has a port: %s", s);
        return fromString;
    }
    
    public static HostAndPort fromParts(final String s, final int n) {
        i71.h(isValidPort(n), "Port out of range: %s", n);
        final HostAndPort fromString = fromString(s);
        i71.m(fromString.hasPort() ^ true, "Host has a port: %s", s);
        return new HostAndPort(fromString.host, n, fromString.hasBracketlessColons);
    }
    
    public static HostAndPort fromString(String concat) {
        i71.r(concat);
        final boolean startsWith = ((String)concat).startsWith("[");
        int int1 = -1;
        boolean b = true;
        boolean b2 = false;
        Object o = null;
        String s = null;
        Label_0122: {
            String substring = null;
            String substring2 = null;
            Label_0041: {
                if (!startsWith) {
                    final int index = ((String)concat).indexOf(58);
                    if (index >= 0) {
                        final int n = index + 1;
                        if (((String)concat).indexOf(58, n) == -1) {
                            substring = ((String)concat).substring(0, index);
                            substring2 = ((String)concat).substring(n);
                            break Label_0041;
                        }
                    }
                    b2 = (index >= 0);
                    o = concat;
                    s = null;
                    break Label_0122;
                }
                final String[] hostAndPortFromBracketedHost = getHostAndPortFromBracketedHost((String)concat);
                substring = hostAndPortFromBracketedHost[0];
                substring2 = hostAndPortFromBracketedHost[1];
            }
            final String s2 = substring;
            b2 = false;
            s = substring2;
            o = s2;
        }
        if (!ir1.b(s)) {
            if (s.startsWith("+") || !gg.c().h(s)) {
                b = false;
            }
            i71.m(b, "Unparseable port number: %s", concat);
            try {
                int1 = Integer.parseInt(s);
                i71.m(isValidPort(int1), "Port number out of range: %s", concat);
            }
            catch (final NumberFormatException ex) {
                if (((String)concat).length() != 0) {
                    concat = "Unparseable port number: ".concat((String)concat);
                }
                else {
                    concat = new String("Unparseable port number: ");
                }
                throw new IllegalArgumentException((String)concat);
            }
        }
        return new HostAndPort((String)o, int1, b2);
    }
    
    private static String[] getHostAndPortFromBracketedHost(String substring) {
        final boolean b = false;
        i71.m(substring.charAt(0) == '[', "Bracketed host-port string must start with a bracket: %s", substring);
        final int index = substring.indexOf(58);
        final int lastIndex = substring.lastIndexOf(93);
        i71.m(index > -1 && lastIndex > index, "Invalid bracketed host/port: %s", substring);
        final String substring2 = substring.substring(1, lastIndex);
        final int index2 = lastIndex + 1;
        if (index2 == substring.length()) {
            substring = "";
        }
        else {
            boolean b2 = b;
            if (substring.charAt(index2) == ':') {
                b2 = true;
            }
            i71.m(b2, "Only a colon may follow a close bracket: %s", substring);
            int i;
            int beginIndex;
            for (beginIndex = (i = lastIndex + 2); i < substring.length(); ++i) {
                i71.m(Character.isDigit(substring.charAt(i)), "Port must be numeric: %s", substring);
            }
            substring = substring.substring(beginIndex);
        }
        return new String[] { substring2, substring };
    }
    
    private static boolean isValidPort(final int n) {
        return n >= 0 && n <= 65535;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o instanceof HostAndPort) {
            final HostAndPort hostAndPort = (HostAndPort)o;
            if (!b11.a(this.host, hostAndPort.host) || this.port != hostAndPort.port) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    public String getHost() {
        return this.host;
    }
    
    public int getPort() {
        i71.x(this.hasPort());
        return this.port;
    }
    
    public int getPortOrDefault(int port) {
        if (this.hasPort()) {
            port = this.port;
        }
        return port;
    }
    
    public boolean hasPort() {
        return this.port >= 0;
    }
    
    @Override
    public int hashCode() {
        return b11.b(this.host, this.port);
    }
    
    public HostAndPort requireBracketsForIPv6() {
        i71.m(this.hasBracketlessColons ^ true, "Possible bracketless IPv6 literal: %s", this.host);
        return this;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(this.host.length() + 8);
        if (this.host.indexOf(58) >= 0) {
            sb.append('[');
            sb.append(this.host);
            sb.append(']');
        }
        else {
            sb.append(this.host);
        }
        if (this.hasPort()) {
            sb.append(':');
            sb.append(this.port);
        }
        return sb.toString();
    }
    
    public HostAndPort withDefaultPort(final int n) {
        i71.d(isValidPort(n));
        if (this.hasPort()) {
            return this;
        }
        return new HostAndPort(this.host, n, this.hasBracketlessColons);
    }
}

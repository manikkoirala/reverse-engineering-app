// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.hash;

import com.google.common.primitives.Longs;

enum LittleEndianByteArray$JavaLittleEndianBytes
{
    private static final LittleEndianByteArray$JavaLittleEndianBytes[] $VALUES;
    
    INSTANCE {
        @Override
        public long getLongLittleEndian(final byte[] array, final int n) {
            return Longs.d(array[n + 7], array[n + 6], array[n + 5], array[n + 4], array[n + 3], array[n + 2], array[n + 1], array[n]);
        }
        
        @Override
        public void putLongLittleEndian(final byte[] array, final int n, final long n2) {
            long n3 = 255L;
            for (int i = 0; i < 8; ++i) {
                array[n + i] = (byte)((n2 & n3) >> i * 8);
                n3 <<= 8;
            }
        }
    };
    
    private static /* synthetic */ LittleEndianByteArray$JavaLittleEndianBytes[] $values() {
        return new LittleEndianByteArray$JavaLittleEndianBytes[] { LittleEndianByteArray$JavaLittleEndianBytes.INSTANCE };
    }
    
    static {
        $VALUES = $values();
    }
}

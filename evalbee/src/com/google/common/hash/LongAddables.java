// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.hash;

import java.util.concurrent.atomic.AtomicLong;

public abstract class LongAddables
{
    public static final is1 a;
    
    static {
        is1 a2 = null;
        try {
            new LongAdder();
            final is1 is1 = new is1() {
                public bm0 a() {
                    return new LongAdder();
                }
            };
        }
        finally {
            a2 = new is1() {
                public bm0 a() {
                    return new PureJavaLongAddable(null);
                }
            };
        }
        a = a2;
    }
    
    public static bm0 a() {
        return (bm0)LongAddables.a.get();
    }
    
    public static final class PureJavaLongAddable extends AtomicLong implements bm0
    {
        private PureJavaLongAddable() {
        }
        
        @Override
        public void add(final long delta) {
            this.getAndAdd(delta);
        }
        
        @Override
        public void increment() {
            this.getAndIncrement();
        }
        
        @Override
        public long sum() {
            return this.get();
        }
    }
}

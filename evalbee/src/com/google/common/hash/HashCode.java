// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.hash;

import com.google.common.primitives.UnsignedInts;
import java.io.Serializable;
import com.google.common.primitives.Ints;

public abstract class HashCode
{
    private static final char[] hexDigits;
    
    static {
        hexDigits = "0123456789abcdef".toCharArray();
    }
    
    public static int a(final char c) {
        if (c >= '0' && c <= '9') {
            return c - '0';
        }
        if (c >= 'a' && c <= 'f') {
            return c - 'a' + 10;
        }
        final StringBuilder sb = new StringBuilder(32);
        sb.append("Illegal hexadecimal character: ");
        sb.append(c);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static HashCode fromBytes(final byte[] array) {
        final int length = array.length;
        boolean b = true;
        if (length < 1) {
            b = false;
        }
        i71.e(b, "A HashCode must contain at least 1 byte.");
        return fromBytesNoCopy(array.clone());
    }
    
    public static HashCode fromBytesNoCopy(final byte[] array) {
        return new BytesHashCode(array);
    }
    
    public static HashCode fromInt(final int n) {
        return new IntHashCode(n);
    }
    
    public static HashCode fromLong(final long n) {
        return new LongHashCode(n);
    }
    
    public static HashCode fromString(final String s) {
        final int length = s.length();
        final boolean b = true;
        int i = 0;
        i71.m(length >= 2, "input string (%s) must have at least 2 characters", s);
        i71.m(s.length() % 2 == 0 && b, "input string (%s) must have an even number of characters", s);
        final byte[] array = new byte[s.length() / 2];
        while (i < s.length()) {
            array[i / 2] = (byte)((a(s.charAt(i)) << 4) + a(s.charAt(i + 1)));
            i += 2;
        }
        return fromBytesNoCopy(array);
    }
    
    public abstract byte[] asBytes();
    
    public abstract int asInt();
    
    public abstract long asLong();
    
    public abstract int bits();
    
    @Override
    public final boolean equals(final Object o) {
        final boolean b = o instanceof HashCode;
        boolean b3;
        final boolean b2 = b3 = false;
        if (b) {
            final HashCode hashCode = (HashCode)o;
            b3 = b2;
            if (this.bits() == hashCode.bits()) {
                b3 = b2;
                if (this.equalsSameBits(hashCode)) {
                    b3 = true;
                }
            }
        }
        return b3;
    }
    
    public abstract boolean equalsSameBits(final HashCode p0);
    
    public byte[] getBytesInternal() {
        return this.asBytes();
    }
    
    @Override
    public final int hashCode() {
        if (this.bits() >= 32) {
            return this.asInt();
        }
        final byte[] bytesInternal = this.getBytesInternal();
        int n = bytesInternal[0] & 0xFF;
        for (int i = 1; i < bytesInternal.length; ++i) {
            n |= (bytesInternal[i] & 0xFF) << i * 8;
        }
        return n;
    }
    
    @Override
    public final String toString() {
        final byte[] bytesInternal = this.getBytesInternal();
        final StringBuilder sb = new StringBuilder(bytesInternal.length * 2);
        for (final byte b : bytesInternal) {
            final char[] hexDigits = HashCode.hexDigits;
            sb.append(hexDigits[b >> 4 & 0xF]);
            sb.append(hexDigits[b & 0xF]);
        }
        return sb.toString();
    }
    
    public int writeBytesTo(final byte[] array, final int n, int j) {
        j = Ints.j(j, this.bits() / 8);
        i71.w(n, n + j, array.length);
        this.writeBytesToImpl(array, n, j);
        return j;
    }
    
    public abstract void writeBytesToImpl(final byte[] p0, final int p1, final int p2);
    
    public static final class BytesHashCode extends HashCode implements Serializable
    {
        private static final long serialVersionUID = 0L;
        final byte[] bytes;
        
        public BytesHashCode(final byte[] array) {
            this.bytes = (byte[])i71.r(array);
        }
        
        @Override
        public byte[] asBytes() {
            return this.bytes.clone();
        }
        
        @Override
        public int asInt() {
            final byte[] bytes = this.bytes;
            i71.z(bytes.length >= 4, "HashCode#asInt() requires >= 4 bytes (it only has %s bytes).", bytes.length);
            final byte[] bytes2 = this.bytes;
            return (bytes2[3] & 0xFF) << 24 | ((bytes2[0] & 0xFF) | (bytes2[1] & 0xFF) << 8 | (bytes2[2] & 0xFF) << 16);
        }
        
        @Override
        public long asLong() {
            final byte[] bytes = this.bytes;
            i71.z(bytes.length >= 8, "HashCode#asLong() requires >= 8 bytes (it only has %s bytes).", bytes.length);
            return this.padToLong();
        }
        
        @Override
        public int bits() {
            return this.bytes.length * 8;
        }
        
        @Override
        public boolean equalsSameBits(final HashCode hashCode) {
            if (this.bytes.length != hashCode.getBytesInternal().length) {
                return false;
            }
            boolean b = true;
            int n = 0;
            while (true) {
                final byte[] bytes = this.bytes;
                if (n >= bytes.length) {
                    break;
                }
                b &= (bytes[n] == hashCode.getBytesInternal()[n]);
                ++n;
            }
            return b;
        }
        
        @Override
        public byte[] getBytesInternal() {
            return this.bytes;
        }
        
        public long padToLong() {
            long n = this.bytes[0] & 0xFF;
            for (int i = 1; i < Math.min(this.bytes.length, 8); ++i) {
                n |= ((long)this.bytes[i] & 0xFFL) << i * 8;
            }
            return n;
        }
        
        @Override
        public void writeBytesToImpl(final byte[] array, final int n, final int n2) {
            System.arraycopy(this.bytes, 0, array, n, n2);
        }
    }
    
    public static final class IntHashCode extends HashCode implements Serializable
    {
        private static final long serialVersionUID = 0L;
        final int hash;
        
        public IntHashCode(final int hash) {
            this.hash = hash;
        }
        
        @Override
        public byte[] asBytes() {
            final int hash = this.hash;
            return new byte[] { (byte)hash, (byte)(hash >> 8), (byte)(hash >> 16), (byte)(hash >> 24) };
        }
        
        @Override
        public int asInt() {
            return this.hash;
        }
        
        @Override
        public long asLong() {
            throw new IllegalStateException("this HashCode only has 32 bits; cannot create a long");
        }
        
        @Override
        public int bits() {
            return 32;
        }
        
        @Override
        public boolean equalsSameBits(final HashCode hashCode) {
            return this.hash == hashCode.asInt();
        }
        
        public long padToLong() {
            return UnsignedInts.f(this.hash);
        }
        
        @Override
        public void writeBytesToImpl(final byte[] array, final int n, final int n2) {
            for (int i = 0; i < n2; ++i) {
                array[n + i] = (byte)(this.hash >> i * 8);
            }
        }
    }
    
    public static final class LongHashCode extends HashCode implements Serializable
    {
        private static final long serialVersionUID = 0L;
        final long hash;
        
        public LongHashCode(final long hash) {
            this.hash = hash;
        }
        
        @Override
        public byte[] asBytes() {
            final long hash = this.hash;
            return new byte[] { (byte)hash, (byte)(hash >> 8), (byte)(hash >> 16), (byte)(hash >> 24), (byte)(hash >> 32), (byte)(hash >> 40), (byte)(hash >> 48), (byte)(hash >> 56) };
        }
        
        @Override
        public int asInt() {
            return (int)this.hash;
        }
        
        @Override
        public long asLong() {
            return this.hash;
        }
        
        @Override
        public int bits() {
            return 64;
        }
        
        @Override
        public boolean equalsSameBits(final HashCode hashCode) {
            return this.hash == hashCode.asLong();
        }
        
        public long padToLong() {
            return this.hash;
        }
        
        @Override
        public void writeBytesToImpl(final byte[] array, final int n, final int n2) {
            for (int i = 0; i < n2; ++i) {
                array[n + i] = (byte)(this.hash >> i * 8);
            }
        }
    }
}

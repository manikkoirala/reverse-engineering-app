// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.hash;

import java.nio.ByteOrder;
import java.nio.ByteBuffer;
import java.io.Serializable;

final class Murmur3_128HashFunction extends m implements Serializable
{
    static final oc0 GOOD_FAST_HASH_128;
    static final oc0 MURMUR3_128;
    private static final long serialVersionUID = 0L;
    private final int seed;
    
    static {
        MURMUR3_128 = new Murmur3_128HashFunction(0);
        GOOD_FAST_HASH_128 = new Murmur3_128HashFunction(Hashing.a);
    }
    
    public Murmur3_128HashFunction(final int seed) {
        this.seed = seed;
    }
    
    public int bits() {
        return 128;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof Murmur3_128HashFunction;
        boolean b2 = false;
        if (b) {
            final Murmur3_128HashFunction murmur3_128HashFunction = (Murmur3_128HashFunction)o;
            b2 = b2;
            if (this.seed == murmur3_128HashFunction.seed) {
                b2 = true;
            }
        }
        return b2;
    }
    
    @Override
    public int hashCode() {
        return Murmur3_128HashFunction.class.hashCode() ^ this.seed;
    }
    
    @Override
    public qc0 newHasher() {
        return new a(this.seed);
    }
    
    @Override
    public String toString() {
        final int seed = this.seed;
        final StringBuilder sb = new StringBuilder(32);
        sb.append("Hashing.murmur3_128(");
        sb.append(seed);
        sb.append(")");
        return sb.toString();
    }
    
    public static final class a extends l0
    {
        public long d;
        public long e;
        public int f;
        
        public a(final int n) {
            super(16);
            final long n2 = n;
            this.d = n2;
            this.e = n2;
            this.f = 0;
        }
        
        public static long s(long n) {
            n = (n ^ n >>> 33) * -49064778989728563L;
            n = (n ^ n >>> 33) * -4265267296055464877L;
            return n ^ n >>> 33;
        }
        
        public static long t(final long n) {
            return Long.rotateLeft(n * -8663945395140668459L, 31) * 5545529020109919103L;
        }
        
        public static long u(final long n) {
            return Long.rotateLeft(n * 5545529020109919103L, 33) * -8663945395140668459L;
        }
        
        @Override
        public HashCode l() {
            final long d = this.d;
            final int f = this.f;
            final long n = f;
            final long n2 = this.e ^ (long)f;
            final long d2 = (d ^ n) + n2;
            this.d = d2;
            this.e = n2 + d2;
            this.d = s(d2);
            final long s = s(this.e);
            final long d3 = this.d + s;
            this.d = d3;
            this.e = s + d3;
            return HashCode.fromBytesNoCopy(ByteBuffer.wrap(new byte[16]).order(ByteOrder.LITTLE_ENDIAN).putLong(this.d).putLong(this.e).array());
        }
        
        @Override
        public void o(final ByteBuffer byteBuffer) {
            this.r(byteBuffer.getLong(), byteBuffer.getLong());
            this.f += 16;
        }
        
        @Override
        public void p(final ByteBuffer byteBuffer) {
            this.f += byteBuffer.remaining();
            final int remaining = byteBuffer.remaining();
            long n = 0L;
            long n16 = 0L;
            Label_0400: {
                long n14 = 0L;
                Label_0388: {
                    long n13 = 0L;
                    Label_0368: {
                        long n12 = 0L;
                        Label_0348: {
                            long n11 = 0L;
                            Label_0328: {
                                long n10 = 0L;
                                Label_0308: {
                                    long n9 = 0L;
                                    Label_0288: {
                                        long n8 = 0L;
                                        Label_0250: {
                                            long n7 = 0L;
                                            Label_0232: {
                                                long n6 = 0L;
                                                Label_0211: {
                                                    long n5 = 0L;
                                                    Label_0190: {
                                                        long n4 = 0L;
                                                        Label_0169: {
                                                            long n3 = 0L;
                                                            Label_0148: {
                                                                long n2 = 0L;
                                                                switch (remaining) {
                                                                    default: {
                                                                        throw new AssertionError((Object)"Should never get here.");
                                                                    }
                                                                    case 15: {
                                                                        n2 = ((long)f12.c(byteBuffer.get(14)) << 48 ^ 0x0L);
                                                                        break;
                                                                    }
                                                                    case 14: {
                                                                        n2 = 0L;
                                                                        break;
                                                                    }
                                                                    case 13: {
                                                                        n3 = 0L;
                                                                        break Label_0148;
                                                                    }
                                                                    case 12: {
                                                                        n4 = 0L;
                                                                        break Label_0169;
                                                                    }
                                                                    case 11: {
                                                                        n5 = 0L;
                                                                        break Label_0190;
                                                                    }
                                                                    case 10: {
                                                                        n6 = 0L;
                                                                        break Label_0211;
                                                                    }
                                                                    case 9: {
                                                                        n7 = 0L;
                                                                        break Label_0232;
                                                                    }
                                                                    case 8: {
                                                                        n8 = 0L;
                                                                        break Label_0250;
                                                                    }
                                                                    case 7: {
                                                                        n9 = ((long)f12.c(byteBuffer.get(6)) << 48 ^ 0x0L);
                                                                        break Label_0288;
                                                                    }
                                                                    case 6: {
                                                                        n9 = 0L;
                                                                        break Label_0288;
                                                                    }
                                                                    case 5: {
                                                                        n10 = 0L;
                                                                        break Label_0308;
                                                                    }
                                                                    case 4: {
                                                                        n11 = 0L;
                                                                        break Label_0328;
                                                                    }
                                                                    case 3: {
                                                                        n12 = 0L;
                                                                        break Label_0348;
                                                                    }
                                                                    case 2: {
                                                                        n13 = 0L;
                                                                        break Label_0368;
                                                                    }
                                                                    case 1: {
                                                                        n14 = 0L;
                                                                        break Label_0388;
                                                                    }
                                                                }
                                                                n3 = (n2 ^ (long)f12.c(byteBuffer.get(13)) << 40);
                                                            }
                                                            n4 = (n3 ^ (long)f12.c(byteBuffer.get(12)) << 32);
                                                        }
                                                        n5 = (n4 ^ (long)f12.c(byteBuffer.get(11)) << 24);
                                                    }
                                                    n6 = (n5 ^ (long)f12.c(byteBuffer.get(10)) << 16);
                                                }
                                                n7 = (n6 ^ (long)f12.c(byteBuffer.get(9)) << 8);
                                            }
                                            n8 = (n7 ^ (long)f12.c(byteBuffer.get(8)));
                                        }
                                        final long n15 = byteBuffer.getLong() ^ 0x0L;
                                        n = n8;
                                        n16 = n15;
                                        break Label_0400;
                                    }
                                    n10 = (n9 ^ (long)f12.c(byteBuffer.get(5)) << 40);
                                }
                                n11 = (n10 ^ (long)f12.c(byteBuffer.get(4)) << 32);
                            }
                            n12 = (n11 ^ (long)f12.c(byteBuffer.get(3)) << 24);
                        }
                        n13 = (n12 ^ (long)f12.c(byteBuffer.get(2)) << 16);
                    }
                    n14 = (n13 ^ (long)f12.c(byteBuffer.get(1)) << 8);
                }
                n16 = ((long)f12.c(byteBuffer.get(0)) ^ n14);
            }
            this.d ^= t(n16);
            this.e ^= u(n);
        }
        
        public final void r(long rotateLeft, final long n) {
            rotateLeft = (t(rotateLeft) ^ this.d);
            this.d = rotateLeft;
            rotateLeft = Long.rotateLeft(rotateLeft, 27);
            final long e = this.e;
            this.d = (rotateLeft + e) * 5L + 1390208809L;
            rotateLeft = (u(n) ^ e);
            this.e = rotateLeft;
            this.e = (Long.rotateLeft(rotateLeft, 31) + this.d) * 5L + 944331445L;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.hash;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.security.NoSuchAlgorithmException;
import java.security.MessageDigest;
import java.io.Serializable;

final class MessageDigestHashFunction extends m implements Serializable
{
    private final int bytes;
    private final MessageDigest prototype;
    private final boolean supportsClone;
    private final String toString;
    
    public MessageDigestHashFunction(final String s, final int bytes, final String s2) {
        this.toString = (String)i71.r(s2);
        final MessageDigest messageDigest = getMessageDigest(s);
        this.prototype = messageDigest;
        final int digestLength = messageDigest.getDigestLength();
        i71.i(bytes >= 4 && bytes <= digestLength, "bytes (%s) must be >= 4 and < %s", bytes, digestLength);
        this.bytes = bytes;
        this.supportsClone = supportsClone(messageDigest);
    }
    
    public MessageDigestHashFunction(final String s, final String s2) {
        final MessageDigest messageDigest = getMessageDigest(s);
        this.prototype = messageDigest;
        this.bytes = messageDigest.getDigestLength();
        this.toString = (String)i71.r(s2);
        this.supportsClone = supportsClone(messageDigest);
    }
    
    private static MessageDigest getMessageDigest(final String algorithm) {
        try {
            return MessageDigest.getInstance(algorithm);
        }
        catch (final NoSuchAlgorithmException detailMessage) {
            throw new AssertionError((Object)detailMessage);
        }
    }
    
    private static boolean supportsClone(final MessageDigest messageDigest) {
        try {
            messageDigest.clone();
            return true;
        }
        catch (final CloneNotSupportedException ex) {
            return false;
        }
    }
    
    public int bits() {
        return this.bytes * 8;
    }
    
    @Override
    public qc0 newHasher() {
        Label_0032: {
            if (!this.supportsClone) {
                break Label_0032;
            }
            try {
                return new b((MessageDigest)this.prototype.clone(), this.bytes, null);
                return new b(getMessageDigest(this.prototype.getAlgorithm()), this.bytes, null);
            }
            catch (final CloneNotSupportedException ex) {
                return new b(getMessageDigest(this.prototype.getAlgorithm()), this.bytes, null);
            }
        }
    }
    
    @Override
    public String toString() {
        return this.toString;
    }
    
    public Object writeReplace() {
        return new SerializedForm(this.prototype.getAlgorithm(), this.bytes, this.toString, null);
    }
    
    public static final class SerializedForm implements Serializable
    {
        private static final long serialVersionUID = 0L;
        private final String algorithmName;
        private final int bytes;
        private final String toString;
        
        private SerializedForm(final String algorithmName, final int bytes, final String toString) {
            this.algorithmName = algorithmName;
            this.bytes = bytes;
            this.toString = toString;
        }
        
        private Object readResolve() {
            return new MessageDigestHashFunction(this.algorithmName, this.bytes, this.toString);
        }
    }
    
    public static final class b extends f
    {
        public final MessageDigest b;
        public final int c;
        public boolean d;
        
        public b(final MessageDigest b, final int c) {
            this.b = b;
            this.c = c;
        }
        
        @Override
        public HashCode e() {
            this.q();
            this.d = true;
            HashCode hashCode;
            if (this.c == this.b.getDigestLength()) {
                hashCode = HashCode.fromBytesNoCopy(this.b.digest());
            }
            else {
                hashCode = HashCode.fromBytesNoCopy(Arrays.copyOf(this.b.digest(), this.c));
            }
            return hashCode;
        }
        
        @Override
        public void m(final byte input) {
            this.q();
            this.b.update(input);
        }
        
        @Override
        public void n(final ByteBuffer input) {
            this.q();
            this.b.update(input);
        }
        
        @Override
        public void p(final byte[] input, final int offset, final int len) {
            this.q();
            this.b.update(input, offset, len);
        }
        
        public final void q() {
            i71.y(this.d ^ true, "Cannot re-use a Hasher after calling hash() on it");
        }
    }
}

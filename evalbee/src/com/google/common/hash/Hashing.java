// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.hash;

import java.util.zip.Adler32;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

public abstract class Hashing
{
    public static final int a;
    
    static {
        a = (int)System.currentTimeMillis();
    }
    
    public static oc0 a() {
        return Murmur3_128HashFunction.MURMUR3_128;
    }
    
    public enum ChecksumType implements me0
    {
        private static final ChecksumType[] $VALUES;
        
        ADLER_32("Hashing.adler32()") {
            @Override
            public Checksum get() {
                return new Adler32();
            }
        }, 
        CRC_32("Hashing.crc32()") {
            @Override
            public Checksum get() {
                return new CRC32();
            }
        };
        
        public final oc0 hashFunction;
        
        private static /* synthetic */ ChecksumType[] $values() {
            return new ChecksumType[] { ChecksumType.CRC_32, ChecksumType.ADLER_32 };
        }
        
        static {
            $VALUES = $values();
        }
        
        private ChecksumType(final String s) {
            this.hashFunction = new ChecksumHashFunction(this, 32, s);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.hash;

import java.io.Serializable;

public interface Funnel<T> extends Serializable
{
    void funnel(final T p0, final c81 p1);
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.hash;

import java.util.Iterator;
import java.io.Serializable;
import java.nio.charset.Charset;

public abstract class Funnels
{
    public static Funnel a(final Charset charset) {
        return new StringCharsetFunnel(charset);
    }
    
    public enum ByteArrayFunnel implements Funnel<byte[]>
    {
        private static final ByteArrayFunnel[] $VALUES;
        
        INSTANCE;
        
        private static /* synthetic */ ByteArrayFunnel[] $values() {
            return new ByteArrayFunnel[] { ByteArrayFunnel.INSTANCE };
        }
        
        static {
            $VALUES = $values();
        }
        
        @Override
        public void funnel(final byte[] array, final c81 c81) {
            c81.f(array);
        }
        
        @Override
        public String toString() {
            return "Funnels.byteArrayFunnel()";
        }
    }
    
    public enum IntegerFunnel implements Funnel<Integer>
    {
        private static final IntegerFunnel[] $VALUES;
        
        INSTANCE;
        
        private static /* synthetic */ IntegerFunnel[] $values() {
            return new IntegerFunnel[] { IntegerFunnel.INSTANCE };
        }
        
        static {
            $VALUES = $values();
        }
        
        @Override
        public void funnel(final Integer n, final c81 c81) {
            c81.a(n);
        }
        
        @Override
        public String toString() {
            return "Funnels.integerFunnel()";
        }
    }
    
    public enum LongFunnel implements Funnel<Long>
    {
        private static final LongFunnel[] $VALUES;
        
        INSTANCE;
        
        private static /* synthetic */ LongFunnel[] $values() {
            return new LongFunnel[] { LongFunnel.INSTANCE };
        }
        
        static {
            $VALUES = $values();
        }
        
        @Override
        public void funnel(final Long n, final c81 c81) {
            c81.b(n);
        }
        
        @Override
        public String toString() {
            return "Funnels.longFunnel()";
        }
    }
    
    public static class SequentialFunnel<E> implements Funnel<Iterable<? extends E>>, Serializable
    {
        private final Funnel<E> elementFunnel;
        
        public SequentialFunnel(final Funnel<E> funnel) {
            this.elementFunnel = (Funnel)i71.r(funnel);
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof SequentialFunnel && this.elementFunnel.equals(((SequentialFunnel)o).elementFunnel);
        }
        
        @Override
        public void funnel(final Iterable<? extends E> iterable, final c81 c81) {
            final Iterator<? extends E> iterator = iterable.iterator();
            while (iterator.hasNext()) {
                this.elementFunnel.funnel((E)iterator.next(), c81);
            }
        }
        
        @Override
        public int hashCode() {
            return SequentialFunnel.class.hashCode() ^ this.elementFunnel.hashCode();
        }
        
        @Override
        public String toString() {
            final String value = String.valueOf(this.elementFunnel);
            final StringBuilder sb = new StringBuilder(value.length() + 26);
            sb.append("Funnels.sequentialFunnel(");
            sb.append(value);
            sb.append(")");
            return sb.toString();
        }
    }
    
    public static class StringCharsetFunnel implements Funnel<CharSequence>, Serializable
    {
        private final Charset charset;
        
        public StringCharsetFunnel(final Charset charset) {
            this.charset = (Charset)i71.r(charset);
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof StringCharsetFunnel && this.charset.equals(((StringCharsetFunnel)o).charset);
        }
        
        @Override
        public void funnel(final CharSequence charSequence, final c81 c81) {
            c81.d(charSequence, this.charset);
        }
        
        @Override
        public int hashCode() {
            return StringCharsetFunnel.class.hashCode() ^ this.charset.hashCode();
        }
        
        @Override
        public String toString() {
            final String name = this.charset.name();
            final StringBuilder sb = new StringBuilder(String.valueOf(name).length() + 22);
            sb.append("Funnels.stringFunnel(");
            sb.append(name);
            sb.append(")");
            return sb.toString();
        }
        
        public Object writeReplace() {
            return new SerializedForm(this.charset);
        }
        
        public static class SerializedForm implements Serializable
        {
            private static final long serialVersionUID = 0L;
            private final String charsetCanonicalName;
            
            public SerializedForm(final Charset charset) {
                this.charsetCanonicalName = charset.name();
            }
            
            private Object readResolve() {
                return Funnels.a(Charset.forName(this.charsetCanonicalName));
            }
        }
    }
    
    public enum UnencodedCharsFunnel implements Funnel<CharSequence>
    {
        private static final UnencodedCharsFunnel[] $VALUES;
        
        INSTANCE;
        
        private static /* synthetic */ UnencodedCharsFunnel[] $values() {
            return new UnencodedCharsFunnel[] { UnencodedCharsFunnel.INSTANCE };
        }
        
        static {
            $VALUES = $values();
        }
        
        @Override
        public void funnel(final CharSequence charSequence, final c81 c81) {
            c81.c(charSequence);
        }
        
        @Override
        public String toString() {
            return "Funnels.unencodedCharsFunnel()";
        }
    }
}

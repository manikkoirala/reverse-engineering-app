// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.hash;

import java.util.Arrays;
import com.google.common.primitives.Ints;
import com.google.common.math.LongMath;
import java.math.RoundingMode;
import java.util.concurrent.atomic.AtomicLongArray;
import com.google.common.primitives.Longs;

enum BloomFilterStrategies implements Strategy
{
    private static final BloomFilterStrategies[] $VALUES;
    
    MURMUR128_MITZ_32 {
        @Override
        public <T> boolean mightContain(final T t, final Funnel<? super T> funnel, final int n, final a a) {
            final long b = a.b();
            final long long1 = Hashing.a().hashObject(t, funnel).asLong();
            final int n2 = (int)long1;
            final int n3 = (int)(long1 >>> 32);
            for (int i = 1; i <= n; ++i) {
                final int n4 = i * n3 + n2;
                int n5;
                if ((n5 = n4) < 0) {
                    n5 = ~n4;
                }
                if (!a.d(n5 % b)) {
                    return false;
                }
            }
            return true;
        }
        
        @Override
        public <T> boolean put(final T t, final Funnel<? super T> funnel, final int n, final a a) {
            final long b = a.b();
            final long long1 = Hashing.a().hashObject(t, funnel).asLong();
            final int n2 = (int)long1;
            final int n3 = (int)(long1 >>> 32);
            boolean b2 = false;
            for (int i = 1; i <= n; ++i) {
                final int n4 = i * n3 + n2;
                int n5;
                if ((n5 = n4) < 0) {
                    n5 = ~n4;
                }
                b2 |= a.g(n5 % b);
            }
            return b2;
        }
    }, 
    MURMUR128_MITZ_64 {
        private long lowerEight(final byte[] array) {
            return Longs.d(array[7], array[6], array[5], array[4], array[3], array[2], array[1], array[0]);
        }
        
        private long upperEight(final byte[] array) {
            return Longs.d(array[15], array[14], array[13], array[12], array[11], array[10], array[9], array[8]);
        }
        
        @Override
        public <T> boolean mightContain(final T t, final Funnel<? super T> funnel, final int n, final a a) {
            final long b = a.b();
            final byte[] bytesInternal = Hashing.a().hashObject(t, funnel).getBytesInternal();
            long lowerEight = this.lowerEight(bytesInternal);
            final long upperEight = this.upperEight(bytesInternal);
            for (int i = 0; i < n; ++i) {
                if (!a.d((Long.MAX_VALUE & lowerEight) % b)) {
                    return false;
                }
                lowerEight += upperEight;
            }
            return true;
        }
        
        @Override
        public <T> boolean put(final T t, final Funnel<? super T> funnel, final int n, final a a) {
            final long b = a.b();
            final byte[] bytesInternal = Hashing.a().hashObject(t, funnel).getBytesInternal();
            long lowerEight = this.lowerEight(bytesInternal);
            final long upperEight = this.upperEight(bytesInternal);
            int i = 0;
            boolean b2 = false;
            while (i < n) {
                b2 |= a.g((Long.MAX_VALUE & lowerEight) % b);
                lowerEight += upperEight;
                ++i;
            }
            return b2;
        }
    };
    
    private static /* synthetic */ BloomFilterStrategies[] $values() {
        return new BloomFilterStrategies[] { BloomFilterStrategies.MURMUR128_MITZ_32, BloomFilterStrategies.MURMUR128_MITZ_64 };
    }
    
    static {
        $VALUES = $values();
    }
    
    public static final class a
    {
        public final AtomicLongArray a;
        public final bm0 b;
        
        public a(final long n) {
            i71.e(n > 0L, "data length is zero!");
            this.a = new AtomicLongArray(Ints.c(LongMath.c(n, 64L, RoundingMode.CEILING)));
            this.b = LongAddables.a();
        }
        
        public a(final long[] array) {
            final int length = array.length;
            int i = 0;
            i71.e(length > 0, "data length is zero!");
            this.a = new AtomicLongArray(array);
            this.b = LongAddables.a();
            final int length2 = array.length;
            long n = 0L;
            while (i < length2) {
                n += Long.bitCount(array[i]);
                ++i;
            }
            this.b.add(n);
        }
        
        public static long[] h(final AtomicLongArray atomicLongArray) {
            final int length = atomicLongArray.length();
            final long[] array = new long[length];
            for (int i = 0; i < length; ++i) {
                array[i] = atomicLongArray.get(i);
            }
            return array;
        }
        
        public long a() {
            return this.b.sum();
        }
        
        public long b() {
            return this.a.length() * 64L;
        }
        
        public a c() {
            return new a(h(this.a));
        }
        
        public boolean d(final long n) {
            return (1L << (int)n & this.a.get((int)(n >>> 6))) != 0x0L;
        }
        
        public void e(final a a) {
            final int length = this.a.length();
            final int length2 = a.a.length();
            int i = 0;
            i71.i(length == length2, "BitArrays must be of equal length (%s != %s)", this.a.length(), a.a.length());
            while (i < this.a.length()) {
                this.f(i, a.a.get(i));
                ++i;
            }
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof a && Arrays.equals(h(this.a), h(((a)o).a));
        }
        
        public void f(int bitCount, final long n) {
            while (true) {
                long value;
                long n2;
                do {
                    value = this.a.get(bitCount);
                    n2 = (value | n);
                    if (value == n2) {
                        bitCount = 0;
                        if (bitCount != 0) {
                            bitCount = Long.bitCount(n2);
                            this.b.add(bitCount - Long.bitCount(value));
                        }
                        return;
                    }
                } while (!this.a.compareAndSet(bitCount, value, n2));
                bitCount = 1;
                continue;
            }
        }
        
        public boolean g(long newValue) {
            if (this.d(newValue)) {
                return false;
            }
            final int n = (int)(newValue >>> 6);
            final int n2 = (int)newValue;
            long value;
            do {
                value = this.a.get(n);
                newValue = (value | 1L << n2);
                if (value == newValue) {
                    return false;
                }
            } while (!this.a.compareAndSet(n, value, newValue));
            this.b.increment();
            return true;
        }
        
        @Override
        public int hashCode() {
            return Arrays.hashCode(h(this.a));
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.hash;

import com.google.common.primitives.SignedBytes;
import java.io.DataOutputStream;
import java.io.OutputStream;
import java.math.RoundingMode;
import java.io.IOException;
import com.google.common.math.LongMath;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.Serializable;

public final class BloomFilter<T> implements m71, Serializable
{
    private final BloomFilterStrategies.a bits;
    private final Funnel<? super T> funnel;
    private final int numHashFunctions;
    private final Strategy strategy;
    
    private BloomFilter(final BloomFilterStrategies.a a, final int numHashFunctions, final Funnel<? super T> funnel, final Strategy strategy) {
        final boolean b = true;
        i71.h(numHashFunctions > 0, "numHashFunctions (%s) must be > 0", numHashFunctions);
        i71.h(numHashFunctions <= 255 && b, "numHashFunctions (%s) must be <= 255", numHashFunctions);
        this.bits = (BloomFilterStrategies.a)i71.r(a);
        this.numHashFunctions = numHashFunctions;
        this.funnel = (Funnel)i71.r(funnel);
        this.strategy = (Strategy)i71.r(strategy);
    }
    
    public static /* synthetic */ BloomFilterStrategies.a access$000(final BloomFilter bloomFilter) {
        return bloomFilter.bits;
    }
    
    public static /* synthetic */ int access$100(final BloomFilter bloomFilter) {
        return bloomFilter.numHashFunctions;
    }
    
    public static /* synthetic */ Funnel access$200(final BloomFilter bloomFilter) {
        return bloomFilter.funnel;
    }
    
    public static /* synthetic */ Strategy access$300(final BloomFilter bloomFilter) {
        return bloomFilter.strategy;
    }
    
    public static <T> BloomFilter<T> create(final Funnel<? super T> funnel, final int n) {
        return create(funnel, (long)n);
    }
    
    public static <T> BloomFilter<T> create(final Funnel<? super T> funnel, final int n, final double n2) {
        return create(funnel, (long)n, n2);
    }
    
    public static <T> BloomFilter<T> create(final Funnel<? super T> funnel, final long n) {
        return create(funnel, n, 0.03);
    }
    
    public static <T> BloomFilter<T> create(final Funnel<? super T> funnel, final long n, final double n2) {
        return create(funnel, n, n2, (Strategy)BloomFilterStrategies.MURMUR128_MITZ_64);
    }
    
    public static <T> BloomFilter<T> create(final Funnel<? super T> funnel, long n, final double n2, final Strategy strategy) {
        i71.r(funnel);
        final long n3 = lcmp(n, 0L);
        final boolean b = true;
        i71.j(n3 >= 0, "Expected insertions (%s) must be >= 0", n);
        i71.m(n2 > 0.0, "False positive probability (%s) must be > 0.0", n2);
        i71.m(n2 < 1.0 && b, "False positive probability (%s) must be < 1.0", n2);
        i71.r(strategy);
        if (n3 == 0) {
            n = 1L;
        }
        final long optimalNumOfBits = optimalNumOfBits(n, n2);
        final int optimalNumOfHashFunctions = optimalNumOfHashFunctions(n, optimalNumOfBits);
        try {
            return new BloomFilter<T>(new BloomFilterStrategies.a(optimalNumOfBits), optimalNumOfHashFunctions, (Funnel<? super Object>)funnel, strategy);
        }
        catch (final IllegalArgumentException cause) {
            final StringBuilder sb = new StringBuilder(57);
            sb.append("Could not create BloomFilter of ");
            sb.append(optimalNumOfBits);
            sb.append(" bits");
            throw new IllegalArgumentException(sb.toString(), cause);
        }
    }
    
    public static long optimalNumOfBits(final long n, final double n2) {
        double a = n2;
        if (n2 == 0.0) {
            a = Double.MIN_VALUE;
        }
        return (long)(-n * Math.log(a) / (Math.log(2.0) * Math.log(2.0)));
    }
    
    public static int optimalNumOfHashFunctions(final long n, final long n2) {
        return Math.max(1, (int)Math.round(n2 / (double)n * Math.log(2.0)));
    }
    
    public static <T> BloomFilter<T> readFrom(final InputStream in, final Funnel<? super T> funnel) {
        i71.s(in, "InputStream");
        i71.s(funnel, "Funnel");
        int int1 = -1;
        int byte1;
        int j;
        try {
            final DataInputStream dataInputStream = new DataInputStream(in);
            byte1 = dataInputStream.readByte();
            try {
                final int c = f12.c(dataInputStream.readByte());
                try {
                    final int n = int1 = dataInputStream.readInt();
                    final BloomFilterStrategies bloomFilterStrategies = BloomFilterStrategies.values()[byte1];
                    int1 = n;
                    int1 = n;
                    final BloomFilterStrategies.a a = new BloomFilterStrategies.a(LongMath.b(n, 64L));
                    for (int i = 0; i < n; ++i) {
                        int1 = n;
                        a.f(i, dataInputStream.readLong());
                    }
                    int1 = n;
                    return new BloomFilter<T>(a, c, (Funnel<? super Object>)funnel, (Strategy)bloomFilterStrategies);
                }
                catch (final RuntimeException cause) {
                    j = int1;
                    int1 = byte1;
                    byte1 = c;
                }
            }
            catch (final RuntimeException cause) {
                final int n2 = -1;
                int1 = byte1;
                final int n3 = -1;
                byte1 = n2;
                j = n3;
            }
        }
        catch (final RuntimeException cause) {
            j = -1;
            byte1 = -1;
        }
        final StringBuilder sb = new StringBuilder(134);
        sb.append("Unable to deserialize BloomFilter from InputStream. strategyOrdinal: ");
        sb.append(int1);
        sb.append(" numHashFunctions: ");
        sb.append(byte1);
        sb.append(" dataLength: ");
        sb.append(j);
        final RuntimeException cause;
        throw new IOException(sb.toString(), cause);
    }
    
    private Object writeReplace() {
        return new SerialForm((BloomFilter<Object>)this);
    }
    
    @Deprecated
    @Override
    public boolean apply(final T t) {
        return this.mightContain(t);
    }
    
    public long approximateElementCount() {
        final long b = this.bits.b();
        final double n = (double)this.bits.a();
        final double n2 = (double)b;
        return pu.c(-Math.log1p(-(n / n2)) * n2 / this.numHashFunctions, RoundingMode.HALF_UP);
    }
    
    public long bitSize() {
        return this.bits.b();
    }
    
    public BloomFilter<T> copy() {
        return new BloomFilter<T>(this.bits.c(), this.numHashFunctions, this.funnel, this.strategy);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof BloomFilter) {
            final BloomFilter bloomFilter = (BloomFilter)o;
            if (this.numHashFunctions != bloomFilter.numHashFunctions || !this.funnel.equals(bloomFilter.funnel) || !this.bits.equals(bloomFilter.bits) || !this.strategy.equals(bloomFilter.strategy)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    public double expectedFpp() {
        return Math.pow(this.bits.a() / (double)this.bitSize(), this.numHashFunctions);
    }
    
    @Override
    public int hashCode() {
        return b11.b(this.numHashFunctions, this.funnel, this.strategy, this.bits);
    }
    
    public boolean isCompatible(final BloomFilter<T> bloomFilter) {
        i71.r(bloomFilter);
        return this != bloomFilter && this.numHashFunctions == bloomFilter.numHashFunctions && this.bitSize() == bloomFilter.bitSize() && this.strategy.equals(bloomFilter.strategy) && this.funnel.equals(bloomFilter.funnel);
    }
    
    public boolean mightContain(final T t) {
        return this.strategy.mightContain(t, this.funnel, this.numHashFunctions, this.bits);
    }
    
    public boolean put(final T t) {
        return this.strategy.put(t, this.funnel, this.numHashFunctions, this.bits);
    }
    
    public void putAll(final BloomFilter<T> bloomFilter) {
        i71.r(bloomFilter);
        i71.e(this != bloomFilter, "Cannot combine a BloomFilter with itself.");
        final int numHashFunctions = this.numHashFunctions;
        final int numHashFunctions2 = bloomFilter.numHashFunctions;
        i71.i(numHashFunctions == numHashFunctions2, "BloomFilters must have the same number of hash functions (%s != %s)", numHashFunctions, numHashFunctions2);
        i71.k(this.bitSize() == bloomFilter.bitSize(), "BloomFilters must have the same size underlying bit arrays (%s != %s)", this.bitSize(), bloomFilter.bitSize());
        i71.n(this.strategy.equals(bloomFilter.strategy), "BloomFilters must have equal strategies (%s != %s)", this.strategy, bloomFilter.strategy);
        i71.n(this.funnel.equals(bloomFilter.funnel), "BloomFilters must have equal funnels (%s != %s)", this.funnel, bloomFilter.funnel);
        this.bits.e(bloomFilter.bits);
    }
    
    public void writeTo(final OutputStream out) {
        final DataOutputStream dataOutputStream = new DataOutputStream(out);
        dataOutputStream.writeByte(SignedBytes.a(this.strategy.ordinal()));
        dataOutputStream.writeByte(f12.a(this.numHashFunctions));
        dataOutputStream.writeInt(this.bits.a.length());
        for (int i = 0; i < this.bits.a.length(); ++i) {
            dataOutputStream.writeLong(this.bits.a.get(i));
        }
    }
    
    public static class SerialForm<T> implements Serializable
    {
        private static final long serialVersionUID = 1L;
        final long[] data;
        final Funnel<? super T> funnel;
        final int numHashFunctions;
        final Strategy strategy;
        
        public SerialForm(final BloomFilter<T> bloomFilter) {
            this.data = BloomFilterStrategies.a.h(BloomFilter.access$000((BloomFilter<Object>)bloomFilter).a);
            this.numHashFunctions = BloomFilter.access$100((BloomFilter<Object>)bloomFilter);
            this.funnel = BloomFilter.access$200((BloomFilter<Object>)bloomFilter);
            this.strategy = BloomFilter.access$300((BloomFilter<Object>)bloomFilter);
        }
        
        public Object readResolve() {
            return new BloomFilter(new BloomFilterStrategies.a(this.data), this.numHashFunctions, this.funnel, this.strategy, null);
        }
    }
    
    public interface Strategy extends Serializable
    {
         <T> boolean mightContain(final T p0, final Funnel<? super T> p1, final int p2, final BloomFilterStrategies.a p3);
        
        int ordinal();
        
         <T> boolean put(final T p0, final Funnel<? super T> p1, final int p2, final BloomFilterStrategies.a p3);
    }
}

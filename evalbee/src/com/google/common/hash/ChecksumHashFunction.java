// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.hash;

import java.util.zip.Checksum;
import java.io.Serializable;

final class ChecksumHashFunction extends m implements Serializable
{
    private static final long serialVersionUID = 0L;
    private final int bits;
    private final me0 checksumSupplier;
    private final String toString;
    
    public ChecksumHashFunction(final me0 me0, final int bits, final String s) {
        this.checksumSupplier = (me0)i71.r(me0);
        i71.h(bits == 32 || bits == 64, "bits (%s) must be either 32 or 64", bits);
        this.bits = bits;
        this.toString = (String)i71.r(s);
    }
    
    public static /* synthetic */ int access$100(final ChecksumHashFunction checksumHashFunction) {
        return checksumHashFunction.bits;
    }
    
    public int bits() {
        return this.bits;
    }
    
    @Override
    public qc0 newHasher() {
        return new b((Checksum)this.checksumSupplier.get(), null);
    }
    
    @Override
    public String toString() {
        return this.toString;
    }
    
    public final class b extends f
    {
        public final Checksum b;
        public final ChecksumHashFunction c;
        
        public b(final ChecksumHashFunction c, final Checksum checksum) {
            this.c = c;
            this.b = (Checksum)i71.r(checksum);
        }
        
        @Override
        public HashCode e() {
            final long value = this.b.getValue();
            if (ChecksumHashFunction.access$100(this.c) == 32) {
                return HashCode.fromInt((int)value);
            }
            return HashCode.fromLong(value);
        }
        
        @Override
        public void m(final byte b) {
            this.b.update(b);
        }
        
        @Override
        public void p(final byte[] array, final int n, final int n2) {
            this.b.update(array, n, n2);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.hash;

import java.nio.ByteBuffer;
import java.io.Serializable;

final class SipHashFunction extends m implements Serializable
{
    static final oc0 SIP_HASH_24;
    private static final long serialVersionUID = 0L;
    private final int c;
    private final int d;
    private final long k0;
    private final long k1;
    
    static {
        SIP_HASH_24 = new SipHashFunction(2, 4, 506097522914230528L, 1084818905618843912L);
    }
    
    public SipHashFunction(final int c, final int d, final long k0, final long k2) {
        final boolean b = true;
        i71.h(c > 0, "The number of SipRound iterations (c=%s) during Compression must be positive.", c);
        i71.h(d > 0 && b, "The number of SipRound iterations (d=%s) during Finalization must be positive.", d);
        this.c = c;
        this.d = d;
        this.k0 = k0;
        this.k1 = k2;
    }
    
    public int bits() {
        return 64;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof SipHashFunction;
        boolean b3;
        final boolean b2 = b3 = false;
        if (b) {
            final SipHashFunction sipHashFunction = (SipHashFunction)o;
            b3 = b2;
            if (this.c == sipHashFunction.c) {
                b3 = b2;
                if (this.d == sipHashFunction.d) {
                    b3 = b2;
                    if (this.k0 == sipHashFunction.k0) {
                        b3 = b2;
                        if (this.k1 == sipHashFunction.k1) {
                            b3 = true;
                        }
                    }
                }
            }
        }
        return b3;
    }
    
    @Override
    public int hashCode() {
        return (int)((long)(SipHashFunction.class.hashCode() ^ this.c ^ this.d) ^ this.k0 ^ this.k1);
    }
    
    @Override
    public qc0 newHasher() {
        return new a(this.c, this.d, this.k0, this.k1);
    }
    
    @Override
    public String toString() {
        final int c = this.c;
        final int d = this.d;
        final long k0 = this.k0;
        final long k2 = this.k1;
        final StringBuilder sb = new StringBuilder(81);
        sb.append("Hashing.sipHash");
        sb.append(c);
        sb.append(d);
        sb.append("(");
        sb.append(k0);
        sb.append(", ");
        sb.append(k2);
        sb.append(")");
        return sb.toString();
    }
    
    public static final class a extends l0
    {
        public final int d;
        public final int e;
        public long f;
        public long g;
        public long h;
        public long i;
        public long j;
        public long k;
        
        public a(final int d, final int e, final long n, final long n2) {
            super(8);
            this.j = 0L;
            this.k = 0L;
            this.d = d;
            this.e = e;
            this.f = (0x736F6D6570736575L ^ n);
            this.g = (0x646F72616E646F6DL ^ n2);
            this.h = (0x6C7967656E657261L ^ n);
            this.i = (0x7465646279746573L ^ n2);
        }
        
        @Override
        public HashCode l() {
            this.r(this.k ^= this.j << 56);
            this.h ^= 0xFFL;
            this.s(this.e);
            return HashCode.fromLong(this.f ^ this.g ^ this.h ^ this.i);
        }
        
        @Override
        public void o(final ByteBuffer byteBuffer) {
            this.j += 8L;
            this.r(byteBuffer.getLong());
        }
        
        @Override
        public void p(final ByteBuffer byteBuffer) {
            this.j += byteBuffer.remaining();
            int n = 0;
            while (byteBuffer.hasRemaining()) {
                this.k ^= ((long)byteBuffer.get() & 0xFFL) << n;
                n += 8;
            }
        }
        
        public final void r(final long n) {
            this.i ^= n;
            this.s(this.d);
            this.f ^= n;
        }
        
        public final void s(final int n) {
            for (int i = 0; i < n; ++i) {
                final long f = this.f;
                final long g = this.g;
                this.f = f + g;
                this.h += this.i;
                this.g = Long.rotateLeft(g, 13);
                final long rotateLeft = Long.rotateLeft(this.i, 16);
                final long g2 = this.g;
                final long f2 = this.f;
                this.g = (g2 ^ f2);
                this.i = (rotateLeft ^ this.h);
                final long rotateLeft2 = Long.rotateLeft(f2, 32);
                final long h = this.h;
                final long g3 = this.g;
                this.h = h + g3;
                this.f = rotateLeft2 + this.i;
                this.g = Long.rotateLeft(g3, 17);
                final long rotateLeft3 = Long.rotateLeft(this.i, 21);
                final long g4 = this.g;
                final long h2 = this.h;
                this.g = (g4 ^ h2);
                this.i = (rotateLeft3 ^ this.f);
                this.h = Long.rotateLeft(h2, 32);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.hash;

import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;

final class LongAdder extends Striped64 implements Serializable, bm0
{
    private static final long serialVersionUID = 7249069246863182397L;
    
    public LongAdder() {
    }
    
    private void readObject(final ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        super.busy = 0;
        super.cells = null;
        super.base = objectInputStream.readLong();
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeLong(this.sum());
    }
    
    @Override
    public void add(final long n) {
        final b[] cells = super.cells;
        if (cells == null) {
            final long base = super.base;
            if (this.casBase(base, base + n)) {
                return;
            }
        }
        final int[] array = Striped64.threadHashCode.get();
        boolean a;
        final boolean b = a = true;
        if (array != null) {
            a = b;
            if (cells != null) {
                final int length = cells.length;
                a = b;
                if (length >= 1) {
                    final b b2 = cells[length - 1 & array[0]];
                    a = b;
                    if (b2 != null) {
                        final long a2 = b2.a;
                        a = b2.a(a2, a2 + n);
                        if (a) {
                            return;
                        }
                    }
                }
            }
        }
        this.retryUpdate(n, array, a);
    }
    
    public void decrement() {
        this.add(-1L);
    }
    
    @Override
    public double doubleValue() {
        return (double)this.sum();
    }
    
    @Override
    public float floatValue() {
        return (float)this.sum();
    }
    
    public final long fn(final long n, final long n2) {
        return n + n2;
    }
    
    @Override
    public void increment() {
        this.add(1L);
    }
    
    @Override
    public int intValue() {
        return (int)this.sum();
    }
    
    @Override
    public long longValue() {
        return this.sum();
    }
    
    public void reset() {
        this.internalReset(0L);
    }
    
    @Override
    public long sum() {
        long base = super.base;
        final b[] cells = super.cells;
        long n = base;
        if (cells != null) {
            final int length = cells.length;
            int n2 = 0;
            while (true) {
                n = base;
                if (n2 >= length) {
                    break;
                }
                final b b = cells[n2];
                long n3 = base;
                if (b != null) {
                    n3 = base + b.a;
                }
                ++n2;
                base = n3;
            }
        }
        return n;
    }
    
    public long sumThenReset() {
        long base = super.base;
        final b[] cells = super.cells;
        super.base = 0L;
        long n = base;
        if (cells != null) {
            final int length = cells.length;
            int n2 = 0;
            while (true) {
                n = base;
                if (n2 >= length) {
                    break;
                }
                final b b = cells[n2];
                long n3 = base;
                if (b != null) {
                    n3 = base + b.a;
                    b.a = 0L;
                }
                ++n2;
                base = n3;
            }
        }
        return n;
    }
    
    @Override
    public String toString() {
        return Long.toString(this.sum());
    }
}

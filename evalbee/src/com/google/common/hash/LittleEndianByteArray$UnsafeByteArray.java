// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.hash;

import java.security.PrivilegedActionException;
import java.security.AccessController;
import java.lang.reflect.Field;
import java.security.PrivilegedExceptionAction;
import sun.misc.Unsafe;

enum LittleEndianByteArray$UnsafeByteArray
{
    private static final LittleEndianByteArray$UnsafeByteArray[] $VALUES;
    private static final int BYTE_ARRAY_BASE_OFFSET;
    
    UNSAFE_BIG_ENDIAN {
        @Override
        public long getLongLittleEndian(final byte[] o, final int n) {
            return Long.reverseBytes(LittleEndianByteArray$UnsafeByteArray.access$200().getLong(o, n + (long)LittleEndianByteArray$UnsafeByteArray.access$100()));
        }
        
        @Override
        public void putLongLittleEndian(final byte[] o, final int n, long reverseBytes) {
            reverseBytes = Long.reverseBytes(reverseBytes);
            LittleEndianByteArray$UnsafeByteArray.access$200().putLong(o, n + (long)LittleEndianByteArray$UnsafeByteArray.access$100(), reverseBytes);
        }
    }, 
    UNSAFE_LITTLE_ENDIAN {
        @Override
        public long getLongLittleEndian(final byte[] o, final int n) {
            return LittleEndianByteArray$UnsafeByteArray.access$200().getLong(o, n + (long)LittleEndianByteArray$UnsafeByteArray.access$100());
        }
        
        @Override
        public void putLongLittleEndian(final byte[] o, final int n, final long x) {
            LittleEndianByteArray$UnsafeByteArray.access$200().putLong(o, n + (long)LittleEndianByteArray$UnsafeByteArray.access$100(), x);
        }
    };
    
    private static final Unsafe theUnsafe;
    
    private static /* synthetic */ LittleEndianByteArray$UnsafeByteArray[] $values() {
        return new LittleEndianByteArray$UnsafeByteArray[] { LittleEndianByteArray$UnsafeByteArray.UNSAFE_LITTLE_ENDIAN, LittleEndianByteArray$UnsafeByteArray.UNSAFE_BIG_ENDIAN };
    }
    
    static {
        $VALUES = $values();
        final Unsafe unsafe = theUnsafe = getUnsafe();
        BYTE_ARRAY_BASE_OFFSET = unsafe.arrayBaseOffset(byte[].class);
        if (unsafe.arrayIndexScale(byte[].class) == 1) {
            return;
        }
        throw new AssertionError();
    }
    
    public static /* synthetic */ int access$100() {
        return LittleEndianByteArray$UnsafeByteArray.BYTE_ARRAY_BASE_OFFSET;
    }
    
    public static /* synthetic */ Unsafe access$200() {
        return LittleEndianByteArray$UnsafeByteArray.theUnsafe;
    }
    
    private static Unsafe getUnsafe() {
        try {
            return Unsafe.getUnsafe();
        }
        catch (final SecurityException ex) {
            try {
                return AccessController.doPrivileged((PrivilegedExceptionAction<Unsafe>)new PrivilegedExceptionAction() {
                    public Unsafe a() {
                        for (final Field field : Unsafe.class.getDeclaredFields()) {
                            field.setAccessible(true);
                            final Object value = field.get(null);
                            if (Unsafe.class.isInstance(value)) {
                                return Unsafe.class.cast(value);
                            }
                        }
                        throw new NoSuchFieldError("the Unsafe");
                    }
                });
            }
            catch (final PrivilegedActionException ex2) {
                throw new RuntimeException("Could not initialize intrinsics", ex2.getCause());
            }
        }
    }
}

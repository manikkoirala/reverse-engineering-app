// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.hash;

import java.nio.ByteOrder;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import com.google.common.primitives.Ints;
import java.io.Serializable;

final class Murmur3_32HashFunction extends m implements Serializable
{
    private static final int C1 = -862048943;
    private static final int C2 = 461845907;
    private static final int CHUNK_SIZE = 4;
    static final oc0 GOOD_FAST_HASH_32;
    static final oc0 MURMUR3_32;
    static final oc0 MURMUR3_32_FIXED;
    private static final long serialVersionUID = 0L;
    private final int seed;
    private final boolean supplementaryPlaneFix;
    
    static {
        MURMUR3_32 = new Murmur3_32HashFunction(0, false);
        MURMUR3_32_FIXED = new Murmur3_32HashFunction(0, true);
        GOOD_FAST_HASH_32 = new Murmur3_32HashFunction(Hashing.a, true);
    }
    
    public Murmur3_32HashFunction(final int seed, final boolean supplementaryPlaneFix) {
        this.seed = seed;
        this.supplementaryPlaneFix = supplementaryPlaneFix;
    }
    
    private static long charToThreeUtf8Bytes(final char c) {
        return (long)(c >>> 12) | 0xE0L | (long)(((c >>> 6 & 0x3F) | 0x80) << 8) | (long)(((c & '?') | 0x80) << 16);
    }
    
    private static long charToTwoUtf8Bytes(final char c) {
        return (long)(c >>> 6) | 0xC0L | (long)(((c & '?') | 0x80) << 8);
    }
    
    private static long codePointToFourUtf8Bytes(final int n) {
        return (long)(n >>> 18) | 0xF0L | ((long)(n >>> 12 & 0x3F) | 0x80L) << 8 | ((long)(n >>> 6 & 0x3F) | 0x80L) << 16 | ((long)(n & 0x3F) | 0x80L) << 24;
    }
    
    private static HashCode fmix(int n, final int n2) {
        n ^= n2;
        n = (n ^ n >>> 16) * -2048144789;
        n = (n ^ n >>> 13) * -1028477387;
        return HashCode.fromInt(n ^ n >>> 16);
    }
    
    private static int getIntLittleEndian(final byte[] array, final int n) {
        return Ints.f(array[n + 3], array[n + 2], array[n + 1], array[n]);
    }
    
    private static int mixH1(final int n, final int n2) {
        return Integer.rotateLeft(n ^ n2, 13) * 5 - 430675100;
    }
    
    private static int mixK1(final int n) {
        return Integer.rotateLeft(n * -862048943, 15) * 461845907;
    }
    
    public int bits() {
        return 32;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof Murmur3_32HashFunction;
        boolean b3;
        final boolean b2 = b3 = false;
        if (b) {
            final Murmur3_32HashFunction murmur3_32HashFunction = (Murmur3_32HashFunction)o;
            b3 = b2;
            if (this.seed == murmur3_32HashFunction.seed) {
                b3 = b2;
                if (this.supplementaryPlaneFix == murmur3_32HashFunction.supplementaryPlaneFix) {
                    b3 = true;
                }
            }
        }
        return b3;
    }
    
    @Override
    public HashCode hashBytes(final byte[] array, final int n, final int n2) {
        i71.w(n, n + n2, array.length);
        int n3 = this.seed;
        int n4 = 0;
        int n5 = 0;
        while (true) {
            final int n6 = n5 + 4;
            if (n6 > n2) {
                break;
            }
            n3 = mixH1(n3, mixK1(getIntLittleEndian(array, n5 + n)));
            n5 = n6;
        }
        final int n7 = 0;
        for (int i = n5, n8 = n7; i < n2; ++i, n8 += 8) {
            n4 ^= f12.c(array[n + i]) << n8;
        }
        return fmix(mixK1(n4) ^ n3, n2);
    }
    
    @Override
    public int hashCode() {
        return Murmur3_32HashFunction.class.hashCode() ^ this.seed;
    }
    
    @Override
    public HashCode hashInt(int mixK1) {
        mixK1 = mixK1(mixK1);
        return fmix(mixH1(this.seed, mixK1), 4);
    }
    
    @Override
    public HashCode hashLong(final long n) {
        return fmix(mixH1(mixH1(this.seed, mixK1((int)n)), mixK1((int)(n >>> 32))), 8);
    }
    
    @Override
    public HashCode hashString(final CharSequence seq, final Charset charset) {
        if (kg.c.equals(charset)) {
            final int length = seq.length();
            int n = this.seed;
            final int n2 = 0;
            int i = 0;
            int n3 = 0;
            while (true) {
                final int n4 = i + 4;
                if (n4 > length) {
                    break;
                }
                final char char1 = seq.charAt(i);
                final char char2 = seq.charAt(i + 1);
                final char char3 = seq.charAt(i + 2);
                final char char4 = seq.charAt(i + 3);
                if (char1 >= '\u0080' || char2 >= '\u0080' || char3 >= '\u0080' || char4 >= '\u0080') {
                    break;
                }
                n = mixH1(n, mixK1(char2 << 8 | char1 | char3 << 16 | char4 << 24));
                n3 += 4;
                i = n4;
            }
            long n5 = 0L;
            int n6 = n3;
            int n7 = n2;
            int n8 = n;
            while (i < length) {
                final char char5 = seq.charAt(i);
                long n9;
                int n10;
                int n11;
                if (char5 < '\u0080') {
                    n9 = (n5 | (long)char5 << n7);
                    n7 += 8;
                    n10 = n6 + 1;
                    n11 = i;
                }
                else if (char5 < '\u0800') {
                    n9 = (n5 | charToTwoUtf8Bytes(char5) << n7);
                    n7 += 16;
                    n10 = n6 + 2;
                    n11 = i;
                }
                else if (char5 >= '\ud800' && char5 <= '\udfff') {
                    final int codePoint = Character.codePointAt(seq, i);
                    if (codePoint == char5) {
                        return this.hashBytes(seq.toString().getBytes(charset));
                    }
                    ++i;
                    n9 = (n5 | codePointToFourUtf8Bytes(codePoint) << n7);
                    int n12 = n7;
                    if (this.supplementaryPlaneFix) {
                        n12 = n7 + 32;
                    }
                    final int n13 = n6 + 4;
                    n7 = n12;
                    n11 = i;
                    n10 = n13;
                }
                else {
                    n9 = (n5 | charToThreeUtf8Bytes(char5) << n7);
                    n7 += 24;
                    n10 = n6 + 3;
                    n11 = i;
                }
                int mixH1 = n8;
                int n14 = n7;
                n5 = n9;
                if (n7 >= 32) {
                    mixH1 = mixH1(n8, mixK1((int)n9));
                    n5 = n9 >>> 32;
                    n14 = n7 - 32;
                }
                ++n11;
                n8 = mixH1;
                n7 = n14;
                i = n11;
                n6 = n10;
            }
            return fmix(mixK1((int)n5) ^ n8, n6);
        }
        return this.hashBytes(seq.toString().getBytes(charset));
    }
    
    @Override
    public HashCode hashUnencodedChars(final CharSequence charSequence) {
        int n = this.seed;
        for (int i = 1; i < charSequence.length(); i += 2) {
            n = mixH1(n, mixK1(charSequence.charAt(i - 1) | charSequence.charAt(i) << 16));
        }
        int n2 = n;
        if ((charSequence.length() & 0x1) == 0x1) {
            n2 = (n ^ mixK1(charSequence.charAt(charSequence.length() - 1)));
        }
        return fmix(n2, charSequence.length() * 2);
    }
    
    @Override
    public qc0 newHasher() {
        return new a(this.seed);
    }
    
    @Override
    public String toString() {
        final int seed = this.seed;
        final StringBuilder sb = new StringBuilder(31);
        sb.append("Hashing.murmur3_32(");
        sb.append(seed);
        sb.append(")");
        return sb.toString();
    }
    
    public static final class a extends n
    {
        public int a;
        public long b;
        public int c;
        public int d;
        public boolean e;
        
        public a(final int a) {
            this.a = a;
            this.d = 0;
            this.e = false;
        }
        
        @Override
        public qc0 a(final int n) {
            this.m(4, n);
            return this;
        }
        
        @Override
        public qc0 b(final long n) {
            this.m(4, (int)n);
            this.m(4, n >>> 32);
            return this;
        }
        
        @Override
        public qc0 d(final CharSequence seq, final Charset charset) {
            if (kg.c.equals(charset)) {
                final int length = seq.length();
                int n = 0;
                int i;
                while (true) {
                    final int n2 = n + 4;
                    i = n;
                    if (n2 > length) {
                        break;
                    }
                    final char char1 = seq.charAt(n);
                    final char char2 = seq.charAt(n + 1);
                    final char char3 = seq.charAt(n + 2);
                    final char char4 = seq.charAt(n + 3);
                    i = n;
                    if (char1 >= '\u0080') {
                        break;
                    }
                    i = n;
                    if (char2 >= '\u0080') {
                        break;
                    }
                    i = n;
                    if (char3 >= '\u0080') {
                        break;
                    }
                    i = n;
                    if (char4 >= '\u0080') {
                        break;
                    }
                    this.m(4, char2 << 8 | char1 | char3 << 16 | char4 << 24);
                    n = n2;
                }
                while (i < length) {
                    final char char5 = seq.charAt(i);
                    Label_0308: {
                        if (char5 < '\u0080') {
                            this.m(1, char5);
                        }
                        else {
                            long n3;
                            int n4;
                            if (char5 < '\u0800') {
                                n3 = charToTwoUtf8Bytes(char5);
                                n4 = 2;
                            }
                            else if (char5 >= '\ud800' && char5 <= '\udfff') {
                                final int codePoint = Character.codePointAt(seq, i);
                                if (codePoint == char5) {
                                    this.j(seq.subSequence(i, length).toString().getBytes(charset));
                                    return this;
                                }
                                ++i;
                                this.m(4, codePointToFourUtf8Bytes(codePoint));
                                break Label_0308;
                            }
                            else {
                                n3 = charToThreeUtf8Bytes(char5);
                                n4 = 3;
                            }
                            this.m(n4, n3);
                        }
                    }
                    ++i;
                }
                return this;
            }
            return super.d(seq, charset);
        }
        
        @Override
        public HashCode e() {
            i71.x(this.e ^ true);
            this.e = true;
            final int a = this.a ^ mixK1((int)this.b);
            this.a = a;
            return fmix(a, this.d);
        }
        
        @Override
        public qc0 h(final byte[] array, final int n, final int n2) {
            i71.w(n, n + n2, array.length);
            int n3 = 0;
            int i;
            while (true) {
                final int n4 = n3 + 4;
                i = n3;
                if (n4 > n2) {
                    break;
                }
                this.m(4, getIntLittleEndian(array, n3 + n));
                n3 = n4;
            }
            while (i < n2) {
                this.l(array[n + i]);
                ++i;
            }
            return this;
        }
        
        @Override
        public qc0 i(final ByteBuffer byteBuffer) {
            final ByteOrder order = byteBuffer.order();
            byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
            while (byteBuffer.remaining() >= 4) {
                this.a(byteBuffer.getInt());
            }
            while (byteBuffer.hasRemaining()) {
                this.l(byteBuffer.get());
            }
            byteBuffer.order(order);
            return this;
        }
        
        @Override
        public qc0 k(final char c) {
            this.m(2, c);
            return this;
        }
        
        public qc0 l(final byte b) {
            this.m(1, b & 0xFF);
            return this;
        }
        
        public final void m(final int n, long b) {
            final long b2 = this.b;
            final int c = this.c;
            b = ((b & 0xFFFFFFFFL) << c | b2);
            this.b = b;
            final int c2 = c + n * 8;
            this.c = c2;
            this.d += n;
            if (c2 >= 32) {
                this.a = mixH1(this.a, mixK1((int)b));
                this.b >>>= 32;
                this.c -= 32;
            }
        }
    }
}

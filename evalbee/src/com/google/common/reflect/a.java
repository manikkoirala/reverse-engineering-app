// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.reflect;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.lang.reflect.Type;
import java.lang.reflect.Constructor;
import java.lang.annotation.Annotation;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Member;
import java.lang.reflect.AnnotatedElement;

public abstract class a implements AnnotatedElement, Member
{
    public final AccessibleObject a;
    public final Member b;
    
    public a(final AccessibleObject a) {
        i71.r(a);
        this.a = a;
        this.b = (Member)a;
    }
    
    public abstract TypeToken a();
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof a;
        boolean b3;
        final boolean b2 = b3 = false;
        if (b) {
            final a a = (a)o;
            b3 = b2;
            if (this.a().equals(a.a())) {
                b3 = b2;
                if (this.b.equals(a.b)) {
                    b3 = true;
                }
            }
        }
        return b3;
    }
    
    @Override
    public final Annotation getAnnotation(final Class annotationClass) {
        return this.a.getAnnotation((Class<Annotation>)annotationClass);
    }
    
    @Override
    public final Annotation[] getAnnotations() {
        return this.a.getAnnotations();
    }
    
    @Override
    public final Annotation[] getDeclaredAnnotations() {
        return this.a.getDeclaredAnnotations();
    }
    
    @Override
    public final Class getDeclaringClass() {
        return this.b.getDeclaringClass();
    }
    
    @Override
    public final int getModifiers() {
        return this.b.getModifiers();
    }
    
    @Override
    public final String getName() {
        return this.b.getName();
    }
    
    @Override
    public int hashCode() {
        return this.b.hashCode();
    }
    
    @Override
    public final boolean isAnnotationPresent(final Class annotationClass) {
        return this.a.isAnnotationPresent(annotationClass);
    }
    
    @Override
    public final boolean isSynthetic() {
        return this.b.isSynthetic();
    }
    
    @Override
    public String toString() {
        return this.b.toString();
    }
    
    public abstract static class a extends com.google.common.reflect.a
    {
        public final Constructor c;
        
        public a(final Constructor c) {
            super(c);
            this.c = c;
        }
        
        public Type[] b() {
            Type[] genericParameterTypes;
            final Type[] original = genericParameterTypes = this.c.getGenericParameterTypes();
            if (original.length > 0) {
                genericParameterTypes = original;
                if (this.c()) {
                    final Class[] parameterTypes = this.c.getParameterTypes();
                    genericParameterTypes = original;
                    if (original.length == parameterTypes.length) {
                        genericParameterTypes = original;
                        if (parameterTypes[0] == this.getDeclaringClass().getEnclosingClass()) {
                            genericParameterTypes = Arrays.copyOfRange(original, 1, original.length);
                        }
                    }
                }
            }
            return genericParameterTypes;
        }
        
        public final boolean c() {
            final Class declaringClass = this.c.getDeclaringClass();
            final Constructor enclosingConstructor = declaringClass.getEnclosingConstructor();
            boolean b = true;
            if (enclosingConstructor != null) {
                return true;
            }
            final Method enclosingMethod = declaringClass.getEnclosingMethod();
            if (enclosingMethod != null) {
                return Modifier.isStatic(enclosingMethod.getModifiers()) ^ true;
            }
            if (declaringClass.getEnclosingClass() == null || Modifier.isStatic(declaringClass.getModifiers())) {
                b = false;
            }
            return b;
        }
    }
    
    public abstract static class b extends a
    {
        public final Method c;
        
        public b(final Method c) {
            super(c);
            this.c = c;
        }
    }
}

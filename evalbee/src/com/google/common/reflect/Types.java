// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.reflect;

import java.lang.reflect.Method;
import java.security.AccessControlException;
import com.google.common.collect.ImmutableMap;
import java.lang.reflect.Proxy;
import java.util.Iterator;
import java.util.Arrays;
import com.google.common.collect.ImmutableList;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.InvocationTargetException;
import java.io.Serializable;
import java.util.Objects;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.GenericDeclaration;
import java.lang.reflect.WildcardType;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.GenericArrayType;
import java.util.concurrent.atomic.AtomicReference;
import java.lang.reflect.Array;
import com.google.common.base.Predicates;
import java.util.Collection;
import java.lang.reflect.Type;

public abstract class Types
{
    public static final fh0 a;
    
    static {
        a = fh0.h(", ").j("null");
    }
    
    public static /* synthetic */ fh0 d() {
        return Types.a;
    }
    
    public static void f(final Type[] array, final String s) {
        for (final Type type : array) {
            if (type instanceof Class) {
                final Class clazz = (Class)type;
                i71.n(clazz.isPrimitive() ^ true, "Primitive type '%s' used as %s", clazz, s);
            }
        }
    }
    
    public static Iterable g(final Iterable iterable) {
        return rg0.d(iterable, Predicates.g(Predicates.d(Object.class)));
    }
    
    public static Class h(final Class componentType) {
        return Array.newInstance(componentType, 0).getClass();
    }
    
    public static Type i(final Type type) {
        i71.r(type);
        final AtomicReference atomicReference = new AtomicReference();
        new qz1(atomicReference) {
            public final AtomicReference b;
            
            @Override
            public void b(final Class clazz) {
                this.b.set(clazz.getComponentType());
            }
            
            @Override
            public void c(final GenericArrayType genericArrayType) {
                this.b.set(genericArrayType.getGenericComponentType());
            }
            
            @Override
            public void e(final TypeVariable typeVariable) {
                this.b.set(p(typeVariable.getBounds()));
            }
            
            @Override
            public void f(final WildcardType wildcardType) {
                this.b.set(p(wildcardType.getUpperBounds()));
            }
        }.a(type);
        return (Type)atomicReference.get();
    }
    
    public static Type j(final Type type) {
        if (!(type instanceof WildcardType)) {
            return JavaVersion.CURRENT.newArrayType(type);
        }
        final WildcardType wildcardType = (WildcardType)type;
        final Type[] lowerBounds = wildcardType.getLowerBounds();
        final int length = lowerBounds.length;
        final boolean b = true;
        i71.e(length <= 1, "Wildcard cannot have more than one lower bounds.");
        if (lowerBounds.length == 1) {
            return q(j(lowerBounds[0]));
        }
        final Type[] upperBounds = wildcardType.getUpperBounds();
        i71.e(upperBounds.length == 1 && b, "Wildcard should have only one upper bound.");
        return o(j(upperBounds[0]));
    }
    
    public static TypeVariable k(final GenericDeclaration genericDeclaration, final String s, final Type... array) {
        Type[] array2 = array;
        if (array.length == 0) {
            array2 = new Type[] { Object.class };
        }
        return n(genericDeclaration, s, array2);
    }
    
    public static ParameterizedType l(final Class clazz, final Type... array) {
        return new ParameterizedTypeImpl(ClassOwnership.JVM_BEHAVIOR.getOwnerType(clazz), clazz, array);
    }
    
    public static ParameterizedType m(final Type type, final Class clazz, final Type... array) {
        if (type == null) {
            return l(clazz, array);
        }
        i71.r(array);
        i71.m(clazz.getEnclosingClass() != null, "Owner type for unenclosed %s", clazz);
        return new ParameterizedTypeImpl(type, clazz, array);
    }
    
    public static TypeVariable n(final GenericDeclaration genericDeclaration, final String s, final Type[] array) {
        return (TypeVariable)zc1.a(TypeVariable.class, new d(new c(genericDeclaration, s, array)));
    }
    
    public static WildcardType o(final Type type) {
        return new WildcardTypeImpl(new Type[0], new Type[] { type });
    }
    
    public static Type p(final Type[] array) {
        for (int length = array.length, i = 0; i < length; ++i) {
            final Type j = i(array[i]);
            if (j != null) {
                if (j instanceof Class) {
                    final Class clazz = (Class)j;
                    if (clazz.isPrimitive()) {
                        return clazz;
                    }
                }
                return o(j);
            }
        }
        return null;
    }
    
    public static WildcardType q(final Type type) {
        return new WildcardTypeImpl(new Type[] { type }, new Type[] { Object.class });
    }
    
    public static Type[] r(final Collection collection) {
        return collection.toArray(new Type[0]);
    }
    
    public static String s(final Type type) {
        String s;
        if (type instanceof Class) {
            s = ((Class)type).getName();
        }
        else {
            s = type.toString();
        }
        return s;
    }
    
    public enum ClassOwnership
    {
        private static final ClassOwnership[] $VALUES;
        static final ClassOwnership JVM_BEHAVIOR;
        
        LOCAL_CLASS_HAS_NO_OWNER {
            @Override
            public Class<?> getOwnerType(final Class<?> clazz) {
                if (clazz.isLocalClass()) {
                    return null;
                }
                return clazz.getEnclosingClass();
            }
        }, 
        OWNED_BY_ENCLOSING_CLASS {
            @Override
            public Class<?> getOwnerType(final Class<?> clazz) {
                return clazz.getEnclosingClass();
            }
        };
        
        private static /* synthetic */ ClassOwnership[] $values() {
            return new ClassOwnership[] { ClassOwnership.OWNED_BY_ENCLOSING_CLASS, ClassOwnership.LOCAL_CLASS_HAS_NO_OWNER };
        }
        
        static {
            $VALUES = $values();
            JVM_BEHAVIOR = detectJvmBehavior();
        }
        
        private static ClassOwnership detectJvmBehavior() {
            new a() {};
            final ParameterizedType obj = (ParameterizedType)Types$ClassOwnership$b.class.getGenericSuperclass();
            Objects.requireNonNull(obj);
            final ParameterizedType parameterizedType = obj;
            for (final ClassOwnership classOwnership : values()) {
                if (classOwnership.getOwnerType(a.class) == parameterizedType.getOwnerType()) {
                    return classOwnership;
                }
            }
            throw new AssertionError();
        }
        
        public abstract Class<?> getOwnerType(final Class<?> p0);
        
        public abstract class a
        {
        }
    }
    
    public static final class GenericArrayTypeImpl implements GenericArrayType, Serializable
    {
        private static final long serialVersionUID = 0L;
        private final Type componentType;
        
        public GenericArrayTypeImpl(final Type type) {
            this.componentType = JavaVersion.CURRENT.usedInGenericType(type);
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof GenericArrayType && b11.a(this.getGenericComponentType(), ((GenericArrayType)o).getGenericComponentType());
        }
        
        @Override
        public Type getGenericComponentType() {
            return this.componentType;
        }
        
        @Override
        public int hashCode() {
            return this.componentType.hashCode();
        }
        
        @Override
        public String toString() {
            return String.valueOf(Types.s(this.componentType)).concat("[]");
        }
    }
    
    public enum JavaVersion
    {
        private static final JavaVersion[] $VALUES;
        static final JavaVersion CURRENT;
        
        JAVA6 {
            @Override
            public GenericArrayType newArrayType(final Type type) {
                return new GenericArrayTypeImpl(type);
            }
            
            @Override
            public Type usedInGenericType(final Type type) {
                i71.r(type);
                Type type2 = type;
                if (type instanceof Class) {
                    final Class clazz = (Class)type;
                    type2 = type;
                    if (clazz.isArray()) {
                        type2 = new GenericArrayTypeImpl(clazz.getComponentType());
                    }
                }
                return type2;
            }
        }, 
        JAVA7 {
            @Override
            public Type newArrayType(final Type type) {
                if (type instanceof Class) {
                    return Types.h((Class)type);
                }
                return new GenericArrayTypeImpl(type);
            }
            
            @Override
            public Type usedInGenericType(final Type type) {
                return (Type)i71.r(type);
            }
        }, 
        JAVA8 {
            @Override
            public Type newArrayType(final Type type) {
                return JavaVersion.JAVA7.newArrayType(type);
            }
            
            @Override
            public String typeName(final Type obj) {
                try {
                    return (String)Type.class.getMethod("getTypeName", (Class<?>[])new Class[0]).invoke(obj, new Object[0]);
                }
                catch (final IllegalAccessException cause) {
                    throw new RuntimeException(cause);
                }
                catch (final InvocationTargetException cause2) {
                    throw new RuntimeException(cause2);
                }
                catch (final NoSuchMethodException ex) {
                    throw new AssertionError((Object)"Type.getTypeName should be available in Java 8");
                }
            }
            
            @Override
            public Type usedInGenericType(final Type type) {
                return JavaVersion.JAVA7.usedInGenericType(type);
            }
        }, 
        JAVA9 {
            @Override
            public boolean jdkTypeDuplicatesOwnerName() {
                return false;
            }
            
            @Override
            public Type newArrayType(final Type type) {
                return JavaVersion.JAVA8.newArrayType(type);
            }
            
            @Override
            public String typeName(final Type type) {
                return JavaVersion.JAVA8.typeName(type);
            }
            
            @Override
            public Type usedInGenericType(final Type type) {
                return JavaVersion.JAVA8.usedInGenericType(type);
            }
        };
        
        private static /* synthetic */ JavaVersion[] $values() {
            return new JavaVersion[] { JavaVersion.JAVA6, JavaVersion.JAVA7, JavaVersion.JAVA8, JavaVersion.JAVA9 };
        }
        
        static {
            $VALUES = $values();
            if (AnnotatedElement.class.isAssignableFrom(TypeVariable.class)) {
                if (new lz1() {}.capture().toString().contains("java.util.Map.java.util.Map")) {
                    final JavaVersion current3;
                    CURRENT = current3;
                }
                else {
                    final JavaVersion current4;
                    CURRENT = current4;
                }
            }
            else if (new lz1() {}.capture() instanceof Class) {
                final JavaVersion current2;
                CURRENT = current2;
            }
            else {
                final JavaVersion current;
                CURRENT = current;
            }
        }
        
        public boolean jdkTypeDuplicatesOwnerName() {
            return true;
        }
        
        public abstract Type newArrayType(final Type p0);
        
        public String typeName(final Type type) {
            return Types.s(type);
        }
        
        public final ImmutableList<Type> usedInGenericType(final Type[] array) {
            final ImmutableList.a builder = ImmutableList.builder();
            for (int length = array.length, i = 0; i < length; ++i) {
                builder.i(this.usedInGenericType(array[i]));
            }
            return builder.l();
        }
        
        public abstract Type usedInGenericType(final Type p0);
    }
    
    public static final class ParameterizedTypeImpl implements ParameterizedType, Serializable
    {
        private static final long serialVersionUID = 0L;
        private final ImmutableList<Type> argumentsList;
        private final Type ownerType;
        private final Class<?> rawType;
        
        public ParameterizedTypeImpl(final Type ownerType, final Class<?> rawType, final Type[] array) {
            i71.r(rawType);
            i71.d(array.length == rawType.getTypeParameters().length);
            f(array, "type parameter");
            this.ownerType = ownerType;
            this.rawType = rawType;
            this.argumentsList = JavaVersion.CURRENT.usedInGenericType(array);
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof ParameterizedType;
            final boolean b2 = false;
            if (!b) {
                return false;
            }
            final ParameterizedType parameterizedType = (ParameterizedType)o;
            boolean b3 = b2;
            if (this.getRawType().equals(parameterizedType.getRawType())) {
                b3 = b2;
                if (b11.a(this.getOwnerType(), parameterizedType.getOwnerType())) {
                    b3 = b2;
                    if (Arrays.equals(this.getActualTypeArguments(), parameterizedType.getActualTypeArguments())) {
                        b3 = true;
                    }
                }
            }
            return b3;
        }
        
        @Override
        public Type[] getActualTypeArguments() {
            return r(this.argumentsList);
        }
        
        @Override
        public Type getOwnerType() {
            return this.ownerType;
        }
        
        @Override
        public Type getRawType() {
            return this.rawType;
        }
        
        @Override
        public int hashCode() {
            final Type ownerType = this.ownerType;
            int hashCode;
            if (ownerType == null) {
                hashCode = 0;
            }
            else {
                hashCode = ownerType.hashCode();
            }
            return hashCode ^ this.argumentsList.hashCode() ^ this.rawType.hashCode();
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            if (this.ownerType != null) {
                final JavaVersion current = JavaVersion.CURRENT;
                if (current.jdkTypeDuplicatesOwnerName()) {
                    sb.append(current.typeName(this.ownerType));
                    sb.append('.');
                }
            }
            sb.append(this.rawType.getName());
            sb.append('<');
            final fh0 d = Types.d();
            final ImmutableList<Type> argumentsList = this.argumentsList;
            final JavaVersion current2 = JavaVersion.CURRENT;
            Objects.requireNonNull(current2);
            sb.append(d.d(rg0.n(argumentsList, new com.google.common.reflect.c(current2))));
            sb.append('>');
            return sb.toString();
        }
    }
    
    public static final class WildcardTypeImpl implements WildcardType, Serializable
    {
        private static final long serialVersionUID = 0L;
        private final ImmutableList<Type> lowerBounds;
        private final ImmutableList<Type> upperBounds;
        
        public WildcardTypeImpl(final Type[] array, final Type[] array2) {
            f(array, "lower bound for wildcard");
            f(array2, "upper bound for wildcard");
            final JavaVersion current = JavaVersion.CURRENT;
            this.lowerBounds = current.usedInGenericType(array);
            this.upperBounds = current.usedInGenericType(array2);
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof WildcardType;
            boolean b3;
            final boolean b2 = b3 = false;
            if (b) {
                final WildcardType wildcardType = (WildcardType)o;
                b3 = b2;
                if (this.lowerBounds.equals(Arrays.asList(wildcardType.getLowerBounds()))) {
                    b3 = b2;
                    if (this.upperBounds.equals(Arrays.asList(wildcardType.getUpperBounds()))) {
                        b3 = true;
                    }
                }
            }
            return b3;
        }
        
        @Override
        public Type[] getLowerBounds() {
            return r(this.lowerBounds);
        }
        
        @Override
        public Type[] getUpperBounds() {
            return r(this.upperBounds);
        }
        
        @Override
        public int hashCode() {
            return this.lowerBounds.hashCode() ^ this.upperBounds.hashCode();
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("?");
            for (final Type type : this.lowerBounds) {
                sb.append(" super ");
                sb.append(JavaVersion.CURRENT.typeName(type));
            }
            for (final Type type2 : g(this.upperBounds)) {
                sb.append(" extends ");
                sb.append(JavaVersion.CURRENT.typeName(type2));
            }
            return sb.toString();
        }
    }
    
    public abstract static final class b
    {
        public static final boolean a;
        
        static {
            a = (b.class.getTypeParameters()[0].equals(Types.k(b.class, "X", new Type[0])) ^ true);
        }
    }
    
    public static final class c
    {
        public final GenericDeclaration a;
        public final String b;
        public final ImmutableList c;
        
        public c(final GenericDeclaration genericDeclaration, final String s, final Type[] array) {
            f(array, "bound for type variable");
            this.a = (GenericDeclaration)i71.r(genericDeclaration);
            this.b = (String)i71.r(s);
            this.c = ImmutableList.copyOf(array);
        }
        
        public GenericDeclaration a() {
            return this.a;
        }
        
        public String b() {
            return this.b;
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean a = Types.b.a;
            final boolean b = true;
            boolean b2 = true;
            if (a) {
                if (o != null && Proxy.isProxyClass(o.getClass()) && Proxy.getInvocationHandler(o) instanceof d) {
                    final c a2 = d.a((d)Proxy.getInvocationHandler(o));
                    if (!this.b.equals(a2.b()) || !this.a.equals(a2.a()) || !this.c.equals(a2.c)) {
                        b2 = false;
                    }
                    return b2;
                }
                return false;
            }
            else {
                if (o instanceof TypeVariable) {
                    final TypeVariable typeVariable = (TypeVariable)o;
                    return this.b.equals(typeVariable.getName()) && this.a.equals(typeVariable.getGenericDeclaration()) && b;
                }
                return false;
            }
        }
        
        @Override
        public int hashCode() {
            return this.a.hashCode() ^ this.b.hashCode();
        }
        
        @Override
        public String toString() {
            return this.b;
        }
    }
    
    public static final class d implements InvocationHandler
    {
        public static final ImmutableMap b;
        public final c a;
        
        static {
            final ImmutableMap.b builder = ImmutableMap.builder();
            final Method[] methods = c.class.getMethods();
            final int length = methods.length;
            int n = 0;
        Label_0047_Outer:
            while (true) {
                Label_0065: {
                    if (n >= length) {
                        break Label_0065;
                    }
                    final Method method = methods[n];
                    Label_0059: {
                        if (!method.getDeclaringClass().equals(c.class)) {
                            break Label_0059;
                        }
                        while (true) {
                            try {
                                method.setAccessible(true);
                                builder.g(method.getName(), method);
                                ++n;
                                continue Label_0047_Outer;
                                b = builder.c();
                            }
                            catch (final AccessControlException ex) {
                                continue;
                            }
                            break;
                        }
                    }
                }
            }
        }
        
        public d(final c a) {
            this.a = a;
        }
        
        public static /* synthetic */ c a(final d d) {
            return d.a;
        }
        
        @Override
        public Object invoke(Object invoke, final Method method, final Object[] args) {
            final String name = method.getName();
            final Method method2 = d.b.get(name);
            if (method2 != null) {
                try {
                    invoke = method2.invoke(this.a, args);
                    return invoke;
                }
                catch (final InvocationTargetException ex) {
                    throw ex.getCause();
                }
            }
            throw new UnsupportedOperationException(name);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.reflect;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.lang.reflect.GenericDeclaration;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.Iterator;
import java.util.Arrays;
import com.google.common.collect.ImmutableMap;
import java.util.HashMap;
import com.google.common.collect.Maps;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.WildcardType;
import java.lang.reflect.Type;
import java.util.Map;

public final class b
{
    public final c a;
    
    public b() {
        this.a = new c();
    }
    
    public b(final c a) {
        this.a = a;
    }
    
    public static b d(final Type type) {
        return new b().o(b.g(type));
    }
    
    public static Object e(final Class clazz, final Object o) {
        try {
            return clazz.cast(o);
        }
        catch (final ClassCastException ex) {
            final String value = String.valueOf(o);
            final String simpleName = clazz.getSimpleName();
            final StringBuilder sb = new StringBuilder(value.length() + 10 + simpleName.length());
            sb.append(value);
            sb.append(" is not a ");
            sb.append(simpleName);
            throw new IllegalArgumentException(sb.toString());
        }
    }
    
    public static b f(Type a) {
        a = e.b.a(a);
        return new b().o(b.g(a));
    }
    
    public static void g(final Map map, final Type type, final Type obj) {
        if (type.equals(obj)) {
            return;
        }
        new qz1(map, obj) {
            public final Map b;
            public final Type c;
            
            @Override
            public void b(final Class obj) {
                if (this.c instanceof WildcardType) {
                    return;
                }
                final String value = String.valueOf(obj);
                final String value2 = String.valueOf(this.c);
                final StringBuilder sb = new StringBuilder(value.length() + 25 + value2.length());
                sb.append("No type mapping from ");
                sb.append(value);
                sb.append(" to ");
                sb.append(value2);
                throw new IllegalArgumentException(sb.toString());
            }
            
            @Override
            public void c(final GenericArrayType genericArrayType) {
                final Type c = this.c;
                if (c instanceof WildcardType) {
                    return;
                }
                final Type i = Types.i(c);
                i71.m(i != null, "%s is not an array type.", this.c);
                g(this.b, genericArrayType.getGenericComponentType(), i);
            }
            
            @Override
            public void d(final ParameterizedType parameterizedType) {
                final Type c = this.c;
                if (c instanceof WildcardType) {
                    return;
                }
                final ParameterizedType parameterizedType2 = (ParameterizedType)e(ParameterizedType.class, c);
                if (parameterizedType.getOwnerType() != null && parameterizedType2.getOwnerType() != null) {
                    g(this.b, parameterizedType.getOwnerType(), parameterizedType2.getOwnerType());
                }
                i71.n(parameterizedType.getRawType().equals(parameterizedType2.getRawType()), "Inconsistent raw type: %s vs. %s", parameterizedType, this.c);
                final Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
                final Type[] actualTypeArguments2 = parameterizedType2.getActualTypeArguments();
                final int length = actualTypeArguments.length;
                final int length2 = actualTypeArguments2.length;
                int i = 0;
                i71.n(length == length2, "%s not compatible with %s", parameterizedType, parameterizedType2);
                while (i < actualTypeArguments.length) {
                    g(this.b, actualTypeArguments[i], actualTypeArguments2[i]);
                    ++i;
                }
            }
            
            @Override
            public void e(final TypeVariable typeVariable) {
                this.b.put(new d(typeVariable), this.c);
            }
            
            @Override
            public void f(final WildcardType wildcardType) {
                final Type c = this.c;
                if (!(c instanceof WildcardType)) {
                    return;
                }
                final WildcardType wildcardType2 = (WildcardType)c;
                final Type[] upperBounds = wildcardType.getUpperBounds();
                final Type[] upperBounds2 = wildcardType2.getUpperBounds();
                final Type[] lowerBounds = wildcardType.getLowerBounds();
                final Type[] lowerBounds2 = wildcardType2.getLowerBounds();
                final int length = upperBounds.length;
                final int length2 = upperBounds2.length;
                final int n = 0;
                i71.n(length == length2 && lowerBounds.length == lowerBounds2.length, "Incompatible type: %s vs. %s", wildcardType, this.c);
                int n2 = 0;
                int i;
                while (true) {
                    i = n;
                    if (n2 >= upperBounds.length) {
                        break;
                    }
                    g(this.b, upperBounds[n2], upperBounds2[n2]);
                    ++n2;
                }
                while (i < lowerBounds.length) {
                    g(this.b, lowerBounds[i], lowerBounds2[i]);
                    ++i;
                }
            }
        }.a(type);
    }
    
    public final Type h(final GenericArrayType genericArrayType) {
        return Types.j(this.j(genericArrayType.getGenericComponentType()));
    }
    
    public final ParameterizedType i(final ParameterizedType parameterizedType) {
        final Type ownerType = parameterizedType.getOwnerType();
        Type j;
        if (ownerType == null) {
            j = null;
        }
        else {
            j = this.j(ownerType);
        }
        return Types.m(j, (Class)this.j(parameterizedType.getRawType()), this.k(parameterizedType.getActualTypeArguments()));
    }
    
    public Type j(final Type type) {
        i71.r(type);
        if (type instanceof TypeVariable) {
            return this.a.a((TypeVariable)type);
        }
        if (type instanceof ParameterizedType) {
            return this.i((ParameterizedType)type);
        }
        if (type instanceof GenericArrayType) {
            return this.h((GenericArrayType)type);
        }
        Type m = type;
        if (type instanceof WildcardType) {
            m = this.m((WildcardType)type);
        }
        return m;
    }
    
    public final Type[] k(final Type[] array) {
        final Type[] array2 = new Type[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = this.j(array[i]);
        }
        return array2;
    }
    
    public Type[] l(final Type[] array) {
        for (int i = 0; i < array.length; ++i) {
            array[i] = this.j(array[i]);
        }
        return array;
    }
    
    public final WildcardType m(final WildcardType wildcardType) {
        return new Types.WildcardTypeImpl(this.k(wildcardType.getLowerBounds()), this.k(wildcardType.getUpperBounds()));
    }
    
    public b n(final Type type, final Type type2) {
        final HashMap p2 = Maps.p();
        g(p2, (Type)i71.r(type), (Type)i71.r(type2));
        return this.o(p2);
    }
    
    public b o(final Map map) {
        return new b(this.a.c(map));
    }
    
    public static final class b extends qz1
    {
        public final Map b;
        
        public b() {
            this.b = Maps.p();
        }
        
        public static ImmutableMap g(final Type type) {
            i71.r(type);
            final b b = new b();
            b.a(type);
            return ImmutableMap.copyOf((Map<?, ?>)b.b);
        }
        
        @Override
        public void b(final Class clazz) {
            this.a(clazz.getGenericSuperclass());
            this.a(clazz.getGenericInterfaces());
        }
        
        @Override
        public void d(final ParameterizedType parameterizedType) {
            final Class clazz = (Class)parameterizedType.getRawType();
            final TypeVariable[] typeParameters = clazz.getTypeParameters();
            final Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
            i71.x(typeParameters.length == actualTypeArguments.length);
            for (int i = 0; i < typeParameters.length; ++i) {
                this.h(new d(typeParameters[i]), actualTypeArguments[i]);
            }
            this.a(clazz);
            this.a(parameterizedType.getOwnerType());
        }
        
        @Override
        public void e(final TypeVariable typeVariable) {
            this.a(typeVariable.getBounds());
        }
        
        @Override
        public void f(final WildcardType wildcardType) {
            this.a(wildcardType.getUpperBounds());
        }
        
        public final void h(final d d, Type type) {
            if (this.b.containsKey(d)) {
                return;
            }
            for (Type type2 = type; type2 != null; type2 = (Type)this.b.get(com.google.common.reflect.b.d.c(type2))) {
                if (d.a(type2)) {
                    while (type != null) {
                        type = this.b.remove(com.google.common.reflect.b.d.c(type));
                    }
                    return;
                }
            }
            this.b.put(d, type);
        }
    }
    
    public static class c
    {
        public final ImmutableMap a;
        
        public c() {
            this.a = ImmutableMap.of();
        }
        
        public c(final ImmutableMap a) {
            this.a = a;
        }
        
        public final Type a(final TypeVariable typeVariable) {
            return this.b(typeVariable, new c(this, typeVariable, this) {
                public final TypeVariable b;
                public final c c;
                
                @Override
                public Type b(final TypeVariable typeVariable, final c c) {
                    if (typeVariable.getGenericDeclaration().equals(this.b.getGenericDeclaration())) {
                        return typeVariable;
                    }
                    return this.c.b(typeVariable, c);
                }
            });
        }
        
        public Type b(final TypeVariable typeVariable, final c c) {
            final Type type = this.a.get(new d(typeVariable));
            if (type != null) {
                return new b(c, null).j(type);
            }
            final Type[] bounds = typeVariable.getBounds();
            if (bounds.length == 0) {
                return typeVariable;
            }
            final Type[] c2 = new b(c, null).k(bounds);
            if (Types.b.a && Arrays.equals(bounds, c2)) {
                return typeVariable;
            }
            return Types.k(typeVariable.getGenericDeclaration(), typeVariable.getName(), c2);
        }
        
        public final c c(final Map map) {
            final ImmutableMap.b builder = ImmutableMap.builder();
            builder.j(this.a);
            for (final Map.Entry<d, V> entry : map.entrySet()) {
                final d d = entry.getKey();
                final Type type = (Type)entry.getValue();
                i71.m(d.a(type) ^ true, "Type variable %s bound to itself", d);
                builder.g(d, type);
            }
            return new c(builder.d());
        }
    }
    
    public static final class d
    {
        public final TypeVariable a;
        
        public d(final TypeVariable typeVariable) {
            this.a = (TypeVariable)i71.r(typeVariable);
        }
        
        public static d c(final Type type) {
            if (type instanceof TypeVariable) {
                return new d((TypeVariable)type);
            }
            return null;
        }
        
        public boolean a(final Type type) {
            return type instanceof TypeVariable && this.b((TypeVariable)type);
        }
        
        public final boolean b(final TypeVariable typeVariable) {
            return this.a.getGenericDeclaration().equals(typeVariable.getGenericDeclaration()) && this.a.getName().equals(typeVariable.getName());
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof d && this.b(((d)o).a);
        }
        
        @Override
        public int hashCode() {
            return b11.b(this.a.getGenericDeclaration(), this.a.getName());
        }
        
        @Override
        public String toString() {
            return this.a.toString();
        }
    }
    
    public static class e
    {
        public static final e b;
        public final AtomicInteger a;
        
        static {
            b = new e();
        }
        
        public e() {
            this(new AtomicInteger());
        }
        
        public e(final AtomicInteger a) {
            this.a = a;
        }
        
        public final Type a(Type b) {
            i71.r(b);
            if (b instanceof Class) {
                return b;
            }
            if (b instanceof TypeVariable) {
                return b;
            }
            if (b instanceof GenericArrayType) {
                return Types.j(this.e().a(((GenericArrayType)b).getGenericComponentType()));
            }
            if (b instanceof ParameterizedType) {
                final ParameterizedType parameterizedType = (ParameterizedType)b;
                final Class clazz = (Class)parameterizedType.getRawType();
                final TypeVariable[] typeParameters = clazz.getTypeParameters();
                final Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
                for (int i = 0; i < actualTypeArguments.length; ++i) {
                    actualTypeArguments[i] = this.d(typeParameters[i]).a(actualTypeArguments[i]);
                }
                return Types.m(this.e().c(parameterizedType.getOwnerType()), clazz, actualTypeArguments);
            }
            if (b instanceof WildcardType) {
                final WildcardType wildcardType = (WildcardType)b;
                if (wildcardType.getLowerBounds().length == 0) {
                    b = this.b(wildcardType.getUpperBounds());
                }
                return b;
            }
            throw new AssertionError((Object)"must have been one of the known types");
        }
        
        public TypeVariable b(final Type[] array) {
            final int incrementAndGet = this.a.incrementAndGet();
            final String f = fh0.g('&').f(array);
            final StringBuilder sb = new StringBuilder(String.valueOf(f).length() + 33);
            sb.append("capture#");
            sb.append(incrementAndGet);
            sb.append("-of ? extends ");
            sb.append(f);
            return Types.k(e.class, sb.toString(), array);
        }
        
        public final Type c(final Type type) {
            if (type == null) {
                return null;
            }
            return this.a(type);
        }
        
        public final e d(final TypeVariable typeVariable) {
            return new e(this, this.a, typeVariable) {
                public final TypeVariable c;
                
                @Override
                public TypeVariable b(final Type[] a) {
                    final LinkedHashSet set = new LinkedHashSet((Collection<? extends E>)Arrays.asList(a));
                    set.addAll(Arrays.asList(this.c.getBounds()));
                    if (set.size() > 1) {
                        set.remove(Object.class);
                    }
                    return super.b((Type[])set.toArray(new Type[0]));
                }
            };
        }
        
        public final e e() {
            return new e(this.a);
        }
    }
}

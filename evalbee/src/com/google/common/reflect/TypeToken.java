// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.reflect;

import java.util.HashMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Ordering;
import java.util.Comparator;
import java.util.Map;
import java.util.Arrays;
import java.util.Set;
import java.util.Collection;
import java.lang.reflect.Method;
import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Iterator;
import java.util.Objects;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.lang.reflect.WildcardType;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.Type;
import java.io.Serializable;

public abstract class TypeToken<T> extends lz1 implements Serializable
{
    private static final long serialVersionUID = 3637540370352322684L;
    private transient b covariantTypeResolver;
    private transient b invariantTypeResolver;
    private final Type runtimeType;
    
    public TypeToken() {
        final Type capture = this.capture();
        this.runtimeType = capture;
        i71.B(capture instanceof TypeVariable ^ true, "Cannot construct a TypeToken for a type variable.\nYou probably meant to call new TypeToken<%s>(getClass()) that can resolve the type variable for you.\nIf you do need to create a TypeToken of a type variable, please use TypeToken.of() instead.", capture);
    }
    
    public TypeToken(final Class<?> clazz) {
        final Type capture = super.capture();
        if (capture instanceof Class) {
            this.runtimeType = capture;
        }
        else {
            this.runtimeType = b.d(clazz).j(capture);
        }
    }
    
    private TypeToken(final Type type) {
        this.runtimeType = (Type)i71.r(type);
    }
    
    public static /* synthetic */ Type access$500(final TypeToken typeToken) {
        return typeToken.runtimeType;
    }
    
    private static e any(final Type[] array) {
        return new e(array, true);
    }
    
    private TypeToken<? super T> boundAsSuperclass(final Type type) {
        TypeToken<?> of;
        if ((of = of(type)).getRawType().isInterface()) {
            of = null;
        }
        return (TypeToken<? super T>)of;
    }
    
    private ImmutableList<TypeToken<? super T>> boundsAsInterfaces(final Type[] array) {
        final ImmutableList.a builder = ImmutableList.builder();
        for (int length = array.length, i = 0; i < length; ++i) {
            final TypeToken<?> of = of(array[i]);
            if (of.getRawType().isInterface()) {
                builder.i(of);
            }
        }
        return builder.l();
    }
    
    private static Type canonicalizeTypeArg(final TypeVariable<?> typeVariable, final Type type) {
        Type type2;
        if (type instanceof WildcardType) {
            type2 = canonicalizeWildcardType(typeVariable, (WildcardType)type);
        }
        else {
            type2 = canonicalizeWildcardsInType(type);
        }
        return type2;
    }
    
    private static WildcardType canonicalizeWildcardType(final TypeVariable<?> typeVariable, final WildcardType wildcardType) {
        final Type[] bounds = typeVariable.getBounds();
        final ArrayList list = new ArrayList();
        for (final Type type : wildcardType.getUpperBounds()) {
            if (!any(bounds).a(type)) {
                list.add(canonicalizeWildcardsInType(type));
            }
        }
        return new Types.WildcardTypeImpl(wildcardType.getLowerBounds(), (Type[])list.toArray(new Type[0]));
    }
    
    private static ParameterizedType canonicalizeWildcardsInParameterizedType(final ParameterizedType parameterizedType) {
        final Class clazz = (Class)parameterizedType.getRawType();
        final TypeVariable[] typeParameters = clazz.getTypeParameters();
        final Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
        for (int i = 0; i < actualTypeArguments.length; ++i) {
            actualTypeArguments[i] = canonicalizeTypeArg(typeParameters[i], actualTypeArguments[i]);
        }
        return Types.m(parameterizedType.getOwnerType(), clazz, actualTypeArguments);
    }
    
    private static Type canonicalizeWildcardsInType(final Type type) {
        if (type instanceof ParameterizedType) {
            return canonicalizeWildcardsInParameterizedType((ParameterizedType)type);
        }
        Type j = type;
        if (type instanceof GenericArrayType) {
            j = Types.j(canonicalizeWildcardsInType(((GenericArrayType)type).getGenericComponentType()));
        }
        return j;
    }
    
    private static e every(final Type[] array) {
        return new e(array, false);
    }
    
    private TypeToken<? extends T> getArraySubtype(final Class<?> obj) {
        final Class<?> componentType = obj.getComponentType();
        if (componentType != null) {
            final TypeToken<?> componentType2 = this.getComponentType();
            Objects.requireNonNull(componentType2);
            return (TypeToken<? extends T>)of(newArrayClassOrGenericArrayType(componentType2.getSubtype(componentType).runtimeType));
        }
        final String value = String.valueOf(obj);
        final String value2 = String.valueOf(this);
        final StringBuilder sb = new StringBuilder(value.length() + 36 + value2.length());
        sb.append(value);
        sb.append(" does not appear to be a subtype of ");
        sb.append(value2);
        throw new IllegalArgumentException(sb.toString());
    }
    
    private TypeToken<? super T> getArraySupertype(final Class<? super T> obj) {
        final TypeToken<?> componentType = this.getComponentType();
        if (componentType != null) {
            final Class<?> componentType2 = obj.getComponentType();
            Objects.requireNonNull(componentType2);
            return (TypeToken<? super T>)of(newArrayClassOrGenericArrayType(componentType.getSupertype(componentType2).runtimeType));
        }
        final String value = String.valueOf(obj);
        final String value2 = String.valueOf(this);
        final StringBuilder sb = new StringBuilder(value.length() + 23 + value2.length());
        sb.append(value);
        sb.append(" isn't a super type of ");
        sb.append(value2);
        throw new IllegalArgumentException(sb.toString());
    }
    
    private b getCovariantTypeResolver() {
        b covariantTypeResolver;
        if ((covariantTypeResolver = this.covariantTypeResolver) == null) {
            covariantTypeResolver = b.d(this.runtimeType);
            this.covariantTypeResolver = covariantTypeResolver;
        }
        return covariantTypeResolver;
    }
    
    private b getInvariantTypeResolver() {
        b invariantTypeResolver;
        if ((invariantTypeResolver = this.invariantTypeResolver) == null) {
            invariantTypeResolver = b.f(this.runtimeType);
            this.invariantTypeResolver = invariantTypeResolver;
        }
        return invariantTypeResolver;
    }
    
    private Type getOwnerTypeIfPresent() {
        final Type runtimeType = this.runtimeType;
        if (runtimeType instanceof ParameterizedType) {
            return ((ParameterizedType)runtimeType).getOwnerType();
        }
        if (runtimeType instanceof Class) {
            return ((Class)runtimeType).getEnclosingClass();
        }
        return null;
    }
    
    private ImmutableSet<Class<? super T>> getRawTypes() {
        final ImmutableSet.a builder = ImmutableSet.builder();
        new qz1(this, builder) {
            public final ImmutableSet.a b;
            
            @Override
            public void b(final Class clazz) {
                this.b.i(clazz);
            }
            
            @Override
            public void c(final GenericArrayType genericArrayType) {
                this.b.i(Types.h(TypeToken.of(genericArrayType.getGenericComponentType()).getRawType()));
            }
            
            @Override
            public void d(final ParameterizedType parameterizedType) {
                this.b.i(parameterizedType.getRawType());
            }
            
            @Override
            public void e(final TypeVariable typeVariable) {
                this.a(typeVariable.getBounds());
            }
            
            @Override
            public void f(final WildcardType wildcardType) {
                this.a(wildcardType.getUpperBounds());
            }
        }.a(this.runtimeType);
        return builder.m();
    }
    
    private TypeToken<? extends T> getSubtypeFromLowerBounds(final Class<?> obj, final Type[] array) {
        if (array.length > 0) {
            return (TypeToken<? extends T>)of(array[0]).getSubtype(obj);
        }
        final String value = String.valueOf(obj);
        final String value2 = String.valueOf(this);
        final StringBuilder sb = new StringBuilder(value.length() + 21 + value2.length());
        sb.append(value);
        sb.append(" isn't a subclass of ");
        sb.append(value2);
        throw new IllegalArgumentException(sb.toString());
    }
    
    private TypeToken<? super T> getSupertypeFromUpperBounds(final Class<? super T> obj, final Type[] array) {
        for (int length = array.length, i = 0; i < length; ++i) {
            final TypeToken<?> of = of(array[i]);
            if (of.isSubtypeOf(obj)) {
                return of.getSupertype(obj);
            }
        }
        final String value = String.valueOf(obj);
        final String value2 = String.valueOf(this);
        final StringBuilder sb = new StringBuilder(value.length() + 23 + value2.length());
        sb.append(value);
        sb.append(" isn't a super type of ");
        sb.append(value2);
        throw new IllegalArgumentException(sb.toString());
    }
    
    private boolean is(final Type obj, final TypeVariable<?> typeVariable) {
        final boolean equals = this.runtimeType.equals(obj);
        boolean b = true;
        if (equals) {
            return true;
        }
        if (obj instanceof WildcardType) {
            final WildcardType canonicalizeWildcardType = canonicalizeWildcardType(typeVariable, (WildcardType)obj);
            if (!every(canonicalizeWildcardType.getUpperBounds()).b(this.runtimeType) || !every(canonicalizeWildcardType.getLowerBounds()).a(this.runtimeType)) {
                b = false;
            }
            return b;
        }
        return canonicalizeWildcardsInType(this.runtimeType).equals(canonicalizeWildcardsInType(obj));
    }
    
    private boolean isOwnedBySubtypeOf(final Type type) {
        final Iterator<Object> iterator = this.getTypes().iterator();
        while (iterator.hasNext()) {
            final Type ownerTypeIfPresent = iterator.next().getOwnerTypeIfPresent();
            if (ownerTypeIfPresent != null && of(ownerTypeIfPresent).isSubtypeOf(type)) {
                return true;
            }
        }
        return false;
    }
    
    private boolean isSubtypeOfArrayType(final GenericArrayType genericArrayType) {
        final Type runtimeType = this.runtimeType;
        TypeToken<?> typeToken;
        if (runtimeType instanceof Class) {
            final Class clazz = (Class)runtimeType;
            if (!clazz.isArray()) {
                return false;
            }
            typeToken = of(clazz.getComponentType());
        }
        else {
            if (!(runtimeType instanceof GenericArrayType)) {
                return false;
            }
            typeToken = of(((GenericArrayType)runtimeType).getGenericComponentType());
        }
        return typeToken.isSubtypeOf(genericArrayType.getGenericComponentType());
    }
    
    private boolean isSubtypeOfParameterizedType(final ParameterizedType parameterizedType) {
        final Class<?> rawType = of(parameterizedType).getRawType();
        final boolean someRawTypeIsSubclass = this.someRawTypeIsSubclassOf(rawType);
        boolean b = false;
        if (!someRawTypeIsSubclass) {
            return false;
        }
        final TypeVariable<Class<?>>[] typeParameters = rawType.getTypeParameters();
        final Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
        for (int i = 0; i < typeParameters.length; ++i) {
            if (!of(this.getCovariantTypeResolver().j(typeParameters[i])).is(actualTypeArguments[i], typeParameters[i])) {
                return false;
            }
        }
        if (Modifier.isStatic(((Class)parameterizedType.getRawType()).getModifiers()) || parameterizedType.getOwnerType() == null || this.isOwnedBySubtypeOf(parameterizedType.getOwnerType())) {
            b = true;
        }
        return b;
    }
    
    private boolean isSupertypeOfArray(final GenericArrayType genericArrayType) {
        final Type runtimeType = this.runtimeType;
        if (!(runtimeType instanceof Class)) {
            return runtimeType instanceof GenericArrayType && of(genericArrayType.getGenericComponentType()).isSubtypeOf(((GenericArrayType)this.runtimeType).getGenericComponentType());
        }
        final Class clazz = (Class)runtimeType;
        if (!clazz.isArray()) {
            return clazz.isAssignableFrom(Object[].class);
        }
        return of(genericArrayType.getGenericComponentType()).isSubtypeOf(clazz.getComponentType());
    }
    
    private boolean isWrapper() {
        return e81.b().contains(this.runtimeType);
    }
    
    private static Type newArrayClassOrGenericArrayType(final Type type) {
        return Types.JavaVersion.JAVA7.newArrayType(type);
    }
    
    public static <T> TypeToken<T> of(final Class<T> clazz) {
        return new SimpleTypeToken<T>((Type)clazz);
    }
    
    public static TypeToken<?> of(final Type type) {
        return new SimpleTypeToken<Object>(type);
    }
    
    private TypeToken<?> resolveSupertype(final Type type) {
        final TypeToken<?> of = of(this.getCovariantTypeResolver().j(type));
        of.covariantTypeResolver = this.covariantTypeResolver;
        of.invariantTypeResolver = this.invariantTypeResolver;
        return of;
    }
    
    private Type resolveTypeArgsForSubclass(final Class<?> clazz) {
        if (this.runtimeType instanceof Class && (clazz.getTypeParameters().length == 0 || this.getRawType().getTypeParameters().length != 0)) {
            return clazz;
        }
        final TypeToken<?> genericType = toGenericType(clazz);
        return new b().n(genericType.getSupertype(this.getRawType()).runtimeType, this.runtimeType).j(genericType.runtimeType);
    }
    
    private boolean someRawTypeIsSubclassOf(final Class<?> clazz) {
        final w02 iterator = this.getRawTypes().iterator();
        while (iterator.hasNext()) {
            if (clazz.isAssignableFrom((Class)iterator.next())) {
                return true;
            }
        }
        return false;
    }
    
    public static <T> TypeToken<? extends T> toGenericType(final Class<T> clazz) {
        Type type;
        if (clazz.isArray()) {
            type = Types.j(toGenericType(clazz.getComponentType()).runtimeType);
        }
        else {
            final TypeVariable[] typeParameters = clazz.getTypeParameters();
            Type runtimeType;
            if (clazz.isMemberClass() && !Modifier.isStatic(clazz.getModifiers())) {
                runtimeType = toGenericType(clazz.getEnclosingClass()).runtimeType;
            }
            else {
                runtimeType = null;
            }
            if (typeParameters.length <= 0 && (runtimeType == null || runtimeType == clazz.getEnclosingClass())) {
                return of((Class<? extends T>)clazz);
            }
            type = Types.m(runtimeType, clazz, (Type[])typeParameters);
        }
        return (TypeToken<? extends T>)of(type);
    }
    
    public final a constructor(final Constructor<?> constructor) {
        i71.n(constructor.getDeclaringClass() == this.getRawType(), "%s not declared by %s", constructor, this.getRawType());
        return new a.a(this, constructor) {
            public final TypeToken d;
            
            @Override
            public TypeToken a() {
                return this.d;
            }
            
            @Override
            public Type[] b() {
                return this.d.getInvariantTypeResolver().l(super.b());
            }
            
            @Override
            public String toString() {
                final String value = String.valueOf(this.a());
                final String f = fh0.h(", ").f(this.b());
                final StringBuilder sb = new StringBuilder(value.length() + 2 + String.valueOf(f).length());
                sb.append(value);
                sb.append("(");
                sb.append(f);
                sb.append(")");
                return sb.toString();
            }
        };
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof TypeToken && this.runtimeType.equals(((TypeToken)o).runtimeType);
    }
    
    public final TypeToken<?> getComponentType() {
        final Type i = Types.i(this.runtimeType);
        if (i == null) {
            return null;
        }
        return of(i);
    }
    
    public final ImmutableList<TypeToken<? super T>> getGenericInterfaces() {
        final Type runtimeType = this.runtimeType;
        if (runtimeType instanceof TypeVariable) {
            return this.boundsAsInterfaces(((TypeVariable)runtimeType).getBounds());
        }
        if (runtimeType instanceof WildcardType) {
            return this.boundsAsInterfaces(((WildcardType)runtimeType).getUpperBounds());
        }
        final ImmutableList.a builder = ImmutableList.builder();
        final Type[] genericInterfaces = this.getRawType().getGenericInterfaces();
        for (int length = genericInterfaces.length, i = 0; i < length; ++i) {
            builder.i(this.resolveSupertype(genericInterfaces[i]));
        }
        return builder.l();
    }
    
    public final TypeToken<? super T> getGenericSuperclass() {
        final Type runtimeType = this.runtimeType;
        Type type;
        if (runtimeType instanceof TypeVariable) {
            type = ((TypeVariable)runtimeType).getBounds()[0];
        }
        else if (runtimeType instanceof WildcardType) {
            type = ((WildcardType)runtimeType).getUpperBounds()[0];
        }
        else {
            final Type genericSuperclass = this.getRawType().getGenericSuperclass();
            if (genericSuperclass == null) {
                return null;
            }
            return (TypeToken<? super T>)this.resolveSupertype(genericSuperclass);
        }
        return this.boundAsSuperclass(type);
    }
    
    public final Class<? super T> getRawType() {
        return this.getRawTypes().iterator().next();
    }
    
    public final TypeToken<? extends T> getSubtype(final Class<?> clazz) {
        i71.m(this.runtimeType instanceof TypeVariable ^ true, "Cannot get subtype of type variable <%s>", this);
        final Type runtimeType = this.runtimeType;
        if (runtimeType instanceof WildcardType) {
            return this.getSubtypeFromLowerBounds(clazz, ((WildcardType)runtimeType).getLowerBounds());
        }
        if (this.isArray()) {
            return this.getArraySubtype(clazz);
        }
        i71.n(this.getRawType().isAssignableFrom(clazz), "%s isn't a subclass of %s", clazz, this);
        final TypeToken<?> of = of(this.resolveTypeArgsForSubclass(clazz));
        i71.n(of.isSubtypeOf(this), "%s does not appear to be a subtype of %s", of, this);
        return (TypeToken<? extends T>)of;
    }
    
    public final TypeToken<? super T> getSupertype(final Class<? super T> clazz) {
        i71.n(this.someRawTypeIsSubclassOf(clazz), "%s is not a super class of %s", clazz, this);
        final Type runtimeType = this.runtimeType;
        if (runtimeType instanceof TypeVariable) {
            return this.getSupertypeFromUpperBounds(clazz, ((TypeVariable)runtimeType).getBounds());
        }
        if (runtimeType instanceof WildcardType) {
            return this.getSupertypeFromUpperBounds(clazz, ((WildcardType)runtimeType).getUpperBounds());
        }
        if (clazz.isArray()) {
            return this.getArraySupertype(clazz);
        }
        return (TypeToken<? super T>)this.resolveSupertype(toGenericType(clazz).runtimeType);
    }
    
    public final Type getType() {
        return this.runtimeType;
    }
    
    public final TypeSet getTypes() {
        return new TypeSet();
    }
    
    @Override
    public int hashCode() {
        return this.runtimeType.hashCode();
    }
    
    public final boolean isArray() {
        return this.getComponentType() != null;
    }
    
    public final boolean isPrimitive() {
        final Type runtimeType = this.runtimeType;
        return runtimeType instanceof Class && ((Class)runtimeType).isPrimitive();
    }
    
    public final boolean isSubtypeOf(final TypeToken<?> typeToken) {
        return this.isSubtypeOf(typeToken.getType());
    }
    
    public final boolean isSubtypeOf(final Type obj) {
        i71.r(obj);
        if (obj instanceof WildcardType) {
            return any(((WildcardType)obj).getLowerBounds()).b(this.runtimeType);
        }
        final Type runtimeType = this.runtimeType;
        if (runtimeType instanceof WildcardType) {
            return any(((WildcardType)runtimeType).getUpperBounds()).a(obj);
        }
        final boolean b = runtimeType instanceof TypeVariable;
        boolean b2 = false;
        if (b) {
            if (runtimeType.equals(obj) || any(((TypeVariable)this.runtimeType).getBounds()).a(obj)) {
                b2 = true;
            }
            return b2;
        }
        if (runtimeType instanceof GenericArrayType) {
            return of(obj).isSupertypeOfArray((GenericArrayType)this.runtimeType);
        }
        if (obj instanceof Class) {
            return this.someRawTypeIsSubclassOf((Class<?>)obj);
        }
        if (obj instanceof ParameterizedType) {
            return this.isSubtypeOfParameterizedType((ParameterizedType)obj);
        }
        return obj instanceof GenericArrayType && this.isSubtypeOfArrayType((GenericArrayType)obj);
    }
    
    public final boolean isSupertypeOf(final TypeToken<?> typeToken) {
        return typeToken.isSubtypeOf(this.getType());
    }
    
    public final boolean isSupertypeOf(final Type type) {
        return of(type).isSubtypeOf(this.getType());
    }
    
    public final a method(final Method method) {
        i71.n(this.someRawTypeIsSubclassOf(method.getDeclaringClass()), "%s not declared by %s", method, this);
        return new a.b(this, method) {
            public final TypeToken d;
            
            @Override
            public TypeToken a() {
                return this.d;
            }
            
            @Override
            public String toString() {
                final String value = String.valueOf(this.a());
                final String string = super.toString();
                final StringBuilder sb = new StringBuilder(value.length() + 1 + String.valueOf(string).length());
                sb.append(value);
                sb.append(".");
                sb.append(string);
                return sb.toString();
            }
        };
    }
    
    public final TypeToken<T> rejectTypeVariables() {
        new qz1(this) {
            public final TypeToken b;
            
            @Override
            public void c(final GenericArrayType genericArrayType) {
                this.a(genericArrayType.getGenericComponentType());
            }
            
            @Override
            public void d(final ParameterizedType parameterizedType) {
                this.a(parameterizedType.getActualTypeArguments());
                this.a(parameterizedType.getOwnerType());
            }
            
            @Override
            public void e(final TypeVariable typeVariable) {
                final String value = String.valueOf(TypeToken.access$500(this.b));
                final StringBuilder sb = new StringBuilder(value.length() + 58);
                sb.append(value);
                sb.append("contains a type variable and is not safe for the operation");
                throw new IllegalArgumentException(sb.toString());
            }
            
            @Override
            public void f(final WildcardType wildcardType) {
                this.a(wildcardType.getLowerBounds());
                this.a(wildcardType.getUpperBounds());
            }
        }.a(this.runtimeType);
        return this;
    }
    
    public final TypeToken<?> resolveType(final Type type) {
        i71.r(type);
        return of(this.getInvariantTypeResolver().j(type));
    }
    
    @Override
    public String toString() {
        return Types.s(this.runtimeType);
    }
    
    public final TypeToken<T> unwrap() {
        if (this.isWrapper()) {
            return of((Class<T>)e81.c((Class)this.runtimeType));
        }
        return this;
    }
    
    public final <X> TypeToken<T> where(final nz1 nz1, final TypeToken<X> typeToken) {
        new b();
        throw null;
    }
    
    public final <X> TypeToken<T> where(final nz1 nz1, final Class<X> clazz) {
        return this.where(nz1, (TypeToken<Object>)of((Class<X>)clazz));
    }
    
    public final TypeToken<T> wrap() {
        if (this.isPrimitive()) {
            return of((Class<T>)e81.d((Class)this.runtimeType));
        }
        return this;
    }
    
    public Object writeReplace() {
        return of(new b().j(this.runtimeType));
    }
    
    public final class ClassSet extends TypeSet
    {
        private static final long serialVersionUID = 0L;
        private transient ImmutableSet<TypeToken<? super T>> classes;
        final TypeToken this$0;
        
        private ClassSet(final TypeToken this$0) {
            this.this$0 = this$0.super();
        }
        
        private Object readResolve() {
            return this.this$0.getTypes().classes();
        }
        
        @Override
        public TypeSet classes() {
            return this;
        }
        
        @Override
        public Set<TypeToken<? super T>> delegate() {
            ImmutableSet<TypeToken<? super T>> classes;
            if ((classes = this.classes) == null) {
                classes = c70.g(f.a.a().d(this.this$0)).c(TypeFilter.IGNORE_TYPE_VARIABLE_OR_WILDCARD).l();
                this.classes = classes;
            }
            return classes;
        }
        
        @Override
        public TypeSet interfaces() {
            throw new UnsupportedOperationException("classes().interfaces() not supported.");
        }
        
        @Override
        public Set<Class<? super T>> rawTypes() {
            return (Set<Class<? super T>>)ImmutableSet.copyOf((Collection<?>)f.b.a().c(this.this$0.getRawTypes()));
        }
    }
    
    public final class InterfaceSet extends TypeSet
    {
        private static final long serialVersionUID = 0L;
        private final transient TypeSet allTypes;
        private transient ImmutableSet<TypeToken<? super T>> interfaces;
        final TypeToken this$0;
        
        public InterfaceSet(final TypeToken this$0, final TypeSet allTypes) {
            this.this$0 = this$0.super();
            this.allTypes = allTypes;
        }
        
        private Object readResolve() {
            return this.this$0.getTypes().interfaces();
        }
        
        @Override
        public TypeSet classes() {
            throw new UnsupportedOperationException("interfaces().classes() not supported.");
        }
        
        @Override
        public Set<TypeToken<? super T>> delegate() {
            ImmutableSet<TypeToken<? super T>> interfaces;
            if ((interfaces = this.interfaces) == null) {
                interfaces = c70.g(this.allTypes).c(TypeFilter.INTERFACE_ONLY).l();
                this.interfaces = interfaces;
            }
            return interfaces;
        }
        
        @Override
        public TypeSet interfaces() {
            return this;
        }
        
        @Override
        public Set<Class<? super T>> rawTypes() {
            return c70.g(f.b.c(this.this$0.getRawTypes())).c(new pz1()).l();
        }
    }
    
    public static final class SimpleTypeToken<T> extends TypeToken<T>
    {
        private static final long serialVersionUID = 0L;
        
        public SimpleTypeToken(final Type type) {
            super(type, null);
        }
    }
    
    public enum TypeFilter implements m71
    {
        private static final TypeFilter[] $VALUES;
        
        IGNORE_TYPE_VARIABLE_OR_WILDCARD {
            public boolean apply(final TypeToken<?> typeToken) {
                return !(TypeToken.access$500((TypeToken<Object>)typeToken) instanceof TypeVariable) && !(TypeToken.access$500((TypeToken<Object>)typeToken) instanceof WildcardType);
            }
        }, 
        INTERFACE_ONLY {
            public boolean apply(final TypeToken<?> typeToken) {
                return typeToken.getRawType().isInterface();
            }
        };
        
        private static /* synthetic */ TypeFilter[] $values() {
            return new TypeFilter[] { TypeFilter.IGNORE_TYPE_VARIABLE_OR_WILDCARD, TypeFilter.INTERFACE_ONLY };
        }
        
        static {
            $VALUES = $values();
        }
    }
    
    public class TypeSet extends c80 implements Serializable
    {
        private static final long serialVersionUID = 0L;
        final TypeToken this$0;
        private transient ImmutableSet<TypeToken<? super T>> types;
        
        public TypeSet(final TypeToken this$0) {
            this.this$0 = this$0;
        }
        
        public TypeSet classes() {
            return (TypeSet)this.this$0.new ClassSet(null);
        }
        
        @Override
        public Set<TypeToken<? super T>> delegate() {
            ImmutableSet<TypeToken<? super T>> types;
            if ((types = this.types) == null) {
                types = c70.g(f.a.d(this.this$0)).c(TypeFilter.IGNORE_TYPE_VARIABLE_OR_WILDCARD).l();
                this.types = types;
            }
            return types;
        }
        
        public TypeSet interfaces() {
            return (TypeSet)new InterfaceSet(this);
        }
        
        public Set<Class<? super T>> rawTypes() {
            return (Set<Class<? super T>>)ImmutableSet.copyOf((Collection<?>)f.b.c(this.this$0.getRawTypes()));
        }
    }
    
    public static class e
    {
        public final Type[] a;
        public final boolean b;
        
        public e(final Type[] a, final boolean b) {
            this.a = a;
            this.b = b;
        }
        
        public boolean a(final Type type) {
            final Type[] a = this.a;
            for (int length = a.length, i = 0; i < length; ++i) {
                final boolean subtype = TypeToken.of(a[i]).isSubtypeOf(type);
                final boolean b = this.b;
                if (subtype == b) {
                    return b;
                }
            }
            return this.b ^ true;
        }
        
        public boolean b(final Type type) {
            final TypeToken<?> of = TypeToken.of(type);
            final Type[] a = this.a;
            for (int length = a.length, i = 0; i < length; ++i) {
                final boolean subtype = of.isSubtypeOf(a[i]);
                final boolean b = this.b;
                if (subtype == b) {
                    return b;
                }
            }
            return this.b ^ true;
        }
    }
    
    public abstract static class f
    {
        public static final f a;
        public static final f b;
        
        static {
            a = new f() {
                public Iterable i(final TypeToken typeToken) {
                    return typeToken.getGenericInterfaces();
                }
                
                public Class j(final TypeToken typeToken) {
                    return typeToken.getRawType();
                }
                
                public TypeToken k(final TypeToken typeToken) {
                    return typeToken.getGenericSuperclass();
                }
            };
            b = new f() {
                public Iterable i(final Class clazz) {
                    return Arrays.asList(clazz.getInterfaces());
                }
                
                public Class j(final Class clazz) {
                    return clazz;
                }
                
                public Class k(final Class clazz) {
                    return clazz.getSuperclass();
                }
            };
        }
        
        public static ImmutableList h(final Map map, final Comparator comparator) {
            return new Ordering(comparator, map) {
                public final Comparator a;
                public final Map b;
                
                @Override
                public int compare(Object value, Object value2) {
                    final Comparator a = this.a;
                    value = this.b.get(value);
                    Objects.requireNonNull(value);
                    value2 = this.b.get(value2);
                    Objects.requireNonNull(value2);
                    return a.compare(value, value2);
                }
            }.immutableSortedCopy((Iterable<Object>)map.keySet());
        }
        
        public final f a() {
            return new e(this, this) {
                @Override
                public ImmutableList c(final Iterable iterable) {
                    final ImmutableList.a builder = ImmutableList.builder();
                    for (final Object next : iterable) {
                        if (!((e)this).f(next).isInterface()) {
                            builder.i(next);
                        }
                    }
                    return super.c(builder.l());
                }
                
                @Override
                public Iterable e(final Object o) {
                    return ImmutableSet.of();
                }
            };
        }
        
        public final int b(final Object o, final Map map) {
            final Integer n = map.get(o);
            if (n != null) {
                return n;
            }
            int n2 = this.f(o).isInterface() ? 1 : 0;
            final Iterator iterator = this.e(o).iterator();
            while (iterator.hasNext()) {
                n2 = Math.max(n2, this.b(iterator.next(), map));
            }
            final Object g = this.g(o);
            int max = n2;
            if (g != null) {
                max = Math.max(n2, this.b(g, map));
            }
            final int i = max + 1;
            map.put(o, i);
            return i;
        }
        
        public ImmutableList c(final Iterable iterable) {
            final HashMap p = Maps.p();
            final Iterator iterator = iterable.iterator();
            while (iterator.hasNext()) {
                this.b(iterator.next(), p);
            }
            return h(p, Ordering.natural().reverse());
        }
        
        public final ImmutableList d(final Object o) {
            return this.c(ImmutableList.of(o));
        }
        
        public abstract Iterable e(final Object p0);
        
        public abstract Class f(final Object p0);
        
        public abstract Object g(final Object p0);
        
        public abstract static class e extends f
        {
            public final f c;
            
            public e(final f c) {
                super(null);
                this.c = c;
            }
            
            @Override
            public Class f(final Object o) {
                return this.c.f(o);
            }
            
            @Override
            public Object g(final Object o) {
                return this.c.g(o);
            }
        }
    }
}

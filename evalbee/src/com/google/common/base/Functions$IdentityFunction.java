// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.base;

enum Functions$IdentityFunction implements m90
{
    private static final Functions$IdentityFunction[] $VALUES;
    
    INSTANCE;
    
    private static /* synthetic */ Functions$IdentityFunction[] $values() {
        return new Functions$IdentityFunction[] { Functions$IdentityFunction.INSTANCE };
    }
    
    static {
        $VALUES = $values();
    }
    
    @Override
    public Object apply(final Object o) {
        return o;
    }
    
    @Override
    public String toString() {
        return "Functions.identity()";
    }
}

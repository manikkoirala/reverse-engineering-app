// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.base;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.Serializable;

final class JdkPattern extends hi implements Serializable
{
    private static final long serialVersionUID = 0L;
    private final Pattern pattern;
    
    public JdkPattern(final Pattern pattern) {
        this.pattern = (Pattern)i71.r(pattern);
    }
    
    @Override
    public int flags() {
        return this.pattern.flags();
    }
    
    @Override
    public ei matcher(final CharSequence input) {
        return new a(this.pattern.matcher(input));
    }
    
    @Override
    public String pattern() {
        return this.pattern.pattern();
    }
    
    @Override
    public String toString() {
        return this.pattern.toString();
    }
    
    public static final class a extends ei
    {
        public final Matcher a;
        
        public a(final Matcher matcher) {
            this.a = (Matcher)i71.r(matcher);
        }
        
        @Override
        public boolean a() {
            return this.a.find();
        }
    }
}

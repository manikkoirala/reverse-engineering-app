// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.base;

import java.io.Serializable;

final class FunctionalEquivalence<F, T> extends Equivalence implements Serializable
{
    private static final long serialVersionUID = 0L;
    private final m90 function;
    private final Equivalence resultEquivalence;
    
    public FunctionalEquivalence(final m90 m90, final Equivalence equivalence) {
        this.function = (m90)i71.r(m90);
        this.resultEquivalence = (Equivalence)i71.r(equivalence);
    }
    
    @Override
    public boolean doEquivalent(final F n, final F n2) {
        return this.resultEquivalence.equivalent(this.function.apply(n), this.function.apply(n2));
    }
    
    @Override
    public int doHash(final F n) {
        return this.resultEquivalence.hash(this.function.apply(n));
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof FunctionalEquivalence) {
            final FunctionalEquivalence functionalEquivalence = (FunctionalEquivalence)o;
            if (!this.function.equals(functionalEquivalence.function) || !this.resultEquivalence.equals(functionalEquivalence.resultEquivalence)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return b11.b(this.function, this.resultEquivalence);
    }
    
    @Override
    public String toString() {
        final String value = String.valueOf(this.resultEquivalence);
        final String value2 = String.valueOf(this.function);
        final StringBuilder sb = new StringBuilder(value.length() + 13 + value2.length());
        sb.append(value);
        sb.append(".onResultOf(");
        sb.append(value2);
        sb.append(")");
        return sb.toString();
    }
}

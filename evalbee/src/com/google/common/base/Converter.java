// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.base;

import java.io.Serializable;
import java.util.Iterator;

public abstract class Converter implements m90
{
    private final boolean handleNullAutomatically;
    private transient Converter reverse;
    
    public Converter() {
        this(true);
    }
    
    public Converter(final boolean handleNullAutomatically) {
        this.handleNullAutomatically = handleNullAutomatically;
    }
    
    public static <A, B> Converter from(final m90 m90, final m90 m91) {
        return new FunctionBasedConverter<Object, Object>(m90, m91, null);
    }
    
    public static <T> Converter identity() {
        return IdentityConverter.INSTANCE;
    }
    
    public final Object a(final Object o) {
        return this.doBackward(m01.a(o));
    }
    
    public final <C> Converter andThen(final Converter converter) {
        return this.doAndThen(converter);
    }
    
    @Deprecated
    @Override
    public final Object apply(final Object o) {
        return this.convert(o);
    }
    
    public final Object b(final Object o) {
        return this.doForward(m01.a(o));
    }
    
    public final Object convert(final Object o) {
        return this.correctedDoForward(o);
    }
    
    public Iterable<Object> convertAll(final Iterable<Object> iterable) {
        i71.s(iterable, "fromIterable");
        return new Iterable(this, iterable) {
            public final Iterable a;
            public final Converter b;
            
            @Override
            public Iterator iterator() {
                return new Iterator(this) {
                    public final Iterator a = b.a.iterator();
                    public final Converter$a b;
                    
                    @Override
                    public boolean hasNext() {
                        return this.a.hasNext();
                    }
                    
                    @Override
                    public Object next() {
                        return this.b.b.convert(this.a.next());
                    }
                    
                    @Override
                    public void remove() {
                        this.a.remove();
                    }
                };
            }
        };
    }
    
    public Object correctedDoBackward(Object r) {
        if (this.handleNullAutomatically) {
            if (r == null) {
                r = null;
            }
            else {
                r = i71.r(this.doBackward(r));
            }
            return r;
        }
        return this.a(r);
    }
    
    public Object correctedDoForward(Object r) {
        if (this.handleNullAutomatically) {
            if (r == null) {
                r = null;
            }
            else {
                r = i71.r(this.doForward(r));
            }
            return r;
        }
        return this.b(r);
    }
    
    public <C> Converter doAndThen(final Converter converter) {
        return new ConverterComposition<Object, Object, Object>(this, (Converter)i71.r(converter));
    }
    
    public abstract Object doBackward(final Object p0);
    
    public abstract Object doForward(final Object p0);
    
    @Override
    public boolean equals(final Object obj) {
        return super.equals(obj);
    }
    
    public Converter reverse() {
        Converter reverse;
        if ((reverse = this.reverse) == null) {
            reverse = new ReverseConverter<Object, Object>(this);
            this.reverse = reverse;
        }
        return reverse;
    }
    
    public static final class ConverterComposition<A, B, C> extends Converter implements Serializable
    {
        private static final long serialVersionUID = 0L;
        final Converter first;
        final Converter second;
        
        public ConverterComposition(final Converter first, final Converter second) {
            this.first = first;
            this.second = second;
        }
        
        @Override
        public A correctedDoBackward(final C c) {
            return (A)this.first.correctedDoBackward(this.second.correctedDoBackward(c));
        }
        
        @Override
        public C correctedDoForward(final A a) {
            return (C)this.second.correctedDoForward(this.first.correctedDoForward(a));
        }
        
        @Override
        public A doBackward(final C c) {
            throw new AssertionError();
        }
        
        @Override
        public C doForward(final A a) {
            throw new AssertionError();
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof ConverterComposition;
            boolean b3;
            final boolean b2 = b3 = false;
            if (b) {
                final ConverterComposition converterComposition = (ConverterComposition)o;
                b3 = b2;
                if (this.first.equals(converterComposition.first)) {
                    b3 = b2;
                    if (this.second.equals(converterComposition.second)) {
                        b3 = true;
                    }
                }
            }
            return b3;
        }
        
        @Override
        public int hashCode() {
            return this.first.hashCode() * 31 + this.second.hashCode();
        }
        
        @Override
        public String toString() {
            final String value = String.valueOf(this.first);
            final String value2 = String.valueOf(this.second);
            final StringBuilder sb = new StringBuilder(value.length() + 10 + value2.length());
            sb.append(value);
            sb.append(".andThen(");
            sb.append(value2);
            sb.append(")");
            return sb.toString();
        }
    }
    
    public static final class FunctionBasedConverter<A, B> extends Converter implements Serializable
    {
        private final m90 backwardFunction;
        private final m90 forwardFunction;
        
        private FunctionBasedConverter(final m90 m90, final m90 m91) {
            this.forwardFunction = (m90)i71.r(m90);
            this.backwardFunction = (m90)i71.r(m91);
        }
        
        @Override
        public A doBackward(final B b) {
            return (A)this.backwardFunction.apply(b);
        }
        
        @Override
        public B doForward(final A a) {
            return (B)this.forwardFunction.apply(a);
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof FunctionBasedConverter;
            boolean b3;
            final boolean b2 = b3 = false;
            if (b) {
                final FunctionBasedConverter functionBasedConverter = (FunctionBasedConverter)o;
                b3 = b2;
                if (this.forwardFunction.equals(functionBasedConverter.forwardFunction)) {
                    b3 = b2;
                    if (this.backwardFunction.equals(functionBasedConverter.backwardFunction)) {
                        b3 = true;
                    }
                }
            }
            return b3;
        }
        
        @Override
        public int hashCode() {
            return this.forwardFunction.hashCode() * 31 + this.backwardFunction.hashCode();
        }
        
        @Override
        public String toString() {
            final String value = String.valueOf(this.forwardFunction);
            final String value2 = String.valueOf(this.backwardFunction);
            final StringBuilder sb = new StringBuilder(value.length() + 18 + value2.length());
            sb.append("Converter.from(");
            sb.append(value);
            sb.append(", ");
            sb.append(value2);
            sb.append(")");
            return sb.toString();
        }
    }
    
    public static final class IdentityConverter<T> extends Converter implements Serializable
    {
        static final IdentityConverter<?> INSTANCE;
        private static final long serialVersionUID = 0L;
        
        static {
            INSTANCE = new IdentityConverter<Object>();
        }
        
        private IdentityConverter() {
        }
        
        private Object readResolve() {
            return IdentityConverter.INSTANCE;
        }
        
        @Override
        public <S> Converter doAndThen(final Converter converter) {
            return (Converter)i71.s(converter, "otherConverter");
        }
        
        @Override
        public T doBackward(final T t) {
            return t;
        }
        
        @Override
        public T doForward(final T t) {
            return t;
        }
        
        @Override
        public IdentityConverter<T> reverse() {
            return this;
        }
        
        @Override
        public String toString() {
            return "Converter.identity()";
        }
    }
    
    public static final class ReverseConverter<A, B> extends Converter implements Serializable
    {
        private static final long serialVersionUID = 0L;
        final Converter original;
        
        public ReverseConverter(final Converter original) {
            this.original = original;
        }
        
        @Override
        public B correctedDoBackward(final A a) {
            return (B)this.original.correctedDoForward(a);
        }
        
        @Override
        public A correctedDoForward(final B b) {
            return (A)this.original.correctedDoBackward(b);
        }
        
        @Override
        public B doBackward(final A a) {
            throw new AssertionError();
        }
        
        @Override
        public A doForward(final B b) {
            throw new AssertionError();
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof ReverseConverter && this.original.equals(((ReverseConverter)o).original);
        }
        
        @Override
        public int hashCode() {
            return ~this.original.hashCode();
        }
        
        @Override
        public Converter reverse() {
            return this.original;
        }
        
        @Override
        public String toString() {
            final String value = String.valueOf(this.original);
            final StringBuilder sb = new StringBuilder(value.length() + 10);
            sb.append(value);
            sb.append(".reverse()");
            return sb.toString();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.base;

import java.io.Serializable;

public abstract class Equivalence
{
    public static Equivalence equals() {
        return Equals.INSTANCE;
    }
    
    public static Equivalence identity() {
        return Identity.INSTANCE;
    }
    
    public abstract boolean doEquivalent(final Object p0, final Object p1);
    
    public abstract int doHash(final Object p0);
    
    public final boolean equivalent(final Object o, final Object o2) {
        return o == o2 || (o != null && o2 != null && this.doEquivalent(o, o2));
    }
    
    public final m71 equivalentTo(final Object o) {
        return new EquivalentToPredicate<Object>(this, o);
    }
    
    public final int hash(final Object o) {
        if (o == null) {
            return 0;
        }
        return this.doHash(o);
    }
    
    public final <F> Equivalence onResultOf(final m90 m90) {
        return new FunctionalEquivalence<Object, Object>(m90, this);
    }
    
    public final <S> Equivalence pairwise() {
        return new PairwiseEquivalence<Object, Object>(this);
    }
    
    public final <S> Wrapper<S> wrap(final S n) {
        return new Wrapper<S>(this, n, null);
    }
    
    public static final class Equals extends Equivalence implements Serializable
    {
        static final Equals INSTANCE;
        private static final long serialVersionUID = 1L;
        
        static {
            INSTANCE = new Equals();
        }
        
        private Object readResolve() {
            return Equals.INSTANCE;
        }
        
        @Override
        public boolean doEquivalent(final Object o, final Object obj) {
            return o.equals(obj);
        }
        
        @Override
        public int doHash(final Object o) {
            return o.hashCode();
        }
    }
    
    public static final class EquivalentToPredicate<T> implements m71, Serializable
    {
        private static final long serialVersionUID = 0L;
        private final Equivalence equivalence;
        private final T target;
        
        public EquivalentToPredicate(final Equivalence equivalence, final T target) {
            this.equivalence = (Equivalence)i71.r(equivalence);
            this.target = target;
        }
        
        @Override
        public boolean apply(final T t) {
            return this.equivalence.equivalent(t, this.target);
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (o instanceof EquivalentToPredicate) {
                final EquivalentToPredicate equivalentToPredicate = (EquivalentToPredicate)o;
                if (!this.equivalence.equals(equivalentToPredicate.equivalence) || !b11.a(this.target, equivalentToPredicate.target)) {
                    b = false;
                }
                return b;
            }
            return false;
        }
        
        @Override
        public int hashCode() {
            return b11.b(this.equivalence, this.target);
        }
        
        @Override
        public String toString() {
            final String value = String.valueOf(this.equivalence);
            final String value2 = String.valueOf(this.target);
            final StringBuilder sb = new StringBuilder(value.length() + 15 + value2.length());
            sb.append(value);
            sb.append(".equivalentTo(");
            sb.append(value2);
            sb.append(")");
            return sb.toString();
        }
    }
    
    public static final class Identity extends Equivalence implements Serializable
    {
        static final Identity INSTANCE;
        private static final long serialVersionUID = 1L;
        
        static {
            INSTANCE = new Identity();
        }
        
        private Object readResolve() {
            return Identity.INSTANCE;
        }
        
        @Override
        public boolean doEquivalent(final Object o, final Object o2) {
            return false;
        }
        
        @Override
        public int doHash(final Object o) {
            return System.identityHashCode(o);
        }
    }
    
    public static final class Wrapper<T> implements Serializable
    {
        private static final long serialVersionUID = 0L;
        private final Equivalence equivalence;
        private final T reference;
        
        private Wrapper(final Equivalence equivalence, final T reference) {
            this.equivalence = (Equivalence)i71.r(equivalence);
            this.reference = reference;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (o == this) {
                return true;
            }
            if (o instanceof Wrapper) {
                final Wrapper wrapper = (Wrapper)o;
                if (this.equivalence.equals(wrapper.equivalence)) {
                    return this.equivalence.equivalent(this.reference, wrapper.reference);
                }
            }
            return false;
        }
        
        public T get() {
            return this.reference;
        }
        
        @Override
        public int hashCode() {
            return this.equivalence.hash(this.reference);
        }
        
        @Override
        public String toString() {
            final String value = String.valueOf(this.equivalence);
            final String value2 = String.valueOf(this.reference);
            final StringBuilder sb = new StringBuilder(value.length() + 7 + value2.length());
            sb.append(value);
            sb.append(".wrap(");
            sb.append(value2);
            sb.append(")");
            return sb.toString();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.base;

import java.util.Map;
import java.io.Serializable;

class Functions$FunctionForMapNoDefault<K, V> implements m90, Serializable
{
    private static final long serialVersionUID = 0L;
    final Map<K, V> map;
    
    public Functions$FunctionForMapNoDefault(final Map<K, V> map) {
        this.map = (Map)i71.r(map);
    }
    
    @Override
    public V apply(final K k) {
        final V value = this.map.get(k);
        i71.m(value != null || this.map.containsKey(k), "Key '%s' not present in map", k);
        return (V)m01.a(value);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof Functions$FunctionForMapNoDefault && this.map.equals(((Functions$FunctionForMapNoDefault)o).map);
    }
    
    @Override
    public int hashCode() {
        return this.map.hashCode();
    }
    
    @Override
    public String toString() {
        final String value = String.valueOf(this.map);
        final StringBuilder sb = new StringBuilder(value.length() + 18);
        sb.append("Functions.forMap(");
        sb.append(value);
        sb.append(")");
        return sb.toString();
    }
}

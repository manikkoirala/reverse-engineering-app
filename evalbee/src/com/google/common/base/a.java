// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.base;

import java.util.Arrays;
import java.lang.reflect.Array;
import java.util.Map;
import java.util.Collection;

public abstract class a
{
    public static Object a(final Object o, final Object o2) {
        if (o != null) {
            return o;
        }
        if (o2 != null) {
            return o2;
        }
        throw new NullPointerException("Both parameters are null");
    }
    
    public static b b(final Class clazz) {
        return new b(clazz.getSimpleName(), null);
    }
    
    public static b c(final Object o) {
        return new b(o.getClass().getSimpleName(), null);
    }
    
    public static final class b
    {
        public final String a;
        public final com.google.common.base.a.b.b b;
        public com.google.common.base.a.b.b c;
        public boolean d;
        public boolean e;
        
        public b(final String s) {
            final com.google.common.base.a.b.b b = new com.google.common.base.a.b.b(null);
            this.b = b;
            this.c = b;
            this.d = false;
            this.e = false;
            this.a = (String)i71.r(s);
        }
        
        public static boolean l(final Object o) {
            final boolean b = o instanceof CharSequence;
            final boolean b2 = true;
            boolean b3 = true;
            if (b) {
                if (((CharSequence)o).length() != 0) {
                    b3 = false;
                }
                return b3;
            }
            if (o instanceof Collection) {
                return ((Collection)o).isEmpty();
            }
            if (o instanceof Map) {
                return ((Map)o).isEmpty();
            }
            if (o instanceof Optional) {
                return ((Optional)o).isPresent() ^ true;
            }
            return o.getClass().isArray() && Array.getLength(o) == 0 && b2;
        }
        
        public com.google.common.base.a.b a(final String s, final double d) {
            return this.j(s, String.valueOf(d));
        }
        
        public com.google.common.base.a.b b(final String s, final int i) {
            return this.j(s, String.valueOf(i));
        }
        
        public com.google.common.base.a.b c(final String s, final long l) {
            return this.j(s, String.valueOf(l));
        }
        
        public com.google.common.base.a.b d(final String s, final Object o) {
            return this.h(s, o);
        }
        
        public com.google.common.base.a.b e(final String s, final boolean b) {
            return this.j(s, String.valueOf(b));
        }
        
        public final com.google.common.base.a.b.b f() {
            final com.google.common.base.a.b.b b = new com.google.common.base.a.b.b(null);
            this.c.c = b;
            return this.c = b;
        }
        
        public final com.google.common.base.a.b g(final Object b) {
            this.f().b = b;
            return this;
        }
        
        public final com.google.common.base.a.b h(final String s, final Object b) {
            final com.google.common.base.a.b.b f = this.f();
            f.b = b;
            f.a = (String)i71.r(s);
            return this;
        }
        
        public final a i() {
            final a a = new a(null);
            this.c.c = (com.google.common.base.a.b.b)a;
            return (a)(this.c = (com.google.common.base.a.b.b)a);
        }
        
        public final com.google.common.base.a.b j(final String s, final Object b) {
            final a i = this.i();
            ((com.google.common.base.a.b.b)i).b = b;
            ((com.google.common.base.a.b.b)i).a = (String)i71.r(s);
            return this;
        }
        
        public com.google.common.base.a.b k(final Object o) {
            return this.g(o);
        }
        
        public com.google.common.base.a.b m() {
            this.d = true;
            return this;
        }
        
        @Override
        public String toString() {
            final boolean d = this.d;
            final boolean e = this.e;
            final StringBuilder sb = new StringBuilder(32);
            sb.append(this.a);
            sb.append('{');
            com.google.common.base.a.b.b b = this.b.c;
            String str = "";
            while (b != null) {
                final Object b2 = b.b;
                String s = null;
                Label_0196: {
                    if (!(b instanceof a)) {
                        if (b2 == null) {
                            s = str;
                            if (d) {
                                break Label_0196;
                            }
                        }
                        else if (e) {
                            s = str;
                            if (l(b2)) {
                                break Label_0196;
                            }
                        }
                    }
                    sb.append(str);
                    final String a = b.a;
                    if (a != null) {
                        sb.append(a);
                        sb.append('=');
                    }
                    if (b2 != null && b2.getClass().isArray()) {
                        final String deepToString = Arrays.deepToString(new Object[] { b2 });
                        sb.append(deepToString, 1, deepToString.length() - 1);
                    }
                    else {
                        sb.append(b2);
                    }
                    s = ", ";
                }
                b = b.c;
                str = s;
            }
            sb.append('}');
            return sb.toString();
        }
        
        public static final class a extends com.google.common.base.a.b.b
        {
            public a() {
                super(null);
            }
        }
        
        public static class b
        {
            public String a;
            public Object b;
            public b c;
        }
    }
}

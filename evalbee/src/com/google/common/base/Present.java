// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.base;

import java.util.Collections;
import java.util.Set;

final class Present<T> extends Optional<T>
{
    private static final long serialVersionUID = 0L;
    private final T reference;
    
    public Present(final T reference) {
        this.reference = reference;
    }
    
    @Override
    public Set<T> asSet() {
        return Collections.singleton(this.reference);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof Present && this.reference.equals(((Present)o).reference);
    }
    
    @Override
    public T get() {
        return this.reference;
    }
    
    @Override
    public int hashCode() {
        return this.reference.hashCode() + 1502476572;
    }
    
    @Override
    public boolean isPresent() {
        return true;
    }
    
    @Override
    public Optional<T> or(final Optional<? extends T> optional) {
        i71.r(optional);
        return this;
    }
    
    @Override
    public T or(final is1 is1) {
        i71.r(is1);
        return this.reference;
    }
    
    @Override
    public T or(final T t) {
        i71.s(t, "use Optional.orNull() instead of Optional.or(null)");
        return this.reference;
    }
    
    @Override
    public T orNull() {
        return this.reference;
    }
    
    @Override
    public String toString() {
        final String value = String.valueOf(this.reference);
        final StringBuilder sb = new StringBuilder(value.length() + 13);
        sb.append("Optional.of(");
        sb.append(value);
        sb.append(")");
        return sb.toString();
    }
    
    @Override
    public <V> Optional<V> transform(final m90 m90) {
        return new Present<V>((V)i71.s(m90.apply(this.reference), "the Function passed to Optional.transform() must not return null."));
    }
}

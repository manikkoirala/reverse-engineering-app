// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.base;

import java.io.Serializable;

final class Enums$StringConverter<T extends Enum<T>> extends Converter implements Serializable
{
    private static final long serialVersionUID = 0L;
    private final Class<T> enumClass;
    
    public Enums$StringConverter(final Class<T> clazz) {
        this.enumClass = (Class)i71.r(clazz);
    }
    
    public String doBackward(final T t) {
        return t.name();
    }
    
    public T doForward(final String name) {
        return Enum.valueOf(this.enumClass, name);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof Enums$StringConverter && this.enumClass.equals(((Enums$StringConverter)o).enumClass);
    }
    
    @Override
    public int hashCode() {
        return this.enumClass.hashCode();
    }
    
    @Override
    public String toString() {
        final String name = this.enumClass.getName();
        final StringBuilder sb = new StringBuilder(name.length() + 29);
        sb.append("Enums.stringConverter(");
        sb.append(name);
        sb.append(".class)");
        return sb.toString();
    }
}

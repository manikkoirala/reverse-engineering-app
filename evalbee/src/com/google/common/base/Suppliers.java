// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.base;

import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.io.Serializable;

public abstract class Suppliers
{
    public static is1 a(final is1 is1) {
        if (!(is1 instanceof a) && !(is1 instanceof MemoizingSupplier)) {
            is1 is2;
            if (is1 instanceof Serializable) {
                is2 = new MemoizingSupplier<Object>(is1);
            }
            else {
                is2 = new a(is1);
            }
            return is2;
        }
        return is1;
    }
    
    public static is1 b(final Object o) {
        return new SupplierOfInstance<Object>(o);
    }
    
    public static class ExpiringMemoizingSupplier<T> implements is1, Serializable
    {
        private static final long serialVersionUID = 0L;
        final is1 delegate;
        final long durationNanos;
        transient volatile long expirationNanos;
        transient volatile T value;
        
        public ExpiringMemoizingSupplier(final is1 is1, final long duration, final TimeUnit timeUnit) {
            this.delegate = (is1)i71.r(is1);
            this.durationNanos = timeUnit.toNanos(duration);
            i71.l(duration > 0L, "duration (%s %s) must be > 0", duration, timeUnit);
        }
        
        @Override
        public T get() {
            final long expirationNanos = this.expirationNanos;
            final long g = b.g();
            Label_0080: {
                if (expirationNanos != 0L && g - expirationNanos < 0L) {
                    break Label_0080;
                }
                synchronized (this) {
                    if (expirationNanos == this.expirationNanos) {
                        final Object value = this.delegate.get();
                        this.value = (T)value;
                        long expirationNanos2;
                        if ((expirationNanos2 = g + this.durationNanos) == 0L) {
                            expirationNanos2 = 1L;
                        }
                        this.expirationNanos = expirationNanos2;
                        return (T)value;
                    }
                    monitorexit(this);
                    return (T)m01.a(this.value);
                }
            }
        }
        
        @Override
        public String toString() {
            final String value = String.valueOf(this.delegate);
            final long durationNanos = this.durationNanos;
            final StringBuilder sb = new StringBuilder(value.length() + 62);
            sb.append("Suppliers.memoizeWithExpiration(");
            sb.append(value);
            sb.append(", ");
            sb.append(durationNanos);
            sb.append(", NANOS)");
            return sb.toString();
        }
    }
    
    public static class MemoizingSupplier<T> implements is1, Serializable
    {
        private static final long serialVersionUID = 0L;
        final is1 delegate;
        transient volatile boolean initialized;
        transient T value;
        
        public MemoizingSupplier(final is1 is1) {
            this.delegate = (is1)i71.r(is1);
        }
        
        @Override
        public T get() {
            if (!this.initialized) {
                synchronized (this) {
                    if (!this.initialized) {
                        final Object value = this.delegate.get();
                        this.value = (T)value;
                        this.initialized = true;
                        return (T)value;
                    }
                }
            }
            return (T)m01.a(this.value);
        }
        
        @Override
        public String toString() {
            Object obj;
            if (this.initialized) {
                final String value = String.valueOf(this.value);
                final StringBuilder sb = new StringBuilder(value.length() + 25);
                sb.append("<supplier that returned ");
                sb.append(value);
                sb.append(">");
                obj = sb.toString();
            }
            else {
                obj = this.delegate;
            }
            final String value2 = String.valueOf(obj);
            final StringBuilder sb2 = new StringBuilder(value2.length() + 19);
            sb2.append("Suppliers.memoize(");
            sb2.append(value2);
            sb2.append(")");
            return sb2.toString();
        }
    }
    
    public static class SupplierComposition<F, T> implements is1, Serializable
    {
        private static final long serialVersionUID = 0L;
        final m90 function;
        final is1 supplier;
        
        public SupplierComposition(final m90 m90, final is1 is1) {
            this.function = (m90)i71.r(m90);
            this.supplier = (is1)i71.r(is1);
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof SupplierComposition;
            boolean b3;
            final boolean b2 = b3 = false;
            if (b) {
                final SupplierComposition supplierComposition = (SupplierComposition)o;
                b3 = b2;
                if (this.function.equals(supplierComposition.function)) {
                    b3 = b2;
                    if (this.supplier.equals(supplierComposition.supplier)) {
                        b3 = true;
                    }
                }
            }
            return b3;
        }
        
        @Override
        public T get() {
            return (T)this.function.apply(this.supplier.get());
        }
        
        @Override
        public int hashCode() {
            return b11.b(this.function, this.supplier);
        }
        
        @Override
        public String toString() {
            final String value = String.valueOf(this.function);
            final String value2 = String.valueOf(this.supplier);
            final StringBuilder sb = new StringBuilder(value.length() + 21 + value2.length());
            sb.append("Suppliers.compose(");
            sb.append(value);
            sb.append(", ");
            sb.append(value2);
            sb.append(")");
            return sb.toString();
        }
    }
    
    public enum SupplierFunctionImpl implements m90
    {
        private static final SupplierFunctionImpl[] $VALUES;
        
        INSTANCE;
        
        private static /* synthetic */ SupplierFunctionImpl[] $values() {
            return new SupplierFunctionImpl[] { SupplierFunctionImpl.INSTANCE };
        }
        
        static {
            $VALUES = $values();
        }
        
        public Object apply(final is1 is1) {
            return is1.get();
        }
        
        @Override
        public String toString() {
            return "Suppliers.supplierFunction()";
        }
    }
    
    public static class SupplierOfInstance<T> implements is1, Serializable
    {
        private static final long serialVersionUID = 0L;
        final T instance;
        
        public SupplierOfInstance(final T instance) {
            this.instance = instance;
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof SupplierOfInstance && b11.a(this.instance, ((SupplierOfInstance)o).instance);
        }
        
        @Override
        public T get() {
            return this.instance;
        }
        
        @Override
        public int hashCode() {
            return b11.b(this.instance);
        }
        
        @Override
        public String toString() {
            final String value = String.valueOf(this.instance);
            final StringBuilder sb = new StringBuilder(value.length() + 22);
            sb.append("Suppliers.ofInstance(");
            sb.append(value);
            sb.append(")");
            return sb.toString();
        }
    }
    
    public static class ThreadSafeSupplier<T> implements is1, Serializable
    {
        private static final long serialVersionUID = 0L;
        final is1 delegate;
        
        public ThreadSafeSupplier(final is1 is1) {
            this.delegate = (is1)i71.r(is1);
        }
        
        @Override
        public T get() {
            synchronized (this.delegate) {
                return (T)this.delegate.get();
            }
        }
        
        @Override
        public String toString() {
            final String value = String.valueOf(this.delegate);
            final StringBuilder sb = new StringBuilder(value.length() + 32);
            sb.append("Suppliers.synchronizedSupplier(");
            sb.append(value);
            sb.append(")");
            return sb.toString();
        }
    }
    
    public static class a implements is1
    {
        public volatile is1 a;
        public volatile boolean b;
        public Object c;
        
        public a(final is1 is1) {
            this.a = (is1)i71.r(is1);
        }
        
        @Override
        public Object get() {
            if (!this.b) {
                synchronized (this) {
                    if (!this.b) {
                        final is1 a = this.a;
                        Objects.requireNonNull(a);
                        final Object value = a.get();
                        this.c = value;
                        this.b = true;
                        this.a = null;
                        return value;
                    }
                }
            }
            return m01.a(this.c);
        }
        
        @Override
        public String toString() {
            Object obj;
            if ((obj = this.a) == null) {
                final String value = String.valueOf(this.c);
                final StringBuilder sb = new StringBuilder(value.length() + 25);
                sb.append("<supplier that returned ");
                sb.append(value);
                sb.append(">");
                obj = sb.toString();
            }
            final String value2 = String.valueOf(obj);
            final StringBuilder sb2 = new StringBuilder(value2.length() + 19);
            sb2.append("Suppliers.memoize(");
            sb2.append(value2);
            sb2.append(")");
            return sb2.toString();
        }
    }
}

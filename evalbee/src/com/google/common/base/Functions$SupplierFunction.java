// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.base;

import java.io.Serializable;

class Functions$SupplierFunction<F, T> implements m90, Serializable
{
    private static final long serialVersionUID = 0L;
    private final is1 supplier;
    
    private Functions$SupplierFunction(final is1 is1) {
        this.supplier = (is1)i71.r(is1);
    }
    
    @Override
    public T apply(final F n) {
        return (T)this.supplier.get();
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof Functions$SupplierFunction && this.supplier.equals(((Functions$SupplierFunction)o).supplier);
    }
    
    @Override
    public int hashCode() {
        return this.supplier.hashCode();
    }
    
    @Override
    public String toString() {
        final String value = String.valueOf(this.supplier);
        final StringBuilder sb = new StringBuilder(value.length() + 23);
        sb.append("Functions.forSupplier(");
        sb.append(value);
        sb.append(")");
        return sb.toString();
    }
}

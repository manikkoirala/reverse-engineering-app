// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.base;

import java.io.Serializable;
import java.util.Objects;

public enum CaseFormat
{
    private static final CaseFormat[] $VALUES;
    
    LOWER_CAMEL(gg.d('A', 'Z'), "") {
        @Override
        public String normalizeFirstWord(final String s) {
            return d9.c(s);
        }
        
        @Override
        public String normalizeWord(final String s) {
            return firstCharOnlyToUpper(s);
        }
    }, 
    LOWER_HYPHEN(gg.f('-'), "-") {
        @Override
        public String convert(final CaseFormat caseFormat, final String s) {
            if (caseFormat == CaseFormat.LOWER_UNDERSCORE) {
                return s.replace('-', '_');
            }
            if (caseFormat == CaseFormat.UPPER_UNDERSCORE) {
                return d9.e(s.replace('-', '_'));
            }
            return super.convert(caseFormat, s);
        }
        
        @Override
        public String normalizeWord(final String s) {
            return d9.c(s);
        }
    }, 
    LOWER_UNDERSCORE(gg.f('_'), "_") {
        @Override
        public String convert(final CaseFormat caseFormat, final String s) {
            if (caseFormat == CaseFormat.LOWER_HYPHEN) {
                return s.replace('_', '-');
            }
            if (caseFormat == CaseFormat.UPPER_UNDERSCORE) {
                return d9.e(s);
            }
            return super.convert(caseFormat, s);
        }
        
        @Override
        public String normalizeWord(final String s) {
            return d9.c(s);
        }
    }, 
    UPPER_CAMEL(gg.d('A', 'Z'), "") {
        @Override
        public String normalizeWord(final String s) {
            return firstCharOnlyToUpper(s);
        }
    }, 
    UPPER_UNDERSCORE(gg.f('_'), "_") {
        @Override
        public String convert(final CaseFormat caseFormat, final String s) {
            if (caseFormat == CaseFormat.LOWER_HYPHEN) {
                return d9.c(s.replace('_', '-'));
            }
            if (caseFormat == CaseFormat.LOWER_UNDERSCORE) {
                return d9.c(s);
            }
            return super.convert(caseFormat, s);
        }
        
        @Override
        public String normalizeWord(final String s) {
            return d9.e(s);
        }
    };
    
    private final gg wordBoundary;
    private final String wordSeparator;
    
    private static /* synthetic */ CaseFormat[] $values() {
        return new CaseFormat[] { CaseFormat.LOWER_HYPHEN, CaseFormat.LOWER_UNDERSCORE, CaseFormat.LOWER_CAMEL, CaseFormat.UPPER_CAMEL, CaseFormat.UPPER_UNDERSCORE };
    }
    
    static {
        $VALUES = $values();
    }
    
    private CaseFormat(final gg wordBoundary, final String wordSeparator) {
        this.wordBoundary = wordBoundary;
        this.wordSeparator = wordSeparator;
    }
    
    private static String firstCharOnlyToUpper(String s) {
        if (!s.isEmpty()) {
            final char d = d9.d(s.charAt(0));
            s = d9.c(s.substring(1));
            final StringBuilder sb = new StringBuilder(String.valueOf(s).length() + 1);
            sb.append(d);
            sb.append(s);
            s = sb.toString();
        }
        return s;
    }
    
    public String convert(final CaseFormat caseFormat, final String s) {
        StringBuilder sb = null;
        int beginIndex = 0;
        int e = -1;
        while (true) {
            e = this.wordBoundary.e(s, e + 1);
            if (e == -1) {
                break;
            }
            String str;
            if (beginIndex == 0) {
                sb = new StringBuilder(s.length() + caseFormat.wordSeparator.length() * 4);
                str = caseFormat.normalizeFirstWord(s.substring(beginIndex, e));
            }
            else {
                Objects.requireNonNull(sb);
                str = caseFormat.normalizeWord(s.substring(beginIndex, e));
            }
            sb.append(str);
            sb.append(caseFormat.wordSeparator);
            beginIndex = this.wordSeparator.length() + e;
        }
        String s2;
        if (beginIndex == 0) {
            s2 = caseFormat.normalizeFirstWord(s);
        }
        else {
            Objects.requireNonNull(sb);
            sb.append(caseFormat.normalizeWord(s.substring(beginIndex)));
            s2 = sb.toString();
        }
        return s2;
    }
    
    public Converter converterTo(final CaseFormat caseFormat) {
        return new StringConverter(this, caseFormat);
    }
    
    public String normalizeFirstWord(final String s) {
        return this.normalizeWord(s);
    }
    
    public abstract String normalizeWord(final String p0);
    
    public final String to(final CaseFormat caseFormat, String convert) {
        i71.r(caseFormat);
        i71.r(convert);
        if (caseFormat != this) {
            convert = this.convert(caseFormat, convert);
        }
        return convert;
    }
    
    public static final class StringConverter extends Converter implements Serializable
    {
        private static final long serialVersionUID = 0L;
        private final CaseFormat sourceFormat;
        private final CaseFormat targetFormat;
        
        public StringConverter(final CaseFormat caseFormat, final CaseFormat caseFormat2) {
            this.sourceFormat = (CaseFormat)i71.r(caseFormat);
            this.targetFormat = (CaseFormat)i71.r(caseFormat2);
        }
        
        public String doBackward(final String s) {
            return this.targetFormat.to(this.sourceFormat, s);
        }
        
        public String doForward(final String s) {
            return this.sourceFormat.to(this.targetFormat, s);
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof StringConverter;
            boolean b3;
            final boolean b2 = b3 = false;
            if (b) {
                final StringConverter stringConverter = (StringConverter)o;
                b3 = b2;
                if (this.sourceFormat.equals(stringConverter.sourceFormat)) {
                    b3 = b2;
                    if (this.targetFormat.equals(stringConverter.targetFormat)) {
                        b3 = true;
                    }
                }
            }
            return b3;
        }
        
        @Override
        public int hashCode() {
            return this.sourceFormat.hashCode() ^ this.targetFormat.hashCode();
        }
        
        @Override
        public String toString() {
            final String value = String.valueOf(this.sourceFormat);
            final String value2 = String.valueOf(this.targetFormat);
            final StringBuilder sb = new StringBuilder(value.length() + 14 + value2.length());
            sb.append(value);
            sb.append(".converterTo(");
            sb.append(value2);
            sb.append(")");
            return sb.toString();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.base;

enum Functions$ToStringFunction implements m90
{
    private static final Functions$ToStringFunction[] $VALUES;
    
    INSTANCE;
    
    private static /* synthetic */ Functions$ToStringFunction[] $values() {
        return new Functions$ToStringFunction[] { Functions$ToStringFunction.INSTANCE };
    }
    
    static {
        $VALUES = $values();
    }
    
    @Override
    public String apply(final Object o) {
        i71.r(o);
        return o.toString();
    }
    
    @Override
    public String toString() {
        return "Functions.toStringFunction()";
    }
}

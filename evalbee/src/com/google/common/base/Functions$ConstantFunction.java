// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.base;

import java.io.Serializable;

class Functions$ConstantFunction<E> implements m90, Serializable
{
    private static final long serialVersionUID = 0L;
    private final E value;
    
    public Functions$ConstantFunction(final E value) {
        this.value = value;
    }
    
    @Override
    public E apply(final Object o) {
        return this.value;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof Functions$ConstantFunction && b11.a(this.value, ((Functions$ConstantFunction)o).value);
    }
    
    @Override
    public int hashCode() {
        final E value = this.value;
        int hashCode;
        if (value == null) {
            hashCode = 0;
        }
        else {
            hashCode = value.hashCode();
        }
        return hashCode;
    }
    
    @Override
    public String toString() {
        final String value = String.valueOf(this.value);
        final StringBuilder sb = new StringBuilder(value.length() + 20);
        sb.append("Functions.constant(");
        sb.append(value);
        sb.append(")");
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.base;

import java.util.Set;
import java.util.Iterator;
import java.io.Serializable;

public abstract class Optional<T> implements Serializable
{
    private static final long serialVersionUID = 0L;
    
    public static <T> Optional<T> absent() {
        return Absent.withType();
    }
    
    public static <T> Optional<T> fromNullable(final T t) {
        Optional<Object> absent;
        if (t == null) {
            absent = absent();
        }
        else {
            absent = new Present<Object>(t);
        }
        return (Optional<T>)absent;
    }
    
    public static <T> Optional<T> of(final T t) {
        return new Present<T>((T)i71.r(t));
    }
    
    public static <T> Iterable<T> presentInstances(final Iterable<? extends Optional<? extends T>> iterable) {
        i71.r(iterable);
        return new Iterable(iterable) {
            public final Iterable a;
            
            @Override
            public Iterator iterator() {
                return new AbstractIterator(this) {
                    public final Iterator c = (Iterator)i71.r(d.a.iterator());
                    public final Optional$a d;
                    
                    @Override
                    public Object b() {
                        while (this.c.hasNext()) {
                            final Optional optional = this.c.next();
                            if (optional.isPresent()) {
                                return optional.get();
                            }
                        }
                        return this.c();
                    }
                };
            }
        };
    }
    
    public abstract Set<T> asSet();
    
    @Override
    public abstract boolean equals(final Object p0);
    
    public abstract T get();
    
    @Override
    public abstract int hashCode();
    
    public abstract boolean isPresent();
    
    public abstract Optional<T> or(final Optional<? extends T> p0);
    
    public abstract T or(final is1 p0);
    
    public abstract T or(final T p0);
    
    public abstract T orNull();
    
    @Override
    public abstract String toString();
    
    public abstract <V> Optional<V> transform(final m90 p0);
}

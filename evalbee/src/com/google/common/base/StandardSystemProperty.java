// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.base;

public enum StandardSystemProperty
{
    private static final StandardSystemProperty[] $VALUES;
    
    FILE_SEPARATOR("file.separator"), 
    JAVA_CLASS_PATH("java.class.path"), 
    JAVA_CLASS_VERSION("java.class.version"), 
    JAVA_COMPILER("java.compiler"), 
    @Deprecated
    JAVA_EXT_DIRS("java.ext.dirs"), 
    JAVA_HOME("java.home"), 
    JAVA_IO_TMPDIR("java.io.tmpdir"), 
    JAVA_LIBRARY_PATH("java.library.path"), 
    JAVA_SPECIFICATION_NAME("java.specification.name"), 
    JAVA_SPECIFICATION_VENDOR("java.specification.vendor"), 
    JAVA_SPECIFICATION_VERSION("java.specification.version"), 
    JAVA_VENDOR("java.vendor"), 
    JAVA_VENDOR_URL("java.vendor.url"), 
    JAVA_VERSION("java.version"), 
    JAVA_VM_NAME("java.vm.name"), 
    JAVA_VM_SPECIFICATION_NAME("java.vm.specification.name"), 
    JAVA_VM_SPECIFICATION_VENDOR("java.vm.specification.vendor"), 
    JAVA_VM_SPECIFICATION_VERSION("java.vm.specification.version"), 
    JAVA_VM_VENDOR("java.vm.vendor"), 
    JAVA_VM_VERSION("java.vm.version"), 
    LINE_SEPARATOR("line.separator"), 
    OS_ARCH("os.arch"), 
    OS_NAME("os.name"), 
    OS_VERSION("os.version"), 
    PATH_SEPARATOR("path.separator"), 
    USER_DIR("user.dir"), 
    USER_HOME("user.home"), 
    USER_NAME("user.name");
    
    private final String key;
    
    private static /* synthetic */ StandardSystemProperty[] $values() {
        return new StandardSystemProperty[] { StandardSystemProperty.JAVA_VERSION, StandardSystemProperty.JAVA_VENDOR, StandardSystemProperty.JAVA_VENDOR_URL, StandardSystemProperty.JAVA_HOME, StandardSystemProperty.JAVA_VM_SPECIFICATION_VERSION, StandardSystemProperty.JAVA_VM_SPECIFICATION_VENDOR, StandardSystemProperty.JAVA_VM_SPECIFICATION_NAME, StandardSystemProperty.JAVA_VM_VERSION, StandardSystemProperty.JAVA_VM_VENDOR, StandardSystemProperty.JAVA_VM_NAME, StandardSystemProperty.JAVA_SPECIFICATION_VERSION, StandardSystemProperty.JAVA_SPECIFICATION_VENDOR, StandardSystemProperty.JAVA_SPECIFICATION_NAME, StandardSystemProperty.JAVA_CLASS_VERSION, StandardSystemProperty.JAVA_CLASS_PATH, StandardSystemProperty.JAVA_LIBRARY_PATH, StandardSystemProperty.JAVA_IO_TMPDIR, StandardSystemProperty.JAVA_COMPILER, StandardSystemProperty.JAVA_EXT_DIRS, StandardSystemProperty.OS_NAME, StandardSystemProperty.OS_ARCH, StandardSystemProperty.OS_VERSION, StandardSystemProperty.FILE_SEPARATOR, StandardSystemProperty.PATH_SEPARATOR, StandardSystemProperty.LINE_SEPARATOR, StandardSystemProperty.USER_NAME, StandardSystemProperty.USER_HOME, StandardSystemProperty.USER_DIR };
    }
    
    static {
        $VALUES = $values();
    }
    
    private StandardSystemProperty(final String key) {
        this.key = key;
    }
    
    public String key() {
        return this.key;
    }
    
    @Override
    public String toString() {
        final String key = this.key();
        final String value = this.value();
        final StringBuilder sb = new StringBuilder(String.valueOf(key).length() + 1 + String.valueOf(value).length());
        sb.append(key);
        sb.append("=");
        sb.append(value);
        return sb.toString();
    }
    
    public String value() {
        return System.getProperty(this.key);
    }
}

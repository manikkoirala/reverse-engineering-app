// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.base;

import java.util.Map;
import java.io.Serializable;

class Functions$ForMapWithDefault<K, V> implements m90, Serializable
{
    private static final long serialVersionUID = 0L;
    final V defaultValue;
    final Map<K, ? extends V> map;
    
    public Functions$ForMapWithDefault(final Map<K, ? extends V> map, final V defaultValue) {
        this.map = (Map)i71.r(map);
        this.defaultValue = defaultValue;
    }
    
    @Override
    public V apply(final K k) {
        final V value = (V)this.map.get(k);
        Object o;
        if (value == null && !this.map.containsKey(k)) {
            o = this.defaultValue;
        }
        else {
            o = m01.a(value);
        }
        return (V)o;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof Functions$ForMapWithDefault;
        boolean b3;
        final boolean b2 = b3 = false;
        if (b) {
            final Functions$ForMapWithDefault functions$ForMapWithDefault = (Functions$ForMapWithDefault)o;
            b3 = b2;
            if (this.map.equals(functions$ForMapWithDefault.map)) {
                b3 = b2;
                if (b11.a(this.defaultValue, functions$ForMapWithDefault.defaultValue)) {
                    b3 = true;
                }
            }
        }
        return b3;
    }
    
    @Override
    public int hashCode() {
        return b11.b(this.map, this.defaultValue);
    }
    
    @Override
    public String toString() {
        final String value = String.valueOf(this.map);
        final String value2 = String.valueOf(this.defaultValue);
        final StringBuilder sb = new StringBuilder(value.length() + 33 + value2.length());
        sb.append("Functions.forMap(");
        sb.append(value);
        sb.append(", defaultValue=");
        sb.append(value2);
        sb.append(")");
        return sb.toString();
    }
}

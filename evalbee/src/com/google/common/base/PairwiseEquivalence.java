// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.base;

import java.util.Iterator;
import java.io.Serializable;

final class PairwiseEquivalence<E, T extends E> extends Equivalence implements Serializable
{
    private static final long serialVersionUID = 1L;
    final Equivalence elementEquivalence;
    
    public PairwiseEquivalence(final Equivalence equivalence) {
        this.elementEquivalence = (Equivalence)i71.r(equivalence);
    }
    
    public boolean doEquivalent(final Iterable<T> iterable, final Iterable<T> iterable2) {
        final Iterator<T> iterator = iterable.iterator();
        final Iterator<T> iterator2 = iterable2.iterator();
        do {
            final boolean hasNext = iterator.hasNext();
            final boolean b = false;
            if (hasNext && iterator2.hasNext()) {
                continue;
            }
            boolean b2 = b;
            if (!iterator.hasNext()) {
                b2 = b;
                if (!iterator2.hasNext()) {
                    b2 = true;
                }
            }
            return b2;
        } while (this.elementEquivalence.equivalent(iterator.next(), iterator2.next()));
        return false;
    }
    
    public int doHash(final Iterable<T> iterable) {
        final Iterator<T> iterator = iterable.iterator();
        int n = 78721;
        while (iterator.hasNext()) {
            n = n * 24943 + this.elementEquivalence.hash(iterator.next());
        }
        return n;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof PairwiseEquivalence && this.elementEquivalence.equals(((PairwiseEquivalence)o).elementEquivalence);
    }
    
    @Override
    public int hashCode() {
        return this.elementEquivalence.hashCode() ^ 0x46A3EB07;
    }
    
    @Override
    public String toString() {
        final String value = String.valueOf(this.elementEquivalence);
        final StringBuilder sb = new StringBuilder(value.length() + 11);
        sb.append(value);
        sb.append(".pairwise()");
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.base;

import java.util.regex.Pattern;
import java.util.Locale;
import java.util.logging.Logger;

public abstract class b
{
    public static final Logger a;
    public static final y41 b;
    
    static {
        a = Logger.getLogger(b.class.getName());
        b = d();
    }
    
    public static hi a(final String s) {
        i71.r(s);
        return com.google.common.base.b.b.a(s);
    }
    
    public static String b(final String s) {
        String s2 = s;
        if (f(s)) {
            s2 = null;
        }
        return s2;
    }
    
    public static String c(final double d) {
        return String.format(Locale.ROOT, "%.4g", d);
    }
    
    public static y41 d() {
        return new b(null);
    }
    
    public static boolean e() {
        return com.google.common.base.b.b.b();
    }
    
    public static boolean f(final String s) {
        return s == null || s.isEmpty();
    }
    
    public static long g() {
        return System.nanoTime();
    }
    
    public static final class b implements y41
    {
        @Override
        public hi a(final String regex) {
            return new JdkPattern(Pattern.compile(regex));
        }
        
        @Override
        public boolean b() {
            return true;
        }
    }
}

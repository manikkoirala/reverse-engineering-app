// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.base;

import java.io.Serializable;

class Functions$FunctionComposition<A, B, C> implements m90, Serializable
{
    private static final long serialVersionUID = 0L;
    private final m90 f;
    private final m90 g;
    
    public Functions$FunctionComposition(final m90 m90, final m90 m91) {
        this.g = (m90)i71.r(m90);
        this.f = (m90)i71.r(m91);
    }
    
    @Override
    public C apply(final A a) {
        return (C)this.g.apply(this.f.apply(a));
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof Functions$FunctionComposition;
        boolean b3;
        final boolean b2 = b3 = false;
        if (b) {
            final Functions$FunctionComposition functions$FunctionComposition = (Functions$FunctionComposition)o;
            b3 = b2;
            if (this.f.equals(functions$FunctionComposition.f)) {
                b3 = b2;
                if (this.g.equals(functions$FunctionComposition.g)) {
                    b3 = true;
                }
            }
        }
        return b3;
    }
    
    @Override
    public int hashCode() {
        return this.f.hashCode() ^ this.g.hashCode();
    }
    
    @Override
    public String toString() {
        final String value = String.valueOf(this.g);
        final String value2 = String.valueOf(this.f);
        final StringBuilder sb = new StringBuilder(value.length() + 2 + value2.length());
        sb.append(value);
        sb.append("(");
        sb.append(value2);
        sb.append(")");
        return sb.toString();
    }
}

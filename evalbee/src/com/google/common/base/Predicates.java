// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.base;

import java.util.List;
import java.io.Serializable;
import java.util.Iterator;
import java.util.Collection;

public abstract class Predicates
{
    public static m71 b() {
        return ObjectPredicate.ALWAYS_TRUE.withNarrowedType();
    }
    
    public static m71 c(final m71 m71, final m90 m72) {
        return new CompositionPredicate<Object, Object>(m71, m72, null);
    }
    
    public static m71 d(final Object o) {
        m71 m71;
        if (o == null) {
            m71 = f();
        }
        else {
            m71 = new IsEqualToPredicate(o, null).withNarrowedType();
        }
        return m71;
    }
    
    public static m71 e(final Collection collection) {
        return new InPredicate<Object>(collection, null);
    }
    
    public static m71 f() {
        return ObjectPredicate.IS_NULL.withNarrowedType();
    }
    
    public static m71 g(final m71 m71) {
        return new NotPredicate<Object>(m71);
    }
    
    public static String h(final String str, final Iterable iterable) {
        final StringBuilder sb = new StringBuilder("Predicates.");
        sb.append(str);
        sb.append('(');
        final Iterator iterator = iterable.iterator();
        int n = 1;
        while (iterator.hasNext()) {
            final Object next = iterator.next();
            if (n == 0) {
                sb.append(',');
            }
            sb.append(next);
            n = 0;
        }
        sb.append(')');
        return sb.toString();
    }
    
    public static class AndPredicate<T> implements m71, Serializable
    {
        private static final long serialVersionUID = 0L;
        private final List<? extends m71> components;
        
        private AndPredicate(final List<? extends m71> components) {
            this.components = components;
        }
        
        @Override
        public boolean apply(final T t) {
            for (int i = 0; i < this.components.size(); ++i) {
                if (!((m71)this.components.get(i)).apply(t)) {
                    return false;
                }
            }
            return true;
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof AndPredicate && this.components.equals(((AndPredicate)o).components);
        }
        
        @Override
        public int hashCode() {
            return this.components.hashCode() + 306654252;
        }
        
        @Override
        public String toString() {
            return h("and", this.components);
        }
    }
    
    public static class CompositionPredicate<A, B> implements m71, Serializable
    {
        private static final long serialVersionUID = 0L;
        final m90 f;
        final m71 p;
        
        private CompositionPredicate(final m71 m71, final m90 m72) {
            this.p = (m71)i71.r(m71);
            this.f = (m90)i71.r(m72);
        }
        
        @Override
        public boolean apply(final A a) {
            return this.p.apply(this.f.apply(a));
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof CompositionPredicate;
            boolean b3;
            final boolean b2 = b3 = false;
            if (b) {
                final CompositionPredicate compositionPredicate = (CompositionPredicate)o;
                b3 = b2;
                if (this.f.equals(compositionPredicate.f)) {
                    b3 = b2;
                    if (this.p.equals(compositionPredicate.p)) {
                        b3 = true;
                    }
                }
            }
            return b3;
        }
        
        @Override
        public int hashCode() {
            return this.f.hashCode() ^ this.p.hashCode();
        }
        
        @Override
        public String toString() {
            final String value = String.valueOf(this.p);
            final String value2 = String.valueOf(this.f);
            final StringBuilder sb = new StringBuilder(value.length() + 2 + value2.length());
            sb.append(value);
            sb.append("(");
            sb.append(value2);
            sb.append(")");
            return sb.toString();
        }
    }
    
    public static class ContainsPatternFromStringPredicate extends ContainsPatternPredicate
    {
        private static final long serialVersionUID = 0L;
        
        public ContainsPatternFromStringPredicate(final String s) {
            super(b.a(s));
        }
        
        @Override
        public String toString() {
            final String pattern = super.pattern.pattern();
            final StringBuilder sb = new StringBuilder(String.valueOf(pattern).length() + 28);
            sb.append("Predicates.containsPattern(");
            sb.append(pattern);
            sb.append(")");
            return sb.toString();
        }
    }
    
    public static class ContainsPatternPredicate implements m71, Serializable
    {
        private static final long serialVersionUID = 0L;
        final hi pattern;
        
        public ContainsPatternPredicate(final hi hi) {
            this.pattern = (hi)i71.r(hi);
        }
        
        public boolean apply(final CharSequence charSequence) {
            return this.pattern.matcher(charSequence).a();
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof ContainsPatternPredicate;
            boolean b3;
            final boolean b2 = b3 = false;
            if (b) {
                final ContainsPatternPredicate containsPatternPredicate = (ContainsPatternPredicate)o;
                b3 = b2;
                if (b11.a(this.pattern.pattern(), containsPatternPredicate.pattern.pattern())) {
                    b3 = b2;
                    if (this.pattern.flags() == containsPatternPredicate.pattern.flags()) {
                        b3 = true;
                    }
                }
            }
            return b3;
        }
        
        @Override
        public int hashCode() {
            return b11.b(this.pattern.pattern(), this.pattern.flags());
        }
        
        @Override
        public String toString() {
            final String string = a.c(this.pattern).d("pattern", this.pattern.pattern()).b("pattern.flags", this.pattern.flags()).toString();
            final StringBuilder sb = new StringBuilder(String.valueOf(string).length() + 21);
            sb.append("Predicates.contains(");
            sb.append(string);
            sb.append(")");
            return sb.toString();
        }
    }
    
    public static class InPredicate<T> implements m71, Serializable
    {
        private static final long serialVersionUID = 0L;
        private final Collection<?> target;
        
        private InPredicate(final Collection<?> collection) {
            this.target = (Collection)i71.r(collection);
        }
        
        @Override
        public boolean apply(final T t) {
            try {
                return this.target.contains(t);
            }
            catch (final NullPointerException | ClassCastException ex) {
                return false;
            }
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof InPredicate && this.target.equals(((InPredicate)o).target);
        }
        
        @Override
        public int hashCode() {
            return this.target.hashCode();
        }
        
        @Override
        public String toString() {
            final String value = String.valueOf(this.target);
            final StringBuilder sb = new StringBuilder(value.length() + 15);
            sb.append("Predicates.in(");
            sb.append(value);
            sb.append(")");
            return sb.toString();
        }
    }
    
    public static class InstanceOfPredicate<T> implements m71, Serializable
    {
        private static final long serialVersionUID = 0L;
        private final Class<?> clazz;
        
        private InstanceOfPredicate(final Class<?> clazz) {
            this.clazz = (Class)i71.r(clazz);
        }
        
        @Override
        public boolean apply(final T t) {
            return this.clazz.isInstance(t);
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof InstanceOfPredicate;
            boolean b2 = false;
            if (b) {
                final InstanceOfPredicate instanceOfPredicate = (InstanceOfPredicate)o;
                b2 = b2;
                if (this.clazz == instanceOfPredicate.clazz) {
                    b2 = true;
                }
            }
            return b2;
        }
        
        @Override
        public int hashCode() {
            return this.clazz.hashCode();
        }
        
        @Override
        public String toString() {
            final String name = this.clazz.getName();
            final StringBuilder sb = new StringBuilder(name.length() + 23);
            sb.append("Predicates.instanceOf(");
            sb.append(name);
            sb.append(")");
            return sb.toString();
        }
    }
    
    public static class IsEqualToPredicate implements m71, Serializable
    {
        private static final long serialVersionUID = 0L;
        private final Object target;
        
        private IsEqualToPredicate(final Object target) {
            this.target = target;
        }
        
        @Override
        public boolean apply(final Object obj) {
            return this.target.equals(obj);
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof IsEqualToPredicate && this.target.equals(((IsEqualToPredicate)o).target);
        }
        
        @Override
        public int hashCode() {
            return this.target.hashCode();
        }
        
        @Override
        public String toString() {
            final String value = String.valueOf(this.target);
            final StringBuilder sb = new StringBuilder(value.length() + 20);
            sb.append("Predicates.equalTo(");
            sb.append(value);
            sb.append(")");
            return sb.toString();
        }
        
        public <T> m71 withNarrowedType() {
            return this;
        }
    }
    
    public static class NotPredicate<T> implements m71, Serializable
    {
        private static final long serialVersionUID = 0L;
        final m71 predicate;
        
        public NotPredicate(final m71 m71) {
            this.predicate = (m71)i71.r(m71);
        }
        
        @Override
        public boolean apply(final T t) {
            return this.predicate.apply(t) ^ true;
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof NotPredicate && this.predicate.equals(((NotPredicate)o).predicate);
        }
        
        @Override
        public int hashCode() {
            return ~this.predicate.hashCode();
        }
        
        @Override
        public String toString() {
            final String value = String.valueOf(this.predicate);
            final StringBuilder sb = new StringBuilder(value.length() + 16);
            sb.append("Predicates.not(");
            sb.append(value);
            sb.append(")");
            return sb.toString();
        }
    }
    
    public enum ObjectPredicate implements m71
    {
        private static final ObjectPredicate[] $VALUES;
        
        ALWAYS_FALSE {
            @Override
            public boolean apply(final Object o) {
                return false;
            }
            
            @Override
            public String toString() {
                return "Predicates.alwaysFalse()";
            }
        }, 
        ALWAYS_TRUE {
            @Override
            public boolean apply(final Object o) {
                return true;
            }
            
            @Override
            public String toString() {
                return "Predicates.alwaysTrue()";
            }
        }, 
        IS_NULL {
            @Override
            public boolean apply(final Object o) {
                return o == null;
            }
            
            @Override
            public String toString() {
                return "Predicates.isNull()";
            }
        }, 
        NOT_NULL {
            @Override
            public boolean apply(final Object o) {
                return o != null;
            }
            
            @Override
            public String toString() {
                return "Predicates.notNull()";
            }
        };
        
        private static /* synthetic */ ObjectPredicate[] $values() {
            return new ObjectPredicate[] { ObjectPredicate.ALWAYS_TRUE, ObjectPredicate.ALWAYS_FALSE, ObjectPredicate.IS_NULL, ObjectPredicate.NOT_NULL };
        }
        
        static {
            $VALUES = $values();
        }
        
        public <T> m71 withNarrowedType() {
            return this;
        }
    }
    
    public static class OrPredicate<T> implements m71, Serializable
    {
        private static final long serialVersionUID = 0L;
        private final List<? extends m71> components;
        
        private OrPredicate(final List<? extends m71> components) {
            this.components = components;
        }
        
        @Override
        public boolean apply(final T t) {
            for (int i = 0; i < this.components.size(); ++i) {
                if (((m71)this.components.get(i)).apply(t)) {
                    return true;
                }
            }
            return false;
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof OrPredicate && this.components.equals(((OrPredicate)o).components);
        }
        
        @Override
        public int hashCode() {
            return this.components.hashCode() + 87855567;
        }
        
        @Override
        public String toString() {
            return h("or", this.components);
        }
    }
    
    public static class SubtypeOfPredicate implements m71, Serializable
    {
        private static final long serialVersionUID = 0L;
        private final Class<?> clazz;
        
        private SubtypeOfPredicate(final Class<?> clazz) {
            this.clazz = (Class)i71.r(clazz);
        }
        
        public boolean apply(final Class<?> clazz) {
            return this.clazz.isAssignableFrom(clazz);
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof SubtypeOfPredicate;
            boolean b2 = false;
            if (b) {
                final SubtypeOfPredicate subtypeOfPredicate = (SubtypeOfPredicate)o;
                b2 = b2;
                if (this.clazz == subtypeOfPredicate.clazz) {
                    b2 = true;
                }
            }
            return b2;
        }
        
        @Override
        public int hashCode() {
            return this.clazz.hashCode();
        }
        
        @Override
        public String toString() {
            final String name = this.clazz.getName();
            final StringBuilder sb = new StringBuilder(name.length() + 22);
            sb.append("Predicates.subtypeOf(");
            sb.append(name);
            sb.append(")");
            return sb.toString();
        }
    }
}

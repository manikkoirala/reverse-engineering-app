// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.base;

import java.util.Collections;
import java.util.Set;

final class Absent<T> extends Optional<T>
{
    static final Absent<Object> INSTANCE;
    private static final long serialVersionUID = 0L;
    
    static {
        INSTANCE = new Absent<Object>();
    }
    
    private Absent() {
    }
    
    private Object readResolve() {
        return Absent.INSTANCE;
    }
    
    public static <T> Optional<T> withType() {
        return (Optional<T>)Absent.INSTANCE;
    }
    
    @Override
    public Set<T> asSet() {
        return Collections.emptySet();
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this;
    }
    
    @Override
    public T get() {
        throw new IllegalStateException("Optional.get() cannot be called on an absent value");
    }
    
    @Override
    public int hashCode() {
        return 2040732332;
    }
    
    @Override
    public boolean isPresent() {
        return false;
    }
    
    @Override
    public Optional<T> or(final Optional<? extends T> optional) {
        return (Optional)i71.r(optional);
    }
    
    @Override
    public T or(final is1 is1) {
        return (T)i71.s(is1.get(), "use Optional.orNull() instead of a Supplier that returns null");
    }
    
    @Override
    public T or(final T t) {
        return (T)i71.s(t, "use Optional.orNull() instead of Optional.or(null)");
    }
    
    @Override
    public T orNull() {
        return null;
    }
    
    @Override
    public String toString() {
        return "Optional.absent()";
    }
    
    @Override
    public <V> Optional<V> transform(final m90 m90) {
        i71.r(m90);
        return Optional.absent();
    }
}

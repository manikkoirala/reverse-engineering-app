// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.base;

import java.io.Serializable;

class Functions$PredicateFunction<T> implements m90, Serializable
{
    private static final long serialVersionUID = 0L;
    private final m71 predicate;
    
    private Functions$PredicateFunction(final m71 m71) {
        this.predicate = (m71)i71.r(m71);
    }
    
    @Override
    public Boolean apply(final T t) {
        return this.predicate.apply(t);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof Functions$PredicateFunction && this.predicate.equals(((Functions$PredicateFunction)o).predicate);
    }
    
    @Override
    public int hashCode() {
        return this.predicate.hashCode();
    }
    
    @Override
    public String toString() {
        final String value = String.valueOf(this.predicate);
        final StringBuilder sb = new StringBuilder(value.length() + 24);
        sb.append("Functions.forPredicate(");
        sb.append(value);
        sb.append(")");
        return sb.toString();
    }
}

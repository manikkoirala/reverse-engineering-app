// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.BitSet;
import java.util.HashSet;
import java.util.Objects;
import java.util.Iterator;
import java.util.Set;
import java.util.Comparator;
import java.util.Arrays;
import java.util.AbstractMap;
import java.util.SortedMap;
import java.util.Collection;
import java.io.Serializable;
import java.util.Map;

public abstract class ImmutableMap<K, V> implements Map<K, V>, Serializable
{
    static final Entry<?, ?>[] EMPTY_ENTRY_ARRAY;
    private transient ImmutableSet<Entry<K, V>> entrySet;
    private transient ImmutableSet<K> keySet;
    private transient ImmutableSetMultimap<K, V> multimapView;
    private transient ImmutableCollection<V> values;
    
    static {
        EMPTY_ENTRY_ARRAY = new Entry[0];
    }
    
    public static <K, V> b builder() {
        return new b();
    }
    
    public static <K, V> b builderWithExpectedSize(final int n) {
        hh.b(n, "expectedSize");
        return new b(n);
    }
    
    public static void checkNoConflict(final boolean b, final String s, final Object o, final Object o2) {
        if (b) {
            return;
        }
        throw conflictException(s, o, o2);
    }
    
    public static IllegalArgumentException conflictException(final String s, final Object obj, final Object obj2) {
        final String value = String.valueOf(obj);
        final String value2 = String.valueOf(obj2);
        final StringBuilder sb = new StringBuilder(String.valueOf(s).length() + 34 + value.length() + value2.length());
        sb.append("Multiple entries with same ");
        sb.append(s);
        sb.append(": ");
        sb.append(value);
        sb.append(" and ");
        sb.append(value2);
        return new IllegalArgumentException(sb.toString());
    }
    
    public static <K, V> ImmutableMap<K, V> copyOf(final Iterable<? extends Entry<? extends K, ? extends V>> iterable) {
        int size;
        if (iterable instanceof Collection) {
            size = ((Collection)iterable).size();
        }
        else {
            size = 4;
        }
        final b b = new b(size);
        b.i(iterable);
        return b.a();
    }
    
    public static <K, V> ImmutableMap<K, V> copyOf(final Map<? extends K, ? extends V> map) {
        if (map instanceof ImmutableMap && !(map instanceof SortedMap)) {
            final ImmutableMap immutableMap = (ImmutableMap)map;
            if (!immutableMap.isPartialView()) {
                return immutableMap;
            }
        }
        return copyOf((Iterable<? extends Entry<? extends K, ? extends V>>)map.entrySet());
    }
    
    public static <K, V> Entry<K, V> entryOf(final K key, final V value) {
        hh.a(key, value);
        return (Entry<K, V>)new AbstractMap.SimpleImmutableEntry(key, value);
    }
    
    public static <K, V> ImmutableMap<K, V> of() {
        return (ImmutableMap<K, V>)RegularImmutableMap.EMPTY;
    }
    
    public static <K, V> ImmutableMap<K, V> of(final K k, final V v) {
        hh.a(k, v);
        return (ImmutableMap<K, V>)RegularImmutableMap.create(1, new Object[] { k, v });
    }
    
    public static <K, V> ImmutableMap<K, V> of(final K k, final V v, final K i, final V v2) {
        hh.a(k, v);
        hh.a(i, v2);
        return (ImmutableMap<K, V>)RegularImmutableMap.create(2, new Object[] { k, v, i, v2 });
    }
    
    public static <K, V> ImmutableMap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3) {
        hh.a(k, v);
        hh.a(i, v2);
        hh.a(j, v3);
        return (ImmutableMap<K, V>)RegularImmutableMap.create(3, new Object[] { k, v, i, v2, j, v3 });
    }
    
    public static <K, V> ImmutableMap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4) {
        hh.a(k, v);
        hh.a(i, v2);
        hh.a(j, v3);
        hh.a(l, v4);
        return (ImmutableMap<K, V>)RegularImmutableMap.create(4, new Object[] { k, v, i, v2, j, v3, l, v4 });
    }
    
    public static <K, V> ImmutableMap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4, final K m, final V v5) {
        hh.a(k, v);
        hh.a(i, v2);
        hh.a(j, v3);
        hh.a(l, v4);
        hh.a(m, v5);
        return (ImmutableMap<K, V>)RegularImmutableMap.create(5, new Object[] { k, v, i, v2, j, v3, l, v4, m, v5 });
    }
    
    public static <K, V> ImmutableMap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4, final K m, final V v5, final K k2, final V v6) {
        hh.a(k, v);
        hh.a(i, v2);
        hh.a(j, v3);
        hh.a(l, v4);
        hh.a(m, v5);
        hh.a(k2, v6);
        return (ImmutableMap<K, V>)RegularImmutableMap.create(6, new Object[] { k, v, i, v2, j, v3, l, v4, m, v5, k2, v6 });
    }
    
    public static <K, V> ImmutableMap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4, final K m, final V v5, final K k2, final V v6, final K k3, final V v7) {
        hh.a(k, v);
        hh.a(i, v2);
        hh.a(j, v3);
        hh.a(l, v4);
        hh.a(m, v5);
        hh.a(k2, v6);
        hh.a(k3, v7);
        return (ImmutableMap<K, V>)RegularImmutableMap.create(7, new Object[] { k, v, i, v2, j, v3, l, v4, m, v5, k2, v6, k3, v7 });
    }
    
    public static <K, V> ImmutableMap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4, final K m, final V v5, final K k2, final V v6, final K k3, final V v7, final K k4, final V v8) {
        hh.a(k, v);
        hh.a(i, v2);
        hh.a(j, v3);
        hh.a(l, v4);
        hh.a(m, v5);
        hh.a(k2, v6);
        hh.a(k3, v7);
        hh.a(k4, v8);
        return (ImmutableMap<K, V>)RegularImmutableMap.create(8, new Object[] { k, v, i, v2, j, v3, l, v4, m, v5, k2, v6, k3, v7, k4, v8 });
    }
    
    public static <K, V> ImmutableMap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4, final K m, final V v5, final K k2, final V v6, final K k3, final V v7, final K k4, final V v8, final K k5, final V v9) {
        hh.a(k, v);
        hh.a(i, v2);
        hh.a(j, v3);
        hh.a(l, v4);
        hh.a(m, v5);
        hh.a(k2, v6);
        hh.a(k3, v7);
        hh.a(k4, v8);
        hh.a(k5, v9);
        return (ImmutableMap<K, V>)RegularImmutableMap.create(9, new Object[] { k, v, i, v2, j, v3, l, v4, m, v5, k2, v6, k3, v7, k4, v8, k5, v9 });
    }
    
    public static <K, V> ImmutableMap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4, final K m, final V v5, final K k2, final V v6, final K k3, final V v7, final K k4, final V v8, final K k5, final V v9, final K k6, final V v10) {
        hh.a(k, v);
        hh.a(i, v2);
        hh.a(j, v3);
        hh.a(l, v4);
        hh.a(m, v5);
        hh.a(k2, v6);
        hh.a(k3, v7);
        hh.a(k4, v8);
        hh.a(k5, v9);
        hh.a(k6, v10);
        return (ImmutableMap<K, V>)RegularImmutableMap.create(10, new Object[] { k, v, i, v2, j, v3, l, v4, m, v5, k2, v6, k3, v7, k4, v8, k5, v9, k6, v10 });
    }
    
    @SafeVarargs
    public static <K, V> ImmutableMap<K, V> ofEntries(final Entry<? extends K, ? extends V>... a) {
        return copyOf((Iterable<? extends Entry<? extends K, ? extends V>>)Arrays.asList(a));
    }
    
    public ImmutableSetMultimap<K, V> asMultimap() {
        if (this.isEmpty()) {
            return ImmutableSetMultimap.of();
        }
        ImmutableSetMultimap<K, V> multimapView;
        if ((multimapView = this.multimapView) == null) {
            multimapView = new ImmutableSetMultimap<K, V>(new MapViewOfValuesAsSingletonSets(null), this.size(), null);
            this.multimapView = multimapView;
        }
        return multimapView;
    }
    
    @Deprecated
    @Override
    public final void clear() {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public boolean containsKey(final Object o) {
        return this.get(o) != null;
    }
    
    @Override
    public boolean containsValue(final Object o) {
        return this.values().contains(o);
    }
    
    public abstract ImmutableSet<Entry<K, V>> createEntrySet();
    
    public abstract ImmutableSet<K> createKeySet();
    
    public abstract ImmutableCollection<V> createValues();
    
    @Override
    public ImmutableSet<Entry<K, V>> entrySet() {
        ImmutableSet<Entry<K, V>> entrySet;
        if ((entrySet = this.entrySet) == null) {
            entrySet = this.createEntrySet();
            this.entrySet = entrySet;
        }
        return entrySet;
    }
    
    @Override
    public boolean equals(final Object o) {
        return Maps.i(this, o);
    }
    
    @Override
    public abstract V get(final Object p0);
    
    @Override
    public final V getOrDefault(Object value, final V v) {
        value = this.get(value);
        if (value != null) {
            return (V)value;
        }
        return v;
    }
    
    @Override
    public int hashCode() {
        return Sets.b(this.entrySet());
    }
    
    @Override
    public boolean isEmpty() {
        return this.size() == 0;
    }
    
    public boolean isHashCodeFast() {
        return false;
    }
    
    public abstract boolean isPartialView();
    
    public w02 keyIterator() {
        return new w02(this, this.entrySet().iterator()) {
            public final w02 a;
            
            @Override
            public boolean hasNext() {
                return this.a.hasNext();
            }
            
            @Override
            public Object next() {
                return this.a.next().getKey();
            }
        };
    }
    
    @Override
    public ImmutableSet<K> keySet() {
        ImmutableSet<K> keySet;
        if ((keySet = this.keySet) == null) {
            keySet = this.createKeySet();
            this.keySet = keySet;
        }
        return keySet;
    }
    
    @Deprecated
    @Override
    public final V put(final K k, final V v) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public final void putAll(final Map<? extends K, ? extends V> map) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public final V remove(final Object o) {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public String toString() {
        return Maps.y(this);
    }
    
    @Override
    public ImmutableCollection<V> values() {
        ImmutableCollection<V> values;
        if ((values = this.values) == null) {
            values = this.createValues();
            this.values = values;
        }
        return values;
    }
    
    Object writeReplace() {
        return new SerializedForm((ImmutableMap<Object, Object>)this);
    }
    
    public abstract static class IteratorBasedImmutableMap<K, V> extends ImmutableMap<K, V>
    {
        @Override
        public ImmutableSet<Entry<K, V>> createEntrySet() {
            return (ImmutableSet<Entry<K, V>>)new EntrySetImpl();
        }
        
        @Override
        public ImmutableSet<K> createKeySet() {
            return (ImmutableSet<K>)new ImmutableMapKeySet((ImmutableMap<Object, Object>)this);
        }
        
        @Override
        public ImmutableCollection<V> createValues() {
            return (ImmutableCollection<V>)new ImmutableMapValues((ImmutableMap<Object, Object>)this);
        }
        
        public abstract w02 entryIterator();
        
        public class EntrySetImpl extends ImmutableMapEntrySet<K, V>
        {
            final IteratorBasedImmutableMap this$0;
            
            public EntrySetImpl(final IteratorBasedImmutableMap this$0) {
                this.this$0 = this$0;
            }
            
            @Override
            public w02 iterator() {
                return this.this$0.entryIterator();
            }
            
            @Override
            public ImmutableMap<K, V> map() {
                return this.this$0;
            }
        }
    }
    
    public final class MapViewOfValuesAsSingletonSets extends IteratorBasedImmutableMap<K, ImmutableSet<V>>
    {
        final ImmutableMap this$0;
        
        private MapViewOfValuesAsSingletonSets(final ImmutableMap this$0) {
            this.this$0 = this$0;
        }
        
        @Override
        public boolean containsKey(final Object o) {
            return this.this$0.containsKey(o);
        }
        
        @Override
        public ImmutableSet<K> createKeySet() {
            return this.this$0.keySet();
        }
        
        @Override
        public w02 entryIterator() {
            return new w02(this, this.this$0.entrySet().iterator()) {
                public final Iterator a;
                
                public Entry b() {
                    return new s(this, (Entry)this.a.next()) {
                        public final Entry a;
                        
                        public ImmutableSet a() {
                            return ImmutableSet.of(this.a.getValue());
                        }
                        
                        @Override
                        public Object getKey() {
                            return this.a.getKey();
                        }
                    };
                }
                
                @Override
                public boolean hasNext() {
                    return this.a.hasNext();
                }
            };
        }
        
        @Override
        public ImmutableSet<V> get(Object value) {
            value = this.this$0.get(value);
            ImmutableSet<V> of;
            if (value == null) {
                of = null;
            }
            else {
                of = ImmutableSet.of(value);
            }
            return of;
        }
        
        @Override
        public int hashCode() {
            return this.this$0.hashCode();
        }
        
        @Override
        public boolean isHashCodeFast() {
            return this.this$0.isHashCodeFast();
        }
        
        @Override
        public boolean isPartialView() {
            return this.this$0.isPartialView();
        }
        
        @Override
        public int size() {
            return this.this$0.size();
        }
    }
    
    public static class SerializedForm<K, V> implements Serializable
    {
        private static final boolean USE_LEGACY_SERIALIZATION = true;
        private static final long serialVersionUID = 0L;
        private final Object keys;
        private final Object values;
        
        public SerializedForm(final ImmutableMap<K, V> immutableMap) {
            final Object[] keys = new Object[immutableMap.size()];
            final Object[] values = new Object[immutableMap.size()];
            final w02 iterator = immutableMap.entrySet().iterator();
            int n = 0;
            while (iterator.hasNext()) {
                final Entry<Object, V> entry = iterator.next();
                keys[n] = entry.getKey();
                values[n] = entry.getValue();
                ++n;
            }
            this.keys = keys;
            this.values = values;
        }
        
        public final Object legacyReadResolve() {
            final Object[] array = (Object[])this.keys;
            final Object[] array2 = (Object[])this.values;
            final b builder = this.makeBuilder(array.length);
            for (int i = 0; i < array.length; ++i) {
                builder.g(array[i], array2[i]);
            }
            return builder.d();
        }
        
        public b makeBuilder(final int n) {
            return new b(n);
        }
        
        public final Object readResolve() {
            final Object keys = this.keys;
            if (!(keys instanceof ImmutableSet)) {
                return this.legacyReadResolve();
            }
            final ImmutableSet set = (ImmutableSet)keys;
            final ImmutableCollection collection = (ImmutableCollection)this.values;
            final b builder = this.makeBuilder(set.size());
            final w02 iterator = set.iterator();
            final w02 iterator2 = collection.iterator();
            while (iterator.hasNext()) {
                builder.g(iterator.next(), iterator2.next());
            }
            return builder.d();
        }
    }
    
    public static class b
    {
        public Comparator a;
        public Object[] b;
        public int c;
        public boolean d;
        public a e;
        
        public b() {
            this(4);
        }
        
        public b(final int n) {
            this.b = new Object[n * 2];
            this.c = 0;
            this.d = false;
        }
        
        public static void k(final Object[] array, final int toIndex, final Comparator comparator) {
            final Entry[] a = new Entry[toIndex];
            final int n = 0;
            for (int i = 0; i < toIndex; ++i) {
                final int n2 = i * 2;
                final Object o = array[n2];
                Objects.requireNonNull(o);
                final Object o2 = array[n2 + 1];
                Objects.requireNonNull(o2);
                a[i] = (Entry)new AbstractMap.SimpleImmutableEntry(o, o2);
            }
            Arrays.sort((Entry[])a, 0, toIndex, Ordering.from((Comparator<Object>)comparator).onResultOf(Maps.J()));
            for (int j = n; j < toIndex; ++j) {
                final int n3 = j * 2;
                array[n3] = a[j].getKey();
                array[n3 + 1] = a[j].getValue();
            }
        }
        
        public ImmutableMap a() {
            return this.d();
        }
        
        public final ImmutableMap b(final boolean b) {
            if (b) {
                final a e = this.e;
                if (e != null) {
                    throw e.a();
                }
            }
            final int c = this.c;
            Object[] b2;
            int n;
            if (this.a == null) {
                b2 = this.b;
                n = c;
            }
            else {
                if (this.d) {
                    this.b = Arrays.copyOf(this.b, c * 2);
                }
                final Object[] b3 = this.b;
                n = c;
                b2 = b3;
                if (!b) {
                    final Object[] f = this.f(b3, this.c);
                    n = c;
                    b2 = f;
                    if (f.length < this.b.length) {
                        n = f.length >>> 1;
                        b2 = f;
                    }
                }
                k(b2, n, this.a);
            }
            this.d = true;
            final RegularImmutableMap<Object, Object> create = RegularImmutableMap.create(n, b2, this);
            if (b) {
                final a e2 = this.e;
                if (e2 != null) {
                    throw e2.a();
                }
            }
            return create;
        }
        
        public ImmutableMap c() {
            return this.b(false);
        }
        
        public ImmutableMap d() {
            return this.b(true);
        }
        
        public final void e(int n) {
            n *= 2;
            final Object[] b = this.b;
            if (n > b.length) {
                this.b = Arrays.copyOf(b, ImmutableCollection.b.e(b.length, n));
                this.d = false;
            }
        }
        
        public final Object[] f(final Object[] array, final int n) {
            final HashSet set = new HashSet();
            final BitSet set2 = new BitSet();
            for (int i = n - 1; i >= 0; --i) {
                final Object obj = array[i * 2];
                Objects.requireNonNull(obj);
                if (!set.add(obj)) {
                    set2.set(i);
                }
            }
            if (set2.isEmpty()) {
                return array;
            }
            final Object[] array2 = new Object[(n - set2.cardinality()) * 2];
            int j = 0;
            int n2 = 0;
            while (j < n * 2) {
                if (set2.get(j >>> 1)) {
                    j += 2;
                }
                else {
                    final int n3 = n2 + 1;
                    final int n4 = j + 1;
                    final Object obj2 = array[j];
                    Objects.requireNonNull(obj2);
                    array2[n2] = obj2;
                    n2 = n3 + 1;
                    j = n4 + 1;
                    final Object obj3 = array[n4];
                    Objects.requireNonNull(obj3);
                    array2[n3] = obj3;
                }
            }
            return array2;
        }
        
        public b g(final Object o, final Object o2) {
            this.e(this.c + 1);
            hh.a(o, o2);
            final Object[] b = this.b;
            final int c = this.c;
            b[c * 2] = o;
            b[c * 2 + 1] = o2;
            this.c = c + 1;
            return this;
        }
        
        public b h(final Entry entry) {
            return this.g(entry.getKey(), entry.getValue());
        }
        
        public b i(final Iterable iterable) {
            if (iterable instanceof Collection) {
                this.e(this.c + ((Collection)iterable).size());
            }
            final Iterator iterator = iterable.iterator();
            while (iterator.hasNext()) {
                this.h((Entry)iterator.next());
            }
            return this;
        }
        
        public b j(final Map map) {
            return this.i(map.entrySet());
        }
        
        public static final class a
        {
            public final Object a;
            public final Object b;
            public final Object c;
            
            public a(final Object a, final Object b, final Object c) {
                this.a = a;
                this.b = b;
                this.c = c;
            }
            
            public IllegalArgumentException a() {
                final String value = String.valueOf(this.a);
                final String value2 = String.valueOf(this.b);
                final String value3 = String.valueOf(this.a);
                final String value4 = String.valueOf(this.c);
                final StringBuilder sb = new StringBuilder(value.length() + 39 + value2.length() + value3.length() + value4.length());
                sb.append("Multiple entries with same key: ");
                sb.append(value);
                sb.append("=");
                sb.append(value2);
                sb.append(" and ");
                sb.append(value3);
                sb.append("=");
                sb.append(value4);
                return new IllegalArgumentException(sb.toString());
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;

final class ImmutableMapKeySet<K, V> extends IndexedImmutableSet<K>
{
    private final ImmutableMap<K, V> map;
    
    public ImmutableMapKeySet(final ImmutableMap<K, V> map) {
        this.map = map;
    }
    
    @Override
    public boolean contains(final Object o) {
        return this.map.containsKey(o);
    }
    
    @Override
    public K get(final int n) {
        return ((Map.Entry)this.map.entrySet().asList().get(n)).getKey();
    }
    
    @Override
    public boolean isPartialView() {
        return true;
    }
    
    @Override
    public w02 iterator() {
        return this.map.keyIterator();
    }
    
    @Override
    public int size() {
        return this.map.size();
    }
    
    public Object writeReplace() {
        return new KeySetSerializedForm((ImmutableMap<Object, ?>)this.map);
    }
    
    public static class KeySetSerializedForm<K> implements Serializable
    {
        private static final long serialVersionUID = 0L;
        final ImmutableMap<K, ?> map;
        
        public KeySetSerializedForm(final ImmutableMap<K, ?> map) {
            this.map = map;
        }
        
        public Object readResolve() {
            return this.map.keySet();
        }
    }
}

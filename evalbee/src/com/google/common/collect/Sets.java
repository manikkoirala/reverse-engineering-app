// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.AbstractSet;
import java.util.Collections;
import java.util.SortedSet;
import java.io.Serializable;
import java.util.NavigableSet;
import java.util.LinkedHashSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Collection;
import java.util.Set;

public abstract class Sets
{
    public static boolean a(final Set set, final Object o) {
        boolean b = true;
        if (set == o) {
            return true;
        }
        if (!(o instanceof Set)) {
            return false;
        }
        final Set set2 = (Set)o;
        try {
            if (set.size() != set2.size() || !set.containsAll(set2)) {
                b = false;
            }
            return b;
        }
        catch (final NullPointerException | ClassCastException ex) {
            return false;
        }
    }
    
    public static int b(final Set set) {
        final Iterator iterator = set.iterator();
        int n = 0;
        while (iterator.hasNext()) {
            final Object next = iterator.next();
            int hashCode;
            if (next != null) {
                hashCode = next.hashCode();
            }
            else {
                hashCode = 0;
            }
            n = ~(~(n + hashCode));
        }
        return n;
    }
    
    public static HashSet c() {
        return new HashSet();
    }
    
    public static HashSet d(final int n) {
        return new HashSet(Maps.e(n));
    }
    
    public static LinkedHashSet e() {
        return new LinkedHashSet();
    }
    
    public static boolean f(final Set set, final Collection collection) {
        i71.r(collection);
        Object elementSet = collection;
        if (collection instanceof j) {
            elementSet = ((j)collection).elementSet();
        }
        if (elementSet instanceof Set && ((Collection)elementSet).size() > set.size()) {
            return Iterators.r(set.iterator(), (Collection)elementSet);
        }
        return g(set, ((Collection)elementSet).iterator());
    }
    
    public static boolean g(final Set set, final Iterator iterator) {
        boolean b = false;
        while (iterator.hasNext()) {
            b |= set.remove(iterator.next());
        }
        return b;
    }
    
    public static NavigableSet h(final NavigableSet set) {
        if (!(set instanceof ImmutableCollection) && !(set instanceof UnmodifiableNavigableSet)) {
            return new UnmodifiableNavigableSet(set);
        }
        return set;
    }
    
    public static final class UnmodifiableNavigableSet<E> extends e80 implements NavigableSet<E>, Serializable
    {
        private static final long serialVersionUID = 0L;
        private final NavigableSet<E> delegate;
        private transient UnmodifiableNavigableSet<E> descendingSet;
        private final SortedSet<E> unmodifiableDelegate;
        
        public UnmodifiableNavigableSet(final NavigableSet<E> s) {
            this.delegate = (NavigableSet)i71.r(s);
            this.unmodifiableDelegate = Collections.unmodifiableSortedSet(s);
        }
        
        @Override
        public E ceiling(final E e) {
            return this.delegate.ceiling(e);
        }
        
        @Override
        public SortedSet<E> delegate() {
            return this.unmodifiableDelegate;
        }
        
        @Override
        public Iterator<E> descendingIterator() {
            return Iterators.x(this.delegate.descendingIterator());
        }
        
        @Override
        public NavigableSet<E> descendingSet() {
            UnmodifiableNavigableSet<E> descendingSet;
            if ((descendingSet = this.descendingSet) == null) {
                descendingSet = new UnmodifiableNavigableSet<E>(this.delegate.descendingSet());
                this.descendingSet = descendingSet;
                descendingSet.descendingSet = this;
            }
            return descendingSet;
        }
        
        @Override
        public E floor(final E e) {
            return this.delegate.floor(e);
        }
        
        @Override
        public NavigableSet<E> headSet(final E e, final boolean b) {
            return Sets.h(this.delegate.headSet(e, b));
        }
        
        @Override
        public E higher(final E e) {
            return this.delegate.higher(e);
        }
        
        @Override
        public E lower(final E e) {
            return this.delegate.lower(e);
        }
        
        @Override
        public E pollFirst() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public E pollLast() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public NavigableSet<E> subSet(final E e, final boolean b, final E e2, final boolean b2) {
            return Sets.h(this.delegate.subSet(e, b, e2, b2));
        }
        
        @Override
        public NavigableSet<E> tailSet(final E e, final boolean b) {
            return Sets.h(this.delegate.tailSet(e, b));
        }
    }
    
    public abstract static class a extends AbstractSet
    {
        @Override
        public boolean removeAll(final Collection collection) {
            return Sets.f(this, collection);
        }
        
        @Override
        public boolean retainAll(final Collection collection) {
            return super.retainAll((Collection<?>)i71.r(collection));
        }
    }
}

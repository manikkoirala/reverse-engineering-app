// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Objects;
import java.util.Set;
import java.util.Arrays;
import java.io.Serializable;
import java.util.Iterator;
import java.util.Collection;

public abstract class ImmutableMultiset<E> extends ImmutableMultisetGwtSerializationDependencies<E> implements j
{
    private transient ImmutableList<E> asList;
    private transient ImmutableSet<j.a> entrySet;
    
    public static <E> b builder() {
        return new b();
    }
    
    private static <E> ImmutableMultiset<E> copyFromElements(final E... array) {
        return new b().g((Object[])array).k();
    }
    
    public static <E> ImmutableMultiset<E> copyFromEntries(final Collection<? extends j.a> collection) {
        final b b = new b(collection.size());
        for (final j.a a : collection) {
            b.j(a.getElement(), a.getCount());
        }
        return b.k();
    }
    
    public static <E> ImmutableMultiset<E> copyOf(final Iterable<? extends E> iterable) {
        if (iterable instanceof ImmutableMultiset) {
            final ImmutableMultiset immutableMultiset = (ImmutableMultiset)iterable;
            if (!immutableMultiset.isPartialView()) {
                return immutableMultiset;
            }
        }
        final b b = new b(Multisets.h(iterable));
        b.h(iterable);
        return b.k();
    }
    
    public static <E> ImmutableMultiset<E> copyOf(final Iterator<? extends E> iterator) {
        return new b().i(iterator).k();
    }
    
    public static <E> ImmutableMultiset<E> copyOf(final E[] array) {
        return (ImmutableMultiset<E>)copyFromElements((Object[])array);
    }
    
    private ImmutableSet<j.a> createEntrySet() {
        Serializable of;
        if (this.isEmpty()) {
            of = ImmutableSet.of();
        }
        else {
            of = new EntrySet(null);
        }
        return (ImmutableSet<j.a>)of;
    }
    
    public static <E> ImmutableMultiset<E> of() {
        return (ImmutableMultiset<E>)RegularImmutableMultiset.EMPTY;
    }
    
    public static <E> ImmutableMultiset<E> of(final E e) {
        return copyFromElements(e);
    }
    
    public static <E> ImmutableMultiset<E> of(final E e, final E e2) {
        return copyFromElements(e, e2);
    }
    
    public static <E> ImmutableMultiset<E> of(final E e, final E e2, final E e3) {
        return copyFromElements(e, e2, e3);
    }
    
    public static <E> ImmutableMultiset<E> of(final E e, final E e2, final E e3, final E e4) {
        return copyFromElements(e, e2, e3, e4);
    }
    
    public static <E> ImmutableMultiset<E> of(final E e, final E e2, final E e3, final E e4, final E e5) {
        return copyFromElements(e, e2, e3, e4, e5);
    }
    
    public static <E> ImmutableMultiset<E> of(final E e, final E e2, final E e3, final E e4, final E e5, final E e6, final E... array) {
        return new b().f(e).f(e2).f(e3).f(e4).f(e5).f(e6).g((Object[])array).k();
    }
    
    @Deprecated
    @Override
    public final int add(final E e, final int n) {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public ImmutableList<E> asList() {
        ImmutableList<E> asList;
        if ((asList = this.asList) == null) {
            asList = super.asList();
            this.asList = asList;
        }
        return asList;
    }
    
    @Override
    public boolean contains(final Object o) {
        return this.count(o) > 0;
    }
    
    @Override
    public int copyIntoArray(final Object[] a, int fromIndex) {
        for (final j.a a2 : this.entrySet()) {
            Arrays.fill(a, fromIndex, a2.getCount() + fromIndex, a2.getElement());
            fromIndex += a2.getCount();
        }
        return fromIndex;
    }
    
    @Override
    public abstract /* synthetic */ int count(final Object p0);
    
    @Override
    public abstract ImmutableSet<E> elementSet();
    
    @Override
    public ImmutableSet<j.a> entrySet() {
        ImmutableSet<j.a> entrySet;
        if ((entrySet = this.entrySet) == null) {
            entrySet = this.createEntrySet();
            this.entrySet = entrySet;
        }
        return entrySet;
    }
    
    @Override
    public boolean equals(final Object o) {
        return Multisets.f(this, o);
    }
    
    public abstract j.a getEntry(final int p0);
    
    @Override
    public int hashCode() {
        return Sets.b(this.entrySet());
    }
    
    @Override
    public w02 iterator() {
        return new w02(this, this.entrySet().iterator()) {
            public int a;
            public Object b;
            public final Iterator c;
            
            @Override
            public boolean hasNext() {
                return this.a > 0 || this.c.hasNext();
            }
            
            @Override
            public Object next() {
                if (this.a <= 0) {
                    final j.a a = this.c.next();
                    this.b = a.getElement();
                    this.a = a.getCount();
                }
                --this.a;
                final Object b = this.b;
                Objects.requireNonNull(b);
                return b;
            }
        };
    }
    
    @Deprecated
    @Override
    public final int remove(final Object o, final int n) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public final int setCount(final E e, final int n) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public final boolean setCount(final E e, final int n, final int n2) {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public String toString() {
        return this.entrySet().toString();
    }
    
    @Override
    abstract Object writeReplace();
    
    public final class EntrySet extends IndexedImmutableSet<j.a>
    {
        private static final long serialVersionUID = 0L;
        final ImmutableMultiset this$0;
        
        private EntrySet(final ImmutableMultiset this$0) {
            this.this$0 = this$0;
        }
        
        @Override
        public boolean contains(final Object o) {
            final boolean b = o instanceof j.a;
            boolean b2 = false;
            if (b) {
                final j.a a = (j.a)o;
                if (a.getCount() <= 0) {
                    return false;
                }
                b2 = b2;
                if (this.this$0.count(a.getElement()) == a.getCount()) {
                    b2 = true;
                }
            }
            return b2;
        }
        
        @Override
        public j.a get(final int n) {
            return this.this$0.getEntry(n);
        }
        
        @Override
        public int hashCode() {
            return this.this$0.hashCode();
        }
        
        @Override
        public boolean isPartialView() {
            return this.this$0.isPartialView();
        }
        
        @Override
        public int size() {
            return this.this$0.elementSet().size();
        }
        
        public Object writeReplace() {
            return new EntrySetSerializedForm(this.this$0);
        }
    }
    
    public static class EntrySetSerializedForm<E> implements Serializable
    {
        final ImmutableMultiset<E> multiset;
        
        public EntrySetSerializedForm(final ImmutableMultiset<E> multiset) {
            this.multiset = multiset;
        }
        
        public Object readResolve() {
            return this.multiset.entrySet();
        }
    }
    
    public static class b extends ImmutableCollection.b
    {
        public k a;
        public boolean b;
        public boolean c;
        
        public b() {
            this(4);
        }
        
        public b(final int n) {
            this.b = false;
            this.c = false;
            this.a = k.c(n);
        }
        
        public b(final boolean b) {
            this.b = false;
            this.c = false;
            this.a = null;
        }
        
        public static k l(final Iterable iterable) {
            if (iterable instanceof RegularImmutableMultiset) {
                return ((RegularImmutableMultiset)iterable).contents;
            }
            if (iterable instanceof AbstractMapBasedMultiset) {
                return ((AbstractMapBasedMultiset)iterable).backingMap;
            }
            return null;
        }
        
        public b f(final Object o) {
            return this.j(o, 1);
        }
        
        public b g(final Object... array) {
            super.b(array);
            return this;
        }
        
        public b h(final Iterable iterable) {
            Objects.requireNonNull(this.a);
            if (iterable instanceof j) {
                final j d = Multisets.d(iterable);
                final k l = l(d);
                if (l != null) {
                    final k a = this.a;
                    a.d(Math.max(a.C(), l.C()));
                    for (int i = l.e(); i >= 0; i = l.s(i)) {
                        this.j(l.i(i), l.k(i));
                    }
                }
                else {
                    final Set entrySet = d.entrySet();
                    final k a2 = this.a;
                    a2.d(Math.max(a2.C(), entrySet.size()));
                    for (final j.a a3 : d.entrySet()) {
                        this.j(a3.getElement(), a3.getCount());
                    }
                }
            }
            else {
                super.c(iterable);
            }
            return this;
        }
        
        public b i(final Iterator iterator) {
            super.d(iterator);
            return this;
        }
        
        public b j(final Object o, final int n) {
            Objects.requireNonNull(this.a);
            if (n == 0) {
                return this;
            }
            if (this.b) {
                this.a = new k(this.a);
                this.c = false;
            }
            this.b = false;
            i71.r(o);
            final k a = this.a;
            a.u(o, n + a.f(o));
            return this;
        }
        
        public ImmutableMultiset k() {
            Objects.requireNonNull(this.a);
            if (this.a.C() == 0) {
                return ImmutableMultiset.of();
            }
            if (this.c) {
                this.a = new k(this.a);
                this.c = false;
            }
            this.b = true;
            return new RegularImmutableMultiset(this.a);
        }
    }
}

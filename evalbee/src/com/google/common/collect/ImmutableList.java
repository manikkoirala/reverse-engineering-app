// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.io.Serializable;
import java.util.ListIterator;
import java.util.Comparator;
import java.util.Arrays;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.util.Iterator;
import java.util.Collection;
import java.util.RandomAccess;
import java.util.List;

public abstract class ImmutableList<E> extends ImmutableCollection<E> implements List<E>, RandomAccess
{
    private static final z02 EMPTY_ITR;
    
    static {
        EMPTY_ITR = new b(RegularImmutableList.EMPTY, 0);
    }
    
    public static <E> ImmutableList<E> asImmutableList(final Object[] array) {
        return asImmutableList(array, array.length);
    }
    
    public static <E> ImmutableList<E> asImmutableList(final Object[] array, final int n) {
        if (n == 0) {
            return of();
        }
        return new RegularImmutableList<E>(array, n);
    }
    
    public static <E> a builder() {
        return new a();
    }
    
    public static <E> a builderWithExpectedSize(final int n) {
        hh.b(n, "expectedSize");
        return new a(n);
    }
    
    private static <E> ImmutableList<E> construct(final Object... array) {
        return (ImmutableList<E>)asImmutableList(t01.b(array));
    }
    
    public static <E> ImmutableList<E> copyOf(final Iterable<? extends E> iterable) {
        i71.r(iterable);
        ImmutableList<Object> list;
        if (iterable instanceof Collection) {
            list = copyOf((Collection<?>)iterable);
        }
        else {
            list = copyOf((Iterator<?>)iterable.iterator());
        }
        return (ImmutableList<E>)list;
    }
    
    public static <E> ImmutableList<E> copyOf(final Collection<? extends E> collection) {
        if (collection instanceof ImmutableCollection) {
            ImmutableList<Object> list2;
            final ImmutableList list = list2 = ((ImmutableCollection)collection).asList();
            if (list.isPartialView()) {
                list2 = asImmutableList(list.toArray());
            }
            return (ImmutableList<E>)list2;
        }
        return construct(collection.toArray());
    }
    
    public static <E> ImmutableList<E> copyOf(final Iterator<? extends E> iterator) {
        if (!iterator.hasNext()) {
            return of();
        }
        final E next = iterator.next();
        if (!iterator.hasNext()) {
            return of(next);
        }
        return new a().i(next).k(iterator).l();
    }
    
    public static <E> ImmutableList<E> copyOf(final E[] array) {
        ImmutableList<Object> list;
        if (array.length == 0) {
            list = of();
        }
        else {
            list = construct((Object[])array.clone());
        }
        return (ImmutableList<E>)list;
    }
    
    public static <E> ImmutableList<E> of() {
        return (ImmutableList<E>)RegularImmutableList.EMPTY;
    }
    
    public static <E> ImmutableList<E> of(final E e) {
        return construct(e);
    }
    
    public static <E> ImmutableList<E> of(final E e, final E e2) {
        return construct(e, e2);
    }
    
    public static <E> ImmutableList<E> of(final E e, final E e2, final E e3) {
        return construct(e, e2, e3);
    }
    
    public static <E> ImmutableList<E> of(final E e, final E e2, final E e3, final E e4) {
        return construct(e, e2, e3, e4);
    }
    
    public static <E> ImmutableList<E> of(final E e, final E e2, final E e3, final E e4, final E e5) {
        return construct(e, e2, e3, e4, e5);
    }
    
    public static <E> ImmutableList<E> of(final E e, final E e2, final E e3, final E e4, final E e5, final E e6) {
        return construct(e, e2, e3, e4, e5, e6);
    }
    
    public static <E> ImmutableList<E> of(final E e, final E e2, final E e3, final E e4, final E e5, final E e6, final E e7) {
        return construct(e, e2, e3, e4, e5, e6, e7);
    }
    
    public static <E> ImmutableList<E> of(final E e, final E e2, final E e3, final E e4, final E e5, final E e6, final E e7, final E e8) {
        return construct(e, e2, e3, e4, e5, e6, e7, e8);
    }
    
    public static <E> ImmutableList<E> of(final E e, final E e2, final E e3, final E e4, final E e5, final E e6, final E e7, final E e8, final E e9) {
        return construct(e, e2, e3, e4, e5, e6, e7, e8, e9);
    }
    
    public static <E> ImmutableList<E> of(final E e, final E e2, final E e3, final E e4, final E e5, final E e6, final E e7, final E e8, final E e9, final E e10) {
        return construct(e, e2, e3, e4, e5, e6, e7, e8, e9, e10);
    }
    
    public static <E> ImmutableList<E> of(final E e, final E e2, final E e3, final E e4, final E e5, final E e6, final E e7, final E e8, final E e9, final E e10, final E e11) {
        return construct(e, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11);
    }
    
    @SafeVarargs
    public static <E> ImmutableList<E> of(final E e, final E e2, final E e3, final E e4, final E e5, final E e6, final E e7, final E e8, final E e9, final E e10, final E e11, final E e12, final E... array) {
        i71.e(array.length <= 2147483635, "the total number of elements must fit in an int");
        final Object[] array2 = new Object[array.length + 12];
        array2[0] = e;
        array2[1] = e2;
        array2[2] = e3;
        array2[3] = e4;
        array2[4] = e5;
        array2[5] = e6;
        array2[6] = e7;
        array2[7] = e8;
        array2[8] = e9;
        array2[9] = e10;
        array2[10] = e11;
        array2[11] = e12;
        System.arraycopy(array, 0, array2, 12, array.length);
        return (ImmutableList<E>)construct(array2);
    }
    
    private void readObject(final ObjectInputStream objectInputStream) {
        throw new InvalidObjectException("Use SerializedForm");
    }
    
    public static <E extends Comparable<? super E>> ImmutableList<E> sortedCopyOf(final Iterable<? extends E> iterable) {
        final Comparable[] a = (Comparable[])rg0.l(iterable, new Comparable[0]);
        t01.b((Object[])a);
        Arrays.sort(a);
        return (ImmutableList<E>)asImmutableList(a);
    }
    
    public static <E> ImmutableList<E> sortedCopyOf(final Comparator<? super E> c, final Iterable<? extends E> iterable) {
        i71.r(c);
        final Object[] k = rg0.k(iterable);
        t01.b(k);
        Arrays.sort(k, (Comparator<? super Object>)c);
        return (ImmutableList<E>)asImmutableList(k);
    }
    
    @Deprecated
    @Override
    public final void add(final int n, final E e) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public final boolean addAll(final int n, final Collection<? extends E> collection) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public final ImmutableList<E> asList() {
        return this;
    }
    
    @Override
    public boolean contains(final Object o) {
        return this.indexOf(o) >= 0;
    }
    
    @Override
    public int copyIntoArray(final Object[] array, final int n) {
        final int size = this.size();
        for (int i = 0; i < size; ++i) {
            array[n + i] = this.get(i);
        }
        return n + size;
    }
    
    @Override
    public boolean equals(final Object o) {
        return Lists.d(this, o);
    }
    
    @Override
    public int hashCode() {
        final int size = this.size();
        int n = 1;
        for (int i = 0; i < size; ++i) {
            n = ~(~(n * 31 + this.get(i).hashCode()));
        }
        return n;
    }
    
    @Override
    public int indexOf(final Object o) {
        int e;
        if (o == null) {
            e = -1;
        }
        else {
            e = Lists.e(this, o);
        }
        return e;
    }
    
    @Override
    public w02 iterator() {
        return this.listIterator();
    }
    
    @Override
    public int lastIndexOf(final Object o) {
        int g;
        if (o == null) {
            g = -1;
        }
        else {
            g = Lists.g(this, o);
        }
        return g;
    }
    
    @Override
    public z02 listIterator() {
        return this.listIterator(0);
    }
    
    @Override
    public z02 listIterator(final int n) {
        i71.u(n, this.size());
        if (this.isEmpty()) {
            return ImmutableList.EMPTY_ITR;
        }
        return new b(this, n);
    }
    
    @Deprecated
    @Override
    public final E remove(final int n) {
        throw new UnsupportedOperationException();
    }
    
    public ImmutableList<E> reverse() {
        ImmutableList list;
        if (this.size() <= 1) {
            list = this;
        }
        else {
            list = new ReverseImmutableList<E>(this);
        }
        return list;
    }
    
    @Deprecated
    @Override
    public final E set(final int n, final E e) {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public ImmutableList<E> subList(final int n, final int n2) {
        i71.w(n, n2, this.size());
        final int n3 = n2 - n;
        if (n3 == this.size()) {
            return this;
        }
        if (n3 == 0) {
            return of();
        }
        return this.subListUnchecked(n, n2);
    }
    
    public ImmutableList<E> subListUnchecked(final int n, final int n2) {
        return new SubList(n, n2 - n);
    }
    
    public Object writeReplace() {
        return new SerializedForm(this.toArray());
    }
    
    public static class ReverseImmutableList<E> extends ImmutableList<E>
    {
        private final transient ImmutableList<E> forwardList;
        
        public ReverseImmutableList(final ImmutableList<E> forwardList) {
            this.forwardList = forwardList;
        }
        
        private int reverseIndex(final int n) {
            return this.size() - 1 - n;
        }
        
        private int reversePosition(final int n) {
            return this.size() - n;
        }
        
        @Override
        public boolean contains(final Object o) {
            return this.forwardList.contains(o);
        }
        
        @Override
        public E get(final int n) {
            i71.p(n, this.size());
            return this.forwardList.get(this.reverseIndex(n));
        }
        
        @Override
        public int indexOf(final Object o) {
            final int lastIndex = this.forwardList.lastIndexOf(o);
            int reverseIndex;
            if (lastIndex >= 0) {
                reverseIndex = this.reverseIndex(lastIndex);
            }
            else {
                reverseIndex = -1;
            }
            return reverseIndex;
        }
        
        @Override
        public boolean isPartialView() {
            return this.forwardList.isPartialView();
        }
        
        @Override
        public int lastIndexOf(final Object o) {
            final int index = this.forwardList.indexOf(o);
            int reverseIndex;
            if (index >= 0) {
                reverseIndex = this.reverseIndex(index);
            }
            else {
                reverseIndex = -1;
            }
            return reverseIndex;
        }
        
        @Override
        public ImmutableList<E> reverse() {
            return this.forwardList;
        }
        
        @Override
        public int size() {
            return this.forwardList.size();
        }
        
        @Override
        public ImmutableList<E> subList(final int n, final int n2) {
            i71.w(n, n2, this.size());
            return this.forwardList.subList(this.reversePosition(n2), this.reversePosition(n)).reverse();
        }
    }
    
    public static class SerializedForm implements Serializable
    {
        private static final long serialVersionUID = 0L;
        final Object[] elements;
        
        public SerializedForm(final Object[] elements) {
            this.elements = elements;
        }
        
        public Object readResolve() {
            return ImmutableList.copyOf(this.elements);
        }
    }
    
    public class SubList extends ImmutableList<E>
    {
        final transient int length;
        final transient int offset;
        final ImmutableList this$0;
        
        public SubList(final ImmutableList this$0, final int offset, final int length) {
            this.this$0 = this$0;
            this.offset = offset;
            this.length = length;
        }
        
        @Override
        public E get(final int n) {
            i71.p(n, this.length);
            return this.this$0.get(n + this.offset);
        }
        
        @Override
        public Object[] internalArray() {
            return this.this$0.internalArray();
        }
        
        @Override
        public int internalArrayEnd() {
            return this.this$0.internalArrayStart() + this.offset + this.length;
        }
        
        @Override
        public int internalArrayStart() {
            return this.this$0.internalArrayStart() + this.offset;
        }
        
        @Override
        public boolean isPartialView() {
            return true;
        }
        
        @Override
        public int size() {
            return this.length;
        }
        
        @Override
        public ImmutableList<E> subList(final int n, final int n2) {
            i71.w(n, n2, this.length);
            final ImmutableList this$0 = this.this$0;
            final int offset = this.offset;
            return this$0.subList(n + offset, n2 + offset);
        }
    }
    
    public static final class a extends ImmutableCollection.a
    {
        public a() {
            this(4);
        }
        
        public a(final int n) {
            super(n);
        }
        
        public a i(final Object o) {
            super.f(o);
            return this;
        }
        
        public a j(final Object... array) {
            super.b(array);
            return this;
        }
        
        public a k(final Iterator iterator) {
            super.d(iterator);
            return this;
        }
        
        public ImmutableList l() {
            super.c = true;
            return ImmutableList.asImmutableList(super.a, super.b);
        }
    }
    
    public static class b extends o
    {
        public final ImmutableList c;
        
        public b(final ImmutableList c, final int n) {
            super(c.size(), n);
            this.c = c;
        }
        
        @Override
        public Object b(final int n) {
            return this.c.get(n);
        }
    }
}

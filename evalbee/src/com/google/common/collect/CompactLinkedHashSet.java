// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Set;
import java.util.Arrays;
import java.util.Objects;
import java.util.Collections;
import java.util.Collection;

class CompactLinkedHashSet<E> extends CompactHashSet<E>
{
    private static final int ENDPOINT = -2;
    private transient int firstEntry;
    private transient int lastEntry;
    private transient int[] predecessor;
    private transient int[] successor;
    
    public CompactLinkedHashSet() {
    }
    
    public CompactLinkedHashSet(final int n) {
        super(n);
    }
    
    public static <E> CompactLinkedHashSet<E> create() {
        return new CompactLinkedHashSet<E>();
    }
    
    public static <E> CompactLinkedHashSet<E> create(final Collection<? extends E> c) {
        final CompactLinkedHashSet<Object> withExpectedSize = createWithExpectedSize(c.size());
        withExpectedSize.addAll(c);
        return (CompactLinkedHashSet<E>)withExpectedSize;
    }
    
    @SafeVarargs
    public static <E> CompactLinkedHashSet<E> create(final E... elements) {
        final CompactLinkedHashSet<Object> withExpectedSize = createWithExpectedSize(elements.length);
        Collections.addAll(withExpectedSize, elements);
        return (CompactLinkedHashSet<E>)withExpectedSize;
    }
    
    public static <E> CompactLinkedHashSet<E> createWithExpectedSize(final int n) {
        return new CompactLinkedHashSet<E>(n);
    }
    
    private int getPredecessor(final int n) {
        return this.requirePredecessors()[n] - 1;
    }
    
    private int[] requirePredecessors() {
        final int[] predecessor = this.predecessor;
        Objects.requireNonNull(predecessor);
        return predecessor;
    }
    
    private int[] requireSuccessors() {
        final int[] successor = this.successor;
        Objects.requireNonNull(successor);
        return successor;
    }
    
    private void setPredecessor(final int n, final int n2) {
        this.requirePredecessors()[n] = n2 + 1;
    }
    
    private void setSucceeds(final int lastEntry, final int firstEntry) {
        if (lastEntry == -2) {
            this.firstEntry = firstEntry;
        }
        else {
            this.setSuccessor(lastEntry, firstEntry);
        }
        if (firstEntry == -2) {
            this.lastEntry = lastEntry;
        }
        else {
            this.setPredecessor(firstEntry, lastEntry);
        }
    }
    
    private void setSuccessor(final int n, final int n2) {
        this.requireSuccessors()[n] = n2 + 1;
    }
    
    @Override
    public int adjustAfterRemove(final int n, final int n2) {
        int n3 = n;
        if (n >= this.size()) {
            n3 = n2;
        }
        return n3;
    }
    
    @Override
    public int allocArrays() {
        final int allocArrays = super.allocArrays();
        this.predecessor = new int[allocArrays];
        this.successor = new int[allocArrays];
        return allocArrays;
    }
    
    @Override
    public void clear() {
        if (this.needsAllocArrays()) {
            return;
        }
        this.firstEntry = -2;
        this.lastEntry = -2;
        final int[] predecessor = this.predecessor;
        if (predecessor != null && this.successor != null) {
            Arrays.fill(predecessor, 0, this.size(), 0);
            Arrays.fill(this.successor, 0, this.size(), 0);
        }
        super.clear();
    }
    
    @Override
    public Set<E> convertToHashFloodingResistantImplementation() {
        final Set<E> convertToHashFloodingResistantImplementation = super.convertToHashFloodingResistantImplementation();
        this.predecessor = null;
        this.successor = null;
        return convertToHashFloodingResistantImplementation;
    }
    
    @Override
    public int firstEntryIndex() {
        return this.firstEntry;
    }
    
    @Override
    public int getSuccessor(final int n) {
        return this.requireSuccessors()[n] - 1;
    }
    
    @Override
    public void init(final int n) {
        super.init(n);
        this.firstEntry = -2;
        this.lastEntry = -2;
    }
    
    @Override
    public void insertEntry(final int n, final E e, final int n2, final int n3) {
        super.insertEntry(n, e, n2, n3);
        this.setSucceeds(this.lastEntry, n);
        this.setSucceeds(n, -2);
    }
    
    @Override
    public void moveLastEntry(final int n, final int n2) {
        final int n3 = this.size() - 1;
        super.moveLastEntry(n, n2);
        this.setSucceeds(this.getPredecessor(n), this.getSuccessor(n));
        if (n < n3) {
            this.setSucceeds(this.getPredecessor(n3), n);
            this.setSucceeds(n, this.getSuccessor(n3));
        }
        this.requirePredecessors()[n3] = 0;
        this.requireSuccessors()[n3] = 0;
    }
    
    @Override
    public void resizeEntries(final int n) {
        super.resizeEntries(n);
        this.predecessor = Arrays.copyOf(this.requirePredecessors(), n);
        this.successor = Arrays.copyOf(this.requireSuccessors(), n);
    }
    
    @Override
    public Object[] toArray() {
        return t01.f(this);
    }
    
    @Override
    public <T> T[] toArray(final T[] array) {
        return (T[])t01.g(this, array);
    }
}

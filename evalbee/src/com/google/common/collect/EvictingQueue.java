// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Collection;
import java.util.ArrayDeque;
import java.util.Queue;
import java.io.Serializable;

public final class EvictingQueue<E> extends b80 implements Serializable
{
    private static final long serialVersionUID = 0L;
    private final Queue<E> delegate;
    final int maxSize;
    
    private EvictingQueue(final int n) {
        i71.h(n >= 0, "maxSize (%s) must >= 0", n);
        this.delegate = new ArrayDeque<E>(n);
        this.maxSize = n;
    }
    
    public static <E> EvictingQueue<E> create(final int n) {
        return new EvictingQueue<E>(n);
    }
    
    @Override
    public boolean add(final E e) {
        i71.r(e);
        if (this.maxSize == 0) {
            return true;
        }
        if (this.size() == this.maxSize) {
            this.delegate.remove();
        }
        this.delegate.add(e);
        return true;
    }
    
    @Override
    public boolean addAll(final Collection<? extends E> collection) {
        final int size = collection.size();
        if (size >= this.maxSize) {
            this.clear();
            return rg0.a(this, rg0.j(collection, size - this.maxSize));
        }
        return this.standardAddAll((Collection<Object>)collection);
    }
    
    @Override
    public Queue<E> delegate() {
        return this.delegate;
    }
    
    @Override
    public boolean offer(final E e) {
        return this.add(e);
    }
    
    public int remainingCapacity() {
        return this.maxSize - this.size();
    }
    
    @Override
    public Object[] toArray() {
        return super.toArray();
    }
}

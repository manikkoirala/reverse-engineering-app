// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.ArrayList;
import java.util.List;
import java.io.Serializable;

final class MultimapBuilder$ArrayListSupplier<V> implements is1, Serializable
{
    private final int expectedValuesPerKey;
    
    public MultimapBuilder$ArrayListSupplier(final int n) {
        this.expectedValuesPerKey = hh.b(n, "expectedValuesPerKey");
    }
    
    @Override
    public List<V> get() {
        return new ArrayList<V>(this.expectedValuesPerKey);
    }
}

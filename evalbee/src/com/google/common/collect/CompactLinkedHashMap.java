// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Arrays;
import java.util.Objects;

class CompactLinkedHashMap<K, V> extends CompactHashMap<K, V>
{
    private static final int ENDPOINT = -2;
    private final boolean accessOrder;
    private transient int firstEntry;
    private transient int lastEntry;
    transient long[] links;
    
    public CompactLinkedHashMap() {
        this(3);
    }
    
    public CompactLinkedHashMap(final int n) {
        this(n, false);
    }
    
    public CompactLinkedHashMap(final int n, final boolean accessOrder) {
        super(n);
        this.accessOrder = accessOrder;
    }
    
    public static <K, V> CompactLinkedHashMap<K, V> create() {
        return new CompactLinkedHashMap<K, V>();
    }
    
    public static <K, V> CompactLinkedHashMap<K, V> createWithExpectedSize(final int n) {
        return new CompactLinkedHashMap<K, V>(n);
    }
    
    private int getPredecessor(final int n) {
        return (int)(this.link(n) >>> 32) - 1;
    }
    
    private long link(final int n) {
        return this.requireLinks()[n];
    }
    
    private long[] requireLinks() {
        final long[] links = this.links;
        Objects.requireNonNull(links);
        return links;
    }
    
    private void setLink(final int n, final long n2) {
        this.requireLinks()[n] = n2;
    }
    
    private void setPredecessor(final int n, final int n2) {
        this.setLink(n, (this.link(n) & 0xFFFFFFFFL) | (long)(n2 + 1) << 32);
    }
    
    private void setSucceeds(final int lastEntry, final int firstEntry) {
        if (lastEntry == -2) {
            this.firstEntry = firstEntry;
        }
        else {
            this.setSuccessor(lastEntry, firstEntry);
        }
        if (firstEntry == -2) {
            this.lastEntry = lastEntry;
        }
        else {
            this.setPredecessor(firstEntry, lastEntry);
        }
    }
    
    private void setSuccessor(final int n, final int n2) {
        this.setLink(n, (this.link(n) & 0xFFFFFFFF00000000L) | ((long)(n2 + 1) & 0xFFFFFFFFL));
    }
    
    @Override
    public void accessEntry(final int n) {
        if (this.accessOrder) {
            this.setSucceeds(this.getPredecessor(n), this.getSuccessor(n));
            this.setSucceeds(this.lastEntry, n);
            this.setSucceeds(n, -2);
            this.incrementModCount();
        }
    }
    
    @Override
    public int adjustAfterRemove(final int n, final int n2) {
        int n3 = n;
        if (n >= this.size()) {
            n3 = n2;
        }
        return n3;
    }
    
    @Override
    public int allocArrays() {
        final int allocArrays = super.allocArrays();
        this.links = new long[allocArrays];
        return allocArrays;
    }
    
    @Override
    public void clear() {
        if (this.needsAllocArrays()) {
            return;
        }
        this.firstEntry = -2;
        this.lastEntry = -2;
        final long[] links = this.links;
        if (links != null) {
            Arrays.fill(links, 0, this.size(), 0L);
        }
        super.clear();
    }
    
    @Override
    public Map<K, V> convertToHashFloodingResistantImplementation() {
        final Map<K, V> convertToHashFloodingResistantImplementation = super.convertToHashFloodingResistantImplementation();
        this.links = null;
        return convertToHashFloodingResistantImplementation;
    }
    
    @Override
    public Map<K, V> createHashFloodingResistantDelegate(final int initialCapacity) {
        return new LinkedHashMap<K, V>(initialCapacity, 1.0f, this.accessOrder);
    }
    
    @Override
    public int firstEntryIndex() {
        return this.firstEntry;
    }
    
    @Override
    public int getSuccessor(final int n) {
        return (int)this.link(n) - 1;
    }
    
    @Override
    public void init(final int n) {
        super.init(n);
        this.firstEntry = -2;
        this.lastEntry = -2;
    }
    
    @Override
    public void insertEntry(final int n, final K k, final V v, final int n2, final int n3) {
        super.insertEntry(n, k, v, n2, n3);
        this.setSucceeds(this.lastEntry, n);
        this.setSucceeds(n, -2);
    }
    
    @Override
    public void moveLastEntry(final int n, final int n2) {
        final int n3 = this.size() - 1;
        super.moveLastEntry(n, n2);
        this.setSucceeds(this.getPredecessor(n), this.getSuccessor(n));
        if (n < n3) {
            this.setSucceeds(this.getPredecessor(n3), n);
            this.setSucceeds(n, this.getSuccessor(n3));
        }
        this.setLink(n3, 0L);
    }
    
    @Override
    public void resizeEntries(final int newLength) {
        super.resizeEntries(newLength);
        this.links = Arrays.copyOf(this.requireLinks(), newLength);
    }
}

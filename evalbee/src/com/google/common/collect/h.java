// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Iterator;
import java.util.Collection;
import java.util.Set;

public abstract class h extends t70 implements j
{
    @Override
    public int add(final Object o, final int n) {
        return this.delegate().add(o, n);
    }
    
    @Override
    public int count(final Object o) {
        return this.delegate().count(o);
    }
    
    @Override
    public abstract j delegate();
    
    @Override
    public abstract Set entrySet();
    
    @Override
    public boolean equals(final Object o) {
        return o == this || this.delegate().equals(o);
    }
    
    @Override
    public int hashCode() {
        return this.delegate().hashCode();
    }
    
    @Override
    public int remove(final Object o, final int n) {
        return this.delegate().remove(o, n);
    }
    
    @Override
    public int setCount(final Object o, final int n) {
        return this.delegate().setCount(o, n);
    }
    
    @Override
    public boolean setCount(final Object o, final int n, final int n2) {
        return this.delegate().setCount(o, n, n2);
    }
    
    public boolean standardAdd(final Object o) {
        this.add(o, 1);
        return true;
    }
    
    @Override
    public boolean standardAddAll(final Collection<Object> collection) {
        return Multisets.c(this, collection);
    }
    
    @Override
    public void standardClear() {
        Iterators.d(this.entrySet().iterator());
    }
    
    @Override
    public boolean standardContains(final Object o) {
        return this.count(o) > 0;
    }
    
    public int standardCount(final Object o) {
        for (final a a : this.entrySet()) {
            if (b11.a(a.getElement(), o)) {
                return a.getCount();
            }
        }
        return 0;
    }
    
    public boolean standardEquals(final Object o) {
        return Multisets.f(this, o);
    }
    
    public int standardHashCode() {
        return this.entrySet().hashCode();
    }
    
    public Iterator<Object> standardIterator() {
        return Multisets.i(this);
    }
    
    @Override
    public boolean standardRemove(final Object o) {
        boolean b = true;
        if (this.remove(o, 1) <= 0) {
            b = false;
        }
        return b;
    }
    
    @Override
    public boolean standardRemoveAll(final Collection<?> collection) {
        return Multisets.k(this, collection);
    }
    
    @Override
    public boolean standardRetainAll(final Collection<?> collection) {
        return Multisets.l(this, collection);
    }
    
    public int standardSetCount(final Object o, final int n) {
        return Multisets.m(this, o, n);
    }
    
    public boolean standardSetCount(final Object o, final int n, final int n2) {
        return Multisets.n(this, o, n, n2);
    }
    
    public int standardSize() {
        return Multisets.j(this);
    }
    
    @Override
    public String standardToString() {
        return this.entrySet().toString();
    }
}

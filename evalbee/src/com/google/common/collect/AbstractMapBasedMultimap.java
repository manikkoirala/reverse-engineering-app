// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.ConcurrentModificationException;
import java.util.AbstractCollection;
import java.util.ListIterator;
import java.util.Comparator;
import java.util.SortedSet;
import java.util.NavigableSet;
import java.util.Objects;
import java.util.RandomAccess;
import java.util.Collections;
import java.util.SortedMap;
import java.util.NavigableMap;
import java.util.Set;
import java.util.List;
import java.util.Iterator;
import java.util.Collection;
import java.util.Map;
import java.io.Serializable;

abstract class AbstractMapBasedMultimap<K, V> extends a implements Serializable
{
    private static final long serialVersionUID = 2447537837011683357L;
    private transient Map<K, Collection<V>> map;
    private transient int totalSize;
    
    public AbstractMapBasedMultimap(final Map<K, Collection<V>> map) {
        i71.d(map.isEmpty());
        this.map = map;
    }
    
    public static /* synthetic */ Map access$000(final AbstractMapBasedMultimap abstractMapBasedMultimap) {
        return abstractMapBasedMultimap.map;
    }
    
    public static /* synthetic */ int access$208(final AbstractMapBasedMultimap abstractMapBasedMultimap) {
        return abstractMapBasedMultimap.totalSize++;
    }
    
    public static /* synthetic */ int access$210(final AbstractMapBasedMultimap abstractMapBasedMultimap) {
        return abstractMapBasedMultimap.totalSize--;
    }
    
    public static /* synthetic */ int access$212(final AbstractMapBasedMultimap abstractMapBasedMultimap, int totalSize) {
        totalSize += abstractMapBasedMultimap.totalSize;
        return abstractMapBasedMultimap.totalSize = totalSize;
    }
    
    public static /* synthetic */ int access$220(final AbstractMapBasedMultimap abstractMapBasedMultimap, int totalSize) {
        totalSize = abstractMapBasedMultimap.totalSize - totalSize;
        return abstractMapBasedMultimap.totalSize = totalSize;
    }
    
    private Collection<V> getOrCreateCollection(final K k) {
        Collection<V> collection;
        if ((collection = this.map.get(k)) == null) {
            collection = this.createCollection(k);
            this.map.put(k, collection);
        }
        return collection;
    }
    
    private static <E> Iterator<E> iteratorOrListIterator(final Collection<E> collection) {
        Iterator iterator;
        if (collection instanceof List) {
            iterator = ((List)collection).listIterator();
        }
        else {
            iterator = collection.iterator();
        }
        return iterator;
    }
    
    private void removeValuesForKey(final Object o) {
        final Collection collection = (Collection)Maps.x(this.map, o);
        if (collection != null) {
            final int size = collection.size();
            collection.clear();
            this.totalSize -= size;
        }
    }
    
    public Map<K, Collection<V>> backingMap() {
        return this.map;
    }
    
    @Override
    public void clear() {
        final Iterator<Collection<V>> iterator = this.map.values().iterator();
        while (iterator.hasNext()) {
            iterator.next().clear();
        }
        this.map.clear();
        this.totalSize = 0;
    }
    
    @Override
    public boolean containsKey(final Object o) {
        return this.map.containsKey(o);
    }
    
    @Override
    public Map<K, Collection<V>> createAsMap() {
        return new c(this.map);
    }
    
    abstract Collection<V> createCollection();
    
    public Collection<V> createCollection(final K k) {
        return this.createCollection();
    }
    
    @Override
    public Collection<Map.Entry<K, V>> createEntries() {
        if (this instanceof pm1) {
            return new com.google.common.collect.a.b();
        }
        return new a();
    }
    
    @Override
    public Set<K> createKeySet() {
        return new e(this.map);
    }
    
    @Override
    public com.google.common.collect.j createKeys() {
        return new Multimaps.c(this);
    }
    
    public final Map<K, Collection<V>> createMaybeNavigableAsMap() {
        final Map<K, Collection<V>> map = this.map;
        if (map instanceof NavigableMap) {
            return new f((NavigableMap)this.map);
        }
        if (map instanceof SortedMap) {
            return new i((SortedMap)this.map);
        }
        return new c(this.map);
    }
    
    public final Set<K> createMaybeNavigableKeySet() {
        final Map<K, Collection<V>> map = this.map;
        if (map instanceof NavigableMap) {
            return new g((NavigableMap)this.map);
        }
        if (map instanceof SortedMap) {
            return new j((SortedMap)this.map);
        }
        return new e(this.map);
    }
    
    public Collection<V> createUnmodifiableEmptyCollection() {
        return this.unmodifiableCollectionSubclass(this.createCollection());
    }
    
    @Override
    public Collection<V> createValues() {
        return new com.google.common.collect.a.c();
    }
    
    @Override
    public Collection<Map.Entry<K, V>> entries() {
        return super.entries();
    }
    
    @Override
    Iterator<Map.Entry<K, V>> entryIterator() {
        return new d(this) {
            public Map.Entry c(final Object o, final Object o2) {
                return Maps.j(o, o2);
            }
        };
    }
    
    @Override
    public Collection<V> get(final K k) {
        Collection<V> collection;
        if ((collection = this.map.get(k)) == null) {
            collection = this.createCollection(k);
        }
        return this.wrapCollection(k, collection);
    }
    
    @Override
    public boolean put(final K k, final V v) {
        final Collection collection = this.map.get(k);
        if (collection == null) {
            final Collection<V> collection2 = this.createCollection(k);
            if (collection2.add(v)) {
                ++this.totalSize;
                this.map.put(k, collection2);
                return true;
            }
            throw new AssertionError((Object)"New Collection violated the Collection spec");
        }
        else {
            if (collection.add(v)) {
                ++this.totalSize;
                return true;
            }
            return false;
        }
    }
    
    @Override
    public Collection<V> removeAll(final Object o) {
        final Collection collection = this.map.remove(o);
        if (collection == null) {
            return this.createUnmodifiableEmptyCollection();
        }
        final Collection<V> collection2 = this.createCollection();
        collection2.addAll(collection);
        this.totalSize -= collection.size();
        collection.clear();
        return this.unmodifiableCollectionSubclass(collection2);
    }
    
    @Override
    public Collection<V> replaceValues(final K k, final Iterable<? extends V> iterable) {
        final Iterator<? extends V> iterator = iterable.iterator();
        if (!iterator.hasNext()) {
            return this.removeAll(k);
        }
        final Collection<V> orCreateCollection = this.getOrCreateCollection(k);
        final Collection<V> collection = this.createCollection();
        collection.addAll((Collection<? extends V>)orCreateCollection);
        this.totalSize -= orCreateCollection.size();
        orCreateCollection.clear();
        while (iterator.hasNext()) {
            if (orCreateCollection.add(iterator.next())) {
                ++this.totalSize;
            }
        }
        return this.unmodifiableCollectionSubclass(collection);
    }
    
    final void setMap(final Map<K, Collection<V>> map) {
        this.map = map;
        this.totalSize = 0;
        for (final Collection collection : map.values()) {
            i71.d(collection.isEmpty() ^ true);
            this.totalSize += collection.size();
        }
    }
    
    @Override
    public int size() {
        return this.totalSize;
    }
    
    public <E> Collection<E> unmodifiableCollectionSubclass(final Collection<E> c) {
        return Collections.unmodifiableCollection((Collection<? extends E>)c);
    }
    
    @Override
    Iterator<V> valueIterator() {
        return new d(this) {
            @Override
            public Object b(final Object o, final Object o2) {
                return o2;
            }
        };
    }
    
    @Override
    public Collection<V> values() {
        return (Collection<V>)super.values();
    }
    
    public Collection<V> wrapCollection(final K k, final Collection<V> collection) {
        return new k(k, collection, null);
    }
    
    public final List<V> wrapList(final K k, final List<V> list, final k i) {
        List list2;
        if (list instanceof RandomAccess) {
            list2 = new h(k, list, i);
        }
        else {
            list2 = new l(k, list, i);
        }
        return list2;
    }
    
    public class c extends u
    {
        public final transient Map d;
        public final AbstractMapBasedMultimap e;
        
        public c(final AbstractMapBasedMultimap e, final Map d) {
            this.e = e;
            this.d = d;
        }
        
        public Set a() {
            return new a();
        }
        
        @Override
        public void clear() {
            if (this.d == AbstractMapBasedMultimap.access$000(this.e)) {
                this.e.clear();
            }
            else {
                Iterators.d(new b());
            }
        }
        
        @Override
        public boolean containsKey(final Object o) {
            return Maps.v(this.d, o);
        }
        
        public Collection d(final Object o) {
            final Collection collection = (Collection)Maps.w(this.d, o);
            if (collection == null) {
                return null;
            }
            return this.e.wrapCollection(o, collection);
        }
        
        public Collection e(final Object o) {
            final Collection collection = this.d.remove(o);
            if (collection == null) {
                return null;
            }
            final Collection<V> collection2 = this.e.createCollection();
            collection2.addAll(collection);
            AbstractMapBasedMultimap.access$220(this.e, collection.size());
            collection.clear();
            return collection2;
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || this.d.equals(o);
        }
        
        public Entry f(final Entry entry) {
            final Object key = entry.getKey();
            return Maps.j(key, this.e.wrapCollection(key, (Collection<V>)entry.getValue()));
        }
        
        @Override
        public int hashCode() {
            return this.d.hashCode();
        }
        
        @Override
        public Set keySet() {
            return this.e.keySet();
        }
        
        @Override
        public int size() {
            return this.d.size();
        }
        
        @Override
        public String toString() {
            return this.d.toString();
        }
        
        public class a extends Maps.j
        {
            public final c a;
            
            public a(final c a) {
                this.a = a;
            }
            
            @Override
            public Map a() {
                return this.a;
            }
            
            @Override
            public boolean contains(final Object o) {
                return lh.c(this.a.d.entrySet(), o);
            }
            
            @Override
            public Iterator iterator() {
                return this.a.new b();
            }
            
            @Override
            public boolean remove(final Object o) {
                if (!this.contains(o)) {
                    return false;
                }
                final Entry obj = (Entry)o;
                Objects.requireNonNull(obj);
                this.a.e.removeValuesForKey(((Map.Entry)obj).getKey());
                return true;
            }
        }
        
        public class b implements Iterator
        {
            public final Iterator a;
            public Collection b;
            public final c c;
            
            public b(final c c) {
                this.c = c;
                this.a = c.d.entrySet().iterator();
            }
            
            public Entry b() {
                final Map.Entry<K, Collection> entry = this.a.next();
                this.b = entry.getValue();
                return this.c.f((Entry)entry);
            }
            
            @Override
            public boolean hasNext() {
                return this.a.hasNext();
            }
            
            @Override
            public void remove() {
                i71.y(this.b != null, "no calls to next() since the last call to remove()");
                this.a.remove();
                AbstractMapBasedMultimap.access$220(this.c.e, this.b.size());
                this.b.clear();
                this.b = null;
            }
        }
    }
    
    public abstract class d implements Iterator
    {
        public final Iterator a;
        public Object b;
        public Collection c;
        public Iterator d;
        public final AbstractMapBasedMultimap e;
        
        public d(final AbstractMapBasedMultimap e) {
            this.e = e;
            this.a = AbstractMapBasedMultimap.access$000(e).entrySet().iterator();
            this.b = null;
            this.c = null;
            this.d = Iterators.j();
        }
        
        public abstract Object b(final Object p0, final Object p1);
        
        @Override
        public boolean hasNext() {
            return this.a.hasNext() || this.d.hasNext();
        }
        
        @Override
        public Object next() {
            if (!this.d.hasNext()) {
                final Map.Entry<Object, V> entry = this.a.next();
                this.b = entry.getKey();
                final Collection c = (Collection)entry.getValue();
                this.c = c;
                this.d = c.iterator();
            }
            return this.b(k01.a(this.b), this.d.next());
        }
        
        @Override
        public void remove() {
            this.d.remove();
            final Collection c = this.c;
            Objects.requireNonNull(c);
            if (c.isEmpty()) {
                this.a.remove();
            }
            AbstractMapBasedMultimap.access$210(this.e);
        }
    }
    
    public class e extends Maps.m
    {
        public final AbstractMapBasedMultimap b;
        
        public e(final AbstractMapBasedMultimap b, final Map map) {
            this.b = b;
            super(map);
        }
        
        @Override
        public void clear() {
            Iterators.d(this.iterator());
        }
        
        @Override
        public boolean containsAll(final Collection collection) {
            return ((Maps.m)this).a().keySet().containsAll(collection);
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || ((Maps.m)this).a().keySet().equals(o);
        }
        
        @Override
        public int hashCode() {
            return ((Maps.m)this).a().keySet().hashCode();
        }
        
        @Override
        public Iterator iterator() {
            return new Iterator(this, ((Maps.m)this).a().entrySet().iterator()) {
                public Map.Entry a;
                public final Iterator b;
                public final e c;
                
                @Override
                public boolean hasNext() {
                    return this.b.hasNext();
                }
                
                @Override
                public Object next() {
                    final Map.Entry<Object, V> a = this.b.next();
                    this.a = (Map.Entry)a;
                    return a.getKey();
                }
                
                @Override
                public void remove() {
                    i71.y(this.a != null, "no calls to next() since the last call to remove()");
                    final Collection collection = this.a.getValue();
                    this.b.remove();
                    AbstractMapBasedMultimap.access$220(this.c.b, collection.size());
                    collection.clear();
                    this.a = null;
                }
            };
        }
        
        @Override
        public boolean remove(final Object o) {
            final Collection collection = ((Maps.m)this).a().remove(o);
            boolean b = false;
            int size;
            if (collection != null) {
                size = collection.size();
                collection.clear();
                AbstractMapBasedMultimap.access$220(this.b, size);
            }
            else {
                size = 0;
            }
            if (size > 0) {
                b = true;
            }
            return b;
        }
    }
    
    public class f extends i implements NavigableMap
    {
        public final AbstractMapBasedMultimap h;
        
        public f(final AbstractMapBasedMultimap h, final NavigableMap navigableMap) {
            this.h = h.super(navigableMap);
        }
        
        @Override
        public Entry ceilingEntry(final Object o) {
            final Map.Entry<Object, Object> ceilingEntry = this.m().ceilingEntry(o);
            Entry f;
            if (ceilingEntry == null) {
                f = null;
            }
            else {
                f = ((c)this).f((Entry)ceilingEntry);
            }
            return f;
        }
        
        @Override
        public Object ceilingKey(final Object o) {
            return this.m().ceilingKey(o);
        }
        
        @Override
        public NavigableSet descendingKeySet() {
            return this.descendingMap().navigableKeySet();
        }
        
        @Override
        public NavigableMap descendingMap() {
            return this.h.new f(this.m().descendingMap());
        }
        
        @Override
        public Entry firstEntry() {
            final Map.Entry firstEntry = this.m().firstEntry();
            Entry f;
            if (firstEntry == null) {
                f = null;
            }
            else {
                f = ((c)this).f((Entry)firstEntry);
            }
            return f;
        }
        
        @Override
        public Entry floorEntry(final Object o) {
            final Map.Entry<Object, Object> floorEntry = this.m().floorEntry(o);
            Entry f;
            if (floorEntry == null) {
                f = null;
            }
            else {
                f = ((c)this).f((Entry)floorEntry);
            }
            return f;
        }
        
        @Override
        public Object floorKey(final Object o) {
            return this.m().floorKey(o);
        }
        
        @Override
        public NavigableMap headMap(final Object o, final boolean b) {
            return this.h.new f(this.m().headMap(o, b));
        }
        
        @Override
        public Entry higherEntry(final Object o) {
            final Map.Entry<Object, Object> higherEntry = this.m().higherEntry(o);
            Entry f;
            if (higherEntry == null) {
                f = null;
            }
            else {
                f = ((c)this).f((Entry)higherEntry);
            }
            return f;
        }
        
        @Override
        public Object higherKey(final Object o) {
            return this.m().higherKey(o);
        }
        
        public NavigableSet j() {
            return this.h.new g(this.m());
        }
        
        public NavigableMap k(final Object o) {
            return this.headMap(o, false);
        }
        
        @Override
        public NavigableSet keySet() {
            return (NavigableSet)super.h();
        }
        
        public Entry l(final Iterator iterator) {
            if (!iterator.hasNext()) {
                return null;
            }
            final Map.Entry<K, Collection> entry = iterator.next();
            final Collection<V> collection = this.h.createCollection();
            collection.addAll(entry.getValue());
            iterator.remove();
            return Maps.j(entry.getKey(), this.h.unmodifiableCollectionSubclass(collection));
        }
        
        @Override
        public Entry lastEntry() {
            final Map.Entry lastEntry = this.m().lastEntry();
            Entry f;
            if (lastEntry == null) {
                f = null;
            }
            else {
                f = ((c)this).f((Entry)lastEntry);
            }
            return f;
        }
        
        @Override
        public Entry lowerEntry(final Object o) {
            final Map.Entry<Object, Object> lowerEntry = this.m().lowerEntry(o);
            Entry f;
            if (lowerEntry == null) {
                f = null;
            }
            else {
                f = ((c)this).f((Entry)lowerEntry);
            }
            return f;
        }
        
        @Override
        public Object lowerKey(final Object o) {
            return this.m().lowerKey(o);
        }
        
        public NavigableMap m() {
            return (NavigableMap)super.i();
        }
        
        public NavigableMap n(final Object o, final Object o2) {
            return this.subMap(o, true, o2, false);
        }
        
        @Override
        public NavigableSet navigableKeySet() {
            return this.keySet();
        }
        
        public NavigableMap o(final Object o) {
            return this.tailMap(o, true);
        }
        
        @Override
        public Entry pollFirstEntry() {
            return this.l(((Maps.u)this).entrySet().iterator());
        }
        
        @Override
        public Entry pollLastEntry() {
            return this.l(this.descendingMap().entrySet().iterator());
        }
        
        @Override
        public NavigableMap subMap(final Object o, final boolean b, final Object o2, final boolean b2) {
            return this.h.new f(this.m().subMap(o, b, o2, b2));
        }
        
        @Override
        public NavigableMap tailMap(final Object o, final boolean b) {
            return this.h.new f(this.m().tailMap(o, b));
        }
    }
    
    public class i extends c implements SortedMap
    {
        public SortedSet f;
        public final AbstractMapBasedMultimap g;
        
        public i(final AbstractMapBasedMultimap g, final SortedMap sortedMap) {
            this.g = g.super(sortedMap);
        }
        
        @Override
        public Comparator comparator() {
            return this.i().comparator();
        }
        
        @Override
        public Object firstKey() {
            return this.i().firstKey();
        }
        
        public SortedSet g() {
            return this.g.new j(this.i());
        }
        
        public SortedSet h() {
            SortedSet f;
            if ((f = this.f) == null) {
                f = this.g();
                this.f = f;
            }
            return f;
        }
        
        @Override
        public SortedMap headMap(final Object o) {
            return this.g.new i(this.i().headMap(o));
        }
        
        public SortedMap i() {
            return (SortedMap)super.d;
        }
        
        @Override
        public Object lastKey() {
            return this.i().lastKey();
        }
        
        @Override
        public SortedMap subMap(final Object o, final Object o2) {
            return this.g.new i(this.i().subMap(o, o2));
        }
        
        @Override
        public SortedMap tailMap(final Object o) {
            return this.g.new i(this.i().tailMap(o));
        }
    }
    
    public class g extends j implements NavigableSet
    {
        public final AbstractMapBasedMultimap d;
        
        public g(final AbstractMapBasedMultimap d, final NavigableMap navigableMap) {
            this.d = d.super(navigableMap);
        }
        
        public NavigableSet c(final Object o) {
            return this.headSet(o, false);
        }
        
        @Override
        public Object ceiling(final Object o) {
            return this.g().ceilingKey(o);
        }
        
        @Override
        public Iterator descendingIterator() {
            return this.descendingSet().iterator();
        }
        
        @Override
        public NavigableSet descendingSet() {
            return this.d.new g(this.g().descendingMap());
        }
        
        @Override
        public Object floor(final Object o) {
            return this.g().floorKey(o);
        }
        
        public NavigableMap g() {
            return (NavigableMap)super.b();
        }
        
        @Override
        public NavigableSet headSet(final Object o, final boolean b) {
            return this.d.new g(this.g().headMap(o, b));
        }
        
        @Override
        public Object higher(final Object o) {
            return this.g().higherKey(o);
        }
        
        public NavigableSet i(final Object o, final Object o2) {
            return this.subSet(o, true, o2, false);
        }
        
        public NavigableSet l(final Object o) {
            return this.tailSet(o, true);
        }
        
        @Override
        public Object lower(final Object o) {
            return this.g().lowerKey(o);
        }
        
        @Override
        public Object pollFirst() {
            return Iterators.q(((e)this).iterator());
        }
        
        @Override
        public Object pollLast() {
            return Iterators.q(this.descendingIterator());
        }
        
        @Override
        public NavigableSet subSet(final Object o, final boolean b, final Object o2, final boolean b2) {
            return this.d.new g(this.g().subMap(o, b, o2, b2));
        }
        
        @Override
        public NavigableSet tailSet(final Object o, final boolean b) {
            return this.d.new g(this.g().tailMap(o, b));
        }
    }
    
    public class j extends e implements SortedSet
    {
        public final AbstractMapBasedMultimap c;
        
        public j(final AbstractMapBasedMultimap c, final SortedMap sortedMap) {
            this.c = c.super(sortedMap);
        }
        
        public SortedMap b() {
            return (SortedMap)super.a();
        }
        
        @Override
        public Comparator comparator() {
            return this.b().comparator();
        }
        
        @Override
        public Object first() {
            return this.b().firstKey();
        }
        
        @Override
        public SortedSet headSet(final Object o) {
            return this.c.new j(this.b().headMap(o));
        }
        
        @Override
        public Object last() {
            return this.b().lastKey();
        }
        
        @Override
        public SortedSet subSet(final Object o, final Object o2) {
            return this.c.new j(this.b().subMap(o, o2));
        }
        
        @Override
        public SortedSet tailSet(final Object o) {
            return this.c.new j(this.b().tailMap(o));
        }
    }
    
    public class h extends l implements RandomAccess
    {
        public h(final AbstractMapBasedMultimap abstractMapBasedMultimap, final Object o, final List list, final k k) {
            abstractMapBasedMultimap.super(o, list, k);
        }
    }
    
    public class l extends k implements List
    {
        public final AbstractMapBasedMultimap f;
        
        public l(final AbstractMapBasedMultimap f, final Object o, final List list, final k k) {
            this.f = f.super(o, list, k);
        }
        
        @Override
        public void add(final int n, final Object o) {
            ((k)this).i();
            final boolean empty = ((k)this).c().isEmpty();
            this.m().add(n, o);
            AbstractMapBasedMultimap.access$208(this.f);
            if (empty) {
                ((k)this).a();
            }
        }
        
        @Override
        public boolean addAll(int size, final Collection collection) {
            if (collection.isEmpty()) {
                return false;
            }
            final int size2 = ((k)this).size();
            final boolean addAll = this.m().addAll(size, collection);
            if (addAll) {
                size = ((k)this).c().size();
                AbstractMapBasedMultimap.access$212(this.f, size - size2);
                if (size2 == 0) {
                    ((k)this).a();
                }
            }
            return addAll;
        }
        
        @Override
        public Object get(final int n) {
            ((k)this).i();
            return this.m().get(n);
        }
        
        @Override
        public int indexOf(final Object o) {
            ((k)this).i();
            return this.m().indexOf(o);
        }
        
        @Override
        public int lastIndexOf(final Object o) {
            ((k)this).i();
            return this.m().lastIndexOf(o);
        }
        
        @Override
        public ListIterator listIterator() {
            ((k)this).i();
            return new a();
        }
        
        @Override
        public ListIterator listIterator(final int n) {
            ((k)this).i();
            return new a(n);
        }
        
        public List m() {
            return (List)((k)this).c();
        }
        
        @Override
        public Object remove(final int n) {
            ((k)this).i();
            final Object remove = this.m().remove(n);
            AbstractMapBasedMultimap.access$210(this.f);
            ((k)this).l();
            return remove;
        }
        
        @Override
        public Object set(final int n, final Object o) {
            ((k)this).i();
            return this.m().set(n, o);
        }
        
        @Override
        public List subList(final int n, final int n2) {
            ((k)this).i();
            final AbstractMapBasedMultimap f = this.f;
            final Object g = ((k)this).g();
            final List subList = this.m().subList(n, n2);
            AbstractCollection b;
            if (((k)this).b() == null) {
                b = this;
            }
            else {
                b = ((k)this).b();
            }
            return f.wrapList(g, subList, (k)b);
        }
        
        public class a extends k.a implements ListIterator
        {
            public final l d;
            
            public a(final l d) {
                (k)(this.d = d).super();
            }
            
            public a(final l d, final int n) {
                (k)(this.d = d).super(d.m().listIterator(n));
            }
            
            @Override
            public void add(final Object o) {
                final boolean empty = this.d.isEmpty();
                this.d().add(o);
                AbstractMapBasedMultimap.access$208(this.d.f);
                if (empty) {
                    ((k)this.d).a();
                }
            }
            
            public final ListIterator d() {
                return (ListIterator)((k.a)this).b();
            }
            
            @Override
            public boolean hasPrevious() {
                return this.d().hasPrevious();
            }
            
            @Override
            public int nextIndex() {
                return this.d().nextIndex();
            }
            
            @Override
            public Object previous() {
                return this.d().previous();
            }
            
            @Override
            public int previousIndex() {
                return this.d().previousIndex();
            }
            
            @Override
            public void set(final Object o) {
                this.d().set(o);
            }
        }
    }
    
    public class k extends AbstractCollection
    {
        public final Object a;
        public Collection b;
        public final k c;
        public final Collection d;
        public final AbstractMapBasedMultimap e;
        
        public k(final AbstractMapBasedMultimap e, final Object a, final Collection b, final k c) {
            this.e = e;
            this.a = a;
            this.b = b;
            this.c = c;
            Collection c2;
            if (c == null) {
                c2 = null;
            }
            else {
                c2 = c.c();
            }
            this.d = c2;
        }
        
        public void a() {
            final k c = this.c;
            if (c != null) {
                c.a();
            }
            else {
                AbstractMapBasedMultimap.access$000(this.e).put(this.a, this.b);
            }
        }
        
        @Override
        public boolean add(final Object o) {
            this.i();
            final boolean empty = this.b.isEmpty();
            final boolean add = this.b.add(o);
            if (add) {
                AbstractMapBasedMultimap.access$208(this.e);
                if (empty) {
                    this.a();
                }
            }
            return add;
        }
        
        @Override
        public boolean addAll(final Collection collection) {
            if (collection.isEmpty()) {
                return false;
            }
            final int size = this.size();
            final boolean addAll = this.b.addAll(collection);
            if (addAll) {
                AbstractMapBasedMultimap.access$212(this.e, this.b.size() - size);
                if (size == 0) {
                    this.a();
                }
            }
            return addAll;
        }
        
        public k b() {
            return this.c;
        }
        
        public Collection c() {
            return this.b;
        }
        
        @Override
        public void clear() {
            final int size = this.size();
            if (size == 0) {
                return;
            }
            this.b.clear();
            AbstractMapBasedMultimap.access$220(this.e, size);
            this.l();
        }
        
        @Override
        public boolean contains(final Object o) {
            this.i();
            return this.b.contains(o);
        }
        
        @Override
        public boolean containsAll(final Collection collection) {
            this.i();
            return this.b.containsAll(collection);
        }
        
        @Override
        public boolean equals(final Object o) {
            if (o == this) {
                return true;
            }
            this.i();
            return this.b.equals(o);
        }
        
        Object g() {
            return this.a;
        }
        
        @Override
        public int hashCode() {
            this.i();
            return this.b.hashCode();
        }
        
        public void i() {
            final k c = this.c;
            if (c != null) {
                c.i();
                if (this.c.c() != this.d) {
                    throw new ConcurrentModificationException();
                }
            }
            else if (this.b.isEmpty()) {
                final Collection b = AbstractMapBasedMultimap.access$000(this.e).get(this.a);
                if (b != null) {
                    this.b = b;
                }
            }
        }
        
        @Override
        public Iterator iterator() {
            this.i();
            return new a();
        }
        
        public void l() {
            final k c = this.c;
            if (c != null) {
                c.l();
            }
            else if (this.b.isEmpty()) {
                AbstractMapBasedMultimap.access$000(this.e).remove(this.a);
            }
        }
        
        @Override
        public boolean remove(final Object o) {
            this.i();
            final boolean remove = this.b.remove(o);
            if (remove) {
                AbstractMapBasedMultimap.access$210(this.e);
                this.l();
            }
            return remove;
        }
        
        @Override
        public boolean removeAll(final Collection collection) {
            if (collection.isEmpty()) {
                return false;
            }
            final int size = this.size();
            final boolean removeAll = this.b.removeAll(collection);
            if (removeAll) {
                AbstractMapBasedMultimap.access$212(this.e, this.b.size() - size);
                this.l();
            }
            return removeAll;
        }
        
        @Override
        public boolean retainAll(final Collection collection) {
            i71.r(collection);
            final int size = this.size();
            final boolean retainAll = this.b.retainAll(collection);
            if (retainAll) {
                AbstractMapBasedMultimap.access$212(this.e, this.b.size() - size);
                this.l();
            }
            return retainAll;
        }
        
        @Override
        public int size() {
            this.i();
            return this.b.size();
        }
        
        @Override
        public String toString() {
            this.i();
            return this.b.toString();
        }
        
        public class a implements Iterator
        {
            public final Iterator a;
            public final Collection b;
            public final k c;
            
            public a(final k c) {
                this.c = c;
                final Collection b = c.b;
                this.b = b;
                this.a = iteratorOrListIterator((Collection<Object>)b);
            }
            
            public a(final k c, final Iterator a) {
                this.c = c;
                this.b = c.b;
                this.a = a;
            }
            
            public Iterator b() {
                this.c();
                return this.a;
            }
            
            public void c() {
                this.c.i();
                if (this.c.b == this.b) {
                    return;
                }
                throw new ConcurrentModificationException();
            }
            
            @Override
            public boolean hasNext() {
                this.c();
                return this.a.hasNext();
            }
            
            @Override
            public Object next() {
                this.c();
                return this.a.next();
            }
            
            @Override
            public void remove() {
                this.a.remove();
                AbstractMapBasedMultimap.access$210(this.c.e);
                this.c.l();
            }
        }
    }
    
    public class m extends o implements NavigableSet
    {
        public final AbstractMapBasedMultimap g;
        
        public m(final AbstractMapBasedMultimap g, final Object o, final NavigableSet set, final k k) {
            this.g = g.super(o, set, k);
        }
        
        @Override
        public Object ceiling(final Object o) {
            return this.n().ceiling(o);
        }
        
        @Override
        public Iterator descendingIterator() {
            return (k)this.new a(this.n().descendingIterator());
        }
        
        @Override
        public NavigableSet descendingSet() {
            return this.o(this.n().descendingSet());
        }
        
        @Override
        public Object floor(final Object o) {
            return this.n().floor(o);
        }
        
        @Override
        public NavigableSet headSet(final Object o, final boolean b) {
            return this.o(this.n().headSet(o, b));
        }
        
        @Override
        public Object higher(final Object o) {
            return this.n().higher(o);
        }
        
        @Override
        public Object lower(final Object o) {
            return this.n().lower(o);
        }
        
        public NavigableSet n() {
            return (NavigableSet)super.m();
        }
        
        public final NavigableSet o(final NavigableSet set) {
            final AbstractMapBasedMultimap g = this.g;
            final Object a = super.a;
            AbstractCollection b;
            if (((k)this).b() == null) {
                b = this;
            }
            else {
                b = ((k)this).b();
            }
            return g.new m(a, set, (k)b);
        }
        
        @Override
        public Object pollFirst() {
            return Iterators.q(((k)this).iterator());
        }
        
        @Override
        public Object pollLast() {
            return Iterators.q(this.descendingIterator());
        }
        
        @Override
        public NavigableSet subSet(final Object o, final boolean b, final Object o2, final boolean b2) {
            return this.o(this.n().subSet(o, b, o2, b2));
        }
        
        @Override
        public NavigableSet tailSet(final Object o, final boolean b) {
            return this.o(this.n().tailSet(o, b));
        }
    }
    
    public class o extends k implements SortedSet
    {
        public final AbstractMapBasedMultimap f;
        
        public o(final AbstractMapBasedMultimap f, final Object o, final SortedSet set, final k k) {
            this.f = f.super(o, set, k);
        }
        
        @Override
        public Comparator comparator() {
            return this.m().comparator();
        }
        
        @Override
        public Object first() {
            ((k)this).i();
            return this.m().first();
        }
        
        @Override
        public SortedSet headSet(final Object o) {
            ((k)this).i();
            final AbstractMapBasedMultimap f = this.f;
            final Object g = ((k)this).g();
            final SortedSet<Object> headSet = this.m().headSet(o);
            AbstractCollection b;
            if (((k)this).b() == null) {
                b = this;
            }
            else {
                b = ((k)this).b();
            }
            return f.new o(g, headSet, (k)b);
        }
        
        @Override
        public Object last() {
            ((k)this).i();
            return this.m().last();
        }
        
        public SortedSet m() {
            return (SortedSet)((k)this).c();
        }
        
        @Override
        public SortedSet subSet(final Object o, final Object o2) {
            ((k)this).i();
            final AbstractMapBasedMultimap f = this.f;
            final Object g = ((k)this).g();
            final SortedSet<Object> subSet = this.m().subSet(o, o2);
            AbstractCollection b;
            if (((k)this).b() == null) {
                b = this;
            }
            else {
                b = ((k)this).b();
            }
            return f.new o(g, subSet, (k)b);
        }
        
        @Override
        public SortedSet tailSet(final Object o) {
            ((k)this).i();
            final AbstractMapBasedMultimap f = this.f;
            final Object g = ((k)this).g();
            final SortedSet<Object> tailSet = this.m().tailSet(o);
            AbstractCollection b;
            if (((k)this).b() == null) {
                b = this;
            }
            else {
                b = ((k)this).b();
            }
            return f.new o(g, tailSet, (k)b);
        }
    }
    
    public class n extends k implements Set
    {
        public final AbstractMapBasedMultimap f;
        
        public n(final AbstractMapBasedMultimap f, final Object o, final Set set) {
            this.f = f.super(o, set, null);
        }
        
        @Override
        public boolean removeAll(final Collection collection) {
            if (collection.isEmpty()) {
                return false;
            }
            final int size = ((k)this).size();
            final boolean f = Sets.f((Set)super.b, collection);
            if (f) {
                AbstractMapBasedMultimap.access$212(this.f, super.b.size() - size);
                ((k)this).l();
            }
            return f;
        }
    }
}

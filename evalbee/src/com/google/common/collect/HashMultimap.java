// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Set;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.Map;
import java.io.ObjectInputStream;

public final class HashMultimap<K, V> extends HashMultimapGwtSerializationDependencies<K, V>
{
    private static final int DEFAULT_VALUES_PER_KEY = 2;
    private static final long serialVersionUID = 0L;
    transient int expectedValuesPerKey;
    
    private HashMultimap() {
        this(12, 2);
    }
    
    private HashMultimap(final int n, final int expectedValuesPerKey) {
        super(com.google.common.collect.l.c(n));
        this.expectedValuesPerKey = 2;
        i71.d(expectedValuesPerKey >= 0);
        this.expectedValuesPerKey = expectedValuesPerKey;
    }
    
    private HashMultimap(final rx0 rx0) {
        super(com.google.common.collect.l.c(rx0.keySet().size()));
        this.expectedValuesPerKey = 2;
        this.putAll(rx0);
    }
    
    public static <K, V> HashMultimap<K, V> create() {
        return new HashMultimap<K, V>();
    }
    
    public static <K, V> HashMultimap<K, V> create(final int n, final int n2) {
        return new HashMultimap<K, V>(n, n2);
    }
    
    public static <K, V> HashMultimap<K, V> create(final rx0 rx0) {
        return new HashMultimap<K, V>(rx0);
    }
    
    private void readObject(final ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        this.expectedValuesPerKey = 2;
        final int h = com.google.common.collect.n.h(objectInputStream);
        this.setMap(com.google.common.collect.l.c(12));
        com.google.common.collect.n.e(this, objectInputStream, h);
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        com.google.common.collect.n.j(this, objectOutputStream);
    }
    
    public Set<V> createCollection() {
        return com.google.common.collect.l.d(this.expectedValuesPerKey);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.TreeSet;
import java.util.SortedSet;
import java.util.Comparator;
import java.io.Serializable;

final class MultimapBuilder$TreeSetSupplier<V> implements is1, Serializable
{
    private final Comparator<? super V> comparator;
    
    public MultimapBuilder$TreeSetSupplier(final Comparator<? super V> comparator) {
        this.comparator = (Comparator)i71.r(comparator);
    }
    
    @Override
    public SortedSet<V> get() {
        return new TreeSet<V>(this.comparator);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;

final class ImmutableMapValues<K, V> extends ImmutableCollection<V>
{
    private final ImmutableMap<K, V> map;
    
    public ImmutableMapValues(final ImmutableMap<K, V> map) {
        this.map = map;
    }
    
    public static /* synthetic */ ImmutableMap access$000(final ImmutableMapValues immutableMapValues) {
        return immutableMapValues.map;
    }
    
    @Override
    public ImmutableList<V> asList() {
        return new ImmutableList<V>(this, this.map.entrySet().asList()) {
            final ImmutableList val$entryList;
            
            @Override
            public V get(final int n) {
                return ((Map.Entry)this.val$entryList.get(n)).getValue();
            }
            
            @Override
            public boolean isPartialView() {
                return true;
            }
            
            @Override
            public int size() {
                return this.val$entryList.size();
            }
        };
    }
    
    @Override
    public boolean contains(final Object o) {
        return o != null && Iterators.f(this.iterator(), o);
    }
    
    @Override
    public boolean isPartialView() {
        return true;
    }
    
    @Override
    public w02 iterator() {
        return new w02(this) {
            public final w02 a = ImmutableMapValues.access$000(b).entrySet().iterator();
            public final ImmutableMapValues b;
            
            @Override
            public boolean hasNext() {
                return this.a.hasNext();
            }
            
            @Override
            public Object next() {
                return this.a.next().getValue();
            }
        };
    }
    
    @Override
    public int size() {
        return this.map.size();
    }
    
    public Object writeReplace() {
        return new SerializedForm((ImmutableMap<?, Object>)this.map);
    }
    
    public static class SerializedForm<V> implements Serializable
    {
        private static final long serialVersionUID = 0L;
        final ImmutableMap<?, V> map;
        
        public SerializedForm(final ImmutableMap<?, V> map) {
            this.map = map;
        }
        
        public Object readResolve() {
            return this.map.values();
        }
    }
}

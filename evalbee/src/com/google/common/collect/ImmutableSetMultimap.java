// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import com.google.common.base.a;
import java.util.Set;
import java.io.ObjectOutputStream;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.util.Iterator;
import java.util.Collection;
import java.util.Comparator;
import java.util.Map;

public class ImmutableSetMultimap<K, V> extends ImmutableMultimap<K, V> implements pm1
{
    private static final long serialVersionUID = 0L;
    private final transient ImmutableSet<V> emptySet;
    private transient ImmutableSet<Map.Entry<K, V>> entries;
    private transient ImmutableSetMultimap<V, K> inverse;
    
    public ImmutableSetMultimap(final ImmutableMap<K, ImmutableSet<V>> immutableMap, final int n, final Comparator<? super V> comparator) {
        super((ImmutableMap<K, ? extends ImmutableCollection<Object>>)immutableMap, n);
        this.emptySet = emptySet(comparator);
    }
    
    public static <K, V> a builder() {
        return new a();
    }
    
    public static <K, V> ImmutableSetMultimap<K, V> copyOf(final Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
        return new a().i(iterable).f();
    }
    
    public static <K, V> ImmutableSetMultimap<K, V> copyOf(final rx0 rx0) {
        return copyOf(rx0, (Comparator<? super V>)null);
    }
    
    private static <K, V> ImmutableSetMultimap<K, V> copyOf(final rx0 rx0, final Comparator<? super V> comparator) {
        i71.r(rx0);
        if (rx0.isEmpty() && comparator == null) {
            return of();
        }
        if (rx0 instanceof ImmutableSetMultimap) {
            final ImmutableSetMultimap immutableSetMultimap = (ImmutableSetMultimap)rx0;
            if (!immutableSetMultimap.isPartialView()) {
                return immutableSetMultimap;
            }
        }
        return fromMapEntries((Collection<? extends Map.Entry<? extends K, ? extends Collection<? extends V>>>)rx0.asMap().entrySet(), comparator);
    }
    
    private static <V> ImmutableSet<V> emptySet(final Comparator<? super V> comparator) {
        ImmutableSet<Object> set;
        if (comparator == null) {
            set = ImmutableSet.of();
        }
        else {
            set = ImmutableSortedSet.emptySet((Comparator<? super Object>)comparator);
        }
        return (ImmutableSet<V>)set;
    }
    
    public static <K, V> ImmutableSetMultimap<K, V> fromMapEntries(final Collection<? extends Map.Entry<? extends K, ? extends Collection<? extends V>>> collection, final Comparator<? super V> comparator) {
        if (collection.isEmpty()) {
            return of();
        }
        final ImmutableMap.b b = new ImmutableMap.b(collection.size());
        final Iterator iterator = collection.iterator();
        int n = 0;
        while (iterator.hasNext()) {
            final Map.Entry<Object, V> entry = (Map.Entry<Object, V>)iterator.next();
            final Object key = entry.getKey();
            final ImmutableSet<Object> valueSet = valueSet((Comparator<? super Object>)comparator, (Collection<?>)entry.getValue());
            if (!valueSet.isEmpty()) {
                b.g(key, valueSet);
                n += valueSet.size();
            }
        }
        return new ImmutableSetMultimap<K, V>(b.d(), n, (Comparator<? super Object>)comparator);
    }
    
    private ImmutableSetMultimap<V, K> invert() {
        final a builder = builder();
        for (final Map.Entry<K, Object> entry : this.entries()) {
            builder.g(entry.getValue(), entry.getKey());
        }
        final ImmutableSetMultimap f = builder.f();
        f.inverse = (ImmutableSetMultimap<V, K>)this;
        return f;
    }
    
    public static <K, V> ImmutableSetMultimap<K, V> of() {
        return (ImmutableSetMultimap<K, V>)EmptyImmutableSetMultimap.INSTANCE;
    }
    
    public static <K, V> ImmutableSetMultimap<K, V> of(final K k, final V v) {
        final a builder = builder();
        builder.g(k, v);
        return builder.f();
    }
    
    public static <K, V> ImmutableSetMultimap<K, V> of(final K k, final V v, final K i, final V v2) {
        final a builder = builder();
        builder.g(k, v);
        builder.g(i, v2);
        return builder.f();
    }
    
    public static <K, V> ImmutableSetMultimap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3) {
        final a builder = builder();
        builder.g(k, v);
        builder.g(i, v2);
        builder.g(j, v3);
        return builder.f();
    }
    
    public static <K, V> ImmutableSetMultimap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4) {
        final a builder = builder();
        builder.g(k, v);
        builder.g(i, v2);
        builder.g(j, v3);
        builder.g(l, v4);
        return builder.f();
    }
    
    public static <K, V> ImmutableSetMultimap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4, final K m, final V v5) {
        final a builder = builder();
        builder.g(k, v);
        builder.g(i, v2);
        builder.g(j, v3);
        builder.g(l, v4);
        builder.g(m, v5);
        return builder.f();
    }
    
    private void readObject(final ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        final Comparator comparator = (Comparator)objectInputStream.readObject();
        final int int1 = objectInputStream.readInt();
        if (int1 >= 0) {
            final ImmutableMap.b builder = ImmutableMap.builder();
            int i = 0;
            int n = 0;
            while (i < int1) {
                final Object object = objectInputStream.readObject();
                final int int2 = objectInputStream.readInt();
                if (int2 <= 0) {
                    final StringBuilder sb = new StringBuilder(31);
                    sb.append("Invalid value count ");
                    sb.append(int2);
                    throw new InvalidObjectException(sb.toString());
                }
                final ImmutableSet.a valuesBuilder = valuesBuilder(comparator);
                for (int j = 0; j < int2; ++j) {
                    valuesBuilder.i(objectInputStream.readObject());
                }
                final ImmutableSet m = valuesBuilder.m();
                if (m.size() != int2) {
                    final String value = String.valueOf(object);
                    final StringBuilder sb2 = new StringBuilder(value.length() + 40);
                    sb2.append("Duplicate key-value pairs exist for key ");
                    sb2.append(value);
                    throw new InvalidObjectException(sb2.toString());
                }
                builder.g(object, m);
                n += int2;
                ++i;
            }
            try {
                d.a.b(this, builder.d());
                d.b.a(this, n);
                b.a.b(this, emptySet(comparator));
                return;
            }
            catch (final IllegalArgumentException cause) {
                throw (InvalidObjectException)new InvalidObjectException(cause.getMessage()).initCause(cause);
            }
        }
        final StringBuilder sb3 = new StringBuilder(29);
        sb3.append("Invalid key count ");
        sb3.append(int1);
        throw new InvalidObjectException(sb3.toString());
    }
    
    private static <V> ImmutableSet<V> valueSet(final Comparator<? super V> comparator, final Collection<? extends V> collection) {
        ImmutableSet<Object> set;
        if (comparator == null) {
            set = ImmutableSet.copyOf((Collection<?>)collection);
        }
        else {
            set = ImmutableSortedSet.copyOf((Comparator<? super Object>)comparator, (Collection<?>)collection);
        }
        return (ImmutableSet<V>)set;
    }
    
    private static <V> ImmutableSet.a valuesBuilder(final Comparator<? super V> comparator) {
        ImmutableSet.a a;
        if (comparator == null) {
            a = new ImmutableSet.a();
        }
        else {
            a = new ImmutableSortedSet.a(comparator);
        }
        return a;
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(this.valueComparator());
        n.j(this, objectOutputStream);
    }
    
    @Override
    public ImmutableSet<Map.Entry<K, V>> entries() {
        ImmutableSet<Map.Entry<K, V>> entries;
        if ((entries = this.entries) == null) {
            entries = (ImmutableSet<Map.Entry<K, V>>)new EntrySet((ImmutableSetMultimap<Object, Object>)this);
            this.entries = entries;
        }
        return entries;
    }
    
    @Override
    public ImmutableSet<V> get(final K k) {
        return (ImmutableSet)com.google.common.base.a.a(super.map.get(k), this.emptySet);
    }
    
    @Override
    public ImmutableSetMultimap<V, K> inverse() {
        ImmutableSetMultimap<V, K> inverse;
        if ((inverse = this.inverse) == null) {
            inverse = this.invert();
            this.inverse = inverse;
        }
        return inverse;
    }
    
    @Deprecated
    @Override
    public final ImmutableSet<V> removeAll(final Object o) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public final ImmutableSet<V> replaceValues(final K k, final Iterable<? extends V> iterable) {
        throw new UnsupportedOperationException();
    }
    
    public Comparator<? super V> valueComparator() {
        final ImmutableSet<V> emptySet = this.emptySet;
        Comparator comparator;
        if (emptySet instanceof ImmutableSortedSet) {
            comparator = ((ImmutableSortedSet)emptySet).comparator();
        }
        else {
            comparator = null;
        }
        return comparator;
    }
    
    public static final class EntrySet<K, V> extends ImmutableSet<Map.Entry<K, V>>
    {
        private final transient ImmutableSetMultimap<K, V> multimap;
        
        public EntrySet(final ImmutableSetMultimap<K, V> multimap) {
            this.multimap = multimap;
        }
        
        @Override
        public boolean contains(final Object o) {
            if (o instanceof Map.Entry) {
                final Map.Entry entry = (Map.Entry)o;
                return this.multimap.containsEntry(entry.getKey(), entry.getValue());
            }
            return false;
        }
        
        @Override
        public boolean isPartialView() {
            return false;
        }
        
        @Override
        public w02 iterator() {
            return this.multimap.entryIterator();
        }
        
        @Override
        public int size() {
            return this.multimap.size();
        }
    }
    
    public static final class a extends c
    {
        @Override
        public Collection b() {
            return l.g();
        }
        
        public ImmutableSetMultimap f() {
            final Set entrySet = super.a.entrySet();
            final Comparator b = super.b;
            Object immutableSortedCopy = entrySet;
            if (b != null) {
                immutableSortedCopy = Ordering.from((Comparator<Object>)b).onKeys().immutableSortedCopy(entrySet);
            }
            return ImmutableSetMultimap.fromMapEntries((Collection<? extends Map.Entry<?, ? extends Collection<?>>>)immutableSortedCopy, (Comparator<? super Object>)super.c);
        }
        
        public a g(final Object o, final Object o2) {
            super.c(o, o2);
            return this;
        }
        
        public a h(final Map.Entry entry) {
            super.d(entry);
            return this;
        }
        
        public a i(final Iterable iterable) {
            super.e(iterable);
            return this;
        }
    }
    
    public abstract static final class b
    {
        public static final n.b a;
        
        static {
            a = n.a(ImmutableSetMultimap.class, "emptySet");
        }
    }
}

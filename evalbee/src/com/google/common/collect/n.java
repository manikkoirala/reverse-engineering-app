// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.lang.reflect.Field;
import java.util.Iterator;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.io.ObjectInputStream;
import java.util.Map;

public abstract class n
{
    public static b a(final Class clazz, final String name) {
        try {
            return new b(clazz.getDeclaredField(name), null);
        }
        catch (final NoSuchFieldException detailMessage) {
            throw new AssertionError((Object)detailMessage);
        }
    }
    
    public static void b(final Map map, final ObjectInputStream objectInputStream) {
        c(map, objectInputStream, objectInputStream.readInt());
    }
    
    public static void c(final Map map, final ObjectInputStream objectInputStream, final int n) {
        for (int i = 0; i < n; ++i) {
            map.put(objectInputStream.readObject(), objectInputStream.readObject());
        }
    }
    
    public static void d(final rx0 rx0, final ObjectInputStream objectInputStream) {
        e(rx0, objectInputStream, objectInputStream.readInt());
    }
    
    public static void e(final rx0 rx0, final ObjectInputStream objectInputStream, final int n) {
        for (int i = 0; i < n; ++i) {
            final Collection value = rx0.get(objectInputStream.readObject());
            for (int int1 = objectInputStream.readInt(), j = 0; j < int1; ++j) {
                value.add(objectInputStream.readObject());
            }
        }
    }
    
    public static void f(final j j, final ObjectInputStream objectInputStream) {
        g(j, objectInputStream, objectInputStream.readInt());
    }
    
    public static void g(final j j, final ObjectInputStream objectInputStream, final int n) {
        for (int i = 0; i < n; ++i) {
            j.add(objectInputStream.readObject(), objectInputStream.readInt());
        }
    }
    
    public static int h(final ObjectInputStream objectInputStream) {
        return objectInputStream.readInt();
    }
    
    public static void i(final Map map, final ObjectOutputStream objectOutputStream) {
        objectOutputStream.writeInt(map.size());
        for (final Map.Entry<Object, V> entry : map.entrySet()) {
            objectOutputStream.writeObject(entry.getKey());
            objectOutputStream.writeObject(entry.getValue());
        }
    }
    
    public static void j(final rx0 rx0, final ObjectOutputStream objectOutputStream) {
        objectOutputStream.writeInt(rx0.asMap().size());
        for (final Map.Entry<Object, V> entry : rx0.asMap().entrySet()) {
            objectOutputStream.writeObject(entry.getKey());
            objectOutputStream.writeInt(((Collection)entry.getValue()).size());
            final Iterator iterator2 = ((Collection)entry.getValue()).iterator();
            while (iterator2.hasNext()) {
                objectOutputStream.writeObject(iterator2.next());
            }
        }
    }
    
    public static void k(final j j, final ObjectOutputStream objectOutputStream) {
        objectOutputStream.writeInt(j.entrySet().size());
        for (final j.a a : j.entrySet()) {
            objectOutputStream.writeObject(a.getElement());
            objectOutputStream.writeInt(a.getCount());
        }
    }
    
    public static final class b
    {
        public final Field a;
        
        public b(final Field a) {
            (this.a = a).setAccessible(true);
        }
        
        public void a(final Object obj, final int i) {
            try {
                this.a.set(obj, i);
            }
            catch (final IllegalAccessException detailMessage) {
                throw new AssertionError((Object)detailMessage);
            }
        }
        
        public void b(final Object obj, final Object value) {
            try {
                this.a.set(obj, value);
            }
            catch (final IllegalAccessException detailMessage) {
                throw new AssertionError((Object)detailMessage);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import com.google.common.base.Predicates;
import java.util.Objects;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.Map;
import java.io.Serializable;

class StandardTable<R, C, V> extends com.google.common.collect.e implements Serializable
{
    private static final long serialVersionUID = 0L;
    final Map<R, Map<C, V>> backingMap;
    private transient Set<C> columnKeySet;
    private transient f columnMap;
    final is1 factory;
    private transient Map<R, Map<C, V>> rowMap;
    
    public StandardTable(final Map<R, Map<C, V>> backingMap, final is1 factory) {
        this.backingMap = backingMap;
        this.factory = factory;
    }
    
    private boolean containsMapping(final Object o, final Object o2, final Object o3) {
        return o3 != null && o3.equals(this.get(o, o2));
    }
    
    private Map<C, V> getOrCreate(final R r) {
        Map map;
        if ((map = this.backingMap.get(r)) == null) {
            map = (Map)this.factory.get();
            this.backingMap.put(r, map);
        }
        return map;
    }
    
    private Map<R, V> removeColumn(final Object o) {
        final LinkedHashMap linkedHashMap = new LinkedHashMap();
        final Iterator<Map.Entry<R, Map<C, V>>> iterator = this.backingMap.entrySet().iterator();
        while (iterator.hasNext()) {
            final Map.Entry<K, Map> entry = (Map.Entry<K, Map>)iterator.next();
            final Object remove = entry.getValue().remove(o);
            if (remove != null) {
                linkedHashMap.put(entry.getKey(), remove);
                if (!entry.getValue().isEmpty()) {
                    continue;
                }
                iterator.remove();
            }
        }
        return linkedHashMap;
    }
    
    private boolean removeMapping(final Object o, final Object o2, final Object o3) {
        if (this.containsMapping(o, o2, o3)) {
            this.remove(o, o2);
            return true;
        }
        return false;
    }
    
    @Override
    Iterator<q.a> cellIterator() {
        return new b(null);
    }
    
    @Override
    public Set<q.a> cellSet() {
        return super.cellSet();
    }
    
    @Override
    public void clear() {
        this.backingMap.clear();
    }
    
    @Override
    public Map<R, V> column(final C c) {
        return new c(c);
    }
    
    @Override
    public Set<C> columnKeySet() {
        Set<C> columnKeySet;
        if ((columnKeySet = this.columnKeySet) == null) {
            columnKeySet = new e(null);
            this.columnKeySet = columnKeySet;
        }
        return columnKeySet;
    }
    
    @Override
    public Map<C, Map<R, V>> columnMap() {
        f columnMap;
        if ((columnMap = this.columnMap) == null) {
            columnMap = new f(null);
            this.columnMap = columnMap;
        }
        return columnMap;
    }
    
    @Override
    public boolean contains(final Object o, final Object o2) {
        return o != null && o2 != null && super.contains(o, o2);
    }
    
    @Override
    public boolean containsColumn(final Object o) {
        if (o == null) {
            return false;
        }
        final Iterator<Map<C, V>> iterator = this.backingMap.values().iterator();
        while (iterator.hasNext()) {
            if (Maps.v(iterator.next(), o)) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public boolean containsRow(final Object o) {
        return o != null && Maps.v(this.backingMap, o);
    }
    
    @Override
    public boolean containsValue(final Object o) {
        return o != null && super.containsValue(o);
    }
    
    public Iterator<C> createColumnKeyIterator() {
        return new d(null);
    }
    
    public Map<R, Map<C, V>> createRowMap() {
        return new h();
    }
    
    @Override
    public V get(Object value, final Object o) {
        if (value != null && o != null) {
            value = super.get(value, o);
        }
        else {
            value = null;
        }
        return (V)value;
    }
    
    @Override
    public boolean isEmpty() {
        return this.backingMap.isEmpty();
    }
    
    @Override
    public V put(final R r, final C c, final V v) {
        i71.r(r);
        i71.r(c);
        i71.r(v);
        return this.getOrCreate(r).put(c, v);
    }
    
    @Override
    public V remove(final Object o, Object remove) {
        if (o == null || remove == null) {
            return null;
        }
        final Map map = (Map)Maps.w(this.backingMap, o);
        if (map == null) {
            return null;
        }
        remove = map.remove(remove);
        if (map.isEmpty()) {
            this.backingMap.remove(o);
        }
        return (V)remove;
    }
    
    @Override
    public Map<C, V> row(final R r) {
        return new g(r);
    }
    
    @Override
    public Set<R> rowKeySet() {
        return this.rowMap().keySet();
    }
    
    @Override
    public Map<R, Map<C, V>> rowMap() {
        Map<R, Map<C, V>> rowMap;
        if ((rowMap = this.rowMap) == null) {
            rowMap = this.createRowMap();
            this.rowMap = rowMap;
        }
        return rowMap;
    }
    
    @Override
    public int size() {
        final Iterator<Map<C, V>> iterator = this.backingMap.values().iterator();
        int n = 0;
        while (iterator.hasNext()) {
            n += iterator.next().size();
        }
        return n;
    }
    
    @Override
    public Collection<V> values() {
        return super.values();
    }
    
    public class b implements Iterator
    {
        public final Iterator a;
        public Map.Entry b;
        public Iterator c;
        public final StandardTable d;
        
        public b(final StandardTable d) {
            this.d = d;
            this.a = d.backingMap.entrySet().iterator();
            this.c = Iterators.j();
        }
        
        public q.a b() {
            if (!this.c.hasNext()) {
                final Map.Entry<K, Map> b = this.a.next();
                this.b = (Map.Entry)b;
                this.c = b.getValue().entrySet().iterator();
            }
            Objects.requireNonNull(this.b);
            final Map.Entry<Object, V> entry = this.c.next();
            return Tables.c(this.b.getKey(), entry.getKey(), entry.getValue());
        }
        
        @Override
        public boolean hasNext() {
            return this.a.hasNext() || this.c.hasNext();
        }
        
        @Override
        public void remove() {
            this.c.remove();
            final Map.Entry b = this.b;
            Objects.requireNonNull(b);
            if (((Map)((Map.Entry)b).getValue()).isEmpty()) {
                this.a.remove();
                this.b = null;
            }
        }
    }
    
    public class c extends u
    {
        public final Object d;
        public final StandardTable e;
        
        public c(final StandardTable e, final Object o) {
            this.e = e;
            this.d = i71.r(o);
        }
        
        public Set a() {
            return new a(null);
        }
        
        @Override
        public Set b() {
            return new StandardTable.c.c();
        }
        
        @Override
        public Collection c() {
            return new d();
        }
        
        @Override
        public boolean containsKey(final Object o) {
            return this.e.contains(o, this.d);
        }
        
        public boolean d(final m71 m71) {
            final Iterator<Map.Entry<R, Map<C, V>>> iterator = this.e.backingMap.entrySet().iterator();
            boolean b = false;
            while (iterator.hasNext()) {
                final Map.Entry<K, Map> entry = (Map.Entry<K, Map>)iterator.next();
                final Map map = entry.getValue();
                final Object value = map.get(this.d);
                if (value != null && m71.apply(Maps.j(entry.getKey(), value))) {
                    map.remove(this.d);
                    if (map.isEmpty()) {
                        iterator.remove();
                    }
                    b = true;
                }
            }
            return b;
        }
        
        @Override
        public Object get(final Object o) {
            return this.e.get(o, this.d);
        }
        
        @Override
        public Object put(final Object o, final Object o2) {
            return this.e.put(o, this.d, o2);
        }
        
        @Override
        public Object remove(final Object o) {
            return this.e.remove(o, this.d);
        }
        
        public class a extends Sets.a
        {
            public final StandardTable.c a;
            
            public a(final StandardTable.c a) {
                this.a = a;
            }
            
            @Override
            public void clear() {
                this.a.d(Predicates.b());
            }
            
            @Override
            public boolean contains(final Object o) {
                if (o instanceof Entry) {
                    final Entry entry = (Entry)o;
                    return this.a.e.containsMapping(entry.getKey(), this.a.d, entry.getValue());
                }
                return false;
            }
            
            @Override
            public boolean isEmpty() {
                final StandardTable.c a = this.a;
                return a.e.containsColumn(a.d) ^ true;
            }
            
            @Override
            public Iterator iterator() {
                return this.a.new b(null);
            }
            
            @Override
            public boolean remove(final Object o) {
                if (o instanceof Entry) {
                    final Entry entry = (Entry)o;
                    return this.a.e.removeMapping(entry.getKey(), this.a.d, entry.getValue());
                }
                return false;
            }
            
            @Override
            public boolean retainAll(final Collection collection) {
                return this.a.d(Predicates.g(Predicates.e(collection)));
            }
            
            @Override
            public int size() {
                final Iterator<Map<C, V>> iterator = this.a.e.backingMap.values().iterator();
                int n = 0;
                while (iterator.hasNext()) {
                    if (iterator.next().containsKey(this.a.d)) {
                        ++n;
                    }
                }
                return n;
            }
        }
        
        public class b extends AbstractIterator
        {
            public final Iterator c;
            public final StandardTable.c d;
            
            public b(final StandardTable.c d) {
                this.d = d;
                this.c = d.e.backingMap.entrySet().iterator();
            }
            
            public Entry e() {
                while (this.c.hasNext()) {
                    final Map.Entry<K, Map> entry = this.c.next();
                    if (entry.getValue().containsKey(this.d.d)) {
                        return new a((Entry)entry);
                    }
                }
                return (Entry)this.c();
            }
            
            public class a extends s
            {
                public final Entry a;
                public final b b;
                
                public a(final b b, final Entry a) {
                    this.b = b;
                    this.a = a;
                }
                
                @Override
                public Object getKey() {
                    return this.a.getKey();
                }
                
                @Override
                public Object getValue() {
                    return this.a.getValue().get(this.b.d.d);
                }
                
                @Override
                public Object setValue(final Object o) {
                    return k01.a(this.a.getValue().put(this.b.d.d, i71.r(o)));
                }
            }
        }
        
        public class c extends m
        {
            public final StandardTable.c b;
            
            public c(final StandardTable.c b) {
                super(this.b = b);
            }
            
            @Override
            public boolean contains(final Object o) {
                final StandardTable.c b = this.b;
                return b.e.contains(o, b.d);
            }
            
            @Override
            public boolean remove(final Object o) {
                final StandardTable.c b = this.b;
                return b.e.remove(o, b.d) != null;
            }
            
            @Override
            public boolean retainAll(final Collection collection) {
                return this.b.d(Maps.o(Predicates.g(Predicates.e(collection))));
            }
        }
        
        public class d extends t
        {
            public final StandardTable.c b;
            
            public d(final StandardTable.c b) {
                super(this.b = b);
            }
            
            @Override
            public boolean remove(final Object o) {
                return o != null && this.b.d(Maps.M(Predicates.d(o)));
            }
            
            @Override
            public boolean removeAll(final Collection collection) {
                return this.b.d(Maps.M(Predicates.e(collection)));
            }
            
            @Override
            public boolean retainAll(final Collection collection) {
                return this.b.d(Maps.M(Predicates.g(Predicates.e(collection))));
            }
        }
    }
    
    public class d extends AbstractIterator
    {
        public final Map c;
        public final Iterator d;
        public Iterator e;
        public final StandardTable f;
        
        public d(final StandardTable f) {
            this.f = f;
            this.c = (Map)f.factory.get();
            this.d = f.backingMap.values().iterator();
            this.e = Iterators.h();
        }
        
        @Override
        public Object b() {
            while (true) {
                if (this.e.hasNext()) {
                    final Map.Entry<Object, V> entry = this.e.next();
                    if (!this.c.containsKey(entry.getKey())) {
                        this.c.put(entry.getKey(), entry.getValue());
                        return entry.getKey();
                    }
                    continue;
                }
                else {
                    if (!this.d.hasNext()) {
                        return this.c();
                    }
                    this.e = this.d.next().entrySet().iterator();
                }
            }
        }
    }
    
    public class e extends i
    {
        public final StandardTable b;
        
        public e(final StandardTable b) {
            this.b = b.super(null);
        }
        
        @Override
        public boolean contains(final Object o) {
            return this.b.containsColumn(o);
        }
        
        @Override
        public Iterator iterator() {
            return this.b.createColumnKeyIterator();
        }
        
        @Override
        public boolean remove(final Object o) {
            boolean b = false;
            if (o == null) {
                return false;
            }
            final Iterator<Map<C, V>> iterator = this.b.backingMap.values().iterator();
            while (iterator.hasNext()) {
                final Map map = iterator.next();
                if (map.keySet().remove(o)) {
                    if (map.isEmpty()) {
                        iterator.remove();
                    }
                    b = true;
                }
            }
            return b;
        }
        
        @Override
        public boolean removeAll(final Collection collection) {
            i71.r(collection);
            final Iterator<Map<C, V>> iterator = this.b.backingMap.values().iterator();
            boolean b = false;
            while (iterator.hasNext()) {
                final Map map = iterator.next();
                if (Iterators.r(map.keySet().iterator(), collection)) {
                    if (map.isEmpty()) {
                        iterator.remove();
                    }
                    b = true;
                }
            }
            return b;
        }
        
        @Override
        public boolean retainAll(final Collection collection) {
            i71.r(collection);
            final Iterator<Map<C, V>> iterator = this.b.backingMap.values().iterator();
            boolean b = false;
            while (iterator.hasNext()) {
                final Map map = iterator.next();
                if (map.keySet().retainAll(collection)) {
                    if (map.isEmpty()) {
                        iterator.remove();
                    }
                    b = true;
                }
            }
            return b;
        }
        
        @Override
        public int size() {
            return Iterators.u(this.iterator());
        }
    }
    
    public abstract class i extends Sets.a
    {
        public final StandardTable a;
        
        public i(final StandardTable a) {
            this.a = a;
        }
        
        @Override
        public void clear() {
            this.a.backingMap.clear();
        }
        
        @Override
        public boolean isEmpty() {
            return this.a.backingMap.isEmpty();
        }
    }
    
    public class f extends u
    {
        public final StandardTable d;
        
        public f(final StandardTable d) {
            this.d = d;
        }
        
        public Set a() {
            return new a();
        }
        
        @Override
        public Collection c() {
            return new b();
        }
        
        @Override
        public boolean containsKey(final Object o) {
            return this.d.containsColumn(o);
        }
        
        public Map d(final Object obj) {
            Map<R, V> column;
            if (this.d.containsColumn(obj)) {
                final StandardTable d = this.d;
                Objects.requireNonNull(obj);
                column = d.column(obj);
            }
            else {
                column = null;
            }
            return column;
        }
        
        public Map e(final Object o) {
            Map access$900;
            if (this.d.containsColumn(o)) {
                access$900 = this.d.removeColumn(o);
            }
            else {
                access$900 = null;
            }
            return access$900;
        }
        
        @Override
        public Set keySet() {
            return this.d.columnKeySet();
        }
        
        public class a extends StandardTable.i
        {
            public final f b;
            
            public a(final f b) {
                this.b = b;
                b.d.super(null);
            }
            
            @Override
            public boolean contains(final Object o) {
                if (o instanceof Entry) {
                    final Entry entry = (Entry)o;
                    if (this.b.d.containsColumn(entry.getKey())) {
                        final Map d = this.b.d(entry.getKey());
                        Objects.requireNonNull(d);
                        return d.equals(entry.getValue());
                    }
                }
                return false;
            }
            
            @Override
            public Iterator iterator() {
                return Maps.d(this.b.d.columnKeySet(), new m90(this) {
                    public final a a;
                    
                    public Map a(final Object o) {
                        return this.a.b.d.column(o);
                    }
                });
            }
            
            @Override
            public boolean remove(final Object o) {
                if (this.contains(o) && o instanceof Entry) {
                    this.b.d.removeColumn(((Entry)o).getKey());
                    return true;
                }
                return false;
            }
            
            @Override
            public boolean removeAll(final Collection collection) {
                i71.r(collection);
                return Sets.g(this, collection.iterator());
            }
            
            @Override
            public boolean retainAll(final Collection collection) {
                i71.r(collection);
                final Iterator iterator = Lists.k(this.b.d.columnKeySet().iterator()).iterator();
                boolean b = false;
                while (iterator.hasNext()) {
                    final Object next = iterator.next();
                    if (!collection.contains(Maps.j(next, this.b.d.column(next)))) {
                        this.b.d.removeColumn(next);
                        b = true;
                    }
                }
                return b;
            }
            
            @Override
            public int size() {
                return this.b.d.columnKeySet().size();
            }
        }
        
        public class b extends t
        {
            public final f b;
            
            public b(final f b) {
                super(this.b = b);
            }
            
            @Override
            public boolean remove(final Object o) {
                for (final Map.Entry<K, Map> entry : ((Maps.u)this.b).entrySet()) {
                    if (entry.getValue().equals(o)) {
                        this.b.d.removeColumn(entry.getKey());
                        return true;
                    }
                }
                return false;
            }
            
            @Override
            public boolean removeAll(final Collection collection) {
                i71.r(collection);
                final Iterator iterator = Lists.k(this.b.d.columnKeySet().iterator()).iterator();
                boolean b = false;
                while (iterator.hasNext()) {
                    final Object next = iterator.next();
                    if (collection.contains(this.b.d.column(next))) {
                        this.b.d.removeColumn(next);
                        b = true;
                    }
                }
                return b;
            }
            
            @Override
            public boolean retainAll(final Collection collection) {
                i71.r(collection);
                final Iterator iterator = Lists.k(this.b.d.columnKeySet().iterator()).iterator();
                boolean b = false;
                while (iterator.hasNext()) {
                    final Object next = iterator.next();
                    if (!collection.contains(this.b.d.column(next))) {
                        this.b.d.removeColumn(next);
                        b = true;
                    }
                }
                return b;
            }
        }
    }
    
    public class g extends l
    {
        public final Object a;
        public Map b;
        public final StandardTable c;
        
        public g(final StandardTable c, final Object o) {
            this.c = c;
            this.a = i71.r(o);
        }
        
        @Override
        Iterator a() {
            this.d();
            final Map b = this.b;
            if (b == null) {
                return Iterators.j();
            }
            return new Iterator(this, b.entrySet().iterator()) {
                public final Iterator a;
                public final g b;
                
                public Entry b() {
                    return this.b.e((Entry)this.a.next());
                }
                
                @Override
                public boolean hasNext() {
                    return this.a.hasNext();
                }
                
                @Override
                public void remove() {
                    this.a.remove();
                    this.b.c();
                }
            };
        }
        
        public Map b() {
            return this.c.backingMap.get(this.a);
        }
        
        public void c() {
            this.d();
            final Map b = this.b;
            if (b != null && b.isEmpty()) {
                this.c.backingMap.remove(this.a);
                this.b = null;
            }
        }
        
        @Override
        public void clear() {
            this.d();
            final Map b = this.b;
            if (b != null) {
                b.clear();
            }
            this.c();
        }
        
        @Override
        public boolean containsKey(final Object o) {
            this.d();
            if (o != null) {
                final Map b = this.b;
                if (b != null && Maps.v(b, o)) {
                    return true;
                }
            }
            return false;
        }
        
        public final void d() {
            final Map b = this.b;
            if (b == null || (b.isEmpty() && this.c.backingMap.containsKey(this.a))) {
                this.b = this.b();
            }
        }
        
        public Entry e(final Entry entry) {
            return new y70(this, entry) {
                public final Entry a;
                
                @Override
                public Entry b() {
                    return this.a;
                }
                
                @Override
                public boolean equals(final Object o) {
                    return this.standardEquals(o);
                }
                
                @Override
                public Object setValue(final Object o) {
                    return super.setValue(i71.r(o));
                }
            };
        }
        
        @Override
        public Object get(Object w) {
            this.d();
            if (w != null) {
                final Map b = this.b;
                if (b != null) {
                    w = Maps.w(b, w);
                    return w;
                }
            }
            w = null;
            return w;
        }
        
        @Override
        public Object put(final Object o, final Object o2) {
            i71.r(o);
            i71.r(o2);
            final Map b = this.b;
            if (b != null && !b.isEmpty()) {
                return this.b.put(o, o2);
            }
            return this.c.put(this.a, o, o2);
        }
        
        @Override
        public Object remove(Object x) {
            this.d();
            final Map b = this.b;
            if (b == null) {
                return null;
            }
            x = Maps.x(b, x);
            this.c();
            return x;
        }
        
        @Override
        public int size() {
            this.d();
            final Map b = this.b;
            int size;
            if (b == null) {
                size = 0;
            }
            else {
                size = b.size();
            }
            return size;
        }
    }
    
    public class h extends u
    {
        public final StandardTable d;
        
        public h(final StandardTable d) {
            this.d = d;
        }
        
        public Set a() {
            return new a();
        }
        
        @Override
        public boolean containsKey(final Object o) {
            return this.d.containsRow(o);
        }
        
        public Map d(final Object obj) {
            Map<C, V> row;
            if (this.d.containsRow(obj)) {
                final StandardTable d = this.d;
                Objects.requireNonNull(obj);
                row = d.row(obj);
            }
            else {
                row = null;
            }
            return row;
        }
        
        public Map e(final Object o) {
            Map map;
            if (o == null) {
                map = null;
            }
            else {
                map = this.d.backingMap.remove(o);
            }
            return map;
        }
        
        public class a extends StandardTable.i
        {
            public final h b;
            
            public a(final h b) {
                this.b = b;
                b.d.super(null);
            }
            
            @Override
            public boolean contains(final Object o) {
                final boolean b = o instanceof Entry;
                boolean b3;
                final boolean b2 = b3 = false;
                if (b) {
                    final Entry entry = (Entry)o;
                    b3 = b2;
                    if (entry.getKey() != null) {
                        b3 = b2;
                        if (entry.getValue() instanceof Map) {
                            b3 = b2;
                            if (lh.c(this.b.d.backingMap.entrySet(), entry)) {
                                b3 = true;
                            }
                        }
                    }
                }
                return b3;
            }
            
            @Override
            public Iterator iterator() {
                return Maps.d(this.b.d.backingMap.keySet(), new m90(this) {
                    public final a a;
                    
                    public Map a(final Object o) {
                        return this.a.b.d.row(o);
                    }
                });
            }
            
            @Override
            public boolean remove(final Object o) {
                final boolean b = o instanceof Entry;
                boolean b3;
                final boolean b2 = b3 = false;
                if (b) {
                    final Entry entry = (Entry)o;
                    b3 = b2;
                    if (entry.getKey() != null) {
                        b3 = b2;
                        if (entry.getValue() instanceof Map) {
                            b3 = b2;
                            if (this.b.d.backingMap.entrySet().remove(entry)) {
                                b3 = true;
                            }
                        }
                    }
                }
                return b3;
            }
            
            @Override
            public int size() {
                return this.b.d.backingMap.size();
            }
        }
    }
}

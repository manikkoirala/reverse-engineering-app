// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Collections;
import java.util.List;
import java.util.Comparator;

public abstract class Ordering implements Comparator
{
    static final int LEFT_IS_GREATER = 1;
    static final int RIGHT_IS_GREATER = -1;
    
    public static Ordering allEqual() {
        return AllEqualOrdering.INSTANCE;
    }
    
    public static Ordering arbitrary() {
        return b.a;
    }
    
    public static <T> Ordering compound(final Iterable<? extends Comparator<? super T>> iterable) {
        return new CompoundOrdering<Object>(iterable);
    }
    
    public static <T> Ordering explicit(final T t, final T... array) {
        return explicit((List<Object>)Lists.a(t, array));
    }
    
    public static <T> Ordering explicit(final List<T> list) {
        return new ExplicitOrdering<Object>(list);
    }
    
    @Deprecated
    public static <T> Ordering from(final Ordering ordering) {
        return (Ordering)i71.r(ordering);
    }
    
    public static <T> Ordering from(final Comparator<T> comparator) {
        Ordering ordering;
        if (comparator instanceof Ordering) {
            ordering = (Ordering)comparator;
        }
        else {
            ordering = new ComparatorOrdering<Object>(comparator);
        }
        return ordering;
    }
    
    public static <C extends Comparable> Ordering natural() {
        return NaturalOrdering.INSTANCE;
    }
    
    public static Ordering usingToString() {
        return UsingToStringOrdering.INSTANCE;
    }
    
    @Deprecated
    public int binarySearch(final List<Object> list, final Object key) {
        return Collections.binarySearch(list, key, this);
    }
    
    @Override
    public abstract int compare(final Object p0, final Object p1);
    
    public <U> Ordering compound(final Comparator<? super U> comparator) {
        return new CompoundOrdering<Object>(this, (Comparator<?>)i71.r(comparator));
    }
    
    public <E> List<E> greatestOf(final Iterable<E> iterable, final int n) {
        return (List<E>)this.reverse().leastOf((Iterable<Object>)iterable, n);
    }
    
    public <E> List<E> greatestOf(final Iterator<E> iterator, final int n) {
        return (List<E>)this.reverse().leastOf((Iterator<Object>)iterator, n);
    }
    
    public <E> ImmutableList<E> immutableSortedCopy(final Iterable<E> iterable) {
        return ImmutableList.sortedCopyOf((Comparator<? super E>)this, (Iterable<? extends E>)iterable);
    }
    
    public boolean isOrdered(final Iterable<Object> iterable) {
        final Iterator<Object> iterator = iterable.iterator();
        if (iterator.hasNext()) {
            Object next = iterator.next();
            while (iterator.hasNext()) {
                final Object next2 = iterator.next();
                if (this.compare(next, next2) > 0) {
                    return false;
                }
                next = next2;
            }
        }
        return true;
    }
    
    public boolean isStrictlyOrdered(final Iterable<Object> iterable) {
        final Iterator<Object> iterator = iterable.iterator();
        if (iterator.hasNext()) {
            Object next = iterator.next();
            while (iterator.hasNext()) {
                final Object next2 = iterator.next();
                if (this.compare(next, next2) >= 0) {
                    return false;
                }
                next = next2;
            }
        }
        return true;
    }
    
    public <E> List<E> leastOf(final Iterable<E> iterable, final int newLength) {
        if (iterable instanceof Collection) {
            final Collection collection = (Collection)iterable;
            if (collection.size() <= newLength * 2L) {
                final Object[] array = collection.toArray();
                Arrays.sort(array, this);
                Object[] copy = array;
                if (array.length > newLength) {
                    copy = Arrays.copyOf(array, newLength);
                }
                return (List<E>)Collections.unmodifiableList((List<?>)Arrays.asList((T[])copy));
            }
        }
        return this.leastOf((Iterator<E>)iterable.iterator(), newLength);
    }
    
    public <E> List<E> leastOf(final Iterator<E> iterator, final int fromIndex) {
        i71.r(iterator);
        hh.b(fromIndex, "k");
        if (fromIndex == 0 || !iterator.hasNext()) {
            return Collections.emptyList();
        }
        if (fromIndex >= 1073741823) {
            final ArrayList k = Lists.k(iterator);
            Collections.sort((List<Object>)k, this);
            if (k.size() > fromIndex) {
                k.subList(fromIndex, k.size()).clear();
            }
            k.trimToSize();
            return (List<E>)Collections.unmodifiableList((List<?>)k);
        }
        final hx1 a = hx1.a(fromIndex, this);
        a.c(iterator);
        return a.f();
    }
    
    public <S> Ordering lexicographical() {
        return new LexicographicalOrdering<Object>(this);
    }
    
    public <E> E max(final Iterable<E> iterable) {
        return this.max(iterable.iterator());
    }
    
    public <E> E max(E e, final E e2) {
        if (this.compare(e, e2) < 0) {
            e = e2;
        }
        return e;
    }
    
    public <E> E max(final E e, final E e2, final E e3, final E... array) {
        E e4 = this.max(this.max(e, e2), e3);
        for (int length = array.length, i = 0; i < length; ++i) {
            e4 = this.max(e4, array[i]);
        }
        return e4;
    }
    
    public <E> E max(final Iterator<E> iterator) {
        E e = iterator.next();
        while (iterator.hasNext()) {
            e = this.max(e, iterator.next());
        }
        return e;
    }
    
    public <E> E min(final Iterable<E> iterable) {
        return this.min(iterable.iterator());
    }
    
    public <E> E min(E e, final E e2) {
        if (this.compare(e, e2) > 0) {
            e = e2;
        }
        return e;
    }
    
    public <E> E min(final E e, final E e2, final E e3, final E... array) {
        E e4 = this.min(this.min(e, e2), e3);
        for (int length = array.length, i = 0; i < length; ++i) {
            e4 = this.min(e4, array[i]);
        }
        return e4;
    }
    
    public <E> E min(final Iterator<E> iterator) {
        E e = iterator.next();
        while (iterator.hasNext()) {
            e = this.min(e, iterator.next());
        }
        return e;
    }
    
    public <S> Ordering nullsFirst() {
        return new NullsFirstOrdering<Object>(this);
    }
    
    public <S> Ordering nullsLast() {
        return new NullsLastOrdering<Object>(this);
    }
    
    public <T2> Ordering onKeys() {
        return this.onResultOf(Maps.l());
    }
    
    public <F> Ordering onResultOf(final m90 m90) {
        return new ByFunctionOrdering<Object, Object>(m90, this);
    }
    
    public <S> Ordering reverse() {
        return new ReverseOrdering<Object>(this);
    }
    
    public <E> List<E> sortedCopy(final Iterable<E> iterable) {
        final Object[] k = rg0.k(iterable);
        Arrays.sort(k, this);
        return Lists.j(Arrays.asList(k));
    }
    
    public static class IncomparableValueException extends ClassCastException
    {
        private static final long serialVersionUID = 0L;
        final Object value;
        
        public IncomparableValueException(final Object o) {
            final String value = String.valueOf(o);
            final StringBuilder sb = new StringBuilder(value.length() + 22);
            sb.append("Cannot compare value: ");
            sb.append(value);
            super(sb.toString());
            this.value = o;
        }
    }
    
    public static class a extends Ordering
    {
        public final AtomicInteger a;
        public final ConcurrentMap b;
        
        public a() {
            this.a = new AtomicInteger(0);
            this.b = l.i(new MapMaker()).i();
        }
        
        public final Integer a(final Object o) {
            Integer value;
            if ((value = (Integer)this.b.get(o)) == null) {
                value = this.a.getAndIncrement();
                final Integer n = this.b.putIfAbsent(o, value);
                if (n != null) {
                    value = n;
                }
            }
            return value;
        }
        
        public int b(final Object o) {
            return System.identityHashCode(o);
        }
        
        @Override
        public int compare(final Object o, final Object o2) {
            if (o == o2) {
                return 0;
            }
            int n = -1;
            if (o == null) {
                return -1;
            }
            if (o2 == null) {
                return 1;
            }
            final int b = this.b(o);
            final int b2 = this.b(o2);
            if (b != b2) {
                if (b >= b2) {
                    n = 1;
                }
                return n;
            }
            final int compareTo = this.a(o).compareTo(this.a(o2));
            if (compareTo != 0) {
                return compareTo;
            }
            throw new AssertionError();
        }
        
        @Override
        public String toString() {
            return "Ordering.arbitrary()";
        }
    }
    
    public abstract static class b
    {
        public static final Ordering a;
        
        static {
            a = new a();
        }
    }
}

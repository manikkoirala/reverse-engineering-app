// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.AbstractCollection;
import java.util.AbstractSet;
import java.util.NoSuchElementException;
import java.lang.ref.WeakReference;
import java.lang.ref.Reference;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import com.google.common.primitives.Ints;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.Iterator;
import java.util.ArrayList;
import java.lang.ref.ReferenceQueue;
import java.util.Collection;
import com.google.common.base.Equivalence;
import java.util.Map;
import java.util.Set;
import java.io.Serializable;
import java.util.concurrent.ConcurrentMap;
import java.util.AbstractMap;

class MapMakerInternalMap<K, V, E extends h, S extends Segment<K, V, E, S>> extends AbstractMap<K, V> implements ConcurrentMap<K, V>, Serializable
{
    static final long CLEANUP_EXECUTOR_DELAY_SECS = 60L;
    static final int CONTAINS_VALUE_RETRIES = 3;
    static final int DRAIN_MAX = 16;
    static final int DRAIN_THRESHOLD = 63;
    static final int MAXIMUM_CAPACITY = 1073741824;
    static final int MAX_SEGMENTS = 65536;
    static final v UNSET_WEAK_VALUE_REFERENCE;
    private static final long serialVersionUID = 5L;
    final int concurrencyLevel;
    final transient i entryHelper;
    transient Set<Entry<K, V>> entrySet;
    final Equivalence keyEquivalence;
    transient Set<K> keySet;
    final transient int segmentMask;
    final transient int segmentShift;
    final transient Segment<K, V, E, S>[] segments;
    transient Collection<V> values;
    
    static {
        UNSET_WEAK_VALUE_REFERENCE = (v)new v() {
            public v c(final ReferenceQueue referenceQueue, final d d) {
                return this;
            }
            
            @Override
            public void clear() {
            }
            
            public d d() {
                return null;
            }
            
            @Override
            public Object get() {
                return null;
            }
        };
    }
    
    private MapMakerInternalMap(final MapMaker mapMaker, final i entryHelper) {
        this.concurrencyLevel = Math.min(mapMaker.b(), 65536);
        this.keyEquivalence = mapMaker.d();
        this.entryHelper = entryHelper;
        final int min = Math.min(mapMaker.c(), 1073741824);
        final int n = 0;
        final int n2 = 1;
        int n3 = 0;
        int i;
        for (i = 1; i < this.concurrencyLevel; i <<= 1) {
            ++n3;
        }
        this.segmentShift = 32 - n3;
        this.segmentMask = i - 1;
        this.segments = this.newSegmentArray(i);
        final int n4 = min / i;
        int n5 = n2;
        int n6 = n4;
        if (i * n4 < min) {
            n6 = n4 + 1;
            n5 = n2;
        }
        int n7;
        while (true) {
            n7 = n;
            if (n5 >= n6) {
                break;
            }
            n5 <<= 1;
        }
        while (true) {
            final Segment<K, V, E, S>[] segments = this.segments;
            if (n7 >= segments.length) {
                break;
            }
            segments[n7] = this.createSegment(n5, -1);
            ++n7;
        }
    }
    
    public static <K, V> MapMakerInternalMap<K, V, ? extends h, ?> create(final MapMaker mapMaker) {
        final Strength e = mapMaker.e();
        final Strength strong = Strength.STRONG;
        if (e == strong && mapMaker.f() == strong) {
            return new MapMakerInternalMap<K, V, h, Object>(mapMaker, (i)n.a.h());
        }
        if (mapMaker.e() == strong && mapMaker.f() == Strength.WEAK) {
            return new MapMakerInternalMap<K, V, h, Object>(mapMaker, (i)o.a.h());
        }
        final Strength e2 = mapMaker.e();
        final Strength weak = Strength.WEAK;
        if (e2 == weak && mapMaker.f() == strong) {
            return new MapMakerInternalMap<K, V, h, Object>(mapMaker, (i)s.a.h());
        }
        if (mapMaker.e() == weak && mapMaker.f() == weak) {
            return new MapMakerInternalMap<K, V, h, Object>(mapMaker, (i)t.a.h());
        }
        throw new AssertionError();
    }
    
    public static <K> MapMakerInternalMap<K, MapMaker.Dummy, ? extends h, ?> createWithDummyValues(final MapMaker mapMaker) {
        final Strength e = mapMaker.e();
        final Strength strong = Strength.STRONG;
        if (e == strong && mapMaker.f() == strong) {
            return new MapMakerInternalMap<K, MapMaker.Dummy, h, Object>(mapMaker, (i)m.a.h());
        }
        final Strength e2 = mapMaker.e();
        final Strength weak = Strength.WEAK;
        if (e2 == weak && mapMaker.f() == strong) {
            return new MapMakerInternalMap<K, MapMaker.Dummy, h, Object>(mapMaker, (i)r.a.h());
        }
        if (mapMaker.f() == weak) {
            throw new IllegalArgumentException("Map cannot have both weak and dummy values");
        }
        throw new AssertionError();
    }
    
    public static int rehash(int n) {
        n += (n << 15 ^ 0xFFFFCD7D);
        n ^= n >>> 10;
        n += n << 3;
        n ^= n >>> 6;
        n += (n << 2) + (n << 14);
        return n ^ n >>> 16;
    }
    
    private static <E> ArrayList<E> toArrayList(final Collection<E> collection) {
        final ArrayList list = new ArrayList(collection.size());
        Iterators.a(list, collection.iterator());
        return list;
    }
    
    public static <K, V, E extends h> v unsetWeakValueReference() {
        return MapMakerInternalMap.UNSET_WEAK_VALUE_REFERENCE;
    }
    
    @Override
    public void clear() {
        final Segment<K, V, E, S>[] segments = this.segments;
        for (int length = segments.length, i = 0; i < length; ++i) {
            segments[i].clear();
        }
    }
    
    @Override
    public boolean containsKey(final Object o) {
        if (o == null) {
            return false;
        }
        final int hash = this.hash(o);
        return this.segmentFor(hash).containsKey(o, hash);
    }
    
    @Override
    public boolean containsValue(final Object o) {
        if (o == null) {
            return false;
        }
        final Segment<K, V, E, S>[] segments = this.segments;
        long n = -1L;
        long n2;
        for (int i = 0; i < 3; ++i, n = n2) {
            final int length = segments.length;
            n2 = 0L;
            for (final Segment<K, V, E, S> segment : segments) {
                final int count = segment.count;
                final AtomicReferenceArray<E> table = segment.table;
                for (int k = 0; k < table.length(); ++k) {
                    for (h next = table.get(k); next != null; next = next.getNext()) {
                        final V liveValue = segment.getLiveValue((E)next);
                        if (liveValue != null && this.valueEquivalence().equivalent(o, liveValue)) {
                            return true;
                        }
                    }
                }
                n2 += segment.modCount;
            }
            if (n2 == n) {
                break;
            }
        }
        return false;
    }
    
    public E copyEntry(final E e, final E e2) {
        return this.segmentFor(e.getHash()).copyEntry(e, e2);
    }
    
    public Segment<K, V, E, S> createSegment(final int n, final int n2) {
        return (Segment<K, V, E, S>)this.entryHelper.f(this, n, n2);
    }
    
    @Override
    public Set<Entry<K, V>> entrySet() {
        Set<Entry<K, V>> entrySet = this.entrySet;
        if (entrySet == null) {
            entrySet = new f();
            this.entrySet = entrySet;
        }
        return entrySet;
    }
    
    @Override
    public V get(final Object o) {
        if (o == null) {
            return null;
        }
        final int hash = this.hash(o);
        return this.segmentFor(hash).get(o, hash);
    }
    
    public E getEntry(final Object o) {
        if (o == null) {
            return null;
        }
        final int hash = this.hash(o);
        return this.segmentFor(hash).getEntry(o, hash);
    }
    
    public V getLiveValue(final E e) {
        if (e.getKey() == null) {
            return null;
        }
        return (V)e.getValue();
    }
    
    public int hash(final Object o) {
        return rehash(this.keyEquivalence.hash(o));
    }
    
    @Override
    public boolean isEmpty() {
        final Segment<K, V, E, S>[] segments = this.segments;
        boolean b = false;
        long n = 0L;
        for (int i = 0; i < segments.length; ++i) {
            if (segments[i].count != 0) {
                return false;
            }
            n += segments[i].modCount;
        }
        if (n != 0L) {
            for (int j = 0; j < segments.length; ++j) {
                if (segments[j].count != 0) {
                    return false;
                }
                n -= segments[j].modCount;
            }
            if (n == 0L) {
                b = true;
            }
            return b;
        }
        return true;
    }
    
    public boolean isLiveForTesting(final h h) {
        return this.segmentFor(h.getHash()).getLiveValueForTesting(h) != null;
    }
    
    @Override
    public Set<K> keySet() {
        Set<K> keySet = this.keySet;
        if (keySet == null) {
            keySet = new k();
            this.keySet = keySet;
        }
        return keySet;
    }
    
    public Strength keyStrength() {
        return this.entryHelper.b();
    }
    
    public final Segment<K, V, E, S>[] newSegmentArray(final int n) {
        return new Segment[n];
    }
    
    @Override
    public V put(final K k, final V v) {
        i71.r(k);
        i71.r(v);
        final int hash = this.hash(k);
        return this.segmentFor(hash).put(k, hash, v, false);
    }
    
    @Override
    public void putAll(final Map<? extends K, ? extends V> map) {
        for (final Map.Entry<K, V> entry : map.entrySet()) {
            this.put(entry.getKey(), entry.getValue());
        }
    }
    
    @Override
    public V putIfAbsent(final K k, final V v) {
        i71.r(k);
        i71.r(v);
        final int hash = this.hash(k);
        return this.segmentFor(hash).put(k, hash, v, true);
    }
    
    public void reclaimKey(final E e) {
        final int hash = e.getHash();
        this.segmentFor(hash).reclaimKey(e, hash);
    }
    
    public void reclaimValue(final v v) {
        final h b = v.b();
        final int hash = b.getHash();
        this.segmentFor(hash).reclaimValue((K)b.getKey(), hash, v);
    }
    
    @Override
    public V remove(final Object o) {
        if (o == null) {
            return null;
        }
        final int hash = this.hash(o);
        return this.segmentFor(hash).remove(o, hash);
    }
    
    @Override
    public boolean remove(final Object o, final Object o2) {
        if (o != null && o2 != null) {
            final int hash = this.hash(o);
            return this.segmentFor(hash).remove(o, hash, o2);
        }
        return false;
    }
    
    @Override
    public V replace(final K k, final V v) {
        i71.r(k);
        i71.r(v);
        final int hash = this.hash(k);
        return this.segmentFor(hash).replace(k, hash, v);
    }
    
    @Override
    public boolean replace(final K k, final V v, final V v2) {
        i71.r(k);
        i71.r(v2);
        if (v == null) {
            return false;
        }
        final int hash = this.hash(k);
        return this.segmentFor(hash).replace(k, hash, v, v2);
    }
    
    public Segment<K, V, E, S> segmentFor(final int n) {
        return this.segments[n >>> this.segmentShift & this.segmentMask];
    }
    
    @Override
    public int size() {
        final Segment<K, V, E, S>[] segments = this.segments;
        long n = 0L;
        for (int i = 0; i < segments.length; ++i) {
            n += segments[i].count;
        }
        return Ints.k(n);
    }
    
    public Equivalence valueEquivalence() {
        return this.entryHelper.d().defaultEquivalence();
    }
    
    public Strength valueStrength() {
        return this.entryHelper.d();
    }
    
    @Override
    public Collection<V> values() {
        Collection<V> values = this.values;
        if (values == null) {
            values = new q();
            this.values = values;
        }
        return values;
    }
    
    public Object writeReplace() {
        return new SerializationProxy(this.entryHelper.b(), this.entryHelper.d(), this.keyEquivalence, this.entryHelper.d().defaultEquivalence(), this.concurrencyLevel, (ConcurrentMap<Object, Object>)this);
    }
    
    public abstract static class AbstractSerializationProxy<K, V> extends u70 implements Serializable
    {
        private static final long serialVersionUID = 3L;
        final int concurrencyLevel;
        transient ConcurrentMap<K, V> delegate;
        final Equivalence keyEquivalence;
        final Strength keyStrength;
        final Equivalence valueEquivalence;
        final Strength valueStrength;
        
        public AbstractSerializationProxy(final Strength keyStrength, final Strength valueStrength, final Equivalence keyEquivalence, final Equivalence valueEquivalence, final int concurrencyLevel, final ConcurrentMap<K, V> delegate) {
            this.keyStrength = keyStrength;
            this.valueStrength = valueStrength;
            this.keyEquivalence = keyEquivalence;
            this.valueEquivalence = valueEquivalence;
            this.concurrencyLevel = concurrencyLevel;
            this.delegate = delegate;
        }
        
        @Override
        public ConcurrentMap<K, V> delegate() {
            return this.delegate;
        }
        
        public void readEntries(final ObjectInputStream objectInputStream) {
            while (true) {
                final Object object = objectInputStream.readObject();
                if (object == null) {
                    break;
                }
                this.delegate.put((K)object, (V)objectInputStream.readObject());
            }
        }
        
        public MapMaker readMapMaker(final ObjectInputStream objectInputStream) {
            return new MapMaker().g(objectInputStream.readInt()).j(this.keyStrength).k(this.valueStrength).h(this.keyEquivalence).a(this.concurrencyLevel);
        }
        
        public void writeMapTo(final ObjectOutputStream objectOutputStream) {
            objectOutputStream.writeInt(this.delegate.size());
            for (final Map.Entry<Object, V> entry : this.delegate.entrySet()) {
                objectOutputStream.writeObject(entry.getKey());
                objectOutputStream.writeObject(entry.getValue());
            }
            objectOutputStream.writeObject(null);
        }
    }
    
    public abstract static class Segment<K, V, E extends h, S extends Segment<K, V, E, S>> extends ReentrantLock
    {
        volatile int count;
        final MapMakerInternalMap<K, V, E, S> map;
        final int maxSegmentSize;
        int modCount;
        final AtomicInteger readCount;
        volatile AtomicReferenceArray<E> table;
        int threshold;
        
        public Segment(final MapMakerInternalMap<K, V, E, S> map, final int n, final int maxSegmentSize) {
            this.readCount = new AtomicInteger();
            this.map = map;
            this.maxSegmentSize = maxSegmentSize;
            this.initTable(this.newEntryArray(n));
        }
        
        public static <K, V, E extends h> boolean isCollected(final E e) {
            return e.getValue() == null;
        }
        
        public abstract E castForTesting(final h p0);
        
        public void clear() {
            if (this.count != 0) {
                this.lock();
                try {
                    final AtomicReferenceArray<E> table = this.table;
                    for (int i = 0; i < table.length(); ++i) {
                        table.set(i, null);
                    }
                    this.maybeClearReferenceQueues();
                    this.readCount.set(0);
                    ++this.modCount;
                    this.count = 0;
                }
                finally {
                    this.unlock();
                }
            }
        }
        
        public <T> void clearReferenceQueue(final ReferenceQueue<T> referenceQueue) {
            while (referenceQueue.poll() != null) {}
        }
        
        public boolean clearValueForTesting(final K k, final int n, final v v) {
            this.lock();
            try {
                final AtomicReferenceArray<E> table = this.table;
                final int n2 = table.length() - 1 & n;
                Object next;
                final h h = (h)(next = table.get(n2));
                while (next != null) {
                    final Object key = ((h)next).getKey();
                    if (((h)next).getHash() == n && key != null && this.map.keyEquivalence.equivalent(k, key)) {
                        if (((u)next).getValueReference() == v) {
                            table.set(n2, this.removeFromChain(h, next));
                            return true;
                        }
                        return false;
                    }
                    else {
                        next = ((h)next).getNext();
                    }
                }
                return false;
            }
            finally {
                this.unlock();
            }
        }
        
        public boolean containsKey(Object value, final int n) {
            try {
                final int count = this.count;
                final boolean b = false;
                if (count != 0) {
                    final h liveEntry = this.getLiveEntry(value, n);
                    boolean b2 = b;
                    if (liveEntry != null) {
                        value = liveEntry.getValue();
                        b2 = b;
                        if (value != null) {
                            b2 = true;
                        }
                    }
                    return b2;
                }
                return false;
            }
            finally {
                this.postReadCleanup();
            }
        }
        
        public boolean containsValue(final Object o) {
            try {
                if (this.count != 0) {
                    final AtomicReferenceArray<E> table = this.table;
                    for (int length = table.length(), i = 0; i < length; ++i) {
                        for (Object next = table.get(i); next != null; next = ((h)next).getNext()) {
                            final V liveValue = this.getLiveValue((E)next);
                            if (liveValue != null) {
                                if (this.map.valueEquivalence().equivalent(o, liveValue)) {
                                    return true;
                                }
                            }
                        }
                    }
                }
                return false;
            }
            finally {
                this.postReadCleanup();
            }
        }
        
        public E copyEntry(final E e, final E e2) {
            return (E)this.map.entryHelper.c((Segment)this.self(), e, e2);
        }
        
        public E copyForTesting(final h h, final h h2) {
            return (E)this.map.entryHelper.c((Segment)this.self(), this.castForTesting(h), this.castForTesting(h2));
        }
        
        public void drainKeyReferenceQueue(final ReferenceQueue<K> referenceQueue) {
            int n = 0;
            do {
                final Reference<? extends K> poll = referenceQueue.poll();
                if (poll == null) {
                    break;
                }
                this.map.reclaimKey((E)poll);
            } while (++n != 16);
        }
        
        public void drainValueReferenceQueue(final ReferenceQueue<V> referenceQueue) {
            int n = 0;
            do {
                final Reference<? extends V> poll = referenceQueue.poll();
                if (poll == null) {
                    break;
                }
                this.map.reclaimValue((v)poll);
            } while (++n != 16);
        }
        
        public void expand() {
            final AtomicReferenceArray<E> table = this.table;
            final int length = table.length();
            if (length >= 1073741824) {
                return;
            }
            int count = this.count;
            final AtomicReferenceArray<E> entryArray = this.newEntryArray(length << 1);
            this.threshold = entryArray.length() * 3 / 4;
            final int n = entryArray.length() - 1;
            int n2;
            for (int i = 0; i < length; ++i, count = n2) {
                Object next = table.get(i);
                n2 = count;
                if (next != null) {
                    h h = ((h)next).getNext();
                    int n3 = ((h)next).getHash() & n;
                    if (h == null) {
                        entryArray.set(n3, (E)next);
                        n2 = count;
                    }
                    else {
                        h newValue = (h)next;
                        while (h != null) {
                            final int n4 = h.getHash() & n;
                            int n5;
                            if (n4 != (n5 = n3)) {
                                newValue = h;
                                n5 = n4;
                            }
                            h = h.getNext();
                            n3 = n5;
                        }
                        entryArray.set(n3, (E)newValue);
                        while (true) {
                            n2 = count;
                            if (next == newValue) {
                                break;
                            }
                            final int n6 = ((h)next).getHash() & n;
                            final h copyEntry = this.copyEntry((h)next, entryArray.get(n6));
                            if (copyEntry != null) {
                                entryArray.set(n6, copyEntry);
                            }
                            else {
                                --count;
                            }
                            next = ((h)next).getNext();
                        }
                    }
                }
            }
            this.table = entryArray;
            this.count = count;
        }
        
        public V get(Object value, final int n) {
            try {
                final h liveEntry = this.getLiveEntry(value, n);
                if (liveEntry == null) {
                    return null;
                }
                value = liveEntry.getValue();
                if (value == null) {
                    this.tryDrainReferenceQueues();
                }
                return (V)value;
            }
            finally {
                this.postReadCleanup();
            }
        }
        
        public E getEntry(final Object o, final int n) {
            if (this.count != 0) {
                for (h h = this.getFirst(n); h != null; h = h.getNext()) {
                    if (h.getHash() == n) {
                        final Object key = h.getKey();
                        if (key == null) {
                            this.tryDrainReferenceQueues();
                        }
                        else if (this.map.keyEquivalence.equivalent(o, key)) {
                            return (E)h;
                        }
                    }
                }
            }
            return null;
        }
        
        public E getFirst(final int n) {
            final AtomicReferenceArray<E> table = this.table;
            return table.get(n & table.length() - 1);
        }
        
        public ReferenceQueue<K> getKeyReferenceQueueForTesting() {
            throw new AssertionError();
        }
        
        public E getLiveEntry(final Object o, final int n) {
            return this.getEntry(o, n);
        }
        
        public V getLiveValue(final E e) {
            if (e.getKey() == null) {
                this.tryDrainReferenceQueues();
                return null;
            }
            final Object value = e.getValue();
            if (value == null) {
                this.tryDrainReferenceQueues();
                return null;
            }
            return (V)value;
        }
        
        public V getLiveValueForTesting(final h h) {
            return this.getLiveValue(this.castForTesting(h));
        }
        
        public ReferenceQueue<V> getValueReferenceQueueForTesting() {
            throw new AssertionError();
        }
        
        public v getWeakValueReferenceForTesting(final h h) {
            throw new AssertionError();
        }
        
        public void initTable(final AtomicReferenceArray<E> table) {
            final int threshold = table.length() * 3 / 4;
            this.threshold = threshold;
            if (threshold == this.maxSegmentSize) {
                this.threshold = threshold + 1;
            }
            this.table = table;
        }
        
        public void maybeClearReferenceQueues() {
        }
        
        public void maybeDrainReferenceQueues() {
        }
        
        public AtomicReferenceArray<E> newEntryArray(final int length) {
            return new AtomicReferenceArray<E>(length);
        }
        
        public E newEntryForTesting(final K k, final int n, final h h) {
            return (E)this.map.entryHelper.e((Segment)this.self(), k, n, this.castForTesting(h));
        }
        
        public v newWeakValueReferenceForTesting(final h h, final V v) {
            throw new AssertionError();
        }
        
        public void postReadCleanup() {
            if ((this.readCount.incrementAndGet() & 0x3F) == 0x0) {
                this.runCleanup();
            }
        }
        
        public void preWriteCleanup() {
            this.runLockedCleanup();
        }
        
        public V put(final K k, final int n, final V v, final boolean b) {
            this.lock();
            try {
                this.preWriteCleanup();
                int count;
                if ((count = this.count + 1) > this.threshold) {
                    this.expand();
                    count = this.count + 1;
                }
                final AtomicReferenceArray<E> table = this.table;
                final int n2 = table.length() - 1 & n;
                Object next;
                final h h = (h)(next = table.get(n2));
                while (next != null) {
                    final Object key = ((h)next).getKey();
                    if (((h)next).getHash() == n && key != null && this.map.keyEquivalence.equivalent(k, key)) {
                        final Object value = ((h)next).getValue();
                        if (value == null) {
                            ++this.modCount;
                            this.setValue((E)next, v);
                            this.count = this.count;
                            return null;
                        }
                        if (b) {
                            return (V)value;
                        }
                        ++this.modCount;
                        this.setValue((E)next, v);
                        return (V)value;
                    }
                    else {
                        next = ((h)next).getNext();
                    }
                }
                ++this.modCount;
                final h e = this.map.entryHelper.e((Segment)this.self(), k, n, h);
                this.setValue((E)e, v);
                table.set(n2, (E)e);
                this.count = count;
                return null;
            }
            finally {
                this.unlock();
            }
        }
        
        public boolean reclaimKey(final E e, int n) {
            this.lock();
            try {
                final AtomicReferenceArray<E> table = this.table;
                n &= table.length() - 1;
                Object next;
                for (h h = (h)(next = table.get(n)); next != null; next = ((h)next).getNext()) {
                    if (next == e) {
                        ++this.modCount;
                        final h removeFromChain = this.removeFromChain(h, (h)next);
                        final int count = this.count;
                        table.set(n, removeFromChain);
                        this.count = count - 1;
                        return true;
                    }
                }
                return false;
            }
            finally {
                this.unlock();
            }
        }
        
        public boolean reclaimValue(final K k, int count, final v v) {
            this.lock();
            try {
                final AtomicReferenceArray<E> table = this.table;
                final int n = table.length() - 1 & count;
                Object next;
                final h h = (h)(next = table.get(n));
                while (next != null) {
                    final Object key = ((h)next).getKey();
                    if (((h)next).getHash() == count && key != null && this.map.keyEquivalence.equivalent(k, key)) {
                        if (((u)next).getValueReference() == v) {
                            ++this.modCount;
                            final h removeFromChain = this.removeFromChain(h, (h)next);
                            count = this.count;
                            table.set(n, removeFromChain);
                            this.count = count - 1;
                            return true;
                        }
                        return false;
                    }
                    else {
                        next = ((h)next).getNext();
                    }
                }
                return false;
            }
            finally {
                this.unlock();
            }
        }
        
        public V remove(Object value, int count) {
            this.lock();
            try {
                this.preWriteCleanup();
                final AtomicReferenceArray<E> table = this.table;
                final int n = table.length() - 1 & count;
                Object next;
                for (h h = (h)(next = table.get(n)); next != null; next = ((h)next).getNext()) {
                    final Object key = ((h)next).getKey();
                    if (((h)next).getHash() == count && key != null && this.map.keyEquivalence.equivalent(value, key)) {
                        value = ((h)next).getValue();
                        if (value == null) {
                            if (!isCollected(next)) {
                                return null;
                            }
                        }
                        ++this.modCount;
                        final h removeFromChain = this.removeFromChain(h, (h)next);
                        count = this.count;
                        table.set(n, removeFromChain);
                        this.count = count - 1;
                        return (V)value;
                    }
                }
                return null;
            }
            finally {
                this.unlock();
            }
        }
        
        public boolean remove(Object value, int count, final Object o) {
            this.lock();
            try {
                this.preWriteCleanup();
                final AtomicReferenceArray<E> table = this.table;
                final int n = table.length() - 1 & count;
                Object next;
                final h h = (h)(next = table.get(n));
                while (true) {
                    boolean b = false;
                    if (next == null) {
                        return false;
                    }
                    final Object key = ((h)next).getKey();
                    if (((h)next).getHash() == count && key != null && this.map.keyEquivalence.equivalent(value, key)) {
                        value = ((h)next).getValue();
                        if (this.map.valueEquivalence().equivalent(o, value)) {
                            b = true;
                        }
                        else if (!isCollected(next)) {
                            return false;
                        }
                        ++this.modCount;
                        final h removeFromChain = this.removeFromChain(h, (h)next);
                        count = this.count;
                        table.set(n, removeFromChain);
                        this.count = count - 1;
                        return b;
                    }
                    next = ((h)next).getNext();
                }
            }
            finally {
                this.unlock();
            }
        }
        
        public boolean removeEntryForTesting(final E e) {
            final int hash = e.getHash();
            final AtomicReferenceArray<E> table = this.table;
            final int n = hash & table.length() - 1;
            Object next;
            for (h h = (h)(next = table.get(n)); next != null; next = ((h)next).getNext()) {
                if (next == e) {
                    ++this.modCount;
                    final h removeFromChain = this.removeFromChain(h, (h)next);
                    final int count = this.count;
                    table.set(n, removeFromChain);
                    this.count = count - 1;
                    return true;
                }
            }
            return false;
        }
        
        public E removeFromChain(E next, final E e) {
            int count = this.count;
            Object next2 = e.getNext();
            while (next != e) {
                final h copyEntry = this.copyEntry(next, (h)next2);
                if (copyEntry != null) {
                    next2 = copyEntry;
                }
                else {
                    --count;
                }
                next = (E)next.getNext();
            }
            this.count = count;
            return (E)next2;
        }
        
        public E removeFromChainForTesting(final h h, final h h2) {
            return this.removeFromChain(this.castForTesting(h), this.castForTesting(h2));
        }
        
        public boolean removeTableEntryForTesting(final h h) {
            return this.removeEntryForTesting(this.castForTesting(h));
        }
        
        public V replace(final K k, int count, final V v) {
            this.lock();
            try {
                this.preWriteCleanup();
                final AtomicReferenceArray<E> table = this.table;
                final int n = table.length() - 1 & count;
                Object next;
                final h h = (h)(next = table.get(n));
                while (next != null) {
                    final Object key = ((h)next).getKey();
                    if (((h)next).getHash() == count && key != null && this.map.keyEquivalence.equivalent(k, key)) {
                        final Object value = ((h)next).getValue();
                        if (value == null) {
                            if (isCollected(next)) {
                                ++this.modCount;
                                final h removeFromChain = this.removeFromChain(h, (h)next);
                                count = this.count;
                                table.set(n, removeFromChain);
                                this.count = count - 1;
                            }
                            return null;
                        }
                        ++this.modCount;
                        this.setValue((E)next, v);
                        return (V)value;
                    }
                    else {
                        next = ((h)next).getNext();
                    }
                }
                return null;
            }
            finally {
                this.unlock();
            }
        }
        
        public boolean replace(final K k, int count, final V v, final V v2) {
            this.lock();
            try {
                this.preWriteCleanup();
                final AtomicReferenceArray<E> table = this.table;
                final int n = table.length() - 1 & count;
                Object next;
                final h h = (h)(next = table.get(n));
                while (next != null) {
                    final Object key = ((h)next).getKey();
                    if (((h)next).getHash() == count && key != null && this.map.keyEquivalence.equivalent(k, key)) {
                        final Object value = ((h)next).getValue();
                        if (value == null) {
                            if (isCollected(next)) {
                                ++this.modCount;
                                final h removeFromChain = this.removeFromChain(h, (h)next);
                                count = this.count;
                                table.set(n, removeFromChain);
                                this.count = count - 1;
                            }
                            return false;
                        }
                        if (this.map.valueEquivalence().equivalent(v, value)) {
                            ++this.modCount;
                            this.setValue((E)next, v2);
                            return true;
                        }
                        return false;
                    }
                    else {
                        next = ((h)next).getNext();
                    }
                }
                return false;
            }
            finally {
                this.unlock();
            }
        }
        
        public void runCleanup() {
            this.runLockedCleanup();
        }
        
        public void runLockedCleanup() {
            if (this.tryLock()) {
                try {
                    this.maybeDrainReferenceQueues();
                    this.readCount.set(0);
                }
                finally {
                    this.unlock();
                }
            }
        }
        
        public abstract S self();
        
        public void setTableEntryForTesting(final int i, final h h) {
            this.table.set(i, this.castForTesting(h));
        }
        
        public void setValue(final E e, final V v) {
            this.map.entryHelper.a((Segment)this.self(), e, v);
        }
        
        public void setValueForTesting(final h h, final V v) {
            this.map.entryHelper.a((Segment)this.self(), this.castForTesting(h), v);
        }
        
        public void setWeakValueReferenceForTesting(final h h, final v v) {
            throw new AssertionError();
        }
        
        public void tryDrainReferenceQueues() {
            if (this.tryLock()) {
                try {
                    this.maybeDrainReferenceQueues();
                }
                finally {
                    this.unlock();
                }
            }
        }
    }
    
    public static final class SerializationProxy<K, V> extends AbstractSerializationProxy<K, V>
    {
        private static final long serialVersionUID = 3L;
        
        public SerializationProxy(final Strength strength, final Strength strength2, final Equivalence equivalence, final Equivalence equivalence2, final int n, final ConcurrentMap<K, V> concurrentMap) {
            super(strength, strength2, equivalence, equivalence2, n, concurrentMap);
        }
        
        private void readObject(final ObjectInputStream objectInputStream) {
            objectInputStream.defaultReadObject();
            super.delegate = this.readMapMaker(objectInputStream).i();
            this.readEntries(objectInputStream);
        }
        
        private Object readResolve() {
            return super.delegate;
        }
        
        private void writeObject(final ObjectOutputStream objectOutputStream) {
            objectOutputStream.defaultWriteObject();
            this.writeMapTo(objectOutputStream);
        }
    }
    
    public enum Strength
    {
        private static final Strength[] $VALUES;
        
        STRONG {
            @Override
            public Equivalence defaultEquivalence() {
                return Equivalence.equals();
            }
        }, 
        WEAK {
            @Override
            public Equivalence defaultEquivalence() {
                return Equivalence.identity();
            }
        };
        
        private static /* synthetic */ Strength[] $values() {
            return new Strength[] { Strength.STRONG, Strength.WEAK };
        }
        
        static {
            $VALUES = $values();
        }
        
        public abstract Equivalence defaultEquivalence();
    }
    
    public static final class StrongKeyDummyValueSegment<K> extends Segment<K, MapMaker.Dummy, m, StrongKeyDummyValueSegment<K>>
    {
        public StrongKeyDummyValueSegment(final MapMakerInternalMap<K, MapMaker.Dummy, m, StrongKeyDummyValueSegment<K>> mapMakerInternalMap, final int n, final int n2) {
            super(mapMakerInternalMap, n, n2);
        }
        
        public m castForTesting(final h h) {
            return (m)h;
        }
        
        public StrongKeyDummyValueSegment<K> self() {
            return this;
        }
    }
    
    public static final class StrongKeyStrongValueSegment<K, V> extends Segment<K, V, n, StrongKeyStrongValueSegment<K, V>>
    {
        public StrongKeyStrongValueSegment(final MapMakerInternalMap<K, V, n, StrongKeyStrongValueSegment<K, V>> mapMakerInternalMap, final int n, final int n2) {
            super(mapMakerInternalMap, n, n2);
        }
        
        public n castForTesting(final h h) {
            return (n)h;
        }
        
        public StrongKeyStrongValueSegment<K, V> self() {
            return this;
        }
    }
    
    public static final class StrongKeyWeakValueSegment<K, V> extends Segment<K, V, o, StrongKeyWeakValueSegment<K, V>>
    {
        private final ReferenceQueue<V> queueForValues;
        
        public StrongKeyWeakValueSegment(final MapMakerInternalMap<K, V, o, StrongKeyWeakValueSegment<K, V>> mapMakerInternalMap, final int n, final int n2) {
            super(mapMakerInternalMap, n, n2);
            this.queueForValues = new ReferenceQueue<V>();
        }
        
        public static /* synthetic */ ReferenceQueue access$100(final StrongKeyWeakValueSegment strongKeyWeakValueSegment) {
            return strongKeyWeakValueSegment.queueForValues;
        }
        
        public o castForTesting(final h h) {
            return (o)h;
        }
        
        @Override
        public ReferenceQueue<V> getValueReferenceQueueForTesting() {
            return this.queueForValues;
        }
        
        @Override
        public v getWeakValueReferenceForTesting(final h h) {
            return this.castForTesting(h).getValueReference();
        }
        
        @Override
        public void maybeClearReferenceQueues() {
            this.clearReferenceQueue(this.queueForValues);
        }
        
        @Override
        public void maybeDrainReferenceQueues() {
            this.drainValueReferenceQueue(this.queueForValues);
        }
        
        @Override
        public v newWeakValueReferenceForTesting(final h h, final V v) {
            return new w(this.queueForValues, v, this.castForTesting(h));
        }
        
        public StrongKeyWeakValueSegment<K, V> self() {
            return this;
        }
        
        @Override
        public void setWeakValueReferenceForTesting(final h h, final v v) {
            final o castForTesting = this.castForTesting(h);
            final v a = o.a(castForTesting);
            o.b(castForTesting, v);
            a.clear();
        }
    }
    
    public static final class WeakKeyDummyValueSegment<K> extends Segment<K, MapMaker.Dummy, r, WeakKeyDummyValueSegment<K>>
    {
        private final ReferenceQueue<K> queueForKeys;
        
        public WeakKeyDummyValueSegment(final MapMakerInternalMap<K, MapMaker.Dummy, r, WeakKeyDummyValueSegment<K>> mapMakerInternalMap, final int n, final int n2) {
            super(mapMakerInternalMap, n, n2);
            this.queueForKeys = new ReferenceQueue<K>();
        }
        
        public static /* synthetic */ ReferenceQueue access$200(final WeakKeyDummyValueSegment weakKeyDummyValueSegment) {
            return weakKeyDummyValueSegment.queueForKeys;
        }
        
        public r castForTesting(final h h) {
            return (r)h;
        }
        
        @Override
        public ReferenceQueue<K> getKeyReferenceQueueForTesting() {
            return this.queueForKeys;
        }
        
        @Override
        public void maybeClearReferenceQueues() {
            this.clearReferenceQueue(this.queueForKeys);
        }
        
        @Override
        public void maybeDrainReferenceQueues() {
            this.drainKeyReferenceQueue(this.queueForKeys);
        }
        
        public WeakKeyDummyValueSegment<K> self() {
            return this;
        }
    }
    
    public static final class WeakKeyStrongValueSegment<K, V> extends Segment<K, V, s, WeakKeyStrongValueSegment<K, V>>
    {
        private final ReferenceQueue<K> queueForKeys;
        
        public WeakKeyStrongValueSegment(final MapMakerInternalMap<K, V, s, WeakKeyStrongValueSegment<K, V>> mapMakerInternalMap, final int n, final int n2) {
            super(mapMakerInternalMap, n, n2);
            this.queueForKeys = new ReferenceQueue<K>();
        }
        
        public static /* synthetic */ ReferenceQueue access$300(final WeakKeyStrongValueSegment weakKeyStrongValueSegment) {
            return weakKeyStrongValueSegment.queueForKeys;
        }
        
        public s castForTesting(final h h) {
            return (s)h;
        }
        
        @Override
        public ReferenceQueue<K> getKeyReferenceQueueForTesting() {
            return this.queueForKeys;
        }
        
        @Override
        public void maybeClearReferenceQueues() {
            this.clearReferenceQueue(this.queueForKeys);
        }
        
        @Override
        public void maybeDrainReferenceQueues() {
            this.drainKeyReferenceQueue(this.queueForKeys);
        }
        
        public WeakKeyStrongValueSegment<K, V> self() {
            return this;
        }
    }
    
    public static final class WeakKeyWeakValueSegment<K, V> extends Segment<K, V, t, WeakKeyWeakValueSegment<K, V>>
    {
        private final ReferenceQueue<K> queueForKeys;
        private final ReferenceQueue<V> queueForValues;
        
        public WeakKeyWeakValueSegment(final MapMakerInternalMap<K, V, t, WeakKeyWeakValueSegment<K, V>> mapMakerInternalMap, final int n, final int n2) {
            super(mapMakerInternalMap, n, n2);
            this.queueForKeys = new ReferenceQueue<K>();
            this.queueForValues = new ReferenceQueue<V>();
        }
        
        public static /* synthetic */ ReferenceQueue access$400(final WeakKeyWeakValueSegment weakKeyWeakValueSegment) {
            return weakKeyWeakValueSegment.queueForKeys;
        }
        
        public static /* synthetic */ ReferenceQueue access$500(final WeakKeyWeakValueSegment weakKeyWeakValueSegment) {
            return weakKeyWeakValueSegment.queueForValues;
        }
        
        public t castForTesting(final h h) {
            return (t)h;
        }
        
        @Override
        public ReferenceQueue<K> getKeyReferenceQueueForTesting() {
            return this.queueForKeys;
        }
        
        @Override
        public ReferenceQueue<V> getValueReferenceQueueForTesting() {
            return this.queueForValues;
        }
        
        @Override
        public v getWeakValueReferenceForTesting(final h h) {
            return this.castForTesting(h).getValueReference();
        }
        
        @Override
        public void maybeClearReferenceQueues() {
            this.clearReferenceQueue(this.queueForKeys);
        }
        
        @Override
        public void maybeDrainReferenceQueues() {
            this.drainKeyReferenceQueue(this.queueForKeys);
            this.drainValueReferenceQueue(this.queueForValues);
        }
        
        @Override
        public v newWeakValueReferenceForTesting(final h h, final V v) {
            return new w(this.queueForValues, v, this.castForTesting(h));
        }
        
        public WeakKeyWeakValueSegment<K, V> self() {
            return this;
        }
        
        @Override
        public void setWeakValueReferenceForTesting(final h h, final v v) {
            final t castForTesting = this.castForTesting(h);
            final v a = t.a(castForTesting);
            t.b(castForTesting, v);
            a.clear();
        }
    }
    
    public abstract static class b implements h
    {
        public final Object a;
        public final int b;
        public final h c;
        
        public b(final Object a, final int b, final h c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
        
        @Override
        public int getHash() {
            return this.b;
        }
        
        @Override
        public Object getKey() {
            return this.a;
        }
        
        @Override
        public h getNext() {
            return this.c;
        }
    }
    
    public interface h
    {
        int getHash();
        
        Object getKey();
        
        h getNext();
        
        Object getValue();
    }
    
    public abstract static class c extends WeakReference implements h
    {
        public final int a;
        public final h b;
        
        public c(final ReferenceQueue q, final Object referent, final int a, final h b) {
            super(referent, q);
            this.a = a;
            this.b = b;
        }
        
        @Override
        public int getHash() {
            return this.a;
        }
        
        @Override
        public Object getKey() {
            return this.get();
        }
        
        @Override
        public h getNext() {
            return this.b;
        }
    }
    
    public abstract static final class d implements h
    {
    }
    
    public final class e extends g
    {
        public e(final MapMakerInternalMap mapMakerInternalMap) {
            mapMakerInternalMap.super();
        }
        
        public Entry g() {
            return ((g)this).d();
        }
    }
    
    public final class f extends l
    {
        public final MapMakerInternalMap a;
        
        public f(final MapMakerInternalMap a) {
            this.a = a;
            super(null);
        }
        
        @Override
        public void clear() {
            this.a.clear();
        }
        
        @Override
        public boolean contains(final Object o) {
            final boolean b = o instanceof Entry;
            final boolean b2 = false;
            if (!b) {
                return false;
            }
            final Entry entry = (Entry)o;
            final Object key = entry.getKey();
            if (key == null) {
                return false;
            }
            final V value = this.a.get(key);
            boolean b3 = b2;
            if (value != null) {
                b3 = b2;
                if (this.a.valueEquivalence().equivalent(entry.getValue(), value)) {
                    b3 = true;
                }
            }
            return b3;
        }
        
        @Override
        public boolean isEmpty() {
            return this.a.isEmpty();
        }
        
        @Override
        public Iterator iterator() {
            return this.a.new e();
        }
        
        @Override
        public boolean remove(final Object o) {
            final boolean b = o instanceof Entry;
            final boolean b2 = false;
            if (!b) {
                return false;
            }
            final Entry entry = (Entry)o;
            final Object key = entry.getKey();
            boolean b3 = b2;
            if (key != null) {
                b3 = b2;
                if (this.a.remove(key, entry.getValue())) {
                    b3 = true;
                }
            }
            return b3;
        }
        
        @Override
        public int size() {
            return this.a.size();
        }
    }
    
    public abstract class g implements Iterator
    {
        public int a;
        public int b;
        public Segment c;
        public AtomicReferenceArray d;
        public h e;
        public x f;
        public x g;
        public final MapMakerInternalMap h;
        
        public g(final MapMakerInternalMap h) {
            this.h = h;
            this.a = h.segments.length - 1;
            this.b = -1;
            this.b();
        }
        
        public final void b() {
            this.f = null;
            if (this.e()) {
                return;
            }
            if (this.f()) {
                return;
            }
            while (true) {
                final int a = this.a;
                if (a < 0) {
                    break;
                }
                final Segment<K, V, E, S>[] segments = this.h.segments;
                this.a = a - 1;
                final Segment<K, V, E, S> c = segments[a];
                this.c = (Segment)c;
                if (c.count == 0) {
                    continue;
                }
                final AtomicReferenceArray<E> table = this.c.table;
                this.d = table;
                this.b = table.length() - 1;
                if (this.f()) {
                    break;
                }
            }
        }
        
        public boolean c(final h h) {
            try {
                final Object key = h.getKey();
                final Object liveValue = this.h.getLiveValue(h);
                if (liveValue != null) {
                    this.f = this.h.new x(key, liveValue);
                    return true;
                }
                return false;
            }
            finally {
                this.c.postReadCleanup();
            }
        }
        
        public x d() {
            final x f = this.f;
            if (f != null) {
                this.g = f;
                this.b();
                return this.g;
            }
            throw new NoSuchElementException();
        }
        
        public boolean e() {
            h h = this.e;
            if (h != null) {
                while (true) {
                    this.e = h.getNext();
                    final h e = this.e;
                    if (e == null) {
                        break;
                    }
                    if (this.c(e)) {
                        return true;
                    }
                    h = this.e;
                }
            }
            return false;
        }
        
        public boolean f() {
            h e;
            do {
                final int b = this.b;
                if (b < 0) {
                    return false;
                }
                final AtomicReferenceArray d = this.d;
                this.b = b - 1;
                e = (h)d.get(b);
                this.e = e;
            } while (e == null || (!this.c(e) && !this.e()));
            return true;
        }
        
        @Override
        public boolean hasNext() {
            return this.f != null;
        }
        
        @Override
        public void remove() {
            hh.e(this.g != null);
            this.h.remove(this.g.getKey());
            this.g = null;
        }
    }
    
    public interface i
    {
        void a(final Segment p0, final h p1, final Object p2);
        
        Strength b();
        
        h c(final Segment p0, final h p1, final h p2);
        
        Strength d();
        
        h e(final Segment p0, final Object p1, final int p2, final h p3);
        
        Segment f(final MapMakerInternalMap p0, final int p1, final int p2);
    }
    
    public final class j extends g
    {
        public j(final MapMakerInternalMap mapMakerInternalMap) {
            mapMakerInternalMap.super();
        }
        
        @Override
        public Object next() {
            return ((g)this).d().getKey();
        }
    }
    
    public final class k extends l
    {
        public final MapMakerInternalMap a;
        
        public k(final MapMakerInternalMap a) {
            this.a = a;
            super(null);
        }
        
        @Override
        public void clear() {
            this.a.clear();
        }
        
        @Override
        public boolean contains(final Object o) {
            return this.a.containsKey(o);
        }
        
        @Override
        public boolean isEmpty() {
            return this.a.isEmpty();
        }
        
        @Override
        public Iterator iterator() {
            return this.a.new j();
        }
        
        @Override
        public boolean remove(final Object o) {
            return this.a.remove(o) != null;
        }
        
        @Override
        public int size() {
            return this.a.size();
        }
    }
    
    public abstract static class l extends AbstractSet
    {
        @Override
        public Object[] toArray() {
            return toArrayList((Collection<Object>)this).toArray();
        }
        
        @Override
        public Object[] toArray(final Object[] a) {
            return toArrayList((Collection<Object>)this).toArray(a);
        }
    }
    
    public static final class m extends b
    {
        public m(final Object o, final int n, final m m) {
            super(o, n, m);
        }
        
        public m a(final m m) {
            return new m(super.a, super.b, m);
        }
        
        public MapMaker.Dummy b() {
            return MapMaker.Dummy.VALUE;
        }
        
        public static final class a implements i
        {
            public static final a a;
            
            static {
                a = new a();
            }
            
            public static a h() {
                return m.a.a;
            }
            
            @Override
            public Strength b() {
                return Strength.STRONG;
            }
            
            @Override
            public Strength d() {
                return Strength.STRONG;
            }
            
            public m g(final StrongKeyDummyValueSegment strongKeyDummyValueSegment, final m m, final m i) {
                return m.a(i);
            }
            
            public m i(final StrongKeyDummyValueSegment strongKeyDummyValueSegment, final Object o, final int n, final m m) {
                return new m(o, n, m);
            }
            
            public StrongKeyDummyValueSegment j(final MapMakerInternalMap mapMakerInternalMap, final int n, final int n2) {
                return new StrongKeyDummyValueSegment(mapMakerInternalMap, n, n2);
            }
            
            public void k(final StrongKeyDummyValueSegment strongKeyDummyValueSegment, final m m, final MapMaker.Dummy dummy) {
            }
        }
    }
    
    public static final class n extends b
    {
        public volatile Object d;
        
        public n(final Object o, final int n, final n n2) {
            super(o, n, n2);
            this.d = null;
        }
        
        public n a(n n) {
            n = new n(super.a, super.b, n);
            n.d = this.d;
            return n;
        }
        
        public void b(final Object d) {
            this.d = d;
        }
        
        @Override
        public Object getValue() {
            return this.d;
        }
        
        public static final class a implements i
        {
            public static final a a;
            
            static {
                a = new a();
            }
            
            public static a h() {
                return n.a.a;
            }
            
            @Override
            public Strength b() {
                return Strength.STRONG;
            }
            
            @Override
            public Strength d() {
                return Strength.STRONG;
            }
            
            public n g(final StrongKeyStrongValueSegment strongKeyStrongValueSegment, final n n, final n n2) {
                return n.a(n2);
            }
            
            public n i(final StrongKeyStrongValueSegment strongKeyStrongValueSegment, final Object o, final int n, final n n2) {
                return new n(o, n, n2);
            }
            
            public StrongKeyStrongValueSegment j(final MapMakerInternalMap mapMakerInternalMap, final int n, final int n2) {
                return new StrongKeyStrongValueSegment(mapMakerInternalMap, n, n2);
            }
            
            public void k(final StrongKeyStrongValueSegment strongKeyStrongValueSegment, final n n, final Object o) {
                n.b(o);
            }
        }
    }
    
    public static final class o extends b implements u
    {
        public volatile v d;
        
        public o(final Object o, final int n, final o o2) {
            super(o, n, o2);
            this.d = MapMakerInternalMap.unsetWeakValueReference();
        }
        
        public static /* synthetic */ v a(final o o) {
            return o.d;
        }
        
        public static /* synthetic */ v b(final o o, final v d) {
            return o.d = d;
        }
        
        public o c(final ReferenceQueue referenceQueue, o o) {
            o = new o(super.a, super.b, o);
            o.d = this.d.a(referenceQueue, o);
            return o;
        }
        
        public void d(final Object o, final ReferenceQueue referenceQueue) {
            final v d = this.d;
            this.d = new w(referenceQueue, o, this);
            d.clear();
        }
        
        @Override
        public Object getValue() {
            return this.d.get();
        }
        
        @Override
        public v getValueReference() {
            return this.d;
        }
        
        public static final class a implements i
        {
            public static final a a;
            
            static {
                a = new a();
            }
            
            public static a h() {
                return o.a.a;
            }
            
            @Override
            public Strength b() {
                return Strength.STRONG;
            }
            
            @Override
            public Strength d() {
                return Strength.WEAK;
            }
            
            public o g(final StrongKeyWeakValueSegment strongKeyWeakValueSegment, final o o, final o o2) {
                if (Segment.isCollected(o)) {
                    return null;
                }
                return o.c(StrongKeyWeakValueSegment.access$100(strongKeyWeakValueSegment), o2);
            }
            
            public o i(final StrongKeyWeakValueSegment strongKeyWeakValueSegment, final Object o, final int n, final o o2) {
                return new o(o, n, o2);
            }
            
            public StrongKeyWeakValueSegment j(final MapMakerInternalMap mapMakerInternalMap, final int n, final int n2) {
                return new StrongKeyWeakValueSegment(mapMakerInternalMap, n, n2);
            }
            
            public void k(final StrongKeyWeakValueSegment strongKeyWeakValueSegment, final o o, final Object o2) {
                o.d(o2, StrongKeyWeakValueSegment.access$100(strongKeyWeakValueSegment));
            }
        }
    }
    
    public final class p extends g
    {
        public p(final MapMakerInternalMap mapMakerInternalMap) {
            mapMakerInternalMap.super();
        }
        
        @Override
        public Object next() {
            return ((g)this).d().getValue();
        }
    }
    
    public final class q extends AbstractCollection
    {
        public final MapMakerInternalMap a;
        
        public q(final MapMakerInternalMap a) {
            this.a = a;
        }
        
        @Override
        public void clear() {
            this.a.clear();
        }
        
        @Override
        public boolean contains(final Object o) {
            return this.a.containsValue(o);
        }
        
        @Override
        public boolean isEmpty() {
            return this.a.isEmpty();
        }
        
        @Override
        public Iterator iterator() {
            return this.a.new p();
        }
        
        @Override
        public int size() {
            return this.a.size();
        }
        
        @Override
        public Object[] toArray() {
            return toArrayList((Collection<Object>)this).toArray();
        }
        
        @Override
        public Object[] toArray(final Object[] a) {
            return toArrayList((Collection<Object>)this).toArray(a);
        }
    }
    
    public static final class r extends c
    {
        public r(final ReferenceQueue referenceQueue, final Object o, final int n, final r r) {
            super(referenceQueue, o, n, r);
        }
        
        public r a(final ReferenceQueue referenceQueue, final r r) {
            return new r(referenceQueue, ((c)this).getKey(), super.a, r);
        }
        
        public MapMaker.Dummy b() {
            return MapMaker.Dummy.VALUE;
        }
        
        public static final class a implements i
        {
            public static final a a;
            
            static {
                a = new a();
            }
            
            public static a h() {
                return r.a.a;
            }
            
            @Override
            public Strength b() {
                return Strength.WEAK;
            }
            
            @Override
            public Strength d() {
                return Strength.STRONG;
            }
            
            public r g(final WeakKeyDummyValueSegment weakKeyDummyValueSegment, final r r, final r r2) {
                if (((c)r).getKey() == null) {
                    return null;
                }
                return r.a(WeakKeyDummyValueSegment.access$200(weakKeyDummyValueSegment), r2);
            }
            
            public r i(final WeakKeyDummyValueSegment weakKeyDummyValueSegment, final Object o, final int n, final r r) {
                return new r(WeakKeyDummyValueSegment.access$200(weakKeyDummyValueSegment), o, n, r);
            }
            
            public WeakKeyDummyValueSegment j(final MapMakerInternalMap mapMakerInternalMap, final int n, final int n2) {
                return new WeakKeyDummyValueSegment(mapMakerInternalMap, n, n2);
            }
            
            public void k(final WeakKeyDummyValueSegment weakKeyDummyValueSegment, final r r, final MapMaker.Dummy dummy) {
            }
        }
    }
    
    public static final class s extends c
    {
        public volatile Object c;
        
        public s(final ReferenceQueue referenceQueue, final Object o, final int n, final s s) {
            super(referenceQueue, o, n, s);
            this.c = null;
        }
        
        public s a(final ReferenceQueue referenceQueue, final s s) {
            final s s2 = new s(referenceQueue, ((c)this).getKey(), super.a, s);
            s2.b(this.c);
            return s2;
        }
        
        public void b(final Object c) {
            this.c = c;
        }
        
        @Override
        public Object getValue() {
            return this.c;
        }
        
        public static final class a implements i
        {
            public static final a a;
            
            static {
                a = new a();
            }
            
            public static a h() {
                return s.a.a;
            }
            
            @Override
            public Strength b() {
                return Strength.WEAK;
            }
            
            @Override
            public Strength d() {
                return Strength.STRONG;
            }
            
            public s g(final WeakKeyStrongValueSegment weakKeyStrongValueSegment, final s s, final s s2) {
                if (((c)s).getKey() == null) {
                    return null;
                }
                return s.a(WeakKeyStrongValueSegment.access$300(weakKeyStrongValueSegment), s2);
            }
            
            public s i(final WeakKeyStrongValueSegment weakKeyStrongValueSegment, final Object o, final int n, final s s) {
                return new s(WeakKeyStrongValueSegment.access$300(weakKeyStrongValueSegment), o, n, s);
            }
            
            public WeakKeyStrongValueSegment j(final MapMakerInternalMap mapMakerInternalMap, final int n, final int n2) {
                return new WeakKeyStrongValueSegment(mapMakerInternalMap, n, n2);
            }
            
            public void k(final WeakKeyStrongValueSegment weakKeyStrongValueSegment, final s s, final Object o) {
                s.b(o);
            }
        }
    }
    
    public static final class t extends c implements u
    {
        public volatile v c;
        
        public t(final ReferenceQueue referenceQueue, final Object o, final int n, final t t) {
            super(referenceQueue, o, n, t);
            this.c = MapMakerInternalMap.unsetWeakValueReference();
        }
        
        public static /* synthetic */ v a(final t t) {
            return t.c;
        }
        
        public static /* synthetic */ v b(final t t, final v c) {
            return t.c = c;
        }
        
        public t c(final ReferenceQueue referenceQueue, final ReferenceQueue referenceQueue2, final t t) {
            final t t2 = new t(referenceQueue, ((c)this).getKey(), super.a, t);
            t2.c = this.c.a(referenceQueue2, t2);
            return t2;
        }
        
        public void d(final Object o, final ReferenceQueue referenceQueue) {
            final v c = this.c;
            this.c = new w(referenceQueue, o, this);
            c.clear();
        }
        
        @Override
        public Object getValue() {
            return this.c.get();
        }
        
        @Override
        public v getValueReference() {
            return this.c;
        }
        
        public static final class a implements i
        {
            public static final a a;
            
            static {
                a = new a();
            }
            
            public static a h() {
                return t.a.a;
            }
            
            @Override
            public Strength b() {
                return Strength.WEAK;
            }
            
            @Override
            public Strength d() {
                return Strength.WEAK;
            }
            
            public t g(final WeakKeyWeakValueSegment weakKeyWeakValueSegment, final t t, final t t2) {
                if (((c)t).getKey() == null) {
                    return null;
                }
                if (Segment.isCollected(t)) {
                    return null;
                }
                return t.c(WeakKeyWeakValueSegment.access$400(weakKeyWeakValueSegment), WeakKeyWeakValueSegment.access$500(weakKeyWeakValueSegment), t2);
            }
            
            public t i(final WeakKeyWeakValueSegment weakKeyWeakValueSegment, final Object o, final int n, final t t) {
                return new t(WeakKeyWeakValueSegment.access$400(weakKeyWeakValueSegment), o, n, t);
            }
            
            public WeakKeyWeakValueSegment j(final MapMakerInternalMap mapMakerInternalMap, final int n, final int n2) {
                return new WeakKeyWeakValueSegment(mapMakerInternalMap, n, n2);
            }
            
            public void k(final WeakKeyWeakValueSegment weakKeyWeakValueSegment, final t t, final Object o) {
                t.d(o, WeakKeyWeakValueSegment.access$500(weakKeyWeakValueSegment));
            }
        }
    }
    
    public interface u extends h
    {
        v getValueReference();
    }
    
    public interface v
    {
        v a(final ReferenceQueue p0, final h p1);
        
        h b();
        
        void clear();
        
        Object get();
    }
    
    public static final class w extends WeakReference implements v
    {
        public final h a;
        
        public w(final ReferenceQueue q, final Object referent, final h a) {
            super(referent, q);
            this.a = a;
        }
        
        @Override
        public v a(final ReferenceQueue referenceQueue, final h h) {
            return new w(referenceQueue, this.get(), h);
        }
        
        @Override
        public h b() {
            return this.a;
        }
    }
    
    public final class x extends s
    {
        public final Object a;
        public Object b;
        public final MapMakerInternalMap c;
        
        public x(final MapMakerInternalMap c, final Object a, final Object b) {
            this.c = c;
            this.a = a;
            this.b = b;
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof Entry;
            boolean b3;
            final boolean b2 = b3 = false;
            if (b) {
                final Entry entry = (Entry)o;
                b3 = b2;
                if (this.a.equals(entry.getKey())) {
                    b3 = b2;
                    if (this.b.equals(entry.getValue())) {
                        b3 = true;
                    }
                }
            }
            return b3;
        }
        
        @Override
        public Object getKey() {
            return this.a;
        }
        
        @Override
        public Object getValue() {
            return this.b;
        }
        
        @Override
        public int hashCode() {
            return this.a.hashCode() ^ this.b.hashCode();
        }
        
        @Override
        public Object setValue(final Object b) {
            final Object put = this.c.put(this.a, b);
            this.b = b;
            return put;
        }
    }
}

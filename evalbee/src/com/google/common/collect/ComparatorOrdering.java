// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Comparator;
import java.io.Serializable;

final class ComparatorOrdering<T> extends Ordering implements Serializable
{
    private static final long serialVersionUID = 0L;
    final Comparator<T> comparator;
    
    public ComparatorOrdering(final Comparator<T> comparator) {
        this.comparator = (Comparator)i71.r(comparator);
    }
    
    @Override
    public int compare(final T t, final T t2) {
        return this.comparator.compare(t, t2);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof ComparatorOrdering && this.comparator.equals(((ComparatorOrdering)o).comparator));
    }
    
    @Override
    public int hashCode() {
        return this.comparator.hashCode();
    }
    
    @Override
    public String toString() {
        return this.comparator.toString();
    }
}

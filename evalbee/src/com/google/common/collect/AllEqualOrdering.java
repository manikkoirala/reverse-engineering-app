// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.List;
import java.io.Serializable;

final class AllEqualOrdering extends Ordering implements Serializable
{
    static final AllEqualOrdering INSTANCE;
    private static final long serialVersionUID = 0L;
    
    static {
        INSTANCE = new AllEqualOrdering();
    }
    
    public AllEqualOrdering() {
    }
    
    private Object readResolve() {
        return AllEqualOrdering.INSTANCE;
    }
    
    @Override
    public int compare(final Object o, final Object o2) {
        return 0;
    }
    
    @Override
    public <E> ImmutableList<E> immutableSortedCopy(final Iterable<E> iterable) {
        return ImmutableList.copyOf((Iterable<? extends E>)iterable);
    }
    
    @Override
    public <S> Ordering reverse() {
        return this;
    }
    
    @Override
    public <E> List<E> sortedCopy(final Iterable<E> iterable) {
        return Lists.j(iterable);
    }
    
    @Override
    public String toString() {
        return "Ordering.allEqual()";
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.NoSuchElementException;
import com.google.common.primitives.Booleans;
import java.io.Serializable;

abstract class Cut<C extends Comparable> implements Comparable<Cut<C>>, Serializable
{
    private static final long serialVersionUID = 0L;
    final C endpoint;
    
    public Cut(final C endpoint) {
        this.endpoint = endpoint;
    }
    
    public static <C extends Comparable> Cut<C> aboveAll() {
        return (Cut<C>)AboveAll.access$100();
    }
    
    public static <C extends Comparable> Cut<C> aboveValue(final C c) {
        return new AboveValue<C>(c);
    }
    
    public static <C extends Comparable> Cut<C> belowAll() {
        return (Cut<C>)BelowAll.access$000();
    }
    
    public static <C extends Comparable> Cut<C> belowValue(final C c) {
        return new BelowValue<C>(c);
    }
    
    public Cut<C> canonical(final DiscreteDomain discreteDomain) {
        return this;
    }
    
    @Override
    public int compareTo(final Cut<C> cut) {
        if (cut == belowAll()) {
            return 1;
        }
        if (cut == aboveAll()) {
            return -1;
        }
        final int compareOrThrow = Range.compareOrThrow(this.endpoint, cut.endpoint);
        if (compareOrThrow != 0) {
            return compareOrThrow;
        }
        return Booleans.c(this instanceof AboveValue, cut instanceof AboveValue);
    }
    
    public abstract void describeAsLowerBound(final StringBuilder p0);
    
    public abstract void describeAsUpperBound(final StringBuilder p0);
    
    public C endpoint() {
        return this.endpoint;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof Cut;
        boolean b3;
        final boolean b2 = b3 = false;
        if (!b) {
            return b3;
        }
        final Cut cut = (Cut)o;
        try {
            final int compareTo = this.compareTo(cut);
            b3 = b2;
            if (compareTo == 0) {
                b3 = true;
            }
            return b3;
        }
        catch (final ClassCastException ex) {
            b3 = b2;
            return b3;
        }
    }
    
    public abstract C greatestValueBelow(final DiscreteDomain p0);
    
    @Override
    public abstract int hashCode();
    
    public abstract boolean isLessThan(final C p0);
    
    public abstract C leastValueAbove(final DiscreteDomain p0);
    
    public abstract BoundType typeAsLowerBound();
    
    public abstract BoundType typeAsUpperBound();
    
    public abstract Cut<C> withLowerBoundType(final BoundType p0, final DiscreteDomain p1);
    
    public abstract Cut<C> withUpperBoundType(final BoundType p0, final DiscreteDomain p1);
    
    public static final class AboveAll extends Cut<Comparable<?>>
    {
        private static final AboveAll INSTANCE;
        private static final long serialVersionUID = 0L;
        
        static {
            INSTANCE = new AboveAll();
        }
        
        private AboveAll() {
            super("");
        }
        
        public static /* synthetic */ AboveAll access$100() {
            return AboveAll.INSTANCE;
        }
        
        private Object readResolve() {
            return AboveAll.INSTANCE;
        }
        
        @Override
        public int compareTo(final Cut<Comparable<?>> cut) {
            return (cut != this) ? 1 : 0;
        }
        
        @Override
        public void describeAsLowerBound(final StringBuilder sb) {
            throw new AssertionError();
        }
        
        @Override
        public void describeAsUpperBound(final StringBuilder sb) {
            sb.append("+\u221e)");
        }
        
        @Override
        public Comparable<?> endpoint() {
            throw new IllegalStateException("range unbounded on this side");
        }
        
        @Override
        public Comparable<?> greatestValueBelow(final DiscreteDomain discreteDomain) {
            return discreteDomain.maxValue();
        }
        
        @Override
        public int hashCode() {
            return System.identityHashCode(this);
        }
        
        @Override
        public boolean isLessThan(final Comparable<?> comparable) {
            return false;
        }
        
        @Override
        public Comparable<?> leastValueAbove(final DiscreteDomain discreteDomain) {
            throw new AssertionError();
        }
        
        @Override
        public String toString() {
            return "+\u221e";
        }
        
        @Override
        public BoundType typeAsLowerBound() {
            throw new AssertionError((Object)"this statement should be unreachable");
        }
        
        @Override
        public BoundType typeAsUpperBound() {
            throw new IllegalStateException();
        }
        
        @Override
        public Cut<Comparable<?>> withLowerBoundType(final BoundType boundType, final DiscreteDomain discreteDomain) {
            throw new AssertionError((Object)"this statement should be unreachable");
        }
        
        @Override
        public Cut<Comparable<?>> withUpperBoundType(final BoundType boundType, final DiscreteDomain discreteDomain) {
            throw new IllegalStateException();
        }
    }
    
    public static final class AboveValue<C extends Comparable> extends Cut<C>
    {
        private static final long serialVersionUID = 0L;
        
        public AboveValue(final C c) {
            super((Comparable)i71.r(c));
        }
        
        @Override
        public Cut<C> canonical(final DiscreteDomain discreteDomain) {
            final Comparable leastValueAbove = this.leastValueAbove(discreteDomain);
            Cut<Comparable> cut;
            if (leastValueAbove != null) {
                cut = Cut.belowValue(leastValueAbove);
            }
            else {
                cut = Cut.aboveAll();
            }
            return (Cut<C>)cut;
        }
        
        @Override
        public void describeAsLowerBound(final StringBuilder sb) {
            sb.append('(');
            sb.append(super.endpoint);
        }
        
        @Override
        public void describeAsUpperBound(final StringBuilder sb) {
            sb.append(super.endpoint);
            sb.append(']');
        }
        
        @Override
        public C greatestValueBelow(final DiscreteDomain discreteDomain) {
            return (C)super.endpoint;
        }
        
        @Override
        public int hashCode() {
            return ~super.endpoint.hashCode();
        }
        
        @Override
        public boolean isLessThan(final C c) {
            return Range.compareOrThrow(super.endpoint, c) < 0;
        }
        
        @Override
        public C leastValueAbove(final DiscreteDomain discreteDomain) {
            return (C)discreteDomain.next(super.endpoint);
        }
        
        @Override
        public String toString() {
            final String value = String.valueOf(super.endpoint);
            final StringBuilder sb = new StringBuilder(value.length() + 2);
            sb.append("/");
            sb.append(value);
            sb.append("\\");
            return sb.toString();
        }
        
        @Override
        public BoundType typeAsLowerBound() {
            return BoundType.OPEN;
        }
        
        @Override
        public BoundType typeAsUpperBound() {
            return BoundType.CLOSED;
        }
        
        @Override
        public Cut<C> withLowerBoundType(final BoundType boundType, final DiscreteDomain discreteDomain) {
            final int n = Cut$a.a[boundType.ordinal()];
            if (n == 1) {
                final Comparable next = discreteDomain.next(super.endpoint);
                Cut<Comparable> cut;
                if (next == null) {
                    cut = Cut.belowAll();
                }
                else {
                    cut = Cut.belowValue(next);
                }
                return (Cut<C>)cut;
            }
            if (n == 2) {
                return this;
            }
            throw new AssertionError();
        }
        
        @Override
        public Cut<C> withUpperBoundType(final BoundType boundType, final DiscreteDomain discreteDomain) {
            final int n = Cut$a.a[boundType.ordinal()];
            if (n == 1) {
                return this;
            }
            if (n == 2) {
                final Comparable next = discreteDomain.next(super.endpoint);
                Cut<Comparable> cut;
                if (next == null) {
                    cut = Cut.aboveAll();
                }
                else {
                    cut = Cut.belowValue(next);
                }
                return (Cut<C>)cut;
            }
            throw new AssertionError();
        }
    }
    
    public static final class BelowAll extends Cut<Comparable<?>>
    {
        private static final BelowAll INSTANCE;
        private static final long serialVersionUID = 0L;
        
        static {
            INSTANCE = new BelowAll();
        }
        
        private BelowAll() {
            super("");
        }
        
        public static /* synthetic */ BelowAll access$000() {
            return BelowAll.INSTANCE;
        }
        
        private Object readResolve() {
            return BelowAll.INSTANCE;
        }
        
        @Override
        public Cut<Comparable<?>> canonical(final DiscreteDomain discreteDomain) {
            try {
                return Cut.belowValue((Comparable<?>)discreteDomain.minValue());
            }
            catch (final NoSuchElementException ex) {
                return this;
            }
        }
        
        @Override
        public int compareTo(final Cut<Comparable<?>> cut) {
            int n;
            if (cut == this) {
                n = 0;
            }
            else {
                n = -1;
            }
            return n;
        }
        
        @Override
        public void describeAsLowerBound(final StringBuilder sb) {
            sb.append("(-\u221e");
        }
        
        @Override
        public void describeAsUpperBound(final StringBuilder sb) {
            throw new AssertionError();
        }
        
        @Override
        public Comparable<?> endpoint() {
            throw new IllegalStateException("range unbounded on this side");
        }
        
        @Override
        public Comparable<?> greatestValueBelow(final DiscreteDomain discreteDomain) {
            throw new AssertionError();
        }
        
        @Override
        public int hashCode() {
            return System.identityHashCode(this);
        }
        
        @Override
        public boolean isLessThan(final Comparable<?> comparable) {
            return true;
        }
        
        @Override
        public Comparable<?> leastValueAbove(final DiscreteDomain discreteDomain) {
            return discreteDomain.minValue();
        }
        
        @Override
        public String toString() {
            return "-\u221e";
        }
        
        @Override
        public BoundType typeAsLowerBound() {
            throw new IllegalStateException();
        }
        
        @Override
        public BoundType typeAsUpperBound() {
            throw new AssertionError((Object)"this statement should be unreachable");
        }
        
        @Override
        public Cut<Comparable<?>> withLowerBoundType(final BoundType boundType, final DiscreteDomain discreteDomain) {
            throw new IllegalStateException();
        }
        
        @Override
        public Cut<Comparable<?>> withUpperBoundType(final BoundType boundType, final DiscreteDomain discreteDomain) {
            throw new AssertionError((Object)"this statement should be unreachable");
        }
    }
    
    public static final class BelowValue<C extends Comparable> extends Cut<C>
    {
        private static final long serialVersionUID = 0L;
        
        public BelowValue(final C c) {
            super((Comparable)i71.r(c));
        }
        
        @Override
        public void describeAsLowerBound(final StringBuilder sb) {
            sb.append('[');
            sb.append(super.endpoint);
        }
        
        @Override
        public void describeAsUpperBound(final StringBuilder sb) {
            sb.append(super.endpoint);
            sb.append(')');
        }
        
        @Override
        public C greatestValueBelow(final DiscreteDomain discreteDomain) {
            return (C)discreteDomain.previous(super.endpoint);
        }
        
        @Override
        public int hashCode() {
            return super.endpoint.hashCode();
        }
        
        @Override
        public boolean isLessThan(final C c) {
            return Range.compareOrThrow(super.endpoint, c) <= 0;
        }
        
        @Override
        public C leastValueAbove(final DiscreteDomain discreteDomain) {
            return (C)super.endpoint;
        }
        
        @Override
        public String toString() {
            final String value = String.valueOf(super.endpoint);
            final StringBuilder sb = new StringBuilder(value.length() + 2);
            sb.append("\\");
            sb.append(value);
            sb.append("/");
            return sb.toString();
        }
        
        @Override
        public BoundType typeAsLowerBound() {
            return BoundType.CLOSED;
        }
        
        @Override
        public BoundType typeAsUpperBound() {
            return BoundType.OPEN;
        }
        
        @Override
        public Cut<C> withLowerBoundType(final BoundType boundType, final DiscreteDomain discreteDomain) {
            final int n = Cut$a.a[boundType.ordinal()];
            if (n == 1) {
                return this;
            }
            if (n == 2) {
                final Comparable previous = discreteDomain.previous(super.endpoint);
                Cut<Comparable> belowAll;
                if (previous == null) {
                    belowAll = Cut.belowAll();
                }
                else {
                    belowAll = new AboveValue<Comparable>(previous);
                }
                return (Cut<C>)belowAll;
            }
            throw new AssertionError();
        }
        
        @Override
        public Cut<C> withUpperBoundType(final BoundType boundType, final DiscreteDomain discreteDomain) {
            final int n = Cut$a.a[boundType.ordinal()];
            if (n == 1) {
                final Comparable previous = discreteDomain.previous(super.endpoint);
                Cut<Comparable> aboveAll;
                if (previous == null) {
                    aboveAll = Cut.aboveAll();
                }
                else {
                    aboveAll = new AboveValue<Comparable>(previous);
                }
                return (Cut<C>)aboveAll;
            }
            if (n == 2) {
                return this;
            }
            throw new AssertionError();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Comparator;
import java.util.List;
import java.util.Collection;
import com.google.common.base.a;
import java.util.Map;
import java.util.Set;
import java.util.Iterator;
import java.io.Serializable;

public abstract class ImmutableTable<R, C, V> extends e implements Serializable
{
    public static <R, C, V> a builder() {
        return new a();
    }
    
    public static <R, C, V> q.a cellOf(final R r, final C c, final V v) {
        return Tables.c(i71.s(r, "rowKey"), i71.s(c, "columnKey"), i71.s(v, "value"));
    }
    
    public static <R, C, V> ImmutableTable<R, C, V> copyOf(final q q) {
        if (q instanceof ImmutableTable) {
            return (ImmutableTable)q;
        }
        return copyOf(q.cellSet());
    }
    
    public static <R, C, V> ImmutableTable<R, C, V> copyOf(final Iterable<? extends q.a> iterable) {
        final a builder = builder();
        final Iterator<? extends q.a> iterator = iterable.iterator();
        while (iterator.hasNext()) {
            builder.c((q.a)iterator.next());
        }
        return builder.a();
    }
    
    public static <R, C, V> ImmutableTable<R, C, V> of() {
        return (ImmutableTable<R, C, V>)SparseImmutableTable.EMPTY;
    }
    
    public static <R, C, V> ImmutableTable<R, C, V> of(final R r, final C c, final V v) {
        return new SingletonImmutableTable<R, C, V>(r, c, v);
    }
    
    public final w02 cellIterator() {
        throw new AssertionError((Object)"should never be called");
    }
    
    @Override
    public ImmutableSet<q.a> cellSet() {
        return (ImmutableSet)super.cellSet();
    }
    
    @Deprecated
    @Override
    public final void clear() {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public ImmutableMap<R, V> column(final C c) {
        i71.s(c, "columnKey");
        return (ImmutableMap)com.google.common.base.a.a(this.columnMap().get(c), ImmutableMap.of());
    }
    
    @Override
    public ImmutableSet<C> columnKeySet() {
        return this.columnMap().keySet();
    }
    
    @Override
    public abstract ImmutableMap<C, Map<R, V>> columnMap();
    
    @Override
    public boolean contains(final Object o, final Object o2) {
        return this.get(o, o2) != null;
    }
    
    @Override
    public boolean containsValue(final Object o) {
        return this.values().contains(o);
    }
    
    @Override
    public abstract ImmutableSet<q.a> createCellSet();
    
    public abstract SerializedForm createSerializedForm();
    
    @Override
    public abstract ImmutableCollection<V> createValues();
    
    @Deprecated
    @Override
    public final V put(final R r, final C c, final V v) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public final void putAll(final q q) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public final V remove(final Object o, final Object o2) {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public ImmutableMap<C, V> row(final R r) {
        i71.s(r, "rowKey");
        return (ImmutableMap)com.google.common.base.a.a(this.rowMap().get(r), ImmutableMap.of());
    }
    
    @Override
    public ImmutableSet<R> rowKeySet() {
        return this.rowMap().keySet();
    }
    
    @Override
    public abstract ImmutableMap<R, Map<C, V>> rowMap();
    
    @Override
    public ImmutableCollection<V> values() {
        return (ImmutableCollection)super.values();
    }
    
    @Override
    public final Iterator<V> valuesIterator() {
        throw new AssertionError((Object)"should never be called");
    }
    
    public final Object writeReplace() {
        return this.createSerializedForm();
    }
    
    public static final class SerializedForm implements Serializable
    {
        private static final long serialVersionUID = 0L;
        private final int[] cellColumnIndices;
        private final int[] cellRowIndices;
        private final Object[] cellValues;
        private final Object[] columnKeys;
        private final Object[] rowKeys;
        
        private SerializedForm(final Object[] rowKeys, final Object[] columnKeys, final Object[] cellValues, final int[] cellRowIndices, final int[] cellColumnIndices) {
            this.rowKeys = rowKeys;
            this.columnKeys = columnKeys;
            this.cellValues = cellValues;
            this.cellRowIndices = cellRowIndices;
            this.cellColumnIndices = cellColumnIndices;
        }
        
        public static SerializedForm create(final ImmutableTable<?, ?, ?> immutableTable, final int[] array, final int[] array2) {
            return new SerializedForm(immutableTable.rowKeySet().toArray(), immutableTable.columnKeySet().toArray(), immutableTable.values().toArray(), array, array2);
        }
        
        public Object readResolve() {
            final Object[] cellValues = this.cellValues;
            if (cellValues.length == 0) {
                return ImmutableTable.of();
            }
            final int length = cellValues.length;
            int n = 0;
            if (length == 1) {
                return ImmutableTable.of(this.rowKeys[0], this.columnKeys[0], cellValues[0]);
            }
            final ImmutableList.a a = new ImmutableList.a(cellValues.length);
            while (true) {
                final Object[] cellValues2 = this.cellValues;
                if (n >= cellValues2.length) {
                    break;
                }
                a.i(ImmutableTable.cellOf(this.rowKeys[this.cellRowIndices[n]], this.columnKeys[this.cellColumnIndices[n]], cellValues2[n]));
                ++n;
            }
            return RegularImmutableTable.forOrderedComponents(a.l(), (ImmutableSet<Object>)ImmutableSet.copyOf(this.rowKeys), (ImmutableSet<Object>)ImmutableSet.copyOf(this.columnKeys));
        }
    }
    
    public static final class a
    {
        public final List a;
        public Comparator b;
        public Comparator c;
        
        public a() {
            this.a = Lists.i();
        }
        
        public ImmutableTable a() {
            return this.b();
        }
        
        public ImmutableTable b() {
            final int size = this.a.size();
            if (size == 0) {
                return ImmutableTable.of();
            }
            if (size != 1) {
                return RegularImmutableTable.forCells(this.a, this.b, this.c);
            }
            return new SingletonImmutableTable((q.a)rg0.h(this.a));
        }
        
        public a c(final q.a a) {
            if (a instanceof Tables.ImmutableCell) {
                i71.s(a.getRowKey(), "row");
                i71.s(a.getColumnKey(), "column");
                i71.s(a.getValue(), "value");
                this.a.add(a);
            }
            else {
                this.d(a.getRowKey(), a.getColumnKey(), a.getValue());
            }
            return this;
        }
        
        public a d(final Object o, final Object o2, final Object o3) {
            this.a.add(ImmutableTable.cellOf(o, o2, o3));
            return this;
        }
    }
}

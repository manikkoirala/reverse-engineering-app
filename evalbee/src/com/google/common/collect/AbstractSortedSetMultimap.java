// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Comparator;
import java.util.Collections;
import java.util.NavigableSet;
import java.util.SortedSet;
import java.util.Set;
import java.util.Collection;
import java.util.Map;

abstract class AbstractSortedSetMultimap<K, V> extends AbstractSetMultimap<K, V> implements to1
{
    private static final long serialVersionUID = 430848587173315748L;
    
    public AbstractSortedSetMultimap(final Map<K, Collection<V>> map) {
        super(map);
    }
    
    @Override
    public Map<K, Collection<V>> asMap() {
        return super.asMap();
    }
    
    public abstract SortedSet<V> createCollection();
    
    @Override
    public SortedSet<V> createUnmodifiableEmptyCollection() {
        return this.unmodifiableCollectionSubclass(this.createCollection());
    }
    
    @Override
    public SortedSet<V> get(final K k) {
        return (SortedSet)super.get(k);
    }
    
    @Override
    public SortedSet<V> removeAll(final Object o) {
        return (SortedSet)super.removeAll(o);
    }
    
    @Override
    public SortedSet<V> replaceValues(final K k, final Iterable<? extends V> iterable) {
        return (SortedSet)super.replaceValues(k, iterable);
    }
    
    @Override
    public <E> SortedSet<E> unmodifiableCollectionSubclass(final Collection<E> collection) {
        if (collection instanceof NavigableSet) {
            return Sets.h((NavigableSet)collection);
        }
        return Collections.unmodifiableSortedSet((SortedSet<E>)(SortedSet)collection);
    }
    
    @Override
    public Collection<V> values() {
        return super.values();
    }
    
    @Override
    public Collection<V> wrapCollection(final K k, final Collection<V> collection) {
        if (collection instanceof NavigableSet) {
            return new m(k, (NavigableSet)collection, null);
        }
        return new o(k, (SortedSet)collection, null);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.NoSuchElementException;
import java.util.ConcurrentModificationException;
import com.google.common.primitives.Ints;
import java.util.Iterator;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;

abstract class AbstractMapBasedMultiset<E> extends b implements Serializable
{
    private static final long serialVersionUID = 0L;
    transient k backingMap;
    transient long size;
    
    public AbstractMapBasedMultiset(final int n) {
        this.backingMap = this.newBackingMap(n);
    }
    
    private void readObject(final ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        final int h = n.h(objectInputStream);
        this.backingMap = this.newBackingMap(3);
        n.g(this, objectInputStream, h);
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        n.k(this, objectOutputStream);
    }
    
    @Override
    public final int add(final E e, final int n) {
        if (n == 0) {
            return this.count(e);
        }
        final boolean b = true;
        i71.h(n > 0, "occurrences cannot be negative: %s", n);
        final int m = this.backingMap.m(e);
        if (m == -1) {
            this.backingMap.u(e, n);
            this.size += n;
            return 0;
        }
        final int k = this.backingMap.k(m);
        final long n2 = k;
        final long n3 = n;
        final long n4 = n2 + n3;
        i71.j(n4 <= 2147483647L && b, "too many occurrences: %s", n4);
        this.backingMap.B(m, (int)n4);
        this.size += n3;
        return k;
    }
    
    public void addTo(final j j) {
        i71.r(j);
        for (int i = this.backingMap.e(); i >= 0; i = this.backingMap.s(i)) {
            j.add(this.backingMap.i(i), this.backingMap.k(i));
        }
    }
    
    @Override
    public final void clear() {
        this.backingMap.a();
        this.size = 0L;
    }
    
    @Override
    public final int count(final Object o) {
        return this.backingMap.f(o);
    }
    
    @Override
    public final int distinctElements() {
        return this.backingMap.C();
    }
    
    @Override
    public final Iterator<E> elementIterator() {
        return new c(this) {
            public final AbstractMapBasedMultiset e;
            
            @Override
            public Object c(final int n) {
                return this.e.backingMap.i(n);
            }
        };
    }
    
    public final Iterator<j.a> entryIterator() {
        return new c(this) {
            public final AbstractMapBasedMultiset e;
            
            public j.a d(final int n) {
                return this.e.backingMap.g(n);
            }
        };
    }
    
    @Override
    public final Iterator<E> iterator() {
        return Multisets.i(this);
    }
    
    public abstract k newBackingMap(final int p0);
    
    @Override
    public final int remove(final Object o, int n) {
        if (n == 0) {
            return this.count(o);
        }
        i71.h(n > 0, "occurrences cannot be negative: %s", n);
        final int m = this.backingMap.m(o);
        if (m == -1) {
            return 0;
        }
        final int k = this.backingMap.k(m);
        if (k > n) {
            this.backingMap.B(m, k - n);
        }
        else {
            this.backingMap.x(m);
            n = k;
        }
        this.size -= n;
        return k;
    }
    
    @Override
    public final int setCount(final E e, final int n) {
        hh.b(n, "count");
        final k backingMap = this.backingMap;
        int n2;
        if (n == 0) {
            n2 = backingMap.v(e);
        }
        else {
            n2 = backingMap.u(e, n);
        }
        this.size += n - n2;
        return n2;
    }
    
    @Override
    public final boolean setCount(final E e, final int n, final int n2) {
        hh.b(n, "oldCount");
        hh.b(n2, "newCount");
        final int m = this.backingMap.m(e);
        if (m == -1) {
            if (n != 0) {
                return false;
            }
            if (n2 > 0) {
                this.backingMap.u(e, n2);
                this.size += n2;
            }
            return true;
        }
        else {
            if (this.backingMap.k(m) != n) {
                return false;
            }
            final k backingMap = this.backingMap;
            long size;
            if (n2 == 0) {
                backingMap.x(m);
                size = this.size - n;
            }
            else {
                backingMap.B(m, n2);
                size = this.size + (n2 - n);
            }
            this.size = size;
            return true;
        }
    }
    
    @Override
    public final int size() {
        return Ints.k(this.size);
    }
    
    public abstract class c implements Iterator
    {
        public int a;
        public int b;
        public int c;
        public final AbstractMapBasedMultiset d;
        
        public c(final AbstractMapBasedMultiset d) {
            this.d = d;
            this.a = d.backingMap.e();
            this.b = -1;
            this.c = d.backingMap.d;
        }
        
        public final void b() {
            if (this.d.backingMap.d == this.c) {
                return;
            }
            throw new ConcurrentModificationException();
        }
        
        public abstract Object c(final int p0);
        
        @Override
        public boolean hasNext() {
            this.b();
            return this.a >= 0;
        }
        
        @Override
        public Object next() {
            if (this.hasNext()) {
                final Object c = this.c(this.a);
                final int a = this.a;
                this.b = a;
                this.a = this.d.backingMap.s(a);
                return c;
            }
            throw new NoSuchElementException();
        }
        
        @Override
        public void remove() {
            this.b();
            hh.e(this.b != -1);
            final AbstractMapBasedMultiset d = this.d;
            d.size -= d.backingMap.x(this.b);
            this.a = this.d.backingMap.t(this.a, this.b);
            this.b = -1;
            this.c = this.d.backingMap.d;
        }
    }
}

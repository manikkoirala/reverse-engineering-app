// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Collections;
import java.util.Set;
import java.io.Serializable;
import com.google.common.primitives.Ints;
import java.util.Collection;
import java.util.Iterator;

public abstract class Multisets
{
    public static boolean a(final j j, final AbstractMapBasedMultiset abstractMapBasedMultiset) {
        if (abstractMapBasedMultiset.isEmpty()) {
            return false;
        }
        abstractMapBasedMultiset.addTo(j);
        return true;
    }
    
    public static boolean b(final j j, final j i) {
        if (i instanceof AbstractMapBasedMultiset) {
            return a(j, (AbstractMapBasedMultiset)i);
        }
        if (i.isEmpty()) {
            return false;
        }
        for (final j.a a : i.entrySet()) {
            j.add(a.getElement(), a.getCount());
        }
        return true;
    }
    
    public static boolean c(final j j, final Collection collection) {
        i71.r(j);
        i71.r(collection);
        if (collection instanceof j) {
            return b(j, d(collection));
        }
        return !collection.isEmpty() && Iterators.a(j, collection.iterator());
    }
    
    public static j d(final Iterable iterable) {
        return (j)iterable;
    }
    
    public static Iterator e(final Iterator iterator) {
        return new r(iterator) {
            public Object c(final j.a a) {
                return a.getElement();
            }
        };
    }
    
    public static boolean f(final j j, final Object o) {
        if (o == j) {
            return true;
        }
        if (o instanceof j) {
            final j i = (j)o;
            if (j.size() == i.size()) {
                if (j.entrySet().size() == i.entrySet().size()) {
                    for (final j.a a : i.entrySet()) {
                        if (j.count(a.getElement()) != a.getCount()) {
                            return false;
                        }
                    }
                    return true;
                }
            }
        }
        return false;
    }
    
    public static j.a g(final Object o, final int n) {
        return new ImmutableEntry<Object>(o, n);
    }
    
    public static int h(final Iterable iterable) {
        if (iterable instanceof j) {
            return ((j)iterable).elementSet().size();
        }
        return 11;
    }
    
    public static Iterator i(final j j) {
        return new e(j, j.entrySet().iterator());
    }
    
    public static int j(final j j) {
        final Iterator iterator = j.entrySet().iterator();
        long n = 0L;
        while (iterator.hasNext()) {
            n += ((j.a)iterator.next()).getCount();
        }
        return Ints.k(n);
    }
    
    public static boolean k(final j j, final Collection collection) {
        Object elementSet = collection;
        if (collection instanceof j) {
            elementSet = ((j)collection).elementSet();
        }
        return j.elementSet().removeAll((Collection)elementSet);
    }
    
    public static boolean l(final j j, final Collection collection) {
        i71.r(collection);
        Object elementSet = collection;
        if (collection instanceof j) {
            elementSet = ((j)collection).elementSet();
        }
        return j.elementSet().retainAll((Collection)elementSet);
    }
    
    public static int m(final j j, final Object o, int n) {
        hh.b(n, "count");
        final int count = j.count(o);
        n -= count;
        if (n > 0) {
            j.add(o, n);
        }
        else if (n < 0) {
            j.remove(o, -n);
        }
        return count;
    }
    
    public static boolean n(final j j, final Object o, final int n, final int n2) {
        hh.b(n, "oldCount");
        hh.b(n2, "newCount");
        if (j.count(o) == n) {
            j.setCount(o, n2);
            return true;
        }
        return false;
    }
    
    public static j o(final j j) {
        if (!(j instanceof UnmodifiableMultiset) && !(j instanceof ImmutableMultiset)) {
            return new UnmodifiableMultiset<Object>((j)i71.r(j));
        }
        return j;
    }
    
    public static o p(final o o) {
        return new UnmodifiableSortedMultiset<Object>((o)i71.r(o));
    }
    
    public static class ImmutableEntry<E> extends b implements Serializable
    {
        private static final long serialVersionUID = 0L;
        private final int count;
        private final E element;
        
        public ImmutableEntry(final E element, final int count) {
            this.element = element;
            hh.b(this.count = count, "count");
        }
        
        @Override
        public final int getCount() {
            return this.count;
        }
        
        @Override
        public final E getElement() {
            return this.element;
        }
        
        public ImmutableEntry<E> nextInBucket() {
            return null;
        }
    }
    
    public abstract static class b implements a
    {
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof a;
            boolean b3;
            final boolean b2 = b3 = false;
            if (b) {
                final a a = (a)o;
                b3 = b2;
                if (((j.a)this).getCount() == a.getCount()) {
                    b3 = b2;
                    if (b11.a(((j.a)this).getElement(), a.getElement())) {
                        b3 = true;
                    }
                }
            }
            return b3;
        }
        
        @Override
        public int hashCode() {
            final Object element = ((j.a)this).getElement();
            int hashCode;
            if (element == null) {
                hashCode = 0;
            }
            else {
                hashCode = element.hashCode();
            }
            return hashCode ^ ((j.a)this).getCount();
        }
        
        @Override
        public String toString() {
            String str = String.valueOf(((j.a)this).getElement());
            final int count = ((j.a)this).getCount();
            if (count != 1) {
                final StringBuilder sb = new StringBuilder(str.length() + 14);
                sb.append(str);
                sb.append(" x ");
                sb.append(count);
                str = sb.toString();
            }
            return str;
        }
    }
    
    public static class UnmodifiableMultiset<E> extends h implements Serializable
    {
        private static final long serialVersionUID = 0L;
        final j delegate;
        transient Set<E> elementSet;
        transient Set<a> entrySet;
        
        public UnmodifiableMultiset(final j delegate) {
            this.delegate = delegate;
        }
        
        @Override
        public int add(final E e, final int n) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public boolean add(final E e) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public boolean addAll(final Collection<? extends E> collection) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void clear() {
            throw new UnsupportedOperationException();
        }
        
        public Set<E> createElementSet() {
            return Collections.unmodifiableSet((Set<? extends E>)this.delegate.elementSet());
        }
        
        @Override
        public j delegate() {
            return this.delegate;
        }
        
        @Override
        public Set<E> elementSet() {
            Set<E> elementSet;
            if ((elementSet = this.elementSet) == null) {
                elementSet = this.createElementSet();
                this.elementSet = elementSet;
            }
            return elementSet;
        }
        
        @Override
        public Set<a> entrySet() {
            Object entrySet;
            if ((entrySet = this.entrySet) == null) {
                entrySet = Collections.unmodifiableSet((Set<? extends a>)this.delegate.entrySet());
                this.entrySet = (Set<a>)entrySet;
            }
            return (Set<a>)entrySet;
        }
        
        @Override
        public Iterator<E> iterator() {
            return Iterators.x(this.delegate.iterator());
        }
        
        @Override
        public int remove(final Object o, final int n) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public boolean remove(final Object o) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public boolean removeAll(final Collection<?> collection) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public boolean retainAll(final Collection<?> collection) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public int setCount(final E e, final int n) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public boolean setCount(final E e, final int n, final int n2) {
            throw new UnsupportedOperationException();
        }
    }
    
    public abstract static class c extends a
    {
        public abstract j a();
        
        @Override
        public void clear() {
            this.a().clear();
        }
        
        @Override
        public boolean contains(final Object o) {
            return this.a().contains(o);
        }
        
        @Override
        public boolean containsAll(final Collection collection) {
            return this.a().containsAll(collection);
        }
        
        @Override
        public boolean isEmpty() {
            return this.a().isEmpty();
        }
        
        @Override
        public boolean remove(final Object o) {
            return this.a().remove(o, Integer.MAX_VALUE) > 0;
        }
        
        @Override
        public int size() {
            return this.a().entrySet().size();
        }
    }
    
    public abstract static class d extends a
    {
        public abstract j a();
        
        @Override
        public void clear() {
            this.a().clear();
        }
        
        @Override
        public boolean contains(final Object o) {
            final boolean b = o instanceof j.a;
            boolean b2 = false;
            if (b) {
                final j.a a = (j.a)o;
                if (a.getCount() <= 0) {
                    return false;
                }
                b2 = b2;
                if (this.a().count(a.getElement()) == a.getCount()) {
                    b2 = true;
                }
            }
            return b2;
        }
        
        @Override
        public boolean remove(Object element) {
            if (element instanceof j.a) {
                final j.a a = (j.a)element;
                element = a.getElement();
                final int count = a.getCount();
                if (count != 0) {
                    return this.a().setCount(element, count, 0);
                }
            }
            return false;
        }
    }
    
    public static final class e implements Iterator
    {
        public final j a;
        public final Iterator b;
        public j.a c;
        public int d;
        public int e;
        public boolean f;
        
        public e(final j a, final Iterator b) {
            this.a = a;
            this.b = b;
        }
        
        @Override
        public boolean hasNext() {
            return this.d > 0 || this.b.hasNext();
        }
        
        @Override
        public Object next() {
            if (this.hasNext()) {
                if (this.d == 0) {
                    final j.a c = this.b.next();
                    this.c = c;
                    final int count = c.getCount();
                    this.d = count;
                    this.e = count;
                }
                --this.d;
                this.f = true;
                final j.a c2 = this.c;
                Objects.requireNonNull(c2);
                return c2.getElement();
            }
            throw new NoSuchElementException();
        }
        
        @Override
        public void remove() {
            hh.e(this.f);
            if (this.e == 1) {
                this.b.remove();
            }
            else {
                final j a = this.a;
                final j.a c = this.c;
                Objects.requireNonNull(c);
                a.remove(c.getElement());
            }
            --this.e;
            this.f = false;
        }
    }
}

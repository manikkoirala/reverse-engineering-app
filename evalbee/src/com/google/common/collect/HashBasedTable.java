// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;
import java.util.LinkedHashMap;
import java.util.Map;

public class HashBasedTable<R, C, V> extends StandardTable<R, C, V>
{
    private static final long serialVersionUID = 0L;
    
    public HashBasedTable(final Map<R, Map<C, V>> map, final Factory<C, V> factory) {
        super(map, factory);
    }
    
    public static <R, C, V> HashBasedTable<R, C, V> create() {
        return new HashBasedTable<R, C, V>(new LinkedHashMap<R, Map<C, V>>(), new Factory<C, V>(0));
    }
    
    public static <R, C, V> HashBasedTable<R, C, V> create(final int n, final int n2) {
        hh.b(n2, "expectedCellsPerRow");
        return new HashBasedTable<R, C, V>(Maps.s(n), new Factory<C, V>(n2));
    }
    
    public static <R, C, V> HashBasedTable<R, C, V> create(final q q) {
        final HashBasedTable<Object, Object, Object> create = create();
        create.putAll(q);
        return (HashBasedTable<R, C, V>)create;
    }
    
    public static class Factory<C, V> implements is1, Serializable
    {
        private static final long serialVersionUID = 0L;
        final int expectedSize;
        
        public Factory(final int expectedSize) {
            this.expectedSize = expectedSize;
        }
        
        @Override
        public Map<C, V> get() {
            return Maps.s(this.expectedSize);
        }
    }
}

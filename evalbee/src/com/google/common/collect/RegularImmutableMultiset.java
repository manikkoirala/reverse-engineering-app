// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Iterator;
import java.io.Serializable;
import java.util.Set;
import com.google.common.primitives.Ints;

class RegularImmutableMultiset<E> extends ImmutableMultiset<E>
{
    static final RegularImmutableMultiset<Object> EMPTY;
    final transient k contents;
    private transient ImmutableSet<E> elementSet;
    private final transient int size;
    
    static {
        EMPTY = new RegularImmutableMultiset<Object>(k.b());
    }
    
    public RegularImmutableMultiset(final k contents) {
        this.contents = contents;
        long n = 0L;
        for (int i = 0; i < contents.C(); ++i) {
            n += contents.k(i);
        }
        this.size = Ints.k(n);
    }
    
    @Override
    public int count(final Object o) {
        return this.contents.f(o);
    }
    
    @Override
    public ImmutableSet<E> elementSet() {
        ImmutableSet<E> elementSet;
        if ((elementSet = this.elementSet) == null) {
            elementSet = new ElementSet(null);
            this.elementSet = elementSet;
        }
        return elementSet;
    }
    
    @Override
    public j.a getEntry(final int n) {
        return this.contents.g(n);
    }
    
    @Override
    public boolean isPartialView() {
        return false;
    }
    
    @Override
    public int size() {
        return this.size;
    }
    
    public Object writeReplace() {
        return new SerializedForm(this);
    }
    
    public final class ElementSet extends IndexedImmutableSet<E>
    {
        final RegularImmutableMultiset this$0;
        
        private ElementSet(final RegularImmutableMultiset this$0) {
            this.this$0 = this$0;
        }
        
        @Override
        public boolean contains(final Object o) {
            return this.this$0.contains(o);
        }
        
        @Override
        public E get(final int n) {
            return (E)this.this$0.contents.i(n);
        }
        
        @Override
        public boolean isPartialView() {
            return true;
        }
        
        @Override
        public int size() {
            return this.this$0.contents.C();
        }
    }
    
    public static class SerializedForm implements Serializable
    {
        private static final long serialVersionUID = 0L;
        final int[] counts;
        final Object[] elements;
        
        public SerializedForm(final j j) {
            final int size = j.entrySet().size();
            this.elements = new Object[size];
            this.counts = new int[size];
            final Iterator iterator = j.entrySet().iterator();
            int n = 0;
            while (iterator.hasNext()) {
                final j.a a = (j.a)iterator.next();
                this.elements[n] = a.getElement();
                this.counts[n] = a.getCount();
                ++n;
            }
        }
        
        public Object readResolve() {
            final b b = new b(this.elements.length);
            int n = 0;
            while (true) {
                final Object[] elements = this.elements;
                if (n >= elements.length) {
                    break;
                }
                b.j(elements[n], this.counts[n]);
                ++n;
            }
            return b.k();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Set;
import java.util.Map;
import java.lang.reflect.Array;
import java.util.Arrays;

public abstract class l
{
    public static Object[] a(final Object[] original, final int from, final int to, final Object[] array) {
        return Arrays.copyOfRange(original, from, to, array.getClass());
    }
    
    public static Object[] b(final Object[] array, final int length) {
        return (Object[])Array.newInstance(array.getClass().getComponentType(), length);
    }
    
    public static Map c(final int n) {
        return CompactHashMap.createWithExpectedSize(n);
    }
    
    public static Set d(final int n) {
        return CompactHashSet.createWithExpectedSize(n);
    }
    
    public static Map e(final int n) {
        return CompactLinkedHashMap.createWithExpectedSize(n);
    }
    
    public static Set f(final int n) {
        return CompactLinkedHashSet.createWithExpectedSize(n);
    }
    
    public static Set g() {
        return CompactHashSet.create();
    }
    
    public static Map h() {
        return CompactHashMap.create();
    }
    
    public static MapMaker i(final MapMaker mapMaker) {
        return mapMaker.l();
    }
}

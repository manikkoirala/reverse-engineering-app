// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Iterator;
import java.util.Collection;
import java.util.Set;
import java.util.AbstractCollection;

public abstract class b extends AbstractCollection implements j
{
    private transient Set<Object> elementSet;
    private transient Set<j.a> entrySet;
    
    @Override
    public int add(final Object o, final int n) {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public final boolean add(final Object o) {
        this.add(o, 1);
        return true;
    }
    
    @Override
    public final boolean addAll(final Collection<Object> collection) {
        return Multisets.c(this, collection);
    }
    
    @Override
    public boolean contains(final Object o) {
        return this.count(o) > 0;
    }
    
    public Set<Object> createElementSet() {
        return new a();
    }
    
    Set<j.a> createEntrySet() {
        return new b();
    }
    
    public abstract int distinctElements();
    
    public abstract Iterator elementIterator();
    
    @Override
    public Set<Object> elementSet() {
        Set<Object> elementSet;
        if ((elementSet = this.elementSet) == null) {
            elementSet = this.createElementSet();
            this.elementSet = elementSet;
        }
        return elementSet;
    }
    
    abstract Iterator entryIterator();
    
    @Override
    public Set<j.a> entrySet() {
        Set<j.a> entrySet;
        if ((entrySet = this.entrySet) == null) {
            entrySet = this.createEntrySet();
            this.entrySet = entrySet;
        }
        return entrySet;
    }
    
    @Override
    public final boolean equals(final Object o) {
        return Multisets.f(this, o);
    }
    
    @Override
    public final int hashCode() {
        return this.entrySet().hashCode();
    }
    
    @Override
    public boolean isEmpty() {
        return this.entrySet().isEmpty();
    }
    
    @Override
    public abstract int remove(final Object p0, final int p1);
    
    @Override
    public final boolean remove(final Object o) {
        boolean b = true;
        if (this.remove(o, 1) <= 0) {
            b = false;
        }
        return b;
    }
    
    @Override
    public final boolean removeAll(final Collection<?> collection) {
        return Multisets.k(this, collection);
    }
    
    @Override
    public final boolean retainAll(final Collection<?> collection) {
        return Multisets.l(this, collection);
    }
    
    @Override
    public int setCount(final Object o, final int n) {
        return Multisets.m(this, o, n);
    }
    
    @Override
    public boolean setCount(final Object o, final int n, final int n2) {
        return Multisets.n(this, o, n, n2);
    }
    
    @Override
    public final String toString() {
        return this.entrySet().toString();
    }
    
    public class a extends c
    {
        public final b a;
        
        public a(final b a) {
            this.a = a;
        }
        
        @Override
        public j a() {
            return this.a;
        }
        
        @Override
        public Iterator iterator() {
            return this.a.elementIterator();
        }
    }
    
    public class b extends d
    {
        public final com.google.common.collect.b a;
        
        public b(final com.google.common.collect.b a) {
            this.a = a;
        }
        
        @Override
        public j a() {
            return this.a;
        }
        
        @Override
        public Iterator iterator() {
            return this.a.entryIterator();
        }
        
        @Override
        public int size() {
            return this.a.distinctElements();
        }
    }
}

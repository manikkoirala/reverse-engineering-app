// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Iterator;
import java.util.ListIterator;

public abstract class s extends r implements ListIterator
{
    public s(final ListIterator listIterator) {
        super(listIterator);
    }
    
    @Override
    public void add(final Object o) {
        throw new UnsupportedOperationException();
    }
    
    public final ListIterator c() {
        return Iterators.c(super.a);
    }
    
    @Override
    public final boolean hasPrevious() {
        return this.c().hasPrevious();
    }
    
    @Override
    public final int nextIndex() {
        return this.c().nextIndex();
    }
    
    @Override
    public final Object previous() {
        return this.b(this.c().previous());
    }
    
    @Override
    public final int previousIndex() {
        return this.c().previousIndex();
    }
    
    @Override
    public void set(final Object o) {
        throw new UnsupportedOperationException();
    }
}

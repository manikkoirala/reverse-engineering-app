// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.NoSuchElementException;
import com.google.common.primitives.Ints;
import java.util.Set;
import java.util.Arrays;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.util.Iterator;
import java.util.Collection;
import java.io.Serializable;

public final class EnumMultiset<E extends Enum<E>> extends b implements Serializable
{
    private static final long serialVersionUID = 0L;
    private transient int[] counts;
    private transient int distinctElements;
    private transient E[] enumConstants;
    private transient long size;
    private transient Class<E> type;
    
    private EnumMultiset(final Class<E> type) {
        this.type = type;
        i71.d(type.isEnum());
        final E[] enumConstants = type.getEnumConstants();
        this.enumConstants = enumConstants;
        this.counts = new int[enumConstants.length];
    }
    
    public static /* synthetic */ Enum[] access$000(final EnumMultiset enumMultiset) {
        return enumMultiset.enumConstants;
    }
    
    public static /* synthetic */ int[] access$100(final EnumMultiset enumMultiset) {
        return enumMultiset.counts;
    }
    
    public static /* synthetic */ int access$210(final EnumMultiset enumMultiset) {
        return enumMultiset.distinctElements--;
    }
    
    public static /* synthetic */ long access$322(final EnumMultiset enumMultiset, long size) {
        size = enumMultiset.size - size;
        return enumMultiset.size = size;
    }
    
    private void checkIsE(final Object obj) {
        i71.r(obj);
        if (this.isActuallyE(obj)) {
            return;
        }
        final String value = String.valueOf(this.type);
        final String value2 = String.valueOf(obj);
        final StringBuilder sb = new StringBuilder(value.length() + 21 + value2.length());
        sb.append("Expected an ");
        sb.append(value);
        sb.append(" but got ");
        sb.append(value2);
        throw new ClassCastException(sb.toString());
    }
    
    public static <E extends Enum<E>> EnumMultiset<E> create(final Class<E> clazz) {
        return new EnumMultiset<E>(clazz);
    }
    
    public static <E extends Enum<E>> EnumMultiset<E> create(final Iterable<E> iterable) {
        final Iterator<E> iterator = iterable.iterator();
        i71.e(iterator.hasNext(), "EnumMultiset constructor passed empty Iterable");
        final EnumMultiset enumMultiset = new EnumMultiset<E>((Class<E>)iterator.next().getDeclaringClass());
        rg0.a(enumMultiset, iterable);
        return (EnumMultiset<E>)enumMultiset;
    }
    
    public static <E extends Enum<E>> EnumMultiset<E> create(final Iterable<E> iterable, final Class<E> clazz) {
        final EnumMultiset<E> create = create(clazz);
        rg0.a(create, iterable);
        return create;
    }
    
    private boolean isActuallyE(final Object o) {
        final boolean b = o instanceof Enum;
        boolean b3;
        final boolean b2 = b3 = false;
        if (b) {
            final Enum enum1 = (Enum)o;
            final int ordinal = enum1.ordinal();
            final E[] enumConstants = this.enumConstants;
            b3 = b2;
            if (ordinal < enumConstants.length) {
                b3 = b2;
                if (enumConstants[ordinal] == enum1) {
                    b3 = true;
                }
            }
        }
        return b3;
    }
    
    private void readObject(final ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        final Class type = (Class)objectInputStream.readObject();
        this.type = type;
        final E[] enumConstants = type.getEnumConstants();
        this.enumConstants = enumConstants;
        this.counts = new int[enumConstants.length];
        n.f(this, objectInputStream);
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(this.type);
        n.k(this, objectOutputStream);
    }
    
    public int add(final E e, final int n) {
        this.checkIsE(e);
        hh.b(n, "occurrences");
        if (n == 0) {
            return this.count(e);
        }
        final int ordinal = e.ordinal();
        final int n2 = this.counts[ordinal];
        final long n3 = n2;
        final long n4 = n;
        final long n5 = n3 + n4;
        i71.j(n5 <= 2147483647L, "too many occurrences: %s", n5);
        this.counts[ordinal] = (int)n5;
        if (n2 == 0) {
            ++this.distinctElements;
        }
        this.size += n4;
        return n2;
    }
    
    @Override
    public void clear() {
        Arrays.fill(this.counts, 0);
        this.size = 0L;
        this.distinctElements = 0;
    }
    
    @Override
    public int count(final Object o) {
        if (o != null && this.isActuallyE(o)) {
            return this.counts[((Enum)o).ordinal()];
        }
        return 0;
    }
    
    @Override
    public int distinctElements() {
        return this.distinctElements;
    }
    
    @Override
    public Iterator<E> elementIterator() {
        return new c(this) {
            public final EnumMultiset d;
            
            public Enum c(final int n) {
                return EnumMultiset.access$000(this.d)[n];
            }
        };
    }
    
    public Iterator<j.a> entryIterator() {
        return new c(this) {
            public final EnumMultiset d;
            
            public j.a c(final int n) {
                return new Multisets.b(this, n) {
                    public final int a;
                    public final EnumMultiset$b b;
                    
                    public Enum a() {
                        return EnumMultiset.access$000(this.b.d)[this.a];
                    }
                    
                    @Override
                    public int getCount() {
                        return EnumMultiset.access$100(this.b.d)[this.a];
                    }
                };
            }
        };
    }
    
    @Override
    public Iterator<E> iterator() {
        return Multisets.i(this);
    }
    
    @Override
    public int remove(final Object o, final int n) {
        if (o == null || !this.isActuallyE(o)) {
            return 0;
        }
        final Enum enum1 = (Enum)o;
        hh.b(n, "occurrences");
        if (n == 0) {
            return this.count(o);
        }
        final int ordinal = enum1.ordinal();
        final int[] counts = this.counts;
        final int n2 = counts[ordinal];
        if (n2 == 0) {
            return 0;
        }
        if (n2 <= n) {
            counts[ordinal] = 0;
            --this.distinctElements;
            this.size -= n2;
        }
        else {
            counts[ordinal] = n2 - n;
            this.size -= n;
        }
        return n2;
    }
    
    public int setCount(final E e, int distinctElements) {
        this.checkIsE(e);
        hh.b(distinctElements, "count");
        final int ordinal = e.ordinal();
        final int[] counts = this.counts;
        final int n = counts[ordinal];
        counts[ordinal] = distinctElements;
        this.size += distinctElements - n;
        if (n == 0 && distinctElements > 0) {
            distinctElements = this.distinctElements + 1;
        }
        else {
            if (n <= 0 || distinctElements != 0) {
                return n;
            }
            distinctElements = this.distinctElements - 1;
        }
        this.distinctElements = distinctElements;
        return n;
    }
    
    @Override
    public int size() {
        return Ints.k(this.size);
    }
    
    public abstract class c implements Iterator
    {
        public int a;
        public int b;
        public final EnumMultiset c;
        
        public c(final EnumMultiset c) {
            this.c = c;
            this.a = 0;
            this.b = -1;
        }
        
        public abstract Object b(final int p0);
        
        @Override
        public boolean hasNext() {
            while (this.a < EnumMultiset.access$000(this.c).length) {
                final int[] access$100 = EnumMultiset.access$100(this.c);
                final int a = this.a;
                if (access$100[a] > 0) {
                    return true;
                }
                this.a = a + 1;
            }
            return false;
        }
        
        @Override
        public Object next() {
            if (this.hasNext()) {
                final Object b = this.b(this.a);
                final int a = this.a;
                this.b = a;
                this.a = a + 1;
                return b;
            }
            throw new NoSuchElementException();
        }
        
        @Override
        public void remove() {
            hh.e(this.b >= 0);
            if (EnumMultiset.access$100(this.c)[this.b] > 0) {
                EnumMultiset.access$210(this.c);
                final EnumMultiset c = this.c;
                EnumMultiset.access$322(c, EnumMultiset.access$100(c)[this.b]);
                EnumMultiset.access$100(this.c)[this.b] = 0;
            }
            this.b = -1;
        }
    }
}

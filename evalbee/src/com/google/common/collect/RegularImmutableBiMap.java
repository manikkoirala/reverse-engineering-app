// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Map;

final class RegularImmutableBiMap<K, V> extends ImmutableBiMap<K, V>
{
    static final RegularImmutableBiMap<Object, Object> EMPTY;
    final transient Object[] alternatingKeysAndValues;
    private final transient RegularImmutableBiMap<V, K> inverse;
    private final transient Object keyHashTable;
    private final transient int keyOffset;
    private final transient int size;
    
    static {
        EMPTY = new RegularImmutableBiMap<Object, Object>();
    }
    
    private RegularImmutableBiMap() {
        this.keyHashTable = null;
        this.alternatingKeysAndValues = new Object[0];
        this.keyOffset = 0;
        this.size = 0;
        this.inverse = (RegularImmutableBiMap<V, K>)this;
    }
    
    private RegularImmutableBiMap(final Object keyHashTable, final Object[] alternatingKeysAndValues, final int size, final RegularImmutableBiMap<V, K> inverse) {
        this.keyHashTable = keyHashTable;
        this.alternatingKeysAndValues = alternatingKeysAndValues;
        this.keyOffset = 1;
        this.size = size;
        this.inverse = inverse;
    }
    
    public RegularImmutableBiMap(final Object[] alternatingKeysAndValues, final int size) {
        this.alternatingKeysAndValues = alternatingKeysAndValues;
        this.size = size;
        this.keyOffset = 0;
        int chooseTableSize;
        if (size >= 2) {
            chooseTableSize = ImmutableSet.chooseTableSize(size);
        }
        else {
            chooseTableSize = 0;
        }
        this.keyHashTable = RegularImmutableMap.createHashTableOrThrow(alternatingKeysAndValues, size, chooseTableSize, 0);
        this.inverse = (RegularImmutableBiMap<V, K>)new RegularImmutableBiMap(RegularImmutableMap.createHashTableOrThrow(alternatingKeysAndValues, size, chooseTableSize, 1), alternatingKeysAndValues, size, (RegularImmutableBiMap<Object, Object>)this);
    }
    
    @Override
    public ImmutableSet<Entry<K, V>> createEntrySet() {
        return (ImmutableSet<Entry<K, V>>)new RegularImmutableMap.EntrySet((ImmutableMap<Object, Object>)this, this.alternatingKeysAndValues, this.keyOffset, this.size);
    }
    
    @Override
    public ImmutableSet<K> createKeySet() {
        return new RegularImmutableMap.KeySet<K>(this, (ImmutableList<K>)new RegularImmutableMap.KeysOrValuesAsList(this.alternatingKeysAndValues, this.keyOffset, this.size));
    }
    
    @Override
    public V get(Object value) {
        if ((value = RegularImmutableMap.get(this.keyHashTable, this.alternatingKeysAndValues, this.size, this.keyOffset, value)) == null) {
            value = null;
        }
        return (V)value;
    }
    
    @Override
    public ImmutableBiMap<V, K> inverse() {
        return (ImmutableBiMap<V, K>)this.inverse;
    }
    
    @Override
    public boolean isPartialView() {
        return false;
    }
    
    @Override
    public int size() {
        return this.size;
    }
}

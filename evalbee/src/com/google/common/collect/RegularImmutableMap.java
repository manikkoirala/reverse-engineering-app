// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Iterator;
import java.util.AbstractMap;
import java.util.Map;
import java.util.Arrays;
import java.util.Objects;

final class RegularImmutableMap<K, V> extends ImmutableMap<K, V>
{
    private static final byte ABSENT = -1;
    private static final int BYTE_MASK = 255;
    private static final int BYTE_MAX_SIZE = 128;
    static final ImmutableMap<Object, Object> EMPTY;
    private static final int SHORT_MASK = 65535;
    private static final int SHORT_MAX_SIZE = 32768;
    private static final long serialVersionUID = 0L;
    final transient Object[] alternatingKeysAndValues;
    private final transient Object hashTable;
    private final transient int size;
    
    static {
        EMPTY = new RegularImmutableMap<Object, Object>(null, new Object[0], 0);
    }
    
    private RegularImmutableMap(final Object hashTable, final Object[] alternatingKeysAndValues, final int size) {
        this.hashTable = hashTable;
        this.alternatingKeysAndValues = alternatingKeysAndValues;
        this.size = size;
    }
    
    public static <K, V> RegularImmutableMap<K, V> create(final int n, final Object[] array) {
        return create(n, array, null);
    }
    
    public static <K, V> RegularImmutableMap<K, V> create(int intValue, final Object[] original, final b b) {
        if (intValue == 0) {
            return (RegularImmutableMap)RegularImmutableMap.EMPTY;
        }
        if (intValue == 1) {
            final Object obj = original[0];
            Objects.requireNonNull(obj);
            final Object obj2 = original[1];
            Objects.requireNonNull(obj2);
            hh.a(obj, obj2);
            return new RegularImmutableMap<K, V>(null, original, 1);
        }
        i71.u(intValue, original.length >> 1);
        Object hashTable;
        final Object o = hashTable = createHashTable(original, intValue, ImmutableSet.chooseTableSize(intValue), 0);
        Object[] copy = original;
        if (o instanceof Object[]) {
            final Object[] array = (Object[])o;
            final a e = (a)array[2];
            if (b == null) {
                throw e.a();
            }
            b.e = e;
            hashTable = array[0];
            intValue = (int)array[1];
            copy = Arrays.copyOf(original, intValue * 2);
        }
        return new RegularImmutableMap<K, V>(hashTable, copy, intValue);
    }
    
    private static Object createHashTable(Object[] array, final int n, int i, final int n2) {
        final Object o = null;
        final Object o2 = null;
        Object o3 = null;
        if (n == 1) {
            final Object obj = array[n2];
            Objects.requireNonNull(obj);
            final Object obj2 = array[n2 ^ 0x1];
            Objects.requireNonNull(obj2);
            hh.a(obj, obj2);
            return null;
        }
        final int n3 = i - 1;
        final int n4 = 0;
        final int n5 = 0;
        final int n6 = 0;
        if (i <= 128) {
            final byte[] a = new byte[i];
            Arrays.fill(a, (byte)(-1));
            int j = 0;
            int n7;
            int n8;
            Object obj3;
            Object obj4;
            int c;
            Object o4;
            int n9;
            Object obj5;
            for (i = n6; i < n; ++i) {
                n7 = i * 2 + n2;
                n8 = j * 2 + n2;
                obj3 = array[n7];
                Objects.requireNonNull(obj3);
                obj4 = array[n7 ^ 0x1];
                Objects.requireNonNull(obj4);
                hh.a(obj3, obj4);
                c = rc0.c(obj3.hashCode());
                while (true) {
                    c &= n3;
                    o4 = (a[c] & 0xFF);
                    if (o4 == 255) {
                        a[c] = (byte)n8;
                        if (j < i) {
                            array[n8] = obj3;
                            array[n8 ^ 0x1] = obj4;
                        }
                        ++j;
                        break;
                    }
                    if (obj3.equals(array[o4])) {
                        n9 = (int)(o4 ^ 0x1);
                        obj5 = array[n9];
                        Objects.requireNonNull(obj5);
                        o3 = new b.a(obj3, obj4, obj5);
                        array[n9] = obj4;
                        break;
                    }
                    ++c;
                }
            }
            if (j == n) {
                array = (Object[])a;
            }
            else {
                array = new Object[] { a, j, o3 };
            }
            return array;
        }
        if (i <= 32768) {
            final short[] a2 = new short[i];
            Arrays.fill(a2, (short)(-1));
            int k = 0;
            i = n4;
            Object o5 = o;
            while (i < n) {
                final int n10 = i * 2 + n2;
                final int n11 = k * 2 + n2;
                final Object obj6 = array[n10];
                Objects.requireNonNull(obj6);
                final Object obj7 = array[n10 ^ 0x1];
                Objects.requireNonNull(obj7);
                hh.a(obj6, obj7);
                int c2 = rc0.c(obj6.hashCode());
                while (true) {
                    final int n12 = c2 & n3;
                    final Object o6 = a2[n12] & 0xFFFF;
                    if (o6 == 65535) {
                        a2[n12] = (short)n11;
                        if (k < i) {
                            array[n11] = obj6;
                            array[n11 ^ 0x1] = obj7;
                        }
                        ++k;
                        break;
                    }
                    if (obj6.equals(array[o6])) {
                        final int n13 = (int)(o6 ^ 0x1);
                        final Object obj8 = array[n13];
                        Objects.requireNonNull(obj8);
                        o5 = new b.a(obj6, obj7, obj8);
                        array[n13] = obj7;
                        break;
                    }
                    c2 = n12 + 1;
                }
                ++i;
            }
            if (k == n) {
                array = (Object[])a2;
            }
            else {
                array = new Object[] { a2, k, o5 };
            }
            return array;
        }
        final int[] a3 = new int[i];
        Arrays.fill(a3, -1);
        final int n14 = 0;
        i = n5;
        int l = n14;
        Object o7 = o2;
        while (i < n) {
            final int n15 = i * 2 + n2;
            final int n16 = l * 2 + n2;
            final Object obj9 = array[n15];
            Objects.requireNonNull(obj9);
            final Object obj10 = array[n15 ^ 0x1];
            Objects.requireNonNull(obj10);
            hh.a(obj9, obj10);
            int c3 = rc0.c(obj9.hashCode());
            while (true) {
                final int n17 = c3 & n3;
                final Object o8 = a3[n17];
                if (o8 == -1) {
                    a3[n17] = n16;
                    if (l < i) {
                        array[n16] = obj9;
                        array[n16 ^ 0x1] = obj10;
                    }
                    ++l;
                    break;
                }
                if (obj9.equals(array[o8])) {
                    final int n18 = (int)(o8 ^ 0x1);
                    final Object obj11 = array[n18];
                    Objects.requireNonNull(obj11);
                    o7 = new b.a(obj9, obj10, obj11);
                    array[n18] = obj10;
                    break;
                }
                c3 = n17 + 1;
            }
            ++i;
        }
        if (l == n) {
            array = (Object[])a3;
        }
        else {
            array = new Object[] { a3, l, o7 };
        }
        return array;
    }
    
    public static Object createHashTableOrThrow(final Object[] array, final int n, final int n2, final int n3) {
        final Object hashTable = createHashTable(array, n, n2, n3);
        if (!(hashTable instanceof Object[])) {
            return hashTable;
        }
        throw ((a)((Object[])hashTable)[2]).a();
    }
    
    public static Object get(Object obj, final Object[] array, int n, int n2, final Object obj2) {
        final Object o = null;
        if (obj2 == null) {
            return null;
        }
        if (n == 1) {
            final Object obj3 = array[n2];
            Objects.requireNonNull(obj3);
            obj = o;
            if (obj3.equals(obj2)) {
                obj = array[n2 ^ 0x1];
                Objects.requireNonNull(obj);
            }
            return obj;
        }
        if (obj == null) {
            return null;
        }
        if (obj instanceof byte[]) {
            final byte[] array2 = (byte[])obj;
            n2 = array2.length;
            n = rc0.c(obj2.hashCode());
            while (true) {
                n &= n2 - 1;
                final int n3 = array2[n] & 0xFF;
                if (n3 == 255) {
                    return null;
                }
                if (obj2.equals(array[n3])) {
                    return array[n3 ^ 0x1];
                }
                ++n;
            }
        }
        else if (obj instanceof short[]) {
            final short[] array3 = (short[])obj;
            n2 = array3.length;
            n = rc0.c(obj2.hashCode());
            while (true) {
                final int n4 = n & n2 - 1;
                n = (array3[n4] & 0xFFFF);
                if (n == 65535) {
                    return null;
                }
                if (obj2.equals(array[n])) {
                    return array[n ^ 0x1];
                }
                n = n4 + 1;
            }
        }
        else {
            final int[] array4 = (int[])obj;
            n2 = array4.length;
            n = rc0.c(obj2.hashCode());
            while (true) {
                final int n5 = n & n2 - 1;
                n = array4[n5];
                if (n == -1) {
                    return null;
                }
                if (obj2.equals(array[n])) {
                    return array[n ^ 0x1];
                }
                n = n5 + 1;
            }
        }
    }
    
    @Override
    public ImmutableSet<Entry<K, V>> createEntrySet() {
        return (ImmutableSet<Entry<K, V>>)new EntrySet((ImmutableMap<Object, Object>)this, this.alternatingKeysAndValues, 0, this.size);
    }
    
    @Override
    public ImmutableSet<K> createKeySet() {
        return new KeySet<K>(this, (ImmutableList<K>)new KeysOrValuesAsList(this.alternatingKeysAndValues, 0, this.size));
    }
    
    @Override
    public ImmutableCollection<V> createValues() {
        return (ImmutableCollection<V>)new KeysOrValuesAsList(this.alternatingKeysAndValues, 1, this.size);
    }
    
    @Override
    public V get(Object value) {
        if ((value = get(this.hashTable, this.alternatingKeysAndValues, this.size, 0, value)) == null) {
            value = null;
        }
        return (V)value;
    }
    
    @Override
    public boolean isPartialView() {
        return false;
    }
    
    @Override
    public int size() {
        return this.size;
    }
    
    public static class EntrySet<K, V> extends ImmutableSet<Entry<K, V>>
    {
        private final transient Object[] alternatingKeysAndValues;
        private final transient int keyOffset;
        private final transient ImmutableMap<K, V> map;
        private final transient int size;
        
        public EntrySet(final ImmutableMap<K, V> map, final Object[] alternatingKeysAndValues, final int keyOffset, final int size) {
            this.map = map;
            this.alternatingKeysAndValues = alternatingKeysAndValues;
            this.keyOffset = keyOffset;
            this.size = size;
        }
        
        public static /* synthetic */ int access$000(final EntrySet set) {
            return set.size;
        }
        
        public static /* synthetic */ Object[] access$100(final EntrySet set) {
            return set.alternatingKeysAndValues;
        }
        
        public static /* synthetic */ int access$200(final EntrySet set) {
            return set.keyOffset;
        }
        
        @Override
        public boolean contains(Object key) {
            final boolean b = key instanceof Entry;
            boolean b3;
            final boolean b2 = b3 = false;
            if (b) {
                final Entry entry = (Entry)key;
                key = entry.getKey();
                final Object value = entry.getValue();
                b3 = b2;
                if (value != null) {
                    b3 = b2;
                    if (value.equals(this.map.get(key))) {
                        b3 = true;
                    }
                }
            }
            return b3;
        }
        
        @Override
        public int copyIntoArray(final Object[] array, final int n) {
            return this.asList().copyIntoArray(array, n);
        }
        
        @Override
        public ImmutableList<Entry<K, V>> createAsList() {
            return new ImmutableList<Entry<K, V>>(this) {
                final EntrySet this$0;
                
                @Override
                public Entry<K, V> get(int n) {
                    i71.p(n, EntrySet.access$000(this.this$0));
                    final Object[] access$100 = EntrySet.access$100(this.this$0);
                    n *= 2;
                    final Object o = access$100[EntrySet.access$200(this.this$0) + n];
                    Objects.requireNonNull(o);
                    final Object o2 = EntrySet.access$100(this.this$0)[n + (EntrySet.access$200(this.this$0) ^ 0x1)];
                    Objects.requireNonNull(o2);
                    return (Entry<K, V>)new AbstractMap.SimpleImmutableEntry(o, o2);
                }
                
                @Override
                public boolean isPartialView() {
                    return true;
                }
                
                @Override
                public int size() {
                    return EntrySet.access$000(this.this$0);
                }
            };
        }
        
        @Override
        public boolean isPartialView() {
            return true;
        }
        
        @Override
        public w02 iterator() {
            return this.asList().iterator();
        }
        
        @Override
        public int size() {
            return this.size;
        }
    }
    
    public static final class KeySet<K> extends ImmutableSet<K>
    {
        private final transient ImmutableList<K> list;
        private final transient ImmutableMap<K, ?> map;
        
        public KeySet(final ImmutableMap<K, ?> map, final ImmutableList<K> list) {
            this.map = map;
            this.list = list;
        }
        
        @Override
        public ImmutableList<K> asList() {
            return this.list;
        }
        
        @Override
        public boolean contains(final Object o) {
            return this.map.get(o) != null;
        }
        
        @Override
        public int copyIntoArray(final Object[] array, final int n) {
            return this.asList().copyIntoArray(array, n);
        }
        
        @Override
        public boolean isPartialView() {
            return true;
        }
        
        @Override
        public w02 iterator() {
            return this.asList().iterator();
        }
        
        @Override
        public int size() {
            return this.map.size();
        }
    }
    
    public static final class KeysOrValuesAsList extends ImmutableList<Object>
    {
        private final transient Object[] alternatingKeysAndValues;
        private final transient int offset;
        private final transient int size;
        
        public KeysOrValuesAsList(final Object[] alternatingKeysAndValues, final int offset, final int size) {
            this.alternatingKeysAndValues = alternatingKeysAndValues;
            this.offset = offset;
            this.size = size;
        }
        
        @Override
        public Object get(final int n) {
            i71.p(n, this.size);
            final Object obj = this.alternatingKeysAndValues[n * 2 + this.offset];
            Objects.requireNonNull(obj);
            return obj;
        }
        
        @Override
        public boolean isPartialView() {
            return true;
        }
        
        @Override
        public int size() {
            return this.size;
        }
    }
}

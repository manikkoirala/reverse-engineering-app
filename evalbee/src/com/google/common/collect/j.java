// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Iterator;
import java.util.Set;
import java.util.Collection;

public interface j extends Collection
{
    int add(final Object p0, final int p1);
    
    boolean contains(final Object p0);
    
    boolean containsAll(final Collection p0);
    
    int count(final Object p0);
    
    Set elementSet();
    
    Set entrySet();
    
    boolean equals(final Object p0);
    
    int hashCode();
    
    Iterator iterator();
    
    int remove(final Object p0, final int p1);
    
    boolean remove(final Object p0);
    
    int setCount(final Object p0, final int p1);
    
    boolean setCount(final Object p0, final int p1, final int p2);
    
    int size();
    
    public interface a
    {
        int getCount();
        
        Object getElement();
        
        String toString();
    }
}

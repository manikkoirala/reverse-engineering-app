// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.io.Serializable;

final class NullsLastOrdering<T> extends Ordering implements Serializable
{
    private static final long serialVersionUID = 0L;
    final Ordering ordering;
    
    public NullsLastOrdering(final Ordering ordering) {
        this.ordering = ordering;
    }
    
    @Override
    public int compare(final T t, final T t2) {
        if (t == t2) {
            return 0;
        }
        if (t == null) {
            return 1;
        }
        if (t2 == null) {
            return -1;
        }
        return this.ordering.compare(t, t2);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof NullsLastOrdering && this.ordering.equals(((NullsLastOrdering)o).ordering));
    }
    
    @Override
    public int hashCode() {
        return this.ordering.hashCode() ^ 0xC9177248;
    }
    
    @Override
    public <S extends T> Ordering nullsFirst() {
        return this.ordering.nullsFirst();
    }
    
    @Override
    public <S extends T> Ordering nullsLast() {
        return this;
    }
    
    @Override
    public <S extends T> Ordering reverse() {
        return this.ordering.reverse().nullsFirst();
    }
    
    @Override
    public String toString() {
        final String value = String.valueOf(this.ordering);
        final StringBuilder sb = new StringBuilder(value.length() + 12);
        sb.append(value);
        sb.append(".nullsLast()");
        return sb.toString();
    }
}

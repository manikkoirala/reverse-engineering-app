// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Collection;
import java.util.Map;

abstract class ArrayListMultimapGwtSerializationDependencies<K, V> extends AbstractListMultimap<K, V>
{
    public ArrayListMultimapGwtSerializationDependencies(final Map<K, Collection<V>> map) {
        super(map);
    }
}

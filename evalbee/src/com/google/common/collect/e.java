// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.AbstractCollection;
import java.util.AbstractSet;
import java.util.Map;
import java.util.Iterator;
import java.util.Collection;
import java.util.Set;

public abstract class e implements q
{
    private transient Set<a> cellSet;
    private transient Collection<Object> values;
    
    abstract Iterator cellIterator();
    
    @Override
    public Set cellSet() {
        Set<a> cellSet;
        if ((cellSet = this.cellSet) == null) {
            cellSet = this.createCellSet();
            this.cellSet = cellSet;
        }
        return cellSet;
    }
    
    @Override
    public abstract void clear();
    
    @Override
    public boolean contains(final Object o, final Object o2) {
        final Map map = (Map)Maps.w(this.rowMap(), o);
        return map != null && Maps.v(map, o2);
    }
    
    @Override
    public boolean containsColumn(final Object o) {
        return Maps.v(this.columnMap(), o);
    }
    
    @Override
    public boolean containsRow(final Object o) {
        return Maps.v(this.rowMap(), o);
    }
    
    @Override
    public boolean containsValue(final Object o) {
        final Iterator iterator = this.rowMap().values().iterator();
        while (iterator.hasNext()) {
            if (((Map)iterator.next()).containsValue(o)) {
                return true;
            }
        }
        return false;
    }
    
    public Set<a> createCellSet() {
        return new b();
    }
    
    public Collection<Object> createValues() {
        return new c();
    }
    
    @Override
    public boolean equals(final Object o) {
        return Tables.b(this, o);
    }
    
    @Override
    public Object get(Object w, final Object o) {
        final Map map = (Map)Maps.w(this.rowMap(), w);
        if (map == null) {
            w = null;
        }
        else {
            w = Maps.w(map, o);
        }
        return w;
    }
    
    @Override
    public int hashCode() {
        return this.cellSet().hashCode();
    }
    
    @Override
    public boolean isEmpty() {
        return this.size() == 0;
    }
    
    @Override
    public abstract Object put(final Object p0, final Object p1, final Object p2);
    
    @Override
    public void putAll(final q q) {
        for (final a a : q.cellSet()) {
            this.put(a.getRowKey(), a.getColumnKey(), a.getValue());
        }
    }
    
    @Override
    public String toString() {
        return this.rowMap().toString();
    }
    
    @Override
    public Collection values() {
        Collection<Object> values;
        if ((values = this.values) == null) {
            values = this.createValues();
            this.values = values;
        }
        return values;
    }
    
    public Iterator<Object> valuesIterator() {
        return new r(this, this.cellSet().iterator()) {
            public Object c(final a a) {
                return a.getValue();
            }
        };
    }
    
    public class b extends AbstractSet
    {
        public final e a;
        
        public b(final e a) {
            this.a = a;
        }
        
        @Override
        public void clear() {
            this.a.clear();
        }
        
        @Override
        public boolean contains(final Object o) {
            final boolean b = o instanceof a;
            boolean b3;
            final boolean b2 = b3 = false;
            if (b) {
                final a a = (a)o;
                final Map map = (Map)Maps.w(this.a.rowMap(), a.getRowKey());
                b3 = b2;
                if (map != null) {
                    b3 = b2;
                    if (lh.c(map.entrySet(), Maps.j(a.getColumnKey(), a.getValue()))) {
                        b3 = true;
                    }
                }
            }
            return b3;
        }
        
        @Override
        public Iterator iterator() {
            return this.a.cellIterator();
        }
        
        @Override
        public boolean remove(final Object o) {
            final boolean b = o instanceof a;
            boolean b3;
            final boolean b2 = b3 = false;
            if (b) {
                final a a = (a)o;
                final Map map = (Map)Maps.w(this.a.rowMap(), a.getRowKey());
                b3 = b2;
                if (map != null) {
                    b3 = b2;
                    if (lh.d(map.entrySet(), Maps.j(a.getColumnKey(), a.getValue()))) {
                        b3 = true;
                    }
                }
            }
            return b3;
        }
        
        @Override
        public int size() {
            return this.a.size();
        }
    }
    
    public class c extends AbstractCollection
    {
        public final e a;
        
        public c(final e a) {
            this.a = a;
        }
        
        @Override
        public void clear() {
            this.a.clear();
        }
        
        @Override
        public boolean contains(final Object o) {
            return this.a.containsValue(o);
        }
        
        @Override
        public Iterator iterator() {
            return this.a.valuesIterator();
        }
        
        @Override
        public int size() {
            return this.a.size();
        }
    }
}

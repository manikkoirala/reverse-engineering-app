// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.NoSuchElementException;
import java.util.ConcurrentModificationException;
import java.util.Arrays;
import com.google.common.primitives.Ints;
import java.util.Iterator;
import java.io.ObjectOutputStream;
import java.util.Objects;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.Collections;
import java.util.Collection;
import java.io.Serializable;
import java.util.AbstractSet;

class CompactHashSet<E> extends AbstractSet<E> implements Serializable
{
    static final double HASH_FLOODING_FPP = 0.001;
    private static final int MAX_HASH_BUCKET_LENGTH = 9;
    transient Object[] elements;
    private transient int[] entries;
    private transient int metadata;
    private transient int size;
    private transient Object table;
    
    public CompactHashSet() {
        this.init(3);
    }
    
    public CompactHashSet(final int n) {
        this.init(n);
    }
    
    public static /* synthetic */ int access$000(final CompactHashSet set) {
        return set.metadata;
    }
    
    public static <E> CompactHashSet<E> create() {
        return new CompactHashSet<E>();
    }
    
    public static <E> CompactHashSet<E> create(final Collection<? extends E> c) {
        final CompactHashSet<Object> withExpectedSize = createWithExpectedSize(c.size());
        withExpectedSize.addAll(c);
        return (CompactHashSet<E>)withExpectedSize;
    }
    
    @SafeVarargs
    public static <E> CompactHashSet<E> create(final E... elements) {
        final CompactHashSet<Object> withExpectedSize = createWithExpectedSize(elements.length);
        Collections.addAll(withExpectedSize, elements);
        return (CompactHashSet<E>)withExpectedSize;
    }
    
    private Set<E> createHashFloodingResistantDelegate(final int initialCapacity) {
        return new LinkedHashSet<E>(initialCapacity, 1.0f);
    }
    
    public static <E> CompactHashSet<E> createWithExpectedSize(final int n) {
        return new CompactHashSet<E>(n);
    }
    
    private E element(final int n) {
        return (E)this.requireElements()[n];
    }
    
    private int entry(final int n) {
        return this.requireEntries()[n];
    }
    
    private int hashTableMask() {
        return (1 << (this.metadata & 0x1F)) - 1;
    }
    
    private void readObject(final ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        final int int1 = objectInputStream.readInt();
        if (int1 >= 0) {
            this.init(int1);
            for (int i = 0; i < int1; ++i) {
                this.add(objectInputStream.readObject());
            }
            return;
        }
        final StringBuilder sb = new StringBuilder(25);
        sb.append("Invalid size: ");
        sb.append(int1);
        throw new InvalidObjectException(sb.toString());
    }
    
    private Object[] requireElements() {
        final Object[] elements = this.elements;
        Objects.requireNonNull(elements);
        return elements;
    }
    
    private int[] requireEntries() {
        final int[] entries = this.entries;
        Objects.requireNonNull(entries);
        return entries;
    }
    
    private Object requireTable() {
        final Object table = this.table;
        Objects.requireNonNull(table);
        return table;
    }
    
    private void resizeMeMaybe(int min) {
        final int length = this.requireEntries().length;
        if (min > length) {
            min = Math.min(1073741823, Math.max(1, length >>> 1) + length | 0x1);
            if (min != length) {
                this.resizeEntries(min);
            }
        }
    }
    
    private int resizeTable(final int n, int i, int j, int n2) {
        final Object a = ii.a(i);
        final int hashTableMask = i - 1;
        if (n2 != 0) {
            ii.i(a, j & hashTableMask, n2 + 1);
        }
        final Object requireTable = this.requireTable();
        final int[] requireEntries = this.requireEntries();
        int n3;
        int n4;
        int n5;
        int h;
        for (i = 0; i <= n; ++i) {
            for (j = ii.h(requireTable, i); j != 0; j = ii.c(n4, n)) {
                n3 = j - 1;
                n4 = requireEntries[n3];
                n2 = (ii.b(n4, n) | i);
                n5 = (n2 & hashTableMask);
                h = ii.h(a, n5);
                ii.i(a, n5, j);
                requireEntries[n3] = ii.d(n2, h, hashTableMask);
            }
        }
        this.table = a;
        this.setHashTableMask(hashTableMask);
        return hashTableMask;
    }
    
    private void setElement(final int n, final E e) {
        this.requireElements()[n] = e;
    }
    
    private void setEntry(final int n, final int n2) {
        this.requireEntries()[n] = n2;
    }
    
    private void setHashTableMask(int numberOfLeadingZeros) {
        numberOfLeadingZeros = Integer.numberOfLeadingZeros(numberOfLeadingZeros);
        this.metadata = ii.d(this.metadata, 32 - numberOfLeadingZeros, 31);
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeInt(this.size());
        final Iterator<E> iterator = this.iterator();
        while (iterator.hasNext()) {
            objectOutputStream.writeObject(iterator.next());
        }
    }
    
    @Override
    public boolean add(final E e) {
        if (this.needsAllocArrays()) {
            this.allocArrays();
        }
        final Set<E> delegateOrNull = this.delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.add(e);
        }
        final int[] requireEntries = this.requireEntries();
        final Object[] requireElements = this.requireElements();
        final int size = this.size;
        final int size2 = size + 1;
        final int d = rc0.d(e);
        final int hashTableMask = this.hashTableMask();
        final int n = d & hashTableMask;
        int n2 = ii.h(this.requireTable(), n);
        int resizeTable = 0;
        Label_0237: {
            if (n2 == 0) {
                if (size2 <= hashTableMask) {
                    ii.i(this.requireTable(), n, size2);
                    resizeTable = hashTableMask;
                    break Label_0237;
                }
            }
            else {
                final int b = ii.b(d, hashTableMask);
                int n3 = 0;
                while (true) {
                    final int n4 = n2 - 1;
                    final int n5 = requireEntries[n4];
                    if (ii.b(n5, hashTableMask) == b && b11.a(e, requireElements[n4])) {
                        return false;
                    }
                    n2 = ii.c(n5, hashTableMask);
                    ++n3;
                    if (n2 != 0) {
                        continue;
                    }
                    if (n3 >= 9) {
                        return this.convertToHashFloodingResistantImplementation().add(e);
                    }
                    if (size2 > hashTableMask) {
                        break;
                    }
                    requireEntries[n4] = ii.d(n5, size2, hashTableMask);
                    resizeTable = hashTableMask;
                    break Label_0237;
                }
            }
            resizeTable = this.resizeTable(hashTableMask, ii.e(hashTableMask), d, size);
        }
        this.resizeMeMaybe(size2);
        this.insertEntry(size, e, d, resizeTable);
        this.size = size2;
        this.incrementModCount();
        return true;
    }
    
    public int adjustAfterRemove(final int n, final int n2) {
        return n - 1;
    }
    
    public int allocArrays() {
        i71.y(this.needsAllocArrays(), "Arrays already allocated");
        final int metadata = this.metadata;
        final int j = ii.j(metadata);
        this.table = ii.a(j);
        this.setHashTableMask(j - 1);
        this.entries = new int[metadata];
        this.elements = new Object[metadata];
        return metadata;
    }
    
    @Override
    public void clear() {
        if (this.needsAllocArrays()) {
            return;
        }
        this.incrementModCount();
        final Set<E> delegateOrNull = this.delegateOrNull();
        if (delegateOrNull != null) {
            this.metadata = Ints.e(this.size(), 3, 1073741823);
            delegateOrNull.clear();
            this.table = null;
        }
        else {
            Arrays.fill(this.requireElements(), 0, this.size, null);
            ii.g(this.requireTable());
            Arrays.fill(this.requireEntries(), 0, this.size, 0);
        }
        this.size = 0;
    }
    
    @Override
    public boolean contains(final Object o) {
        if (this.needsAllocArrays()) {
            return false;
        }
        final Set<E> delegateOrNull = this.delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.contains(o);
        }
        final int d = rc0.d(o);
        final int hashTableMask = this.hashTableMask();
        int n = ii.h(this.requireTable(), d & hashTableMask);
        if (n == 0) {
            return false;
        }
        final int b = ii.b(d, hashTableMask);
        int entry;
        do {
            --n;
            entry = this.entry(n);
            if (ii.b(entry, hashTableMask) == b && b11.a(o, this.element(n))) {
                return true;
            }
        } while ((n = ii.c(entry, hashTableMask)) != 0);
        return false;
    }
    
    public Set<E> convertToHashFloodingResistantImplementation() {
        final Set<E> hashFloodingResistantDelegate = this.createHashFloodingResistantDelegate(this.hashTableMask() + 1);
        for (int i = this.firstEntryIndex(); i >= 0; i = this.getSuccessor(i)) {
            hashFloodingResistantDelegate.add(this.element(i));
        }
        this.table = hashFloodingResistantDelegate;
        this.entries = null;
        this.elements = null;
        this.incrementModCount();
        return hashFloodingResistantDelegate;
    }
    
    public Set<E> delegateOrNull() {
        final Object table = this.table;
        if (table instanceof Set) {
            return (Set<E>)table;
        }
        return null;
    }
    
    public int firstEntryIndex() {
        int n;
        if (this.isEmpty()) {
            n = -1;
        }
        else {
            n = 0;
        }
        return n;
    }
    
    public int getSuccessor(int n) {
        if (++n >= this.size) {
            n = -1;
        }
        return n;
    }
    
    public void incrementModCount() {
        this.metadata += 32;
    }
    
    public void init(final int n) {
        i71.e(n >= 0, "Expected size must be >= 0");
        this.metadata = Ints.e(n, 1, 1073741823);
    }
    
    public void insertEntry(final int n, final E e, final int n2, final int n3) {
        this.setEntry(n, ii.d(n2, 0, n3));
        this.setElement(n, e);
    }
    
    @Override
    public boolean isEmpty() {
        return this.size() == 0;
    }
    
    public boolean isUsingHashFloodingResistance() {
        return this.delegateOrNull() != null;
    }
    
    @Override
    public Iterator<E> iterator() {
        final Set<E> delegateOrNull = this.delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.iterator();
        }
        return new Iterator(this) {
            public int a = CompactHashSet.access$000(d);
            public int b = d.firstEntryIndex();
            public int c = -1;
            public final CompactHashSet d;
            
            public final void b() {
                if (CompactHashSet.access$000(this.d) == this.a) {
                    return;
                }
                throw new ConcurrentModificationException();
            }
            
            public void c() {
                this.a += 32;
            }
            
            @Override
            public boolean hasNext() {
                return this.b >= 0;
            }
            
            @Override
            public Object next() {
                this.b();
                if (this.hasNext()) {
                    final int b = this.b;
                    this.c = b;
                    final Object access$100 = this.d.element(b);
                    this.b = this.d.getSuccessor(this.b);
                    return access$100;
                }
                throw new NoSuchElementException();
            }
            
            @Override
            public void remove() {
                this.b();
                hh.e(this.c >= 0);
                this.c();
                final CompactHashSet d = this.d;
                d.remove(d.element(this.c));
                this.b = this.d.adjustAfterRemove(this.b, this.c);
                this.c = -1;
            }
        };
    }
    
    public void moveLastEntry(final int n, final int n2) {
        final Object requireTable = this.requireTable();
        final int[] requireEntries = this.requireEntries();
        final Object[] requireElements = this.requireElements();
        final int n3 = this.size() - 1;
        if (n < n3) {
            final Object o = requireElements[n3];
            requireElements[n] = o;
            requireElements[n3] = null;
            requireEntries[n] = requireEntries[n3];
            requireEntries[n3] = 0;
            final int n4 = rc0.d(o) & n2;
            final int h = ii.h(requireTable, n4);
            final int n5 = n3 + 1;
            int c;
            if ((c = h) == n5) {
                ii.i(requireTable, n4, n + 1);
            }
            else {
                int n6;
                int n7;
                do {
                    n6 = c - 1;
                    n7 = requireEntries[n6];
                    c = ii.c(n7, n2);
                } while (c != n5);
                requireEntries[n6] = ii.d(n7, n + 1, n2);
            }
        }
        else {
            requireElements[n] = null;
            requireEntries[n] = 0;
        }
    }
    
    public boolean needsAllocArrays() {
        return this.table == null;
    }
    
    @Override
    public boolean remove(final Object o) {
        if (this.needsAllocArrays()) {
            return false;
        }
        final Set<E> delegateOrNull = this.delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.remove(o);
        }
        final int hashTableMask = this.hashTableMask();
        final int f = ii.f(o, null, hashTableMask, this.requireTable(), this.requireEntries(), this.requireElements(), null);
        if (f == -1) {
            return false;
        }
        this.moveLastEntry(f, hashTableMask);
        --this.size;
        this.incrementModCount();
        return true;
    }
    
    public void resizeEntries(final int n) {
        this.entries = Arrays.copyOf(this.requireEntries(), n);
        this.elements = Arrays.copyOf(this.requireElements(), n);
    }
    
    @Override
    public int size() {
        final Set<E> delegateOrNull = this.delegateOrNull();
        int n;
        if (delegateOrNull != null) {
            n = delegateOrNull.size();
        }
        else {
            n = this.size;
        }
        return n;
    }
    
    @Override
    public Object[] toArray() {
        if (this.needsAllocArrays()) {
            return new Object[0];
        }
        final Set<E> delegateOrNull = this.delegateOrNull();
        Object[] array;
        if (delegateOrNull != null) {
            array = delegateOrNull.toArray();
        }
        else {
            array = Arrays.copyOf(this.requireElements(), this.size);
        }
        return array;
    }
    
    @Override
    public <T> T[] toArray(final T[] array) {
        if (this.needsAllocArrays()) {
            if (array.length > 0) {
                array[0] = null;
            }
            return array;
        }
        final Set<E> delegateOrNull = this.delegateOrNull();
        Object[] array2;
        if (delegateOrNull != null) {
            array2 = delegateOrNull.toArray(array);
        }
        else {
            array2 = t01.h(this.requireElements(), 0, this.size, array);
        }
        return (T[])array2;
    }
    
    public void trimToSize() {
        if (this.needsAllocArrays()) {
            return;
        }
        final Set<E> delegateOrNull = (Set<E>)this.delegateOrNull();
        if (delegateOrNull != null) {
            final Set<E> hashFloodingResistantDelegate = this.createHashFloodingResistantDelegate(this.size());
            hashFloodingResistantDelegate.addAll((Collection<? extends E>)delegateOrNull);
            this.table = hashFloodingResistantDelegate;
            return;
        }
        final int size = this.size;
        if (size < this.requireEntries().length) {
            this.resizeEntries(size);
        }
        final int j = ii.j(size);
        final int hashTableMask = this.hashTableMask();
        if (j < hashTableMask) {
            this.resizeTable(hashTableMask, j, 0, 0);
        }
    }
}

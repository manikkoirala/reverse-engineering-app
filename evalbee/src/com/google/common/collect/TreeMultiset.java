// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.ConcurrentModificationException;
import com.google.common.base.a;
import java.util.Set;
import java.util.NavigableSet;
import com.google.common.primitives.Ints;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Iterator;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.util.Collection;
import java.util.Comparator;
import java.io.Serializable;

public final class TreeMultiset<E> extends d implements Serializable
{
    private static final long serialVersionUID = 1L;
    private final transient e header;
    private final transient GeneralRange<E> range;
    private final transient f rootReference;
    
    public TreeMultiset(final f rootReference, final GeneralRange<E> range, final e header) {
        super(range.comparator());
        this.rootReference = rootReference;
        this.range = range;
        this.header = header;
    }
    
    public TreeMultiset(final Comparator<? super E> comparator) {
        super(comparator);
        this.range = GeneralRange.all(comparator);
        final e header = new e();
        successor(this.header = header, header);
        this.rootReference = new f(null);
    }
    
    public static /* synthetic */ GeneralRange access$1400(final TreeMultiset treeMultiset) {
        return treeMultiset.range;
    }
    
    public static /* synthetic */ e access$1600(final TreeMultiset treeMultiset) {
        return treeMultiset.header;
    }
    
    private long aggregateAboveRange(final Aggregate aggregate, final e e) {
        if (e == null) {
            return 0L;
        }
        final int compare = this.comparator().compare(k01.a(this.range.getUpperEndpoint()), e.x());
        if (compare > 0) {
            return this.aggregateAboveRange(aggregate, TreeMultiset.e.j(e));
        }
        long n2;
        long n3;
        if (compare == 0) {
            final int n = TreeMultiset$d.a[this.range.getUpperBoundType().ordinal()];
            if (n != 1) {
                if (n == 2) {
                    return aggregate.treeAggregate(TreeMultiset.e.j(e));
                }
                throw new AssertionError();
            }
            else {
                n2 = aggregate.nodeAggregate(e);
                n3 = aggregate.treeAggregate(TreeMultiset.e.j(e));
            }
        }
        else {
            n2 = aggregate.treeAggregate(TreeMultiset.e.j(e)) + aggregate.nodeAggregate(e);
            n3 = this.aggregateAboveRange(aggregate, TreeMultiset.e.h(e));
        }
        return n2 + n3;
    }
    
    private long aggregateBelowRange(final Aggregate aggregate, final e e) {
        if (e == null) {
            return 0L;
        }
        final int compare = this.comparator().compare(k01.a(this.range.getLowerEndpoint()), e.x());
        if (compare < 0) {
            return this.aggregateBelowRange(aggregate, TreeMultiset.e.h(e));
        }
        long n2;
        long n3;
        if (compare == 0) {
            final int n = TreeMultiset$d.a[this.range.getLowerBoundType().ordinal()];
            if (n != 1) {
                if (n == 2) {
                    return aggregate.treeAggregate(TreeMultiset.e.h(e));
                }
                throw new AssertionError();
            }
            else {
                n2 = aggregate.nodeAggregate(e);
                n3 = aggregate.treeAggregate(TreeMultiset.e.h(e));
            }
        }
        else {
            n2 = aggregate.treeAggregate(TreeMultiset.e.h(e)) + aggregate.nodeAggregate(e);
            n3 = this.aggregateBelowRange(aggregate, TreeMultiset.e.j(e));
        }
        return n2 + n3;
    }
    
    private long aggregateForEntries(final Aggregate aggregate) {
        final e e = (e)this.rootReference.c();
        long treeAggregate;
        final long n = treeAggregate = aggregate.treeAggregate(e);
        if (this.range.hasLowerBound()) {
            treeAggregate = n - this.aggregateBelowRange(aggregate, e);
        }
        long n2 = treeAggregate;
        if (this.range.hasUpperBound()) {
            n2 = treeAggregate - this.aggregateAboveRange(aggregate, e);
        }
        return n2;
    }
    
    public static <E extends Comparable> TreeMultiset<E> create() {
        return new TreeMultiset<E>(Ordering.natural());
    }
    
    public static <E extends Comparable> TreeMultiset<E> create(final Iterable<? extends E> iterable) {
        final TreeMultiset<Comparable> create = create();
        rg0.a(create, iterable);
        return (TreeMultiset<E>)create;
    }
    
    public static <E> TreeMultiset<E> create(final Comparator<? super E> comparator) {
        TreeMultiset treeMultiset;
        if (comparator == null) {
            treeMultiset = new TreeMultiset(Ordering.natural());
        }
        else {
            treeMultiset = new TreeMultiset((Comparator<? super E>)comparator);
        }
        return treeMultiset;
    }
    
    public static int distinctElements(final e e) {
        int g;
        if (e == null) {
            g = 0;
        }
        else {
            g = TreeMultiset.e.g(e);
        }
        return g;
    }
    
    private e firstNode() {
        final e e = (e)this.rootReference.c();
        final e e2 = null;
        if (e == null) {
            return null;
        }
        e l = null;
        Label_0106: {
            e e3;
            if (this.range.hasLowerBound()) {
                final Object a = k01.a(this.range.getLowerEndpoint());
                e3 = e.s(this.comparator(), a);
                if (e3 == null) {
                    return null;
                }
                l = e3;
                if (this.range.getLowerBoundType() != BoundType.OPEN) {
                    break Label_0106;
                }
                l = e3;
                if (this.comparator().compare(a, e3.x()) != 0) {
                    break Label_0106;
                }
            }
            else {
                e3 = this.header;
            }
            l = e3.L();
        }
        e e4 = e2;
        if (l != this.header) {
            if (!this.range.contains((E)l.x())) {
                e4 = e2;
            }
            else {
                e4 = l;
            }
        }
        return e4;
    }
    
    private e lastNode() {
        final e e = (e)this.rootReference.c();
        final e e2 = null;
        if (e == null) {
            return null;
        }
        e c = null;
        Label_0106: {
            e e3;
            if (this.range.hasUpperBound()) {
                final Object a = k01.a(this.range.getUpperEndpoint());
                e3 = e.v(this.comparator(), a);
                if (e3 == null) {
                    return null;
                }
                c = e3;
                if (this.range.getUpperBoundType() != BoundType.OPEN) {
                    break Label_0106;
                }
                c = e3;
                if (this.comparator().compare(a, e3.x()) != 0) {
                    break Label_0106;
                }
            }
            else {
                e3 = this.header;
            }
            c = e3.z();
        }
        e e4 = e2;
        if (c != this.header) {
            if (!this.range.contains((E)c.x())) {
                e4 = e2;
            }
            else {
                e4 = c;
            }
        }
        return e4;
    }
    
    private void readObject(final ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        final Comparator comparator = (Comparator)objectInputStream.readObject();
        n.a(d.class, "comparator").b(this, comparator);
        n.a(TreeMultiset.class, "range").b(this, GeneralRange.all(comparator));
        n.a(TreeMultiset.class, "rootReference").b(this, new f(null));
        final e e = new e();
        n.a(TreeMultiset.class, "header").b(this, e);
        successor(e, e);
        n.f(this, objectInputStream);
    }
    
    private static <T> void successor(final e e, final e e2) {
        TreeMultiset.e.n(e, e2);
        TreeMultiset.e.m(e2, e);
    }
    
    private static <T> void successor(final e e, final e e2, final e e3) {
        successor(e, e2);
        successor(e2, e3);
    }
    
    private j.a wrapEntry(final e e) {
        return new Multisets.b(this, e) {
            public final TreeMultiset.e a;
            public final TreeMultiset b;
            
            @Override
            public int getCount() {
                int n;
                if ((n = this.a.w()) == 0) {
                    n = this.b.count(this.getElement());
                }
                return n;
            }
            
            @Override
            public Object getElement() {
                return this.a.x();
            }
        };
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(this.elementSet().comparator());
        n.k(this, objectOutputStream);
    }
    
    @Override
    public int add(final E e, final int n) {
        hh.b(n, "occurrences");
        if (n == 0) {
            return this.count(e);
        }
        i71.d(this.range.contains(e));
        final e e2 = (e)this.rootReference.c();
        if (e2 == null) {
            this.comparator().compare(e, e);
            final e e3 = new e(e, n);
            final e header = this.header;
            successor(header, e3, header);
            this.rootReference.a(e2, e3);
            return 0;
        }
        final int[] array = { 0 };
        this.rootReference.a(e2, e2.o(this.comparator(), e, n, array));
        return array[0];
    }
    
    @Override
    public void clear() {
        if (!this.range.hasLowerBound() && !this.range.hasUpperBound()) {
            e l = this.header.L();
            e header;
            while (true) {
                header = this.header;
                if (l == header) {
                    break;
                }
                final e i = l.L();
                e.e(l, 0);
                e.i(l, null);
                e.k(l, null);
                e.m(l, null);
                e.n(l, null);
                l = i;
            }
            successor(header, header);
            this.rootReference.b();
        }
        else {
            Iterators.d(this.entryIterator());
        }
    }
    
    @Override
    public int count(final Object o) {
        try {
            final e e = (e)this.rootReference.c();
            if (this.range.contains((E)o)) {
                if (e != null) {
                    return e.t(this.comparator(), o);
                }
            }
            return 0;
        }
        catch (final ClassCastException | NullPointerException ex) {
            return 0;
        }
    }
    
    @Override
    public Iterator<j.a> descendingEntryIterator() {
        return new Iterator(this) {
            public e a = c.lastNode();
            public j.a b = null;
            public final TreeMultiset c;
            
            public j.a b() {
                if (this.hasNext()) {
                    Objects.requireNonNull(this.a);
                    final j.a access$1500 = this.c.wrapEntry(this.a);
                    this.b = access$1500;
                    Object c;
                    if (this.a.z() == TreeMultiset.access$1600(this.c)) {
                        c = null;
                    }
                    else {
                        c = this.a.z();
                    }
                    this.a = (e)c;
                    return access$1500;
                }
                throw new NoSuchElementException();
            }
            
            @Override
            public boolean hasNext() {
                if (this.a == null) {
                    return false;
                }
                if (TreeMultiset.access$1400(this.c).tooLow(this.a.x())) {
                    this.a = null;
                    return false;
                }
                return true;
            }
            
            @Override
            public void remove() {
                i71.y(this.b != null, "no calls to next() since the last call to remove()");
                this.c.setCount(this.b.getElement(), 0);
                this.b = null;
            }
        };
    }
    
    @Override
    public int distinctElements() {
        return Ints.k(this.aggregateForEntries(Aggregate.DISTINCT));
    }
    
    @Override
    public Iterator<E> elementIterator() {
        return Multisets.e(this.entryIterator());
    }
    
    public Iterator<j.a> entryIterator() {
        return new Iterator(this) {
            public e a = c.firstNode();
            public j.a b;
            public final TreeMultiset c;
            
            public j.a b() {
                if (this.hasNext()) {
                    final TreeMultiset c = this.c;
                    final e a = this.a;
                    Objects.requireNonNull(a);
                    final j.a access$1500 = c.wrapEntry(a);
                    this.b = access$1500;
                    Object l;
                    if (this.a.L() == TreeMultiset.access$1600(this.c)) {
                        l = null;
                    }
                    else {
                        l = this.a.L();
                    }
                    this.a = (e)l;
                    return access$1500;
                }
                throw new NoSuchElementException();
            }
            
            @Override
            public boolean hasNext() {
                if (this.a == null) {
                    return false;
                }
                if (TreeMultiset.access$1400(this.c).tooHigh(this.a.x())) {
                    this.a = null;
                    return false;
                }
                return true;
            }
            
            @Override
            public void remove() {
                i71.y(this.b != null, "no calls to next() since the last call to remove()");
                this.c.setCount(this.b.getElement(), 0);
                this.b = null;
            }
        };
    }
    
    @Override
    public o headMultiset(final E e, final BoundType boundType) {
        return new TreeMultiset<Object>(this.rootReference, this.range.intersect(GeneralRange.upTo(this.comparator(), e, boundType)), this.header);
    }
    
    @Override
    public Iterator<E> iterator() {
        return Multisets.i(this);
    }
    
    @Override
    public int remove(final Object o, final int n) {
        hh.b(n, "occurrences");
        if (n == 0) {
            return this.count(o);
        }
        final e e = (e)this.rootReference.c();
        final int[] array = { 0 };
        try {
            if (this.range.contains((E)o)) {
                if (e != null) {
                    this.rootReference.a(e, e.E(this.comparator(), o, n, array));
                    return array[0];
                }
            }
            return 0;
        }
        catch (final ClassCastException | NullPointerException ex) {
            return 0;
        }
    }
    
    @Override
    public int setCount(final E e, final int n) {
        hh.b(n, "count");
        final boolean contains = this.range.contains(e);
        boolean b = true;
        if (!contains) {
            if (n != 0) {
                b = false;
            }
            i71.d(b);
            return 0;
        }
        final e e2 = (e)this.rootReference.c();
        if (e2 == null) {
            if (n > 0) {
                this.add(e, n);
            }
            return 0;
        }
        final int[] array = { 0 };
        this.rootReference.a(e2, e2.K(this.comparator(), e, n, array));
        return array[0];
    }
    
    @Override
    public boolean setCount(final E e, final int n, final int n2) {
        hh.b(n2, "newCount");
        hh.b(n, "oldCount");
        i71.d(this.range.contains(e));
        final e e2 = (e)this.rootReference.c();
        boolean b = false;
        if (e2 != null) {
            final int[] array = { 0 };
            this.rootReference.a(e2, e2.J(this.comparator(), e, n, n2, array));
            if (array[0] == n) {
                b = true;
            }
            return b;
        }
        if (n == 0) {
            if (n2 > 0) {
                this.add(e, n2);
            }
            return true;
        }
        return false;
    }
    
    @Override
    public int size() {
        return Ints.k(this.aggregateForEntries(Aggregate.SIZE));
    }
    
    @Override
    public o tailMultiset(final E e, final BoundType boundType) {
        return new TreeMultiset<Object>(this.rootReference, this.range.intersect(GeneralRange.downTo(this.comparator(), e, boundType)), this.header);
    }
    
    public enum Aggregate
    {
        private static final Aggregate[] $VALUES;
        
        DISTINCT {
            @Override
            public int nodeAggregate(final e e) {
                return 1;
            }
            
            @Override
            public long treeAggregate(final e e) {
                long n;
                if (e == null) {
                    n = 0L;
                }
                else {
                    n = TreeMultiset.e.g(e);
                }
                return n;
            }
        }, 
        SIZE {
            @Override
            public int nodeAggregate(final e e) {
                return TreeMultiset.e.d(e);
            }
            
            @Override
            public long treeAggregate(final e e) {
                long f;
                if (e == null) {
                    f = 0L;
                }
                else {
                    f = TreeMultiset.e.f(e);
                }
                return f;
            }
        };
        
        private static /* synthetic */ Aggregate[] $values() {
            return new Aggregate[] { Aggregate.SIZE, Aggregate.DISTINCT };
        }
        
        static {
            $VALUES = $values();
        }
        
        public abstract int nodeAggregate(final e p0);
        
        public abstract long treeAggregate(final e p0);
    }
    
    public static final class e
    {
        public final Object a;
        public int b;
        public int c;
        public long d;
        public int e;
        public e f;
        public e g;
        public e h;
        public e i;
        
        public e() {
            this.a = null;
            this.b = 1;
        }
        
        public e(final Object a, final int b) {
            i71.d(b > 0);
            this.a = a;
            this.b = b;
            this.d = b;
            this.c = 1;
            this.e = 1;
            this.f = null;
            this.g = null;
        }
        
        public static long M(final e e) {
            long d;
            if (e == null) {
                d = 0L;
            }
            else {
                d = e.d;
            }
            return d;
        }
        
        public static /* synthetic */ int d(final e e) {
            return e.b;
        }
        
        public static /* synthetic */ int e(final e e, final int b) {
            return e.b = b;
        }
        
        public static /* synthetic */ long f(final e e) {
            return e.d;
        }
        
        public static /* synthetic */ int g(final e e) {
            return e.c;
        }
        
        public static /* synthetic */ e h(final e e) {
            return e.f;
        }
        
        public static /* synthetic */ e i(final e e, final e f) {
            return e.f = f;
        }
        
        public static /* synthetic */ e j(final e e) {
            return e.g;
        }
        
        public static /* synthetic */ e k(final e e, final e g) {
            return e.g = g;
        }
        
        public static /* synthetic */ e m(final e e, final e h) {
            return e.h = h;
        }
        
        public static /* synthetic */ e n(final e e, final e i) {
            return e.i = i;
        }
        
        public static int y(final e e) {
            int e2;
            if (e == null) {
                e2 = 0;
            }
            else {
                e2 = e.e;
            }
            return e2;
        }
        
        public final e A() {
            final int r = this.r();
            if (r == -2) {
                Objects.requireNonNull(this.g);
                if (this.g.r() > 0) {
                    this.g = this.g.I();
                }
                return this.H();
            }
            if (r != 2) {
                this.C();
                return this;
            }
            Objects.requireNonNull(this.f);
            if (this.f.r() < 0) {
                this.f = this.f.H();
            }
            return this.I();
        }
        
        public final void B() {
            this.D();
            this.C();
        }
        
        public final void C() {
            this.e = Math.max(y(this.f), y(this.g)) + 1;
        }
        
        public final void D() {
            this.c = TreeMultiset.distinctElements(this.f) + 1 + TreeMultiset.distinctElements(this.g);
            this.d = this.b + M(this.f) + M(this.g);
        }
        
        public e E(final Comparator comparator, final Object o, final int n, final int[] array) {
            final int compare = comparator.compare(o, this.x());
            if (compare < 0) {
                final e f = this.f;
                if (f == null) {
                    array[0] = 0;
                    return this;
                }
                this.f = f.E(comparator, o, n, array);
                final int n2 = array[0];
                if (n2 > 0) {
                    if (n >= n2) {
                        --this.c;
                        this.d -= n2;
                    }
                    else {
                        this.d -= n;
                    }
                }
                e a;
                if (n2 == 0) {
                    a = this;
                }
                else {
                    a = this.A();
                }
                return a;
            }
            else if (compare > 0) {
                final e g = this.g;
                if (g == null) {
                    array[0] = 0;
                    return this;
                }
                this.g = g.E(comparator, o, n, array);
                final int n3 = array[0];
                if (n3 > 0) {
                    if (n >= n3) {
                        --this.c;
                        this.d -= n3;
                    }
                    else {
                        this.d -= n;
                    }
                }
                return this.A();
            }
            else {
                final int b = this.b;
                if (n >= (array[0] = b)) {
                    return this.u();
                }
                this.b = b - n;
                this.d -= n;
                return this;
            }
        }
        
        public final e F(final e e) {
            final e g = this.g;
            if (g == null) {
                return this.f;
            }
            this.g = g.F(e);
            --this.c;
            this.d -= e.b;
            return this.A();
        }
        
        public final e G(final e e) {
            final e f = this.f;
            if (f == null) {
                return this.g;
            }
            this.f = f.G(e);
            --this.c;
            this.d -= e.b;
            return this.A();
        }
        
        public final e H() {
            i71.x(this.g != null);
            final e g = this.g;
            this.g = g.f;
            g.f = this;
            g.d = this.d;
            g.c = this.c;
            this.B();
            g.C();
            return g;
        }
        
        public final e I() {
            i71.x(this.f != null);
            final e f = this.f;
            this.f = f.g;
            f.g = this;
            f.d = this.d;
            f.c = this.c;
            this.B();
            f.C();
            return f;
        }
        
        public e J(final Comparator comparator, final Object o, int n, final int b, final int[] array) {
            final int compare = comparator.compare(o, this.x());
            if (compare < 0) {
                final e f = this.f;
                if (f != null) {
                    this.f = f.J(comparator, o, n, b, array);
                    final int n2 = array[0];
                    if (n2 == n) {
                        Label_0126: {
                            if (b == 0 && n2 != 0) {
                                n = this.c - 1;
                            }
                            else {
                                if (b <= 0 || n2 != 0) {
                                    break Label_0126;
                                }
                                n = this.c + 1;
                            }
                            this.c = n;
                        }
                        this.d += b - n2;
                    }
                    return this.A();
                }
                array[0] = 0;
                if (n == 0 && b > 0) {
                    return this.p(o, b);
                }
                return this;
            }
            else {
                if (compare <= 0) {
                    final int b2 = this.b;
                    if (n == (array[0] = b2)) {
                        if (b == 0) {
                            return this.u();
                        }
                        this.d += b - b2;
                        this.b = b;
                    }
                    return this;
                }
                final e g = this.g;
                if (g != null) {
                    this.g = g.J(comparator, o, n, b, array);
                    final int n3 = array[0];
                    if (n3 == n) {
                        Label_0259: {
                            if (b == 0 && n3 != 0) {
                                n = this.c - 1;
                            }
                            else {
                                if (b <= 0 || n3 != 0) {
                                    break Label_0259;
                                }
                                n = this.c + 1;
                            }
                            this.c = n;
                        }
                        this.d += b - n3;
                    }
                    return this.A();
                }
                array[0] = 0;
                if (n == 0 && b > 0) {
                    return this.q(o, b);
                }
                return this;
            }
        }
        
        public e K(final Comparator comparator, final Object o, final int b, final int[] array) {
            final int compare = comparator.compare(o, this.x());
            long n;
            int n2;
            if (compare < 0) {
                final e f = this.f;
                if (f == null) {
                    array[0] = 0;
                    e p4;
                    if (b > 0) {
                        p4 = this.p(o, b);
                    }
                    else {
                        p4 = this;
                    }
                    return p4;
                }
                this.f = f.K(comparator, o, b, array);
                Label_0116: {
                    int c;
                    if (b == 0 && array[0] != 0) {
                        c = this.c - 1;
                    }
                    else {
                        if (b <= 0 || array[0] != 0) {
                            break Label_0116;
                        }
                        c = this.c + 1;
                    }
                    this.c = c;
                }
                n = this.d;
                n2 = array[0];
            }
            else if (compare > 0) {
                final e g = this.g;
                if (g == null) {
                    array[0] = 0;
                    e q;
                    if (b > 0) {
                        q = this.q(o, b);
                    }
                    else {
                        q = this;
                    }
                    return q;
                }
                this.g = g.K(comparator, o, b, array);
                Label_0248: {
                    int c2;
                    if (b == 0 && array[0] != 0) {
                        c2 = this.c - 1;
                    }
                    else {
                        if (b <= 0 || array[0] != 0) {
                            break Label_0248;
                        }
                        c2 = this.c + 1;
                    }
                    this.c = c2;
                }
                n = this.d;
                n2 = array[0];
            }
            else {
                final int b2 = this.b;
                array[0] = b2;
                if (b == 0) {
                    return this.u();
                }
                this.d += b - b2;
                this.b = b;
                return this;
            }
            this.d = n + (b - n2);
            return this.A();
        }
        
        public final e L() {
            final e i = this.i;
            Objects.requireNonNull(i);
            return i;
        }
        
        public e o(final Comparator comparator, final Object o, final int n, final int[] array) {
            final int compare = comparator.compare(o, this.x());
            boolean b = true;
            if (compare < 0) {
                final e f = this.f;
                if (f == null) {
                    array[0] = 0;
                    return this.p(o, n);
                }
                final int e = f.e;
                final e o2 = f.o(comparator, o, n, array);
                this.f = o2;
                if (array[0] == 0) {
                    ++this.c;
                }
                this.d += n;
                e a;
                if (o2.e == e) {
                    a = this;
                }
                else {
                    a = this.A();
                }
                return a;
            }
            else {
                if (compare <= 0) {
                    final int b2 = this.b;
                    array[0] = b2;
                    final long n2 = b2;
                    final long n3 = n;
                    if (n2 + n3 > 2147483647L) {
                        b = false;
                    }
                    i71.d(b);
                    this.b += n;
                    this.d += n3;
                    return this;
                }
                final e g = this.g;
                if (g == null) {
                    array[0] = 0;
                    return this.q(o, n);
                }
                final int e2 = g.e;
                final e o3 = g.o(comparator, o, n, array);
                this.g = o3;
                if (array[0] == 0) {
                    ++this.c;
                }
                this.d += n;
                e a2;
                if (o3.e == e2) {
                    a2 = this;
                }
                else {
                    a2 = this.A();
                }
                return a2;
            }
        }
        
        public final e p(final Object o, final int n) {
            this.f = new e(o, n);
            successor(this.z(), this.f, this);
            this.e = Math.max(2, this.e);
            ++this.c;
            this.d += n;
            return this;
        }
        
        public final e q(final Object o, final int n) {
            successor(this, this.g = new e(o, n), this.L());
            this.e = Math.max(2, this.e);
            ++this.c;
            this.d += n;
            return this;
        }
        
        public final int r() {
            return y(this.f) - y(this.g);
        }
        
        public final e s(final Comparator comparator, final Object o) {
            final int compare = comparator.compare(o, this.x());
            if (compare < 0) {
                final e f = this.f;
                e e;
                if (f == null) {
                    e = this;
                }
                else {
                    e = (e)com.google.common.base.a.a(f.s(comparator, o), this);
                }
                return e;
            }
            if (compare == 0) {
                return this;
            }
            final e g = this.g;
            e s;
            if (g == null) {
                s = null;
            }
            else {
                s = g.s(comparator, o);
            }
            return s;
        }
        
        public int t(final Comparator comparator, final Object o) {
            final int compare = comparator.compare(o, this.x());
            int t = 0;
            final int n = 0;
            if (compare < 0) {
                final e f = this.f;
                int t2;
                if (f == null) {
                    t2 = n;
                }
                else {
                    t2 = f.t(comparator, o);
                }
                return t2;
            }
            if (compare > 0) {
                final e g = this.g;
                if (g != null) {
                    t = g.t(comparator, o);
                }
                return t;
            }
            return this.b;
        }
        
        @Override
        public String toString() {
            return Multisets.g(this.x(), this.w()).toString();
        }
        
        public final e u() {
            final int b = this.b;
            this.b = 0;
            successor(this.z(), this.L());
            final e f = this.f;
            if (f == null) {
                return this.g;
            }
            final e g = this.g;
            if (g == null) {
                return f;
            }
            e e;
            if (f.e >= g.e) {
                e = this.z();
                e.f = this.f.F(e);
                e.g = this.g;
            }
            else {
                e = this.L();
                e.g = this.g.G(e);
                e.f = this.f;
            }
            e.c = this.c - 1;
            e.d = this.d - b;
            return e.A();
        }
        
        public final e v(final Comparator comparator, final Object o) {
            final int compare = comparator.compare(o, this.x());
            if (compare > 0) {
                final e g = this.g;
                e e;
                if (g == null) {
                    e = this;
                }
                else {
                    e = (e)com.google.common.base.a.a(g.v(comparator, o), this);
                }
                return e;
            }
            if (compare == 0) {
                return this;
            }
            final e f = this.f;
            e v;
            if (f == null) {
                v = null;
            }
            else {
                v = f.v(comparator, o);
            }
            return v;
        }
        
        public int w() {
            return this.b;
        }
        
        public Object x() {
            return k01.a(this.a);
        }
        
        public final e z() {
            final e h = this.h;
            Objects.requireNonNull(h);
            return h;
        }
    }
    
    public static final class f
    {
        public Object a;
        
        public void a(final Object o, final Object a) {
            if (this.a == o) {
                this.a = a;
                return;
            }
            throw new ConcurrentModificationException();
        }
        
        public void b() {
            this.a = null;
        }
        
        public Object c() {
            return this.a;
        }
    }
}

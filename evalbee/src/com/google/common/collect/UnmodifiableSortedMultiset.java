// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.SortedSet;
import java.util.Collection;
import java.util.Set;
import java.util.NavigableSet;
import java.util.Comparator;

final class UnmodifiableSortedMultiset<E> extends UnmodifiableMultiset<E> implements o
{
    private static final long serialVersionUID = 0L;
    private transient UnmodifiableSortedMultiset<E> descendingMultiset;
    
    public UnmodifiableSortedMultiset(final o o) {
        super(o);
    }
    
    @Override
    public Comparator<? super E> comparator() {
        return this.delegate().comparator();
    }
    
    public NavigableSet<E> createElementSet() {
        return Sets.h(this.delegate().elementSet());
    }
    
    @Override
    public o delegate() {
        return (o)super.delegate();
    }
    
    @Override
    public o descendingMultiset() {
        UnmodifiableSortedMultiset<E> descendingMultiset;
        if ((descendingMultiset = this.descendingMultiset) == null) {
            descendingMultiset = new UnmodifiableSortedMultiset<E>(this.delegate().descendingMultiset());
            descendingMultiset.descendingMultiset = this;
            this.descendingMultiset = descendingMultiset;
        }
        return descendingMultiset;
    }
    
    @Override
    public NavigableSet<E> elementSet() {
        return (NavigableSet)super.elementSet();
    }
    
    @Override
    public a firstEntry() {
        return this.delegate().firstEntry();
    }
    
    @Override
    public o headMultiset(final E e, final BoundType boundType) {
        return Multisets.p(this.delegate().headMultiset(e, boundType));
    }
    
    @Override
    public a lastEntry() {
        return this.delegate().lastEntry();
    }
    
    @Override
    public a pollFirstEntry() {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public a pollLastEntry() {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public o subMultiset(final E e, final BoundType boundType, final E e2, final BoundType boundType2) {
        return Multisets.p(this.delegate().subMultiset(e, boundType, e2, boundType2));
    }
    
    @Override
    public o tailMultiset(final E e, final BoundType boundType) {
        return Multisets.p(this.delegate().tailMultiset(e, boundType));
    }
}

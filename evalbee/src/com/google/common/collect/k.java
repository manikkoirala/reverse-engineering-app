// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Arrays;

public class k
{
    public transient Object[] a;
    public transient int[] b;
    public transient int c;
    public transient int d;
    public transient int[] e;
    public transient long[] f;
    public transient float g;
    public transient int h;
    
    public k() {
        this.n(3, 1.0f);
    }
    
    public k(final int n) {
        this(n, 1.0f);
    }
    
    public k(final int n, final float n2) {
        this.n(n, n2);
    }
    
    public k(final k k) {
        this.n(k.C(), 1.0f);
        for (int i = k.e(); i != -1; i = k.s(i)) {
            this.u(k.i(i), k.k(i));
        }
    }
    
    public static long D(final long n, final int n2) {
        return (n & 0xFFFFFFFF00000000L) | (0xFFFFFFFFL & (long)n2);
    }
    
    public static k b() {
        return new k();
    }
    
    public static k c(final int n) {
        return new k(n);
    }
    
    public static int h(final long n) {
        return (int)(n >>> 32);
    }
    
    public static int j(final long n) {
        return (int)n;
    }
    
    public static long[] q(final int n) {
        final long[] a = new long[n];
        Arrays.fill(a, -1L);
        return a;
    }
    
    public static int[] r(final int n) {
        final int[] a = new int[n];
        Arrays.fill(a, -1);
        return a;
    }
    
    public final void A(int i) {
        if (this.e.length >= 1073741824) {
            this.h = Integer.MAX_VALUE;
            return;
        }
        final int n = (int)(i * this.g);
        final int[] r = r(i);
        final long[] f = this.f;
        final int length = r.length;
        int h;
        int n2;
        for (i = 0; i < this.c; ++i) {
            h = h(f[i]);
            n2 = (h & length - 1);
            f[r[n2] = i] = ((long)h << 32 | ((long)r[n2] & 0xFFFFFFFFL));
        }
        this.h = n + 1;
        this.e = r;
    }
    
    public void B(final int n, final int n2) {
        i71.p(n, this.c);
        this.b[n] = n2;
    }
    
    public int C() {
        return this.c;
    }
    
    public void a() {
        ++this.d;
        Arrays.fill(this.a, 0, this.c, null);
        Arrays.fill(this.b, 0, this.c, 0);
        Arrays.fill(this.e, -1);
        Arrays.fill(this.f, -1L);
        this.c = 0;
    }
    
    public void d(final int n) {
        if (n > this.f.length) {
            this.y(n);
        }
        if (n >= this.h) {
            this.A(Math.max(2, Integer.highestOneBit(n - 1) << 1));
        }
    }
    
    public int e() {
        int n;
        if (this.c == 0) {
            n = -1;
        }
        else {
            n = 0;
        }
        return n;
    }
    
    public int f(final Object o) {
        final int m = this.m(o);
        int n;
        if (m == -1) {
            n = 0;
        }
        else {
            n = this.b[m];
        }
        return n;
    }
    
    public j.a g(final int n) {
        i71.p(n, this.c);
        return new a(n);
    }
    
    public Object i(final int n) {
        i71.p(n, this.c);
        return this.a[n];
    }
    
    public int k(final int n) {
        i71.p(n, this.c);
        return this.b[n];
    }
    
    public final int l() {
        return this.e.length - 1;
    }
    
    public int m(final Object o) {
        final int d = rc0.d(o);
        long n;
        for (int i = this.e[this.l() & d]; i != -1; i = j(n)) {
            n = this.f[i];
            if (h(n) == d && b11.a(o, this.a[i])) {
                return i;
            }
        }
        return -1;
    }
    
    public void n(final int n, final float g) {
        final boolean b = false;
        i71.e(n >= 0, "Initial capacity must be non-negative");
        boolean b2 = b;
        if (g > 0.0f) {
            b2 = true;
        }
        i71.e(b2, "Illegal load factor");
        final int a = rc0.a(n, g);
        this.e = r(a);
        this.g = g;
        this.a = new Object[n];
        this.b = new int[n];
        this.f = q(n);
        this.h = Math.max(1, (int)(a * g));
    }
    
    public void o(final int n, final Object o, final int n2, final int n3) {
        this.f[n] = ((long)n3 << 32 | 0xFFFFFFFFL);
        this.a[n] = o;
        this.b[n] = n2;
    }
    
    public void p(final int n) {
        final int n2 = this.C() - 1;
        if (n < n2) {
            final Object[] a = this.a;
            a[n] = a[n2];
            final int[] b = this.b;
            b[n] = b[n2];
            a[n2] = null;
            b[n2] = 0;
            final long[] f = this.f;
            final long n3 = f[n2];
            f[n] = n3;
            f[n2] = -1L;
            final int n4 = h(n3) & this.l();
            final int[] e = this.e;
            int n5;
            if ((n5 = e[n4]) == n2) {
                e[n4] = n;
            }
            else {
                long n6;
                while (true) {
                    n6 = this.f[n5];
                    final int j = j(n6);
                    if (j == n2) {
                        break;
                    }
                    n5 = j;
                }
                this.f[n5] = D(n6, n);
            }
        }
        else {
            this.a[n] = null;
            this.b[n] = 0;
            this.f[n] = -1L;
        }
    }
    
    public int s(int n) {
        if (++n >= this.c) {
            n = -1;
        }
        return n;
    }
    
    public int t(final int n, final int n2) {
        return n - 1;
    }
    
    public int u(final Object o, final int n) {
        hh.d(n, "count");
        final long[] f = this.f;
        final Object[] a = this.a;
        final int[] b = this.b;
        final int d = rc0.d(o);
        final int n2 = this.l() & d;
        final int c = this.c;
        final int[] e = this.e;
        int n3;
        if ((n3 = e[n2]) == -1) {
            e[n2] = c;
        }
        else {
            while (true) {
                final long n4 = f[n3];
                if (h(n4) == d && b11.a(o, a[n3])) {
                    final int n5 = b[n3];
                    b[n3] = n;
                    return n5;
                }
                final int j = j(n4);
                if (j == -1) {
                    f[n3] = D(n4, c);
                    break;
                }
                n3 = j;
            }
        }
        if (c != Integer.MAX_VALUE) {
            final int c2 = c + 1;
            this.z(c2);
            this.o(c, o, n, d);
            this.c = c2;
            if (c >= this.h) {
                this.A(this.e.length * 2);
            }
            ++this.d;
            return 0;
        }
        throw new IllegalStateException("Cannot contain more than Integer.MAX_VALUE elements!");
    }
    
    public int v(final Object o) {
        return this.w(o, rc0.d(o));
    }
    
    public final int w(final Object o, int n) {
        final int n2 = this.l() & n;
        int n3 = this.e[n2];
        if (n3 == -1) {
            return 0;
        }
        int n4 = -1;
        while (h(this.f[n3]) != n || !b11.a(o, this.a[n3])) {
            final int j = j(this.f[n3]);
            if (j == -1) {
                return 0;
            }
            n4 = n3;
            n3 = j;
        }
        n = this.b[n3];
        if (n4 == -1) {
            this.e[n2] = j(this.f[n3]);
        }
        else {
            final long[] f = this.f;
            f[n4] = D(f[n4], j(f[n3]));
        }
        this.p(n3);
        --this.c;
        ++this.d;
        return n;
    }
    
    public int x(final int n) {
        return this.w(this.a[n], h(this.f[n]));
    }
    
    public void y(final int n) {
        this.a = Arrays.copyOf(this.a, n);
        this.b = Arrays.copyOf(this.b, n);
        final long[] f = this.f;
        final int length = f.length;
        final long[] copy = Arrays.copyOf(f, n);
        if (n > length) {
            Arrays.fill(copy, length, n, -1L);
        }
        this.f = copy;
    }
    
    public final void z(int n) {
        final int length = this.f.length;
        if (n > length) {
            if ((n = Math.max(1, length >>> 1) + length) < 0) {
                n = Integer.MAX_VALUE;
            }
            if (n != length) {
                this.y(n);
            }
        }
    }
    
    public class a extends b
    {
        public final Object a;
        public int b;
        public final k c;
        
        public a(final k c, final int b) {
            this.c = c;
            this.a = c.a[b];
            this.b = b;
        }
        
        public void a() {
            final int b = this.b;
            if (b == -1 || b >= this.c.C() || !b11.a(this.a, this.c.a[this.b])) {
                this.b = this.c.m(this.a);
            }
        }
        
        @Override
        public int getCount() {
            this.a();
            final int b = this.b;
            int n;
            if (b == -1) {
                n = 0;
            }
            else {
                n = this.c.b[b];
            }
            return n;
        }
        
        @Override
        public Object getElement() {
            return this.a;
        }
    }
}

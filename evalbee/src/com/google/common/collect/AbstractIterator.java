// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.NoSuchElementException;

public abstract class AbstractIterator extends w02
{
    public State a;
    public Object b;
    
    public AbstractIterator() {
        this.a = State.NOT_READY;
    }
    
    public abstract Object b();
    
    public final Object c() {
        this.a = State.DONE;
        return null;
    }
    
    public final boolean d() {
        this.a = State.FAILED;
        this.b = this.b();
        if (this.a != State.DONE) {
            this.a = State.READY;
            return true;
        }
        return false;
    }
    
    @Override
    public final boolean hasNext() {
        i71.x(this.a != State.FAILED);
        final int n = AbstractIterator$a.a[this.a.ordinal()];
        return n != 1 && (n == 2 || this.d());
    }
    
    @Override
    public final Object next() {
        if (this.hasNext()) {
            this.a = State.NOT_READY;
            final Object a = k01.a(this.b);
            this.b = null;
            return a;
        }
        throw new NoSuchElementException();
    }
    
    public enum State
    {
        private static final State[] $VALUES;
        
        DONE, 
        FAILED, 
        NOT_READY, 
        READY;
        
        private static /* synthetic */ State[] $values() {
            return new State[] { State.READY, State.NOT_READY, State.DONE, State.FAILED };
        }
        
        static {
            $VALUES = $values();
        }
    }
}

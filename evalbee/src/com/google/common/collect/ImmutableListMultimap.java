// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.List;
import java.io.ObjectOutputStream;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.util.Iterator;
import java.util.Comparator;
import java.util.Collection;
import java.util.Map;

public class ImmutableListMultimap<K, V> extends ImmutableMultimap<K, V> implements dk0
{
    private static final long serialVersionUID = 0L;
    private transient ImmutableListMultimap<V, K> inverse;
    
    public ImmutableListMultimap(final ImmutableMap<K, ImmutableList<V>> immutableMap, final int n) {
        super((ImmutableMap<K, ? extends ImmutableCollection<Object>>)immutableMap, n);
    }
    
    public static <K, V> a builder() {
        return new a();
    }
    
    public static <K, V> ImmutableListMultimap<K, V> copyOf(final Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
        return new a().i(iterable).f();
    }
    
    public static <K, V> ImmutableListMultimap<K, V> copyOf(final rx0 rx0) {
        if (rx0.isEmpty()) {
            return of();
        }
        if (rx0 instanceof ImmutableListMultimap) {
            final ImmutableListMultimap immutableListMultimap = (ImmutableListMultimap)rx0;
            if (!immutableListMultimap.isPartialView()) {
                return immutableListMultimap;
            }
        }
        return fromMapEntries((Collection<? extends Map.Entry<? extends K, ? extends Collection<? extends V>>>)rx0.asMap().entrySet(), (Comparator<? super V>)null);
    }
    
    public static <K, V> ImmutableListMultimap<K, V> fromMapEntries(final Collection<? extends Map.Entry<? extends K, ? extends Collection<? extends V>>> collection, final Comparator<? super V> comparator) {
        if (collection.isEmpty()) {
            return of();
        }
        final ImmutableMap.b b = new ImmutableMap.b(collection.size());
        final Iterator iterator = collection.iterator();
        int n = 0;
        while (iterator.hasNext()) {
            final Map.Entry<Object, V> entry = (Map.Entry<Object, V>)iterator.next();
            final Object key = entry.getKey();
            final Collection collection2 = (Collection)entry.getValue();
            ImmutableList<Object> list;
            if (comparator == null) {
                list = ImmutableList.copyOf((Collection<?>)collection2);
            }
            else {
                list = ImmutableList.sortedCopyOf((Comparator<? super Object>)comparator, (Iterable<?>)collection2);
            }
            if (!list.isEmpty()) {
                b.g(key, list);
                n += list.size();
            }
        }
        return new ImmutableListMultimap<K, V>(b.d(), n);
    }
    
    private ImmutableListMultimap<V, K> invert() {
        final a builder = builder();
        for (final Map.Entry<K, Object> entry : this.entries()) {
            builder.g(entry.getValue(), entry.getKey());
        }
        final ImmutableListMultimap f = builder.f();
        f.inverse = (ImmutableListMultimap<V, K>)this;
        return f;
    }
    
    public static <K, V> ImmutableListMultimap<K, V> of() {
        return (ImmutableListMultimap<K, V>)EmptyImmutableListMultimap.INSTANCE;
    }
    
    public static <K, V> ImmutableListMultimap<K, V> of(final K k, final V v) {
        final a builder = builder();
        builder.g(k, v);
        return builder.f();
    }
    
    public static <K, V> ImmutableListMultimap<K, V> of(final K k, final V v, final K i, final V v2) {
        final a builder = builder();
        builder.g(k, v);
        builder.g(i, v2);
        return builder.f();
    }
    
    public static <K, V> ImmutableListMultimap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3) {
        final a builder = builder();
        builder.g(k, v);
        builder.g(i, v2);
        builder.g(j, v3);
        return builder.f();
    }
    
    public static <K, V> ImmutableListMultimap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4) {
        final a builder = builder();
        builder.g(k, v);
        builder.g(i, v2);
        builder.g(j, v3);
        builder.g(l, v4);
        return builder.f();
    }
    
    public static <K, V> ImmutableListMultimap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4, final K m, final V v5) {
        final a builder = builder();
        builder.g(k, v);
        builder.g(i, v2);
        builder.g(j, v3);
        builder.g(l, v4);
        builder.g(m, v5);
        return builder.f();
    }
    
    private void readObject(final ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        final int int1 = objectInputStream.readInt();
        if (int1 >= 0) {
            final ImmutableMap.b builder = ImmutableMap.builder();
            int i = 0;
            int n = 0;
            while (i < int1) {
                final Object object = objectInputStream.readObject();
                final int int2 = objectInputStream.readInt();
                if (int2 <= 0) {
                    final StringBuilder sb = new StringBuilder(31);
                    sb.append("Invalid value count ");
                    sb.append(int2);
                    throw new InvalidObjectException(sb.toString());
                }
                final ImmutableList.a builder2 = ImmutableList.builder();
                for (int j = 0; j < int2; ++j) {
                    builder2.i(objectInputStream.readObject());
                }
                builder.g(object, builder2.l());
                n += int2;
                ++i;
            }
            try {
                d.a.b(this, builder.d());
                d.b.a(this, n);
                return;
            }
            catch (final IllegalArgumentException cause) {
                throw (InvalidObjectException)new InvalidObjectException(cause.getMessage()).initCause(cause);
            }
        }
        final StringBuilder sb2 = new StringBuilder(29);
        sb2.append("Invalid key count ");
        sb2.append(int1);
        throw new InvalidObjectException(sb2.toString());
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        n.j(this, objectOutputStream);
    }
    
    @Override
    public ImmutableList<V> get(final K k) {
        ImmutableList<Object> of;
        if ((of = (ImmutableList)super.map.get(k)) == null) {
            of = ImmutableList.of();
        }
        return (ImmutableList<V>)of;
    }
    
    @Override
    public ImmutableListMultimap<V, K> inverse() {
        ImmutableListMultimap<V, K> inverse;
        if ((inverse = this.inverse) == null) {
            inverse = this.invert();
            this.inverse = inverse;
        }
        return inverse;
    }
    
    @Deprecated
    @Override
    public final ImmutableList<V> removeAll(final Object o) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public final ImmutableList<V> replaceValues(final K k, final Iterable<? extends V> iterable) {
        throw new UnsupportedOperationException();
    }
    
    public static final class a extends c
    {
        public ImmutableListMultimap f() {
            return (ImmutableListMultimap)super.a();
        }
        
        public a g(final Object o, final Object o2) {
            super.c(o, o2);
            return this;
        }
        
        public a h(final Map.Entry entry) {
            super.d(entry);
            return this;
        }
        
        public a i(final Iterable iterable) {
            super.e(iterable);
            return this;
        }
    }
}

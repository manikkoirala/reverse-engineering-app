// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.NavigableSet;
import java.util.Comparator;

public abstract class g extends h implements o
{
    public transient Comparator a;
    public transient NavigableSet b;
    public transient Set c;
    
    public Set b() {
        return new a();
    }
    
    public abstract Iterator c();
    
    @Override
    public Comparator comparator() {
        Comparator a;
        if ((a = this.a) == null) {
            a = Ordering.from((Comparator<Object>)this.g().comparator()).reverse();
            this.a = a;
        }
        return a;
    }
    
    @Override
    public j delegate() {
        return this.g();
    }
    
    @Override
    public o descendingMultiset() {
        return this.g();
    }
    
    @Override
    public NavigableSet elementSet() {
        NavigableSet b;
        if ((b = this.b) == null) {
            b = new p.b(this);
            this.b = b;
        }
        return b;
    }
    
    @Override
    public Set entrySet() {
        Set c;
        if ((c = this.c) == null) {
            c = this.b();
            this.c = c;
        }
        return c;
    }
    
    @Override
    public j.a firstEntry() {
        return this.g().lastEntry();
    }
    
    public abstract o g();
    
    @Override
    public o headMultiset(final Object o, final BoundType boundType) {
        return this.g().tailMultiset(o, boundType).descendingMultiset();
    }
    
    @Override
    public j.a lastEntry() {
        return this.g().firstEntry();
    }
    
    @Override
    public j.a pollFirstEntry() {
        return this.g().pollLastEntry();
    }
    
    @Override
    public j.a pollLastEntry() {
        return this.g().pollFirstEntry();
    }
    
    @Override
    public o subMultiset(final Object o, final BoundType boundType, final Object o2, final BoundType boundType2) {
        return this.g().subMultiset(o2, boundType2, o, boundType).descendingMultiset();
    }
    
    @Override
    public o tailMultiset(final Object o, final BoundType boundType) {
        return this.g().headMultiset(o, boundType).descendingMultiset();
    }
    
    @Override
    public Object[] toArray() {
        return this.standardToArray();
    }
    
    @Override
    public Object[] toArray(final Object[] array) {
        return this.standardToArray(array);
    }
    
    @Override
    public String toString() {
        return this.entrySet().toString();
    }
    
    public class a extends d
    {
        public final g a;
        
        public a(final g a) {
            this.a = a;
        }
        
        @Override
        public j a() {
            return this.a;
        }
        
        @Override
        public Iterator iterator() {
            return this.a.c();
        }
        
        @Override
        public int size() {
            return this.a.g().entrySet().size();
        }
    }
}

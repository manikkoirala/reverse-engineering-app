// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Iterator;
import java.util.Comparator;
import java.io.Serializable;

final class LexicographicalOrdering<T> extends Ordering implements Serializable
{
    private static final long serialVersionUID = 0L;
    final Comparator<? super T> elementOrder;
    
    public LexicographicalOrdering(final Comparator<? super T> elementOrder) {
        this.elementOrder = elementOrder;
    }
    
    public int compare(final Iterable<T> iterable, final Iterable<T> iterable2) {
        final Iterator<T> iterator = iterable.iterator();
        final Iterator<T> iterator2 = iterable2.iterator();
        while (iterator.hasNext()) {
            if (!iterator2.hasNext()) {
                return 1;
            }
            final int compare = this.elementOrder.compare(iterator.next(), iterator2.next());
            if (compare != 0) {
                return compare;
            }
        }
        if (iterator2.hasNext()) {
            return -1;
        }
        return 0;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof LexicographicalOrdering && this.elementOrder.equals(((LexicographicalOrdering)o).elementOrder));
    }
    
    @Override
    public int hashCode() {
        return this.elementOrder.hashCode() ^ 0x7BB78CF5;
    }
    
    @Override
    public String toString() {
        final String value = String.valueOf(this.elementOrder);
        final StringBuilder sb = new StringBuilder(value.length() + 18);
        sb.append(value);
        sb.append(".lexicographical()");
        return sb.toString();
    }
}

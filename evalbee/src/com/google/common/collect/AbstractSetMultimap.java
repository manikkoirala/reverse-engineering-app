// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Collections;
import java.util.Set;
import java.util.Collection;
import java.util.Map;

abstract class AbstractSetMultimap<K, V> extends AbstractMapBasedMultimap<K, V> implements pm1
{
    private static final long serialVersionUID = 7431625294878419160L;
    
    public AbstractSetMultimap(final Map<K, Collection<V>> map) {
        super(map);
    }
    
    @Override
    public Map<K, Collection<V>> asMap() {
        return (Map<K, Collection<V>>)super.asMap();
    }
    
    @Override
    abstract Set<V> createCollection();
    
    @Override
    public Set<V> createUnmodifiableEmptyCollection() {
        return Collections.emptySet();
    }
    
    @Override
    public Set<Map.Entry<K, V>> entries() {
        return (Set)super.entries();
    }
    
    @Override
    public boolean equals(final Object o) {
        return super.equals(o);
    }
    
    @Override
    public Set<V> get(final K k) {
        return (Set)super.get(k);
    }
    
    @Override
    public boolean put(final K k, final V v) {
        return super.put(k, v);
    }
    
    @Override
    public Set<V> removeAll(final Object o) {
        return (Set)super.removeAll(o);
    }
    
    @Override
    public Set<V> replaceValues(final K k, final Iterable<? extends V> iterable) {
        return (Set)super.replaceValues(k, iterable);
    }
    
    @Override
    public <E> Collection<E> unmodifiableCollectionSubclass(final Collection<E> collection) {
        return (Collection<E>)Collections.unmodifiableSet((Set<?>)collection);
    }
    
    @Override
    public Collection<V> wrapCollection(final K k, final Collection<V> collection) {
        return new n(k, (Set)collection);
    }
}

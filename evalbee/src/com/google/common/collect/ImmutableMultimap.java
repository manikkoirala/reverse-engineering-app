// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Objects;
import java.util.Iterator;
import java.util.Set;
import java.util.Collection;
import java.util.Map;
import java.io.Serializable;

public abstract class ImmutableMultimap<K, V> extends hb implements Serializable
{
    private static final long serialVersionUID = 0L;
    final transient ImmutableMap<K, ? extends ImmutableCollection<V>> map;
    final transient int size;
    
    public ImmutableMultimap(final ImmutableMap<K, ? extends ImmutableCollection<V>> map, final int size) {
        this.map = map;
        this.size = size;
    }
    
    public static <K, V> c builder() {
        return new c();
    }
    
    public static <K, V> ImmutableMultimap<K, V> copyOf(final Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
        return (ImmutableMultimap<K, V>)ImmutableListMultimap.copyOf((Iterable<? extends Map.Entry<?, ?>>)iterable);
    }
    
    public static <K, V> ImmutableMultimap<K, V> copyOf(final rx0 rx0) {
        if (rx0 instanceof ImmutableMultimap) {
            final ImmutableMultimap immutableMultimap = (ImmutableMultimap)rx0;
            if (!immutableMultimap.isPartialView()) {
                return immutableMultimap;
            }
        }
        return (ImmutableMultimap<K, V>)ImmutableListMultimap.copyOf(rx0);
    }
    
    public static <K, V> ImmutableMultimap<K, V> of() {
        return (ImmutableMultimap<K, V>)ImmutableListMultimap.of();
    }
    
    public static <K, V> ImmutableMultimap<K, V> of(final K k, final V v) {
        return ImmutableListMultimap.of(k, v);
    }
    
    public static <K, V> ImmutableMultimap<K, V> of(final K k, final V v, final K i, final V v2) {
        return ImmutableListMultimap.of(k, v, i, v2);
    }
    
    public static <K, V> ImmutableMultimap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3) {
        return ImmutableListMultimap.of(k, v, i, v2, j, v3);
    }
    
    public static <K, V> ImmutableMultimap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4) {
        return ImmutableListMultimap.of(k, v, i, v2, j, v3, l, v4);
    }
    
    public static <K, V> ImmutableMultimap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4, final K m, final V v5) {
        return ImmutableListMultimap.of(k, v, i, v2, j, v3, l, v4, m, v5);
    }
    
    @Override
    public ImmutableMap<K, Collection<V>> asMap() {
        return (ImmutableMap<K, Collection<V>>)this.map;
    }
    
    @Deprecated
    @Override
    public final void clear() {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public boolean containsKey(final Object o) {
        return this.map.containsKey(o);
    }
    
    @Override
    public boolean containsValue(final Object o) {
        return o != null && super.containsValue(o);
    }
    
    @Override
    public Map<K, Collection<V>> createAsMap() {
        throw new AssertionError((Object)"should never be called");
    }
    
    @Override
    public ImmutableCollection<Map.Entry<K, V>> createEntries() {
        return (ImmutableCollection<Map.Entry<K, V>>)new EntryCollection((ImmutableMultimap<Object, Object>)this);
    }
    
    @Override
    public Set<K> createKeySet() {
        throw new AssertionError((Object)"unreachable");
    }
    
    @Override
    public ImmutableMultiset<K> createKeys() {
        return new Keys();
    }
    
    @Override
    public ImmutableCollection<V> createValues() {
        return (ImmutableCollection<V>)new Values((ImmutableMultimap<Object, Object>)this);
    }
    
    @Override
    public ImmutableCollection<Map.Entry<K, V>> entries() {
        return (ImmutableCollection)super.entries();
    }
    
    public w02 entryIterator() {
        return new w02(this) {
            public final Iterator a = d.map.entrySet().iterator();
            public Object b = null;
            public Iterator c = Iterators.h();
            public final ImmutableMultimap d;
            
            public Map.Entry b() {
                if (!this.c.hasNext()) {
                    final Map.Entry<Object, V> entry = this.a.next();
                    this.b = entry.getKey();
                    this.c = ((ImmutableCollection)entry.getValue()).iterator();
                }
                final Object b = this.b;
                Objects.requireNonNull(b);
                return Maps.j(b, this.c.next());
            }
            
            @Override
            public boolean hasNext() {
                return this.c.hasNext() || this.a.hasNext();
            }
        };
    }
    
    @Override
    public abstract ImmutableCollection<V> get(final K p0);
    
    public abstract ImmutableMultimap<V, K> inverse();
    
    public boolean isPartialView() {
        return this.map.isPartialView();
    }
    
    @Override
    public ImmutableSet<K> keySet() {
        return this.map.keySet();
    }
    
    @Override
    public ImmutableMultiset<K> keys() {
        return (ImmutableMultiset)super.keys();
    }
    
    @Deprecated
    @Override
    public final boolean put(final K k, final V v) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public final boolean putAll(final K k, final Iterable<? extends V> iterable) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public final boolean putAll(final rx0 rx0) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public final boolean remove(final Object o, final Object o2) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public ImmutableCollection<V> removeAll(final Object o) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public ImmutableCollection<V> replaceValues(final K k, final Iterable<? extends V> iterable) {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public int size() {
        return this.size;
    }
    
    public w02 valueIterator() {
        return new w02(this) {
            public Iterator a = c.map.values().iterator();
            public Iterator b = Iterators.h();
            public final ImmutableMultimap c;
            
            @Override
            public boolean hasNext() {
                return this.b.hasNext() || this.a.hasNext();
            }
            
            @Override
            public Object next() {
                if (!this.b.hasNext()) {
                    this.b = this.a.next().iterator();
                }
                return this.b.next();
            }
        };
    }
    
    @Override
    public ImmutableCollection<V> values() {
        return (ImmutableCollection)super.values();
    }
    
    public static class EntryCollection<K, V> extends ImmutableCollection<Map.Entry<K, V>>
    {
        private static final long serialVersionUID = 0L;
        final ImmutableMultimap<K, V> multimap;
        
        public EntryCollection(final ImmutableMultimap<K, V> multimap) {
            this.multimap = multimap;
        }
        
        @Override
        public boolean contains(final Object o) {
            if (o instanceof Map.Entry) {
                final Map.Entry entry = (Map.Entry)o;
                return this.multimap.containsEntry(entry.getKey(), entry.getValue());
            }
            return false;
        }
        
        @Override
        public boolean isPartialView() {
            return this.multimap.isPartialView();
        }
        
        @Override
        public w02 iterator() {
            return this.multimap.entryIterator();
        }
        
        @Override
        public int size() {
            return this.multimap.size();
        }
    }
    
    public class Keys extends ImmutableMultiset<K>
    {
        final ImmutableMultimap this$0;
        
        public Keys(final ImmutableMultimap this$0) {
            this.this$0 = this$0;
        }
        
        @Override
        public boolean contains(final Object o) {
            return this.this$0.containsKey(o);
        }
        
        @Override
        public int count(final Object o) {
            final Collection collection = (Collection)this.this$0.map.get(o);
            int size;
            if (collection == null) {
                size = 0;
            }
            else {
                size = collection.size();
            }
            return size;
        }
        
        @Override
        public ImmutableSet<K> elementSet() {
            return this.this$0.keySet();
        }
        
        @Override
        public j.a getEntry(final int n) {
            final Map.Entry entry = (Map.Entry)this.this$0.map.entrySet().asList().get(n);
            return Multisets.g(entry.getKey(), ((Collection)entry.getValue()).size());
        }
        
        @Override
        public boolean isPartialView() {
            return true;
        }
        
        @Override
        public int size() {
            return this.this$0.size();
        }
        
        public Object writeReplace() {
            return new KeysSerializedForm(this.this$0);
        }
    }
    
    public static final class KeysSerializedForm implements Serializable
    {
        final ImmutableMultimap<?, ?> multimap;
        
        public KeysSerializedForm(final ImmutableMultimap<?, ?> multimap) {
            this.multimap = multimap;
        }
        
        public Object readResolve() {
            return this.multimap.keys();
        }
    }
    
    public static final class Values<K, V> extends ImmutableCollection<V>
    {
        private static final long serialVersionUID = 0L;
        private final transient ImmutableMultimap<K, V> multimap;
        
        public Values(final ImmutableMultimap<K, V> multimap) {
            this.multimap = multimap;
        }
        
        @Override
        public boolean contains(final Object o) {
            return this.multimap.containsValue(o);
        }
        
        @Override
        public int copyIntoArray(final Object[] array, int copyIntoArray) {
            final w02 iterator = this.multimap.map.values().iterator();
            while (iterator.hasNext()) {
                copyIntoArray = ((ImmutableCollection)iterator.next()).copyIntoArray(array, copyIntoArray);
            }
            return copyIntoArray;
        }
        
        @Override
        public boolean isPartialView() {
            return true;
        }
        
        @Override
        public w02 iterator() {
            return this.multimap.valueIterator();
        }
        
        @Override
        public int size() {
            return this.multimap.size();
        }
    }
    
    public static class c
    {
        public final Map a;
        public Comparator b;
        public Comparator c;
        
        public c() {
            this.a = l.h();
        }
        
        public ImmutableMultimap a() {
            final Set entrySet = this.a.entrySet();
            final Comparator b = this.b;
            Object immutableSortedCopy = entrySet;
            if (b != null) {
                immutableSortedCopy = Ordering.from((Comparator<Object>)b).onKeys().immutableSortedCopy(entrySet);
            }
            return ImmutableListMultimap.fromMapEntries((Collection<? extends Map.Entry<?, ? extends Collection<?>>>)immutableSortedCopy, (Comparator<? super Object>)this.c);
        }
        
        public Collection b() {
            return new ArrayList();
        }
        
        public c c(final Object o, final Object o2) {
            hh.a(o, o2);
            Collection b;
            if ((b = this.a.get(o)) == null) {
                final Map a = this.a;
                b = this.b();
                a.put(o, b);
            }
            b.add(o2);
            return this;
        }
        
        public c d(final Map.Entry entry) {
            return this.c(entry.getKey(), entry.getValue());
        }
        
        public c e(final Iterable iterable) {
            final Iterator iterator = iterable.iterator();
            while (iterator.hasNext()) {
                this.d((Map.Entry)iterator.next());
            }
            return this;
        }
    }
    
    public abstract static class d
    {
        public static final n.b a;
        public static final n.b b;
        
        static {
            a = n.a(ImmutableMultimap.class, "map");
            b = n.a(ImmutableMultimap.class, "size");
        }
    }
}

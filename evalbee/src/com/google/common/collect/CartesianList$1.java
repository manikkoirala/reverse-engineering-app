// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.List;

class CartesianList$1 extends ImmutableList<Object>
{
    final f this$0;
    final int val$index;
    
    public CartesianList$1(final f f, final int val$index) {
        this.val$index = val$index;
    }
    
    @Override
    public Object get(final int n) {
        i71.p(n, this.size());
        return ((List)f.a(null).get(n)).get(f.b(null, this.val$index, n));
    }
    
    @Override
    public boolean isPartialView() {
        return true;
    }
    
    @Override
    public int size() {
        return f.a(null).size();
    }
}

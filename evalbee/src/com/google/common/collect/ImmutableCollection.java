// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Collection;
import java.io.Serializable;
import java.util.AbstractCollection;

public abstract class ImmutableCollection<E> extends AbstractCollection<E> implements Serializable
{
    private static final Object[] EMPTY_ARRAY;
    
    static {
        EMPTY_ARRAY = new Object[0];
    }
    
    @Deprecated
    @Override
    public final boolean add(final E e) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public final boolean addAll(final Collection<? extends E> collection) {
        throw new UnsupportedOperationException();
    }
    
    public ImmutableList<E> asList() {
        ImmutableList<Object> list;
        if (this.isEmpty()) {
            list = ImmutableList.of();
        }
        else {
            list = ImmutableList.asImmutableList(this.toArray());
        }
        return (ImmutableList<E>)list;
    }
    
    @Deprecated
    @Override
    public final void clear() {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public abstract boolean contains(final Object p0);
    
    public int copyIntoArray(final Object[] array, int n) {
        final w02 iterator = this.iterator();
        while (iterator.hasNext()) {
            array[n] = iterator.next();
            ++n;
        }
        return n;
    }
    
    public Object[] internalArray() {
        return null;
    }
    
    public int internalArrayEnd() {
        throw new UnsupportedOperationException();
    }
    
    public int internalArrayStart() {
        throw new UnsupportedOperationException();
    }
    
    public abstract boolean isPartialView();
    
    @Override
    public abstract w02 iterator();
    
    @Deprecated
    @Override
    public final boolean remove(final Object o) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public final boolean removeAll(final Collection<?> collection) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public final boolean retainAll(final Collection<?> collection) {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public final Object[] toArray() {
        return this.toArray(ImmutableCollection.EMPTY_ARRAY);
    }
    
    @Override
    public final <T> T[] toArray(final T[] array) {
        i71.r(array);
        final int size = this.size();
        Object[] e;
        if (array.length < size) {
            final Object[] internalArray = this.internalArray();
            if (internalArray != null) {
                return (T[])l.a(internalArray, this.internalArrayStart(), this.internalArrayEnd(), array);
            }
            e = t01.e(array, size);
        }
        else {
            e = array;
            if (array.length > size) {
                array[size] = null;
                e = array;
            }
        }
        this.copyIntoArray(e, 0);
        return (T[])e;
    }
    
    Object writeReplace() {
        return new ImmutableList.SerializedForm(this.toArray());
    }
    
    public abstract static class a extends b
    {
        public Object[] a;
        public int b;
        public boolean c;
        
        public a(final int n) {
            hh.b(n, "initialCapacity");
            this.a = new Object[n];
            this.b = 0;
        }
        
        @Override
        public b b(final Object... array) {
            this.g(array, array.length);
            return this;
        }
        
        public a f(final Object o) {
            i71.r(o);
            this.h(this.b + 1);
            this.a[this.b++] = o;
            return this;
        }
        
        public final void g(final Object[] array, final int n) {
            t01.c(array, n);
            this.h(this.b + n);
            System.arraycopy(array, 0, this.a, this.b, n);
            this.b += n;
        }
        
        public final void h(final int n) {
            final Object[] a = this.a;
            if (a.length < n) {
                this.a = Arrays.copyOf(a, ImmutableCollection.b.e(a.length, n));
            }
            else {
                if (!this.c) {
                    return;
                }
                this.a = a.clone();
            }
            this.c = false;
        }
    }
    
    public abstract static class b
    {
        public static int e(int n, int n2) {
            if (n2 >= 0) {
                if ((n = n + (n >> 1) + 1) < n2) {
                    n = Integer.highestOneBit(n2 - 1) << 1;
                }
                if ((n2 = n) < 0) {
                    n2 = Integer.MAX_VALUE;
                }
                return n2;
            }
            throw new AssertionError((Object)"cannot store more than MAX_VALUE elements");
        }
        
        public abstract b a(final Object p0);
        
        public b b(final Object... array) {
            for (int length = array.length, i = 0; i < length; ++i) {
                this.a(array[i]);
            }
            return this;
        }
        
        public b c(final Iterable iterable) {
            final Iterator iterator = iterable.iterator();
            while (iterator.hasNext()) {
                this.a(iterator.next());
            }
            return this;
        }
        
        public b d(final Iterator iterator) {
            while (iterator.hasNext()) {
                this.a(iterator.next());
            }
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.io.Serializable;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.Iterator;
import java.util.Comparator;

final class EmptyContiguousSet<C extends Comparable> extends ContiguousSet<C>
{
    public EmptyContiguousSet(final DiscreteDomain discreteDomain) {
        super(discreteDomain);
    }
    
    @Override
    public ImmutableList<C> asList() {
        return ImmutableList.of();
    }
    
    @Override
    public boolean contains(final Object o) {
        return false;
    }
    
    @Override
    public ImmutableSortedSet<C> createDescendingSet() {
        return (ImmutableSortedSet<C>)ImmutableSortedSet.emptySet(Ordering.natural().reverse());
    }
    
    @Override
    public w02 descendingIterator() {
        return Iterators.h();
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof Set && ((Set)o).isEmpty();
    }
    
    @Override
    public C first() {
        throw new NoSuchElementException();
    }
    
    @Override
    public int hashCode() {
        return 0;
    }
    
    @Override
    public ContiguousSet<C> headSetImpl(final C c, final boolean b) {
        return this;
    }
    
    @Override
    public int indexOf(final Object o) {
        return -1;
    }
    
    @Override
    public ContiguousSet<C> intersection(final ContiguousSet<C> set) {
        return this;
    }
    
    @Override
    public boolean isEmpty() {
        return true;
    }
    
    @Override
    public boolean isHashCodeFast() {
        return true;
    }
    
    @Override
    public boolean isPartialView() {
        return false;
    }
    
    @Override
    public w02 iterator() {
        return Iterators.h();
    }
    
    @Override
    public C last() {
        throw new NoSuchElementException();
    }
    
    @Override
    public Range<C> range() {
        throw new NoSuchElementException();
    }
    
    @Override
    public Range<C> range(final BoundType boundType, final BoundType boundType2) {
        throw new NoSuchElementException();
    }
    
    @Override
    public int size() {
        return 0;
    }
    
    @Override
    public ContiguousSet<C> subSetImpl(final C c, final boolean b, final C c2, final boolean b2) {
        return this;
    }
    
    @Override
    public ContiguousSet<C> tailSetImpl(final C c, final boolean b) {
        return this;
    }
    
    @Override
    public String toString() {
        return "[]";
    }
    
    @Override
    public Object writeReplace() {
        return new SerializedForm(super.domain, null);
    }
    
    public static final class SerializedForm<C extends Comparable> implements Serializable
    {
        private static final long serialVersionUID = 0L;
        private final DiscreteDomain domain;
        
        private SerializedForm(final DiscreteDomain domain) {
            this.domain = domain;
        }
        
        private Object readResolve() {
            return new EmptyContiguousSet(this.domain);
        }
    }
}

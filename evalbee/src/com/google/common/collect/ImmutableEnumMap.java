// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;
import java.util.EnumMap;

final class ImmutableEnumMap<K extends Enum<K>, V> extends IteratorBasedImmutableMap<K, V>
{
    private final transient EnumMap<K, V> delegate;
    
    private ImmutableEnumMap(final EnumMap<K, V> delegate) {
        this.delegate = delegate;
        i71.d(delegate.isEmpty() ^ true);
    }
    
    public static <K extends Enum<K>, V> ImmutableMap<K, V> asImmutable(final EnumMap<K, V> enumMap) {
        final int size = enumMap.size();
        if (size == 0) {
            return ImmutableMap.of();
        }
        if (size != 1) {
            return new ImmutableEnumMap<K, V>((EnumMap<Enum, Object>)enumMap);
        }
        final Entry entry = (Entry)rg0.h(enumMap.entrySet());
        return ImmutableMap.of((K)entry.getKey(), (V)entry.getValue());
    }
    
    @Override
    public boolean containsKey(final Object key) {
        return this.delegate.containsKey(key);
    }
    
    @Override
    public w02 entryIterator() {
        return Maps.F(this.delegate.entrySet().iterator());
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        Object delegate = o;
        if (o instanceof ImmutableEnumMap) {
            delegate = ((ImmutableEnumMap)o).delegate;
        }
        return this.delegate.equals(delegate);
    }
    
    @Override
    public V get(final Object key) {
        return this.delegate.get(key);
    }
    
    @Override
    public boolean isPartialView() {
        return false;
    }
    
    @Override
    public w02 keyIterator() {
        return Iterators.x(this.delegate.keySet().iterator());
    }
    
    @Override
    public int size() {
        return this.delegate.size();
    }
    
    public Object writeReplace() {
        return new EnumSerializedForm((EnumMap<Enum, Object>)this.delegate);
    }
    
    public static class EnumSerializedForm<K extends Enum<K>, V> implements Serializable
    {
        private static final long serialVersionUID = 0L;
        final EnumMap<K, V> delegate;
        
        public EnumSerializedForm(final EnumMap<K, V> delegate) {
            this.delegate = delegate;
        }
        
        public Object readResolve() {
            return new ImmutableEnumMap(this.delegate, null);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.AbstractCollection;
import java.util.Iterator;
import java.util.Set;
import java.util.Collection;
import java.util.Map;

public abstract class a implements rx0
{
    private transient Map<Object, Collection<Object>> asMap;
    private transient Collection<Map.Entry<Object, Object>> entries;
    private transient Set<Object> keySet;
    private transient j keys;
    private transient Collection<Object> values;
    
    @Override
    public Map<Object, Collection<Object>> asMap() {
        Map<Object, Collection<Object>> asMap;
        if ((asMap = this.asMap) == null) {
            asMap = this.createAsMap();
            this.asMap = asMap;
        }
        return asMap;
    }
    
    @Override
    public boolean containsEntry(final Object o, final Object o2) {
        final Collection collection = this.asMap().get(o);
        return collection != null && collection.contains(o2);
    }
    
    @Override
    public boolean containsValue(final Object o) {
        final Iterator<Collection<Object>> iterator = this.asMap().values().iterator();
        while (iterator.hasNext()) {
            if (iterator.next().contains(o)) {
                return true;
            }
        }
        return false;
    }
    
    public abstract Map createAsMap();
    
    public abstract Collection createEntries();
    
    public abstract Set createKeySet();
    
    public abstract j createKeys();
    
    public abstract Collection createValues();
    
    @Override
    public Collection entries() {
        Collection<Map.Entry<Object, Object>> entries;
        if ((entries = this.entries) == null) {
            entries = this.createEntries();
            this.entries = entries;
        }
        return entries;
    }
    
    abstract Iterator entryIterator();
    
    @Override
    public boolean equals(final Object o) {
        return Multimaps.c(this, o);
    }
    
    @Override
    public int hashCode() {
        return this.asMap().hashCode();
    }
    
    @Override
    public boolean isEmpty() {
        return this.size() == 0;
    }
    
    @Override
    public Set<Object> keySet() {
        Set<Object> keySet;
        if ((keySet = this.keySet) == null) {
            keySet = this.createKeySet();
            this.keySet = keySet;
        }
        return keySet;
    }
    
    @Override
    public j keys() {
        j keys;
        if ((keys = this.keys) == null) {
            keys = this.createKeys();
            this.keys = keys;
        }
        return keys;
    }
    
    @Override
    public abstract boolean put(final Object p0, final Object p1);
    
    @Override
    public boolean putAll(final Object o, final Iterable<Object> iterable) {
        i71.r(iterable);
        final boolean b = iterable instanceof Collection;
        boolean b2 = true;
        final boolean b3 = true;
        if (b) {
            final Collection collection = (Collection)iterable;
            return !collection.isEmpty() && this.get(o).addAll(collection) && b3;
        }
        final Iterator iterator = iterable.iterator();
        if (!iterator.hasNext() || !Iterators.a(this.get(o), iterator)) {
            b2 = false;
        }
        return b2;
    }
    
    @Override
    public boolean putAll(final rx0 rx0) {
        final Iterator iterator = rx0.entries().iterator();
        boolean b = false;
        while (iterator.hasNext()) {
            final Map.Entry<Object, V> entry = (Map.Entry<Object, V>)iterator.next();
            b |= this.put(entry.getKey(), entry.getValue());
        }
        return b;
    }
    
    @Override
    public boolean remove(final Object o, final Object o2) {
        final Collection collection = this.asMap().get(o);
        return collection != null && collection.remove(o2);
    }
    
    @Override
    public String toString() {
        return this.asMap().toString();
    }
    
    Iterator<Object> valueIterator() {
        return Maps.K(this.entries().iterator());
    }
    
    @Override
    public Collection<Object> values() {
        Collection<Object> values;
        if ((values = this.values) == null) {
            values = this.createValues();
            this.values = values;
        }
        return values;
    }
    
    public class a extends Multimaps.b
    {
        public final com.google.common.collect.a a;
        
        public a(final com.google.common.collect.a a) {
            this.a = a;
        }
        
        @Override
        public rx0 a() {
            return this.a;
        }
        
        @Override
        public Iterator iterator() {
            return this.a.entryIterator();
        }
    }
    
    public class b extends a implements Set
    {
        public b(final a a) {
            a.super();
        }
        
        @Override
        public boolean equals(final Object o) {
            return Sets.a(this, o);
        }
        
        @Override
        public int hashCode() {
            return Sets.b(this);
        }
    }
    
    public class c extends AbstractCollection
    {
        public final a a;
        
        public c(final a a) {
            this.a = a;
        }
        
        @Override
        public void clear() {
            this.a.clear();
        }
        
        @Override
        public boolean contains(final Object o) {
            return this.a.containsValue(o);
        }
        
        @Override
        public Iterator iterator() {
            return this.a.valueIterator();
        }
        
        @Override
        public int size() {
            return this.a.size();
        }
    }
}

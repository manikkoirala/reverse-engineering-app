// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.AbstractCollection;
import java.util.SortedSet;
import java.util.AbstractMap;
import java.util.HashSet;
import java.util.Comparator;
import java.util.NavigableSet;
import java.io.Serializable;
import com.google.common.base.Converter;
import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.Collection;
import com.google.common.base.Predicates;
import java.util.NavigableMap;
import java.util.Collections;
import java.util.Set;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedMap;

public abstract class Maps
{
    public static SortedMap A(final SortedMap sortedMap, final k k) {
        return new q(sortedMap, k);
    }
    
    public static Map.Entry B(final k k, final Map.Entry entry) {
        i71.r(k);
        i71.r(entry);
        return new s(entry, k) {
            public final Entry a;
            public final k b;
            
            @Override
            public Object getKey() {
                return this.a.getKey();
            }
            
            @Override
            public Object getValue() {
                return this.b.a(this.a.getKey(), this.a.getValue());
            }
        };
    }
    
    public static Map C(final Map map, final m90 m90) {
        return z(map, c(m90));
    }
    
    public static SortedMap D(final SortedMap sortedMap, final m90 m90) {
        return A(sortedMap, c(m90));
    }
    
    public static Map.Entry E(final Map.Entry entry) {
        i71.r(entry);
        return new s(entry) {
            public final Entry a;
            
            @Override
            public Object getKey() {
                return this.a.getKey();
            }
            
            @Override
            public Object getValue() {
                return this.a.getValue();
            }
        };
    }
    
    public static w02 F(final Iterator iterator) {
        return new w02(iterator) {
            public final Iterator a;
            
            public Map.Entry b() {
                return Maps.E((Map.Entry)this.a.next());
            }
            
            @Override
            public boolean hasNext() {
                return this.a.hasNext();
            }
        };
    }
    
    public static Set G(final Set s) {
        return new s(Collections.unmodifiableSet((Set<?>)s));
    }
    
    public static NavigableMap H(final NavigableMap navigableMap) {
        i71.r(navigableMap);
        if (navigableMap instanceof UnmodifiableNavigableMap) {
            return navigableMap;
        }
        return new UnmodifiableNavigableMap(navigableMap);
    }
    
    public static Map.Entry I(Map.Entry e) {
        if (e == null) {
            e = null;
        }
        else {
            e = E(e);
        }
        return e;
    }
    
    public static m90 J() {
        return EntryFunction.VALUE;
    }
    
    public static Iterator K(final Iterator iterator) {
        return new com.google.common.collect.r(iterator) {
            public Object c(final Map.Entry entry) {
                return entry.getValue();
            }
        };
    }
    
    public static Object L(final Map.Entry entry) {
        Object value;
        if (entry == null) {
            value = null;
        }
        else {
            value = entry.getValue();
        }
        return value;
    }
    
    public static m71 M(final m71 m71) {
        return Predicates.c(m71, J());
    }
    
    public static m90 b(final k k) {
        i71.r(k);
        return new m90(k) {
            public final k a;
            
            public Map.Entry a(final Map.Entry entry) {
                return Maps.B(this.a, entry);
            }
        };
    }
    
    public static k c(final m90 m90) {
        i71.r(m90);
        return (k)new k(m90) {
            public final m90 a;
            
            @Override
            public Object a(final Object o, final Object o2) {
                return this.a.apply(o2);
            }
        };
    }
    
    public static Iterator d(final Set set, final m90 m90) {
        return new com.google.common.collect.r(set.iterator(), m90) {
            public final m90 b;
            
            public Map.Entry c(final Object o) {
                return Maps.j(o, this.b.apply(o));
            }
        };
    }
    
    public static int e(final int n) {
        if (n < 3) {
            hh.b(n, "expectedSize");
            return n + 1;
        }
        if (n < 1073741824) {
            return (int)(n / 0.75f + 1.0f);
        }
        return Integer.MAX_VALUE;
    }
    
    public static boolean f(final Collection collection, final Object o) {
        return o instanceof Map.Entry && collection.contains(E((Map.Entry)o));
    }
    
    public static boolean g(final Map map, final Object o) {
        return Iterators.f(m(map.entrySet().iterator()), o);
    }
    
    public static boolean h(final Map map, final Object o) {
        return Iterators.f(K(map.entrySet().iterator()), o);
    }
    
    public static boolean i(final Map map, final Object o) {
        return map == o || (o instanceof Map && map.entrySet().equals(((Map)o).entrySet()));
    }
    
    public static Map.Entry j(final Object o, final Object o2) {
        return new ImmutableEntry(o, o2);
    }
    
    public static ImmutableMap k(final Collection collection) {
        final ImmutableMap.b b = new ImmutableMap.b(collection.size());
        final Iterator iterator = collection.iterator();
        int i = 0;
        while (iterator.hasNext()) {
            b.g(iterator.next(), i);
            ++i;
        }
        return b.d();
    }
    
    public static m90 l() {
        return EntryFunction.KEY;
    }
    
    public static Iterator m(final Iterator iterator) {
        return new com.google.common.collect.r(iterator) {
            public Object c(final Map.Entry entry) {
                return entry.getKey();
            }
        };
    }
    
    public static Object n(final Map.Entry entry) {
        Object key;
        if (entry == null) {
            key = null;
        }
        else {
            key = entry.getKey();
        }
        return key;
    }
    
    public static m71 o(final m71 m71) {
        return Predicates.c(m71, l());
    }
    
    public static HashMap p() {
        return new HashMap();
    }
    
    public static HashMap q(final int n) {
        return new HashMap(e(n));
    }
    
    public static LinkedHashMap r() {
        return new LinkedHashMap();
    }
    
    public static LinkedHashMap s(final int n) {
        return new LinkedHashMap(e(n));
    }
    
    public static void t(final Map map, final Map map2) {
        for (final Map.Entry<Object, V> entry : map2.entrySet()) {
            map.put(entry.getKey(), entry.getValue());
        }
    }
    
    public static boolean u(final Collection collection, final Object o) {
        return o instanceof Map.Entry && collection.remove(E((Map.Entry)o));
    }
    
    public static boolean v(final Map map, final Object o) {
        i71.r(map);
        try {
            return map.containsKey(o);
        }
        catch (final ClassCastException | NullPointerException ex) {
            return false;
        }
    }
    
    public static Object w(final Map map, final Object o) {
        i71.r(map);
        try {
            return map.get(o);
        }
        catch (final ClassCastException | NullPointerException ex) {
            return null;
        }
    }
    
    public static Object x(final Map map, final Object o) {
        i71.r(map);
        try {
            return map.remove(o);
        }
        catch (final ClassCastException | NullPointerException ex) {
            return null;
        }
    }
    
    public static String y(final Map map) {
        final StringBuilder b = lh.b(map.size());
        b.append('{');
        final Iterator iterator = map.entrySet().iterator();
        int n = 1;
        while (iterator.hasNext()) {
            final Map.Entry<Object, V> entry = (Map.Entry<Object, V>)iterator.next();
            if (n == 0) {
                b.append(", ");
            }
            b.append(entry.getKey());
            b.append('=');
            b.append(entry.getValue());
            n = 0;
        }
        b.append('}');
        return b.toString();
    }
    
    public static Map z(final Map map, final k k) {
        return new p(map, k);
    }
    
    public static final class BiMapConverter<A, B> extends Converter implements Serializable
    {
        private static final long serialVersionUID = 0L;
        private final ub bimap;
        
        public BiMapConverter(final ub ub) {
            this.bimap = (ub)i71.r(ub);
        }
        
        private static <X, Y> Y convert(final ub ub, final X x) {
            final Y value = ub.get(x);
            i71.m(value != null, "No non-null mapping present for input: %s", x);
            return value;
        }
        
        @Override
        public A doBackward(final B b) {
            return convert(this.bimap.inverse(), b);
        }
        
        @Override
        public B doForward(final A a) {
            return convert(this.bimap, a);
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof BiMapConverter && this.bimap.equals(((BiMapConverter)o).bimap);
        }
        
        @Override
        public int hashCode() {
            return this.bimap.hashCode();
        }
        
        @Override
        public String toString() {
            final String value = String.valueOf(this.bimap);
            final StringBuilder sb = new StringBuilder(value.length() + 18);
            sb.append("Maps.asConverter(");
            sb.append(value);
            sb.append(")");
            return sb.toString();
        }
    }
    
    public enum EntryFunction implements m90
    {
        private static final EntryFunction[] $VALUES;
        
        KEY {
            public Object apply(final Map.Entry<?, ?> entry) {
                return entry.getKey();
            }
        }, 
        VALUE {
            public Object apply(final Map.Entry<?, ?> entry) {
                return entry.getValue();
            }
        };
        
        private static /* synthetic */ EntryFunction[] $values() {
            return new EntryFunction[] { EntryFunction.KEY, EntryFunction.VALUE };
        }
        
        static {
            $VALUES = $values();
        }
    }
    
    public static class UnmodifiableBiMap<K, V> extends x70 implements ub, Serializable
    {
        private static final long serialVersionUID = 0L;
        final ub delegate;
        ub inverse;
        final Map<K, V> unmodifiableMap;
        transient Set<V> values;
        
        public UnmodifiableBiMap(final ub ub, final ub inverse) {
            this.unmodifiableMap = Collections.unmodifiableMap((Map<? extends K, ? extends V>)ub);
            this.delegate = ub;
            this.inverse = inverse;
        }
        
        @Override
        public Map<K, V> delegate() {
            return this.unmodifiableMap;
        }
        
        @Override
        public V forcePut(final K k, final V v) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public ub inverse() {
            ub inverse;
            if ((inverse = this.inverse) == null) {
                inverse = new UnmodifiableBiMap<Object, Object>(this.delegate.inverse(), this);
                this.inverse = inverse;
            }
            return inverse;
        }
        
        @Override
        public Set<V> values() {
            Object values;
            if ((values = this.values) == null) {
                values = Collections.unmodifiableSet((Set<? extends V>)this.delegate.values());
                this.values = (Set<V>)values;
            }
            return (Set<V>)values;
        }
    }
    
    public static class UnmodifiableNavigableMap<K, V> extends d80 implements NavigableMap<K, V>, Serializable
    {
        private final NavigableMap<K, ? extends V> delegate;
        private transient UnmodifiableNavigableMap<K, V> descendingMap;
        
        public UnmodifiableNavigableMap(final NavigableMap<K, ? extends V> delegate) {
            this.delegate = delegate;
        }
        
        public UnmodifiableNavigableMap(final NavigableMap<K, ? extends V> delegate, final UnmodifiableNavigableMap<K, V> descendingMap) {
            this.delegate = delegate;
            this.descendingMap = descendingMap;
        }
        
        @Override
        public Entry<K, V> ceilingEntry(final K k) {
            return I((Entry)this.delegate.ceilingEntry(k));
        }
        
        @Override
        public K ceilingKey(final K k) {
            return this.delegate.ceilingKey(k);
        }
        
        @Override
        public SortedMap<K, V> delegate() {
            return Collections.unmodifiableSortedMap((SortedMap<K, ? extends V>)this.delegate);
        }
        
        @Override
        public NavigableSet<K> descendingKeySet() {
            return Sets.h(this.delegate.descendingKeySet());
        }
        
        @Override
        public NavigableMap<K, V> descendingMap() {
            UnmodifiableNavigableMap<K, V> descendingMap;
            if ((descendingMap = this.descendingMap) == null) {
                descendingMap = new UnmodifiableNavigableMap<K, V>(this.delegate.descendingMap(), this);
                this.descendingMap = descendingMap;
            }
            return descendingMap;
        }
        
        @Override
        public Entry<K, V> firstEntry() {
            return I((Entry)this.delegate.firstEntry());
        }
        
        @Override
        public Entry<K, V> floorEntry(final K k) {
            return I((Entry)this.delegate.floorEntry(k));
        }
        
        @Override
        public K floorKey(final K k) {
            return this.delegate.floorKey(k);
        }
        
        @Override
        public NavigableMap<K, V> headMap(final K k, final boolean b) {
            return Maps.H(this.delegate.headMap(k, b));
        }
        
        @Override
        public SortedMap<K, V> headMap(final K k) {
            return this.headMap(k, false);
        }
        
        @Override
        public Entry<K, V> higherEntry(final K k) {
            return I((Entry)this.delegate.higherEntry(k));
        }
        
        @Override
        public K higherKey(final K k) {
            return this.delegate.higherKey(k);
        }
        
        @Override
        public Set<K> keySet() {
            return this.navigableKeySet();
        }
        
        @Override
        public Entry<K, V> lastEntry() {
            return I((Entry)this.delegate.lastEntry());
        }
        
        @Override
        public Entry<K, V> lowerEntry(final K k) {
            return I((Entry)this.delegate.lowerEntry(k));
        }
        
        @Override
        public K lowerKey(final K k) {
            return this.delegate.lowerKey(k);
        }
        
        @Override
        public NavigableSet<K> navigableKeySet() {
            return Sets.h(this.delegate.navigableKeySet());
        }
        
        @Override
        public final Entry<K, V> pollFirstEntry() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public final Entry<K, V> pollLastEntry() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public NavigableMap<K, V> subMap(final K k, final boolean b, final K i, final boolean b2) {
            return Maps.H(this.delegate.subMap(k, b, i, b2));
        }
        
        @Override
        public SortedMap<K, V> subMap(final K k, final K i) {
            return this.subMap(k, true, i, false);
        }
        
        @Override
        public NavigableMap<K, V> tailMap(final K k, final boolean b) {
            return Maps.H(this.delegate.tailMap(k, b));
        }
        
        @Override
        public SortedMap<K, V> tailMap(final K k) {
            return this.tailMap(k, true);
        }
    }
    
    public abstract static class i extends x70 implements NavigableMap
    {
        public transient Comparator a;
        public transient Set b;
        public transient NavigableSet c;
        
        public static Ordering i(final Comparator comparator) {
            return Ordering.from((Comparator<Object>)comparator).reverse();
        }
        
        public Set b() {
            return new a();
        }
        
        public abstract Iterator c();
        
        @Override
        public Entry ceilingEntry(final Object o) {
            return (Entry)this.g().floorEntry(o);
        }
        
        @Override
        public Object ceilingKey(final Object o) {
            return this.g().floorKey(o);
        }
        
        @Override
        public Comparator comparator() {
            Comparator a;
            if ((a = this.a) == null) {
                Comparator comparator;
                if ((comparator = this.g().comparator()) == null) {
                    comparator = Ordering.natural();
                }
                a = i(comparator);
                this.a = a;
            }
            return a;
        }
        
        @Override
        public final Map delegate() {
            return this.g();
        }
        
        @Override
        public NavigableSet descendingKeySet() {
            return this.g().navigableKeySet();
        }
        
        @Override
        public NavigableMap descendingMap() {
            return this.g();
        }
        
        @Override
        public Set entrySet() {
            Set b;
            if ((b = this.b) == null) {
                b = this.b();
                this.b = b;
            }
            return b;
        }
        
        @Override
        public Entry firstEntry() {
            return (Entry)this.g().lastEntry();
        }
        
        @Override
        public Object firstKey() {
            return this.g().lastKey();
        }
        
        @Override
        public Entry floorEntry(final Object o) {
            return (Entry)this.g().ceilingEntry(o);
        }
        
        @Override
        public Object floorKey(final Object o) {
            return this.g().ceilingKey(o);
        }
        
        public abstract NavigableMap g();
        
        @Override
        public NavigableMap headMap(final Object o, final boolean b) {
            return this.g().tailMap(o, b).descendingMap();
        }
        
        @Override
        public SortedMap headMap(final Object o) {
            return this.headMap(o, false);
        }
        
        @Override
        public Entry higherEntry(final Object o) {
            return (Entry)this.g().lowerEntry(o);
        }
        
        @Override
        public Object higherKey(final Object o) {
            return this.g().lowerKey(o);
        }
        
        @Override
        public Set keySet() {
            return this.navigableKeySet();
        }
        
        @Override
        public Entry lastEntry() {
            return (Entry)this.g().firstEntry();
        }
        
        @Override
        public Object lastKey() {
            return this.g().firstKey();
        }
        
        @Override
        public Entry lowerEntry(final Object o) {
            return (Entry)this.g().higherEntry(o);
        }
        
        @Override
        public Object lowerKey(final Object o) {
            return this.g().higherKey(o);
        }
        
        @Override
        public NavigableSet navigableKeySet() {
            NavigableSet c;
            if ((c = this.c) == null) {
                c = new n(this);
                this.c = c;
            }
            return c;
        }
        
        @Override
        public Entry pollFirstEntry() {
            return (Entry)this.g().pollLastEntry();
        }
        
        @Override
        public Entry pollLastEntry() {
            return (Entry)this.g().pollFirstEntry();
        }
        
        @Override
        public NavigableMap subMap(final Object o, final boolean b, final Object o2, final boolean b2) {
            return this.g().subMap(o2, b2, o, b).descendingMap();
        }
        
        @Override
        public SortedMap subMap(final Object o, final Object o2) {
            return this.subMap(o, true, o2, false);
        }
        
        @Override
        public NavigableMap tailMap(final Object o, final boolean b) {
            return this.g().headMap(o, b).descendingMap();
        }
        
        @Override
        public SortedMap tailMap(final Object o) {
            return this.tailMap(o, true);
        }
        
        @Override
        public String toString() {
            return this.standardToString();
        }
        
        @Override
        public Collection values() {
            return new t(this);
        }
        
        public class a extends j
        {
            public final i a;
            
            public a(final i a) {
                this.a = a;
            }
            
            @Override
            public Map a() {
                return this.a;
            }
            
            @Override
            public Iterator iterator() {
                return this.a.c();
            }
        }
    }
    
    public abstract static class j extends Sets.a
    {
        public abstract Map a();
        
        @Override
        public void clear() {
            this.a().clear();
        }
        
        @Override
        public boolean contains(final Object o) {
            final boolean b = o instanceof Map.Entry;
            boolean b3;
            final boolean b2 = b3 = false;
            if (b) {
                final Map.Entry entry = (Map.Entry)o;
                final Object key = entry.getKey();
                final Object w = Maps.w(this.a(), key);
                b3 = b2;
                if (b11.a(w, entry.getValue())) {
                    if (w == null) {
                        b3 = b2;
                        if (!this.a().containsKey(key)) {
                            return b3;
                        }
                    }
                    b3 = true;
                }
            }
            return b3;
        }
        
        @Override
        public boolean isEmpty() {
            return this.a().isEmpty();
        }
        
        @Override
        public boolean remove(final Object o) {
            return this.contains(o) && o instanceof Map.Entry && this.a().keySet().remove(((Map.Entry)o).getKey());
        }
        
        @Override
        public boolean removeAll(final Collection collection) {
            try {
                return super.removeAll((Collection)i71.r(collection));
            }
            catch (final UnsupportedOperationException ex) {
                return Sets.g(this, collection.iterator());
            }
        }
        
        @Override
        public boolean retainAll(final Collection collection) {
            try {
                return super.retainAll((Collection)i71.r(collection));
            }
            catch (final UnsupportedOperationException ex) {
                final HashSet d = Sets.d(collection.size());
                for (final Object next : collection) {
                    if (this.contains(next) && next instanceof Map.Entry) {
                        d.add(((Map.Entry<Object, V>)next).getKey());
                    }
                }
                return this.a().keySet().retainAll(d);
            }
        }
        
        @Override
        public int size() {
            return this.a().size();
        }
    }
    
    public interface k
    {
        Object a(final Object p0, final Object p1);
    }
    
    public abstract static class l extends AbstractMap
    {
        abstract Iterator a();
        
        @Override
        public void clear() {
            Iterators.d(this.a());
        }
        
        @Override
        public Set entrySet() {
            return new j(this) {
                public final l a;
                
                @Override
                public Map a() {
                    return this.a;
                }
                
                @Override
                public Iterator iterator() {
                    return this.a.a();
                }
            };
        }
    }
    
    public static class m extends Sets.a
    {
        public final Map a;
        
        public m(final Map map) {
            this.a = (Map)i71.r(map);
        }
        
        public Map a() {
            return this.a;
        }
        
        @Override
        public void clear() {
            this.a().clear();
        }
        
        @Override
        public boolean contains(final Object o) {
            return this.a().containsKey(o);
        }
        
        @Override
        public boolean isEmpty() {
            return this.a().isEmpty();
        }
        
        @Override
        public Iterator iterator() {
            return Maps.m(this.a().entrySet().iterator());
        }
        
        @Override
        public boolean remove(final Object o) {
            if (this.contains(o)) {
                this.a().remove(o);
                return true;
            }
            return false;
        }
        
        @Override
        public int size() {
            return this.a().size();
        }
    }
    
    public static class n extends o implements NavigableSet
    {
        public n(final NavigableMap navigableMap) {
            super(navigableMap);
        }
        
        public NavigableMap c() {
            return (NavigableMap)super.a;
        }
        
        @Override
        public Object ceiling(final Object o) {
            return this.c().ceilingKey(o);
        }
        
        @Override
        public Iterator descendingIterator() {
            return this.descendingSet().iterator();
        }
        
        @Override
        public NavigableSet descendingSet() {
            return this.c().descendingKeySet();
        }
        
        @Override
        public Object floor(final Object o) {
            return this.c().floorKey(o);
        }
        
        @Override
        public NavigableSet headSet(final Object o, final boolean b) {
            return this.c().headMap(o, b).navigableKeySet();
        }
        
        @Override
        public SortedSet headSet(final Object o) {
            return this.headSet(o, false);
        }
        
        @Override
        public Object higher(final Object o) {
            return this.c().higherKey(o);
        }
        
        @Override
        public Object lower(final Object o) {
            return this.c().lowerKey(o);
        }
        
        @Override
        public Object pollFirst() {
            return Maps.n((Map.Entry)this.c().pollFirstEntry());
        }
        
        @Override
        public Object pollLast() {
            return Maps.n((Map.Entry)this.c().pollLastEntry());
        }
        
        @Override
        public NavigableSet subSet(final Object o, final boolean b, final Object o2, final boolean b2) {
            return this.c().subMap(o, b, o2, b2).navigableKeySet();
        }
        
        @Override
        public SortedSet subSet(final Object o, final Object o2) {
            return this.subSet(o, true, o2, false);
        }
        
        @Override
        public NavigableSet tailSet(final Object o, final boolean b) {
            return this.c().tailMap(o, b).navigableKeySet();
        }
        
        @Override
        public SortedSet tailSet(final Object o) {
            return this.tailSet(o, true);
        }
    }
    
    public static class o extends m implements SortedSet
    {
        public o(final SortedMap sortedMap) {
            super(sortedMap);
        }
        
        public SortedMap b() {
            return (SortedMap)super.a();
        }
        
        @Override
        public Comparator comparator() {
            return this.b().comparator();
        }
        
        @Override
        public Object first() {
            return this.b().firstKey();
        }
        
        @Override
        public SortedSet headSet(final Object o) {
            return new o(this.b().headMap(o));
        }
        
        @Override
        public Object last() {
            return this.b().lastKey();
        }
        
        @Override
        public SortedSet subSet(final Object o, final Object o2) {
            return new o(this.b().subMap(o, o2));
        }
        
        @Override
        public SortedSet tailSet(final Object o) {
            return new o(this.b().tailMap(o));
        }
    }
    
    public static class p extends l
    {
        public final Map a;
        public final k b;
        
        public p(final Map map, final k k) {
            this.a = (Map)i71.r(map);
            this.b = (k)i71.r(k);
        }
        
        @Override
        Iterator a() {
            return Iterators.w(this.a.entrySet().iterator(), Maps.b(this.b));
        }
        
        @Override
        public void clear() {
            this.a.clear();
        }
        
        @Override
        public boolean containsKey(final Object o) {
            return this.a.containsKey(o);
        }
        
        @Override
        public Object get(final Object o) {
            final Object value = this.a.get(o);
            if (value == null && !this.a.containsKey(o)) {
                return null;
            }
            return this.b.a(o, k01.a(value));
        }
        
        @Override
        public Set keySet() {
            return this.a.keySet();
        }
        
        @Override
        public Object remove(Object a) {
            if (this.a.containsKey(a)) {
                a = this.b.a(a, k01.a(this.a.remove(a)));
            }
            else {
                a = null;
            }
            return a;
        }
        
        @Override
        public int size() {
            return this.a.size();
        }
        
        @Override
        public Collection values() {
            return new t(this);
        }
    }
    
    public static class q extends p implements SortedMap
    {
        public q(final SortedMap sortedMap, final k k) {
            super(sortedMap, k);
        }
        
        public SortedMap b() {
            return (SortedMap)super.a;
        }
        
        @Override
        public Comparator comparator() {
            return this.b().comparator();
        }
        
        @Override
        public Object firstKey() {
            return this.b().firstKey();
        }
        
        @Override
        public SortedMap headMap(final Object o) {
            return Maps.A(this.b().headMap(o), super.b);
        }
        
        @Override
        public Object lastKey() {
            return this.b().lastKey();
        }
        
        @Override
        public SortedMap subMap(final Object o, final Object o2) {
            return Maps.A(this.b().subMap(o, o2), super.b);
        }
        
        @Override
        public SortedMap tailMap(final Object o) {
            return Maps.A(this.b().tailMap(o), super.b);
        }
    }
    
    public static class r extends t70
    {
        public final Collection a;
        
        public r(final Collection a) {
            this.a = a;
        }
        
        @Override
        public Collection delegate() {
            return this.a;
        }
        
        @Override
        public Iterator iterator() {
            return Maps.F(this.a.iterator());
        }
        
        @Override
        public Object[] toArray() {
            return this.standardToArray();
        }
        
        @Override
        public Object[] toArray(final Object[] array) {
            return this.standardToArray(array);
        }
    }
    
    public static class s extends r implements Set
    {
        public s(final Set set) {
            super(set);
        }
        
        @Override
        public boolean equals(final Object o) {
            return Sets.a(this, o);
        }
        
        @Override
        public int hashCode() {
            return Sets.b(this);
        }
    }
    
    public static class t extends AbstractCollection
    {
        public final Map a;
        
        public t(final Map map) {
            this.a = (Map)i71.r(map);
        }
        
        public final Map a() {
            return this.a;
        }
        
        @Override
        public void clear() {
            this.a().clear();
        }
        
        @Override
        public boolean contains(final Object o) {
            return this.a().containsValue(o);
        }
        
        @Override
        public boolean isEmpty() {
            return this.a().isEmpty();
        }
        
        @Override
        public Iterator iterator() {
            return Maps.K(this.a().entrySet().iterator());
        }
        
        @Override
        public boolean remove(final Object o) {
            try {
                return super.remove(o);
            }
            catch (final UnsupportedOperationException ex) {
                for (final Map.Entry<K, Object> entry : this.a().entrySet()) {
                    if (b11.a(o, entry.getValue())) {
                        this.a().remove(entry.getKey());
                        return true;
                    }
                }
                return false;
            }
        }
        
        @Override
        public boolean removeAll(final Collection collection) {
            try {
                return super.removeAll((Collection<?>)i71.r(collection));
            }
            catch (final UnsupportedOperationException ex) {
                final HashSet c = Sets.c();
                for (final Map.Entry<K, Object> entry : this.a().entrySet()) {
                    if (collection.contains(entry.getValue())) {
                        c.add(entry.getKey());
                    }
                }
                return this.a().keySet().removeAll(c);
            }
        }
        
        @Override
        public boolean retainAll(final Collection collection) {
            try {
                return super.retainAll((Collection<?>)i71.r(collection));
            }
            catch (final UnsupportedOperationException ex) {
                final HashSet c = Sets.c();
                for (final Map.Entry<K, Object> entry : this.a().entrySet()) {
                    if (collection.contains(entry.getValue())) {
                        c.add(entry.getKey());
                    }
                }
                return this.a().keySet().retainAll(c);
            }
        }
        
        @Override
        public int size() {
            return this.a().size();
        }
    }
    
    public abstract static class u extends AbstractMap
    {
        public transient Set a;
        public transient Set b;
        public transient Collection c;
        
        abstract Set a();
        
        public Set b() {
            return new m(this);
        }
        
        public Collection c() {
            return new t(this);
        }
        
        @Override
        public Set entrySet() {
            Set a;
            if ((a = this.a) == null) {
                a = this.a();
                this.a = a;
            }
            return a;
        }
        
        @Override
        public Set keySet() {
            Set b;
            if ((b = this.b) == null) {
                b = this.b();
                this.b = b;
            }
            return b;
        }
        
        @Override
        public Collection values() {
            Collection c;
            if ((c = this.c) == null) {
                c = this.c();
                this.c = c;
            }
            return c;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.AbstractSequentialList;
import java.util.AbstractList;
import java.io.Serializable;
import java.util.Collection;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Iterator;
import java.util.RandomAccess;
import com.google.common.primitives.Ints;
import java.util.List;

public abstract class Lists
{
    public static List a(final Object o, final Object[] array) {
        return new OnePlusArrayList(o, array);
    }
    
    public static ImmutableList b(final String s) {
        return new StringAsImmutableList((String)i71.r(s));
    }
    
    public static int c(final int n) {
        hh.b(n, "arraySize");
        return Ints.k(n + 5L + n / 10);
    }
    
    public static boolean d(final List list, final Object o) {
        if (o == i71.r(list)) {
            return true;
        }
        if (!(o instanceof List)) {
            return false;
        }
        final List list2 = (List)o;
        final int size = list.size();
        if (size != list2.size()) {
            return false;
        }
        if (list instanceof RandomAccess && list2 instanceof RandomAccess) {
            for (int i = 0; i < size; ++i) {
                if (!b11.a(list.get(i), list2.get(i))) {
                    return false;
                }
            }
            return true;
        }
        return Iterators.g(list.iterator(), list2.iterator());
    }
    
    public static int e(final List list, final Object o) {
        if (list instanceof RandomAccess) {
            return f(list, o);
        }
        final ListIterator listIterator = list.listIterator();
        while (listIterator.hasNext()) {
            if (b11.a(o, listIterator.next())) {
                return listIterator.previousIndex();
            }
        }
        return -1;
    }
    
    public static int f(final List list, final Object o) {
        final int size = list.size();
        int i = 0;
        final int n = 0;
        if (o == null) {
            for (int j = n; j < size; ++j) {
                if (list.get(j) == null) {
                    return j;
                }
            }
        }
        else {
            while (i < size) {
                if (o.equals(list.get(i))) {
                    return i;
                }
                ++i;
            }
        }
        return -1;
    }
    
    public static int g(final List list, final Object o) {
        if (list instanceof RandomAccess) {
            return h(list, o);
        }
        final ListIterator listIterator = list.listIterator(list.size());
        while (listIterator.hasPrevious()) {
            if (b11.a(o, listIterator.previous())) {
                return listIterator.nextIndex();
            }
        }
        return -1;
    }
    
    public static int h(final List list, final Object o) {
        if (o == null) {
            for (int i = list.size() - 1; i >= 0; --i) {
                if (list.get(i) == null) {
                    return i;
                }
            }
        }
        else {
            for (int j = list.size() - 1; j >= 0; --j) {
                if (o.equals(list.get(j))) {
                    return j;
                }
            }
        }
        return -1;
    }
    
    public static ArrayList i() {
        return new ArrayList();
    }
    
    public static ArrayList j(final Iterable iterable) {
        i71.r(iterable);
        ArrayList k;
        if (iterable instanceof Collection) {
            k = new ArrayList((Collection<?>)iterable);
        }
        else {
            k = k(iterable.iterator());
        }
        return k;
    }
    
    public static ArrayList k(final Iterator iterator) {
        final ArrayList i = i();
        Iterators.a(i, iterator);
        return i;
    }
    
    public static ArrayList l(final int initialCapacity) {
        hh.b(initialCapacity, "initialArraySize");
        return new ArrayList(initialCapacity);
    }
    
    public static ArrayList m(final int n) {
        return new ArrayList(c(n));
    }
    
    public static List n(final List list, final m90 m90) {
        Serializable s;
        if (list instanceof RandomAccess) {
            s = new TransformingRandomAccessList<Object, Object>(list, m90);
        }
        else {
            s = new TransformingSequentialList<Object, Object>(list, m90);
        }
        return (List)s;
    }
    
    public static class OnePlusArrayList<E> extends AbstractList<E> implements Serializable, RandomAccess
    {
        private static final long serialVersionUID = 0L;
        final E first;
        final E[] rest;
        
        public OnePlusArrayList(final E first, final E[] array) {
            this.first = first;
            this.rest = (E[])i71.r(array);
        }
        
        @Override
        public E get(final int n) {
            i71.p(n, this.size());
            E first;
            if (n == 0) {
                first = this.first;
            }
            else {
                first = this.rest[n - 1];
            }
            return first;
        }
        
        @Override
        public int size() {
            return tf0.g(this.rest.length, 1);
        }
    }
    
    public static final class StringAsImmutableList extends ImmutableList<Character>
    {
        private final String string;
        
        public StringAsImmutableList(final String string) {
            this.string = string;
        }
        
        @Override
        public Character get(final int index) {
            i71.p(index, this.size());
            return this.string.charAt(index);
        }
        
        @Override
        public int indexOf(final Object o) {
            int index;
            if (o instanceof Character) {
                index = this.string.indexOf((char)o);
            }
            else {
                index = -1;
            }
            return index;
        }
        
        @Override
        public boolean isPartialView() {
            return false;
        }
        
        @Override
        public int lastIndexOf(final Object o) {
            int lastIndex;
            if (o instanceof Character) {
                lastIndex = this.string.lastIndexOf((char)o);
            }
            else {
                lastIndex = -1;
            }
            return lastIndex;
        }
        
        @Override
        public int size() {
            return this.string.length();
        }
        
        @Override
        public ImmutableList<Character> subList(final int beginIndex, final int endIndex) {
            i71.w(beginIndex, endIndex, this.size());
            return Lists.b(this.string.substring(beginIndex, endIndex));
        }
    }
    
    public static class TransformingRandomAccessList<F, T> extends AbstractList<T> implements RandomAccess, Serializable
    {
        private static final long serialVersionUID = 0L;
        final List<F> fromList;
        final m90 function;
        
        public TransformingRandomAccessList(final List<F> list, final m90 m90) {
            this.fromList = (List)i71.r(list);
            this.function = (m90)i71.r(m90);
        }
        
        @Override
        public void clear() {
            this.fromList.clear();
        }
        
        @Override
        public T get(final int n) {
            return (T)this.function.apply(this.fromList.get(n));
        }
        
        @Override
        public boolean isEmpty() {
            return this.fromList.isEmpty();
        }
        
        @Override
        public Iterator<T> iterator() {
            return this.listIterator();
        }
        
        @Override
        public ListIterator<T> listIterator(final int n) {
            return new s(this, this.fromList.listIterator(n)) {
                public final TransformingRandomAccessList b;
                
                public Object b(final Object o) {
                    return this.b.function.apply(o);
                }
            };
        }
        
        @Override
        public T remove(final int n) {
            return (T)this.function.apply(this.fromList.remove(n));
        }
        
        @Override
        public int size() {
            return this.fromList.size();
        }
    }
    
    public static class TransformingSequentialList<F, T> extends AbstractSequentialList<T> implements Serializable
    {
        private static final long serialVersionUID = 0L;
        final List<F> fromList;
        final m90 function;
        
        public TransformingSequentialList(final List<F> list, final m90 m90) {
            this.fromList = (List)i71.r(list);
            this.function = (m90)i71.r(m90);
        }
        
        @Override
        public void clear() {
            this.fromList.clear();
        }
        
        @Override
        public ListIterator<T> listIterator(final int n) {
            return new s(this, this.fromList.listIterator(n)) {
                public final TransformingSequentialList b;
                
                public Object b(final Object o) {
                    return this.b.function.apply(o);
                }
            };
        }
        
        @Override
        public int size() {
            return this.fromList.size();
        }
    }
    
    public static class TwoPlusArrayList<E> extends AbstractList<E> implements Serializable, RandomAccess
    {
        private static final long serialVersionUID = 0L;
        final E first;
        final E[] rest;
        final E second;
        
        public TwoPlusArrayList(final E first, final E second, final E[] array) {
            this.first = first;
            this.second = second;
            this.rest = (E[])i71.r(array);
        }
        
        @Override
        public E get(final int n) {
            if (n == 0) {
                return this.first;
            }
            if (n != 1) {
                i71.p(n, this.size());
                return this.rest[n - 2];
            }
            return this.second;
        }
        
        @Override
        public int size() {
            return tf0.g(this.rest.length, 2);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.ListIterator;

class RegularImmutableAsList<E> extends ImmutableAsList<E>
{
    private final ImmutableCollection<E> delegate;
    private final ImmutableList<? extends E> delegateList;
    
    public RegularImmutableAsList(final ImmutableCollection<E> delegate, final ImmutableList<? extends E> delegateList) {
        this.delegate = delegate;
        this.delegateList = delegateList;
    }
    
    public RegularImmutableAsList(final ImmutableCollection<E> collection, final Object[] array) {
        this(collection, ImmutableList.asImmutableList(array));
    }
    
    public RegularImmutableAsList(final ImmutableCollection<E> collection, final Object[] array, final int n) {
        this(collection, ImmutableList.asImmutableList(array, n));
    }
    
    @Override
    public int copyIntoArray(final Object[] array, final int n) {
        return this.delegateList.copyIntoArray(array, n);
    }
    
    @Override
    public ImmutableCollection<E> delegateCollection() {
        return this.delegate;
    }
    
    public ImmutableList<? extends E> delegateList() {
        return this.delegateList;
    }
    
    @Override
    public E get(final int n) {
        return (E)this.delegateList.get(n);
    }
    
    @Override
    public Object[] internalArray() {
        return this.delegateList.internalArray();
    }
    
    @Override
    public int internalArrayEnd() {
        return this.delegateList.internalArrayEnd();
    }
    
    @Override
    public int internalArrayStart() {
        return this.delegateList.internalArrayStart();
    }
    
    @Override
    public z02 listIterator(final int n) {
        return this.delegateList.listIterator(n);
    }
}

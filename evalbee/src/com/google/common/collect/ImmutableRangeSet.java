// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Collections;
import com.google.common.primitives.Ints;
import java.util.Objects;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.Comparator;
import java.util.List;
import java.util.Collection;
import java.io.Serializable;

public final class ImmutableRangeSet<C extends Comparable> extends y implements Serializable
{
    private static final ImmutableRangeSet<Comparable<?>> ALL;
    private static final ImmutableRangeSet<Comparable<?>> EMPTY;
    private transient ImmutableRangeSet<C> complement;
    private final transient ImmutableList<Range<C>> ranges;
    
    static {
        EMPTY = new ImmutableRangeSet<Comparable<?>>(ImmutableList.of());
        ALL = new ImmutableRangeSet<Comparable<?>>((ImmutableList<Range<Comparable<?>>>)ImmutableList.of(Range.all()));
    }
    
    public ImmutableRangeSet(final ImmutableList<Range<C>> ranges) {
        this.ranges = ranges;
    }
    
    private ImmutableRangeSet(final ImmutableList<Range<C>> ranges, final ImmutableRangeSet<C> complement) {
        this.ranges = ranges;
        this.complement = complement;
    }
    
    public static /* synthetic */ ImmutableList access$000(final ImmutableRangeSet set) {
        return set.ranges;
    }
    
    public static <C extends Comparable> ImmutableRangeSet<C> all() {
        return (ImmutableRangeSet<C>)ImmutableRangeSet.ALL;
    }
    
    public static <C extends Comparable<?>> a builder() {
        return new a();
    }
    
    public static <C extends Comparable> ImmutableRangeSet<C> copyOf(final gc1 gc1) {
        i71.r(gc1);
        if (gc1.isEmpty()) {
            return of();
        }
        if (gc1.encloses(Range.all())) {
            return all();
        }
        if (gc1 instanceof ImmutableRangeSet) {
            final ImmutableRangeSet set = (ImmutableRangeSet)gc1;
            if (!set.isPartialView()) {
                return set;
            }
        }
        return new ImmutableRangeSet<C>(ImmutableList.copyOf((Collection<? extends Range<C>>)gc1.asRanges()));
    }
    
    public static <C extends Comparable<?>> ImmutableRangeSet<C> copyOf(final Iterable<Range<C>> iterable) {
        return new a().b(iterable).c();
    }
    
    private ImmutableList<Range<C>> intersectRanges(final Range<C> range) {
        if (this.ranges.isEmpty() || range.isEmpty()) {
            return ImmutableList.of();
        }
        if (range.encloses(this.span())) {
            return this.ranges;
        }
        int a;
        if (range.hasLowerBound()) {
            a = SortedLists.a(this.ranges, Range.upperBoundFn(), range.lowerBound, SortedLists.KeyPresentBehavior.FIRST_AFTER, SortedLists.KeyAbsentBehavior.NEXT_HIGHER);
        }
        else {
            a = 0;
        }
        int n;
        if (range.hasUpperBound()) {
            n = SortedLists.a(this.ranges, Range.lowerBoundFn(), range.upperBound, SortedLists.KeyPresentBehavior.FIRST_PRESENT, SortedLists.KeyAbsentBehavior.NEXT_HIGHER);
        }
        else {
            n = this.ranges.size();
        }
        final int n2 = n - a;
        if (n2 == 0) {
            return ImmutableList.of();
        }
        return new ImmutableList<Range<C>>(this, n2, a, range) {
            final ImmutableRangeSet this$0;
            final int val$fromIndex;
            final int val$length;
            final Range val$range;
            
            @Override
            public Range<C> get(final int n) {
                i71.p(n, this.val$length);
                if (n != 0 && n != this.val$length - 1) {
                    return (Range)ImmutableRangeSet.access$000(this.this$0).get(n + this.val$fromIndex);
                }
                return ((Range)ImmutableRangeSet.access$000(this.this$0).get(n + this.val$fromIndex)).intersection(this.val$range);
            }
            
            @Override
            public boolean isPartialView() {
                return true;
            }
            
            @Override
            public int size() {
                return this.val$length;
            }
        };
    }
    
    public static <C extends Comparable> ImmutableRangeSet<C> of() {
        return (ImmutableRangeSet<C>)ImmutableRangeSet.EMPTY;
    }
    
    public static <C extends Comparable> ImmutableRangeSet<C> of(final Range<C> range) {
        i71.r(range);
        if (range.isEmpty()) {
            return of();
        }
        if (range.equals(Range.all())) {
            return all();
        }
        return new ImmutableRangeSet<C>((ImmutableList<Range<C>>)ImmutableList.of(range));
    }
    
    public static <C extends Comparable<?>> ImmutableRangeSet<C> unionOf(final Iterable<Range<C>> iterable) {
        return copyOf(TreeRangeSet.create(iterable));
    }
    
    @Deprecated
    @Override
    public void add(final Range<C> range) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public void addAll(final gc1 gc1) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public void addAll(final Iterable<Range<C>> iterable) {
        throw new UnsupportedOperationException();
    }
    
    public ImmutableSet<Range<C>> asDescendingSetOfRanges() {
        if (this.ranges.isEmpty()) {
            return ImmutableSet.of();
        }
        return new RegularImmutableSortedSet<Range<C>>(this.ranges.reverse(), Range.rangeLexOrdering().reverse());
    }
    
    @Override
    public ImmutableSet<Range<C>> asRanges() {
        if (this.ranges.isEmpty()) {
            return ImmutableSet.of();
        }
        return new RegularImmutableSortedSet<Range<C>>(this.ranges, Range.rangeLexOrdering());
    }
    
    public ImmutableSortedSet<C> asSet(final DiscreteDomain discreteDomain) {
        i71.r(discreteDomain);
        if (this.isEmpty()) {
            return ImmutableSortedSet.of();
        }
        final Range<C> canonical = this.span().canonical(discreteDomain);
        if (canonical.hasLowerBound()) {
            if (!canonical.hasUpperBound()) {
                try {
                    discreteDomain.maxValue();
                }
                catch (final NoSuchElementException ex) {
                    throw new IllegalArgumentException("Neither the DiscreteDomain nor this range set are bounded above");
                }
            }
            return new AsSet(discreteDomain);
        }
        throw new IllegalArgumentException("Neither the DiscreteDomain nor this range set are bounded below");
    }
    
    @Override
    public ImmutableRangeSet<C> complement() {
        final ImmutableRangeSet<C> complement = this.complement;
        if (complement != null) {
            return complement;
        }
        ImmutableRangeSet<Comparable> complement2;
        if (this.ranges.isEmpty()) {
            complement2 = (ImmutableRangeSet<Comparable>)all();
        }
        else {
            if (this.ranges.size() != 1 || !this.ranges.get(0).equals(Range.all())) {
                return this.complement = new ImmutableRangeSet<C>(new ComplementRanges(), this);
            }
            complement2 = (ImmutableRangeSet<Comparable>)of();
        }
        return this.complement = (ImmutableRangeSet<C>)complement2;
    }
    
    public ImmutableRangeSet<C> difference(final gc1 gc1) {
        final TreeRangeSet<Comparable> create = TreeRangeSet.create(this);
        create.removeAll(gc1);
        return (ImmutableRangeSet<C>)copyOf(create);
    }
    
    @Override
    public boolean encloses(final Range<C> range) {
        final int b = SortedLists.b(this.ranges, Range.lowerBoundFn(), range.lowerBound, Ordering.natural(), SortedLists.KeyPresentBehavior.ANY_PRESENT, SortedLists.KeyAbsentBehavior.NEXT_LOWER);
        return b != -1 && ((Range<C>)this.ranges.get(b)).encloses(range);
    }
    
    public ImmutableRangeSet<C> intersection(final gc1 gc1) {
        final TreeRangeSet<Comparable> create = TreeRangeSet.create(this);
        create.removeAll(gc1.complement());
        return (ImmutableRangeSet<C>)copyOf(create);
    }
    
    public boolean intersects(final Range<C> range) {
        final int b = SortedLists.b(this.ranges, Range.lowerBoundFn(), range.lowerBound, Ordering.natural(), SortedLists.KeyPresentBehavior.ANY_PRESENT, SortedLists.KeyAbsentBehavior.NEXT_HIGHER);
        final int size = this.ranges.size();
        boolean b2 = true;
        if (b < size && this.ranges.get(b).isConnected(range) && !this.ranges.get(b).intersection(range).isEmpty()) {
            return true;
        }
        if (b > 0) {
            final ImmutableList<Range<C>> ranges = this.ranges;
            final int n = b - 1;
            if (((Range<C>)ranges.get(n)).isConnected(range) && !((Range<C>)this.ranges.get(n)).intersection(range).isEmpty()) {
                return b2;
            }
        }
        b2 = false;
        return b2;
    }
    
    @Override
    public boolean isEmpty() {
        return this.ranges.isEmpty();
    }
    
    public boolean isPartialView() {
        return this.ranges.isPartialView();
    }
    
    @Override
    public Range<C> rangeContaining(final C c) {
        final int b = SortedLists.b(this.ranges, Range.lowerBoundFn(), Cut.belowValue(c), Ordering.natural(), SortedLists.KeyPresentBehavior.ANY_PRESENT, SortedLists.KeyAbsentBehavior.NEXT_LOWER);
        Range<C> range = null;
        if (b != -1) {
            final Range range2 = this.ranges.get(b);
            range = range;
            if (range2.contains(c)) {
                range = range2;
            }
        }
        return range;
    }
    
    @Deprecated
    @Override
    public void remove(final Range<C> range) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public void removeAll(final gc1 gc1) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public void removeAll(final Iterable<Range<C>> iterable) {
        throw new UnsupportedOperationException();
    }
    
    public Range<C> span() {
        if (!this.ranges.isEmpty()) {
            final Cut<C> lowerBound = this.ranges.get(0).lowerBound;
            final ImmutableList<Range<C>> ranges = this.ranges;
            return Range.create((Cut<C>)lowerBound, (Cut<C>)((Range)ranges.get(ranges.size() - 1)).upperBound);
        }
        throw new NoSuchElementException();
    }
    
    public ImmutableRangeSet<C> subRangeSet(final Range<C> range) {
        if (!this.isEmpty()) {
            final Range<C> span = this.span();
            if (range.encloses(span)) {
                return this;
            }
            if (range.isConnected(span)) {
                return new ImmutableRangeSet<C>(this.intersectRanges(range));
            }
        }
        return of();
    }
    
    public ImmutableRangeSet<C> union(final gc1 gc1) {
        return unionOf((Iterable<Range<C>>)rg0.c(this.asRanges(), gc1.asRanges()));
    }
    
    public Object writeReplace() {
        return new SerializedForm((ImmutableList<Range<Comparable>>)this.ranges);
    }
    
    public final class AsSet extends ImmutableSortedSet<C>
    {
        private final DiscreteDomain domain;
        private transient Integer size;
        final ImmutableRangeSet this$0;
        
        public AsSet(final ImmutableRangeSet this$0, final DiscreteDomain domain) {
            this.this$0 = this$0;
            super(Ordering.natural());
            this.domain = domain;
        }
        
        public static /* synthetic */ DiscreteDomain access$100(final AsSet set) {
            return set.domain;
        }
        
        @Override
        public boolean contains(final Object o) {
            if (o == null) {
                return false;
            }
            try {
                return this.this$0.contains((Comparable)o);
            }
            catch (final ClassCastException ex) {
                return false;
            }
        }
        
        @Override
        public ImmutableSortedSet<C> createDescendingSet() {
            return new DescendingImmutableSortedSet<C>(this);
        }
        
        @Override
        public w02 descendingIterator() {
            return new AbstractIterator(this) {
                public final Iterator c = ImmutableRangeSet.access$000(e.this$0).reverse().iterator();
                public Iterator d = Iterators.h();
                public final AsSet e;
                
                public Comparable e() {
                    while (!this.d.hasNext()) {
                        if (!this.c.hasNext()) {
                            final Object o = this.c();
                            return (Comparable)o;
                        }
                        this.d = ContiguousSet.create(this.c.next(), AsSet.access$100(this.e)).descendingIterator();
                    }
                    final Object o = this.d.next();
                    return (Comparable)o;
                }
            };
        }
        
        @Override
        public ImmutableSortedSet<C> headSetImpl(final C c, final boolean b) {
            return this.subSet(Range.upTo(c, BoundType.forBoolean(b)));
        }
        
        @Override
        public int indexOf(final Object obj) {
            if (this.contains(obj)) {
                Objects.requireNonNull(obj);
                final Comparable comparable = (Comparable)obj;
                final w02 iterator = ImmutableRangeSet.access$000(this.this$0).iterator();
                long n = 0L;
                while (iterator.hasNext()) {
                    final Range range = iterator.next();
                    if (range.contains(comparable)) {
                        return Ints.k(n + ContiguousSet.create((Range<Comparable>)range, this.domain).indexOf(comparable));
                    }
                    n += ContiguousSet.create((Range<Comparable>)range, this.domain).size();
                }
                throw new AssertionError((Object)"impossible");
            }
            return -1;
        }
        
        @Override
        public boolean isPartialView() {
            return ImmutableRangeSet.access$000(this.this$0).isPartialView();
        }
        
        @Override
        public w02 iterator() {
            return new AbstractIterator(this) {
                public final Iterator c = ImmutableRangeSet.access$000(e.this$0).iterator();
                public Iterator d = Iterators.h();
                public final AsSet e;
                
                public Comparable e() {
                    while (!this.d.hasNext()) {
                        if (!this.c.hasNext()) {
                            final Object o = this.c();
                            return (Comparable)o;
                        }
                        this.d = ContiguousSet.create(this.c.next(), AsSet.access$100(this.e)).iterator();
                    }
                    final Object o = this.d.next();
                    return (Comparable)o;
                }
            };
        }
        
        @Override
        public int size() {
            Integer size;
            if ((size = this.size) == null) {
                final w02 iterator = ImmutableRangeSet.access$000(this.this$0).iterator();
                long n = 0L;
                long n2;
                do {
                    n2 = n;
                    if (!iterator.hasNext()) {
                        break;
                    }
                    n2 = (n += ContiguousSet.create(iterator.next(), this.domain).size());
                } while (n2 < 2147483647L);
                size = Ints.k(n2);
                this.size = size;
            }
            return size;
        }
        
        public ImmutableSortedSet<C> subSet(final Range<C> range) {
            return this.this$0.subRangeSet(range).asSet(this.domain);
        }
        
        @Override
        public ImmutableSortedSet<C> subSetImpl(final C c, final boolean b, final C c2, final boolean b2) {
            if (!b && !b2 && Range.compareOrThrow(c, c2) == 0) {
                return ImmutableSortedSet.of();
            }
            return this.subSet(Range.range(c, BoundType.forBoolean(b), c2, BoundType.forBoolean(b2)));
        }
        
        @Override
        public ImmutableSortedSet<C> tailSetImpl(final C c, final boolean b) {
            return this.subSet(Range.downTo(c, BoundType.forBoolean(b)));
        }
        
        @Override
        public String toString() {
            return ImmutableRangeSet.access$000(this.this$0).toString();
        }
        
        @Override
        public Object writeReplace() {
            return new AsSetSerializedForm(ImmutableRangeSet.access$000(this.this$0), this.domain);
        }
    }
    
    public static class AsSetSerializedForm<C extends Comparable> implements Serializable
    {
        private final DiscreteDomain domain;
        private final ImmutableList<Range<C>> ranges;
        
        public AsSetSerializedForm(final ImmutableList<Range<C>> ranges, final DiscreteDomain domain) {
            this.ranges = ranges;
            this.domain = domain;
        }
        
        public Object readResolve() {
            return new ImmutableRangeSet<Comparable>((ImmutableList<Range<Comparable>>)this.ranges).asSet(this.domain);
        }
    }
    
    public final class ComplementRanges extends ImmutableList<Range<C>>
    {
        private final boolean positiveBoundedAbove;
        private final boolean positiveBoundedBelow;
        private final int size;
        final ImmutableRangeSet this$0;
        
        public ComplementRanges(final ImmutableRangeSet this$0) {
            this.this$0 = this$0;
            final boolean hasLowerBound = ((Range)ImmutableRangeSet.access$000(this$0).get(0)).hasLowerBound();
            this.positiveBoundedBelow = hasLowerBound;
            final boolean hasUpperBound = ((Range)rg0.f(ImmutableRangeSet.access$000(this$0))).hasUpperBound();
            this.positiveBoundedAbove = hasUpperBound;
            int n2;
            final int n = n2 = ImmutableRangeSet.access$000(this$0).size() - 1;
            if (hasLowerBound) {
                n2 = n + 1;
            }
            int size = n2;
            if (hasUpperBound) {
                size = n2 + 1;
            }
            this.size = size;
        }
        
        @Override
        public Range<C> get(final int n) {
            i71.p(n, this.size);
            Cut<Comparable> cut = null;
            Label_0068: {
                Object o;
                if (this.positiveBoundedBelow) {
                    if (n == 0) {
                        cut = (Cut<Comparable>)Cut.belowAll();
                        break Label_0068;
                    }
                    o = ImmutableRangeSet.access$000(this.this$0).get(n - 1);
                }
                else {
                    o = ImmutableRangeSet.access$000(this.this$0).get(n);
                }
                cut = (Cut<Comparable>)((Range)o).upperBound;
            }
            Cut<Comparable> cut2;
            if (this.positiveBoundedAbove && n == this.size - 1) {
                cut2 = (Cut<Comparable>)Cut.aboveAll();
            }
            else {
                cut2 = (Cut<Comparable>)((Range)ImmutableRangeSet.access$000(this.this$0).get(n + ((this.positiveBoundedBelow ^ true) ? 1 : 0))).lowerBound;
            }
            return Range.create((Cut<C>)cut, (Cut<C>)cut2);
        }
        
        @Override
        public boolean isPartialView() {
            return true;
        }
        
        @Override
        public int size() {
            return this.size;
        }
    }
    
    public static final class SerializedForm<C extends Comparable> implements Serializable
    {
        private final ImmutableList<Range<C>> ranges;
        
        public SerializedForm(final ImmutableList<Range<C>> ranges) {
            this.ranges = ranges;
        }
        
        public Object readResolve() {
            if (this.ranges.isEmpty()) {
                return ImmutableRangeSet.of();
            }
            if (this.ranges.equals(ImmutableList.of(Range.all()))) {
                return ImmutableRangeSet.all();
            }
            return new ImmutableRangeSet((ImmutableList<Range<Comparable>>)this.ranges);
        }
    }
    
    public static class a
    {
        public final List a;
        
        public a() {
            this.a = Lists.i();
        }
        
        public a a(final Range range) {
            i71.m(range.isEmpty() ^ true, "range must not be empty, but was %s", range);
            this.a.add(range);
            return this;
        }
        
        public a b(final Iterable iterable) {
            final Iterator iterator = iterable.iterator();
            while (iterator.hasNext()) {
                this.a((Range)iterator.next());
            }
            return this;
        }
        
        public ImmutableRangeSet c() {
            final ImmutableList.a a = new ImmutableList.a(this.a.size());
            Collections.sort((List<Object>)this.a, Range.rangeLexOrdering());
            final b51 p = Iterators.p(this.a.iterator());
            while (p.hasNext()) {
                Range span = (Range)p.next();
                while (p.hasNext()) {
                    final Range range = (Range)p.peek();
                    if (!span.isConnected(range)) {
                        break;
                    }
                    i71.n(span.intersection(range).isEmpty(), "Overlapping ranges not permitted but found %s overlapping %s", span, range);
                    span = span.span((Range)p.next());
                }
                a.i(span);
            }
            final ImmutableList l = a.l();
            if (l.isEmpty()) {
                return ImmutableRangeSet.of();
            }
            if (l.size() == 1 && ((Range)rg0.h(l)).equals(Range.all())) {
                return ImmutableRangeSet.all();
            }
            return new ImmutableRangeSet(l);
        }
    }
}

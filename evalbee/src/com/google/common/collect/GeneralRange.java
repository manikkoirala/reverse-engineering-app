// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Comparator;
import java.io.Serializable;

final class GeneralRange<T> implements Serializable
{
    private final Comparator<? super T> comparator;
    private final boolean hasLowerBound;
    private final boolean hasUpperBound;
    private final BoundType lowerBoundType;
    private final T lowerEndpoint;
    private transient GeneralRange<T> reverse;
    private final BoundType upperBoundType;
    private final T upperEndpoint;
    
    private GeneralRange(final Comparator<? super T> comparator, final boolean hasLowerBound, final T lowerEndpoint, final BoundType boundType, final boolean hasUpperBound, final T upperEndpoint, final BoundType boundType2) {
        this.comparator = (Comparator)i71.r(comparator);
        this.hasLowerBound = hasLowerBound;
        this.hasUpperBound = hasUpperBound;
        this.lowerEndpoint = lowerEndpoint;
        this.lowerBoundType = (BoundType)i71.r(boundType);
        this.upperEndpoint = upperEndpoint;
        this.upperBoundType = (BoundType)i71.r(boundType2);
        if (hasLowerBound) {
            comparator.compare((Object)k01.a(lowerEndpoint), (Object)k01.a(lowerEndpoint));
        }
        if (hasUpperBound) {
            comparator.compare((Object)k01.a(upperEndpoint), (Object)k01.a(upperEndpoint));
        }
        if (hasLowerBound && hasUpperBound) {
            final int compare = comparator.compare((Object)k01.a(lowerEndpoint), (Object)k01.a(upperEndpoint));
            final boolean b = true;
            i71.n(compare <= 0, "lowerEndpoint (%s) > upperEndpoint (%s)", lowerEndpoint, upperEndpoint);
            if (compare == 0) {
                final BoundType open = BoundType.OPEN;
                boolean b2 = b;
                if (boundType == open) {
                    b2 = (boundType2 != open && b);
                }
                i71.d(b2);
            }
        }
    }
    
    public static <T> GeneralRange<T> all(final Comparator<? super T> comparator) {
        final BoundType open = BoundType.OPEN;
        return new GeneralRange<T>((Comparator<? super Object>)comparator, false, null, open, false, null, open);
    }
    
    public static <T> GeneralRange<T> downTo(final Comparator<? super T> comparator, final T t, final BoundType boundType) {
        return new GeneralRange<T>(comparator, true, t, boundType, false, null, BoundType.OPEN);
    }
    
    public static <T extends Comparable> GeneralRange<T> from(final Range<T> range) {
        final boolean hasLowerBound = range.hasLowerBound();
        Comparable upperEndpoint = null;
        Comparable lowerEndpoint;
        if (hasLowerBound) {
            lowerEndpoint = range.lowerEndpoint();
        }
        else {
            lowerEndpoint = null;
        }
        BoundType boundType;
        if (range.hasLowerBound()) {
            boundType = range.lowerBoundType();
        }
        else {
            boundType = BoundType.OPEN;
        }
        if (range.hasUpperBound()) {
            upperEndpoint = range.upperEndpoint();
        }
        BoundType boundType2;
        if (range.hasUpperBound()) {
            boundType2 = range.upperBoundType();
        }
        else {
            boundType2 = BoundType.OPEN;
        }
        return new GeneralRange<T>(Ordering.natural(), range.hasLowerBound(), lowerEndpoint, boundType, range.hasUpperBound(), upperEndpoint, boundType2);
    }
    
    public static <T> GeneralRange<T> range(final Comparator<? super T> comparator, final T t, final BoundType boundType, final T t2, final BoundType boundType2) {
        return new GeneralRange<T>(comparator, true, t, boundType, true, t2, boundType2);
    }
    
    public static <T> GeneralRange<T> upTo(final Comparator<? super T> comparator, final T t, final BoundType boundType) {
        return new GeneralRange<T>(comparator, false, null, BoundType.OPEN, true, t, boundType);
    }
    
    public Comparator<? super T> comparator() {
        return this.comparator;
    }
    
    public boolean contains(final T t) {
        return !this.tooLow(t) && !this.tooHigh(t);
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof GeneralRange;
        boolean b3;
        final boolean b2 = b3 = false;
        if (b) {
            final GeneralRange generalRange = (GeneralRange)o;
            b3 = b2;
            if (this.comparator.equals(generalRange.comparator)) {
                b3 = b2;
                if (this.hasLowerBound == generalRange.hasLowerBound) {
                    b3 = b2;
                    if (this.hasUpperBound == generalRange.hasUpperBound) {
                        b3 = b2;
                        if (this.getLowerBoundType().equals(generalRange.getLowerBoundType())) {
                            b3 = b2;
                            if (this.getUpperBoundType().equals(generalRange.getUpperBoundType())) {
                                b3 = b2;
                                if (b11.a(this.getLowerEndpoint(), generalRange.getLowerEndpoint())) {
                                    b3 = b2;
                                    if (b11.a(this.getUpperEndpoint(), generalRange.getUpperEndpoint())) {
                                        b3 = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return b3;
    }
    
    public BoundType getLowerBoundType() {
        return this.lowerBoundType;
    }
    
    public T getLowerEndpoint() {
        return this.lowerEndpoint;
    }
    
    public BoundType getUpperBoundType() {
        return this.upperBoundType;
    }
    
    public T getUpperEndpoint() {
        return this.upperEndpoint;
    }
    
    public boolean hasLowerBound() {
        return this.hasLowerBound;
    }
    
    public boolean hasUpperBound() {
        return this.hasUpperBound;
    }
    
    @Override
    public int hashCode() {
        return b11.b(this.comparator, this.getLowerEndpoint(), this.getLowerBoundType(), this.getUpperEndpoint(), this.getUpperBoundType());
    }
    
    public GeneralRange<T> intersect(final GeneralRange<T> generalRange) {
        i71.r(generalRange);
        i71.d(this.comparator.equals(generalRange.comparator));
        final boolean hasLowerBound = this.hasLowerBound;
        final T lowerEndpoint = this.getLowerEndpoint();
        final BoundType lowerBoundType = this.getLowerBoundType();
        boolean b = false;
        Object lowerEndpoint2 = null;
        BoundType lowerBoundType2 = null;
        Label_0157: {
            boolean hasLowerBound2;
            if (!this.hasLowerBound()) {
                hasLowerBound2 = generalRange.hasLowerBound;
            }
            else {
                b = hasLowerBound;
                lowerEndpoint2 = lowerEndpoint;
                lowerBoundType2 = lowerBoundType;
                if (!generalRange.hasLowerBound()) {
                    break Label_0157;
                }
                final int compare = this.comparator.compare(this.getLowerEndpoint(), generalRange.getLowerEndpoint());
                hasLowerBound2 = hasLowerBound;
                if (compare >= 0) {
                    b = hasLowerBound;
                    lowerEndpoint2 = lowerEndpoint;
                    lowerBoundType2 = lowerBoundType;
                    if (compare != 0) {
                        break Label_0157;
                    }
                    b = hasLowerBound;
                    lowerEndpoint2 = lowerEndpoint;
                    lowerBoundType2 = lowerBoundType;
                    if (generalRange.getLowerBoundType() != BoundType.OPEN) {
                        break Label_0157;
                    }
                    hasLowerBound2 = hasLowerBound;
                }
            }
            lowerEndpoint2 = generalRange.getLowerEndpoint();
            lowerBoundType2 = generalRange.getLowerBoundType();
            b = hasLowerBound2;
        }
        final boolean hasUpperBound = this.hasUpperBound;
        final T upperEndpoint = this.getUpperEndpoint();
        final BoundType upperBoundType = this.getUpperBoundType();
        boolean hasUpperBound2 = false;
        Object upperEndpoint2 = null;
        BoundType upperBoundType2 = null;
        Label_0287: {
            if (!this.hasUpperBound()) {
                hasUpperBound2 = generalRange.hasUpperBound;
            }
            else {
                hasUpperBound2 = hasUpperBound;
                upperEndpoint2 = upperEndpoint;
                upperBoundType2 = upperBoundType;
                if (!generalRange.hasUpperBound()) {
                    break Label_0287;
                }
                final int compare2 = this.comparator.compare(this.getUpperEndpoint(), generalRange.getUpperEndpoint());
                hasUpperBound2 = hasUpperBound;
                if (compare2 <= 0) {
                    hasUpperBound2 = hasUpperBound;
                    upperEndpoint2 = upperEndpoint;
                    upperBoundType2 = upperBoundType;
                    if (compare2 != 0) {
                        break Label_0287;
                    }
                    hasUpperBound2 = hasUpperBound;
                    upperEndpoint2 = upperEndpoint;
                    upperBoundType2 = upperBoundType;
                    if (generalRange.getUpperBoundType() != BoundType.OPEN) {
                        break Label_0287;
                    }
                    hasUpperBound2 = hasUpperBound;
                }
            }
            upperEndpoint2 = generalRange.getUpperEndpoint();
            upperBoundType2 = generalRange.getUpperBoundType();
        }
        Label_0356: {
            if (b && hasUpperBound2) {
                final int compare3 = this.comparator.compare((Object)lowerEndpoint2, (Object)upperEndpoint2);
                if (compare3 <= 0) {
                    if (compare3 != 0) {
                        break Label_0356;
                    }
                    final BoundType open = BoundType.OPEN;
                    if (lowerBoundType2 != open || upperBoundType2 != open) {
                        break Label_0356;
                    }
                }
                final BoundType open2 = BoundType.OPEN;
                final BoundType closed = BoundType.CLOSED;
                final Object o = upperEndpoint2;
                lowerBoundType2 = open2;
                final BoundType boundType = closed;
                return new GeneralRange<T>((Comparator<? super Object>)this.comparator, b, o, lowerBoundType2, hasUpperBound2, upperEndpoint2, boundType);
            }
        }
        final BoundType boundType = upperBoundType2;
        final Object o = lowerEndpoint2;
        return new GeneralRange<T>((Comparator<? super Object>)this.comparator, b, o, lowerBoundType2, hasUpperBound2, upperEndpoint2, boundType);
    }
    
    public boolean isEmpty() {
        return (this.hasUpperBound() && this.tooLow(k01.a(this.getUpperEndpoint()))) || (this.hasLowerBound() && this.tooHigh(k01.a(this.getLowerEndpoint())));
    }
    
    public GeneralRange<T> reverse() {
        GeneralRange<T> reverse;
        if ((reverse = this.reverse) == null) {
            reverse = new GeneralRange<T>(Ordering.from(this.comparator).reverse(), this.hasUpperBound, this.getUpperEndpoint(), this.getUpperBoundType(), this.hasLowerBound, this.getLowerEndpoint(), this.getLowerBoundType());
            reverse.reverse = this;
            this.reverse = reverse;
        }
        return reverse;
    }
    
    @Override
    public String toString() {
        final String value = String.valueOf(this.comparator);
        final BoundType lowerBoundType = this.lowerBoundType;
        final BoundType closed = BoundType.CLOSED;
        char c;
        if (lowerBoundType == closed) {
            c = '[';
        }
        else {
            c = '(';
        }
        Object lowerEndpoint;
        if (this.hasLowerBound) {
            lowerEndpoint = this.lowerEndpoint;
        }
        else {
            lowerEndpoint = "-\u221e";
        }
        final String value2 = String.valueOf(lowerEndpoint);
        Object upperEndpoint;
        if (this.hasUpperBound) {
            upperEndpoint = this.upperEndpoint;
        }
        else {
            upperEndpoint = "\u221e";
        }
        final String value3 = String.valueOf(upperEndpoint);
        char c2;
        if (this.upperBoundType == closed) {
            c2 = ']';
        }
        else {
            c2 = ')';
        }
        final StringBuilder sb = new StringBuilder(value.length() + 4 + value2.length() + value3.length());
        sb.append(value);
        sb.append(":");
        sb.append(c);
        sb.append(value2);
        sb.append(',');
        sb.append(value3);
        sb.append(c2);
        return sb.toString();
    }
    
    public boolean tooHigh(final T t) {
        final boolean hasUpperBound = this.hasUpperBound();
        boolean b = false;
        if (!hasUpperBound) {
            return false;
        }
        final int compare = this.comparator.compare((Object)t, (Object)k01.a(this.getUpperEndpoint()));
        final boolean b2 = compare > 0;
        final boolean b3 = compare == 0;
        if (this.getUpperBoundType() == BoundType.OPEN) {
            b = true;
        }
        return (b3 & b) | b2;
    }
    
    public boolean tooLow(final T t) {
        final boolean hasLowerBound = this.hasLowerBound();
        boolean b = false;
        if (!hasLowerBound) {
            return false;
        }
        final int compare = this.comparator.compare((Object)t, (Object)k01.a(this.getLowerEndpoint()));
        final boolean b2 = compare < 0;
        final boolean b3 = compare == 0;
        if (this.getLowerBoundType() == BoundType.OPEN) {
            b = true;
        }
        return (b3 & b) | b2;
    }
}

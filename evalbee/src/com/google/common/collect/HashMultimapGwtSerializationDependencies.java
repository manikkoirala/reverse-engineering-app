// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Collection;
import java.util.Map;

abstract class HashMultimapGwtSerializationDependencies<K, V> extends AbstractSetMultimap<K, V>
{
    public HashMultimapGwtSerializationDependencies(final Map<K, Collection<V>> map) {
        super(map);
    }
}

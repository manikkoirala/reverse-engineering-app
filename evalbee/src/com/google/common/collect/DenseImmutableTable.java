// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Map;
import java.util.Objects;
import java.util.Collection;

final class DenseImmutableTable<R, C, V> extends RegularImmutableTable<R, C, V>
{
    private final int[] cellColumnIndices;
    private final int[] cellRowIndices;
    private final int[] columnCounts;
    private final ImmutableMap<C, Integer> columnKeyToIndex;
    private final ImmutableMap<C, ImmutableMap<R, V>> columnMap;
    private final int[] rowCounts;
    private final ImmutableMap<R, Integer> rowKeyToIndex;
    private final ImmutableMap<R, ImmutableMap<C, V>> rowMap;
    private final V[][] values;
    
    public DenseImmutableTable(final ImmutableList<q.a> list, final ImmutableSet<R> set, final ImmutableSet<C> set2) {
        this.values = (V[][])new Object[set.size()][set2.size()];
        final ImmutableMap k = Maps.k(set);
        this.rowKeyToIndex = k;
        final ImmutableMap i = Maps.k(set2);
        this.columnKeyToIndex = i;
        this.rowCounts = new int[k.size()];
        this.columnCounts = new int[i.size()];
        final int[] cellRowIndices = new int[list.size()];
        final int[] cellColumnIndices = new int[list.size()];
        for (int j = 0; j < list.size(); ++j) {
            final q.a a = (q.a)list.get(j);
            final Object rowKey = a.getRowKey();
            final Object columnKey = a.getColumnKey();
            final Integer obj = this.rowKeyToIndex.get(rowKey);
            Objects.requireNonNull(obj);
            final int intValue = obj;
            final Integer obj2 = this.columnKeyToIndex.get(columnKey);
            Objects.requireNonNull(obj2);
            final int intValue2 = obj2;
            this.checkNoDuplicate((R)rowKey, (C)columnKey, this.values[intValue][intValue2], (V)a.getValue());
            this.values[intValue][intValue2] = (V)a.getValue();
            final int[] rowCounts = this.rowCounts;
            ++rowCounts[intValue];
            final int[] columnCounts = this.columnCounts;
            ++columnCounts[intValue2];
            cellRowIndices[j] = intValue;
            cellColumnIndices[j] = intValue2;
        }
        this.cellRowIndices = cellRowIndices;
        this.cellColumnIndices = cellColumnIndices;
        this.rowMap = new RowMap(null);
        this.columnMap = new ColumnMap(null);
    }
    
    public static /* synthetic */ int[] access$200(final DenseImmutableTable denseImmutableTable) {
        return denseImmutableTable.rowCounts;
    }
    
    public static /* synthetic */ ImmutableMap access$300(final DenseImmutableTable denseImmutableTable) {
        return denseImmutableTable.columnKeyToIndex;
    }
    
    public static /* synthetic */ Object[][] access$400(final DenseImmutableTable denseImmutableTable) {
        return denseImmutableTable.values;
    }
    
    public static /* synthetic */ int[] access$500(final DenseImmutableTable denseImmutableTable) {
        return denseImmutableTable.columnCounts;
    }
    
    public static /* synthetic */ ImmutableMap access$600(final DenseImmutableTable denseImmutableTable) {
        return denseImmutableTable.rowKeyToIndex;
    }
    
    @Override
    public ImmutableMap<C, Map<R, V>> columnMap() {
        return ImmutableMap.copyOf((Map<? extends C, ? extends Map<R, V>>)this.columnMap);
    }
    
    @Override
    public SerializedForm createSerializedForm() {
        return SerializedForm.create(this, this.cellRowIndices, this.cellColumnIndices);
    }
    
    @Override
    public V get(Object o, final Object o2) {
        final Integer n = this.rowKeyToIndex.get(o);
        final Integer n2 = this.columnKeyToIndex.get(o2);
        if (n != null && n2 != null) {
            o = this.values[n][n2];
        }
        else {
            o = null;
        }
        return (V)o;
    }
    
    @Override
    public q.a getCell(int n) {
        final int n2 = this.cellRowIndices[n];
        n = this.cellColumnIndices[n];
        final Object value = this.rowKeySet().asList().get(n2);
        final Object value2 = this.columnKeySet().asList().get(n);
        final V obj = this.values[n2][n];
        Objects.requireNonNull(obj);
        return ImmutableTable.cellOf(value, value2, obj);
    }
    
    @Override
    public V getValue(final int n) {
        final V obj = this.values[this.cellRowIndices[n]][this.cellColumnIndices[n]];
        Objects.requireNonNull(obj);
        return obj;
    }
    
    @Override
    public ImmutableMap<R, Map<C, V>> rowMap() {
        return ImmutableMap.copyOf((Map<? extends R, ? extends Map<C, V>>)this.rowMap);
    }
    
    @Override
    public int size() {
        return this.cellRowIndices.length;
    }
    
    public final class Column extends ImmutableArrayMap<R, V>
    {
        private final int columnIndex;
        final DenseImmutableTable this$0;
        
        public Column(final DenseImmutableTable this$0, final int columnIndex) {
            this.this$0 = this$0;
            super(DenseImmutableTable.access$500(this$0)[columnIndex]);
            this.columnIndex = columnIndex;
        }
        
        @Override
        public V getValue(final int n) {
            return (V)DenseImmutableTable.access$400(this.this$0)[n][this.columnIndex];
        }
        
        @Override
        public boolean isPartialView() {
            return true;
        }
        
        @Override
        public ImmutableMap<R, Integer> keyToIndex() {
            return DenseImmutableTable.access$600(this.this$0);
        }
    }
    
    public final class ColumnMap extends ImmutableArrayMap<C, ImmutableMap<R, V>>
    {
        final DenseImmutableTable this$0;
        
        private ColumnMap(final DenseImmutableTable this$0) {
            this.this$0 = this$0;
            super(DenseImmutableTable.access$500(this$0).length);
        }
        
        public ImmutableMap<R, V> getValue(final int n) {
            return this.this$0.new Column(n);
        }
        
        @Override
        public boolean isPartialView() {
            return false;
        }
        
        @Override
        public ImmutableMap<C, Integer> keyToIndex() {
            return DenseImmutableTable.access$300(this.this$0);
        }
    }
    
    public abstract static class ImmutableArrayMap<K, V> extends IteratorBasedImmutableMap<K, V>
    {
        private final int size;
        
        public ImmutableArrayMap(final int size) {
            this.size = size;
        }
        
        private boolean isFull() {
            return this.size == this.keyToIndex().size();
        }
        
        @Override
        public ImmutableSet<K> createKeySet() {
            ImmutableSet<K> set;
            if (this.isFull()) {
                set = this.keyToIndex().keySet();
            }
            else {
                set = super.createKeySet();
            }
            return set;
        }
        
        @Override
        public w02 entryIterator() {
            return new AbstractIterator(this) {
                public int c = -1;
                public final int d = e.keyToIndex().size();
                public final ImmutableArrayMap e;
                
                public Entry e() {
                    int n = this.c;
                    while (true) {
                        this.c = n + 1;
                        final int c = this.c;
                        if (c >= this.d) {
                            return (Entry)this.c();
                        }
                        final V value = this.e.getValue(c);
                        if (value != null) {
                            return Maps.j(this.e.getKey(this.c), value);
                        }
                        n = this.c;
                    }
                }
            };
        }
        
        @Override
        public V get(Object value) {
            final Integer n = this.keyToIndex().get(value);
            if (n == null) {
                value = null;
            }
            else {
                value = this.getValue(n);
            }
            return (V)value;
        }
        
        public K getKey(final int n) {
            return this.keyToIndex().keySet().asList().get(n);
        }
        
        public abstract V getValue(final int p0);
        
        public abstract ImmutableMap<K, Integer> keyToIndex();
        
        @Override
        public int size() {
            return this.size;
        }
    }
    
    public final class Row extends ImmutableArrayMap<C, V>
    {
        private final int rowIndex;
        final DenseImmutableTable this$0;
        
        public Row(final DenseImmutableTable this$0, final int rowIndex) {
            this.this$0 = this$0;
            super(DenseImmutableTable.access$200(this$0)[rowIndex]);
            this.rowIndex = rowIndex;
        }
        
        @Override
        public V getValue(final int n) {
            return (V)DenseImmutableTable.access$400(this.this$0)[this.rowIndex][n];
        }
        
        @Override
        public boolean isPartialView() {
            return true;
        }
        
        @Override
        public ImmutableMap<C, Integer> keyToIndex() {
            return DenseImmutableTable.access$300(this.this$0);
        }
    }
    
    public final class RowMap extends ImmutableArrayMap<R, ImmutableMap<C, V>>
    {
        final DenseImmutableTable this$0;
        
        private RowMap(final DenseImmutableTable this$0) {
            this.this$0 = this$0;
            super(DenseImmutableTable.access$200(this$0).length);
        }
        
        public ImmutableMap<C, V> getValue(final int n) {
            return this.this$0.new Row(n);
        }
        
        @Override
        public boolean isPartialView() {
            return false;
        }
        
        @Override
        public ImmutableMap<R, Integer> keyToIndex() {
            return DenseImmutableTable.access$600(this.this$0);
        }
    }
}

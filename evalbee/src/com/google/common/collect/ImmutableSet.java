// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.io.Serializable;
import java.util.SortedSet;
import java.util.Iterator;
import java.util.Collection;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;

public abstract class ImmutableSet<E> extends ImmutableCollection<E> implements Set<E>
{
    private static final int CUTOFF = 751619276;
    private static final double DESIRED_LOAD_FACTOR = 0.7;
    static final int MAX_TABLE_SIZE = 1073741824;
    private transient ImmutableList<E> asList;
    
    public static <E> a builder() {
        return new a();
    }
    
    public static <E> a builderWithExpectedSize(final int n) {
        hh.b(n, "expectedSize");
        return new a(n);
    }
    
    public static int chooseTableSize(int a) {
        final int max = Math.max(a, 2);
        boolean b = true;
        if (max < 751619276) {
            for (a = Integer.highestOneBit(max - 1) << 1; a * 0.7 < max; a <<= 1) {}
            return a;
        }
        if (max >= 1073741824) {
            b = false;
        }
        i71.e(b, "collection too large");
        return 1073741824;
    }
    
    private static <E> ImmutableSet<E> construct(final int toIndex, final Object... array) {
        if (toIndex == 0) {
            return of();
        }
        if (toIndex == 1) {
            final Object obj = array[0];
            Objects.requireNonNull(obj);
            return of(obj);
        }
        final int chooseTableSize = chooseTableSize(toIndex);
        final Object[] array2 = new Object[chooseTableSize];
        final int n = chooseTableSize - 1;
        int i = 0;
        int n3;
        int n2 = n3 = 0;
        while (i < toIndex) {
            final Object a = t01.a(array[i], i);
            final int hashCode = a.hashCode();
            int c = rc0.c(hashCode);
            while (true) {
                final int n4 = c & n;
                final Object o = array2[n4];
                if (o == null) {
                    array2[n4] = (array[n3] = a);
                    n2 += hashCode;
                    ++n3;
                    break;
                }
                if (o.equals(a)) {
                    break;
                }
                ++c;
            }
            ++i;
        }
        Arrays.fill(array, n3, toIndex, null);
        if (n3 == 1) {
            final Object obj2 = array[0];
            Objects.requireNonNull(obj2);
            return new SingletonImmutableSet<E>((E)obj2);
        }
        if (chooseTableSize(n3) < chooseTableSize / 2) {
            return (ImmutableSet<E>)construct(n3, array);
        }
        Object[] copy = array;
        if (shouldTrim(n3, array.length)) {
            copy = Arrays.copyOf(array, n3);
        }
        return new RegularImmutableSet<E>(copy, n2, array2, n, n3);
    }
    
    public static <E> ImmutableSet<E> copyOf(final Iterable<? extends E> iterable) {
        ImmutableSet<Object> set;
        if (iterable instanceof Collection) {
            set = copyOf((Collection<?>)iterable);
        }
        else {
            set = copyOf((Iterator<?>)iterable.iterator());
        }
        return (ImmutableSet<E>)set;
    }
    
    public static <E> ImmutableSet<E> copyOf(final Collection<? extends E> collection) {
        if (collection instanceof ImmutableSet && !(collection instanceof SortedSet)) {
            final ImmutableSet set = (ImmutableSet)collection;
            if (!set.isPartialView()) {
                return set;
            }
        }
        final Object[] array = collection.toArray();
        return (ImmutableSet<E>)construct(array.length, array);
    }
    
    public static <E> ImmutableSet<E> copyOf(final Iterator<? extends E> iterator) {
        if (!iterator.hasNext()) {
            return of();
        }
        final E next = iterator.next();
        if (!iterator.hasNext()) {
            return of(next);
        }
        return new a().i(next).k(iterator).m();
    }
    
    public static <E> ImmutableSet<E> copyOf(final E[] array) {
        final int length = array.length;
        if (length == 0) {
            return of();
        }
        if (length != 1) {
            return construct(array.length, (Object[])array.clone());
        }
        return of(array[0]);
    }
    
    public static <E> ImmutableSet<E> of() {
        return (ImmutableSet<E>)RegularImmutableSet.EMPTY;
    }
    
    public static <E> ImmutableSet<E> of(final E e) {
        return new SingletonImmutableSet<E>(e);
    }
    
    public static <E> ImmutableSet<E> of(final E e, final E e2) {
        return construct(2, e, e2);
    }
    
    public static <E> ImmutableSet<E> of(final E e, final E e2, final E e3) {
        return construct(3, e, e2, e3);
    }
    
    public static <E> ImmutableSet<E> of(final E e, final E e2, final E e3, final E e4) {
        return construct(4, e, e2, e3, e4);
    }
    
    public static <E> ImmutableSet<E> of(final E e, final E e2, final E e3, final E e4, final E e5) {
        return construct(5, e, e2, e3, e4, e5);
    }
    
    @SafeVarargs
    public static <E> ImmutableSet<E> of(final E e, final E e2, final E e3, final E e4, final E e5, final E e6, final E... array) {
        i71.e(array.length <= 2147483641, "the total number of elements must fit in an int");
        final int n = array.length + 6;
        final Object[] array2 = new Object[n];
        array2[0] = e;
        array2[1] = e2;
        array2[2] = e3;
        array2[3] = e4;
        array2[4] = e5;
        array2[5] = e6;
        System.arraycopy(array, 0, array2, 6, array.length);
        return (ImmutableSet<E>)construct(n, array2);
    }
    
    private static boolean shouldTrim(final int n, final int n2) {
        return n < (n2 >> 1) + (n2 >> 2);
    }
    
    @Override
    public ImmutableList<E> asList() {
        ImmutableList<E> asList;
        if ((asList = this.asList) == null) {
            asList = this.createAsList();
            this.asList = asList;
        }
        return asList;
    }
    
    public ImmutableList<E> createAsList() {
        return ImmutableList.asImmutableList(this.toArray());
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || ((!(o instanceof ImmutableSet) || !this.isHashCodeFast() || !((ImmutableSet)o).isHashCodeFast() || this.hashCode() == o.hashCode()) && Sets.a(this, o));
    }
    
    @Override
    public int hashCode() {
        return Sets.b(this);
    }
    
    public boolean isHashCodeFast() {
        return false;
    }
    
    @Override
    public abstract w02 iterator();
    
    @Override
    Object writeReplace() {
        return new SerializedForm(this.toArray());
    }
    
    public static class SerializedForm implements Serializable
    {
        private static final long serialVersionUID = 0L;
        final Object[] elements;
        
        public SerializedForm(final Object[] elements) {
            this.elements = elements;
        }
        
        public Object readResolve() {
            return ImmutableSet.copyOf(this.elements);
        }
    }
    
    public static class a extends ImmutableCollection.a
    {
        public Object[] d;
        public int e;
        
        public a() {
            super(4);
        }
        
        public a(final int n) {
            super(n);
            this.d = new Object[ImmutableSet.chooseTableSize(n)];
        }
        
        public a i(final Object o) {
            i71.r(o);
            if (this.d != null && ImmutableSet.chooseTableSize(super.b) <= this.d.length) {
                this.l(o);
                return this;
            }
            this.d = null;
            super.f(o);
            return this;
        }
        
        public a j(final Object... array) {
            if (this.d != null) {
                for (int length = array.length, i = 0; i < length; ++i) {
                    this.i(array[i]);
                }
            }
            else {
                super.b(array);
            }
            return this;
        }
        
        public a k(final Iterator iterator) {
            i71.r(iterator);
            while (iterator.hasNext()) {
                this.i(iterator.next());
            }
            return this;
        }
        
        public final void l(final Object obj) {
            Objects.requireNonNull(this.d);
            final int length = this.d.length;
            final int hashCode = obj.hashCode();
            int c = rc0.c(hashCode);
            while (true) {
                c &= length - 1;
                final Object[] d = this.d;
                final Object o = d[c];
                if (o == null) {
                    d[c] = obj;
                    this.e += hashCode;
                    super.f(obj);
                    return;
                }
                if (o.equals(obj)) {
                    return;
                }
                ++c;
            }
        }
        
        public ImmutableSet m() {
            final int b = super.b;
            if (b == 0) {
                return ImmutableSet.of();
            }
            if (b != 1) {
                ImmutableSet<Object> access$100;
                if (this.d != null && ImmutableSet.chooseTableSize(b) == this.d.length) {
                    Object[] array;
                    if (shouldTrim(super.b, super.a.length)) {
                        array = Arrays.copyOf(super.a, super.b);
                    }
                    else {
                        array = super.a;
                    }
                    final int e = this.e;
                    final Object[] d = this.d;
                    access$100 = new RegularImmutableSet<Object>(array, e, d, d.length - 1, super.b);
                }
                else {
                    access$100 = construct(super.b, super.a);
                    super.b = access$100.size();
                }
                super.c = true;
                this.d = null;
                return access$100;
            }
            final Object obj = super.a[0];
            Objects.requireNonNull(obj);
            return ImmutableSet.of(obj);
        }
    }
}

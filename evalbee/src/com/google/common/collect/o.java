// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Set;
import java.util.NavigableSet;
import java.util.Comparator;

public interface o extends j, ro1
{
    Comparator comparator();
    
    o descendingMultiset();
    
    NavigableSet elementSet();
    
    Set entrySet();
    
    a firstEntry();
    
    o headMultiset(final Object p0, final BoundType p1);
    
    a lastEntry();
    
    a pollFirstEntry();
    
    a pollLastEntry();
    
    o subMultiset(final Object p0, final BoundType p1, final Object p2, final BoundType p3);
    
    o tailMultiset(final Object p0, final BoundType p1);
}

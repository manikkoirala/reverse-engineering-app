// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Iterator;
import java.util.Map;
import java.io.Serializable;

public final class ImmutableClassToInstanceMap<B> extends x70 implements Serializable
{
    private static final ImmutableClassToInstanceMap<Object> EMPTY;
    private final ImmutableMap<Class<? extends B>, B> delegate;
    
    static {
        EMPTY = new ImmutableClassToInstanceMap<Object>(ImmutableMap.of());
    }
    
    private ImmutableClassToInstanceMap(final ImmutableMap<Class<? extends B>, B> delegate) {
        this.delegate = delegate;
    }
    
    public static <B> b builder() {
        return new b();
    }
    
    public static <B, S extends B> ImmutableClassToInstanceMap<B> copyOf(final Map<? extends Class<? extends S>, ? extends S> map) {
        if (map instanceof ImmutableClassToInstanceMap) {
            return (ImmutableClassToInstanceMap<B>)map;
        }
        return new b().c(map).a();
    }
    
    public static <B> ImmutableClassToInstanceMap<B> of() {
        return (ImmutableClassToInstanceMap<B>)ImmutableClassToInstanceMap.EMPTY;
    }
    
    public static <B, T extends B> ImmutableClassToInstanceMap<B> of(final Class<T> clazz, final T t) {
        return new ImmutableClassToInstanceMap<B>((ImmutableMap<Class<? extends B>, B>)ImmutableMap.of(clazz, t));
    }
    
    @Override
    public Map<Class<? extends B>, B> delegate() {
        return this.delegate;
    }
    
    public <T extends B> T getInstance(final Class<T> clazz) {
        return (T)this.delegate.get(i71.r(clazz));
    }
    
    @Deprecated
    public <T extends B> T putInstance(final Class<T> clazz, final T t) {
        throw new UnsupportedOperationException();
    }
    
    public Object readResolve() {
        ImmutableClassToInstanceMap<Object> of;
        if (this.isEmpty()) {
            of = of();
        }
        else {
            of = (ImmutableClassToInstanceMap<Object>)this;
        }
        return of;
    }
    
    public static final class b
    {
        public final ImmutableMap.b a;
        
        public b() {
            this.a = ImmutableMap.builder();
        }
        
        public static Object b(final Class clazz, final Object obj) {
            return e81.d(clazz).cast(obj);
        }
        
        public ImmutableClassToInstanceMap a() {
            final ImmutableMap d = this.a.d();
            if (d.isEmpty()) {
                return ImmutableClassToInstanceMap.of();
            }
            return new ImmutableClassToInstanceMap(d, null);
        }
        
        public b c(final Map map) {
            for (final Map.Entry<Class, V> entry : map.entrySet()) {
                final Class clazz = entry.getKey();
                this.a.g(clazz, b(clazz, entry.getValue()));
            }
            return this;
        }
    }
}

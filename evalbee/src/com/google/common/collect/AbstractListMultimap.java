// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Collections;
import java.util.List;
import java.util.Collection;
import java.util.Map;

abstract class AbstractListMultimap<K, V> extends AbstractMapBasedMultimap<K, V> implements dk0
{
    private static final long serialVersionUID = 6588350623831699109L;
    
    public AbstractListMultimap(final Map<K, Collection<V>> map) {
        super(map);
    }
    
    @Override
    public Map<K, Collection<V>> asMap() {
        return (Map<K, Collection<V>>)super.asMap();
    }
    
    public abstract List<V> createCollection();
    
    @Override
    public List<V> createUnmodifiableEmptyCollection() {
        return Collections.emptyList();
    }
    
    @Override
    public boolean equals(final Object o) {
        return super.equals(o);
    }
    
    @Override
    public List<V> get(final K k) {
        return (List)super.get(k);
    }
    
    @Override
    public boolean put(final K k, final V v) {
        return super.put(k, v);
    }
    
    @Override
    public List<V> removeAll(final Object o) {
        return (List)super.removeAll(o);
    }
    
    @Override
    public List<V> replaceValues(final K k, final Iterable<? extends V> iterable) {
        return (List)super.replaceValues(k, iterable);
    }
    
    @Override
    public <E> Collection<E> unmodifiableCollectionSubclass(final Collection<E> collection) {
        return (Collection<E>)Collections.unmodifiableList((List<?>)collection);
    }
    
    @Override
    public Collection<V> wrapCollection(final K k, final Collection<V> collection) {
        return this.wrapList(k, (List<V>)collection, null);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Iterator;

final class RegularImmutableSet<E> extends ImmutableSet<E>
{
    static final RegularImmutableSet<Object> EMPTY;
    private static final Object[] EMPTY_ARRAY;
    final transient Object[] elements;
    private final transient int hashCode;
    private final transient int mask;
    private final transient int size;
    final transient Object[] table;
    
    static {
        final Object[] array = EMPTY_ARRAY = new Object[0];
        EMPTY = new RegularImmutableSet<Object>(array, 0, array, 0, 0);
    }
    
    public RegularImmutableSet(final Object[] elements, final int hashCode, final Object[] table, final int mask, final int size) {
        this.elements = elements;
        this.hashCode = hashCode;
        this.table = table;
        this.mask = mask;
        this.size = size;
    }
    
    @Override
    public boolean contains(final Object obj) {
        final Object[] table = this.table;
        if (obj == null || table.length == 0) {
            return false;
        }
        int d = rc0.d(obj);
        while (true) {
            d &= this.mask;
            final Object o = table[d];
            if (o == null) {
                return false;
            }
            if (o.equals(obj)) {
                return true;
            }
            ++d;
        }
    }
    
    @Override
    public int copyIntoArray(final Object[] array, final int n) {
        System.arraycopy(this.elements, 0, array, n, this.size);
        return n + this.size;
    }
    
    @Override
    public ImmutableList<E> createAsList() {
        return ImmutableList.asImmutableList(this.elements, this.size);
    }
    
    @Override
    public int hashCode() {
        return this.hashCode;
    }
    
    @Override
    public Object[] internalArray() {
        return this.elements;
    }
    
    @Override
    public int internalArrayEnd() {
        return this.size;
    }
    
    @Override
    public int internalArrayStart() {
        return 0;
    }
    
    @Override
    public boolean isHashCodeFast() {
        return true;
    }
    
    @Override
    public boolean isPartialView() {
        return false;
    }
    
    @Override
    public w02 iterator() {
        return this.asList().iterator();
    }
    
    @Override
    public int size() {
        return this.size;
    }
}

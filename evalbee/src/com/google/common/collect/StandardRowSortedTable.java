// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Comparator;
import java.util.SortedSet;
import java.util.Set;
import java.util.Map;
import java.util.SortedMap;

class StandardRowSortedTable<R, C, V> extends StandardTable<R, C, V> implements wf1
{
    private static final long serialVersionUID = 0L;
    
    public StandardRowSortedTable(final SortedMap<R, Map<C, V>> sortedMap, final is1 is1) {
        super(sortedMap, is1);
    }
    
    private SortedMap<R, Map<C, V>> sortedBackingMap() {
        return (SortedMap)super.backingMap;
    }
    
    @Override
    public SortedMap<R, Map<C, V>> createRowMap() {
        return new b(null);
    }
    
    @Override
    public SortedSet<R> rowKeySet() {
        return (SortedSet)this.rowMap().keySet();
    }
    
    @Override
    public SortedMap<R, Map<C, V>> rowMap() {
        return (SortedMap)super.rowMap();
    }
    
    public class b extends h implements SortedMap
    {
        public final StandardRowSortedTable e;
        
        public b(final StandardRowSortedTable e) {
            this.e = e.super();
        }
        
        @Override
        public Comparator comparator() {
            return this.e.sortedBackingMap().comparator();
        }
        
        public SortedSet f() {
            return new Maps.o(this);
        }
        
        @Override
        public Object firstKey() {
            return this.e.sortedBackingMap().firstKey();
        }
        
        public SortedSet g() {
            return (SortedSet)super.keySet();
        }
        
        @Override
        public SortedMap headMap(final Object o) {
            i71.r(o);
            return new StandardRowSortedTable((SortedMap<Object, Map<Object, Object>>)this.e.sortedBackingMap().headMap(o), this.e.factory).rowMap();
        }
        
        @Override
        public Object lastKey() {
            return this.e.sortedBackingMap().lastKey();
        }
        
        @Override
        public SortedMap subMap(final Object o, final Object o2) {
            i71.r(o);
            i71.r(o2);
            return new StandardRowSortedTable((SortedMap<Object, Map<Object, Object>>)this.e.sortedBackingMap().subMap(o, o2), this.e.factory).rowMap();
        }
        
        @Override
        public SortedMap tailMap(final Object o) {
            i71.r(o);
            return new StandardRowSortedTable((SortedMap<Object, Map<Object, Object>>)this.e.sortedBackingMap().tailMap(o), this.e.factory).rowMap();
        }
    }
}

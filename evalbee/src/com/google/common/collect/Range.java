// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Comparator;
import java.util.SortedSet;

public final class Range<C extends Comparable> extends RangeGwtSerializationDependencies implements m71
{
    private static final Range<Comparable> ALL;
    private static final long serialVersionUID = 0L;
    final Cut<C> lowerBound;
    final Cut<C> upperBound;
    
    static {
        ALL = new Range<Comparable>((Cut<Comparable>)Cut.belowAll(), (Cut<Comparable>)Cut.aboveAll());
    }
    
    private Range(final Cut<C> cut, final Cut<C> cut2) {
        this.lowerBound = (Cut)i71.r(cut);
        this.upperBound = (Cut)i71.r(cut2);
        if (cut.compareTo(cut2) <= 0 && cut != Cut.aboveAll() && cut2 != Cut.belowAll()) {
            return;
        }
        final String value = String.valueOf(toString(cut, cut2));
        String concat;
        if (value.length() != 0) {
            concat = "Invalid range: ".concat(value);
        }
        else {
            concat = new String("Invalid range: ");
        }
        throw new IllegalArgumentException(concat);
    }
    
    public static <C extends Comparable<?>> Range<C> all() {
        return (Range<C>)Range.ALL;
    }
    
    public static <C extends Comparable<?>> Range<C> atLeast(final C c) {
        return create((Cut<C>)Cut.belowValue((C)c), Cut.aboveAll());
    }
    
    public static <C extends Comparable<?>> Range<C> atMost(final C c) {
        return create(Cut.belowAll(), (Cut<C>)Cut.aboveValue((C)c));
    }
    
    public static <C extends Comparable<?>> Range<C> closed(final C c, final C c2) {
        return create((Cut<C>)Cut.belowValue((C)c), (Cut<C>)Cut.aboveValue((C)c2));
    }
    
    public static <C extends Comparable<?>> Range<C> closedOpen(final C c, final C c2) {
        return create((Cut<C>)Cut.belowValue((C)c), (Cut<C>)Cut.belowValue((C)c2));
    }
    
    public static int compareOrThrow(final Comparable comparable, final Comparable comparable2) {
        return comparable.compareTo(comparable2);
    }
    
    public static <C extends Comparable<?>> Range<C> create(final Cut<C> cut, final Cut<C> cut2) {
        return new Range<C>(cut, cut2);
    }
    
    public static <C extends Comparable<?>> Range<C> downTo(final C c, final BoundType boundType) {
        final int n = Range$a.a[boundType.ordinal()];
        if (n == 1) {
            return greaterThan(c);
        }
        if (n == 2) {
            return atLeast(c);
        }
        throw new AssertionError();
    }
    
    public static <C extends Comparable<?>> Range<C> encloseAll(final Iterable<C> iterable) {
        i71.r(iterable);
        if (iterable instanceof SortedSet) {
            final SortedSet set = (SortedSet)iterable;
            final Comparator comparator = set.comparator();
            if (Ordering.natural().equals(comparator) || comparator == null) {
                return closed(set.first(), set.last());
            }
        }
        final Iterator iterator = iterable.iterator();
        Comparable<?> comparable2;
        Comparable<?> comparable = comparable2 = (Comparable)i71.r(iterator.next());
        while (iterator.hasNext()) {
            final Comparable comparable3 = (Comparable)i71.r(iterator.next());
            comparable = Ordering.natural().min(comparable, comparable3);
            comparable2 = Ordering.natural().max(comparable2, comparable3);
        }
        return closed(comparable, comparable2);
    }
    
    public static <C extends Comparable<?>> Range<C> greaterThan(final C c) {
        return create((Cut<C>)Cut.aboveValue((C)c), Cut.aboveAll());
    }
    
    public static <C extends Comparable<?>> Range<C> lessThan(final C c) {
        return create(Cut.belowAll(), (Cut<C>)Cut.belowValue((C)c));
    }
    
    public static <C extends Comparable<?>> m90 lowerBoundFn() {
        return b.a;
    }
    
    public static <C extends Comparable<?>> Range<C> open(final C c, final C c2) {
        return create((Cut<C>)Cut.aboveValue((C)c), (Cut<C>)Cut.belowValue((C)c2));
    }
    
    public static <C extends Comparable<?>> Range<C> openClosed(final C c, final C c2) {
        return create((Cut<C>)Cut.aboveValue((C)c), (Cut<C>)Cut.aboveValue((C)c2));
    }
    
    public static <C extends Comparable<?>> Range<C> range(final C c, final BoundType boundType, final C c2, final BoundType boundType2) {
        i71.r(boundType);
        i71.r(boundType2);
        final BoundType open = BoundType.OPEN;
        Cut<C> cut;
        if (boundType == open) {
            cut = Cut.aboveValue(c);
        }
        else {
            cut = Cut.belowValue(c);
        }
        Cut<C> cut2;
        if (boundType2 == open) {
            cut2 = Cut.belowValue(c2);
        }
        else {
            cut2 = Cut.aboveValue(c2);
        }
        return create(cut, cut2);
    }
    
    public static <C extends Comparable<?>> Ordering rangeLexOrdering() {
        return RangeLexOrdering.INSTANCE;
    }
    
    public static <C extends Comparable<?>> Range<C> singleton(final C c) {
        return closed(c, c);
    }
    
    private static String toString(final Cut<?> cut, final Cut<?> cut2) {
        final StringBuilder sb = new StringBuilder(16);
        cut.describeAsLowerBound(sb);
        sb.append("..");
        cut2.describeAsUpperBound(sb);
        return sb.toString();
    }
    
    public static <C extends Comparable<?>> Range<C> upTo(final C c, final BoundType boundType) {
        final int n = Range$a.a[boundType.ordinal()];
        if (n == 1) {
            return lessThan(c);
        }
        if (n == 2) {
            return atMost(c);
        }
        throw new AssertionError();
    }
    
    public static <C extends Comparable<?>> m90 upperBoundFn() {
        return c.a;
    }
    
    @Deprecated
    public boolean apply(final C c) {
        return this.contains(c);
    }
    
    public Range<C> canonical(final DiscreteDomain discreteDomain) {
        i71.r(discreteDomain);
        final Cut<C> canonical = this.lowerBound.canonical(discreteDomain);
        final Cut<C> canonical2 = this.upperBound.canonical(discreteDomain);
        Range create;
        if (canonical == this.lowerBound && canonical2 == this.upperBound) {
            create = this;
        }
        else {
            create = create(canonical, canonical2);
        }
        return create;
    }
    
    public boolean contains(final C c) {
        i71.r(c);
        return this.lowerBound.isLessThan(c) && !this.upperBound.isLessThan(c);
    }
    
    public boolean containsAll(final Iterable<? extends C> iterable) {
        final boolean i = rg0.i(iterable);
        boolean b = true;
        if (i) {
            return true;
        }
        if (iterable instanceof SortedSet) {
            final SortedSet set = (SortedSet)iterable;
            final Comparator comparator = set.comparator();
            if (Ordering.natural().equals(comparator) || comparator == null) {
                if (!this.contains((C)set.first()) || !this.contains((C)set.last())) {
                    b = false;
                }
                return b;
            }
        }
        final Iterator iterator = iterable.iterator();
        while (iterator.hasNext()) {
            if (!this.contains((C)iterator.next())) {
                return false;
            }
        }
        return true;
    }
    
    public boolean encloses(final Range<C> range) {
        return this.lowerBound.compareTo(range.lowerBound) <= 0 && this.upperBound.compareTo(range.upperBound) >= 0;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof Range;
        boolean b3;
        final boolean b2 = b3 = false;
        if (b) {
            final Range range = (Range)o;
            b3 = b2;
            if (this.lowerBound.equals(range.lowerBound)) {
                b3 = b2;
                if (this.upperBound.equals(range.upperBound)) {
                    b3 = true;
                }
            }
        }
        return b3;
    }
    
    public Range<C> gap(Range<C> obj) {
        if (this.lowerBound.compareTo(obj.upperBound) < 0 && obj.lowerBound.compareTo(this.upperBound) < 0) {
            final String value = String.valueOf(this);
            final String value2 = String.valueOf(obj);
            final StringBuilder sb = new StringBuilder(value.length() + 39 + value2.length());
            sb.append("Ranges have a nonempty intersection: ");
            sb.append(value);
            sb.append(", ");
            sb.append(value2);
            throw new IllegalArgumentException(sb.toString());
        }
        final boolean b = this.lowerBound.compareTo(obj.lowerBound) < 0;
        Range range;
        if (b) {
            range = this;
        }
        else {
            range = obj;
        }
        if (!b) {
            obj = this;
        }
        return create(range.upperBound, obj.lowerBound);
    }
    
    public boolean hasLowerBound() {
        return this.lowerBound != Cut.belowAll();
    }
    
    public boolean hasUpperBound() {
        return this.upperBound != Cut.aboveAll();
    }
    
    @Override
    public int hashCode() {
        return this.lowerBound.hashCode() * 31 + this.upperBound.hashCode();
    }
    
    public Range<C> intersection(final Range<C> range) {
        final int compareTo = this.lowerBound.compareTo(range.lowerBound);
        final int compareTo2 = this.upperBound.compareTo(range.upperBound);
        if (compareTo >= 0 && compareTo2 <= 0) {
            return this;
        }
        if (compareTo <= 0 && compareTo2 >= 0) {
            return range;
        }
        Cut<C> cut;
        if (compareTo >= 0) {
            cut = this.lowerBound;
        }
        else {
            cut = range.lowerBound;
        }
        Cut<C> cut2;
        if (compareTo2 <= 0) {
            cut2 = this.upperBound;
        }
        else {
            cut2 = range.upperBound;
        }
        i71.n(cut.compareTo(cut2) <= 0, "intersection is undefined for disconnected ranges %s and %s", this, range);
        return create(cut, cut2);
    }
    
    public boolean isConnected(final Range<C> range) {
        return this.lowerBound.compareTo(range.upperBound) <= 0 && range.lowerBound.compareTo(this.upperBound) <= 0;
    }
    
    public boolean isEmpty() {
        return this.lowerBound.equals(this.upperBound);
    }
    
    public BoundType lowerBoundType() {
        return this.lowerBound.typeAsLowerBound();
    }
    
    public C lowerEndpoint() {
        return this.lowerBound.endpoint();
    }
    
    public Object readResolve() {
        if (this.equals(Range.ALL)) {
            return all();
        }
        return this;
    }
    
    public Range<C> span(final Range<C> range) {
        final int compareTo = this.lowerBound.compareTo(range.lowerBound);
        final int compareTo2 = this.upperBound.compareTo(range.upperBound);
        if (compareTo <= 0 && compareTo2 >= 0) {
            return this;
        }
        if (compareTo >= 0 && compareTo2 <= 0) {
            return range;
        }
        Cut<C> cut;
        if (compareTo <= 0) {
            cut = this.lowerBound;
        }
        else {
            cut = range.lowerBound;
        }
        Cut<C> cut2;
        if (compareTo2 >= 0) {
            cut2 = this.upperBound;
        }
        else {
            cut2 = range.upperBound;
        }
        return create(cut, cut2);
    }
    
    @Override
    public String toString() {
        return toString(this.lowerBound, this.upperBound);
    }
    
    public BoundType upperBoundType() {
        return this.upperBound.typeAsUpperBound();
    }
    
    public C upperEndpoint() {
        return this.upperBound.endpoint();
    }
    
    public static class RangeLexOrdering extends Ordering implements Serializable
    {
        static final Ordering INSTANCE;
        private static final long serialVersionUID = 0L;
        
        static {
            INSTANCE = new RangeLexOrdering();
        }
        
        private RangeLexOrdering() {
        }
        
        public int compare(final Range<?> range, final Range<?> range2) {
            return ji.f().d(range.lowerBound, range2.lowerBound).d(range.upperBound, range2.upperBound).e();
        }
    }
    
    public static class b implements m90
    {
        public static final b a;
        
        static {
            a = new b();
        }
        
        public Cut a(final Range range) {
            return range.lowerBound;
        }
    }
    
    public static class c implements m90
    {
        public static final c a;
        
        static {
            a = new c();
        }
        
        public Cut a(final Range range) {
            return range.upperBound;
        }
    }
}

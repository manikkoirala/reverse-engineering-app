// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.NavigableSet;
import java.util.Iterator;
import java.util.Comparator;
import java.util.SortedSet;
import java.util.NoSuchElementException;

public abstract class p
{
    public static Object c(final j.a a) {
        Object element;
        if (a == null) {
            element = null;
        }
        else {
            element = a.getElement();
        }
        return element;
    }
    
    public static Object d(final j.a a) {
        if (a != null) {
            return a.getElement();
        }
        throw new NoSuchElementException();
    }
    
    public abstract static class a extends c implements SortedSet
    {
        public final o a;
        
        public a(final o a) {
            this.a = a;
        }
        
        public final o b() {
            return this.a;
        }
        
        @Override
        public Comparator comparator() {
            return this.b().comparator();
        }
        
        @Override
        public Object first() {
            return d(this.b().firstEntry());
        }
        
        @Override
        public SortedSet headSet(final Object o) {
            return this.b().headMultiset(o, BoundType.OPEN).elementSet();
        }
        
        @Override
        public Iterator iterator() {
            return Multisets.e(this.b().entrySet().iterator());
        }
        
        @Override
        public Object last() {
            return d(this.b().lastEntry());
        }
        
        @Override
        public SortedSet subSet(final Object o, final Object o2) {
            return this.b().subMultiset(o, BoundType.CLOSED, o2, BoundType.OPEN).elementSet();
        }
        
        @Override
        public SortedSet tailSet(final Object o) {
            return this.b().tailMultiset(o, BoundType.CLOSED).elementSet();
        }
    }
    
    public static class b extends a implements NavigableSet
    {
        public b(final o o) {
            super(o);
        }
        
        @Override
        public Object ceiling(final Object o) {
            return c(((a)this).b().tailMultiset(o, BoundType.CLOSED).firstEntry());
        }
        
        @Override
        public Iterator descendingIterator() {
            return this.descendingSet().iterator();
        }
        
        @Override
        public NavigableSet descendingSet() {
            return new b(((a)this).b().descendingMultiset());
        }
        
        @Override
        public Object floor(final Object o) {
            return c(((a)this).b().headMultiset(o, BoundType.CLOSED).lastEntry());
        }
        
        @Override
        public NavigableSet headSet(final Object o, final boolean b) {
            return new b(((a)this).b().headMultiset(o, BoundType.forBoolean(b)));
        }
        
        @Override
        public Object higher(final Object o) {
            return c(((a)this).b().tailMultiset(o, BoundType.OPEN).firstEntry());
        }
        
        @Override
        public Object lower(final Object o) {
            return c(((a)this).b().headMultiset(o, BoundType.OPEN).lastEntry());
        }
        
        @Override
        public Object pollFirst() {
            return c(((a)this).b().pollFirstEntry());
        }
        
        @Override
        public Object pollLast() {
            return c(((a)this).b().pollLastEntry());
        }
        
        @Override
        public NavigableSet subSet(final Object o, final boolean b, final Object o2, final boolean b2) {
            return new b(((a)this).b().subMultiset(o, BoundType.forBoolean(b), o2, BoundType.forBoolean(b2)));
        }
        
        @Override
        public NavigableSet tailSet(final Object o, final boolean b) {
            return new b(((a)this).b().tailMultiset(o, BoundType.forBoolean(b)));
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.AbstractCollection;
import java.util.Objects;
import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Iterator;
import java.io.Serializable;
import java.util.Comparator;
import java.util.NavigableSet;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.util.Map;
import java.util.List;
import java.util.SortedSet;
import java.util.Collections;
import java.util.Set;
import java.util.Collection;

public abstract class Multimaps
{
    public static boolean c(final rx0 rx0, final Object o) {
        return o == rx0 || (o instanceof rx0 && rx0.asMap().equals(((rx0)o).asMap()));
    }
    
    public static Collection d(final Collection c) {
        if (c instanceof Set) {
            return Maps.G((Set)c);
        }
        return new Maps.r(Collections.unmodifiableCollection((Collection<?>)c));
    }
    
    public static Collection e(final Collection c) {
        if (c instanceof SortedSet) {
            return Collections.unmodifiableSortedSet((SortedSet<Object>)c);
        }
        if (c instanceof Set) {
            return Collections.unmodifiableSet((Set<?>)(Set<? extends T>)c);
        }
        if (c instanceof List) {
            return Collections.unmodifiableList((List<?>)(List<? extends T>)c);
        }
        return Collections.unmodifiableCollection((Collection<?>)c);
    }
    
    public static class CustomListMultimap<K, V> extends AbstractListMultimap<K, V>
    {
        private static final long serialVersionUID = 0L;
        transient is1 factory;
        
        public CustomListMultimap(final Map<K, Collection<V>> map, final is1 is1) {
            super(map);
            this.factory = (is1)i71.r(is1);
        }
        
        private void readObject(final ObjectInputStream objectInputStream) {
            objectInputStream.defaultReadObject();
            this.factory = (is1)objectInputStream.readObject();
            this.setMap((Map<K, Collection<V>>)objectInputStream.readObject());
        }
        
        private void writeObject(final ObjectOutputStream objectOutputStream) {
            objectOutputStream.defaultWriteObject();
            objectOutputStream.writeObject(this.factory);
            objectOutputStream.writeObject(this.backingMap());
        }
        
        @Override
        public Map<K, Collection<V>> createAsMap() {
            return this.createMaybeNavigableAsMap();
        }
        
        @Override
        public List<V> createCollection() {
            return (List)this.factory.get();
        }
        
        @Override
        public Set<K> createKeySet() {
            return this.createMaybeNavigableKeySet();
        }
    }
    
    public static class CustomMultimap<K, V> extends AbstractMapBasedMultimap<K, V>
    {
        private static final long serialVersionUID = 0L;
        transient is1 factory;
        
        public CustomMultimap(final Map<K, Collection<V>> map, final is1 is1) {
            super(map);
            this.factory = (is1)i71.r(is1);
        }
        
        private void readObject(final ObjectInputStream objectInputStream) {
            objectInputStream.defaultReadObject();
            this.factory = (is1)objectInputStream.readObject();
            this.setMap((Map<K, Collection<V>>)objectInputStream.readObject());
        }
        
        private void writeObject(final ObjectOutputStream objectOutputStream) {
            objectOutputStream.defaultWriteObject();
            objectOutputStream.writeObject(this.factory);
            objectOutputStream.writeObject(this.backingMap());
        }
        
        @Override
        public Map<K, Collection<V>> createAsMap() {
            return this.createMaybeNavigableAsMap();
        }
        
        public Collection<V> createCollection() {
            return (Collection)this.factory.get();
        }
        
        @Override
        public Set<K> createKeySet() {
            return this.createMaybeNavigableKeySet();
        }
        
        @Override
        public <E> Collection<E> unmodifiableCollectionSubclass(final Collection<E> c) {
            if (c instanceof NavigableSet) {
                return Sets.h((NavigableSet)c);
            }
            if (c instanceof SortedSet) {
                return (Collection<E>)Collections.unmodifiableSortedSet((SortedSet<Object>)c);
            }
            if (c instanceof Set) {
                return (Collection<E>)Collections.unmodifiableSet((Set<?>)c);
            }
            if (c instanceof List) {
                return (Collection<E>)Collections.unmodifiableList((List<?>)c);
            }
            return Collections.unmodifiableCollection((Collection<? extends E>)c);
        }
        
        @Override
        public Collection<V> wrapCollection(final K k, final Collection<V> collection) {
            if (collection instanceof List) {
                return this.wrapList(k, (List<V>)collection, null);
            }
            if (collection instanceof NavigableSet) {
                return new m(k, (NavigableSet)collection, null);
            }
            if (collection instanceof SortedSet) {
                return new o(k, (SortedSet)collection, null);
            }
            if (collection instanceof Set) {
                return new n(k, (Set)collection);
            }
            return new k(k, collection, null);
        }
    }
    
    public static class CustomSetMultimap<K, V> extends AbstractSetMultimap<K, V>
    {
        private static final long serialVersionUID = 0L;
        transient is1 factory;
        
        public CustomSetMultimap(final Map<K, Collection<V>> map, final is1 is1) {
            super(map);
            this.factory = (is1)i71.r(is1);
        }
        
        private void readObject(final ObjectInputStream objectInputStream) {
            objectInputStream.defaultReadObject();
            this.factory = (is1)objectInputStream.readObject();
            this.setMap((Map<K, Collection<V>>)objectInputStream.readObject());
        }
        
        private void writeObject(final ObjectOutputStream objectOutputStream) {
            objectOutputStream.defaultWriteObject();
            objectOutputStream.writeObject(this.factory);
            objectOutputStream.writeObject(this.backingMap());
        }
        
        @Override
        public Map<K, Collection<V>> createAsMap() {
            return this.createMaybeNavigableAsMap();
        }
        
        public Set<V> createCollection() {
            return (Set)this.factory.get();
        }
        
        @Override
        public Set<K> createKeySet() {
            return this.createMaybeNavigableKeySet();
        }
        
        @Override
        public <E> Collection<E> unmodifiableCollectionSubclass(final Collection<E> collection) {
            if (collection instanceof NavigableSet) {
                return Sets.h((NavigableSet)collection);
            }
            if (collection instanceof SortedSet) {
                return (Collection<E>)Collections.unmodifiableSortedSet((SortedSet<Object>)(SortedSet)collection);
            }
            return (Collection<E>)Collections.unmodifiableSet((Set<?>)(Set)collection);
        }
        
        @Override
        public Collection<V> wrapCollection(final K k, final Collection<V> collection) {
            if (collection instanceof NavigableSet) {
                return new m(k, (NavigableSet)collection, null);
            }
            if (collection instanceof SortedSet) {
                return new o(k, (SortedSet)collection, null);
            }
            return new n(k, (Set)collection);
        }
    }
    
    public static class CustomSortedSetMultimap<K, V> extends AbstractSortedSetMultimap<K, V>
    {
        private static final long serialVersionUID = 0L;
        transient is1 factory;
        transient Comparator<? super V> valueComparator;
        
        public CustomSortedSetMultimap(final Map<K, Collection<V>> map, final is1 is1) {
            super(map);
            this.factory = (is1)i71.r(is1);
            this.valueComparator = ((SortedSet)is1.get()).comparator();
        }
        
        private void readObject(final ObjectInputStream objectInputStream) {
            objectInputStream.defaultReadObject();
            final is1 factory = (is1)objectInputStream.readObject();
            this.factory = factory;
            this.valueComparator = ((SortedSet)factory.get()).comparator();
            this.setMap((Map<K, Collection<V>>)objectInputStream.readObject());
        }
        
        private void writeObject(final ObjectOutputStream objectOutputStream) {
            objectOutputStream.defaultWriteObject();
            objectOutputStream.writeObject(this.factory);
            objectOutputStream.writeObject(this.backingMap());
        }
        
        @Override
        public Map<K, Collection<V>> createAsMap() {
            return this.createMaybeNavigableAsMap();
        }
        
        @Override
        public SortedSet<V> createCollection() {
            return (SortedSet)this.factory.get();
        }
        
        @Override
        public Set<K> createKeySet() {
            return this.createMaybeNavigableKeySet();
        }
        
        @Override
        public Comparator<? super V> valueComparator() {
            return this.valueComparator;
        }
    }
    
    public static class MapMultimap<K, V> extends a implements pm1, Serializable
    {
        private static final long serialVersionUID = 7845222491160860175L;
        final Map<K, V> map;
        
        public MapMultimap(final Map<K, V> map) {
            this.map = (Map)i71.r(map);
        }
        
        @Override
        public void clear() {
            this.map.clear();
        }
        
        @Override
        public boolean containsEntry(final Object o, final Object o2) {
            return this.map.entrySet().contains(Maps.j(o, o2));
        }
        
        @Override
        public boolean containsKey(final Object o) {
            return this.map.containsKey(o);
        }
        
        @Override
        public boolean containsValue(final Object o) {
            return this.map.containsValue(o);
        }
        
        @Override
        public Map<K, Collection<V>> createAsMap() {
            return new Multimaps.a(this);
        }
        
        @Override
        public Collection<Map.Entry<K, V>> createEntries() {
            throw new AssertionError((Object)"unreachable");
        }
        
        @Override
        public Set<K> createKeySet() {
            return this.map.keySet();
        }
        
        @Override
        public j createKeys() {
            return new Multimaps.c(this);
        }
        
        @Override
        public Collection<V> createValues() {
            return this.map.values();
        }
        
        @Override
        public Set<Map.Entry<K, V>> entries() {
            return this.map.entrySet();
        }
        
        public Iterator<Map.Entry<K, V>> entryIterator() {
            return this.map.entrySet().iterator();
        }
        
        @Override
        public Set<V> get(final K k) {
            return new Sets.a(this, k) {
                public final Object a;
                public final MapMultimap b;
                
                @Override
                public Iterator iterator() {
                    return new Iterator(this) {
                        public int a;
                        public final Multimaps$MapMultimap$a b;
                        
                        @Override
                        public boolean hasNext() {
                            if (this.a == 0) {
                                final Sets.a b = this.b;
                                if (b.b.map.containsKey(b.a)) {
                                    return true;
                                }
                            }
                            return false;
                        }
                        
                        @Override
                        public Object next() {
                            if (this.hasNext()) {
                                ++this.a;
                                final Sets.a b = this.b;
                                return k01.a(b.b.map.get(b.a));
                            }
                            throw new NoSuchElementException();
                        }
                        
                        @Override
                        public void remove() {
                            final int a = this.a;
                            boolean b = true;
                            if (a != 1) {
                                b = false;
                            }
                            hh.e(b);
                            this.a = -1;
                            final Sets.a b2 = this.b;
                            b2.b.map.remove(b2.a);
                        }
                    };
                }
                
                @Override
                public int size() {
                    return this.b.map.containsKey(this.a) ? 1 : 0;
                }
            };
        }
        
        @Override
        public int hashCode() {
            return this.map.hashCode();
        }
        
        @Override
        public boolean put(final K k, final V v) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public boolean putAll(final K k, final Iterable<? extends V> iterable) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public boolean putAll(final rx0 rx0) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public boolean remove(final Object o, final Object o2) {
            return this.map.entrySet().remove(Maps.j(o, o2));
        }
        
        @Override
        public Set<V> removeAll(final Object o) {
            final HashSet set = new HashSet(2);
            if (!this.map.containsKey(o)) {
                return set;
            }
            set.add(this.map.remove(o));
            return set;
        }
        
        @Override
        public Set<V> replaceValues(final K k, final Iterable<? extends V> iterable) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public int size() {
            return this.map.size();
        }
    }
    
    public static class UnmodifiableListMultimap<K, V> extends UnmodifiableMultimap<K, V> implements dk0
    {
        private static final long serialVersionUID = 0L;
        
        public UnmodifiableListMultimap(final dk0 dk0) {
            super(dk0);
        }
        
        @Override
        public dk0 delegate() {
            return (dk0)super.delegate();
        }
        
        @Override
        public List<V> get(final K k) {
            return Collections.unmodifiableList((List<? extends V>)this.delegate().get((Object)k));
        }
        
        @Override
        public List<V> removeAll(final Object o) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public List<V> replaceValues(final K k, final Iterable<? extends V> iterable) {
            throw new UnsupportedOperationException();
        }
    }
    
    public static class UnmodifiableMultimap<K, V> extends z70 implements Serializable
    {
        private static final long serialVersionUID = 0L;
        final rx0 delegate;
        transient Collection<Map.Entry<K, V>> entries;
        transient Set<K> keySet;
        transient j keys;
        transient Map<K, Collection<V>> map;
        transient Collection<V> values;
        
        public UnmodifiableMultimap(final rx0 rx0) {
            this.delegate = (rx0)i71.r(rx0);
        }
        
        @Override
        public Map<K, Collection<V>> asMap() {
            Object map;
            if ((map = this.map) == null) {
                map = Collections.unmodifiableMap((Map<? extends K, ? extends Collection<V>>)Maps.C(this.delegate.asMap(), new m90(this) {
                    public Collection a(final Collection collection) {
                        return e(collection);
                    }
                }));
                this.map = (Map<K, Collection<V>>)map;
            }
            return (Map<K, Collection<V>>)map;
        }
        
        @Override
        public void clear() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public rx0 delegate() {
            return this.delegate;
        }
        
        @Override
        public Collection<Map.Entry<K, V>> entries() {
            Collection<Map.Entry<K, V>> entries;
            if ((entries = this.entries) == null) {
                entries = d(this.delegate.entries());
                this.entries = entries;
            }
            return entries;
        }
        
        @Override
        public Collection<V> get(final K k) {
            return e(this.delegate.get(k));
        }
        
        @Override
        public Set<K> keySet() {
            Object keySet;
            if ((keySet = this.keySet) == null) {
                keySet = Collections.unmodifiableSet((Set<? extends K>)this.delegate.keySet());
                this.keySet = (Set<K>)keySet;
            }
            return (Set<K>)keySet;
        }
        
        @Override
        public j keys() {
            j keys;
            if ((keys = this.keys) == null) {
                keys = Multisets.o(this.delegate.keys());
                this.keys = keys;
            }
            return keys;
        }
        
        @Override
        public boolean put(final K k, final V v) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public boolean putAll(final K k, final Iterable<? extends V> iterable) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public boolean putAll(final rx0 rx0) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public boolean remove(final Object o, final Object o2) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public Collection<V> removeAll(final Object o) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public Collection<V> replaceValues(final K k, final Iterable<? extends V> iterable) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public Collection<V> values() {
            Object values;
            if ((values = this.values) == null) {
                values = Collections.unmodifiableCollection((Collection<? extends V>)this.delegate.values());
                this.values = (Collection<V>)values;
            }
            return (Collection<V>)values;
        }
    }
    
    public static class UnmodifiableSetMultimap<K, V> extends UnmodifiableMultimap<K, V> implements pm1
    {
        private static final long serialVersionUID = 0L;
        
        public UnmodifiableSetMultimap(final pm1 pm1) {
            super(pm1);
        }
        
        @Override
        public pm1 delegate() {
            return (pm1)super.delegate();
        }
        
        @Override
        public Set<Map.Entry<K, V>> entries() {
            return Maps.G(this.delegate().entries());
        }
        
        @Override
        public Set<V> get(final K k) {
            return Collections.unmodifiableSet((Set<? extends V>)this.delegate().get((Object)k));
        }
        
        @Override
        public Set<V> removeAll(final Object o) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public Set<V> replaceValues(final K k, final Iterable<? extends V> iterable) {
            throw new UnsupportedOperationException();
        }
    }
    
    public static class UnmodifiableSortedSetMultimap<K, V> extends UnmodifiableSetMultimap<K, V> implements to1
    {
        private static final long serialVersionUID = 0L;
        
        public UnmodifiableSortedSetMultimap(final to1 to1) {
            super(to1);
        }
        
        @Override
        public to1 delegate() {
            return (to1)super.delegate();
        }
        
        @Override
        public SortedSet<V> get(final K k) {
            return Collections.unmodifiableSortedSet((SortedSet<V>)this.delegate().get((Object)k));
        }
        
        @Override
        public SortedSet<V> removeAll(final Object o) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public SortedSet<V> replaceValues(final K k, final Iterable<? extends V> iterable) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public Comparator<? super V> valueComparator() {
            return this.delegate().valueComparator();
        }
    }
    
    public static final class a extends u
    {
        public final rx0 d;
        
        public a(final rx0 rx0) {
            this.d = (rx0)i71.r(rx0);
        }
        
        public static /* synthetic */ rx0 d(final Multimaps.a a) {
            return a.d;
        }
        
        public Set a() {
            return new Multimaps.a.a();
        }
        
        @Override
        public void clear() {
            this.d.clear();
        }
        
        @Override
        public boolean containsKey(final Object o) {
            return this.d.containsKey(o);
        }
        
        public Collection e(final Object o) {
            Collection value;
            if (this.containsKey(o)) {
                value = this.d.get(o);
            }
            else {
                value = null;
            }
            return value;
        }
        
        public Collection f(final Object o) {
            Collection removeAll;
            if (this.containsKey(o)) {
                removeAll = this.d.removeAll(o);
            }
            else {
                removeAll = null;
            }
            return removeAll;
        }
        
        public void g(final Object o) {
            this.d.keySet().remove(o);
        }
        
        @Override
        public boolean isEmpty() {
            return this.d.isEmpty();
        }
        
        @Override
        public Set keySet() {
            return this.d.keySet();
        }
        
        @Override
        public int size() {
            return this.d.keySet().size();
        }
        
        public class a extends j
        {
            public final Multimaps.a a;
            
            public a(final Multimaps.a a) {
                this.a = a;
            }
            
            @Override
            public Map a() {
                return this.a;
            }
            
            @Override
            public Iterator iterator() {
                return Maps.d(Multimaps.a.d(this.a).keySet(), new m90(this) {
                    public final a a;
                    
                    public Collection a(final Object o) {
                        return Multimaps.a.d(this.a.a).get(o);
                    }
                });
            }
            
            @Override
            public boolean remove(final Object o) {
                if (!((Maps.j)this).contains(o)) {
                    return false;
                }
                final Entry obj = (Entry)o;
                Objects.requireNonNull(obj);
                this.a.g(((Map.Entry)obj).getKey());
                return true;
            }
        }
    }
    
    public abstract static class b extends AbstractCollection
    {
        public abstract rx0 a();
        
        @Override
        public void clear() {
            this.a().clear();
        }
        
        @Override
        public boolean contains(final Object o) {
            if (o instanceof Map.Entry) {
                final Map.Entry entry = (Map.Entry)o;
                return this.a().containsEntry(entry.getKey(), entry.getValue());
            }
            return false;
        }
        
        @Override
        public boolean remove(final Object o) {
            if (o instanceof Map.Entry) {
                final Map.Entry entry = (Map.Entry)o;
                return this.a().remove(entry.getKey(), entry.getValue());
            }
            return false;
        }
        
        @Override
        public int size() {
            return this.a().size();
        }
    }
    
    public static class c extends b
    {
        public final rx0 a;
        
        public c(final rx0 a) {
            this.a = a;
        }
        
        @Override
        public void clear() {
            this.a.clear();
        }
        
        @Override
        public boolean contains(final Object o) {
            return this.a.containsKey(o);
        }
        
        @Override
        public int count(final Object o) {
            final Collection collection = (Collection)Maps.w(this.a.asMap(), o);
            int size;
            if (collection == null) {
                size = 0;
            }
            else {
                size = collection.size();
            }
            return size;
        }
        
        @Override
        public int distinctElements() {
            return this.a.asMap().size();
        }
        
        @Override
        public Iterator elementIterator() {
            throw new AssertionError((Object)"should never be called");
        }
        
        @Override
        public Set elementSet() {
            return this.a.keySet();
        }
        
        public Iterator entryIterator() {
            return new r(this, this.a.asMap().entrySet().iterator()) {
                public j.a c(final Map.Entry entry) {
                    return new Multisets.b(this, entry) {
                        public final Map.Entry a;
                        
                        @Override
                        public int getCount() {
                            return this.a.getValue().size();
                        }
                        
                        @Override
                        public Object getElement() {
                            return this.a.getKey();
                        }
                    };
                }
            };
        }
        
        @Override
        public Iterator iterator() {
            return Maps.m(this.a.entries().iterator());
        }
        
        @Override
        public int remove(final Object o, final int n) {
            hh.b(n, "occurrences");
            if (n == 0) {
                return this.count(o);
            }
            final Collection collection = (Collection)Maps.w(this.a.asMap(), o);
            int i = 0;
            if (collection == null) {
                return 0;
            }
            final int size = collection.size();
            if (n >= size) {
                collection.clear();
            }
            else {
                final Iterator iterator = collection.iterator();
                while (i < n) {
                    iterator.next();
                    iterator.remove();
                    ++i;
                }
            }
            return size;
        }
        
        @Override
        public int size() {
            return this.a.size();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Objects;

class RegularImmutableList<E> extends ImmutableList<E>
{
    static final ImmutableList<Object> EMPTY;
    final transient Object[] array;
    private final transient int size;
    
    static {
        EMPTY = new RegularImmutableList<Object>(new Object[0], 0);
    }
    
    public RegularImmutableList(final Object[] array, final int size) {
        this.array = array;
        this.size = size;
    }
    
    @Override
    public int copyIntoArray(final Object[] array, final int n) {
        System.arraycopy(this.array, 0, array, n, this.size);
        return n + this.size;
    }
    
    @Override
    public E get(final int n) {
        i71.p(n, this.size);
        final Object obj = this.array[n];
        Objects.requireNonNull(obj);
        return (E)obj;
    }
    
    @Override
    public Object[] internalArray() {
        return this.array;
    }
    
    @Override
    public int internalArrayEnd() {
        return this.size;
    }
    
    @Override
    public int internalArrayStart() {
        return 0;
    }
    
    @Override
    public boolean isPartialView() {
        return false;
    }
    
    @Override
    public int size() {
        return this.size;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.RandomAccess;
import java.util.Set;
import java.io.Serializable;
import java.util.Iterator;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Collections;
import java.util.List;
import java.util.Comparator;

abstract class RegularImmutableTable<R, C, V> extends ImmutableTable<R, C, V>
{
    public RegularImmutableTable() {
    }
    
    public static <R, C, V> RegularImmutableTable<R, C, V> forCells(final Iterable<q.a> iterable) {
        return forCellsInternal(iterable, (Comparator<? super R>)null, (Comparator<? super C>)null);
    }
    
    public static <R, C, V> RegularImmutableTable<R, C, V> forCells(final List<q.a> list, final Comparator<? super R> comparator, final Comparator<? super C> comparator2) {
        i71.r(list);
        if (comparator != null || comparator2 != null) {
            Collections.sort((List<Object>)list, new m(comparator, comparator2));
        }
        return forCellsInternal((Iterable<q.a>)list, comparator, comparator2);
    }
    
    private static <R, C, V> RegularImmutableTable<R, C, V> forCellsInternal(final Iterable<q.a> iterable, final Comparator<? super R> comparator, final Comparator<? super C> comparator2) {
        final LinkedHashSet set = new LinkedHashSet();
        final LinkedHashSet set2 = new LinkedHashSet();
        final ImmutableList<Object> copy = (ImmutableList<Object>)ImmutableList.copyOf((Iterable<? extends q.a>)iterable);
        for (final q.a a : iterable) {
            set.add(a.getRowKey());
            set2.add(a.getColumnKey());
        }
        ImmutableSet<Object> set3;
        if (comparator == null) {
            set3 = (ImmutableSet<Object>)ImmutableSet.copyOf((Collection<? extends R>)set);
        }
        else {
            set3 = (ImmutableSet<Object>)ImmutableSet.copyOf((Collection<? extends R>)ImmutableList.sortedCopyOf((Comparator<? super Object>)comparator, (Iterable<?>)set));
        }
        ImmutableSet<Object> set4;
        if (comparator2 == null) {
            set4 = (ImmutableSet<Object>)ImmutableSet.copyOf((Collection<? extends C>)set2);
        }
        else {
            set4 = (ImmutableSet<Object>)ImmutableSet.copyOf((Collection<? extends C>)ImmutableList.sortedCopyOf((Comparator<? super Object>)comparator2, (Iterable<?>)set2));
        }
        return (RegularImmutableTable<R, C, V>)forOrderedComponents((ImmutableList<q.a>)copy, (ImmutableSet<R>)set3, (ImmutableSet<C>)set4);
    }
    
    public static <R, C, V> RegularImmutableTable<R, C, V> forOrderedComponents(final ImmutableList<q.a> list, final ImmutableSet<R> set, final ImmutableSet<C> set2) {
        Serializable s;
        if (list.size() > set.size() * (long)set2.size() / 2L) {
            s = new DenseImmutableTable<Object, Object, Object>(list, set, set2);
        }
        else {
            s = new SparseImmutableTable<Object, Object, Object>(list, set, set2);
        }
        return (RegularImmutableTable<R, C, V>)s;
    }
    
    public final void checkNoDuplicate(final R r, final C c, final V v, final V v2) {
        i71.o(v == null, "Duplicate key: (row=%s, column=%s), values: [%s, %s].", r, c, v2, v);
    }
    
    @Override
    public final ImmutableSet<q.a> createCellSet() {
        Serializable of;
        if (this.isEmpty()) {
            of = ImmutableSet.of();
        }
        else {
            of = new CellSet(null);
        }
        return (ImmutableSet<q.a>)of;
    }
    
    @Override
    public final ImmutableCollection<V> createValues() {
        RandomAccess of;
        if (this.isEmpty()) {
            of = ImmutableList.of();
        }
        else {
            of = new Values(null);
        }
        return (ImmutableCollection<V>)of;
    }
    
    public abstract q.a getCell(final int p0);
    
    public abstract V getValue(final int p0);
    
    @Override
    public abstract /* synthetic */ int size();
    
    public final class CellSet extends IndexedImmutableSet<q.a>
    {
        final RegularImmutableTable this$0;
        
        private CellSet(final RegularImmutableTable this$0) {
            this.this$0 = this$0;
        }
        
        @Override
        public boolean contains(Object value) {
            final boolean b = value instanceof q.a;
            boolean b3;
            final boolean b2 = b3 = false;
            if (b) {
                final q.a a = (q.a)value;
                value = this.this$0.get(a.getRowKey(), a.getColumnKey());
                b3 = b2;
                if (value != null) {
                    b3 = b2;
                    if (value.equals(a.getValue())) {
                        b3 = true;
                    }
                }
            }
            return b3;
        }
        
        @Override
        public q.a get(final int n) {
            return this.this$0.getCell(n);
        }
        
        @Override
        public boolean isPartialView() {
            return false;
        }
        
        @Override
        public int size() {
            return this.this$0.size();
        }
    }
    
    public final class Values extends ImmutableList<V>
    {
        final RegularImmutableTable this$0;
        
        private Values(final RegularImmutableTable this$0) {
            this.this$0 = this$0;
        }
        
        @Override
        public V get(final int n) {
            return this.this$0.getValue(n);
        }
        
        @Override
        public boolean isPartialView() {
            return true;
        }
        
        @Override
        public int size() {
            return this.this$0.size();
        }
    }
}

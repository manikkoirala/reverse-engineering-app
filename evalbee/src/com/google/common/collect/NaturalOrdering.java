// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.io.Serializable;

final class NaturalOrdering extends Ordering implements Serializable
{
    static final NaturalOrdering INSTANCE;
    private static final long serialVersionUID = 0L;
    private transient Ordering nullsFirst;
    private transient Ordering nullsLast;
    
    static {
        INSTANCE = new NaturalOrdering();
    }
    
    private NaturalOrdering() {
    }
    
    private Object readResolve() {
        return NaturalOrdering.INSTANCE;
    }
    
    public int compare(final Comparable<?> comparable, final Comparable<?> comparable2) {
        i71.r(comparable);
        i71.r(comparable2);
        return comparable.compareTo(comparable2);
    }
    
    @Override
    public <S extends Comparable<?>> Ordering nullsFirst() {
        Ordering nullsFirst;
        if ((nullsFirst = this.nullsFirst) == null) {
            nullsFirst = super.nullsFirst();
            this.nullsFirst = nullsFirst;
        }
        return nullsFirst;
    }
    
    @Override
    public <S extends Comparable<?>> Ordering nullsLast() {
        Ordering nullsLast;
        if ((nullsLast = this.nullsLast) == null) {
            nullsLast = super.nullsLast();
            this.nullsLast = nullsLast;
        }
        return nullsLast;
    }
    
    @Override
    public <S extends Comparable<?>> Ordering reverse() {
        return ReverseNaturalOrdering.INSTANCE;
    }
    
    @Override
    public String toString() {
        return "Ordering.natural()";
    }
}

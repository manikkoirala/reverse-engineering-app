// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Comparator;
import java.util.ListIterator;
import java.util.Queue;
import java.util.Deque;
import java.util.Iterator;
import java.util.SortedMap;
import java.util.NavigableSet;
import java.util.NavigableMap;
import java.util.RandomAccess;
import java.util.Map;
import java.util.Collection;
import java.util.Set;
import java.util.List;
import java.util.SortedSet;

public abstract class Synchronized
{
    public static Collection g(final Collection collection, final Object o) {
        return new SynchronizedCollection(collection, o, null);
    }
    
    public static List h(final List list, final Object o) {
        SynchronizedObject synchronizedObject;
        if (list instanceof RandomAccess) {
            synchronizedObject = new SynchronizedRandomAccessList<Object>(list, o);
        }
        else {
            synchronizedObject = new SynchronizedList<Object>(list, o);
        }
        return (List)synchronizedObject;
    }
    
    public static Map i(final Map map, final Object o) {
        return new SynchronizedMap(map, o);
    }
    
    public static j j(final j j, final Object o) {
        if (!(j instanceof SynchronizedMultiset) && !(j instanceof ImmutableMultiset)) {
            return new SynchronizedMultiset<Object>(j, o);
        }
        return j;
    }
    
    public static NavigableMap k(final NavigableMap navigableMap, final Object o) {
        return new SynchronizedNavigableMap(navigableMap, o);
    }
    
    public static NavigableSet l(final NavigableSet set, final Object o) {
        return new SynchronizedNavigableSet(set, o);
    }
    
    public static Map.Entry m(final Map.Entry entry, final Object o) {
        if (entry == null) {
            return null;
        }
        return new SynchronizedEntry(entry, o);
    }
    
    public static Set n(final Set set, final Object o) {
        return new SynchronizedSet(set, o);
    }
    
    public static SortedMap o(final SortedMap sortedMap, final Object o) {
        return new SynchronizedSortedMap(sortedMap, o);
    }
    
    public static SortedSet p(final SortedSet set, final Object o) {
        return new SynchronizedSortedSet(set, o);
    }
    
    public static Collection q(final Collection collection, final Object o) {
        if (collection instanceof SortedSet) {
            return p((SortedSet)collection, o);
        }
        if (collection instanceof Set) {
            return n((Set)collection, o);
        }
        if (collection instanceof List) {
            return h((List)collection, o);
        }
        return g(collection, o);
    }
    
    public static Set r(final Set set, final Object o) {
        if (set instanceof SortedSet) {
            return p((SortedSet)set, o);
        }
        return n(set, o);
    }
    
    public static class SynchronizedAsMap<K, V> extends SynchronizedMap<K, Collection<V>>
    {
        private static final long serialVersionUID = 0L;
        transient Set<Entry<K, Collection<V>>> asMapEntrySet;
        transient Collection<Collection<V>> asMapValues;
        
        public SynchronizedAsMap(final Map<K, Collection<V>> map, final Object o) {
            super(map, o);
        }
        
        @Override
        public boolean containsValue(final Object o) {
            return this.values().contains(o);
        }
        
        @Override
        public Set<Entry<K, Collection<V>>> entrySet() {
            synchronized (super.mutex) {
                if (this.asMapEntrySet == null) {
                    this.asMapEntrySet = (Set<Entry<K, Collection<V>>>)new SynchronizedAsMapEntries((Set<Entry<Object, Collection<Object>>>)this.delegate().entrySet(), super.mutex);
                }
                return this.asMapEntrySet;
            }
        }
        
        @Override
        public Collection<V> get(final Object o) {
            synchronized (super.mutex) {
                final Collection collection = super.get(o);
                Collection<V> d;
                if (collection == null) {
                    d = null;
                }
                else {
                    d = q(collection, super.mutex);
                }
                return d;
            }
        }
        
        @Override
        public Collection<Collection<V>> values() {
            synchronized (super.mutex) {
                if (this.asMapValues == null) {
                    this.asMapValues = (Collection<Collection<V>>)new SynchronizedAsMapValues((Collection<Collection<Object>>)this.delegate().values(), super.mutex);
                }
                return this.asMapValues;
            }
        }
    }
    
    public static class SynchronizedMap<K, V> extends SynchronizedObject implements Map<K, V>
    {
        private static final long serialVersionUID = 0L;
        transient Set<Entry<K, V>> entrySet;
        transient Set<K> keySet;
        transient Collection<V> values;
        
        public SynchronizedMap(final Map<K, V> map, final Object o) {
            super(map, o);
        }
        
        @Override
        public void clear() {
            synchronized (super.mutex) {
                this.delegate().clear();
            }
        }
        
        @Override
        public boolean containsKey(final Object o) {
            synchronized (super.mutex) {
                return this.delegate().containsKey(o);
            }
        }
        
        @Override
        public boolean containsValue(final Object o) {
            synchronized (super.mutex) {
                return this.delegate().containsValue(o);
            }
        }
        
        Map<K, V> delegate() {
            return (Map)super.delegate();
        }
        
        @Override
        public Set<Entry<K, V>> entrySet() {
            synchronized (super.mutex) {
                if (this.entrySet == null) {
                    this.entrySet = Synchronized.n(this.delegate().entrySet(), super.mutex);
                }
                return this.entrySet;
            }
        }
        
        @Override
        public boolean equals(final Object o) {
            if (o == this) {
                return true;
            }
            synchronized (super.mutex) {
                return this.delegate().equals(o);
            }
        }
        
        @Override
        public V get(Object value) {
            synchronized (super.mutex) {
                value = this.delegate().get(value);
                return (V)value;
            }
        }
        
        @Override
        public int hashCode() {
            synchronized (super.mutex) {
                return this.delegate().hashCode();
            }
        }
        
        @Override
        public boolean isEmpty() {
            synchronized (super.mutex) {
                return this.delegate().isEmpty();
            }
        }
        
        @Override
        public Set<K> keySet() {
            synchronized (super.mutex) {
                if (this.keySet == null) {
                    this.keySet = Synchronized.n(this.delegate().keySet(), super.mutex);
                }
                return this.keySet;
            }
        }
        
        @Override
        public V put(final K k, final V v) {
            synchronized (super.mutex) {
                return this.delegate().put(k, v);
            }
        }
        
        @Override
        public void putAll(final Map<? extends K, ? extends V> map) {
            synchronized (super.mutex) {
                this.delegate().putAll(map);
            }
        }
        
        @Override
        public V remove(Object remove) {
            synchronized (super.mutex) {
                remove = this.delegate().remove(remove);
                return (V)remove;
            }
        }
        
        @Override
        public int size() {
            synchronized (super.mutex) {
                return this.delegate().size();
            }
        }
        
        @Override
        public Collection<V> values() {
            synchronized (super.mutex) {
                if (this.values == null) {
                    this.values = g(this.delegate().values(), super.mutex);
                }
                return this.values;
            }
        }
    }
    
    public static class SynchronizedAsMapEntries<K, V> extends SynchronizedSet<Map.Entry<K, Collection<V>>>
    {
        private static final long serialVersionUID = 0L;
        
        public SynchronizedAsMapEntries(final Set<Map.Entry<K, Collection<V>>> set, final Object o) {
            super(set, o);
        }
        
        @Override
        public boolean contains(final Object o) {
            synchronized (super.mutex) {
                return Maps.f(this.delegate(), o);
            }
        }
        
        @Override
        public boolean containsAll(final Collection<?> collection) {
            synchronized (super.mutex) {
                return lh.a(this.delegate(), collection);
            }
        }
        
        @Override
        public boolean equals(final Object o) {
            if (o == this) {
                return true;
            }
            synchronized (super.mutex) {
                return Sets.a(this.delegate(), o);
            }
        }
        
        @Override
        public Iterator<Map.Entry<K, Collection<V>>> iterator() {
            return new r(this, super.iterator()) {
                public final SynchronizedAsMapEntries b;
                
                public Map.Entry c(final Map.Entry entry) {
                    return new y70(this, entry) {
                        public final Entry a;
                        public final Synchronized$SynchronizedAsMapEntries$a b;
                        
                        @Override
                        public Entry b() {
                            return this.a;
                        }
                        
                        public Collection c() {
                            return q(this.a.getValue(), this.b.b.mutex);
                        }
                    };
                }
            };
        }
        
        @Override
        public boolean remove(final Object o) {
            synchronized (super.mutex) {
                return Maps.u(this.delegate(), o);
            }
        }
        
        @Override
        public boolean removeAll(final Collection<?> collection) {
            synchronized (super.mutex) {
                return Iterators.r(this.delegate().iterator(), collection);
            }
        }
        
        @Override
        public boolean retainAll(final Collection<?> collection) {
            synchronized (super.mutex) {
                return Iterators.s(this.delegate().iterator(), collection);
            }
        }
        
        @Override
        public Object[] toArray() {
            synchronized (super.mutex) {
                return t01.f(this.delegate());
            }
        }
        
        @Override
        public <T> T[] toArray(final T[] array) {
            synchronized (super.mutex) {
                return (T[])t01.g(this.delegate(), array);
            }
        }
    }
    
    public static class SynchronizedSet<E> extends SynchronizedCollection<E> implements Set<E>
    {
        private static final long serialVersionUID = 0L;
        
        public SynchronizedSet(final Set<E> set, final Object o) {
            super(set, o, null);
        }
        
        Set<E> delegate() {
            return (Set)super.delegate();
        }
        
        @Override
        public boolean equals(final Object o) {
            if (o == this) {
                return true;
            }
            synchronized (super.mutex) {
                return this.delegate().equals(o);
            }
        }
        
        @Override
        public int hashCode() {
            synchronized (super.mutex) {
                return this.delegate().hashCode();
            }
        }
    }
    
    public static class SynchronizedCollection<E> extends SynchronizedObject implements Collection<E>
    {
        private static final long serialVersionUID = 0L;
        
        private SynchronizedCollection(final Collection<E> collection, final Object o) {
            super(collection, o);
        }
        
        @Override
        public boolean add(final E e) {
            synchronized (super.mutex) {
                return this.delegate().add(e);
            }
        }
        
        @Override
        public boolean addAll(final Collection<? extends E> collection) {
            synchronized (super.mutex) {
                return this.delegate().addAll(collection);
            }
        }
        
        @Override
        public void clear() {
            synchronized (super.mutex) {
                this.delegate().clear();
            }
        }
        
        @Override
        public boolean contains(final Object o) {
            synchronized (super.mutex) {
                return this.delegate().contains(o);
            }
        }
        
        @Override
        public boolean containsAll(final Collection<?> collection) {
            synchronized (super.mutex) {
                return this.delegate().containsAll(collection);
            }
        }
        
        Collection<E> delegate() {
            return (Collection)super.delegate();
        }
        
        @Override
        public boolean isEmpty() {
            synchronized (super.mutex) {
                return this.delegate().isEmpty();
            }
        }
        
        @Override
        public Iterator<E> iterator() {
            return this.delegate().iterator();
        }
        
        @Override
        public boolean remove(final Object o) {
            synchronized (super.mutex) {
                return this.delegate().remove(o);
            }
        }
        
        @Override
        public boolean removeAll(final Collection<?> collection) {
            synchronized (super.mutex) {
                return this.delegate().removeAll(collection);
            }
        }
        
        @Override
        public boolean retainAll(final Collection<?> collection) {
            synchronized (super.mutex) {
                return this.delegate().retainAll(collection);
            }
        }
        
        @Override
        public int size() {
            synchronized (super.mutex) {
                return this.delegate().size();
            }
        }
        
        @Override
        public Object[] toArray() {
            synchronized (super.mutex) {
                return this.delegate().toArray();
            }
        }
        
        @Override
        public <T> T[] toArray(final T[] array) {
            synchronized (super.mutex) {
                return this.delegate().toArray(array);
            }
        }
    }
    
    public static class SynchronizedAsMapValues<V> extends SynchronizedCollection<Collection<V>>
    {
        private static final long serialVersionUID = 0L;
        
        public SynchronizedAsMapValues(final Collection<Collection<V>> collection, final Object o) {
            super(collection, o, null);
        }
        
        @Override
        public Iterator<Collection<V>> iterator() {
            return new r(this, super.iterator()) {
                public final SynchronizedAsMapValues b;
                
                public Collection c(final Collection collection) {
                    return q(collection, this.b.mutex);
                }
            };
        }
    }
    
    public static class SynchronizedBiMap<K, V> extends SynchronizedMap<K, V> implements ub
    {
        private static final long serialVersionUID = 0L;
        private transient ub inverse;
        private transient Set<V> valueSet;
        
        private SynchronizedBiMap(final ub ub, final Object o, final ub inverse) {
            super(ub, o);
            this.inverse = inverse;
        }
        
        public ub delegate() {
            return (ub)super.delegate();
        }
        
        @Override
        public V forcePut(final K k, final V v) {
            synchronized (super.mutex) {
                return (V)this.delegate().forcePut(k, v);
            }
        }
        
        @Override
        public ub inverse() {
            synchronized (super.mutex) {
                if (this.inverse == null) {
                    this.inverse = new SynchronizedBiMap<Object, Object>(this.delegate().inverse(), super.mutex, this);
                }
                return this.inverse;
            }
        }
        
        @Override
        public Set<V> values() {
            synchronized (super.mutex) {
                if (this.valueSet == null) {
                    this.valueSet = Synchronized.n(this.delegate().values(), super.mutex);
                }
                return this.valueSet;
            }
        }
    }
    
    public static final class SynchronizedDeque<E> extends SynchronizedQueue<E> implements Deque<E>
    {
        private static final long serialVersionUID = 0L;
        
        public SynchronizedDeque(final Deque<E> deque, final Object o) {
            super(deque, o);
        }
        
        @Override
        public void addFirst(final E e) {
            synchronized (super.mutex) {
                this.delegate().addFirst(e);
            }
        }
        
        @Override
        public void addLast(final E e) {
            synchronized (super.mutex) {
                this.delegate().addLast(e);
            }
        }
        
        public Deque<E> delegate() {
            return (Deque)super.delegate();
        }
        
        @Override
        public Iterator<E> descendingIterator() {
            synchronized (super.mutex) {
                return this.delegate().descendingIterator();
            }
        }
        
        @Override
        public E getFirst() {
            synchronized (super.mutex) {
                return this.delegate().getFirst();
            }
        }
        
        @Override
        public E getLast() {
            synchronized (super.mutex) {
                return this.delegate().getLast();
            }
        }
        
        @Override
        public boolean offerFirst(final E e) {
            synchronized (super.mutex) {
                return this.delegate().offerFirst(e);
            }
        }
        
        @Override
        public boolean offerLast(final E e) {
            synchronized (super.mutex) {
                return this.delegate().offerLast(e);
            }
        }
        
        @Override
        public E peekFirst() {
            synchronized (super.mutex) {
                return this.delegate().peekFirst();
            }
        }
        
        @Override
        public E peekLast() {
            synchronized (super.mutex) {
                return this.delegate().peekLast();
            }
        }
        
        @Override
        public E pollFirst() {
            synchronized (super.mutex) {
                return this.delegate().pollFirst();
            }
        }
        
        @Override
        public E pollLast() {
            synchronized (super.mutex) {
                return this.delegate().pollLast();
            }
        }
        
        @Override
        public E pop() {
            synchronized (super.mutex) {
                return this.delegate().pop();
            }
        }
        
        @Override
        public void push(final E e) {
            synchronized (super.mutex) {
                this.delegate().push(e);
            }
        }
        
        @Override
        public E removeFirst() {
            synchronized (super.mutex) {
                return this.delegate().removeFirst();
            }
        }
        
        @Override
        public boolean removeFirstOccurrence(final Object o) {
            synchronized (super.mutex) {
                return this.delegate().removeFirstOccurrence(o);
            }
        }
        
        @Override
        public E removeLast() {
            synchronized (super.mutex) {
                return this.delegate().removeLast();
            }
        }
        
        @Override
        public boolean removeLastOccurrence(final Object o) {
            synchronized (super.mutex) {
                return this.delegate().removeLastOccurrence(o);
            }
        }
    }
    
    public static class SynchronizedEntry<K, V> extends SynchronizedObject implements Entry<K, V>
    {
        private static final long serialVersionUID = 0L;
        
        public SynchronizedEntry(final Entry<K, V> entry, final Object o) {
            super(entry, o);
        }
        
        public Entry<K, V> delegate() {
            return (Entry)super.delegate();
        }
        
        @Override
        public boolean equals(final Object o) {
            synchronized (super.mutex) {
                return this.delegate().equals(o);
            }
        }
        
        @Override
        public K getKey() {
            synchronized (super.mutex) {
                return this.delegate().getKey();
            }
        }
        
        @Override
        public V getValue() {
            synchronized (super.mutex) {
                return this.delegate().getValue();
            }
        }
        
        @Override
        public int hashCode() {
            synchronized (super.mutex) {
                return this.delegate().hashCode();
            }
        }
        
        @Override
        public V setValue(final V value) {
            synchronized (super.mutex) {
                return this.delegate().setValue(value);
            }
        }
    }
    
    public static class SynchronizedList<E> extends SynchronizedCollection<E> implements List<E>
    {
        private static final long serialVersionUID = 0L;
        
        public SynchronizedList(final List<E> list, final Object o) {
            super(list, o, null);
        }
        
        @Override
        public void add(final int n, final E e) {
            synchronized (super.mutex) {
                this.delegate().add(n, e);
            }
        }
        
        @Override
        public boolean addAll(final int n, final Collection<? extends E> collection) {
            synchronized (super.mutex) {
                return this.delegate().addAll(n, collection);
            }
        }
        
        public List<E> delegate() {
            return (List)super.delegate();
        }
        
        @Override
        public boolean equals(final Object o) {
            if (o == this) {
                return true;
            }
            synchronized (super.mutex) {
                return this.delegate().equals(o);
            }
        }
        
        @Override
        public E get(final int n) {
            synchronized (super.mutex) {
                return this.delegate().get(n);
            }
        }
        
        @Override
        public int hashCode() {
            synchronized (super.mutex) {
                return this.delegate().hashCode();
            }
        }
        
        @Override
        public int indexOf(final Object o) {
            synchronized (super.mutex) {
                return this.delegate().indexOf(o);
            }
        }
        
        @Override
        public int lastIndexOf(final Object o) {
            synchronized (super.mutex) {
                return this.delegate().lastIndexOf(o);
            }
        }
        
        @Override
        public ListIterator<E> listIterator() {
            return this.delegate().listIterator();
        }
        
        @Override
        public ListIterator<E> listIterator(final int n) {
            return this.delegate().listIterator(n);
        }
        
        @Override
        public E remove(final int n) {
            synchronized (super.mutex) {
                return this.delegate().remove(n);
            }
        }
        
        @Override
        public E set(final int n, final E e) {
            synchronized (super.mutex) {
                return this.delegate().set(n, e);
            }
        }
        
        @Override
        public List<E> subList(final int n, final int n2) {
            synchronized (super.mutex) {
                return h(this.delegate().subList(n, n2), super.mutex);
            }
        }
    }
    
    public static class SynchronizedListMultimap<K, V> extends SynchronizedMultimap<K, V> implements dk0
    {
        private static final long serialVersionUID = 0L;
        
        public SynchronizedListMultimap(final dk0 dk0, final Object o) {
            super(dk0, o);
        }
        
        public dk0 delegate() {
            return (dk0)super.delegate();
        }
        
        @Override
        public List<V> get(final K k) {
            synchronized (super.mutex) {
                return h(this.delegate().get((Object)k), super.mutex);
            }
        }
        
        @Override
        public List<V> removeAll(final Object o) {
            synchronized (super.mutex) {
                return this.delegate().removeAll(o);
            }
        }
        
        @Override
        public List<V> replaceValues(final K k, final Iterable<? extends V> iterable) {
            synchronized (super.mutex) {
                return this.delegate().replaceValues((Object)k, (Iterable)iterable);
            }
        }
    }
    
    public static class SynchronizedMultimap<K, V> extends SynchronizedObject implements rx0
    {
        private static final long serialVersionUID = 0L;
        transient Map<K, Collection<V>> asMap;
        transient Collection<Map.Entry<K, V>> entries;
        transient Set<K> keySet;
        transient j keys;
        transient Collection<V> valuesCollection;
        
        public SynchronizedMultimap(final rx0 rx0, final Object o) {
            super(rx0, o);
        }
        
        @Override
        public Map<K, Collection<V>> asMap() {
            synchronized (super.mutex) {
                if (this.asMap == null) {
                    this.asMap = (Map<K, Collection<V>>)new SynchronizedAsMap(this.delegate().asMap(), super.mutex);
                }
                return this.asMap;
            }
        }
        
        @Override
        public void clear() {
            synchronized (super.mutex) {
                this.delegate().clear();
            }
        }
        
        @Override
        public boolean containsEntry(final Object o, final Object o2) {
            synchronized (super.mutex) {
                return this.delegate().containsEntry(o, o2);
            }
        }
        
        @Override
        public boolean containsKey(final Object o) {
            synchronized (super.mutex) {
                return this.delegate().containsKey(o);
            }
        }
        
        @Override
        public boolean containsValue(final Object o) {
            synchronized (super.mutex) {
                return this.delegate().containsValue(o);
            }
        }
        
        public rx0 delegate() {
            return (rx0)super.delegate();
        }
        
        @Override
        public Collection<Map.Entry<K, V>> entries() {
            synchronized (super.mutex) {
                if (this.entries == null) {
                    this.entries = q(this.delegate().entries(), super.mutex);
                }
                return this.entries;
            }
        }
        
        @Override
        public boolean equals(final Object o) {
            if (o == this) {
                return true;
            }
            synchronized (super.mutex) {
                return this.delegate().equals(o);
            }
        }
        
        @Override
        public Collection<V> get(final K k) {
            synchronized (super.mutex) {
                return q(this.delegate().get(k), super.mutex);
            }
        }
        
        @Override
        public int hashCode() {
            synchronized (super.mutex) {
                return this.delegate().hashCode();
            }
        }
        
        @Override
        public boolean isEmpty() {
            synchronized (super.mutex) {
                return this.delegate().isEmpty();
            }
        }
        
        @Override
        public Set<K> keySet() {
            synchronized (super.mutex) {
                if (this.keySet == null) {
                    this.keySet = r(this.delegate().keySet(), super.mutex);
                }
                return this.keySet;
            }
        }
        
        @Override
        public j keys() {
            synchronized (super.mutex) {
                if (this.keys == null) {
                    this.keys = Synchronized.j(this.delegate().keys(), super.mutex);
                }
                return this.keys;
            }
        }
        
        @Override
        public boolean put(final K k, final V v) {
            synchronized (super.mutex) {
                return this.delegate().put(k, v);
            }
        }
        
        @Override
        public boolean putAll(final K k, final Iterable<? extends V> iterable) {
            synchronized (super.mutex) {
                return this.delegate().putAll(k, iterable);
            }
        }
        
        @Override
        public boolean putAll(final rx0 rx0) {
            synchronized (super.mutex) {
                return this.delegate().putAll(rx0);
            }
        }
        
        @Override
        public boolean remove(final Object o, final Object o2) {
            synchronized (super.mutex) {
                return this.delegate().remove(o, o2);
            }
        }
        
        @Override
        public Collection<V> removeAll(final Object o) {
            synchronized (super.mutex) {
                return this.delegate().removeAll(o);
            }
        }
        
        @Override
        public Collection<V> replaceValues(final K k, final Iterable<? extends V> iterable) {
            synchronized (super.mutex) {
                return this.delegate().replaceValues(k, iterable);
            }
        }
        
        @Override
        public int size() {
            synchronized (super.mutex) {
                return this.delegate().size();
            }
        }
        
        @Override
        public Collection<V> values() {
            synchronized (super.mutex) {
                if (this.valuesCollection == null) {
                    this.valuesCollection = g(this.delegate().values(), super.mutex);
                }
                return this.valuesCollection;
            }
        }
    }
    
    public static class SynchronizedMultiset<E> extends SynchronizedCollection<E> implements j
    {
        private static final long serialVersionUID = 0L;
        transient Set<E> elementSet;
        transient Set<a> entrySet;
        
        public SynchronizedMultiset(final j j, final Object o) {
            super(j, o, null);
        }
        
        @Override
        public int add(final E e, int add) {
            synchronized (super.mutex) {
                add = this.delegate().add(e, add);
                return add;
            }
        }
        
        @Override
        public int count(final Object o) {
            synchronized (super.mutex) {
                return this.delegate().count(o);
            }
        }
        
        public j delegate() {
            return (j)super.delegate();
        }
        
        @Override
        public Set<E> elementSet() {
            synchronized (super.mutex) {
                if (this.elementSet == null) {
                    this.elementSet = r(this.delegate().elementSet(), super.mutex);
                }
                return this.elementSet;
            }
        }
        
        @Override
        public Set<a> entrySet() {
            synchronized (super.mutex) {
                if (this.entrySet == null) {
                    this.entrySet = r(this.delegate().entrySet(), super.mutex);
                }
                return this.entrySet;
            }
        }
        
        @Override
        public boolean equals(final Object o) {
            if (o == this) {
                return true;
            }
            synchronized (super.mutex) {
                return this.delegate().equals(o);
            }
        }
        
        @Override
        public int hashCode() {
            synchronized (super.mutex) {
                return this.delegate().hashCode();
            }
        }
        
        @Override
        public int remove(final Object o, int remove) {
            synchronized (super.mutex) {
                remove = this.delegate().remove(o, remove);
                return remove;
            }
        }
        
        @Override
        public int setCount(final E e, int setCount) {
            synchronized (super.mutex) {
                setCount = this.delegate().setCount(e, setCount);
                return setCount;
            }
        }
        
        @Override
        public boolean setCount(final E e, final int n, final int n2) {
            synchronized (super.mutex) {
                return this.delegate().setCount(e, n, n2);
            }
        }
    }
    
    public static class SynchronizedNavigableMap<K, V> extends SynchronizedSortedMap<K, V> implements NavigableMap<K, V>
    {
        private static final long serialVersionUID = 0L;
        transient NavigableSet<K> descendingKeySet;
        transient NavigableMap<K, V> descendingMap;
        transient NavigableSet<K> navigableKeySet;
        
        public SynchronizedNavigableMap(final NavigableMap<K, V> navigableMap, final Object o) {
            super(navigableMap, o);
        }
        
        @Override
        public Entry<K, V> ceilingEntry(final K k) {
            synchronized (super.mutex) {
                return m((Entry)this.delegate().ceilingEntry(k), super.mutex);
            }
        }
        
        @Override
        public K ceilingKey(final K k) {
            synchronized (super.mutex) {
                return this.delegate().ceilingKey(k);
            }
        }
        
        public NavigableMap<K, V> delegate() {
            return (NavigableMap)super.delegate();
        }
        
        @Override
        public NavigableSet<K> descendingKeySet() {
            synchronized (super.mutex) {
                final NavigableSet<K> descendingKeySet = this.descendingKeySet;
                if (descendingKeySet == null) {
                    return this.descendingKeySet = Synchronized.l(this.delegate().descendingKeySet(), super.mutex);
                }
                return descendingKeySet;
            }
        }
        
        @Override
        public NavigableMap<K, V> descendingMap() {
            synchronized (super.mutex) {
                final NavigableMap<K, V> descendingMap = this.descendingMap;
                if (descendingMap == null) {
                    return this.descendingMap = Synchronized.k(this.delegate().descendingMap(), super.mutex);
                }
                return descendingMap;
            }
        }
        
        @Override
        public Entry<K, V> firstEntry() {
            synchronized (super.mutex) {
                return m((Entry)this.delegate().firstEntry(), super.mutex);
            }
        }
        
        @Override
        public Entry<K, V> floorEntry(final K k) {
            synchronized (super.mutex) {
                return m((Entry)this.delegate().floorEntry(k), super.mutex);
            }
        }
        
        @Override
        public K floorKey(final K k) {
            synchronized (super.mutex) {
                return this.delegate().floorKey(k);
            }
        }
        
        @Override
        public NavigableMap<K, V> headMap(final K k, final boolean b) {
            synchronized (super.mutex) {
                return Synchronized.k(this.delegate().headMap(k, b), super.mutex);
            }
        }
        
        @Override
        public SortedMap<K, V> headMap(final K k) {
            return this.headMap(k, false);
        }
        
        @Override
        public Entry<K, V> higherEntry(final K k) {
            synchronized (super.mutex) {
                return m((Entry)this.delegate().higherEntry(k), super.mutex);
            }
        }
        
        @Override
        public K higherKey(final K k) {
            synchronized (super.mutex) {
                return this.delegate().higherKey(k);
            }
        }
        
        @Override
        public Set<K> keySet() {
            return this.navigableKeySet();
        }
        
        @Override
        public Entry<K, V> lastEntry() {
            synchronized (super.mutex) {
                return m((Entry)this.delegate().lastEntry(), super.mutex);
            }
        }
        
        @Override
        public Entry<K, V> lowerEntry(final K k) {
            synchronized (super.mutex) {
                return m((Entry)this.delegate().lowerEntry(k), super.mutex);
            }
        }
        
        @Override
        public K lowerKey(final K k) {
            synchronized (super.mutex) {
                return this.delegate().lowerKey(k);
            }
        }
        
        @Override
        public NavigableSet<K> navigableKeySet() {
            synchronized (super.mutex) {
                final NavigableSet<K> navigableKeySet = this.navigableKeySet;
                if (navigableKeySet == null) {
                    return this.navigableKeySet = Synchronized.l(this.delegate().navigableKeySet(), super.mutex);
                }
                return navigableKeySet;
            }
        }
        
        @Override
        public Entry<K, V> pollFirstEntry() {
            synchronized (super.mutex) {
                return m((Entry)this.delegate().pollFirstEntry(), super.mutex);
            }
        }
        
        @Override
        public Entry<K, V> pollLastEntry() {
            synchronized (super.mutex) {
                return m((Entry)this.delegate().pollLastEntry(), super.mutex);
            }
        }
        
        @Override
        public NavigableMap<K, V> subMap(final K k, final boolean b, final K i, final boolean b2) {
            synchronized (super.mutex) {
                return Synchronized.k(this.delegate().subMap(k, b, i, b2), super.mutex);
            }
        }
        
        @Override
        public SortedMap<K, V> subMap(final K k, final K i) {
            return this.subMap(k, true, i, false);
        }
        
        @Override
        public NavigableMap<K, V> tailMap(final K k, final boolean b) {
            synchronized (super.mutex) {
                return Synchronized.k(this.delegate().tailMap(k, b), super.mutex);
            }
        }
        
        @Override
        public SortedMap<K, V> tailMap(final K k) {
            return this.tailMap(k, true);
        }
    }
    
    public static class SynchronizedSortedMap<K, V> extends SynchronizedMap<K, V> implements SortedMap<K, V>
    {
        private static final long serialVersionUID = 0L;
        
        public SynchronizedSortedMap(final SortedMap<K, V> sortedMap, final Object o) {
            super(sortedMap, o);
        }
        
        @Override
        public Comparator<? super K> comparator() {
            synchronized (super.mutex) {
                return this.delegate().comparator();
            }
        }
        
        public SortedMap<K, V> delegate() {
            return (SortedMap)super.delegate();
        }
        
        @Override
        public K firstKey() {
            synchronized (super.mutex) {
                return this.delegate().firstKey();
            }
        }
        
        @Override
        public SortedMap<K, V> headMap(final K k) {
            synchronized (super.mutex) {
                return Synchronized.o(this.delegate().headMap(k), super.mutex);
            }
        }
        
        @Override
        public K lastKey() {
            synchronized (super.mutex) {
                return this.delegate().lastKey();
            }
        }
        
        @Override
        public SortedMap<K, V> subMap(final K k, final K i) {
            synchronized (super.mutex) {
                return Synchronized.o(this.delegate().subMap(k, i), super.mutex);
            }
        }
        
        @Override
        public SortedMap<K, V> tailMap(final K k) {
            synchronized (super.mutex) {
                return Synchronized.o(this.delegate().tailMap(k), super.mutex);
            }
        }
    }
    
    public static class SynchronizedNavigableSet<E> extends SynchronizedSortedSet<E> implements NavigableSet<E>
    {
        private static final long serialVersionUID = 0L;
        transient NavigableSet<E> descendingSet;
        
        public SynchronizedNavigableSet(final NavigableSet<E> set, final Object o) {
            super(set, o);
        }
        
        @Override
        public E ceiling(final E e) {
            synchronized (super.mutex) {
                return this.delegate().ceiling(e);
            }
        }
        
        public NavigableSet<E> delegate() {
            return (NavigableSet)super.delegate();
        }
        
        @Override
        public Iterator<E> descendingIterator() {
            return this.delegate().descendingIterator();
        }
        
        @Override
        public NavigableSet<E> descendingSet() {
            synchronized (super.mutex) {
                final NavigableSet<E> descendingSet = this.descendingSet;
                if (descendingSet == null) {
                    return this.descendingSet = Synchronized.l(this.delegate().descendingSet(), super.mutex);
                }
                return descendingSet;
            }
        }
        
        @Override
        public E floor(final E e) {
            synchronized (super.mutex) {
                return this.delegate().floor(e);
            }
        }
        
        @Override
        public NavigableSet<E> headSet(final E e, final boolean b) {
            synchronized (super.mutex) {
                return Synchronized.l(this.delegate().headSet(e, b), super.mutex);
            }
        }
        
        @Override
        public SortedSet<E> headSet(final E e) {
            return this.headSet(e, false);
        }
        
        @Override
        public E higher(final E e) {
            synchronized (super.mutex) {
                return this.delegate().higher(e);
            }
        }
        
        @Override
        public E lower(final E e) {
            synchronized (super.mutex) {
                return this.delegate().lower(e);
            }
        }
        
        @Override
        public E pollFirst() {
            synchronized (super.mutex) {
                return this.delegate().pollFirst();
            }
        }
        
        @Override
        public E pollLast() {
            synchronized (super.mutex) {
                return this.delegate().pollLast();
            }
        }
        
        @Override
        public NavigableSet<E> subSet(final E e, final boolean b, final E e2, final boolean b2) {
            synchronized (super.mutex) {
                return Synchronized.l(this.delegate().subSet(e, b, e2, b2), super.mutex);
            }
        }
        
        @Override
        public SortedSet<E> subSet(final E e, final E e2) {
            return this.subSet(e, true, e2, false);
        }
        
        @Override
        public NavigableSet<E> tailSet(final E e, final boolean b) {
            synchronized (super.mutex) {
                return Synchronized.l(this.delegate().tailSet(e, b), super.mutex);
            }
        }
        
        @Override
        public SortedSet<E> tailSet(final E e) {
            return this.tailSet(e, true);
        }
    }
    
    public static class SynchronizedSortedSet<E> extends SynchronizedSet<E> implements SortedSet<E>
    {
        private static final long serialVersionUID = 0L;
        
        public SynchronizedSortedSet(final SortedSet<E> set, final Object o) {
            super(set, o);
        }
        
        @Override
        public Comparator<? super E> comparator() {
            synchronized (super.mutex) {
                return this.delegate().comparator();
            }
        }
        
        public SortedSet<E> delegate() {
            return (SortedSet)super.delegate();
        }
        
        @Override
        public E first() {
            synchronized (super.mutex) {
                return this.delegate().first();
            }
        }
        
        @Override
        public SortedSet<E> headSet(final E e) {
            synchronized (super.mutex) {
                return p(this.delegate().headSet(e), super.mutex);
            }
        }
        
        @Override
        public E last() {
            synchronized (super.mutex) {
                return this.delegate().last();
            }
        }
        
        @Override
        public SortedSet<E> subSet(final E e, final E e2) {
            synchronized (super.mutex) {
                return p(this.delegate().subSet(e, e2), super.mutex);
            }
        }
        
        @Override
        public SortedSet<E> tailSet(final E e) {
            synchronized (super.mutex) {
                return p(this.delegate().tailSet(e), super.mutex);
            }
        }
    }
    
    public static class SynchronizedObject implements Serializable
    {
        private static final long serialVersionUID = 0L;
        final Object delegate;
        final Object mutex;
        
        public SynchronizedObject(Object mutex, final Object o) {
            this.delegate = i71.r(mutex);
            mutex = o;
            if (o == null) {
                mutex = this;
            }
            this.mutex = mutex;
        }
        
        private void writeObject(final ObjectOutputStream objectOutputStream) {
            synchronized (this.mutex) {
                objectOutputStream.defaultWriteObject();
            }
        }
        
        Object delegate() {
            return this.delegate;
        }
        
        @Override
        public String toString() {
            synchronized (this.mutex) {
                return this.delegate.toString();
            }
        }
    }
    
    public static class SynchronizedQueue<E> extends SynchronizedCollection<E> implements Queue<E>
    {
        private static final long serialVersionUID = 0L;
        
        public SynchronizedQueue(final Queue<E> queue, final Object o) {
            super(queue, o, null);
        }
        
        public Queue<E> delegate() {
            return (Queue)super.delegate();
        }
        
        @Override
        public E element() {
            synchronized (super.mutex) {
                return this.delegate().element();
            }
        }
        
        @Override
        public boolean offer(final E e) {
            synchronized (super.mutex) {
                return this.delegate().offer(e);
            }
        }
        
        @Override
        public E peek() {
            synchronized (super.mutex) {
                return this.delegate().peek();
            }
        }
        
        @Override
        public E poll() {
            synchronized (super.mutex) {
                return this.delegate().poll();
            }
        }
        
        @Override
        public E remove() {
            synchronized (super.mutex) {
                return this.delegate().remove();
            }
        }
    }
    
    public static class SynchronizedRandomAccessList<E> extends SynchronizedList<E> implements RandomAccess
    {
        private static final long serialVersionUID = 0L;
        
        public SynchronizedRandomAccessList(final List<E> list, final Object o) {
            super(list, o);
        }
    }
    
    public static class SynchronizedSetMultimap<K, V> extends SynchronizedMultimap<K, V> implements pm1
    {
        private static final long serialVersionUID = 0L;
        transient Set<Map.Entry<K, V>> entrySet;
        
        public SynchronizedSetMultimap(final pm1 pm1, final Object o) {
            super(pm1, o);
        }
        
        public pm1 delegate() {
            return (pm1)super.delegate();
        }
        
        @Override
        public Set<Map.Entry<K, V>> entries() {
            synchronized (super.mutex) {
                if (this.entrySet == null) {
                    this.entrySet = Synchronized.n(this.delegate().entries(), super.mutex);
                }
                return this.entrySet;
            }
        }
        
        @Override
        public Set<V> get(final K k) {
            synchronized (super.mutex) {
                return Synchronized.n(this.delegate().get((Object)k), super.mutex);
            }
        }
        
        @Override
        public Set<V> removeAll(final Object o) {
            synchronized (super.mutex) {
                return this.delegate().removeAll(o);
            }
        }
        
        @Override
        public Set<V> replaceValues(final K k, final Iterable<? extends V> iterable) {
            synchronized (super.mutex) {
                return this.delegate().replaceValues((Object)k, (Iterable)iterable);
            }
        }
    }
    
    public static class SynchronizedSortedSetMultimap<K, V> extends SynchronizedSetMultimap<K, V> implements to1
    {
        private static final long serialVersionUID = 0L;
        
        public SynchronizedSortedSetMultimap(final to1 to1, final Object o) {
            super(to1, o);
        }
        
        public to1 delegate() {
            return (to1)super.delegate();
        }
        
        @Override
        public SortedSet<V> get(final K k) {
            synchronized (super.mutex) {
                return p(this.delegate().get((Object)k), super.mutex);
            }
        }
        
        @Override
        public SortedSet<V> removeAll(final Object o) {
            synchronized (super.mutex) {
                return this.delegate().removeAll(o);
            }
        }
        
        @Override
        public SortedSet<V> replaceValues(final K k, final Iterable<? extends V> iterable) {
            synchronized (super.mutex) {
                return this.delegate().replaceValues((Object)k, (Iterable)iterable);
            }
        }
        
        @Override
        public Comparator<? super V> valueComparator() {
            synchronized (super.mutex) {
                return this.delegate().valueComparator();
            }
        }
    }
    
    public static final class SynchronizedTable<R, C, V> extends SynchronizedObject implements q
    {
        public SynchronizedTable(final q q, final Object o) {
            super(q, o);
        }
        
        @Override
        public Set<a> cellSet() {
            synchronized (super.mutex) {
                return Synchronized.n(this.delegate().cellSet(), super.mutex);
            }
        }
        
        @Override
        public void clear() {
            synchronized (super.mutex) {
                this.delegate().clear();
            }
        }
        
        @Override
        public Map<R, V> column(final C c) {
            synchronized (super.mutex) {
                return Synchronized.i(this.delegate().column(c), super.mutex);
            }
        }
        
        @Override
        public Set<C> columnKeySet() {
            synchronized (super.mutex) {
                return Synchronized.n(this.delegate().columnKeySet(), super.mutex);
            }
        }
        
        @Override
        public Map<C, Map<R, V>> columnMap() {
            synchronized (super.mutex) {
                return Synchronized.i(Maps.C(this.delegate().columnMap(), new m90(this) {
                    public final SynchronizedTable a;
                    
                    public Map a(final Map map) {
                        return Synchronized.i(map, this.a.mutex);
                    }
                }), super.mutex);
            }
        }
        
        @Override
        public boolean contains(final Object o, final Object o2) {
            synchronized (super.mutex) {
                return this.delegate().contains(o, o2);
            }
        }
        
        @Override
        public boolean containsColumn(final Object o) {
            synchronized (super.mutex) {
                return this.delegate().containsColumn(o);
            }
        }
        
        @Override
        public boolean containsRow(final Object o) {
            synchronized (super.mutex) {
                return this.delegate().containsRow(o);
            }
        }
        
        @Override
        public boolean containsValue(final Object o) {
            synchronized (super.mutex) {
                return this.delegate().containsValue(o);
            }
        }
        
        public q delegate() {
            return (q)super.delegate();
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            synchronized (super.mutex) {
                return this.delegate().equals(o);
            }
        }
        
        @Override
        public V get(Object value, final Object o) {
            synchronized (super.mutex) {
                value = this.delegate().get(value, o);
                return (V)value;
            }
        }
        
        @Override
        public int hashCode() {
            synchronized (super.mutex) {
                return this.delegate().hashCode();
            }
        }
        
        @Override
        public boolean isEmpty() {
            synchronized (super.mutex) {
                return this.delegate().isEmpty();
            }
        }
        
        @Override
        public V put(final R r, final C c, final V v) {
            synchronized (super.mutex) {
                return (V)this.delegate().put(r, c, v);
            }
        }
        
        @Override
        public void putAll(final q q) {
            synchronized (super.mutex) {
                this.delegate().putAll(q);
            }
        }
        
        @Override
        public V remove(Object remove, final Object o) {
            synchronized (super.mutex) {
                remove = this.delegate().remove(remove, o);
                return (V)remove;
            }
        }
        
        @Override
        public Map<C, V> row(final R r) {
            synchronized (super.mutex) {
                return Synchronized.i(this.delegate().row(r), super.mutex);
            }
        }
        
        @Override
        public Set<R> rowKeySet() {
            synchronized (super.mutex) {
                return Synchronized.n(this.delegate().rowKeySet(), super.mutex);
            }
        }
        
        @Override
        public Map<R, Map<C, V>> rowMap() {
            synchronized (super.mutex) {
                return Synchronized.i(Maps.C(this.delegate().rowMap(), new m90(this) {
                    public final SynchronizedTable a;
                    
                    public Map a(final Map map) {
                        return Synchronized.i(map, this.a.mutex);
                    }
                }), super.mutex);
            }
        }
        
        @Override
        public int size() {
            synchronized (super.mutex) {
                return this.delegate().size();
            }
        }
        
        @Override
        public Collection<V> values() {
            synchronized (super.mutex) {
                return g(this.delegate().values(), super.mutex);
            }
        }
    }
}

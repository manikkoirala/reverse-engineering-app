// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.LinkedHashMap;
import java.util.Iterator;
import java.util.Collection;
import java.util.Set;
import java.util.HashMap;
import java.util.Map;
import java.io.Serializable;

public final class MutableClassToInstanceMap<B> extends x70 implements Serializable
{
    private final Map<Class<? extends B>, B> delegate;
    
    private MutableClassToInstanceMap(final Map<Class<? extends B>, B> map) {
        this.delegate = (Map)i71.r(map);
    }
    
    private static <B, T extends B> T cast(final Class<T> clazz, final B obj) {
        return e81.d(clazz).cast(obj);
    }
    
    public static <B> Entry<Class<? extends B>, B> checkedEntry(final Entry<Class<? extends B>, B> entry) {
        return new y70(entry) {
            public final Entry a;
            
            @Override
            public Entry b() {
                return this.a;
            }
            
            @Override
            public Object setValue(final Object o) {
                return super.setValue(cast((Class<Object>)this.getKey(), o));
            }
        };
    }
    
    public static <B> MutableClassToInstanceMap<B> create() {
        return new MutableClassToInstanceMap<B>(new HashMap<Class<? extends B>, B>());
    }
    
    public static <B> MutableClassToInstanceMap<B> create(final Map<Class<? extends B>, B> map) {
        return new MutableClassToInstanceMap<B>(map);
    }
    
    private Object writeReplace() {
        return new SerializedForm(this.delegate());
    }
    
    @Override
    public Map<Class<? extends B>, B> delegate() {
        return this.delegate;
    }
    
    @Override
    public Set<Entry<Class<? extends B>, B>> entrySet() {
        return new c80(this) {
            public final MutableClassToInstanceMap a;
            
            @Override
            public Set delegate() {
                return this.a.delegate().entrySet();
            }
            
            @Override
            public Iterator iterator() {
                return new r(this, this.delegate().iterator()) {
                    public Entry c(final Entry entry) {
                        return MutableClassToInstanceMap.checkedEntry((Entry<Class<?>, Object>)entry);
                    }
                };
            }
            
            @Override
            public Object[] toArray() {
                return this.standardToArray();
            }
            
            @Override
            public Object[] toArray(final Object[] array) {
                return this.standardToArray(array);
            }
        };
    }
    
    public <T extends B> T getInstance(final Class<T> clazz) {
        return cast(clazz, this.get(clazz));
    }
    
    public B put(final Class<? extends B> clazz, final B b) {
        return (B)super.put(clazz, cast((Class<Object>)clazz, b));
    }
    
    @Override
    public void putAll(final Map<? extends Class<? extends B>, ? extends B> m) {
        final LinkedHashMap linkedHashMap = new LinkedHashMap((Map<? extends K, ? extends V>)m);
        for (final Map.Entry<Class<T>, V> entry : linkedHashMap.entrySet()) {
            cast((Class<Object>)entry.getKey(), entry.getValue());
        }
        super.putAll(linkedHashMap);
    }
    
    public <T extends B> T putInstance(final Class<T> clazz, final T t) {
        return cast(clazz, (Object)this.put((Class<? extends B>)clazz, (B)t));
    }
    
    public static final class SerializedForm<B> implements Serializable
    {
        private static final long serialVersionUID = 0L;
        private final Map<Class<? extends B>, B> backingMap;
        
        public SerializedForm(final Map<Class<? extends B>, B> backingMap) {
            this.backingMap = backingMap;
        }
        
        public Object readResolve() {
            return MutableClassToInstanceMap.create(this.backingMap);
        }
    }
}

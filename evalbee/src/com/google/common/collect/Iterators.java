// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.PriorityQueue;
import java.util.Queue;
import java.util.ArrayDeque;
import java.util.Deque;
import com.google.common.primitives.Ints;
import java.util.NoSuchElementException;
import java.util.Comparator;
import java.util.ListIterator;
import java.util.Iterator;
import java.util.Collection;

public abstract class Iterators
{
    public static boolean a(final Collection collection, final Iterator iterator) {
        i71.r(collection);
        i71.r(iterator);
        boolean b = false;
        while (iterator.hasNext()) {
            b |= collection.add(iterator.next());
        }
        return b;
    }
    
    public static int b(final Iterator iterator, final int n) {
        i71.r(iterator);
        int n2 = 0;
        i71.e(n >= 0, "numberToAdvance must be nonnegative");
        while (n2 < n && iterator.hasNext()) {
            iterator.next();
            ++n2;
        }
        return n2;
    }
    
    public static ListIterator c(final Iterator iterator) {
        return (ListIterator)iterator;
    }
    
    public static void d(final Iterator iterator) {
        i71.r(iterator);
        while (iterator.hasNext()) {
            iterator.next();
            iterator.remove();
        }
    }
    
    public static Iterator e(final Iterator iterator) {
        return new f(iterator);
    }
    
    public static boolean f(final Iterator iterator, final Object o) {
        if (o == null) {
            while (iterator.hasNext()) {
                if (iterator.next() == null) {
                    return true;
                }
            }
        }
        else {
            while (iterator.hasNext()) {
                if (o.equals(iterator.next())) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public static boolean g(final Iterator iterator, final Iterator iterator2) {
        while (iterator.hasNext()) {
            if (!iterator2.hasNext()) {
                return false;
            }
            if (!b11.a(iterator.next(), iterator2.next())) {
                return false;
            }
        }
        return iterator2.hasNext() ^ true;
    }
    
    public static w02 h() {
        return i();
    }
    
    public static z02 i() {
        return e.e;
    }
    
    public static Iterator j() {
        return EmptyModifiableIterator.INSTANCE;
    }
    
    public static w02 k(final Iterator iterator, final m71 m71) {
        i71.r(iterator);
        i71.r(m71);
        return new AbstractIterator(iterator, m71) {
            public final Iterator c;
            public final m71 d;
            
            @Override
            public Object b() {
                while (this.c.hasNext()) {
                    final Object next = this.c.next();
                    if (this.d.apply(next)) {
                        return next;
                    }
                }
                return this.c();
            }
        };
    }
    
    public static Object l(final Iterator iterator) {
        Object next;
        do {
            next = iterator.next();
        } while (iterator.hasNext());
        return next;
    }
    
    public static Object m(final Iterator iterator, Object next) {
        if (iterator.hasNext()) {
            next = iterator.next();
        }
        return next;
    }
    
    public static Object n(final Iterator iterator) {
        final Object next = iterator.next();
        if (!iterator.hasNext()) {
            return next;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("expected one element but was: <");
        sb.append(next);
        for (int n = 0; n < 4 && iterator.hasNext(); ++n) {
            sb.append(", ");
            sb.append(iterator.next());
        }
        if (iterator.hasNext()) {
            sb.append(", ...");
        }
        sb.append('>');
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static w02 o(final Iterable iterable, final Comparator comparator) {
        i71.s(iterable, "iterators");
        i71.s(comparator, "comparator");
        return new g(iterable, comparator);
    }
    
    public static b51 p(final Iterator iterator) {
        if (iterator instanceof h) {
            return (h)iterator;
        }
        return new h(iterator);
    }
    
    public static Object q(final Iterator iterator) {
        if (iterator.hasNext()) {
            final Object next = iterator.next();
            iterator.remove();
            return next;
        }
        return null;
    }
    
    public static boolean r(final Iterator iterator, final Collection collection) {
        i71.r(collection);
        boolean b = false;
        while (iterator.hasNext()) {
            if (collection.contains(iterator.next())) {
                iterator.remove();
                b = true;
            }
        }
        return b;
    }
    
    public static boolean s(final Iterator iterator, final Collection collection) {
        i71.r(collection);
        boolean b = false;
        while (iterator.hasNext()) {
            if (!collection.contains(iterator.next())) {
                iterator.remove();
                b = true;
            }
        }
        return b;
    }
    
    public static w02 t(final Object o) {
        return new w02(o) {
            public boolean a;
            public final Object b;
            
            @Override
            public boolean hasNext() {
                return this.a ^ true;
            }
            
            @Override
            public Object next() {
                if (!this.a) {
                    this.a = true;
                    return this.b;
                }
                throw new NoSuchElementException();
            }
        };
    }
    
    public static int u(final Iterator iterator) {
        long n = 0L;
        while (iterator.hasNext()) {
            iterator.next();
            ++n;
        }
        return Ints.k(n);
    }
    
    public static String v(final Iterator iterator) {
        final StringBuilder sb = new StringBuilder();
        sb.append('[');
        int n = 1;
        while (iterator.hasNext()) {
            if (n == 0) {
                sb.append(", ");
            }
            sb.append(iterator.next());
            n = 0;
        }
        sb.append(']');
        return sb.toString();
    }
    
    public static Iterator w(final Iterator iterator, final m90 m90) {
        i71.r(m90);
        return new r(iterator, m90) {
            public final m90 b;
            
            public Object b(final Object o) {
                return this.b.apply(o);
            }
        };
    }
    
    public static w02 x(final Iterator iterator) {
        i71.r(iterator);
        if (iterator instanceof w02) {
            return (w02)iterator;
        }
        return new w02(iterator) {
            public final Iterator a;
            
            @Override
            public boolean hasNext() {
                return this.a.hasNext();
            }
            
            @Override
            public Object next() {
                return this.a.next();
            }
        };
    }
    
    public enum EmptyModifiableIterator implements Iterator<Object>
    {
        private static final EmptyModifiableIterator[] $VALUES;
        
        INSTANCE;
        
        private static /* synthetic */ EmptyModifiableIterator[] $values() {
            return new EmptyModifiableIterator[] { EmptyModifiableIterator.INSTANCE };
        }
        
        static {
            $VALUES = $values();
        }
        
        @Override
        public boolean hasNext() {
            return false;
        }
        
        @Override
        public Object next() {
            throw new NoSuchElementException();
        }
        
        @Override
        public void remove() {
            hh.e(false);
        }
    }
    
    public static final class e extends o
    {
        public static final z02 e;
        public final Object[] c;
        public final int d;
        
        static {
            e = new e(new Object[0], 0, 0, 0);
        }
        
        public e(final Object[] c, final int d, final int n, final int n2) {
            super(n, n2);
            this.c = c;
            this.d = d;
        }
        
        @Override
        public Object b(final int n) {
            return this.c[this.d + n];
        }
    }
    
    public static class f implements Iterator
    {
        public Iterator a;
        public Iterator b;
        public Iterator c;
        public Deque d;
        
        public f(final Iterator iterator) {
            this.b = Iterators.h();
            this.c = (Iterator)i71.r(iterator);
        }
        
        public final Iterator b() {
            while (true) {
                final Iterator c = this.c;
                if (c != null && c.hasNext()) {
                    return this.c;
                }
                final Deque d = this.d;
                if (d == null || d.isEmpty()) {
                    return null;
                }
                this.c = this.d.removeFirst();
            }
        }
        
        @Override
        public boolean hasNext() {
            while (!((Iterator)i71.r(this.b)).hasNext()) {
                final Iterator b = this.b();
                if ((this.c = b) == null) {
                    return false;
                }
                final Iterator b2 = b.next();
                this.b = b2;
                if (!(b2 instanceof f)) {
                    continue;
                }
                final f f = (f)b2;
                this.b = f.b;
                if (this.d == null) {
                    this.d = new ArrayDeque();
                }
                this.d.addFirst(this.c);
                if (f.d != null) {
                    while (!f.d.isEmpty()) {
                        this.d.addFirst(f.d.removeLast());
                    }
                }
                this.c = f.c;
            }
            return true;
        }
        
        @Override
        public Object next() {
            if (this.hasNext()) {
                final Iterator b = this.b;
                this.a = b;
                return b.next();
            }
            throw new NoSuchElementException();
        }
        
        @Override
        public void remove() {
            final Iterator a = this.a;
            if (a != null) {
                a.remove();
                this.a = null;
                return;
            }
            throw new IllegalStateException("no calls to next() since the last call to remove()");
        }
    }
    
    public static class g extends w02
    {
        public final Queue a;
        
        public g(final Iterable iterable, final Comparator comparator) {
            this.a = new PriorityQueue(2, new sg0(comparator));
            for (final Iterator iterator2 : iterable) {
                if (iterator2.hasNext()) {
                    this.a.add(Iterators.p(iterator2));
                }
            }
        }
        
        @Override
        public boolean hasNext() {
            return this.a.isEmpty() ^ true;
        }
        
        @Override
        public Object next() {
            final b51 b51 = this.a.remove();
            final Object next = b51.next();
            if (b51.hasNext()) {
                this.a.add(b51);
            }
            return next;
        }
    }
    
    public static class h implements b51
    {
        public final Iterator a;
        public boolean b;
        public Object c;
        
        public h(final Iterator iterator) {
            this.a = (Iterator)i71.r(iterator);
        }
        
        @Override
        public boolean hasNext() {
            return this.b || this.a.hasNext();
        }
        
        @Override
        public Object next() {
            if (!this.b) {
                return this.a.next();
            }
            final Object a = k01.a(this.c);
            this.b = false;
            this.c = null;
            return a;
        }
        
        @Override
        public Object peek() {
            if (!this.b) {
                this.c = this.a.next();
                this.b = true;
            }
            return k01.a(this.c);
        }
        
        @Override
        public void remove() {
            i71.y(this.b ^ true, "Can't remove after you've peeked at next");
            this.a.remove();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import com.google.common.primitives.Ints;
import java.math.BigInteger;
import java.io.Serializable;
import java.util.NoSuchElementException;

public abstract class DiscreteDomain
{
    final boolean supportsFastOffset;
    
    public DiscreteDomain() {
        this(false);
    }
    
    public DiscreteDomain(final boolean supportsFastOffset) {
        this.supportsFastOffset = supportsFastOffset;
    }
    
    public static DiscreteDomain bigIntegers() {
        return BigIntegerDomain.access$300();
    }
    
    public static DiscreteDomain integers() {
        return IntegerDomain.access$000();
    }
    
    public static DiscreteDomain longs() {
        return LongDomain.access$200();
    }
    
    public abstract long distance(final Comparable p0, final Comparable p1);
    
    public Comparable maxValue() {
        throw new NoSuchElementException();
    }
    
    public Comparable minValue() {
        throw new NoSuchElementException();
    }
    
    public abstract Comparable next(final Comparable p0);
    
    public abstract Comparable offset(final Comparable p0, final long p1);
    
    public abstract Comparable previous(final Comparable p0);
    
    public static final class BigIntegerDomain extends DiscreteDomain implements Serializable
    {
        private static final BigIntegerDomain INSTANCE;
        private static final BigInteger MAX_LONG;
        private static final BigInteger MIN_LONG;
        private static final long serialVersionUID = 0L;
        
        static {
            INSTANCE = new BigIntegerDomain();
            MIN_LONG = BigInteger.valueOf(Long.MIN_VALUE);
            MAX_LONG = BigInteger.valueOf(Long.MAX_VALUE);
        }
        
        public BigIntegerDomain() {
            super(true, null);
        }
        
        public static /* synthetic */ BigIntegerDomain access$300() {
            return BigIntegerDomain.INSTANCE;
        }
        
        private Object readResolve() {
            return BigIntegerDomain.INSTANCE;
        }
        
        public long distance(final BigInteger val, final BigInteger bigInteger) {
            return bigInteger.subtract(val).max(BigIntegerDomain.MIN_LONG).min(BigIntegerDomain.MAX_LONG).longValue();
        }
        
        public BigInteger next(final BigInteger bigInteger) {
            return bigInteger.add(BigInteger.ONE);
        }
        
        public BigInteger offset(final BigInteger bigInteger, final long val) {
            hh.c(val, "distance");
            return bigInteger.add(BigInteger.valueOf(val));
        }
        
        public BigInteger previous(final BigInteger bigInteger) {
            return bigInteger.subtract(BigInteger.ONE);
        }
        
        @Override
        public String toString() {
            return "DiscreteDomain.bigIntegers()";
        }
    }
    
    public static final class IntegerDomain extends DiscreteDomain implements Serializable
    {
        private static final IntegerDomain INSTANCE;
        private static final long serialVersionUID = 0L;
        
        static {
            INSTANCE = new IntegerDomain();
        }
        
        public IntegerDomain() {
            super(true, null);
        }
        
        public static /* synthetic */ IntegerDomain access$000() {
            return IntegerDomain.INSTANCE;
        }
        
        private Object readResolve() {
            return IntegerDomain.INSTANCE;
        }
        
        public long distance(final Integer n, final Integer n2) {
            return n2 - (long)n;
        }
        
        @Override
        public Integer maxValue() {
            return Integer.MAX_VALUE;
        }
        
        @Override
        public Integer minValue() {
            return Integer.MIN_VALUE;
        }
        
        public Integer next(Integer value) {
            final int intValue = value;
            if (intValue == Integer.MAX_VALUE) {
                value = null;
            }
            else {
                value = intValue + 1;
            }
            return value;
        }
        
        public Integer offset(final Integer n, final long n2) {
            hh.c(n2, "distance");
            return Ints.c(n + n2);
        }
        
        public Integer previous(Integer value) {
            final int intValue = value;
            if (intValue == Integer.MIN_VALUE) {
                value = null;
            }
            else {
                value = intValue - 1;
            }
            return value;
        }
        
        @Override
        public String toString() {
            return "DiscreteDomain.integers()";
        }
    }
    
    public static final class LongDomain extends DiscreteDomain implements Serializable
    {
        private static final LongDomain INSTANCE;
        private static final long serialVersionUID = 0L;
        
        static {
            INSTANCE = new LongDomain();
        }
        
        public LongDomain() {
            super(true, null);
        }
        
        public static /* synthetic */ LongDomain access$200() {
            return LongDomain.INSTANCE;
        }
        
        private Object readResolve() {
            return LongDomain.INSTANCE;
        }
        
        public long distance(final Long n, final Long n2) {
            final long n3 = n2 - n;
            if (n2 > n && n3 < 0L) {
                return Long.MAX_VALUE;
            }
            if (n2 < n && n3 > 0L) {
                return Long.MIN_VALUE;
            }
            return n3;
        }
        
        @Override
        public Long maxValue() {
            return Long.MAX_VALUE;
        }
        
        @Override
        public Long minValue() {
            return Long.MIN_VALUE;
        }
        
        public Long next(Long value) {
            final long longValue = value;
            if (longValue == Long.MAX_VALUE) {
                value = null;
            }
            else {
                value = longValue + 1L;
            }
            return value;
        }
        
        public Long offset(final Long n, long l) {
            hh.c(l, "distance");
            l += n;
            if (l < 0L) {
                i71.e(n < 0L, "overflow");
            }
            return l;
        }
        
        public Long previous(Long value) {
            final long longValue = value;
            if (longValue == Long.MIN_VALUE) {
                value = null;
            }
            else {
                value = longValue - 1L;
            }
            return value;
        }
        
        @Override
        public String toString() {
            return "DiscreteDomain.longs()";
        }
    }
}

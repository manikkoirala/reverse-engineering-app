// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.io.Serializable;
import java.util.Collections;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.util.SortedSet;
import java.util.Iterator;
import java.util.Collection;
import java.util.Arrays;
import java.util.Comparator;
import java.util.NavigableSet;

public abstract class ImmutableSortedSet<E> extends ImmutableSortedSetFauxverideShim<E> implements NavigableSet<E>, ro1
{
    final transient Comparator<? super E> comparator;
    transient ImmutableSortedSet<E> descendingSet;
    
    public ImmutableSortedSet(final Comparator<? super E> comparator) {
        this.comparator = comparator;
    }
    
    public static <E> ImmutableSortedSet<E> construct(final Comparator<? super E> c, final int n, final E... original) {
        if (n == 0) {
            return (ImmutableSortedSet<E>)emptySet((Comparator<? super Object>)c);
        }
        t01.c(original, n);
        Arrays.sort(original, 0, n, c);
        int i = 1;
        int n2 = 1;
        while (i < n) {
            final E e = original[i];
            int n3 = n2;
            if (c.compare(e, original[n2 - 1]) != 0) {
                original[n2] = e;
                n3 = n2 + 1;
            }
            ++i;
            n2 = n3;
        }
        Arrays.fill(original, n2, n, null);
        E[] copy = original;
        if (n2 < original.length / 2) {
            copy = Arrays.copyOf(original, n2);
        }
        return new RegularImmutableSortedSet<E>(ImmutableList.asImmutableList(copy, n2), (Comparator<? super Object>)c);
    }
    
    public static <E> ImmutableSortedSet<E> copyOf(final Iterable<? extends E> iterable) {
        return copyOf((Comparator<? super E>)Ordering.natural(), iterable);
    }
    
    public static <E> ImmutableSortedSet<E> copyOf(final Collection<? extends E> collection) {
        return copyOf((Comparator<? super E>)Ordering.natural(), collection);
    }
    
    public static <E> ImmutableSortedSet<E> copyOf(final Comparator<? super E> comparator, final Iterable<? extends E> iterable) {
        i71.r(comparator);
        if (so1.b(comparator, iterable) && iterable instanceof ImmutableSortedSet) {
            final ImmutableSortedSet set = (ImmutableSortedSet)iterable;
            if (!set.isPartialView()) {
                return set;
            }
        }
        final Object[] k = rg0.k(iterable);
        return construct(comparator, ((E[])k).length, (E[])k);
    }
    
    public static <E> ImmutableSortedSet<E> copyOf(final Comparator<? super E> comparator, final Collection<? extends E> collection) {
        return copyOf(comparator, (Iterable<? extends E>)collection);
    }
    
    public static <E> ImmutableSortedSet<E> copyOf(final Comparator<? super E> comparator, final Iterator<? extends E> iterator) {
        return new a(comparator).p(iterator).q();
    }
    
    public static <E> ImmutableSortedSet<E> copyOf(final Iterator<? extends E> iterator) {
        return copyOf((Comparator<? super E>)Ordering.natural(), iterator);
    }
    
    public static <E extends Comparable<? super E>> ImmutableSortedSet<E> copyOf(final E[] array) {
        return construct(Ordering.natural(), array.length, (E[])array.clone());
    }
    
    public static <E> ImmutableSortedSet<E> copyOfSorted(final SortedSet<E> set) {
        final Comparator a = so1.a(set);
        final ImmutableList<Object> copy = ImmutableList.copyOf((Collection<?>)set);
        if (copy.isEmpty()) {
            return (ImmutableSortedSet<E>)emptySet(a);
        }
        return new RegularImmutableSortedSet<E>(copy, a);
    }
    
    public static <E> RegularImmutableSortedSet<E> emptySet(final Comparator<? super E> obj) {
        if (Ordering.natural().equals(obj)) {
            return (RegularImmutableSortedSet<E>)RegularImmutableSortedSet.NATURAL_EMPTY_SET;
        }
        return new RegularImmutableSortedSet<E>(ImmutableList.of(), obj);
    }
    
    public static <E extends Comparable<?>> a naturalOrder() {
        return new a(Ordering.natural());
    }
    
    public static <E> ImmutableSortedSet<E> of() {
        return (ImmutableSortedSet<E>)RegularImmutableSortedSet.NATURAL_EMPTY_SET;
    }
    
    public static <E extends Comparable<? super E>> ImmutableSortedSet<E> of(final E e) {
        return new RegularImmutableSortedSet<E>(ImmutableList.of(e), Ordering.natural());
    }
    
    public static <E extends Comparable<? super E>> ImmutableSortedSet<E> of(final E e, final E e2) {
        return construct(Ordering.natural(), 2, e, e2);
    }
    
    public static <E extends Comparable<? super E>> ImmutableSortedSet<E> of(final E e, final E e2, final E e3) {
        return construct(Ordering.natural(), 3, e, e2, e3);
    }
    
    public static <E extends Comparable<? super E>> ImmutableSortedSet<E> of(final E e, final E e2, final E e3, final E e4) {
        return construct(Ordering.natural(), 4, e, e2, e3, e4);
    }
    
    public static <E extends Comparable<? super E>> ImmutableSortedSet<E> of(final E e, final E e2, final E e3, final E e4, final E e5) {
        return construct(Ordering.natural(), 5, e, e2, e3, e4, e5);
    }
    
    public static <E extends Comparable<? super E>> ImmutableSortedSet<E> of(final E e, final E e2, final E e3, final E e4, final E e5, final E e6, final E... array) {
        final int n = array.length + 6;
        final Comparable[] array2 = new Comparable[n];
        array2[0] = e;
        array2[1] = e2;
        array2[2] = e3;
        array2[3] = e4;
        array2[4] = e5;
        array2[5] = e6;
        System.arraycopy(array, 0, array2, 6, array.length);
        return construct(Ordering.natural(), n, (E[])array2);
    }
    
    public static <E> a orderedBy(final Comparator<E> comparator) {
        return new a(comparator);
    }
    
    private void readObject(final ObjectInputStream objectInputStream) {
        throw new InvalidObjectException("Use SerializedForm");
    }
    
    public static <E extends Comparable<?>> a reverseOrder() {
        return new a(Collections.reverseOrder());
    }
    
    public static int unsafeCompare(final Comparator<?> comparator, final Object o, final Object o2) {
        return comparator.compare(o, o2);
    }
    
    @Override
    public E ceiling(final E e) {
        return (E)rg0.e(this.tailSet(e, true), null);
    }
    
    @Override
    public Comparator<? super E> comparator() {
        return this.comparator;
    }
    
    public abstract ImmutableSortedSet<E> createDescendingSet();
    
    @Override
    public abstract w02 descendingIterator();
    
    @Override
    public ImmutableSortedSet<E> descendingSet() {
        ImmutableSortedSet<E> descendingSet;
        if ((descendingSet = this.descendingSet) == null) {
            descendingSet = this.createDescendingSet();
            this.descendingSet = descendingSet;
            descendingSet.descendingSet = this;
        }
        return descendingSet;
    }
    
    @Override
    public E first() {
        return this.iterator().next();
    }
    
    @Override
    public E floor(final E e) {
        return (E)Iterators.m(this.headSet(e, true).descendingIterator(), null);
    }
    
    @Override
    public ImmutableSortedSet<E> headSet(final E e) {
        return this.headSet(e, false);
    }
    
    @Override
    public ImmutableSortedSet<E> headSet(final E e, final boolean b) {
        return (ImmutableSortedSet<E>)this.headSetImpl(i71.r(e), b);
    }
    
    public abstract ImmutableSortedSet<E> headSetImpl(final E p0, final boolean p1);
    
    @Override
    public E higher(final E e) {
        return (E)rg0.e(this.tailSet(e, false), null);
    }
    
    public abstract int indexOf(final Object p0);
    
    @Override
    public abstract w02 iterator();
    
    @Override
    public E last() {
        return this.descendingIterator().next();
    }
    
    @Override
    public E lower(final E e) {
        return (E)Iterators.m(this.headSet(e, false).descendingIterator(), null);
    }
    
    @Deprecated
    @Override
    public final E pollFirst() {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public final E pollLast() {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public ImmutableSortedSet<E> subSet(final E e, final E e2) {
        return this.subSet(e, true, e2, false);
    }
    
    @Override
    public ImmutableSortedSet<E> subSet(final E e, final boolean b, final E e2, final boolean b2) {
        i71.r(e);
        i71.r(e2);
        i71.d(this.comparator.compare((Object)e, (Object)e2) <= 0);
        return this.subSetImpl(e, b, e2, b2);
    }
    
    public abstract ImmutableSortedSet<E> subSetImpl(final E p0, final boolean p1, final E p2, final boolean p3);
    
    @Override
    public ImmutableSortedSet<E> tailSet(final E e) {
        return this.tailSet(e, true);
    }
    
    @Override
    public ImmutableSortedSet<E> tailSet(final E e, final boolean b) {
        return (ImmutableSortedSet<E>)this.tailSetImpl(i71.r(e), b);
    }
    
    public abstract ImmutableSortedSet<E> tailSetImpl(final E p0, final boolean p1);
    
    public int unsafeCompare(final Object o, final Object o2) {
        return unsafeCompare(this.comparator, o, o2);
    }
    
    public Object writeReplace() {
        return new SerializedForm((Comparator<? super Object>)this.comparator, this.toArray());
    }
    
    public static class SerializedForm<E> implements Serializable
    {
        private static final long serialVersionUID = 0L;
        final Comparator<? super E> comparator;
        final Object[] elements;
        
        public SerializedForm(final Comparator<? super E> comparator, final Object[] elements) {
            this.comparator = comparator;
            this.elements = elements;
        }
        
        public Object readResolve() {
            return new a(this.comparator).o(this.elements).q();
        }
    }
    
    public static final class a extends ImmutableSet.a
    {
        public final Comparator f;
        
        public a(final Comparator comparator) {
            this.f = (Comparator)i71.r(comparator);
        }
        
        public a n(final Object o) {
            super.i(o);
            return this;
        }
        
        public a o(final Object... array) {
            super.j(array);
            return this;
        }
        
        public a p(final Iterator iterator) {
            super.k(iterator);
            return this;
        }
        
        public ImmutableSortedSet q() {
            final ImmutableSortedSet<Object> construct = ImmutableSortedSet.construct(this.f, super.b, super.a);
            super.b = construct.size();
            super.c = true;
            return construct;
        }
    }
}

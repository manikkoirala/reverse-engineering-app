// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Iterator;
import java.io.Serializable;
import java.util.Map;

abstract class ImmutableMapEntrySet<K, V> extends ImmutableSet<Map.Entry<K, V>>
{
    public ImmutableMapEntrySet() {
    }
    
    @Override
    public boolean contains(final Object o) {
        final boolean b = o instanceof Map.Entry;
        boolean b3;
        final boolean b2 = b3 = false;
        if (b) {
            final Map.Entry entry = (Map.Entry)o;
            final V value = this.map().get(entry.getKey());
            b3 = b2;
            if (value != null) {
                b3 = b2;
                if (value.equals(entry.getValue())) {
                    b3 = true;
                }
            }
        }
        return b3;
    }
    
    @Override
    public int hashCode() {
        return this.map().hashCode();
    }
    
    @Override
    public boolean isHashCodeFast() {
        return this.map().isHashCodeFast();
    }
    
    @Override
    public boolean isPartialView() {
        return this.map().isPartialView();
    }
    
    public abstract ImmutableMap<K, V> map();
    
    @Override
    public int size() {
        return this.map().size();
    }
    
    public Object writeReplace() {
        return new EntrySetSerializedForm(this.map());
    }
    
    public static class EntrySetSerializedForm<K, V> implements Serializable
    {
        private static final long serialVersionUID = 0L;
        final ImmutableMap<K, V> map;
        
        public EntrySetSerializedForm(final ImmutableMap<K, V> map) {
            this.map = map;
        }
        
        public Object readResolve() {
            return this.map.entrySet();
        }
    }
    
    public static final class RegularEntrySet<K, V> extends ImmutableMapEntrySet<K, V>
    {
        private final transient ImmutableList<Map.Entry<K, V>> entries;
        private final transient ImmutableMap<K, V> map;
        
        public RegularEntrySet(final ImmutableMap<K, V> map, final ImmutableList<Map.Entry<K, V>> entries) {
            this.map = map;
            this.entries = entries;
        }
        
        public RegularEntrySet(final ImmutableMap<K, V> immutableMap, final Map.Entry<K, V>[] array) {
            this(immutableMap, ImmutableList.asImmutableList(array));
        }
        
        @Override
        public int copyIntoArray(final Object[] array, final int n) {
            return this.entries.copyIntoArray(array, n);
        }
        
        @Override
        public ImmutableList<Map.Entry<K, V>> createAsList() {
            return this.entries;
        }
        
        @Override
        public w02 iterator() {
            return this.entries.iterator();
        }
        
        @Override
        public ImmutableMap<K, V> map() {
            return this.map;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Iterator;

abstract class IndexedImmutableSet<E> extends ImmutableSet<E>
{
    public IndexedImmutableSet() {
    }
    
    @Override
    public int copyIntoArray(final Object[] array, final int n) {
        return this.asList().copyIntoArray(array, n);
    }
    
    @Override
    public ImmutableList<E> createAsList() {
        return new ImmutableList<E>(this) {
            final IndexedImmutableSet this$0;
            
            @Override
            public E get(final int n) {
                return this.this$0.get(n);
            }
            
            @Override
            public boolean isPartialView() {
                return this.this$0.isPartialView();
            }
            
            @Override
            public int size() {
                return this.this$0.size();
            }
        };
    }
    
    public abstract E get(final int p0);
    
    @Override
    public w02 iterator() {
        return this.asList().iterator();
    }
}

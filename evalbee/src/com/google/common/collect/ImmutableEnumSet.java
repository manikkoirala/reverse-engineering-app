// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Iterator;
import java.io.Serializable;
import java.util.Collection;
import java.util.EnumSet;

final class ImmutableEnumSet<E extends Enum<E>> extends ImmutableSet<E>
{
    private final transient EnumSet<E> delegate;
    private transient int hashCode;
    
    private ImmutableEnumSet(final EnumSet<E> delegate) {
        this.delegate = delegate;
    }
    
    public static ImmutableSet asImmutable(final EnumSet set) {
        final int size = set.size();
        if (size == 0) {
            return ImmutableSet.of();
        }
        if (size != 1) {
            return new ImmutableEnumSet(set);
        }
        return ImmutableSet.of(rg0.h(set));
    }
    
    @Override
    public boolean contains(final Object o) {
        return this.delegate.contains(o);
    }
    
    @Override
    public boolean containsAll(final Collection<?> collection) {
        Serializable delegate = (Serializable)collection;
        if (collection instanceof ImmutableEnumSet) {
            delegate = ((ImmutableEnumSet)collection).delegate;
        }
        return this.delegate.containsAll((Collection<?>)delegate);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        Object delegate = o;
        if (o instanceof ImmutableEnumSet) {
            delegate = ((ImmutableEnumSet)o).delegate;
        }
        return this.delegate.equals(delegate);
    }
    
    @Override
    public int hashCode() {
        int hashCode;
        if ((hashCode = this.hashCode) == 0) {
            hashCode = this.delegate.hashCode();
            this.hashCode = hashCode;
        }
        return hashCode;
    }
    
    @Override
    public boolean isEmpty() {
        return this.delegate.isEmpty();
    }
    
    @Override
    public boolean isHashCodeFast() {
        return true;
    }
    
    @Override
    public boolean isPartialView() {
        return false;
    }
    
    @Override
    public w02 iterator() {
        return Iterators.x(this.delegate.iterator());
    }
    
    @Override
    public int size() {
        return this.delegate.size();
    }
    
    @Override
    public String toString() {
        return this.delegate.toString();
    }
    
    public Object writeReplace() {
        return new EnumSerializedForm((EnumSet<Enum>)this.delegate);
    }
    
    public static class EnumSerializedForm<E extends Enum<E>> implements Serializable
    {
        private static final long serialVersionUID = 0L;
        final EnumSet<E> delegate;
        
        public EnumSerializedForm(final EnumSet<E> delegate) {
            this.delegate = delegate;
        }
        
        public Object readResolve() {
            return new ImmutableEnumSet(this.delegate.clone(), null);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.io.Serializable;
import java.util.Set;
import java.util.Objects;
import java.util.Iterator;
import java.util.Collection;

final class RegularContiguousSet<C extends Comparable> extends ContiguousSet<C>
{
    private static final long serialVersionUID = 0L;
    private final Range<C> range;
    
    public RegularContiguousSet(final Range<C> range, final DiscreteDomain discreteDomain) {
        super(discreteDomain);
        this.range = range;
    }
    
    private static boolean equalsOrThrow(final Comparable<?> comparable, final Comparable<?> comparable2) {
        return comparable2 != null && Range.compareOrThrow(comparable, comparable2) == 0;
    }
    
    private ContiguousSet<C> intersectionInCurrentDomain(final Range<C> range) {
        ContiguousSet<C> create;
        if (this.range.isConnected(range)) {
            create = ContiguousSet.create(this.range.intersection(range), super.domain);
        }
        else {
            create = new EmptyContiguousSet<C>(super.domain);
        }
        return create;
    }
    
    @Override
    public boolean contains(final Object o) {
        if (o == null) {
            return false;
        }
        try {
            return this.range.contains((C)o);
        }
        catch (final ClassCastException ex) {
            return false;
        }
    }
    
    @Override
    public boolean containsAll(final Collection<?> collection) {
        return lh.a(this, collection);
    }
    
    @Override
    public ImmutableList<C> createAsList() {
        if (super.domain.supportsFastOffset) {
            return new ImmutableAsList<C>(this) {
                final RegularContiguousSet this$0;
                
                @Override
                public ImmutableSortedSet<C> delegateCollection() {
                    return this.this$0;
                }
                
                @Override
                public C get(final int n) {
                    i71.p(n, this.size());
                    final RegularContiguousSet this$0 = this.this$0;
                    return (C)this$0.domain.offset(this$0.first(), n);
                }
            };
        }
        return super.createAsList();
    }
    
    @Override
    public w02 descendingIterator() {
        return new a0(this, this.last()) {
            public final Comparable b = c.first();
            public final RegularContiguousSet c;
            
            public Comparable c(Comparable previous) {
                if (equalsOrThrow(previous, this.b)) {
                    previous = null;
                }
                else {
                    previous = this.c.domain.previous(previous);
                }
                return previous;
            }
        };
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof RegularContiguousSet) {
            final RegularContiguousSet set = (RegularContiguousSet)o;
            if (super.domain.equals(set.domain)) {
                if (!this.first().equals(set.first()) || !this.last().equals(set.last())) {
                    b = false;
                }
                return b;
            }
        }
        return super.equals(o);
    }
    
    @Override
    public C first() {
        final Comparable leastValueAbove = this.range.lowerBound.leastValueAbove(super.domain);
        Objects.requireNonNull(leastValueAbove);
        return (C)leastValueAbove;
    }
    
    @Override
    public int hashCode() {
        return Sets.b(this);
    }
    
    @Override
    public ContiguousSet<C> headSetImpl(final C c, final boolean b) {
        return this.intersectionInCurrentDomain(Range.upTo(c, BoundType.forBoolean(b)));
    }
    
    @Override
    public int indexOf(final Object obj) {
        int n;
        if (this.contains(obj)) {
            final DiscreteDomain domain = super.domain;
            final Comparable first = this.first();
            Objects.requireNonNull(obj);
            n = (int)domain.distance(first, (Comparable)obj);
        }
        else {
            n = -1;
        }
        return n;
    }
    
    @Override
    public ContiguousSet<C> intersection(final ContiguousSet<C> set) {
        i71.r(set);
        i71.d(super.domain.equals(set.domain));
        if (set.isEmpty()) {
            return set;
        }
        final Comparable comparable = Ordering.natural().max(this.first(), (Comparable)set.first());
        final Comparable comparable2 = Ordering.natural().min(this.last(), (Comparable)set.last());
        ContiguousSet<Comparable> create;
        if (comparable.compareTo(comparable2) <= 0) {
            create = ContiguousSet.create((Range<Comparable>)Range.closed(comparable, comparable2), super.domain);
        }
        else {
            create = new EmptyContiguousSet<Comparable>(super.domain);
        }
        return (ContiguousSet<C>)create;
    }
    
    @Override
    public boolean isEmpty() {
        return false;
    }
    
    @Override
    public boolean isPartialView() {
        return false;
    }
    
    @Override
    public w02 iterator() {
        return new a0(this, this.first()) {
            public final Comparable b = c.last();
            public final RegularContiguousSet c;
            
            public Comparable c(Comparable next) {
                if (equalsOrThrow(next, this.b)) {
                    next = null;
                }
                else {
                    next = this.c.domain.next(next);
                }
                return next;
            }
        };
    }
    
    @Override
    public C last() {
        final Comparable greatestValueBelow = this.range.upperBound.greatestValueBelow(super.domain);
        Objects.requireNonNull(greatestValueBelow);
        return (C)greatestValueBelow;
    }
    
    @Override
    public Range<C> range() {
        final BoundType closed = BoundType.CLOSED;
        return this.range(closed, closed);
    }
    
    @Override
    public Range<C> range(final BoundType boundType, final BoundType boundType2) {
        return Range.create(this.range.lowerBound.withLowerBoundType(boundType, super.domain), this.range.upperBound.withUpperBoundType(boundType2, super.domain));
    }
    
    @Override
    public int size() {
        final long distance = super.domain.distance(this.first(), this.last());
        int n;
        if (distance >= 2147483647L) {
            n = Integer.MAX_VALUE;
        }
        else {
            n = (int)distance + 1;
        }
        return n;
    }
    
    @Override
    public ContiguousSet<C> subSetImpl(final C c, final boolean b, final C c2, final boolean b2) {
        if (c.compareTo(c2) == 0 && !b && !b2) {
            return new EmptyContiguousSet<C>(super.domain);
        }
        return this.intersectionInCurrentDomain(Range.range(c, BoundType.forBoolean(b), c2, BoundType.forBoolean(b2)));
    }
    
    @Override
    public ContiguousSet<C> tailSetImpl(final C c, final boolean b) {
        return this.intersectionInCurrentDomain(Range.downTo(c, BoundType.forBoolean(b)));
    }
    
    @Override
    public Object writeReplace() {
        return new SerializedForm(this.range, super.domain, null);
    }
    
    public static final class SerializedForm<C extends Comparable> implements Serializable
    {
        final DiscreteDomain domain;
        final Range<C> range;
        
        private SerializedForm(final Range<C> range, final DiscreteDomain domain) {
            this.range = range;
            this.domain = domain;
        }
        
        private Object readResolve() {
            return new RegularContiguousSet((Range<Comparable>)this.range, this.domain);
        }
    }
}

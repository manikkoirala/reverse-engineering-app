// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

@Deprecated
public class ComputationException extends RuntimeException
{
    private static final long serialVersionUID = 0L;
    
    public ComputationException(final Throwable cause) {
        super(cause);
    }
}

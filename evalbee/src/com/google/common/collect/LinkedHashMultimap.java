// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.ConcurrentModificationException;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.Iterator;
import java.io.ObjectOutputStream;
import java.util.Map;
import java.util.Objects;
import java.util.Collection;
import java.io.ObjectInputStream;

public final class LinkedHashMultimap<K, V> extends LinkedHashMultimapGwtSerializationDependencies<K, V>
{
    private static final int DEFAULT_KEY_CAPACITY = 16;
    private static final int DEFAULT_VALUE_SET_CAPACITY = 2;
    static final double VALUE_SET_LOAD_FACTOR = 1.0;
    private static final long serialVersionUID = 1L;
    private transient ValueEntry<K, V> multimapHeaderEntry;
    transient int valueSetCapacity;
    
    private LinkedHashMultimap(final int n, final int valueSetCapacity) {
        super(com.google.common.collect.l.e(n));
        this.valueSetCapacity = 2;
        hh.b(valueSetCapacity, "expectedValuesPerKey");
        this.valueSetCapacity = valueSetCapacity;
        final ValueEntry<Object, Object> header = (ValueEntry<Object, Object>)ValueEntry.newHeader();
        succeedsInMultimap(this.multimapHeaderEntry = (ValueEntry<K, V>)header, (ValueEntry<K, V>)header);
    }
    
    public static /* synthetic */ ValueEntry access$300(final LinkedHashMultimap linkedHashMultimap) {
        return linkedHashMultimap.multimapHeaderEntry;
    }
    
    public static <K, V> LinkedHashMultimap<K, V> create() {
        return new LinkedHashMultimap<K, V>(16, 2);
    }
    
    public static <K, V> LinkedHashMultimap<K, V> create(final int n, final int n2) {
        return new LinkedHashMultimap<K, V>(Maps.e(n), Maps.e(n2));
    }
    
    public static <K, V> LinkedHashMultimap<K, V> create(final rx0 rx0) {
        final LinkedHashMultimap<Object, Object> create = create(rx0.keySet().size(), 2);
        create.putAll(rx0);
        return (LinkedHashMultimap<K, V>)create;
    }
    
    private static <K, V> void deleteFromMultimap(final ValueEntry<K, V> valueEntry) {
        succeedsInMultimap(valueEntry.getPredecessorInMultimap(), valueEntry.getSuccessorInMultimap());
    }
    
    private static <K, V> void deleteFromValueSet(final c c) {
        succeedsInValueSet(c.getPredecessorInValueSet(), c.getSuccessorInValueSet());
    }
    
    private void readObject(final ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        final ValueEntry<K, V> header = ValueEntry.newHeader();
        succeedsInMultimap(this.multimapHeaderEntry = header, header);
        this.valueSetCapacity = 2;
        final int int1 = objectInputStream.readInt();
        final Map e = com.google.common.collect.l.e(12);
        final int n = 0;
        for (int i = 0; i < int1; ++i) {
            final Object object = objectInputStream.readObject();
            e.put(object, this.createCollection((K)object));
        }
        for (int int2 = objectInputStream.readInt(), j = n; j < int2; ++j) {
            final Object object2 = objectInputStream.readObject();
            final Object object3 = objectInputStream.readObject();
            final Collection obj = e.get(object2);
            Objects.requireNonNull(obj);
            obj.add(object3);
        }
        this.setMap(e);
    }
    
    private static <K, V> void succeedsInMultimap(final ValueEntry<K, V> predecessorInMultimap, final ValueEntry<K, V> successorInMultimap) {
        predecessorInMultimap.setSuccessorInMultimap(successorInMultimap);
        successorInMultimap.setPredecessorInMultimap(predecessorInMultimap);
    }
    
    private static <K, V> void succeedsInValueSet(final c predecessorInValueSet, final c successorInValueSet) {
        predecessorInValueSet.setSuccessorInValueSet(successorInValueSet);
        successorInValueSet.setPredecessorInValueSet(predecessorInValueSet);
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeInt(this.keySet().size());
        final Iterator<K> iterator = this.keySet().iterator();
        while (iterator.hasNext()) {
            objectOutputStream.writeObject(iterator.next());
        }
        objectOutputStream.writeInt(this.size());
        for (final Map.Entry<Object, V> entry : this.entries()) {
            objectOutputStream.writeObject(entry.getKey());
            objectOutputStream.writeObject(entry.getValue());
        }
    }
    
    @Override
    public void clear() {
        super.clear();
        final ValueEntry<K, V> multimapHeaderEntry = this.multimapHeaderEntry;
        succeedsInMultimap((ValueEntry<Object, Object>)multimapHeaderEntry, (ValueEntry<Object, Object>)multimapHeaderEntry);
    }
    
    @Override
    public Collection<V> createCollection(final K k) {
        return new b(k, this.valueSetCapacity);
    }
    
    public Set<V> createCollection() {
        return com.google.common.collect.l.f(this.valueSetCapacity);
    }
    
    @Override
    public Set<Map.Entry<K, V>> entries() {
        return super.entries();
    }
    
    public Iterator<Map.Entry<K, V>> entryIterator() {
        return new Iterator(this) {
            public ValueEntry a = LinkedHashMultimap.access$300(c).getSuccessorInMultimap();
            public ValueEntry b;
            public final LinkedHashMultimap c;
            
            public Map.Entry b() {
                if (this.hasNext()) {
                    final ValueEntry a = this.a;
                    this.b = a;
                    this.a = (ValueEntry)a.getSuccessorInMultimap();
                    return a;
                }
                throw new NoSuchElementException();
            }
            
            @Override
            public boolean hasNext() {
                return this.a != LinkedHashMultimap.access$300(this.c);
            }
            
            @Override
            public void remove() {
                i71.y(this.b != null, "no calls to next() since the last call to remove()");
                this.c.remove(this.b.getKey(), this.b.getValue());
                this.b = null;
            }
        };
    }
    
    @Override
    public Set<K> keySet() {
        return (Set<K>)super.keySet();
    }
    
    @Override
    public Set<V> replaceValues(final K k, final Iterable<? extends V> iterable) {
        return super.replaceValues(k, iterable);
    }
    
    public Iterator<V> valueIterator() {
        return Maps.K(this.entryIterator());
    }
    
    @Override
    public Collection<V> values() {
        return super.values();
    }
    
    public static final class ValueEntry<K, V> extends ImmutableEntry<K, V> implements c
    {
        ValueEntry<K, V> nextInValueBucket;
        ValueEntry<K, V> predecessorInMultimap;
        c predecessorInValueSet;
        final int smearedValueHash;
        ValueEntry<K, V> successorInMultimap;
        c successorInValueSet;
        
        public ValueEntry(final K k, final V v, final int smearedValueHash, final ValueEntry<K, V> nextInValueBucket) {
            super(k, v);
            this.smearedValueHash = smearedValueHash;
            this.nextInValueBucket = nextInValueBucket;
        }
        
        public static <K, V> ValueEntry<K, V> newHeader() {
            return new ValueEntry<K, V>(null, null, 0, null);
        }
        
        public ValueEntry<K, V> getPredecessorInMultimap() {
            final ValueEntry<K, V> predecessorInMultimap = this.predecessorInMultimap;
            Objects.requireNonNull(predecessorInMultimap);
            return predecessorInMultimap;
        }
        
        @Override
        public c getPredecessorInValueSet() {
            final c predecessorInValueSet = this.predecessorInValueSet;
            Objects.requireNonNull(predecessorInValueSet);
            return predecessorInValueSet;
        }
        
        public ValueEntry<K, V> getSuccessorInMultimap() {
            final ValueEntry<K, V> successorInMultimap = this.successorInMultimap;
            Objects.requireNonNull(successorInMultimap);
            return successorInMultimap;
        }
        
        @Override
        public c getSuccessorInValueSet() {
            final c successorInValueSet = this.successorInValueSet;
            Objects.requireNonNull(successorInValueSet);
            return successorInValueSet;
        }
        
        public boolean matchesValue(final Object o, final int n) {
            return this.smearedValueHash == n && b11.a(this.getValue(), o);
        }
        
        public void setPredecessorInMultimap(final ValueEntry<K, V> predecessorInMultimap) {
            this.predecessorInMultimap = predecessorInMultimap;
        }
        
        @Override
        public void setPredecessorInValueSet(final c predecessorInValueSet) {
            this.predecessorInValueSet = predecessorInValueSet;
        }
        
        public void setSuccessorInMultimap(final ValueEntry<K, V> successorInMultimap) {
            this.successorInMultimap = successorInMultimap;
        }
        
        @Override
        public void setSuccessorInValueSet(final c successorInValueSet) {
            this.successorInValueSet = successorInValueSet;
        }
    }
    
    public final class b extends Sets.a implements c
    {
        public final Object a;
        public ValueEntry[] b;
        public int c;
        public int d;
        public c e;
        public c f;
        public final LinkedHashMultimap g;
        
        public b(final LinkedHashMultimap g, final Object a, final int n) {
            this.g = g;
            this.c = 0;
            this.d = 0;
            this.a = a;
            this.e = this;
            this.f = this;
            this.b = new ValueEntry[rc0.a(n, 1.0)];
        }
        
        public static /* synthetic */ c a(final b b) {
            return b.e;
        }
        
        public static /* synthetic */ int b(final b b) {
            return b.d;
        }
        
        @Override
        public boolean add(Object o) {
            final int d = rc0.d(o);
            final int n = this.c() & d;
            ValueEntry<K, V> nextInValueBucket;
            ValueEntry valueEntry;
            for (valueEntry = (ValueEntry)(nextInValueBucket = this.b[n]); nextInValueBucket != null; nextInValueBucket = nextInValueBucket.nextInValueBucket) {
                if (nextInValueBucket.matchesValue(o, d)) {
                    return false;
                }
            }
            o = new ValueEntry(this.a, o, d, valueEntry);
            succeedsInValueSet(this.f, (c)o);
            succeedsInValueSet((c)o, (c)this);
            succeedsInMultimap(LinkedHashMultimap.access$300(this.g).getPredecessorInMultimap(), (ValueEntry<Object, Object>)o);
            succeedsInMultimap((ValueEntry)o, (ValueEntry<Object, Object>)LinkedHashMultimap.access$300(this.g));
            this.b[n] = (ValueEntry)o;
            ++this.c;
            ++this.d;
            this.g();
            return true;
        }
        
        public final int c() {
            return this.b.length - 1;
        }
        
        @Override
        public void clear() {
            Arrays.fill(this.b, null);
            this.c = 0;
            for (Object o = this.e; o != this; o = ((c)o).getSuccessorInValueSet()) {
                deleteFromMultimap((ValueEntry<Object, Object>)o);
            }
            succeedsInValueSet((c)this, (c)this);
            ++this.d;
        }
        
        @Override
        public boolean contains(final Object o) {
            final int d = rc0.d(o);
            for (ValueEntry<K, V> nextInValueBucket = this.b[this.c() & d]; nextInValueBucket != null; nextInValueBucket = nextInValueBucket.nextInValueBucket) {
                if (nextInValueBucket.matchesValue(o, d)) {
                    return true;
                }
            }
            return false;
        }
        
        public final void g() {
            if (rc0.b(this.c, this.b.length, 1.0)) {
                final int n = this.b.length * 2;
                final ValueEntry[] b = new ValueEntry[n];
                this.b = b;
                for (Object o = this.e; o != this; o = ((c)o).getSuccessorInValueSet()) {
                    final ValueEntry valueEntry = (ValueEntry)o;
                    final int n2 = valueEntry.smearedValueHash & n - 1;
                    valueEntry.nextInValueBucket = b[n2];
                    b[n2] = valueEntry;
                }
            }
        }
        
        @Override
        public c getPredecessorInValueSet() {
            return this.f;
        }
        
        @Override
        public c getSuccessorInValueSet() {
            return this.e;
        }
        
        @Override
        public Iterator iterator() {
            return new Iterator(this) {
                public c a = LinkedHashMultimap.b.a(d);
                public ValueEntry b;
                public int c = LinkedHashMultimap.b.b(d);
                public final b d;
                
                public final void b() {
                    if (LinkedHashMultimap.b.b(this.d) == this.c) {
                        return;
                    }
                    throw new ConcurrentModificationException();
                }
                
                @Override
                public boolean hasNext() {
                    this.b();
                    return this.a != this.d;
                }
                
                @Override
                public Object next() {
                    if (this.hasNext()) {
                        final ValueEntry b = (ValueEntry)this.a;
                        final Object value = b.getValue();
                        this.b = b;
                        this.a = b.getSuccessorInValueSet();
                        return value;
                    }
                    throw new NoSuchElementException();
                }
                
                @Override
                public void remove() {
                    this.b();
                    i71.y(this.b != null, "no calls to next() since the last call to remove()");
                    this.d.remove(this.b.getValue());
                    this.c = LinkedHashMultimap.b.b(this.d);
                    this.b = null;
                }
            };
        }
        
        @Override
        public boolean remove(final Object o) {
            final int d = rc0.d(o);
            final int n = this.c() & d;
            ValueEntry valueEntry = this.b[n];
            ValueEntry valueEntry2 = null;
            while (valueEntry != null) {
                if (valueEntry.matchesValue(o, d)) {
                    if (valueEntry2 == null) {
                        this.b[n] = (ValueEntry)valueEntry.nextInValueBucket;
                    }
                    else {
                        valueEntry2.nextInValueBucket = valueEntry.nextInValueBucket;
                    }
                    deleteFromValueSet((c)valueEntry);
                    deleteFromMultimap((ValueEntry<Object, Object>)valueEntry);
                    --this.c;
                    ++this.d;
                    return true;
                }
                final ValueEntry<K, V> nextInValueBucket = valueEntry.nextInValueBucket;
                valueEntry2 = valueEntry;
                valueEntry = (ValueEntry)nextInValueBucket;
            }
            return false;
        }
        
        @Override
        public void setPredecessorInValueSet(final c f) {
            this.f = f;
        }
        
        @Override
        public void setSuccessorInValueSet(final c e) {
            this.e = e;
        }
        
        @Override
        public int size() {
            return this.c;
        }
    }
    
    public interface c
    {
        c getPredecessorInValueSet();
        
        c getSuccessorInValueSet();
        
        void setPredecessorInValueSet(final c p0);
        
        void setSuccessorInValueSet(final c p0);
    }
}

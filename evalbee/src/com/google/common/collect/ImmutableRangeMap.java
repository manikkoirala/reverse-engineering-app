// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Collections;
import java.util.NoSuchElementException;
import java.util.List;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import java.io.Serializable;

public class ImmutableRangeMap<K extends Comparable<?>, V> implements fc1, Serializable
{
    private static final ImmutableRangeMap<Comparable<?>, Object> EMPTY;
    private static final long serialVersionUID = 0L;
    private final transient ImmutableList<Range<K>> ranges;
    private final transient ImmutableList<V> values;
    
    static {
        EMPTY = new ImmutableRangeMap<Comparable<?>, Object>(ImmutableList.of(), ImmutableList.of());
    }
    
    public ImmutableRangeMap(final ImmutableList<Range<K>> ranges, final ImmutableList<V> values) {
        this.ranges = ranges;
        this.values = values;
    }
    
    public static /* synthetic */ ImmutableList access$000(final ImmutableRangeMap immutableRangeMap) {
        return immutableRangeMap.ranges;
    }
    
    public static <K extends Comparable<?>, V> a builder() {
        return new a();
    }
    
    public static <K extends Comparable<?>, V> ImmutableRangeMap<K, V> copyOf(final fc1 fc1) {
        if (fc1 instanceof ImmutableRangeMap) {
            return (ImmutableRangeMap)fc1;
        }
        final Map mapOfRanges = fc1.asMapOfRanges();
        final ImmutableList.a a = new ImmutableList.a(mapOfRanges.size());
        final ImmutableList.a a2 = new ImmutableList.a(mapOfRanges.size());
        for (final Map.Entry<Range<?>, V> entry : mapOfRanges.entrySet()) {
            a.i(entry.getKey());
            a2.i(entry.getValue());
        }
        return new ImmutableRangeMap<K, V>(a.l(), a2.l());
    }
    
    public static <K extends Comparable<?>, V> ImmutableRangeMap<K, V> of() {
        return (ImmutableRangeMap<K, V>)ImmutableRangeMap.EMPTY;
    }
    
    public static <K extends Comparable<?>, V> ImmutableRangeMap<K, V> of(final Range<K> range, final V v) {
        return new ImmutableRangeMap<K, V>((ImmutableList<Range<K>>)ImmutableList.of((Range<K>)range), ImmutableList.of(v));
    }
    
    public ImmutableMap<Range<K>, V> asDescendingMapOfRanges() {
        if (this.ranges.isEmpty()) {
            return ImmutableMap.of();
        }
        return new ImmutableSortedMap<Range<K>, V>(new RegularImmutableSortedSet<Range<K>>(this.ranges.reverse(), Range.rangeLexOrdering().reverse()), this.values.reverse());
    }
    
    @Override
    public ImmutableMap<Range<K>, V> asMapOfRanges() {
        if (this.ranges.isEmpty()) {
            return ImmutableMap.of();
        }
        return new ImmutableSortedMap<Range<K>, V>(new RegularImmutableSortedSet<Range<K>>(this.ranges, Range.rangeLexOrdering()), this.values);
    }
    
    @Deprecated
    public final void clear() {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof fc1 && this.asMapOfRanges().equals(((fc1)o).asMapOfRanges());
    }
    
    public V get(final K k) {
        final int a = SortedLists.a(this.ranges, Range.lowerBoundFn(), Cut.belowValue(k), SortedLists.KeyPresentBehavior.ANY_PRESENT, SortedLists.KeyAbsentBehavior.NEXT_LOWER);
        Object value = null;
        if (a == -1) {
            return null;
        }
        if (((Range<K>)this.ranges.get(a)).contains(k)) {
            value = this.values.get(a);
        }
        return (V)value;
    }
    
    public Map.Entry<Range<K>, V> getEntry(final K k) {
        final int a = SortedLists.a(this.ranges, Range.lowerBoundFn(), Cut.belowValue(k), SortedLists.KeyPresentBehavior.ANY_PRESENT, SortedLists.KeyAbsentBehavior.NEXT_LOWER);
        Map.Entry<Range<K>, V> j = null;
        if (a == -1) {
            return null;
        }
        final Range range = this.ranges.get(a);
        if (range.contains(k)) {
            j = Maps.j(range, this.values.get(a));
        }
        return j;
    }
    
    @Override
    public int hashCode() {
        return this.asMapOfRanges().hashCode();
    }
    
    @Deprecated
    public final void put(final Range<K> range, final V v) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    public final void putAll(final fc1 fc1) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    public final void putCoalescing(final Range<K> range, final V v) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    public final void remove(final Range<K> range) {
        throw new UnsupportedOperationException();
    }
    
    public Range<K> span() {
        if (!this.ranges.isEmpty()) {
            final Range range = this.ranges.get(0);
            final ImmutableList<Range<K>> ranges = this.ranges;
            return Range.create((Cut<K>)range.lowerBound, (Cut<K>)((Range)ranges.get(ranges.size() - 1)).upperBound);
        }
        throw new NoSuchElementException();
    }
    
    public ImmutableRangeMap<K, V> subRangeMap(final Range<K> range) {
        if (((Range)i71.r(range)).isEmpty()) {
            return of();
        }
        if (this.ranges.isEmpty() || range.encloses(this.span())) {
            return this;
        }
        final ImmutableList<Range<K>> ranges = this.ranges;
        final m90 upperBoundFn = Range.upperBoundFn();
        final Cut<K> lowerBound = range.lowerBound;
        final SortedLists.KeyPresentBehavior first_AFTER = SortedLists.KeyPresentBehavior.FIRST_AFTER;
        final SortedLists.KeyAbsentBehavior next_HIGHER = SortedLists.KeyAbsentBehavior.NEXT_HIGHER;
        final int a = SortedLists.a(ranges, upperBoundFn, lowerBound, first_AFTER, next_HIGHER);
        final int a2 = SortedLists.a(this.ranges, Range.lowerBoundFn(), range.upperBound, SortedLists.KeyPresentBehavior.ANY_PRESENT, next_HIGHER);
        if (a >= a2) {
            return of();
        }
        return new ImmutableRangeMap<K, V>(this, new ImmutableList<Range<K>>(this, a2 - a, a, range) {
            final ImmutableRangeMap this$0;
            final int val$len;
            final int val$off;
            final Range val$range;
            
            @Override
            public Range<K> get(final int n) {
                i71.p(n, this.val$len);
                if (n != 0 && n != this.val$len - 1) {
                    return (Range)ImmutableRangeMap.access$000(this.this$0).get(n + this.val$off);
                }
                return ((Range)ImmutableRangeMap.access$000(this.this$0).get(n + this.val$off)).intersection(this.val$range);
            }
            
            @Override
            public boolean isPartialView() {
                return true;
            }
            
            @Override
            public int size() {
                return this.val$len;
            }
        }, this.values.subList(a, a2), range, this) {
            final ImmutableRangeMap val$outer;
            final Range val$range;
            
            @Override
            public ImmutableRangeMap<K, V> subRangeMap(final Range<K> range) {
                if (this.val$range.isConnected(range)) {
                    return this.val$outer.subRangeMap(range.intersection(this.val$range));
                }
                return ImmutableRangeMap.of();
            }
        };
    }
    
    @Override
    public String toString() {
        return this.asMapOfRanges().toString();
    }
    
    public Object writeReplace() {
        return new SerializedForm(this.asMapOfRanges());
    }
    
    public static class SerializedForm<K extends Comparable<?>, V> implements Serializable
    {
        private static final long serialVersionUID = 0L;
        private final ImmutableMap<Range<K>, V> mapOfRanges;
        
        public SerializedForm(final ImmutableMap<Range<K>, V> mapOfRanges) {
            this.mapOfRanges = mapOfRanges;
        }
        
        public Object createRangeMap() {
            final a a = new a();
            for (final Map.Entry<Range, V> entry : this.mapOfRanges.entrySet()) {
                a.b(entry.getKey(), entry.getValue());
            }
            return a.a();
        }
        
        public Object readResolve() {
            if (this.mapOfRanges.isEmpty()) {
                return ImmutableRangeMap.of();
            }
            return this.createRangeMap();
        }
    }
    
    public static final class a
    {
        public final List a;
        
        public a() {
            this.a = Lists.i();
        }
        
        public ImmutableRangeMap a() {
            Collections.sort((List<Object>)this.a, Range.rangeLexOrdering().onKeys());
            final ImmutableList.a a = new ImmutableList.a(this.a.size());
            final ImmutableList.a a2 = new ImmutableList.a(this.a.size());
            for (int i = 0; i < this.a.size(); ++i) {
                final Range obj = this.a.get(i).getKey();
                if (i > 0) {
                    final Range obj2 = this.a.get(i - 1).getKey();
                    if (obj.isConnected(obj2)) {
                        if (!obj.intersection(obj2).isEmpty()) {
                            final String value = String.valueOf(obj2);
                            final String value2 = String.valueOf(obj);
                            final StringBuilder sb = new StringBuilder(value.length() + 47 + value2.length());
                            sb.append("Overlapping ranges: range ");
                            sb.append(value);
                            sb.append(" overlaps with entry ");
                            sb.append(value2);
                            throw new IllegalArgumentException(sb.toString());
                        }
                    }
                }
                a.i(obj);
                a2.i(((Map.Entry<K, Object>)this.a.get(i)).getValue());
            }
            return new ImmutableRangeMap(a.l(), a2.l());
        }
        
        public a b(final Range range, final Object o) {
            i71.r(range);
            i71.r(o);
            i71.m(range.isEmpty() ^ true, "Range must not be empty, but was %s", range);
            this.a.add(Maps.j(range, o));
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.io.Serializable;
import java.util.SortedSet;
import java.util.Set;
import java.util.NavigableSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Collection;
import java.util.Comparator;

public abstract class ImmutableSortedMultiset<E> extends ImmutableSortedMultisetFauxverideShim<E> implements o
{
    transient ImmutableSortedMultiset<E> descendingMultiset;
    
    public static <E> ImmutableSortedMultiset<E> copyOf(final Iterable<? extends E> iterable) {
        return copyOf((Comparator<? super E>)Ordering.natural(), iterable);
    }
    
    public static <E> ImmutableSortedMultiset<E> copyOf(final Comparator<? super E> comparator, final Iterable<? extends E> iterable) {
        if (iterable instanceof ImmutableSortedMultiset) {
            final ImmutableSortedMultiset immutableSortedMultiset = (ImmutableSortedMultiset)iterable;
            if (comparator.equals(immutableSortedMultiset.comparator())) {
                if (immutableSortedMultiset.isPartialView()) {
                    return (ImmutableSortedMultiset<E>)copyOfSortedEntries((Comparator<? super Object>)comparator, immutableSortedMultiset.entrySet().asList());
                }
                return immutableSortedMultiset;
            }
        }
        return new a(comparator).o(iterable).r();
    }
    
    public static <E> ImmutableSortedMultiset<E> copyOf(final Comparator<? super E> comparator, final Iterator<? extends E> iterator) {
        i71.r(comparator);
        return new a(comparator).p(iterator).r();
    }
    
    public static <E> ImmutableSortedMultiset<E> copyOf(final Iterator<? extends E> iterator) {
        return copyOf((Comparator<? super E>)Ordering.natural(), iterator);
    }
    
    public static <E extends Comparable<? super E>> ImmutableSortedMultiset<E> copyOf(final E[] a) {
        return copyOf((Comparator<? super E>)Ordering.natural(), (Iterable<? extends E>)Arrays.asList(a));
    }
    
    public static <E> ImmutableSortedMultiset<E> copyOfSorted(final o o) {
        return copyOfSortedEntries((Comparator<? super E>)o.comparator(), (Collection<j.a>)Lists.j(o.entrySet()));
    }
    
    private static <E> ImmutableSortedMultiset<E> copyOfSortedEntries(final Comparator<? super E> comparator, final Collection<j.a> collection) {
        if (collection.isEmpty()) {
            return (ImmutableSortedMultiset<E>)emptyMultiset((Comparator<? super Object>)comparator);
        }
        final ImmutableList.a a = new ImmutableList.a(collection.size());
        final long[] array = new long[collection.size() + 1];
        final Iterator iterator = collection.iterator();
        int n = 0;
        while (iterator.hasNext()) {
            final j.a a2 = (j.a)iterator.next();
            a.i(a2.getElement());
            final int n2 = n + 1;
            array[n2] = array[n] + a2.getCount();
            n = n2;
        }
        return new RegularImmutableSortedMultiset<E>(new RegularImmutableSortedSet<Object>(a.l(), (Comparator<? super Object>)comparator), array, 0, collection.size());
    }
    
    public static <E> ImmutableSortedMultiset<E> emptyMultiset(final Comparator<? super E> obj) {
        if (Ordering.natural().equals(obj)) {
            return (ImmutableSortedMultiset<E>)RegularImmutableSortedMultiset.NATURAL_EMPTY_MULTISET;
        }
        return new RegularImmutableSortedMultiset<E>(obj);
    }
    
    public static <E extends Comparable<?>> a naturalOrder() {
        return new a(Ordering.natural());
    }
    
    public static <E> ImmutableSortedMultiset<E> of() {
        return (ImmutableSortedMultiset<E>)RegularImmutableSortedMultiset.NATURAL_EMPTY_MULTISET;
    }
    
    public static <E extends Comparable<? super E>> ImmutableSortedMultiset<E> of(final E e) {
        return new RegularImmutableSortedMultiset<E>((RegularImmutableSortedSet)ImmutableSortedSet.of(e), new long[] { 0L, 1L }, 0, 1);
    }
    
    public static <E extends Comparable<? super E>> ImmutableSortedMultiset<E> of(final E e, final E e2) {
        return copyOf((Comparator<? super E>)Ordering.natural(), (Iterable<? extends E>)Arrays.asList(e, e2));
    }
    
    public static <E extends Comparable<? super E>> ImmutableSortedMultiset<E> of(final E e, final E e2, final E e3) {
        return copyOf((Comparator<? super E>)Ordering.natural(), (Iterable<? extends E>)Arrays.asList(e, e2, e3));
    }
    
    public static <E extends Comparable<? super E>> ImmutableSortedMultiset<E> of(final E e, final E e2, final E e3, final E e4) {
        return copyOf((Comparator<? super E>)Ordering.natural(), (Iterable<? extends E>)Arrays.asList(e, e2, e3, e4));
    }
    
    public static <E extends Comparable<? super E>> ImmutableSortedMultiset<E> of(final E e, final E e2, final E e3, final E e4, final E e5) {
        return copyOf((Comparator<? super E>)Ordering.natural(), (Iterable<? extends E>)Arrays.asList(e, e2, e3, e4, e5));
    }
    
    public static <E extends Comparable<? super E>> ImmutableSortedMultiset<E> of(final E e, final E e2, final E e3, final E e4, final E e5, final E e6, final E... elements) {
        final ArrayList l = Lists.l(elements.length + 6);
        Collections.addAll(l, e, e2, e3, e4, e5, e6);
        Collections.addAll(l, elements);
        return (ImmutableSortedMultiset<E>)copyOf((Comparator<? super Object>)Ordering.natural(), (Iterable<?>)l);
    }
    
    public static <E> a orderedBy(final Comparator<E> comparator) {
        return new a(comparator);
    }
    
    public static <E extends Comparable<?>> a reverseOrder() {
        return new a(Ordering.natural().reverse());
    }
    
    @Override
    public final Comparator<? super E> comparator() {
        return this.elementSet().comparator();
    }
    
    @Override
    public ImmutableSortedMultiset<E> descendingMultiset() {
        o descendingMultiset;
        if ((descendingMultiset = this.descendingMultiset) == null) {
            if (this.isEmpty()) {
                descendingMultiset = emptyMultiset((Comparator<? super E>)Ordering.from(this.comparator()).reverse());
            }
            else {
                descendingMultiset = new DescendingImmutableSortedMultiset<E>(this);
            }
            this.descendingMultiset = (ImmutableSortedMultiset<E>)descendingMultiset;
        }
        return (ImmutableSortedMultiset<E>)descendingMultiset;
    }
    
    @Override
    public abstract ImmutableSortedSet<E> elementSet();
    
    @Override
    public abstract ImmutableSortedMultiset<E> headMultiset(final E p0, final BoundType p1);
    
    @Deprecated
    @Override
    public final j.a pollFirstEntry() {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public final j.a pollLastEntry() {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public ImmutableSortedMultiset<E> subMultiset(final E e, final BoundType boundType, final E e2, final BoundType boundType2) {
        i71.n(this.comparator().compare((Object)e, (Object)e2) <= 0, "Expected lowerBound <= upperBound but %s > %s", e, e2);
        return this.tailMultiset(e, boundType).headMultiset(e2, boundType2);
    }
    
    @Override
    public abstract ImmutableSortedMultiset<E> tailMultiset(final E p0, final BoundType p1);
    
    public Object writeReplace() {
        return new SerializedForm(this);
    }
    
    public static final class SerializedForm<E> implements Serializable
    {
        final Comparator<? super E> comparator;
        final int[] counts;
        final E[] elements;
        
        public SerializedForm(final o o) {
            this.comparator = o.comparator();
            final int size = o.entrySet().size();
            this.elements = (E[])new Object[size];
            this.counts = new int[size];
            final Iterator iterator = o.entrySet().iterator();
            int n = 0;
            while (iterator.hasNext()) {
                final j.a a = (j.a)iterator.next();
                this.elements[n] = (E)a.getElement();
                this.counts[n] = a.getCount();
                ++n;
            }
        }
        
        public Object readResolve() {
            final int length = this.elements.length;
            final a a = new a(this.comparator);
            for (int i = 0; i < length; ++i) {
                a.q(this.elements[i], this.counts[i]);
            }
            return a.r();
        }
    }
    
    public static class a extends b
    {
        public final Comparator d;
        public Object[] e;
        public int[] f;
        public int g;
        public boolean h;
        
        public a(final Comparator comparator) {
            super(true);
            this.d = (Comparator)i71.r(comparator);
            this.e = new Object[4];
            this.f = new int[4];
        }
        
        public a m(final Object o) {
            return this.q(o, 1);
        }
        
        public a n(final Object... array) {
            for (int length = array.length, i = 0; i < length; ++i) {
                this.m(array[i]);
            }
            return this;
        }
        
        public a o(final Iterable iterable) {
            if (iterable instanceof j) {
                for (final j.a a : ((j)iterable).entrySet()) {
                    this.q(a.getElement(), a.getCount());
                }
            }
            else {
                final Iterator<Object> iterator2 = iterable.iterator();
                while (iterator2.hasNext()) {
                    this.m(iterator2.next());
                }
            }
            return this;
        }
        
        public a p(final Iterator iterator) {
            while (iterator.hasNext()) {
                this.m(iterator.next());
            }
            return this;
        }
        
        public a q(final Object o, final int n) {
            i71.r(o);
            hh.b(n, "occurrences");
            if (n == 0) {
                return this;
            }
            this.u();
            final Object[] e = this.e;
            final int g = this.g;
            e[g] = o;
            this.f[g] = n;
            this.g = g + 1;
            return this;
        }
        
        public ImmutableSortedMultiset r() {
            this.t();
            final int g = this.g;
            if (g == 0) {
                return ImmutableSortedMultiset.emptyMultiset(this.d);
            }
            final RegularImmutableSortedSet set = (RegularImmutableSortedSet)ImmutableSortedSet.construct(this.d, g, this.e);
            final long[] array = new long[this.g + 1];
            int n;
            for (int i = 0; i < this.g; i = n) {
                n = i + 1;
                array[n] = array[i] + this.f[i];
            }
            this.h = true;
            return new RegularImmutableSortedMultiset(set, array, 0, this.g);
        }
        
        public final void s(final boolean b) {
            final int g = this.g;
            if (g == 0) {
                return;
            }
            final Object[] copy = Arrays.copyOf(this.e, g);
            Arrays.sort(copy, this.d);
            int i = 1;
            int g2 = 1;
            while (i < copy.length) {
                int n = g2;
                if (this.d.compare(copy[g2 - 1], copy[i]) < 0) {
                    copy[g2] = copy[i];
                    n = g2 + 1;
                }
                ++i;
                g2 = n;
            }
            Arrays.fill(copy, g2, this.g, null);
            Object[] copy2 = copy;
            if (b) {
                final int g3 = this.g;
                copy2 = copy;
                if (g2 * 4 > g3 * 3) {
                    copy2 = Arrays.copyOf(copy, tf0.g(g3, g3 / 2 + 1));
                }
            }
            final int[] f = new int[copy2.length];
            for (int j = 0; j < this.g; ++j) {
                final int binarySearch = Arrays.binarySearch(copy2, 0, g2, this.e[j], this.d);
                final int n2 = this.f[j];
                if (n2 >= 0) {
                    f[binarySearch] += n2;
                }
                else {
                    f[binarySearch] = ~n2;
                }
            }
            this.e = copy2;
            this.f = f;
            this.g = g2;
        }
        
        public final void t() {
            this.s(false);
            int n = 0;
            int g = 0;
            int g2;
            while (true) {
                g2 = this.g;
                if (n >= g2) {
                    break;
                }
                final int[] f = this.f;
                final int n2 = f[n];
                int n3 = g;
                if (n2 > 0) {
                    final Object[] e = this.e;
                    e[g] = e[n];
                    f[g] = n2;
                    n3 = g + 1;
                }
                ++n;
                g = n3;
            }
            Arrays.fill(this.e, g, g2, null);
            Arrays.fill(this.f, g, this.g, 0);
            this.g = g;
        }
        
        public final void u() {
            final int g = this.g;
            final Object[] e = this.e;
            if (g == e.length) {
                this.s(true);
            }
            else if (this.h) {
                this.e = Arrays.copyOf(e, e.length);
            }
            this.h = false;
        }
    }
}

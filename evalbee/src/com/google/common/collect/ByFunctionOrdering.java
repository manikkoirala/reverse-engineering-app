// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.io.Serializable;

final class ByFunctionOrdering<F, T> extends Ordering implements Serializable
{
    private static final long serialVersionUID = 0L;
    final m90 function;
    final Ordering ordering;
    
    public ByFunctionOrdering(final m90 m90, final Ordering ordering) {
        this.function = (m90)i71.r(m90);
        this.ordering = (Ordering)i71.r(ordering);
    }
    
    @Override
    public int compare(final F n, final F n2) {
        return this.ordering.compare(this.function.apply(n), this.function.apply(n2));
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof ByFunctionOrdering) {
            final ByFunctionOrdering byFunctionOrdering = (ByFunctionOrdering)o;
            if (!this.function.equals(byFunctionOrdering.function) || !this.ordering.equals(byFunctionOrdering.ordering)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return b11.b(this.function, this.ordering);
    }
    
    @Override
    public String toString() {
        final String value = String.valueOf(this.ordering);
        final String value2 = String.valueOf(this.function);
        final StringBuilder sb = new StringBuilder(value.length() + 13 + value2.length());
        sb.append(value);
        sb.append(".onResultOf(");
        sb.append(value2);
        sb.append(")");
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Iterator;

public abstract class r implements Iterator
{
    public final Iterator a;
    
    public r(final Iterator iterator) {
        this.a = (Iterator)i71.r(iterator);
    }
    
    abstract Object b(final Object p0);
    
    @Override
    public final boolean hasNext() {
        return this.a.hasNext();
    }
    
    @Override
    public final Object next() {
        return this.b(this.a.next());
    }
    
    @Override
    public final void remove() {
        this.a.remove();
    }
}

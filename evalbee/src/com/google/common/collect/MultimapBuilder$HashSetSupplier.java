// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Set;
import java.io.Serializable;

final class MultimapBuilder$HashSetSupplier<V> implements is1, Serializable
{
    private final int expectedValuesPerKey;
    
    public MultimapBuilder$HashSetSupplier(final int n) {
        this.expectedValuesPerKey = hh.b(n, "expectedValuesPerKey");
    }
    
    @Override
    public Set<V> get() {
        return l.d(this.expectedValuesPerKey);
    }
}

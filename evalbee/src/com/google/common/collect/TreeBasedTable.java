// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.NoSuchElementException;
import java.io.Serializable;
import java.util.Collection;
import java.util.SortedSet;
import java.util.Objects;
import java.util.Set;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Comparator;

public class TreeBasedTable<R, C, V> extends StandardRowSortedTable<R, C, V>
{
    private static final long serialVersionUID = 0L;
    private final Comparator<? super C> columnComparator;
    
    public TreeBasedTable(final Comparator<? super R> comparator, final Comparator<? super C> columnComparator) {
        super(new TreeMap(comparator), new Factory<Object, Object>(columnComparator));
        this.columnComparator = columnComparator;
    }
    
    public static <R extends Comparable, C extends Comparable, V> TreeBasedTable<R, C, V> create() {
        return new TreeBasedTable<R, C, V>(Ordering.natural(), Ordering.natural());
    }
    
    public static <R, C, V> TreeBasedTable<R, C, V> create(final TreeBasedTable<R, C, ? extends V> treeBasedTable) {
        final TreeBasedTable treeBasedTable2 = new TreeBasedTable((Comparator<? super R>)treeBasedTable.rowComparator(), (Comparator<? super C>)treeBasedTable.columnComparator());
        treeBasedTable2.putAll(treeBasedTable);
        return treeBasedTable2;
    }
    
    public static <R, C, V> TreeBasedTable<R, C, V> create(final Comparator<? super R> comparator, final Comparator<? super C> comparator2) {
        i71.r(comparator);
        i71.r(comparator2);
        return new TreeBasedTable<R, C, V>(comparator, comparator2);
    }
    
    @Deprecated
    public Comparator<? super C> columnComparator() {
        return this.columnComparator;
    }
    
    @Override
    public Iterator<C> createColumnKeyIterator() {
        final Comparator<? super C> columnComparator = this.columnComparator();
        return new AbstractIterator(this, Iterators.o(rg0.n(super.backingMap.values(), new bz1()), columnComparator), columnComparator) {
            public Object c;
            public final Iterator d;
            public final Comparator e;
            
            @Override
            public Object b() {
                while (this.d.hasNext()) {
                    final Object next = this.d.next();
                    final Object c = this.c;
                    if (c == null || this.e.compare(next, c) != 0) {
                        return this.c = next;
                    }
                }
                this.c = null;
                return this.c();
            }
        };
    }
    
    @Override
    public SortedMap<C, V> row(final R r) {
        return new b(r);
    }
    
    @Deprecated
    public Comparator<? super R> rowComparator() {
        final Comparator<? super R> comparator = this.rowKeySet().comparator();
        Objects.requireNonNull(comparator);
        return comparator;
    }
    
    @Override
    public SortedSet<R> rowKeySet() {
        return super.rowKeySet();
    }
    
    @Override
    public SortedMap<R, Map<C, V>> rowMap() {
        return super.rowMap();
    }
    
    public static class Factory<C, V> implements is1, Serializable
    {
        private static final long serialVersionUID = 0L;
        final Comparator<? super C> comparator;
        
        public Factory(final Comparator<? super C> comparator) {
            this.comparator = comparator;
        }
        
        @Override
        public TreeMap<C, V> get() {
            return new TreeMap<C, V>(this.comparator);
        }
    }
    
    public class b extends g implements SortedMap
    {
        public final Object d;
        public final Object e;
        public transient SortedMap f;
        public final TreeBasedTable g;
        
        public b(final TreeBasedTable treeBasedTable, final Object o) {
            this(treeBasedTable, o, null, null);
        }
        
        public b(final TreeBasedTable g, final Object o, final Object d, final Object e) {
            this.g = g.super(o);
            this.d = d;
            this.e = e;
            i71.d(d == null || e == null || this.f(d, e) <= 0);
        }
        
        @Override
        public void c() {
            this.j();
            final SortedMap f = this.f;
            if (f != null && f.isEmpty()) {
                this.g.backingMap.remove(super.a);
                this.f = null;
                super.b = null;
            }
        }
        
        @Override
        public Comparator comparator() {
            return this.g.columnComparator();
        }
        
        @Override
        public boolean containsKey(final Object o) {
            return this.i(o) && super.containsKey(o);
        }
        
        public int f(final Object o, final Object o2) {
            return this.comparator().compare(o, o2);
        }
        
        @Override
        public Object firstKey() {
            ((g)this).d();
            final Map b = super.b;
            if (b != null) {
                return ((SortedMap<Object, V>)b).firstKey();
            }
            throw new NoSuchElementException();
        }
        
        public SortedMap g() {
            this.j();
            final SortedMap f = this.f;
            if (f != null) {
                final Object d = this.d;
                SortedMap<Object, Object> tailMap = f;
                if (d != null) {
                    tailMap = f.tailMap(d);
                }
                final Object e = this.e;
                SortedMap<Object, Object> headMap = tailMap;
                if (e != null) {
                    headMap = tailMap.headMap(e);
                }
                return headMap;
            }
            return null;
        }
        
        public SortedSet h() {
            return new Maps.o(this);
        }
        
        @Override
        public SortedMap headMap(final Object o) {
            i71.d(this.i(i71.r(o)));
            return this.g.new b(super.a, this.d, o);
        }
        
        public boolean i(final Object o) {
            if (o != null) {
                final Object d = this.d;
                if (d == null || this.f(d, o) <= 0) {
                    final Object e = this.e;
                    if (e == null || this.f(e, o) > 0) {
                        return true;
                    }
                }
            }
            return false;
        }
        
        public void j() {
            final SortedMap f = this.f;
            if (f == null || (f.isEmpty() && this.g.backingMap.containsKey(super.a))) {
                this.f = this.g.backingMap.get(super.a);
            }
        }
        
        @Override
        public Object lastKey() {
            ((g)this).d();
            final Map b = super.b;
            if (b != null) {
                return ((SortedMap<Object, V>)b).lastKey();
            }
            throw new NoSuchElementException();
        }
        
        @Override
        public Object put(final Object o, final Object o2) {
            i71.d(this.i(i71.r(o)));
            return super.put(o, o2);
        }
        
        @Override
        public SortedMap subMap(final Object o, final Object o2) {
            i71.d(this.i(i71.r(o)) && this.i(i71.r(o2)));
            return this.g.new b(super.a, o, o2);
        }
        
        @Override
        public SortedMap tailMap(final Object o) {
            i71.d(this.i(i71.r(o)));
            return this.g.new b(super.a, o, this.e);
        }
    }
}

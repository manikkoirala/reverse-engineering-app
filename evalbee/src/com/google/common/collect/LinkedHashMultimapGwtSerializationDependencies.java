// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Collection;
import java.util.Map;

abstract class LinkedHashMultimapGwtSerializationDependencies<K, V> extends AbstractSetMultimap<K, V>
{
    public LinkedHashMultimapGwtSerializationDependencies(final Map<K, Collection<V>> map) {
        super(map);
    }
}

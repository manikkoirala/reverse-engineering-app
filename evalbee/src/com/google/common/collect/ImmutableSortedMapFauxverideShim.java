// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Map;

abstract class ImmutableSortedMapFauxverideShim<K, V> extends ImmutableMap<K, V>
{
    public ImmutableSortedMapFauxverideShim() {
    }
    
    @Deprecated
    public static <K, V> ImmutableSortedMap.b builder() {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    public static <K, V> ImmutableSortedMap.b builderWithExpectedSize(final int n) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    public static <K, V> ImmutableSortedMap<K, V> of(final K k, final V v) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    public static <K, V> ImmutableSortedMap<K, V> of(final K k, final V v, final K i, final V v2) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    public static <K, V> ImmutableSortedMap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    public static <K, V> ImmutableSortedMap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    public static <K, V> ImmutableSortedMap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4, final K m, final V v5) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    public static <K, V> ImmutableSortedMap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4, final K m, final V v5, final K k2, final V v6) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    public static <K, V> ImmutableSortedMap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4, final K m, final V v5, final K k2, final V v6, final K k3, final V v7) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    public static <K, V> ImmutableSortedMap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4, final K m, final V v5, final K k2, final V v6, final K k3, final V v7, final K k4, final V v8) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    public static <K, V> ImmutableSortedMap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4, final K m, final V v5, final K k2, final V v6, final K k3, final V v7, final K k4, final V v8, final K k5, final V v9) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    public static <K, V> ImmutableSortedMap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4, final K m, final V v5, final K k2, final V v6, final K k3, final V v7, final K k4, final V v8, final K k5, final V v9, final K k6, final V v10) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    public static <K, V> ImmutableSortedMap<K, V> ofEntries(final Entry<? extends K, ? extends V>... array) {
        throw new UnsupportedOperationException();
    }
}

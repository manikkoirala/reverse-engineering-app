// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.EnumSet;
import java.util.Set;
import java.io.Serializable;

final class MultimapBuilder$EnumSetSupplier<V extends Enum<V>> implements is1, Serializable
{
    private final Class<V> clazz;
    
    public MultimapBuilder$EnumSetSupplier(final Class<V> clazz) {
        this.clazz = (Class)i71.r(clazz);
    }
    
    @Override
    public Set<V> get() {
        return EnumSet.noneOf(this.clazz);
    }
}

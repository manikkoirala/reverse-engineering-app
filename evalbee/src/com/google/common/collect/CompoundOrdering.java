// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Arrays;
import java.util.Comparator;
import java.io.Serializable;

final class CompoundOrdering<T> extends Ordering implements Serializable
{
    private static final long serialVersionUID = 0L;
    final Comparator<? super T>[] comparators;
    
    public CompoundOrdering(final Iterable<? extends Comparator<? super T>> iterable) {
        this.comparators = (Comparator[])rg0.l(iterable, new Comparator[0]);
    }
    
    public CompoundOrdering(final Comparator<? super T> comparator, final Comparator<? super T> comparator2) {
        this.comparators = new Comparator[] { comparator, comparator2 };
    }
    
    @Override
    public int compare(final T t, final T t2) {
        int n = 0;
        while (true) {
            final Comparator<? super T>[] comparators = this.comparators;
            if (n >= comparators.length) {
                return 0;
            }
            final int compare = comparators[n].compare((Object)t, (Object)t2);
            if (compare != 0) {
                return compare;
            }
            ++n;
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof CompoundOrdering && Arrays.equals(this.comparators, ((CompoundOrdering)o).comparators));
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(this.comparators);
    }
    
    @Override
    public String toString() {
        final String string = Arrays.toString(this.comparators);
        final StringBuilder sb = new StringBuilder(String.valueOf(string).length() + 19);
        sb.append("Ordering.compound(");
        sb.append(string);
        sb.append(")");
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.io.Serializable;

final class Count implements Serializable
{
    private int value;
    
    public Count(final int value) {
        this.value = value;
    }
    
    public void add(final int n) {
        this.value += n;
    }
    
    public int addAndGet(int value) {
        value += this.value;
        return this.value = value;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof Count && ((Count)o).value == this.value;
    }
    
    public int get() {
        return this.value;
    }
    
    public int getAndSet(final int value) {
        final int value2 = this.value;
        this.value = value;
        return value2;
    }
    
    @Override
    public int hashCode() {
        return this.value;
    }
    
    public void set(final int value) {
        this.value = value;
    }
    
    @Override
    public String toString() {
        return Integer.toString(this.value);
    }
}

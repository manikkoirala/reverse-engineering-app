// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Iterator;
import java.util.AbstractMap;
import java.util.Collection;
import java.util.Set;
import java.util.NavigableSet;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;
import java.util.SortedMap;
import java.util.Map;
import java.util.Comparator;
import java.util.NavigableMap;

public final class ImmutableSortedMap<K, V> extends ImmutableSortedMapFauxverideShim<K, V> implements NavigableMap<K, V>
{
    private static final ImmutableSortedMap<Comparable, Object> NATURAL_EMPTY_MAP;
    private static final Comparator<Comparable> NATURAL_ORDER;
    private static final long serialVersionUID = 0L;
    private transient ImmutableSortedMap<K, V> descendingMap;
    private final transient RegularImmutableSortedSet<K> keySet;
    private final transient ImmutableList<V> valueList;
    
    static {
        NATURAL_ORDER = Ordering.natural();
        NATURAL_EMPTY_MAP = new ImmutableSortedMap<Comparable, Object>((RegularImmutableSortedSet<Comparable>)ImmutableSortedSet.emptySet((Comparator<? super Comparable>)Ordering.natural()), ImmutableList.of());
    }
    
    public ImmutableSortedMap(final RegularImmutableSortedSet<K> set, final ImmutableList<V> list) {
        this(set, list, null);
    }
    
    public ImmutableSortedMap(final RegularImmutableSortedSet<K> keySet, final ImmutableList<V> valueList, final ImmutableSortedMap<K, V> descendingMap) {
        this.keySet = keySet;
        this.valueList = valueList;
        this.descendingMap = descendingMap;
    }
    
    public static /* synthetic */ RegularImmutableSortedSet access$100(final ImmutableSortedMap immutableSortedMap) {
        return immutableSortedMap.keySet;
    }
    
    public static /* synthetic */ ImmutableList access$200(final ImmutableSortedMap immutableSortedMap) {
        return immutableSortedMap.valueList;
    }
    
    public static <K, V> ImmutableSortedMap<K, V> copyOf(final Iterable<? extends Entry<? extends K, ? extends V>> iterable) {
        return copyOf(iterable, (Comparator<? super K>)ImmutableSortedMap.NATURAL_ORDER);
    }
    
    public static <K, V> ImmutableSortedMap<K, V> copyOf(final Iterable<? extends Entry<? extends K, ? extends V>> iterable, final Comparator<? super K> comparator) {
        return fromEntries((Comparator<? super K>)i71.r(comparator), false, iterable);
    }
    
    public static <K, V> ImmutableSortedMap<K, V> copyOf(final Map<? extends K, ? extends V> map) {
        return copyOfInternal(map, (Comparator<? super K>)ImmutableSortedMap.NATURAL_ORDER);
    }
    
    public static <K, V> ImmutableSortedMap<K, V> copyOf(final Map<? extends K, ? extends V> map, final Comparator<? super K> comparator) {
        return (ImmutableSortedMap<K, V>)copyOfInternal((Map<?, ?>)map, (Comparator<? super Object>)i71.r(comparator));
    }
    
    private static <K, V> ImmutableSortedMap<K, V> copyOfInternal(final Map<? extends K, ? extends V> map, final Comparator<? super K> comparator) {
        final boolean b = map instanceof SortedMap;
        boolean equals;
        final boolean b2 = equals = false;
        if (b) {
            final Comparator comparator2 = ((SortedMap)map).comparator();
            if (comparator2 == null) {
                equals = b2;
                if (comparator == ImmutableSortedMap.NATURAL_ORDER) {
                    equals = true;
                }
            }
            else {
                equals = comparator.equals(comparator2);
            }
        }
        if (equals && map instanceof ImmutableSortedMap) {
            final ImmutableSortedMap immutableSortedMap = (ImmutableSortedMap)map;
            if (!immutableSortedMap.isPartialView()) {
                return immutableSortedMap;
            }
        }
        return (ImmutableSortedMap<K, V>)fromEntries((Comparator<? super Object>)comparator, equals, (Iterable<? extends Entry<?, ?>>)map.entrySet());
    }
    
    public static <K, V> ImmutableSortedMap<K, V> copyOfSorted(final SortedMap<K, ? extends V> sortedMap) {
        Comparator<Comparable> comparator;
        if ((comparator = sortedMap.comparator()) == null) {
            comparator = ImmutableSortedMap.NATURAL_ORDER;
        }
        if (sortedMap instanceof ImmutableSortedMap) {
            final ImmutableSortedMap immutableSortedMap = (ImmutableSortedMap)sortedMap;
            if (!immutableSortedMap.isPartialView()) {
                return immutableSortedMap;
            }
        }
        return (ImmutableSortedMap<K, V>)fromEntries((Comparator<? super Object>)comparator, true, (Iterable<? extends Entry<?, ?>>)sortedMap.entrySet());
    }
    
    public static <K, V> ImmutableSortedMap<K, V> emptyMap(final Comparator<? super K> obj) {
        if (Ordering.natural().equals(obj)) {
            return of();
        }
        return new ImmutableSortedMap<K, V>(ImmutableSortedSet.emptySet(obj), ImmutableList.of());
    }
    
    private static <K, V> ImmutableSortedMap<K, V> fromEntries(final Comparator<? super K> comparator, final boolean b, final Iterable<? extends Entry<? extends K, ? extends V>> iterable) {
        final Entry[] array = (Entry[])rg0.l(iterable, ImmutableMap.EMPTY_ENTRY_ARRAY);
        return fromEntries(comparator, b, (Entry<K, V>[])array, array.length);
    }
    
    private static <K, V> ImmutableSortedMap<K, V> fromEntries(final Comparator<? super K> comparator, final boolean b, Entry<K, V>[] a, final int toIndex) {
        if (toIndex == 0) {
            return (ImmutableSortedMap<K, V>)emptyMap((Comparator<? super Object>)comparator);
        }
        int i = 0;
        if (toIndex != 1) {
            final Object[] array = new Object[toIndex];
            final Object[] array2 = new Object[toIndex];
            if (b) {
                while (i < toIndex) {
                    final Map.Entry<Object, V> obj = a[i];
                    Objects.requireNonNull((Entry<K, V>)obj);
                    final Map.Entry<Object, V> entry = (Map.Entry<Object, V>)obj;
                    final Object key = entry.getKey();
                    final V value = entry.getValue();
                    hh.a(key, value);
                    array[i] = key;
                    array2[i] = value;
                    ++i;
                }
            }
            else {
                Arrays.sort((Entry<K, V>[])(Object)a, 0, toIndex, new Comparator(comparator) {
                    public final Comparator a;
                    
                    public int a(final Entry obj, final Entry obj2) {
                        Objects.requireNonNull(obj);
                        Objects.requireNonNull(obj2);
                        return this.a.compare(obj.getKey(), obj2.getKey());
                    }
                });
                final Entry<K, V> obj2 = a[0];
                Objects.requireNonNull(obj2);
                final Map.Entry<Object, V> entry2 = (Map.Entry<Object, V>)obj2;
                Object key2 = entry2.getKey();
                array[0] = key2;
                final V value2 = entry2.getValue();
                array2[0] = value2;
                hh.a(array[0], value2);
                Object key3;
                for (int j = 1; j < toIndex; ++j, key2 = key3) {
                    final Entry<K, V> obj3 = a[j - 1];
                    Objects.requireNonNull(obj3);
                    final Entry entry3 = obj3;
                    final Map.Entry<Object, V> obj4 = a[j];
                    Objects.requireNonNull((Entry<K, V>)obj4);
                    final Map.Entry<Object, V> entry4 = (Map.Entry<Object, V>)obj4;
                    key3 = entry4.getKey();
                    final V value3 = entry4.getValue();
                    hh.a(key3, value3);
                    array[j] = key3;
                    array2[j] = value3;
                    ImmutableMap.checkNoConflict(comparator.compare(key2, key3) != 0, "key", entry3, entry4);
                }
            }
            return new ImmutableSortedMap<K, V>(new RegularImmutableSortedSet<Object>(ImmutableList.asImmutableList(array), (Comparator<? super Object>)comparator), ImmutableList.asImmutableList(array2));
        }
        final Entry<K, V> obj5 = a[0];
        Objects.requireNonNull(obj5);
        a = (Map.Entry<K, V>)obj5;
        return of(comparator, (K)a.getKey(), (V)a.getValue());
    }
    
    private static <K extends Comparable<? super K>, V> ImmutableSortedMap<K, V> fromEntries(final Entry<K, V>... array) {
        return fromEntries(Ordering.natural(), false, array, array.length);
    }
    
    private ImmutableSortedMap<K, V> getSubMap(final int n, final int n2) {
        if (n == 0 && n2 == this.size()) {
            return this;
        }
        if (n == n2) {
            return emptyMap(this.comparator());
        }
        return new ImmutableSortedMap<K, V>(this.keySet.getSubSet(n, n2), this.valueList.subList(n, n2));
    }
    
    public static <K extends Comparable<?>, V> b naturalOrder() {
        return new b(Ordering.natural());
    }
    
    public static <K, V> ImmutableSortedMap<K, V> of() {
        return (ImmutableSortedMap<K, V>)ImmutableSortedMap.NATURAL_EMPTY_MAP;
    }
    
    public static <K extends Comparable<? super K>, V> ImmutableSortedMap<K, V> of(final K k, final V v) {
        return of(Ordering.natural(), k, v);
    }
    
    public static <K extends Comparable<? super K>, V> ImmutableSortedMap<K, V> of(final K k, final V v, final K i, final V v2) {
        return fromEntries(ImmutableMap.entryOf(k, v), ImmutableMap.entryOf(i, v2));
    }
    
    public static <K extends Comparable<? super K>, V> ImmutableSortedMap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3) {
        return fromEntries(ImmutableMap.entryOf(k, v), ImmutableMap.entryOf(i, v2), ImmutableMap.entryOf(j, v3));
    }
    
    public static <K extends Comparable<? super K>, V> ImmutableSortedMap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4) {
        return fromEntries(ImmutableMap.entryOf(k, v), ImmutableMap.entryOf(i, v2), ImmutableMap.entryOf(j, v3), ImmutableMap.entryOf(l, v4));
    }
    
    public static <K extends Comparable<? super K>, V> ImmutableSortedMap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4, final K m, final V v5) {
        return fromEntries(ImmutableMap.entryOf(k, v), ImmutableMap.entryOf(i, v2), ImmutableMap.entryOf(j, v3), ImmutableMap.entryOf(l, v4), ImmutableMap.entryOf(m, v5));
    }
    
    public static <K extends Comparable<? super K>, V> ImmutableSortedMap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4, final K m, final V v5, final K k2, final V v6) {
        return fromEntries(ImmutableMap.entryOf(k, v), ImmutableMap.entryOf(i, v2), ImmutableMap.entryOf(j, v3), ImmutableMap.entryOf(l, v4), ImmutableMap.entryOf(m, v5), ImmutableMap.entryOf(k2, v6));
    }
    
    public static <K extends Comparable<? super K>, V> ImmutableSortedMap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4, final K m, final V v5, final K k2, final V v6, final K k3, final V v7) {
        return fromEntries(ImmutableMap.entryOf(k, v), ImmutableMap.entryOf(i, v2), ImmutableMap.entryOf(j, v3), ImmutableMap.entryOf(l, v4), ImmutableMap.entryOf(m, v5), ImmutableMap.entryOf(k2, v6), ImmutableMap.entryOf(k3, v7));
    }
    
    public static <K extends Comparable<? super K>, V> ImmutableSortedMap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4, final K m, final V v5, final K k2, final V v6, final K k3, final V v7, final K k4, final V v8) {
        return fromEntries(ImmutableMap.entryOf(k, v), ImmutableMap.entryOf(i, v2), ImmutableMap.entryOf(j, v3), ImmutableMap.entryOf(l, v4), ImmutableMap.entryOf(m, v5), ImmutableMap.entryOf(k2, v6), ImmutableMap.entryOf(k3, v7), ImmutableMap.entryOf(k4, v8));
    }
    
    public static <K extends Comparable<? super K>, V> ImmutableSortedMap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4, final K m, final V v5, final K k2, final V v6, final K k3, final V v7, final K k4, final V v8, final K k5, final V v9) {
        return fromEntries(ImmutableMap.entryOf(k, v), ImmutableMap.entryOf(i, v2), ImmutableMap.entryOf(j, v3), ImmutableMap.entryOf(l, v4), ImmutableMap.entryOf(m, v5), ImmutableMap.entryOf(k2, v6), ImmutableMap.entryOf(k3, v7), ImmutableMap.entryOf(k4, v8), ImmutableMap.entryOf(k5, v9));
    }
    
    public static <K extends Comparable<? super K>, V> ImmutableSortedMap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4, final K m, final V v5, final K k2, final V v6, final K k3, final V v7, final K k4, final V v8, final K k5, final V v9, final K k6, final V v10) {
        return fromEntries(ImmutableMap.entryOf(k, v), ImmutableMap.entryOf(i, v2), ImmutableMap.entryOf(j, v3), ImmutableMap.entryOf(l, v4), ImmutableMap.entryOf(m, v5), ImmutableMap.entryOf(k2, v6), ImmutableMap.entryOf(k3, v7), ImmutableMap.entryOf(k4, v8), ImmutableMap.entryOf(k5, v9), ImmutableMap.entryOf(k6, v10));
    }
    
    private static <K, V> ImmutableSortedMap<K, V> of(final Comparator<? super K> comparator, final K k, final V v) {
        return new ImmutableSortedMap<K, V>(new RegularImmutableSortedSet<K>(ImmutableList.of(k), (Comparator<? super K>)i71.r(comparator)), ImmutableList.of(v));
    }
    
    public static <K, V> b orderedBy(final Comparator<K> comparator) {
        return new b(comparator);
    }
    
    public static <K extends Comparable<?>, V> b reverseOrder() {
        return new b(Ordering.natural().reverse());
    }
    
    @Override
    public Entry<K, V> ceilingEntry(final K k) {
        return this.tailMap(k, true).firstEntry();
    }
    
    @Override
    public K ceilingKey(final K k) {
        return (K)Maps.n(this.ceilingEntry(k));
    }
    
    @Override
    public Comparator<? super K> comparator() {
        return this.keySet().comparator();
    }
    
    @Override
    public ImmutableSet<Entry<K, V>> createEntrySet() {
        Serializable of;
        if (this.isEmpty()) {
            of = ImmutableSet.of();
        }
        else {
            of = new EntrySet();
        }
        return (ImmutableSet<Entry<K, V>>)of;
    }
    
    @Override
    public ImmutableSet<K> createKeySet() {
        throw new AssertionError((Object)"should never be called");
    }
    
    @Override
    public ImmutableCollection<V> createValues() {
        throw new AssertionError((Object)"should never be called");
    }
    
    @Override
    public ImmutableSortedSet<K> descendingKeySet() {
        return this.keySet.descendingSet();
    }
    
    @Override
    public ImmutableSortedMap<K, V> descendingMap() {
        ImmutableSortedMap<K, V> descendingMap;
        if ((descendingMap = this.descendingMap) == null) {
            if (this.isEmpty()) {
                return emptyMap((Comparator<? super K>)Ordering.from(this.comparator()).reverse());
            }
            descendingMap = new ImmutableSortedMap<K, V>((RegularImmutableSortedSet)this.keySet.descendingSet(), this.valueList.reverse(), this);
        }
        return descendingMap;
    }
    
    @Override
    public ImmutableSet<Entry<K, V>> entrySet() {
        return super.entrySet();
    }
    
    @Override
    public Entry<K, V> firstEntry() {
        Entry<K, V> entry;
        if (this.isEmpty()) {
            entry = null;
        }
        else {
            entry = (Entry<K, V>)this.entrySet().asList().get(0);
        }
        return entry;
    }
    
    @Override
    public K firstKey() {
        return this.keySet().first();
    }
    
    @Override
    public Entry<K, V> floorEntry(final K k) {
        return this.headMap(k, true).lastEntry();
    }
    
    @Override
    public K floorKey(final K k) {
        return (K)Maps.n(this.floorEntry(k));
    }
    
    @Override
    public V get(Object value) {
        final int index = this.keySet.indexOf(value);
        if (index == -1) {
            value = null;
        }
        else {
            value = this.valueList.get(index);
        }
        return (V)value;
    }
    
    @Override
    public ImmutableSortedMap<K, V> headMap(final K k) {
        return this.headMap(k, false);
    }
    
    @Override
    public ImmutableSortedMap<K, V> headMap(final K k, final boolean b) {
        return this.getSubMap(0, this.keySet.headIndex((K)i71.r(k), b));
    }
    
    @Override
    public Entry<K, V> higherEntry(final K k) {
        return this.tailMap(k, false).firstEntry();
    }
    
    @Override
    public K higherKey(final K k) {
        return (K)Maps.n(this.higherEntry(k));
    }
    
    @Override
    public boolean isPartialView() {
        return this.keySet.isPartialView() || this.valueList.isPartialView();
    }
    
    @Override
    public ImmutableSortedSet<K> keySet() {
        return this.keySet;
    }
    
    @Override
    public Entry<K, V> lastEntry() {
        Entry<K, V> entry;
        if (this.isEmpty()) {
            entry = null;
        }
        else {
            entry = (Entry<K, V>)this.entrySet().asList().get(this.size() - 1);
        }
        return entry;
    }
    
    @Override
    public K lastKey() {
        return this.keySet().last();
    }
    
    @Override
    public Entry<K, V> lowerEntry(final K k) {
        return this.headMap(k, false).lastEntry();
    }
    
    @Override
    public K lowerKey(final K k) {
        return (K)Maps.n(this.lowerEntry(k));
    }
    
    @Override
    public ImmutableSortedSet<K> navigableKeySet() {
        return this.keySet;
    }
    
    @Deprecated
    @Override
    public final Entry<K, V> pollFirstEntry() {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public final Entry<K, V> pollLastEntry() {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public int size() {
        return this.valueList.size();
    }
    
    @Override
    public ImmutableSortedMap<K, V> subMap(final K k, final K i) {
        return this.subMap(k, true, i, false);
    }
    
    @Override
    public ImmutableSortedMap<K, V> subMap(final K k, final boolean b, final K i, final boolean b2) {
        i71.r(k);
        i71.r(i);
        i71.n(this.comparator().compare((Object)k, (Object)i) <= 0, "expected fromKey <= toKey but %s > %s", k, i);
        return this.headMap(i, b2).tailMap(k, b);
    }
    
    @Override
    public ImmutableSortedMap<K, V> tailMap(final K k) {
        return this.tailMap(k, true);
    }
    
    @Override
    public ImmutableSortedMap<K, V> tailMap(final K k, final boolean b) {
        return this.getSubMap(this.keySet.tailIndex((K)i71.r(k), b), this.size());
    }
    
    @Override
    public ImmutableCollection<V> values() {
        return this.valueList;
    }
    
    public Object writeReplace() {
        return new SerializedForm((ImmutableSortedMap<Object, Object>)this);
    }
    
    public class EntrySet extends ImmutableMapEntrySet<K, V>
    {
        final ImmutableSortedMap this$0;
        
        public EntrySet(final ImmutableSortedMap this$0) {
            this.this$0 = this$0;
        }
        
        @Override
        public ImmutableList<Entry<K, V>> createAsList() {
            return new ImmutableList<Entry<K, V>>(this) {
                final EntrySet this$1;
                
                @Override
                public Entry<K, V> get(final int n) {
                    return (Entry<K, V>)new AbstractMap.SimpleImmutableEntry(ImmutableSortedMap.access$100(this.this$1.this$0).asList().get(n), ImmutableSortedMap.access$200(this.this$1.this$0).get(n));
                }
                
                @Override
                public boolean isPartialView() {
                    return true;
                }
                
                @Override
                public int size() {
                    return this.this$1.this$0.size();
                }
            };
        }
        
        @Override
        public w02 iterator() {
            return this.asList().iterator();
        }
        
        @Override
        public ImmutableMap<K, V> map() {
            return this.this$0;
        }
    }
    
    public static class SerializedForm<K, V> extends ImmutableMap.SerializedForm<K, V>
    {
        private static final long serialVersionUID = 0L;
        private final Comparator<? super K> comparator;
        
        public SerializedForm(final ImmutableSortedMap<K, V> immutableSortedMap) {
            super(immutableSortedMap);
            this.comparator = immutableSortedMap.comparator();
        }
        
        public ImmutableSortedMap.b makeBuilder(final int n) {
            return new ImmutableSortedMap.b(this.comparator);
        }
    }
    
    public static class b extends ImmutableMap.b
    {
        public transient Object[] f;
        public transient Object[] g;
        public final Comparator h;
        
        public b(final Comparator comparator) {
            this(comparator, 4);
        }
        
        public b(final Comparator comparator, final int n) {
            this.h = (Comparator)i71.r(comparator);
            this.f = new Object[n];
            this.g = new Object[n];
        }
        
        private void e(int e) {
            final Object[] f = this.f;
            if (e > f.length) {
                e = ImmutableCollection.b.e(f.length, e);
                this.f = Arrays.copyOf(this.f, e);
                this.g = Arrays.copyOf(this.g, e);
            }
        }
        
        public ImmutableSortedMap l() {
            return this.n();
        }
        
        public final ImmutableSortedMap m() {
            throw new UnsupportedOperationException("ImmutableSortedMap.Builder does not yet implement buildKeepingLast()");
        }
        
        public ImmutableSortedMap n() {
            final int c = super.c;
            if (c == 0) {
                return ImmutableSortedMap.emptyMap(this.h);
            }
            int i = 0;
            if (c != 1) {
                final Object[] copy = Arrays.copyOf(this.f, c);
                Arrays.sort(copy, this.h);
                final Object[] array = new Object[super.c];
                while (i < super.c) {
                    if (i > 0) {
                        final Comparator h = this.h;
                        final int n = i - 1;
                        if (h.compare(copy[n], copy[i]) == 0) {
                            final String value = String.valueOf(copy[n]);
                            final String value2 = String.valueOf(copy[i]);
                            final StringBuilder sb = new StringBuilder(value.length() + 57 + value2.length());
                            sb.append("keys required to be distinct but compared as equal: ");
                            sb.append(value);
                            sb.append(" and ");
                            sb.append(value2);
                            throw new IllegalArgumentException(sb.toString());
                        }
                    }
                    final Object o = this.f[i];
                    Objects.requireNonNull(o);
                    final int binarySearch = Arrays.binarySearch(copy, o, this.h);
                    final Object obj = this.g[i];
                    Objects.requireNonNull(obj);
                    array[binarySearch] = obj;
                    ++i;
                }
                return new ImmutableSortedMap(new RegularImmutableSortedSet<Object>(ImmutableList.asImmutableList(copy), this.h), ImmutableList.asImmutableList(array));
            }
            final Comparator h2 = this.h;
            final Object obj2 = this.f[0];
            Objects.requireNonNull(obj2);
            final Object obj3 = this.g[0];
            Objects.requireNonNull(obj3);
            return of(h2, obj2, obj3);
        }
        
        public b o(final Object o, final Object o2) {
            this.e(super.c + 1);
            hh.a(o, o2);
            final Object[] f = this.f;
            final int c = super.c;
            f[c] = o;
            this.g[c] = o2;
            super.c = c + 1;
            return this;
        }
        
        public b p(final Entry entry) {
            super.h(entry);
            return this;
        }
        
        public b q(final Iterable iterable) {
            super.i(iterable);
            return this;
        }
        
        public b r(final Map map) {
            super.j(map);
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.RandomAccess;
import java.util.Comparator;
import java.util.List;

public abstract class SortedLists
{
    public static int a(final List list, final m90 m90, final Comparable comparable, final KeyPresentBehavior keyPresentBehavior, final KeyAbsentBehavior keyAbsentBehavior) {
        i71.r(comparable);
        return b(list, m90, comparable, Ordering.natural(), keyPresentBehavior, keyAbsentBehavior);
    }
    
    public static int b(final List list, final m90 m90, final Object o, final Comparator comparator, final KeyPresentBehavior keyPresentBehavior, final KeyAbsentBehavior keyAbsentBehavior) {
        return c(Lists.n(list, m90), o, comparator, keyPresentBehavior, keyAbsentBehavior);
    }
    
    public static int c(final List list, final Object o, final Comparator comparator, final KeyPresentBehavior keyPresentBehavior, final KeyAbsentBehavior keyAbsentBehavior) {
        i71.r(comparator);
        i71.r(list);
        i71.r(keyPresentBehavior);
        i71.r(keyAbsentBehavior);
        List j = list;
        if (!(list instanceof RandomAccess)) {
            j = Lists.j(list);
        }
        int n = j.size() - 1;
        int i = 0;
        while (i <= n) {
            final int n2 = i + n >>> 1;
            final int compare = comparator.compare(o, j.get(n2));
            if (compare < 0) {
                n = n2 - 1;
            }
            else {
                if (compare <= 0) {
                    return i + keyPresentBehavior.resultIndex(comparator, o, j.subList(i, n + 1), n2 - i);
                }
                i = n2 + 1;
            }
        }
        return keyAbsentBehavior.resultIndex(i);
    }
    
    public enum KeyAbsentBehavior
    {
        private static final KeyAbsentBehavior[] $VALUES;
        
        INVERTED_INSERTION_INDEX {
            @Override
            public int resultIndex(final int n) {
                return ~n;
            }
        }, 
        NEXT_HIGHER {
            @Override
            public int resultIndex(final int n) {
                return n;
            }
        }, 
        NEXT_LOWER {
            @Override
            public int resultIndex(final int n) {
                return n - 1;
            }
        };
        
        private static /* synthetic */ KeyAbsentBehavior[] $values() {
            return new KeyAbsentBehavior[] { KeyAbsentBehavior.NEXT_LOWER, KeyAbsentBehavior.NEXT_HIGHER, KeyAbsentBehavior.INVERTED_INSERTION_INDEX };
        }
        
        static {
            $VALUES = $values();
        }
        
        public abstract int resultIndex(final int p0);
    }
    
    public enum KeyPresentBehavior
    {
        private static final KeyPresentBehavior[] $VALUES;
        
        ANY_PRESENT {
            @Override
            public <E> int resultIndex(final Comparator<? super E> comparator, final E e, final List<? extends E> list, final int n) {
                return n;
            }
        }, 
        FIRST_AFTER {
            @Override
            public <E> int resultIndex(final Comparator<? super E> comparator, final E e, final List<? extends E> list, final int n) {
                return KeyPresentBehavior.LAST_PRESENT.resultIndex(comparator, e, list, n) + 1;
            }
        }, 
        FIRST_PRESENT {
            @Override
            public <E> int resultIndex(final Comparator<? super E> comparator, final E e, final List<? extends E> list, int n) {
                int i = 0;
                while (i < n) {
                    final int n2 = i + n >>> 1;
                    if (comparator.compare((E)list.get(n2), e) < 0) {
                        i = n2 + 1;
                    }
                    else {
                        n = n2;
                    }
                }
                return i;
            }
        }, 
        LAST_BEFORE {
            @Override
            public <E> int resultIndex(final Comparator<? super E> comparator, final E e, final List<? extends E> list, final int n) {
                return KeyPresentBehavior.FIRST_PRESENT.resultIndex(comparator, e, list, n) - 1;
            }
        }, 
        LAST_PRESENT {
            @Override
            public <E> int resultIndex(final Comparator<? super E> comparator, final E e, final List<? extends E> list, int i) {
                int n = list.size() - 1;
                while (i < n) {
                    final int n2 = i + n + 1 >>> 1;
                    if (comparator.compare((E)list.get(n2), e) > 0) {
                        n = n2 - 1;
                    }
                    else {
                        i = n2;
                    }
                }
                return i;
            }
        };
        
        private static /* synthetic */ KeyPresentBehavior[] $values() {
            return new KeyPresentBehavior[] { KeyPresentBehavior.ANY_PRESENT, KeyPresentBehavior.LAST_PRESENT, KeyPresentBehavior.FIRST_PRESENT, KeyPresentBehavior.FIRST_AFTER, KeyPresentBehavior.LAST_BEFORE };
        }
        
        static {
            $VALUES = $values();
        }
        
        public abstract <E> int resultIndex(final Comparator<? super E> p0, final E p1, final List<? extends E> p2, final int p3);
    }
}

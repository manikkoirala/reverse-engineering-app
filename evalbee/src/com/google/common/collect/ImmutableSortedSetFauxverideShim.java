// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

abstract class ImmutableSortedSetFauxverideShim<E> extends ImmutableSet<E>
{
    public ImmutableSortedSetFauxverideShim() {
    }
    
    @Deprecated
    public static <E> ImmutableSortedSet.a builder() {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    public static <E> ImmutableSortedSet.a builderWithExpectedSize(final int n) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    public static <E> ImmutableSortedSet<E> copyOf(final E[] array) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    public static <E> ImmutableSortedSet<E> of(final E e) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    public static <E> ImmutableSortedSet<E> of(final E e, final E e2) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    public static <E> ImmutableSortedSet<E> of(final E e, final E e2, final E e3) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    public static <E> ImmutableSortedSet<E> of(final E e, final E e2, final E e3, final E e4) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    public static <E> ImmutableSortedSet<E> of(final E e, final E e2, final E e3, final E e4, final E e5) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    public static <E> ImmutableSortedSet<E> of(final E e, final E e2, final E e3, final E e4, final E e5, final E e6, final E... array) {
        throw new UnsupportedOperationException();
    }
}

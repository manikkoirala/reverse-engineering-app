// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.SortedSet;
import java.util.NavigableSet;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Comparator;

public abstract class ContiguousSet<C extends Comparable> extends ImmutableSortedSet<C>
{
    final DiscreteDomain domain;
    
    public ContiguousSet(final DiscreteDomain domain) {
        super(Ordering.natural());
        this.domain = domain;
    }
    
    @Deprecated
    public static <E> a builder() {
        throw new UnsupportedOperationException();
    }
    
    public static ContiguousSet<Integer> closed(final int i, final int j) {
        return create((Range<Integer>)Range.closed(i, (C)j), DiscreteDomain.integers());
    }
    
    public static ContiguousSet<Long> closed(final long l, final long i) {
        return create((Range<Long>)Range.closed(l, (C)i), DiscreteDomain.longs());
    }
    
    public static ContiguousSet<Integer> closedOpen(final int i, final int j) {
        return create((Range<Integer>)Range.closedOpen(i, (C)j), DiscreteDomain.integers());
    }
    
    public static ContiguousSet<Long> closedOpen(final long l, final long i) {
        return create((Range<Long>)Range.closedOpen(l, (C)i), DiscreteDomain.longs());
    }
    
    public static <C extends Comparable> ContiguousSet<C> create(final Range<C> range, final DiscreteDomain discreteDomain) {
        i71.r(range);
        i71.r(discreteDomain);
        try {
            Range<C> intersection;
            if (!range.hasLowerBound()) {
                intersection = range.intersection(Range.atLeast((C)discreteDomain.minValue()));
            }
            else {
                intersection = range;
            }
            Range<C> intersection2 = intersection;
            if (!range.hasUpperBound()) {
                intersection2 = intersection.intersection(Range.atMost((C)discreteDomain.maxValue()));
            }
            final boolean empty = intersection2.isEmpty();
            boolean b = true;
            if (!empty) {
                final Comparable leastValueAbove = range.lowerBound.leastValueAbove(discreteDomain);
                Objects.requireNonNull(leastValueAbove);
                final Comparable comparable = leastValueAbove;
                final Comparable greatestValueBelow = range.upperBound.greatestValueBelow(discreteDomain);
                Objects.requireNonNull(greatestValueBelow);
                if (Range.compareOrThrow(comparable, greatestValueBelow) <= 0) {
                    b = false;
                }
            }
            ro1 ro1;
            if (b) {
                ro1 = new EmptyContiguousSet<Object>(discreteDomain);
            }
            else {
                ro1 = new RegularContiguousSet<Object>(intersection2, discreteDomain);
            }
            return (ContiguousSet<C>)ro1;
        }
        catch (final NoSuchElementException cause) {
            throw new IllegalArgumentException(cause);
        }
    }
    
    @Override
    public ImmutableSortedSet<C> createDescendingSet() {
        return new DescendingImmutableSortedSet<C>(this);
    }
    
    @Override
    public ContiguousSet<C> headSet(final C c) {
        return this.headSetImpl((C)i71.r(c), false);
    }
    
    @Override
    public ContiguousSet<C> headSet(final C c, final boolean b) {
        return this.headSetImpl((C)i71.r(c), b);
    }
    
    @Override
    public abstract ContiguousSet<C> headSetImpl(final C p0, final boolean p1);
    
    public abstract ContiguousSet<C> intersection(final ContiguousSet<C> p0);
    
    public abstract Range<C> range();
    
    public abstract Range<C> range(final BoundType p0, final BoundType p1);
    
    @Override
    public ContiguousSet<C> subSet(final C c, final C c2) {
        i71.r(c);
        i71.r(c2);
        i71.d(this.comparator().compare((Object)c, (Object)c2) <= 0);
        return this.subSetImpl(c, true, c2, false);
    }
    
    @Override
    public ContiguousSet<C> subSet(final C c, final boolean b, final C c2, final boolean b2) {
        i71.r(c);
        i71.r(c2);
        i71.d(this.comparator().compare((Object)c, (Object)c2) <= 0);
        return this.subSetImpl(c, b, c2, b2);
    }
    
    @Override
    public abstract ContiguousSet<C> subSetImpl(final C p0, final boolean p1, final C p2, final boolean p3);
    
    @Override
    public ContiguousSet<C> tailSet(final C c) {
        return this.tailSetImpl((C)i71.r(c), true);
    }
    
    @Override
    public ContiguousSet<C> tailSet(final C c, final boolean b) {
        return this.tailSetImpl((C)i71.r(c), b);
    }
    
    @Override
    public abstract ContiguousSet<C> tailSetImpl(final C p0, final boolean p1);
    
    @Override
    public String toString() {
        return this.range().toString();
    }
}

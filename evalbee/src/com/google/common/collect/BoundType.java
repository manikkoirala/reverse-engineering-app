// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

public enum BoundType
{
    private static final BoundType[] $VALUES;
    
    CLOSED(true), 
    OPEN(false);
    
    final boolean inclusive;
    
    private static /* synthetic */ BoundType[] $values() {
        return new BoundType[] { BoundType.OPEN, BoundType.CLOSED };
    }
    
    static {
        $VALUES = $values();
    }
    
    private BoundType(final boolean inclusive) {
        this.inclusive = inclusive;
    }
    
    public static BoundType forBoolean(final boolean b) {
        BoundType boundType;
        if (b) {
            boundType = BoundType.CLOSED;
        }
        else {
            boundType = BoundType.OPEN;
        }
        return boundType;
    }
}

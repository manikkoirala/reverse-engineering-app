// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.NavigableSet;
import java.util.TreeSet;
import java.util.SortedSet;
import java.util.Set;
import java.util.NavigableMap;
import java.io.ObjectOutputStream;
import java.util.Map;
import java.util.Collection;
import java.io.ObjectInputStream;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Comparator;

public class TreeMultimap<K, V> extends AbstractSortedKeySortedSetMultimap<K, V>
{
    private static final long serialVersionUID = 0L;
    private transient Comparator<? super K> keyComparator;
    private transient Comparator<? super V> valueComparator;
    
    public TreeMultimap(final Comparator<? super K> comparator, final Comparator<? super V> valueComparator) {
        super(new TreeMap(comparator));
        this.keyComparator = comparator;
        this.valueComparator = valueComparator;
    }
    
    private TreeMultimap(final Comparator<? super K> comparator, final Comparator<? super V> comparator2, final rx0 rx0) {
        this(comparator, comparator2);
        this.putAll(rx0);
    }
    
    public static <K extends Comparable, V extends Comparable> TreeMultimap<K, V> create() {
        return new TreeMultimap<K, V>(Ordering.natural(), Ordering.natural());
    }
    
    public static <K, V> TreeMultimap<K, V> create(final Comparator<? super K> comparator, final Comparator<? super V> comparator2) {
        return new TreeMultimap<K, V>((Comparator<? super K>)i71.r(comparator), (Comparator<? super V>)i71.r(comparator2));
    }
    
    public static <K extends Comparable, V extends Comparable> TreeMultimap<K, V> create(final rx0 rx0) {
        return new TreeMultimap<K, V>(Ordering.natural(), Ordering.natural(), rx0);
    }
    
    private void readObject(final ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        this.keyComparator = (Comparator)i71.r(objectInputStream.readObject());
        this.valueComparator = (Comparator)i71.r(objectInputStream.readObject());
        this.setMap(new TreeMap<K, Collection<V>>(this.keyComparator));
        com.google.common.collect.n.d(this, objectInputStream);
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(this.keyComparator());
        objectOutputStream.writeObject(this.valueComparator());
        com.google.common.collect.n.j(this, objectOutputStream);
    }
    
    @Override
    public NavigableMap<K, Collection<V>> asMap() {
        return (NavigableMap)super.asMap();
    }
    
    @Override
    public Map<K, Collection<V>> createAsMap() {
        return this.createMaybeNavigableAsMap();
    }
    
    @Override
    public Collection<V> createCollection(final K k) {
        if (k == null) {
            this.keyComparator().compare((Object)k, (Object)k);
        }
        return super.createCollection(k);
    }
    
    @Override
    public SortedSet<V> createCollection() {
        return new TreeSet<V>(this.valueComparator);
    }
    
    @Override
    public NavigableSet<V> get(final K k) {
        return (NavigableSet)super.get(k);
    }
    
    @Deprecated
    public Comparator<? super K> keyComparator() {
        return this.keyComparator;
    }
    
    @Override
    public NavigableSet<K> keySet() {
        return (NavigableSet)super.keySet();
    }
    
    @Override
    public Comparator<? super V> valueComparator() {
        return this.valueComparator;
    }
}

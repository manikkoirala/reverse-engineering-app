// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.SortedSet;
import java.util.Iterator;
import java.util.Set;
import java.util.NavigableSet;
import java.util.Comparator;

public abstract class d extends b implements o
{
    final Comparator<Object> comparator;
    private transient o descendingMultiset;
    
    public d() {
        this(Ordering.natural());
    }
    
    public d(final Comparator comparator) {
        this.comparator = (Comparator)i71.r(comparator);
    }
    
    @Override
    public Comparator comparator() {
        return this.comparator;
    }
    
    public o createDescendingMultiset() {
        return new a();
    }
    
    @Override
    public NavigableSet<Object> createElementSet() {
        return new p.b(this);
    }
    
    public abstract Iterator descendingEntryIterator();
    
    Iterator<Object> descendingIterator() {
        return Multisets.i(this.descendingMultiset());
    }
    
    @Override
    public o descendingMultiset() {
        o descendingMultiset;
        if ((descendingMultiset = this.descendingMultiset) == null) {
            descendingMultiset = this.createDescendingMultiset();
            this.descendingMultiset = descendingMultiset;
        }
        return descendingMultiset;
    }
    
    @Override
    public NavigableSet elementSet() {
        return (NavigableSet)super.elementSet();
    }
    
    @Override
    public j.a firstEntry() {
        final Iterator entryIterator = this.entryIterator();
        j.a a;
        if (entryIterator.hasNext()) {
            a = entryIterator.next();
        }
        else {
            a = null;
        }
        return a;
    }
    
    @Override
    public j.a lastEntry() {
        final Iterator descendingEntryIterator = this.descendingEntryIterator();
        j.a a;
        if (descendingEntryIterator.hasNext()) {
            a = descendingEntryIterator.next();
        }
        else {
            a = null;
        }
        return a;
    }
    
    @Override
    public j.a pollFirstEntry() {
        final Iterator entryIterator = this.entryIterator();
        if (entryIterator.hasNext()) {
            final j.a a = entryIterator.next();
            final j.a g = Multisets.g(a.getElement(), a.getCount());
            entryIterator.remove();
            return g;
        }
        return null;
    }
    
    @Override
    public j.a pollLastEntry() {
        final Iterator descendingEntryIterator = this.descendingEntryIterator();
        if (descendingEntryIterator.hasNext()) {
            final j.a a = descendingEntryIterator.next();
            final j.a g = Multisets.g(a.getElement(), a.getCount());
            descendingEntryIterator.remove();
            return g;
        }
        return null;
    }
    
    @Override
    public o subMultiset(final Object o, final BoundType boundType, final Object o2, final BoundType boundType2) {
        i71.r(boundType);
        i71.r(boundType2);
        return this.tailMultiset(o, boundType).headMultiset(o2, boundType2);
    }
    
    public class a extends g
    {
        public final d d;
        
        public a(final d d) {
            this.d = d;
        }
        
        @Override
        public Iterator c() {
            return this.d.descendingEntryIterator();
        }
        
        @Override
        public o g() {
            return this.d;
        }
        
        @Override
        public Iterator iterator() {
            return this.d.descendingIterator();
        }
    }
}

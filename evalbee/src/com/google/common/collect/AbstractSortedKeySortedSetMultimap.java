// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Comparator;
import java.util.SortedSet;
import java.util.Set;
import java.util.Map;
import java.util.Collection;
import java.util.SortedMap;

abstract class AbstractSortedKeySortedSetMultimap<K, V> extends AbstractSortedSetMultimap<K, V>
{
    public AbstractSortedKeySortedSetMultimap(final SortedMap<K, Collection<V>> sortedMap) {
        super(sortedMap);
    }
    
    @Override
    public SortedMap<K, Collection<V>> asMap() {
        return (SortedMap)super.asMap();
    }
    
    @Override
    public SortedMap<K, Collection<V>> backingMap() {
        return (SortedMap)super.backingMap();
    }
    
    @Override
    public Set<K> createKeySet() {
        return this.createMaybeNavigableKeySet();
    }
    
    @Override
    public SortedSet<K> keySet() {
        return (SortedSet)super.keySet();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.SortedSet;
import java.util.Set;
import java.util.NavigableSet;

final class DescendingImmutableSortedMultiset<E> extends ImmutableSortedMultiset<E>
{
    private final transient ImmutableSortedMultiset<E> forward;
    
    public DescendingImmutableSortedMultiset(final ImmutableSortedMultiset<E> forward) {
        this.forward = forward;
    }
    
    @Override
    public int count(final Object o) {
        return this.forward.count(o);
    }
    
    @Override
    public ImmutableSortedMultiset<E> descendingMultiset() {
        return this.forward;
    }
    
    @Override
    public ImmutableSortedSet<E> elementSet() {
        return this.forward.elementSet().descendingSet();
    }
    
    @Override
    public j.a firstEntry() {
        return this.forward.lastEntry();
    }
    
    @Override
    public j.a getEntry(final int n) {
        return this.forward.entrySet().asList().reverse().get(n);
    }
    
    @Override
    public ImmutableSortedMultiset<E> headMultiset(final E e, final BoundType boundType) {
        return this.forward.tailMultiset(e, boundType).descendingMultiset();
    }
    
    @Override
    public boolean isPartialView() {
        return this.forward.isPartialView();
    }
    
    @Override
    public j.a lastEntry() {
        return this.forward.firstEntry();
    }
    
    @Override
    public int size() {
        return this.forward.size();
    }
    
    @Override
    public ImmutableSortedMultiset<E> tailMultiset(final E e, final BoundType boundType) {
        return this.forward.headMultiset(e, boundType).descendingMultiset();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.List;

class Sets$CartesianSet$1 extends ImmutableList<List<Object>>
{
    final ImmutableList val$axes;
    
    public Sets$CartesianSet$1(final ImmutableList val$axes) {
        this.val$axes = val$axes;
    }
    
    @Override
    public List<Object> get(final int n) {
        return ((ImmutableSet)this.val$axes.get(n)).asList();
    }
    
    @Override
    public boolean isPartialView() {
        return true;
    }
    
    @Override
    public int size() {
        return this.val$axes.size();
    }
}

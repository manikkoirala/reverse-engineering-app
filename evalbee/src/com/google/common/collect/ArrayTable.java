// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.Iterator;
import java.util.Collection;
import java.io.Serializable;

public final class ArrayTable<R, C, V> extends com.google.common.collect.e implements Serializable
{
    private static final long serialVersionUID = 0L;
    private final V[][] array;
    private final ImmutableMap<C, Integer> columnKeyToIndex;
    private final ImmutableList<C> columnList;
    private transient f columnMap;
    private final ImmutableMap<R, Integer> rowKeyToIndex;
    private final ImmutableList<R> rowList;
    private transient h rowMap;
    
    private ArrayTable(final ArrayTable<R, C, V> arrayTable) {
        final ImmutableList<R> rowList = arrayTable.rowList;
        this.rowList = rowList;
        final ImmutableList<C> columnList = arrayTable.columnList;
        this.columnList = columnList;
        this.rowKeyToIndex = arrayTable.rowKeyToIndex;
        this.columnKeyToIndex = arrayTable.columnKeyToIndex;
        final Object[][] array = new Object[rowList.size()][columnList.size()];
        this.array = (V[][])array;
        for (int i = 0; i < this.rowList.size(); ++i) {
            final V[] array2 = arrayTable.array[i];
            System.arraycopy(array2, 0, array[i], 0, array2.length);
        }
    }
    
    private ArrayTable(final q q) {
        this(q.rowKeySet(), q.columnKeySet());
        this.putAll(q);
    }
    
    private ArrayTable(final Iterable<? extends R> iterable, final Iterable<? extends C> iterable2) {
        final ImmutableList<Object> copy = ImmutableList.copyOf((Iterable<?>)iterable);
        this.rowList = (ImmutableList<R>)copy;
        final ImmutableList<Object> copy2 = ImmutableList.copyOf((Iterable<?>)iterable2);
        this.columnList = (ImmutableList<C>)copy2;
        i71.d(copy.isEmpty() == copy2.isEmpty());
        this.rowKeyToIndex = Maps.k(copy);
        this.columnKeyToIndex = Maps.k(copy2);
        this.array = (V[][])new Object[copy.size()][copy2.size()];
        this.eraseAll();
    }
    
    public static /* synthetic */ ImmutableList access$100(final ArrayTable arrayTable) {
        return arrayTable.columnList;
    }
    
    public static /* synthetic */ ImmutableList access$200(final ArrayTable arrayTable) {
        return arrayTable.rowList;
    }
    
    public static /* synthetic */ ImmutableMap access$300(final ArrayTable arrayTable) {
        return arrayTable.rowKeyToIndex;
    }
    
    public static /* synthetic */ ImmutableMap access$600(final ArrayTable arrayTable) {
        return arrayTable.columnKeyToIndex;
    }
    
    public static <R, C, V> ArrayTable<R, C, V> create(final q q) {
        ArrayTable arrayTable;
        if (q instanceof ArrayTable) {
            arrayTable = new ArrayTable((ArrayTable)q);
        }
        else {
            arrayTable = new ArrayTable(q);
        }
        return arrayTable;
    }
    
    public static <R, C, V> ArrayTable<R, C, V> create(final Iterable<? extends R> iterable, final Iterable<? extends C> iterable2) {
        return new ArrayTable<R, C, V>(iterable, iterable2);
    }
    
    private a getCell(final int n) {
        return new Tables.b(this, n) {
            public final int a = c / ArrayTable.access$100(d).size();
            public final int b = c % ArrayTable.access$100(d).size();
            public final int c;
            public final ArrayTable d;
            
            @Override
            public Object getColumnKey() {
                return ArrayTable.access$100(this.d).get(this.b);
            }
            
            @Override
            public Object getRowKey() {
                return ArrayTable.access$200(this.d).get(this.a);
            }
            
            @Override
            public Object getValue() {
                return this.d.at(this.a, this.b);
            }
        };
    }
    
    private V getValue(final int n) {
        return this.at(n / this.columnList.size(), n % this.columnList.size());
    }
    
    public V at(final int n, final int n2) {
        i71.p(n, this.rowList.size());
        i71.p(n2, this.columnList.size());
        return this.array[n][n2];
    }
    
    public Iterator<a> cellIterator() {
        return new o(this, this.size()) {
            public final ArrayTable c;
            
            public a c(final int n) {
                return this.c.getCell(n);
            }
        };
    }
    
    @Override
    public Set<a> cellSet() {
        return super.cellSet();
    }
    
    @Deprecated
    @Override
    public void clear() {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public Map<R, V> column(final C c) {
        i71.r(c);
        final Integer n = this.columnKeyToIndex.get(c);
        if (n == null) {
            return Collections.emptyMap();
        }
        return new e(n);
    }
    
    public ImmutableList<C> columnKeyList() {
        return this.columnList;
    }
    
    @Override
    public ImmutableSet<C> columnKeySet() {
        return this.columnKeyToIndex.keySet();
    }
    
    @Override
    public Map<C, Map<R, V>> columnMap() {
        f columnMap;
        if ((columnMap = this.columnMap) == null) {
            columnMap = new f(null);
            this.columnMap = columnMap;
        }
        return columnMap;
    }
    
    @Override
    public boolean contains(final Object o, final Object o2) {
        return this.containsRow(o) && this.containsColumn(o2);
    }
    
    @Override
    public boolean containsColumn(final Object o) {
        return this.columnKeyToIndex.containsKey(o);
    }
    
    @Override
    public boolean containsRow(final Object o) {
        return this.rowKeyToIndex.containsKey(o);
    }
    
    @Override
    public boolean containsValue(final Object o) {
        for (final V[] array2 : this.array) {
            for (int length2 = array2.length, j = 0; j < length2; ++j) {
                if (b11.a(o, array2[j])) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public V erase(final Object o, final Object o2) {
        final Integer n = this.rowKeyToIndex.get(o);
        final Integer n2 = this.columnKeyToIndex.get(o2);
        if (n != null && n2 != null) {
            return this.set(n, n2, null);
        }
        return null;
    }
    
    public void eraseAll() {
        final V[][] array = this.array;
        for (int length = array.length, i = 0; i < length; ++i) {
            Arrays.fill(array[i], null);
        }
    }
    
    @Override
    public V get(Object at, final Object o) {
        final Integer n = this.rowKeyToIndex.get(at);
        final Integer n2 = this.columnKeyToIndex.get(o);
        if (n != null && n2 != null) {
            at = this.at(n, n2);
        }
        else {
            at = null;
        }
        return (V)at;
    }
    
    @Override
    public boolean isEmpty() {
        return this.rowList.isEmpty() || this.columnList.isEmpty();
    }
    
    @Override
    public V put(final R r, final C c, final V v) {
        i71.r(r);
        i71.r(c);
        final Integer n = this.rowKeyToIndex.get(r);
        final boolean b = true;
        i71.n(n != null, "Row %s not in %s", r, this.rowList);
        final Integer n2 = this.columnKeyToIndex.get(c);
        i71.n(n2 != null && b, "Column %s not in %s", c, this.columnList);
        return this.set(n, n2, v);
    }
    
    @Override
    public void putAll(final q q) {
        super.putAll(q);
    }
    
    @Deprecated
    @Override
    public V remove(final Object o, final Object o2) {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public Map<C, V> row(final R r) {
        i71.r(r);
        final Integer n = this.rowKeyToIndex.get(r);
        if (n == null) {
            return Collections.emptyMap();
        }
        return new g(n);
    }
    
    public ImmutableList<R> rowKeyList() {
        return this.rowList;
    }
    
    @Override
    public ImmutableSet<R> rowKeySet() {
        return this.rowKeyToIndex.keySet();
    }
    
    @Override
    public Map<R, Map<C, V>> rowMap() {
        h rowMap;
        if ((rowMap = this.rowMap) == null) {
            rowMap = new h(null);
            this.rowMap = rowMap;
        }
        return rowMap;
    }
    
    public V set(final int n, final int n2, final V v) {
        i71.p(n, this.rowList.size());
        i71.p(n2, this.columnList.size());
        final V[] array = this.array[n];
        final V v2 = array[n2];
        array[n2] = v;
        return v2;
    }
    
    @Override
    public int size() {
        return this.rowList.size() * this.columnList.size();
    }
    
    public V[][] toArray(final Class<V> componentType) {
        final Object[][] array = (Object[][])Array.newInstance(componentType, this.rowList.size(), this.columnList.size());
        for (int i = 0; i < this.rowList.size(); ++i) {
            final V[] array2 = this.array[i];
            System.arraycopy(array2, 0, array[i], 0, array2.length);
        }
        return (V[][])array;
    }
    
    @Override
    public Collection<V> values() {
        return super.values();
    }
    
    @Override
    public Iterator<V> valuesIterator() {
        return new o(this, this.size()) {
            public final ArrayTable c;
            
            @Override
            public Object b(final int n) {
                return this.c.getValue(n);
            }
        };
    }
    
    public abstract static class d extends l
    {
        public final ImmutableMap a;
        
        public d(final ImmutableMap a) {
            this.a = a;
        }
        
        public Iterator a() {
            return new o(this, this.size()) {
                public final d c;
                
                public Entry c(final int n) {
                    return this.c.b(n);
                }
            };
        }
        
        public Entry b(final int n) {
            i71.p(n, this.size());
            return new s(this, n) {
                public final int a;
                public final d b;
                
                @Override
                public Object getKey() {
                    return this.b.c(this.a);
                }
                
                @Override
                public Object getValue() {
                    return this.b.e(this.a);
                }
                
                @Override
                public Object setValue(final Object o) {
                    return this.b.f(this.a, o);
                }
            };
        }
        
        public Object c(final int n) {
            return this.a.keySet().asList().get(n);
        }
        
        @Override
        public void clear() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public boolean containsKey(final Object o) {
            return this.a.containsKey(o);
        }
        
        public abstract String d();
        
        public abstract Object e(final int p0);
        
        public abstract Object f(final int p0, final Object p1);
        
        @Override
        public Object get(final Object o) {
            final Integer n = this.a.get(o);
            if (n == null) {
                return null;
            }
            return this.e(n);
        }
        
        @Override
        public boolean isEmpty() {
            return this.a.isEmpty();
        }
        
        @Override
        public Set keySet() {
            return this.a.keySet();
        }
        
        @Override
        public Object put(final Object obj, final Object o) {
            final Integer n = this.a.get(obj);
            if (n != null) {
                return this.f(n, o);
            }
            final String d = this.d();
            final String value = String.valueOf(obj);
            final String value2 = String.valueOf(this.a.keySet());
            final StringBuilder sb = new StringBuilder(String.valueOf(d).length() + 9 + value.length() + value2.length());
            sb.append(d);
            sb.append(" ");
            sb.append(value);
            sb.append(" not in ");
            sb.append(value2);
            throw new IllegalArgumentException(sb.toString());
        }
        
        @Override
        public Object remove(final Object o) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public int size() {
            return this.a.size();
        }
    }
    
    public class e extends d
    {
        public final int b;
        public final ArrayTable c;
        
        public e(final ArrayTable c, final int b) {
            this.c = c;
            super(ArrayTable.access$300(c), null);
            this.b = b;
        }
        
        @Override
        public String d() {
            return "Row";
        }
        
        @Override
        public Object e(final int n) {
            return this.c.at(n, this.b);
        }
        
        @Override
        public Object f(final int n, final Object o) {
            return this.c.set(n, this.b, o);
        }
    }
    
    public class f extends d
    {
        public final ArrayTable b;
        
        public f(final ArrayTable b) {
            this.b = b;
            super(ArrayTable.access$600(b), null);
        }
        
        @Override
        public String d() {
            return "Column";
        }
        
        public Map g(final int n) {
            return this.b.new e(n);
        }
        
        public Map h(final Object o, final Map map) {
            throw new UnsupportedOperationException();
        }
        
        public Map i(final int n, final Map map) {
            throw new UnsupportedOperationException();
        }
    }
    
    public class g extends d
    {
        public final int b;
        public final ArrayTable c;
        
        public g(final ArrayTable c, final int b) {
            this.c = c;
            super(ArrayTable.access$600(c), null);
            this.b = b;
        }
        
        @Override
        public String d() {
            return "Column";
        }
        
        @Override
        public Object e(final int n) {
            return this.c.at(this.b, n);
        }
        
        @Override
        public Object f(final int n, final Object o) {
            return this.c.set(this.b, n, o);
        }
    }
    
    public class h extends d
    {
        public final ArrayTable b;
        
        public h(final ArrayTable b) {
            this.b = b;
            super(ArrayTable.access$300(b), null);
        }
        
        @Override
        public String d() {
            return "Row";
        }
        
        public Map g(final int n) {
            return this.b.new g(n);
        }
        
        public Map h(final Object o, final Map map) {
            throw new UnsupportedOperationException();
        }
        
        public Map i(final int n, final Map map) {
            throw new UnsupportedOperationException();
        }
    }
}

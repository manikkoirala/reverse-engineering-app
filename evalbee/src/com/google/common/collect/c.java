// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Set;
import java.util.SortedMap;
import java.util.NoSuchElementException;
import java.util.NavigableSet;
import java.util.Map;
import java.util.Iterator;
import java.util.NavigableMap;

public abstract class c extends l implements NavigableMap
{
    public abstract Iterator b();
    
    @Override
    public Entry ceilingEntry(final Object o) {
        return (Entry)this.tailMap(o, true).firstEntry();
    }
    
    @Override
    public Object ceilingKey(final Object o) {
        return Maps.n(this.ceilingEntry(o));
    }
    
    @Override
    public NavigableSet descendingKeySet() {
        return this.descendingMap().navigableKeySet();
    }
    
    @Override
    public NavigableMap descendingMap() {
        return new b(null);
    }
    
    @Override
    public Entry firstEntry() {
        return (Entry)Iterators.m(((Maps.l)this).a(), null);
    }
    
    @Override
    public Object firstKey() {
        final Entry firstEntry = this.firstEntry();
        if (firstEntry != null) {
            return firstEntry.getKey();
        }
        throw new NoSuchElementException();
    }
    
    @Override
    public Entry floorEntry(final Object o) {
        return (Entry)this.headMap(o, true).lastEntry();
    }
    
    @Override
    public Object floorKey(final Object o) {
        return Maps.n(this.floorEntry(o));
    }
    
    @Override
    public SortedMap headMap(final Object o) {
        return this.headMap(o, false);
    }
    
    @Override
    public Entry higherEntry(final Object o) {
        return (Entry)this.tailMap(o, false).firstEntry();
    }
    
    @Override
    public Object higherKey(final Object o) {
        return Maps.n(this.higherEntry(o));
    }
    
    @Override
    public Set keySet() {
        return this.navigableKeySet();
    }
    
    @Override
    public Entry lastEntry() {
        return (Entry)Iterators.m(this.b(), null);
    }
    
    @Override
    public Object lastKey() {
        final Entry lastEntry = this.lastEntry();
        if (lastEntry != null) {
            return lastEntry.getKey();
        }
        throw new NoSuchElementException();
    }
    
    @Override
    public Entry lowerEntry(final Object o) {
        return (Entry)this.headMap(o, false).lastEntry();
    }
    
    @Override
    public Object lowerKey(final Object o) {
        return Maps.n(this.lowerEntry(o));
    }
    
    @Override
    public NavigableSet navigableKeySet() {
        return new Maps.n(this);
    }
    
    @Override
    public Entry pollFirstEntry() {
        return (Entry)Iterators.q(((Maps.l)this).a());
    }
    
    @Override
    public Entry pollLastEntry() {
        return (Entry)Iterators.q(this.b());
    }
    
    @Override
    public SortedMap subMap(final Object o, final Object o2) {
        return this.subMap(o, true, o2, false);
    }
    
    @Override
    public SortedMap tailMap(final Object o) {
        return this.tailMap(o, true);
    }
    
    public final class b extends i
    {
        public final c d;
        
        public b(final c d) {
            this.d = d;
        }
        
        @Override
        public Iterator c() {
            return this.d.b();
        }
        
        @Override
        public NavigableMap g() {
            return this.d;
        }
    }
}

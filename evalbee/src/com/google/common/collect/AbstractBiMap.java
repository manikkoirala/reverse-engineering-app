// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.Map;
import java.io.Serializable;

abstract class AbstractBiMap<K, V> extends x70 implements ub, Serializable
{
    private static final long serialVersionUID = 0L;
    private transient Map<K, V> delegate;
    private transient Set<Entry<K, V>> entrySet;
    transient AbstractBiMap<V, K> inverse;
    private transient Set<K> keySet;
    private transient Set<V> valueSet;
    
    private AbstractBiMap(final Map<K, V> delegate, final AbstractBiMap<V, K> inverse) {
        this.delegate = delegate;
        this.inverse = inverse;
    }
    
    public AbstractBiMap(final Map<K, V> map, final Map<V, K> map2) {
        this.setDelegates(map, map2);
    }
    
    public static /* synthetic */ Map access$100(final AbstractBiMap abstractBiMap) {
        return abstractBiMap.delegate;
    }
    
    private V putInBothMaps(final K k, final V v, final boolean b) {
        this.checkKey(k);
        this.checkValue(v);
        final boolean containsKey = this.containsKey(k);
        if (containsKey && b11.a(v, this.get(k))) {
            return v;
        }
        if (b) {
            this.inverse().remove(v);
        }
        else {
            i71.m(this.containsValue(v) ^ true, "value already present: %s", v);
        }
        final V put = this.delegate.put(k, v);
        this.updateInverseMap(k, containsKey, put, v);
        return put;
    }
    
    private V removeFromBothMaps(Object a) {
        a = k01.a(this.delegate.remove(a));
        this.removeFromInverseMap(a);
        return (V)a;
    }
    
    private void removeFromInverseMap(final V v) {
        this.inverse.delegate.remove(v);
    }
    
    private void updateInverseMap(final K k, final boolean b, final V v, final V v2) {
        if (b) {
            this.removeFromInverseMap(k01.a(v));
        }
        this.inverse.delegate.put((K)v2, (V)k);
    }
    
    public K checkKey(final K k) {
        return k;
    }
    
    public V checkValue(final V v) {
        return v;
    }
    
    @Override
    public void clear() {
        this.delegate.clear();
        this.inverse.delegate.clear();
    }
    
    @Override
    public boolean containsValue(final Object o) {
        return this.inverse.containsKey(o);
    }
    
    @Override
    public Map<K, V> delegate() {
        return this.delegate;
    }
    
    @Override
    public Set<Entry<K, V>> entrySet() {
        Set<Entry<K, V>> entrySet;
        if ((entrySet = this.entrySet) == null) {
            entrySet = new c(null);
            this.entrySet = entrySet;
        }
        return entrySet;
    }
    
    public Iterator<Entry<K, V>> entrySetIterator() {
        return new Iterator(this, this.delegate.entrySet().iterator()) {
            public Entry a;
            public final Iterator b;
            public final AbstractBiMap c;
            
            public Entry b() {
                final Entry a = (Entry)this.b.next();
                this.a = a;
                return this.c.new b(a);
            }
            
            @Override
            public boolean hasNext() {
                return this.b.hasNext();
            }
            
            @Override
            public void remove() {
                final Entry a = this.a;
                if (a != null) {
                    final Object value = a.getValue();
                    this.b.remove();
                    this.c.removeFromInverseMap(value);
                    this.a = null;
                    return;
                }
                throw new IllegalStateException("no calls to next() since the last call to remove()");
            }
        };
    }
    
    @Override
    public V forcePut(final K k, final V v) {
        return this.putInBothMaps(k, v, true);
    }
    
    @Override
    public ub inverse() {
        return this.inverse;
    }
    
    @Override
    public Set<K> keySet() {
        Set<K> keySet;
        if ((keySet = this.keySet) == null) {
            keySet = new d(null);
            this.keySet = keySet;
        }
        return keySet;
    }
    
    public AbstractBiMap<V, K> makeInverse(final Map<V, K> map) {
        return new Inverse<V, K>(map, this);
    }
    
    @Override
    public V put(final K k, final V v) {
        return this.putInBothMaps(k, v, false);
    }
    
    @Override
    public void putAll(final Map<? extends K, ? extends V> map) {
        for (final Map.Entry<K, V> entry : map.entrySet()) {
            this.put(entry.getKey(), entry.getValue());
        }
    }
    
    @Override
    public V remove(Object removeFromBothMaps) {
        if (this.containsKey(removeFromBothMaps)) {
            removeFromBothMaps = this.removeFromBothMaps(removeFromBothMaps);
        }
        else {
            removeFromBothMaps = null;
        }
        return (V)removeFromBothMaps;
    }
    
    public void setDelegates(final Map<K, V> delegate, final Map<V, K> map) {
        final Map<K, V> delegate2 = this.delegate;
        final boolean b = true;
        i71.x(delegate2 == null);
        i71.x(this.inverse == null);
        i71.d(delegate.isEmpty());
        i71.d(map.isEmpty());
        i71.d(delegate != map && b);
        this.delegate = delegate;
        this.inverse = this.makeInverse(map);
    }
    
    public void setInverse(final AbstractBiMap<V, K> inverse) {
        this.inverse = inverse;
    }
    
    @Override
    public Set<V> values() {
        Set<V> valueSet;
        if ((valueSet = this.valueSet) == null) {
            valueSet = new e(null);
            this.valueSet = valueSet;
        }
        return valueSet;
    }
    
    public static class Inverse<K, V> extends AbstractBiMap<K, V>
    {
        private static final long serialVersionUID = 0L;
        
        public Inverse(final Map<K, V> map, final AbstractBiMap<V, K> abstractBiMap) {
            super(map, (AbstractBiMap<Object, Object>)abstractBiMap, null);
        }
        
        private void readObject(final ObjectInputStream objectInputStream) {
            objectInputStream.defaultReadObject();
            this.setInverse((AbstractBiMap<V, K>)objectInputStream.readObject());
        }
        
        private void writeObject(final ObjectOutputStream objectOutputStream) {
            objectOutputStream.defaultWriteObject();
            objectOutputStream.writeObject(this.inverse());
        }
        
        @Override
        public K checkKey(final K k) {
            return super.inverse.checkValue(k);
        }
        
        @Override
        public V checkValue(final V v) {
            return super.inverse.checkKey(v);
        }
        
        public Object readResolve() {
            return this.inverse().inverse();
        }
    }
    
    public class b extends y70
    {
        public final Entry a;
        public final AbstractBiMap b;
        
        public b(final AbstractBiMap b, final Entry a) {
            this.b = b;
            this.a = a;
        }
        
        @Override
        public Entry b() {
            return this.a;
        }
        
        @Override
        public Object setValue(final Object value) {
            this.b.checkValue(value);
            i71.y(this.b.entrySet().contains(this), "entry no longer in map");
            if (b11.a(value, this.getValue())) {
                return value;
            }
            i71.m(this.b.containsValue(value) ^ true, "value already present: %s", value);
            final Object setValue = this.a.setValue(value);
            i71.y(b11.a(value, this.b.get(this.getKey())), "entry no longer in map");
            this.b.updateInverseMap(this.getKey(), true, setValue, value);
            return setValue;
        }
    }
    
    public class c extends c80
    {
        public final Set a;
        public final AbstractBiMap b;
        
        public c(final AbstractBiMap b) {
            this.b = b;
            this.a = AbstractBiMap.access$100(b).entrySet();
        }
        
        @Override
        public void clear() {
            this.b.clear();
        }
        
        @Override
        public boolean contains(final Object o) {
            return Maps.f(this.delegate(), o);
        }
        
        @Override
        public boolean containsAll(final Collection collection) {
            return this.standardContainsAll(collection);
        }
        
        @Override
        public Set delegate() {
            return this.a;
        }
        
        @Override
        public Iterator iterator() {
            return this.b.entrySetIterator();
        }
        
        @Override
        public boolean remove(final Object o) {
            if (this.a.contains(o) && o instanceof Entry) {
                final Entry entry = (Entry)o;
                AbstractBiMap.access$100(this.b.inverse).remove(entry.getValue());
                this.a.remove(entry);
                return true;
            }
            return false;
        }
        
        @Override
        public boolean removeAll(final Collection collection) {
            return this.standardRemoveAll(collection);
        }
        
        @Override
        public boolean retainAll(final Collection collection) {
            return this.standardRetainAll(collection);
        }
        
        @Override
        public Object[] toArray() {
            return this.standardToArray();
        }
        
        @Override
        public Object[] toArray(final Object[] array) {
            return this.standardToArray(array);
        }
    }
    
    public class d extends c80
    {
        public final AbstractBiMap a;
        
        public d(final AbstractBiMap a) {
            this.a = a;
        }
        
        @Override
        public void clear() {
            this.a.clear();
        }
        
        @Override
        public Set delegate() {
            return AbstractBiMap.access$100(this.a).keySet();
        }
        
        @Override
        public Iterator iterator() {
            return Maps.m(this.a.entrySet().iterator());
        }
        
        @Override
        public boolean remove(final Object o) {
            if (!this.contains(o)) {
                return false;
            }
            this.a.removeFromBothMaps(o);
            return true;
        }
        
        @Override
        public boolean removeAll(final Collection collection) {
            return this.standardRemoveAll(collection);
        }
        
        @Override
        public boolean retainAll(final Collection collection) {
            return this.standardRetainAll(collection);
        }
    }
    
    public class e extends c80
    {
        public final Set a;
        public final AbstractBiMap b;
        
        public e(final AbstractBiMap b) {
            this.b = b;
            this.a = b.inverse.keySet();
        }
        
        @Override
        public Set delegate() {
            return this.a;
        }
        
        @Override
        public Iterator iterator() {
            return Maps.K(this.b.entrySet().iterator());
        }
        
        @Override
        public Object[] toArray() {
            return this.standardToArray();
        }
        
        @Override
        public Object[] toArray(final Object[] array) {
            return this.standardToArray(array);
        }
        
        @Override
        public String toString() {
            return this.standardToString();
        }
    }
}

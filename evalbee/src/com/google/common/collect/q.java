// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public interface q
{
    Set cellSet();
    
    void clear();
    
    Map column(final Object p0);
    
    Set columnKeySet();
    
    Map columnMap();
    
    boolean contains(final Object p0, final Object p1);
    
    boolean containsColumn(final Object p0);
    
    boolean containsRow(final Object p0);
    
    boolean containsValue(final Object p0);
    
    boolean equals(final Object p0);
    
    Object get(final Object p0, final Object p1);
    
    int hashCode();
    
    boolean isEmpty();
    
    Object put(final Object p0, final Object p1, final Object p2);
    
    void putAll(final q p0);
    
    Object remove(final Object p0, final Object p1);
    
    Map row(final Object p0);
    
    Set rowKeySet();
    
    Map rowMap();
    
    int size();
    
    Collection values();
    
    public interface a
    {
        Object getColumnKey();
        
        Object getRowKey();
        
        Object getValue();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Collection;
import java.util.Set;
import java.util.Map;

class SingletonImmutableTable<R, C, V> extends ImmutableTable<R, C, V>
{
    final C singleColumnKey;
    final R singleRowKey;
    final V singleValue;
    
    public SingletonImmutableTable(final q.a a) {
        this(a.getRowKey(), a.getColumnKey(), a.getValue());
    }
    
    public SingletonImmutableTable(final R r, final C c, final V v) {
        this.singleRowKey = (R)i71.r(r);
        this.singleColumnKey = (C)i71.r(c);
        this.singleValue = (V)i71.r(v);
    }
    
    @Override
    public ImmutableMap<R, V> column(final C c) {
        i71.r(c);
        Map<K, V> map;
        if (this.containsColumn(c)) {
            map = (Map<K, V>)ImmutableMap.of(this.singleRowKey, this.singleValue);
        }
        else {
            map = (Map<K, V>)ImmutableMap.of();
        }
        return (ImmutableMap<R, V>)map;
    }
    
    @Override
    public ImmutableMap<C, Map<R, V>> columnMap() {
        return (ImmutableMap<C, Map<R, V>>)ImmutableMap.of(this.singleColumnKey, ImmutableMap.of(this.singleRowKey, this.singleValue));
    }
    
    @Override
    public ImmutableSet<q.a> createCellSet() {
        return ImmutableSet.of(ImmutableTable.cellOf(this.singleRowKey, this.singleColumnKey, this.singleValue));
    }
    
    @Override
    public SerializedForm createSerializedForm() {
        return SerializedForm.create(this, new int[] { 0 }, new int[] { 0 });
    }
    
    @Override
    public ImmutableCollection<V> createValues() {
        return ImmutableSet.of(this.singleValue);
    }
    
    @Override
    public ImmutableMap<R, Map<C, V>> rowMap() {
        return (ImmutableMap<R, Map<C, V>>)ImmutableMap.of(this.singleRowKey, ImmutableMap.of(this.singleColumnKey, this.singleValue));
    }
    
    @Override
    public int size() {
        return 1;
    }
}

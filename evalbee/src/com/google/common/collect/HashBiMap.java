// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.NoSuchElementException;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.AbstractSet;
import java.util.Collection;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.io.Serializable;
import java.util.AbstractMap;

public final class HashBiMap<K, V> extends AbstractMap<K, V> implements ub, Serializable
{
    private static final int ABSENT = -1;
    private static final int ENDPOINT = -2;
    private transient Set<Entry<K, V>> entrySet;
    private transient int firstInInsertionOrder;
    private transient int[] hashTableKToV;
    private transient int[] hashTableVToK;
    private transient ub inverse;
    private transient Set<K> keySet;
    transient K[] keys;
    private transient int lastInInsertionOrder;
    transient int modCount;
    private transient int[] nextInBucketKToV;
    private transient int[] nextInBucketVToK;
    private transient int[] nextInInsertionOrder;
    private transient int[] prevInInsertionOrder;
    transient int size;
    private transient Set<V> valueSet;
    transient V[] values;
    
    private HashBiMap(final int n) {
        this.init(n);
    }
    
    public static /* synthetic */ int access$000(final HashBiMap hashBiMap) {
        return hashBiMap.firstInInsertionOrder;
    }
    
    public static /* synthetic */ int[] access$100(final HashBiMap hashBiMap) {
        return hashBiMap.nextInInsertionOrder;
    }
    
    public static /* synthetic */ ub access$302(final HashBiMap hashBiMap, final ub inverse) {
        return hashBiMap.inverse = inverse;
    }
    
    private int bucket(final int n) {
        return n & this.hashTableKToV.length - 1;
    }
    
    public static <K, V> HashBiMap<K, V> create() {
        return create(16);
    }
    
    public static <K, V> HashBiMap<K, V> create(final int n) {
        return new HashBiMap<K, V>(n);
    }
    
    public static <K, V> HashBiMap<K, V> create(final Map<? extends K, ? extends V> m) {
        final HashBiMap<Object, Object> create = create(m.size());
        create.putAll(m);
        return (HashBiMap<K, V>)create;
    }
    
    private static int[] createFilledWithAbsent(final int n) {
        final int[] a = new int[n];
        Arrays.fill(a, -1);
        return a;
    }
    
    private void deleteFromTableKToV(final int n, int i) {
        i71.d(n != -1);
        i = this.bucket(i);
        final int[] hashTableKToV = this.hashTableKToV;
        int n2 = hashTableKToV[i];
        if (n2 == n) {
            final int[] nextInBucketKToV = this.nextInBucketKToV;
            hashTableKToV[i] = nextInBucketKToV[n];
            nextInBucketKToV[n] = -1;
            return;
        }
        int[] nextInBucketKToV2;
        int n3;
        for (i = this.nextInBucketKToV[n2]; i != -1; i = n3) {
            if (i == n) {
                nextInBucketKToV2 = this.nextInBucketKToV;
                nextInBucketKToV2[n2] = nextInBucketKToV2[n];
                nextInBucketKToV2[n] = -1;
                return;
            }
            n3 = this.nextInBucketKToV[i];
            n2 = i;
        }
        final String value = String.valueOf(this.keys[n]);
        final StringBuilder sb = new StringBuilder(value.length() + 32);
        sb.append("Expected to find entry with key ");
        sb.append(value);
        throw new AssertionError((Object)sb.toString());
    }
    
    private void deleteFromTableVToK(final int n, int i) {
        i71.d(n != -1);
        i = this.bucket(i);
        final int[] hashTableVToK = this.hashTableVToK;
        int n2 = hashTableVToK[i];
        if (n2 == n) {
            final int[] nextInBucketVToK = this.nextInBucketVToK;
            hashTableVToK[i] = nextInBucketVToK[n];
            nextInBucketVToK[n] = -1;
            return;
        }
        int[] nextInBucketVToK2;
        int n3;
        for (i = this.nextInBucketVToK[n2]; i != -1; i = n3) {
            if (i == n) {
                nextInBucketVToK2 = this.nextInBucketVToK;
                nextInBucketVToK2[n2] = nextInBucketVToK2[n];
                nextInBucketVToK2[n] = -1;
                return;
            }
            n3 = this.nextInBucketVToK[i];
            n2 = i;
        }
        final String value = String.valueOf(this.values[n]);
        final StringBuilder sb = new StringBuilder(value.length() + 34);
        sb.append("Expected to find entry with value ");
        sb.append(value);
        throw new AssertionError((Object)sb.toString());
    }
    
    private void ensureCapacity(int i) {
        final int[] nextInBucketKToV = this.nextInBucketKToV;
        if (nextInBucketKToV.length < i) {
            final int e = ImmutableCollection.b.e(nextInBucketKToV.length, i);
            this.keys = Arrays.copyOf(this.keys, e);
            this.values = Arrays.copyOf(this.values, e);
            this.nextInBucketKToV = expandAndFillWithAbsent(this.nextInBucketKToV, e);
            this.nextInBucketVToK = expandAndFillWithAbsent(this.nextInBucketVToK, e);
            this.prevInInsertionOrder = expandAndFillWithAbsent(this.prevInInsertionOrder, e);
            this.nextInInsertionOrder = expandAndFillWithAbsent(this.nextInInsertionOrder, e);
        }
        if (this.hashTableKToV.length < i) {
            i = rc0.a(i, 1.0);
            this.hashTableKToV = createFilledWithAbsent(i);
            this.hashTableVToK = createFilledWithAbsent(i);
            int bucket;
            int[] nextInBucketKToV2;
            int[] hashTableKToV;
            int bucket2;
            int[] nextInBucketVToK;
            int[] hashTableVToK;
            for (i = 0; i < this.size; ++i) {
                bucket = this.bucket(rc0.d(this.keys[i]));
                nextInBucketKToV2 = this.nextInBucketKToV;
                hashTableKToV = this.hashTableKToV;
                nextInBucketKToV2[i] = hashTableKToV[bucket];
                hashTableKToV[bucket] = i;
                bucket2 = this.bucket(rc0.d(this.values[i]));
                nextInBucketVToK = this.nextInBucketVToK;
                hashTableVToK = this.hashTableVToK;
                nextInBucketVToK[i] = hashTableVToK[bucket2];
                hashTableVToK[bucket2] = i;
            }
        }
    }
    
    private static int[] expandAndFillWithAbsent(int[] copy, final int n) {
        final int length = copy.length;
        copy = Arrays.copyOf(copy, n);
        Arrays.fill(copy, length, n, -1);
        return copy;
    }
    
    private void insertIntoTableKToV(final int n, int bucket) {
        i71.d(n != -1);
        bucket = this.bucket(bucket);
        final int[] nextInBucketKToV = this.nextInBucketKToV;
        final int[] hashTableKToV = this.hashTableKToV;
        nextInBucketKToV[n] = hashTableKToV[bucket];
        hashTableKToV[bucket] = n;
    }
    
    private void insertIntoTableVToK(final int n, int bucket) {
        i71.d(n != -1);
        bucket = this.bucket(bucket);
        final int[] nextInBucketVToK = this.nextInBucketVToK;
        final int[] hashTableVToK = this.hashTableVToK;
        nextInBucketVToK[n] = hashTableVToK[bucket];
        hashTableVToK[bucket] = n;
    }
    
    private void moveEntryToIndex(final int n, final int n2) {
        if (n == n2) {
            return;
        }
        final int n3 = this.prevInInsertionOrder[n];
        final int n4 = this.nextInInsertionOrder[n];
        this.setSucceeds(n3, n2);
        this.setSucceeds(n2, n4);
        final K[] keys = this.keys;
        final K k = keys[n];
        final V[] values = this.values;
        final V v = values[n];
        keys[n2] = k;
        values[n2] = v;
        final int bucket = this.bucket(rc0.d(k));
        final int[] hashTableKToV = this.hashTableKToV;
        int n5 = hashTableKToV[bucket];
        if (n5 == n) {
            hashTableKToV[bucket] = n2;
        }
        else {
            int n6 = this.nextInBucketKToV[n5];
            int n7;
            while (true) {
                n7 = n5;
                n5 = n6;
                if (n5 == n) {
                    break;
                }
                n6 = this.nextInBucketKToV[n5];
            }
            this.nextInBucketKToV[n7] = n2;
        }
        final int[] nextInBucketKToV = this.nextInBucketKToV;
        nextInBucketKToV[n2] = nextInBucketKToV[n];
        nextInBucketKToV[n] = -1;
        final int bucket2 = this.bucket(rc0.d(v));
        final int[] hashTableVToK = this.hashTableVToK;
        int n8 = hashTableVToK[bucket2];
        if (n8 == n) {
            hashTableVToK[bucket2] = n2;
        }
        else {
            int n9 = this.nextInBucketVToK[n8];
            int n10;
            while (true) {
                n10 = n8;
                n8 = n9;
                if (n8 == n) {
                    break;
                }
                n9 = this.nextInBucketVToK[n8];
            }
            this.nextInBucketVToK[n10] = n2;
        }
        final int[] nextInBucketVToK = this.nextInBucketVToK;
        nextInBucketVToK[n2] = nextInBucketVToK[n];
        nextInBucketVToK[n] = -1;
    }
    
    private void readObject(final ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        final int h = n.h(objectInputStream);
        this.init(16);
        n.c(this, objectInputStream, h);
    }
    
    private void removeEntry(int size, final int n, final int n2) {
        i71.d(size != -1);
        this.deleteFromTableKToV(size, n);
        this.deleteFromTableVToK(size, n2);
        this.setSucceeds(this.prevInInsertionOrder[size], this.nextInInsertionOrder[size]);
        this.moveEntryToIndex(this.size - 1, size);
        final K[] keys = this.keys;
        size = this.size;
        keys[size - 1] = null;
        this.values[size - 1] = null;
        this.size = size - 1;
        ++this.modCount;
    }
    
    private void replaceKeyInEntry(int n, final K obj, final boolean b) {
        i71.d(n != -1);
        final int d = rc0.d(obj);
        int entryByKey = this.findEntryByKey(obj, d);
        int lastInInsertionOrder = this.lastInInsertionOrder;
        int n4;
        int n5;
        if (entryByKey != -1) {
            if (!b) {
                final String value = String.valueOf(obj);
                final StringBuilder sb = new StringBuilder(value.length() + 28);
                sb.append("Key already present in map: ");
                sb.append(value);
                throw new IllegalArgumentException(sb.toString());
            }
            final int n2 = this.prevInInsertionOrder[entryByKey];
            final int n3 = this.nextInInsertionOrder[entryByKey];
            this.removeEntryKeyHashKnown(entryByKey, d);
            lastInInsertionOrder = n2;
            n4 = n;
            n5 = n3;
            if (n == this.size) {
                n4 = entryByKey;
                lastInInsertionOrder = n2;
                n5 = n3;
            }
        }
        else {
            n5 = -2;
            n4 = n;
        }
        if (lastInInsertionOrder == n4) {
            n = this.prevInInsertionOrder[n4];
        }
        else if ((n = lastInInsertionOrder) == this.size) {
            n = entryByKey;
        }
        if (n5 == n4) {
            entryByKey = this.nextInInsertionOrder[n4];
        }
        else if (n5 != this.size) {
            entryByKey = n5;
        }
        this.setSucceeds(this.prevInInsertionOrder[n4], this.nextInInsertionOrder[n4]);
        this.deleteFromTableKToV(n4, rc0.d(this.keys[n4]));
        this.keys[n4] = obj;
        this.insertIntoTableKToV(n4, rc0.d(obj));
        this.setSucceeds(n, n4);
        this.setSucceeds(n4, entryByKey);
    }
    
    private void replaceValueInEntry(final int n, final V obj, final boolean b) {
        i71.d(n != -1);
        final int d = rc0.d(obj);
        final int entryByValue = this.findEntryByValue(obj, d);
        int n2 = n;
        if (entryByValue != -1) {
            if (!b) {
                final String value = String.valueOf(obj);
                final StringBuilder sb = new StringBuilder(value.length() + 30);
                sb.append("Value already present in map: ");
                sb.append(value);
                throw new IllegalArgumentException(sb.toString());
            }
            this.removeEntryValueHashKnown(entryByValue, d);
            if ((n2 = n) == this.size) {
                n2 = entryByValue;
            }
        }
        this.deleteFromTableVToK(n2, rc0.d(this.values[n2]));
        this.values[n2] = obj;
        this.insertIntoTableVToK(n2, d);
    }
    
    private void setSucceeds(final int lastInInsertionOrder, final int firstInInsertionOrder) {
        if (lastInInsertionOrder == -2) {
            this.firstInInsertionOrder = firstInInsertionOrder;
        }
        else {
            this.nextInInsertionOrder[lastInInsertionOrder] = firstInInsertionOrder;
        }
        if (firstInInsertionOrder == -2) {
            this.lastInInsertionOrder = lastInInsertionOrder;
        }
        else {
            this.prevInInsertionOrder[firstInInsertionOrder] = lastInInsertionOrder;
        }
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        n.i(this, objectOutputStream);
    }
    
    @Override
    public void clear() {
        Arrays.fill(this.keys, 0, this.size, null);
        Arrays.fill(this.values, 0, this.size, null);
        Arrays.fill(this.hashTableKToV, -1);
        Arrays.fill(this.hashTableVToK, -1);
        Arrays.fill(this.nextInBucketKToV, 0, this.size, -1);
        Arrays.fill(this.nextInBucketVToK, 0, this.size, -1);
        Arrays.fill(this.prevInInsertionOrder, 0, this.size, -1);
        Arrays.fill(this.nextInInsertionOrder, 0, this.size, -1);
        this.size = 0;
        this.firstInInsertionOrder = -2;
        this.lastInInsertionOrder = -2;
        ++this.modCount;
    }
    
    @Override
    public boolean containsKey(final Object o) {
        return this.findEntryByKey(o) != -1;
    }
    
    @Override
    public boolean containsValue(final Object o) {
        return this.findEntryByValue(o) != -1;
    }
    
    @Override
    public Set<Entry<K, V>> entrySet() {
        Set<Entry<K, V>> entrySet;
        if ((entrySet = this.entrySet) == null) {
            entrySet = new c();
            this.entrySet = entrySet;
        }
        return entrySet;
    }
    
    public int findEntry(final Object o, int i, final int[] array, final int[] array2, final Object[] array3) {
        for (i = array[this.bucket(i)]; i != -1; i = array2[i]) {
            if (b11.a(array3[i], o)) {
                return i;
            }
        }
        return -1;
    }
    
    public int findEntryByKey(final Object o) {
        return this.findEntryByKey(o, rc0.d(o));
    }
    
    public int findEntryByKey(final Object o, final int n) {
        return this.findEntry(o, n, this.hashTableKToV, this.nextInBucketKToV, this.keys);
    }
    
    public int findEntryByValue(final Object o) {
        return this.findEntryByValue(o, rc0.d(o));
    }
    
    public int findEntryByValue(final Object o, final int n) {
        return this.findEntry(o, n, this.hashTableVToK, this.nextInBucketVToK, this.values);
    }
    
    @Override
    public V forcePut(final K k, final V v) {
        return this.put(k, v, true);
    }
    
    @Override
    public V get(Object o) {
        final int entryByKey = this.findEntryByKey(o);
        if (entryByKey == -1) {
            o = null;
        }
        else {
            o = this.values[entryByKey];
        }
        return (V)o;
    }
    
    public K getInverse(Object o) {
        final int entryByValue = this.findEntryByValue(o);
        if (entryByValue == -1) {
            o = null;
        }
        else {
            o = this.keys[entryByValue];
        }
        return (K)o;
    }
    
    public void init(final int n) {
        hh.b(n, "expectedSize");
        final int a = rc0.a(n, 1.0);
        this.size = 0;
        this.keys = (K[])new Object[n];
        this.values = (V[])new Object[n];
        this.hashTableKToV = createFilledWithAbsent(a);
        this.hashTableVToK = createFilledWithAbsent(a);
        this.nextInBucketKToV = createFilledWithAbsent(n);
        this.nextInBucketVToK = createFilledWithAbsent(n);
        this.firstInInsertionOrder = -2;
        this.lastInInsertionOrder = -2;
        this.prevInInsertionOrder = createFilledWithAbsent(n);
        this.nextInInsertionOrder = createFilledWithAbsent(n);
    }
    
    @Override
    public ub inverse() {
        ub inverse;
        if ((inverse = this.inverse) == null) {
            inverse = new Inverse<Object, Object>(this);
            this.inverse = inverse;
        }
        return inverse;
    }
    
    @Override
    public Set<K> keySet() {
        Set<K> keySet;
        if ((keySet = this.keySet) == null) {
            keySet = new e();
            this.keySet = keySet;
        }
        return keySet;
    }
    
    @Override
    public V put(final K k, final V v) {
        return this.put(k, v, false);
    }
    
    public V put(final K k, final V v, final boolean b) {
        final int d = rc0.d(k);
        final int entryByKey = this.findEntryByKey(k, d);
        if (entryByKey == -1) {
            final int d2 = rc0.d(v);
            final int entryByValue = this.findEntryByValue(v, d2);
            if (b) {
                if (entryByValue != -1) {
                    this.removeEntryValueHashKnown(entryByValue, d2);
                }
            }
            else {
                i71.m(entryByValue == -1, "Value already present: %s", v);
            }
            this.ensureCapacity(this.size + 1);
            final K[] keys = this.keys;
            final int size = this.size;
            keys[size] = k;
            this.values[size] = v;
            this.insertIntoTableKToV(size, d);
            this.insertIntoTableVToK(this.size, d2);
            this.setSucceeds(this.lastInInsertionOrder, this.size);
            this.setSucceeds(this.size, -2);
            ++this.size;
            ++this.modCount;
            return null;
        }
        final V v2 = this.values[entryByKey];
        if (b11.a(v2, v)) {
            return v;
        }
        this.replaceValueInEntry(entryByKey, v, b);
        return v2;
    }
    
    public K putInverse(final V v, final K k, final boolean b) {
        final int d = rc0.d(v);
        final int entryByValue = this.findEntryByValue(v, d);
        if (entryByValue == -1) {
            int lastInInsertionOrder = this.lastInInsertionOrder;
            final int d2 = rc0.d(k);
            final int entryByKey = this.findEntryByKey(k, d2);
            if (b) {
                if (entryByKey != -1) {
                    lastInInsertionOrder = this.prevInInsertionOrder[entryByKey];
                    this.removeEntryKeyHashKnown(entryByKey, d2);
                }
            }
            else {
                i71.m(entryByKey == -1, "Key already present: %s", k);
            }
            this.ensureCapacity(this.size + 1);
            final K[] keys = this.keys;
            final int size = this.size;
            keys[size] = k;
            this.values[size] = v;
            this.insertIntoTableKToV(size, d2);
            this.insertIntoTableVToK(this.size, d);
            int firstInInsertionOrder;
            if (lastInInsertionOrder == -2) {
                firstInInsertionOrder = this.firstInInsertionOrder;
            }
            else {
                firstInInsertionOrder = this.nextInInsertionOrder[lastInInsertionOrder];
            }
            this.setSucceeds(lastInInsertionOrder, this.size);
            this.setSucceeds(this.size, firstInInsertionOrder);
            ++this.size;
            ++this.modCount;
            return null;
        }
        final K i = this.keys[entryByValue];
        if (b11.a(i, k)) {
            return k;
        }
        this.replaceKeyInEntry(entryByValue, k, b);
        return i;
    }
    
    @Override
    public V remove(Object o) {
        final int d = rc0.d(o);
        final int entryByKey = this.findEntryByKey(o, d);
        if (entryByKey == -1) {
            return null;
        }
        o = this.values[entryByKey];
        this.removeEntryKeyHashKnown(entryByKey, d);
        return (V)o;
    }
    
    public void removeEntry(final int n) {
        this.removeEntryKeyHashKnown(n, rc0.d(this.keys[n]));
    }
    
    public void removeEntryKeyHashKnown(final int n, final int n2) {
        this.removeEntry(n, n2, rc0.d(this.values[n]));
    }
    
    public void removeEntryValueHashKnown(final int n, final int n2) {
        this.removeEntry(n, rc0.d(this.keys[n]), n2);
    }
    
    public K removeInverse(Object o) {
        final int d = rc0.d(o);
        final int entryByValue = this.findEntryByValue(o, d);
        if (entryByValue == -1) {
            return null;
        }
        o = this.keys[entryByValue];
        this.removeEntryValueHashKnown(entryByValue, d);
        return (K)o;
    }
    
    @Override
    public int size() {
        return this.size;
    }
    
    @Override
    public Set<V> values() {
        Set<V> valueSet;
        if ((valueSet = this.valueSet) == null) {
            valueSet = new f();
            this.valueSet = valueSet;
        }
        return valueSet;
    }
    
    public static class Inverse<K, V> extends AbstractMap<V, K> implements ub, Serializable
    {
        private final HashBiMap<K, V> forward;
        private transient Set<Entry<V, K>> inverseEntrySet;
        
        public Inverse(final HashBiMap<K, V> forward) {
            this.forward = forward;
        }
        
        private void readObject(final ObjectInputStream objectInputStream) {
            objectInputStream.defaultReadObject();
            HashBiMap.access$302((HashBiMap<Object, Object>)this.forward, this);
        }
        
        @Override
        public void clear() {
            this.forward.clear();
        }
        
        @Override
        public boolean containsKey(final Object o) {
            return this.forward.containsValue(o);
        }
        
        @Override
        public boolean containsValue(final Object o) {
            return this.forward.containsKey(o);
        }
        
        @Override
        public Set<Entry<V, K>> entrySet() {
            Set<Entry<V, K>> inverseEntrySet;
            if ((inverseEntrySet = this.inverseEntrySet) == null) {
                inverseEntrySet = new d(this.forward);
                this.inverseEntrySet = inverseEntrySet;
            }
            return inverseEntrySet;
        }
        
        @Override
        public K forcePut(final V v, final K k) {
            return this.forward.putInverse(v, k, true);
        }
        
        @Override
        public K get(final Object o) {
            return this.forward.getInverse(o);
        }
        
        @Override
        public ub inverse() {
            return this.forward;
        }
        
        @Override
        public Set<V> keySet() {
            return this.forward.values();
        }
        
        @Override
        public K put(final V v, final K k) {
            return this.forward.putInverse(v, k, false);
        }
        
        @Override
        public K remove(final Object o) {
            return this.forward.removeInverse(o);
        }
        
        @Override
        public int size() {
            return this.forward.size;
        }
        
        @Override
        public Set<K> values() {
            return this.forward.keySet();
        }
    }
    
    public final class a extends s
    {
        public final Object a;
        public int b;
        public final HashBiMap c;
        
        public a(final HashBiMap c, final int b) {
            this.c = c;
            this.a = k01.a(c.keys[b]);
            this.b = b;
        }
        
        public void a() {
            final int b = this.b;
            if (b != -1) {
                final HashBiMap c = this.c;
                if (b <= c.size && b11.a(c.keys[b], this.a)) {
                    return;
                }
            }
            this.b = this.c.findEntryByKey(this.a);
        }
        
        @Override
        public Object getKey() {
            return this.a;
        }
        
        @Override
        public Object getValue() {
            this.a();
            final int b = this.b;
            Object o;
            if (b == -1) {
                o = k01.b();
            }
            else {
                o = k01.a(this.c.values[b]);
            }
            return o;
        }
        
        @Override
        public Object setValue(final Object o) {
            this.a();
            final int b = this.b;
            if (b == -1) {
                this.c.put(this.a, o);
                return k01.b();
            }
            final Object a = k01.a(this.c.values[b]);
            if (b11.a(a, o)) {
                return o;
            }
            this.c.replaceValueInEntry(this.b, o, false);
            return a;
        }
    }
    
    public static final class b extends s
    {
        public final HashBiMap a;
        public final Object b;
        public int c;
        
        public b(final HashBiMap a, final int c) {
            this.a = a;
            this.b = k01.a(a.values[c]);
            this.c = c;
        }
        
        public final void a() {
            final int c = this.c;
            if (c != -1) {
                final HashBiMap a = this.a;
                if (c <= a.size && b11.a(this.b, a.values[c])) {
                    return;
                }
            }
            this.c = this.a.findEntryByValue(this.b);
        }
        
        @Override
        public Object getKey() {
            return this.b;
        }
        
        @Override
        public Object getValue() {
            this.a();
            final int c = this.c;
            Object o;
            if (c == -1) {
                o = k01.b();
            }
            else {
                o = k01.a(this.a.keys[c]);
            }
            return o;
        }
        
        @Override
        public Object setValue(final Object o) {
            this.a();
            final int c = this.c;
            if (c == -1) {
                this.a.putInverse(this.b, o, false);
                return k01.b();
            }
            final Object a = k01.a(this.a.keys[c]);
            if (b11.a(a, o)) {
                return o;
            }
            this.a.replaceKeyInEntry(this.c, o, false);
            return a;
        }
    }
    
    public final class c extends g
    {
        public final HashBiMap b;
        
        public c(final HashBiMap b) {
            super(this.b = b);
        }
        
        public Entry b(final int n) {
            return this.b.new a(n);
        }
        
        @Override
        public boolean contains(Object key) {
            final boolean b = key instanceof Entry;
            boolean b3;
            final boolean b2 = b3 = false;
            if (b) {
                final Entry entry = (Entry)key;
                key = entry.getKey();
                final Object value = entry.getValue();
                final int entryByKey = this.b.findEntryByKey(key);
                b3 = b2;
                if (entryByKey != -1) {
                    b3 = b2;
                    if (b11.a(value, this.b.values[entryByKey])) {
                        b3 = true;
                    }
                }
            }
            return b3;
        }
        
        @Override
        public boolean remove(Object key) {
            if (key instanceof Entry) {
                final Entry entry = (Entry)key;
                key = entry.getKey();
                final Object value = entry.getValue();
                final int d = rc0.d(key);
                final int entryByKey = this.b.findEntryByKey(key, d);
                if (entryByKey != -1 && b11.a(value, this.b.values[entryByKey])) {
                    this.b.removeEntryKeyHashKnown(entryByKey, d);
                    return true;
                }
            }
            return false;
        }
    }
    
    public static class d extends g
    {
        public d(final HashBiMap hashBiMap) {
            super(hashBiMap);
        }
        
        public Entry b(final int n) {
            return new b(super.a, n);
        }
        
        @Override
        public boolean contains(Object key) {
            final boolean b = key instanceof Entry;
            boolean b3;
            final boolean b2 = b3 = false;
            if (b) {
                final Entry entry = (Entry)key;
                key = entry.getKey();
                final Object value = entry.getValue();
                final int entryByValue = super.a.findEntryByValue(key);
                b3 = b2;
                if (entryByValue != -1) {
                    b3 = b2;
                    if (b11.a(super.a.keys[entryByValue], value)) {
                        b3 = true;
                    }
                }
            }
            return b3;
        }
        
        @Override
        public boolean remove(Object key) {
            if (key instanceof Entry) {
                final Entry entry = (Entry)key;
                key = entry.getKey();
                final Object value = entry.getValue();
                final int d = rc0.d(key);
                final int entryByValue = super.a.findEntryByValue(key, d);
                if (entryByValue != -1 && b11.a(super.a.keys[entryByValue], value)) {
                    super.a.removeEntryValueHashKnown(entryByValue, d);
                    return true;
                }
            }
            return false;
        }
    }
    
    public abstract static class g extends AbstractSet
    {
        public final HashBiMap a;
        
        public g(final HashBiMap a) {
            this.a = a;
        }
        
        public abstract Object a(final int p0);
        
        @Override
        public void clear() {
            this.a.clear();
        }
        
        @Override
        public Iterator iterator() {
            return new Iterator(this) {
                public int a = HashBiMap.access$000(e.a);
                public int b = -1;
                public int c;
                public int d;
                public final g e;
                
                {
                    final HashBiMap a = e.a;
                    this.c = a.modCount;
                    this.d = a.size;
                }
                
                public final void b() {
                    if (this.e.a.modCount == this.c) {
                        return;
                    }
                    throw new ConcurrentModificationException();
                }
                
                @Override
                public boolean hasNext() {
                    this.b();
                    return this.a != -2 && this.d > 0;
                }
                
                @Override
                public Object next() {
                    if (this.hasNext()) {
                        final Object a = this.e.a(this.a);
                        this.b = this.a;
                        this.a = HashBiMap.access$100(this.e.a)[this.a];
                        --this.d;
                        return a;
                    }
                    throw new NoSuchElementException();
                }
                
                @Override
                public void remove() {
                    this.b();
                    hh.e(this.b != -1);
                    this.e.a.removeEntry(this.b);
                    final int a = this.a;
                    final HashBiMap a2 = this.e.a;
                    if (a == a2.size) {
                        this.a = this.b;
                    }
                    this.b = -1;
                    this.c = a2.modCount;
                }
            };
        }
        
        @Override
        public int size() {
            return this.a.size;
        }
    }
    
    public final class e extends g
    {
        public final HashBiMap b;
        
        public e(final HashBiMap b) {
            super(this.b = b);
        }
        
        @Override
        public Object a(final int n) {
            return k01.a(this.b.keys[n]);
        }
        
        @Override
        public boolean contains(final Object o) {
            return this.b.containsKey(o);
        }
        
        @Override
        public boolean remove(final Object o) {
            final int d = rc0.d(o);
            final int entryByKey = this.b.findEntryByKey(o, d);
            if (entryByKey != -1) {
                this.b.removeEntryKeyHashKnown(entryByKey, d);
                return true;
            }
            return false;
        }
    }
    
    public final class f extends g
    {
        public final HashBiMap b;
        
        public f(final HashBiMap b) {
            super(this.b = b);
        }
        
        @Override
        public Object a(final int n) {
            return k01.a(this.b.values[n]);
        }
        
        @Override
        public boolean contains(final Object o) {
            return this.b.containsValue(o);
        }
        
        @Override
        public boolean remove(final Object o) {
            final int d = rc0.d(o);
            final int entryByValue = this.b.findEntryByValue(o, d);
            if (entryByValue != -1) {
                this.b.removeEntryValueHashKnown(entryByValue, d);
                return true;
            }
            return false;
        }
    }
}

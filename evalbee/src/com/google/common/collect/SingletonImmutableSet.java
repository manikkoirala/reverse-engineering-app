// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Iterator;

final class SingletonImmutableSet<E> extends ImmutableSet<E>
{
    final transient E element;
    
    public SingletonImmutableSet(final E e) {
        this.element = (E)i71.r(e);
    }
    
    @Override
    public ImmutableList<E> asList() {
        return ImmutableList.of(this.element);
    }
    
    @Override
    public boolean contains(final Object obj) {
        return this.element.equals(obj);
    }
    
    @Override
    public int copyIntoArray(final Object[] array, final int n) {
        array[n] = this.element;
        return n + 1;
    }
    
    @Override
    public final int hashCode() {
        return this.element.hashCode();
    }
    
    @Override
    public boolean isPartialView() {
        return false;
    }
    
    @Override
    public w02 iterator() {
        return Iterators.t(this.element);
    }
    
    @Override
    public int size() {
        return 1;
    }
    
    @Override
    public String toString() {
        final String string = this.element.toString();
        final StringBuilder sb = new StringBuilder(String.valueOf(string).length() + 2);
        sb.append('[');
        sb.append(string);
        sb.append(']');
        return sb.toString();
    }
}

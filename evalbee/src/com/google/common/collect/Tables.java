// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Collection;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.Set;
import java.io.Serializable;
import java.util.Collections;
import java.util.Map;

public abstract class Tables
{
    public static final m90 a;
    
    static {
        a = new m90() {
            public Map a(final Map m) {
                return Collections.unmodifiableMap((Map<?, ?>)m);
            }
        };
    }
    
    public static boolean b(final q q, final Object o) {
        return o == q || (o instanceof q && q.cellSet().equals(((q)o).cellSet()));
    }
    
    public static q.a c(final Object o, final Object o2, final Object o3) {
        return new ImmutableCell<Object, Object, Object>(o, o2, o3);
    }
    
    public static m90 d() {
        return Tables.a;
    }
    
    public static final class ImmutableCell<R, C, V> extends b implements Serializable
    {
        private static final long serialVersionUID = 0L;
        private final C columnKey;
        private final R rowKey;
        private final V value;
        
        public ImmutableCell(final R rowKey, final C columnKey, final V value) {
            this.rowKey = rowKey;
            this.columnKey = columnKey;
            this.value = value;
        }
        
        @Override
        public C getColumnKey() {
            return this.columnKey;
        }
        
        @Override
        public R getRowKey() {
            return this.rowKey;
        }
        
        @Override
        public V getValue() {
            return this.value;
        }
    }
    
    public static final class UnmodifiableRowSortedMap<R, C, V> extends UnmodifiableTable<R, C, V> implements wf1
    {
        private static final long serialVersionUID = 0L;
        
        public UnmodifiableRowSortedMap(final wf1 wf1) {
            super(wf1);
        }
        
        @Override
        public wf1 delegate() {
            return (wf1)super.delegate();
        }
        
        @Override
        public SortedSet<R> rowKeySet() {
            return Collections.unmodifiableSortedSet((SortedSet<R>)this.delegate().rowKeySet());
        }
        
        @Override
        public SortedMap<R, Map<C, V>> rowMap() {
            return Collections.unmodifiableSortedMap((SortedMap<R, ? extends Map<C, V>>)Maps.D(this.delegate().rowMap(), d()));
        }
    }
    
    public static class UnmodifiableTable<R, C, V> extends i implements Serializable
    {
        private static final long serialVersionUID = 0L;
        final q delegate;
        
        public UnmodifiableTable(final q q) {
            this.delegate = (q)i71.r(q);
        }
        
        @Override
        public Set<a> cellSet() {
            return Collections.unmodifiableSet((Set<? extends a>)super.cellSet());
        }
        
        @Override
        public void clear() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public Map<R, V> column(final C c) {
            return Collections.unmodifiableMap((Map<? extends R, ? extends V>)super.column(c));
        }
        
        @Override
        public Set<C> columnKeySet() {
            return Collections.unmodifiableSet((Set<? extends C>)super.columnKeySet());
        }
        
        @Override
        public Map<C, Map<R, V>> columnMap() {
            return Collections.unmodifiableMap((Map<? extends C, ? extends Map<R, V>>)Maps.C(super.columnMap(), d()));
        }
        
        @Override
        public q delegate() {
            return this.delegate;
        }
        
        @Override
        public V put(final R r, final C c, final V v) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void putAll(final q q) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public V remove(final Object o, final Object o2) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public Map<C, V> row(final R r) {
            return Collections.unmodifiableMap((Map<? extends C, ? extends V>)super.row(r));
        }
        
        @Override
        public Set<R> rowKeySet() {
            return Collections.unmodifiableSet((Set<? extends R>)super.rowKeySet());
        }
        
        @Override
        public Map<R, Map<C, V>> rowMap() {
            return Collections.unmodifiableMap((Map<? extends R, ? extends Map<C, V>>)Maps.C(super.rowMap(), d()));
        }
        
        @Override
        public Collection<V> values() {
            return Collections.unmodifiableCollection((Collection<? extends V>)super.values());
        }
    }
    
    public abstract static class b implements a
    {
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (o == this) {
                return true;
            }
            if (o instanceof a) {
                final a a = (a)o;
                if (!b11.a(((q.a)this).getRowKey(), a.getRowKey()) || !b11.a(((q.a)this).getColumnKey(), a.getColumnKey()) || !b11.a(((q.a)this).getValue(), a.getValue())) {
                    b = false;
                }
                return b;
            }
            return false;
        }
        
        @Override
        public int hashCode() {
            return b11.b(((q.a)this).getRowKey(), ((q.a)this).getColumnKey(), ((q.a)this).getValue());
        }
        
        @Override
        public String toString() {
            final String value = String.valueOf(((q.a)this).getRowKey());
            final String value2 = String.valueOf(((q.a)this).getColumnKey());
            final String value3 = String.valueOf(((q.a)this).getValue());
            final StringBuilder sb = new StringBuilder(value.length() + 4 + value2.length() + value3.length());
            sb.append("(");
            sb.append(value);
            sb.append(",");
            sb.append(value2);
            sb.append(")=");
            sb.append(value3);
            return sb.toString();
        }
    }
}

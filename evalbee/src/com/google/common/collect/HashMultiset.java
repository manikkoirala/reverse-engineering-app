// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Set;
import java.util.Collection;

public final class HashMultiset<E> extends AbstractMapBasedMultiset<E>
{
    private static final long serialVersionUID = 0L;
    
    public HashMultiset(final int n) {
        super(n);
    }
    
    public static <E> HashMultiset<E> create() {
        return create(3);
    }
    
    public static <E> HashMultiset<E> create(final int n) {
        return new HashMultiset<E>(n);
    }
    
    public static <E> HashMultiset<E> create(final Iterable<? extends E> iterable) {
        final HashMultiset<Object> create = create(Multisets.h(iterable));
        rg0.a(create, iterable);
        return (HashMultiset<E>)create;
    }
    
    @Override
    public k newBackingMap(final int n) {
        return new k(n);
    }
}

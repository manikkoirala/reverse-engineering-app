// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.LinkedHashMap;
import java.util.Collection;

final class SparseImmutableTable<R, C, V> extends RegularImmutableTable<R, C, V>
{
    static final ImmutableTable<Object, Object, Object> EMPTY;
    private final int[] cellColumnInRowIndices;
    private final int[] cellRowIndices;
    private final ImmutableMap<C, ImmutableMap<R, V>> columnMap;
    private final ImmutableMap<R, ImmutableMap<C, V>> rowMap;
    
    static {
        EMPTY = new SparseImmutableTable<Object, Object, Object>(ImmutableList.of(), ImmutableSet.of(), ImmutableSet.of());
    }
    
    public SparseImmutableTable(final ImmutableList<q.a> list, final ImmutableSet<R> set, final ImmutableSet<C> set2) {
        final ImmutableMap k = Maps.k(set);
        final LinkedHashMap r = Maps.r();
        final w02 iterator = set.iterator();
        while (iterator.hasNext()) {
            r.put(iterator.next(), new LinkedHashMap());
        }
        final LinkedHashMap r2 = Maps.r();
        final w02 iterator2 = set2.iterator();
        while (iterator2.hasNext()) {
            r2.put(iterator2.next(), new LinkedHashMap());
        }
        final int[] cellRowIndices = new int[list.size()];
        final int[] cellColumnInRowIndices = new int[list.size()];
        for (int i = 0; i < list.size(); ++i) {
            final q.a a = (q.a)list.get(i);
            final Object rowKey = a.getRowKey();
            final Object columnKey = a.getColumnKey();
            final Object value = a.getValue();
            final Integer obj = (Integer)k.get(rowKey);
            Objects.requireNonNull(obj);
            cellRowIndices[i] = obj;
            final Map obj2 = (Map)r.get(rowKey);
            Objects.requireNonNull(obj2);
            final Map map = obj2;
            cellColumnInRowIndices[i] = map.size();
            this.checkNoDuplicate((R)rowKey, (C)columnKey, map.put(columnKey, value), (V)value);
            final Map obj3 = (Map)r2.get(columnKey);
            Objects.requireNonNull(obj3);
            obj3.put(rowKey, value);
        }
        this.cellRowIndices = cellRowIndices;
        this.cellColumnInRowIndices = cellColumnInRowIndices;
        final ImmutableMap.b b = new ImmutableMap.b(r.size());
        for (final Map.Entry<Object, V> entry : r.entrySet()) {
            b.g(entry.getKey(), ImmutableMap.copyOf((Map<?, ?>)entry.getValue()));
        }
        this.rowMap = b.d();
        final ImmutableMap.b b2 = new ImmutableMap.b(r2.size());
        for (final Map.Entry<Object, V> entry2 : r2.entrySet()) {
            b2.g(entry2.getKey(), ImmutableMap.copyOf((Map<?, ?>)entry2.getValue()));
        }
        this.columnMap = b2.d();
    }
    
    @Override
    public ImmutableMap<C, Map<R, V>> columnMap() {
        return ImmutableMap.copyOf((Map<? extends C, ? extends Map<R, V>>)this.columnMap);
    }
    
    @Override
    public SerializedForm createSerializedForm() {
        final ImmutableMap k = Maps.k(this.columnKeySet());
        final int[] array = new int[this.cellSet().size()];
        final w02 iterator = this.cellSet().iterator();
        int n = 0;
        while (iterator.hasNext()) {
            final Integer obj = (Integer)k.get(((q.a)iterator.next()).getColumnKey());
            Objects.requireNonNull(obj);
            array[n] = obj;
            ++n;
        }
        return SerializedForm.create(this, this.cellRowIndices, array);
    }
    
    @Override
    public q.a getCell(int n) {
        final Map.Entry entry = (Map.Entry)this.rowMap.entrySet().asList().get(this.cellRowIndices[n]);
        final ImmutableMap immutableMap = (ImmutableMap)entry.getValue();
        n = this.cellColumnInRowIndices[n];
        final Map.Entry entry2 = (Map.Entry)immutableMap.entrySet().asList().get(n);
        return ImmutableTable.cellOf(entry.getKey(), (Object)entry2.getKey(), entry2.getValue());
    }
    
    @Override
    public V getValue(int n) {
        final ImmutableMap immutableMap = this.rowMap.values().asList().get(this.cellRowIndices[n]);
        n = this.cellColumnInRowIndices[n];
        return (V)immutableMap.values().asList().get(n);
    }
    
    @Override
    public ImmutableMap<R, Map<C, V>> rowMap() {
        return ImmutableMap.copyOf((Map<? extends R, ? extends Map<C, V>>)this.rowMap);
    }
    
    @Override
    public int size() {
        return this.cellRowIndices.length;
    }
}

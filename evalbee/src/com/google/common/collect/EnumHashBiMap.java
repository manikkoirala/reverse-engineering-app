// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Set;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.io.ObjectInputStream;
import java.util.Map;
import java.util.EnumMap;

public final class EnumHashBiMap<K extends Enum<K>, V> extends AbstractBiMap<K, V>
{
    private static final long serialVersionUID = 0L;
    private transient Class<K> keyType;
    
    private EnumHashBiMap(final Class<K> clazz) {
        super(new EnumMap(clazz), Maps.q(clazz.getEnumConstants().length));
        this.keyType = clazz;
    }
    
    public static <K extends Enum<K>, V> EnumHashBiMap<K, V> create(final Class<K> clazz) {
        return new EnumHashBiMap<K, V>(clazz);
    }
    
    public static <K extends Enum<K>, V> EnumHashBiMap<K, V> create(final Map<K, ? extends V> map) {
        final EnumHashBiMap<Enum, Object> create = create((Class<Enum>)EnumBiMap.inferKeyType((Map<K, ?>)map));
        create.putAll(map);
        return (EnumHashBiMap<K, V>)create;
    }
    
    private void readObject(final ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        this.keyType = (Class)objectInputStream.readObject();
        this.setDelegates(new EnumMap<K, V>(this.keyType), new HashMap<V, K>(this.keyType.getEnumConstants().length * 3 / 2));
        n.b(this, objectInputStream);
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(this.keyType);
        n.i(this, objectOutputStream);
    }
    
    @Override
    public K checkKey(final K k) {
        return (K)i71.r(k);
    }
    
    @Override
    public V forcePut(final K k, final V v) {
        return super.forcePut(k, v);
    }
    
    public Class<K> keyType() {
        return this.keyType;
    }
    
    @Override
    public V put(final K k, final V v) {
        return super.put(k, v);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import com.google.common.base.a;
import com.google.common.base.Equivalence;

public final class MapMaker
{
    public boolean a;
    public int b;
    public int c;
    public MapMakerInternalMap.Strength d;
    public MapMakerInternalMap.Strength e;
    public Equivalence f;
    
    public MapMaker() {
        this.b = -1;
        this.c = -1;
    }
    
    public MapMaker a(final int c) {
        final int c2 = this.c;
        final boolean b = true;
        i71.z(c2 == -1, "concurrency level was already set to %s", c2);
        i71.d(c > 0 && b);
        this.c = c;
        return this;
    }
    
    public int b() {
        int c;
        if ((c = this.c) == -1) {
            c = 4;
        }
        return c;
    }
    
    public int c() {
        int b;
        if ((b = this.b) == -1) {
            b = 16;
        }
        return b;
    }
    
    public Equivalence d() {
        return (Equivalence)com.google.common.base.a.a(this.f, this.e().defaultEquivalence());
    }
    
    public MapMakerInternalMap.Strength e() {
        return (MapMakerInternalMap.Strength)com.google.common.base.a.a(this.d, MapMakerInternalMap.Strength.STRONG);
    }
    
    public MapMakerInternalMap.Strength f() {
        return (MapMakerInternalMap.Strength)com.google.common.base.a.a(this.e, MapMakerInternalMap.Strength.STRONG);
    }
    
    public MapMaker g(final int b) {
        final int b2 = this.b;
        final boolean b3 = true;
        i71.z(b2 == -1, "initial capacity was already set to %s", b2);
        i71.d(b >= 0 && b3);
        this.b = b;
        return this;
    }
    
    public MapMaker h(final Equivalence equivalence) {
        final Equivalence f = this.f;
        i71.B(f == null, "key equivalence was already set to %s", f);
        this.f = (Equivalence)i71.r(equivalence);
        this.a = true;
        return this;
    }
    
    public ConcurrentMap i() {
        if (!this.a) {
            return new ConcurrentHashMap(this.c(), 0.75f, this.b());
        }
        return MapMakerInternalMap.create(this);
    }
    
    public MapMaker j(final MapMakerInternalMap.Strength strength) {
        final MapMakerInternalMap.Strength d = this.d;
        i71.B(d == null, "Key strength was already set to %s", d);
        this.d = (MapMakerInternalMap.Strength)i71.r(strength);
        if (strength != MapMakerInternalMap.Strength.STRONG) {
            this.a = true;
        }
        return this;
    }
    
    public MapMaker k(final MapMakerInternalMap.Strength strength) {
        final MapMakerInternalMap.Strength e = this.e;
        i71.B(e == null, "Value strength was already set to %s", e);
        this.e = (MapMakerInternalMap.Strength)i71.r(strength);
        if (strength != MapMakerInternalMap.Strength.STRONG) {
            this.a = true;
        }
        return this;
    }
    
    public MapMaker l() {
        return this.j(MapMakerInternalMap.Strength.WEAK);
    }
    
    @Override
    public String toString() {
        final a.b c = com.google.common.base.a.c(this);
        final int b = this.b;
        if (b != -1) {
            c.b("initialCapacity", b);
        }
        final int c2 = this.c;
        if (c2 != -1) {
            c.b("concurrencyLevel", c2);
        }
        final MapMakerInternalMap.Strength d = this.d;
        if (d != null) {
            c.d("keyStrength", d9.c(d.toString()));
        }
        final MapMakerInternalMap.Strength e = this.e;
        if (e != null) {
            c.d("valueStrength", d9.c(e.toString()));
        }
        if (this.f != null) {
            c.k("keyEquivalence");
        }
        return c.toString();
    }
    
    public enum Dummy
    {
        private static final Dummy[] $VALUES;
        
        VALUE;
        
        private static /* synthetic */ Dummy[] $values() {
            return new Dummy[] { Dummy.VALUE };
        }
        
        static {
            $VALUES = $values();
        }
    }
}

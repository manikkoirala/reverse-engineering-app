// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import com.google.common.primitives.Ints;
import java.util.SortedSet;
import java.util.Set;
import java.util.NavigableSet;
import java.util.Comparator;

final class RegularImmutableSortedMultiset<E> extends ImmutableSortedMultiset<E>
{
    static final ImmutableSortedMultiset<Comparable> NATURAL_EMPTY_MULTISET;
    private static final long[] ZERO_CUMULATIVE_COUNTS;
    private final transient long[] cumulativeCounts;
    final transient RegularImmutableSortedSet<E> elementSet;
    private final transient int length;
    private final transient int offset;
    
    static {
        ZERO_CUMULATIVE_COUNTS = new long[] { 0L };
        NATURAL_EMPTY_MULTISET = new RegularImmutableSortedMultiset<Comparable>(Ordering.natural());
    }
    
    public RegularImmutableSortedMultiset(final RegularImmutableSortedSet<E> elementSet, final long[] cumulativeCounts, final int offset, final int length) {
        this.elementSet = elementSet;
        this.cumulativeCounts = cumulativeCounts;
        this.offset = offset;
        this.length = length;
    }
    
    public RegularImmutableSortedMultiset(final Comparator<? super E> comparator) {
        this.elementSet = ImmutableSortedSet.emptySet(comparator);
        this.cumulativeCounts = RegularImmutableSortedMultiset.ZERO_CUMULATIVE_COUNTS;
        this.offset = 0;
        this.length = 0;
    }
    
    private int getCount(final int n) {
        final long[] cumulativeCounts = this.cumulativeCounts;
        final int offset = this.offset;
        return (int)(cumulativeCounts[offset + n + 1] - cumulativeCounts[offset + n]);
    }
    
    @Override
    public int count(final Object o) {
        final int index = this.elementSet.indexOf(o);
        int count;
        if (index >= 0) {
            count = this.getCount(index);
        }
        else {
            count = 0;
        }
        return count;
    }
    
    @Override
    public ImmutableSortedSet<E> elementSet() {
        return this.elementSet;
    }
    
    @Override
    public j.a firstEntry() {
        j.a entry;
        if (this.isEmpty()) {
            entry = null;
        }
        else {
            entry = this.getEntry(0);
        }
        return entry;
    }
    
    @Override
    public j.a getEntry(final int n) {
        return Multisets.g(this.elementSet.asList().get(n), this.getCount(n));
    }
    
    public ImmutableSortedMultiset<E> getSubMultiset(final int n, final int n2) {
        i71.w(n, n2, this.length);
        if (n == n2) {
            return ImmutableSortedMultiset.emptyMultiset(this.comparator());
        }
        if (n == 0 && n2 == this.length) {
            return this;
        }
        return new RegularImmutableSortedMultiset((RegularImmutableSortedSet<Object>)this.elementSet.getSubSet(n, n2), this.cumulativeCounts, this.offset + n, n2 - n);
    }
    
    @Override
    public ImmutableSortedMultiset<E> headMultiset(final E e, final BoundType boundType) {
        return this.getSubMultiset(0, this.elementSet.headIndex(e, i71.r(boundType) == BoundType.CLOSED));
    }
    
    @Override
    public boolean isPartialView() {
        final int offset = this.offset;
        boolean b = true;
        if (offset <= 0) {
            b = (this.length < this.cumulativeCounts.length - 1 && b);
        }
        return b;
    }
    
    @Override
    public j.a lastEntry() {
        j.a entry;
        if (this.isEmpty()) {
            entry = null;
        }
        else {
            entry = this.getEntry(this.length - 1);
        }
        return entry;
    }
    
    @Override
    public int size() {
        final long[] cumulativeCounts = this.cumulativeCounts;
        final int offset = this.offset;
        return Ints.k(cumulativeCounts[this.length + offset] - cumulativeCounts[offset]);
    }
    
    @Override
    public ImmutableSortedMultiset<E> tailMultiset(final E e, final BoundType boundType) {
        return this.getSubMultiset(this.elementSet.tailIndex(e, i71.r(boundType) == BoundType.CLOSED), this.length);
    }
}

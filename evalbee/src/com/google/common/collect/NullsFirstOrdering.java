// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.io.Serializable;

final class NullsFirstOrdering<T> extends Ordering implements Serializable
{
    private static final long serialVersionUID = 0L;
    final Ordering ordering;
    
    public NullsFirstOrdering(final Ordering ordering) {
        this.ordering = ordering;
    }
    
    @Override
    public int compare(final T t, final T t2) {
        if (t == t2) {
            return 0;
        }
        if (t == null) {
            return -1;
        }
        if (t2 == null) {
            return 1;
        }
        return this.ordering.compare(t, t2);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof NullsFirstOrdering && this.ordering.equals(((NullsFirstOrdering)o).ordering));
    }
    
    @Override
    public int hashCode() {
        return this.ordering.hashCode() ^ 0x39153A74;
    }
    
    @Override
    public <S extends T> Ordering nullsFirst() {
        return this;
    }
    
    @Override
    public <S extends T> Ordering nullsLast() {
        return this.ordering.nullsLast();
    }
    
    @Override
    public <S extends T> Ordering reverse() {
        return this.ordering.reverse().nullsLast();
    }
    
    @Override
    public String toString() {
        final String value = String.valueOf(this.ordering);
        final StringBuilder sb = new StringBuilder(value.length() + 13);
        sb.append(value);
        sb.append(".nullsFirst()");
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Set;
import java.io.Serializable;

final class MultimapBuilder$LinkedHashSetSupplier<V> implements is1, Serializable
{
    private final int expectedValuesPerKey;
    
    public MultimapBuilder$LinkedHashSetSupplier(final int n) {
        this.expectedValuesPerKey = hh.b(n, "expectedValuesPerKey");
    }
    
    @Override
    public Set<V> get() {
        return l.f(this.expectedValuesPerKey);
    }
}

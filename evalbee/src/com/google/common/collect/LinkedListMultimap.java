// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.NoSuchElementException;
import java.util.ConcurrentModificationException;
import java.util.ListIterator;
import java.util.AbstractSequentialList;
import java.util.Set;
import java.util.Collection;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Map;
import java.io.Serializable;

public class LinkedListMultimap<K, V> extends a implements dk0, Serializable
{
    private static final long serialVersionUID = 0L;
    private transient g head;
    private transient Map<K, f> keyToKeyList;
    private transient int modCount;
    private transient int size;
    private transient g tail;
    
    public LinkedListMultimap() {
        this(12);
    }
    
    private LinkedListMultimap(final int n) {
        this.keyToKeyList = l.c(n);
    }
    
    private LinkedListMultimap(final rx0 rx0) {
        this(rx0.keySet().size());
        this.putAll(rx0);
    }
    
    public static /* synthetic */ int access$000(final LinkedListMultimap linkedListMultimap) {
        return linkedListMultimap.modCount;
    }
    
    public static /* synthetic */ g access$100(final LinkedListMultimap linkedListMultimap) {
        return linkedListMultimap.tail;
    }
    
    public static /* synthetic */ g access$200(final LinkedListMultimap linkedListMultimap) {
        return linkedListMultimap.head;
    }
    
    public static /* synthetic */ Map access$500(final LinkedListMultimap linkedListMultimap) {
        return linkedListMultimap.keyToKeyList;
    }
    
    public static /* synthetic */ int access$800(final LinkedListMultimap linkedListMultimap) {
        return linkedListMultimap.size;
    }
    
    private g addNode(final K k, final V v, final g g) {
        final g g2 = new g(k, v);
        Label_0293: {
            Map<K, f> map;
            f f;
            if (this.head == null) {
                this.tail = g2;
                this.head = g2;
                map = this.keyToKeyList;
                f = new f(g2);
            }
            else {
                if (g != null) {
                    final f obj = this.keyToKeyList.get(k);
                    Objects.requireNonNull(obj);
                    ++obj.c;
                    g2.d = g.d;
                    g2.f = g.f;
                    g2.c = g;
                    g2.e = g;
                    final g f2 = g.f;
                    if (f2 == null) {
                        obj.a = g2;
                    }
                    else {
                        f2.e = g2;
                    }
                    final g d = g.d;
                    if (d == null) {
                        this.head = g2;
                    }
                    else {
                        d.c = g2;
                    }
                    g.d = g2;
                    g.f = g2;
                    break Label_0293;
                }
                final g tail = this.tail;
                Objects.requireNonNull(tail);
                tail.c = g2;
                g2.d = this.tail;
                this.tail = g2;
                final f f3 = this.keyToKeyList.get(k);
                if (f3 != null) {
                    ++f3.c;
                    final g b = f3.b;
                    b.e = g2;
                    g2.f = b;
                    f3.b = g2;
                    break Label_0293;
                }
                map = this.keyToKeyList;
                f = new f(g2);
            }
            map.put(k, f);
            ++this.modCount;
        }
        ++this.size;
        return g2;
    }
    
    public static <K, V> LinkedListMultimap<K, V> create() {
        return new LinkedListMultimap<K, V>();
    }
    
    public static <K, V> LinkedListMultimap<K, V> create(final int n) {
        return new LinkedListMultimap<K, V>(n);
    }
    
    public static <K, V> LinkedListMultimap<K, V> create(final rx0 rx0) {
        return new LinkedListMultimap<K, V>(rx0);
    }
    
    private List<V> getCopy(final K k) {
        return Collections.unmodifiableList((List<? extends V>)Lists.k(new i(k)));
    }
    
    private void readObject(final ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        this.keyToKeyList = (Map<K, f>)CompactLinkedHashMap.create();
        for (int int1 = objectInputStream.readInt(), i = 0; i < int1; ++i) {
            this.put(objectInputStream.readObject(), objectInputStream.readObject());
        }
    }
    
    private void removeAllNodes(final K k) {
        Iterators.d(new i(k));
    }
    
    private void removeNode(g f) {
        final g d = f.d;
        final g c = f.c;
        if (d != null) {
            d.c = c;
        }
        else {
            this.head = c;
        }
        final g c2 = f.c;
        if (c2 != null) {
            c2.d = d;
        }
        else {
            this.tail = d;
        }
        if (f.f == null && f.e == null) {
            final f obj = this.keyToKeyList.remove(f.a);
            Objects.requireNonNull(obj);
            obj.c = 0;
            ++this.modCount;
        }
        else {
            final f obj2 = this.keyToKeyList.get(f.a);
            Objects.requireNonNull(obj2);
            --obj2.c;
            final g f2 = f.f;
            if (f2 == null) {
                final g e = f.e;
                Objects.requireNonNull(e);
                obj2.a = e;
            }
            else {
                f2.e = f.e;
            }
            final g e2 = f.e;
            f = f.f;
            if (e2 == null) {
                Objects.requireNonNull(f);
                obj2.b = f;
            }
            else {
                e2.f = f;
            }
        }
        --this.size;
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeInt(this.size());
        for (final Map.Entry<Object, V> entry : this.entries()) {
            objectOutputStream.writeObject(entry.getKey());
            objectOutputStream.writeObject(entry.getValue());
        }
    }
    
    @Override
    public void clear() {
        this.head = null;
        this.tail = null;
        this.keyToKeyList.clear();
        this.size = 0;
        ++this.modCount;
    }
    
    @Override
    public boolean containsKey(final Object o) {
        return this.keyToKeyList.containsKey(o);
    }
    
    @Override
    public boolean containsValue(final Object o) {
        return this.values().contains(o);
    }
    
    @Override
    public Map<K, Collection<V>> createAsMap() {
        return new Multimaps.a(this);
    }
    
    @Override
    public List<Map.Entry<K, V>> createEntries() {
        return new b();
    }
    
    @Override
    public Set<K> createKeySet() {
        return new c();
    }
    
    @Override
    public j createKeys() {
        return new Multimaps.c(this);
    }
    
    @Override
    public List<V> createValues() {
        return new d();
    }
    
    @Override
    public List<Map.Entry<K, V>> entries() {
        return (List)super.entries();
    }
    
    public Iterator<Map.Entry<K, V>> entryIterator() {
        throw new AssertionError((Object)"should never be called");
    }
    
    @Override
    public List<V> get(final K k) {
        return new AbstractSequentialList(this, k) {
            public final Object a;
            public final LinkedListMultimap b;
            
            @Override
            public ListIterator listIterator(final int n) {
                return this.b.new i(this.a, n);
            }
            
            @Override
            public int size() {
                final f f = LinkedListMultimap.access$500(this.b).get(this.a);
                int c;
                if (f == null) {
                    c = 0;
                }
                else {
                    c = f.c;
                }
                return c;
            }
        };
    }
    
    @Override
    public boolean isEmpty() {
        return this.head == null;
    }
    
    @Override
    public boolean put(final K k, final V v) {
        this.addNode(k, v, null);
        return true;
    }
    
    @Override
    public List<V> removeAll(final Object o) {
        final List<V> copy = this.getCopy(o);
        this.removeAllNodes(o);
        return copy;
    }
    
    @Override
    public List<V> replaceValues(final K k, final Iterable<? extends V> iterable) {
        final List<V> copy = this.getCopy(k);
        final i i = new i(k);
        final Iterator<? extends V> iterator = iterable.iterator();
        while (i.hasNext() && iterator.hasNext()) {
            i.next();
            i.set(iterator.next());
        }
        while (i.hasNext()) {
            i.next();
            i.remove();
        }
        while (iterator.hasNext()) {
            i.add(iterator.next());
        }
        return copy;
    }
    
    @Override
    public int size() {
        return this.size;
    }
    
    @Override
    public List<V> values() {
        return (List)super.values();
    }
    
    public class b extends AbstractSequentialList
    {
        public final LinkedListMultimap a;
        
        public b(final LinkedListMultimap a) {
            this.a = a;
        }
        
        @Override
        public ListIterator listIterator(final int n) {
            return this.a.new h(n);
        }
        
        @Override
        public int size() {
            return LinkedListMultimap.access$800(this.a);
        }
    }
    
    public class c extends Sets.a
    {
        public final LinkedListMultimap a;
        
        public c(final LinkedListMultimap a) {
            this.a = a;
        }
        
        @Override
        public boolean contains(final Object o) {
            return this.a.containsKey(o);
        }
        
        @Override
        public Iterator iterator() {
            return this.a.new e(null);
        }
        
        @Override
        public boolean remove(final Object o) {
            return this.a.removeAll(o).isEmpty() ^ true;
        }
        
        @Override
        public int size() {
            return LinkedListMultimap.access$500(this.a).size();
        }
    }
    
    public class d extends AbstractSequentialList
    {
        public final LinkedListMultimap a;
        
        public d(final LinkedListMultimap a) {
            this.a = a;
        }
        
        @Override
        public ListIterator listIterator(final int n) {
            final h h = this.a.new h(n);
            return new s(this, h, h) {
                public final h b;
                
                public Object d(final Map.Entry entry) {
                    return entry.getValue();
                }
                
                @Override
                public void set(final Object o) {
                    this.b.g(o);
                }
            };
        }
        
        @Override
        public int size() {
            return LinkedListMultimap.access$800(this.a);
        }
    }
    
    public class e implements Iterator
    {
        public final Set a;
        public g b;
        public g c;
        public int d;
        public final LinkedListMultimap e;
        
        public e(final LinkedListMultimap e) {
            this.e = e;
            this.a = Sets.d(e.keySet().size());
            this.b = LinkedListMultimap.access$200(e);
            this.d = LinkedListMultimap.access$000(e);
        }
        
        public final void b() {
            if (LinkedListMultimap.access$000(this.e) == this.d) {
                return;
            }
            throw new ConcurrentModificationException();
        }
        
        @Override
        public boolean hasNext() {
            this.b();
            return this.b != null;
        }
        
        @Override
        public Object next() {
            this.b();
            final g b = this.b;
            if (b != null) {
                this.c = b;
                this.a.add(b.a);
                g c;
                do {
                    c = this.b.c;
                    this.b = c;
                } while (c != null && !this.a.add(c.a));
                return this.c.a;
            }
            throw new NoSuchElementException();
        }
        
        @Override
        public void remove() {
            this.b();
            i71.y(this.c != null, "no calls to next() since the last call to remove()");
            this.e.removeAllNodes(this.c.a);
            this.c = null;
            this.d = LinkedListMultimap.access$000(this.e);
        }
    }
    
    public static class f
    {
        public g a;
        public g b;
        public int c;
        
        public f(final g g) {
            this.a = g;
            this.b = g;
            g.f = null;
            g.e = null;
            this.c = 1;
        }
    }
    
    public static final class g extends s
    {
        public final Object a;
        public Object b;
        public g c;
        public g d;
        public g e;
        public g f;
        
        public g(final Object a, final Object b) {
            this.a = a;
            this.b = b;
        }
        
        @Override
        public Object getKey() {
            return this.a;
        }
        
        @Override
        public Object getValue() {
            return this.b;
        }
        
        @Override
        public Object setValue(final Object b) {
            final Object b2 = this.b;
            this.b = b;
            return b2;
        }
    }
    
    public class h implements ListIterator
    {
        public int a;
        public g b;
        public g c;
        public g d;
        public int e;
        public final LinkedListMultimap f;
        
        public h(final LinkedListMultimap f, int i) {
            this.f = f;
            this.e = LinkedListMultimap.access$000(f);
            final int size = f.size();
            i71.u(i, size);
            if (i >= size / 2) {
                this.d = LinkedListMultimap.access$100(f);
                this.a = size;
                while (i < size) {
                    this.e();
                    ++i;
                }
            }
            else {
                this.b = LinkedListMultimap.access$200(f);
                while (i > 0) {
                    this.d();
                    --i;
                }
            }
            this.c = null;
        }
        
        public void b(final Map.Entry entry) {
            throw new UnsupportedOperationException();
        }
        
        public final void c() {
            if (LinkedListMultimap.access$000(this.f) == this.e) {
                return;
            }
            throw new ConcurrentModificationException();
        }
        
        public g d() {
            this.c();
            final g b = this.b;
            if (b != null) {
                this.c = b;
                this.d = b;
                this.b = b.c;
                ++this.a;
                return b;
            }
            throw new NoSuchElementException();
        }
        
        public g e() {
            this.c();
            final g d = this.d;
            if (d != null) {
                this.c = d;
                this.b = d;
                this.d = d.d;
                --this.a;
                return d;
            }
            throw new NoSuchElementException();
        }
        
        public void f(final Map.Entry entry) {
            throw new UnsupportedOperationException();
        }
        
        public void g(final Object b) {
            i71.x(this.c != null);
            this.c.b = b;
        }
        
        @Override
        public boolean hasNext() {
            this.c();
            return this.b != null;
        }
        
        @Override
        public boolean hasPrevious() {
            this.c();
            return this.d != null;
        }
        
        @Override
        public int nextIndex() {
            return this.a;
        }
        
        @Override
        public int previousIndex() {
            return this.a - 1;
        }
        
        @Override
        public void remove() {
            this.c();
            i71.y(this.c != null, "no calls to next() since the last call to remove()");
            final g c = this.c;
            if (c != this.b) {
                this.d = c.d;
                --this.a;
            }
            else {
                this.b = c.c;
            }
            this.f.removeNode(c);
            this.c = null;
            this.e = LinkedListMultimap.access$000(this.f);
        }
    }
    
    public class i implements ListIterator
    {
        public final Object a;
        public int b;
        public g c;
        public g d;
        public g e;
        public final LinkedListMultimap f;
        
        public i(final LinkedListMultimap f, final Object a) {
            this.f = f;
            this.a = a;
            final f f2 = LinkedListMultimap.access$500(f).get(a);
            g a2;
            if (f2 == null) {
                a2 = null;
            }
            else {
                a2 = f2.a;
            }
            this.c = a2;
        }
        
        public i(final LinkedListMultimap f, final Object a, int i) {
            this.f = f;
            final f f2 = LinkedListMultimap.access$500(f).get(a);
            int c;
            if (f2 == null) {
                c = 0;
            }
            else {
                c = f2.c;
            }
            i71.u(i, c);
            if (i >= c / 2) {
                g b;
                if (f2 == null) {
                    b = null;
                }
                else {
                    b = f2.b;
                }
                this.e = b;
                this.b = c;
                while (i < c) {
                    this.previous();
                    ++i;
                }
            }
            else {
                g a2;
                if (f2 == null) {
                    a2 = null;
                }
                else {
                    a2 = f2.a;
                }
                this.c = a2;
                while (i > 0) {
                    this.next();
                    --i;
                }
            }
            this.a = a;
            this.d = null;
        }
        
        @Override
        public void add(final Object o) {
            this.e = this.f.addNode(this.a, o, this.c);
            ++this.b;
            this.d = null;
        }
        
        @Override
        public boolean hasNext() {
            return this.c != null;
        }
        
        @Override
        public boolean hasPrevious() {
            return this.e != null;
        }
        
        @Override
        public Object next() {
            final g c = this.c;
            if (c != null) {
                this.d = c;
                this.e = c;
                this.c = c.e;
                ++this.b;
                return c.b;
            }
            throw new NoSuchElementException();
        }
        
        @Override
        public int nextIndex() {
            return this.b;
        }
        
        @Override
        public Object previous() {
            final g e = this.e;
            if (e != null) {
                this.d = e;
                this.c = e;
                this.e = e.f;
                --this.b;
                return e.b;
            }
            throw new NoSuchElementException();
        }
        
        @Override
        public int previousIndex() {
            return this.b - 1;
        }
        
        @Override
        public void remove() {
            i71.y(this.d != null, "no calls to next() since the last call to remove()");
            final g d = this.d;
            if (d != this.c) {
                this.e = d.f;
                --this.b;
            }
            else {
                this.c = d.e;
            }
            this.f.removeNode(d);
            this.d = null;
        }
        
        @Override
        public void set(final Object b) {
            i71.x(this.d != null);
            this.d.b = b;
        }
    }
}

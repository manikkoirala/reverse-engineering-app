// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.AbstractCollection;
import java.util.NoSuchElementException;
import java.util.ConcurrentModificationException;
import java.util.AbstractSet;
import java.util.LinkedHashMap;
import java.util.Arrays;
import com.google.common.primitives.Ints;
import java.util.Iterator;
import java.io.ObjectOutputStream;
import java.util.Objects;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.io.Serializable;
import java.util.AbstractMap;

class CompactHashMap<K, V> extends AbstractMap<K, V> implements Serializable
{
    static final double HASH_FLOODING_FPP = 0.001;
    private static final int MAX_HASH_BUCKET_LENGTH = 9;
    private static final Object NOT_FOUND;
    transient int[] entries;
    private transient Set<Entry<K, V>> entrySetView;
    private transient Set<K> keySetView;
    transient Object[] keys;
    private transient int metadata;
    private transient int size;
    private transient Object table;
    transient Object[] values;
    private transient Collection<V> valuesView;
    
    static {
        NOT_FOUND = new Object();
    }
    
    public CompactHashMap() {
        this.init(3);
    }
    
    public CompactHashMap(final int n) {
        this.init(n);
    }
    
    public static /* synthetic */ int access$000(final CompactHashMap compactHashMap) {
        return compactHashMap.metadata;
    }
    
    public static /* synthetic */ int access$1210(final CompactHashMap compactHashMap) {
        return compactHashMap.size--;
    }
    
    public static /* synthetic */ Object access$300() {
        return CompactHashMap.NOT_FOUND;
    }
    
    public static <K, V> CompactHashMap<K, V> create() {
        return new CompactHashMap<K, V>();
    }
    
    public static <K, V> CompactHashMap<K, V> createWithExpectedSize(final int n) {
        return new CompactHashMap<K, V>(n);
    }
    
    private int entry(final int n) {
        return this.requireEntries()[n];
    }
    
    private int hashTableMask() {
        return (1 << (this.metadata & 0x1F)) - 1;
    }
    
    private int indexOf(final Object o) {
        if (this.needsAllocArrays()) {
            return -1;
        }
        final int d = rc0.d(o);
        final int hashTableMask = this.hashTableMask();
        int n = ii.h(this.requireTable(), d & hashTableMask);
        if (n == 0) {
            return -1;
        }
        final int b = ii.b(d, hashTableMask);
        int entry;
        do {
            --n;
            entry = this.entry(n);
            if (ii.b(entry, hashTableMask) == b && b11.a(o, this.key(n))) {
                return n;
            }
        } while ((n = ii.c(entry, hashTableMask)) != 0);
        return -1;
    }
    
    private K key(final int n) {
        return (K)this.requireKeys()[n];
    }
    
    private void readObject(final ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        final int int1 = objectInputStream.readInt();
        if (int1 >= 0) {
            this.init(int1);
            for (int i = 0; i < int1; ++i) {
                this.put(objectInputStream.readObject(), objectInputStream.readObject());
            }
            return;
        }
        final StringBuilder sb = new StringBuilder(25);
        sb.append("Invalid size: ");
        sb.append(int1);
        throw new InvalidObjectException(sb.toString());
    }
    
    private Object removeHelper(Object value) {
        if (this.needsAllocArrays()) {
            return CompactHashMap.NOT_FOUND;
        }
        final int hashTableMask = this.hashTableMask();
        final int f = ii.f(value, null, hashTableMask, this.requireTable(), this.requireEntries(), this.requireKeys(), null);
        if (f == -1) {
            return CompactHashMap.NOT_FOUND;
        }
        value = this.value(f);
        this.moveLastEntry(f, hashTableMask);
        --this.size;
        this.incrementModCount();
        return value;
    }
    
    private int[] requireEntries() {
        final int[] entries = this.entries;
        Objects.requireNonNull(entries);
        return entries;
    }
    
    private Object[] requireKeys() {
        final Object[] keys = this.keys;
        Objects.requireNonNull(keys);
        return keys;
    }
    
    private Object requireTable() {
        final Object table = this.table;
        Objects.requireNonNull(table);
        return table;
    }
    
    private Object[] requireValues() {
        final Object[] values = this.values;
        Objects.requireNonNull(values);
        return values;
    }
    
    private void resizeMeMaybe(int min) {
        final int length = this.requireEntries().length;
        if (min > length) {
            min = Math.min(1073741823, Math.max(1, length >>> 1) + length | 0x1);
            if (min != length) {
                this.resizeEntries(min);
            }
        }
    }
    
    private int resizeTable(final int n, int i, int j, int h) {
        final Object a = ii.a(i);
        final int hashTableMask = i - 1;
        if (h != 0) {
            ii.i(a, j & hashTableMask, h + 1);
        }
        final Object requireTable = this.requireTable();
        final int[] requireEntries = this.requireEntries();
        int n2;
        int n3;
        int n4;
        int n5;
        for (i = 0; i <= n; ++i) {
            for (j = ii.h(requireTable, i); j != 0; j = ii.c(n3, n)) {
                n2 = j - 1;
                n3 = requireEntries[n2];
                n4 = (ii.b(n3, n) | i);
                n5 = (n4 & hashTableMask);
                h = ii.h(a, n5);
                ii.i(a, n5, j);
                requireEntries[n2] = ii.d(n4, h, hashTableMask);
            }
        }
        this.table = a;
        this.setHashTableMask(hashTableMask);
        return hashTableMask;
    }
    
    private void setEntry(final int n, final int n2) {
        this.requireEntries()[n] = n2;
    }
    
    private void setHashTableMask(int numberOfLeadingZeros) {
        numberOfLeadingZeros = Integer.numberOfLeadingZeros(numberOfLeadingZeros);
        this.metadata = ii.d(this.metadata, 32 - numberOfLeadingZeros, 31);
    }
    
    private void setKey(final int n, final K k) {
        this.requireKeys()[n] = k;
    }
    
    private void setValue(final int n, final V v) {
        this.requireValues()[n] = v;
    }
    
    private V value(final int n) {
        return (V)this.requireValues()[n];
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeInt(this.size());
        final Iterator<Entry<K, V>> entrySetIterator = this.entrySetIterator();
        while (entrySetIterator.hasNext()) {
            final Map.Entry<Object, V> entry = (Map.Entry<Object, V>)entrySetIterator.next();
            objectOutputStream.writeObject(entry.getKey());
            objectOutputStream.writeObject(entry.getValue());
        }
    }
    
    public void accessEntry(final int n) {
    }
    
    public int adjustAfterRemove(final int n, final int n2) {
        return n - 1;
    }
    
    public int allocArrays() {
        i71.y(this.needsAllocArrays(), "Arrays already allocated");
        final int metadata = this.metadata;
        final int j = ii.j(metadata);
        this.table = ii.a(j);
        this.setHashTableMask(j - 1);
        this.entries = new int[metadata];
        this.keys = new Object[metadata];
        this.values = new Object[metadata];
        return metadata;
    }
    
    @Override
    public void clear() {
        if (this.needsAllocArrays()) {
            return;
        }
        this.incrementModCount();
        final Map<K, V> delegateOrNull = this.delegateOrNull();
        if (delegateOrNull != null) {
            this.metadata = Ints.e(this.size(), 3, 1073741823);
            delegateOrNull.clear();
            this.table = null;
        }
        else {
            Arrays.fill(this.requireKeys(), 0, this.size, null);
            Arrays.fill(this.requireValues(), 0, this.size, null);
            ii.g(this.requireTable());
            Arrays.fill(this.requireEntries(), 0, this.size, 0);
        }
        this.size = 0;
    }
    
    @Override
    public boolean containsKey(final Object o) {
        final Map<K, V> delegateOrNull = this.delegateOrNull();
        boolean containsKey;
        if (delegateOrNull != null) {
            containsKey = delegateOrNull.containsKey(o);
        }
        else {
            containsKey = (this.indexOf(o) != -1);
        }
        return containsKey;
    }
    
    @Override
    public boolean containsValue(final Object o) {
        final Map<K, V> delegateOrNull = this.delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.containsValue(o);
        }
        for (int i = 0; i < this.size; ++i) {
            if (b11.a(o, this.value(i))) {
                return true;
            }
        }
        return false;
    }
    
    public Map<K, V> convertToHashFloodingResistantImplementation() {
        final Map<K, V> hashFloodingResistantDelegate = this.createHashFloodingResistantDelegate(this.hashTableMask() + 1);
        for (int i = this.firstEntryIndex(); i >= 0; i = this.getSuccessor(i)) {
            hashFloodingResistantDelegate.put(this.key(i), this.value(i));
        }
        this.table = hashFloodingResistantDelegate;
        this.entries = null;
        this.keys = null;
        this.values = null;
        this.incrementModCount();
        return hashFloodingResistantDelegate;
    }
    
    public Set<Entry<K, V>> createEntrySet() {
        return new d();
    }
    
    public Map<K, V> createHashFloodingResistantDelegate(final int initialCapacity) {
        return new LinkedHashMap<K, V>(initialCapacity, 1.0f);
    }
    
    public Set<K> createKeySet() {
        return new f();
    }
    
    public Collection<V> createValues() {
        return new h();
    }
    
    public Map<K, V> delegateOrNull() {
        final Object table = this.table;
        if (table instanceof Map) {
            return (Map<K, V>)table;
        }
        return null;
    }
    
    @Override
    public Set<Entry<K, V>> entrySet() {
        Set<Entry<K, V>> entrySetView;
        if ((entrySetView = this.entrySetView) == null) {
            entrySetView = this.createEntrySet();
            this.entrySetView = entrySetView;
        }
        return entrySetView;
    }
    
    public Iterator<Entry<K, V>> entrySetIterator() {
        final Map<K, V> delegateOrNull = this.delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.entrySet().iterator();
        }
        return new e(this) {
            public final CompactHashMap e;
            
            public Entry e(final int n) {
                return this.e.new g(n);
            }
        };
    }
    
    public int firstEntryIndex() {
        int n;
        if (this.isEmpty()) {
            n = -1;
        }
        else {
            n = 0;
        }
        return n;
    }
    
    @Override
    public V get(final Object o) {
        final Map<K, V> delegateOrNull = this.delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.get(o);
        }
        final int index = this.indexOf(o);
        if (index == -1) {
            return null;
        }
        this.accessEntry(index);
        return this.value(index);
    }
    
    public int getSuccessor(int n) {
        if (++n >= this.size) {
            n = -1;
        }
        return n;
    }
    
    public void incrementModCount() {
        this.metadata += 32;
    }
    
    public void init(final int n) {
        i71.e(n >= 0, "Expected size must be >= 0");
        this.metadata = Ints.e(n, 1, 1073741823);
    }
    
    public void insertEntry(final int n, final K k, final V v, final int n2, final int n3) {
        this.setEntry(n, ii.d(n2, 0, n3));
        this.setKey(n, k);
        this.setValue(n, v);
    }
    
    @Override
    public boolean isEmpty() {
        return this.size() == 0;
    }
    
    @Override
    public Set<K> keySet() {
        Set<K> keySetView;
        if ((keySetView = this.keySetView) == null) {
            keySetView = this.createKeySet();
            this.keySetView = keySetView;
        }
        return keySetView;
    }
    
    public Iterator<K> keySetIterator() {
        final Map<K, V> delegateOrNull = this.delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.keySet().iterator();
        }
        return new e(this) {
            public final CompactHashMap e;
            
            @Override
            public Object c(final int n) {
                return this.e.key(n);
            }
        };
    }
    
    public void moveLastEntry(final int n, final int n2) {
        final Object requireTable = this.requireTable();
        final int[] requireEntries = this.requireEntries();
        final Object[] requireKeys = this.requireKeys();
        final Object[] requireValues = this.requireValues();
        final int n3 = this.size() - 1;
        if (n < n3) {
            final Object o = requireKeys[n3];
            requireKeys[n] = o;
            requireValues[n] = requireValues[n3];
            requireValues[n3] = (requireKeys[n3] = null);
            requireEntries[n] = requireEntries[n3];
            requireEntries[n3] = 0;
            final int n4 = rc0.d(o) & n2;
            final int h = ii.h(requireTable, n4);
            final int n5 = n3 + 1;
            int c;
            if ((c = h) == n5) {
                ii.i(requireTable, n4, n + 1);
            }
            else {
                int n6;
                int n7;
                do {
                    n6 = c - 1;
                    n7 = requireEntries[n6];
                    c = ii.c(n7, n2);
                } while (c != n5);
                requireEntries[n6] = ii.d(n7, n + 1, n2);
            }
        }
        else {
            requireValues[n] = (requireKeys[n] = null);
            requireEntries[n] = 0;
        }
    }
    
    public boolean needsAllocArrays() {
        return this.table == null;
    }
    
    @Override
    public V put(final K k, final V v) {
        if (this.needsAllocArrays()) {
            this.allocArrays();
        }
        final Map<K, V> delegateOrNull = this.delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.put(k, v);
        }
        final int[] requireEntries = this.requireEntries();
        final Object[] requireKeys = this.requireKeys();
        final Object[] requireValues = this.requireValues();
        final int size = this.size;
        final int size2 = size + 1;
        final int d = rc0.d(k);
        final int hashTableMask = this.hashTableMask();
        final int n = d & hashTableMask;
        int n2 = ii.h(this.requireTable(), n);
        int resizeTable = 0;
        Label_0265: {
            Label_0262: {
                if (n2 == 0) {
                    if (size2 <= hashTableMask) {
                        ii.i(this.requireTable(), n, size2);
                        break Label_0262;
                    }
                }
                else {
                    final int b = ii.b(d, hashTableMask);
                    int n3 = 0;
                    while (true) {
                        final int n4 = n2 - 1;
                        final int n5 = requireEntries[n4];
                        if (ii.b(n5, hashTableMask) == b && b11.a(k, requireKeys[n4])) {
                            final Object o = requireValues[n4];
                            requireValues[n4] = v;
                            this.accessEntry(n4);
                            return (V)o;
                        }
                        n2 = ii.c(n5, hashTableMask);
                        ++n3;
                        if (n2 != 0) {
                            continue;
                        }
                        if (n3 >= 9) {
                            return this.convertToHashFloodingResistantImplementation().put(k, v);
                        }
                        if (size2 > hashTableMask) {
                            break;
                        }
                        requireEntries[n4] = ii.d(n5, size2, hashTableMask);
                        break Label_0262;
                    }
                }
                resizeTable = this.resizeTable(hashTableMask, ii.e(hashTableMask), d, size);
                break Label_0265;
            }
            resizeTable = hashTableMask;
        }
        this.resizeMeMaybe(size2);
        this.insertEntry(size, k, v, d, resizeTable);
        this.size = size2;
        this.incrementModCount();
        return null;
    }
    
    @Override
    public V remove(Object removeHelper) {
        final Map<K, V> delegateOrNull = this.delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.remove(removeHelper);
        }
        if ((removeHelper = this.removeHelper(removeHelper)) == CompactHashMap.NOT_FOUND) {
            removeHelper = null;
        }
        return (V)removeHelper;
    }
    
    public void resizeEntries(final int newLength) {
        this.entries = Arrays.copyOf(this.requireEntries(), newLength);
        this.keys = Arrays.copyOf(this.requireKeys(), newLength);
        this.values = Arrays.copyOf(this.requireValues(), newLength);
    }
    
    @Override
    public int size() {
        final Map<K, V> delegateOrNull = this.delegateOrNull();
        int n;
        if (delegateOrNull != null) {
            n = delegateOrNull.size();
        }
        else {
            n = this.size;
        }
        return n;
    }
    
    public void trimToSize() {
        if (this.needsAllocArrays()) {
            return;
        }
        final Map<K, V> delegateOrNull = (Map<K, V>)this.delegateOrNull();
        if (delegateOrNull != null) {
            final Map<K, V> hashFloodingResistantDelegate = this.createHashFloodingResistantDelegate(this.size());
            hashFloodingResistantDelegate.putAll((Map<? extends K, ? extends V>)delegateOrNull);
            this.table = hashFloodingResistantDelegate;
            return;
        }
        final int size = this.size;
        if (size < this.requireEntries().length) {
            this.resizeEntries(size);
        }
        final int j = ii.j(size);
        final int hashTableMask = this.hashTableMask();
        if (j < hashTableMask) {
            this.resizeTable(hashTableMask, j, 0, 0);
        }
    }
    
    @Override
    public Collection<V> values() {
        Collection<V> valuesView;
        if ((valuesView = this.valuesView) == null) {
            valuesView = this.createValues();
            this.valuesView = valuesView;
        }
        return valuesView;
    }
    
    public Iterator<V> valuesIterator() {
        final Map<K, V> delegateOrNull = this.delegateOrNull();
        if (delegateOrNull != null) {
            return delegateOrNull.values().iterator();
        }
        return new e(this) {
            public final CompactHashMap e;
            
            @Override
            public Object c(final int n) {
                return this.e.value(n);
            }
        };
    }
    
    public class d extends AbstractSet
    {
        public final CompactHashMap a;
        
        public d(final CompactHashMap a) {
            this.a = a;
        }
        
        @Override
        public void clear() {
            this.a.clear();
        }
        
        @Override
        public boolean contains(final Object o) {
            final Map<K, V> delegateOrNull = this.a.delegateOrNull();
            if (delegateOrNull != null) {
                return delegateOrNull.entrySet().contains(o);
            }
            final boolean b = o instanceof Entry;
            boolean b3;
            final boolean b2 = b3 = false;
            if (b) {
                final Entry entry = (Entry)o;
                final int access$500 = this.a.indexOf(entry.getKey());
                b3 = b2;
                if (access$500 != -1) {
                    b3 = b2;
                    if (b11.a(this.a.value(access$500), entry.getValue())) {
                        b3 = true;
                    }
                }
            }
            return b3;
        }
        
        @Override
        public Iterator iterator() {
            return this.a.entrySetIterator();
        }
        
        @Override
        public boolean remove(final Object o) {
            final Map<K, V> delegateOrNull = this.a.delegateOrNull();
            if (delegateOrNull != null) {
                return delegateOrNull.entrySet().remove(o);
            }
            if (!(o instanceof Entry)) {
                return false;
            }
            final Entry entry = (Entry)o;
            if (this.a.needsAllocArrays()) {
                return false;
            }
            final int access$700 = this.a.hashTableMask();
            final int f = ii.f(entry.getKey(), entry.getValue(), access$700, this.a.requireTable(), this.a.requireEntries(), this.a.requireKeys(), this.a.requireValues());
            if (f == -1) {
                return false;
            }
            this.a.moveLastEntry(f, access$700);
            CompactHashMap.access$1210(this.a);
            this.a.incrementModCount();
            return true;
        }
        
        @Override
        public int size() {
            return this.a.size();
        }
    }
    
    public abstract class e implements Iterator
    {
        public int a;
        public int b;
        public int c;
        public final CompactHashMap d;
        
        public e(final CompactHashMap d) {
            this.d = d;
            this.a = CompactHashMap.access$000(d);
            this.b = d.firstEntryIndex();
            this.c = -1;
        }
        
        public final void b() {
            if (CompactHashMap.access$000(this.d) == this.a) {
                return;
            }
            throw new ConcurrentModificationException();
        }
        
        public abstract Object c(final int p0);
        
        public void d() {
            this.a += 32;
        }
        
        @Override
        public boolean hasNext() {
            return this.b >= 0;
        }
        
        @Override
        public Object next() {
            this.b();
            if (this.hasNext()) {
                final int b = this.b;
                this.c = b;
                final Object c = this.c(b);
                this.b = this.d.getSuccessor(this.b);
                return c;
            }
            throw new NoSuchElementException();
        }
        
        @Override
        public void remove() {
            this.b();
            hh.e(this.c >= 0);
            this.d();
            final CompactHashMap d = this.d;
            d.remove(d.key(this.c));
            this.b = this.d.adjustAfterRemove(this.b, this.c);
            this.c = -1;
        }
    }
    
    public class f extends AbstractSet
    {
        public final CompactHashMap a;
        
        public f(final CompactHashMap a) {
            this.a = a;
        }
        
        @Override
        public void clear() {
            this.a.clear();
        }
        
        @Override
        public boolean contains(final Object o) {
            return this.a.containsKey(o);
        }
        
        @Override
        public Iterator iterator() {
            return this.a.keySetIterator();
        }
        
        @Override
        public boolean remove(final Object o) {
            final Map<K, V> delegateOrNull = this.a.delegateOrNull();
            boolean remove;
            if (delegateOrNull != null) {
                remove = delegateOrNull.keySet().remove(o);
            }
            else {
                remove = (this.a.removeHelper(o) != CompactHashMap.access$300());
            }
            return remove;
        }
        
        @Override
        public int size() {
            return this.a.size();
        }
    }
    
    public final class g extends s
    {
        public final Object a;
        public int b;
        public final CompactHashMap c;
        
        public g(final CompactHashMap c, final int b) {
            this.c = c;
            this.a = c.key(b);
            this.b = b;
        }
        
        public final void a() {
            final int b = this.b;
            if (b == -1 || b >= this.c.size() || !b11.a(this.a, this.c.key(this.b))) {
                this.b = this.c.indexOf(this.a);
            }
        }
        
        @Override
        public Object getKey() {
            return this.a;
        }
        
        @Override
        public Object getValue() {
            final Map<K, V> delegateOrNull = this.c.delegateOrNull();
            if (delegateOrNull != null) {
                return k01.a(delegateOrNull.get(this.a));
            }
            this.a();
            final int b = this.b;
            Object o;
            if (b == -1) {
                o = k01.b();
            }
            else {
                o = this.c.value(b);
            }
            return o;
        }
        
        @Override
        public Object setValue(final Object o) {
            final Map<K, V> delegateOrNull = this.c.delegateOrNull();
            if (delegateOrNull != null) {
                return k01.a(delegateOrNull.put((K)this.a, (V)o));
            }
            this.a();
            final int b = this.b;
            if (b == -1) {
                this.c.put(this.a, o);
                return k01.b();
            }
            final Object access$600 = this.c.value(b);
            this.c.setValue(this.b, o);
            return access$600;
        }
    }
    
    public class h extends AbstractCollection
    {
        public final CompactHashMap a;
        
        public h(final CompactHashMap a) {
            this.a = a;
        }
        
        @Override
        public void clear() {
            this.a.clear();
        }
        
        @Override
        public Iterator iterator() {
            return this.a.valuesIterator();
        }
        
        @Override
        public int size() {
            return this.a.size();
        }
    }
}

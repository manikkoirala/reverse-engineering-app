// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Collection;
import java.util.List;
import java.io.Serializable;

final class ExplicitOrdering<T> extends Ordering implements Serializable
{
    private static final long serialVersionUID = 0L;
    final ImmutableMap<T, Integer> rankMap;
    
    public ExplicitOrdering(final ImmutableMap<T, Integer> rankMap) {
        this.rankMap = rankMap;
    }
    
    public ExplicitOrdering(final List<T> list) {
        this(Maps.k(list));
    }
    
    private int rank(final T t) {
        final Integer n = this.rankMap.get(t);
        if (n != null) {
            return n;
        }
        throw new IncomparableValueException(t);
    }
    
    @Override
    public int compare(final T t, final T t2) {
        return this.rank(t) - this.rank(t2);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof ExplicitOrdering && this.rankMap.equals(((ExplicitOrdering)o).rankMap);
    }
    
    @Override
    public int hashCode() {
        return this.rankMap.hashCode();
    }
    
    @Override
    public String toString() {
        final String value = String.valueOf(this.rankMap.keySet());
        final StringBuilder sb = new StringBuilder(value.length() + 19);
        sb.append("Ordering.explicit(");
        sb.append(value);
        sb.append(")");
        return sb.toString();
    }
}

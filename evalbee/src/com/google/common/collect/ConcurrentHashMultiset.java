// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import com.google.common.primitives.Ints;
import java.util.Set;
import java.util.Map;
import java.io.ObjectOutputStream;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import java.io.ObjectInputStream;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.ConcurrentMap;
import java.io.Serializable;

public final class ConcurrentHashMultiset<E> extends b implements Serializable
{
    private static final long serialVersionUID = 1L;
    private final transient ConcurrentMap<E, AtomicInteger> countMap;
    
    public ConcurrentHashMultiset(final ConcurrentMap<E, AtomicInteger> countMap) {
        i71.m(countMap.isEmpty(), "the backing map (%s) must be empty", countMap);
        this.countMap = countMap;
    }
    
    public static /* synthetic */ ConcurrentMap access$100(final ConcurrentHashMultiset concurrentHashMultiset) {
        return concurrentHashMultiset.countMap;
    }
    
    public static <E> ConcurrentHashMultiset<E> create() {
        return new ConcurrentHashMultiset<E>(new ConcurrentHashMap<E, AtomicInteger>());
    }
    
    public static <E> ConcurrentHashMultiset<E> create(final Iterable<? extends E> iterable) {
        final ConcurrentHashMultiset<Object> create = create();
        rg0.a(create, iterable);
        return (ConcurrentHashMultiset<E>)create;
    }
    
    public static <E> ConcurrentHashMultiset<E> create(final ConcurrentMap<E, AtomicInteger> concurrentMap) {
        return new ConcurrentHashMultiset<E>(concurrentMap);
    }
    
    private void readObject(final ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        e.a.b(this, objectInputStream.readObject());
    }
    
    private List<E> snapshot() {
        final ArrayList m = Lists.m(this.size());
        for (final j.a a : this.entrySet()) {
            final Object element = a.getElement();
            for (int i = a.getCount(); i > 0; --i) {
                m.add(element);
            }
        }
        return m;
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(this.countMap);
    }
    
    @Override
    public int add(final E e, final int initialValue) {
        i71.r(e);
        if (initialValue == 0) {
            return this.count(e);
        }
        hh.d(initialValue, "occurrences");
        AtomicInteger atomicInteger;
        while ((atomicInteger = (AtomicInteger)Maps.w(this.countMap, e)) != null || (atomicInteger = this.countMap.putIfAbsent(e, new AtomicInteger(initialValue))) != null) {
            while (true) {
                final int value = atomicInteger.get();
                if (value != 0) {
                    try {
                        if (atomicInteger.compareAndSet(value, tf0.a(value, initialValue))) {
                            return value;
                        }
                        continue;
                    }
                    catch (final ArithmeticException ex) {
                        final StringBuilder sb = new StringBuilder(65);
                        sb.append("Overflow adding ");
                        sb.append(initialValue);
                        sb.append(" occurrences to a count of ");
                        sb.append(value);
                        throw new IllegalArgumentException(sb.toString());
                    }
                    break;
                }
                break;
            }
            final AtomicInteger atomicInteger2 = new AtomicInteger(initialValue);
            if (this.countMap.putIfAbsent(e, atomicInteger2) == null || this.countMap.replace(e, atomicInteger, atomicInteger2)) {
                return 0;
            }
        }
        return 0;
    }
    
    @Override
    public void clear() {
        this.countMap.clear();
    }
    
    @Override
    public int count(final Object o) {
        final AtomicInteger atomicInteger = (AtomicInteger)Maps.w(this.countMap, o);
        int value;
        if (atomicInteger == null) {
            value = 0;
        }
        else {
            value = atomicInteger.get();
        }
        return value;
    }
    
    @Override
    public Set<E> createElementSet() {
        return new c80(this, this.countMap.keySet()) {
            public final Set a;
            
            @Override
            public boolean contains(final Object o) {
                return o != null && lh.c(this.a, o);
            }
            
            @Override
            public boolean containsAll(final Collection collection) {
                return this.standardContainsAll(collection);
            }
            
            @Override
            public Set delegate() {
                return this.a;
            }
            
            @Override
            public boolean remove(final Object o) {
                return o != null && lh.d(this.a, o);
            }
            
            @Override
            public boolean removeAll(final Collection collection) {
                return this.standardRemoveAll(collection);
            }
        };
    }
    
    @Deprecated
    public Set<j.a> createEntrySet() {
        return new d(null);
    }
    
    @Override
    public int distinctElements() {
        return this.countMap.size();
    }
    
    @Override
    public Iterator<E> elementIterator() {
        throw new AssertionError((Object)"should never be called");
    }
    
    public Iterator<j.a> entryIterator() {
        return new v70(this, new AbstractIterator(this) {
            public final Iterator c = ConcurrentHashMultiset.access$100(d).entrySet().iterator();
            public final ConcurrentHashMultiset d;
            
            public j.a e() {
                while (this.c.hasNext()) {
                    final Map.Entry<K, AtomicInteger> entry = this.c.next();
                    final int value = entry.getValue().get();
                    if (value != 0) {
                        return Multisets.g(entry.getKey(), value);
                    }
                }
                return (j.a)this.c();
            }
        }) {
            public j.a a;
            public final Iterator b;
            public final ConcurrentHashMultiset c;
            
            @Override
            public Iterator b() {
                return this.b;
            }
            
            public j.a c() {
                return this.a = (j.a)super.next();
            }
            
            @Override
            public void remove() {
                i71.y(this.a != null, "no calls to next() since the last call to remove()");
                this.c.setCount(this.a.getElement(), 0);
                this.a = null;
            }
        };
    }
    
    @Override
    public boolean isEmpty() {
        return this.countMap.isEmpty();
    }
    
    @Override
    public Iterator<E> iterator() {
        return Multisets.i(this);
    }
    
    @Override
    public int remove(final Object o, final int n) {
        if (n == 0) {
            return this.count(o);
        }
        hh.d(n, "occurrences");
        final AtomicInteger atomicInteger = (AtomicInteger)Maps.w(this.countMap, o);
        if (atomicInteger == null) {
            return 0;
        }
        int value;
        int max;
        do {
            value = atomicInteger.get();
            if (value == 0) {
                return 0;
            }
            max = Math.max(0, value - n);
        } while (!atomicInteger.compareAndSet(value, max));
        if (max == 0) {
            this.countMap.remove(o, atomicInteger);
        }
        return value;
    }
    
    public boolean removeExactly(final Object o, final int n) {
        if (n == 0) {
            return true;
        }
        hh.d(n, "occurrences");
        final AtomicInteger atomicInteger = (AtomicInteger)Maps.w(this.countMap, o);
        if (atomicInteger == null) {
            return false;
        }
        int value;
        int newValue;
        do {
            value = atomicInteger.get();
            if (value < n) {
                return false;
            }
            newValue = value - n;
        } while (!atomicInteger.compareAndSet(value, newValue));
        if (newValue == 0) {
            this.countMap.remove(o, atomicInteger);
        }
        return true;
    }
    
    @Override
    public int setCount(final E e, final int initialValue) {
        i71.r(e);
        hh.b(initialValue, "count");
    Label_0013:
        while (true) {
            AtomicInteger atomicInteger;
            if ((atomicInteger = (AtomicInteger)Maps.w(this.countMap, e)) == null) {
                if (initialValue == 0) {
                    return 0;
                }
                if ((atomicInteger = this.countMap.putIfAbsent(e, new AtomicInteger(initialValue))) == null) {
                    return 0;
                }
            }
            int value;
            do {
                value = atomicInteger.get();
                if (value == 0) {
                    if (initialValue == 0) {
                        return 0;
                    }
                    final AtomicInteger atomicInteger2 = new AtomicInteger(initialValue);
                    if (this.countMap.putIfAbsent(e, atomicInteger2) == null || this.countMap.replace(e, atomicInteger, atomicInteger2)) {
                        return 0;
                    }
                    continue Label_0013;
                }
            } while (!atomicInteger.compareAndSet(value, initialValue));
            if (initialValue == 0) {
                this.countMap.remove(e, atomicInteger);
            }
            return value;
        }
    }
    
    @Override
    public boolean setCount(final E e, final int n, final int initialValue) {
        i71.r(e);
        hh.b(n, "oldCount");
        hh.b(initialValue, "newCount");
        final AtomicInteger atomicInteger = (AtomicInteger)Maps.w(this.countMap, e);
        final boolean b = false;
        boolean b2 = false;
        if (atomicInteger != null) {
            final int value = atomicInteger.get();
            if (value == n) {
                if (value == 0) {
                    if (initialValue == 0) {
                        this.countMap.remove(e, atomicInteger);
                        return true;
                    }
                    final AtomicInteger atomicInteger2 = new AtomicInteger(initialValue);
                    if (this.countMap.putIfAbsent(e, atomicInteger2) != null) {
                        final boolean b3 = b;
                        if (!this.countMap.replace(e, atomicInteger, atomicInteger2)) {
                            return b3;
                        }
                    }
                    return true;
                }
                else if (atomicInteger.compareAndSet(value, initialValue)) {
                    if (initialValue == 0) {
                        this.countMap.remove(e, atomicInteger);
                    }
                    return true;
                }
            }
            return false;
        }
        if (n != 0) {
            return false;
        }
        if (initialValue == 0) {
            return true;
        }
        if (this.countMap.putIfAbsent(e, new AtomicInteger(initialValue)) == null) {
            b2 = true;
        }
        return b2;
    }
    
    @Override
    public int size() {
        final Iterator<Object> iterator = (Iterator<Object>)this.countMap.values().iterator();
        long n = 0L;
        while (iterator.hasNext()) {
            n += iterator.next().get();
        }
        return Ints.k(n);
    }
    
    @Override
    public Object[] toArray() {
        return this.snapshot().toArray();
    }
    
    @Override
    public <T> T[] toArray(final T[] array) {
        return this.snapshot().toArray(array);
    }
    
    public class d extends b
    {
        public final ConcurrentHashMultiset b;
        
        public d(final ConcurrentHashMultiset b) {
            this.b = b.super();
        }
        
        public ConcurrentHashMultiset b() {
            return this.b;
        }
        
        public final List c() {
            final ArrayList m = Lists.m(((b)this).size());
            Iterators.a(m, ((b)this).iterator());
            return m;
        }
        
        @Override
        public Object[] toArray() {
            return this.c().toArray();
        }
        
        @Override
        public Object[] toArray(final Object[] array) {
            return this.c().toArray(array);
        }
    }
    
    public abstract static class e
    {
        public static final n.b a;
        
        static {
            a = n.a(ConcurrentHashMultiset.class, "countMap");
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Set;
import java.util.Collection;

public final class LinkedHashMultiset<E> extends AbstractMapBasedMultiset<E>
{
    public LinkedHashMultiset(final int n) {
        super(n);
    }
    
    public static <E> LinkedHashMultiset<E> create() {
        return create(3);
    }
    
    public static <E> LinkedHashMultiset<E> create(final int n) {
        return new LinkedHashMultiset<E>(n);
    }
    
    public static <E> LinkedHashMultiset<E> create(final Iterable<? extends E> iterable) {
        final LinkedHashMultiset<Object> create = create(Multisets.h(iterable));
        rg0.a(create, iterable);
        return (LinkedHashMultiset<E>)create;
    }
    
    @Override
    public k newBackingMap(final int n) {
        return new v01(n);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Set;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.util.Map;
import java.util.EnumMap;

public final class EnumBiMap<K extends Enum<K>, V extends Enum<V>> extends AbstractBiMap<K, V>
{
    private static final long serialVersionUID = 0L;
    private transient Class<K> keyType;
    private transient Class<V> valueType;
    
    private EnumBiMap(final Class<K> clazz, final Class<V> clazz2) {
        super(new EnumMap(clazz), new EnumMap(clazz2));
        this.keyType = clazz;
        this.valueType = clazz2;
    }
    
    public static <K extends Enum<K>, V extends Enum<V>> EnumBiMap<K, V> create(final Class<K> clazz, final Class<V> clazz2) {
        return new EnumBiMap<K, V>(clazz, clazz2);
    }
    
    public static <K extends Enum<K>, V extends Enum<V>> EnumBiMap<K, V> create(final Map<K, V> map) {
        final EnumBiMap<Enum, Enum> create = create((Class<Enum>)inferKeyType((Map<K, ?>)map), (Class<Enum>)inferValueType((Map<?, V>)map));
        create.putAll(map);
        return (EnumBiMap<K, V>)create;
    }
    
    public static <K extends Enum<K>> Class<K> inferKeyType(final Map<K, ?> map) {
        if (map instanceof EnumBiMap) {
            return ((EnumBiMap)map).keyType();
        }
        if (map instanceof EnumHashBiMap) {
            return ((EnumHashBiMap)map).keyType();
        }
        i71.d(map.isEmpty() ^ true);
        return map.keySet().iterator().next().getDeclaringClass();
    }
    
    private static <V extends Enum<V>> Class<V> inferValueType(final Map<?, V> map) {
        if (map instanceof EnumBiMap) {
            return (Class<V>)((EnumBiMap)map).valueType;
        }
        i71.d(map.isEmpty() ^ true);
        return ((Enum)map.values().iterator().next()).getDeclaringClass();
    }
    
    private void readObject(final ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        this.keyType = (Class)objectInputStream.readObject();
        this.valueType = (Class)objectInputStream.readObject();
        this.setDelegates(new EnumMap<K, V>(this.keyType), new EnumMap<V, K>(this.valueType));
        n.b(this, objectInputStream);
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(this.keyType);
        objectOutputStream.writeObject(this.valueType);
        n.i(this, objectOutputStream);
    }
    
    @Override
    public K checkKey(final K k) {
        return (K)i71.r(k);
    }
    
    @Override
    public V checkValue(final V v) {
        return (V)i71.r(v);
    }
    
    public Class<K> keyType() {
        return this.keyType;
    }
    
    public Class<V> valueType() {
        return this.valueType;
    }
}

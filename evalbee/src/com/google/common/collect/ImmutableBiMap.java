// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Set;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

public abstract class ImmutableBiMap<K, V> extends ImmutableMap<K, V> implements ub
{
    public static <K, V> a builder() {
        return new a();
    }
    
    public static <K, V> a builderWithExpectedSize(final int n) {
        hh.b(n, "expectedSize");
        return new a(n);
    }
    
    public static <K, V> ImmutableBiMap<K, V> copyOf(final Iterable<? extends Entry<? extends K, ? extends V>> iterable) {
        int size;
        if (iterable instanceof Collection) {
            size = ((Collection)iterable).size();
        }
        else {
            size = 4;
        }
        return new a(size).q(iterable).l();
    }
    
    public static <K, V> ImmutableBiMap<K, V> copyOf(final Map<? extends K, ? extends V> map) {
        if (map instanceof ImmutableBiMap) {
            final ImmutableBiMap immutableBiMap = (ImmutableBiMap)map;
            if (!immutableBiMap.isPartialView()) {
                return immutableBiMap;
            }
        }
        return copyOf((Iterable<? extends Entry<? extends K, ? extends V>>)map.entrySet());
    }
    
    public static <K, V> ImmutableBiMap<K, V> of() {
        return (ImmutableBiMap<K, V>)RegularImmutableBiMap.EMPTY;
    }
    
    public static <K, V> ImmutableBiMap<K, V> of(final K k, final V v) {
        hh.a(k, v);
        return new RegularImmutableBiMap<K, V>(new Object[] { k, v }, 1);
    }
    
    public static <K, V> ImmutableBiMap<K, V> of(final K k, final V v, final K i, final V v2) {
        hh.a(k, v);
        hh.a(i, v2);
        return new RegularImmutableBiMap<K, V>(new Object[] { k, v, i, v2 }, 2);
    }
    
    public static <K, V> ImmutableBiMap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3) {
        hh.a(k, v);
        hh.a(i, v2);
        hh.a(j, v3);
        return new RegularImmutableBiMap<K, V>(new Object[] { k, v, i, v2, j, v3 }, 3);
    }
    
    public static <K, V> ImmutableBiMap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4) {
        hh.a(k, v);
        hh.a(i, v2);
        hh.a(j, v3);
        hh.a(l, v4);
        return new RegularImmutableBiMap<K, V>(new Object[] { k, v, i, v2, j, v3, l, v4 }, 4);
    }
    
    public static <K, V> ImmutableBiMap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4, final K m, final V v5) {
        hh.a(k, v);
        hh.a(i, v2);
        hh.a(j, v3);
        hh.a(l, v4);
        hh.a(m, v5);
        return new RegularImmutableBiMap<K, V>(new Object[] { k, v, i, v2, j, v3, l, v4, m, v5 }, 5);
    }
    
    public static <K, V> ImmutableBiMap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4, final K m, final V v5, final K k2, final V v6) {
        hh.a(k, v);
        hh.a(i, v2);
        hh.a(j, v3);
        hh.a(l, v4);
        hh.a(m, v5);
        hh.a(k2, v6);
        return new RegularImmutableBiMap<K, V>(new Object[] { k, v, i, v2, j, v3, l, v4, m, v5, k2, v6 }, 6);
    }
    
    public static <K, V> ImmutableBiMap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4, final K m, final V v5, final K k2, final V v6, final K k3, final V v7) {
        hh.a(k, v);
        hh.a(i, v2);
        hh.a(j, v3);
        hh.a(l, v4);
        hh.a(m, v5);
        hh.a(k2, v6);
        hh.a(k3, v7);
        return new RegularImmutableBiMap<K, V>(new Object[] { k, v, i, v2, j, v3, l, v4, m, v5, k2, v6, k3, v7 }, 7);
    }
    
    public static <K, V> ImmutableBiMap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4, final K m, final V v5, final K k2, final V v6, final K k3, final V v7, final K k4, final V v8) {
        hh.a(k, v);
        hh.a(i, v2);
        hh.a(j, v3);
        hh.a(l, v4);
        hh.a(m, v5);
        hh.a(k2, v6);
        hh.a(k3, v7);
        hh.a(k4, v8);
        return new RegularImmutableBiMap<K, V>(new Object[] { k, v, i, v2, j, v3, l, v4, m, v5, k2, v6, k3, v7, k4, v8 }, 8);
    }
    
    public static <K, V> ImmutableBiMap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4, final K m, final V v5, final K k2, final V v6, final K k3, final V v7, final K k4, final V v8, final K k5, final V v9) {
        hh.a(k, v);
        hh.a(i, v2);
        hh.a(j, v3);
        hh.a(l, v4);
        hh.a(m, v5);
        hh.a(k2, v6);
        hh.a(k3, v7);
        hh.a(k4, v8);
        hh.a(k5, v9);
        return new RegularImmutableBiMap<K, V>(new Object[] { k, v, i, v2, j, v3, l, v4, m, v5, k2, v6, k3, v7, k4, v8, k5, v9 }, 9);
    }
    
    public static <K, V> ImmutableBiMap<K, V> of(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4, final K m, final V v5, final K k2, final V v6, final K k3, final V v7, final K k4, final V v8, final K k5, final V v9, final K k6, final V v10) {
        hh.a(k, v);
        hh.a(i, v2);
        hh.a(j, v3);
        hh.a(l, v4);
        hh.a(m, v5);
        hh.a(k2, v6);
        hh.a(k3, v7);
        hh.a(k4, v8);
        hh.a(k5, v9);
        hh.a(k6, v10);
        return new RegularImmutableBiMap<K, V>(new Object[] { k, v, i, v2, j, v3, l, v4, m, v5, k2, v6, k3, v7, k4, v8, k5, v9, k6, v10 }, 10);
    }
    
    @SafeVarargs
    public static <K, V> ImmutableBiMap<K, V> ofEntries(final Entry<? extends K, ? extends V>... a) {
        return copyOf((Iterable<? extends Entry<? extends K, ? extends V>>)Arrays.asList(a));
    }
    
    @Override
    public final ImmutableSet<V> createValues() {
        throw new AssertionError((Object)"should never be called");
    }
    
    @Deprecated
    @Override
    public final V forcePut(final K k, final V v) {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public abstract ImmutableBiMap<V, K> inverse();
    
    @Override
    public ImmutableSet<V> values() {
        return (ImmutableSet<V>)this.inverse().keySet();
    }
    
    public Object writeReplace() {
        return new SerializedForm((ImmutableBiMap<Object, Object>)this);
    }
    
    public static class SerializedForm<K, V> extends ImmutableMap.SerializedForm<K, V>
    {
        private static final long serialVersionUID = 0L;
        
        public SerializedForm(final ImmutableBiMap<K, V> immutableBiMap) {
            super(immutableBiMap);
        }
        
        public ImmutableBiMap.a makeBuilder(final int n) {
            return new ImmutableBiMap.a(n);
        }
    }
    
    public static final class a extends b
    {
        public a() {
        }
        
        public a(final int n) {
            super(n);
        }
        
        public ImmutableBiMap l() {
            return this.n();
        }
        
        public ImmutableBiMap m() {
            throw new UnsupportedOperationException("Not supported for bimaps");
        }
        
        public ImmutableBiMap n() {
            final int c = super.c;
            if (c == 0) {
                return ImmutableBiMap.of();
            }
            if (super.a != null) {
                if (super.d) {
                    super.b = Arrays.copyOf(super.b, c * 2);
                }
                ImmutableMap.b.k(super.b, super.c, super.a);
            }
            super.d = true;
            return new RegularImmutableBiMap(super.b, super.c);
        }
        
        public a o(final Object o, final Object o2) {
            super.g(o, o2);
            return this;
        }
        
        public a p(final Entry entry) {
            super.h(entry);
            return this;
        }
        
        public a q(final Iterable iterable) {
            super.i(iterable);
            return this;
        }
        
        public a r(final Map map) {
            super.j(map);
            return this;
        }
    }
}

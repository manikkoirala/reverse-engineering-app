// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.SortedMap;
import java.util.Comparator;
import com.google.common.base.a;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;
import java.util.NavigableMap;
import java.util.Set;
import java.io.Serializable;

public class TreeRangeSet<C extends Comparable<?>> extends y implements Serializable
{
    private transient Set<Range<C>> asDescendingSetOfRanges;
    private transient Set<Range<C>> asRanges;
    private transient gc1 complement;
    final NavigableMap<Cut<C>, Range<C>> rangesByLowerBound;
    
    private TreeRangeSet(final NavigableMap<Cut<C>, Range<C>> rangesByLowerBound) {
        this.rangesByLowerBound = rangesByLowerBound;
    }
    
    public static <C extends Comparable<?>> TreeRangeSet<C> create() {
        return new TreeRangeSet<C>(new TreeMap<Cut<C>, Range<C>>());
    }
    
    public static <C extends Comparable<?>> TreeRangeSet<C> create(final gc1 gc1) {
        final TreeRangeSet<Comparable> create = create();
        create.addAll(gc1);
        return (TreeRangeSet<C>)create;
    }
    
    public static <C extends Comparable<?>> TreeRangeSet<C> create(final Iterable<Range<C>> iterable) {
        final TreeRangeSet<Comparable> create = create();
        create.addAll(iterable);
        return (TreeRangeSet<C>)create;
    }
    
    private Range<C> rangeEnclosing(final Range<C> range) {
        i71.r(range);
        final Map.Entry<Cut<C>, Range> floorEntry = this.rangesByLowerBound.floorEntry(range.lowerBound);
        Range range2;
        if (floorEntry != null && floorEntry.getValue().encloses(range)) {
            range2 = floorEntry.getValue();
        }
        else {
            range2 = null;
        }
        return range2;
    }
    
    private void replaceRangeWithSameLowerBound(final Range<C> range) {
        if (range.isEmpty()) {
            this.rangesByLowerBound.remove(range.lowerBound);
        }
        else {
            this.rangesByLowerBound.put(range.lowerBound, range);
        }
    }
    
    @Override
    public void add(final Range<C> range) {
        i71.r(range);
        if (range.isEmpty()) {
            return;
        }
        final Cut<C> lowerBound = (Cut<C>)range.lowerBound;
        final Cut<C> upperBound = (Cut<C>)range.upperBound;
        final Map.Entry<Cut<C>, Range<C>> lowerEntry = this.rangesByLowerBound.lowerEntry((Cut<C>)lowerBound);
        Cut<C> lowerBound2 = (Cut<C>)lowerBound;
        Object upperBound2 = upperBound;
        if (lowerEntry != null) {
            final Range range2 = lowerEntry.getValue();
            lowerBound2 = (Cut<C>)lowerBound;
            upperBound2 = upperBound;
            if (range2.upperBound.compareTo(lowerBound) >= 0) {
                upperBound2 = upperBound;
                if (range2.upperBound.compareTo(upperBound) >= 0) {
                    upperBound2 = range2.upperBound;
                }
                lowerBound2 = (Cut<C>)range2.lowerBound;
            }
        }
        final Map.Entry<Cut<C>, Range<C>> floorEntry = this.rangesByLowerBound.floorEntry((Cut<C>)upperBound2);
        Cut<C> upperBound3 = (Cut<C>)upperBound2;
        if (floorEntry != null) {
            final Range range3 = floorEntry.getValue();
            upperBound3 = (Cut<C>)upperBound2;
            if (range3.upperBound.compareTo((Cut<C>)upperBound2) >= 0) {
                upperBound3 = (Cut<C>)range3.upperBound;
            }
        }
        this.rangesByLowerBound.subMap(lowerBound2, upperBound3).clear();
        this.replaceRangeWithSameLowerBound(Range.create(lowerBound2, upperBound3));
    }
    
    public Set<Range<C>> asDescendingSetOfRanges() {
        Set<Range<C>> asDescendingSetOfRanges;
        if ((asDescendingSetOfRanges = this.asDescendingSetOfRanges) == null) {
            asDescendingSetOfRanges = new b(this.rangesByLowerBound.descendingMap().values());
            this.asDescendingSetOfRanges = asDescendingSetOfRanges;
        }
        return asDescendingSetOfRanges;
    }
    
    @Override
    public Set<Range<C>> asRanges() {
        Set<Range<C>> asRanges;
        if ((asRanges = this.asRanges) == null) {
            asRanges = new b(this.rangesByLowerBound.values());
            this.asRanges = asRanges;
        }
        return asRanges;
    }
    
    @Override
    public gc1 complement() {
        gc1 complement;
        if ((complement = this.complement) == null) {
            complement = new Complement();
            this.complement = complement;
        }
        return complement;
    }
    
    @Override
    public boolean encloses(final Range<C> range) {
        i71.r(range);
        final Map.Entry<Cut<C>, Range<C>> floorEntry = this.rangesByLowerBound.floorEntry(range.lowerBound);
        return floorEntry != null && floorEntry.getValue().encloses(range);
    }
    
    public boolean intersects(final Range<C> range) {
        i71.r(range);
        final Map.Entry<Cut<C>, Range<C>> ceilingEntry = this.rangesByLowerBound.ceilingEntry(range.lowerBound);
        boolean b = true;
        if (ceilingEntry != null && ceilingEntry.getValue().isConnected(range) && !ceilingEntry.getValue().intersection(range).isEmpty()) {
            return true;
        }
        final Map.Entry<Cut<C>, Range<C>> lowerEntry = this.rangesByLowerBound.lowerEntry(range.lowerBound);
        if (lowerEntry == null || !lowerEntry.getValue().isConnected(range) || lowerEntry.getValue().intersection(range).isEmpty()) {
            b = false;
        }
        return b;
    }
    
    @Override
    public Range<C> rangeContaining(final C c) {
        i71.r(c);
        final Map.Entry<Cut<C>, Range<C>> floorEntry = this.rangesByLowerBound.floorEntry(Cut.belowValue(c));
        if (floorEntry != null && floorEntry.getValue().contains(c)) {
            return floorEntry.getValue();
        }
        return null;
    }
    
    @Override
    public void remove(final Range<C> range) {
        i71.r(range);
        if (range.isEmpty()) {
            return;
        }
        final Map.Entry<Cut<C>, Range> lowerEntry = this.rangesByLowerBound.lowerEntry(range.lowerBound);
        if (lowerEntry != null) {
            final Range range2 = lowerEntry.getValue();
            if (range2.upperBound.compareTo((Cut<C>)range.lowerBound) >= 0) {
                if (range.hasUpperBound() && range2.upperBound.compareTo((Cut<C>)range.upperBound) >= 0) {
                    this.replaceRangeWithSameLowerBound(Range.create(range.upperBound, (Cut<C>)range2.upperBound));
                }
                this.replaceRangeWithSameLowerBound(Range.create((Cut<C>)range2.lowerBound, range.lowerBound));
            }
        }
        final Map.Entry<Cut<C>, Range> floorEntry = this.rangesByLowerBound.floorEntry(range.upperBound);
        if (floorEntry != null) {
            final Range range3 = floorEntry.getValue();
            if (range.hasUpperBound() && range3.upperBound.compareTo((Cut<C>)range.upperBound) >= 0) {
                this.replaceRangeWithSameLowerBound(Range.create(range.upperBound, (Cut<C>)range3.upperBound));
            }
        }
        this.rangesByLowerBound.subMap(range.lowerBound, range.upperBound).clear();
    }
    
    public Range<C> span() {
        final Map.Entry<Cut<C>, Range> firstEntry = this.rangesByLowerBound.firstEntry();
        final Map.Entry<Cut<C>, Range> lastEntry = this.rangesByLowerBound.lastEntry();
        if (firstEntry != null && lastEntry != null) {
            return Range.create((Cut<C>)firstEntry.getValue().lowerBound, (Cut<C>)lastEntry.getValue().upperBound);
        }
        throw new NoSuchElementException();
    }
    
    public gc1 subRangeSet(final Range<C> range) {
        TreeRangeSet set;
        if (range.equals(Range.all())) {
            set = this;
        }
        else {
            set = new SubRangeSet(range);
        }
        return set;
    }
    
    public final class Complement extends TreeRangeSet<C>
    {
        final TreeRangeSet this$0;
        
        public Complement(final TreeRangeSet this$0) {
            this.this$0 = this$0;
            super(new c(this$0.rangesByLowerBound), null);
        }
        
        @Override
        public void add(final Range<C> range) {
            this.this$0.remove(range);
        }
        
        @Override
        public gc1 complement() {
            return this.this$0;
        }
        
        @Override
        public boolean contains(final C c) {
            return this.this$0.contains(c) ^ true;
        }
        
        @Override
        public void remove(final Range<C> range) {
            this.this$0.add(range);
        }
    }
    
    public final class SubRangeSet extends TreeRangeSet<C>
    {
        private final Range<C> restriction;
        final TreeRangeSet this$0;
        
        public SubRangeSet(final TreeRangeSet this$0, final Range<C> restriction) {
            this.this$0 = this$0;
            super(new e(Range.all(), restriction, this$0.rangesByLowerBound, null), null);
            this.restriction = restriction;
        }
        
        @Override
        public void add(final Range<C> range) {
            i71.n(this.restriction.encloses(range), "Cannot add range %s to subRangeSet(%s)", range, this.restriction);
            this.this$0.add(range);
        }
        
        @Override
        public void clear() {
            this.this$0.remove(this.restriction);
        }
        
        @Override
        public boolean contains(final C c) {
            return this.restriction.contains(c) && this.this$0.contains(c);
        }
        
        @Override
        public boolean encloses(final Range<C> range) {
            final boolean empty = this.restriction.isEmpty();
            boolean b2;
            final boolean b = b2 = false;
            if (!empty) {
                b2 = b;
                if (this.restriction.encloses(range)) {
                    final Range access$600 = this.this$0.rangeEnclosing(range);
                    b2 = b;
                    if (access$600 != null) {
                        b2 = b;
                        if (!access$600.intersection(this.restriction).isEmpty()) {
                            b2 = true;
                        }
                    }
                }
            }
            return b2;
        }
        
        @Override
        public Range<C> rangeContaining(final C c) {
            final boolean contains = this.restriction.contains(c);
            final Range<C> range = null;
            if (!contains) {
                return null;
            }
            final Range<C> rangeContaining = this.this$0.rangeContaining(c);
            Range<C> intersection;
            if (rangeContaining == null) {
                intersection = range;
            }
            else {
                intersection = rangeContaining.intersection(this.restriction);
            }
            return intersection;
        }
        
        @Override
        public void remove(final Range<C> range) {
            if (range.isConnected(this.restriction)) {
                this.this$0.remove(range.intersection(this.restriction));
            }
        }
        
        @Override
        public gc1 subRangeSet(final Range<C> range) {
            if (range.encloses(this.restriction)) {
                return this;
            }
            if (range.isConnected(this.restriction)) {
                return new SubRangeSet(this.restriction.intersection(range));
            }
            return ImmutableRangeSet.of();
        }
    }
    
    public final class b extends t70 implements Set
    {
        public final Collection a;
        
        public b(final TreeRangeSet set, final Collection a) {
            this.a = a;
        }
        
        @Override
        public Collection delegate() {
            return this.a;
        }
        
        @Override
        public boolean equals(final Object o) {
            return Sets.a(this, o);
        }
        
        @Override
        public int hashCode() {
            return Sets.b(this);
        }
    }
    
    public static final class c extends com.google.common.collect.c
    {
        public final NavigableMap a;
        public final NavigableMap b;
        public final Range c;
        
        public c(final NavigableMap navigableMap) {
            this(navigableMap, Range.all());
        }
        
        public c(final NavigableMap a, final Range c) {
            this.a = a;
            this.b = new d(a);
            this.c = c;
        }
        
        public static /* synthetic */ Range c(final c c) {
            return c.c;
        }
        
        public Iterator a() {
            NavigableMap<Cut<?>, Object> navigableMap;
            if (this.c.hasLowerBound()) {
                navigableMap = this.b.tailMap(this.c.lowerEndpoint(), this.c.lowerBoundType() == BoundType.CLOSED);
            }
            else {
                navigableMap = this.b;
            }
            final b51 p = Iterators.p(navigableMap.values().iterator());
            Cut<Comparable> cut;
            if (this.c.contains(Cut.belowAll()) && (!p.hasNext() || ((Range)p.peek()).lowerBound != Cut.belowAll())) {
                cut = Cut.belowAll();
            }
            else {
                if (!p.hasNext()) {
                    return Iterators.h();
                }
                cut = (Cut<Comparable>)((Range)p.next()).upperBound;
            }
            return new AbstractIterator(this, cut, p) {
                public Cut c = cut;
                public final Cut d;
                public final b51 e;
                public final c f;
                
                public Entry e() {
                    if (!TreeRangeSet.c.c(this.f).upperBound.isLessThan((C)this.c) && this.c != Cut.aboveAll()) {
                        Range<Comparable> range2;
                        Comparable<Cut<C>> c;
                        if (this.e.hasNext()) {
                            final Range range = (Range)this.e.next();
                            range2 = Range.create(this.c, (Cut<Comparable>)range.lowerBound);
                            c = (Comparable<Cut<C>>)range.upperBound;
                        }
                        else {
                            range2 = Range.create(this.c, Cut.aboveAll());
                            c = (Comparable<Cut<C>>)Cut.aboveAll();
                        }
                        this.c = (Cut)c;
                        return Maps.j(range2.lowerBound, range2);
                    }
                    return (Entry)this.c();
                }
            };
        }
        
        @Override
        public Iterator b() {
            Cut<Comparable> aboveAll;
            if (this.c.hasUpperBound()) {
                aboveAll = this.c.upperEndpoint();
            }
            else {
                aboveAll = Cut.aboveAll();
            }
            final b51 p = Iterators.p(this.b.headMap(aboveAll, this.c.hasUpperBound() && this.c.upperBoundType() == BoundType.CLOSED).descendingMap().values().iterator());
            NavigableMap navigableMap;
            Comparable<Cut<C>> comparable;
            if (p.hasNext()) {
                if (((Range)p.peek()).upperBound == Cut.aboveAll()) {
                    final Cut<C> lowerBound = ((Range)p.next()).lowerBound;
                    return new AbstractIterator(this, (Cut)com.google.common.base.a.a(lowerBound, Cut.aboveAll()), p) {
                        public Cut c;
                        public final Cut d;
                        public final b51 e;
                        public final c f;
                        
                        {
                            this.c = cut;
                        }
                        
                        public Entry e() {
                            if (this.c == Cut.belowAll()) {
                                return (Entry)this.c();
                            }
                            if (this.e.hasNext()) {
                                final Range range = (Range)this.e.next();
                                final Range<Comparable> create = Range.create((Cut<Comparable>)range.upperBound, this.c);
                                this.c = range.lowerBound;
                                if (TreeRangeSet.c.c(this.f).lowerBound.isLessThan((C)create.lowerBound)) {
                                    return Maps.j(create.lowerBound, create);
                                }
                            }
                            else if (TreeRangeSet.c.c(this.f).lowerBound.isLessThan((C)Cut.belowAll())) {
                                final Range<Comparable> create2 = Range.create(Cut.belowAll(), this.c);
                                this.c = Cut.belowAll();
                                return Maps.j(Cut.belowAll(), create2);
                            }
                            return (Entry)this.c();
                        }
                    };
                }
                navigableMap = this.a;
                comparable = (Comparable<Cut<C>>)((Range)p.peek()).upperBound;
            }
            else {
                if (!this.c.contains(Cut.belowAll()) || this.a.containsKey(Cut.belowAll())) {
                    return Iterators.h();
                }
                navigableMap = this.a;
                comparable = (Comparable<Cut<C>>)Cut.belowAll();
            }
            final Cut<C> lowerBound = navigableMap.higherKey(comparable);
            return new AbstractIterator(this, (Cut)com.google.common.base.a.a(lowerBound, Cut.aboveAll()), p) {
                public Cut c = cut;
                public final Cut d;
                public final b51 e;
                public final c f;
                
                public Entry e() {
                    if (this.c == Cut.belowAll()) {
                        return (Entry)this.c();
                    }
                    if (this.e.hasNext()) {
                        final Range range = (Range)this.e.next();
                        final Range<Comparable> create = Range.create((Cut<Comparable>)range.upperBound, this.c);
                        this.c = range.lowerBound;
                        if (TreeRangeSet.c.c(this.f).lowerBound.isLessThan((C)create.lowerBound)) {
                            return Maps.j(create.lowerBound, create);
                        }
                    }
                    else if (TreeRangeSet.c.c(this.f).lowerBound.isLessThan((C)Cut.belowAll())) {
                        final Range<Comparable> create2 = Range.create(Cut.belowAll(), this.c);
                        this.c = Cut.belowAll();
                        return Maps.j(Cut.belowAll(), create2);
                    }
                    return (Entry)this.c();
                }
            };
        }
        
        @Override
        public Comparator comparator() {
            return Ordering.natural();
        }
        
        @Override
        public boolean containsKey(final Object o) {
            return this.d(o) != null;
        }
        
        public Range d(final Object o) {
            Label_0056: {
                if (!(o instanceof Cut)) {
                    break Label_0056;
                }
                try {
                    final Cut cut = (Cut)o;
                    final Map.Entry firstEntry = this.h(cut, true).firstEntry();
                    if (firstEntry != null && ((Cut)firstEntry.getKey()).equals(cut)) {
                        return (Range)firstEntry.getValue();
                    }
                    return null;
                }
                catch (final ClassCastException ex) {
                    return null;
                }
            }
        }
        
        public NavigableMap e(final Cut cut, final boolean b) {
            return this.g(Range.upTo(cut, BoundType.forBoolean(b)));
        }
        
        public NavigableMap f(final Cut cut, final boolean b, final Cut cut2, final boolean b2) {
            return this.g(Range.range(cut, BoundType.forBoolean(b), cut2, BoundType.forBoolean(b2)));
        }
        
        public final NavigableMap g(Range intersection) {
            if (!this.c.isConnected(intersection)) {
                return ImmutableSortedMap.of();
            }
            intersection = intersection.intersection(this.c);
            return new c(this.a, intersection);
        }
        
        public NavigableMap h(final Cut cut, final boolean b) {
            return this.g(Range.downTo(cut, BoundType.forBoolean(b)));
        }
        
        @Override
        public int size() {
            return Iterators.u(this.a());
        }
    }
    
    public static final class d extends c
    {
        public final NavigableMap a;
        public final Range b;
        
        public d(final NavigableMap a) {
            this.a = a;
            this.b = Range.all();
        }
        
        public d(final NavigableMap a, final Range b) {
            this.a = a;
            this.b = b;
        }
        
        public static /* synthetic */ Range c(final d d) {
            return d.b;
        }
        
        public Iterator a() {
            if (this.b.hasLowerBound()) {
                final Map.Entry<Cut<?>, Range> lowerEntry = this.a.lowerEntry(this.b.lowerEndpoint());
                if (lowerEntry != null) {
                    if (this.b.lowerBound.isLessThan((C)lowerEntry.getValue().upperBound)) {
                        final Object o = this.a.tailMap(lowerEntry.getKey(), true);
                        return new AbstractIterator(this, ((SortedMap)o).values().iterator()) {
                            public final Iterator c;
                            public final d d;
                            
                            public Entry e() {
                                if (!this.c.hasNext()) {
                                    return (Entry)this.c();
                                }
                                final Range range = this.c.next();
                                if (TreeRangeSet.d.c(this.d).upperBound.isLessThan((C)range.upperBound)) {
                                    return (Entry)this.c();
                                }
                                return Maps.j(range.upperBound, range);
                            }
                        };
                    }
                    final Object o = this.a.tailMap(this.b.lowerEndpoint(), true);
                    return new AbstractIterator(this, ((SortedMap)o).values().iterator()) {
                        public final Iterator c;
                        public final d d;
                        
                        public Entry e() {
                            if (!this.c.hasNext()) {
                                return (Entry)this.c();
                            }
                            final Range range = this.c.next();
                            if (TreeRangeSet.d.c(this.d).upperBound.isLessThan((C)range.upperBound)) {
                                return (Entry)this.c();
                            }
                            return Maps.j(range.upperBound, range);
                        }
                    };
                }
            }
            final Object o = this.a;
            return new AbstractIterator(this, ((SortedMap)o).values().iterator()) {
                public final Iterator c;
                public final d d;
                
                public Entry e() {
                    if (!this.c.hasNext()) {
                        return (Entry)this.c();
                    }
                    final Range range = this.c.next();
                    if (TreeRangeSet.d.c(this.d).upperBound.isLessThan((C)range.upperBound)) {
                        return (Entry)this.c();
                    }
                    return Maps.j(range.upperBound, range);
                }
            };
        }
        
        @Override
        public Iterator b() {
            NavigableMap<Cut<?>, Object> navigableMap;
            if (this.b.hasUpperBound()) {
                navigableMap = this.a.headMap(this.b.upperEndpoint(), false);
            }
            else {
                navigableMap = this.a;
            }
            final b51 p = Iterators.p(navigableMap.descendingMap().values().iterator());
            if (p.hasNext() && this.b.upperBound.isLessThan((C)((Range)p.peek()).upperBound)) {
                p.next();
            }
            return new AbstractIterator(this, p) {
                public final b51 c;
                public final d d;
                
                public Entry e() {
                    if (!this.c.hasNext()) {
                        return (Entry)this.c();
                    }
                    final Range range = (Range)this.c.next();
                    Entry j;
                    if (TreeRangeSet.d.c(this.d).lowerBound.isLessThan((C)range.upperBound)) {
                        j = Maps.j(range.upperBound, range);
                    }
                    else {
                        j = (Entry)this.c();
                    }
                    return j;
                }
            };
        }
        
        @Override
        public Comparator comparator() {
            return Ordering.natural();
        }
        
        @Override
        public boolean containsKey(final Object o) {
            return this.d(o) != null;
        }
        
        public Range d(final Object o) {
            Label_0071: {
                if (!(o instanceof Cut)) {
                    break Label_0071;
                }
                try {
                    final Cut cut = (Cut)o;
                    if (!this.b.contains(cut)) {
                        return null;
                    }
                    final Map.Entry<Cut, Object> lowerEntry = this.a.lowerEntry(cut);
                    if (lowerEntry != null && lowerEntry.getValue().upperBound.equals(cut)) {
                        return lowerEntry.getValue();
                    }
                    return null;
                }
                catch (final ClassCastException ex) {
                    return null;
                }
            }
        }
        
        public NavigableMap e(final Cut cut, final boolean b) {
            return this.g(Range.upTo(cut, BoundType.forBoolean(b)));
        }
        
        public NavigableMap f(final Cut cut, final boolean b, final Cut cut2, final boolean b2) {
            return this.g(Range.range(cut, BoundType.forBoolean(b), cut2, BoundType.forBoolean(b2)));
        }
        
        public final NavigableMap g(final Range range) {
            if (range.isConnected(this.b)) {
                return new d(this.a, range.intersection(this.b));
            }
            return ImmutableSortedMap.of();
        }
        
        public NavigableMap h(final Cut cut, final boolean b) {
            return this.g(Range.downTo(cut, BoundType.forBoolean(b)));
        }
        
        @Override
        public boolean isEmpty() {
            boolean empty;
            if (this.b.equals(Range.all())) {
                empty = this.a.isEmpty();
            }
            else {
                empty = !this.a().hasNext();
            }
            return empty;
        }
        
        @Override
        public int size() {
            if (this.b.equals(Range.all())) {
                return this.a.size();
            }
            return Iterators.u(this.a());
        }
    }
    
    public static final class e extends c
    {
        public final Range a;
        public final Range b;
        public final NavigableMap c;
        public final NavigableMap d;
        
        public e(final Range range, final Range range2, final NavigableMap navigableMap) {
            this.a = (Range)i71.r(range);
            this.b = (Range)i71.r(range2);
            this.c = (NavigableMap)i71.r(navigableMap);
            this.d = new d(navigableMap);
        }
        
        public static /* synthetic */ Range c(final e e) {
            return e.b;
        }
        
        public Iterator a() {
            if (this.b.isEmpty()) {
                return Iterators.h();
            }
            if (this.a.upperBound.isLessThan((C)this.b.lowerBound)) {
                return Iterators.h();
            }
            final boolean lessThan = this.a.lowerBound.isLessThan((C)this.b.lowerBound);
            boolean b = false;
            NavigableMap d;
            Cut<C> lowerBound;
            if (lessThan) {
                d = this.d;
                lowerBound = this.b.lowerBound;
            }
            else {
                final NavigableMap c = this.c;
                final Cut cut = (Cut)this.a.lowerBound.endpoint();
                d = c;
                lowerBound = cut;
                if (this.a.lowerBoundType() == BoundType.CLOSED) {
                    b = true;
                    d = c;
                    lowerBound = cut;
                }
            }
            return new AbstractIterator(this, d.tailMap(lowerBound, b).values().iterator(), Ordering.natural().min(this.a.upperBound, (Cut<C>)Cut.belowValue(this.b.upperBound))) {
                public final Iterator c;
                public final Cut d;
                public final e e;
                
                public Entry e() {
                    if (!this.c.hasNext()) {
                        return (Entry)this.c();
                    }
                    final Range range = this.c.next();
                    if (this.d.isLessThan(range.lowerBound)) {
                        return (Entry)this.c();
                    }
                    final Range intersection = range.intersection(TreeRangeSet.e.c(this.e));
                    return Maps.j(intersection.lowerBound, intersection);
                }
            };
        }
        
        @Override
        public Iterator b() {
            if (this.b.isEmpty()) {
                return Iterators.h();
            }
            final Cut<C> cut = Ordering.natural().min(this.a.upperBound, (Cut<C>)Cut.belowValue(this.b.upperBound));
            return new AbstractIterator(this, this.c.headMap(cut.endpoint(), cut.typeAsUpperBound() == BoundType.CLOSED).descendingMap().values().iterator()) {
                public final Iterator c;
                public final e d;
                
                public Entry e() {
                    // 
                    // This method could not be decompiled.
                    // 
                    // Original Bytecode:
                    // 
                    //     1: getfield        com/google/common/collect/TreeRangeSet$e$b.c:Ljava/util/Iterator;
                    //     4: invokeinterface java/util/Iterator.hasNext:()Z
                    //     9: ifne            20
                    //    12: aload_0        
                    //    13: invokevirtual   com/google/common/collect/AbstractIterator.c:()Ljava/lang/Object;
                    //    16: checkcast       Ljava/util/Map$Entry;
                    //    19: areturn        
                    //    20: aload_0        
                    //    21: getfield        com/google/common/collect/TreeRangeSet$e$b.c:Ljava/util/Iterator;
                    //    24: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
                    //    29: checkcast       Lcom/google/common/collect/Range;
                    //    32: astore_1       
                    //    33: aload_0        
                    //    34: getfield        com/google/common/collect/TreeRangeSet$e$b.d:Lcom/google/common/collect/TreeRangeSet$e;
                    //    37: invokestatic    com/google/common/collect/TreeRangeSet$e.c:(Lcom/google/common/collect/TreeRangeSet$e;)Lcom/google/common/collect/Range;
                    //    40: getfield        com/google/common/collect/Range.lowerBound:Lcom/google/common/collect/Cut;
                    //    43: aload_1        
                    //    44: getfield        com/google/common/collect/Range.upperBound:Lcom/google/common/collect/Cut;
                    //    47: invokevirtual   com/google/common/collect/Cut.compareTo:(Lcom/google/common/collect/Cut;)I
                    //    50: iflt            61
                    //    53: aload_0        
                    //    54: invokevirtual   com/google/common/collect/AbstractIterator.c:()Ljava/lang/Object;
                    //    57: checkcast       Ljava/util/Map$Entry;
                    //    60: areturn        
                    //    61: aload_1        
                    //    62: aload_0        
                    //    63: getfield        com/google/common/collect/TreeRangeSet$e$b.d:Lcom/google/common/collect/TreeRangeSet$e;
                    //    66: invokestatic    com/google/common/collect/TreeRangeSet$e.c:(Lcom/google/common/collect/TreeRangeSet$e;)Lcom/google/common/collect/Range;
                    //    69: invokevirtual   com/google/common/collect/Range.intersection:(Lcom/google/common/collect/Range;)Lcom/google/common/collect/Range;
                    //    72: astore_1       
                    //    73: aload_0        
                    //    74: getfield        com/google/common/collect/TreeRangeSet$e$b.d:Lcom/google/common/collect/TreeRangeSet$e;
                    //    77: invokestatic    com/google/common/collect/TreeRangeSet$e.d:(Lcom/google/common/collect/TreeRangeSet$e;)Lcom/google/common/collect/Range;
                    //    80: aload_1        
                    //    81: getfield        com/google/common/collect/Range.lowerBound:Lcom/google/common/collect/Cut;
                    //    84: invokevirtual   com/google/common/collect/Range.contains:(Ljava/lang/Comparable;)Z
                    //    87: ifeq            99
                    //    90: aload_1        
                    //    91: getfield        com/google/common/collect/Range.lowerBound:Lcom/google/common/collect/Cut;
                    //    94: aload_1        
                    //    95: invokestatic    com/google/common/collect/Maps.j:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;
                    //    98: areturn        
                    //    99: aload_0        
                    //   100: invokevirtual   com/google/common/collect/AbstractIterator.c:()Ljava/lang/Object;
                    //   103: checkcast       Ljava/util/Map$Entry;
                    //   106: areturn        
                    // 
                    // The error that occurred was:
                    // 
                    // java.lang.UnsupportedOperationException: The requested operation is not supported.
                    //     at com.strobel.util.ContractUtils.unsupported(ContractUtils.java:27)
                    //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:284)
                    //     at com.strobel.assembler.metadata.TypeReference.getRawType(TypeReference.java:279)
                    //     at com.strobel.assembler.metadata.TypeReference.makeGenericType(TypeReference.java:154)
                    //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:225)
                    //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:25)
                    //     at com.strobel.assembler.metadata.ParameterizedType.accept(ParameterizedType.java:103)
                    //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visit(TypeSubstitutionVisitor.java:40)
                    //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:211)
                    //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:25)
                    //     at com.strobel.assembler.metadata.ParameterizedType.accept(ParameterizedType.java:103)
                    //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visit(TypeSubstitutionVisitor.java:40)
                    //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:211)
                    //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameterizedType(TypeSubstitutionVisitor.java:25)
                    //     at com.strobel.assembler.metadata.ParameterizedType.accept(ParameterizedType.java:103)
                    //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visit(TypeSubstitutionVisitor.java:40)
                    //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitParameters(TypeSubstitutionVisitor.java:406)
                    //     at com.strobel.assembler.metadata.TypeSubstitutionVisitor.visitMethod(TypeSubstitutionVisitor.java:317)
                    //     at com.strobel.decompiler.ast.TypeAnalysis.inferCall(TypeAnalysis.java:2611)
                    //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1040)
                    //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
                    //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:684)
                    //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:667)
                    //     at com.strobel.decompiler.ast.TypeAnalysis.inferBinaryExpression(TypeAnalysis.java:2124)
                    //     at com.strobel.decompiler.ast.TypeAnalysis.doInferTypeForExpression(TypeAnalysis.java:1545)
                    //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypeForExpression(TypeAnalysis.java:815)
                    //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:684)
                    //     at com.strobel.decompiler.ast.TypeAnalysis.inferTypesForVariables(TypeAnalysis.java:593)
                    //     at com.strobel.decompiler.ast.TypeAnalysis.runInference(TypeAnalysis.java:405)
                    //     at com.strobel.decompiler.ast.TypeAnalysis.run(TypeAnalysis.java:95)
                    //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:109)
                    //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformCall(AstMethodBodyBuilder.java:1151)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:993)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:534)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:548)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:534)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformNode(AstMethodBodyBuilder.java:377)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformBlock(AstMethodBodyBuilder.java:318)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:213)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
                    //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
                    //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
                    //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
                    //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
                    //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
                    // 
                    throw new IllegalStateException("An error occurred while decompiling this method.");
                }
            };
        }
        
        @Override
        public Comparator comparator() {
            return Ordering.natural();
        }
        
        @Override
        public boolean containsKey(final Object o) {
            return this.e(o) != null;
        }
        
        public Range e(final Object o) {
            Label_0144: {
                if (!(o instanceof Cut)) {
                    break Label_0144;
                }
                try {
                    final Cut cut = (Cut)o;
                    if (this.a.contains(cut) && cut.compareTo(this.b.lowerBound) >= 0) {
                        if (cut.compareTo(this.b.upperBound) < 0) {
                            if (cut.equals(this.b.lowerBound)) {
                                final Range range = (Range)Maps.L((Entry)this.c.floorEntry(cut));
                                if (range != null && range.upperBound.compareTo(this.b.lowerBound) > 0) {
                                    return range.intersection(this.b);
                                }
                            }
                            else {
                                final Range range2 = (Range)this.c.get(cut);
                                if (range2 != null) {
                                    return range2.intersection(this.b);
                                }
                            }
                        }
                    }
                    return null;
                }
                catch (final ClassCastException ex) {
                    return null;
                }
            }
        }
        
        public NavigableMap f(final Cut cut, final boolean b) {
            return this.h(Range.upTo(cut, BoundType.forBoolean(b)));
        }
        
        public NavigableMap g(final Cut cut, final boolean b, final Cut cut2, final boolean b2) {
            return this.h(Range.range(cut, BoundType.forBoolean(b), cut2, BoundType.forBoolean(b2)));
        }
        
        public final NavigableMap h(final Range range) {
            if (!range.isConnected(this.a)) {
                return ImmutableSortedMap.of();
            }
            return new e(this.a.intersection(range), this.b, this.c);
        }
        
        public NavigableMap i(final Cut cut, final boolean b) {
            return this.h(Range.downTo(cut, BoundType.forBoolean(b)));
        }
        
        @Override
        public int size() {
            return Iterators.u(this.a());
        }
    }
}

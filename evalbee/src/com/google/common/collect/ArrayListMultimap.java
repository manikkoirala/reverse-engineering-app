// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.util.Iterator;
import java.util.Set;
import java.util.ArrayList;
import java.util.List;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.Map;
import java.io.ObjectInputStream;

public final class ArrayListMultimap<K, V> extends ArrayListMultimapGwtSerializationDependencies<K, V>
{
    private static final int DEFAULT_VALUES_PER_KEY = 3;
    private static final long serialVersionUID = 0L;
    transient int expectedValuesPerKey;
    
    private ArrayListMultimap() {
        this(12, 3);
    }
    
    private ArrayListMultimap(final int n, final int expectedValuesPerKey) {
        super(com.google.common.collect.l.c(n));
        hh.b(expectedValuesPerKey, "expectedValuesPerKey");
        this.expectedValuesPerKey = expectedValuesPerKey;
    }
    
    private ArrayListMultimap(final rx0 rx0) {
        final int size = rx0.keySet().size();
        int expectedValuesPerKey;
        if (rx0 instanceof ArrayListMultimap) {
            expectedValuesPerKey = ((ArrayListMultimap)rx0).expectedValuesPerKey;
        }
        else {
            expectedValuesPerKey = 3;
        }
        this(size, expectedValuesPerKey);
        this.putAll(rx0);
    }
    
    public static <K, V> ArrayListMultimap<K, V> create() {
        return new ArrayListMultimap<K, V>();
    }
    
    public static <K, V> ArrayListMultimap<K, V> create(final int n, final int n2) {
        return new ArrayListMultimap<K, V>(n, n2);
    }
    
    public static <K, V> ArrayListMultimap<K, V> create(final rx0 rx0) {
        return new ArrayListMultimap<K, V>(rx0);
    }
    
    private void readObject(final ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        this.expectedValuesPerKey = 3;
        final int h = com.google.common.collect.n.h(objectInputStream);
        this.setMap((Map<K, Collection<V>>)CompactHashMap.create());
        com.google.common.collect.n.e(this, objectInputStream, h);
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) {
        objectOutputStream.defaultWriteObject();
        com.google.common.collect.n.j(this, objectOutputStream);
    }
    
    @Override
    public List<V> createCollection() {
        return new ArrayList<V>(this.expectedValuesPerKey);
    }
    
    @Deprecated
    public void trimToSize() {
        final Iterator<Collection<V>> iterator = (Iterator<Collection<V>>)this.backingMap().values().iterator();
        while (iterator.hasNext()) {
            ((ArrayList)iterator.next()).trimToSize();
        }
    }
}

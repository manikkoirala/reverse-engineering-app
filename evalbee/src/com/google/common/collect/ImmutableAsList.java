// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.collect;

import java.io.Serializable;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;

abstract class ImmutableAsList<E> extends ImmutableList<E>
{
    public ImmutableAsList() {
    }
    
    private void readObject(final ObjectInputStream objectInputStream) {
        throw new InvalidObjectException("Use SerializedForm");
    }
    
    @Override
    public boolean contains(final Object o) {
        return this.delegateCollection().contains(o);
    }
    
    public abstract ImmutableCollection<E> delegateCollection();
    
    @Override
    public boolean isEmpty() {
        return this.delegateCollection().isEmpty();
    }
    
    @Override
    public boolean isPartialView() {
        return this.delegateCollection().isPartialView();
    }
    
    @Override
    public int size() {
        return this.delegateCollection().size();
    }
    
    @Override
    public Object writeReplace() {
        return new SerializedForm(this.delegateCollection());
    }
    
    public static class SerializedForm implements Serializable
    {
        private static final long serialVersionUID = 0L;
        final ImmutableCollection<?> collection;
        
        public SerializedForm(final ImmutableCollection<?> collection) {
            this.collection = collection;
        }
        
        public Object readResolve() {
            return this.collection.asList();
        }
    }
}

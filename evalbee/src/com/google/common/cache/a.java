// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.cache;

public interface a
{
    long getAccessTime();
    
    int getHash();
    
    Object getKey();
    
    a getNext();
    
    a getNextInAccessQueue();
    
    a getNextInWriteQueue();
    
    a getPreviousInAccessQueue();
    
    a getPreviousInWriteQueue();
    
    LocalCache.s getValueReference();
    
    long getWriteTime();
    
    void setAccessTime(final long p0);
    
    void setNextInAccessQueue(final a p0);
    
    void setNextInWriteQueue(final a p0);
    
    void setPreviousInAccessQueue(final a p0);
    
    void setPreviousInWriteQueue(final a p0);
    
    void setValueReference(final LocalCache.s p0);
    
    void setWriteTime(final long p0);
}

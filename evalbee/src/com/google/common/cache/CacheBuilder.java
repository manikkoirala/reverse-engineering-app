// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.cache;

import com.google.common.base.a;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import com.google.common.base.Suppliers;
import com.google.common.base.Equivalence;
import java.util.logging.Logger;

public final class CacheBuilder
{
    public static final is1 q;
    public static final pe r;
    public static final is1 s;
    public static final hw1 t;
    public static final Logger u;
    public boolean a;
    public int b;
    public int c;
    public long d;
    public long e;
    public y52 f;
    public LocalCache.Strength g;
    public LocalCache.Strength h;
    public long i;
    public long j;
    public long k;
    public Equivalence l;
    public Equivalence m;
    public pd1 n;
    public hw1 o;
    public is1 p;
    
    static {
        q = Suppliers.b(new j() {
            @Override
            public void a(final int n) {
            }
            
            @Override
            public void b() {
            }
            
            @Override
            public void c(final long n) {
            }
            
            @Override
            public void d(final int n) {
            }
            
            @Override
            public void e(final long n) {
            }
            
            @Override
            public pe f() {
                return CacheBuilder.r;
            }
        });
        r = new pe(0L, 0L, 0L, 0L, 0L, 0L);
        s = new is1() {
            public j a() {
                return new i();
            }
        };
        t = new hw1() {
            @Override
            public long a() {
                return 0L;
            }
        };
        u = Logger.getLogger(CacheBuilder.class.getName());
    }
    
    public CacheBuilder() {
        this.a = true;
        this.b = -1;
        this.c = -1;
        this.d = -1L;
        this.e = -1L;
        this.i = -1L;
        this.j = -1L;
        this.k = -1L;
        this.p = CacheBuilder.q;
    }
    
    public static CacheBuilder y() {
        return new CacheBuilder();
    }
    
    public CacheBuilder A(final LocalCache.Strength strength) {
        final LocalCache.Strength g = this.g;
        i71.B(g == null, "Key strength was already set to %s", g);
        this.g = (LocalCache.Strength)i71.r(strength);
        return this;
    }
    
    public CacheBuilder B(final LocalCache.Strength strength) {
        final LocalCache.Strength h = this.h;
        i71.B(h == null, "Value strength was already set to %s", h);
        this.h = (LocalCache.Strength)i71.r(strength);
        return this;
    }
    
    public CacheBuilder C(final hw1 hw1) {
        i71.x(this.o == null);
        this.o = (hw1)i71.r(hw1);
        return this;
    }
    
    public CacheBuilder D(final Equivalence equivalence) {
        final Equivalence m = this.m;
        i71.B(m == null, "value equivalence was already set to %s", m);
        this.m = (Equivalence)i71.r(equivalence);
        return this;
    }
    
    public CacheBuilder E(final y52 y52) {
        final y52 f = this.f;
        final boolean b = true;
        i71.x(f == null);
        if (this.a) {
            final long d = this.d;
            i71.A(d == -1L && b, "weigher can not be combined with maximum size", d);
        }
        this.f = (y52)i71.r(y52);
        return this;
    }
    
    public ne a() {
        this.d();
        this.c();
        return new LocalCache.LocalManualCache<Object, Object>(this);
    }
    
    public tk0 b(final CacheLoader cacheLoader) {
        this.d();
        return new LocalCache.LocalLoadingCache<Object, Object>(this, cacheLoader);
    }
    
    public final void c() {
        i71.y(this.k == -1L, "refreshAfterWrite requires a LoadingCache");
    }
    
    public final void d() {
        final y52 f = this.f;
        boolean b = true;
        final boolean b2 = true;
        String s;
        if (f == null) {
            b = (this.e == -1L && b2);
            s = "maximumWeight requires weigher";
        }
        else if (this.a) {
            if (this.e == -1L) {
                b = false;
            }
            s = "weigher requires maximumWeight";
        }
        else {
            if (this.e == -1L) {
                CacheBuilder.u.log(Level.WARNING, "ignoring weigher specified without maximumWeight");
            }
            return;
        }
        i71.y(b, s);
    }
    
    public CacheBuilder e(final int c) {
        final int c2 = this.c;
        final boolean b = true;
        i71.z(c2 == -1, "concurrency level was already set to %s", c2);
        i71.d(c > 0 && b);
        this.c = c;
        return this;
    }
    
    public CacheBuilder f(final long duration, final TimeUnit timeUnit) {
        final long j = this.j;
        final boolean b = true;
        i71.A(j == -1L, "expireAfterAccess was already set to %s ns", j);
        i71.l(duration >= 0L && b, "duration cannot be negative: %s %s", duration, timeUnit);
        this.j = timeUnit.toNanos(duration);
        return this;
    }
    
    public CacheBuilder g(final long duration, final TimeUnit timeUnit) {
        final long i = this.i;
        final boolean b = true;
        i71.A(i == -1L, "expireAfterWrite was already set to %s ns", i);
        i71.l(duration >= 0L && b, "duration cannot be negative: %s %s", duration, timeUnit);
        this.i = timeUnit.toNanos(duration);
        return this;
    }
    
    public int h() {
        int c;
        if ((c = this.c) == -1) {
            c = 4;
        }
        return c;
    }
    
    public long i() {
        long j;
        if ((j = this.j) == -1L) {
            j = 0L;
        }
        return j;
    }
    
    public long j() {
        long i;
        if ((i = this.i) == -1L) {
            i = 0L;
        }
        return i;
    }
    
    public int k() {
        int b;
        if ((b = this.b) == -1) {
            b = 16;
        }
        return b;
    }
    
    public Equivalence l() {
        return (Equivalence)com.google.common.base.a.a(this.l, this.m().defaultEquivalence());
    }
    
    public LocalCache.Strength m() {
        return (LocalCache.Strength)com.google.common.base.a.a(this.g, LocalCache.Strength.STRONG);
    }
    
    public long n() {
        if (this.i != 0L && this.j != 0L) {
            long n;
            if (this.f == null) {
                n = this.d;
            }
            else {
                n = this.e;
            }
            return n;
        }
        return 0L;
    }
    
    public long o() {
        long k;
        if ((k = this.k) == -1L) {
            k = 0L;
        }
        return k;
    }
    
    public pd1 p() {
        return (pd1)com.google.common.base.a.a(this.n, NullListener.INSTANCE);
    }
    
    public is1 q() {
        return this.p;
    }
    
    public hw1 r(final boolean b) {
        final hw1 o = this.o;
        if (o != null) {
            return o;
        }
        hw1 hw1;
        if (b) {
            hw1 = hw1.b();
        }
        else {
            hw1 = CacheBuilder.t;
        }
        return hw1;
    }
    
    public Equivalence s() {
        return (Equivalence)com.google.common.base.a.a(this.m, this.t().defaultEquivalence());
    }
    
    public LocalCache.Strength t() {
        return (LocalCache.Strength)com.google.common.base.a.a(this.h, LocalCache.Strength.STRONG);
    }
    
    @Override
    public String toString() {
        final a.b c = com.google.common.base.a.c(this);
        final int b = this.b;
        if (b != -1) {
            c.b("initialCapacity", b);
        }
        final int c2 = this.c;
        if (c2 != -1) {
            c.b("concurrencyLevel", c2);
        }
        final long d = this.d;
        if (d != -1L) {
            c.c("maximumSize", d);
        }
        final long e = this.e;
        if (e != -1L) {
            c.c("maximumWeight", e);
        }
        final long i = this.i;
        if (i != -1L) {
            final StringBuilder sb = new StringBuilder(22);
            sb.append(i);
            sb.append("ns");
            c.d("expireAfterWrite", sb.toString());
        }
        final long j = this.j;
        if (j != -1L) {
            final StringBuilder sb2 = new StringBuilder(22);
            sb2.append(j);
            sb2.append("ns");
            c.d("expireAfterAccess", sb2.toString());
        }
        final LocalCache.Strength g = this.g;
        if (g != null) {
            c.d("keyStrength", d9.c(g.toString()));
        }
        final LocalCache.Strength h = this.h;
        if (h != null) {
            c.d("valueStrength", d9.c(h.toString()));
        }
        if (this.l != null) {
            c.k("keyEquivalence");
        }
        if (this.m != null) {
            c.k("valueEquivalence");
        }
        if (this.n != null) {
            c.k("removalListener");
        }
        return c.toString();
    }
    
    public y52 u() {
        return (y52)com.google.common.base.a.a(this.f, OneWeigher.INSTANCE);
    }
    
    public CacheBuilder v(final Equivalence equivalence) {
        final Equivalence l = this.l;
        i71.B(l == null, "key equivalence was already set to %s", l);
        this.l = (Equivalence)i71.r(equivalence);
        return this;
    }
    
    public CacheBuilder w(final long d) {
        final long d2 = this.d;
        final boolean b = true;
        i71.A(d2 == -1L, "maximum size was already set to %s", d2);
        final long e = this.e;
        i71.A(e == -1L, "maximum weight was already set to %s", e);
        i71.y(this.f == null, "maximum size can not be combined with weigher");
        i71.e(d >= 0L && b, "maximum size must not be negative");
        this.d = d;
        return this;
    }
    
    public CacheBuilder x(final long e) {
        final long e2 = this.e;
        final boolean b = true;
        i71.A(e2 == -1L, "maximum weight was already set to %s", e2);
        final long d = this.d;
        i71.A(d == -1L, "maximum size was already set to %s", d);
        i71.e(e >= 0L && b, "maximum weight must not be negative");
        this.e = e;
        return this;
    }
    
    public CacheBuilder z(final pd1 pd1) {
        i71.x(this.n == null);
        this.n = (pd1)i71.r(pd1);
        return this;
    }
    
    public enum NullListener implements pd1
    {
        private static final NullListener[] $VALUES;
        
        INSTANCE;
        
        private static /* synthetic */ NullListener[] $values() {
            return new NullListener[] { NullListener.INSTANCE };
        }
        
        static {
            $VALUES = $values();
        }
        
        @Override
        public void onRemoval(final RemovalNotification<Object, Object> removalNotification) {
        }
    }
    
    public enum OneWeigher implements y52
    {
        private static final OneWeigher[] $VALUES;
        
        INSTANCE;
        
        private static /* synthetic */ OneWeigher[] $values() {
            return new OneWeigher[] { OneWeigher.INSTANCE };
        }
        
        static {
            $VALUES = $values();
        }
        
        @Override
        public int weigh(final Object o, final Object o2) {
            return 1;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.cache;

import java.lang.ref.WeakReference;
import java.util.AbstractCollection;
import java.lang.ref.SoftReference;
import com.google.common.util.concurrent.e;
import com.google.common.util.concurrent.h;
import java.util.NoSuchElementException;
import java.util.AbstractSet;
import com.google.common.util.concurrent.g;
import java.util.concurrent.Future;
import java.lang.ref.Reference;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.Callable;
import java.io.Serializable;
import java.io.ObjectInputStream;
import com.google.common.util.concurrent.UncheckedExecutionException;
import java.util.concurrent.ExecutionException;
import com.google.common.util.concurrent.ExecutionError;
import java.util.concurrent.TimeUnit;
import com.google.common.primitives.Ints;
import java.util.LinkedHashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Collections;
import com.google.common.collect.Sets;
import com.google.common.collect.Maps;
import com.google.common.collect.ImmutableMap;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.logging.Level;
import com.google.common.collect.Iterators;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;
import com.google.common.collect.ImmutableSet;
import java.util.Iterator;
import java.util.AbstractQueue;
import java.lang.ref.ReferenceQueue;
import java.util.Collection;
import java.util.Set;
import com.google.common.base.Equivalence;
import java.util.Queue;
import java.util.logging.Logger;
import java.util.concurrent.ConcurrentMap;
import java.util.AbstractMap;

public class LocalCache extends AbstractMap implements ConcurrentMap
{
    public static final Logger A;
    public static final s C;
    public static final Queue D;
    public final int a;
    public final int b;
    public final Segment[] c;
    public final int d;
    public final Equivalence e;
    public final Equivalence f;
    public final Strength g;
    public final Strength h;
    public final long i;
    public final y52 j;
    public final long k;
    public final long l;
    public final long m;
    public final Queue n;
    public final pd1 p;
    public final hw1 q;
    public final EntryFactory t;
    public final j v;
    public final CacheLoader w;
    public Set x;
    public Collection y;
    public Set z;
    
    static {
        A = Logger.getLogger(LocalCache.class.getName());
        C = (s)new s() {
            @Override
            public boolean a() {
                return false;
            }
            
            @Override
            public a b() {
                return null;
            }
            
            @Override
            public void c(final Object o) {
            }
            
            @Override
            public int d() {
                return 0;
            }
            
            @Override
            public Object e() {
                return null;
            }
            
            @Override
            public boolean f() {
                return false;
            }
            
            @Override
            public s g(final ReferenceQueue referenceQueue, final Object o, final a a) {
                return this;
            }
            
            @Override
            public Object get() {
                return null;
            }
        };
        D = new AbstractQueue() {
            @Override
            public Iterator iterator() {
                return ImmutableSet.of().iterator();
            }
            
            @Override
            public boolean offer(final Object o) {
                return true;
            }
            
            @Override
            public Object peek() {
                return null;
            }
            
            @Override
            public Object poll() {
                return null;
            }
            
            @Override
            public int size() {
                return 0;
            }
        };
    }
    
    public LocalCache(final CacheBuilder cacheBuilder, final CacheLoader w) {
        this.d = Math.min(cacheBuilder.h(), 65536);
        final Strength m = cacheBuilder.m();
        this.g = m;
        this.h = cacheBuilder.t();
        this.e = cacheBuilder.l();
        this.f = cacheBuilder.s();
        final long n = cacheBuilder.n();
        this.i = n;
        this.j = cacheBuilder.u();
        this.k = cacheBuilder.i();
        this.l = cacheBuilder.j();
        this.m = cacheBuilder.o();
        final pd1 p2 = cacheBuilder.p();
        this.p = p2;
        Queue g;
        if (p2 == CacheBuilder.NullListener.INSTANCE) {
            g = g();
        }
        else {
            g = new ConcurrentLinkedQueue();
        }
        this.n = g;
        this.q = cacheBuilder.r(this.E());
        this.t = EntryFactory.getFactory(m, this.M(), this.Q());
        this.v = (j)cacheBuilder.q().get();
        this.w = w;
        int min;
        final int n2 = min = Math.min(cacheBuilder.k(), 1073741824);
        if (this.h()) {
            min = n2;
            if (!this.f()) {
                min = (int)Math.min(n2, n);
            }
        }
        final int n3 = 0;
        final int n4 = 0;
        final int n5 = 1;
        int n6 = 0;
        int n7;
        for (n7 = 1; n7 < this.d && (!this.h() || n7 * 20 <= this.i); n7 <<= 1) {
            ++n6;
        }
        this.b = 32 - n6;
        this.a = n7 - 1;
        this.c = this.w(n7);
        final int n8 = min / n7;
        int i = n5;
        int n9 = n8;
        if (n8 * n7 < min) {
            n9 = n8 + 1;
            i = n5;
        }
        while (i < n9) {
            i <<= 1;
        }
        int n10 = n3;
        if (this.h()) {
            final long j = this.i;
            final long n11 = n7;
            long n12 = j / n11 + 1L;
            int n13 = n4;
            while (true) {
                final Segment[] c = this.c;
                if (n13 >= c.length) {
                    break;
                }
                long n14 = n12;
                if (n13 == j % n11) {
                    n14 = n12 - 1L;
                }
                c[n13] = this.e(i, n14, (j)cacheBuilder.q().get());
                ++n13;
                n12 = n14;
            }
        }
        else {
            while (true) {
                final Segment[] c2 = this.c;
                if (n10 >= c2.length) {
                    break;
                }
                c2[n10] = this.e(i, -1L, (j)cacheBuilder.q().get());
                ++n10;
            }
        }
    }
    
    public static int I(int n) {
        n += (n << 15 ^ 0xFFFFCD7D);
        n ^= n >>> 10;
        n += n << 3;
        n ^= n >>> 6;
        n += (n << 2) + (n << 14);
        return n ^ n >>> 16;
    }
    
    public static ArrayList K(final Collection collection) {
        final ArrayList list = new ArrayList(collection.size());
        Iterators.a(list, collection.iterator());
        return list;
    }
    
    public static s L() {
        return LocalCache.C;
    }
    
    public static void c(final a previousInAccessQueue, final a nextInAccessQueue) {
        previousInAccessQueue.setNextInAccessQueue(nextInAccessQueue);
        nextInAccessQueue.setPreviousInAccessQueue(previousInAccessQueue);
    }
    
    public static void d(final a previousInWriteQueue, final a nextInWriteQueue) {
        previousInWriteQueue.setNextInWriteQueue(nextInWriteQueue);
        nextInWriteQueue.setPreviousInWriteQueue(previousInWriteQueue);
    }
    
    public static Queue g() {
        return LocalCache.D;
    }
    
    public static a x() {
        return NullEntry.INSTANCE;
    }
    
    public static void y(final a a) {
        final a x = x();
        a.setNextInAccessQueue(x);
        a.setPreviousInAccessQueue(x);
    }
    
    public static void z(final a a) {
        final a x = x();
        a.setNextInWriteQueue(x);
        a.setPreviousInWriteQueue(x);
    }
    
    public void A() {
        while (true) {
            final RemovalNotification removalNotification = this.n.poll();
            if (removalNotification == null) {
                break;
            }
            try {
                this.p.onRemoval(removalNotification);
            }
            finally {
                final Throwable thrown;
                LocalCache.A.log(Level.WARNING, "Exception thrown by removal listener", thrown);
            }
        }
    }
    
    public void B(final a a) {
        final int hash = a.getHash();
        this.J(hash).reclaimKey(a, hash);
    }
    
    public void C(final s s) {
        final a b = s.b();
        final int hash = b.getHash();
        this.J(hash).reclaimValue(b.getKey(), hash, s);
    }
    
    public boolean D() {
        return this.i();
    }
    
    public boolean E() {
        return this.F() || this.D();
    }
    
    public boolean F() {
        return this.j() || this.H();
    }
    
    public void G(final Object o) {
        final int q = this.q(i71.r(o));
        this.J(q).refresh(o, q, this.w, false);
    }
    
    public boolean H() {
        return this.m > 0L;
    }
    
    public Segment J(final int n) {
        return this.c[n >>> this.b & this.a];
    }
    
    public boolean M() {
        return this.N() || this.D();
    }
    
    public boolean N() {
        return this.i() || this.h();
    }
    
    public boolean O() {
        return this.g != Strength.STRONG;
    }
    
    public boolean P() {
        return this.h != Strength.STRONG;
    }
    
    public boolean Q() {
        return this.R() || this.F();
    }
    
    public boolean R() {
        return this.j();
    }
    
    public void b() {
        final Segment[] c = this.c;
        for (int length = c.length, i = 0; i < length; ++i) {
            c[i].cleanUp();
        }
    }
    
    @Override
    public void clear() {
        final Segment[] c = this.c;
        for (int length = c.length, i = 0; i < length; ++i) {
            c[i].clear();
        }
    }
    
    @Override
    public boolean containsKey(final Object o) {
        if (o == null) {
            return false;
        }
        final int q = this.q(o);
        return this.J(q).containsKey(o, q);
    }
    
    @Override
    public boolean containsValue(final Object o) {
        if (o == null) {
            return false;
        }
        final long a = this.q.a();
        final Segment[] c = this.c;
        long n = -1L;
        long n2;
        for (int i = 0; i < 3; ++i, n = n2) {
            final int length = c.length;
            n2 = 0L;
            for (final Segment segment : c) {
                final int count = segment.count;
                final AtomicReferenceArray<a> table = segment.table;
                for (int k = 0; k < table.length(); ++k) {
                    for (a next = table.get(k); next != null; next = next.getNext()) {
                        final Object liveValue = segment.getLiveValue(next, a);
                        if (liveValue != null && this.f.equivalent(o, liveValue)) {
                            return true;
                        }
                    }
                }
                n2 += segment.modCount;
            }
            if (n2 == n) {
                break;
            }
        }
        return false;
    }
    
    public Segment e(final int n, final long n2, final j j) {
        return new Segment(this, n, n2, j);
    }
    
    @Override
    public Set entrySet() {
        Set z = this.z;
        if (z == null) {
            z = new g();
            this.z = z;
        }
        return z;
    }
    
    public boolean f() {
        return this.j != CacheBuilder.OneWeigher.INSTANCE;
    }
    
    @Override
    public Object get(final Object o) {
        if (o == null) {
            return null;
        }
        final int q = this.q(o);
        return this.J(q).get(o, q);
    }
    
    @Override
    public Object getOrDefault(Object value, Object o) {
        value = this.get(value);
        if (value != null) {
            o = value;
        }
        return o;
    }
    
    public boolean h() {
        return this.i >= 0L;
    }
    
    public boolean i() {
        return this.k > 0L;
    }
    
    @Override
    public boolean isEmpty() {
        final Segment[] c = this.c;
        boolean b = false;
        long n = 0L;
        for (int i = 0; i < c.length; ++i) {
            if (c[i].count != 0) {
                return false;
            }
            n += c[i].modCount;
        }
        if (n != 0L) {
            for (int j = 0; j < c.length; ++j) {
                if (c[j].count != 0) {
                    return false;
                }
                n -= c[j].modCount;
            }
            if (n == 0L) {
                b = true;
            }
            return b;
        }
        return true;
    }
    
    public boolean j() {
        return this.l > 0L;
    }
    
    public Object k(final Object o, final CacheLoader cacheLoader) {
        final int q = this.q(i71.r(o));
        return this.J(q).get(o, q, cacheLoader);
    }
    
    @Override
    public Set keySet() {
        Set x = this.x;
        if (x == null) {
            x = new j();
            this.x = x;
        }
        return x;
    }
    
    public ImmutableMap l(final Iterable iterable) {
        final LinkedHashMap r = Maps.r();
        final LinkedHashSet e = Sets.e();
        final Iterator iterator = iterable.iterator();
        int n = 0;
        int n2 = 0;
        while (iterator.hasNext()) {
            final Object next = iterator.next();
            final Object value = this.get(next);
            if (!r.containsKey(next)) {
                r.put(next, value);
                if (value == null) {
                    ++n2;
                    e.add(next);
                }
                else {
                    ++n;
                }
            }
        }
        int n3 = n2;
        try {
            final boolean empty = e.isEmpty();
            int n4 = n2;
            Label_0344: {
                if (!empty) {
                    n3 = n2;
                    try {
                        final Map t = this.t(Collections.unmodifiableSet((Set<?>)e), this.w);
                        n3 = n2;
                        final Iterator iterator2 = e.iterator();
                        Object next2;
                        while (true) {
                            n4 = n2;
                            n3 = n2;
                            if (!iterator2.hasNext()) {
                                break Label_0344;
                            }
                            n3 = n2;
                            next2 = iterator2.next();
                            n3 = n2;
                            final Object value2 = t.get(next2);
                            if (value2 == null) {
                                break;
                            }
                            n3 = n2;
                            r.put(next2, value2);
                        }
                        n3 = n2;
                        n3 = n2;
                        final String value3 = String.valueOf(next2);
                        n3 = n2;
                        final int length = value3.length();
                        n3 = n2;
                        n3 = n2;
                        final StringBuilder sb = new StringBuilder(length + 37);
                        n3 = n2;
                        sb.append("loadAll failed to return a value for ");
                        n3 = n2;
                        sb.append(value3);
                        n3 = n2;
                        final CacheLoader.InvalidCacheLoadException ex = new CacheLoader.InvalidCacheLoadException(sb.toString());
                        n3 = n2;
                        throw ex;
                    }
                    catch (final CacheLoader.UnsupportedLoadingOperationException ex2) {
                        n3 = n2;
                        final Iterator iterator3 = e.iterator();
                        while (true) {
                            n4 = n2;
                            n3 = n2;
                            if (!iterator3.hasNext()) {
                                break;
                            }
                            n3 = n2;
                            final Object next3 = iterator3.next();
                            n3 = --n2;
                            r.put(next3, this.k(next3, this.w));
                        }
                    }
                }
            }
            n3 = n4;
            return ImmutableMap.copyOf((Map<?, ?>)r);
        }
        finally {
            this.v.a(n);
            this.v.d(n3);
        }
    }
    
    public ImmutableMap m(final Iterable iterable) {
        final ImmutableMap.b builder = ImmutableMap.builder();
        final Iterator iterator = iterable.iterator();
        int n = 0;
        int n2 = 0;
        while (iterator.hasNext()) {
            final Object next = iterator.next();
            final Object value = this.get(next);
            if (value == null) {
                ++n2;
            }
            else {
                builder.g(next, value);
                ++n;
            }
        }
        this.v.a(n);
        this.v.d(n2);
        return builder.c();
    }
    
    public Object n(Object value) {
        final int q = this.q(i71.r(value));
        value = this.J(q).get(value, q);
        final j v = this.v;
        if (value == null) {
            v.d(1);
        }
        else {
            v.a(1);
        }
        return value;
    }
    
    public Object o(final a a, final long n) {
        if (a.getKey() == null) {
            return null;
        }
        final Object value = a.getValueReference().get();
        if (value == null) {
            return null;
        }
        if (this.s(a, n)) {
            return null;
        }
        return value;
    }
    
    public Object p(final Object o) {
        return this.k(o, this.w);
    }
    
    @Override
    public Object put(final Object o, final Object o2) {
        i71.r(o);
        i71.r(o2);
        final int q = this.q(o);
        return this.J(q).put(o, q, o2, false);
    }
    
    @Override
    public void putAll(final Map map) {
        for (final Map.Entry<Object, V> entry : map.entrySet()) {
            this.put(entry.getKey(), entry.getValue());
        }
    }
    
    @Override
    public Object putIfAbsent(final Object o, final Object o2) {
        i71.r(o);
        i71.r(o2);
        final int q = this.q(o);
        return this.J(q).put(o, q, o2, true);
    }
    
    public int q(final Object o) {
        return I(this.e.hash(o));
    }
    
    public void r(final Iterable iterable) {
        final Iterator iterator = iterable.iterator();
        while (iterator.hasNext()) {
            this.remove(iterator.next());
        }
    }
    
    @Override
    public Object remove(final Object o) {
        if (o == null) {
            return null;
        }
        final int q = this.q(o);
        return this.J(q).remove(o, q);
    }
    
    @Override
    public boolean remove(final Object o, final Object o2) {
        if (o != null && o2 != null) {
            final int q = this.q(o);
            return this.J(q).remove(o, q, o2);
        }
        return false;
    }
    
    @Override
    public Object replace(final Object o, final Object o2) {
        i71.r(o);
        i71.r(o2);
        final int q = this.q(o);
        return this.J(q).replace(o, q, o2);
    }
    
    @Override
    public boolean replace(final Object o, final Object o2, final Object o3) {
        i71.r(o);
        i71.r(o3);
        if (o2 == null) {
            return false;
        }
        final int q = this.q(o);
        return this.J(q).replace(o, q, o2, o3);
    }
    
    public boolean s(final a a, final long n) {
        i71.r(a);
        return (this.i() && n - a.getAccessTime() >= this.k) || (this.j() && n - a.getWriteTime() >= this.l);
    }
    
    @Override
    public int size() {
        return Ints.k(this.v());
    }
    
    public Map t(final Set set, final CacheLoader cacheLoader) {
        i71.r(cacheLoader);
        i71.r(set);
        final dq1 c = dq1.c();
        int n = 1;
        final int n2 = 0;
        try {
            try {
                final Map<Object, Object> loadAll = cacheLoader.loadAll(set);
                if (loadAll == null) {
                    this.v.e(c.e(TimeUnit.NANOSECONDS));
                    final String value = String.valueOf(cacheLoader);
                    final StringBuilder sb = new StringBuilder(value.length() + 31);
                    sb.append(value);
                    sb.append(" returned null map from loadAll");
                    throw new CacheLoader.InvalidCacheLoadException(sb.toString());
                }
                c.i();
                final Iterator<Map.Entry<Object, Object>> iterator = loadAll.entrySet().iterator();
                n = n2;
                while (iterator.hasNext()) {
                    final Map.Entry<Object, V> entry = (Map.Entry<Object, V>)iterator.next();
                    final Object key = entry.getKey();
                    final V value2 = entry.getValue();
                    if (key != null && value2 != null) {
                        this.put(key, value2);
                    }
                    else {
                        n = 1;
                    }
                }
                if (n == 0) {
                    this.v.c(c.e(TimeUnit.NANOSECONDS));
                    return loadAll;
                }
                this.v.e(c.e(TimeUnit.NANOSECONDS));
                final String value3 = String.valueOf(cacheLoader);
                final StringBuilder sb2 = new StringBuilder(value3.length() + 42);
                sb2.append(value3);
                sb2.append(" returned null keys or values from loadAll");
                throw new CacheLoader.InvalidCacheLoadException(sb2.toString());
            }
            finally {
                n = 0;
            }
        }
        catch (final Error error) {
            throw new ExecutionError(error);
        }
        catch (final Exception cause) {
            throw new ExecutionException(cause);
        }
        catch (final RuntimeException ex) {
            throw new UncheckedExecutionException(ex);
        }
        catch (final InterruptedException cause2) {
            Thread.currentThread().interrupt();
            throw new ExecutionException(cause2);
        }
        catch (final CacheLoader.UnsupportedLoadingOperationException ex2) {
            try {
                throw ex2;
            }
            finally {}
        }
        if (n == 0) {
            this.v.e(c.e(TimeUnit.NANOSECONDS));
        }
    }
    
    public long v() {
        final Segment[] c = this.c;
        long n = 0L;
        for (int i = 0; i < c.length; ++i) {
            n += Math.max(0, c[i].count);
        }
        return n;
    }
    
    @Override
    public Collection values() {
        Collection y = this.y;
        if (y == null) {
            y = new t();
            this.y = y;
        }
        return y;
    }
    
    public final Segment[] w(final int n) {
        return new Segment[n];
    }
    
    public enum EntryFactory
    {
        private static final EntryFactory[] $VALUES;
        static final int ACCESS_MASK = 1;
        
        STRONG {
            @Override
            public <K, V> a newEntry(final Segment<K, V> segment, final K k, final int n, final a a) {
                return new o(k, n, a);
            }
        }, 
        STRONG_ACCESS {
            @Override
            public <K, V> a copyEntry(final Segment<K, V> segment, final a a, final a a2) {
                final a copyEntry = super.copyEntry(segment, a, a2);
                this.copyAccessEntry(a, copyEntry);
                return copyEntry;
            }
            
            @Override
            public <K, V> a newEntry(final Segment<K, V> segment, final K k, final int n, final a a) {
                return new m(k, n, a);
            }
        }, 
        STRONG_ACCESS_WRITE {
            @Override
            public <K, V> a copyEntry(final Segment<K, V> segment, final a a, final a a2) {
                final a copyEntry = super.copyEntry(segment, a, a2);
                this.copyAccessEntry(a, copyEntry);
                this.copyWriteEntry(a, copyEntry);
                return copyEntry;
            }
            
            @Override
            public <K, V> a newEntry(final Segment<K, V> segment, final K k, final int n, final a a) {
                return new n(k, n, a);
            }
        }, 
        STRONG_WRITE {
            @Override
            public <K, V> a copyEntry(final Segment<K, V> segment, final a a, final a a2) {
                final a copyEntry = super.copyEntry(segment, a, a2);
                this.copyWriteEntry(a, copyEntry);
                return copyEntry;
            }
            
            @Override
            public <K, V> a newEntry(final Segment<K, V> segment, final K k, final int n, final a a) {
                return new q(k, n, a);
            }
        }, 
        WEAK {
            @Override
            public <K, V> a newEntry(final Segment<K, V> segment, final K k, final int n, final a a) {
                return new w(segment.keyReferenceQueue, k, n, a);
            }
        }, 
        WEAK_ACCESS {
            @Override
            public <K, V> a copyEntry(final Segment<K, V> segment, final a a, final a a2) {
                final a copyEntry = super.copyEntry(segment, a, a2);
                this.copyAccessEntry(a, copyEntry);
                return copyEntry;
            }
            
            @Override
            public <K, V> a newEntry(final Segment<K, V> segment, final K k, final int n, final a a) {
                return new u(segment.keyReferenceQueue, k, n, a);
            }
        }, 
        WEAK_ACCESS_WRITE {
            @Override
            public <K, V> a copyEntry(final Segment<K, V> segment, final a a, final a a2) {
                final a copyEntry = super.copyEntry(segment, a, a2);
                this.copyAccessEntry(a, copyEntry);
                this.copyWriteEntry(a, copyEntry);
                return copyEntry;
            }
            
            @Override
            public <K, V> a newEntry(final Segment<K, V> segment, final K k, final int n, final a a) {
                return new v(segment.keyReferenceQueue, k, n, a);
            }
        };
        
        static final int WEAK_MASK = 4;
        
        WEAK_WRITE {
            @Override
            public <K, V> a copyEntry(final Segment<K, V> segment, final a a, final a a2) {
                final a copyEntry = super.copyEntry(segment, a, a2);
                this.copyWriteEntry(a, copyEntry);
                return copyEntry;
            }
            
            @Override
            public <K, V> a newEntry(final Segment<K, V> segment, final K k, final int n, final a a) {
                return new y(segment.keyReferenceQueue, k, n, a);
            }
        };
        
        static final int WRITE_MASK = 2;
        static final EntryFactory[] factories;
        
        private static /* synthetic */ EntryFactory[] $values() {
            return new EntryFactory[] { EntryFactory.STRONG, EntryFactory.STRONG_ACCESS, EntryFactory.STRONG_WRITE, EntryFactory.STRONG_ACCESS_WRITE, EntryFactory.WEAK, EntryFactory.WEAK_ACCESS, EntryFactory.WEAK_WRITE, EntryFactory.WEAK_ACCESS_WRITE };
        }
        
        static {
            $VALUES = $values();
            final EntryFactory entryFactory;
            final EntryFactory entryFactory2;
            final EntryFactory entryFactory3;
            final EntryFactory entryFactory4;
            final EntryFactory entryFactory5;
            final EntryFactory entryFactory6;
            final EntryFactory entryFactory7;
            final EntryFactory entryFactory8;
            factories = new EntryFactory[] { entryFactory, entryFactory2, entryFactory3, entryFactory4, entryFactory5, entryFactory6, entryFactory7, entryFactory8 };
        }
        
        public static EntryFactory getFactory(final Strength strength, final boolean b, final boolean b2) {
            final Strength weak = Strength.WEAK;
            int n = 0;
            int n2;
            if (strength == weak) {
                n2 = 4;
            }
            else {
                n2 = 0;
            }
            if (b2) {
                n = 2;
            }
            return EntryFactory.factories[n2 | (b ? 1 : 0) | n];
        }
        
        public <K, V> void copyAccessEntry(final a a, final a a2) {
            a2.setAccessTime(a.getAccessTime());
            LocalCache.c(a.getPreviousInAccessQueue(), a2);
            LocalCache.c(a2, a.getNextInAccessQueue());
            LocalCache.y(a);
        }
        
        public <K, V> a copyEntry(final Segment<K, V> segment, final a a, final a a2) {
            return this.newEntry((Segment<Object, V>)segment, a.getKey(), a.getHash(), a2);
        }
        
        public <K, V> void copyWriteEntry(final a a, final a a2) {
            a2.setWriteTime(a.getWriteTime());
            LocalCache.d(a.getPreviousInWriteQueue(), a2);
            LocalCache.d(a2, a.getNextInWriteQueue());
            LocalCache.z(a);
        }
        
        public abstract <K, V> a newEntry(final Segment<K, V> p0, final K p1, final int p2, final a p3);
    }
    
    public static final class LoadingSerializationProxy<K, V> extends ManualSerializationProxy<K, V> implements tk0
    {
        private static final long serialVersionUID = 1L;
        transient tk0 autoDelegate;
        
        public LoadingSerializationProxy(final LocalCache localCache) {
            super(localCache);
        }
        
        private void readObject(final ObjectInputStream objectInputStream) {
            objectInputStream.defaultReadObject();
            this.autoDelegate = this.recreateCacheBuilder().b(super.loader);
        }
        
        private Object readResolve() {
            return this.autoDelegate;
        }
        
        @Override
        public final V apply(final K k) {
            return (V)this.autoDelegate.apply(k);
        }
        
        @Override
        public V get(final K k) {
            return (V)this.autoDelegate.get(k);
        }
        
        @Override
        public ImmutableMap<K, V> getAll(final Iterable<? extends K> iterable) {
            return this.autoDelegate.getAll(iterable);
        }
        
        @Override
        public V getUnchecked(final K k) {
            return (V)this.autoDelegate.getUnchecked(k);
        }
        
        @Override
        public void refresh(final K k) {
            this.autoDelegate.refresh(k);
        }
    }
    
    public static class LocalLoadingCache<K, V> extends LocalManualCache<K, V> implements tk0
    {
        private static final long serialVersionUID = 1L;
        
        public LocalLoadingCache(final CacheBuilder cacheBuilder, final CacheLoader cacheLoader) {
            super(new LocalCache(cacheBuilder, (CacheLoader)i71.r(cacheLoader)), null);
        }
        
        @Override
        public final V apply(final K k) {
            return this.getUnchecked(k);
        }
        
        @Override
        public V get(final K k) {
            return (V)super.localCache.p(k);
        }
        
        @Override
        public ImmutableMap<K, V> getAll(final Iterable<? extends K> iterable) {
            return super.localCache.l(iterable);
        }
        
        @Override
        public V getUnchecked(final K k) {
            try {
                return this.get(k);
            }
            catch (final ExecutionException ex) {
                throw new UncheckedExecutionException(ex.getCause());
            }
        }
        
        @Override
        public void refresh(final K k) {
            super.localCache.G(k);
        }
        
        public Object writeReplace() {
            return new LoadingSerializationProxy(super.localCache);
        }
    }
    
    public static class LocalManualCache<K, V> implements ne, Serializable
    {
        private static final long serialVersionUID = 1L;
        final LocalCache localCache;
        
        public LocalManualCache(final CacheBuilder cacheBuilder) {
            this(new LocalCache(cacheBuilder, null));
        }
        
        private LocalManualCache(final LocalCache localCache) {
            this.localCache = localCache;
        }
        
        @Override
        public ConcurrentMap<K, V> asMap() {
            return this.localCache;
        }
        
        @Override
        public void cleanUp() {
            this.localCache.b();
        }
        
        @Override
        public V get(final K k, final Callable<? extends V> callable) {
            i71.r(callable);
            return (V)this.localCache.k(k, new CacheLoader(this, callable) {
                public final Callable a;
                
                @Override
                public Object load(final Object o) {
                    return this.a.call();
                }
            });
        }
        
        @Override
        public ImmutableMap<K, V> getAllPresent(final Iterable<?> iterable) {
            return this.localCache.m(iterable);
        }
        
        @Override
        public V getIfPresent(final Object o) {
            return (V)this.localCache.n(o);
        }
        
        @Override
        public void invalidate(final Object o) {
            i71.r(o);
            this.localCache.remove(o);
        }
        
        @Override
        public void invalidateAll() {
            this.localCache.clear();
        }
        
        @Override
        public void invalidateAll(final Iterable<?> iterable) {
            this.localCache.r(iterable);
        }
        
        @Override
        public void put(final K k, final V v) {
            this.localCache.put(k, v);
        }
        
        @Override
        public void putAll(final Map<? extends K, ? extends V> map) {
            this.localCache.putAll(map);
        }
        
        @Override
        public long size() {
            return this.localCache.v();
        }
        
        @Override
        public pe stats() {
            final i i = new i();
            i.g(this.localCache.v);
            final Segment[] c = this.localCache.c;
            for (int length = c.length, j = 0; j < length; ++j) {
                i.g(c[j].statsCounter);
            }
            return i.f();
        }
        
        Object writeReplace() {
            return new ManualSerializationProxy(this.localCache);
        }
    }
    
    public static class ManualSerializationProxy<K, V> extends s70 implements Serializable
    {
        private static final long serialVersionUID = 1L;
        final int concurrencyLevel;
        transient ne delegate;
        final long expireAfterAccessNanos;
        final long expireAfterWriteNanos;
        final Equivalence keyEquivalence;
        final Strength keyStrength;
        final CacheLoader loader;
        final long maxWeight;
        final pd1 removalListener;
        final hw1 ticker;
        final Equivalence valueEquivalence;
        final Strength valueStrength;
        final y52 weigher;
        
        private ManualSerializationProxy(final Strength keyStrength, final Strength valueStrength, final Equivalence keyEquivalence, final Equivalence valueEquivalence, final long expireAfterWriteNanos, final long expireAfterAccessNanos, final long maxWeight, final y52 weigher, final int concurrencyLevel, final pd1 removalListener, final hw1 hw1, final CacheLoader loader) {
            this.keyStrength = keyStrength;
            this.valueStrength = valueStrength;
            this.keyEquivalence = keyEquivalence;
            this.valueEquivalence = valueEquivalence;
            this.expireAfterWriteNanos = expireAfterWriteNanos;
            this.expireAfterAccessNanos = expireAfterAccessNanos;
            this.maxWeight = maxWeight;
            this.weigher = weigher;
            this.concurrencyLevel = concurrencyLevel;
            this.removalListener = removalListener;
            hw1 ticker;
            if (hw1 == hw1.b() || (ticker = hw1) == CacheBuilder.t) {
                ticker = null;
            }
            this.ticker = ticker;
            this.loader = loader;
        }
        
        public ManualSerializationProxy(final LocalCache localCache) {
            this(localCache.g, localCache.h, localCache.e, localCache.f, localCache.l, localCache.k, localCache.i, localCache.j, localCache.d, localCache.p, localCache.q, localCache.w);
        }
        
        private void readObject(final ObjectInputStream objectInputStream) {
            objectInputStream.defaultReadObject();
            this.delegate = this.recreateCacheBuilder().a();
        }
        
        private Object readResolve() {
            return this.delegate;
        }
        
        @Override
        public ne delegate() {
            return this.delegate;
        }
        
        public CacheBuilder recreateCacheBuilder() {
            final CacheBuilder z = CacheBuilder.y().A(this.keyStrength).B(this.valueStrength).v(this.keyEquivalence).D(this.valueEquivalence).e(this.concurrencyLevel).z(this.removalListener);
            z.a = false;
            final long expireAfterWriteNanos = this.expireAfterWriteNanos;
            if (expireAfterWriteNanos > 0L) {
                z.g(expireAfterWriteNanos, TimeUnit.NANOSECONDS);
            }
            final long expireAfterAccessNanos = this.expireAfterAccessNanos;
            if (expireAfterAccessNanos > 0L) {
                z.f(expireAfterAccessNanos, TimeUnit.NANOSECONDS);
            }
            final y52 weigher = this.weigher;
            if (weigher != CacheBuilder.OneWeigher.INSTANCE) {
                z.E(weigher);
                final long maxWeight = this.maxWeight;
                if (maxWeight != -1L) {
                    z.x(maxWeight);
                }
            }
            else {
                final long maxWeight2 = this.maxWeight;
                if (maxWeight2 != -1L) {
                    z.w(maxWeight2);
                }
            }
            final hw1 ticker = this.ticker;
            if (ticker != null) {
                z.C(ticker);
            }
            return z;
        }
    }
    
    public enum NullEntry implements a
    {
        private static final NullEntry[] $VALUES;
        
        INSTANCE;
        
        private static /* synthetic */ NullEntry[] $values() {
            return new NullEntry[] { NullEntry.INSTANCE };
        }
        
        static {
            $VALUES = $values();
        }
        
        @Override
        public long getAccessTime() {
            return 0L;
        }
        
        @Override
        public int getHash() {
            return 0;
        }
        
        @Override
        public Object getKey() {
            return null;
        }
        
        @Override
        public a getNext() {
            return null;
        }
        
        @Override
        public a getNextInAccessQueue() {
            return this;
        }
        
        @Override
        public a getNextInWriteQueue() {
            return this;
        }
        
        @Override
        public a getPreviousInAccessQueue() {
            return this;
        }
        
        @Override
        public a getPreviousInWriteQueue() {
            return this;
        }
        
        @Override
        public s getValueReference() {
            return null;
        }
        
        @Override
        public long getWriteTime() {
            return 0L;
        }
        
        @Override
        public void setAccessTime(final long n) {
        }
        
        @Override
        public void setNextInAccessQueue(final a a) {
        }
        
        @Override
        public void setNextInWriteQueue(final a a) {
        }
        
        @Override
        public void setPreviousInAccessQueue(final a a) {
        }
        
        @Override
        public void setPreviousInWriteQueue(final a a) {
        }
        
        @Override
        public void setValueReference(final s s) {
        }
        
        @Override
        public void setWriteTime(final long n) {
        }
    }
    
    public static class Segment<K, V> extends ReentrantLock
    {
        final Queue<a> accessQueue;
        volatile int count;
        final ReferenceQueue<K> keyReferenceQueue;
        final LocalCache map;
        final long maxSegmentWeight;
        int modCount;
        final AtomicInteger readCount;
        final Queue<a> recencyQueue;
        final j statsCounter;
        volatile AtomicReferenceArray<a> table;
        int threshold;
        long totalWeight;
        final ReferenceQueue<V> valueReferenceQueue;
        final Queue<a> writeQueue;
        
        public Segment(final LocalCache map, final int n, final long maxSegmentWeight, final j j) {
            this.readCount = new AtomicInteger();
            this.map = map;
            this.maxSegmentWeight = maxSegmentWeight;
            this.statsCounter = (j)i71.r(j);
            this.initTable(this.newEntryArray(n));
            final boolean o = map.O();
            final ReferenceQueue<V> referenceQueue = null;
            ReferenceQueue keyReferenceQueue;
            if (o) {
                keyReferenceQueue = new ReferenceQueue();
            }
            else {
                keyReferenceQueue = null;
            }
            this.keyReferenceQueue = keyReferenceQueue;
            ReferenceQueue<V> valueReferenceQueue = referenceQueue;
            if (map.P()) {
                valueReferenceQueue = new ReferenceQueue<V>();
            }
            this.valueReferenceQueue = valueReferenceQueue;
            Queue<a> g;
            if (map.N()) {
                g = new ConcurrentLinkedQueue<a>();
            }
            else {
                g = LocalCache.g();
            }
            this.recencyQueue = g;
            Queue g2;
            if (map.R()) {
                g2 = new c0();
            }
            else {
                g2 = LocalCache.g();
            }
            this.writeQueue = g2;
            Queue g3;
            if (map.N()) {
                g3 = new e();
            }
            else {
                g3 = LocalCache.g();
            }
            this.accessQueue = g3;
        }
        
        public void cleanUp() {
            this.runLockedCleanup(this.map.q.a());
            this.runUnlockedCleanup();
        }
        
        public void clear() {
            if (this.count != 0) {
                this.lock();
                try {
                    this.preWriteCleanup(this.map.q.a());
                    final AtomicReferenceArray<a> table = this.table;
                    for (int i = 0; i < table.length(); ++i) {
                        for (a next = table.get(i); next != null; next = next.getNext()) {
                            if (next.getValueReference().a()) {
                                final Object key = next.getKey();
                                final Object value = next.getValueReference().get();
                                RemovalCause removalCause;
                                if (key != null && value != null) {
                                    removalCause = RemovalCause.EXPLICIT;
                                }
                                else {
                                    removalCause = RemovalCause.COLLECTED;
                                }
                                this.enqueueNotification((K)key, next.getHash(), (V)value, next.getValueReference().d(), removalCause);
                            }
                        }
                    }
                    for (int j = 0; j < table.length(); ++j) {
                        table.set(j, null);
                    }
                    this.clearReferenceQueues();
                    this.writeQueue.clear();
                    this.accessQueue.clear();
                    this.readCount.set(0);
                    ++this.modCount;
                    this.count = 0;
                }
                finally {
                    this.unlock();
                    this.postWriteCleanup();
                }
            }
        }
        
        public void clearKeyReferenceQueue() {
            while (this.keyReferenceQueue.poll() != null) {}
        }
        
        public void clearReferenceQueues() {
            if (this.map.O()) {
                this.clearKeyReferenceQueue();
            }
            if (this.map.P()) {
                this.clearValueReferenceQueue();
            }
        }
        
        public void clearValueReferenceQueue() {
            while (this.valueReferenceQueue.poll() != null) {}
        }
        
        public boolean containsKey(Object value, final int n) {
            try {
                final int count = this.count;
                boolean b = false;
                if (count == 0) {
                    return false;
                }
                final a liveEntry = this.getLiveEntry(value, n, this.map.q.a());
                if (liveEntry == null) {
                    return false;
                }
                value = liveEntry.getValueReference().get();
                if (value != null) {
                    b = true;
                }
                return b;
            }
            finally {
                this.postReadCleanup();
            }
        }
        
        public boolean containsValue(final Object o) {
            try {
                if (this.count != 0) {
                    final long a = this.map.q.a();
                    final AtomicReferenceArray<a> table = this.table;
                    for (int length = table.length(), i = 0; i < length; ++i) {
                        for (a next = table.get(i); next != null; next = next.getNext()) {
                            final V liveValue = this.getLiveValue(next, a);
                            if (liveValue != null) {
                                if (this.map.f.equivalent(o, liveValue)) {
                                    return true;
                                }
                            }
                        }
                    }
                }
                return false;
            }
            finally {
                this.postReadCleanup();
            }
        }
        
        public a copyEntry(a copyEntry, final a a) {
            if (copyEntry.getKey() == null) {
                return null;
            }
            final s valueReference = copyEntry.getValueReference();
            final Object value = valueReference.get();
            if (value == null && valueReference.a()) {
                return null;
            }
            copyEntry = this.map.t.copyEntry((Segment<Object, Object>)this, copyEntry, a);
            copyEntry.setValueReference(valueReference.g(this.valueReferenceQueue, value, copyEntry));
            return copyEntry;
        }
        
        public void drainKeyReferenceQueue() {
            int n = 0;
            do {
                final Reference<? extends K> poll = this.keyReferenceQueue.poll();
                if (poll == null) {
                    break;
                }
                this.map.B((a)poll);
            } while (++n != 16);
        }
        
        public void drainRecencyQueue() {
            while (true) {
                final a a = this.recencyQueue.poll();
                if (a == null) {
                    break;
                }
                if (!this.accessQueue.contains(a)) {
                    continue;
                }
                this.accessQueue.add(a);
            }
        }
        
        public void drainReferenceQueues() {
            if (this.map.O()) {
                this.drainKeyReferenceQueue();
            }
            if (this.map.P()) {
                this.drainValueReferenceQueue();
            }
        }
        
        public void drainValueReferenceQueue() {
            int n = 0;
            do {
                final Reference<? extends V> poll = this.valueReferenceQueue.poll();
                if (poll == null) {
                    break;
                }
                this.map.C((s)poll);
            } while (++n != 16);
        }
        
        public void enqueueNotification(final K k, final int n, final V v, final int n2, final RemovalCause removalCause) {
            this.totalWeight -= n2;
            if (removalCause.wasEvicted()) {
                this.statsCounter.b();
            }
            if (this.map.n != LocalCache.D) {
                this.map.n.offer(RemovalNotification.create(k, v, removalCause));
            }
        }
        
        public void evictEntries(a nextEvictable) {
            if (!this.map.h()) {
                return;
            }
            this.drainRecencyQueue();
            if (nextEvictable.getValueReference().d() > this.maxSegmentWeight) {
                if (!this.removeEntry(nextEvictable, nextEvictable.getHash(), RemovalCause.SIZE)) {
                    throw new AssertionError();
                }
            }
            while (this.totalWeight > this.maxSegmentWeight) {
                nextEvictable = this.getNextEvictable();
                if (this.removeEntry(nextEvictable, nextEvictable.getHash(), RemovalCause.SIZE)) {
                    continue;
                }
                throw new AssertionError();
            }
        }
        
        public void expand() {
            final AtomicReferenceArray<a> table = this.table;
            final int length = table.length();
            if (length >= 1073741824) {
                return;
            }
            int count = this.count;
            final AtomicReferenceArray<a> entryArray = this.newEntryArray(length << 1);
            this.threshold = entryArray.length() * 3 / 4;
            final int n = entryArray.length() - 1;
            int n2;
            for (int i = 0; i < length; ++i, count = n2) {
                a next = table.get(i);
                n2 = count;
                if (next != null) {
                    a a = next.getNext();
                    int n3 = next.getHash() & n;
                    if (a == null) {
                        entryArray.set(n3, next);
                        n2 = count;
                    }
                    else {
                        a newValue = next;
                        while (a != null) {
                            final int n4 = a.getHash() & n;
                            int n5;
                            if (n4 != (n5 = n3)) {
                                newValue = a;
                                n5 = n4;
                            }
                            a = a.getNext();
                            n3 = n5;
                        }
                        entryArray.set(n3, newValue);
                        while (true) {
                            n2 = count;
                            if (next == newValue) {
                                break;
                            }
                            final int n6 = next.getHash() & n;
                            final a copyEntry = this.copyEntry(next, entryArray.get(n6));
                            if (copyEntry != null) {
                                entryArray.set(n6, copyEntry);
                            }
                            else {
                                this.removeCollectedEntry(next);
                                --count;
                            }
                            next = next.getNext();
                        }
                    }
                }
            }
            this.table = entryArray;
            this.count = count;
        }
        
        public void expireEntries(final long n) {
            this.drainRecencyQueue();
            while (true) {
                final a a = this.writeQueue.peek();
                if (a == null || !this.map.s(a, n)) {
                    break;
                }
                if (this.removeEntry(a, a.getHash(), RemovalCause.EXPIRED)) {
                    continue;
                }
                throw new AssertionError();
            }
            while (true) {
                final a a2 = this.accessQueue.peek();
                if (a2 == null || !this.map.s(a2, n)) {
                    return;
                }
                if (this.removeEntry(a2, a2.getHash(), RemovalCause.EXPIRED)) {
                    continue;
                }
                throw new AssertionError();
            }
        }
        
        public V get(Object scheduleRefresh, final int n) {
            try {
                if (this.count != 0) {
                    final long a = this.map.q.a();
                    final a liveEntry = this.getLiveEntry(scheduleRefresh, n, a);
                    if (liveEntry == null) {
                        return null;
                    }
                    final Object value = liveEntry.getValueReference().get();
                    if (value != null) {
                        this.recordRead(liveEntry, a);
                        scheduleRefresh = this.scheduleRefresh(liveEntry, liveEntry.getKey(), n, value, a, this.map.w);
                        return (V)scheduleRefresh;
                    }
                    this.tryDrainReferenceQueues();
                }
                return null;
            }
            finally {
                this.postReadCleanup();
            }
        }
        
        public V get(final K k, final int n, final CacheLoader cacheLoader) {
            i71.r(k);
            i71.r(cacheLoader);
            try {
                try {
                    if (this.count != 0) {
                        final a entry = this.getEntry(k, n);
                        if (entry != null) {
                            final long a = this.map.q.a();
                            final V liveValue = this.getLiveValue(entry, a);
                            if (liveValue != null) {
                                this.recordRead(entry, a);
                                this.statsCounter.a(1);
                                final Object scheduleRefresh = this.scheduleRefresh(entry, k, n, liveValue, a, cacheLoader);
                                this.postReadCleanup();
                                return (V)scheduleRefresh;
                            }
                            final s valueReference = entry.getValueReference();
                            if (valueReference.f()) {
                                final V waitForLoadingValue = this.waitForLoadingValue(entry, k, valueReference);
                                this.postReadCleanup();
                                return waitForLoadingValue;
                            }
                        }
                    }
                    final V lockedGetOrLoad = this.lockedGetOrLoad(k, n, cacheLoader);
                    this.postReadCleanup();
                    return lockedGetOrLoad;
                }
                finally {}
            }
            catch (final ExecutionException ex) {
                final Throwable cause = ex.getCause();
                if (cause instanceof Error) {
                    throw new ExecutionError((Error)cause);
                }
                if (cause instanceof RuntimeException) {
                    throw new UncheckedExecutionException(cause);
                }
                throw ex;
            }
            this.postReadCleanup();
        }
        
        public V getAndRecordStats(final K obj, final int n, final k k, final ik0 ik0) {
            Object o = null;
            Label_0120: {
                try {
                    final Object a = t02.a(ik0);
                    if (a != null) {
                        try {
                            this.statsCounter.c(k.h());
                            this.storeLoadedValue(obj, n, k, (V)a);
                            return (V)a;
                        }
                        finally {
                            break Label_0120;
                        }
                    }
                    final String value = String.valueOf(obj);
                    final StringBuilder sb = new StringBuilder(value.length() + 35);
                    sb.append("CacheLoader returned null for key ");
                    sb.append(value);
                    sb.append(".");
                    throw new CacheLoader.InvalidCacheLoadException(sb.toString());
                }
                finally {
                    o = null;
                }
            }
            if (o == null) {
                this.statsCounter.e(k.h());
                this.removeLoadingValue(obj, n, k);
            }
        }
        
        public a getEntry(final Object o, final int n) {
            for (a a = this.getFirst(n); a != null; a = a.getNext()) {
                if (a.getHash() == n) {
                    final Object key = a.getKey();
                    if (key == null) {
                        this.tryDrainReferenceQueues();
                    }
                    else if (this.map.e.equivalent(o, key)) {
                        return a;
                    }
                }
            }
            return null;
        }
        
        public a getFirst(final int n) {
            final AtomicReferenceArray<a> table = this.table;
            return table.get(n & table.length() - 1);
        }
        
        public a getLiveEntry(final Object o, final int n, final long n2) {
            final a entry = this.getEntry(o, n);
            if (entry == null) {
                return null;
            }
            if (this.map.s(entry, n2)) {
                this.tryExpireEntries(n2);
                return null;
            }
            return entry;
        }
        
        public V getLiveValue(final a a, final long n) {
            if (a.getKey() == null) {
                this.tryDrainReferenceQueues();
                return null;
            }
            final Object value = a.getValueReference().get();
            if (value == null) {
                this.tryDrainReferenceQueues();
                return null;
            }
            if (this.map.s(a, n)) {
                this.tryExpireEntries(n);
                return null;
            }
            return (V)value;
        }
        
        public a getNextEvictable() {
            for (final a a : this.accessQueue) {
                if (a.getValueReference().d() > 0) {
                    return a;
                }
            }
            throw new AssertionError();
        }
        
        public void initTable(final AtomicReferenceArray<a> table) {
            this.threshold = table.length() * 3 / 4;
            if (!this.map.f()) {
                final int threshold = this.threshold;
                if (threshold == this.maxSegmentWeight) {
                    this.threshold = threshold + 1;
                }
            }
            this.table = table;
        }
        
        public k insertLoadingValueReference(final K k, final int n, final boolean b) {
            this.lock();
            try {
                final long a = this.map.q.a();
                this.preWriteCleanup(a);
                final AtomicReferenceArray<a> table = this.table;
                final int n2 = table.length() - 1 & n;
                a next;
                final a a2 = next = table.get(n2);
                while (next != null) {
                    final Object key = next.getKey();
                    if (next.getHash() == n && key != null && this.map.e.equivalent(k, key)) {
                        final s valueReference = next.getValueReference();
                        if (!valueReference.f() && (!b || a - next.getWriteTime() >= this.map.m)) {
                            ++this.modCount;
                            final k valueReference2 = new k(valueReference);
                            next.setValueReference(valueReference2);
                            return valueReference2;
                        }
                        return null;
                    }
                    else {
                        next = next.getNext();
                    }
                }
                ++this.modCount;
                final k valueReference3 = new k();
                final a entry = this.newEntry(k, n, a2);
                entry.setValueReference(valueReference3);
                table.set(n2, entry);
                return valueReference3;
            }
            finally {
                this.unlock();
                this.postWriteCleanup();
            }
        }
        
        public ik0 loadAsync(final K k, final int n, final k i, final CacheLoader cacheLoader) {
            final ik0 j = i.k(k, cacheLoader);
            j.addListener(new Runnable(this, k, n, i, j) {
                public final Object a;
                public final int b;
                public final k c;
                public final ik0 d;
                public final Segment e;
                
                @Override
                public void run() {
                    try {
                        this.e.getAndRecordStats(this.a, this.b, this.c, this.d);
                    }
                    finally {
                        final Throwable thrown;
                        LocalCache.A.log(Level.WARNING, "Exception thrown during refresh", thrown);
                        this.c.m(thrown);
                    }
                }
            }, com.google.common.util.concurrent.g.a());
            return j;
        }
        
        public V loadSync(final K k, final int n, final k i, final CacheLoader cacheLoader) {
            return this.getAndRecordStats(k, n, i, i.k(k, cacheLoader));
        }
        
        public V lockedGetOrLoad(final K k, final int n, final CacheLoader cacheLoader) {
            this.lock();
            try {
                final long a = this.map.q.a();
                this.preWriteCleanup(a);
                final int count = this.count;
                final AtomicReferenceArray<a> table = this.table;
                final int n2 = n & table.length() - 1;
                a next;
                final a a2 = next = table.get(n2);
                k i;
                boolean b;
                s valueReference;
                while (true) {
                    i = null;
                    if (next == null) {
                        b = true;
                        valueReference = null;
                        break;
                    }
                    final Object key = next.getKey();
                    if (next.getHash() == n && key != null && this.map.e.equivalent(k, key)) {
                        valueReference = next.getValueReference();
                        if (valueReference.f()) {
                            b = false;
                        }
                        else {
                            final Object value = valueReference.get();
                            int n3;
                            RemovalCause removalCause;
                            if (value == null) {
                                n3 = valueReference.d();
                                removalCause = RemovalCause.COLLECTED;
                            }
                            else {
                                if (!this.map.s(next, a)) {
                                    this.recordLockedRead(next, a);
                                    this.statsCounter.a(1);
                                    return (V)value;
                                }
                                n3 = valueReference.d();
                                removalCause = RemovalCause.EXPIRED;
                            }
                            this.enqueueNotification((K)key, n, (V)value, n3, removalCause);
                            this.writeQueue.remove(next);
                            this.accessQueue.remove(next);
                            this.count = count - 1;
                            b = true;
                        }
                        break;
                    }
                    next = next.getNext();
                }
                a entry = next;
                if (b) {
                    i = new k();
                    if (next == null) {
                        entry = this.newEntry(k, n, a2);
                        entry.setValueReference(i);
                        table.set(n2, entry);
                    }
                    else {
                        next.setValueReference(i);
                        entry = next;
                    }
                }
                this.unlock();
                this.postWriteCleanup();
                if (b) {
                    try {
                        synchronized (entry) {
                            return this.loadSync(k, n, i, cacheLoader);
                        }
                    }
                    finally {
                        this.statsCounter.d(1);
                    }
                }
                return this.waitForLoadingValue(entry, k, valueReference);
            }
            finally {
                this.unlock();
                this.postWriteCleanup();
            }
        }
        
        public a newEntry(final K k, final int n, final a a) {
            return this.map.t.newEntry((Segment<Object, Object>)this, i71.r(k), n, a);
        }
        
        public AtomicReferenceArray<a> newEntryArray(final int length) {
            return new AtomicReferenceArray<a>(length);
        }
        
        public void postReadCleanup() {
            if ((this.readCount.incrementAndGet() & 0x3F) == 0x0) {
                this.cleanUp();
            }
        }
        
        public void postWriteCleanup() {
            this.runUnlockedCleanup();
        }
        
        public void preWriteCleanup(final long n) {
            this.runLockedCleanup(n);
        }
        
        public V put(final K k, int count, final V v, final boolean b) {
            this.lock();
            try {
                final long a = this.map.q.a();
                this.preWriteCleanup(a);
                if (this.count + 1 > this.threshold) {
                    this.expand();
                }
                final AtomicReferenceArray<a> table = this.table;
                final int n = count & table.length() - 1;
                a newValue;
                final a a2 = newValue = table.get(n);
                while (true) {
                    while (newValue != null) {
                        final Object key = newValue.getKey();
                        if (newValue.getHash() == count && key != null && this.map.e.equivalent(k, key)) {
                            final s valueReference = newValue.getValueReference();
                            final Object value = valueReference.get();
                            if (value == null) {
                                ++this.modCount;
                                if (valueReference.a()) {
                                    this.enqueueNotification(k, count, (V)value, valueReference.d(), RemovalCause.COLLECTED);
                                    this.setValue(newValue, k, v, a);
                                    count = this.count;
                                }
                                else {
                                    this.setValue(newValue, k, v, a);
                                    count = this.count + 1;
                                }
                                this.count = count;
                                this.evictEntries(newValue);
                                return null;
                            }
                            if (b) {
                                this.recordLockedRead(newValue, a);
                            }
                            else {
                                ++this.modCount;
                                this.enqueueNotification(k, count, (V)value, valueReference.d(), RemovalCause.REPLACED);
                                this.setValue(newValue, k, v, a);
                                this.evictEntries(newValue);
                            }
                            return (V)value;
                        }
                        else {
                            newValue = newValue.getNext();
                        }
                    }
                    ++this.modCount;
                    newValue = this.newEntry(k, count, a2);
                    this.setValue(newValue, k, v, a);
                    table.set(n, newValue);
                    ++this.count;
                    continue;
                }
            }
            finally {
                this.unlock();
                this.postWriteCleanup();
            }
        }
        
        public boolean reclaimKey(a removeValueFromChain, int count) {
            this.lock();
            try {
                final AtomicReferenceArray<a> table = this.table;
                final int n = table.length() - 1 & count;
                a next;
                for (a a = next = table.get(n); next != null; next = next.getNext()) {
                    if (next == removeValueFromChain) {
                        ++this.modCount;
                        removeValueFromChain = this.removeValueFromChain(a, next, next.getKey(), count, next.getValueReference().get(), next.getValueReference(), RemovalCause.COLLECTED);
                        count = this.count;
                        table.set(n, removeValueFromChain);
                        this.count = count - 1;
                        return true;
                    }
                }
                return false;
            }
            finally {
                this.unlock();
                this.postWriteCleanup();
            }
        }
        
        public boolean reclaimValue(final K k, int count, final s s) {
            this.lock();
            try {
                final AtomicReferenceArray<a> table = this.table;
                final int n = table.length() - 1 & count;
                a next;
                final a a = next = table.get(n);
                while (next != null) {
                    final Object key = next.getKey();
                    if (next.getHash() == count && key != null && this.map.e.equivalent(k, key)) {
                        if (next.getValueReference() == s) {
                            ++this.modCount;
                            final a removeValueFromChain = this.removeValueFromChain(a, next, (K)key, count, s.get(), s, RemovalCause.COLLECTED);
                            count = this.count;
                            table.set(n, removeValueFromChain);
                            this.count = count - 1;
                            return true;
                        }
                        return false;
                    }
                    else {
                        next = next.getNext();
                    }
                }
                return false;
            }
            finally {
                this.unlock();
                if (!this.isHeldByCurrentThread()) {
                    this.postWriteCleanup();
                }
            }
        }
        
        public void recordLockedRead(final a a, final long accessTime) {
            if (this.map.D()) {
                a.setAccessTime(accessTime);
            }
            this.accessQueue.add(a);
        }
        
        public void recordRead(final a a, final long accessTime) {
            if (this.map.D()) {
                a.setAccessTime(accessTime);
            }
            this.recencyQueue.add(a);
        }
        
        public void recordWrite(final a a, final int n, final long n2) {
            this.drainRecencyQueue();
            this.totalWeight += n;
            if (this.map.D()) {
                a.setAccessTime(n2);
            }
            if (this.map.F()) {
                a.setWriteTime(n2);
            }
            this.accessQueue.add(a);
            this.writeQueue.add(a);
        }
        
        public V refresh(final K k, final int n, final CacheLoader cacheLoader, final boolean b) {
            final k insertLoadingValueReference = this.insertLoadingValueReference(k, n, b);
            if (insertLoadingValueReference == null) {
                return null;
            }
            final ik0 loadAsync = this.loadAsync(k, n, insertLoadingValueReference, cacheLoader);
            Label_0043: {
                if (!loadAsync.isDone()) {
                    break Label_0043;
                }
                try {
                    return (V)t02.a(loadAsync);
                    return null;
                }
                finally {
                    return null;
                }
            }
        }
        
        public V remove(final Object o, int count) {
            this.lock();
            try {
                this.preWriteCleanup(this.map.q.a());
                final AtomicReferenceArray<a> table = this.table;
                final int n = table.length() - 1 & count;
                a next;
                for (a a = next = table.get(n); next != null; next = next.getNext()) {
                    final Object key = next.getKey();
                    if (next.getHash() == count && key != null && this.map.e.equivalent(o, key)) {
                        final s valueReference = next.getValueReference();
                        final Object value = valueReference.get();
                        RemovalCause removalCause;
                        if (value != null) {
                            removalCause = RemovalCause.EXPLICIT;
                        }
                        else {
                            if (!valueReference.a()) {
                                break;
                            }
                            removalCause = RemovalCause.COLLECTED;
                        }
                        ++this.modCount;
                        final a removeValueFromChain = this.removeValueFromChain(a, next, (K)key, count, (V)value, valueReference, removalCause);
                        count = this.count;
                        table.set(n, removeValueFromChain);
                        this.count = count - 1;
                        return (V)value;
                    }
                }
                return null;
            }
            finally {
                this.unlock();
                this.postWriteCleanup();
            }
        }
        
        public boolean remove(final Object o, int count, final Object o2) {
            this.lock();
            try {
                this.preWriteCleanup(this.map.q.a());
                final AtomicReferenceArray<a> table = this.table;
                final int length = table.length();
                boolean b = true;
                final int n = length - 1 & count;
                a next;
                for (a a = next = table.get(n); next != null; next = next.getNext()) {
                    final Object key = next.getKey();
                    if (next.getHash() == count && key != null && this.map.e.equivalent(o, key)) {
                        final s valueReference = next.getValueReference();
                        final Object value = valueReference.get();
                        RemovalCause removalCause;
                        if (this.map.f.equivalent(o2, value)) {
                            removalCause = RemovalCause.EXPLICIT;
                        }
                        else {
                            if (value != null || !valueReference.a()) {
                                break;
                            }
                            removalCause = RemovalCause.COLLECTED;
                        }
                        ++this.modCount;
                        final a removeValueFromChain = this.removeValueFromChain(a, next, (K)key, count, (V)value, valueReference, removalCause);
                        count = this.count;
                        table.set(n, removeValueFromChain);
                        this.count = count - 1;
                        if (removalCause != RemovalCause.EXPLICIT) {
                            b = false;
                        }
                        return b;
                    }
                }
                return false;
            }
            finally {
                this.unlock();
                this.postWriteCleanup();
            }
        }
        
        public void removeCollectedEntry(final a a) {
            this.enqueueNotification(a.getKey(), a.getHash(), a.getValueReference().get(), a.getValueReference().d(), RemovalCause.COLLECTED);
            this.writeQueue.remove(a);
            this.accessQueue.remove(a);
        }
        
        public boolean removeEntry(a removeValueFromChain, int count, final RemovalCause removalCause) {
            final AtomicReferenceArray<a> table = this.table;
            final int n = table.length() - 1 & count;
            a next;
            for (a a = next = table.get(n); next != null; next = next.getNext()) {
                if (next == removeValueFromChain) {
                    ++this.modCount;
                    removeValueFromChain = this.removeValueFromChain(a, next, next.getKey(), count, next.getValueReference().get(), next.getValueReference(), removalCause);
                    count = this.count;
                    table.set(n, removeValueFromChain);
                    this.count = count - 1;
                    return true;
                }
            }
            return false;
        }
        
        public a removeEntryFromChain(a next, final a a) {
            int count = this.count;
            a next2 = a.getNext();
            while (next != a) {
                final a copyEntry = this.copyEntry(next, next2);
                if (copyEntry != null) {
                    next2 = copyEntry;
                }
                else {
                    this.removeCollectedEntry(next);
                    --count;
                }
                next = next.getNext();
            }
            this.count = count;
            return next2;
        }
        
        public boolean removeLoadingValue(final K k, final int n, final k i) {
            this.lock();
            try {
                final AtomicReferenceArray<a> table = this.table;
                final int n2 = table.length() - 1 & n;
                a next;
                final a a = next = table.get(n2);
                while (next != null) {
                    final Object key = next.getKey();
                    if (next.getHash() == n && key != null && this.map.e.equivalent(k, key)) {
                        if (next.getValueReference() == i) {
                            if (i.a()) {
                                next.setValueReference(i.j());
                            }
                            else {
                                table.set(n2, this.removeEntryFromChain(a, next));
                            }
                            return true;
                        }
                        break;
                    }
                    else {
                        next = next.getNext();
                    }
                }
                return false;
            }
            finally {
                this.unlock();
                this.postWriteCleanup();
            }
        }
        
        public a removeValueFromChain(final a a, final a a2, final K k, final int n, final V v, final s s, final RemovalCause removalCause) {
            this.enqueueNotification(k, n, v, s.d(), removalCause);
            this.writeQueue.remove(a2);
            this.accessQueue.remove(a2);
            if (s.f()) {
                s.c(null);
                return a;
            }
            return this.removeEntryFromChain(a, a2);
        }
        
        public V replace(final K k, int count, final V v) {
            this.lock();
            try {
                final long a = this.map.q.a();
                this.preWriteCleanup(a);
                final AtomicReferenceArray<a> table = this.table;
                final int n = count & table.length() - 1;
                a next;
                final a a2 = next = table.get(n);
                while (next != null) {
                    final Object key = next.getKey();
                    if (next.getHash() == count && key != null && this.map.e.equivalent(k, key)) {
                        final s valueReference = next.getValueReference();
                        final Object value = valueReference.get();
                        if (value != null) {
                            ++this.modCount;
                            this.enqueueNotification(k, count, (V)value, valueReference.d(), RemovalCause.REPLACED);
                            this.setValue(next, k, v, a);
                            this.evictEntries(next);
                            return (V)value;
                        }
                        if (valueReference.a()) {
                            ++this.modCount;
                            final a removeValueFromChain = this.removeValueFromChain(a2, next, (K)key, count, (V)value, valueReference, RemovalCause.COLLECTED);
                            count = this.count;
                            table.set(n, removeValueFromChain);
                            this.count = count - 1;
                            break;
                        }
                        break;
                    }
                    else {
                        next = next.getNext();
                    }
                }
                return null;
            }
            finally {
                this.unlock();
                this.postWriteCleanup();
            }
        }
        
        public boolean replace(final K k, int count, final V v, final V v2) {
            this.lock();
            try {
                final long a = this.map.q.a();
                this.preWriteCleanup(a);
                final AtomicReferenceArray<a> table = this.table;
                final int n = count & table.length() - 1;
                a next;
                final a a2 = next = table.get(n);
                while (next != null) {
                    final Object key = next.getKey();
                    if (next.getHash() == count && key != null && this.map.e.equivalent(k, key)) {
                        final s valueReference = next.getValueReference();
                        final Object value = valueReference.get();
                        if (value == null) {
                            if (valueReference.a()) {
                                ++this.modCount;
                                final a removeValueFromChain = this.removeValueFromChain(a2, next, (K)key, count, (V)value, valueReference, RemovalCause.COLLECTED);
                                count = this.count;
                                table.set(n, removeValueFromChain);
                                this.count = count - 1;
                                break;
                            }
                            break;
                        }
                        else {
                            if (this.map.f.equivalent(v, value)) {
                                ++this.modCount;
                                this.enqueueNotification(k, count, (V)value, valueReference.d(), RemovalCause.REPLACED);
                                this.setValue(next, k, v2, a);
                                this.evictEntries(next);
                                return true;
                            }
                            this.recordLockedRead(next, a);
                            break;
                        }
                    }
                    else {
                        next = next.getNext();
                    }
                }
                return false;
            }
            finally {
                this.unlock();
                this.postWriteCleanup();
            }
        }
        
        public void runLockedCleanup(final long n) {
            if (this.tryLock()) {
                try {
                    this.drainReferenceQueues();
                    this.expireEntries(n);
                    this.readCount.set(0);
                }
                finally {
                    this.unlock();
                }
            }
        }
        
        public void runUnlockedCleanup() {
            if (!this.isHeldByCurrentThread()) {
                this.map.A();
            }
        }
        
        public V scheduleRefresh(final a a, final K k, final int n, final V v, final long n2, final CacheLoader cacheLoader) {
            if (this.map.H() && n2 - a.getWriteTime() > this.map.m && !a.getValueReference().f()) {
                final V refresh = this.refresh(k, n, cacheLoader, true);
                if (refresh != null) {
                    return refresh;
                }
            }
            return v;
        }
        
        public void setValue(final a a, final K k, final V v, final long n) {
            final s valueReference = a.getValueReference();
            final int weigh = this.map.j.weigh(k, v);
            i71.y(weigh >= 0, "Weights must be non-negative");
            a.setValueReference(this.map.h.referenceValue((Segment<Object, V>)this, a, v, weigh));
            this.recordWrite(a, weigh, n);
            valueReference.c(v);
        }
        
        public boolean storeLoadedValue(final K k, final int n, final k i, final V v) {
            this.lock();
            try {
                final long a = this.map.q.a();
                this.preWriteCleanup(a);
                int count;
                if ((count = this.count + 1) > this.threshold) {
                    this.expand();
                    count = this.count + 1;
                }
                final AtomicReferenceArray<a> table = this.table;
                final int n2 = n & table.length() - 1;
                a newValue;
                final a a2 = newValue = table.get(n2);
                while (true) {
                    while (newValue != null) {
                        final Object key = newValue.getKey();
                        if (newValue.getHash() == n && key != null && this.map.e.equivalent(k, key)) {
                            final s valueReference = newValue.getValueReference();
                            final Object value = valueReference.get();
                            if (i != valueReference && (value != null || valueReference == LocalCache.C)) {
                                this.enqueueNotification(k, n, v, 0, RemovalCause.REPLACED);
                                return false;
                            }
                            ++this.modCount;
                            int count2 = count;
                            if (i.a()) {
                                RemovalCause removalCause;
                                if (value == null) {
                                    removalCause = RemovalCause.COLLECTED;
                                }
                                else {
                                    removalCause = RemovalCause.REPLACED;
                                }
                                this.enqueueNotification(k, n, (V)value, i.d(), removalCause);
                                count2 = count - 1;
                            }
                            this.setValue(newValue, k, v, a);
                            this.count = count2;
                            this.evictEntries(newValue);
                            return true;
                        }
                        else {
                            newValue = newValue.getNext();
                        }
                    }
                    ++this.modCount;
                    newValue = this.newEntry(k, n, a2);
                    this.setValue(newValue, k, v, a);
                    table.set(n2, newValue);
                    this.count = count;
                    continue;
                }
            }
            finally {
                this.unlock();
                this.postWriteCleanup();
            }
        }
        
        public void tryDrainReferenceQueues() {
            if (this.tryLock()) {
                try {
                    this.drainReferenceQueues();
                }
                finally {
                    this.unlock();
                }
            }
        }
        
        public void tryExpireEntries(final long n) {
            if (this.tryLock()) {
                try {
                    this.expireEntries(n);
                }
                finally {
                    this.unlock();
                }
            }
        }
        
        public V waitForLoadingValue(final a a, final K obj, final s s) {
            if (s.f()) {
                i71.B(Thread.holdsLock(a) ^ true, "Recursive load of: %s", obj);
                try {
                    final Object e = s.e();
                    if (e != null) {
                        this.recordRead(a, this.map.q.a());
                        return (V)e;
                    }
                    final String value = String.valueOf(obj);
                    final StringBuilder sb = new StringBuilder(value.length() + 35);
                    sb.append("CacheLoader returned null for key ");
                    sb.append(value);
                    sb.append(".");
                    throw new CacheLoader.InvalidCacheLoadException(sb.toString());
                }
                finally {
                    this.statsCounter.d(1);
                }
            }
            throw new AssertionError();
        }
    }
    
    public enum Strength
    {
        private static final Strength[] $VALUES;
        
        SOFT {
            @Override
            public Equivalence defaultEquivalence() {
                return Equivalence.identity();
            }
            
            @Override
            public <K, V> s referenceValue(final Segment<K, V> segment, final a a, final V v, final int n) {
                l l;
                if (n == 1) {
                    l = new l(segment.valueReferenceQueue, v, a);
                }
                else {
                    l = new z(segment.valueReferenceQueue, v, a, n);
                }
                return l;
            }
        }, 
        STRONG {
            @Override
            public Equivalence defaultEquivalence() {
                return Equivalence.equals();
            }
            
            @Override
            public <K, V> s referenceValue(final Segment<K, V> segment, final a a, final V v, final int n) {
                p p4;
                if (n == 1) {
                    p4 = new p(v);
                }
                else {
                    p4 = new a0(v, n);
                }
                return p4;
            }
        }, 
        WEAK {
            @Override
            public Equivalence defaultEquivalence() {
                return Equivalence.identity();
            }
            
            @Override
            public <K, V> s referenceValue(final Segment<K, V> segment, final a a, final V v, final int n) {
                x x;
                if (n == 1) {
                    x = new x(segment.valueReferenceQueue, v, a);
                }
                else {
                    x = new b0(segment.valueReferenceQueue, v, a, n);
                }
                return x;
            }
        };
        
        private static /* synthetic */ Strength[] $values() {
            return new Strength[] { Strength.STRONG, Strength.SOFT, Strength.WEAK };
        }
        
        static {
            $VALUES = $values();
        }
        
        public abstract Equivalence defaultEquivalence();
        
        public abstract <K, V> s referenceValue(final Segment<K, V> p0, final a p1, final V p2, final int p3);
    }
    
    public static final class a0 extends p
    {
        public final int b;
        
        public a0(final Object o, final int b) {
            super(o);
            this.b = b;
        }
        
        @Override
        public int d() {
            return this.b;
        }
    }
    
    public static final class b0 extends x
    {
        public final int b;
        
        public b0(final ReferenceQueue referenceQueue, final Object o, final a a, final int b) {
            super(referenceQueue, o, a);
            this.b = b;
        }
        
        @Override
        public int d() {
            return this.b;
        }
        
        @Override
        public s g(final ReferenceQueue referenceQueue, final Object o, final a a) {
            return new b0(referenceQueue, o, a, this.b);
        }
    }
    
    public abstract class c extends AbstractSet
    {
        public final LocalCache a;
        
        public c(final LocalCache a) {
            this.a = a;
        }
        
        @Override
        public void clear() {
            this.a.clear();
        }
        
        @Override
        public boolean isEmpty() {
            return this.a.isEmpty();
        }
        
        @Override
        public int size() {
            return this.a.size();
        }
        
        @Override
        public Object[] toArray() {
            return K(this).toArray();
        }
        
        @Override
        public Object[] toArray(final Object[] a) {
            return K(this).toArray(a);
        }
    }
    
    public static final class c0 extends AbstractQueue
    {
        public final a a;
        
        public c0() {
            this.a = new d(this) {
                public a a = this;
                public a b = this;
                
                @Override
                public a getNextInWriteQueue() {
                    return this.a;
                }
                
                @Override
                public a getPreviousInWriteQueue() {
                    return this.b;
                }
                
                @Override
                public long getWriteTime() {
                    return Long.MAX_VALUE;
                }
                
                @Override
                public void setNextInWriteQueue(final a a) {
                    this.a = a;
                }
                
                @Override
                public void setPreviousInWriteQueue(final a b) {
                    this.b = b;
                }
                
                @Override
                public void setWriteTime(final long n) {
                }
            };
        }
        
        public boolean a(final a a) {
            LocalCache.d(a.getPreviousInWriteQueue(), a.getNextInWriteQueue());
            LocalCache.d(this.a.getPreviousInWriteQueue(), a);
            LocalCache.d(a, this.a);
            return true;
        }
        
        public a b() {
            a nextInWriteQueue;
            if ((nextInWriteQueue = this.a.getNextInWriteQueue()) == this.a) {
                nextInWriteQueue = null;
            }
            return nextInWriteQueue;
        }
        
        public a c() {
            final a nextInWriteQueue = this.a.getNextInWriteQueue();
            if (nextInWriteQueue == this.a) {
                return null;
            }
            this.remove(nextInWriteQueue);
            return nextInWriteQueue;
        }
        
        @Override
        public void clear() {
            a nextInWriteQueue = this.a.getNextInWriteQueue();
            a a;
            while (true) {
                a = this.a;
                if (nextInWriteQueue == a) {
                    break;
                }
                final a nextInWriteQueue2 = nextInWriteQueue.getNextInWriteQueue();
                LocalCache.z(nextInWriteQueue);
                nextInWriteQueue = nextInWriteQueue2;
            }
            a.setNextInWriteQueue(a);
            final a a2 = this.a;
            a2.setPreviousInWriteQueue(a2);
        }
        
        @Override
        public boolean contains(final Object o) {
            return ((a)o).getNextInWriteQueue() != NullEntry.INSTANCE;
        }
        
        @Override
        public boolean isEmpty() {
            return this.a.getNextInWriteQueue() == this.a;
        }
        
        @Override
        public Iterator iterator() {
            return new a0(this, this.b()) {
                public final c0 b;
                
                public a c(a nextInWriteQueue) {
                    if ((nextInWriteQueue = nextInWriteQueue.getNextInWriteQueue()) == this.b.a) {
                        nextInWriteQueue = null;
                    }
                    return nextInWriteQueue;
                }
            };
        }
        
        @Override
        public boolean remove(final Object o) {
            final a a = (a)o;
            final a previousInWriteQueue = a.getPreviousInWriteQueue();
            final a nextInWriteQueue = a.getNextInWriteQueue();
            LocalCache.d(previousInWriteQueue, nextInWriteQueue);
            LocalCache.z(a);
            return nextInWriteQueue != NullEntry.INSTANCE;
        }
        
        @Override
        public int size() {
            a a = this.a.getNextInWriteQueue();
            int n = 0;
            while (a != this.a) {
                ++n;
                a = a.getNextInWriteQueue();
            }
            return n;
        }
    }
    
    public abstract static class d implements a
    {
        @Override
        public long getAccessTime() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public int getHash() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public Object getKey() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public a getNext() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public a getNextInAccessQueue() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public a getNextInWriteQueue() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public a getPreviousInAccessQueue() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public a getPreviousInWriteQueue() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public s getValueReference() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public long getWriteTime() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void setAccessTime(final long n) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void setNextInAccessQueue(final a a) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void setNextInWriteQueue(final a a) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void setPreviousInAccessQueue(final a a) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void setPreviousInWriteQueue(final a a) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void setValueReference(final s s) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void setWriteTime(final long n) {
            throw new UnsupportedOperationException();
        }
    }
    
    public final class d0 implements Entry
    {
        public final Object a;
        public Object b;
        public final LocalCache c;
        
        public d0(final LocalCache c, final Object a, final Object b) {
            this.c = c;
            this.a = a;
            this.b = b;
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof Entry;
            boolean b3;
            final boolean b2 = b3 = false;
            if (b) {
                final Entry entry = (Entry)o;
                b3 = b2;
                if (this.a.equals(entry.getKey())) {
                    b3 = b2;
                    if (this.b.equals(entry.getValue())) {
                        b3 = true;
                    }
                }
            }
            return b3;
        }
        
        @Override
        public Object getKey() {
            return this.a;
        }
        
        @Override
        public Object getValue() {
            return this.b;
        }
        
        @Override
        public int hashCode() {
            return this.a.hashCode() ^ this.b.hashCode();
        }
        
        @Override
        public Object setValue(final Object b) {
            final Object put = this.c.put(this.a, b);
            this.b = b;
            return put;
        }
        
        @Override
        public String toString() {
            final String value = String.valueOf(this.getKey());
            final String value2 = String.valueOf(this.getValue());
            final StringBuilder sb = new StringBuilder(value.length() + 1 + value2.length());
            sb.append(value);
            sb.append("=");
            sb.append(value2);
            return sb.toString();
        }
    }
    
    public static final class e extends AbstractQueue
    {
        public final a a;
        
        public e() {
            this.a = new d(this) {
                public a a = this;
                public a b = this;
                
                @Override
                public long getAccessTime() {
                    return Long.MAX_VALUE;
                }
                
                @Override
                public a getNextInAccessQueue() {
                    return this.a;
                }
                
                @Override
                public a getPreviousInAccessQueue() {
                    return this.b;
                }
                
                @Override
                public void setAccessTime(final long n) {
                }
                
                @Override
                public void setNextInAccessQueue(final a a) {
                    this.a = a;
                }
                
                @Override
                public void setPreviousInAccessQueue(final a b) {
                    this.b = b;
                }
            };
        }
        
        public boolean a(final a a) {
            LocalCache.c(a.getPreviousInAccessQueue(), a.getNextInAccessQueue());
            LocalCache.c(this.a.getPreviousInAccessQueue(), a);
            LocalCache.c(a, this.a);
            return true;
        }
        
        public a b() {
            a nextInAccessQueue;
            if ((nextInAccessQueue = this.a.getNextInAccessQueue()) == this.a) {
                nextInAccessQueue = null;
            }
            return nextInAccessQueue;
        }
        
        public a c() {
            final a nextInAccessQueue = this.a.getNextInAccessQueue();
            if (nextInAccessQueue == this.a) {
                return null;
            }
            this.remove(nextInAccessQueue);
            return nextInAccessQueue;
        }
        
        @Override
        public void clear() {
            a nextInAccessQueue = this.a.getNextInAccessQueue();
            a a;
            while (true) {
                a = this.a;
                if (nextInAccessQueue == a) {
                    break;
                }
                final a nextInAccessQueue2 = nextInAccessQueue.getNextInAccessQueue();
                LocalCache.y(nextInAccessQueue);
                nextInAccessQueue = nextInAccessQueue2;
            }
            a.setNextInAccessQueue(a);
            final a a2 = this.a;
            a2.setPreviousInAccessQueue(a2);
        }
        
        @Override
        public boolean contains(final Object o) {
            return ((a)o).getNextInAccessQueue() != NullEntry.INSTANCE;
        }
        
        @Override
        public boolean isEmpty() {
            return this.a.getNextInAccessQueue() == this.a;
        }
        
        @Override
        public Iterator iterator() {
            return new a0(this, this.b()) {
                public final e b;
                
                public a c(a nextInAccessQueue) {
                    if ((nextInAccessQueue = nextInAccessQueue.getNextInAccessQueue()) == this.b.a) {
                        nextInAccessQueue = null;
                    }
                    return nextInAccessQueue;
                }
            };
        }
        
        @Override
        public boolean remove(final Object o) {
            final a a = (a)o;
            final a previousInAccessQueue = a.getPreviousInAccessQueue();
            final a nextInAccessQueue = a.getNextInAccessQueue();
            LocalCache.c(previousInAccessQueue, nextInAccessQueue);
            LocalCache.y(a);
            return nextInAccessQueue != NullEntry.INSTANCE;
        }
        
        @Override
        public int size() {
            a a = this.a.getNextInAccessQueue();
            int n = 0;
            while (a != this.a) {
                ++n;
                a = a.getNextInAccessQueue();
            }
            return n;
        }
    }
    
    public final class f extends h
    {
        public f(final LocalCache localCache) {
            localCache.super();
        }
        
        public Entry g() {
            return ((h)this).d();
        }
    }
    
    public final class g extends c
    {
        public final LocalCache b;
        
        public g(final LocalCache b) {
            this.b = b.super();
        }
        
        @Override
        public boolean contains(final Object o) {
            final boolean b = o instanceof Entry;
            final boolean b2 = false;
            if (!b) {
                return false;
            }
            final Entry entry = (Entry)o;
            final Object key = entry.getKey();
            if (key == null) {
                return false;
            }
            final Object value = this.b.get(key);
            boolean b3 = b2;
            if (value != null) {
                b3 = b2;
                if (this.b.f.equivalent(entry.getValue(), value)) {
                    b3 = true;
                }
            }
            return b3;
        }
        
        @Override
        public Iterator iterator() {
            return this.b.new f();
        }
        
        @Override
        public boolean remove(Object key) {
            final boolean b = key instanceof Entry;
            final boolean b2 = false;
            if (!b) {
                return false;
            }
            final Entry entry = (Entry)key;
            key = entry.getKey();
            boolean b3 = b2;
            if (key != null) {
                b3 = b2;
                if (this.b.remove(key, entry.getValue())) {
                    b3 = true;
                }
            }
            return b3;
        }
    }
    
    public abstract class h implements Iterator
    {
        public int a;
        public int b;
        public Segment c;
        public AtomicReferenceArray d;
        public a e;
        public d0 f;
        public d0 g;
        public final LocalCache h;
        
        public h(final LocalCache h) {
            this.h = h;
            this.a = h.c.length - 1;
            this.b = -1;
            this.b();
        }
        
        public final void b() {
            this.f = null;
            if (this.e()) {
                return;
            }
            if (this.f()) {
                return;
            }
            while (true) {
                final int a = this.a;
                if (a < 0) {
                    break;
                }
                final Segment[] c = this.h.c;
                this.a = a - 1;
                final Segment c2 = c[a];
                this.c = c2;
                if (c2.count == 0) {
                    continue;
                }
                final AtomicReferenceArray<a> table = this.c.table;
                this.d = table;
                this.b = table.length() - 1;
                if (this.f()) {
                    break;
                }
            }
        }
        
        public boolean c(final a a) {
            try {
                final long a2 = this.h.q.a();
                final Object key = a.getKey();
                final Object o = this.h.o(a, a2);
                if (o != null) {
                    this.f = this.h.new d0(key, o);
                    return true;
                }
                return false;
            }
            finally {
                this.c.postReadCleanup();
            }
        }
        
        public d0 d() {
            final d0 f = this.f;
            if (f != null) {
                this.g = f;
                this.b();
                return this.g;
            }
            throw new NoSuchElementException();
        }
        
        public boolean e() {
            a a = this.e;
            if (a != null) {
                while (true) {
                    this.e = a.getNext();
                    final a e = this.e;
                    if (e == null) {
                        break;
                    }
                    if (this.c(e)) {
                        return true;
                    }
                    a = this.e;
                }
            }
            return false;
        }
        
        public boolean f() {
            a e;
            do {
                final int b = this.b;
                if (b < 0) {
                    return false;
                }
                final AtomicReferenceArray d = this.d;
                this.b = b - 1;
                e = (a)d.get(b);
                this.e = e;
            } while (e == null || (!this.c(e) && !this.e()));
            return true;
        }
        
        @Override
        public boolean hasNext() {
            return this.f != null;
        }
        
        @Override
        public void remove() {
            i71.x(this.g != null);
            this.h.remove(this.g.getKey());
            this.g = null;
        }
    }
    
    public final class i extends h
    {
        public i(final LocalCache localCache) {
            localCache.super();
        }
        
        @Override
        public Object next() {
            return ((h)this).d().getKey();
        }
    }
    
    public final class j extends c
    {
        public final LocalCache b;
        
        public j(final LocalCache b) {
            this.b = b.super();
        }
        
        @Override
        public boolean contains(final Object o) {
            return this.b.containsKey(o);
        }
        
        @Override
        public Iterator iterator() {
            return this.b.new i();
        }
        
        @Override
        public boolean remove(final Object o) {
            return this.b.remove(o) != null;
        }
    }
    
    public static class k implements s
    {
        public volatile s a;
        public final com.google.common.util.concurrent.h b;
        public final dq1 c;
        
        public k() {
            this(LocalCache.L());
        }
        
        public k(final s a) {
            this.b = com.google.common.util.concurrent.h.n();
            this.c = dq1.d();
            this.a = a;
        }
        
        @Override
        public boolean a() {
            return this.a.a();
        }
        
        @Override
        public a b() {
            return null;
        }
        
        @Override
        public void c(final Object o) {
            if (o != null) {
                this.l(o);
            }
            else {
                this.a = LocalCache.L();
            }
        }
        
        @Override
        public int d() {
            return this.a.d();
        }
        
        @Override
        public Object e() {
            return t02.a(this.b);
        }
        
        @Override
        public boolean f() {
            return true;
        }
        
        @Override
        public s g(final ReferenceQueue referenceQueue, final Object o, final a a) {
            return this;
        }
        
        @Override
        public Object get() {
            return this.a.get();
        }
        
        public long h() {
            return this.c.e(TimeUnit.NANOSECONDS);
        }
        
        public final ik0 i(final Throwable t) {
            return com.google.common.util.concurrent.e.c(t);
        }
        
        public s j() {
            return this.a;
        }
        
        public ik0 k(Object load, final CacheLoader cacheLoader) {
            try {
                this.c.h();
                final Object value = this.a.get();
                if (value == null) {
                    load = cacheLoader.load(load);
                    ik0 ik0;
                    if (this.l(load)) {
                        ik0 = this.b;
                    }
                    else {
                        ik0 = com.google.common.util.concurrent.e.d(load);
                    }
                    return ik0;
                }
                final ik0 reload = cacheLoader.reload(load, value);
                if (reload == null) {
                    return com.google.common.util.concurrent.e.d(null);
                }
                load = new m90(this) {
                    public final k a;
                    
                    @Override
                    public Object apply(final Object o) {
                        this.a.l(o);
                        return o;
                    }
                };
                return com.google.common.util.concurrent.e.e(reload, (m90)load, com.google.common.util.concurrent.g.a());
            }
            finally {
                final Throwable t;
                ik0 ik2;
                if (this.m(t)) {
                    ik2 = this.b;
                }
                else {
                    ik2 = this.i(t);
                }
                if (t instanceof InterruptedException) {
                    Thread.currentThread().interrupt();
                }
                return ik2;
            }
        }
        
        public boolean l(final Object o) {
            return this.b.set(o);
        }
        
        public boolean m(final Throwable exception) {
            return this.b.setException(exception);
        }
    }
    
    public interface s
    {
        boolean a();
        
        a b();
        
        void c(final Object p0);
        
        int d();
        
        Object e();
        
        boolean f();
        
        s g(final ReferenceQueue p0, final Object p1, final a p2);
        
        Object get();
    }
    
    public static class l extends SoftReference implements s
    {
        public final a a;
        
        public l(final ReferenceQueue q, final Object referent, final a a) {
            super(referent, q);
            this.a = a;
        }
        
        @Override
        public boolean a() {
            return true;
        }
        
        @Override
        public a b() {
            return this.a;
        }
        
        @Override
        public void c(final Object o) {
        }
        
        @Override
        public int d() {
            return 1;
        }
        
        @Override
        public Object e() {
            return this.get();
        }
        
        @Override
        public boolean f() {
            return false;
        }
        
        @Override
        public s g(final ReferenceQueue referenceQueue, final Object o, final a a) {
            return new l(referenceQueue, o, a);
        }
    }
    
    public static final class m extends o
    {
        public volatile long e;
        public a f;
        public a g;
        
        public m(final Object o, final int n, final a a) {
            super(o, n, a);
            this.e = Long.MAX_VALUE;
            this.f = LocalCache.x();
            this.g = LocalCache.x();
        }
        
        @Override
        public long getAccessTime() {
            return this.e;
        }
        
        @Override
        public a getNextInAccessQueue() {
            return this.f;
        }
        
        @Override
        public a getPreviousInAccessQueue() {
            return this.g;
        }
        
        @Override
        public void setAccessTime(final long e) {
            this.e = e;
        }
        
        @Override
        public void setNextInAccessQueue(final a f) {
            this.f = f;
        }
        
        @Override
        public void setPreviousInAccessQueue(final a g) {
            this.g = g;
        }
    }
    
    public static final class n extends o
    {
        public volatile long e;
        public a f;
        public a g;
        public volatile long h;
        public a i;
        public a j;
        
        public n(final Object o, final int n, final a a) {
            super(o, n, a);
            this.e = Long.MAX_VALUE;
            this.f = LocalCache.x();
            this.g = LocalCache.x();
            this.h = Long.MAX_VALUE;
            this.i = LocalCache.x();
            this.j = LocalCache.x();
        }
        
        @Override
        public long getAccessTime() {
            return this.e;
        }
        
        @Override
        public a getNextInAccessQueue() {
            return this.f;
        }
        
        @Override
        public a getNextInWriteQueue() {
            return this.i;
        }
        
        @Override
        public a getPreviousInAccessQueue() {
            return this.g;
        }
        
        @Override
        public a getPreviousInWriteQueue() {
            return this.j;
        }
        
        @Override
        public long getWriteTime() {
            return this.h;
        }
        
        @Override
        public void setAccessTime(final long e) {
            this.e = e;
        }
        
        @Override
        public void setNextInAccessQueue(final a f) {
            this.f = f;
        }
        
        @Override
        public void setNextInWriteQueue(final a i) {
            this.i = i;
        }
        
        @Override
        public void setPreviousInAccessQueue(final a g) {
            this.g = g;
        }
        
        @Override
        public void setPreviousInWriteQueue(final a j) {
            this.j = j;
        }
        
        @Override
        public void setWriteTime(final long h) {
            this.h = h;
        }
    }
    
    public static class o extends d
    {
        public final Object a;
        public final int b;
        public final a c;
        public volatile s d;
        
        public o(final Object a, final int b, final a c) {
            this.d = LocalCache.L();
            this.a = a;
            this.b = b;
            this.c = c;
        }
        
        @Override
        public int getHash() {
            return this.b;
        }
        
        @Override
        public Object getKey() {
            return this.a;
        }
        
        @Override
        public a getNext() {
            return this.c;
        }
        
        @Override
        public s getValueReference() {
            return this.d;
        }
        
        @Override
        public void setValueReference(final s d) {
            this.d = d;
        }
    }
    
    public static class p implements s
    {
        public final Object a;
        
        public p(final Object a) {
            this.a = a;
        }
        
        @Override
        public boolean a() {
            return true;
        }
        
        @Override
        public a b() {
            return null;
        }
        
        @Override
        public void c(final Object o) {
        }
        
        @Override
        public int d() {
            return 1;
        }
        
        @Override
        public Object e() {
            return this.get();
        }
        
        @Override
        public boolean f() {
            return false;
        }
        
        @Override
        public s g(final ReferenceQueue referenceQueue, final Object o, final a a) {
            return this;
        }
        
        @Override
        public Object get() {
            return this.a;
        }
    }
    
    public static final class q extends o
    {
        public volatile long e;
        public a f;
        public a g;
        
        public q(final Object o, final int n, final a a) {
            super(o, n, a);
            this.e = Long.MAX_VALUE;
            this.f = LocalCache.x();
            this.g = LocalCache.x();
        }
        
        @Override
        public a getNextInWriteQueue() {
            return this.f;
        }
        
        @Override
        public a getPreviousInWriteQueue() {
            return this.g;
        }
        
        @Override
        public long getWriteTime() {
            return this.e;
        }
        
        @Override
        public void setNextInWriteQueue(final a f) {
            this.f = f;
        }
        
        @Override
        public void setPreviousInWriteQueue(final a g) {
            this.g = g;
        }
        
        @Override
        public void setWriteTime(final long e) {
            this.e = e;
        }
    }
    
    public final class r extends h
    {
        public r(final LocalCache localCache) {
            localCache.super();
        }
        
        @Override
        public Object next() {
            return ((h)this).d().getValue();
        }
    }
    
    public final class t extends AbstractCollection
    {
        public final LocalCache a;
        
        public t(final LocalCache a) {
            this.a = a;
        }
        
        @Override
        public void clear() {
            this.a.clear();
        }
        
        @Override
        public boolean contains(final Object o) {
            return this.a.containsValue(o);
        }
        
        @Override
        public boolean isEmpty() {
            return this.a.isEmpty();
        }
        
        @Override
        public Iterator iterator() {
            return this.a.new r();
        }
        
        @Override
        public int size() {
            return this.a.size();
        }
        
        @Override
        public Object[] toArray() {
            return K(this).toArray();
        }
        
        @Override
        public Object[] toArray(final Object[] a) {
            return K(this).toArray(a);
        }
    }
    
    public static final class u extends w
    {
        public volatile long d;
        public a e;
        public a f;
        
        public u(final ReferenceQueue referenceQueue, final Object o, final int n, final a a) {
            super(referenceQueue, o, n, a);
            this.d = Long.MAX_VALUE;
            this.e = LocalCache.x();
            this.f = LocalCache.x();
        }
        
        @Override
        public long getAccessTime() {
            return this.d;
        }
        
        @Override
        public a getNextInAccessQueue() {
            return this.e;
        }
        
        @Override
        public a getPreviousInAccessQueue() {
            return this.f;
        }
        
        @Override
        public void setAccessTime(final long d) {
            this.d = d;
        }
        
        @Override
        public void setNextInAccessQueue(final a e) {
            this.e = e;
        }
        
        @Override
        public void setPreviousInAccessQueue(final a f) {
            this.f = f;
        }
    }
    
    public static final class v extends w
    {
        public volatile long d;
        public a e;
        public a f;
        public volatile long g;
        public a h;
        public a i;
        
        public v(final ReferenceQueue referenceQueue, final Object o, final int n, final a a) {
            super(referenceQueue, o, n, a);
            this.d = Long.MAX_VALUE;
            this.e = LocalCache.x();
            this.f = LocalCache.x();
            this.g = Long.MAX_VALUE;
            this.h = LocalCache.x();
            this.i = LocalCache.x();
        }
        
        @Override
        public long getAccessTime() {
            return this.d;
        }
        
        @Override
        public a getNextInAccessQueue() {
            return this.e;
        }
        
        @Override
        public a getNextInWriteQueue() {
            return this.h;
        }
        
        @Override
        public a getPreviousInAccessQueue() {
            return this.f;
        }
        
        @Override
        public a getPreviousInWriteQueue() {
            return this.i;
        }
        
        @Override
        public long getWriteTime() {
            return this.g;
        }
        
        @Override
        public void setAccessTime(final long d) {
            this.d = d;
        }
        
        @Override
        public void setNextInAccessQueue(final a e) {
            this.e = e;
        }
        
        @Override
        public void setNextInWriteQueue(final a h) {
            this.h = h;
        }
        
        @Override
        public void setPreviousInAccessQueue(final a f) {
            this.f = f;
        }
        
        @Override
        public void setPreviousInWriteQueue(final a i) {
            this.i = i;
        }
        
        @Override
        public void setWriteTime(final long g) {
            this.g = g;
        }
    }
    
    public static class w extends WeakReference implements a
    {
        public final int a;
        public final a b;
        public volatile s c;
        
        public w(final ReferenceQueue q, final Object referent, final int a, final a b) {
            super(referent, q);
            this.c = LocalCache.L();
            this.a = a;
            this.b = b;
        }
        
        @Override
        public long getAccessTime() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public int getHash() {
            return this.a;
        }
        
        @Override
        public Object getKey() {
            return this.get();
        }
        
        @Override
        public a getNext() {
            return this.b;
        }
        
        @Override
        public a getNextInAccessQueue() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public a getNextInWriteQueue() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public a getPreviousInAccessQueue() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public a getPreviousInWriteQueue() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public s getValueReference() {
            return this.c;
        }
        
        @Override
        public long getWriteTime() {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void setAccessTime(final long n) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void setNextInAccessQueue(final a a) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void setNextInWriteQueue(final a a) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void setPreviousInAccessQueue(final a a) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void setPreviousInWriteQueue(final a a) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void setValueReference(final s c) {
            this.c = c;
        }
        
        @Override
        public void setWriteTime(final long n) {
            throw new UnsupportedOperationException();
        }
    }
    
    public static class x extends WeakReference implements s
    {
        public final a a;
        
        public x(final ReferenceQueue q, final Object referent, final a a) {
            super(referent, q);
            this.a = a;
        }
        
        @Override
        public boolean a() {
            return true;
        }
        
        @Override
        public a b() {
            return this.a;
        }
        
        @Override
        public void c(final Object o) {
        }
        
        @Override
        public int d() {
            return 1;
        }
        
        @Override
        public Object e() {
            return this.get();
        }
        
        @Override
        public boolean f() {
            return false;
        }
        
        @Override
        public s g(final ReferenceQueue referenceQueue, final Object o, final a a) {
            return new x(referenceQueue, o, a);
        }
    }
    
    public static final class y extends w
    {
        public volatile long d;
        public a e;
        public a f;
        
        public y(final ReferenceQueue referenceQueue, final Object o, final int n, final a a) {
            super(referenceQueue, o, n, a);
            this.d = Long.MAX_VALUE;
            this.e = LocalCache.x();
            this.f = LocalCache.x();
        }
        
        @Override
        public a getNextInWriteQueue() {
            return this.e;
        }
        
        @Override
        public a getPreviousInWriteQueue() {
            return this.f;
        }
        
        @Override
        public long getWriteTime() {
            return this.d;
        }
        
        @Override
        public void setNextInWriteQueue(final a e) {
            this.e = e;
        }
        
        @Override
        public void setPreviousInWriteQueue(final a f) {
            this.f = f;
        }
        
        @Override
        public void setWriteTime(final long d) {
            this.d = d;
        }
    }
    
    public static final class z extends l
    {
        public final int b;
        
        public z(final ReferenceQueue referenceQueue, final Object o, final a a, final int b) {
            super(referenceQueue, o, a);
            this.b = b;
        }
        
        @Override
        public int d() {
            return this.b;
        }
        
        @Override
        public s g(final ReferenceQueue referenceQueue, final Object o, final a a) {
            return new z(referenceQueue, o, a, this.b);
        }
    }
}

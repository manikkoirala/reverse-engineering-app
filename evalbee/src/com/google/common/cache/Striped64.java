// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.cache;

import java.security.PrivilegedActionException;
import java.security.AccessController;
import java.lang.reflect.Field;
import java.security.PrivilegedExceptionAction;
import java.util.Random;
import sun.misc.Unsafe;

abstract class Striped64 extends Number
{
    static final int NCPU;
    private static final Unsafe UNSAFE;
    private static final long baseOffset;
    private static final long busyOffset;
    static final Random rng;
    static final ThreadLocal<int[]> threadHashCode;
    transient volatile long base;
    transient volatile int busy;
    transient volatile b[] cells;
    
    static {
        threadHashCode = new ThreadLocal<int[]>();
        rng = new Random();
        NCPU = Runtime.getRuntime().availableProcessors();
        try {
            final Unsafe unsafe = UNSAFE = getUnsafe();
            baseOffset = unsafe.objectFieldOffset(Striped64.class.getDeclaredField("base"));
            busyOffset = unsafe.objectFieldOffset(Striped64.class.getDeclaredField("busy"));
        }
        catch (final Exception cause) {
            throw new Error(cause);
        }
    }
    
    public Striped64() {
    }
    
    private static Unsafe getUnsafe() {
        try {
            return Unsafe.getUnsafe();
        }
        catch (final SecurityException ex) {
            try {
                return AccessController.doPrivileged((PrivilegedExceptionAction<Unsafe>)new PrivilegedExceptionAction() {
                    public Unsafe a() {
                        for (final Field field : Unsafe.class.getDeclaredFields()) {
                            field.setAccessible(true);
                            final Object value = field.get(null);
                            if (Unsafe.class.isInstance(value)) {
                                return Unsafe.class.cast(value);
                            }
                        }
                        throw new NoSuchFieldError("the Unsafe");
                    }
                });
            }
            catch (final PrivilegedActionException ex2) {
                throw new RuntimeException("Could not initialize intrinsics", ex2.getCause());
            }
        }
    }
    
    final boolean casBase(final long expected, final long x) {
        return Striped64.UNSAFE.compareAndSwapLong(this, Striped64.baseOffset, expected, x);
    }
    
    final boolean casBusy() {
        return Striped64.UNSAFE.compareAndSwapInt(this, Striped64.busyOffset, 0, 1);
    }
    
    abstract long fn(final long p0, final long p1);
    
    final void internalReset(final long n) {
        final b[] cells = this.cells;
        this.base = n;
        if (cells != null) {
            for (final b b : cells) {
                if (b != null) {
                    b.a = n;
                }
            }
        }
    }
    
    final void retryUpdate(final long n, int[] value, boolean b) {
        int nextInt;
        if (value == null) {
            final ThreadLocal<int[]> threadHashCode = Striped64.threadHashCode;
            value = new int[] { 0 };
            threadHashCode.set((int[])value);
            if ((nextInt = Striped64.rng.nextInt()) == 0) {
                nextInt = 1;
            }
            value[0] = nextInt;
        }
        else {
            nextInt = value[0];
        }
        final int n2 = 0;
        int n3 = nextInt;
        int i = n2;
        while (true) {
            final b[] cells = this.cells;
            if (cells != null) {
                final int length = cells.length;
                if (length > 0) {
                    final b b2 = cells[length - 1 & n3];
                    boolean b5 = false;
                    int n5 = 0;
                    Label_0393: {
                        if (b2 == null) {
                            if (this.busy == 0) {
                                final b b3 = new b(n);
                                if (this.busy == 0 && this.casBusy()) {
                                    try {
                                        final b[] cells2 = this.cells;
                                        boolean b4 = false;
                                        Label_0186: {
                                            if (cells2 != null) {
                                                final int length2 = cells2.length;
                                                if (length2 > 0) {
                                                    final int n4 = length2 - 1 & n3;
                                                    if (cells2[n4] == null) {
                                                        cells2[n4] = b3;
                                                        b4 = true;
                                                        break Label_0186;
                                                    }
                                                }
                                            }
                                            b4 = false;
                                        }
                                        this.busy = 0;
                                        if (b4) {
                                            break;
                                        }
                                        continue;
                                    }
                                    finally {
                                        this.busy = 0;
                                    }
                                }
                            }
                        }
                        else {
                            if (!b) {
                                b5 = true;
                                n5 = i;
                                break Label_0393;
                            }
                            final long a = b2.a;
                            if (b2.a(a, this.fn(a, n))) {
                                break;
                            }
                            if (length < Striped64.NCPU) {
                                if (this.cells == cells) {
                                    if (i == 0) {
                                        n5 = 1;
                                        b5 = b;
                                        break Label_0393;
                                    }
                                    b5 = b;
                                    n5 = i;
                                    if (this.busy != 0) {
                                        break Label_0393;
                                    }
                                    b5 = b;
                                    n5 = i;
                                    if (this.casBusy()) {
                                        try {
                                            if (this.cells == cells) {
                                                final b[] cells3 = new b[length << 1];
                                                for (i = 0; i < length; ++i) {
                                                    cells3[i] = cells[i];
                                                }
                                                this.cells = cells3;
                                            }
                                            this.busy = 0;
                                            i = 0;
                                            continue;
                                        }
                                        finally {
                                            this.busy = 0;
                                        }
                                    }
                                    break Label_0393;
                                }
                            }
                        }
                        n5 = 0;
                        b5 = b;
                    }
                    final int n6 = n3 ^ n3 << 13;
                    final int n7 = n6 ^ n6 >>> 17;
                    n3 = (n7 ^ n7 << 5);
                    value[0] = n3;
                    b = b5;
                    i = n5;
                    continue;
                }
            }
            if (this.busy == 0 && this.cells == cells && this.casBusy()) {
                try {
                    boolean b6;
                    if (this.cells == cells) {
                        final b[] cells4 = new b[2];
                        cells4[n3 & 0x1] = new b(n);
                        this.cells = cells4;
                        b6 = true;
                    }
                    else {
                        b6 = false;
                    }
                    this.busy = 0;
                    if (b6) {
                        break;
                    }
                    continue;
                }
                finally {
                    this.busy = 0;
                }
            }
            final long base = this.base;
            if (this.casBase(base, this.fn(base, n))) {
                break;
            }
        }
    }
    
    public static final class b
    {
        public static final Unsafe b;
        public static final long c;
        public volatile long a;
        
        static {
            try {
                c = (b = getUnsafe()).objectFieldOffset(b.class.getDeclaredField("a"));
            }
            catch (final Exception cause) {
                throw new Error(cause);
            }
        }
        
        public b(final long a) {
            this.a = a;
        }
        
        public final boolean a(final long expected, final long x) {
            return Striped64.b.b.compareAndSwapLong(this, Striped64.b.c, expected, x);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.cache;

import java.util.AbstractMap;

public final class RemovalNotification<K, V> extends SimpleImmutableEntry<K, V>
{
    private static final long serialVersionUID = 0L;
    private final RemovalCause cause;
    
    private RemovalNotification(final K key, final V value, final RemovalCause removalCause) {
        super(key, value);
        this.cause = (RemovalCause)i71.r(removalCause);
    }
    
    public static <K, V> RemovalNotification<K, V> create(final K k, final V v, final RemovalCause removalCause) {
        return new RemovalNotification<K, V>(k, v, removalCause);
    }
    
    public RemovalCause getCause() {
        return this.cause;
    }
    
    public boolean wasEvicted() {
        return this.cause.wasEvicted();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.cache;

import java.io.Serializable;
import com.google.common.util.concurrent.e;
import java.util.concurrent.Callable;
import java.util.Map;
import java.util.concurrent.Executor;

public abstract class CacheLoader
{
    public static <K, V> CacheLoader asyncReloading(final CacheLoader cacheLoader, final Executor executor) {
        i71.r(cacheLoader);
        i71.r(executor);
        return new CacheLoader(cacheLoader, executor) {
            public final CacheLoader a;
            public final Executor b;
            
            @Override
            public Object load(final Object o) {
                return this.a.load(o);
            }
            
            @Override
            public Map loadAll(final Iterable iterable) {
                return this.a.loadAll(iterable);
            }
            
            @Override
            public ik0 reload(final Object o, final Object o2) {
                final kk0 a = kk0.a(new Callable(this, o, o2) {
                    public final Object a;
                    public final Object b;
                    public final CacheLoader$a c;
                    
                    @Override
                    public Object call() {
                        return this.c.a.reload(this.a, this.b).get();
                    }
                });
                this.b.execute(a);
                return a;
            }
        };
    }
    
    public static <V> CacheLoader from(final is1 is1) {
        return new SupplierToCacheLoader<Object>(is1);
    }
    
    public static <K, V> CacheLoader from(final m90 m90) {
        return new FunctionToCacheLoader<Object, Object>(m90);
    }
    
    public abstract Object load(final Object p0);
    
    public Map<Object, Object> loadAll(final Iterable<Object> iterable) {
        throw new UnsupportedLoadingOperationException();
    }
    
    public ik0 reload(final Object o, final Object o2) {
        i71.r(o);
        i71.r(o2);
        return e.d(this.load(o));
    }
    
    public static final class FunctionToCacheLoader<K, V> extends CacheLoader implements Serializable
    {
        private static final long serialVersionUID = 0L;
        private final m90 computingFunction;
        
        public FunctionToCacheLoader(final m90 m90) {
            this.computingFunction = (m90)i71.r(m90);
        }
        
        @Override
        public V load(final K k) {
            return (V)this.computingFunction.apply(i71.r(k));
        }
    }
    
    public static final class InvalidCacheLoadException extends RuntimeException
    {
        public InvalidCacheLoadException(final String message) {
            super(message);
        }
    }
    
    public static final class SupplierToCacheLoader<V> extends CacheLoader implements Serializable
    {
        private static final long serialVersionUID = 0L;
        private final is1 computingSupplier;
        
        public SupplierToCacheLoader(final is1 is1) {
            this.computingSupplier = (is1)i71.r(is1);
        }
        
        @Override
        public V load(final Object o) {
            i71.r(o);
            return (V)this.computingSupplier.get();
        }
    }
    
    public static final class UnsupportedLoadingOperationException extends UnsupportedOperationException
    {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.common.cache;

public enum RemovalCause
{
    private static final RemovalCause[] $VALUES;
    
    COLLECTED {
        @Override
        public boolean wasEvicted() {
            return true;
        }
    }, 
    EXPIRED {
        @Override
        public boolean wasEvicted() {
            return true;
        }
    }, 
    EXPLICIT {
        @Override
        public boolean wasEvicted() {
            return false;
        }
    }, 
    REPLACED {
        @Override
        public boolean wasEvicted() {
            return false;
        }
    }, 
    SIZE {
        @Override
        public boolean wasEvicted() {
            return true;
        }
    };
    
    private static /* synthetic */ RemovalCause[] $values() {
        return new RemovalCause[] { RemovalCause.EXPLICIT, RemovalCause.REPLACED, RemovalCause.COLLECTED, RemovalCause.EXPIRED, RemovalCause.SIZE };
    }
    
    static {
        $VALUES = $values();
    }
    
    public abstract boolean wasEvicted();
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.cct;

import java.util.Map;
import com.google.android.datatransport.runtime.retries.RetryStrategy;
import com.google.android.datatransport.runtime.retries.Function;
import com.google.android.datatransport.runtime.retries.Retries;
import com.google.android.datatransport.runtime.backends.BackendResponse;
import java.util.Locale;
import android.os.Build;
import android.os.Build$VERSION;
import java.net.MalformedURLException;
import java.util.zip.GZIPInputStream;
import java.util.TimeZone;
import java.util.Calendar;
import android.telephony.TelephonyManager;
import com.google.android.datatransport.runtime.EncodedPayload;
import java.util.Iterator;
import java.nio.charset.Charset;
import com.google.android.datatransport.cct.internal.LogEvent;
import com.google.android.datatransport.Encoding;
import com.google.android.datatransport.cct.internal.AndroidClientInfo;
import com.google.android.datatransport.cct.internal.ClientInfo;
import com.google.android.datatransport.cct.internal.QosTier;
import com.google.android.datatransport.cct.internal.LogRequest;
import java.util.List;
import java.util.ArrayList;
import com.google.android.datatransport.runtime.EventInternal;
import java.util.HashMap;
import com.google.android.datatransport.runtime.backends.BackendRequest;
import android.content.pm.PackageManager$NameNotFoundException;
import com.google.android.datatransport.cct.internal.NetworkConnectionInfo;
import android.net.NetworkInfo;
import java.io.Closeable;
import java.net.ConnectException;
import java.net.UnknownHostException;
import com.google.firebase.encoders.EncodingException;
import java.io.IOException;
import com.google.android.datatransport.cct.internal.LogResponse;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.Writer;
import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.zip.GZIPOutputStream;
import java.net.HttpURLConnection;
import com.google.android.datatransport.runtime.logging.Logging;
import com.google.android.datatransport.cct.internal.BatchedLogRequest;
import com.google.android.datatransport.runtime.time.Clock;
import java.net.URL;
import android.net.ConnectivityManager;
import android.content.Context;
import com.google.android.datatransport.runtime.backends.TransportBackend;

final class CctTransportBackend implements TransportBackend
{
    private static final String ACCEPT_ENCODING_HEADER_KEY = "Accept-Encoding";
    static final String API_KEY_HEADER_KEY = "X-Goog-Api-Key";
    private static final int CONNECTION_TIME_OUT = 30000;
    private static final String CONTENT_ENCODING_HEADER_KEY = "Content-Encoding";
    private static final String CONTENT_TYPE_HEADER_KEY = "Content-Type";
    private static final String GZIP_CONTENT_ENCODING = "gzip";
    private static final int INVALID_VERSION_CODE = -1;
    private static final String JSON_CONTENT_TYPE = "application/json";
    private static final String KEY_APPLICATION_BUILD = "application_build";
    private static final String KEY_COUNTRY = "country";
    private static final String KEY_DEVICE = "device";
    private static final String KEY_FINGERPRINT = "fingerprint";
    private static final String KEY_HARDWARE = "hardware";
    private static final String KEY_LOCALE = "locale";
    private static final String KEY_MANUFACTURER = "manufacturer";
    private static final String KEY_MCC_MNC = "mcc_mnc";
    static final String KEY_MOBILE_SUBTYPE = "mobile-subtype";
    private static final String KEY_MODEL = "model";
    static final String KEY_NETWORK_TYPE = "net-type";
    private static final String KEY_OS_BUILD = "os-uild";
    private static final String KEY_PRODUCT = "product";
    private static final String KEY_SDK_VERSION = "sdk-version";
    private static final String KEY_TIMEZONE_OFFSET = "tz-offset";
    private static final String LOG_TAG = "CctTransportBackend";
    private static final int READ_TIME_OUT = 130000;
    private final Context applicationContext;
    private final ConnectivityManager connectivityManager;
    private final hp dataEncoder;
    final URL endPoint;
    private final int readTimeout;
    private final Clock uptimeClock;
    private final Clock wallTimeClock;
    
    public CctTransportBackend(final Context context, final Clock clock, final Clock clock2) {
        this(context, clock, clock2, 130000);
    }
    
    public CctTransportBackend(final Context applicationContext, final Clock wallTimeClock, final Clock uptimeClock, final int readTimeout) {
        this.dataEncoder = BatchedLogRequest.createDataEncoder();
        this.applicationContext = applicationContext;
        this.connectivityManager = (ConnectivityManager)applicationContext.getSystemService("connectivity");
        this.endPoint = parseUrlOrThrow(CCTDestination.DEFAULT_END_POINT);
        this.uptimeClock = uptimeClock;
        this.wallTimeClock = wallTimeClock;
        this.readTimeout = readTimeout;
    }
    
    private HttpResponse doSend(HttpRequest inputStream) {
        Logging.i("CctTransportBackend", "Making request to: %s", ((HttpRequest)inputStream).url);
        final HttpURLConnection httpURLConnection = (HttpURLConnection)((HttpRequest)inputStream).url.openConnection();
        httpURLConnection.setConnectTimeout(30000);
        httpURLConnection.setReadTimeout(this.readTimeout);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setInstanceFollowRedirects(false);
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.setRequestProperty("User-Agent", String.format("datatransport/%s android/", "3.1.9"));
        httpURLConnection.setRequestProperty("Content-Encoding", "gzip");
        httpURLConnection.setRequestProperty("Content-Type", "application/json");
        httpURLConnection.setRequestProperty("Accept-Encoding", "gzip");
        final String apiKey = ((HttpRequest)inputStream).apiKey;
        if (apiKey != null) {
            httpURLConnection.setRequestProperty("X-Goog-Api-Key", apiKey);
        }
        try {
            final OutputStream outputStream = httpURLConnection.getOutputStream();
            try {
                Closeable closeable = new GZIPOutputStream(outputStream);
                try {
                    this.dataEncoder.a(((HttpRequest)inputStream).requestBody, new BufferedWriter(new OutputStreamWriter((OutputStream)closeable)));
                    ((OutputStream)closeable).close();
                    if (outputStream != null) {
                        outputStream.close();
                    }
                    final int responseCode = httpURLConnection.getResponseCode();
                    Logging.i("CctTransportBackend", "Status Code: %d", responseCode);
                    Logging.d("CctTransportBackend", "Content-Type: %s", httpURLConnection.getHeaderField("Content-Type"));
                    Logging.d("CctTransportBackend", "Content-Encoding: %s", httpURLConnection.getHeaderField("Content-Encoding"));
                    if (responseCode != 302 && responseCode != 301) {
                        if (responseCode != 307) {
                            if (responseCode != 200) {
                                return new HttpResponse(responseCode, null, 0L);
                            }
                            inputStream = (IOException)httpURLConnection.getInputStream();
                            try {
                                final InputStream maybeUnGzip = maybeUnGzip((InputStream)inputStream, httpURLConnection.getHeaderField("Content-Encoding"));
                                try {
                                    closeable = new InputStreamReader(maybeUnGzip);
                                    final HttpResponse httpResponse = new HttpResponse(responseCode, null, LogResponse.fromJson(new BufferedReader((Reader)closeable)).getNextRequestWaitMillis());
                                    if (maybeUnGzip != null) {
                                        maybeUnGzip.close();
                                    }
                                    if (inputStream != null) {
                                        ((InputStream)inputStream).close();
                                    }
                                    return httpResponse;
                                }
                                finally {
                                    if (maybeUnGzip != null) {
                                        try {
                                            maybeUnGzip.close();
                                        }
                                        finally {
                                            final Throwable t;
                                            final Throwable exception;
                                            t.addSuppressed(exception);
                                        }
                                    }
                                }
                            }
                            finally {
                                if (inputStream != null) {
                                    try {
                                        ((InputStream)inputStream).close();
                                    }
                                    finally {
                                        final Throwable exception2;
                                        ((Throwable)outputStream).addSuppressed(exception2);
                                    }
                                }
                            }
                        }
                    }
                    return new HttpResponse(responseCode, new URL(httpURLConnection.getHeaderField("Location")), 0L);
                }
                finally {
                    try {
                        ((OutputStream)closeable).close();
                    }
                    finally {
                        final Throwable exception3;
                        inputStream.addSuppressed(exception3);
                    }
                }
            }
            finally {
                if (outputStream != null) {
                    try {
                        outputStream.close();
                    }
                    finally {
                        final Throwable exception4;
                        inputStream.addSuppressed(exception4);
                    }
                }
            }
        }
        catch (final IOException inputStream) {
            goto Label_0504;
        }
        catch (final EncodingException ex) {}
        catch (final UnknownHostException inputStream) {}
        catch (final ConnectException ex2) {}
        Logging.e("CctTransportBackend", "Couldn't open connection, returning with 500", inputStream);
        return new HttpResponse(500, null, 0L);
    }
    
    private static int getNetSubtypeValue(final NetworkInfo networkInfo) {
        NetworkConnectionInfo.MobileSubtype mobileSubtype;
        if (networkInfo == null) {
            mobileSubtype = NetworkConnectionInfo.MobileSubtype.UNKNOWN_MOBILE_SUBTYPE;
        }
        else {
            int subtype = networkInfo.getSubtype();
            if (subtype != -1) {
                if (NetworkConnectionInfo.MobileSubtype.forNumber(subtype) == null) {
                    subtype = 0;
                }
                return subtype;
            }
            mobileSubtype = NetworkConnectionInfo.MobileSubtype.COMBINED;
        }
        return mobileSubtype.getValue();
    }
    
    private static int getNetTypeValue(final NetworkInfo networkInfo) {
        if (networkInfo == null) {
            return NetworkConnectionInfo.NetworkType.NONE.getValue();
        }
        return networkInfo.getType();
    }
    
    private static int getPackageVersionCode(final Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        }
        catch (final PackageManager$NameNotFoundException ex) {
            Logging.e("CctTransportBackend", "Unable to find version code for package", (Throwable)ex);
            return -1;
        }
    }
    
    private BatchedLogRequest getRequestBody(BackendRequest backendRequest) {
        final HashMap hashMap = new HashMap();
        for (final EventInternal eventInternal : backendRequest.getEvents()) {
            final String transportName = eventInternal.getTransportName();
            if (!hashMap.containsKey(transportName)) {
                final ArrayList value = new ArrayList();
                value.add(eventInternal);
                hashMap.put(transportName, value);
            }
            else {
                ((List<EventInternal>)hashMap.get(transportName)).add(eventInternal);
            }
        }
        final ArrayList list = new ArrayList();
        final Iterator iterator2 = hashMap.entrySet().iterator();
        while (iterator2.hasNext()) {
            backendRequest = (BackendRequest)iterator2.next();
            final EventInternal eventInternal2 = ((Map.Entry<K, List<EventInternal>>)backendRequest).getValue().get(0);
            final LogRequest.Builder setClientInfo = LogRequest.builder().setQosTier(QosTier.DEFAULT).setRequestTimeMs(this.wallTimeClock.getTime()).setRequestUptimeMs(this.uptimeClock.getTime()).setClientInfo(ClientInfo.builder().setClientType(ClientInfo.ClientType.ANDROID_FIREBASE).setAndroidClientInfo(AndroidClientInfo.builder().setSdkVersion(eventInternal2.getInteger("sdk-version")).setModel(eventInternal2.get("model")).setHardware(eventInternal2.get("hardware")).setDevice(eventInternal2.get("device")).setProduct(eventInternal2.get("product")).setOsBuild(eventInternal2.get("os-uild")).setManufacturer(eventInternal2.get("manufacturer")).setFingerprint(eventInternal2.get("fingerprint")).setCountry(eventInternal2.get("country")).setLocale(eventInternal2.get("locale")).setMccMnc(eventInternal2.get("mcc_mnc")).setApplicationBuild(eventInternal2.get("application_build")).build()).build());
            try {
                setClientInfo.setSource(Integer.parseInt(((Map.Entry<String, V>)backendRequest).getKey()));
            }
            catch (final NumberFormatException ex) {
                setClientInfo.setSource(((Map.Entry<String, V>)backendRequest).getKey());
            }
            final ArrayList logEvents = new ArrayList();
            for (final EventInternal eventInternal3 : ((Map.Entry<K, List>)backendRequest).getValue()) {
                final EncodedPayload encodedPayload = eventInternal3.getEncodedPayload();
                final Encoding encoding = encodedPayload.getEncoding();
                LogEvent.Builder builder;
                if (encoding.equals(Encoding.of("proto"))) {
                    builder = LogEvent.protoBuilder(encodedPayload.getBytes());
                }
                else {
                    if (!encoding.equals(Encoding.of("json"))) {
                        Logging.w("CctTransportBackend", "Received event of unsupported encoding %s. Skipping...", encoding);
                        continue;
                    }
                    builder = LogEvent.jsonBuilder(new String(encodedPayload.getBytes(), Charset.forName("UTF-8")));
                }
                builder.setEventTimeMs(eventInternal3.getEventMillis()).setEventUptimeMs(eventInternal3.getUptimeMillis()).setTimezoneOffsetSeconds(eventInternal3.getLong("tz-offset")).setNetworkConnectionInfo(NetworkConnectionInfo.builder().setNetworkType(NetworkConnectionInfo.NetworkType.forNumber(eventInternal3.getInteger("net-type"))).setMobileSubtype(NetworkConnectionInfo.MobileSubtype.forNumber(eventInternal3.getInteger("mobile-subtype"))).build());
                if (eventInternal3.getCode() != null) {
                    builder.setEventCode(eventInternal3.getCode());
                }
                logEvents.add(builder.build());
            }
            setClientInfo.setLogEvents(logEvents);
            list.add(setClientInfo.build());
        }
        return BatchedLogRequest.create(list);
    }
    
    private static TelephonyManager getTelephonyManager(final Context context) {
        return (TelephonyManager)context.getSystemService("phone");
    }
    
    public static long getTzOffset() {
        Calendar.getInstance();
        return TimeZone.getDefault().getOffset(Calendar.getInstance().getTimeInMillis()) / 1000;
    }
    
    private static InputStream maybeUnGzip(final InputStream in, final String anObject) {
        if ("gzip".equals(anObject)) {
            return new GZIPInputStream(in);
        }
        return in;
    }
    
    private static URL parseUrlOrThrow(final String s) {
        try {
            return new URL(s);
        }
        catch (final MalformedURLException cause) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid url: ");
            sb.append(s);
            throw new IllegalArgumentException(sb.toString(), cause);
        }
    }
    
    @Override
    public EventInternal decorate(final EventInternal eventInternal) {
        final NetworkInfo activeNetworkInfo = this.connectivityManager.getActiveNetworkInfo();
        return eventInternal.toBuilder().addMetadata("sdk-version", Build$VERSION.SDK_INT).addMetadata("model", Build.MODEL).addMetadata("hardware", Build.HARDWARE).addMetadata("device", Build.DEVICE).addMetadata("product", Build.PRODUCT).addMetadata("os-uild", Build.ID).addMetadata("manufacturer", Build.MANUFACTURER).addMetadata("fingerprint", Build.FINGERPRINT).addMetadata("tz-offset", getTzOffset()).addMetadata("net-type", getNetTypeValue(activeNetworkInfo)).addMetadata("mobile-subtype", getNetSubtypeValue(activeNetworkInfo)).addMetadata("country", Locale.getDefault().getCountry()).addMetadata("locale", Locale.getDefault().getLanguage()).addMetadata("mcc_mnc", getTelephonyManager(this.applicationContext).getSimOperator()).addMetadata("application_build", Integer.toString(getPackageVersionCode(this.applicationContext))).build();
    }
    
    @Override
    public BackendResponse send(final BackendRequest backendRequest) {
        final BatchedLogRequest requestBody = this.getRequestBody(backendRequest);
        final URL endPoint = this.endPoint;
        final byte[] extras = backendRequest.getExtras();
        String s = null;
        final String s2 = null;
        URL urlOrThrow = endPoint;
        if (extras != null) {
            try {
                final CCTDestination fromByteArray = CCTDestination.fromByteArray(backendRequest.getExtras());
                String apiKey = s2;
                if (fromByteArray.getAPIKey() != null) {
                    apiKey = fromByteArray.getAPIKey();
                }
                urlOrThrow = endPoint;
                s = apiKey;
                if (fromByteArray.getEndPoint() != null) {
                    urlOrThrow = parseUrlOrThrow(fromByteArray.getEndPoint());
                    s = apiKey;
                }
            }
            catch (final IllegalArgumentException ex) {
                return BackendResponse.fatalError();
            }
        }
        try {
            final HttpResponse httpResponse = Retries.retry(5, new HttpRequest(urlOrThrow, requestBody, s), (Function<HttpRequest, HttpResponse, Throwable>)new a(this), new b());
            final int code = httpResponse.code;
            if (code == 200) {
                return BackendResponse.ok(httpResponse.nextRequestMillis);
            }
            if (code >= 500 || code == 404) {
                return BackendResponse.transientError();
            }
            if (code == 400) {
                return BackendResponse.invalidPayload();
            }
            return BackendResponse.fatalError();
        }
        catch (final IOException ex2) {
            Logging.e("CctTransportBackend", "Could not make request to the backend", ex2);
            return BackendResponse.transientError();
        }
    }
    
    public static final class HttpRequest
    {
        final String apiKey;
        final BatchedLogRequest requestBody;
        final URL url;
        
        public HttpRequest(final URL url, final BatchedLogRequest requestBody, final String apiKey) {
            this.url = url;
            this.requestBody = requestBody;
            this.apiKey = apiKey;
        }
        
        public HttpRequest withUrl(final URL url) {
            return new HttpRequest(url, this.requestBody, this.apiKey);
        }
    }
    
    public static final class HttpResponse
    {
        final int code;
        final long nextRequestMillis;
        final URL redirectUrl;
        
        public HttpResponse(final int code, final URL redirectUrl, final long nextRequestMillis) {
            this.code = code;
            this.redirectUrl = redirectUrl;
            this.nextRequestMillis = nextRequestMillis;
        }
    }
}

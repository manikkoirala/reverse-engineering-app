// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.cct.internal;

import com.google.auto.value.AutoValue$Builder;
import com.google.auto.value.AutoValue;

@AutoValue
public abstract class AndroidClientInfo
{
    public static Builder builder() {
        return (Builder)new AutoValue_AndroidClientInfo.Builder();
    }
    
    public abstract String getApplicationBuild();
    
    public abstract String getCountry();
    
    public abstract String getDevice();
    
    public abstract String getFingerprint();
    
    public abstract String getHardware();
    
    public abstract String getLocale();
    
    public abstract String getManufacturer();
    
    public abstract String getMccMnc();
    
    public abstract String getModel();
    
    public abstract String getOsBuild();
    
    public abstract String getProduct();
    
    public abstract Integer getSdkVersion();
    
    @AutoValue$Builder
    public abstract static class Builder
    {
        public abstract AndroidClientInfo build();
        
        public abstract Builder setApplicationBuild(final String p0);
        
        public abstract Builder setCountry(final String p0);
        
        public abstract Builder setDevice(final String p0);
        
        public abstract Builder setFingerprint(final String p0);
        
        public abstract Builder setHardware(final String p0);
        
        public abstract Builder setLocale(final String p0);
        
        public abstract Builder setManufacturer(final String p0);
        
        public abstract Builder setMccMnc(final String p0);
        
        public abstract Builder setModel(final String p0);
        
        public abstract Builder setOsBuild(final String p0);
        
        public abstract Builder setProduct(final String p0);
        
        public abstract Builder setSdkVersion(final Integer p0);
    }
}

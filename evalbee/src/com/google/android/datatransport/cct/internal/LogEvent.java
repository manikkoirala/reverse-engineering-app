// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.cct.internal;

import com.google.auto.value.AutoValue$Builder;
import com.google.auto.value.AutoValue;

@AutoValue
public abstract class LogEvent
{
    private static Builder builder() {
        return (Builder)new AutoValue_LogEvent.Builder();
    }
    
    public static Builder jsonBuilder(final String sourceExtensionJsonProto3) {
        return builder().setSourceExtensionJsonProto3(sourceExtensionJsonProto3);
    }
    
    public static Builder protoBuilder(final byte[] sourceExtension) {
        return builder().setSourceExtension(sourceExtension);
    }
    
    public abstract Integer getEventCode();
    
    public abstract long getEventTimeMs();
    
    public abstract long getEventUptimeMs();
    
    public abstract NetworkConnectionInfo getNetworkConnectionInfo();
    
    public abstract byte[] getSourceExtension();
    
    public abstract String getSourceExtensionJsonProto3();
    
    public abstract long getTimezoneOffsetSeconds();
    
    @AutoValue$Builder
    public abstract static class Builder
    {
        public abstract LogEvent build();
        
        public abstract Builder setEventCode(final Integer p0);
        
        public abstract Builder setEventTimeMs(final long p0);
        
        public abstract Builder setEventUptimeMs(final long p0);
        
        public abstract Builder setNetworkConnectionInfo(final NetworkConnectionInfo p0);
        
        public abstract Builder setSourceExtension(final byte[] p0);
        
        public abstract Builder setSourceExtensionJsonProto3(final String p0);
        
        public abstract Builder setTimezoneOffsetSeconds(final long p0);
    }
}

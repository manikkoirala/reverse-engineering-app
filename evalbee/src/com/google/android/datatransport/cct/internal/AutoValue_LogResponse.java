// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.cct.internal;

final class AutoValue_LogResponse extends LogResponse
{
    private final long nextRequestWaitMillis;
    
    public AutoValue_LogResponse(final long nextRequestWaitMillis) {
        this.nextRequestWaitMillis = nextRequestWaitMillis;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof LogResponse) {
            if (this.nextRequestWaitMillis != ((LogResponse)o).getNextRequestWaitMillis()) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public long getNextRequestWaitMillis() {
        return this.nextRequestWaitMillis;
    }
    
    @Override
    public int hashCode() {
        final long nextRequestWaitMillis = this.nextRequestWaitMillis;
        return (int)(nextRequestWaitMillis ^ nextRequestWaitMillis >>> 32) ^ 0xF4243;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("LogResponse{nextRequestWaitMillis=");
        sb.append(this.nextRequestWaitMillis);
        sb.append("}");
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.cct.internal;

final class AutoValue_NetworkConnectionInfo extends NetworkConnectionInfo
{
    private final MobileSubtype mobileSubtype;
    private final NetworkType networkType;
    
    private AutoValue_NetworkConnectionInfo(final NetworkType networkType, final MobileSubtype mobileSubtype) {
        this.networkType = networkType;
        this.mobileSubtype = mobileSubtype;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof NetworkConnectionInfo) {
            final NetworkConnectionInfo networkConnectionInfo = (NetworkConnectionInfo)o;
            final NetworkType networkType = this.networkType;
            if (networkType == null) {
                if (networkConnectionInfo.getNetworkType() != null) {
                    return false;
                }
            }
            else if (!networkType.equals(networkConnectionInfo.getNetworkType())) {
                return false;
            }
            final MobileSubtype mobileSubtype = this.mobileSubtype;
            final MobileSubtype mobileSubtype2 = networkConnectionInfo.getMobileSubtype();
            if (mobileSubtype == null) {
                if (mobileSubtype2 == null) {
                    return b;
                }
            }
            else if (mobileSubtype.equals(mobileSubtype2)) {
                return b;
            }
            b = false;
            return b;
        }
        return false;
    }
    
    @Override
    public MobileSubtype getMobileSubtype() {
        return this.mobileSubtype;
    }
    
    @Override
    public NetworkType getNetworkType() {
        return this.networkType;
    }
    
    @Override
    public int hashCode() {
        final NetworkType networkType = this.networkType;
        int hashCode = 0;
        int hashCode2;
        if (networkType == null) {
            hashCode2 = 0;
        }
        else {
            hashCode2 = networkType.hashCode();
        }
        final MobileSubtype mobileSubtype = this.mobileSubtype;
        if (mobileSubtype != null) {
            hashCode = mobileSubtype.hashCode();
        }
        return (hashCode2 ^ 0xF4243) * 1000003 ^ hashCode;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("NetworkConnectionInfo{networkType=");
        sb.append(this.networkType);
        sb.append(", mobileSubtype=");
        sb.append(this.mobileSubtype);
        sb.append("}");
        return sb.toString();
    }
    
    public static final class Builder extends NetworkConnectionInfo.Builder
    {
        private MobileSubtype mobileSubtype;
        private NetworkType networkType;
        
        @Override
        public NetworkConnectionInfo build() {
            return new AutoValue_NetworkConnectionInfo(this.networkType, this.mobileSubtype, null);
        }
        
        @Override
        public NetworkConnectionInfo.Builder setMobileSubtype(final MobileSubtype mobileSubtype) {
            this.mobileSubtype = mobileSubtype;
            return this;
        }
        
        @Override
        public NetworkConnectionInfo.Builder setNetworkType(final NetworkType networkType) {
            this.networkType = networkType;
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.cct.internal;

import java.util.List;
import com.google.auto.value.AutoValue;

@AutoValue
public abstract class BatchedLogRequest
{
    public static BatchedLogRequest create(final List<LogRequest> list) {
        return new AutoValue_BatchedLogRequest(list);
    }
    
    public static hp createDataEncoder() {
        return new mh0().j(AutoBatchedLogRequestEncoder.CONFIG).k(true).i();
    }
    
    public abstract List<LogRequest> getLogRequests();
}

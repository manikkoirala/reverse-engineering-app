// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.cct.internal;

import com.google.auto.value.AutoValue$Builder;
import com.google.auto.value.AutoValue;

@AutoValue
public abstract class ClientInfo
{
    public static Builder builder() {
        return (Builder)new AutoValue_ClientInfo.Builder();
    }
    
    public abstract AndroidClientInfo getAndroidClientInfo();
    
    public abstract ClientType getClientType();
    
    @AutoValue$Builder
    public abstract static class Builder
    {
        public abstract ClientInfo build();
        
        public abstract Builder setAndroidClientInfo(final AndroidClientInfo p0);
        
        public abstract Builder setClientType(final ClientType p0);
    }
    
    public enum ClientType
    {
        private static final ClientType[] $VALUES;
        
        ANDROID_FIREBASE(23), 
        UNKNOWN(0);
        
        private final int value;
        
        private ClientType(final int value) {
            this.value = value;
        }
    }
}

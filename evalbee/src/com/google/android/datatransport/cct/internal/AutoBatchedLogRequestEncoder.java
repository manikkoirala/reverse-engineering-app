// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.cct.internal;

public final class AutoBatchedLogRequestEncoder implements jk
{
    public static final int CODEGEN_VERSION = 2;
    public static final jk CONFIG;
    
    static {
        CONFIG = new AutoBatchedLogRequestEncoder();
    }
    
    private AutoBatchedLogRequestEncoder() {
    }
    
    @Override
    public void configure(final zw zw) {
        final BatchedLogRequestEncoder instance = BatchedLogRequestEncoder.INSTANCE;
        zw.a(BatchedLogRequest.class, instance);
        zw.a(AutoValue_BatchedLogRequest.class, instance);
        final LogRequestEncoder instance2 = LogRequestEncoder.INSTANCE;
        zw.a(LogRequest.class, instance2);
        zw.a(AutoValue_LogRequest.class, instance2);
        final ClientInfoEncoder instance3 = ClientInfoEncoder.INSTANCE;
        zw.a(ClientInfo.class, instance3);
        zw.a(AutoValue_ClientInfo.class, instance3);
        final AndroidClientInfoEncoder instance4 = AndroidClientInfoEncoder.INSTANCE;
        zw.a(AndroidClientInfo.class, instance4);
        zw.a(AutoValue_AndroidClientInfo.class, instance4);
        final LogEventEncoder instance5 = LogEventEncoder.INSTANCE;
        zw.a(LogEvent.class, instance5);
        zw.a(AutoValue_LogEvent.class, instance5);
        final NetworkConnectionInfoEncoder instance6 = NetworkConnectionInfoEncoder.INSTANCE;
        zw.a(NetworkConnectionInfo.class, instance6);
        zw.a(AutoValue_NetworkConnectionInfo.class, instance6);
    }
    
    public static final class AndroidClientInfoEncoder implements w01
    {
        private static final n00 APPLICATIONBUILD_DESCRIPTOR;
        private static final n00 COUNTRY_DESCRIPTOR;
        private static final n00 DEVICE_DESCRIPTOR;
        private static final n00 FINGERPRINT_DESCRIPTOR;
        private static final n00 HARDWARE_DESCRIPTOR;
        static final AndroidClientInfoEncoder INSTANCE;
        private static final n00 LOCALE_DESCRIPTOR;
        private static final n00 MANUFACTURER_DESCRIPTOR;
        private static final n00 MCCMNC_DESCRIPTOR;
        private static final n00 MODEL_DESCRIPTOR;
        private static final n00 OSBUILD_DESCRIPTOR;
        private static final n00 PRODUCT_DESCRIPTOR;
        private static final n00 SDKVERSION_DESCRIPTOR;
        
        static {
            INSTANCE = new AndroidClientInfoEncoder();
            SDKVERSION_DESCRIPTOR = n00.d("sdkVersion");
            MODEL_DESCRIPTOR = n00.d("model");
            HARDWARE_DESCRIPTOR = n00.d("hardware");
            DEVICE_DESCRIPTOR = n00.d("device");
            PRODUCT_DESCRIPTOR = n00.d("product");
            OSBUILD_DESCRIPTOR = n00.d("osBuild");
            MANUFACTURER_DESCRIPTOR = n00.d("manufacturer");
            FINGERPRINT_DESCRIPTOR = n00.d("fingerprint");
            LOCALE_DESCRIPTOR = n00.d("locale");
            COUNTRY_DESCRIPTOR = n00.d("country");
            MCCMNC_DESCRIPTOR = n00.d("mccMnc");
            APPLICATIONBUILD_DESCRIPTOR = n00.d("applicationBuild");
        }
        
        private AndroidClientInfoEncoder() {
        }
        
        public void encode(final AndroidClientInfo androidClientInfo, final x01 x01) {
            x01.f(AndroidClientInfoEncoder.SDKVERSION_DESCRIPTOR, androidClientInfo.getSdkVersion());
            x01.f(AndroidClientInfoEncoder.MODEL_DESCRIPTOR, androidClientInfo.getModel());
            x01.f(AndroidClientInfoEncoder.HARDWARE_DESCRIPTOR, androidClientInfo.getHardware());
            x01.f(AndroidClientInfoEncoder.DEVICE_DESCRIPTOR, androidClientInfo.getDevice());
            x01.f(AndroidClientInfoEncoder.PRODUCT_DESCRIPTOR, androidClientInfo.getProduct());
            x01.f(AndroidClientInfoEncoder.OSBUILD_DESCRIPTOR, androidClientInfo.getOsBuild());
            x01.f(AndroidClientInfoEncoder.MANUFACTURER_DESCRIPTOR, androidClientInfo.getManufacturer());
            x01.f(AndroidClientInfoEncoder.FINGERPRINT_DESCRIPTOR, androidClientInfo.getFingerprint());
            x01.f(AndroidClientInfoEncoder.LOCALE_DESCRIPTOR, androidClientInfo.getLocale());
            x01.f(AndroidClientInfoEncoder.COUNTRY_DESCRIPTOR, androidClientInfo.getCountry());
            x01.f(AndroidClientInfoEncoder.MCCMNC_DESCRIPTOR, androidClientInfo.getMccMnc());
            x01.f(AndroidClientInfoEncoder.APPLICATIONBUILD_DESCRIPTOR, androidClientInfo.getApplicationBuild());
        }
    }
    
    public static final class BatchedLogRequestEncoder implements w01
    {
        static final BatchedLogRequestEncoder INSTANCE;
        private static final n00 LOGREQUEST_DESCRIPTOR;
        
        static {
            INSTANCE = new BatchedLogRequestEncoder();
            LOGREQUEST_DESCRIPTOR = n00.d("logRequest");
        }
        
        private BatchedLogRequestEncoder() {
        }
        
        public void encode(final BatchedLogRequest batchedLogRequest, final x01 x01) {
            x01.f(BatchedLogRequestEncoder.LOGREQUEST_DESCRIPTOR, batchedLogRequest.getLogRequests());
        }
    }
    
    public static final class ClientInfoEncoder implements w01
    {
        private static final n00 ANDROIDCLIENTINFO_DESCRIPTOR;
        private static final n00 CLIENTTYPE_DESCRIPTOR;
        static final ClientInfoEncoder INSTANCE;
        
        static {
            INSTANCE = new ClientInfoEncoder();
            CLIENTTYPE_DESCRIPTOR = n00.d("clientType");
            ANDROIDCLIENTINFO_DESCRIPTOR = n00.d("androidClientInfo");
        }
        
        private ClientInfoEncoder() {
        }
        
        public void encode(final ClientInfo clientInfo, final x01 x01) {
            x01.f(ClientInfoEncoder.CLIENTTYPE_DESCRIPTOR, clientInfo.getClientType());
            x01.f(ClientInfoEncoder.ANDROIDCLIENTINFO_DESCRIPTOR, clientInfo.getAndroidClientInfo());
        }
    }
    
    public static final class LogEventEncoder implements w01
    {
        private static final n00 EVENTCODE_DESCRIPTOR;
        private static final n00 EVENTTIMEMS_DESCRIPTOR;
        private static final n00 EVENTUPTIMEMS_DESCRIPTOR;
        static final LogEventEncoder INSTANCE;
        private static final n00 NETWORKCONNECTIONINFO_DESCRIPTOR;
        private static final n00 SOURCEEXTENSIONJSONPROTO3_DESCRIPTOR;
        private static final n00 SOURCEEXTENSION_DESCRIPTOR;
        private static final n00 TIMEZONEOFFSETSECONDS_DESCRIPTOR;
        
        static {
            INSTANCE = new LogEventEncoder();
            EVENTTIMEMS_DESCRIPTOR = n00.d("eventTimeMs");
            EVENTCODE_DESCRIPTOR = n00.d("eventCode");
            EVENTUPTIMEMS_DESCRIPTOR = n00.d("eventUptimeMs");
            SOURCEEXTENSION_DESCRIPTOR = n00.d("sourceExtension");
            SOURCEEXTENSIONJSONPROTO3_DESCRIPTOR = n00.d("sourceExtensionJsonProto3");
            TIMEZONEOFFSETSECONDS_DESCRIPTOR = n00.d("timezoneOffsetSeconds");
            NETWORKCONNECTIONINFO_DESCRIPTOR = n00.d("networkConnectionInfo");
        }
        
        private LogEventEncoder() {
        }
        
        public void encode(final LogEvent logEvent, final x01 x01) {
            x01.e(LogEventEncoder.EVENTTIMEMS_DESCRIPTOR, logEvent.getEventTimeMs());
            x01.f(LogEventEncoder.EVENTCODE_DESCRIPTOR, logEvent.getEventCode());
            x01.e(LogEventEncoder.EVENTUPTIMEMS_DESCRIPTOR, logEvent.getEventUptimeMs());
            x01.f(LogEventEncoder.SOURCEEXTENSION_DESCRIPTOR, logEvent.getSourceExtension());
            x01.f(LogEventEncoder.SOURCEEXTENSIONJSONPROTO3_DESCRIPTOR, logEvent.getSourceExtensionJsonProto3());
            x01.e(LogEventEncoder.TIMEZONEOFFSETSECONDS_DESCRIPTOR, logEvent.getTimezoneOffsetSeconds());
            x01.f(LogEventEncoder.NETWORKCONNECTIONINFO_DESCRIPTOR, logEvent.getNetworkConnectionInfo());
        }
    }
    
    public static final class LogRequestEncoder implements w01
    {
        private static final n00 CLIENTINFO_DESCRIPTOR;
        static final LogRequestEncoder INSTANCE;
        private static final n00 LOGEVENT_DESCRIPTOR;
        private static final n00 LOGSOURCENAME_DESCRIPTOR;
        private static final n00 LOGSOURCE_DESCRIPTOR;
        private static final n00 QOSTIER_DESCRIPTOR;
        private static final n00 REQUESTTIMEMS_DESCRIPTOR;
        private static final n00 REQUESTUPTIMEMS_DESCRIPTOR;
        
        static {
            INSTANCE = new LogRequestEncoder();
            REQUESTTIMEMS_DESCRIPTOR = n00.d("requestTimeMs");
            REQUESTUPTIMEMS_DESCRIPTOR = n00.d("requestUptimeMs");
            CLIENTINFO_DESCRIPTOR = n00.d("clientInfo");
            LOGSOURCE_DESCRIPTOR = n00.d("logSource");
            LOGSOURCENAME_DESCRIPTOR = n00.d("logSourceName");
            LOGEVENT_DESCRIPTOR = n00.d("logEvent");
            QOSTIER_DESCRIPTOR = n00.d("qosTier");
        }
        
        private LogRequestEncoder() {
        }
        
        public void encode(final LogRequest logRequest, final x01 x01) {
            x01.e(LogRequestEncoder.REQUESTTIMEMS_DESCRIPTOR, logRequest.getRequestTimeMs());
            x01.e(LogRequestEncoder.REQUESTUPTIMEMS_DESCRIPTOR, logRequest.getRequestUptimeMs());
            x01.f(LogRequestEncoder.CLIENTINFO_DESCRIPTOR, logRequest.getClientInfo());
            x01.f(LogRequestEncoder.LOGSOURCE_DESCRIPTOR, logRequest.getLogSource());
            x01.f(LogRequestEncoder.LOGSOURCENAME_DESCRIPTOR, logRequest.getLogSourceName());
            x01.f(LogRequestEncoder.LOGEVENT_DESCRIPTOR, logRequest.getLogEvents());
            x01.f(LogRequestEncoder.QOSTIER_DESCRIPTOR, logRequest.getQosTier());
        }
    }
    
    public static final class NetworkConnectionInfoEncoder implements w01
    {
        static final NetworkConnectionInfoEncoder INSTANCE;
        private static final n00 MOBILESUBTYPE_DESCRIPTOR;
        private static final n00 NETWORKTYPE_DESCRIPTOR;
        
        static {
            INSTANCE = new NetworkConnectionInfoEncoder();
            NETWORKTYPE_DESCRIPTOR = n00.d("networkType");
            MOBILESUBTYPE_DESCRIPTOR = n00.d("mobileSubtype");
        }
        
        private NetworkConnectionInfoEncoder() {
        }
        
        public void encode(final NetworkConnectionInfo networkConnectionInfo, final x01 x01) {
            x01.f(NetworkConnectionInfoEncoder.NETWORKTYPE_DESCRIPTOR, networkConnectionInfo.getNetworkType());
            x01.f(NetworkConnectionInfoEncoder.MOBILESUBTYPE_DESCRIPTOR, networkConnectionInfo.getMobileSubtype());
        }
    }
}

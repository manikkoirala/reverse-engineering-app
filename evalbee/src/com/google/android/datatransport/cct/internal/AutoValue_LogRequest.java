// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.cct.internal;

import java.util.List;

final class AutoValue_LogRequest extends LogRequest
{
    private final ClientInfo clientInfo;
    private final List<LogEvent> logEvents;
    private final Integer logSource;
    private final String logSourceName;
    private final QosTier qosTier;
    private final long requestTimeMs;
    private final long requestUptimeMs;
    
    private AutoValue_LogRequest(final long requestTimeMs, final long requestUptimeMs, final ClientInfo clientInfo, final Integer logSource, final String logSourceName, final List<LogEvent> logEvents, final QosTier qosTier) {
        this.requestTimeMs = requestTimeMs;
        this.requestUptimeMs = requestUptimeMs;
        this.clientInfo = clientInfo;
        this.logSource = logSource;
        this.logSourceName = logSourceName;
        this.logEvents = logEvents;
        this.qosTier = qosTier;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof LogRequest) {
            final LogRequest logRequest = (LogRequest)o;
            if (this.requestTimeMs == logRequest.getRequestTimeMs() && this.requestUptimeMs == logRequest.getRequestUptimeMs()) {
                final ClientInfo clientInfo = this.clientInfo;
                if (clientInfo == null) {
                    if (logRequest.getClientInfo() != null) {
                        return false;
                    }
                }
                else if (!clientInfo.equals(logRequest.getClientInfo())) {
                    return false;
                }
                final Integer logSource = this.logSource;
                if (logSource == null) {
                    if (logRequest.getLogSource() != null) {
                        return false;
                    }
                }
                else if (!logSource.equals(logRequest.getLogSource())) {
                    return false;
                }
                final String logSourceName = this.logSourceName;
                if (logSourceName == null) {
                    if (logRequest.getLogSourceName() != null) {
                        return false;
                    }
                }
                else if (!logSourceName.equals(logRequest.getLogSourceName())) {
                    return false;
                }
                final List<LogEvent> logEvents = this.logEvents;
                if (logEvents == null) {
                    if (logRequest.getLogEvents() != null) {
                        return false;
                    }
                }
                else if (!logEvents.equals(logRequest.getLogEvents())) {
                    return false;
                }
                final QosTier qosTier = this.qosTier;
                final QosTier qosTier2 = logRequest.getQosTier();
                if (qosTier == null) {
                    if (qosTier2 == null) {
                        return b;
                    }
                }
                else if (qosTier.equals(qosTier2)) {
                    return b;
                }
            }
            b = false;
            return b;
        }
        return false;
    }
    
    @Override
    public ClientInfo getClientInfo() {
        return this.clientInfo;
    }
    
    @Override
    public List<LogEvent> getLogEvents() {
        return this.logEvents;
    }
    
    @Override
    public Integer getLogSource() {
        return this.logSource;
    }
    
    @Override
    public String getLogSourceName() {
        return this.logSourceName;
    }
    
    @Override
    public QosTier getQosTier() {
        return this.qosTier;
    }
    
    @Override
    public long getRequestTimeMs() {
        return this.requestTimeMs;
    }
    
    @Override
    public long getRequestUptimeMs() {
        return this.requestUptimeMs;
    }
    
    @Override
    public int hashCode() {
        final long requestTimeMs = this.requestTimeMs;
        final int n = (int)(requestTimeMs ^ requestTimeMs >>> 32);
        final long requestUptimeMs = this.requestUptimeMs;
        final int n2 = (int)(requestUptimeMs >>> 32 ^ requestUptimeMs);
        final ClientInfo clientInfo = this.clientInfo;
        int hashCode = 0;
        int hashCode2;
        if (clientInfo == null) {
            hashCode2 = 0;
        }
        else {
            hashCode2 = clientInfo.hashCode();
        }
        final Integer logSource = this.logSource;
        int hashCode3;
        if (logSource == null) {
            hashCode3 = 0;
        }
        else {
            hashCode3 = logSource.hashCode();
        }
        final String logSourceName = this.logSourceName;
        int hashCode4;
        if (logSourceName == null) {
            hashCode4 = 0;
        }
        else {
            hashCode4 = logSourceName.hashCode();
        }
        final List<LogEvent> logEvents = this.logEvents;
        int hashCode5;
        if (logEvents == null) {
            hashCode5 = 0;
        }
        else {
            hashCode5 = logEvents.hashCode();
        }
        final QosTier qosTier = this.qosTier;
        if (qosTier != null) {
            hashCode = qosTier.hashCode();
        }
        return ((((((n ^ 0xF4243) * 1000003 ^ n2) * 1000003 ^ hashCode2) * 1000003 ^ hashCode3) * 1000003 ^ hashCode4) * 1000003 ^ hashCode5) * 1000003 ^ hashCode;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("LogRequest{requestTimeMs=");
        sb.append(this.requestTimeMs);
        sb.append(", requestUptimeMs=");
        sb.append(this.requestUptimeMs);
        sb.append(", clientInfo=");
        sb.append(this.clientInfo);
        sb.append(", logSource=");
        sb.append(this.logSource);
        sb.append(", logSourceName=");
        sb.append(this.logSourceName);
        sb.append(", logEvents=");
        sb.append(this.logEvents);
        sb.append(", qosTier=");
        sb.append(this.qosTier);
        sb.append("}");
        return sb.toString();
    }
    
    public static final class Builder extends LogRequest.Builder
    {
        private ClientInfo clientInfo;
        private List<LogEvent> logEvents;
        private Integer logSource;
        private String logSourceName;
        private QosTier qosTier;
        private Long requestTimeMs;
        private Long requestUptimeMs;
        
        @Override
        public LogRequest build() {
            final Long requestTimeMs = this.requestTimeMs;
            String string = "";
            if (requestTimeMs == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("");
                sb.append(" requestTimeMs");
                string = sb.toString();
            }
            String string2 = string;
            if (this.requestUptimeMs == null) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(string);
                sb2.append(" requestUptimeMs");
                string2 = sb2.toString();
            }
            if (string2.isEmpty()) {
                return new AutoValue_LogRequest(this.requestTimeMs, this.requestUptimeMs, this.clientInfo, this.logSource, this.logSourceName, this.logEvents, this.qosTier, null);
            }
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Missing required properties:");
            sb3.append(string2);
            throw new IllegalStateException(sb3.toString());
        }
        
        @Override
        public LogRequest.Builder setClientInfo(final ClientInfo clientInfo) {
            this.clientInfo = clientInfo;
            return this;
        }
        
        @Override
        public LogRequest.Builder setLogEvents(final List<LogEvent> logEvents) {
            this.logEvents = logEvents;
            return this;
        }
        
        @Override
        public LogRequest.Builder setLogSource(final Integer logSource) {
            this.logSource = logSource;
            return this;
        }
        
        @Override
        public LogRequest.Builder setLogSourceName(final String logSourceName) {
            this.logSourceName = logSourceName;
            return this;
        }
        
        @Override
        public LogRequest.Builder setQosTier(final QosTier qosTier) {
            this.qosTier = qosTier;
            return this;
        }
        
        @Override
        public LogRequest.Builder setRequestTimeMs(final long l) {
            this.requestTimeMs = l;
            return this;
        }
        
        @Override
        public LogRequest.Builder setRequestUptimeMs(final long l) {
            this.requestUptimeMs = l;
            return this;
        }
    }
}

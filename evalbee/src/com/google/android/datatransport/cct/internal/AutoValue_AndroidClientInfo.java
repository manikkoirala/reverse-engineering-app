// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.cct.internal;

final class AutoValue_AndroidClientInfo extends AndroidClientInfo
{
    private final String applicationBuild;
    private final String country;
    private final String device;
    private final String fingerprint;
    private final String hardware;
    private final String locale;
    private final String manufacturer;
    private final String mccMnc;
    private final String model;
    private final String osBuild;
    private final String product;
    private final Integer sdkVersion;
    
    private AutoValue_AndroidClientInfo(final Integer sdkVersion, final String model, final String hardware, final String device, final String product, final String osBuild, final String manufacturer, final String fingerprint, final String locale, final String country, final String mccMnc, final String applicationBuild) {
        this.sdkVersion = sdkVersion;
        this.model = model;
        this.hardware = hardware;
        this.device = device;
        this.product = product;
        this.osBuild = osBuild;
        this.manufacturer = manufacturer;
        this.fingerprint = fingerprint;
        this.locale = locale;
        this.country = country;
        this.mccMnc = mccMnc;
        this.applicationBuild = applicationBuild;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof AndroidClientInfo) {
            final AndroidClientInfo androidClientInfo = (AndroidClientInfo)o;
            final Integer sdkVersion = this.sdkVersion;
            if (sdkVersion == null) {
                if (androidClientInfo.getSdkVersion() != null) {
                    return false;
                }
            }
            else if (!sdkVersion.equals(androidClientInfo.getSdkVersion())) {
                return false;
            }
            final String model = this.model;
            if (model == null) {
                if (androidClientInfo.getModel() != null) {
                    return false;
                }
            }
            else if (!model.equals(androidClientInfo.getModel())) {
                return false;
            }
            final String hardware = this.hardware;
            if (hardware == null) {
                if (androidClientInfo.getHardware() != null) {
                    return false;
                }
            }
            else if (!hardware.equals(androidClientInfo.getHardware())) {
                return false;
            }
            final String device = this.device;
            if (device == null) {
                if (androidClientInfo.getDevice() != null) {
                    return false;
                }
            }
            else if (!device.equals(androidClientInfo.getDevice())) {
                return false;
            }
            final String product = this.product;
            if (product == null) {
                if (androidClientInfo.getProduct() != null) {
                    return false;
                }
            }
            else if (!product.equals(androidClientInfo.getProduct())) {
                return false;
            }
            final String osBuild = this.osBuild;
            if (osBuild == null) {
                if (androidClientInfo.getOsBuild() != null) {
                    return false;
                }
            }
            else if (!osBuild.equals(androidClientInfo.getOsBuild())) {
                return false;
            }
            final String manufacturer = this.manufacturer;
            if (manufacturer == null) {
                if (androidClientInfo.getManufacturer() != null) {
                    return false;
                }
            }
            else if (!manufacturer.equals(androidClientInfo.getManufacturer())) {
                return false;
            }
            final String fingerprint = this.fingerprint;
            if (fingerprint == null) {
                if (androidClientInfo.getFingerprint() != null) {
                    return false;
                }
            }
            else if (!fingerprint.equals(androidClientInfo.getFingerprint())) {
                return false;
            }
            final String locale = this.locale;
            if (locale == null) {
                if (androidClientInfo.getLocale() != null) {
                    return false;
                }
            }
            else if (!locale.equals(androidClientInfo.getLocale())) {
                return false;
            }
            final String country = this.country;
            if (country == null) {
                if (androidClientInfo.getCountry() != null) {
                    return false;
                }
            }
            else if (!country.equals(androidClientInfo.getCountry())) {
                return false;
            }
            final String mccMnc = this.mccMnc;
            if (mccMnc == null) {
                if (androidClientInfo.getMccMnc() != null) {
                    return false;
                }
            }
            else if (!mccMnc.equals(androidClientInfo.getMccMnc())) {
                return false;
            }
            final String applicationBuild = this.applicationBuild;
            final String applicationBuild2 = androidClientInfo.getApplicationBuild();
            if (applicationBuild == null) {
                if (applicationBuild2 == null) {
                    return b;
                }
            }
            else if (applicationBuild.equals(applicationBuild2)) {
                return b;
            }
            b = false;
            return b;
        }
        return false;
    }
    
    @Override
    public String getApplicationBuild() {
        return this.applicationBuild;
    }
    
    @Override
    public String getCountry() {
        return this.country;
    }
    
    @Override
    public String getDevice() {
        return this.device;
    }
    
    @Override
    public String getFingerprint() {
        return this.fingerprint;
    }
    
    @Override
    public String getHardware() {
        return this.hardware;
    }
    
    @Override
    public String getLocale() {
        return this.locale;
    }
    
    @Override
    public String getManufacturer() {
        return this.manufacturer;
    }
    
    @Override
    public String getMccMnc() {
        return this.mccMnc;
    }
    
    @Override
    public String getModel() {
        return this.model;
    }
    
    @Override
    public String getOsBuild() {
        return this.osBuild;
    }
    
    @Override
    public String getProduct() {
        return this.product;
    }
    
    @Override
    public Integer getSdkVersion() {
        return this.sdkVersion;
    }
    
    @Override
    public int hashCode() {
        final Integer sdkVersion = this.sdkVersion;
        int hashCode = 0;
        int hashCode2;
        if (sdkVersion == null) {
            hashCode2 = 0;
        }
        else {
            hashCode2 = sdkVersion.hashCode();
        }
        final String model = this.model;
        int hashCode3;
        if (model == null) {
            hashCode3 = 0;
        }
        else {
            hashCode3 = model.hashCode();
        }
        final String hardware = this.hardware;
        int hashCode4;
        if (hardware == null) {
            hashCode4 = 0;
        }
        else {
            hashCode4 = hardware.hashCode();
        }
        final String device = this.device;
        int hashCode5;
        if (device == null) {
            hashCode5 = 0;
        }
        else {
            hashCode5 = device.hashCode();
        }
        final String product = this.product;
        int hashCode6;
        if (product == null) {
            hashCode6 = 0;
        }
        else {
            hashCode6 = product.hashCode();
        }
        final String osBuild = this.osBuild;
        int hashCode7;
        if (osBuild == null) {
            hashCode7 = 0;
        }
        else {
            hashCode7 = osBuild.hashCode();
        }
        final String manufacturer = this.manufacturer;
        int hashCode8;
        if (manufacturer == null) {
            hashCode8 = 0;
        }
        else {
            hashCode8 = manufacturer.hashCode();
        }
        final String fingerprint = this.fingerprint;
        int hashCode9;
        if (fingerprint == null) {
            hashCode9 = 0;
        }
        else {
            hashCode9 = fingerprint.hashCode();
        }
        final String locale = this.locale;
        int hashCode10;
        if (locale == null) {
            hashCode10 = 0;
        }
        else {
            hashCode10 = locale.hashCode();
        }
        final String country = this.country;
        int hashCode11;
        if (country == null) {
            hashCode11 = 0;
        }
        else {
            hashCode11 = country.hashCode();
        }
        final String mccMnc = this.mccMnc;
        int hashCode12;
        if (mccMnc == null) {
            hashCode12 = 0;
        }
        else {
            hashCode12 = mccMnc.hashCode();
        }
        final String applicationBuild = this.applicationBuild;
        if (applicationBuild != null) {
            hashCode = applicationBuild.hashCode();
        }
        return (((((((((((hashCode2 ^ 0xF4243) * 1000003 ^ hashCode3) * 1000003 ^ hashCode4) * 1000003 ^ hashCode5) * 1000003 ^ hashCode6) * 1000003 ^ hashCode7) * 1000003 ^ hashCode8) * 1000003 ^ hashCode9) * 1000003 ^ hashCode10) * 1000003 ^ hashCode11) * 1000003 ^ hashCode12) * 1000003 ^ hashCode;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("AndroidClientInfo{sdkVersion=");
        sb.append(this.sdkVersion);
        sb.append(", model=");
        sb.append(this.model);
        sb.append(", hardware=");
        sb.append(this.hardware);
        sb.append(", device=");
        sb.append(this.device);
        sb.append(", product=");
        sb.append(this.product);
        sb.append(", osBuild=");
        sb.append(this.osBuild);
        sb.append(", manufacturer=");
        sb.append(this.manufacturer);
        sb.append(", fingerprint=");
        sb.append(this.fingerprint);
        sb.append(", locale=");
        sb.append(this.locale);
        sb.append(", country=");
        sb.append(this.country);
        sb.append(", mccMnc=");
        sb.append(this.mccMnc);
        sb.append(", applicationBuild=");
        sb.append(this.applicationBuild);
        sb.append("}");
        return sb.toString();
    }
    
    public static final class Builder extends AndroidClientInfo.Builder
    {
        private String applicationBuild;
        private String country;
        private String device;
        private String fingerprint;
        private String hardware;
        private String locale;
        private String manufacturer;
        private String mccMnc;
        private String model;
        private String osBuild;
        private String product;
        private Integer sdkVersion;
        
        @Override
        public AndroidClientInfo build() {
            return new AutoValue_AndroidClientInfo(this.sdkVersion, this.model, this.hardware, this.device, this.product, this.osBuild, this.manufacturer, this.fingerprint, this.locale, this.country, this.mccMnc, this.applicationBuild, null);
        }
        
        @Override
        public AndroidClientInfo.Builder setApplicationBuild(final String applicationBuild) {
            this.applicationBuild = applicationBuild;
            return this;
        }
        
        @Override
        public AndroidClientInfo.Builder setCountry(final String country) {
            this.country = country;
            return this;
        }
        
        @Override
        public AndroidClientInfo.Builder setDevice(final String device) {
            this.device = device;
            return this;
        }
        
        @Override
        public AndroidClientInfo.Builder setFingerprint(final String fingerprint) {
            this.fingerprint = fingerprint;
            return this;
        }
        
        @Override
        public AndroidClientInfo.Builder setHardware(final String hardware) {
            this.hardware = hardware;
            return this;
        }
        
        @Override
        public AndroidClientInfo.Builder setLocale(final String locale) {
            this.locale = locale;
            return this;
        }
        
        @Override
        public AndroidClientInfo.Builder setManufacturer(final String manufacturer) {
            this.manufacturer = manufacturer;
            return this;
        }
        
        @Override
        public AndroidClientInfo.Builder setMccMnc(final String mccMnc) {
            this.mccMnc = mccMnc;
            return this;
        }
        
        @Override
        public AndroidClientInfo.Builder setModel(final String model) {
            this.model = model;
            return this;
        }
        
        @Override
        public AndroidClientInfo.Builder setOsBuild(final String osBuild) {
            this.osBuild = osBuild;
            return this;
        }
        
        @Override
        public AndroidClientInfo.Builder setProduct(final String product) {
            this.product = product;
            return this;
        }
        
        @Override
        public AndroidClientInfo.Builder setSdkVersion(final Integer sdkVersion) {
            this.sdkVersion = sdkVersion;
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.cct.internal;

import java.util.List;

final class AutoValue_BatchedLogRequest extends BatchedLogRequest
{
    private final List<LogRequest> logRequests;
    
    public AutoValue_BatchedLogRequest(final List<LogRequest> logRequests) {
        if (logRequests != null) {
            this.logRequests = logRequests;
            return;
        }
        throw new NullPointerException("Null logRequests");
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof BatchedLogRequest && this.logRequests.equals(((BatchedLogRequest)o).getLogRequests()));
    }
    
    @Override
    public List<LogRequest> getLogRequests() {
        return this.logRequests;
    }
    
    @Override
    public int hashCode() {
        return this.logRequests.hashCode() ^ 0xF4243;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("BatchedLogRequest{logRequests=");
        sb.append(this.logRequests);
        sb.append("}");
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.cct.internal;

import java.util.Arrays;

final class AutoValue_LogEvent extends LogEvent
{
    private final Integer eventCode;
    private final long eventTimeMs;
    private final long eventUptimeMs;
    private final NetworkConnectionInfo networkConnectionInfo;
    private final byte[] sourceExtension;
    private final String sourceExtensionJsonProto3;
    private final long timezoneOffsetSeconds;
    
    private AutoValue_LogEvent(final long eventTimeMs, final Integer eventCode, final long eventUptimeMs, final byte[] sourceExtension, final String sourceExtensionJsonProto3, final long timezoneOffsetSeconds, final NetworkConnectionInfo networkConnectionInfo) {
        this.eventTimeMs = eventTimeMs;
        this.eventCode = eventCode;
        this.eventUptimeMs = eventUptimeMs;
        this.sourceExtension = sourceExtension;
        this.sourceExtensionJsonProto3 = sourceExtensionJsonProto3;
        this.timezoneOffsetSeconds = timezoneOffsetSeconds;
        this.networkConnectionInfo = networkConnectionInfo;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof LogEvent) {
            final LogEvent logEvent = (LogEvent)o;
            if (this.eventTimeMs == logEvent.getEventTimeMs()) {
                final Integer eventCode = this.eventCode;
                if (eventCode == null) {
                    if (logEvent.getEventCode() != null) {
                        return false;
                    }
                }
                else if (!eventCode.equals(logEvent.getEventCode())) {
                    return false;
                }
                if (this.eventUptimeMs == logEvent.getEventUptimeMs()) {
                    final byte[] sourceExtension = this.sourceExtension;
                    byte[] a2;
                    if (logEvent instanceof AutoValue_LogEvent) {
                        a2 = ((AutoValue_LogEvent)logEvent).sourceExtension;
                    }
                    else {
                        a2 = logEvent.getSourceExtension();
                    }
                    if (Arrays.equals(sourceExtension, a2)) {
                        final String sourceExtensionJsonProto3 = this.sourceExtensionJsonProto3;
                        if (sourceExtensionJsonProto3 == null) {
                            if (logEvent.getSourceExtensionJsonProto3() != null) {
                                return false;
                            }
                        }
                        else if (!sourceExtensionJsonProto3.equals(logEvent.getSourceExtensionJsonProto3())) {
                            return false;
                        }
                        if (this.timezoneOffsetSeconds == logEvent.getTimezoneOffsetSeconds()) {
                            final NetworkConnectionInfo networkConnectionInfo = this.networkConnectionInfo;
                            final NetworkConnectionInfo networkConnectionInfo2 = logEvent.getNetworkConnectionInfo();
                            if (networkConnectionInfo == null) {
                                if (networkConnectionInfo2 == null) {
                                    return b;
                                }
                            }
                            else if (networkConnectionInfo.equals(networkConnectionInfo2)) {
                                return b;
                            }
                        }
                    }
                }
            }
            b = false;
            return b;
        }
        return false;
    }
    
    @Override
    public Integer getEventCode() {
        return this.eventCode;
    }
    
    @Override
    public long getEventTimeMs() {
        return this.eventTimeMs;
    }
    
    @Override
    public long getEventUptimeMs() {
        return this.eventUptimeMs;
    }
    
    @Override
    public NetworkConnectionInfo getNetworkConnectionInfo() {
        return this.networkConnectionInfo;
    }
    
    @Override
    public byte[] getSourceExtension() {
        return this.sourceExtension;
    }
    
    @Override
    public String getSourceExtensionJsonProto3() {
        return this.sourceExtensionJsonProto3;
    }
    
    @Override
    public long getTimezoneOffsetSeconds() {
        return this.timezoneOffsetSeconds;
    }
    
    @Override
    public int hashCode() {
        final long eventTimeMs = this.eventTimeMs;
        final int n = (int)(eventTimeMs ^ eventTimeMs >>> 32);
        final Integer eventCode = this.eventCode;
        int hashCode = 0;
        int hashCode2;
        if (eventCode == null) {
            hashCode2 = 0;
        }
        else {
            hashCode2 = eventCode.hashCode();
        }
        final long eventUptimeMs = this.eventUptimeMs;
        final int n2 = (int)(eventUptimeMs ^ eventUptimeMs >>> 32);
        final int hashCode3 = Arrays.hashCode(this.sourceExtension);
        final String sourceExtensionJsonProto3 = this.sourceExtensionJsonProto3;
        int hashCode4;
        if (sourceExtensionJsonProto3 == null) {
            hashCode4 = 0;
        }
        else {
            hashCode4 = sourceExtensionJsonProto3.hashCode();
        }
        final long timezoneOffsetSeconds = this.timezoneOffsetSeconds;
        final int n3 = (int)(timezoneOffsetSeconds >>> 32 ^ timezoneOffsetSeconds);
        final NetworkConnectionInfo networkConnectionInfo = this.networkConnectionInfo;
        if (networkConnectionInfo != null) {
            hashCode = networkConnectionInfo.hashCode();
        }
        return ((((((n ^ 0xF4243) * 1000003 ^ hashCode2) * 1000003 ^ n2) * 1000003 ^ hashCode3) * 1000003 ^ hashCode4) * 1000003 ^ n3) * 1000003 ^ hashCode;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("LogEvent{eventTimeMs=");
        sb.append(this.eventTimeMs);
        sb.append(", eventCode=");
        sb.append(this.eventCode);
        sb.append(", eventUptimeMs=");
        sb.append(this.eventUptimeMs);
        sb.append(", sourceExtension=");
        sb.append(Arrays.toString(this.sourceExtension));
        sb.append(", sourceExtensionJsonProto3=");
        sb.append(this.sourceExtensionJsonProto3);
        sb.append(", timezoneOffsetSeconds=");
        sb.append(this.timezoneOffsetSeconds);
        sb.append(", networkConnectionInfo=");
        sb.append(this.networkConnectionInfo);
        sb.append("}");
        return sb.toString();
    }
    
    public static final class Builder extends LogEvent.Builder
    {
        private Integer eventCode;
        private Long eventTimeMs;
        private Long eventUptimeMs;
        private NetworkConnectionInfo networkConnectionInfo;
        private byte[] sourceExtension;
        private String sourceExtensionJsonProto3;
        private Long timezoneOffsetSeconds;
        
        @Override
        public LogEvent build() {
            final Long eventTimeMs = this.eventTimeMs;
            String string = "";
            if (eventTimeMs == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("");
                sb.append(" eventTimeMs");
                string = sb.toString();
            }
            String string2 = string;
            if (this.eventUptimeMs == null) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(string);
                sb2.append(" eventUptimeMs");
                string2 = sb2.toString();
            }
            String string3 = string2;
            if (this.timezoneOffsetSeconds == null) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(string2);
                sb3.append(" timezoneOffsetSeconds");
                string3 = sb3.toString();
            }
            if (string3.isEmpty()) {
                return new AutoValue_LogEvent(this.eventTimeMs, this.eventCode, this.eventUptimeMs, this.sourceExtension, this.sourceExtensionJsonProto3, this.timezoneOffsetSeconds, this.networkConnectionInfo, null);
            }
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("Missing required properties:");
            sb4.append(string3);
            throw new IllegalStateException(sb4.toString());
        }
        
        @Override
        public LogEvent.Builder setEventCode(final Integer eventCode) {
            this.eventCode = eventCode;
            return this;
        }
        
        @Override
        public LogEvent.Builder setEventTimeMs(final long l) {
            this.eventTimeMs = l;
            return this;
        }
        
        @Override
        public LogEvent.Builder setEventUptimeMs(final long l) {
            this.eventUptimeMs = l;
            return this;
        }
        
        @Override
        public LogEvent.Builder setNetworkConnectionInfo(final NetworkConnectionInfo networkConnectionInfo) {
            this.networkConnectionInfo = networkConnectionInfo;
            return this;
        }
        
        @Override
        public LogEvent.Builder setSourceExtension(final byte[] sourceExtension) {
            this.sourceExtension = sourceExtension;
            return this;
        }
        
        @Override
        public LogEvent.Builder setSourceExtensionJsonProto3(final String sourceExtensionJsonProto3) {
            this.sourceExtensionJsonProto3 = sourceExtensionJsonProto3;
            return this;
        }
        
        @Override
        public LogEvent.Builder setTimezoneOffsetSeconds(final long l) {
            this.timezoneOffsetSeconds = l;
            return this;
        }
    }
}

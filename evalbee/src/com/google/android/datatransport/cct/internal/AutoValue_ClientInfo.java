// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.cct.internal;

final class AutoValue_ClientInfo extends ClientInfo
{
    private final AndroidClientInfo androidClientInfo;
    private final ClientType clientType;
    
    private AutoValue_ClientInfo(final ClientType clientType, final AndroidClientInfo androidClientInfo) {
        this.clientType = clientType;
        this.androidClientInfo = androidClientInfo;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof ClientInfo) {
            final ClientInfo clientInfo = (ClientInfo)o;
            final ClientType clientType = this.clientType;
            if (clientType == null) {
                if (clientInfo.getClientType() != null) {
                    return false;
                }
            }
            else if (!clientType.equals(clientInfo.getClientType())) {
                return false;
            }
            final AndroidClientInfo androidClientInfo = this.androidClientInfo;
            final AndroidClientInfo androidClientInfo2 = clientInfo.getAndroidClientInfo();
            if (androidClientInfo == null) {
                if (androidClientInfo2 == null) {
                    return b;
                }
            }
            else if (androidClientInfo.equals(androidClientInfo2)) {
                return b;
            }
            b = false;
            return b;
        }
        return false;
    }
    
    @Override
    public AndroidClientInfo getAndroidClientInfo() {
        return this.androidClientInfo;
    }
    
    @Override
    public ClientType getClientType() {
        return this.clientType;
    }
    
    @Override
    public int hashCode() {
        final ClientType clientType = this.clientType;
        int hashCode = 0;
        int hashCode2;
        if (clientType == null) {
            hashCode2 = 0;
        }
        else {
            hashCode2 = clientType.hashCode();
        }
        final AndroidClientInfo androidClientInfo = this.androidClientInfo;
        if (androidClientInfo != null) {
            hashCode = androidClientInfo.hashCode();
        }
        return (hashCode2 ^ 0xF4243) * 1000003 ^ hashCode;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ClientInfo{clientType=");
        sb.append(this.clientType);
        sb.append(", androidClientInfo=");
        sb.append(this.androidClientInfo);
        sb.append("}");
        return sb.toString();
    }
    
    public static final class Builder extends ClientInfo.Builder
    {
        private AndroidClientInfo androidClientInfo;
        private ClientType clientType;
        
        @Override
        public ClientInfo build() {
            return new AutoValue_ClientInfo(this.clientType, this.androidClientInfo, null);
        }
        
        @Override
        public ClientInfo.Builder setAndroidClientInfo(final AndroidClientInfo androidClientInfo) {
            this.androidClientInfo = androidClientInfo;
            return this;
        }
        
        @Override
        public ClientInfo.Builder setClientType(final ClientType clientType) {
            this.clientType = clientType;
            return this;
        }
    }
}

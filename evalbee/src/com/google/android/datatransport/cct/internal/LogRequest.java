// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.cct.internal;

import com.google.auto.value.AutoValue$Builder;
import java.util.List;
import com.google.auto.value.AutoValue;

@AutoValue
public abstract class LogRequest
{
    public static Builder builder() {
        return (Builder)new AutoValue_LogRequest.Builder();
    }
    
    public abstract ClientInfo getClientInfo();
    
    public abstract List<LogEvent> getLogEvents();
    
    public abstract Integer getLogSource();
    
    public abstract String getLogSourceName();
    
    public abstract QosTier getQosTier();
    
    public abstract long getRequestTimeMs();
    
    public abstract long getRequestUptimeMs();
    
    @AutoValue$Builder
    public abstract static class Builder
    {
        public abstract LogRequest build();
        
        public abstract Builder setClientInfo(final ClientInfo p0);
        
        public abstract Builder setLogEvents(final List<LogEvent> p0);
        
        public abstract Builder setLogSource(final Integer p0);
        
        public abstract Builder setLogSourceName(final String p0);
        
        public abstract Builder setQosTier(final QosTier p0);
        
        public abstract Builder setRequestTimeMs(final long p0);
        
        public abstract Builder setRequestUptimeMs(final long p0);
        
        public Builder setSource(final int i) {
            return this.setLogSource(i);
        }
        
        public Builder setSource(final String logSourceName) {
            return this.setLogSourceName(logSourceName);
        }
    }
}

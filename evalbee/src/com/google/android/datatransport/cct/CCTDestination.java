// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.cct;

import java.util.regex.Pattern;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import com.google.android.datatransport.Encoding;
import java.util.Set;
import com.google.android.datatransport.runtime.EncodedDestination;

public final class CCTDestination implements EncodedDestination
{
    private static final String DEFAULT_API_KEY;
    static final String DEFAULT_END_POINT;
    static final String DESTINATION_NAME = "cct";
    private static final String EXTRAS_DELIMITER = "\\";
    private static final String EXTRAS_VERSION_MARKER = "1$";
    public static final CCTDestination INSTANCE;
    static final String LEGACY_END_POINT;
    public static final CCTDestination LEGACY_INSTANCE;
    private static final Set<Encoding> SUPPORTED_ENCODINGS;
    private final String apiKey;
    private final String endPoint;
    
    static {
        final String s = DEFAULT_END_POINT = StringMerger.mergeStrings("hts/frbslgiggolai.o/0clgbthfra=snpoo", "tp:/ieaeogn.ogepscmvc/o/ac?omtjo_rt3");
        final String s2 = LEGACY_END_POINT = StringMerger.mergeStrings("hts/frbslgigp.ogepscmv/ieo/eaybtho", "tp:/ieaeogn-agolai.o/1frlglgc/aclg");
        final String s3 = DEFAULT_API_KEY = StringMerger.mergeStrings("AzSCki82AwsLzKd5O8zo", "IayckHiZRO1EFl1aGoK");
        SUPPORTED_ENCODINGS = Collections.unmodifiableSet((Set<? extends Encoding>)new HashSet<Encoding>(Arrays.asList(Encoding.of("proto"), Encoding.of("json"))));
        INSTANCE = new CCTDestination(s, null);
        LEGACY_INSTANCE = new CCTDestination(s2, s3);
    }
    
    public CCTDestination(final String endPoint, final String apiKey) {
        this.endPoint = endPoint;
        this.apiKey = apiKey;
    }
    
    public static String decodeExtras(final byte[] bytes) {
        return new String(bytes, Charset.forName("UTF-8"));
    }
    
    public static byte[] encodeString(final String s) {
        return s.getBytes(Charset.forName("UTF-8"));
    }
    
    public static CCTDestination fromByteArray(final byte[] bytes) {
        final String s = new String(bytes, Charset.forName("UTF-8"));
        if (!s.startsWith("1$")) {
            throw new IllegalArgumentException("Version marker missing from extras");
        }
        final String[] split = s.substring(2).split(Pattern.quote("\\"), 2);
        if (split.length != 2) {
            throw new IllegalArgumentException("Extra is not a valid encoded LegacyFlgDestination");
        }
        final String s2 = split[0];
        if (!s2.isEmpty()) {
            String s3;
            if ((s3 = split[1]).isEmpty()) {
                s3 = null;
            }
            return new CCTDestination(s2, s3);
        }
        throw new IllegalArgumentException("Missing endpoint in CCTDestination extras");
    }
    
    public byte[] asByteArray() {
        final String apiKey = this.apiKey;
        if (apiKey == null && this.endPoint == null) {
            return null;
        }
        final String endPoint = this.endPoint;
        String s;
        if ((s = apiKey) == null) {
            s = "";
        }
        return String.format("%s%s%s%s", "1$", endPoint, "\\", s).getBytes(Charset.forName("UTF-8"));
    }
    
    public String getAPIKey() {
        return this.apiKey;
    }
    
    public String getEndPoint() {
        return this.endPoint;
    }
    
    @Override
    public byte[] getExtras() {
        return this.asByteArray();
    }
    
    @Override
    public String getName() {
        return "cct";
    }
    
    @Override
    public Set<Encoding> getSupportedEncodings() {
        return CCTDestination.SUPPORTED_ENCODINGS;
    }
}

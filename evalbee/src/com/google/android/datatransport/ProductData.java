// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class ProductData
{
    public static ProductData withProductId(final Integer n) {
        return new AutoValue_ProductData(n);
    }
    
    public abstract Integer getProductId();
}

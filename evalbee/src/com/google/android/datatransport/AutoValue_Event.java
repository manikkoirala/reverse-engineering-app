// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport;

final class AutoValue_Event<T> extends Event<T>
{
    private final Integer code;
    private final T payload;
    private final Priority priority;
    private final ProductData productData;
    
    public AutoValue_Event(final Integer code, final T payload, final Priority priority, final ProductData productData) {
        this.code = code;
        if (payload == null) {
            throw new NullPointerException("Null payload");
        }
        this.payload = payload;
        if (priority != null) {
            this.priority = priority;
            this.productData = productData;
            return;
        }
        throw new NullPointerException("Null priority");
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof Event) {
            final Event event = (Event)o;
            final Integer code = this.code;
            if (code == null) {
                if (event.getCode() != null) {
                    return false;
                }
            }
            else if (!code.equals(event.getCode())) {
                return false;
            }
            if (this.payload.equals(event.getPayload()) && this.priority.equals(event.getPriority())) {
                final ProductData productData = this.productData;
                final ProductData productData2 = event.getProductData();
                if (productData == null) {
                    if (productData2 == null) {
                        return b;
                    }
                }
                else if (productData.equals(productData2)) {
                    return b;
                }
            }
            b = false;
            return b;
        }
        return false;
    }
    
    @Override
    public Integer getCode() {
        return this.code;
    }
    
    @Override
    public T getPayload() {
        return this.payload;
    }
    
    @Override
    public Priority getPriority() {
        return this.priority;
    }
    
    @Override
    public ProductData getProductData() {
        return this.productData;
    }
    
    @Override
    public int hashCode() {
        final Integer code = this.code;
        int hashCode = 0;
        int hashCode2;
        if (code == null) {
            hashCode2 = 0;
        }
        else {
            hashCode2 = code.hashCode();
        }
        final int hashCode3 = this.payload.hashCode();
        final int hashCode4 = this.priority.hashCode();
        final ProductData productData = this.productData;
        if (productData != null) {
            hashCode = productData.hashCode();
        }
        return (((hashCode2 ^ 0xF4243) * 1000003 ^ hashCode3) * 1000003 ^ hashCode4) * 1000003 ^ hashCode;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Event{code=");
        sb.append(this.code);
        sb.append(", payload=");
        sb.append(this.payload);
        sb.append(", priority=");
        sb.append(this.priority);
        sb.append(", productData=");
        sb.append(this.productData);
        sb.append("}");
        return sb.toString();
    }
}

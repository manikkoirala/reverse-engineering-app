// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport;

final class AutoValue_ProductData extends ProductData
{
    private final Integer productId;
    
    public AutoValue_ProductData(final Integer productId) {
        this.productId = productId;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean equals = true;
        if (o == this) {
            return true;
        }
        if (o instanceof ProductData) {
            final ProductData productData = (ProductData)o;
            final Integer productId = this.productId;
            final Integer productId2 = productData.getProductId();
            if (productId == null) {
                if (productId2 != null) {
                    equals = false;
                }
            }
            else {
                equals = productId.equals(productId2);
            }
            return equals;
        }
        return false;
    }
    
    @Override
    public Integer getProductId() {
        return this.productId;
    }
    
    @Override
    public int hashCode() {
        final Integer productId = this.productId;
        int hashCode;
        if (productId == null) {
            hashCode = 0;
        }
        else {
            hashCode = productId.hashCode();
        }
        return hashCode ^ 0xF4243;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ProductData{productId=");
        sb.append(this.productId);
        sb.append("}");
        return sb.toString();
    }
}

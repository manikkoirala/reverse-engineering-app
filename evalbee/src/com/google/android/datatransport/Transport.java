// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport;

public interface Transport<T>
{
    void schedule(final Event<T> p0, final TransportScheduleCallback p1);
    
    void send(final Event<T> p0);
}

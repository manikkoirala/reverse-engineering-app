// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport;

public final class Encoding
{
    private final String name;
    
    private Encoding(final String name) {
        if (name != null) {
            this.name = name;
            return;
        }
        throw new NullPointerException("name is null");
    }
    
    public static Encoding of(final String s) {
        return new Encoding(s);
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof Encoding && this.name.equals(((Encoding)o).name));
    }
    
    public String getName() {
        return this.name;
    }
    
    @Override
    public int hashCode() {
        return this.name.hashCode() ^ 0xF4243;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Encoding{name=\"");
        sb.append(this.name);
        sb.append("\"}");
        return sb.toString();
    }
}

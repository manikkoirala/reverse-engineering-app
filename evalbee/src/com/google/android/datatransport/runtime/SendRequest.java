// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime;

import com.google.auto.value.AutoValue$Builder;
import com.google.android.datatransport.Transformer;
import com.google.android.datatransport.Event;
import com.google.android.datatransport.Encoding;
import com.google.auto.value.AutoValue;

@AutoValue
abstract class SendRequest
{
    public SendRequest() {
    }
    
    public static Builder builder() {
        return (Builder)new AutoValue_SendRequest.Builder();
    }
    
    public abstract Encoding getEncoding();
    
    public abstract Event<?> getEvent();
    
    public byte[] getPayload() {
        return this.getTransformer().apply(this.getEvent().getPayload());
    }
    
    public abstract Transformer<?, byte[]> getTransformer();
    
    public abstract TransportContext getTransportContext();
    
    public abstract String getTransportName();
    
    @AutoValue$Builder
    public abstract static class Builder
    {
        public abstract SendRequest build();
        
        public abstract Builder setEncoding(final Encoding p0);
        
        public abstract Builder setEvent(final Event<?> p0);
        
        public <T> Builder setEvent(final Event<T> event, final Encoding encoding, final Transformer<T, byte[]> transformer) {
            this.setEvent(event);
            this.setEncoding(encoding);
            this.setTransformer(transformer);
            return this;
        }
        
        public abstract Builder setTransformer(final Transformer<?, byte[]> p0);
        
        public abstract Builder setTransportContext(final TransportContext p0);
        
        public abstract Builder setTransportName(final String p0);
    }
}

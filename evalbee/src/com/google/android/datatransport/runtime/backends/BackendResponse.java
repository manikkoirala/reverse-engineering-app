// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.backends;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class BackendResponse
{
    public static BackendResponse fatalError() {
        return new AutoValue_BackendResponse(Status.FATAL_ERROR, -1L);
    }
    
    public static BackendResponse invalidPayload() {
        return new AutoValue_BackendResponse(Status.INVALID_PAYLOAD, -1L);
    }
    
    public static BackendResponse ok(final long n) {
        return new AutoValue_BackendResponse(Status.OK, n);
    }
    
    public static BackendResponse transientError() {
        return new AutoValue_BackendResponse(Status.TRANSIENT_ERROR, -1L);
    }
    
    public abstract long getNextRequestWaitMillis();
    
    public abstract Status getStatus();
    
    public enum Status
    {
        private static final Status[] $VALUES;
        
        FATAL_ERROR, 
        INVALID_PAYLOAD, 
        OK, 
        TRANSIENT_ERROR;
    }
}

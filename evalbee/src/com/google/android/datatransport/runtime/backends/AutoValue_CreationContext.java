// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.backends;

import com.google.android.datatransport.runtime.time.Clock;
import android.content.Context;

final class AutoValue_CreationContext extends CreationContext
{
    private final Context applicationContext;
    private final String backendName;
    private final Clock monotonicClock;
    private final Clock wallClock;
    
    public AutoValue_CreationContext(final Context applicationContext, final Clock wallClock, final Clock monotonicClock, final String backendName) {
        if (applicationContext == null) {
            throw new NullPointerException("Null applicationContext");
        }
        this.applicationContext = applicationContext;
        if (wallClock == null) {
            throw new NullPointerException("Null wallClock");
        }
        this.wallClock = wallClock;
        if (monotonicClock == null) {
            throw new NullPointerException("Null monotonicClock");
        }
        this.monotonicClock = monotonicClock;
        if (backendName != null) {
            this.backendName = backendName;
            return;
        }
        throw new NullPointerException("Null backendName");
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof CreationContext) {
            final CreationContext creationContext = (CreationContext)o;
            if (!this.applicationContext.equals(creationContext.getApplicationContext()) || !this.wallClock.equals(creationContext.getWallClock()) || !this.monotonicClock.equals(creationContext.getMonotonicClock()) || !this.backendName.equals(creationContext.getBackendName())) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public Context getApplicationContext() {
        return this.applicationContext;
    }
    
    @Override
    public String getBackendName() {
        return this.backendName;
    }
    
    @Override
    public Clock getMonotonicClock() {
        return this.monotonicClock;
    }
    
    @Override
    public Clock getWallClock() {
        return this.wallClock;
    }
    
    @Override
    public int hashCode() {
        return (((this.applicationContext.hashCode() ^ 0xF4243) * 1000003 ^ this.wallClock.hashCode()) * 1000003 ^ this.monotonicClock.hashCode()) * 1000003 ^ this.backendName.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("CreationContext{applicationContext=");
        sb.append(this.applicationContext);
        sb.append(", wallClock=");
        sb.append(this.wallClock);
        sb.append(", monotonicClock=");
        sb.append(this.monotonicClock);
        sb.append(", backendName=");
        sb.append(this.backendName);
        sb.append("}");
        return sb.toString();
    }
}

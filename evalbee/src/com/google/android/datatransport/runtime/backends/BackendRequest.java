// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.backends;

import com.google.auto.value.AutoValue$Builder;
import com.google.android.datatransport.runtime.EventInternal;
import com.google.auto.value.AutoValue;

@AutoValue
public abstract class BackendRequest
{
    public static Builder builder() {
        return (Builder)new AutoValue_BackendRequest.Builder();
    }
    
    public static BackendRequest create(final Iterable<EventInternal> events) {
        return builder().setEvents(events).build();
    }
    
    public abstract Iterable<EventInternal> getEvents();
    
    public abstract byte[] getExtras();
    
    @AutoValue$Builder
    public abstract static class Builder
    {
        public abstract BackendRequest build();
        
        public abstract Builder setEvents(final Iterable<EventInternal> p0);
        
        public abstract Builder setExtras(final byte[] p0);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.backends;

import com.google.android.datatransport.runtime.time.Clock;
import android.content.Context;
import com.google.android.datatransport.runtime.dagger.internal.Factory;

public final class CreationContextFactory_Factory implements Factory<CreationContextFactory>
{
    private final q91 applicationContextProvider;
    private final q91 monotonicClockProvider;
    private final q91 wallClockProvider;
    
    public CreationContextFactory_Factory(final q91 applicationContextProvider, final q91 wallClockProvider, final q91 monotonicClockProvider) {
        this.applicationContextProvider = applicationContextProvider;
        this.wallClockProvider = wallClockProvider;
        this.monotonicClockProvider = monotonicClockProvider;
    }
    
    public static CreationContextFactory_Factory create(final q91 q91, final q91 q92, final q91 q93) {
        return new CreationContextFactory_Factory(q91, q92, q93);
    }
    
    public static CreationContextFactory newInstance(final Context context, final Clock clock, final Clock clock2) {
        return new CreationContextFactory(context, clock, clock2);
    }
    
    @Override
    public CreationContextFactory get() {
        return newInstance((Context)this.applicationContextProvider.get(), (Clock)this.wallClockProvider.get(), (Clock)this.monotonicClockProvider.get());
    }
}

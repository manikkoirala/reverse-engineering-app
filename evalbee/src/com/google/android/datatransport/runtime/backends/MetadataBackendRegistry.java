// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.backends;

import android.os.BaseBundle;
import java.lang.reflect.InvocationTargetException;
import android.content.pm.ServiceInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager$NameNotFoundException;
import android.content.ComponentName;
import java.util.Iterator;
import android.os.Bundle;
import java.util.Collections;
import android.util.Log;
import java.util.HashMap;
import android.content.Context;
import java.util.Map;

class MetadataBackendRegistry implements BackendRegistry
{
    private static final String BACKEND_KEY_PREFIX = "backend:";
    private static final String TAG = "BackendRegistry";
    private final BackendFactoryProvider backendFactoryProvider;
    private final Map<String, TransportBackend> backends;
    private final CreationContextFactory creationContextFactory;
    
    public MetadataBackendRegistry(final Context context, final CreationContextFactory creationContextFactory) {
        this(new BackendFactoryProvider(context), creationContextFactory);
    }
    
    public MetadataBackendRegistry(final BackendFactoryProvider backendFactoryProvider, final CreationContextFactory creationContextFactory) {
        this.backends = new HashMap<String, TransportBackend>();
        this.backendFactoryProvider = backendFactoryProvider;
        this.creationContextFactory = creationContextFactory;
    }
    
    @Override
    public TransportBackend get(final String s) {
        synchronized (this) {
            if (this.backends.containsKey(s)) {
                return this.backends.get(s);
            }
            final BackendFactory value = this.backendFactoryProvider.get(s);
            if (value == null) {
                return null;
            }
            final TransportBackend create = value.create(this.creationContextFactory.create(s));
            this.backends.put(s, create);
            return create;
        }
    }
    
    public static class BackendFactoryProvider
    {
        private final Context applicationContext;
        private Map<String, String> backendProviders;
        
        public BackendFactoryProvider(final Context applicationContext) {
            this.backendProviders = null;
            this.applicationContext = applicationContext;
        }
        
        private Map<String, String> discover(final Context context) {
            final Bundle metadata = getMetadata(context);
            if (metadata == null) {
                Log.w("BackendRegistry", "Could not retrieve metadata, returning empty list of transport backends.");
                return Collections.emptyMap();
            }
            final HashMap hashMap = new HashMap();
            for (final String s : ((BaseBundle)metadata).keySet()) {
                final Object value = ((BaseBundle)metadata).get(s);
                if (value instanceof String && s.startsWith("backend:")) {
                    final String[] split = ((String)value).split(",", -1);
                    for (int length = split.length, i = 0; i < length; ++i) {
                        final String trim = split[i].trim();
                        if (!trim.isEmpty()) {
                            hashMap.put(trim, s.substring(8));
                        }
                    }
                }
            }
            return hashMap;
        }
        
        private Map<String, String> getBackendProviders() {
            if (this.backendProviders == null) {
                this.backendProviders = this.discover(this.applicationContext);
            }
            return this.backendProviders;
        }
        
        private static Bundle getMetadata(final Context context) {
            try {
                final PackageManager packageManager = context.getPackageManager();
                if (packageManager == null) {
                    Log.w("BackendRegistry", "Context has no PackageManager.");
                    return null;
                }
                final ServiceInfo serviceInfo = packageManager.getServiceInfo(new ComponentName(context, (Class)TransportBackendDiscovery.class), 128);
                if (serviceInfo == null) {
                    Log.w("BackendRegistry", "TransportBackendDiscovery has no service info.");
                    return null;
                }
                return serviceInfo.metaData;
            }
            catch (final PackageManager$NameNotFoundException ex) {
                Log.w("BackendRegistry", "Application info not found.");
                return null;
            }
        }
        
        public BackendFactory get(final String s) {
            final String className = this.getBackendProviders().get(s);
            if (className == null) {
                return null;
            }
            String s2 = null;
            try {
                return (BackendFactory)Class.forName(className).asSubclass(BackendFactory.class).getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
            }
            catch (final InvocationTargetException ex) {
                String.format("Could not instantiate %s", className);
                goto Label_0082;
            }
            catch (final NoSuchMethodException ex) {
                String.format("Could not instantiate %s", className);
            }
            catch (final InstantiationException ex2) {
                s2 = String.format("Could not instantiate %s.", className);
            }
            catch (final IllegalAccessException ex2) {
                s2 = String.format("Could not instantiate %s.", className);
            }
            catch (final ClassNotFoundException ex2) {
                s2 = String.format("Class %s is not found.", className);
            }
            final InstantiationException ex2;
            Log.w("BackendRegistry", s2, (Throwable)ex2);
            return null;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.backends;

final class AutoValue_BackendResponse extends BackendResponse
{
    private final long nextRequestWaitMillis;
    private final Status status;
    
    public AutoValue_BackendResponse(final Status status, final long nextRequestWaitMillis) {
        if (status != null) {
            this.status = status;
            this.nextRequestWaitMillis = nextRequestWaitMillis;
            return;
        }
        throw new NullPointerException("Null status");
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof BackendResponse) {
            final BackendResponse backendResponse = (BackendResponse)o;
            if (!this.status.equals(backendResponse.getStatus()) || this.nextRequestWaitMillis != backendResponse.getNextRequestWaitMillis()) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public long getNextRequestWaitMillis() {
        return this.nextRequestWaitMillis;
    }
    
    @Override
    public Status getStatus() {
        return this.status;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.status.hashCode();
        final long nextRequestWaitMillis = this.nextRequestWaitMillis;
        return (hashCode ^ 0xF4243) * 1000003 ^ (int)(nextRequestWaitMillis ^ nextRequestWaitMillis >>> 32);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("BackendResponse{status=");
        sb.append(this.status);
        sb.append(", nextRequestWaitMillis=");
        sb.append(this.nextRequestWaitMillis);
        sb.append("}");
        return sb.toString();
    }
}

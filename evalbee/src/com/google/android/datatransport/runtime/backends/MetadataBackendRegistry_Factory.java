// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.backends;

import android.content.Context;
import com.google.android.datatransport.runtime.dagger.internal.Factory;

public final class MetadataBackendRegistry_Factory implements Factory<MetadataBackendRegistry>
{
    private final q91 applicationContextProvider;
    private final q91 creationContextFactoryProvider;
    
    public MetadataBackendRegistry_Factory(final q91 applicationContextProvider, final q91 creationContextFactoryProvider) {
        this.applicationContextProvider = applicationContextProvider;
        this.creationContextFactoryProvider = creationContextFactoryProvider;
    }
    
    public static MetadataBackendRegistry_Factory create(final q91 q91, final q91 q92) {
        return new MetadataBackendRegistry_Factory(q91, q92);
    }
    
    public static MetadataBackendRegistry newInstance(final Context context, final Object o) {
        return new MetadataBackendRegistry(context, (CreationContextFactory)o);
    }
    
    @Override
    public MetadataBackendRegistry get() {
        return newInstance((Context)this.applicationContextProvider.get(), this.creationContextFactoryProvider.get());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.backends;

import java.util.Arrays;
import com.google.android.datatransport.runtime.EventInternal;

final class AutoValue_BackendRequest extends BackendRequest
{
    private final Iterable<EventInternal> events;
    private final byte[] extras;
    
    private AutoValue_BackendRequest(final Iterable<EventInternal> events, final byte[] extras) {
        this.events = events;
        this.extras = extras;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof BackendRequest) {
            final BackendRequest backendRequest = (BackendRequest)o;
            if (this.events.equals(backendRequest.getEvents())) {
                final byte[] extras = this.extras;
                byte[] a2;
                if (backendRequest instanceof AutoValue_BackendRequest) {
                    a2 = ((AutoValue_BackendRequest)backendRequest).extras;
                }
                else {
                    a2 = backendRequest.getExtras();
                }
                if (Arrays.equals(extras, a2)) {
                    return b;
                }
            }
            b = false;
            return b;
        }
        return false;
    }
    
    @Override
    public Iterable<EventInternal> getEvents() {
        return this.events;
    }
    
    @Override
    public byte[] getExtras() {
        return this.extras;
    }
    
    @Override
    public int hashCode() {
        return (this.events.hashCode() ^ 0xF4243) * 1000003 ^ Arrays.hashCode(this.extras);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("BackendRequest{events=");
        sb.append(this.events);
        sb.append(", extras=");
        sb.append(Arrays.toString(this.extras));
        sb.append("}");
        return sb.toString();
    }
    
    public static final class Builder extends BackendRequest.Builder
    {
        private Iterable<EventInternal> events;
        private byte[] extras;
        
        @Override
        public BackendRequest build() {
            final Iterable<EventInternal> events = this.events;
            String string = "";
            if (events == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("");
                sb.append(" events");
                string = sb.toString();
            }
            if (string.isEmpty()) {
                return new AutoValue_BackendRequest(this.events, this.extras, null);
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Missing required properties:");
            sb2.append(string);
            throw new IllegalStateException(sb2.toString());
        }
        
        @Override
        public BackendRequest.Builder setEvents(final Iterable<EventInternal> events) {
            if (events != null) {
                this.events = events;
                return this;
            }
            throw new NullPointerException("Null events");
        }
        
        @Override
        public BackendRequest.Builder setExtras(final byte[] extras) {
            this.extras = extras;
            return this;
        }
    }
}

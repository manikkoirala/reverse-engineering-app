// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.backends;

import com.google.android.datatransport.runtime.time.Monotonic;
import com.google.android.datatransport.runtime.time.WallTime;
import com.google.android.datatransport.runtime.time.Clock;
import android.content.Context;

class CreationContextFactory
{
    private final Context applicationContext;
    private final Clock monotonicClock;
    private final Clock wallClock;
    
    public CreationContextFactory(final Context applicationContext, @WallTime final Clock wallClock, @Monotonic final Clock monotonicClock) {
        this.applicationContext = applicationContext;
        this.wallClock = wallClock;
        this.monotonicClock = monotonicClock;
    }
    
    public CreationContext create(final String s) {
        return CreationContext.create(this.applicationContext, this.wallClock, this.monotonicClock, s);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.backends;

import com.google.android.datatransport.runtime.time.Clock;
import android.content.Context;
import com.google.auto.value.AutoValue;

@AutoValue
public abstract class CreationContext
{
    private static final String DEFAULT_BACKEND_NAME = "cct";
    
    public static CreationContext create(final Context context, final Clock clock, final Clock clock2) {
        return new AutoValue_CreationContext(context, clock, clock2, "cct");
    }
    
    public static CreationContext create(final Context context, final Clock clock, final Clock clock2, final String s) {
        return new AutoValue_CreationContext(context, clock, clock2, s);
    }
    
    public abstract Context getApplicationContext();
    
    public abstract String getBackendName();
    
    public abstract Clock getMonotonicClock();
    
    public abstract Clock getWallClock();
}

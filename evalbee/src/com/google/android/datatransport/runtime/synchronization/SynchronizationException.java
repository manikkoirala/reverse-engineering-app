// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.synchronization;

public class SynchronizationException extends RuntimeException
{
    public SynchronizationException(final String message, final Throwable cause) {
        super(message, cause);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.scheduling.persistence;

import com.google.android.datatransport.runtime.EventInternal;
import com.google.android.datatransport.runtime.TransportContext;
import java.io.Closeable;

public interface EventStore extends Closeable
{
    int cleanUp();
    
    long getNextCallTime(final TransportContext p0);
    
    boolean hasPendingEventsFor(final TransportContext p0);
    
    Iterable<TransportContext> loadActiveContexts();
    
    Iterable<PersistedEvent> loadBatch(final TransportContext p0);
    
    PersistedEvent persist(final TransportContext p0, final EventInternal p1);
    
    void recordFailure(final Iterable<PersistedEvent> p0);
    
    void recordNextCallTime(final TransportContext p0, final long p1);
    
    void recordSuccess(final Iterable<PersistedEvent> p0);
}

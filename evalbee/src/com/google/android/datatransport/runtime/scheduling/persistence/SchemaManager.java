// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.scheduling.persistence;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase$CursorFactory;
import android.content.Context;
import java.util.Arrays;
import java.util.List;
import android.database.sqlite.SQLiteOpenHelper;

final class SchemaManager extends SQLiteOpenHelper
{
    private static final String CREATE_CONTEXTS_SQL_V1 = "CREATE TABLE transport_contexts (_id INTEGER PRIMARY KEY, backend_name TEXT NOT NULL, priority INTEGER NOT NULL, next_request_ms INTEGER NOT NULL)";
    private static final String CREATE_CONTEXT_BACKEND_PRIORITY_INDEX_V1 = "CREATE UNIQUE INDEX contexts_backend_priority on transport_contexts(backend_name, priority)";
    private static final String CREATE_EVENTS_SQL_V1 = "CREATE TABLE events (_id INTEGER PRIMARY KEY, context_id INTEGER NOT NULL, transport_name TEXT NOT NULL, timestamp_ms INTEGER NOT NULL, uptime_ms INTEGER NOT NULL, payload BLOB NOT NULL, code INTEGER, num_attempts INTEGER NOT NULL,FOREIGN KEY (context_id) REFERENCES transport_contexts(_id) ON DELETE CASCADE)";
    private static final String CREATE_EVENT_BACKEND_INDEX_V1 = "CREATE INDEX events_backend_id on events(context_id)";
    private static final String CREATE_EVENT_METADATA_SQL_V1 = "CREATE TABLE event_metadata (_id INTEGER PRIMARY KEY, event_id INTEGER NOT NULL, name TEXT NOT NULL, value TEXT NOT NULL,FOREIGN KEY (event_id) REFERENCES events(_id) ON DELETE CASCADE)";
    private static final String CREATE_GLOBAL_LOG_EVENT_STATE_TABLE = "CREATE TABLE global_log_event_state (last_metrics_upload_ms BIGINT PRIMARY KEY)";
    private static final String CREATE_INITIAL_GLOBAL_LOG_EVENT_STATE_VALUE_SQL;
    private static final String CREATE_LOG_EVENT_DROPPED_TABLE = "CREATE TABLE log_event_dropped (log_source VARCHAR(45) NOT NULL,reason INTEGER NOT NULL,events_dropped_count BIGINT NOT NULL,PRIMARY KEY(log_source, reason))";
    private static final String CREATE_PAYLOADS_TABLE_V4 = "CREATE TABLE event_payloads (sequence_num INTEGER NOT NULL, event_id INTEGER NOT NULL, bytes BLOB NOT NULL,FOREIGN KEY (event_id) REFERENCES events(_id) ON DELETE CASCADE,PRIMARY KEY (sequence_num, event_id))";
    static final String DB_NAME = "com.google.android.datatransport.events";
    private static final String DROP_CONTEXTS_SQL = "DROP TABLE transport_contexts";
    private static final String DROP_EVENTS_SQL = "DROP TABLE events";
    private static final String DROP_EVENT_METADATA_SQL = "DROP TABLE event_metadata";
    private static final String DROP_GLOBAL_LOG_EVENT_STATE_SQL = "DROP TABLE IF EXISTS global_log_event_state";
    private static final String DROP_LOG_EVENT_DROPPED_SQL = "DROP TABLE IF EXISTS log_event_dropped";
    private static final String DROP_PAYLOADS_SQL = "DROP TABLE IF EXISTS event_payloads";
    private static final List<Migration> INCREMENTAL_MIGRATIONS;
    private static final Migration MIGRATE_TO_V1;
    private static final Migration MIGRATE_TO_V2;
    private static final Migration MIGRATE_TO_V3;
    private static final Migration MIGRATE_TO_V4;
    private static final Migration MIGRATION_TO_V5;
    static int SCHEMA_VERSION;
    private boolean configured;
    private final int schemaVersion;
    
    static {
        final StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO global_log_event_state VALUES (");
        sb.append(System.currentTimeMillis());
        sb.append(")");
        CREATE_INITIAL_GLOBAL_LOG_EVENT_STATE_VALUE_SQL = sb.toString();
        SchemaManager.SCHEMA_VERSION = 5;
        INCREMENTAL_MIGRATIONS = Arrays.asList(MIGRATE_TO_V1 = (Migration)new c0(), MIGRATE_TO_V2 = (Migration)new d0(), MIGRATE_TO_V3 = (Migration)new e0(), MIGRATE_TO_V4 = (Migration)new f0(), MIGRATION_TO_V5 = (Migration)new g0());
    }
    
    public SchemaManager(final Context context, final String s, final int schemaVersion) {
        super(context, s, (SQLiteDatabase$CursorFactory)null, schemaVersion);
        this.configured = false;
        this.schemaVersion = schemaVersion;
    }
    
    private void ensureConfigured(final SQLiteDatabase sqLiteDatabase) {
        if (!this.configured) {
            this.onConfigure(sqLiteDatabase);
        }
    }
    
    private void onCreate(final SQLiteDatabase sqLiteDatabase, final int n) {
        this.ensureConfigured(sqLiteDatabase);
        this.upgrade(sqLiteDatabase, 0, n);
    }
    
    private void upgrade(final SQLiteDatabase sqLiteDatabase, int i, final int j) {
        final List<Migration> incremental_MIGRATIONS = SchemaManager.INCREMENTAL_MIGRATIONS;
        if (j <= incremental_MIGRATIONS.size()) {
            while (i < j) {
                SchemaManager.INCREMENTAL_MIGRATIONS.get(i).upgrade(sqLiteDatabase);
                ++i;
            }
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Migration from ");
        sb.append(i);
        sb.append(" to ");
        sb.append(j);
        sb.append(" was requested, but cannot be performed. Only ");
        sb.append(incremental_MIGRATIONS.size());
        sb.append(" migrations are provided");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public void onConfigure(final SQLiteDatabase sqLiteDatabase) {
        this.configured = true;
        sqLiteDatabase.rawQuery("PRAGMA busy_timeout=0;", new String[0]).close();
        sqLiteDatabase.setForeignKeyConstraintsEnabled(true);
    }
    
    public void onCreate(final SQLiteDatabase sqLiteDatabase) {
        this.onCreate(sqLiteDatabase, this.schemaVersion);
    }
    
    public void onDowngrade(final SQLiteDatabase sqLiteDatabase, final int n, final int n2) {
        sqLiteDatabase.execSQL("DROP TABLE events");
        sqLiteDatabase.execSQL("DROP TABLE event_metadata");
        sqLiteDatabase.execSQL("DROP TABLE transport_contexts");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS event_payloads");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS log_event_dropped");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS global_log_event_state");
        this.onCreate(sqLiteDatabase, n2);
    }
    
    public void onOpen(final SQLiteDatabase sqLiteDatabase) {
        this.ensureConfigured(sqLiteDatabase);
    }
    
    public void onUpgrade(final SQLiteDatabase sqLiteDatabase, final int n, final int n2) {
        this.ensureConfigured(sqLiteDatabase);
        this.upgrade(sqLiteDatabase, n, n2);
    }
    
    public interface Migration
    {
        void upgrade(final SQLiteDatabase p0);
    }
}

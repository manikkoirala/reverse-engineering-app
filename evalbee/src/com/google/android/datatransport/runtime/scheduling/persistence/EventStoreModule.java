// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.scheduling.persistence;

import com.google.android.datatransport.runtime.synchronization.SynchronizationGuard;
import com.google.android.datatransport.runtime.dagger.Binds;
import android.content.Context;
import com.google.android.datatransport.runtime.dagger.Provides;
import com.google.android.datatransport.runtime.dagger.Module;

@Module
public abstract class EventStoreModule
{
    @Provides
    public static String dbName() {
        return "com.google.android.datatransport.events";
    }
    
    @Provides
    public static String packageName(final Context context) {
        return context.getPackageName();
    }
    
    @Provides
    public static int schemaVersion() {
        return SchemaManager.SCHEMA_VERSION;
    }
    
    @Provides
    public static EventStoreConfig storeConfig() {
        return EventStoreConfig.DEFAULT;
    }
    
    @Binds
    public abstract ClientHealthMetricsStore clientHealthMetricsStore(final SQLiteEventStore p0);
    
    @Binds
    public abstract EventStore eventStore(final SQLiteEventStore p0);
    
    @Binds
    public abstract SynchronizationGuard synchronizationGuard(final SQLiteEventStore p0);
}

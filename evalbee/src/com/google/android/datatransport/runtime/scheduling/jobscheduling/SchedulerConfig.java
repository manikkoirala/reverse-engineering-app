// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.scheduling.jobscheduling;

import com.google.auto.value.AutoValue$Builder;
import java.util.HashMap;
import android.app.job.JobInfo$Builder;
import java.util.Collections;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import java.util.Set;
import com.google.android.datatransport.Priority;
import java.util.Map;
import com.google.android.datatransport.runtime.time.Clock;
import com.google.auto.value.AutoValue;

@AutoValue
public abstract class SchedulerConfig
{
    private static final long BACKOFF_LOG_BASE = 10000L;
    private static final long ONE_SECOND = 1000L;
    private static final long THIRTY_SECONDS = 30000L;
    private static final long TWENTY_FOUR_HOURS = 86400000L;
    
    private long adjustedExponentialBackoff(int n, final long n2) {
        --n;
        long n3;
        if (n2 > 1L) {
            n3 = n2;
        }
        else {
            n3 = 2L;
        }
        return (long)(Math.pow(3.0, n) * n2 * Math.max(1.0, Math.log(10000.0) / Math.log((double)(n3 * n))));
    }
    
    public static Builder builder() {
        return new Builder();
    }
    
    public static SchedulerConfig create(final Clock clock, final Map<Priority, ConfigValue> map) {
        return new AutoValue_SchedulerConfig(clock, map);
    }
    
    public static SchedulerConfig getDefault(final Clock clock) {
        return builder().addConfig(Priority.DEFAULT, ConfigValue.builder().setDelta(30000L).setMaxAllowedDelay(86400000L).build()).addConfig(Priority.HIGHEST, ConfigValue.builder().setDelta(1000L).setMaxAllowedDelay(86400000L).build()).addConfig(Priority.VERY_LOW, ConfigValue.builder().setDelta(86400000L).setMaxAllowedDelay(86400000L).setFlags(immutableSetOf(Flag.DEVICE_IDLE)).build()).setClock(clock).build();
    }
    
    private static <T> Set<T> immutableSetOf(final T... a) {
        return Collections.unmodifiableSet((Set<? extends T>)new HashSet<T>((Collection<? extends T>)Arrays.asList(a)));
    }
    
    private void populateFlags(final JobInfo$Builder jobInfo$Builder, final Set<Flag> set) {
        if (set.contains(Flag.NETWORK_UNMETERED)) {
            jobInfo$Builder.setRequiredNetworkType(2);
        }
        else {
            jobInfo$Builder.setRequiredNetworkType(1);
        }
        if (set.contains(Flag.DEVICE_CHARGING)) {
            jobInfo$Builder.setRequiresCharging(true);
        }
        if (set.contains(Flag.DEVICE_IDLE)) {
            jobInfo$Builder.setRequiresDeviceIdle(true);
        }
    }
    
    public JobInfo$Builder configureJob(final JobInfo$Builder jobInfo$Builder, final Priority priority, final long n, final int n2) {
        jobInfo$Builder.setMinimumLatency(this.getScheduleDelay(priority, n, n2));
        this.populateFlags(jobInfo$Builder, this.getValues().get(priority).getFlags());
        return jobInfo$Builder;
    }
    
    public abstract Clock getClock();
    
    public Set<Flag> getFlags(final Priority priority) {
        return this.getValues().get(priority).getFlags();
    }
    
    public long getScheduleDelay(final Priority priority, final long n, final int n2) {
        final long time = this.getClock().getTime();
        final ConfigValue configValue = this.getValues().get(priority);
        return Math.min(Math.max(this.adjustedExponentialBackoff(n2, configValue.getDelta()), n - time), configValue.getMaxAllowedDelay());
    }
    
    public abstract Map<Priority, ConfigValue> getValues();
    
    public static class Builder
    {
        private Clock clock;
        private Map<Priority, ConfigValue> values;
        
        public Builder() {
            this.values = new HashMap<Priority, ConfigValue>();
        }
        
        public Builder addConfig(final Priority priority, final ConfigValue configValue) {
            this.values.put(priority, configValue);
            return this;
        }
        
        public SchedulerConfig build() {
            if (this.clock == null) {
                throw new NullPointerException("missing required property: clock");
            }
            if (this.values.keySet().size() >= Priority.values().length) {
                final Map<Priority, ConfigValue> values = this.values;
                this.values = new HashMap<Priority, ConfigValue>();
                return SchedulerConfig.create(this.clock, values);
            }
            throw new IllegalStateException("Not all priorities have been configured");
        }
        
        public Builder setClock(final Clock clock) {
            this.clock = clock;
            return this;
        }
    }
    
    @AutoValue
    public abstract static class ConfigValue
    {
        public static Builder builder() {
            return new AutoValue_SchedulerConfig_ConfigValue.Builder().setFlags(Collections.emptySet());
        }
        
        public abstract long getDelta();
        
        public abstract Set<Flag> getFlags();
        
        public abstract long getMaxAllowedDelay();
        
        @AutoValue$Builder
        public abstract static class Builder
        {
            public abstract ConfigValue build();
            
            public abstract Builder setDelta(final long p0);
            
            public abstract Builder setFlags(final Set<Flag> p0);
            
            public abstract Builder setMaxAllowedDelay(final long p0);
        }
    }
    
    public enum Flag
    {
        private static final Flag[] $VALUES;
        
        DEVICE_CHARGING, 
        DEVICE_IDLE, 
        NETWORK_UNMETERED;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.scheduling.jobscheduling;

import android.os.BaseBundle;
import android.util.Base64;
import android.os.PersistableBundle;
import android.app.job.JobInfo$Builder;
import com.google.android.datatransport.runtime.logging.Logging;
import android.content.ComponentName;
import com.google.android.datatransport.runtime.util.PriorityMapping;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.zip.Adler32;
import com.google.android.datatransport.runtime.TransportContext;
import java.util.Iterator;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import com.google.android.datatransport.runtime.scheduling.persistence.EventStore;
import android.content.Context;

public class JobInfoScheduler implements WorkScheduler
{
    static final String ATTEMPT_NUMBER = "attemptNumber";
    static final String BACKEND_NAME = "backendName";
    static final String EVENT_PRIORITY = "priority";
    static final String EXTRAS = "extras";
    private static final String LOG_TAG = "JobInfoScheduler";
    private final SchedulerConfig config;
    private final Context context;
    private final EventStore eventStore;
    
    public JobInfoScheduler(final Context context, final EventStore eventStore, final SchedulerConfig config) {
        this.context = context;
        this.eventStore = eventStore;
        this.config = config;
    }
    
    private boolean isJobServiceOn(final JobScheduler jobScheduler, final int n, final int n2) {
        final Iterator iterator = jobScheduler.getAllPendingJobs().iterator();
        JobInfo jobInfo;
        boolean b;
        int int1;
        do {
            final boolean hasNext = iterator.hasNext();
            b = false;
            if (!hasNext) {
                return b;
            }
            jobInfo = (JobInfo)iterator.next();
            int1 = ((BaseBundle)jobInfo.getExtras()).getInt("attemptNumber");
        } while (jobInfo.getId() != n);
        b = b;
        if (int1 >= n2) {
            b = true;
            return b;
        }
        return b;
    }
    
    public int getJobId(final TransportContext transportContext) {
        final Adler32 adler32 = new Adler32();
        adler32.update(this.context.getPackageName().getBytes(Charset.forName("UTF-8")));
        adler32.update(transportContext.getBackendName().getBytes(Charset.forName("UTF-8")));
        adler32.update(ByteBuffer.allocate(4).putInt(PriorityMapping.toInt(transportContext.getPriority())).array());
        if (transportContext.getExtras() != null) {
            adler32.update(transportContext.getExtras());
        }
        return (int)adler32.getValue();
    }
    
    @Override
    public void schedule(final TransportContext transportContext, final int n) {
        this.schedule(transportContext, n, false);
    }
    
    @Override
    public void schedule(final TransportContext transportContext, final int i, final boolean b) {
        final ComponentName componentName = new ComponentName(this.context, (Class)JobInfoSchedulerService.class);
        final JobScheduler jobScheduler = (JobScheduler)this.context.getSystemService("jobscheduler");
        final int jobId = this.getJobId(transportContext);
        if (!b && this.isJobServiceOn(jobScheduler, jobId, i)) {
            Logging.d("JobInfoScheduler", "Upload for context %s is already scheduled. Returning...", transportContext);
            return;
        }
        final long nextCallTime = this.eventStore.getNextCallTime(transportContext);
        final JobInfo$Builder configureJob = this.config.configureJob(new JobInfo$Builder(jobId, componentName), transportContext.getPriority(), nextCallTime, i);
        final PersistableBundle extras = new PersistableBundle();
        ((BaseBundle)extras).putInt("attemptNumber", i);
        ((BaseBundle)extras).putString("backendName", transportContext.getBackendName());
        ((BaseBundle)extras).putInt("priority", PriorityMapping.toInt(transportContext.getPriority()));
        if (transportContext.getExtras() != null) {
            ((BaseBundle)extras).putString("extras", Base64.encodeToString(transportContext.getExtras(), 0));
        }
        configureJob.setExtras(extras);
        Logging.d("JobInfoScheduler", "Scheduling upload for context %s with jobId=%d in %dms(Backend next call timestamp %d). Attempt %d", transportContext, jobId, this.config.getScheduleDelay(transportContext.getPriority(), nextCallTime, i), nextCallTime, i);
        jobScheduler.schedule(configureJob.build());
    }
}

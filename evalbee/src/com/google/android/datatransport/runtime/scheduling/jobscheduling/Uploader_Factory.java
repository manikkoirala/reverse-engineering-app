// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.scheduling.jobscheduling;

import com.google.android.datatransport.runtime.scheduling.persistence.ClientHealthMetricsStore;
import com.google.android.datatransport.runtime.time.Clock;
import com.google.android.datatransport.runtime.synchronization.SynchronizationGuard;
import java.util.concurrent.Executor;
import com.google.android.datatransport.runtime.scheduling.persistence.EventStore;
import com.google.android.datatransport.runtime.backends.BackendRegistry;
import android.content.Context;
import com.google.android.datatransport.runtime.dagger.internal.Factory;

public final class Uploader_Factory implements Factory<Uploader>
{
    private final q91 backendRegistryProvider;
    private final q91 clientHealthMetricsStoreProvider;
    private final q91 clockProvider;
    private final q91 contextProvider;
    private final q91 eventStoreProvider;
    private final q91 executorProvider;
    private final q91 guardProvider;
    private final q91 uptimeClockProvider;
    private final q91 workSchedulerProvider;
    
    public Uploader_Factory(final q91 contextProvider, final q91 backendRegistryProvider, final q91 eventStoreProvider, final q91 workSchedulerProvider, final q91 executorProvider, final q91 guardProvider, final q91 clockProvider, final q91 uptimeClockProvider, final q91 clientHealthMetricsStoreProvider) {
        this.contextProvider = contextProvider;
        this.backendRegistryProvider = backendRegistryProvider;
        this.eventStoreProvider = eventStoreProvider;
        this.workSchedulerProvider = workSchedulerProvider;
        this.executorProvider = executorProvider;
        this.guardProvider = guardProvider;
        this.clockProvider = clockProvider;
        this.uptimeClockProvider = uptimeClockProvider;
        this.clientHealthMetricsStoreProvider = clientHealthMetricsStoreProvider;
    }
    
    public static Uploader_Factory create(final q91 q91, final q91 q92, final q91 q93, final q91 q94, final q91 q95, final q91 q96, final q91 q97, final q91 q98, final q91 q99) {
        return new Uploader_Factory(q91, q92, q93, q94, q95, q96, q97, q98, q99);
    }
    
    public static Uploader newInstance(final Context context, final BackendRegistry backendRegistry, final EventStore eventStore, final WorkScheduler workScheduler, final Executor executor, final SynchronizationGuard synchronizationGuard, final Clock clock, final Clock clock2, final ClientHealthMetricsStore clientHealthMetricsStore) {
        return new Uploader(context, backendRegistry, eventStore, workScheduler, executor, synchronizationGuard, clock, clock2, clientHealthMetricsStore);
    }
    
    @Override
    public Uploader get() {
        return newInstance((Context)this.contextProvider.get(), (BackendRegistry)this.backendRegistryProvider.get(), (EventStore)this.eventStoreProvider.get(), (WorkScheduler)this.workSchedulerProvider.get(), (Executor)this.executorProvider.get(), (SynchronizationGuard)this.guardProvider.get(), (Clock)this.clockProvider.get(), (Clock)this.uptimeClockProvider.get(), (ClientHealthMetricsStore)this.clientHealthMetricsStoreProvider.get());
    }
}

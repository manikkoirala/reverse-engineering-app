// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.scheduling.jobscheduling;

import android.os.BaseBundle;
import android.util.Base64;
import com.google.android.datatransport.runtime.util.PriorityMapping;
import com.google.android.datatransport.runtime.TransportContext;
import com.google.android.datatransport.runtime.TransportRuntime;
import android.content.Intent;
import android.content.Context;
import android.content.BroadcastReceiver;

public class AlarmManagerSchedulerBroadcastReceiver extends BroadcastReceiver
{
    public void onReceive(final Context context, final Intent intent) {
        final String queryParameter = intent.getData().getQueryParameter("backendName");
        final String queryParameter2 = intent.getData().getQueryParameter("extras");
        final int intValue = Integer.valueOf(intent.getData().getQueryParameter("priority"));
        final int int1 = ((BaseBundle)intent.getExtras()).getInt("attemptNumber");
        TransportRuntime.initialize(context);
        final TransportContext.Builder setPriority = TransportContext.builder().setBackendName(queryParameter).setPriority(PriorityMapping.valueOf(intValue));
        if (queryParameter2 != null) {
            setPriority.setExtras(Base64.decode(queryParameter2, 0));
        }
        TransportRuntime.getInstance().getUploader().upload(setPriority.build(), int1, new a4());
    }
}

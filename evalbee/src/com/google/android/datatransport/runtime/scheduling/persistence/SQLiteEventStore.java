// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.scheduling.persistence;

import java.util.Objects;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.os.SystemClock;
import com.google.android.datatransport.runtime.firebase.transport.LogSourceMetrics;
import java.util.HashMap;
import java.util.HashSet;
import com.google.android.datatransport.runtime.EncodedPayload;
import com.google.android.datatransport.Priority;
import com.google.android.datatransport.runtime.synchronization.SynchronizationException;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Set;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Arrays;
import com.google.android.datatransport.runtime.firebase.transport.StorageMetrics;
import com.google.android.datatransport.runtime.firebase.transport.GlobalMetrics;
import android.util.Base64;
import com.google.android.datatransport.runtime.util.PriorityMapping;
import android.content.ContentValues;
import com.google.android.datatransport.runtime.logging.Logging;
import com.google.android.datatransport.runtime.firebase.transport.LogEventDropped;
import com.google.android.datatransport.runtime.firebase.transport.TimeWindow;
import com.google.android.datatransport.runtime.firebase.transport.ClientMetrics;
import java.util.Map;
import com.google.android.datatransport.runtime.EventInternal;
import android.database.sqlite.SQLiteDatabase;
import android.database.Cursor;
import com.google.android.datatransport.runtime.TransportContext;
import java.util.List;
import com.google.android.datatransport.runtime.time.Monotonic;
import com.google.android.datatransport.runtime.time.WallTime;
import com.google.android.datatransport.runtime.time.Clock;
import com.google.android.datatransport.Encoding;
import com.google.android.datatransport.runtime.synchronization.SynchronizationGuard;

public class SQLiteEventStore implements EventStore, SynchronizationGuard, ClientHealthMetricsStore
{
    private static final int LOCK_RETRY_BACK_OFF_MILLIS = 50;
    private static final String LOG_TAG = "SQLiteEventStore";
    static final int MAX_RETRIES = 16;
    private static final Encoding PROTOBUF_ENCODING;
    private final EventStoreConfig config;
    private final Clock monotonicClock;
    private final q91 packageName;
    private final SchemaManager schemaManager;
    private final Clock wallClock;
    
    static {
        PROTOBUF_ENCODING = Encoding.of("proto");
    }
    
    public SQLiteEventStore(@WallTime final Clock wallClock, @Monotonic final Clock monotonicClock, final EventStoreConfig config, final SchemaManager schemaManager, final q91 packageName) {
        this.schemaManager = schemaManager;
        this.wallClock = wallClock;
        this.monotonicClock = monotonicClock;
        this.config = config;
        this.packageName = packageName;
    }
    
    private LogEventDropped.Reason convertToReason(final int i) {
        final LogEventDropped.Reason reason_UNKNOWN = LogEventDropped.Reason.REASON_UNKNOWN;
        if (i == reason_UNKNOWN.getNumber()) {
            return reason_UNKNOWN;
        }
        final LogEventDropped.Reason message_TOO_OLD = LogEventDropped.Reason.MESSAGE_TOO_OLD;
        if (i == message_TOO_OLD.getNumber()) {
            return message_TOO_OLD;
        }
        final LogEventDropped.Reason cache_FULL = LogEventDropped.Reason.CACHE_FULL;
        if (i == cache_FULL.getNumber()) {
            return cache_FULL;
        }
        final LogEventDropped.Reason payload_TOO_BIG = LogEventDropped.Reason.PAYLOAD_TOO_BIG;
        if (i == payload_TOO_BIG.getNumber()) {
            return payload_TOO_BIG;
        }
        final LogEventDropped.Reason max_RETRIES_REACHED = LogEventDropped.Reason.MAX_RETRIES_REACHED;
        if (i == max_RETRIES_REACHED.getNumber()) {
            return max_RETRIES_REACHED;
        }
        final LogEventDropped.Reason invalid_PAYLOD = LogEventDropped.Reason.INVALID_PAYLOD;
        if (i == invalid_PAYLOD.getNumber()) {
            return invalid_PAYLOD;
        }
        final LogEventDropped.Reason server_ERROR = LogEventDropped.Reason.SERVER_ERROR;
        if (i == server_ERROR.getNumber()) {
            return server_ERROR;
        }
        Logging.d("SQLiteEventStore", "%n is not valid. No matched LogEventDropped-Reason found. Treated it as REASON_UNKNOWN", (Object)i);
        return reason_UNKNOWN;
    }
    
    private void ensureBeginTransaction(final SQLiteDatabase sqLiteDatabase) {
        this.retryIfDbLocked(new k(sqLiteDatabase), (Function<Throwable, Object>)new m());
    }
    
    private long ensureTransportContext(final SQLiteDatabase sqLiteDatabase, final TransportContext transportContext) {
        final Long transportContextId = this.getTransportContextId(sqLiteDatabase, transportContext);
        if (transportContextId != null) {
            return transportContextId;
        }
        final ContentValues contentValues = new ContentValues();
        contentValues.put("backend_name", transportContext.getBackendName());
        contentValues.put("priority", Integer.valueOf(PriorityMapping.toInt(transportContext.getPriority())));
        contentValues.put("next_request_ms", Integer.valueOf(0));
        if (transportContext.getExtras() != null) {
            contentValues.put("extras", Base64.encodeToString(transportContext.getExtras(), 0));
        }
        return sqLiteDatabase.insert("transport_contexts", (String)null, contentValues);
    }
    
    private GlobalMetrics getGlobalMetrics() {
        return GlobalMetrics.newBuilder().setStorageMetrics(StorageMetrics.newBuilder().setCurrentCacheSizeBytes(this.getByteSize()).setMaxCacheSizeBytes(EventStoreConfig.DEFAULT.getMaxStorageSizeInBytes()).build()).build();
    }
    
    private long getPageCount() {
        return this.getDb().compileStatement("PRAGMA page_count").simpleQueryForLong();
    }
    
    private long getPageSize() {
        return this.getDb().compileStatement("PRAGMA page_size").simpleQueryForLong();
    }
    
    private TimeWindow getTimeWindow() {
        return this.inTransaction((Function<SQLiteDatabase, TimeWindow>)new w(this.wallClock.getTime()));
    }
    
    private Long getTransportContextId(final SQLiteDatabase sqLiteDatabase, final TransportContext transportContext) {
        final StringBuilder sb = new StringBuilder("backend_name = ? and priority = ?");
        final ArrayList list = new ArrayList((Collection<? extends E>)Arrays.asList(transportContext.getBackendName(), String.valueOf(PriorityMapping.toInt(transportContext.getPriority()))));
        if (transportContext.getExtras() != null) {
            sb.append(" and extras = ?");
            list.add(Base64.encodeToString(transportContext.getExtras(), 0));
        }
        else {
            sb.append(" and extras is null");
        }
        return tryWithCursor(sqLiteDatabase.query("transport_contexts", new String[] { "_id" }, sb.toString(), (String[])list.toArray(new String[0]), (String)null, (String)null, (String)null), (Function<Cursor, Long>)new t());
    }
    
    private boolean isStorageAtLimit() {
        return this.getPageCount() * this.getPageSize() >= this.config.getMaxStorageSizeInBytes();
    }
    
    private List<PersistedEvent> join(final List<PersistedEvent> list, final Map<Long, Set<Metadata>> map) {
        final ListIterator<PersistedEvent> listIterator = list.listIterator();
        while (listIterator.hasNext()) {
            final PersistedEvent persistedEvent = listIterator.next();
            if (!map.containsKey(persistedEvent.getId())) {
                continue;
            }
            final EventInternal.Builder builder = persistedEvent.getEvent().toBuilder();
            for (final Metadata metadata : map.get(persistedEvent.getId())) {
                builder.addMetadata(metadata.key, metadata.value);
            }
            listIterator.set(PersistedEvent.create(persistedEvent.getId(), persistedEvent.getTransportContext(), builder.build()));
        }
        return list;
    }
    
    private List<PersistedEvent> loadEvents(final SQLiteDatabase sqLiteDatabase, final TransportContext transportContext, final int i) {
        final ArrayList list = new ArrayList();
        final Long transportContextId = this.getTransportContextId(sqLiteDatabase, transportContext);
        if (transportContextId == null) {
            return list;
        }
        tryWithCursor(sqLiteDatabase.query("events", new String[] { "_id", "transport_name", "timestamp_ms", "uptime_ms", "payload_encoding", "payload", "code", "inline" }, "context_id = ?", new String[] { transportContextId.toString() }, (String)null, (String)null, (String)null, String.valueOf(i)), (Function<Cursor, Object>)new i(this, list, transportContext));
        return list;
    }
    
    private Map<Long, Set<Metadata>> loadMetadata(final SQLiteDatabase sqLiteDatabase, final List<PersistedEvent> list) {
        final HashMap hashMap = new HashMap();
        final StringBuilder sb = new StringBuilder("event_id IN (");
        for (int i = 0; i < list.size(); ++i) {
            sb.append(((PersistedEvent)list.get(i)).getId());
            if (i < list.size() - 1) {
                sb.append(',');
            }
        }
        sb.append(')');
        tryWithCursor(sqLiteDatabase.query("event_metadata", new String[] { "event_id", "name", "value" }, sb.toString(), (String[])null, (String)null, (String)null, (String)null), (Function<Cursor, Object>)new l(hashMap));
        return hashMap;
    }
    
    private static byte[] maybeBase64Decode(final String s) {
        if (s == null) {
            return null;
        }
        return Base64.decode(s, 0);
    }
    
    private void populateLogSourcesMetrics(final ClientMetrics.Builder builder, final Map<String, List<LogEventDropped>> map) {
        for (final Map.Entry entry : map.entrySet()) {
            builder.addLogSourceMetrics(LogSourceMetrics.newBuilder().setLogSource((String)entry.getKey()).setLogEventDroppedList((List<LogEventDropped>)entry.getValue()).build());
        }
    }
    
    private byte[] readPayload(final long l) {
        return tryWithCursor(this.getDb().query("event_payloads", new String[] { "bytes" }, "event_id = ?", new String[] { String.valueOf(l) }, (String)null, (String)null, "sequence_num"), (Function<Cursor, byte[]>)new u());
    }
    
    private <T> T retryIfDbLocked(final Producer<T> producer, final Function<Throwable, T> function) {
        final long time = this.monotonicClock.getTime();
        try {
            return producer.produce();
        }
        catch (final SQLiteDatabaseLockedException ex) {
            if (this.monotonicClock.getTime() >= this.config.getCriticalSectionEnterTimeoutMs() + time) {
                return function.apply((Throwable)ex);
            }
            SystemClock.sleep(50L);
            return producer.produce();
        }
    }
    
    private static Encoding toEncoding(final String s) {
        if (s == null) {
            return SQLiteEventStore.PROTOBUF_ENCODING;
        }
        return Encoding.of(s);
    }
    
    private static String toIdList(final Iterable<PersistedEvent> iterable) {
        final StringBuilder sb = new StringBuilder("(");
        final Iterator<PersistedEvent> iterator = iterable.iterator();
        while (iterator.hasNext()) {
            sb.append(iterator.next().getId());
            if (iterator.hasNext()) {
                sb.append(',');
            }
        }
        sb.append(')');
        return sb.toString();
    }
    
    public static <T> T tryWithCursor(final Cursor cursor, final Function<Cursor, T> function) {
        try {
            return function.apply(cursor);
        }
        finally {
            cursor.close();
        }
    }
    
    @Override
    public int cleanUp() {
        return this.inTransaction((Function<SQLiteDatabase, Integer>)new j(this, this.wallClock.getTime() - this.config.getEventCleanUpAge()));
    }
    
    public void clearDb() {
        this.inTransaction((Function<SQLiteDatabase, Object>)new a());
    }
    
    @Override
    public void close() {
        this.schemaManager.close();
    }
    
    public long getByteSize() {
        return this.getPageCount() * this.getPageSize();
    }
    
    public SQLiteDatabase getDb() {
        final SchemaManager schemaManager = this.schemaManager;
        Objects.requireNonNull(schemaManager);
        return this.retryIfDbLocked(new c(schemaManager), (Function<Throwable, SQLiteDatabase>)new d());
    }
    
    @Override
    public long getNextCallTime(final TransportContext transportContext) {
        return tryWithCursor(this.getDb().rawQuery("SELECT next_request_ms FROM transport_contexts WHERE backend_name = ? and priority = ?", new String[] { transportContext.getBackendName(), String.valueOf(PriorityMapping.toInt(transportContext.getPriority())) }), (Function<Cursor, Long>)new e());
    }
    
    @Override
    public boolean hasPendingEventsFor(final TransportContext transportContext) {
        return this.inTransaction((Function<SQLiteDatabase, Boolean>)new a0(this, transportContext));
    }
    
    public <T> T inTransaction(final Function<SQLiteDatabase, T> function) {
        final SQLiteDatabase db = this.getDb();
        db.beginTransaction();
        try {
            final T apply = function.apply(db);
            db.setTransactionSuccessful();
            return apply;
        }
        finally {
            db.endTransaction();
        }
    }
    
    @Override
    public Iterable<TransportContext> loadActiveContexts() {
        return this.inTransaction((Function<SQLiteDatabase, Iterable<TransportContext>>)new y());
    }
    
    @Override
    public Iterable<PersistedEvent> loadBatch(final TransportContext transportContext) {
        return this.inTransaction((Function<SQLiteDatabase, Iterable<PersistedEvent>>)new o(this, transportContext));
    }
    
    @Override
    public ClientMetrics loadClientMetrics() {
        return this.inTransaction((Function<SQLiteDatabase, ClientMetrics>)new v(this, "SELECT log_source, reason, events_dropped_count FROM log_event_dropped", new HashMap(), ClientMetrics.newBuilder()));
    }
    
    @Override
    public PersistedEvent persist(final TransportContext transportContext, final EventInternal eventInternal) {
        Logging.d("SQLiteEventStore", "Storing event with priority=%s, name=%s for destination %s", transportContext.getPriority(), eventInternal.getTransportName(), transportContext.getBackendName());
        final long longValue = this.inTransaction((Function<SQLiteDatabase, Long>)new b0(this, eventInternal, transportContext));
        if (longValue < 1L) {
            return null;
        }
        return PersistedEvent.create(longValue, transportContext, eventInternal);
    }
    
    @Override
    public void recordFailure(final Iterable<PersistedEvent> iterable) {
        if (!iterable.iterator().hasNext()) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("UPDATE events SET num_attempts = num_attempts + 1 WHERE _id in ");
        sb.append(toIdList(iterable));
        this.inTransaction((Function<SQLiteDatabase, Object>)new q(this, sb.toString(), "SELECT COUNT(*), transport_name FROM events WHERE num_attempts >= 16 GROUP BY transport_name"));
    }
    
    @Override
    public void recordLogEventDropped(final long n, final LogEventDropped.Reason reason, final String s) {
        this.inTransaction((Function<SQLiteDatabase, Object>)new b(s, reason, n));
    }
    
    @Override
    public void recordNextCallTime(final TransportContext transportContext, final long n) {
        this.inTransaction((Function<SQLiteDatabase, Object>)new g(n, transportContext));
    }
    
    @Override
    public void recordSuccess(final Iterable<PersistedEvent> iterable) {
        if (!iterable.iterator().hasNext()) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("DELETE FROM events WHERE _id in ");
        sb.append(toIdList(iterable));
        this.getDb().compileStatement(sb.toString()).execute();
    }
    
    @Override
    public void resetClientMetrics() {
        this.inTransaction((Function<SQLiteDatabase, Object>)new h(this));
    }
    
    @Override
    public <T> T runCriticalSection(final CriticalSection<T> criticalSection) {
        final SQLiteDatabase db = this.getDb();
        this.ensureBeginTransaction(db);
        try {
            final T execute = criticalSection.execute();
            db.setTransactionSuccessful();
            return execute;
        }
        finally {
            db.endTransaction();
        }
    }
    
    public interface Function<T, U>
    {
        U apply(final T p0);
    }
    
    public static class Metadata
    {
        final String key;
        final String value;
        
        private Metadata(final String key, final String value) {
            this.key = key;
            this.value = value;
        }
    }
    
    public interface Producer<T>
    {
        T produce();
    }
}

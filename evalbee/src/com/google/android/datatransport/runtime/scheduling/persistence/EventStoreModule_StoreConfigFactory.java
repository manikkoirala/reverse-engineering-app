// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.scheduling.persistence;

import com.google.android.datatransport.runtime.dagger.internal.Preconditions;
import com.google.android.datatransport.runtime.dagger.internal.Factory;

public final class EventStoreModule_StoreConfigFactory implements Factory<EventStoreConfig>
{
    public static EventStoreModule_StoreConfigFactory create() {
        return InstanceHolder.access$000();
    }
    
    public static EventStoreConfig storeConfig() {
        return Preconditions.checkNotNull(EventStoreModule.storeConfig(), "Cannot return null from a non-@Nullable @Provides method");
    }
    
    @Override
    public EventStoreConfig get() {
        return storeConfig();
    }
    
    public static final class InstanceHolder
    {
        private static final EventStoreModule_StoreConfigFactory INSTANCE;
        
        static {
            INSTANCE = new EventStoreModule_StoreConfigFactory();
        }
        
        private InstanceHolder() {
        }
        
        public static /* synthetic */ EventStoreModule_StoreConfigFactory access$000() {
            return InstanceHolder.INSTANCE;
        }
    }
}

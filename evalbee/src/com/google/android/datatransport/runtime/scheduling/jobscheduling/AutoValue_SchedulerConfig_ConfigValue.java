// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.scheduling.jobscheduling;

import java.util.Set;

final class AutoValue_SchedulerConfig_ConfigValue extends ConfigValue
{
    private final long delta;
    private final Set<Flag> flags;
    private final long maxAllowedDelay;
    
    private AutoValue_SchedulerConfig_ConfigValue(final long delta, final long maxAllowedDelay, final Set<Flag> flags) {
        this.delta = delta;
        this.maxAllowedDelay = maxAllowedDelay;
        this.flags = flags;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof ConfigValue) {
            final ConfigValue configValue = (ConfigValue)o;
            if (this.delta != configValue.getDelta() || this.maxAllowedDelay != configValue.getMaxAllowedDelay() || !this.flags.equals(configValue.getFlags())) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public long getDelta() {
        return this.delta;
    }
    
    @Override
    public Set<Flag> getFlags() {
        return this.flags;
    }
    
    @Override
    public long getMaxAllowedDelay() {
        return this.maxAllowedDelay;
    }
    
    @Override
    public int hashCode() {
        final long delta = this.delta;
        final int n = (int)(delta ^ delta >>> 32);
        final long maxAllowedDelay = this.maxAllowedDelay;
        return ((n ^ 0xF4243) * 1000003 ^ (int)(maxAllowedDelay >>> 32 ^ maxAllowedDelay)) * 1000003 ^ this.flags.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ConfigValue{delta=");
        sb.append(this.delta);
        sb.append(", maxAllowedDelay=");
        sb.append(this.maxAllowedDelay);
        sb.append(", flags=");
        sb.append(this.flags);
        sb.append("}");
        return sb.toString();
    }
    
    public static final class Builder extends ConfigValue.Builder
    {
        private Long delta;
        private Set<Flag> flags;
        private Long maxAllowedDelay;
        
        @Override
        public ConfigValue build() {
            final Long delta = this.delta;
            String string = "";
            if (delta == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("");
                sb.append(" delta");
                string = sb.toString();
            }
            String string2 = string;
            if (this.maxAllowedDelay == null) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(string);
                sb2.append(" maxAllowedDelay");
                string2 = sb2.toString();
            }
            String string3 = string2;
            if (this.flags == null) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(string2);
                sb3.append(" flags");
                string3 = sb3.toString();
            }
            if (string3.isEmpty()) {
                return new AutoValue_SchedulerConfig_ConfigValue(this.delta, this.maxAllowedDelay, this.flags, null);
            }
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("Missing required properties:");
            sb4.append(string3);
            throw new IllegalStateException(sb4.toString());
        }
        
        @Override
        public ConfigValue.Builder setDelta(final long l) {
            this.delta = l;
            return this;
        }
        
        @Override
        public ConfigValue.Builder setFlags(final Set<Flag> flags) {
            if (flags != null) {
                this.flags = flags;
                return this;
            }
            throw new NullPointerException("Null flags");
        }
        
        @Override
        public ConfigValue.Builder setMaxAllowedDelay(final long l) {
            this.maxAllowedDelay = l;
            return this;
        }
    }
}

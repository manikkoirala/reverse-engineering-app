// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.scheduling.jobscheduling;

import com.google.android.datatransport.runtime.logging.Logging;
import android.util.Base64;
import com.google.android.datatransport.runtime.util.PriorityMapping;
import android.net.Uri$Builder;
import com.google.android.datatransport.runtime.TransportContext;
import android.app.PendingIntent;
import android.content.Intent;
import com.google.android.datatransport.runtime.scheduling.persistence.EventStore;
import android.content.Context;
import com.google.android.datatransport.runtime.time.Clock;
import android.app.AlarmManager;

public class AlarmManagerScheduler implements WorkScheduler
{
    static final String ATTEMPT_NUMBER = "attemptNumber";
    static final String BACKEND_NAME = "backendName";
    static final String EVENT_PRIORITY = "priority";
    static final String EXTRAS = "extras";
    private static final String LOG_TAG = "AlarmManagerScheduler";
    private AlarmManager alarmManager;
    private final Clock clock;
    private final SchedulerConfig config;
    private final Context context;
    private final EventStore eventStore;
    
    public AlarmManagerScheduler(final Context context, final EventStore eventStore, final AlarmManager alarmManager, final Clock clock, final SchedulerConfig config) {
        this.context = context;
        this.eventStore = eventStore;
        this.alarmManager = alarmManager;
        this.clock = clock;
        this.config = config;
    }
    
    public AlarmManagerScheduler(final Context context, final EventStore eventStore, final Clock clock, final SchedulerConfig schedulerConfig) {
        this(context, eventStore, (AlarmManager)context.getSystemService("alarm"), clock, schedulerConfig);
    }
    
    public boolean isJobServiceOn(final Intent intent) {
        final Context context = this.context;
        boolean b = false;
        if (PendingIntent.getBroadcast(context, 0, intent, 603979776) != null) {
            b = true;
        }
        return b;
    }
    
    @Override
    public void schedule(final TransportContext transportContext, final int n) {
        this.schedule(transportContext, n, false);
    }
    
    @Override
    public void schedule(final TransportContext transportContext, final int i, final boolean b) {
        final Uri$Builder uri$Builder = new Uri$Builder();
        uri$Builder.appendQueryParameter("backendName", transportContext.getBackendName());
        uri$Builder.appendQueryParameter("priority", String.valueOf(PriorityMapping.toInt(transportContext.getPriority())));
        if (transportContext.getExtras() != null) {
            uri$Builder.appendQueryParameter("extras", Base64.encodeToString(transportContext.getExtras(), 0));
        }
        final Intent intent = new Intent(this.context, (Class)AlarmManagerSchedulerBroadcastReceiver.class);
        intent.setData(uri$Builder.build());
        intent.putExtra("attemptNumber", i);
        if (!b && this.isJobServiceOn(intent)) {
            Logging.d("AlarmManagerScheduler", "Upload for context %s is already scheduled. Returning...", transportContext);
            return;
        }
        final long nextCallTime = this.eventStore.getNextCallTime(transportContext);
        final long scheduleDelay = this.config.getScheduleDelay(transportContext.getPriority(), nextCallTime, i);
        Logging.d("AlarmManagerScheduler", "Scheduling upload for context %s in %dms(Backend next call timestamp %d). Attempt %d", transportContext, scheduleDelay, nextCallTime, i);
        this.alarmManager.set(3, this.clock.getTime() + scheduleDelay, PendingIntent.getBroadcast(this.context, 0, intent, 67108864));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.scheduling;

import com.google.android.datatransport.runtime.dagger.internal.Preconditions;
import com.google.android.datatransport.runtime.time.Clock;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.SchedulerConfig;
import com.google.android.datatransport.runtime.scheduling.persistence.EventStore;
import android.content.Context;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.WorkScheduler;
import com.google.android.datatransport.runtime.dagger.internal.Factory;

public final class SchedulingModule_WorkSchedulerFactory implements Factory<WorkScheduler>
{
    private final q91 clockProvider;
    private final q91 configProvider;
    private final q91 contextProvider;
    private final q91 eventStoreProvider;
    
    public SchedulingModule_WorkSchedulerFactory(final q91 contextProvider, final q91 eventStoreProvider, final q91 configProvider, final q91 clockProvider) {
        this.contextProvider = contextProvider;
        this.eventStoreProvider = eventStoreProvider;
        this.configProvider = configProvider;
        this.clockProvider = clockProvider;
    }
    
    public static SchedulingModule_WorkSchedulerFactory create(final q91 q91, final q91 q92, final q91 q93, final q91 q94) {
        return new SchedulingModule_WorkSchedulerFactory(q91, q92, q93, q94);
    }
    
    public static WorkScheduler workScheduler(final Context context, final EventStore eventStore, final SchedulerConfig schedulerConfig, final Clock clock) {
        return Preconditions.checkNotNull(SchedulingModule.workScheduler(context, eventStore, schedulerConfig, clock), "Cannot return null from a non-@Nullable @Provides method");
    }
    
    @Override
    public WorkScheduler get() {
        return workScheduler((Context)this.contextProvider.get(), (EventStore)this.eventStoreProvider.get(), (SchedulerConfig)this.configProvider.get(), (Clock)this.clockProvider.get());
    }
}

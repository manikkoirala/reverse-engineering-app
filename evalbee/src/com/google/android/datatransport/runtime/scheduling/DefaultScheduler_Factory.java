// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.scheduling;

import com.google.android.datatransport.runtime.synchronization.SynchronizationGuard;
import com.google.android.datatransport.runtime.scheduling.persistence.EventStore;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.WorkScheduler;
import com.google.android.datatransport.runtime.backends.BackendRegistry;
import java.util.concurrent.Executor;
import com.google.android.datatransport.runtime.dagger.internal.Factory;

public final class DefaultScheduler_Factory implements Factory<DefaultScheduler>
{
    private final q91 backendRegistryProvider;
    private final q91 eventStoreProvider;
    private final q91 executorProvider;
    private final q91 guardProvider;
    private final q91 workSchedulerProvider;
    
    public DefaultScheduler_Factory(final q91 executorProvider, final q91 backendRegistryProvider, final q91 workSchedulerProvider, final q91 eventStoreProvider, final q91 guardProvider) {
        this.executorProvider = executorProvider;
        this.backendRegistryProvider = backendRegistryProvider;
        this.workSchedulerProvider = workSchedulerProvider;
        this.eventStoreProvider = eventStoreProvider;
        this.guardProvider = guardProvider;
    }
    
    public static DefaultScheduler_Factory create(final q91 q91, final q91 q92, final q91 q93, final q91 q94, final q91 q95) {
        return new DefaultScheduler_Factory(q91, q92, q93, q94, q95);
    }
    
    public static DefaultScheduler newInstance(final Executor executor, final BackendRegistry backendRegistry, final WorkScheduler workScheduler, final EventStore eventStore, final SynchronizationGuard synchronizationGuard) {
        return new DefaultScheduler(executor, backendRegistry, workScheduler, eventStore, synchronizationGuard);
    }
    
    @Override
    public DefaultScheduler get() {
        return newInstance((Executor)this.executorProvider.get(), (BackendRegistry)this.backendRegistryProvider.get(), (WorkScheduler)this.workSchedulerProvider.get(), (EventStore)this.eventStoreProvider.get(), (SynchronizationGuard)this.guardProvider.get());
    }
}

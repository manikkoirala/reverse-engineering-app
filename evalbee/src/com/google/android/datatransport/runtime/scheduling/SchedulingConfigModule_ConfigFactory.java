// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.scheduling;

import com.google.android.datatransport.runtime.dagger.internal.Preconditions;
import com.google.android.datatransport.runtime.time.Clock;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.SchedulerConfig;
import com.google.android.datatransport.runtime.dagger.internal.Factory;

public final class SchedulingConfigModule_ConfigFactory implements Factory<SchedulerConfig>
{
    private final q91 clockProvider;
    
    public SchedulingConfigModule_ConfigFactory(final q91 clockProvider) {
        this.clockProvider = clockProvider;
    }
    
    public static SchedulerConfig config(final Clock clock) {
        return Preconditions.checkNotNull(SchedulingConfigModule.config(clock), "Cannot return null from a non-@Nullable @Provides method");
    }
    
    public static SchedulingConfigModule_ConfigFactory create(final q91 q91) {
        return new SchedulingConfigModule_ConfigFactory(q91);
    }
    
    @Override
    public SchedulerConfig get() {
        return config((Clock)this.clockProvider.get());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.scheduling.persistence;

import com.google.android.datatransport.runtime.TransportContext;
import com.google.android.datatransport.runtime.EventInternal;

final class AutoValue_PersistedEvent extends PersistedEvent
{
    private final EventInternal event;
    private final long id;
    private final TransportContext transportContext;
    
    public AutoValue_PersistedEvent(final long id, final TransportContext transportContext, final EventInternal event) {
        this.id = id;
        if (transportContext == null) {
            throw new NullPointerException("Null transportContext");
        }
        this.transportContext = transportContext;
        if (event != null) {
            this.event = event;
            return;
        }
        throw new NullPointerException("Null event");
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof PersistedEvent) {
            final PersistedEvent persistedEvent = (PersistedEvent)o;
            if (this.id != persistedEvent.getId() || !this.transportContext.equals(persistedEvent.getTransportContext()) || !this.event.equals(persistedEvent.getEvent())) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public EventInternal getEvent() {
        return this.event;
    }
    
    @Override
    public long getId() {
        return this.id;
    }
    
    @Override
    public TransportContext getTransportContext() {
        return this.transportContext;
    }
    
    @Override
    public int hashCode() {
        final long id = this.id;
        return (((int)(id ^ id >>> 32) ^ 0xF4243) * 1000003 ^ this.transportContext.hashCode()) * 1000003 ^ this.event.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("PersistedEvent{id=");
        sb.append(this.id);
        sb.append(", transportContext=");
        sb.append(this.transportContext);
        sb.append(", event=");
        sb.append(this.event);
        sb.append("}");
        return sb.toString();
    }
}

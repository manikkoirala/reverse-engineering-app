// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.scheduling.persistence;

import com.google.android.datatransport.runtime.dagger.internal.Preconditions;
import com.google.android.datatransport.runtime.dagger.internal.Factory;

public final class EventStoreModule_DbNameFactory implements Factory<String>
{
    public static EventStoreModule_DbNameFactory create() {
        return InstanceHolder.access$000();
    }
    
    public static String dbName() {
        return Preconditions.checkNotNull(EventStoreModule.dbName(), "Cannot return null from a non-@Nullable @Provides method");
    }
    
    @Override
    public String get() {
        return dbName();
    }
    
    public static final class InstanceHolder
    {
        private static final EventStoreModule_DbNameFactory INSTANCE;
        
        static {
            INSTANCE = new EventStoreModule_DbNameFactory();
        }
        
        private InstanceHolder() {
        }
        
        public static /* synthetic */ EventStoreModule_DbNameFactory access$000() {
            return InstanceHolder.INSTANCE;
        }
    }
}

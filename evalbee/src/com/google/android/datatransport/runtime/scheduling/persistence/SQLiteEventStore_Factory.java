// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.scheduling.persistence;

import com.google.android.datatransport.runtime.time.Clock;
import com.google.android.datatransport.runtime.dagger.internal.Factory;

public final class SQLiteEventStore_Factory implements Factory<SQLiteEventStore>
{
    private final q91 clockProvider;
    private final q91 configProvider;
    private final q91 packageNameProvider;
    private final q91 schemaManagerProvider;
    private final q91 wallClockProvider;
    
    public SQLiteEventStore_Factory(final q91 wallClockProvider, final q91 clockProvider, final q91 configProvider, final q91 schemaManagerProvider, final q91 packageNameProvider) {
        this.wallClockProvider = wallClockProvider;
        this.clockProvider = clockProvider;
        this.configProvider = configProvider;
        this.schemaManagerProvider = schemaManagerProvider;
        this.packageNameProvider = packageNameProvider;
    }
    
    public static SQLiteEventStore_Factory create(final q91 q91, final q91 q92, final q91 q93, final q91 q94, final q91 q95) {
        return new SQLiteEventStore_Factory(q91, q92, q93, q94, q95);
    }
    
    public static SQLiteEventStore newInstance(final Clock clock, final Clock clock2, final Object o, final Object o2, final q91 q91) {
        return new SQLiteEventStore(clock, clock2, (EventStoreConfig)o, (SchemaManager)o2, q91);
    }
    
    @Override
    public SQLiteEventStore get() {
        return newInstance((Clock)this.wallClockProvider.get(), (Clock)this.clockProvider.get(), this.configProvider.get(), this.schemaManagerProvider.get(), this.packageNameProvider);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.scheduling.jobscheduling;

import com.google.android.datatransport.Priority;
import java.util.Map;
import com.google.android.datatransport.runtime.time.Clock;

final class AutoValue_SchedulerConfig extends SchedulerConfig
{
    private final Clock clock;
    private final Map<Priority, ConfigValue> values;
    
    public AutoValue_SchedulerConfig(final Clock clock, final Map<Priority, ConfigValue> values) {
        if (clock == null) {
            throw new NullPointerException("Null clock");
        }
        this.clock = clock;
        if (values != null) {
            this.values = values;
            return;
        }
        throw new NullPointerException("Null values");
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof SchedulerConfig) {
            final SchedulerConfig schedulerConfig = (SchedulerConfig)o;
            if (!this.clock.equals(schedulerConfig.getClock()) || !this.values.equals(schedulerConfig.getValues())) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public Clock getClock() {
        return this.clock;
    }
    
    @Override
    public Map<Priority, ConfigValue> getValues() {
        return this.values;
    }
    
    @Override
    public int hashCode() {
        return (this.clock.hashCode() ^ 0xF4243) * 1000003 ^ this.values.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SchedulerConfig{clock=");
        sb.append(this.clock);
        sb.append(", values=");
        sb.append(this.values);
        sb.append("}");
        return sb.toString();
    }
}

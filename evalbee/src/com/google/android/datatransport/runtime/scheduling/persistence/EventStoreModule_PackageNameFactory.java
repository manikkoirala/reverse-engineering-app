// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.scheduling.persistence;

import com.google.android.datatransport.runtime.dagger.internal.Preconditions;
import android.content.Context;
import com.google.android.datatransport.runtime.dagger.internal.Factory;

public final class EventStoreModule_PackageNameFactory implements Factory<String>
{
    private final q91 contextProvider;
    
    public EventStoreModule_PackageNameFactory(final q91 contextProvider) {
        this.contextProvider = contextProvider;
    }
    
    public static EventStoreModule_PackageNameFactory create(final q91 q91) {
        return new EventStoreModule_PackageNameFactory(q91);
    }
    
    public static String packageName(final Context context) {
        return Preconditions.checkNotNull(EventStoreModule.packageName(context), "Cannot return null from a non-@Nullable @Provides method");
    }
    
    @Override
    public String get() {
        return packageName((Context)this.contextProvider.get());
    }
}

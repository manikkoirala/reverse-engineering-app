// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.scheduling.persistence;

final class AutoValue_EventStoreConfig extends EventStoreConfig
{
    private final int criticalSectionEnterTimeoutMs;
    private final long eventCleanUpAge;
    private final int loadBatchSize;
    private final int maxBlobByteSizePerRow;
    private final long maxStorageSizeInBytes;
    
    private AutoValue_EventStoreConfig(final long maxStorageSizeInBytes, final int loadBatchSize, final int criticalSectionEnterTimeoutMs, final long eventCleanUpAge, final int maxBlobByteSizePerRow) {
        this.maxStorageSizeInBytes = maxStorageSizeInBytes;
        this.loadBatchSize = loadBatchSize;
        this.criticalSectionEnterTimeoutMs = criticalSectionEnterTimeoutMs;
        this.eventCleanUpAge = eventCleanUpAge;
        this.maxBlobByteSizePerRow = maxBlobByteSizePerRow;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof EventStoreConfig) {
            final EventStoreConfig eventStoreConfig = (EventStoreConfig)o;
            if (this.maxStorageSizeInBytes != eventStoreConfig.getMaxStorageSizeInBytes() || this.loadBatchSize != eventStoreConfig.getLoadBatchSize() || this.criticalSectionEnterTimeoutMs != eventStoreConfig.getCriticalSectionEnterTimeoutMs() || this.eventCleanUpAge != eventStoreConfig.getEventCleanUpAge() || this.maxBlobByteSizePerRow != eventStoreConfig.getMaxBlobByteSizePerRow()) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int getCriticalSectionEnterTimeoutMs() {
        return this.criticalSectionEnterTimeoutMs;
    }
    
    @Override
    public long getEventCleanUpAge() {
        return this.eventCleanUpAge;
    }
    
    @Override
    public int getLoadBatchSize() {
        return this.loadBatchSize;
    }
    
    @Override
    public int getMaxBlobByteSizePerRow() {
        return this.maxBlobByteSizePerRow;
    }
    
    @Override
    public long getMaxStorageSizeInBytes() {
        return this.maxStorageSizeInBytes;
    }
    
    @Override
    public int hashCode() {
        final long maxStorageSizeInBytes = this.maxStorageSizeInBytes;
        final int n = (int)(maxStorageSizeInBytes ^ maxStorageSizeInBytes >>> 32);
        final int loadBatchSize = this.loadBatchSize;
        final int criticalSectionEnterTimeoutMs = this.criticalSectionEnterTimeoutMs;
        final long eventCleanUpAge = this.eventCleanUpAge;
        return ((((n ^ 0xF4243) * 1000003 ^ loadBatchSize) * 1000003 ^ criticalSectionEnterTimeoutMs) * 1000003 ^ (int)(eventCleanUpAge >>> 32 ^ eventCleanUpAge)) * 1000003 ^ this.maxBlobByteSizePerRow;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("EventStoreConfig{maxStorageSizeInBytes=");
        sb.append(this.maxStorageSizeInBytes);
        sb.append(", loadBatchSize=");
        sb.append(this.loadBatchSize);
        sb.append(", criticalSectionEnterTimeoutMs=");
        sb.append(this.criticalSectionEnterTimeoutMs);
        sb.append(", eventCleanUpAge=");
        sb.append(this.eventCleanUpAge);
        sb.append(", maxBlobByteSizePerRow=");
        sb.append(this.maxBlobByteSizePerRow);
        sb.append("}");
        return sb.toString();
    }
    
    public static final class Builder extends EventStoreConfig.Builder
    {
        private Integer criticalSectionEnterTimeoutMs;
        private Long eventCleanUpAge;
        private Integer loadBatchSize;
        private Integer maxBlobByteSizePerRow;
        private Long maxStorageSizeInBytes;
        
        @Override
        public EventStoreConfig build() {
            final Long maxStorageSizeInBytes = this.maxStorageSizeInBytes;
            String string = "";
            if (maxStorageSizeInBytes == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("");
                sb.append(" maxStorageSizeInBytes");
                string = sb.toString();
            }
            String string2 = string;
            if (this.loadBatchSize == null) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(string);
                sb2.append(" loadBatchSize");
                string2 = sb2.toString();
            }
            String string3 = string2;
            if (this.criticalSectionEnterTimeoutMs == null) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(string2);
                sb3.append(" criticalSectionEnterTimeoutMs");
                string3 = sb3.toString();
            }
            String string4 = string3;
            if (this.eventCleanUpAge == null) {
                final StringBuilder sb4 = new StringBuilder();
                sb4.append(string3);
                sb4.append(" eventCleanUpAge");
                string4 = sb4.toString();
            }
            String string5 = string4;
            if (this.maxBlobByteSizePerRow == null) {
                final StringBuilder sb5 = new StringBuilder();
                sb5.append(string4);
                sb5.append(" maxBlobByteSizePerRow");
                string5 = sb5.toString();
            }
            if (string5.isEmpty()) {
                return new AutoValue_EventStoreConfig(this.maxStorageSizeInBytes, this.loadBatchSize, this.criticalSectionEnterTimeoutMs, this.eventCleanUpAge, this.maxBlobByteSizePerRow, null);
            }
            final StringBuilder sb6 = new StringBuilder();
            sb6.append("Missing required properties:");
            sb6.append(string5);
            throw new IllegalStateException(sb6.toString());
        }
        
        @Override
        public EventStoreConfig.Builder setCriticalSectionEnterTimeoutMs(final int i) {
            this.criticalSectionEnterTimeoutMs = i;
            return this;
        }
        
        @Override
        public EventStoreConfig.Builder setEventCleanUpAge(final long l) {
            this.eventCleanUpAge = l;
            return this;
        }
        
        @Override
        public EventStoreConfig.Builder setLoadBatchSize(final int i) {
            this.loadBatchSize = i;
            return this;
        }
        
        @Override
        public EventStoreConfig.Builder setMaxBlobByteSizePerRow(final int i) {
            this.maxBlobByteSizePerRow = i;
            return this;
        }
        
        @Override
        public EventStoreConfig.Builder setMaxStorageSizeInBytes(final long l) {
            this.maxStorageSizeInBytes = l;
            return this;
        }
    }
}

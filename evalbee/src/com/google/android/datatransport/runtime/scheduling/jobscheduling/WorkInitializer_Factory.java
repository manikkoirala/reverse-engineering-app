// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.scheduling.jobscheduling;

import com.google.android.datatransport.runtime.synchronization.SynchronizationGuard;
import com.google.android.datatransport.runtime.scheduling.persistence.EventStore;
import java.util.concurrent.Executor;
import com.google.android.datatransport.runtime.dagger.internal.Factory;

public final class WorkInitializer_Factory implements Factory<WorkInitializer>
{
    private final q91 executorProvider;
    private final q91 guardProvider;
    private final q91 schedulerProvider;
    private final q91 storeProvider;
    
    public WorkInitializer_Factory(final q91 executorProvider, final q91 storeProvider, final q91 schedulerProvider, final q91 guardProvider) {
        this.executorProvider = executorProvider;
        this.storeProvider = storeProvider;
        this.schedulerProvider = schedulerProvider;
        this.guardProvider = guardProvider;
    }
    
    public static WorkInitializer_Factory create(final q91 q91, final q91 q92, final q91 q93, final q91 q94) {
        return new WorkInitializer_Factory(q91, q92, q93, q94);
    }
    
    public static WorkInitializer newInstance(final Executor executor, final EventStore eventStore, final WorkScheduler workScheduler, final SynchronizationGuard synchronizationGuard) {
        return new WorkInitializer(executor, eventStore, workScheduler, synchronizationGuard);
    }
    
    @Override
    public WorkInitializer get() {
        return newInstance((Executor)this.executorProvider.get(), (EventStore)this.storeProvider.get(), (WorkScheduler)this.schedulerProvider.get(), (SynchronizationGuard)this.guardProvider.get());
    }
}

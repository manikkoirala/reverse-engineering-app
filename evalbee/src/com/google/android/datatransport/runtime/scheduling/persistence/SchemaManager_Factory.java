// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.scheduling.persistence;

import android.content.Context;
import com.google.android.datatransport.runtime.dagger.internal.Factory;

public final class SchemaManager_Factory implements Factory<SchemaManager>
{
    private final q91 contextProvider;
    private final q91 dbNameProvider;
    private final q91 schemaVersionProvider;
    
    public SchemaManager_Factory(final q91 contextProvider, final q91 dbNameProvider, final q91 schemaVersionProvider) {
        this.contextProvider = contextProvider;
        this.dbNameProvider = dbNameProvider;
        this.schemaVersionProvider = schemaVersionProvider;
    }
    
    public static SchemaManager_Factory create(final q91 q91, final q91 q92, final q91 q93) {
        return new SchemaManager_Factory(q91, q92, q93);
    }
    
    public static SchemaManager newInstance(final Context context, final String s, final int n) {
        return new SchemaManager(context, s, n);
    }
    
    @Override
    public SchemaManager get() {
        return newInstance((Context)this.contextProvider.get(), (String)this.dbNameProvider.get(), (int)this.schemaVersionProvider.get());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.scheduling.persistence;

import com.google.android.datatransport.runtime.dagger.internal.Factory;

public final class EventStoreModule_SchemaVersionFactory implements Factory<Integer>
{
    public static EventStoreModule_SchemaVersionFactory create() {
        return InstanceHolder.access$000();
    }
    
    public static int schemaVersion() {
        return EventStoreModule.schemaVersion();
    }
    
    @Override
    public Integer get() {
        return schemaVersion();
    }
    
    public static final class InstanceHolder
    {
        private static final EventStoreModule_SchemaVersionFactory INSTANCE;
        
        static {
            INSTANCE = new EventStoreModule_SchemaVersionFactory();
        }
        
        private InstanceHolder() {
        }
        
        public static /* synthetic */ EventStoreModule_SchemaVersionFactory access$000() {
            return InstanceHolder.INSTANCE;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.scheduling;

import com.google.android.datatransport.runtime.backends.TransportBackend;
import com.google.android.datatransport.runtime.EventInternal;
import com.google.android.datatransport.TransportScheduleCallback;
import com.google.android.datatransport.runtime.TransportContext;
import com.google.android.datatransport.runtime.TransportRuntime;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.WorkScheduler;
import com.google.android.datatransport.runtime.synchronization.SynchronizationGuard;
import java.util.concurrent.Executor;
import com.google.android.datatransport.runtime.scheduling.persistence.EventStore;
import com.google.android.datatransport.runtime.backends.BackendRegistry;
import java.util.logging.Logger;

public class DefaultScheduler implements Scheduler
{
    private static final Logger LOGGER;
    private final BackendRegistry backendRegistry;
    private final EventStore eventStore;
    private final Executor executor;
    private final SynchronizationGuard guard;
    private final WorkScheduler workScheduler;
    
    static {
        LOGGER = Logger.getLogger(TransportRuntime.class.getName());
    }
    
    public DefaultScheduler(final Executor executor, final BackendRegistry backendRegistry, final WorkScheduler workScheduler, final EventStore eventStore, final SynchronizationGuard guard) {
        this.executor = executor;
        this.backendRegistry = backendRegistry;
        this.workScheduler = workScheduler;
        this.eventStore = eventStore;
        this.guard = guard;
    }
    
    @Override
    public void schedule(final TransportContext transportContext, final EventInternal eventInternal, final TransportScheduleCallback transportScheduleCallback) {
        this.executor.execute(new zq(this, transportContext, transportScheduleCallback, eventInternal));
    }
}

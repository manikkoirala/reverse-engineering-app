// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.scheduling.persistence;

import com.google.auto.value.AutoValue$Builder;
import com.google.auto.value.AutoValue;

@AutoValue
abstract class EventStoreConfig
{
    static final EventStoreConfig DEFAULT;
    private static final long DURATION_ONE_WEEK_MS = 604800000L;
    private static final int LOAD_BATCH_SIZE = 200;
    private static final int LOCK_TIME_OUT_MS = 10000;
    private static final int MAX_BLOB_BYTE_SIZE_PER_ROW = 81920;
    private static final long MAX_DB_STORAGE_SIZE_IN_BYTES = 10485760L;
    
    static {
        DEFAULT = builder().setMaxStorageSizeInBytes(10485760L).setLoadBatchSize(200).setCriticalSectionEnterTimeoutMs(10000).setEventCleanUpAge(604800000L).setMaxBlobByteSizePerRow(81920).build();
    }
    
    public EventStoreConfig() {
    }
    
    public static Builder builder() {
        return (Builder)new AutoValue_EventStoreConfig.Builder();
    }
    
    public abstract int getCriticalSectionEnterTimeoutMs();
    
    public abstract long getEventCleanUpAge();
    
    public abstract int getLoadBatchSize();
    
    public abstract int getMaxBlobByteSizePerRow();
    
    public abstract long getMaxStorageSizeInBytes();
    
    public Builder toBuilder() {
        return builder().setMaxStorageSizeInBytes(this.getMaxStorageSizeInBytes()).setLoadBatchSize(this.getLoadBatchSize()).setCriticalSectionEnterTimeoutMs(this.getCriticalSectionEnterTimeoutMs()).setEventCleanUpAge(this.getEventCleanUpAge()).setMaxBlobByteSizePerRow(this.getMaxBlobByteSizePerRow());
    }
    
    @AutoValue$Builder
    public abstract static class Builder
    {
        public abstract EventStoreConfig build();
        
        public abstract Builder setCriticalSectionEnterTimeoutMs(final int p0);
        
        public abstract Builder setEventCleanUpAge(final long p0);
        
        public abstract Builder setLoadBatchSize(final int p0);
        
        public abstract Builder setMaxBlobByteSizePerRow(final int p0);
        
        public abstract Builder setMaxStorageSizeInBytes(final long p0);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.scheduling.jobscheduling;

import android.content.Context;
import android.os.BaseBundle;
import android.util.Base64;
import com.google.android.datatransport.runtime.util.PriorityMapping;
import com.google.android.datatransport.runtime.TransportContext;
import com.google.android.datatransport.runtime.TransportRuntime;
import android.app.job.JobParameters;
import android.app.job.JobService;

public class JobInfoSchedulerService extends JobService
{
    public boolean onStartJob(final JobParameters jobParameters) {
        final String string = ((BaseBundle)jobParameters.getExtras()).getString("backendName");
        final String string2 = ((BaseBundle)jobParameters.getExtras()).getString("extras");
        final int int1 = ((BaseBundle)jobParameters.getExtras()).getInt("priority");
        final int int2 = ((BaseBundle)jobParameters.getExtras()).getInt("attemptNumber");
        TransportRuntime.initialize(((Context)this).getApplicationContext());
        final TransportContext.Builder setPriority = TransportContext.builder().setBackendName(string).setPriority(PriorityMapping.valueOf(int1));
        if (string2 != null) {
            setPriority.setExtras(Base64.decode(string2, 0));
        }
        TransportRuntime.getInstance().getUploader().upload(setPriority.build(), int2, new zg0(this, jobParameters));
        return true;
    }
    
    public boolean onStopJob(final JobParameters jobParameters) {
        return true;
    }
}

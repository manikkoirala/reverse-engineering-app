// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.scheduling.jobscheduling;

import java.util.HashMap;
import com.google.android.datatransport.runtime.backends.BackendRequest;
import java.util.ArrayList;
import com.google.android.datatransport.runtime.logging.Logging;
import com.google.android.datatransport.runtime.backends.BackendResponse;
import android.net.NetworkInfo;
import android.net.ConnectivityManager;
import com.google.android.datatransport.runtime.EncodedPayload;
import com.google.android.datatransport.runtime.firebase.transport.ClientMetrics;
import com.google.android.datatransport.Encoding;
import com.google.android.datatransport.runtime.EventInternal;
import com.google.android.datatransport.runtime.backends.TransportBackend;
import com.google.android.datatransport.runtime.synchronization.SynchronizationException;
import java.util.Objects;
import java.util.Iterator;
import com.google.android.datatransport.runtime.firebase.transport.LogEventDropped;
import com.google.android.datatransport.runtime.scheduling.persistence.PersistedEvent;
import java.util.Map;
import com.google.android.datatransport.runtime.TransportContext;
import com.google.android.datatransport.runtime.time.Monotonic;
import com.google.android.datatransport.runtime.time.WallTime;
import com.google.android.datatransport.runtime.synchronization.SynchronizationGuard;
import java.util.concurrent.Executor;
import com.google.android.datatransport.runtime.scheduling.persistence.EventStore;
import android.content.Context;
import com.google.android.datatransport.runtime.time.Clock;
import com.google.android.datatransport.runtime.scheduling.persistence.ClientHealthMetricsStore;
import com.google.android.datatransport.runtime.backends.BackendRegistry;

public class Uploader
{
    private static final String CLIENT_HEALTH_METRICS_LOG_SOURCE = "GDT_CLIENT_METRICS";
    private static final String LOG_TAG = "Uploader";
    private final BackendRegistry backendRegistry;
    private final ClientHealthMetricsStore clientHealthMetricsStore;
    private final Clock clock;
    private final Context context;
    private final EventStore eventStore;
    private final Executor executor;
    private final SynchronizationGuard guard;
    private final Clock uptimeClock;
    private final WorkScheduler workScheduler;
    
    public Uploader(final Context context, final BackendRegistry backendRegistry, final EventStore eventStore, final WorkScheduler workScheduler, final Executor executor, final SynchronizationGuard guard, @WallTime final Clock clock, @Monotonic final Clock uptimeClock, final ClientHealthMetricsStore clientHealthMetricsStore) {
        this.context = context;
        this.backendRegistry = backendRegistry;
        this.eventStore = eventStore;
        this.workScheduler = workScheduler;
        this.executor = executor;
        this.guard = guard;
        this.clock = clock;
        this.uptimeClock = uptimeClock;
        this.clientHealthMetricsStore = clientHealthMetricsStore;
    }
    
    public EventInternal createMetricsEvent(final TransportBackend transportBackend) {
        final SynchronizationGuard guard = this.guard;
        final ClientHealthMetricsStore clientHealthMetricsStore = this.clientHealthMetricsStore;
        Objects.requireNonNull(clientHealthMetricsStore);
        return transportBackend.decorate(EventInternal.builder().setEventMillis(this.clock.getTime()).setUptimeMillis(this.uptimeClock.getTime()).setTransportName("GDT_CLIENT_METRICS").setEncodedPayload(new EncodedPayload(Encoding.of("proto"), guard.runCriticalSection((SynchronizationGuard.CriticalSection<ClientMetrics>)new j12(clientHealthMetricsStore)).toByteArray())).build());
    }
    
    public boolean isNetworkAvailable() {
        final NetworkInfo activeNetworkInfo = ((ConnectivityManager)this.context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    
    public BackendResponse logAndUpdateState(final TransportContext transportContext, final int n) {
        final TransportBackend value = this.backendRegistry.get(transportContext.getBackendName());
        long max = 0L;
        BackendResponse ok = BackendResponse.ok(0L);
        while (this.guard.runCriticalSection((SynchronizationGuard.CriticalSection<Boolean>)new o12(this, transportContext))) {
            final Iterable iterable = this.guard.runCriticalSection((SynchronizationGuard.CriticalSection<Iterable>)new p12(this, transportContext));
            if (!iterable.iterator().hasNext()) {
                return ok;
            }
            BackendResponse backendResponse;
            if (value == null) {
                Logging.d("Uploader", "Unknown backend for %s, deleting event batch for it...", transportContext);
                backendResponse = BackendResponse.fatalError();
            }
            else {
                final ArrayList events = new ArrayList();
                final Iterator iterator = iterable.iterator();
                while (iterator.hasNext()) {
                    events.add(((PersistedEvent)iterator.next()).getEvent());
                }
                if (transportContext.shouldUploadClientHealthMetrics()) {
                    events.add(this.createMetricsEvent(value));
                }
                backendResponse = value.send(BackendRequest.builder().setEvents(events).setExtras(transportContext.getExtras()).build());
            }
            if (backendResponse.getStatus() == BackendResponse.Status.TRANSIENT_ERROR) {
                this.guard.runCriticalSection((SynchronizationGuard.CriticalSection<Object>)new q12(this, iterable, transportContext, max));
                this.workScheduler.schedule(transportContext, n + 1, true);
                return backendResponse;
            }
            this.guard.runCriticalSection((SynchronizationGuard.CriticalSection<Object>)new r12(this, iterable));
            if (backendResponse.getStatus() == BackendResponse.Status.OK) {
                final long n2 = max = Math.max(max, backendResponse.getNextRequestWaitMillis());
                ok = backendResponse;
                if (!transportContext.shouldUploadClientHealthMetrics()) {
                    continue;
                }
                this.guard.runCriticalSection((SynchronizationGuard.CriticalSection<Object>)new s12(this));
                max = n2;
                ok = backendResponse;
            }
            else {
                ok = backendResponse;
                if (backendResponse.getStatus() != BackendResponse.Status.INVALID_PAYLOAD) {
                    continue;
                }
                final HashMap hashMap = new HashMap();
                final Iterator iterator2 = iterable.iterator();
                while (iterator2.hasNext()) {
                    final String transportName = ((PersistedEvent)iterator2.next()).getEvent().getTransportName();
                    Integer n3;
                    if (!hashMap.containsKey(transportName)) {
                        n3 = 1;
                    }
                    else {
                        n3 = (int)hashMap.get(transportName) + 1;
                    }
                    hashMap.put(transportName, n3);
                }
                this.guard.runCriticalSection((SynchronizationGuard.CriticalSection<Object>)new t12(this, hashMap));
                ok = backendResponse;
            }
        }
        this.guard.runCriticalSection((SynchronizationGuard.CriticalSection<Object>)new k12(this, transportContext, max));
        return ok;
    }
    
    public void upload(final TransportContext transportContext, final int n, final Runnable runnable) {
        this.executor.execute(new l12(this, transportContext, n, runnable));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime;

import java.util.Arrays;
import com.google.android.datatransport.Priority;

final class AutoValue_TransportContext extends TransportContext
{
    private final String backendName;
    private final byte[] extras;
    private final Priority priority;
    
    private AutoValue_TransportContext(final String backendName, final byte[] extras, final Priority priority) {
        this.backendName = backendName;
        this.extras = extras;
        this.priority = priority;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof TransportContext) {
            final TransportContext transportContext = (TransportContext)o;
            if (this.backendName.equals(transportContext.getBackendName())) {
                final byte[] extras = this.extras;
                byte[] a2;
                if (transportContext instanceof AutoValue_TransportContext) {
                    a2 = ((AutoValue_TransportContext)transportContext).extras;
                }
                else {
                    a2 = transportContext.getExtras();
                }
                if (Arrays.equals(extras, a2) && this.priority.equals(transportContext.getPriority())) {
                    return b;
                }
            }
            b = false;
            return b;
        }
        return false;
    }
    
    @Override
    public String getBackendName() {
        return this.backendName;
    }
    
    @Override
    public byte[] getExtras() {
        return this.extras;
    }
    
    @Override
    public Priority getPriority() {
        return this.priority;
    }
    
    @Override
    public int hashCode() {
        return ((this.backendName.hashCode() ^ 0xF4243) * 1000003 ^ Arrays.hashCode(this.extras)) * 1000003 ^ this.priority.hashCode();
    }
    
    public static final class Builder extends TransportContext.Builder
    {
        private String backendName;
        private byte[] extras;
        private Priority priority;
        
        @Override
        public TransportContext build() {
            final String backendName = this.backendName;
            String string = "";
            if (backendName == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("");
                sb.append(" backendName");
                string = sb.toString();
            }
            String string2 = string;
            if (this.priority == null) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(string);
                sb2.append(" priority");
                string2 = sb2.toString();
            }
            if (string2.isEmpty()) {
                return new AutoValue_TransportContext(this.backendName, this.extras, this.priority, null);
            }
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Missing required properties:");
            sb3.append(string2);
            throw new IllegalStateException(sb3.toString());
        }
        
        @Override
        public TransportContext.Builder setBackendName(final String backendName) {
            if (backendName != null) {
                this.backendName = backendName;
                return this;
            }
            throw new NullPointerException("Null backendName");
        }
        
        @Override
        public TransportContext.Builder setExtras(final byte[] extras) {
            this.extras = extras;
            return this;
        }
        
        @Override
        public TransportContext.Builder setPriority(final Priority priority) {
            if (priority != null) {
                this.priority = priority;
                return this;
            }
            throw new NullPointerException("Null priority");
        }
    }
}

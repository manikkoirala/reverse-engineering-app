// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime;

import java.util.Arrays;
import com.google.android.datatransport.Encoding;

public final class EncodedPayload
{
    private final byte[] bytes;
    private final Encoding encoding;
    
    public EncodedPayload(final Encoding encoding, final byte[] bytes) {
        if (encoding == null) {
            throw new NullPointerException("encoding is null");
        }
        if (bytes != null) {
            this.encoding = encoding;
            this.bytes = bytes;
            return;
        }
        throw new NullPointerException("bytes is null");
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EncodedPayload)) {
            return false;
        }
        final EncodedPayload encodedPayload = (EncodedPayload)o;
        return this.encoding.equals(encodedPayload.encoding) && Arrays.equals(this.bytes, encodedPayload.bytes);
    }
    
    public byte[] getBytes() {
        return this.bytes;
    }
    
    public Encoding getEncoding() {
        return this.encoding;
    }
    
    @Override
    public int hashCode() {
        return (this.encoding.hashCode() ^ 0xF4243) * 1000003 ^ Arrays.hashCode(this.bytes);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("EncodedPayload{encoding=");
        sb.append(this.encoding);
        sb.append(", bytes=[...]}");
        return sb.toString();
    }
}

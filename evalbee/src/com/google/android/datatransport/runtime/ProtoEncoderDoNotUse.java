// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime;

import com.google.android.datatransport.runtime.firebase.transport.ClientMetrics;
import java.io.OutputStream;

public abstract class ProtoEncoderDoNotUse
{
    private static final o91 ENCODER;
    
    static {
        ENCODER = o91.a().d(AutoProtoEncoderDoNotUseEncoder.CONFIG).c();
    }
    
    private ProtoEncoderDoNotUse() {
    }
    
    public static void encode(final Object o, final OutputStream outputStream) {
        ProtoEncoderDoNotUse.ENCODER.b(o, outputStream);
    }
    
    public static byte[] encode(final Object o) {
        return ProtoEncoderDoNotUse.ENCODER.c(o);
    }
    
    public abstract ClientMetrics getClientMetrics();
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime;

import com.google.android.datatransport.Transformer;
import com.google.android.datatransport.Event;
import com.google.android.datatransport.Encoding;

final class AutoValue_SendRequest extends SendRequest
{
    private final Encoding encoding;
    private final Event<?> event;
    private final Transformer<?, byte[]> transformer;
    private final TransportContext transportContext;
    private final String transportName;
    
    private AutoValue_SendRequest(final TransportContext transportContext, final String transportName, final Event<?> event, final Transformer<?, byte[]> transformer, final Encoding encoding) {
        this.transportContext = transportContext;
        this.transportName = transportName;
        this.event = event;
        this.transformer = transformer;
        this.encoding = encoding;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof SendRequest) {
            final SendRequest sendRequest = (SendRequest)o;
            if (!this.transportContext.equals(sendRequest.getTransportContext()) || !this.transportName.equals(sendRequest.getTransportName()) || !this.event.equals(sendRequest.getEvent()) || !this.transformer.equals(sendRequest.getTransformer()) || !this.encoding.equals(sendRequest.getEncoding())) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public Encoding getEncoding() {
        return this.encoding;
    }
    
    @Override
    public Event<?> getEvent() {
        return this.event;
    }
    
    @Override
    public Transformer<?, byte[]> getTransformer() {
        return this.transformer;
    }
    
    @Override
    public TransportContext getTransportContext() {
        return this.transportContext;
    }
    
    @Override
    public String getTransportName() {
        return this.transportName;
    }
    
    @Override
    public int hashCode() {
        return ((((this.transportContext.hashCode() ^ 0xF4243) * 1000003 ^ this.transportName.hashCode()) * 1000003 ^ this.event.hashCode()) * 1000003 ^ this.transformer.hashCode()) * 1000003 ^ this.encoding.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SendRequest{transportContext=");
        sb.append(this.transportContext);
        sb.append(", transportName=");
        sb.append(this.transportName);
        sb.append(", event=");
        sb.append(this.event);
        sb.append(", transformer=");
        sb.append(this.transformer);
        sb.append(", encoding=");
        sb.append(this.encoding);
        sb.append("}");
        return sb.toString();
    }
    
    public static final class Builder extends SendRequest.Builder
    {
        private Encoding encoding;
        private Event<?> event;
        private Transformer<?, byte[]> transformer;
        private TransportContext transportContext;
        private String transportName;
        
        @Override
        public SendRequest build() {
            final TransportContext transportContext = this.transportContext;
            String string = "";
            if (transportContext == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("");
                sb.append(" transportContext");
                string = sb.toString();
            }
            String string2 = string;
            if (this.transportName == null) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(string);
                sb2.append(" transportName");
                string2 = sb2.toString();
            }
            String string3 = string2;
            if (this.event == null) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(string2);
                sb3.append(" event");
                string3 = sb3.toString();
            }
            String string4 = string3;
            if (this.transformer == null) {
                final StringBuilder sb4 = new StringBuilder();
                sb4.append(string3);
                sb4.append(" transformer");
                string4 = sb4.toString();
            }
            String string5 = string4;
            if (this.encoding == null) {
                final StringBuilder sb5 = new StringBuilder();
                sb5.append(string4);
                sb5.append(" encoding");
                string5 = sb5.toString();
            }
            if (string5.isEmpty()) {
                return new AutoValue_SendRequest(this.transportContext, this.transportName, this.event, this.transformer, this.encoding, null);
            }
            final StringBuilder sb6 = new StringBuilder();
            sb6.append("Missing required properties:");
            sb6.append(string5);
            throw new IllegalStateException(sb6.toString());
        }
        
        @Override
        public SendRequest.Builder setEncoding(final Encoding encoding) {
            if (encoding != null) {
                this.encoding = encoding;
                return this;
            }
            throw new NullPointerException("Null encoding");
        }
        
        @Override
        public SendRequest.Builder setEvent(final Event<?> event) {
            if (event != null) {
                this.event = event;
                return this;
            }
            throw new NullPointerException("Null event");
        }
        
        @Override
        public SendRequest.Builder setTransformer(final Transformer<?, byte[]> transformer) {
            if (transformer != null) {
                this.transformer = transformer;
                return this;
            }
            throw new NullPointerException("Null transformer");
        }
        
        @Override
        public SendRequest.Builder setTransportContext(final TransportContext transportContext) {
            if (transportContext != null) {
                this.transportContext = transportContext;
                return this;
            }
            throw new NullPointerException("Null transportContext");
        }
        
        @Override
        public SendRequest.Builder setTransportName(final String transportName) {
            if (transportName != null) {
                this.transportName = transportName;
                return this;
            }
            throw new NullPointerException("Null transportName");
        }
    }
}

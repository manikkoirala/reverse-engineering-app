// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.dagger.internal;

import com.google.android.datatransport.runtime.dagger.MembersInjector;

public final class MembersInjectors
{
    private MembersInjectors() {
    }
    
    public static <T> MembersInjector<T> noOp() {
        return (MembersInjector<T>)NoOpMembersInjector.INSTANCE;
    }
    
    public enum NoOpMembersInjector implements MembersInjector<Object>
    {
        private static final NoOpMembersInjector[] $VALUES;
        
        INSTANCE;
        
        @Override
        public void injectMembers(final Object o) {
            Preconditions.checkNotNull(o, "Cannot inject members into a null reference");
        }
    }
}

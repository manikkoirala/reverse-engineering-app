// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.dagger.internal;

import java.util.Map;
import com.google.android.datatransport.runtime.dagger.Lazy;

public final class MapProviderFactory<K, V> extends AbstractMapFactory<K, V, q91> implements Lazy<Map<K, q91>>
{
    private MapProviderFactory(final Map<K, q91> map) {
        super(map);
    }
    
    public static <K, V> Builder<K, V> builder(final int n) {
        return new Builder<K, V>(n, null);
    }
    
    @Override
    public Map<K, q91> get() {
        return this.contributingMap();
    }
    
    public static final class Builder<K, V> extends AbstractMapFactory.Builder<K, V, q91>
    {
        private Builder(final int n) {
            super(n);
        }
        
        public MapProviderFactory<K, V> build() {
            return new MapProviderFactory<K, V>(super.map, null);
        }
        
        public Builder<K, V> put(final K k, final q91 q91) {
            super.put(k, q91);
            return this;
        }
        
        public Builder<K, V> putAll(final q91 q91) {
            super.putAll(q91);
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.dagger.internal;

import java.util.HashSet;
import java.util.Collections;
import java.util.Set;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;

public final class SetBuilder<T>
{
    private static final String SET_CONTRIBUTIONS_CANNOT_BE_NULL = "Set contributions cannot be null";
    private final List<T> contributions;
    
    private SetBuilder(final int initialCapacity) {
        this.contributions = new ArrayList<T>(initialCapacity);
    }
    
    public static <T> SetBuilder<T> newSetBuilder(final int n) {
        return new SetBuilder<T>(n);
    }
    
    public SetBuilder<T> add(final T t) {
        this.contributions.add(Preconditions.checkNotNull(t, "Set contributions cannot be null"));
        return this;
    }
    
    public SetBuilder<T> addAll(final Collection<? extends T> collection) {
        final Iterator<? extends T> iterator = collection.iterator();
        while (iterator.hasNext()) {
            Preconditions.checkNotNull((Object)iterator.next(), "Set contributions cannot be null");
        }
        this.contributions.addAll(collection);
        return this;
    }
    
    public Set<T> build() {
        final int size = this.contributions.size();
        if (size == 0) {
            return Collections.emptySet();
        }
        if (size != 1) {
            return Collections.unmodifiableSet((Set<? extends T>)new HashSet<T>((Collection<? extends T>)this.contributions));
        }
        return Collections.singleton(this.contributions.get(0));
    }
}

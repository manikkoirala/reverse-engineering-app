// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.dagger.internal;

import com.google.android.datatransport.runtime.dagger.Lazy;

public final class DoubleCheck<T> implements q91, Lazy<T>
{
    static final boolean $assertionsDisabled = false;
    private static final Object UNINITIALIZED;
    private volatile Object instance;
    private volatile q91 provider;
    
    static {
        UNINITIALIZED = new Object();
    }
    
    private DoubleCheck(final q91 provider) {
        this.instance = DoubleCheck.UNINITIALIZED;
        this.provider = provider;
    }
    
    public static <P extends q91, T> Lazy<T> lazy(final P p) {
        if (p instanceof Lazy) {
            return (Lazy)p;
        }
        return new DoubleCheck<T>(Preconditions.checkNotNull(p));
    }
    
    public static <P extends q91, T> q91 provider(final P p) {
        Preconditions.checkNotNull(p);
        if (p instanceof DoubleCheck) {
            return p;
        }
        return (q91)new DoubleCheck(p);
    }
    
    public static Object reentrantCheck(final Object obj, final Object obj2) {
        if (obj != DoubleCheck.UNINITIALIZED && !(obj instanceof MemoizedSentinel) && obj != obj2) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Scoped provider was invoked recursively returning different results: ");
            sb.append(obj);
            sb.append(" & ");
            sb.append(obj2);
            sb.append(". This is likely due to a circular dependency.");
            throw new IllegalStateException(sb.toString());
        }
        return obj2;
    }
    
    public T get() {
        final Object instance = this.instance;
        final Object uninitialized = DoubleCheck.UNINITIALIZED;
        final T t = (T)instance;
        if (instance == uninitialized) {
            synchronized (this) {
                if (this.instance == uninitialized) {
                    this.instance = reentrantCheck(this.instance, this.provider.get());
                    this.provider = null;
                }
            }
        }
        return t;
    }
}

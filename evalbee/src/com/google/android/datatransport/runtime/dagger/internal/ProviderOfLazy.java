// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.dagger.internal;

import com.google.android.datatransport.runtime.dagger.Lazy;

public final class ProviderOfLazy<T> implements q91
{
    static final boolean $assertionsDisabled = false;
    private final q91 provider;
    
    private ProviderOfLazy(final q91 provider) {
        this.provider = provider;
    }
    
    public static <T> q91 create(final q91 q91) {
        return (q91)new ProviderOfLazy(Preconditions.checkNotNull(q91));
    }
    
    public Lazy<T> get() {
        return DoubleCheck.lazy(this.provider);
    }
}

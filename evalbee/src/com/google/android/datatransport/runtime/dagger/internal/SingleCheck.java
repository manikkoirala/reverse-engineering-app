// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.dagger.internal;

public final class SingleCheck<T> implements q91
{
    static final boolean $assertionsDisabled = false;
    private static final Object UNINITIALIZED;
    private volatile Object instance;
    private volatile q91 provider;
    
    static {
        UNINITIALIZED = new Object();
    }
    
    private SingleCheck(final q91 provider) {
        this.instance = SingleCheck.UNINITIALIZED;
        this.provider = provider;
    }
    
    public static <P extends q91, T> q91 provider(final P p) {
        if (!(p instanceof SingleCheck) && !(p instanceof DoubleCheck)) {
            return (q91)new SingleCheck(Preconditions.checkNotNull(p));
        }
        return p;
    }
    
    public T get() {
        Object instance;
        if ((instance = this.instance) == SingleCheck.UNINITIALIZED) {
            final q91 provider = this.provider;
            if (provider == null) {
                instance = this.instance;
            }
            else {
                instance = provider.get();
                this.instance = instance;
                this.provider = null;
            }
        }
        return (T)instance;
    }
}

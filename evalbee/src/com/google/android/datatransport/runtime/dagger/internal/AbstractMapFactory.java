// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.dagger.internal;

import java.util.LinkedHashMap;
import java.util.Collections;
import java.util.Map;

abstract class AbstractMapFactory<K, V, V2> implements Factory<Map<K, V2>>
{
    private final Map<K, q91> contributingMap;
    
    public AbstractMapFactory(final Map<K, q91> m) {
        this.contributingMap = Collections.unmodifiableMap((Map<? extends K, ? extends q91>)m);
    }
    
    public static /* synthetic */ Map access$000(final AbstractMapFactory abstractMapFactory) {
        return abstractMapFactory.contributingMap;
    }
    
    public final Map<K, q91> contributingMap() {
        return this.contributingMap;
    }
    
    public abstract static class Builder<K, V, V2>
    {
        final LinkedHashMap<K, q91> map;
        
        public Builder(final int n) {
            this.map = DaggerCollections.newLinkedHashMapWithExpectedSize(n);
        }
        
        public Builder<K, V, V2> put(final K k, final q91 q91) {
            this.map.put(Preconditions.checkNotNull(k, "key"), Preconditions.checkNotNull(q91, "provider"));
            return this;
        }
        
        public Builder<K, V, V2> putAll(final q91 q91) {
            if (q91 instanceof DelegateFactory) {
                return this.putAll(((DelegateFactory)q91).getDelegate());
            }
            this.map.putAll((Map<?, ?>)AbstractMapFactory.access$000((AbstractMapFactory<Object, Object, Object>)q91));
            return this;
        }
    }
}

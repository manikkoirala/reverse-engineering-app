// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.dagger.internal;

import java.util.Iterator;
import java.util.HashSet;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public final class SetFactory<T> implements Factory<Set<T>>
{
    private static final Factory<Set<Object>> EMPTY_FACTORY;
    private final List<q91> collectionProviders;
    private final List<q91> individualProviders;
    
    static {
        EMPTY_FACTORY = InstanceFactory.create(Collections.emptySet());
    }
    
    private SetFactory(final List<q91> individualProviders, final List<q91> collectionProviders) {
        this.individualProviders = individualProviders;
        this.collectionProviders = collectionProviders;
    }
    
    public static <T> Builder<T> builder(final int n, final int n2) {
        return new Builder<T>(n, n2, null);
    }
    
    public static <T> Factory<Set<T>> empty() {
        return (Factory<Set<T>>)SetFactory.EMPTY_FACTORY;
    }
    
    @Override
    public Set<T> get() {
        int size = this.individualProviders.size();
        final ArrayList list = new ArrayList(this.collectionProviders.size());
        final int size2 = this.collectionProviders.size();
        final int n = 0;
        for (int i = 0; i < size2; ++i) {
            final Collection collection = (Collection)this.collectionProviders.get(i).get();
            size += collection.size();
            list.add(collection);
        }
        final HashSet<Object> hashSetWithExpectedSize = DaggerCollections.newHashSetWithExpectedSize(size);
        for (int size3 = this.individualProviders.size(), j = 0; j < size3; ++j) {
            hashSetWithExpectedSize.add(Preconditions.checkNotNull(this.individualProviders.get(j).get()));
        }
        for (int size4 = list.size(), k = n; k < size4; ++k) {
            final Iterator iterator = ((Collection)list.get(k)).iterator();
            while (iterator.hasNext()) {
                hashSetWithExpectedSize.add(Preconditions.checkNotNull(iterator.next()));
            }
        }
        return Collections.unmodifiableSet((Set<? extends T>)hashSetWithExpectedSize);
    }
    
    public static final class Builder<T>
    {
        static final boolean $assertionsDisabled = false;
        private final List<q91> collectionProviders;
        private final List<q91> individualProviders;
        
        private Builder(final int n, final int n2) {
            this.individualProviders = DaggerCollections.presizedList(n);
            this.collectionProviders = DaggerCollections.presizedList(n2);
        }
        
        public Builder<T> addCollectionProvider(final q91 q91) {
            this.collectionProviders.add(q91);
            return this;
        }
        
        public Builder<T> addProvider(final q91 q91) {
            this.individualProviders.add(q91);
            return this;
        }
        
        public SetFactory<T> build() {
            return new SetFactory<T>(this.individualProviders, this.collectionProviders, null);
        }
    }
}

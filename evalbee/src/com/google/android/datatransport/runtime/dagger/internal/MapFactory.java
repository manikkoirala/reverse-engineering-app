// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.dagger.internal;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Collections;

public final class MapFactory<K, V> extends AbstractMapFactory<K, V, V>
{
    private static final q91 EMPTY;
    
    static {
        EMPTY = (q91)InstanceFactory.create(Collections.emptyMap());
    }
    
    private MapFactory(final Map<K, q91> map) {
        super(map);
    }
    
    public static <K, V> Builder<K, V> builder(final int n) {
        return new Builder<K, V>(n, null);
    }
    
    public static <K, V> q91 emptyMapProvider() {
        return MapFactory.EMPTY;
    }
    
    @Override
    public Map<K, V> get() {
        final LinkedHashMap<Object, Object> linkedHashMapWithExpectedSize = DaggerCollections.newLinkedHashMapWithExpectedSize(this.contributingMap().size());
        for (final Map.Entry<Object, V> entry : this.contributingMap().entrySet()) {
            linkedHashMapWithExpectedSize.put(entry.getKey(), ((q91)entry.getValue()).get());
        }
        return Collections.unmodifiableMap((Map<? extends K, ? extends V>)linkedHashMapWithExpectedSize);
    }
    
    public static final class Builder<K, V> extends AbstractMapFactory.Builder<K, V, V>
    {
        private Builder(final int n) {
            super(n);
        }
        
        public MapFactory<K, V> build() {
            return new MapFactory<K, V>(super.map, null);
        }
        
        public Builder<K, V> put(final K k, final q91 q91) {
            super.put(k, q91);
            return this;
        }
        
        public Builder<K, V> putAll(final q91 q91) {
            super.putAll(q91);
            return this;
        }
    }
}

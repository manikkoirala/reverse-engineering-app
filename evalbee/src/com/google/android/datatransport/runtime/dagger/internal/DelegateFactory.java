// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.dagger.internal;

public final class DelegateFactory<T> implements Factory<T>
{
    private q91 delegate;
    
    public static <T> void setDelegate(final q91 q91, final q91 delegate) {
        Preconditions.checkNotNull(delegate);
        final DelegateFactory delegateFactory = (DelegateFactory)q91;
        if (delegateFactory.delegate == null) {
            delegateFactory.delegate = delegate;
            return;
        }
        throw new IllegalStateException();
    }
    
    @Override
    public T get() {
        final q91 delegate = this.delegate;
        if (delegate != null) {
            return (T)delegate.get();
        }
        throw new IllegalStateException();
    }
    
    public q91 getDelegate() {
        return Preconditions.checkNotNull(this.delegate);
    }
    
    @Deprecated
    public void setDelegatedProvider(final q91 q91) {
        setDelegate((q91)this, q91);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.dagger.internal;

public final class Preconditions
{
    private Preconditions() {
    }
    
    public static <T> void checkBuilderRequirement(final T t, final Class<T> clazz) {
        if (t != null) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(clazz.getCanonicalName());
        sb.append(" must be set");
        throw new IllegalStateException(sb.toString());
    }
    
    public static <T> T checkNotNull(final T t) {
        t.getClass();
        return t;
    }
    
    public static <T> T checkNotNull(final T t, final String s) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(s);
    }
    
    public static <T> T checkNotNull(final T t, final String s, final Object obj) {
        if (t != null) {
            return t;
        }
        if (!s.contains("%s")) {
            throw new IllegalArgumentException("errorMessageTemplate has no format specifiers");
        }
        if (s.indexOf("%s") == s.lastIndexOf("%s")) {
            String replacement;
            if (obj instanceof Class) {
                replacement = ((Class)obj).getCanonicalName();
            }
            else {
                replacement = String.valueOf(obj);
            }
            throw new NullPointerException(s.replace("%s", replacement));
        }
        throw new IllegalArgumentException("errorMessageTemplate has more than one format specifier");
    }
}

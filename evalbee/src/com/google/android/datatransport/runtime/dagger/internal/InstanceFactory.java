// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.dagger.internal;

import com.google.android.datatransport.runtime.dagger.Lazy;

public final class InstanceFactory<T> implements Factory<T>, Lazy<T>
{
    private static final InstanceFactory<Object> NULL_INSTANCE_FACTORY;
    private final T instance;
    
    static {
        NULL_INSTANCE_FACTORY = new InstanceFactory<Object>(null);
    }
    
    private InstanceFactory(final T instance) {
        this.instance = instance;
    }
    
    public static <T> Factory<T> create(final T t) {
        return new InstanceFactory<T>(Preconditions.checkNotNull(t, "instance cannot be null"));
    }
    
    public static <T> Factory<T> createNullable(final T t) {
        InstanceFactory<Object> nullInstanceFactory;
        if (t == null) {
            nullInstanceFactory = nullInstanceFactory();
        }
        else {
            nullInstanceFactory = new InstanceFactory<Object>(t);
        }
        return (Factory<T>)nullInstanceFactory;
    }
    
    private static <T> InstanceFactory<T> nullInstanceFactory() {
        return (InstanceFactory<T>)InstanceFactory.NULL_INSTANCE_FACTORY;
    }
    
    @Override
    public T get() {
        return this.instance;
    }
}

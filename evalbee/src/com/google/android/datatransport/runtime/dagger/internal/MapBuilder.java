// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.dagger.internal;

import java.util.Collections;
import java.util.Map;

public final class MapBuilder<K, V>
{
    private final Map<K, V> contributions;
    
    private MapBuilder(final int n) {
        this.contributions = (Map<K, V>)DaggerCollections.newLinkedHashMapWithExpectedSize(n);
    }
    
    public static <K, V> MapBuilder<K, V> newMapBuilder(final int n) {
        return new MapBuilder<K, V>(n);
    }
    
    public Map<K, V> build() {
        if (this.contributions.size() != 0) {
            return Collections.unmodifiableMap((Map<? extends K, ? extends V>)this.contributions);
        }
        return Collections.emptyMap();
    }
    
    public MapBuilder<K, V> put(final K k, final V v) {
        this.contributions.put(k, v);
        return this;
    }
    
    public MapBuilder<K, V> putAll(final Map<K, V> map) {
        this.contributions.putAll((Map<? extends K, ? extends V>)map);
        return this;
    }
}

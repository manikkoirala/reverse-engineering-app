// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.time;

import com.google.android.datatransport.runtime.dagger.internal.Preconditions;
import com.google.android.datatransport.runtime.dagger.internal.Factory;

public final class TimeModule_UptimeClockFactory implements Factory<Clock>
{
    public static TimeModule_UptimeClockFactory create() {
        return InstanceHolder.access$000();
    }
    
    public static Clock uptimeClock() {
        return Preconditions.checkNotNull(TimeModule.uptimeClock(), "Cannot return null from a non-@Nullable @Provides method");
    }
    
    @Override
    public Clock get() {
        return uptimeClock();
    }
    
    public static final class InstanceHolder
    {
        private static final TimeModule_UptimeClockFactory INSTANCE;
        
        static {
            INSTANCE = new TimeModule_UptimeClockFactory();
        }
        
        private InstanceHolder() {
        }
        
        public static /* synthetic */ TimeModule_UptimeClockFactory access$000() {
            return InstanceHolder.INSTANCE;
        }
    }
}

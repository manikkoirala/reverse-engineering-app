// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.time;

import com.google.android.datatransport.runtime.dagger.internal.Preconditions;
import com.google.android.datatransport.runtime.dagger.internal.Factory;

public final class TimeModule_EventClockFactory implements Factory<Clock>
{
    public static TimeModule_EventClockFactory create() {
        return InstanceHolder.access$000();
    }
    
    public static Clock eventClock() {
        return Preconditions.checkNotNull(TimeModule.eventClock(), "Cannot return null from a non-@Nullable @Provides method");
    }
    
    @Override
    public Clock get() {
        return eventClock();
    }
    
    public static final class InstanceHolder
    {
        private static final TimeModule_EventClockFactory INSTANCE;
        
        static {
            INSTANCE = new TimeModule_EventClockFactory();
        }
        
        private InstanceHolder() {
        }
        
        public static /* synthetic */ TimeModule_EventClockFactory access$000() {
            return InstanceHolder.INSTANCE;
        }
    }
}

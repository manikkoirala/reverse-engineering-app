// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.time;

import java.util.concurrent.atomic.AtomicLong;

public class TestClock implements Clock
{
    private final AtomicLong timestamp;
    
    public TestClock(final long initialValue) {
        this.timestamp = new AtomicLong(initialValue);
    }
    
    public void advance(final long delta) {
        if (delta >= 0L) {
            this.timestamp.addAndGet(delta);
            return;
        }
        throw new IllegalArgumentException("cannot advance time backwards.");
    }
    
    @Override
    public long getTime() {
        return this.timestamp.get();
    }
    
    public void tick() {
        this.advance(1L);
    }
}

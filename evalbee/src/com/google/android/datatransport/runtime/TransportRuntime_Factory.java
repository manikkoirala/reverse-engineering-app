// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime;

import com.google.android.datatransport.runtime.scheduling.jobscheduling.WorkInitializer;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.Uploader;
import com.google.android.datatransport.runtime.scheduling.Scheduler;
import com.google.android.datatransport.runtime.time.Clock;
import com.google.android.datatransport.runtime.dagger.internal.Factory;

public final class TransportRuntime_Factory implements Factory<TransportRuntime>
{
    private final q91 eventClockProvider;
    private final q91 initializerProvider;
    private final q91 schedulerProvider;
    private final q91 uploaderProvider;
    private final q91 uptimeClockProvider;
    
    public TransportRuntime_Factory(final q91 eventClockProvider, final q91 uptimeClockProvider, final q91 schedulerProvider, final q91 uploaderProvider, final q91 initializerProvider) {
        this.eventClockProvider = eventClockProvider;
        this.uptimeClockProvider = uptimeClockProvider;
        this.schedulerProvider = schedulerProvider;
        this.uploaderProvider = uploaderProvider;
        this.initializerProvider = initializerProvider;
    }
    
    public static TransportRuntime_Factory create(final q91 q91, final q91 q92, final q91 q93, final q91 q94, final q91 q95) {
        return new TransportRuntime_Factory(q91, q92, q93, q94, q95);
    }
    
    public static TransportRuntime newInstance(final Clock clock, final Clock clock2, final Scheduler scheduler, final Uploader uploader, final WorkInitializer workInitializer) {
        return new TransportRuntime(clock, clock2, scheduler, uploader, workInitializer);
    }
    
    @Override
    public TransportRuntime get() {
        return newInstance((Clock)this.eventClockProvider.get(), (Clock)this.uptimeClockProvider.get(), (Scheduler)this.schedulerProvider.get(), (Uploader)this.uploaderProvider.get(), (WorkInitializer)this.initializerProvider.get());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime;

import com.google.android.datatransport.runtime.logging.Logging;
import com.google.android.datatransport.Priority;
import com.google.android.datatransport.Transport;

public final class ForcedSender
{
    private static final String LOG_TAG = "ForcedSender";
    
    private ForcedSender() {
    }
    
    public static void sendBlocking(final Transport<?> transport, final Priority priority) {
        if (transport instanceof TransportImpl) {
            TransportRuntime.getInstance().getUploader().logAndUpdateState(((TransportImpl)transport).getTransportContext().withPriority(priority), 1);
        }
        else {
            Logging.w("ForcedSender", "Expected instance of `TransportImpl`, got `%s`.", transport);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime;

import com.google.android.datatransport.runtime.dagger.Provides;
import java.util.concurrent.Executors;
import java.util.concurrent.Executor;
import com.google.android.datatransport.runtime.dagger.Module;

@Module
abstract class ExecutionModule
{
    public ExecutionModule() {
    }
    
    @Provides
    public static Executor executor() {
        return new SafeLoggingExecutor(Executors.newSingleThreadExecutor());
    }
}

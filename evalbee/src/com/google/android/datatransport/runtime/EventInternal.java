// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime;

import com.google.auto.value.AutoValue$Builder;
import java.util.Collections;
import java.util.Map;
import java.util.HashMap;
import com.google.auto.value.AutoValue;

@AutoValue
public abstract class EventInternal
{
    public static Builder builder() {
        return new AutoValue_EventInternal.Builder().setAutoMetadata(new HashMap<String, String>());
    }
    
    public final String get(String s) {
        if ((s = this.getAutoMetadata().get(s)) == null) {
            s = "";
        }
        return s;
    }
    
    public abstract Map<String, String> getAutoMetadata();
    
    public abstract Integer getCode();
    
    public abstract EncodedPayload getEncodedPayload();
    
    public abstract long getEventMillis();
    
    public final int getInteger(String s) {
        s = this.getAutoMetadata().get(s);
        int intValue;
        if (s == null) {
            intValue = 0;
        }
        else {
            intValue = Integer.valueOf(s);
        }
        return intValue;
    }
    
    public final long getLong(String s) {
        s = this.getAutoMetadata().get(s);
        long longValue;
        if (s == null) {
            longValue = 0L;
        }
        else {
            longValue = Long.valueOf(s);
        }
        return longValue;
    }
    
    public final Map<String, String> getMetadata() {
        return Collections.unmodifiableMap((Map<? extends String, ? extends String>)this.getAutoMetadata());
    }
    
    public final String getOrDefault(String s, String s2) {
        s = this.getAutoMetadata().get(s);
        if (s != null) {
            s2 = s;
        }
        return s2;
    }
    
    @Deprecated
    public byte[] getPayload() {
        return this.getEncodedPayload().getBytes();
    }
    
    public abstract String getTransportName();
    
    public abstract long getUptimeMillis();
    
    public Builder toBuilder() {
        return new AutoValue_EventInternal.Builder().setTransportName(this.getTransportName()).setCode(this.getCode()).setEncodedPayload(this.getEncodedPayload()).setEventMillis(this.getEventMillis()).setUptimeMillis(this.getUptimeMillis()).setAutoMetadata(new HashMap<String, String>(this.getAutoMetadata()));
    }
    
    @AutoValue$Builder
    public abstract static class Builder
    {
        public final Builder addMetadata(final String s, final int i) {
            this.getAutoMetadata().put(s, String.valueOf(i));
            return this;
        }
        
        public final Builder addMetadata(final String s, final long l) {
            this.getAutoMetadata().put(s, String.valueOf(l));
            return this;
        }
        
        public final Builder addMetadata(final String s, final String s2) {
            this.getAutoMetadata().put(s, s2);
            return this;
        }
        
        public abstract EventInternal build();
        
        public abstract Map<String, String> getAutoMetadata();
        
        public abstract Builder setAutoMetadata(final Map<String, String> p0);
        
        public abstract Builder setCode(final Integer p0);
        
        public abstract Builder setEncodedPayload(final EncodedPayload p0);
        
        public abstract Builder setEventMillis(final long p0);
        
        public abstract Builder setTransportName(final String p0);
        
        public abstract Builder setUptimeMillis(final long p0);
    }
}

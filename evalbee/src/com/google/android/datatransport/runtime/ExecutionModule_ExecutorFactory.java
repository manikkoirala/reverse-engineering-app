// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime;

import com.google.android.datatransport.runtime.dagger.internal.Preconditions;
import java.util.concurrent.Executor;
import com.google.android.datatransport.runtime.dagger.internal.Factory;

public final class ExecutionModule_ExecutorFactory implements Factory<Executor>
{
    public static ExecutionModule_ExecutorFactory create() {
        return InstanceHolder.access$000();
    }
    
    public static Executor executor() {
        return Preconditions.checkNotNull(ExecutionModule.executor(), "Cannot return null from a non-@Nullable @Provides method");
    }
    
    @Override
    public Executor get() {
        return executor();
    }
    
    public static final class InstanceHolder
    {
        private static final ExecutionModule_ExecutorFactory INSTANCE;
        
        static {
            INSTANCE = new ExecutionModule_ExecutorFactory();
        }
        
        private InstanceHolder() {
        }
        
        public static /* synthetic */ ExecutionModule_ExecutorFactory access$000() {
            return InstanceHolder.INSTANCE;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime;

import java.util.Map;

final class AutoValue_EventInternal extends EventInternal
{
    private final Map<String, String> autoMetadata;
    private final Integer code;
    private final EncodedPayload encodedPayload;
    private final long eventMillis;
    private final String transportName;
    private final long uptimeMillis;
    
    private AutoValue_EventInternal(final String transportName, final Integer code, final EncodedPayload encodedPayload, final long eventMillis, final long uptimeMillis, final Map<String, String> autoMetadata) {
        this.transportName = transportName;
        this.code = code;
        this.encodedPayload = encodedPayload;
        this.eventMillis = eventMillis;
        this.uptimeMillis = uptimeMillis;
        this.autoMetadata = autoMetadata;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof EventInternal) {
            final EventInternal eventInternal = (EventInternal)o;
            if (this.transportName.equals(eventInternal.getTransportName())) {
                final Integer code = this.code;
                if (code == null) {
                    if (eventInternal.getCode() != null) {
                        return false;
                    }
                }
                else if (!code.equals(eventInternal.getCode())) {
                    return false;
                }
                if (this.encodedPayload.equals(eventInternal.getEncodedPayload()) && this.eventMillis == eventInternal.getEventMillis() && this.uptimeMillis == eventInternal.getUptimeMillis() && this.autoMetadata.equals(eventInternal.getAutoMetadata())) {
                    return b;
                }
            }
            b = false;
            return b;
        }
        return false;
    }
    
    @Override
    public Map<String, String> getAutoMetadata() {
        return this.autoMetadata;
    }
    
    @Override
    public Integer getCode() {
        return this.code;
    }
    
    @Override
    public EncodedPayload getEncodedPayload() {
        return this.encodedPayload;
    }
    
    @Override
    public long getEventMillis() {
        return this.eventMillis;
    }
    
    @Override
    public String getTransportName() {
        return this.transportName;
    }
    
    @Override
    public long getUptimeMillis() {
        return this.uptimeMillis;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.transportName.hashCode();
        final Integer code = this.code;
        int hashCode2;
        if (code == null) {
            hashCode2 = 0;
        }
        else {
            hashCode2 = code.hashCode();
        }
        final int hashCode3 = this.encodedPayload.hashCode();
        final long eventMillis = this.eventMillis;
        final int n = (int)(eventMillis ^ eventMillis >>> 32);
        final long uptimeMillis = this.uptimeMillis;
        return (((((hashCode ^ 0xF4243) * 1000003 ^ hashCode2) * 1000003 ^ hashCode3) * 1000003 ^ n) * 1000003 ^ (int)(uptimeMillis ^ uptimeMillis >>> 32)) * 1000003 ^ this.autoMetadata.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("EventInternal{transportName=");
        sb.append(this.transportName);
        sb.append(", code=");
        sb.append(this.code);
        sb.append(", encodedPayload=");
        sb.append(this.encodedPayload);
        sb.append(", eventMillis=");
        sb.append(this.eventMillis);
        sb.append(", uptimeMillis=");
        sb.append(this.uptimeMillis);
        sb.append(", autoMetadata=");
        sb.append(this.autoMetadata);
        sb.append("}");
        return sb.toString();
    }
    
    public static final class Builder extends EventInternal.Builder
    {
        private Map<String, String> autoMetadata;
        private Integer code;
        private EncodedPayload encodedPayload;
        private Long eventMillis;
        private String transportName;
        private Long uptimeMillis;
        
        @Override
        public EventInternal build() {
            final String transportName = this.transportName;
            String string = "";
            if (transportName == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("");
                sb.append(" transportName");
                string = sb.toString();
            }
            String string2 = string;
            if (this.encodedPayload == null) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(string);
                sb2.append(" encodedPayload");
                string2 = sb2.toString();
            }
            String string3 = string2;
            if (this.eventMillis == null) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(string2);
                sb3.append(" eventMillis");
                string3 = sb3.toString();
            }
            String string4 = string3;
            if (this.uptimeMillis == null) {
                final StringBuilder sb4 = new StringBuilder();
                sb4.append(string3);
                sb4.append(" uptimeMillis");
                string4 = sb4.toString();
            }
            String string5 = string4;
            if (this.autoMetadata == null) {
                final StringBuilder sb5 = new StringBuilder();
                sb5.append(string4);
                sb5.append(" autoMetadata");
                string5 = sb5.toString();
            }
            if (string5.isEmpty()) {
                return new AutoValue_EventInternal(this.transportName, this.code, this.encodedPayload, this.eventMillis, this.uptimeMillis, this.autoMetadata, null);
            }
            final StringBuilder sb6 = new StringBuilder();
            sb6.append("Missing required properties:");
            sb6.append(string5);
            throw new IllegalStateException(sb6.toString());
        }
        
        @Override
        public Map<String, String> getAutoMetadata() {
            final Map<String, String> autoMetadata = this.autoMetadata;
            if (autoMetadata != null) {
                return autoMetadata;
            }
            throw new IllegalStateException("Property \"autoMetadata\" has not been set");
        }
        
        @Override
        public EventInternal.Builder setAutoMetadata(final Map<String, String> autoMetadata) {
            if (autoMetadata != null) {
                this.autoMetadata = autoMetadata;
                return this;
            }
            throw new NullPointerException("Null autoMetadata");
        }
        
        @Override
        public EventInternal.Builder setCode(final Integer code) {
            this.code = code;
            return this;
        }
        
        @Override
        public EventInternal.Builder setEncodedPayload(final EncodedPayload encodedPayload) {
            if (encodedPayload != null) {
                this.encodedPayload = encodedPayload;
                return this;
            }
            throw new NullPointerException("Null encodedPayload");
        }
        
        @Override
        public EventInternal.Builder setEventMillis(final long l) {
            this.eventMillis = l;
            return this;
        }
        
        @Override
        public EventInternal.Builder setTransportName(final String transportName) {
            if (transportName != null) {
                this.transportName = transportName;
                return this;
            }
            throw new NullPointerException("Null transportName");
        }
        
        @Override
        public EventInternal.Builder setUptimeMillis(final long l) {
            this.uptimeMillis = l;
            return this;
        }
    }
}

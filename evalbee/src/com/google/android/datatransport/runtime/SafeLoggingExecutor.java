// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime;

import com.google.android.datatransport.runtime.logging.Logging;
import java.util.concurrent.Executor;

class SafeLoggingExecutor implements Executor
{
    private final Executor delegate;
    
    public SafeLoggingExecutor(final Executor delegate) {
        this.delegate = delegate;
    }
    
    @Override
    public void execute(final Runnable runnable) {
        this.delegate.execute(new SafeLoggingRunnable(runnable));
    }
    
    public static class SafeLoggingRunnable implements Runnable
    {
        private final Runnable delegate;
        
        public SafeLoggingRunnable(final Runnable delegate) {
            this.delegate = delegate;
        }
        
        @Override
        public void run() {
            try {
                this.delegate.run();
            }
            catch (final Exception ex) {
                Logging.e("Executor", "Background execution failure.", ex);
            }
        }
    }
}

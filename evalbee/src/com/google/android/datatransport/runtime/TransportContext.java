// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime;

import com.google.auto.value.AutoValue$Builder;
import android.util.Base64;
import com.google.android.datatransport.Priority;
import com.google.auto.value.AutoValue;

@AutoValue
public abstract class TransportContext
{
    public static Builder builder() {
        return new AutoValue_TransportContext.Builder().setPriority(Priority.DEFAULT);
    }
    
    public abstract String getBackendName();
    
    public abstract byte[] getExtras();
    
    public abstract Priority getPriority();
    
    public boolean shouldUploadClientHealthMetrics() {
        return this.getExtras() != null;
    }
    
    @Override
    public final String toString() {
        final String backendName = this.getBackendName();
        final Priority priority = this.getPriority();
        String encodeToString;
        if (this.getExtras() == null) {
            encodeToString = "";
        }
        else {
            encodeToString = Base64.encodeToString(this.getExtras(), 2);
        }
        return String.format("TransportContext(%s, %s, %s)", backendName, priority, encodeToString);
    }
    
    public TransportContext withPriority(final Priority priority) {
        return builder().setBackendName(this.getBackendName()).setPriority(priority).setExtras(this.getExtras()).build();
    }
    
    @AutoValue$Builder
    public abstract static class Builder
    {
        public abstract TransportContext build();
        
        public abstract Builder setBackendName(final String p0);
        
        public abstract Builder setExtras(final byte[] p0);
        
        public abstract Builder setPriority(final Priority p0);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.util;

import java.util.Iterator;
import android.util.SparseArray;
import com.google.android.datatransport.Priority;
import java.util.HashMap;

public final class PriorityMapping
{
    private static HashMap<Priority, Integer> PRIORITY_INT_MAP;
    private static SparseArray<Priority> PRIORITY_MAP;
    
    static {
        PriorityMapping.PRIORITY_MAP = (SparseArray<Priority>)new SparseArray();
        (PriorityMapping.PRIORITY_INT_MAP = new HashMap<Priority, Integer>()).put(Priority.DEFAULT, 0);
        PriorityMapping.PRIORITY_INT_MAP.put(Priority.VERY_LOW, 1);
        PriorityMapping.PRIORITY_INT_MAP.put(Priority.HIGHEST, 2);
        for (final Priority key : PriorityMapping.PRIORITY_INT_MAP.keySet()) {
            PriorityMapping.PRIORITY_MAP.append((int)PriorityMapping.PRIORITY_INT_MAP.get(key), (Object)key);
        }
    }
    
    public static int toInt(final Priority priority) {
        final Integer n = PriorityMapping.PRIORITY_INT_MAP.get(priority);
        if (n != null) {
            return n;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("PriorityMapping is missing known Priority value ");
        sb.append(priority);
        throw new IllegalStateException(sb.toString());
    }
    
    public static Priority valueOf(final int i) {
        final Priority priority = (Priority)PriorityMapping.PRIORITY_MAP.get(i);
        if (priority != null) {
            return priority;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unknown Priority for value ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
}

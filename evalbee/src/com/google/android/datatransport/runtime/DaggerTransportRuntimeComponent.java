// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime;

import com.google.android.datatransport.runtime.dagger.internal.Preconditions;
import com.google.android.datatransport.runtime.scheduling.persistence.EventStore;
import com.google.android.datatransport.runtime.dagger.internal.Factory;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.WorkInitializer_Factory;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.Uploader_Factory;
import com.google.android.datatransport.runtime.scheduling.DefaultScheduler_Factory;
import com.google.android.datatransport.runtime.scheduling.SchedulingModule_WorkSchedulerFactory;
import com.google.android.datatransport.runtime.scheduling.SchedulingConfigModule_ConfigFactory;
import com.google.android.datatransport.runtime.scheduling.persistence.SQLiteEventStore_Factory;
import com.google.android.datatransport.runtime.scheduling.persistence.EventStoreModule_StoreConfigFactory;
import com.google.android.datatransport.runtime.scheduling.persistence.EventStoreModule_PackageNameFactory;
import com.google.android.datatransport.runtime.scheduling.persistence.SchemaManager_Factory;
import com.google.android.datatransport.runtime.scheduling.persistence.EventStoreModule_SchemaVersionFactory;
import com.google.android.datatransport.runtime.scheduling.persistence.EventStoreModule_DbNameFactory;
import com.google.android.datatransport.runtime.backends.MetadataBackendRegistry_Factory;
import com.google.android.datatransport.runtime.backends.CreationContextFactory_Factory;
import com.google.android.datatransport.runtime.time.TimeModule_UptimeClockFactory;
import com.google.android.datatransport.runtime.time.TimeModule_EventClockFactory;
import com.google.android.datatransport.runtime.dagger.internal.InstanceFactory;
import com.google.android.datatransport.runtime.dagger.internal.DoubleCheck;
import android.content.Context;

final class DaggerTransportRuntimeComponent extends TransportRuntimeComponent
{
    private q91 configProvider;
    private q91 creationContextFactoryProvider;
    private q91 defaultSchedulerProvider;
    private q91 executorProvider;
    private q91 metadataBackendRegistryProvider;
    private q91 packageNameProvider;
    private q91 sQLiteEventStoreProvider;
    private q91 schemaManagerProvider;
    private q91 setApplicationContextProvider;
    private q91 transportRuntimeProvider;
    private q91 uploaderProvider;
    private q91 workInitializerProvider;
    private q91 workSchedulerProvider;
    
    private DaggerTransportRuntimeComponent(final Context context) {
        this.initialize(context);
    }
    
    public static TransportRuntimeComponent.Builder builder() {
        return new Builder(null);
    }
    
    private void initialize(final Context context) {
        this.executorProvider = DoubleCheck.provider(ExecutionModule_ExecutorFactory.create());
        final Factory<Context> create = InstanceFactory.create(context);
        this.setApplicationContextProvider = (q91)create;
        final CreationContextFactory_Factory create2 = CreationContextFactory_Factory.create((q91)create, (q91)TimeModule_EventClockFactory.create(), (q91)TimeModule_UptimeClockFactory.create());
        this.creationContextFactoryProvider = (q91)create2;
        this.metadataBackendRegistryProvider = DoubleCheck.provider(MetadataBackendRegistry_Factory.create(this.setApplicationContextProvider, (q91)create2));
        this.schemaManagerProvider = (q91)SchemaManager_Factory.create(this.setApplicationContextProvider, (q91)EventStoreModule_DbNameFactory.create(), (q91)EventStoreModule_SchemaVersionFactory.create());
        this.packageNameProvider = DoubleCheck.provider(EventStoreModule_PackageNameFactory.create(this.setApplicationContextProvider));
        this.sQLiteEventStoreProvider = DoubleCheck.provider(SQLiteEventStore_Factory.create((q91)TimeModule_EventClockFactory.create(), (q91)TimeModule_UptimeClockFactory.create(), (q91)EventStoreModule_StoreConfigFactory.create(), this.schemaManagerProvider, this.packageNameProvider));
        final SchedulingConfigModule_ConfigFactory create3 = SchedulingConfigModule_ConfigFactory.create((q91)TimeModule_EventClockFactory.create());
        this.configProvider = (q91)create3;
        final SchedulingModule_WorkSchedulerFactory create4 = SchedulingModule_WorkSchedulerFactory.create(this.setApplicationContextProvider, this.sQLiteEventStoreProvider, (q91)create3, (q91)TimeModule_UptimeClockFactory.create());
        this.workSchedulerProvider = (q91)create4;
        final q91 executorProvider = this.executorProvider;
        final q91 metadataBackendRegistryProvider = this.metadataBackendRegistryProvider;
        final q91 sqLiteEventStoreProvider = this.sQLiteEventStoreProvider;
        this.defaultSchedulerProvider = (q91)DefaultScheduler_Factory.create(executorProvider, metadataBackendRegistryProvider, (q91)create4, sqLiteEventStoreProvider, sqLiteEventStoreProvider);
        final q91 setApplicationContextProvider = this.setApplicationContextProvider;
        final q91 metadataBackendRegistryProvider2 = this.metadataBackendRegistryProvider;
        final q91 sqLiteEventStoreProvider2 = this.sQLiteEventStoreProvider;
        this.uploaderProvider = (q91)Uploader_Factory.create(setApplicationContextProvider, metadataBackendRegistryProvider2, sqLiteEventStoreProvider2, this.workSchedulerProvider, this.executorProvider, sqLiteEventStoreProvider2, (q91)TimeModule_EventClockFactory.create(), (q91)TimeModule_UptimeClockFactory.create(), this.sQLiteEventStoreProvider);
        final q91 executorProvider2 = this.executorProvider;
        final q91 sqLiteEventStoreProvider3 = this.sQLiteEventStoreProvider;
        this.workInitializerProvider = (q91)WorkInitializer_Factory.create(executorProvider2, sqLiteEventStoreProvider3, this.workSchedulerProvider, sqLiteEventStoreProvider3);
        this.transportRuntimeProvider = DoubleCheck.provider(TransportRuntime_Factory.create((q91)TimeModule_EventClockFactory.create(), (q91)TimeModule_UptimeClockFactory.create(), this.defaultSchedulerProvider, this.uploaderProvider, this.workInitializerProvider));
    }
    
    @Override
    public EventStore getEventStore() {
        return (EventStore)this.sQLiteEventStoreProvider.get();
    }
    
    @Override
    public TransportRuntime getTransportRuntime() {
        return (TransportRuntime)this.transportRuntimeProvider.get();
    }
    
    public static final class Builder implements TransportRuntimeComponent.Builder
    {
        private Context setApplicationContext;
        
        private Builder() {
        }
        
        @Override
        public TransportRuntimeComponent build() {
            Preconditions.checkBuilderRequirement(this.setApplicationContext, Context.class);
            return new DaggerTransportRuntimeComponent(this.setApplicationContext, null);
        }
        
        public Builder setApplicationContext(final Context context) {
            this.setApplicationContext = Preconditions.checkNotNull(context);
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime;

import java.lang.annotation.Annotation;
import com.google.firebase.encoders.proto.a;
import com.google.android.datatransport.runtime.firebase.transport.StorageMetrics;
import com.google.android.datatransport.runtime.firebase.transport.GlobalMetrics;
import com.google.android.datatransport.runtime.firebase.transport.LogEventDropped;
import com.google.android.datatransport.runtime.firebase.transport.LogSourceMetrics;
import com.google.android.datatransport.runtime.firebase.transport.TimeWindow;
import com.google.android.datatransport.runtime.firebase.transport.ClientMetrics;

public final class AutoProtoEncoderDoNotUseEncoder implements jk
{
    public static final int CODEGEN_VERSION = 2;
    public static final jk CONFIG;
    
    static {
        CONFIG = new AutoProtoEncoderDoNotUseEncoder();
    }
    
    private AutoProtoEncoderDoNotUseEncoder() {
    }
    
    @Override
    public void configure(final zw zw) {
        zw.a(ProtoEncoderDoNotUse.class, ProtoEncoderDoNotUseEncoder.INSTANCE);
        zw.a(ClientMetrics.class, ClientMetricsEncoder.INSTANCE);
        zw.a(TimeWindow.class, TimeWindowEncoder.INSTANCE);
        zw.a(LogSourceMetrics.class, LogSourceMetricsEncoder.INSTANCE);
        zw.a(LogEventDropped.class, LogEventDroppedEncoder.INSTANCE);
        zw.a(GlobalMetrics.class, GlobalMetricsEncoder.INSTANCE);
        zw.a(StorageMetrics.class, StorageMetricsEncoder.INSTANCE);
    }
    
    public static final class ClientMetricsEncoder implements w01
    {
        private static final n00 APPNAMESPACE_DESCRIPTOR;
        private static final n00 GLOBALMETRICS_DESCRIPTOR;
        static final ClientMetricsEncoder INSTANCE;
        private static final n00 LOGSOURCEMETRICS_DESCRIPTOR;
        private static final n00 WINDOW_DESCRIPTOR;
        
        static {
            INSTANCE = new ClientMetricsEncoder();
            WINDOW_DESCRIPTOR = n00.a("window").b(a.b().c(1).a()).a();
            LOGSOURCEMETRICS_DESCRIPTOR = n00.a("logSourceMetrics").b(a.b().c(2).a()).a();
            GLOBALMETRICS_DESCRIPTOR = n00.a("globalMetrics").b(a.b().c(3).a()).a();
            APPNAMESPACE_DESCRIPTOR = n00.a("appNamespace").b(a.b().c(4).a()).a();
        }
        
        private ClientMetricsEncoder() {
        }
        
        public void encode(final ClientMetrics clientMetrics, final x01 x01) {
            x01.f(ClientMetricsEncoder.WINDOW_DESCRIPTOR, clientMetrics.getWindowInternal());
            x01.f(ClientMetricsEncoder.LOGSOURCEMETRICS_DESCRIPTOR, clientMetrics.getLogSourceMetricsList());
            x01.f(ClientMetricsEncoder.GLOBALMETRICS_DESCRIPTOR, clientMetrics.getGlobalMetricsInternal());
            x01.f(ClientMetricsEncoder.APPNAMESPACE_DESCRIPTOR, clientMetrics.getAppNamespace());
        }
    }
    
    public static final class GlobalMetricsEncoder implements w01
    {
        static final GlobalMetricsEncoder INSTANCE;
        private static final n00 STORAGEMETRICS_DESCRIPTOR;
        
        static {
            INSTANCE = new GlobalMetricsEncoder();
            STORAGEMETRICS_DESCRIPTOR = n00.a("storageMetrics").b(a.b().c(1).a()).a();
        }
        
        private GlobalMetricsEncoder() {
        }
        
        public void encode(final GlobalMetrics globalMetrics, final x01 x01) {
            x01.f(GlobalMetricsEncoder.STORAGEMETRICS_DESCRIPTOR, globalMetrics.getStorageMetricsInternal());
        }
    }
    
    public static final class LogEventDroppedEncoder implements w01
    {
        private static final n00 EVENTSDROPPEDCOUNT_DESCRIPTOR;
        static final LogEventDroppedEncoder INSTANCE;
        private static final n00 REASON_DESCRIPTOR;
        
        static {
            INSTANCE = new LogEventDroppedEncoder();
            EVENTSDROPPEDCOUNT_DESCRIPTOR = n00.a("eventsDroppedCount").b(a.b().c(1).a()).a();
            REASON_DESCRIPTOR = n00.a("reason").b(a.b().c(3).a()).a();
        }
        
        private LogEventDroppedEncoder() {
        }
        
        public void encode(final LogEventDropped logEventDropped, final x01 x01) {
            x01.e(LogEventDroppedEncoder.EVENTSDROPPEDCOUNT_DESCRIPTOR, logEventDropped.getEventsDroppedCount());
            x01.f(LogEventDroppedEncoder.REASON_DESCRIPTOR, logEventDropped.getReason());
        }
    }
    
    public static final class LogSourceMetricsEncoder implements w01
    {
        static final LogSourceMetricsEncoder INSTANCE;
        private static final n00 LOGEVENTDROPPED_DESCRIPTOR;
        private static final n00 LOGSOURCE_DESCRIPTOR;
        
        static {
            INSTANCE = new LogSourceMetricsEncoder();
            LOGSOURCE_DESCRIPTOR = n00.a("logSource").b(a.b().c(1).a()).a();
            LOGEVENTDROPPED_DESCRIPTOR = n00.a("logEventDropped").b(a.b().c(2).a()).a();
        }
        
        private LogSourceMetricsEncoder() {
        }
        
        public void encode(final LogSourceMetrics logSourceMetrics, final x01 x01) {
            x01.f(LogSourceMetricsEncoder.LOGSOURCE_DESCRIPTOR, logSourceMetrics.getLogSource());
            x01.f(LogSourceMetricsEncoder.LOGEVENTDROPPED_DESCRIPTOR, logSourceMetrics.getLogEventDroppedList());
        }
    }
    
    public static final class ProtoEncoderDoNotUseEncoder implements w01
    {
        private static final n00 CLIENTMETRICS_DESCRIPTOR;
        static final ProtoEncoderDoNotUseEncoder INSTANCE;
        
        static {
            INSTANCE = new ProtoEncoderDoNotUseEncoder();
            CLIENTMETRICS_DESCRIPTOR = n00.d("clientMetrics");
        }
        
        private ProtoEncoderDoNotUseEncoder() {
        }
        
        public void encode(final ProtoEncoderDoNotUse protoEncoderDoNotUse, final x01 x01) {
            x01.f(ProtoEncoderDoNotUseEncoder.CLIENTMETRICS_DESCRIPTOR, protoEncoderDoNotUse.getClientMetrics());
        }
    }
    
    public static final class StorageMetricsEncoder implements w01
    {
        private static final n00 CURRENTCACHESIZEBYTES_DESCRIPTOR;
        static final StorageMetricsEncoder INSTANCE;
        private static final n00 MAXCACHESIZEBYTES_DESCRIPTOR;
        
        static {
            INSTANCE = new StorageMetricsEncoder();
            CURRENTCACHESIZEBYTES_DESCRIPTOR = n00.a("currentCacheSizeBytes").b(a.b().c(1).a()).a();
            MAXCACHESIZEBYTES_DESCRIPTOR = n00.a("maxCacheSizeBytes").b(a.b().c(2).a()).a();
        }
        
        private StorageMetricsEncoder() {
        }
        
        public void encode(final StorageMetrics storageMetrics, final x01 x01) {
            x01.e(StorageMetricsEncoder.CURRENTCACHESIZEBYTES_DESCRIPTOR, storageMetrics.getCurrentCacheSizeBytes());
            x01.e(StorageMetricsEncoder.MAXCACHESIZEBYTES_DESCRIPTOR, storageMetrics.getMaxCacheSizeBytes());
        }
    }
    
    public static final class TimeWindowEncoder implements w01
    {
        private static final n00 ENDMS_DESCRIPTOR;
        static final TimeWindowEncoder INSTANCE;
        private static final n00 STARTMS_DESCRIPTOR;
        
        static {
            INSTANCE = new TimeWindowEncoder();
            STARTMS_DESCRIPTOR = n00.a("startMs").b(a.b().c(1).a()).a();
            ENDMS_DESCRIPTOR = n00.a("endMs").b(a.b().c(2).a()).a();
        }
        
        private TimeWindowEncoder() {
        }
        
        public void encode(final TimeWindow timeWindow, final x01 x01) {
            x01.e(TimeWindowEncoder.STARTMS_DESCRIPTOR, timeWindow.getStartMs());
            x01.e(TimeWindowEncoder.ENDMS_DESCRIPTOR, timeWindow.getEndMs());
        }
    }
}

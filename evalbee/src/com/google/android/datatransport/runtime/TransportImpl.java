// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime;

import com.google.android.datatransport.TransportScheduleCallback;
import com.google.android.datatransport.Event;
import com.google.android.datatransport.Transformer;
import com.google.android.datatransport.Encoding;
import com.google.android.datatransport.Transport;

final class TransportImpl<T> implements Transport<T>
{
    private final String name;
    private final Encoding payloadEncoding;
    private final Transformer<T, byte[]> transformer;
    private final TransportContext transportContext;
    private final TransportInternal transportInternal;
    
    public TransportImpl(final TransportContext transportContext, final String name, final Encoding payloadEncoding, final Transformer<T, byte[]> transformer, final TransportInternal transportInternal) {
        this.transportContext = transportContext;
        this.name = name;
        this.payloadEncoding = payloadEncoding;
        this.transformer = transformer;
        this.transportInternal = transportInternal;
    }
    
    public TransportContext getTransportContext() {
        return this.transportContext;
    }
    
    @Override
    public void schedule(final Event<T> event, final TransportScheduleCallback transportScheduleCallback) {
        this.transportInternal.send(SendRequest.builder().setTransportContext(this.transportContext).setEvent(event).setTransportName(this.name).setTransformer(this.transformer).setEncoding(this.payloadEncoding).build(), transportScheduleCallback);
    }
    
    @Override
    public void send(final Event<T> event) {
        this.schedule(event, new a());
    }
}

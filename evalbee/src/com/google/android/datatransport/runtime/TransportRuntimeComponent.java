// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime;

import com.google.android.datatransport.runtime.dagger.BindsInstance;
import android.content.Context;
import com.google.android.datatransport.runtime.scheduling.persistence.EventStore;
import com.google.android.datatransport.runtime.time.TimeModule;
import com.google.android.datatransport.runtime.scheduling.SchedulingConfigModule;
import com.google.android.datatransport.runtime.scheduling.SchedulingModule;
import com.google.android.datatransport.runtime.scheduling.persistence.EventStoreModule;
import com.google.android.datatransport.runtime.backends.BackendRegistryModule;
import com.google.android.datatransport.runtime.dagger.Component;
import java.io.Closeable;

@Component(modules = { BackendRegistryModule.class, EventStoreModule.class, ExecutionModule.class, SchedulingModule.class, SchedulingConfigModule.class, TimeModule.class })
abstract class TransportRuntimeComponent implements Closeable
{
    public TransportRuntimeComponent() {
    }
    
    @Override
    public void close() {
        this.getEventStore().close();
    }
    
    public abstract EventStore getEventStore();
    
    public abstract TransportRuntime getTransportRuntime();
    
    @Component.Builder
    public interface Builder
    {
        TransportRuntimeComponent build();
        
        @BindsInstance
        Builder setApplicationContext(final Context p0);
    }
}

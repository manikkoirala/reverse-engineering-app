// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime;

import com.google.android.datatransport.TransportScheduleCallback;
import com.google.android.datatransport.TransportFactory;
import java.util.concurrent.Callable;
import android.content.Context;
import java.util.Collections;
import com.google.android.datatransport.Encoding;
import java.util.Set;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.WorkInitializer;
import com.google.android.datatransport.runtime.time.Monotonic;
import com.google.android.datatransport.runtime.time.WallTime;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.Uploader;
import com.google.android.datatransport.runtime.scheduling.Scheduler;
import com.google.android.datatransport.runtime.time.Clock;

public class TransportRuntime implements TransportInternal
{
    private static volatile TransportRuntimeComponent instance;
    private final Clock eventClock;
    private final Scheduler scheduler;
    private final Uploader uploader;
    private final Clock uptimeClock;
    
    public TransportRuntime(@WallTime final Clock eventClock, @Monotonic final Clock uptimeClock, final Scheduler scheduler, final Uploader uploader, final WorkInitializer workInitializer) {
        this.eventClock = eventClock;
        this.uptimeClock = uptimeClock;
        this.scheduler = scheduler;
        this.uploader = uploader;
        workInitializer.ensureContextsScheduled();
    }
    
    private EventInternal convert(final SendRequest sendRequest) {
        return EventInternal.builder().setEventMillis(this.eventClock.getTime()).setUptimeMillis(this.uptimeClock.getTime()).setTransportName(sendRequest.getTransportName()).setEncodedPayload(new EncodedPayload(sendRequest.getEncoding(), sendRequest.getPayload())).setCode(sendRequest.getEvent().getCode()).build();
    }
    
    public static TransportRuntime getInstance() {
        final TransportRuntimeComponent instance = TransportRuntime.instance;
        if (instance != null) {
            return instance.getTransportRuntime();
        }
        throw new IllegalStateException("Not initialized!");
    }
    
    private static Set<Encoding> getSupportedEncodings(final Destination destination) {
        if (destination instanceof EncodedDestination) {
            return Collections.unmodifiableSet((Set<? extends Encoding>)((EncodedDestination)destination).getSupportedEncodings());
        }
        return Collections.singleton(Encoding.of("proto"));
    }
    
    public static void initialize(final Context applicationContext) {
        if (TransportRuntime.instance == null) {
            synchronized (TransportRuntime.class) {
                if (TransportRuntime.instance == null) {
                    TransportRuntime.instance = DaggerTransportRuntimeComponent.builder().setApplicationContext(applicationContext).build();
                }
            }
        }
    }
    
    public static void withInstance(final TransportRuntimeComponent instance, final Callable<Void> callable) {
        synchronized (TransportRuntime.class) {
            final TransportRuntimeComponent instance2 = TransportRuntime.instance;
            TransportRuntime.instance = instance;
            monitorexit(TransportRuntime.class);
            try {
                callable.call();
                synchronized (TransportRuntime.class) {
                    TransportRuntime.instance = instance2;
                }
            }
            finally {
                synchronized (TransportRuntime.class) {
                    TransportRuntime.instance = instance2;
                    monitorexit(TransportRuntime.class);
                }
            }
        }
    }
    
    public Uploader getUploader() {
        return this.uploader;
    }
    
    public TransportFactory newFactory(final Destination destination) {
        return new TransportFactoryImpl(getSupportedEncodings(destination), TransportContext.builder().setBackendName(destination.getName()).setExtras(destination.getExtras()).build(), this);
    }
    
    @Deprecated
    public TransportFactory newFactory(final String backendName) {
        return new TransportFactoryImpl(getSupportedEncodings(null), TransportContext.builder().setBackendName(backendName).build(), this);
    }
    
    @Override
    public void send(final SendRequest sendRequest, final TransportScheduleCallback transportScheduleCallback) {
        this.scheduler.schedule(sendRequest.getTransportContext().withPriority(sendRequest.getEvent().getPriority()), this.convert(sendRequest), transportScheduleCallback);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime;

import com.google.android.datatransport.TransportScheduleCallback;

interface TransportInternal
{
    void send(final SendRequest p0, final TransportScheduleCallback p1);
}

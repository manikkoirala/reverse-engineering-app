// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.logging;

import android.os.Build$VERSION;
import android.util.Log;

public final class Logging
{
    private static final String LOG_PREFIX = "TRuntime.";
    private static final int MAX_LOG_TAG_SIZE_IN_SDK_N = 23;
    
    private Logging() {
    }
    
    private static String concatTag(String str, String str2) {
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(str2);
        str2 = (str = sb.toString());
        if (str2.length() > 23) {
            str = str2.substring(0, 23);
        }
        return str;
    }
    
    public static void d(String tag, final String s) {
        tag = getTag(tag);
        if (Log.isLoggable(tag, 3)) {
            Log.d(tag, s);
        }
    }
    
    public static void d(String tag, final String format, final Object o) {
        tag = getTag(tag);
        if (Log.isLoggable(tag, 3)) {
            Log.d(tag, String.format(format, o));
        }
    }
    
    public static void d(String tag, final String format, final Object o, final Object o2) {
        tag = getTag(tag);
        if (Log.isLoggable(tag, 3)) {
            Log.d(tag, String.format(format, o, o2));
        }
    }
    
    public static void d(String tag, final String format, final Object... args) {
        tag = getTag(tag);
        if (Log.isLoggable(tag, 3)) {
            Log.d(tag, String.format(format, args));
        }
    }
    
    public static void e(String tag, final String s, final Throwable t) {
        tag = getTag(tag);
        if (Log.isLoggable(tag, 6)) {
            Log.e(tag, s, t);
        }
    }
    
    private static String getTag(final String str) {
        if (Build$VERSION.SDK_INT < 26) {
            return concatTag("TRuntime.", str);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("TRuntime.");
        sb.append(str);
        return sb.toString();
    }
    
    public static void i(String tag, final String format, final Object o) {
        tag = getTag(tag);
        if (Log.isLoggable(tag, 4)) {
            Log.i(tag, String.format(format, o));
        }
    }
    
    public static void w(String tag, final String format, final Object o) {
        tag = getTag(tag);
        if (Log.isLoggable(tag, 5)) {
            Log.w(tag, String.format(format, o));
        }
    }
}

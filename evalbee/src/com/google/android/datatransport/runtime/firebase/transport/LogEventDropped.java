// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.firebase.transport;

import com.google.firebase.encoders.proto.Protobuf;

public final class LogEventDropped
{
    private static final LogEventDropped DEFAULT_INSTANCE;
    private final long events_dropped_count_;
    private final Reason reason_;
    
    static {
        DEFAULT_INSTANCE = new Builder().build();
    }
    
    public LogEventDropped(final long events_dropped_count_, final Reason reason_) {
        this.events_dropped_count_ = events_dropped_count_;
        this.reason_ = reason_;
    }
    
    public static LogEventDropped getDefaultInstance() {
        return LogEventDropped.DEFAULT_INSTANCE;
    }
    
    public static Builder newBuilder() {
        return new Builder();
    }
    
    @Protobuf(tag = 1)
    public long getEventsDroppedCount() {
        return this.events_dropped_count_;
    }
    
    @Protobuf(tag = 3)
    public Reason getReason() {
        return this.reason_;
    }
    
    public static final class Builder
    {
        private long events_dropped_count_;
        private Reason reason_;
        
        public Builder() {
            this.events_dropped_count_ = 0L;
            this.reason_ = Reason.REASON_UNKNOWN;
        }
        
        public LogEventDropped build() {
            return new LogEventDropped(this.events_dropped_count_, this.reason_);
        }
        
        public Builder setEventsDroppedCount(final long events_dropped_count_) {
            this.events_dropped_count_ = events_dropped_count_;
            return this;
        }
        
        public Builder setReason(final Reason reason_) {
            this.reason_ = reason_;
            return this;
        }
    }
    
    public enum Reason implements j91
    {
        private static final Reason[] $VALUES;
        
        CACHE_FULL(2), 
        INVALID_PAYLOD(5), 
        MAX_RETRIES_REACHED(4), 
        MESSAGE_TOO_OLD(1), 
        PAYLOAD_TOO_BIG(3), 
        REASON_UNKNOWN(0), 
        SERVER_ERROR(6);
        
        private final int number_;
        
        private Reason(final int number_) {
            this.number_ = number_;
        }
        
        @Override
        public int getNumber() {
            return this.number_;
        }
    }
}

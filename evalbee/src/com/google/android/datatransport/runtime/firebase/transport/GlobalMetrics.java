// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.firebase.transport;

import com.google.firebase.encoders.proto.Protobuf;

public final class GlobalMetrics
{
    private static final GlobalMetrics DEFAULT_INSTANCE;
    private final StorageMetrics storage_metrics_;
    
    static {
        DEFAULT_INSTANCE = new Builder().build();
    }
    
    public GlobalMetrics(final StorageMetrics storage_metrics_) {
        this.storage_metrics_ = storage_metrics_;
    }
    
    public static GlobalMetrics getDefaultInstance() {
        return GlobalMetrics.DEFAULT_INSTANCE;
    }
    
    public static Builder newBuilder() {
        return new Builder();
    }
    
    public StorageMetrics getStorageMetrics() {
        StorageMetrics storageMetrics;
        if ((storageMetrics = this.storage_metrics_) == null) {
            storageMetrics = StorageMetrics.getDefaultInstance();
        }
        return storageMetrics;
    }
    
    @Protobuf(tag = 1)
    public StorageMetrics getStorageMetricsInternal() {
        return this.storage_metrics_;
    }
    
    public static final class Builder
    {
        private StorageMetrics storage_metrics_;
        
        public Builder() {
            this.storage_metrics_ = null;
        }
        
        public GlobalMetrics build() {
            return new GlobalMetrics(this.storage_metrics_);
        }
        
        public Builder setStorageMetrics(final StorageMetrics storage_metrics_) {
            this.storage_metrics_ = storage_metrics_;
            return this;
        }
    }
}

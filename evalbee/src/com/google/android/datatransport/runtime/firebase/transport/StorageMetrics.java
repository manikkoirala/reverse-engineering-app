// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.firebase.transport;

import com.google.firebase.encoders.proto.Protobuf;

public final class StorageMetrics
{
    private static final StorageMetrics DEFAULT_INSTANCE;
    private final long current_cache_size_bytes_;
    private final long max_cache_size_bytes_;
    
    static {
        DEFAULT_INSTANCE = new Builder().build();
    }
    
    public StorageMetrics(final long current_cache_size_bytes_, final long max_cache_size_bytes_) {
        this.current_cache_size_bytes_ = current_cache_size_bytes_;
        this.max_cache_size_bytes_ = max_cache_size_bytes_;
    }
    
    public static StorageMetrics getDefaultInstance() {
        return StorageMetrics.DEFAULT_INSTANCE;
    }
    
    public static Builder newBuilder() {
        return new Builder();
    }
    
    @Protobuf(tag = 1)
    public long getCurrentCacheSizeBytes() {
        return this.current_cache_size_bytes_;
    }
    
    @Protobuf(tag = 2)
    public long getMaxCacheSizeBytes() {
        return this.max_cache_size_bytes_;
    }
    
    public static final class Builder
    {
        private long current_cache_size_bytes_;
        private long max_cache_size_bytes_;
        
        public Builder() {
            this.current_cache_size_bytes_ = 0L;
            this.max_cache_size_bytes_ = 0L;
        }
        
        public StorageMetrics build() {
            return new StorageMetrics(this.current_cache_size_bytes_, this.max_cache_size_bytes_);
        }
        
        public Builder setCurrentCacheSizeBytes(final long current_cache_size_bytes_) {
            this.current_cache_size_bytes_ = current_cache_size_bytes_;
            return this;
        }
        
        public Builder setMaxCacheSizeBytes(final long max_cache_size_bytes_) {
            this.max_cache_size_bytes_ = max_cache_size_bytes_;
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.firebase.transport;

import com.google.firebase.encoders.proto.Protobuf;

public final class TimeWindow
{
    private static final TimeWindow DEFAULT_INSTANCE;
    private final long end_ms_;
    private final long start_ms_;
    
    static {
        DEFAULT_INSTANCE = new Builder().build();
    }
    
    public TimeWindow(final long start_ms_, final long end_ms_) {
        this.start_ms_ = start_ms_;
        this.end_ms_ = end_ms_;
    }
    
    public static TimeWindow getDefaultInstance() {
        return TimeWindow.DEFAULT_INSTANCE;
    }
    
    public static Builder newBuilder() {
        return new Builder();
    }
    
    @Protobuf(tag = 2)
    public long getEndMs() {
        return this.end_ms_;
    }
    
    @Protobuf(tag = 1)
    public long getStartMs() {
        return this.start_ms_;
    }
    
    public static final class Builder
    {
        private long end_ms_;
        private long start_ms_;
        
        public Builder() {
            this.start_ms_ = 0L;
            this.end_ms_ = 0L;
        }
        
        public TimeWindow build() {
            return new TimeWindow(this.start_ms_, this.end_ms_);
        }
        
        public Builder setEndMs(final long end_ms_) {
            this.end_ms_ = end_ms_;
            return this;
        }
        
        public Builder setStartMs(final long start_ms_) {
            this.start_ms_ = start_ms_;
            return this;
        }
    }
}

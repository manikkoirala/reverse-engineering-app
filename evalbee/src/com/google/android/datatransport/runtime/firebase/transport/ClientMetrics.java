// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.firebase.transport;

import java.util.Collections;
import java.util.ArrayList;
import java.io.OutputStream;
import com.google.android.datatransport.runtime.ProtoEncoderDoNotUse;
import com.google.firebase.encoders.proto.Protobuf;
import java.util.List;

public final class ClientMetrics
{
    private static final ClientMetrics DEFAULT_INSTANCE;
    private final String app_namespace_;
    private final GlobalMetrics global_metrics_;
    private final List<LogSourceMetrics> log_source_metrics_;
    private final TimeWindow window_;
    
    static {
        DEFAULT_INSTANCE = new Builder().build();
    }
    
    public ClientMetrics(final TimeWindow window_, final List<LogSourceMetrics> log_source_metrics_, final GlobalMetrics global_metrics_, final String app_namespace_) {
        this.window_ = window_;
        this.log_source_metrics_ = log_source_metrics_;
        this.global_metrics_ = global_metrics_;
        this.app_namespace_ = app_namespace_;
    }
    
    public static ClientMetrics getDefaultInstance() {
        return ClientMetrics.DEFAULT_INSTANCE;
    }
    
    public static Builder newBuilder() {
        return new Builder();
    }
    
    @Protobuf(tag = 4)
    public String getAppNamespace() {
        return this.app_namespace_;
    }
    
    public GlobalMetrics getGlobalMetrics() {
        GlobalMetrics globalMetrics;
        if ((globalMetrics = this.global_metrics_) == null) {
            globalMetrics = GlobalMetrics.getDefaultInstance();
        }
        return globalMetrics;
    }
    
    @Protobuf(tag = 3)
    public GlobalMetrics getGlobalMetricsInternal() {
        return this.global_metrics_;
    }
    
    @Protobuf(tag = 2)
    public List<LogSourceMetrics> getLogSourceMetricsList() {
        return this.log_source_metrics_;
    }
    
    public TimeWindow getWindow() {
        TimeWindow timeWindow;
        if ((timeWindow = this.window_) == null) {
            timeWindow = TimeWindow.getDefaultInstance();
        }
        return timeWindow;
    }
    
    @Protobuf(tag = 1)
    public TimeWindow getWindowInternal() {
        return this.window_;
    }
    
    public byte[] toByteArray() {
        return ProtoEncoderDoNotUse.encode(this);
    }
    
    public void writeTo(final OutputStream outputStream) {
        ProtoEncoderDoNotUse.encode(this, outputStream);
    }
    
    public static final class Builder
    {
        private String app_namespace_;
        private GlobalMetrics global_metrics_;
        private List<LogSourceMetrics> log_source_metrics_;
        private TimeWindow window_;
        
        public Builder() {
            this.window_ = null;
            this.log_source_metrics_ = new ArrayList<LogSourceMetrics>();
            this.global_metrics_ = null;
            this.app_namespace_ = "";
        }
        
        public Builder addLogSourceMetrics(final LogSourceMetrics logSourceMetrics) {
            this.log_source_metrics_.add(logSourceMetrics);
            return this;
        }
        
        public ClientMetrics build() {
            return new ClientMetrics(this.window_, Collections.unmodifiableList((List<? extends LogSourceMetrics>)this.log_source_metrics_), this.global_metrics_, this.app_namespace_);
        }
        
        public Builder setAppNamespace(final String app_namespace_) {
            this.app_namespace_ = app_namespace_;
            return this;
        }
        
        public Builder setGlobalMetrics(final GlobalMetrics global_metrics_) {
            this.global_metrics_ = global_metrics_;
            return this;
        }
        
        public Builder setLogSourceMetricsList(final List<LogSourceMetrics> log_source_metrics_) {
            this.log_source_metrics_ = log_source_metrics_;
            return this;
        }
        
        public Builder setWindow(final TimeWindow window_) {
            this.window_ = window_;
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime.firebase.transport;

import java.util.Collections;
import java.util.ArrayList;
import com.google.firebase.encoders.proto.Protobuf;
import java.util.List;

public final class LogSourceMetrics
{
    private static final LogSourceMetrics DEFAULT_INSTANCE;
    private final List<LogEventDropped> log_event_dropped_;
    private final String log_source_;
    
    static {
        DEFAULT_INSTANCE = new Builder().build();
    }
    
    public LogSourceMetrics(final String log_source_, final List<LogEventDropped> log_event_dropped_) {
        this.log_source_ = log_source_;
        this.log_event_dropped_ = log_event_dropped_;
    }
    
    public static LogSourceMetrics getDefaultInstance() {
        return LogSourceMetrics.DEFAULT_INSTANCE;
    }
    
    public static Builder newBuilder() {
        return new Builder();
    }
    
    @Protobuf(tag = 2)
    public List<LogEventDropped> getLogEventDroppedList() {
        return this.log_event_dropped_;
    }
    
    @Protobuf(tag = 1)
    public String getLogSource() {
        return this.log_source_;
    }
    
    public static final class Builder
    {
        private List<LogEventDropped> log_event_dropped_;
        private String log_source_;
        
        public Builder() {
            this.log_source_ = "";
            this.log_event_dropped_ = new ArrayList<LogEventDropped>();
        }
        
        public Builder addLogEventDropped(final LogEventDropped logEventDropped) {
            this.log_event_dropped_.add(logEventDropped);
            return this;
        }
        
        public LogSourceMetrics build() {
            return new LogSourceMetrics(this.log_source_, Collections.unmodifiableList((List<? extends LogEventDropped>)this.log_event_dropped_));
        }
        
        public Builder setLogEventDroppedList(final List<LogEventDropped> log_event_dropped_) {
            this.log_event_dropped_ = log_event_dropped_;
            return this;
        }
        
        public Builder setLogSource(final String log_source_) {
            this.log_source_ = log_source_;
            return this;
        }
    }
}

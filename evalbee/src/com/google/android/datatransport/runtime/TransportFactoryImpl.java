// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport.runtime;

import com.google.android.datatransport.Transport;
import com.google.android.datatransport.Transformer;
import com.google.android.datatransport.Encoding;
import java.util.Set;
import com.google.android.datatransport.TransportFactory;

final class TransportFactoryImpl implements TransportFactory
{
    private final Set<Encoding> supportedPayloadEncodings;
    private final TransportContext transportContext;
    private final TransportInternal transportInternal;
    
    public TransportFactoryImpl(final Set<Encoding> supportedPayloadEncodings, final TransportContext transportContext, final TransportInternal transportInternal) {
        this.supportedPayloadEncodings = supportedPayloadEncodings;
        this.transportContext = transportContext;
        this.transportInternal = transportInternal;
    }
    
    @Override
    public <T> Transport<T> getTransport(final String s, final Class<T> clazz, final Encoding encoding, final Transformer<T, byte[]> transformer) {
        if (this.supportedPayloadEncodings.contains(encoding)) {
            return new TransportImpl<T>(this.transportContext, s, encoding, transformer, this.transportInternal);
        }
        throw new IllegalArgumentException(String.format("%s is not supported byt this factory. Supported encodings are: %s.", encoding, this.supportedPayloadEncodings));
    }
    
    @Override
    public <T> Transport<T> getTransport(final String s, final Class<T> clazz, final Transformer<T, byte[]> transformer) {
        return this.getTransport(s, clazz, Encoding.of("proto"), transformer);
    }
}

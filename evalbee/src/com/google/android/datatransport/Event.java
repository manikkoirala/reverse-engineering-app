// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.datatransport;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class Event<T>
{
    public static <T> Event<T> ofData(final int i, final T t) {
        return new AutoValue_Event<T>(i, t, Priority.DEFAULT, null);
    }
    
    public static <T> Event<T> ofData(final int i, final T t, final ProductData productData) {
        return new AutoValue_Event<T>(i, t, Priority.DEFAULT, productData);
    }
    
    public static <T> Event<T> ofData(final T t) {
        return new AutoValue_Event<T>(null, t, Priority.DEFAULT, null);
    }
    
    public static <T> Event<T> ofData(final T t, final ProductData productData) {
        return new AutoValue_Event<T>(null, t, Priority.DEFAULT, productData);
    }
    
    public static <T> Event<T> ofTelemetry(final int i, final T t) {
        return new AutoValue_Event<T>(i, t, Priority.VERY_LOW, null);
    }
    
    public static <T> Event<T> ofTelemetry(final int i, final T t, final ProductData productData) {
        return new AutoValue_Event<T>(i, t, Priority.VERY_LOW, productData);
    }
    
    public static <T> Event<T> ofTelemetry(final T t) {
        return new AutoValue_Event<T>(null, t, Priority.VERY_LOW, null);
    }
    
    public static <T> Event<T> ofTelemetry(final T t, final ProductData productData) {
        return new AutoValue_Event<T>(null, t, Priority.VERY_LOW, productData);
    }
    
    public static <T> Event<T> ofUrgent(final int i, final T t) {
        return new AutoValue_Event<T>(i, t, Priority.HIGHEST, null);
    }
    
    public static <T> Event<T> ofUrgent(final int i, final T t, final ProductData productData) {
        return new AutoValue_Event<T>(i, t, Priority.HIGHEST, productData);
    }
    
    public static <T> Event<T> ofUrgent(final T t) {
        return new AutoValue_Event<T>(null, t, Priority.HIGHEST, null);
    }
    
    public static <T> Event<T> ofUrgent(final T t, final ProductData productData) {
        return new AutoValue_Event<T>(null, t, Priority.HIGHEST, productData);
    }
    
    public abstract Integer getCode();
    
    public abstract T getPayload();
    
    public abstract Priority getPriority();
    
    public abstract ProductData getProductData();
}

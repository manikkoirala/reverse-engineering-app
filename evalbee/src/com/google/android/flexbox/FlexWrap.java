// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.flexbox;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

@Retention(RetentionPolicy.SOURCE)
public @interface FlexWrap {
    public static final int NOWRAP = 0;
    public static final int WRAP = 1;
    public static final int WRAP_REVERSE = 2;
}

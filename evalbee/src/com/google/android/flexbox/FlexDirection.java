// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.flexbox;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

@Retention(RetentionPolicy.SOURCE)
public @interface FlexDirection {
    public static final int COLUMN = 2;
    public static final int COLUMN_REVERSE = 3;
    public static final int ROW = 0;
    public static final int ROW_REVERSE = 1;
}

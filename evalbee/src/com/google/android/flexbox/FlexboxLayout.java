// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.flexbox;

import android.os.Parcel;
import android.os.Parcelable$Creator;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.view.ViewGroup$MarginLayoutParams;
import android.view.ViewGroup$LayoutParams;
import android.view.View$MeasureSpec;
import java.util.Iterator;
import android.graphics.Canvas;
import android.view.View;
import android.content.res.TypedArray;
import java.util.ArrayList;
import android.util.AttributeSet;
import android.content.Context;
import android.util.SparseIntArray;
import java.util.List;
import android.graphics.drawable.Drawable;
import android.view.ViewGroup;

public class FlexboxLayout extends ViewGroup implements FlexContainer
{
    public static final int SHOW_DIVIDER_BEGINNING = 1;
    public static final int SHOW_DIVIDER_END = 4;
    public static final int SHOW_DIVIDER_MIDDLE = 2;
    public static final int SHOW_DIVIDER_NONE = 0;
    private int mAlignContent;
    private int mAlignItems;
    private Drawable mDividerDrawableHorizontal;
    private Drawable mDividerDrawableVertical;
    private int mDividerHorizontalHeight;
    private int mDividerVerticalWidth;
    private int mFlexDirection;
    private List<FlexLine> mFlexLines;
    private FlexboxHelper.FlexLinesResult mFlexLinesResult;
    private int mFlexWrap;
    private FlexboxHelper mFlexboxHelper;
    private int mJustifyContent;
    private int mMaxLine;
    private SparseIntArray mOrderCache;
    private int[] mReorderedIndices;
    private int mShowDividerHorizontal;
    private int mShowDividerVertical;
    
    public FlexboxLayout(final Context context) {
        this(context, null);
    }
    
    public FlexboxLayout(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public FlexboxLayout(final Context context, final AttributeSet set, int n) {
        super(context, set, n);
        this.mMaxLine = -1;
        this.mFlexboxHelper = new FlexboxHelper(this);
        this.mFlexLines = new ArrayList<FlexLine>();
        this.mFlexLinesResult = new FlexboxHelper.FlexLinesResult();
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.FlexboxLayout, n, 0);
        this.mFlexDirection = obtainStyledAttributes.getInt(R.styleable.FlexboxLayout_flexDirection, 0);
        this.mFlexWrap = obtainStyledAttributes.getInt(R.styleable.FlexboxLayout_flexWrap, 0);
        this.mJustifyContent = obtainStyledAttributes.getInt(R.styleable.FlexboxLayout_justifyContent, 0);
        this.mAlignItems = obtainStyledAttributes.getInt(R.styleable.FlexboxLayout_alignItems, 0);
        this.mAlignContent = obtainStyledAttributes.getInt(R.styleable.FlexboxLayout_alignContent, 0);
        this.mMaxLine = obtainStyledAttributes.getInt(R.styleable.FlexboxLayout_maxLine, -1);
        final Drawable drawable = obtainStyledAttributes.getDrawable(R.styleable.FlexboxLayout_dividerDrawable);
        if (drawable != null) {
            this.setDividerDrawableHorizontal(drawable);
            this.setDividerDrawableVertical(drawable);
        }
        final Drawable drawable2 = obtainStyledAttributes.getDrawable(R.styleable.FlexboxLayout_dividerDrawableHorizontal);
        if (drawable2 != null) {
            this.setDividerDrawableHorizontal(drawable2);
        }
        final Drawable drawable3 = obtainStyledAttributes.getDrawable(R.styleable.FlexboxLayout_dividerDrawableVertical);
        if (drawable3 != null) {
            this.setDividerDrawableVertical(drawable3);
        }
        n = obtainStyledAttributes.getInt(R.styleable.FlexboxLayout_showDivider, 0);
        if (n != 0) {
            this.mShowDividerVertical = n;
            this.mShowDividerHorizontal = n;
        }
        n = obtainStyledAttributes.getInt(R.styleable.FlexboxLayout_showDividerVertical, 0);
        if (n != 0) {
            this.mShowDividerVertical = n;
        }
        n = obtainStyledAttributes.getInt(R.styleable.FlexboxLayout_showDividerHorizontal, 0);
        if (n != 0) {
            this.mShowDividerHorizontal = n;
        }
        obtainStyledAttributes.recycle();
    }
    
    private boolean allFlexLinesAreDummyBefore(final int n) {
        for (int i = 0; i < n; ++i) {
            if (this.mFlexLines.get(i).getItemCountNotGone() > 0) {
                return false;
            }
        }
        return true;
    }
    
    private boolean allViewsAreGoneBefore(final int n, final int n2) {
        for (int i = 1; i <= n2; ++i) {
            final View reorderedChild = this.getReorderedChildAt(n - i);
            if (reorderedChild != null && reorderedChild.getVisibility() != 8) {
                return false;
            }
        }
        return true;
    }
    
    private void drawDividersHorizontal(final Canvas canvas, final boolean b, final boolean b2) {
        final int paddingLeft = ((View)this).getPaddingLeft();
        final int max = Math.max(0, ((View)this).getWidth() - ((View)this).getPaddingRight() - paddingLeft);
        for (int size = this.mFlexLines.size(), i = 0; i < size; ++i) {
            final FlexLine flexLine = this.mFlexLines.get(i);
            for (int j = 0; j < flexLine.mItemCount; ++j) {
                final int n = flexLine.mFirstIndex + j;
                final View reorderedChild = this.getReorderedChildAt(n);
                if (reorderedChild != null) {
                    if (reorderedChild.getVisibility() != 8) {
                        final LayoutParams layoutParams = (LayoutParams)reorderedChild.getLayoutParams();
                        if (this.hasDividerBeforeChildAtAlongMainAxis(n, j)) {
                            int n2;
                            if (b) {
                                n2 = reorderedChild.getRight() + layoutParams.rightMargin;
                            }
                            else {
                                n2 = reorderedChild.getLeft() - layoutParams.leftMargin - this.mDividerVerticalWidth;
                            }
                            this.drawVerticalDivider(canvas, n2, flexLine.mTop, flexLine.mCrossSize);
                        }
                        if (j == flexLine.mItemCount - 1 && (this.mShowDividerVertical & 0x4) > 0) {
                            int n3;
                            if (b) {
                                n3 = reorderedChild.getLeft() - layoutParams.leftMargin - this.mDividerVerticalWidth;
                            }
                            else {
                                n3 = reorderedChild.getRight() + layoutParams.rightMargin;
                            }
                            this.drawVerticalDivider(canvas, n3, flexLine.mTop, flexLine.mCrossSize);
                        }
                    }
                }
            }
            if (this.hasDividerBeforeFlexLine(i)) {
                int mBottom;
                if (b2) {
                    mBottom = flexLine.mBottom;
                }
                else {
                    mBottom = flexLine.mTop - this.mDividerHorizontalHeight;
                }
                this.drawHorizontalDivider(canvas, paddingLeft, mBottom, max);
            }
            if (this.hasEndDividerAfterFlexLine(i) && (this.mShowDividerHorizontal & 0x4) > 0) {
                int mBottom2;
                if (b2) {
                    mBottom2 = flexLine.mTop - this.mDividerHorizontalHeight;
                }
                else {
                    mBottom2 = flexLine.mBottom;
                }
                this.drawHorizontalDivider(canvas, paddingLeft, mBottom2, max);
            }
        }
    }
    
    private void drawDividersVertical(final Canvas canvas, final boolean b, final boolean b2) {
        final int paddingTop = ((View)this).getPaddingTop();
        final int max = Math.max(0, ((View)this).getHeight() - ((View)this).getPaddingBottom() - paddingTop);
        for (int size = this.mFlexLines.size(), i = 0; i < size; ++i) {
            final FlexLine flexLine = this.mFlexLines.get(i);
            for (int j = 0; j < flexLine.mItemCount; ++j) {
                final int n = flexLine.mFirstIndex + j;
                final View reorderedChild = this.getReorderedChildAt(n);
                if (reorderedChild != null) {
                    if (reorderedChild.getVisibility() != 8) {
                        final LayoutParams layoutParams = (LayoutParams)reorderedChild.getLayoutParams();
                        if (this.hasDividerBeforeChildAtAlongMainAxis(n, j)) {
                            int n2;
                            if (b2) {
                                n2 = reorderedChild.getBottom() + layoutParams.bottomMargin;
                            }
                            else {
                                n2 = reorderedChild.getTop() - layoutParams.topMargin - this.mDividerHorizontalHeight;
                            }
                            this.drawHorizontalDivider(canvas, flexLine.mLeft, n2, flexLine.mCrossSize);
                        }
                        if (j == flexLine.mItemCount - 1 && (this.mShowDividerHorizontal & 0x4) > 0) {
                            int n3;
                            if (b2) {
                                n3 = reorderedChild.getTop() - layoutParams.topMargin - this.mDividerHorizontalHeight;
                            }
                            else {
                                n3 = reorderedChild.getBottom() + layoutParams.bottomMargin;
                            }
                            this.drawHorizontalDivider(canvas, flexLine.mLeft, n3, flexLine.mCrossSize);
                        }
                    }
                }
            }
            if (this.hasDividerBeforeFlexLine(i)) {
                int mRight;
                if (b) {
                    mRight = flexLine.mRight;
                }
                else {
                    mRight = flexLine.mLeft - this.mDividerVerticalWidth;
                }
                this.drawVerticalDivider(canvas, mRight, paddingTop, max);
            }
            if (this.hasEndDividerAfterFlexLine(i) && (this.mShowDividerVertical & 0x4) > 0) {
                int mRight2;
                if (b) {
                    mRight2 = flexLine.mLeft - this.mDividerVerticalWidth;
                }
                else {
                    mRight2 = flexLine.mRight;
                }
                this.drawVerticalDivider(canvas, mRight2, paddingTop, max);
            }
        }
    }
    
    private void drawHorizontalDivider(final Canvas canvas, final int n, final int n2, final int n3) {
        final Drawable mDividerDrawableHorizontal = this.mDividerDrawableHorizontal;
        if (mDividerDrawableHorizontal == null) {
            return;
        }
        mDividerDrawableHorizontal.setBounds(n, n2, n3 + n, this.mDividerHorizontalHeight + n2);
        this.mDividerDrawableHorizontal.draw(canvas);
    }
    
    private void drawVerticalDivider(final Canvas canvas, final int n, final int n2, final int n3) {
        final Drawable mDividerDrawableVertical = this.mDividerDrawableVertical;
        if (mDividerDrawableVertical == null) {
            return;
        }
        mDividerDrawableVertical.setBounds(n, n2, this.mDividerVerticalWidth + n, n3 + n2);
        this.mDividerDrawableVertical.draw(canvas);
    }
    
    private boolean hasDividerBeforeChildAtAlongMainAxis(final int n, final int n2) {
        final boolean allViewsAreGoneBefore = this.allViewsAreGoneBefore(n, n2);
        final boolean b = false;
        boolean b2 = false;
        final boolean b3 = false;
        final boolean b4 = false;
        if (allViewsAreGoneBefore) {
            if (this.isMainAxisDirectionHorizontal()) {
                boolean b5 = b4;
                if ((this.mShowDividerVertical & 0x1) != 0x0) {
                    b5 = true;
                }
                return b5;
            }
            boolean b6 = b;
            if ((this.mShowDividerHorizontal & 0x1) != 0x0) {
                b6 = true;
            }
            return b6;
        }
        else {
            if (this.isMainAxisDirectionHorizontal()) {
                if ((this.mShowDividerVertical & 0x2) != 0x0) {
                    b2 = true;
                }
                return b2;
            }
            boolean b7 = b3;
            if ((this.mShowDividerHorizontal & 0x2) != 0x0) {
                b7 = true;
            }
            return b7;
        }
    }
    
    private boolean hasDividerBeforeFlexLine(final int n) {
        final boolean b = false;
        final boolean b2 = false;
        final boolean b3 = false;
        final boolean b4 = false;
        boolean b5 = b3;
        if (n >= 0) {
            if (n >= this.mFlexLines.size()) {
                b5 = b3;
            }
            else if (this.allFlexLinesAreDummyBefore(n)) {
                if (this.isMainAxisDirectionHorizontal()) {
                    boolean b6 = b4;
                    if ((this.mShowDividerHorizontal & 0x1) != 0x0) {
                        b6 = true;
                    }
                    return b6;
                }
                boolean b7 = b;
                if ((this.mShowDividerVertical & 0x1) != 0x0) {
                    b7 = true;
                }
                return b7;
            }
            else {
                if (this.isMainAxisDirectionHorizontal()) {
                    boolean b8 = b2;
                    if ((this.mShowDividerHorizontal & 0x2) != 0x0) {
                        b8 = true;
                    }
                    return b8;
                }
                b5 = b3;
                if ((this.mShowDividerVertical & 0x2) != 0x0) {
                    b5 = true;
                }
            }
        }
        return b5;
    }
    
    private boolean hasEndDividerAfterFlexLine(int i) {
        final boolean b = false;
        final boolean b2 = false;
        boolean b3 = b;
        if (i >= 0) {
            if (i >= this.mFlexLines.size()) {
                b3 = b;
            }
            else {
                ++i;
                while (i < this.mFlexLines.size()) {
                    if (this.mFlexLines.get(i).getItemCountNotGone() > 0) {
                        return false;
                    }
                    ++i;
                }
                if (this.isMainAxisDirectionHorizontal()) {
                    boolean b4 = b2;
                    if ((this.mShowDividerHorizontal & 0x4) != 0x0) {
                        b4 = true;
                    }
                    return b4;
                }
                b3 = b;
                if ((this.mShowDividerVertical & 0x4) != 0x0) {
                    b3 = true;
                }
            }
        }
        return b3;
    }
    
    private void layoutHorizontal(final boolean b, int n, int n2, int mDividerHorizontalHeight, int n3) {
        final int paddingLeft = ((View)this).getPaddingLeft();
        final int paddingRight = ((View)this).getPaddingRight();
        final int n4 = mDividerHorizontalHeight - n;
        n2 = n3 - n2 - ((View)this).getPaddingBottom();
        n = ((View)this).getPaddingTop();
        for (int size = this.mFlexLines.size(), i = 0; i < size; ++i) {
            final FlexLine flexLine = this.mFlexLines.get(i);
            n3 = n2;
            mDividerHorizontalHeight = n;
            if (this.hasDividerBeforeFlexLine(i)) {
                mDividerHorizontalHeight = this.mDividerHorizontalHeight;
                n3 = n2 - mDividerHorizontalHeight;
                mDividerHorizontalHeight += n;
            }
            n2 = this.mJustifyContent;
            n = 4;
            final int n5 = 1;
            float a = 0.0f;
            float n9 = 0.0f;
            float n10 = 0.0f;
            Label_0444: {
                if (n2 != 0) {
                    if (n2 != 1) {
                        if (n2 != 2) {
                            if (n2 == 3) {
                                final float n6 = (float)paddingLeft;
                                n2 = flexLine.getItemCountNotGone();
                                float n7;
                                if (n2 != 1) {
                                    n7 = (float)(n2 - 1);
                                }
                                else {
                                    n7 = 1.0f;
                                }
                                a = (n4 - flexLine.mMainSize) / n7;
                                final float n8 = (float)(n4 - paddingRight);
                                n9 = n6;
                                n10 = n8;
                                break Label_0444;
                            }
                            if (n2 == 4) {
                                n2 = flexLine.getItemCountNotGone();
                                if (n2 != 0) {
                                    a = (n4 - flexLine.mMainSize) / (float)n2;
                                }
                                else {
                                    a = 0.0f;
                                }
                                final float n11 = (float)paddingLeft;
                                final float n12 = a / 2.0f;
                                n9 = n11 + n12;
                                n10 = n4 - paddingRight - n12;
                                break Label_0444;
                            }
                            if (n2 == 5) {
                                n2 = flexLine.getItemCountNotGone();
                                if (n2 != 0) {
                                    a = (n4 - flexLine.mMainSize) / (float)(n2 + 1);
                                }
                                else {
                                    a = 0.0f;
                                }
                                n9 = paddingLeft + a;
                                n10 = n4 - paddingRight - a;
                                break Label_0444;
                            }
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Invalid justifyContent is set: ");
                            sb.append(this.mJustifyContent);
                            throw new IllegalStateException(sb.toString());
                        }
                        else {
                            final float n13 = (float)paddingLeft;
                            n2 = flexLine.mMainSize;
                            n9 = n13 + (n4 - n2) / 2.0f;
                            n10 = n4 - paddingRight - (n4 - n2) / 2.0f;
                        }
                    }
                    else {
                        n2 = flexLine.mMainSize;
                        n9 = (float)(n4 - n2 + paddingRight);
                        n10 = (float)(n2 - paddingLeft);
                    }
                }
                else {
                    n9 = (float)paddingLeft;
                    n10 = (float)(n4 - paddingRight);
                }
                a = 0.0f;
            }
            final float max = Math.max(a, 0.0f);
            for (int j = 0; j < flexLine.mItemCount; ++j) {
                n2 = flexLine.mFirstIndex + j;
                final View reorderedChild = this.getReorderedChildAt(n2);
                if (reorderedChild != null) {
                    if (reorderedChild.getVisibility() == 8) {
                        n = 4;
                    }
                    else {
                        final LayoutParams layoutParams = (LayoutParams)reorderedChild.getLayoutParams();
                        final float n14 = n9 + layoutParams.leftMargin;
                        float n15 = n10 - layoutParams.rightMargin;
                        float n17;
                        if (this.hasDividerBeforeChildAtAlongMainAxis(n2, j)) {
                            n = this.mDividerVerticalWidth;
                            final float n16 = (float)n;
                            n17 = n14 + n16;
                            n15 -= n16;
                        }
                        else {
                            n = 0;
                            n17 = n14;
                        }
                        if (j == flexLine.mItemCount - n5 && (this.mShowDividerVertical & 0x4) > 0) {
                            n2 = this.mDividerVerticalWidth;
                        }
                        else {
                            n2 = 0;
                        }
                        FlexboxHelper flexboxHelper;
                        int round3;
                        int n19;
                        int n20;
                        int round4;
                        if (this.mFlexWrap == 2) {
                            int round2;
                            int n18;
                            if (b) {
                                flexboxHelper = this.mFlexboxHelper;
                                final int round = Math.round(n15);
                                final int measuredWidth = reorderedChild.getMeasuredWidth();
                                final int measuredHeight = reorderedChild.getMeasuredHeight();
                                round2 = Math.round(n15);
                                round3 = round - measuredWidth;
                                n18 = n3 - measuredHeight;
                            }
                            else {
                                flexboxHelper = this.mFlexboxHelper;
                                round3 = Math.round(n17);
                                n18 = n3 - reorderedChild.getMeasuredHeight();
                                round2 = Math.round(n17) + reorderedChild.getMeasuredWidth();
                            }
                            n19 = n3;
                            n20 = n18;
                            round4 = round2;
                        }
                        else {
                            flexboxHelper = this.mFlexboxHelper;
                            int round5;
                            if (b) {
                                round5 = Math.round(n15) - reorderedChild.getMeasuredWidth();
                                round4 = Math.round(n15);
                            }
                            else {
                                round5 = Math.round(n17);
                                round4 = Math.round(n17) + reorderedChild.getMeasuredWidth();
                            }
                            n19 = mDividerHorizontalHeight + reorderedChild.getMeasuredHeight();
                            n20 = mDividerHorizontalHeight;
                            round3 = round5;
                        }
                        final int n21 = 4;
                        flexboxHelper.layoutSingleChildHorizontal(reorderedChild, flexLine, round3, n20, round4, n19);
                        final float n22 = n17 + (reorderedChild.getMeasuredWidth() + max + layoutParams.rightMargin);
                        final float n23 = (float)reorderedChild.getMeasuredWidth();
                        final float n24 = (float)layoutParams.leftMargin;
                        int n25;
                        if (b) {
                            n25 = n2;
                            n2 = n;
                        }
                        else {
                            n25 = n;
                        }
                        flexLine.updatePositionFromView(reorderedChild, n25, 0, n2, 0);
                        final float n26 = n15 - (n23 + max + n24);
                        n9 = n22;
                        n10 = n26;
                        n = n21;
                    }
                }
            }
            n2 = flexLine.mCrossSize;
            n = mDividerHorizontalHeight + n2;
            n2 = n3 - n2;
        }
    }
    
    private void layoutVertical(final boolean b, final boolean b2, int mDividerVerticalWidth, int n, int mDividerHorizontalHeight, int n2) {
        final int paddingTop = ((View)this).getPaddingTop();
        final int paddingBottom = ((View)this).getPaddingBottom();
        final int paddingRight = ((View)this).getPaddingRight();
        final int paddingLeft = ((View)this).getPaddingLeft();
        final int n3 = n2 - n;
        n = mDividerHorizontalHeight - mDividerVerticalWidth - paddingRight;
        final int size = this.mFlexLines.size();
        int i = 0;
        mDividerHorizontalHeight = paddingLeft;
        while (i < size) {
            final FlexLine flexLine = this.mFlexLines.get(i);
            n2 = mDividerHorizontalHeight;
            mDividerVerticalWidth = n;
            if (this.hasDividerBeforeFlexLine(i)) {
                mDividerVerticalWidth = this.mDividerVerticalWidth;
                n2 = mDividerHorizontalHeight + mDividerVerticalWidth;
                mDividerVerticalWidth = n - mDividerVerticalWidth;
            }
            n = this.mJustifyContent;
            mDividerHorizontalHeight = 4;
            float n6 = 0.0f;
            float n7 = 0.0f;
            float a = 0.0f;
            Label_0484: {
                Label_0481: {
                    if (n != 0) {
                        if (n != 1) {
                            if (n == 2) {
                                final float n4 = (float)paddingTop;
                                n = flexLine.mMainSize;
                                final float n5 = (n3 - n) / 2.0f;
                                n6 = n3 - paddingBottom - (n3 - n) / 2.0f;
                                n7 = n4 + n5;
                                break Label_0481;
                            }
                            if (n == 3) {
                                final float n8 = (float)paddingTop;
                                n = flexLine.getItemCountNotGone();
                                float n9;
                                if (n != 1) {
                                    n9 = (float)(n - 1);
                                }
                                else {
                                    n9 = 1.0f;
                                }
                                a = (n3 - flexLine.mMainSize) / n9;
                                final float n10 = (float)(n3 - paddingBottom);
                                n7 = n8;
                                n6 = n10;
                                break Label_0484;
                            }
                            if (n == 4) {
                                n = flexLine.getItemCountNotGone();
                                if (n != 0) {
                                    a = (n3 - flexLine.mMainSize) / (float)n;
                                }
                                else {
                                    a = 0.0f;
                                }
                                final float n11 = (float)paddingTop;
                                final float n12 = a / 2.0f;
                                n7 = n11 + n12;
                                n6 = n3 - paddingBottom - n12;
                                break Label_0484;
                            }
                            if (n == 5) {
                                n = flexLine.getItemCountNotGone();
                                if (n != 0) {
                                    a = (n3 - flexLine.mMainSize) / (float)(n + 1);
                                }
                                else {
                                    a = 0.0f;
                                }
                                n7 = paddingTop + a;
                                n6 = n3 - paddingBottom - a;
                                break Label_0484;
                            }
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Invalid justifyContent is set: ");
                            sb.append(this.mJustifyContent);
                            throw new IllegalStateException(sb.toString());
                        }
                        else {
                            n = flexLine.mMainSize;
                            n7 = (float)(n3 - n + paddingBottom);
                            n -= paddingTop;
                        }
                    }
                    else {
                        n7 = (float)paddingTop;
                        n = n3 - paddingBottom;
                    }
                    n6 = (float)n;
                }
                a = 0.0f;
            }
            final float max = Math.max(a, 0.0f);
            int j = 0;
            n = mDividerHorizontalHeight;
            while (j < flexLine.mItemCount) {
                mDividerHorizontalHeight = flexLine.mFirstIndex + j;
                final View reorderedChild = this.getReorderedChildAt(mDividerHorizontalHeight);
                if (reorderedChild != null) {
                    if (reorderedChild.getVisibility() == 8) {
                        n = 4;
                    }
                    else {
                        final LayoutParams layoutParams = (LayoutParams)reorderedChild.getLayoutParams();
                        final float n13 = n7 + layoutParams.topMargin;
                        float n14 = n6 - layoutParams.bottomMargin;
                        float n16;
                        if (this.hasDividerBeforeChildAtAlongMainAxis(mDividerHorizontalHeight, j)) {
                            n = this.mDividerHorizontalHeight;
                            final float n15 = (float)n;
                            n16 = n13 + n15;
                            n14 -= n15;
                        }
                        else {
                            n16 = n13;
                            n = 0;
                        }
                        if (j == flexLine.mItemCount - 1 && (this.mShowDividerHorizontal & 0x4) > 0) {
                            mDividerHorizontalHeight = this.mDividerHorizontalHeight;
                        }
                        else {
                            mDividerHorizontalHeight = 0;
                        }
                        FlexboxHelper flexboxHelper;
                        boolean b3;
                        int n17;
                        int n18;
                        int n19;
                        int n20;
                        if (b) {
                            if (b2) {
                                flexboxHelper = this.mFlexboxHelper;
                                b3 = true;
                                final int measuredWidth = reorderedChild.getMeasuredWidth();
                                final int round = Math.round(n14);
                                final int measuredHeight = reorderedChild.getMeasuredHeight();
                                n17 = Math.round(n14);
                                n18 = mDividerVerticalWidth - measuredWidth;
                                n19 = round - measuredHeight;
                                n20 = mDividerVerticalWidth;
                            }
                            else {
                                flexboxHelper = this.mFlexboxHelper;
                                b3 = true;
                                n18 = mDividerVerticalWidth - reorderedChild.getMeasuredWidth();
                                n19 = Math.round(n16);
                                n17 = Math.round(n16) + reorderedChild.getMeasuredHeight();
                                n20 = mDividerVerticalWidth;
                            }
                        }
                        else {
                            flexboxHelper = this.mFlexboxHelper;
                            b3 = false;
                            if (b2) {
                                n19 = Math.round(n14) - reorderedChild.getMeasuredHeight();
                                n20 = n2 + reorderedChild.getMeasuredWidth();
                                n17 = Math.round(n14);
                            }
                            else {
                                n19 = Math.round(n16);
                                n20 = n2 + reorderedChild.getMeasuredWidth();
                                n17 = Math.round(n16) + reorderedChild.getMeasuredHeight();
                            }
                            n18 = n2;
                        }
                        final int n21 = 4;
                        flexboxHelper.layoutSingleChildVertical(reorderedChild, flexLine, b3, n18, n19, n20, n17);
                        final float n22 = (float)reorderedChild.getMeasuredHeight();
                        final float n23 = (float)layoutParams.bottomMargin;
                        final float n24 = (float)reorderedChild.getMeasuredHeight();
                        final float n25 = (float)layoutParams.topMargin;
                        int n26;
                        if (b2) {
                            n26 = n;
                            n = mDividerHorizontalHeight;
                        }
                        else {
                            n26 = mDividerHorizontalHeight;
                        }
                        flexLine.updatePositionFromView(reorderedChild, 0, n, 0, n26);
                        final float n27 = n16 + (n22 + max + n23);
                        final float n28 = n14 - (n24 + max + n25);
                        n7 = n27;
                        n6 = n28;
                        n = n21;
                    }
                }
                ++j;
            }
            n = flexLine.mCrossSize;
            mDividerHorizontalHeight = n2 + n;
            n = mDividerVerticalWidth - n;
            ++i;
        }
    }
    
    private void measureHorizontal(final int n, final int n2) {
        this.mFlexLines.clear();
        this.mFlexLinesResult.reset();
        this.mFlexboxHelper.calculateHorizontalFlexLines(this.mFlexLinesResult, n, n2);
        this.mFlexLines = this.mFlexLinesResult.mFlexLines;
        this.mFlexboxHelper.determineMainSize(n, n2);
        if (this.mAlignItems == 3) {
            for (final FlexLine flexLine : this.mFlexLines) {
                int n3 = Integer.MIN_VALUE;
                int max;
                for (int i = 0; i < flexLine.mItemCount; ++i, n3 = max) {
                    final View reorderedChild = this.getReorderedChildAt(flexLine.mFirstIndex + i);
                    max = n3;
                    if (reorderedChild != null) {
                        if (reorderedChild.getVisibility() == 8) {
                            max = n3;
                        }
                        else {
                            final LayoutParams layoutParams = (LayoutParams)reorderedChild.getLayoutParams();
                            int b;
                            if (this.mFlexWrap != 2) {
                                b = reorderedChild.getMeasuredHeight() + Math.max(flexLine.mMaxBaseline - reorderedChild.getBaseline(), layoutParams.topMargin) + layoutParams.bottomMargin;
                            }
                            else {
                                b = reorderedChild.getMeasuredHeight() + layoutParams.topMargin + Math.max(flexLine.mMaxBaseline - reorderedChild.getMeasuredHeight() + reorderedChild.getBaseline(), layoutParams.bottomMargin);
                            }
                            max = Math.max(n3, b);
                        }
                    }
                }
                flexLine.mCrossSize = n3;
            }
        }
        this.mFlexboxHelper.determineCrossSize(n, n2, ((View)this).getPaddingTop() + ((View)this).getPaddingBottom());
        this.mFlexboxHelper.stretchViews();
        this.setMeasuredDimensionForFlex(this.mFlexDirection, n, n2, this.mFlexLinesResult.mChildState);
    }
    
    private void measureVertical(final int n, final int n2) {
        this.mFlexLines.clear();
        this.mFlexLinesResult.reset();
        this.mFlexboxHelper.calculateVerticalFlexLines(this.mFlexLinesResult, n, n2);
        this.mFlexLines = this.mFlexLinesResult.mFlexLines;
        this.mFlexboxHelper.determineMainSize(n, n2);
        this.mFlexboxHelper.determineCrossSize(n, n2, ((View)this).getPaddingLeft() + ((View)this).getPaddingRight());
        this.mFlexboxHelper.stretchViews();
        this.setMeasuredDimensionForFlex(this.mFlexDirection, n, n2, this.mFlexLinesResult.mChildState);
    }
    
    private void setMeasuredDimensionForFlex(int i, int n, final int n2, int n3) {
        final int mode = View$MeasureSpec.getMode(n);
        final int size = View$MeasureSpec.getSize(n);
        final int mode2 = View$MeasureSpec.getMode(n2);
        final int size2 = View$MeasureSpec.getSize(n2);
        int largestMainSize;
        if (i != 0 && i != 1) {
            if (i != 2 && i != 3) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid flex direction: ");
                sb.append(i);
                throw new IllegalArgumentException(sb.toString());
            }
            i = this.getLargestMainSize();
            largestMainSize = this.getSumOfCrossSize() + ((View)this).getPaddingLeft() + ((View)this).getPaddingRight();
        }
        else {
            i = this.getSumOfCrossSize() + ((View)this).getPaddingTop() + ((View)this).getPaddingBottom();
            largestMainSize = this.getLargestMainSize();
        }
        Label_0267: {
            int n4 = 0;
            int combineMeasuredStates = 0;
            Label_0179: {
                if (mode != Integer.MIN_VALUE) {
                    if (mode == 0) {
                        n = View.resolveSizeAndState(largestMainSize, n, n3);
                        break Label_0267;
                    }
                    if (mode != 1073741824) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Unknown width mode is set: ");
                        sb2.append(mode);
                        throw new IllegalStateException(sb2.toString());
                    }
                    n4 = size;
                    combineMeasuredStates = n3;
                    if (size >= largestMainSize) {
                        break Label_0179;
                    }
                }
                else if (size >= largestMainSize) {
                    n4 = largestMainSize;
                    combineMeasuredStates = n3;
                    break Label_0179;
                }
                combineMeasuredStates = View.combineMeasuredStates(n3, 16777216);
                n4 = size;
            }
            n = View.resolveSizeAndState(n4, n, combineMeasuredStates);
            n3 = combineMeasuredStates;
        }
        Label_0397: {
            int n5 = 0;
            int combineMeasuredStates2 = 0;
            Label_0316: {
                if (mode2 != Integer.MIN_VALUE) {
                    if (mode2 == 0) {
                        i = View.resolveSizeAndState(i, n2, n3);
                        break Label_0397;
                    }
                    if (mode2 != 1073741824) {
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("Unknown height mode is set: ");
                        sb3.append(mode2);
                        throw new IllegalStateException(sb3.toString());
                    }
                    n5 = size2;
                    combineMeasuredStates2 = n3;
                    if (size2 >= i) {
                        break Label_0316;
                    }
                }
                else if (size2 >= i) {
                    n5 = i;
                    combineMeasuredStates2 = n3;
                    break Label_0316;
                }
                combineMeasuredStates2 = View.combineMeasuredStates(n3, 256);
                n5 = size2;
            }
            i = View.resolveSizeAndState(n5, n2, combineMeasuredStates2);
        }
        ((View)this).setMeasuredDimension(n, i);
    }
    
    private void setWillNotDrawFlag() {
        ((View)this).setWillNotDraw(this.mDividerDrawableHorizontal == null && this.mDividerDrawableVertical == null);
    }
    
    public void addView(final View view, final int n, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        if (this.mOrderCache == null) {
            this.mOrderCache = new SparseIntArray(this.getChildCount());
        }
        this.mReorderedIndices = this.mFlexboxHelper.createReorderedIndices(view, n, viewGroup$LayoutParams, this.mOrderCache);
        super.addView(view, n, viewGroup$LayoutParams);
    }
    
    public boolean checkLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        return viewGroup$LayoutParams instanceof LayoutParams;
    }
    
    public ViewGroup$LayoutParams generateLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        if (viewGroup$LayoutParams instanceof LayoutParams) {
            return (ViewGroup$LayoutParams)new LayoutParams((LayoutParams)viewGroup$LayoutParams);
        }
        if (viewGroup$LayoutParams instanceof ViewGroup$MarginLayoutParams) {
            return (ViewGroup$LayoutParams)new LayoutParams((ViewGroup$MarginLayoutParams)viewGroup$LayoutParams);
        }
        return (ViewGroup$LayoutParams)new LayoutParams(viewGroup$LayoutParams);
    }
    
    public LayoutParams generateLayoutParams(final AttributeSet set) {
        return new LayoutParams(((View)this).getContext(), set);
    }
    
    public int getAlignContent() {
        return this.mAlignContent;
    }
    
    public int getAlignItems() {
        return this.mAlignItems;
    }
    
    public int getChildHeightMeasureSpec(final int n, final int n2, final int n3) {
        return ViewGroup.getChildMeasureSpec(n, n2, n3);
    }
    
    public int getChildWidthMeasureSpec(final int n, final int n2, final int n3) {
        return ViewGroup.getChildMeasureSpec(n, n2, n3);
    }
    
    public int getDecorationLengthCrossAxis(final View view) {
        return 0;
    }
    
    public int getDecorationLengthMainAxis(final View view, int n, final int n2) {
        final boolean mainAxisDirectionHorizontal = this.isMainAxisDirectionHorizontal();
        int n3 = 0;
        final int n4 = 0;
        if (mainAxisDirectionHorizontal) {
            n3 = n4;
            if (this.hasDividerBeforeChildAtAlongMainAxis(n, n2)) {
                n3 = 0 + this.mDividerVerticalWidth;
            }
            n = n3;
            if ((this.mShowDividerVertical & 0x4) <= 0) {
                return n;
            }
            n = this.mDividerVerticalWidth;
        }
        else {
            if (this.hasDividerBeforeChildAtAlongMainAxis(n, n2)) {
                n3 = 0 + this.mDividerHorizontalHeight;
            }
            n = n3;
            if ((this.mShowDividerHorizontal & 0x4) <= 0) {
                return n;
            }
            n = this.mDividerHorizontalHeight;
        }
        n += n3;
        return n;
    }
    
    public Drawable getDividerDrawableHorizontal() {
        return this.mDividerDrawableHorizontal;
    }
    
    public Drawable getDividerDrawableVertical() {
        return this.mDividerDrawableVertical;
    }
    
    public int getFlexDirection() {
        return this.mFlexDirection;
    }
    
    public View getFlexItemAt(final int n) {
        return this.getChildAt(n);
    }
    
    public int getFlexItemCount() {
        return this.getChildCount();
    }
    
    public List<FlexLine> getFlexLines() {
        final ArrayList list = new ArrayList(this.mFlexLines.size());
        for (final FlexLine flexLine : this.mFlexLines) {
            if (flexLine.getItemCountNotGone() == 0) {
                continue;
            }
            list.add(flexLine);
        }
        return list;
    }
    
    public List<FlexLine> getFlexLinesInternal() {
        return this.mFlexLines;
    }
    
    public int getFlexWrap() {
        return this.mFlexWrap;
    }
    
    public int getJustifyContent() {
        return this.mJustifyContent;
    }
    
    public int getLargestMainSize() {
        final Iterator<FlexLine> iterator = this.mFlexLines.iterator();
        int max = Integer.MIN_VALUE;
        while (iterator.hasNext()) {
            max = Math.max(max, iterator.next().mMainSize);
        }
        return max;
    }
    
    public int getMaxLine() {
        return this.mMaxLine;
    }
    
    public View getReorderedChildAt(final int n) {
        if (n >= 0) {
            final int[] mReorderedIndices = this.mReorderedIndices;
            if (n < mReorderedIndices.length) {
                return this.getChildAt(mReorderedIndices[n]);
            }
        }
        return null;
    }
    
    public View getReorderedFlexItemAt(final int n) {
        return this.getReorderedChildAt(n);
    }
    
    public int getShowDividerHorizontal() {
        return this.mShowDividerHorizontal;
    }
    
    public int getShowDividerVertical() {
        return this.mShowDividerVertical;
    }
    
    public int getSumOfCrossSize() {
        final int size = this.mFlexLines.size();
        int i = 0;
        int n = 0;
        while (i < size) {
            final FlexLine flexLine = this.mFlexLines.get(i);
            int n2 = n;
            if (this.hasDividerBeforeFlexLine(i)) {
                int n3;
                if (this.isMainAxisDirectionHorizontal()) {
                    n3 = this.mDividerHorizontalHeight;
                }
                else {
                    n3 = this.mDividerVerticalWidth;
                }
                n2 = n + n3;
            }
            int n4 = n2;
            if (this.hasEndDividerAfterFlexLine(i)) {
                int n5;
                if (this.isMainAxisDirectionHorizontal()) {
                    n5 = this.mDividerHorizontalHeight;
                }
                else {
                    n5 = this.mDividerVerticalWidth;
                }
                n4 = n2 + n5;
            }
            n = n4 + flexLine.mCrossSize;
            ++i;
        }
        return n;
    }
    
    public boolean isMainAxisDirectionHorizontal() {
        final int mFlexDirection = this.mFlexDirection;
        boolean b = true;
        if (mFlexDirection != 0) {
            b = (mFlexDirection == 1 && b);
        }
        return b;
    }
    
    public void onDraw(final Canvas canvas) {
        if (this.mDividerDrawableVertical == null && this.mDividerDrawableHorizontal == null) {
            return;
        }
        if (this.mShowDividerHorizontal == 0 && this.mShowDividerVertical == 0) {
            return;
        }
        final int b = o32.B((View)this);
        final int mFlexDirection = this.mFlexDirection;
        boolean b2 = false;
        final boolean b3 = false;
        boolean b4 = true;
        boolean b9 = false;
        Label_0214: {
            boolean b8;
            if (mFlexDirection != 0) {
                if (mFlexDirection != 1) {
                    if (mFlexDirection == 2) {
                        if (b != 1) {
                            b4 = false;
                        }
                        boolean b5 = b4;
                        if (this.mFlexWrap == 2) {
                            b5 = (b4 ^ true);
                        }
                        this.drawDividersVertical(canvas, b5, false);
                        return;
                    }
                    if (mFlexDirection != 3) {
                        return;
                    }
                    boolean b6 = b3;
                    if (b == 1) {
                        b6 = true;
                    }
                    boolean b7 = b6;
                    if (this.mFlexWrap == 2) {
                        b7 = (b6 ^ true);
                    }
                    this.drawDividersVertical(canvas, b7, true);
                    return;
                }
                else {
                    b8 = (b9 = (b != 1));
                    if (this.mFlexWrap != 2) {
                        break Label_0214;
                    }
                }
            }
            else {
                b8 = (b9 = (b == 1));
                if (this.mFlexWrap != 2) {
                    break Label_0214;
                }
            }
            b2 = true;
            b9 = b8;
        }
        this.drawDividersHorizontal(canvas, b9, b2);
    }
    
    public void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
        final int b2 = o32.B((View)this);
        final int mFlexDirection = this.mFlexDirection;
        boolean b3 = false;
        final boolean b4 = false;
        boolean b6 = false;
        Label_0177: {
            Label_0175: {
                if (mFlexDirection != 0) {
                    if (mFlexDirection != 1) {
                        boolean b5;
                        if (mFlexDirection != 2) {
                            if (mFlexDirection != 3) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("Invalid flex direction is set: ");
                                sb.append(this.mFlexDirection);
                                throw new IllegalStateException(sb.toString());
                            }
                            b3 = b4;
                            if (b2 == 1) {
                                b3 = true;
                            }
                            if (this.mFlexWrap == 2) {
                                b3 ^= true;
                            }
                            b5 = true;
                        }
                        else {
                            if (b2 == 1) {
                                b3 = true;
                            }
                            if (this.mFlexWrap == 2) {
                                b3 ^= true;
                            }
                            b5 = false;
                        }
                        this.layoutVertical(b3, b5, n, n2, n3, n4);
                        return;
                    }
                    if (b2 == 1) {
                        break Label_0175;
                    }
                }
                else if (b2 != 1) {
                    break Label_0175;
                }
                b6 = true;
                break Label_0177;
            }
            b6 = false;
        }
        this.layoutHorizontal(b6, n, n2, n3, n4);
    }
    
    public void onMeasure(final int n, final int n2) {
        if (this.mOrderCache == null) {
            this.mOrderCache = new SparseIntArray(this.getChildCount());
        }
        if (this.mFlexboxHelper.isOrderChangedFromLastMeasurement(this.mOrderCache)) {
            this.mReorderedIndices = this.mFlexboxHelper.createReorderedIndices(this.mOrderCache);
        }
        final int mFlexDirection = this.mFlexDirection;
        if (mFlexDirection != 0 && mFlexDirection != 1) {
            if (mFlexDirection != 2 && mFlexDirection != 3) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid value for the flex direction is set: ");
                sb.append(this.mFlexDirection);
                throw new IllegalStateException(sb.toString());
            }
            this.measureVertical(n, n2);
        }
        else {
            this.measureHorizontal(n, n2);
        }
    }
    
    public void onNewFlexItemAdded(final View view, int n, int n2, final FlexLine flexLine) {
        if (this.hasDividerBeforeChildAtAlongMainAxis(n, n2)) {
            if (this.isMainAxisDirectionHorizontal()) {
                n2 = flexLine.mMainSize;
                n = this.mDividerVerticalWidth;
            }
            else {
                n2 = flexLine.mMainSize;
                n = this.mDividerHorizontalHeight;
            }
            flexLine.mMainSize = n2 + n;
            flexLine.mDividerLengthInMainSize += n;
        }
    }
    
    public void onNewFlexLineAdded(final FlexLine flexLine) {
        int n;
        int n2;
        if (this.isMainAxisDirectionHorizontal()) {
            if ((this.mShowDividerVertical & 0x4) <= 0) {
                return;
            }
            n = flexLine.mMainSize;
            n2 = this.mDividerVerticalWidth;
        }
        else {
            if ((this.mShowDividerHorizontal & 0x4) <= 0) {
                return;
            }
            n = flexLine.mMainSize;
            n2 = this.mDividerHorizontalHeight;
        }
        flexLine.mMainSize = n + n2;
        flexLine.mDividerLengthInMainSize += n2;
    }
    
    public void setAlignContent(final int mAlignContent) {
        if (this.mAlignContent != mAlignContent) {
            this.mAlignContent = mAlignContent;
            ((View)this).requestLayout();
        }
    }
    
    public void setAlignItems(final int mAlignItems) {
        if (this.mAlignItems != mAlignItems) {
            this.mAlignItems = mAlignItems;
            ((View)this).requestLayout();
        }
    }
    
    public void setDividerDrawable(final Drawable drawable) {
        this.setDividerDrawableHorizontal(drawable);
        this.setDividerDrawableVertical(drawable);
    }
    
    public void setDividerDrawableHorizontal(final Drawable mDividerDrawableHorizontal) {
        if (mDividerDrawableHorizontal == this.mDividerDrawableHorizontal) {
            return;
        }
        int intrinsicHeight;
        if ((this.mDividerDrawableHorizontal = mDividerDrawableHorizontal) != null) {
            intrinsicHeight = mDividerDrawableHorizontal.getIntrinsicHeight();
        }
        else {
            intrinsicHeight = 0;
        }
        this.mDividerHorizontalHeight = intrinsicHeight;
        this.setWillNotDrawFlag();
        ((View)this).requestLayout();
    }
    
    public void setDividerDrawableVertical(final Drawable mDividerDrawableVertical) {
        if (mDividerDrawableVertical == this.mDividerDrawableVertical) {
            return;
        }
        int intrinsicWidth;
        if ((this.mDividerDrawableVertical = mDividerDrawableVertical) != null) {
            intrinsicWidth = mDividerDrawableVertical.getIntrinsicWidth();
        }
        else {
            intrinsicWidth = 0;
        }
        this.mDividerVerticalWidth = intrinsicWidth;
        this.setWillNotDrawFlag();
        ((View)this).requestLayout();
    }
    
    public void setFlexDirection(final int mFlexDirection) {
        if (this.mFlexDirection != mFlexDirection) {
            this.mFlexDirection = mFlexDirection;
            ((View)this).requestLayout();
        }
    }
    
    public void setFlexLines(final List<FlexLine> mFlexLines) {
        this.mFlexLines = mFlexLines;
    }
    
    public void setFlexWrap(final int mFlexWrap) {
        if (this.mFlexWrap != mFlexWrap) {
            this.mFlexWrap = mFlexWrap;
            ((View)this).requestLayout();
        }
    }
    
    public void setJustifyContent(final int mJustifyContent) {
        if (this.mJustifyContent != mJustifyContent) {
            this.mJustifyContent = mJustifyContent;
            ((View)this).requestLayout();
        }
    }
    
    public void setMaxLine(final int mMaxLine) {
        if (this.mMaxLine != mMaxLine) {
            this.mMaxLine = mMaxLine;
            ((View)this).requestLayout();
        }
    }
    
    public void setShowDivider(final int n) {
        this.setShowDividerVertical(n);
        this.setShowDividerHorizontal(n);
    }
    
    public void setShowDividerHorizontal(final int mShowDividerHorizontal) {
        if (mShowDividerHorizontal != this.mShowDividerHorizontal) {
            this.mShowDividerHorizontal = mShowDividerHorizontal;
            ((View)this).requestLayout();
        }
    }
    
    public void setShowDividerVertical(final int mShowDividerVertical) {
        if (mShowDividerVertical != this.mShowDividerVertical) {
            this.mShowDividerVertical = mShowDividerVertical;
            ((View)this).requestLayout();
        }
    }
    
    public void updateViewCache(final int n, final View view) {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface DividerMode {
    }
    
    public static class LayoutParams extends ViewGroup$MarginLayoutParams implements FlexItem
    {
        public static final Parcelable$Creator<LayoutParams> CREATOR;
        private int mAlignSelf;
        private float mFlexBasisPercent;
        private float mFlexGrow;
        private float mFlexShrink;
        private int mMaxHeight;
        private int mMaxWidth;
        private int mMinHeight;
        private int mMinWidth;
        private int mOrder;
        private boolean mWrapBefore;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$Creator<LayoutParams>() {
                public LayoutParams createFromParcel(final Parcel parcel) {
                    return new LayoutParams(parcel);
                }
                
                public LayoutParams[] newArray(final int n) {
                    return new LayoutParams[n];
                }
            };
        }
        
        public LayoutParams(final int n, final int n2) {
            super(new ViewGroup$LayoutParams(n, n2));
            this.mOrder = 1;
            this.mFlexGrow = 0.0f;
            this.mFlexShrink = 1.0f;
            this.mAlignSelf = -1;
            this.mFlexBasisPercent = -1.0f;
            this.mMinWidth = -1;
            this.mMinHeight = -1;
            this.mMaxWidth = 16777215;
            this.mMaxHeight = 16777215;
        }
        
        public LayoutParams(final Context context, final AttributeSet set) {
            super(context, set);
            this.mOrder = 1;
            this.mFlexGrow = 0.0f;
            this.mFlexShrink = 1.0f;
            this.mAlignSelf = -1;
            this.mFlexBasisPercent = -1.0f;
            this.mMinWidth = -1;
            this.mMinHeight = -1;
            this.mMaxWidth = 16777215;
            this.mMaxHeight = 16777215;
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.FlexboxLayout_Layout);
            this.mOrder = obtainStyledAttributes.getInt(R.styleable.FlexboxLayout_Layout_layout_order, 1);
            this.mFlexGrow = obtainStyledAttributes.getFloat(R.styleable.FlexboxLayout_Layout_layout_flexGrow, 0.0f);
            this.mFlexShrink = obtainStyledAttributes.getFloat(R.styleable.FlexboxLayout_Layout_layout_flexShrink, 1.0f);
            this.mAlignSelf = obtainStyledAttributes.getInt(R.styleable.FlexboxLayout_Layout_layout_alignSelf, -1);
            this.mFlexBasisPercent = obtainStyledAttributes.getFraction(R.styleable.FlexboxLayout_Layout_layout_flexBasisPercent, 1, 1, -1.0f);
            this.mMinWidth = obtainStyledAttributes.getDimensionPixelSize(R.styleable.FlexboxLayout_Layout_layout_minWidth, -1);
            this.mMinHeight = obtainStyledAttributes.getDimensionPixelSize(R.styleable.FlexboxLayout_Layout_layout_minHeight, -1);
            this.mMaxWidth = obtainStyledAttributes.getDimensionPixelSize(R.styleable.FlexboxLayout_Layout_layout_maxWidth, 16777215);
            this.mMaxHeight = obtainStyledAttributes.getDimensionPixelSize(R.styleable.FlexboxLayout_Layout_layout_maxHeight, 16777215);
            this.mWrapBefore = obtainStyledAttributes.getBoolean(R.styleable.FlexboxLayout_Layout_layout_wrapBefore, false);
            obtainStyledAttributes.recycle();
        }
        
        public LayoutParams(final Parcel parcel) {
            boolean mWrapBefore = false;
            super(0, 0);
            this.mOrder = 1;
            this.mFlexGrow = 0.0f;
            this.mFlexShrink = 1.0f;
            this.mAlignSelf = -1;
            this.mFlexBasisPercent = -1.0f;
            this.mMinWidth = -1;
            this.mMinHeight = -1;
            this.mMaxWidth = 16777215;
            this.mMaxHeight = 16777215;
            this.mOrder = parcel.readInt();
            this.mFlexGrow = parcel.readFloat();
            this.mFlexShrink = parcel.readFloat();
            this.mAlignSelf = parcel.readInt();
            this.mFlexBasisPercent = parcel.readFloat();
            this.mMinWidth = parcel.readInt();
            this.mMinHeight = parcel.readInt();
            this.mMaxWidth = parcel.readInt();
            this.mMaxHeight = parcel.readInt();
            if (parcel.readByte() != 0) {
                mWrapBefore = true;
            }
            this.mWrapBefore = mWrapBefore;
            super.bottomMargin = parcel.readInt();
            super.leftMargin = parcel.readInt();
            super.rightMargin = parcel.readInt();
            super.topMargin = parcel.readInt();
            super.height = parcel.readInt();
            super.width = parcel.readInt();
        }
        
        public LayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
            super(viewGroup$LayoutParams);
            this.mOrder = 1;
            this.mFlexGrow = 0.0f;
            this.mFlexShrink = 1.0f;
            this.mAlignSelf = -1;
            this.mFlexBasisPercent = -1.0f;
            this.mMinWidth = -1;
            this.mMinHeight = -1;
            this.mMaxWidth = 16777215;
            this.mMaxHeight = 16777215;
        }
        
        public LayoutParams(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
            super(viewGroup$MarginLayoutParams);
            this.mOrder = 1;
            this.mFlexGrow = 0.0f;
            this.mFlexShrink = 1.0f;
            this.mAlignSelf = -1;
            this.mFlexBasisPercent = -1.0f;
            this.mMinWidth = -1;
            this.mMinHeight = -1;
            this.mMaxWidth = 16777215;
            this.mMaxHeight = 16777215;
        }
        
        public LayoutParams(final LayoutParams layoutParams) {
            super((ViewGroup$MarginLayoutParams)layoutParams);
            this.mOrder = 1;
            this.mFlexGrow = 0.0f;
            this.mFlexShrink = 1.0f;
            this.mAlignSelf = -1;
            this.mFlexBasisPercent = -1.0f;
            this.mMinWidth = -1;
            this.mMinHeight = -1;
            this.mMaxWidth = 16777215;
            this.mMaxHeight = 16777215;
            this.mOrder = layoutParams.mOrder;
            this.mFlexGrow = layoutParams.mFlexGrow;
            this.mFlexShrink = layoutParams.mFlexShrink;
            this.mAlignSelf = layoutParams.mAlignSelf;
            this.mFlexBasisPercent = layoutParams.mFlexBasisPercent;
            this.mMinWidth = layoutParams.mMinWidth;
            this.mMinHeight = layoutParams.mMinHeight;
            this.mMaxWidth = layoutParams.mMaxWidth;
            this.mMaxHeight = layoutParams.mMaxHeight;
            this.mWrapBefore = layoutParams.mWrapBefore;
        }
        
        public int describeContents() {
            return 0;
        }
        
        public int getAlignSelf() {
            return this.mAlignSelf;
        }
        
        public float getFlexBasisPercent() {
            return this.mFlexBasisPercent;
        }
        
        public float getFlexGrow() {
            return this.mFlexGrow;
        }
        
        public float getFlexShrink() {
            return this.mFlexShrink;
        }
        
        public int getHeight() {
            return super.height;
        }
        
        public int getMarginBottom() {
            return super.bottomMargin;
        }
        
        public int getMarginLeft() {
            return super.leftMargin;
        }
        
        public int getMarginRight() {
            return super.rightMargin;
        }
        
        public int getMarginTop() {
            return super.topMargin;
        }
        
        public int getMaxHeight() {
            return this.mMaxHeight;
        }
        
        public int getMaxWidth() {
            return this.mMaxWidth;
        }
        
        public int getMinHeight() {
            return this.mMinHeight;
        }
        
        public int getMinWidth() {
            return this.mMinWidth;
        }
        
        public int getOrder() {
            return this.mOrder;
        }
        
        public int getWidth() {
            return super.width;
        }
        
        public boolean isWrapBefore() {
            return this.mWrapBefore;
        }
        
        public void setAlignSelf(final int mAlignSelf) {
            this.mAlignSelf = mAlignSelf;
        }
        
        public void setFlexBasisPercent(final float mFlexBasisPercent) {
            this.mFlexBasisPercent = mFlexBasisPercent;
        }
        
        public void setFlexGrow(final float mFlexGrow) {
            this.mFlexGrow = mFlexGrow;
        }
        
        public void setFlexShrink(final float mFlexShrink) {
            this.mFlexShrink = mFlexShrink;
        }
        
        public void setHeight(final int height) {
            super.height = height;
        }
        
        public void setMaxHeight(final int mMaxHeight) {
            this.mMaxHeight = mMaxHeight;
        }
        
        public void setMaxWidth(final int mMaxWidth) {
            this.mMaxWidth = mMaxWidth;
        }
        
        public void setMinHeight(final int mMinHeight) {
            this.mMinHeight = mMinHeight;
        }
        
        public void setMinWidth(final int mMinWidth) {
            this.mMinWidth = mMinWidth;
        }
        
        public void setOrder(final int mOrder) {
            this.mOrder = mOrder;
        }
        
        public void setWidth(final int width) {
            super.width = width;
        }
        
        public void setWrapBefore(final boolean mWrapBefore) {
            this.mWrapBefore = mWrapBefore;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            parcel.writeInt(this.mOrder);
            parcel.writeFloat(this.mFlexGrow);
            parcel.writeFloat(this.mFlexShrink);
            parcel.writeInt(this.mAlignSelf);
            parcel.writeFloat(this.mFlexBasisPercent);
            parcel.writeInt(this.mMinWidth);
            parcel.writeInt(this.mMinHeight);
            parcel.writeInt(this.mMaxWidth);
            parcel.writeInt(this.mMaxHeight);
            parcel.writeByte((byte)(byte)(this.mWrapBefore ? 1 : 0));
            parcel.writeInt(super.bottomMargin);
            parcel.writeInt(super.leftMargin);
            parcel.writeInt(super.rightMargin);
            parcel.writeInt(super.topMargin);
            parcel.writeInt(super.height);
            parcel.writeInt(super.width);
        }
    }
}

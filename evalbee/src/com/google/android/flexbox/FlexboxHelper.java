// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.flexbox;

import android.view.ViewGroup$MarginLayoutParams;
import android.view.ViewGroup$LayoutParams;
import java.util.Iterator;
import java.util.Collections;
import android.util.SparseIntArray;
import android.graphics.drawable.Drawable;
import android.widget.CompoundButton;
import java.util.Arrays;
import java.util.ArrayList;
import android.view.View$MeasureSpec;
import android.view.View;
import java.util.List;

class FlexboxHelper
{
    static final boolean $assertionsDisabled = false;
    private static final int INITIAL_CAPACITY = 10;
    private static final long MEASURE_SPEC_WIDTH_MASK = 4294967295L;
    private boolean[] mChildrenFrozen;
    private final FlexContainer mFlexContainer;
    int[] mIndexToFlexLine;
    long[] mMeasureSpecCache;
    private long[] mMeasuredSizeCache;
    
    public FlexboxHelper(final FlexContainer mFlexContainer) {
        this.mFlexContainer = mFlexContainer;
    }
    
    private void addFlexLine(final List<FlexLine> list, final FlexLine flexLine, final int mLastIndex, final int mSumCrossSizeBefore) {
        flexLine.mSumCrossSizeBefore = mSumCrossSizeBefore;
        this.mFlexContainer.onNewFlexLineAdded(flexLine);
        flexLine.mLastIndex = mLastIndex;
        list.add(flexLine);
    }
    
    private void checkSizeConstraints(final View view, final int n) {
        final FlexItem flexItem = (FlexItem)view.getLayoutParams();
        int measuredWidth = view.getMeasuredWidth();
        final int measuredHeight = view.getMeasuredHeight();
        final int minWidth = flexItem.getMinWidth();
        int n2 = 1;
        int n4 = 0;
        Label_0083: {
            int n3;
            if (measuredWidth < minWidth) {
                n3 = flexItem.getMinWidth();
            }
            else {
                if (measuredWidth <= flexItem.getMaxWidth()) {
                    n4 = 0;
                    break Label_0083;
                }
                n3 = flexItem.getMaxWidth();
            }
            final int n5 = 1;
            measuredWidth = n3;
            n4 = n5;
        }
        int n6;
        if (measuredHeight < flexItem.getMinHeight()) {
            n6 = flexItem.getMinHeight();
        }
        else if (measuredHeight > flexItem.getMaxHeight()) {
            n6 = flexItem.getMaxHeight();
        }
        else {
            n2 = n4;
            n6 = measuredHeight;
        }
        if (n2 != 0) {
            final int measureSpec = View$MeasureSpec.makeMeasureSpec(measuredWidth, 1073741824);
            final int measureSpec2 = View$MeasureSpec.makeMeasureSpec(n6, 1073741824);
            view.measure(measureSpec, measureSpec2);
            this.updateMeasureCache(n, measureSpec, measureSpec2, view);
            this.mFlexContainer.updateViewCache(n, view);
        }
    }
    
    private List<FlexLine> constructFlexLinesForAlignContentCenter(final List<FlexLine> list, int i, int size) {
        i = (i - size) / 2;
        final ArrayList list2 = new ArrayList();
        final FlexLine flexLine = new FlexLine();
        flexLine.mCrossSize = i;
        for (size = list.size(), i = 0; i < size; ++i) {
            if (i == 0) {
                list2.add(flexLine);
            }
            list2.add(list.get(i));
            if (i == list.size() - 1) {
                list2.add(flexLine);
            }
        }
        return list2;
    }
    
    private List<Order> createOrders(final int initialCapacity) {
        final ArrayList list = new ArrayList(initialCapacity);
        for (int i = 0; i < initialCapacity; ++i) {
            final FlexItem flexItem = (FlexItem)this.mFlexContainer.getFlexItemAt(i).getLayoutParams();
            final Order order = new Order(null);
            order.order = flexItem.getOrder();
            order.index = i;
            list.add(order);
        }
        return list;
    }
    
    private void ensureChildrenFrozen(final int n) {
        final boolean[] mChildrenFrozen = this.mChildrenFrozen;
        if (mChildrenFrozen == null) {
            this.mChildrenFrozen = new boolean[Math.max(n, 10)];
        }
        else if (mChildrenFrozen.length < n) {
            this.mChildrenFrozen = new boolean[Math.max(mChildrenFrozen.length * 2, n)];
        }
        else {
            Arrays.fill(mChildrenFrozen, false);
        }
    }
    
    private void evaluateMinimumSizeForCompoundButton(final CompoundButton compoundButton) {
        final FlexItem flexItem = (FlexItem)((View)compoundButton).getLayoutParams();
        final int minWidth = flexItem.getMinWidth();
        final int minHeight = flexItem.getMinHeight();
        final Drawable a = yj.a(compoundButton);
        int minimumHeight = 0;
        int minimumWidth;
        if (a == null) {
            minimumWidth = 0;
        }
        else {
            minimumWidth = a.getMinimumWidth();
        }
        if (a != null) {
            minimumHeight = a.getMinimumHeight();
        }
        int minWidth2 = minWidth;
        if (minWidth == -1) {
            minWidth2 = minimumWidth;
        }
        flexItem.setMinWidth(minWidth2);
        int minHeight2;
        if ((minHeight2 = minHeight) == -1) {
            minHeight2 = minimumHeight;
        }
        flexItem.setMinHeight(minHeight2);
    }
    
    private void expandFlexItems(final int n, final int n2, final FlexLine flexLine, final int n3, final int n4, final boolean b) {
        final float mTotalFlexGrow = flexLine.mTotalFlexGrow;
        if (mTotalFlexGrow > 0.0f) {
            final int mMainSize = flexLine.mMainSize;
            if (n3 >= mMainSize) {
                final float n5 = (n3 - mMainSize) / mTotalFlexGrow;
                flexLine.mMainSize = n4 + flexLine.mDividerLengthInMainSize;
                if (!b) {
                    flexLine.mCrossSize = Integer.MIN_VALUE;
                }
                int i = 0;
                int n6 = 0;
                int n7 = 0;
                float n8 = 0.0f;
                while (i < flexLine.mItemCount) {
                    final int n9 = flexLine.mFirstIndex + i;
                    final View reorderedFlexItem = this.mFlexContainer.getReorderedFlexItemAt(n9);
                    if (reorderedFlexItem != null && reorderedFlexItem.getVisibility() != 8) {
                        final FlexItem flexItem = (FlexItem)reorderedFlexItem.getLayoutParams();
                        final int flexDirection = this.mFlexContainer.getFlexDirection();
                        int max2;
                        if (flexDirection != 0 && flexDirection != 1) {
                            int n10 = reorderedFlexItem.getMeasuredHeight();
                            final long[] mMeasuredSizeCache = this.mMeasuredSizeCache;
                            if (mMeasuredSizeCache != null) {
                                n10 = this.extractHigherInt(mMeasuredSizeCache[n9]);
                            }
                            int n11 = reorderedFlexItem.getMeasuredWidth();
                            final long[] mMeasuredSizeCache2 = this.mMeasuredSizeCache;
                            if (mMeasuredSizeCache2 != null) {
                                n11 = this.extractLowerInt(mMeasuredSizeCache2[n9]);
                            }
                            if (!this.mChildrenFrozen[n9] && flexItem.getFlexGrow() > 0.0f) {
                                final float n12 = n10 + flexItem.getFlexGrow() * n5;
                                float n13 = n8;
                                float a = n12;
                                if (i == flexLine.mItemCount - 1) {
                                    a = n12 + n8;
                                    n13 = 0.0f;
                                }
                                final int round = Math.round(a);
                                int maxHeight = 0;
                                int n14 = 0;
                                Label_0424: {
                                    if (round > flexItem.getMaxHeight()) {
                                        maxHeight = flexItem.getMaxHeight();
                                        this.mChildrenFrozen[n9] = true;
                                        flexLine.mTotalFlexGrow -= flexItem.getFlexGrow();
                                        n14 = 1;
                                        n8 = n13;
                                    }
                                    else {
                                        n8 = n13 + (a - round);
                                        final double n15 = n8;
                                        double n16;
                                        if (n15 > 1.0) {
                                            maxHeight = round + 1;
                                            n16 = n15 - 1.0;
                                        }
                                        else {
                                            n14 = n6;
                                            maxHeight = round;
                                            if (n15 >= -1.0) {
                                                break Label_0424;
                                            }
                                            maxHeight = round - 1;
                                            n16 = n15 + 1.0;
                                        }
                                        n8 = (float)n16;
                                        n14 = n6;
                                    }
                                }
                                final int childWidthMeasureSpecInternal = this.getChildWidthMeasureSpecInternal(n, flexItem, flexLine.mSumCrossSizeBefore);
                                final int measureSpec = View$MeasureSpec.makeMeasureSpec(maxHeight, 1073741824);
                                reorderedFlexItem.measure(childWidthMeasureSpecInternal, measureSpec);
                                final int measuredWidth = reorderedFlexItem.getMeasuredWidth();
                                final int measuredHeight = reorderedFlexItem.getMeasuredHeight();
                                this.updateMeasureCache(n9, childWidthMeasureSpecInternal, measureSpec, reorderedFlexItem);
                                this.mFlexContainer.updateViewCache(n9, reorderedFlexItem);
                                n6 = n14;
                                n10 = measuredHeight;
                                n11 = measuredWidth;
                            }
                            final int max = Math.max(n7, n11 + flexItem.getMarginLeft() + flexItem.getMarginRight() + this.mFlexContainer.getDecorationLengthCrossAxis(reorderedFlexItem));
                            flexLine.mMainSize += n10 + flexItem.getMarginTop() + flexItem.getMarginBottom();
                            max2 = max;
                        }
                        else {
                            int n17 = reorderedFlexItem.getMeasuredWidth();
                            final long[] mMeasuredSizeCache3 = this.mMeasuredSizeCache;
                            if (mMeasuredSizeCache3 != null) {
                                n17 = this.extractLowerInt(mMeasuredSizeCache3[n9]);
                            }
                            final int measuredHeight2 = reorderedFlexItem.getMeasuredHeight();
                            final long[] mMeasuredSizeCache4 = this.mMeasuredSizeCache;
                            int higherInt = measuredHeight2;
                            if (mMeasuredSizeCache4 != null) {
                                higherInt = this.extractHigherInt(mMeasuredSizeCache4[n9]);
                            }
                            int measuredWidth2;
                            int measuredHeight3;
                            if (!this.mChildrenFrozen[n9] && flexItem.getFlexGrow() > 0.0f) {
                                float a2;
                                final float n18 = a2 = n17 + flexItem.getFlexGrow() * n5;
                                float n19 = n8;
                                if (i == flexLine.mItemCount - 1) {
                                    a2 = n18 + n8;
                                    n19 = 0.0f;
                                }
                                final int round2 = Math.round(a2);
                                int maxWidth = 0;
                                int n20 = 0;
                                Label_0858: {
                                    if (round2 > flexItem.getMaxWidth()) {
                                        maxWidth = flexItem.getMaxWidth();
                                        this.mChildrenFrozen[n9] = true;
                                        flexLine.mTotalFlexGrow -= flexItem.getFlexGrow();
                                        n20 = 1;
                                        n8 = n19;
                                    }
                                    else {
                                        n8 = n19 + (a2 - round2);
                                        final double n21 = n8;
                                        double n22;
                                        if (n21 > 1.0) {
                                            maxWidth = round2 + 1;
                                            n22 = n21 - 1.0;
                                        }
                                        else {
                                            maxWidth = round2;
                                            n20 = n6;
                                            if (n21 >= -1.0) {
                                                break Label_0858;
                                            }
                                            maxWidth = round2 - 1;
                                            n22 = n21 + 1.0;
                                        }
                                        n8 = (float)n22;
                                        n20 = n6;
                                    }
                                }
                                final int childHeightMeasureSpecInternal = this.getChildHeightMeasureSpecInternal(n2, flexItem, flexLine.mSumCrossSizeBefore);
                                final int measureSpec2 = View$MeasureSpec.makeMeasureSpec(maxWidth, 1073741824);
                                reorderedFlexItem.measure(measureSpec2, childHeightMeasureSpecInternal);
                                measuredWidth2 = reorderedFlexItem.getMeasuredWidth();
                                measuredHeight3 = reorderedFlexItem.getMeasuredHeight();
                                this.updateMeasureCache(n9, measureSpec2, childHeightMeasureSpecInternal, reorderedFlexItem);
                                this.mFlexContainer.updateViewCache(n9, reorderedFlexItem);
                                n6 = n20;
                            }
                            else {
                                measuredHeight3 = higherInt;
                                measuredWidth2 = n17;
                            }
                            max2 = Math.max(n7, measuredHeight3 + flexItem.getMarginTop() + flexItem.getMarginBottom() + this.mFlexContainer.getDecorationLengthCrossAxis(reorderedFlexItem));
                            flexLine.mMainSize += measuredWidth2 + flexItem.getMarginLeft() + flexItem.getMarginRight();
                        }
                        flexLine.mCrossSize = Math.max(flexLine.mCrossSize, max2);
                        n7 = max2;
                    }
                    ++i;
                }
                if (n6 != 0 && mMainSize != flexLine.mMainSize) {
                    this.expandFlexItems(n, n2, flexLine, n3, n4, true);
                }
            }
        }
    }
    
    private int getChildHeightMeasureSpecInternal(int n, final FlexItem flexItem, int childHeightMeasureSpec) {
        final FlexContainer mFlexContainer = this.mFlexContainer;
        childHeightMeasureSpec = mFlexContainer.getChildHeightMeasureSpec(n, mFlexContainer.getPaddingTop() + this.mFlexContainer.getPaddingBottom() + flexItem.getMarginTop() + flexItem.getMarginBottom() + childHeightMeasureSpec, flexItem.getHeight());
        final int size = View$MeasureSpec.getSize(childHeightMeasureSpec);
        if (size > flexItem.getMaxHeight()) {
            n = flexItem.getMaxHeight();
        }
        else {
            n = childHeightMeasureSpec;
            if (size >= flexItem.getMinHeight()) {
                return n;
            }
            n = flexItem.getMinHeight();
        }
        n = View$MeasureSpec.makeMeasureSpec(n, View$MeasureSpec.getMode(childHeightMeasureSpec));
        return n;
    }
    
    private int getChildWidthMeasureSpecInternal(int n, final FlexItem flexItem, int childWidthMeasureSpec) {
        final FlexContainer mFlexContainer = this.mFlexContainer;
        childWidthMeasureSpec = mFlexContainer.getChildWidthMeasureSpec(n, mFlexContainer.getPaddingLeft() + this.mFlexContainer.getPaddingRight() + flexItem.getMarginLeft() + flexItem.getMarginRight() + childWidthMeasureSpec, flexItem.getWidth());
        final int size = View$MeasureSpec.getSize(childWidthMeasureSpec);
        if (size > flexItem.getMaxWidth()) {
            n = flexItem.getMaxWidth();
        }
        else {
            n = childWidthMeasureSpec;
            if (size >= flexItem.getMinWidth()) {
                return n;
            }
            n = flexItem.getMinWidth();
        }
        n = View$MeasureSpec.makeMeasureSpec(n, View$MeasureSpec.getMode(childWidthMeasureSpec));
        return n;
    }
    
    private int getFlexItemMarginEndCross(final FlexItem flexItem, final boolean b) {
        if (b) {
            return flexItem.getMarginBottom();
        }
        return flexItem.getMarginRight();
    }
    
    private int getFlexItemMarginEndMain(final FlexItem flexItem, final boolean b) {
        if (b) {
            return flexItem.getMarginRight();
        }
        return flexItem.getMarginBottom();
    }
    
    private int getFlexItemMarginStartCross(final FlexItem flexItem, final boolean b) {
        if (b) {
            return flexItem.getMarginTop();
        }
        return flexItem.getMarginLeft();
    }
    
    private int getFlexItemMarginStartMain(final FlexItem flexItem, final boolean b) {
        if (b) {
            return flexItem.getMarginLeft();
        }
        return flexItem.getMarginTop();
    }
    
    private int getFlexItemSizeCross(final FlexItem flexItem, final boolean b) {
        if (b) {
            return flexItem.getHeight();
        }
        return flexItem.getWidth();
    }
    
    private int getFlexItemSizeMain(final FlexItem flexItem, final boolean b) {
        if (b) {
            return flexItem.getWidth();
        }
        return flexItem.getHeight();
    }
    
    private int getPaddingEndCross(final boolean b) {
        if (b) {
            return this.mFlexContainer.getPaddingBottom();
        }
        return this.mFlexContainer.getPaddingEnd();
    }
    
    private int getPaddingEndMain(final boolean b) {
        if (b) {
            return this.mFlexContainer.getPaddingEnd();
        }
        return this.mFlexContainer.getPaddingBottom();
    }
    
    private int getPaddingStartCross(final boolean b) {
        if (b) {
            return this.mFlexContainer.getPaddingTop();
        }
        return this.mFlexContainer.getPaddingStart();
    }
    
    private int getPaddingStartMain(final boolean b) {
        if (b) {
            return this.mFlexContainer.getPaddingStart();
        }
        return this.mFlexContainer.getPaddingTop();
    }
    
    private int getViewMeasuredSizeCross(final View view, final boolean b) {
        if (b) {
            return view.getMeasuredHeight();
        }
        return view.getMeasuredWidth();
    }
    
    private int getViewMeasuredSizeMain(final View view, final boolean b) {
        if (b) {
            return view.getMeasuredWidth();
        }
        return view.getMeasuredHeight();
    }
    
    private boolean isLastFlexItem(final int n, final int n2, final FlexLine flexLine) {
        boolean b = true;
        if (n != n2 - 1 || flexLine.getItemCountNotGone() == 0) {
            b = false;
        }
        return b;
    }
    
    private boolean isWrapRequired(final View view, int maxLine, final int n, final int n2, final int n3, final FlexItem flexItem, int decorationLengthMainAxis, final int n4, final int n5) {
        final int flexWrap = this.mFlexContainer.getFlexWrap();
        boolean b = false;
        if (flexWrap == 0) {
            return false;
        }
        if (flexItem.isWrapBefore()) {
            return true;
        }
        if (maxLine == 0) {
            return false;
        }
        maxLine = this.mFlexContainer.getMaxLine();
        if (maxLine != -1 && maxLine <= n5 + 1) {
            return false;
        }
        decorationLengthMainAxis = this.mFlexContainer.getDecorationLengthMainAxis(view, decorationLengthMainAxis, n4);
        maxLine = n3;
        if (decorationLengthMainAxis > 0) {
            maxLine = n3 + decorationLengthMainAxis;
        }
        if (n < n2 + maxLine) {
            b = true;
        }
        return b;
    }
    
    private void shrinkFlexItems(final int n, final int n2, final FlexLine flexLine, final int n3, final int n4, final boolean b) {
        final int mMainSize = flexLine.mMainSize;
        final float mTotalFlexShrink = flexLine.mTotalFlexShrink;
        if (mTotalFlexShrink > 0.0f) {
            if (n3 <= mMainSize) {
                final float n5 = (mMainSize - n3) / mTotalFlexShrink;
                flexLine.mMainSize = n4 + flexLine.mDividerLengthInMainSize;
                if (!b) {
                    flexLine.mCrossSize = Integer.MIN_VALUE;
                }
                int i = 0;
                int n6 = 0;
                int n7 = 0;
                float n8 = 0.0f;
                while (i < flexLine.mItemCount) {
                    final int n9 = flexLine.mFirstIndex + i;
                    final View reorderedFlexItem = this.mFlexContainer.getReorderedFlexItemAt(n9);
                    if (reorderedFlexItem != null) {
                        if (reorderedFlexItem.getVisibility() != 8) {
                            final FlexItem flexItem = (FlexItem)reorderedFlexItem.getLayoutParams();
                            final int flexDirection = this.mFlexContainer.getFlexDirection();
                            int b2;
                            if (flexDirection != 0 && flexDirection != 1) {
                                int n10 = reorderedFlexItem.getMeasuredHeight();
                                final long[] mMeasuredSizeCache = this.mMeasuredSizeCache;
                                if (mMeasuredSizeCache != null) {
                                    n10 = this.extractHigherInt(mMeasuredSizeCache[n9]);
                                }
                                int n11 = reorderedFlexItem.getMeasuredWidth();
                                final long[] mMeasuredSizeCache2 = this.mMeasuredSizeCache;
                                if (mMeasuredSizeCache2 != null) {
                                    n11 = this.extractLowerInt(mMeasuredSizeCache2[n9]);
                                }
                                if (!this.mChildrenFrozen[n9] && flexItem.getFlexShrink() > 0.0f) {
                                    final float n12 = n10 - flexItem.getFlexShrink() * n5;
                                    float n13 = n8;
                                    float a = n12;
                                    if (i == flexLine.mItemCount - 1) {
                                        a = n12 + n8;
                                        n13 = 0.0f;
                                    }
                                    final int round = Math.round(a);
                                    int minHeight;
                                    int n14;
                                    if (round < flexItem.getMinHeight()) {
                                        minHeight = flexItem.getMinHeight();
                                        this.mChildrenFrozen[n9] = true;
                                        flexLine.mTotalFlexShrink -= flexItem.getFlexShrink();
                                        n14 = 1;
                                        n8 = n13;
                                    }
                                    else {
                                        final float n15 = n13 + (a - round);
                                        final double n16 = n15;
                                        if (n16 > 1.0) {
                                            minHeight = round + 1;
                                            n8 = n15 - 1.0f;
                                            n14 = n6;
                                        }
                                        else {
                                            n14 = n6;
                                            n8 = n15;
                                            minHeight = round;
                                            if (n16 < -1.0) {
                                                minHeight = round - 1;
                                                n8 = n15 + 1.0f;
                                                n14 = n6;
                                            }
                                        }
                                    }
                                    final int childWidthMeasureSpecInternal = this.getChildWidthMeasureSpecInternal(n, flexItem, flexLine.mSumCrossSizeBefore);
                                    final int measureSpec = View$MeasureSpec.makeMeasureSpec(minHeight, 1073741824);
                                    reorderedFlexItem.measure(childWidthMeasureSpecInternal, measureSpec);
                                    final int measuredWidth = reorderedFlexItem.getMeasuredWidth();
                                    n10 = reorderedFlexItem.getMeasuredHeight();
                                    this.updateMeasureCache(n9, childWidthMeasureSpecInternal, measureSpec, reorderedFlexItem);
                                    this.mFlexContainer.updateViewCache(n9, reorderedFlexItem);
                                    n6 = n14;
                                    n11 = measuredWidth;
                                }
                                final int max = Math.max(n7, n11 + flexItem.getMarginLeft() + flexItem.getMarginRight() + this.mFlexContainer.getDecorationLengthCrossAxis(reorderedFlexItem));
                                flexLine.mMainSize += n10 + flexItem.getMarginTop() + flexItem.getMarginBottom();
                                b2 = max;
                            }
                            else {
                                int n17 = reorderedFlexItem.getMeasuredWidth();
                                final long[] mMeasuredSizeCache3 = this.mMeasuredSizeCache;
                                if (mMeasuredSizeCache3 != null) {
                                    n17 = this.extractLowerInt(mMeasuredSizeCache3[n9]);
                                }
                                int n18 = reorderedFlexItem.getMeasuredHeight();
                                final long[] mMeasuredSizeCache4 = this.mMeasuredSizeCache;
                                if (mMeasuredSizeCache4 != null) {
                                    n18 = this.extractHigherInt(mMeasuredSizeCache4[n9]);
                                }
                                int measuredHeight;
                                if (!this.mChildrenFrozen[n9] && flexItem.getFlexShrink() > 0.0f) {
                                    float a2;
                                    final float n19 = a2 = n17 - flexItem.getFlexShrink() * n5;
                                    float n20 = n8;
                                    if (i == flexLine.mItemCount - 1) {
                                        a2 = n19 + n8;
                                        n20 = 0.0f;
                                    }
                                    final int round2 = Math.round(a2);
                                    int minWidth;
                                    int n21;
                                    if (round2 < flexItem.getMinWidth()) {
                                        minWidth = flexItem.getMinWidth();
                                        this.mChildrenFrozen[n9] = true;
                                        flexLine.mTotalFlexShrink -= flexItem.getFlexShrink();
                                        n21 = 1;
                                        n8 = n20;
                                    }
                                    else {
                                        final float n22 = n20 + (a2 - round2);
                                        final double n23 = n22;
                                        if (n23 > 1.0) {
                                            minWidth = round2 + 1;
                                            n8 = n22 - 1.0f;
                                            n21 = n6;
                                        }
                                        else {
                                            minWidth = round2;
                                            n21 = n6;
                                            n8 = n22;
                                            if (n23 < -1.0) {
                                                minWidth = round2 - 1;
                                                n8 = n22 + 1.0f;
                                                n21 = n6;
                                            }
                                        }
                                    }
                                    final int childHeightMeasureSpecInternal = this.getChildHeightMeasureSpecInternal(n2, flexItem, flexLine.mSumCrossSizeBefore);
                                    final int measureSpec2 = View$MeasureSpec.makeMeasureSpec(minWidth, 1073741824);
                                    reorderedFlexItem.measure(measureSpec2, childHeightMeasureSpecInternal);
                                    n17 = reorderedFlexItem.getMeasuredWidth();
                                    measuredHeight = reorderedFlexItem.getMeasuredHeight();
                                    this.updateMeasureCache(n9, measureSpec2, childHeightMeasureSpecInternal, reorderedFlexItem);
                                    this.mFlexContainer.updateViewCache(n9, reorderedFlexItem);
                                    n6 = n21;
                                }
                                else {
                                    measuredHeight = n18;
                                }
                                final int max2 = Math.max(n7, measuredHeight + flexItem.getMarginTop() + flexItem.getMarginBottom() + this.mFlexContainer.getDecorationLengthCrossAxis(reorderedFlexItem));
                                flexLine.mMainSize += n17 + flexItem.getMarginLeft() + flexItem.getMarginRight();
                                b2 = max2;
                            }
                            flexLine.mCrossSize = Math.max(flexLine.mCrossSize, b2);
                            n7 = b2;
                        }
                    }
                    ++i;
                }
                if (n6 != 0 && mMainSize != flexLine.mMainSize) {
                    this.shrinkFlexItems(n, n2, flexLine, n3, n4, true);
                }
            }
        }
    }
    
    private int[] sortOrdersIntoReorderedIndices(int n, final List<Order> list, final SparseIntArray sparseIntArray) {
        Collections.sort((List<Comparable>)list);
        sparseIntArray.clear();
        final int[] array = new int[n];
        final Iterator<Comparable> iterator = (Iterator<Comparable>)list.iterator();
        n = 0;
        while (iterator.hasNext()) {
            final Order order = iterator.next();
            sparseIntArray.append(array[n] = order.index, order.order);
            ++n;
        }
        return array;
    }
    
    private void stretchViewHorizontally(final View view, int n, final int n2) {
        final FlexItem flexItem = (FlexItem)view.getLayoutParams();
        final int min = Math.min(Math.max(n - flexItem.getMarginLeft() - flexItem.getMarginRight() - this.mFlexContainer.getDecorationLengthCrossAxis(view), flexItem.getMinWidth()), flexItem.getMaxWidth());
        final long[] mMeasuredSizeCache = this.mMeasuredSizeCache;
        if (mMeasuredSizeCache != null) {
            n = this.extractHigherInt(mMeasuredSizeCache[n2]);
        }
        else {
            n = view.getMeasuredHeight();
        }
        n = View$MeasureSpec.makeMeasureSpec(n, 1073741824);
        final int measureSpec = View$MeasureSpec.makeMeasureSpec(min, 1073741824);
        view.measure(measureSpec, n);
        this.updateMeasureCache(n2, measureSpec, n, view);
        this.mFlexContainer.updateViewCache(n2, view);
    }
    
    private void stretchViewVertically(final View view, int n, final int n2) {
        final FlexItem flexItem = (FlexItem)view.getLayoutParams();
        final int min = Math.min(Math.max(n - flexItem.getMarginTop() - flexItem.getMarginBottom() - this.mFlexContainer.getDecorationLengthCrossAxis(view), flexItem.getMinHeight()), flexItem.getMaxHeight());
        final long[] mMeasuredSizeCache = this.mMeasuredSizeCache;
        if (mMeasuredSizeCache != null) {
            n = this.extractLowerInt(mMeasuredSizeCache[n2]);
        }
        else {
            n = view.getMeasuredWidth();
        }
        n = View$MeasureSpec.makeMeasureSpec(n, 1073741824);
        final int measureSpec = View$MeasureSpec.makeMeasureSpec(min, 1073741824);
        view.measure(n, measureSpec);
        this.updateMeasureCache(n2, n, measureSpec, view);
        this.mFlexContainer.updateViewCache(n2, view);
    }
    
    private void updateMeasureCache(final int n, final int n2, final int n3, final View view) {
        final long[] mMeasureSpecCache = this.mMeasureSpecCache;
        if (mMeasureSpecCache != null) {
            mMeasureSpecCache[n] = this.makeCombinedLong(n2, n3);
        }
        final long[] mMeasuredSizeCache = this.mMeasuredSizeCache;
        if (mMeasuredSizeCache != null) {
            mMeasuredSizeCache[n] = this.makeCombinedLong(view.getMeasuredWidth(), view.getMeasuredHeight());
        }
    }
    
    public void calculateFlexLines(final FlexLinesResult flexLinesResult, int mChildState, final int n, final int n2, int i, final int n3, List<FlexLine> mFlexLines) {
        int n4 = n;
        int n5 = n3;
        final boolean mainAxisDirectionHorizontal = this.mFlexContainer.isMainAxisDirectionHorizontal();
        final int mode = View$MeasureSpec.getMode(mChildState);
        final int size = View$MeasureSpec.getSize(mChildState);
        if (mFlexLines == null) {
            mFlexLines = new ArrayList<FlexLine>();
        }
        flexLinesResult.mFlexLines = mFlexLines;
        final boolean b = n5 == -1;
        final int paddingStartMain = this.getPaddingStartMain(mainAxisDirectionHorizontal);
        final int paddingEndMain = this.getPaddingEndMain(mainAxisDirectionHorizontal);
        final int paddingStartCross = this.getPaddingStartCross(mainAxisDirectionHorizontal);
        final int paddingEndCross = this.getPaddingEndCross(mainAxisDirectionHorizontal);
        final FlexLine flexLine = new FlexLine();
        flexLine.mFirstIndex = i;
        final int n6 = paddingEndMain + paddingStartMain;
        flexLine.mMainSize = n6;
        final int flexItemCount = this.mFlexContainer.getFlexItemCount();
        int n7 = Integer.MIN_VALUE;
        final int n8 = 0;
        int n9 = 0;
        int n10 = 0;
        int n11 = b ? 1 : 0;
        final ArrayList<FlexLine> list = mFlexLines;
        FlexLine flexLine2 = flexLine;
        int n12 = n8;
        while (true) {
            while (i < flexItemCount) {
                final View reorderedFlexItem = this.mFlexContainer.getReorderedFlexItemAt(i);
                int n23 = 0;
                int n24 = 0;
                int n25 = 0;
                int n26 = 0;
                int n27 = 0;
                Label_1524: {
                    Label_0273: {
                        if (reorderedFlexItem == null) {
                            if (!this.isLastFlexItem(i, flexItemCount, flexLine2)) {
                                break Label_0273;
                            }
                        }
                        else if (reorderedFlexItem.getVisibility() == 8) {
                            ++flexLine2.mGoneItemCount;
                            ++flexLine2.mItemCount;
                            if (!this.isLastFlexItem(i, flexItemCount, flexLine2)) {
                                break Label_0273;
                            }
                        }
                        else {
                            if (reorderedFlexItem instanceof CompoundButton) {
                                this.evaluateMinimumSizeForCompoundButton((CompoundButton)reorderedFlexItem);
                            }
                            final FlexItem flexItem = (FlexItem)reorderedFlexItem.getLayoutParams();
                            if (flexItem.getAlignSelf() == 4) {
                                flexLine2.mIndicesAlignSelfStretch.add(i);
                            }
                            int n14;
                            final int n13 = n14 = this.getFlexItemSizeMain(flexItem, mainAxisDirectionHorizontal);
                            if (flexItem.getFlexBasisPercent() != -1.0f) {
                                n14 = n13;
                                if (mode == 1073741824) {
                                    n14 = Math.round(size * flexItem.getFlexBasisPercent());
                                }
                            }
                            int n15;
                            if (mainAxisDirectionHorizontal) {
                                n15 = this.mFlexContainer.getChildWidthMeasureSpec(mChildState, n6 + this.getFlexItemMarginStartMain(flexItem, true) + this.getFlexItemMarginEndMain(flexItem, true), n14);
                                final int childHeightMeasureSpec = this.mFlexContainer.getChildHeightMeasureSpec(n4, paddingStartCross + paddingEndCross + this.getFlexItemMarginStartCross(flexItem, true) + this.getFlexItemMarginEndCross(flexItem, true) + n12, this.getFlexItemSizeCross(flexItem, true));
                                reorderedFlexItem.measure(n15, childHeightMeasureSpec);
                                this.updateMeasureCache(i, n15, childHeightMeasureSpec, reorderedFlexItem);
                            }
                            else {
                                final int childWidthMeasureSpec = this.mFlexContainer.getChildWidthMeasureSpec(n4, paddingStartCross + paddingEndCross + this.getFlexItemMarginStartCross(flexItem, false) + this.getFlexItemMarginEndCross(flexItem, false) + n12, this.getFlexItemSizeCross(flexItem, false));
                                n15 = this.mFlexContainer.getChildHeightMeasureSpec(mChildState, this.getFlexItemMarginStartMain(flexItem, false) + n6 + this.getFlexItemMarginEndMain(flexItem, false), n14);
                                reorderedFlexItem.measure(childWidthMeasureSpec, n15);
                                this.updateMeasureCache(i, childWidthMeasureSpec, n15, reorderedFlexItem);
                            }
                            this.mFlexContainer.updateViewCache(i, reorderedFlexItem);
                            this.checkSizeConstraints(reorderedFlexItem, i);
                            final int combineMeasuredStates = View.combineMeasuredStates(n9, reorderedFlexItem.getMeasuredState());
                            int n17;
                            int a;
                            int n18;
                            if (this.isWrapRequired(reorderedFlexItem, mode, size, flexLine2.mMainSize, this.getFlexItemMarginEndMain(flexItem, mainAxisDirectionHorizontal) + (this.getViewMeasuredSizeMain(reorderedFlexItem, mainAxisDirectionHorizontal) + this.getFlexItemMarginStartMain(flexItem, mainAxisDirectionHorizontal)), flexItem, i, n10, list.size())) {
                                if (flexLine2.getItemCountNotGone() > 0) {
                                    int n16;
                                    if (i > 0) {
                                        n16 = i - 1;
                                    }
                                    else {
                                        n16 = 0;
                                    }
                                    this.addFlexLine(list, flexLine2, n16, n12);
                                    n17 = flexLine2.mCrossSize + n12;
                                }
                                else {
                                    n17 = n12;
                                }
                                Label_0948: {
                                    if (mainAxisDirectionHorizontal) {
                                        if (flexItem.getHeight() != -1) {
                                            break Label_0948;
                                        }
                                        final FlexContainer mFlexContainer = this.mFlexContainer;
                                        reorderedFlexItem.measure(n15, mFlexContainer.getChildHeightMeasureSpec(n, mFlexContainer.getPaddingTop() + this.mFlexContainer.getPaddingBottom() + flexItem.getMarginTop() + flexItem.getMarginBottom() + n17, flexItem.getHeight()));
                                    }
                                    else {
                                        if (flexItem.getWidth() != -1) {
                                            break Label_0948;
                                        }
                                        final FlexContainer mFlexContainer2 = this.mFlexContainer;
                                        reorderedFlexItem.measure(mFlexContainer2.getChildWidthMeasureSpec(n, mFlexContainer2.getPaddingLeft() + this.mFlexContainer.getPaddingRight() + flexItem.getMarginLeft() + flexItem.getMarginRight() + n17, flexItem.getWidth()), n15);
                                    }
                                    this.checkSizeConstraints(reorderedFlexItem, i);
                                }
                                flexLine2 = new FlexLine();
                                flexLine2.mItemCount = 1;
                                flexLine2.mMainSize = n6;
                                flexLine2.mFirstIndex = i;
                                a = Integer.MIN_VALUE;
                                n18 = 0;
                            }
                            else {
                                ++flexLine2.mItemCount;
                                ++n10;
                                final int n19 = n12;
                                a = n7;
                                n18 = n10;
                                n17 = n19;
                            }
                            final int n20 = i;
                            final boolean mAnyItemsHaveFlexGrow = flexLine2.mAnyItemsHaveFlexGrow;
                            if (flexItem.getFlexGrow() != 0.0f) {
                                i = 1;
                            }
                            else {
                                i = 0;
                            }
                            flexLine2.mAnyItemsHaveFlexGrow = (((mAnyItemsHaveFlexGrow ? 1 : 0) | i) != 0x0);
                            final boolean mAnyItemsHaveFlexShrink = flexLine2.mAnyItemsHaveFlexShrink;
                            if (flexItem.getFlexShrink() != 0.0f) {
                                i = 1;
                            }
                            else {
                                i = 0;
                            }
                            flexLine2.mAnyItemsHaveFlexShrink = (((mAnyItemsHaveFlexShrink ? 1 : 0) | i) != 0x0);
                            final int[] mIndexToFlexLine = this.mIndexToFlexLine;
                            if (mIndexToFlexLine != null) {
                                mIndexToFlexLine[n20] = list.size();
                            }
                            flexLine2.mMainSize += this.getViewMeasuredSizeMain(reorderedFlexItem, mainAxisDirectionHorizontal) + this.getFlexItemMarginStartMain(flexItem, mainAxisDirectionHorizontal) + this.getFlexItemMarginEndMain(flexItem, mainAxisDirectionHorizontal);
                            flexLine2.mTotalFlexGrow += flexItem.getFlexGrow();
                            flexLine2.mTotalFlexShrink += flexItem.getFlexShrink();
                            this.mFlexContainer.onNewFlexItemAdded(reorderedFlexItem, n20, n18, flexLine2);
                            final int max = Math.max(a, this.getViewMeasuredSizeCross(reorderedFlexItem, mainAxisDirectionHorizontal) + this.getFlexItemMarginStartCross(flexItem, mainAxisDirectionHorizontal) + this.getFlexItemMarginEndCross(flexItem, mainAxisDirectionHorizontal) + this.mFlexContainer.getDecorationLengthCrossAxis(reorderedFlexItem));
                            flexLine2.mCrossSize = Math.max(flexLine2.mCrossSize, max);
                            if (mainAxisDirectionHorizontal) {
                                if (this.mFlexContainer.getFlexWrap() != 2) {
                                    i = Math.max(flexLine2.mMaxBaseline, reorderedFlexItem.getBaseline() + flexItem.getMarginTop());
                                }
                                else {
                                    i = Math.max(flexLine2.mMaxBaseline, reorderedFlexItem.getMeasuredHeight() - reorderedFlexItem.getBaseline() + flexItem.getMarginBottom());
                                }
                                flexLine2.mMaxBaseline = i;
                            }
                            int n21 = n17;
                            if (this.isLastFlexItem(n20, flexItemCount, flexLine2)) {
                                this.addFlexLine(list, flexLine2, n20, n17);
                                n21 = n17 + flexLine2.mCrossSize;
                            }
                            int n22;
                            if (n3 != -1 && list.size() > 0 && list.get(list.size() - 1).mLastIndex >= n3 && n20 >= n3 && n11 == 0) {
                                n22 = -flexLine2.getCrossSize();
                                i = 1;
                            }
                            else {
                                i = n11;
                                n22 = n21;
                            }
                            if (n22 > n2 && i != 0) {
                                mChildState = combineMeasuredStates;
                                flexLinesResult.mChildState = mChildState;
                                return;
                            }
                            n7 = max;
                            n23 = n18;
                            n9 = combineMeasuredStates;
                            n24 = i;
                            i = n3;
                            n25 = n20;
                            n26 = n;
                            n27 = n22;
                            break Label_1524;
                        }
                        this.addFlexLine(list, flexLine2, i, n12);
                    }
                    final int n28 = n4;
                    n27 = n12;
                    n26 = n28;
                    n25 = i;
                    i = n5;
                    n24 = n11;
                    n23 = n10;
                }
                final int n29 = n25 + 1;
                final int n30 = n26;
                n5 = i;
                n12 = n27;
                i = n29;
                n4 = n30;
                n11 = n24;
                n10 = n23;
            }
            mChildState = n9;
            continue;
        }
    }
    
    public void calculateHorizontalFlexLines(final FlexLinesResult flexLinesResult, final int n, final int n2) {
        this.calculateFlexLines(flexLinesResult, n, n2, Integer.MAX_VALUE, 0, -1, null);
    }
    
    public void calculateHorizontalFlexLines(final FlexLinesResult flexLinesResult, final int n, final int n2, final int n3, final int n4, final List<FlexLine> list) {
        this.calculateFlexLines(flexLinesResult, n, n2, n3, n4, -1, list);
    }
    
    public void calculateHorizontalFlexLinesToIndex(final FlexLinesResult flexLinesResult, final int n, final int n2, final int n3, final int n4, final List<FlexLine> list) {
        this.calculateFlexLines(flexLinesResult, n, n2, n3, 0, n4, list);
    }
    
    public void calculateVerticalFlexLines(final FlexLinesResult flexLinesResult, final int n, final int n2) {
        this.calculateFlexLines(flexLinesResult, n2, n, Integer.MAX_VALUE, 0, -1, null);
    }
    
    public void calculateVerticalFlexLines(final FlexLinesResult flexLinesResult, final int n, final int n2, final int n3, final int n4, final List<FlexLine> list) {
        this.calculateFlexLines(flexLinesResult, n2, n, n3, n4, -1, list);
    }
    
    public void calculateVerticalFlexLinesToIndex(final FlexLinesResult flexLinesResult, final int n, final int n2, final int n3, final int n4, final List<FlexLine> list) {
        this.calculateFlexLines(flexLinesResult, n2, n, n3, 0, n4, list);
    }
    
    public void clearFlexLines(final List<FlexLine> list, final int n) {
        int n2;
        if ((n2 = this.mIndexToFlexLine[n]) == -1) {
            n2 = 0;
        }
        if (list.size() > n2) {
            list.subList(n2, list.size()).clear();
        }
        final int[] mIndexToFlexLine = this.mIndexToFlexLine;
        final int toIndex = mIndexToFlexLine.length - 1;
        if (n > toIndex) {
            Arrays.fill(mIndexToFlexLine, -1);
        }
        else {
            Arrays.fill(mIndexToFlexLine, n, toIndex, -1);
        }
        final long[] mMeasureSpecCache = this.mMeasureSpecCache;
        final int toIndex2 = mMeasureSpecCache.length - 1;
        if (n > toIndex2) {
            Arrays.fill(mMeasureSpecCache, 0L);
        }
        else {
            Arrays.fill(mMeasureSpecCache, n, toIndex2, 0L);
        }
    }
    
    public int[] createReorderedIndices(final SparseIntArray sparseIntArray) {
        final int flexItemCount = this.mFlexContainer.getFlexItemCount();
        return this.sortOrdersIntoReorderedIndices(flexItemCount, this.createOrders(flexItemCount), sparseIntArray);
    }
    
    public int[] createReorderedIndices(final View view, int i, final ViewGroup$LayoutParams viewGroup$LayoutParams, final SparseIntArray sparseIntArray) {
        final int flexItemCount = this.mFlexContainer.getFlexItemCount();
        final List<Order> orders = this.createOrders(flexItemCount);
        final Order order = new Order(null);
        if (view != null && viewGroup$LayoutParams instanceof FlexItem) {
            order.order = ((FlexItem)viewGroup$LayoutParams).getOrder();
        }
        else {
            order.order = 1;
        }
        Label_0137: {
            if (i != -1) {
                if (i != flexItemCount) {
                    if (i < this.mFlexContainer.getFlexItemCount()) {
                        order.index = i;
                        while (i < flexItemCount) {
                            final Order order2 = orders.get(i);
                            ++order2.index;
                            ++i;
                        }
                        break Label_0137;
                    }
                }
            }
            order.index = flexItemCount;
        }
        orders.add(order);
        return this.sortOrdersIntoReorderedIndices(flexItemCount + 1, orders, sparseIntArray);
    }
    
    public void determineCrossSize(int n, int i, int n2) {
        final int flexDirection = this.mFlexContainer.getFlexDirection();
        if (flexDirection != 0 && flexDirection != 1) {
            if (flexDirection != 2 && flexDirection != 3) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid flex direction: ");
                sb.append(flexDirection);
                throw new IllegalArgumentException(sb.toString());
            }
            i = View$MeasureSpec.getMode(n);
            n = View$MeasureSpec.getSize(n);
        }
        else {
            n = View$MeasureSpec.getMode(i);
            final int size = View$MeasureSpec.getSize(i);
            i = n;
            n = size;
        }
        final List<FlexLine> flexLinesInternal = this.mFlexContainer.getFlexLinesInternal();
        if (i == 1073741824) {
            final int n3 = this.mFlexContainer.getSumOfCrossSize() + n2;
            final int size2 = flexLinesInternal.size();
            final int n4 = 0;
            i = 0;
            if (size2 == 1) {
                ((FlexLine)flexLinesInternal.get(0)).mCrossSize = n - n2;
            }
            else if (flexLinesInternal.size() >= 2) {
                n2 = this.mFlexContainer.getAlignContent();
                if (n2 != 1) {
                    Label_0762: {
                        if (n2 != 2) {
                            ArrayList flexLines;
                            if (n2 != 3) {
                                if (n2 != 4) {
                                    if (n2 != 5) {
                                        return;
                                    }
                                    if (n3 >= n) {
                                        return;
                                    }
                                    final float n5 = (n - n3) / (float)flexLinesInternal.size();
                                    final int size3 = flexLinesInternal.size();
                                    float n6 = 0.0f;
                                    while (i < size3) {
                                        final FlexLine flexLine = flexLinesInternal.get(i);
                                        float a;
                                        final float n7 = a = flexLine.mCrossSize + n5;
                                        float n8 = n6;
                                        if (i == flexLinesInternal.size() - 1) {
                                            a = n7 + n6;
                                            n8 = 0.0f;
                                        }
                                        n2 = Math.round(a);
                                        final float n9 = n8 + (a - n2);
                                        if (n9 > 1.0f) {
                                            n = n2 + 1;
                                            n6 = n9 - 1.0f;
                                        }
                                        else {
                                            n = n2;
                                            n6 = n9;
                                            if (n9 < -1.0f) {
                                                n = n2 - 1;
                                                n6 = n9 + 1.0f;
                                            }
                                        }
                                        flexLine.mCrossSize = n;
                                        ++i;
                                    }
                                    return;
                                }
                                else {
                                    if (n3 >= n) {
                                        break Label_0762;
                                    }
                                    n = (n - n3) / (flexLinesInternal.size() * 2);
                                    final ArrayList list = new ArrayList();
                                    final FlexLine flexLine2 = new FlexLine();
                                    flexLine2.mCrossSize = n;
                                    final Iterator iterator = flexLinesInternal.iterator();
                                    while (true) {
                                        flexLines = list;
                                        if (!iterator.hasNext()) {
                                            break;
                                        }
                                        final FlexLine flexLine3 = (FlexLine)iterator.next();
                                        list.add(flexLine2);
                                        list.add(flexLine3);
                                        list.add(flexLine2);
                                    }
                                }
                            }
                            else {
                                if (n3 >= n) {
                                    return;
                                }
                                final float a2 = (n - n3) / (float)(flexLinesInternal.size() - 1);
                                final ArrayList list2 = new ArrayList();
                                i = flexLinesInternal.size();
                                float n10 = 0.0f;
                                n = n4;
                                while (true) {
                                    flexLines = list2;
                                    if (n >= i) {
                                        break;
                                    }
                                    list2.add(flexLinesInternal.get(n));
                                    float n11 = n10;
                                    if (n != flexLinesInternal.size() - 1) {
                                        final FlexLine flexLine4 = new FlexLine();
                                        if (n == flexLinesInternal.size() - 2) {
                                            flexLine4.mCrossSize = Math.round(n10 + a2);
                                            n10 = 0.0f;
                                        }
                                        else {
                                            flexLine4.mCrossSize = Math.round(a2);
                                        }
                                        n2 = flexLine4.mCrossSize;
                                        final float n12 = n10 + (a2 - n2);
                                        float n13;
                                        if (n12 > 1.0f) {
                                            flexLine4.mCrossSize = n2 + 1;
                                            n13 = n12 - 1.0f;
                                        }
                                        else {
                                            n13 = n12;
                                            if (n12 < -1.0f) {
                                                flexLine4.mCrossSize = n2 - 1;
                                                n13 = n12 + 1.0f;
                                            }
                                        }
                                        list2.add(flexLine4);
                                        n11 = n13;
                                    }
                                    ++n;
                                    n10 = n11;
                                }
                            }
                            this.mFlexContainer.setFlexLines(flexLines);
                            return;
                        }
                    }
                    this.mFlexContainer.setFlexLines(this.constructFlexLinesForAlignContentCenter(flexLinesInternal, n, n3));
                }
                else {
                    final FlexLine flexLine5 = new FlexLine();
                    flexLine5.mCrossSize = n - n3;
                    flexLinesInternal.add(0, flexLine5);
                }
            }
        }
    }
    
    public void determineMainSize(final int n, final int n2) {
        this.determineMainSize(n, n2, 0);
    }
    
    public void determineMainSize(final int n, final int n2, int i) {
        this.ensureChildrenFrozen(this.mFlexContainer.getFlexItemCount());
        if (i >= this.mFlexContainer.getFlexItemCount()) {
            return;
        }
        final int flexDirection = this.mFlexContainer.getFlexDirection();
        final int flexDirection2 = this.mFlexContainer.getFlexDirection();
        int b;
        int n3;
        int n4;
        if (flexDirection2 != 0 && flexDirection2 != 1) {
            if (flexDirection2 != 2 && flexDirection2 != 3) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid flex direction: ");
                sb.append(flexDirection);
                throw new IllegalArgumentException(sb.toString());
            }
            final int mode = View$MeasureSpec.getMode(n2);
            b = View$MeasureSpec.getSize(n2);
            if (mode != 1073741824) {
                b = this.mFlexContainer.getLargestMainSize();
            }
            n3 = this.mFlexContainer.getPaddingTop();
            n4 = this.mFlexContainer.getPaddingBottom();
        }
        else {
            final int mode2 = View$MeasureSpec.getMode(n);
            b = View$MeasureSpec.getSize(n);
            final int largestMainSize = this.mFlexContainer.getLargestMainSize();
            if (mode2 != 1073741824) {
                b = Math.min(largestMainSize, b);
            }
            n3 = this.mFlexContainer.getPaddingLeft();
            n4 = this.mFlexContainer.getPaddingRight();
        }
        final int n5 = n3 + n4;
        final int[] mIndexToFlexLine = this.mIndexToFlexLine;
        if (mIndexToFlexLine != null) {
            i = mIndexToFlexLine[i];
        }
        else {
            i = 0;
        }
        for (List<FlexLine> flexLinesInternal = this.mFlexContainer.getFlexLinesInternal(); i < flexLinesInternal.size(); ++i) {
            final FlexLine flexLine = flexLinesInternal.get(i);
            final int mMainSize = flexLine.mMainSize;
            if (mMainSize < b && flexLine.mAnyItemsHaveFlexGrow) {
                this.expandFlexItems(n, n2, flexLine, b, n5, false);
            }
            else if (mMainSize > b && flexLine.mAnyItemsHaveFlexShrink) {
                this.shrinkFlexItems(n, n2, flexLine, b, n5, false);
            }
        }
    }
    
    public void ensureIndexToFlexLine(int max) {
        final int[] mIndexToFlexLine = this.mIndexToFlexLine;
        if (mIndexToFlexLine == null) {
            this.mIndexToFlexLine = new int[Math.max(max, 10)];
        }
        else if (mIndexToFlexLine.length < max) {
            max = Math.max(mIndexToFlexLine.length * 2, max);
            this.mIndexToFlexLine = Arrays.copyOf(this.mIndexToFlexLine, max);
        }
    }
    
    public void ensureMeasureSpecCache(int max) {
        final long[] mMeasureSpecCache = this.mMeasureSpecCache;
        if (mMeasureSpecCache == null) {
            this.mMeasureSpecCache = new long[Math.max(max, 10)];
        }
        else if (mMeasureSpecCache.length < max) {
            max = Math.max(mMeasureSpecCache.length * 2, max);
            this.mMeasureSpecCache = Arrays.copyOf(this.mMeasureSpecCache, max);
        }
    }
    
    public void ensureMeasuredSizeCache(int max) {
        final long[] mMeasuredSizeCache = this.mMeasuredSizeCache;
        if (mMeasuredSizeCache == null) {
            this.mMeasuredSizeCache = new long[Math.max(max, 10)];
        }
        else if (mMeasuredSizeCache.length < max) {
            max = Math.max(mMeasuredSizeCache.length * 2, max);
            this.mMeasuredSizeCache = Arrays.copyOf(this.mMeasuredSizeCache, max);
        }
    }
    
    public int extractHigherInt(final long n) {
        return (int)(n >> 32);
    }
    
    public int extractLowerInt(final long n) {
        return (int)n;
    }
    
    public boolean isOrderChangedFromLastMeasurement(final SparseIntArray sparseIntArray) {
        final int flexItemCount = this.mFlexContainer.getFlexItemCount();
        if (sparseIntArray.size() != flexItemCount) {
            return true;
        }
        for (int i = 0; i < flexItemCount; ++i) {
            final View flexItem = this.mFlexContainer.getFlexItemAt(i);
            if (flexItem != null) {
                if (((FlexItem)flexItem.getLayoutParams()).getOrder() != sparseIntArray.get(i)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public void layoutSingleChildHorizontal(final View view, final FlexLine flexLine, final int n, int n2, final int n3, int n4) {
        final FlexItem flexItem = (FlexItem)view.getLayoutParams();
        int n5 = this.mFlexContainer.getAlignItems();
        if (flexItem.getAlignSelf() != -1) {
            n5 = flexItem.getAlignSelf();
        }
        final int mCrossSize = flexLine.mCrossSize;
        Label_0402: {
            int n7 = 0;
            Label_0395: {
                int n6 = 0;
                Label_0364: {
                    Label_0355: {
                        if (n5 != 0) {
                            if (n5 != 1) {
                                if (n5 == 2) {
                                    n4 = (mCrossSize - view.getMeasuredHeight() + flexItem.getMarginTop() - flexItem.getMarginBottom()) / 2;
                                    if (this.mFlexContainer.getFlexWrap() != 2) {
                                        n2 += n4;
                                    }
                                    else {
                                        n2 -= n4;
                                    }
                                    view.layout(n, n2, n3, view.getMeasuredHeight() + n2);
                                    return;
                                }
                                if (n5 != 3) {
                                    if (n5 != 4) {
                                        return;
                                    }
                                }
                                else {
                                    final int flexWrap = this.mFlexContainer.getFlexWrap();
                                    final int mMaxBaseline = flexLine.mMaxBaseline;
                                    if (flexWrap != 2) {
                                        n6 = Math.max(mMaxBaseline - view.getBaseline(), flexItem.getMarginTop());
                                        n2 += n6;
                                        break Label_0364;
                                    }
                                    n7 = Math.max(mMaxBaseline - view.getMeasuredHeight() + view.getBaseline(), flexItem.getMarginBottom());
                                    n2 -= n7;
                                    break Label_0395;
                                }
                            }
                            else {
                                if (this.mFlexContainer.getFlexWrap() != 2) {
                                    n2 += mCrossSize;
                                    view.layout(n, n2 - view.getMeasuredHeight() - flexItem.getMarginBottom(), n3, n2 - flexItem.getMarginBottom());
                                    return;
                                }
                                n2 = n2 - mCrossSize + view.getMeasuredHeight() + flexItem.getMarginTop();
                                n4 = n4 - mCrossSize + view.getMeasuredHeight();
                                break Label_0355;
                            }
                        }
                        if (this.mFlexContainer.getFlexWrap() == 2) {
                            n2 -= flexItem.getMarginBottom();
                            n7 = flexItem.getMarginBottom();
                            break Label_0395;
                        }
                        n2 += flexItem.getMarginTop();
                    }
                    n6 = flexItem.getMarginTop();
                }
                n4 += n6;
                break Label_0402;
            }
            n4 -= n7;
        }
        view.layout(n, n2, n3, n4);
    }
    
    public void layoutSingleChildVertical(final View view, final FlexLine flexLine, final boolean b, int n, final int n2, int n3, final int n4) {
        final FlexItem flexItem = (FlexItem)view.getLayoutParams();
        int n5 = this.mFlexContainer.getAlignItems();
        if (flexItem.getAlignSelf() != -1) {
            n5 = flexItem.getAlignSelf();
        }
        final int mCrossSize = flexLine.mCrossSize;
        Label_0268: {
            int n6 = 0;
            Label_0261: {
                Label_0252: {
                    Label_0230: {
                        Label_0221: {
                            if (n5 != 0) {
                                if (n5 != 1) {
                                    if (n5 != 2) {
                                        if (n5 != 3 && n5 != 4) {
                                            return;
                                        }
                                    }
                                    else {
                                        final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup$MarginLayoutParams)view.getLayoutParams();
                                        n6 = (mCrossSize - view.getMeasuredWidth() + fn0.b(viewGroup$MarginLayoutParams) - fn0.a(viewGroup$MarginLayoutParams)) / 2;
                                        if (!b) {
                                            n += n6;
                                            break Label_0230;
                                        }
                                        n -= n6;
                                        break Label_0261;
                                    }
                                }
                                else {
                                    if (!b) {
                                        n = n + mCrossSize - view.getMeasuredWidth() - flexItem.getMarginRight();
                                        n3 = n3 + mCrossSize - view.getMeasuredWidth();
                                        break Label_0252;
                                    }
                                    n = n - mCrossSize + view.getMeasuredWidth() + flexItem.getMarginLeft();
                                    n3 = n3 - mCrossSize + view.getMeasuredWidth();
                                    break Label_0221;
                                }
                            }
                            if (b) {
                                n -= flexItem.getMarginRight();
                                break Label_0252;
                            }
                            n += flexItem.getMarginLeft();
                        }
                        n6 = flexItem.getMarginLeft();
                    }
                    n3 += n6;
                    break Label_0268;
                }
                n6 = flexItem.getMarginRight();
            }
            n3 -= n6;
        }
        view.layout(n, n2, n3, n4);
    }
    
    public long makeCombinedLong(final int n, final int n2) {
        return ((long)n & 0xFFFFFFFFL) | (long)n2 << 32;
    }
    
    public void stretchViews() {
        this.stretchViews(0);
    }
    
    public void stretchViews(int i) {
        if (i >= this.mFlexContainer.getFlexItemCount()) {
            return;
        }
        final int flexDirection = this.mFlexContainer.getFlexDirection();
        if (this.mFlexContainer.getAlignItems() == 4) {
            final int[] mIndexToFlexLine = this.mIndexToFlexLine;
            if (mIndexToFlexLine != null) {
                i = mIndexToFlexLine[i];
            }
            else {
                i = 0;
            }
            for (List<FlexLine> flexLinesInternal = this.mFlexContainer.getFlexLinesInternal(); i < flexLinesInternal.size(); ++i) {
                final FlexLine flexLine = flexLinesInternal.get(i);
                for (int mItemCount = flexLine.mItemCount, j = 0; j < mItemCount; ++j) {
                    final int n = flexLine.mFirstIndex + j;
                    if (j < this.mFlexContainer.getFlexItemCount()) {
                        final View reorderedFlexItem = this.mFlexContainer.getReorderedFlexItemAt(n);
                        if (reorderedFlexItem != null) {
                            if (reorderedFlexItem.getVisibility() != 8) {
                                final FlexItem flexItem = (FlexItem)reorderedFlexItem.getLayoutParams();
                                if (flexItem.getAlignSelf() == -1 || flexItem.getAlignSelf() == 4) {
                                    if (flexDirection != 0 && flexDirection != 1) {
                                        if (flexDirection != 2 && flexDirection != 3) {
                                            final StringBuilder sb = new StringBuilder();
                                            sb.append("Invalid flex direction: ");
                                            sb.append(flexDirection);
                                            throw new IllegalArgumentException(sb.toString());
                                        }
                                        this.stretchViewHorizontally(reorderedFlexItem, flexLine.mCrossSize, n);
                                    }
                                    else {
                                        this.stretchViewVertically(reorderedFlexItem, flexLine.mCrossSize, n);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else {
            for (final FlexLine flexLine2 : this.mFlexContainer.getFlexLinesInternal()) {
                for (final Integer n2 : flexLine2.mIndicesAlignSelfStretch) {
                    final View reorderedFlexItem2 = this.mFlexContainer.getReorderedFlexItemAt(n2);
                    if (flexDirection != 0 && flexDirection != 1) {
                        if (flexDirection != 2 && flexDirection != 3) {
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Invalid flex direction: ");
                            sb2.append(flexDirection);
                            throw new IllegalArgumentException(sb2.toString());
                        }
                        this.stretchViewHorizontally(reorderedFlexItem2, flexLine2.mCrossSize, n2);
                    }
                    else {
                        this.stretchViewVertically(reorderedFlexItem2, flexLine2.mCrossSize, n2);
                    }
                }
            }
        }
    }
    
    public static class FlexLinesResult
    {
        int mChildState;
        List<FlexLine> mFlexLines;
        
        public void reset() {
            this.mFlexLines = null;
            this.mChildState = 0;
        }
    }
    
    public static class Order implements Comparable<Order>
    {
        int index;
        int order;
        
        private Order() {
        }
        
        @Override
        public int compareTo(final Order order) {
            final int order2 = this.order;
            final int order3 = order.order;
            if (order2 != order3) {
                return order2 - order3;
            }
            return this.index - order.index;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Order{order=");
            sb.append(this.order);
            sb.append(", index=");
            sb.append(this.index);
            sb.append('}');
            return sb.toString();
        }
    }
}

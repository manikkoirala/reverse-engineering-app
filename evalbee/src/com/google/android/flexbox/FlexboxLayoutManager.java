// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.flexbox;

import android.view.ViewGroup$MarginLayoutParams;
import android.view.ViewGroup$LayoutParams;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import androidx.recyclerview.widget.g;
import android.os.Parcelable;
import android.graphics.PointF;
import android.view.View$MeasureSpec;
import androidx.recyclerview.widget.RecyclerView.o;
import android.util.AttributeSet;
import java.util.ArrayList;
import android.util.SparseArray;
import android.view.View;
import androidx.recyclerview.widget.i;
import java.util.List;
import android.content.Context;
import android.graphics.Rect;
import androidx.recyclerview.widget.RecyclerView;

public class FlexboxLayoutManager extends o implements FlexContainer, z.b
{
    static final boolean $assertionsDisabled = false;
    private static final boolean DEBUG = false;
    private static final String TAG = "FlexboxLayoutManager";
    private static final Rect TEMP_RECT;
    private int mAlignItems;
    private AnchorInfo mAnchorInfo;
    private final Context mContext;
    private int mDirtyPosition;
    private int mFlexDirection;
    private List<FlexLine> mFlexLines;
    private FlexboxHelper.FlexLinesResult mFlexLinesResult;
    private int mFlexWrap;
    private final FlexboxHelper mFlexboxHelper;
    private boolean mFromBottomToTop;
    private boolean mIsRtl;
    private int mJustifyContent;
    private int mLastHeight;
    private int mLastWidth;
    private LayoutState mLayoutState;
    private int mMaxLine;
    private androidx.recyclerview.widget.i mOrientationHelper;
    private View mParent;
    private SavedState mPendingSavedState;
    private int mPendingScrollPosition;
    private int mPendingScrollPositionOffset;
    private boolean mRecycleChildrenOnDetach;
    private v mRecycler;
    private a0 mState;
    private androidx.recyclerview.widget.i mSubOrientationHelper;
    private SparseArray<View> mViewCache;
    
    static {
        TEMP_RECT = new Rect();
    }
    
    public FlexboxLayoutManager(final Context context) {
        this(context, 0, 1);
    }
    
    public FlexboxLayoutManager(final Context context, final int n) {
        this(context, n, 1);
    }
    
    public FlexboxLayoutManager(final Context mContext, final int flexDirection, final int flexWrap) {
        this.mMaxLine = -1;
        this.mFlexLines = new ArrayList<FlexLine>();
        this.mFlexboxHelper = new FlexboxHelper(this);
        this.mAnchorInfo = new AnchorInfo(null);
        this.mPendingScrollPosition = -1;
        this.mPendingScrollPositionOffset = Integer.MIN_VALUE;
        this.mLastWidth = Integer.MIN_VALUE;
        this.mLastHeight = Integer.MIN_VALUE;
        this.mViewCache = (SparseArray<View>)new SparseArray();
        this.mDirtyPosition = -1;
        this.mFlexLinesResult = new FlexboxHelper.FlexLinesResult();
        this.setFlexDirection(flexDirection);
        this.setFlexWrap(flexWrap);
        this.setAlignItems(4);
        this.mContext = mContext;
    }
    
    public FlexboxLayoutManager(final Context mContext, final AttributeSet set, int a, final int n) {
        this.mMaxLine = -1;
        this.mFlexLines = new ArrayList<FlexLine>();
        this.mFlexboxHelper = new FlexboxHelper(this);
        this.mAnchorInfo = new AnchorInfo(null);
        this.mPendingScrollPosition = -1;
        this.mPendingScrollPositionOffset = Integer.MIN_VALUE;
        this.mLastWidth = Integer.MIN_VALUE;
        this.mLastHeight = Integer.MIN_VALUE;
        this.mViewCache = (SparseArray<View>)new SparseArray();
        this.mDirtyPosition = -1;
        this.mFlexLinesResult = new FlexboxHelper.FlexLinesResult();
        final d properties = RecyclerView.o.getProperties(mContext, set, a, n);
        a = properties.a;
        Label_0160: {
            if (a != 0) {
                if (a != 1) {
                    break Label_0160;
                }
                if (properties.c) {
                    a = 3;
                }
                else {
                    a = 2;
                }
            }
            else {
                if (properties.c) {
                    this.setFlexDirection(1);
                    break Label_0160;
                }
                a = 0;
            }
            this.setFlexDirection(a);
        }
        this.setFlexWrap(1);
        this.setAlignItems(4);
        this.mContext = mContext;
    }
    
    public static /* synthetic */ int access$2800(final FlexboxLayoutManager flexboxLayoutManager) {
        return flexboxLayoutManager.mFlexWrap;
    }
    
    public static /* synthetic */ int access$2900(final FlexboxLayoutManager flexboxLayoutManager) {
        return flexboxLayoutManager.mFlexDirection;
    }
    
    public static /* synthetic */ boolean access$3000(final FlexboxLayoutManager flexboxLayoutManager) {
        return flexboxLayoutManager.mIsRtl;
    }
    
    public static /* synthetic */ androidx.recyclerview.widget.i access$3100(final FlexboxLayoutManager flexboxLayoutManager) {
        return flexboxLayoutManager.mOrientationHelper;
    }
    
    public static /* synthetic */ androidx.recyclerview.widget.i access$3200(final FlexboxLayoutManager flexboxLayoutManager) {
        return flexboxLayoutManager.mSubOrientationHelper;
    }
    
    public static /* synthetic */ FlexboxHelper access$3300(final FlexboxLayoutManager flexboxLayoutManager) {
        return flexboxLayoutManager.mFlexboxHelper;
    }
    
    public static /* synthetic */ List access$3400(final FlexboxLayoutManager flexboxLayoutManager) {
        return flexboxLayoutManager.mFlexLines;
    }
    
    private boolean canViewBeRecycledFromEnd(final View view, final int n) {
        final boolean mainAxisDirectionHorizontal = this.isMainAxisDirectionHorizontal();
        boolean b = true;
        final boolean b2 = true;
        if (!mainAxisDirectionHorizontal && this.mIsRtl) {
            return this.mOrientationHelper.d(view) <= n && b2;
        }
        if (this.mOrientationHelper.g(view) < this.mOrientationHelper.h() - n) {
            b = false;
        }
        return b;
    }
    
    private boolean canViewBeRecycledFromStart(final View view, final int n) {
        final boolean mainAxisDirectionHorizontal = this.isMainAxisDirectionHorizontal();
        boolean b = true;
        final boolean b2 = true;
        if (!mainAxisDirectionHorizontal && this.mIsRtl) {
            return this.mOrientationHelper.h() - this.mOrientationHelper.g(view) <= n && b2;
        }
        if (this.mOrientationHelper.d(view) > n) {
            b = false;
        }
        return b;
    }
    
    private void clearFlexLines() {
        this.mFlexLines.clear();
        this.mAnchorInfo.reset();
        AnchorInfo.access$2402(this.mAnchorInfo, 0);
    }
    
    private int computeScrollExtent(final a0 a0) {
        if (((RecyclerView.o)this).getChildCount() == 0) {
            return 0;
        }
        final int b = a0.b();
        this.ensureOrientationHelper();
        final View firstReferenceChild = this.findFirstReferenceChild(b);
        final View lastReferenceChild = this.findLastReferenceChild(b);
        if (a0.b() != 0 && firstReferenceChild != null && lastReferenceChild != null) {
            return Math.min(this.mOrientationHelper.n(), this.mOrientationHelper.d(lastReferenceChild) - this.mOrientationHelper.g(firstReferenceChild));
        }
        return 0;
    }
    
    private int computeScrollOffset(final a0 a0) {
        if (((RecyclerView.o)this).getChildCount() == 0) {
            return 0;
        }
        final int b = a0.b();
        final View firstReferenceChild = this.findFirstReferenceChild(b);
        final View lastReferenceChild = this.findLastReferenceChild(b);
        if (a0.b() != 0 && firstReferenceChild != null) {
            if (lastReferenceChild != null) {
                final int position = ((RecyclerView.o)this).getPosition(firstReferenceChild);
                final int position2 = ((RecyclerView.o)this).getPosition(lastReferenceChild);
                final int abs = Math.abs(this.mOrientationHelper.d(lastReferenceChild) - this.mOrientationHelper.g(firstReferenceChild));
                final int[] mIndexToFlexLine = this.mFlexboxHelper.mIndexToFlexLine;
                final int n = mIndexToFlexLine[position];
                if (n != 0) {
                    if (n != -1) {
                        return Math.round(n * (abs / (float)(mIndexToFlexLine[position2] - n + 1)) + (this.mOrientationHelper.m() - this.mOrientationHelper.g(firstReferenceChild)));
                    }
                }
            }
        }
        return 0;
    }
    
    private int computeScrollRange(final a0 a0) {
        if (((RecyclerView.o)this).getChildCount() == 0) {
            return 0;
        }
        final int b = a0.b();
        final View firstReferenceChild = this.findFirstReferenceChild(b);
        final View lastReferenceChild = this.findLastReferenceChild(b);
        if (a0.b() != 0 && firstReferenceChild != null && lastReferenceChild != null) {
            return (int)(Math.abs(this.mOrientationHelper.d(lastReferenceChild) - this.mOrientationHelper.g(firstReferenceChild)) / (float)(this.findLastVisibleItemPosition() - this.findFirstVisibleItemPosition() + 1) * a0.b());
        }
        return 0;
    }
    
    private void ensureLayoutState() {
        if (this.mLayoutState == null) {
            this.mLayoutState = new LayoutState(null);
        }
    }
    
    private void ensureOrientationHelper() {
        if (this.mOrientationHelper != null) {
            return;
        }
        androidx.recyclerview.widget.i mSubOrientationHelper = null;
        Label_0061: {
            Label_0048: {
                if (this.isMainAxisDirectionHorizontal()) {
                    if (this.mFlexWrap == 0) {
                        break Label_0048;
                    }
                }
                else if (this.mFlexWrap != 0) {
                    break Label_0048;
                }
                this.mOrientationHelper = androidx.recyclerview.widget.i.c(this);
                mSubOrientationHelper = androidx.recyclerview.widget.i.a(this);
                break Label_0061;
            }
            this.mOrientationHelper = androidx.recyclerview.widget.i.a(this);
            mSubOrientationHelper = androidx.recyclerview.widget.i.c(this);
        }
        this.mSubOrientationHelper = mSubOrientationHelper;
    }
    
    private int fill(final v v, final a0 a0, final LayoutState layoutState) {
        if (LayoutState.access$2000(layoutState) != Integer.MIN_VALUE) {
            if (LayoutState.access$1200(layoutState) < 0) {
                LayoutState.access$2012(layoutState, LayoutState.access$1200(layoutState));
            }
            this.recycleByLayoutState(v, layoutState);
        }
        final int access$1200 = LayoutState.access$1200(layoutState);
        int access$1201 = LayoutState.access$1200(layoutState);
        final boolean mainAxisDirectionHorizontal = this.isMainAxisDirectionHorizontal();
        int n = 0;
        while ((access$1201 > 0 || LayoutState.access$1100(this.mLayoutState)) && layoutState.hasMore(a0, this.mFlexLines)) {
            final FlexLine flexLine = this.mFlexLines.get(LayoutState.access$1500(layoutState));
            LayoutState.access$2202(layoutState, flexLine.mFirstIndex);
            n += this.layoutFlexLine(flexLine, layoutState);
            if (!mainAxisDirectionHorizontal && this.mIsRtl) {
                LayoutState.access$1020(layoutState, flexLine.getCrossSize() * LayoutState.access$2300(layoutState));
            }
            else {
                LayoutState.access$1012(layoutState, flexLine.getCrossSize() * LayoutState.access$2300(layoutState));
            }
            access$1201 -= flexLine.getCrossSize();
        }
        LayoutState.access$1220(layoutState, n);
        if (LayoutState.access$2000(layoutState) != Integer.MIN_VALUE) {
            LayoutState.access$2012(layoutState, n);
            if (LayoutState.access$1200(layoutState) < 0) {
                LayoutState.access$2012(layoutState, LayoutState.access$1200(layoutState));
            }
            this.recycleByLayoutState(v, layoutState);
        }
        return access$1200 - LayoutState.access$1200(layoutState);
    }
    
    private View findFirstReferenceChild(int position) {
        final View referenceChild = this.findReferenceChild(0, ((RecyclerView.o)this).getChildCount(), position);
        if (referenceChild == null) {
            return null;
        }
        position = ((RecyclerView.o)this).getPosition(referenceChild);
        position = this.mFlexboxHelper.mIndexToFlexLine[position];
        if (position == -1) {
            return null;
        }
        return this.findFirstReferenceViewInLine(referenceChild, this.mFlexLines.get(position));
    }
    
    private View findFirstReferenceViewInLine(View view, final FlexLine flexLine) {
        final boolean mainAxisDirectionHorizontal = this.isMainAxisDirectionHorizontal();
        final int mItemCount = flexLine.mItemCount;
        int i = 1;
        View view2 = view;
        while (i < mItemCount) {
            final View child = ((RecyclerView.o)this).getChildAt(i);
            view = view2;
            Label_0113: {
                if (child != null) {
                    if (child.getVisibility() == 8) {
                        view = view2;
                    }
                    else {
                        if (this.mIsRtl && !mainAxisDirectionHorizontal) {
                            view = view2;
                            if (this.mOrientationHelper.d(view2) >= this.mOrientationHelper.d(child)) {
                                break Label_0113;
                            }
                        }
                        else {
                            view = view2;
                            if (this.mOrientationHelper.g(view2) <= this.mOrientationHelper.g(child)) {
                                break Label_0113;
                            }
                        }
                        view = child;
                    }
                }
            }
            ++i;
            view2 = view;
        }
        return view2;
    }
    
    private View findLastReferenceChild(int position) {
        final View referenceChild = this.findReferenceChild(((RecyclerView.o)this).getChildCount() - 1, -1, position);
        if (referenceChild == null) {
            return null;
        }
        position = ((RecyclerView.o)this).getPosition(referenceChild);
        position = this.mFlexboxHelper.mIndexToFlexLine[position];
        return this.findLastReferenceViewInLine(referenceChild, this.mFlexLines.get(position));
    }
    
    private View findLastReferenceViewInLine(View view, final FlexLine flexLine) {
        final boolean mainAxisDirectionHorizontal = this.isMainAxisDirectionHorizontal();
        View view2;
        for (int i = ((RecyclerView.o)this).getChildCount() - 2; i > ((RecyclerView.o)this).getChildCount() - flexLine.mItemCount - 1; --i, view = view2) {
            final View child = ((RecyclerView.o)this).getChildAt(i);
            view2 = view;
            if (child != null) {
                if (child.getVisibility() == 8) {
                    view2 = view;
                }
                else {
                    if (this.mIsRtl && !mainAxisDirectionHorizontal) {
                        view2 = view;
                        if (this.mOrientationHelper.g(view) <= this.mOrientationHelper.g(child)) {
                            continue;
                        }
                    }
                    else {
                        view2 = view;
                        if (this.mOrientationHelper.d(view) >= this.mOrientationHelper.d(child)) {
                            continue;
                        }
                    }
                    view2 = child;
                }
            }
        }
        return view;
    }
    
    private View findOneVisibleChild(int i, final int n, final boolean b) {
        int n2;
        if (n > i) {
            n2 = 1;
        }
        else {
            n2 = -1;
        }
        while (i != n) {
            final View child = ((RecyclerView.o)this).getChildAt(i);
            if (this.isViewVisible(child, b)) {
                return child;
            }
            i += n2;
        }
        return null;
    }
    
    private View findReferenceChild(int i, final int n, final int n2) {
        this.ensureOrientationHelper();
        this.ensureLayoutState();
        final int m = this.mOrientationHelper.m();
        final int j = this.mOrientationHelper.i();
        int n3;
        if (n > i) {
            n3 = 1;
        }
        else {
            n3 = -1;
        }
        View view = null;
        View view2 = null;
        while (i != n) {
            final View child = ((RecyclerView.o)this).getChildAt(i);
            View view3;
            View view4;
            if (child == null) {
                view3 = view;
                view4 = view2;
            }
            else {
                final int position = ((RecyclerView.o)this).getPosition(child);
                view3 = view;
                view4 = view2;
                if (position >= 0) {
                    view3 = view;
                    view4 = view2;
                    if (position < n2) {
                        if (((p)child.getLayoutParams()).isItemRemoved()) {
                            view3 = view;
                            if ((view4 = view2) == null) {
                                view4 = child;
                                view3 = view;
                            }
                        }
                        else {
                            if (this.mOrientationHelper.g(child) >= m && this.mOrientationHelper.d(child) <= j) {
                                return child;
                            }
                            view3 = view;
                            view4 = view2;
                            if (view == null) {
                                view3 = child;
                                view4 = view2;
                            }
                        }
                    }
                }
            }
            i += n3;
            view = view3;
            view2 = view4;
        }
        if (view != null) {
            view2 = view;
        }
        return view2;
    }
    
    private int fixLayoutEndGap(int n, final v v, final a0 a0, final boolean b) {
        int handleScrollingMainOrientation;
        if (!this.isMainAxisDirectionHorizontal() && this.mIsRtl) {
            final int n2 = n - this.mOrientationHelper.m();
            if (n2 <= 0) {
                return 0;
            }
            handleScrollingMainOrientation = this.handleScrollingMainOrientation(n2, v, a0);
        }
        else {
            final int n3 = this.mOrientationHelper.i() - n;
            if (n3 <= 0) {
                return 0;
            }
            handleScrollingMainOrientation = -this.handleScrollingMainOrientation(-n3, v, a0);
        }
        if (b) {
            n = this.mOrientationHelper.i() - (n + handleScrollingMainOrientation);
            if (n > 0) {
                this.mOrientationHelper.r(n);
                return n + handleScrollingMainOrientation;
            }
        }
        return handleScrollingMainOrientation;
    }
    
    private int fixLayoutStartGap(int n, final v v, final a0 a0, final boolean b) {
        int handleScrollingMainOrientation;
        if (!this.isMainAxisDirectionHorizontal() && this.mIsRtl) {
            final int n2 = this.mOrientationHelper.i() - n;
            if (n2 <= 0) {
                return 0;
            }
            handleScrollingMainOrientation = this.handleScrollingMainOrientation(-n2, v, a0);
        }
        else {
            final int n3 = n - this.mOrientationHelper.m();
            if (n3 <= 0) {
                return 0;
            }
            handleScrollingMainOrientation = -this.handleScrollingMainOrientation(n3, v, a0);
        }
        int n4 = handleScrollingMainOrientation;
        if (b) {
            n = n + handleScrollingMainOrientation - this.mOrientationHelper.m();
            n4 = handleScrollingMainOrientation;
            if (n > 0) {
                this.mOrientationHelper.r(-n);
                n4 = handleScrollingMainOrientation - n;
            }
        }
        return n4;
    }
    
    private int getChildBottom(final View view) {
        return ((RecyclerView.o)this).getDecoratedBottom(view) + ((p)view.getLayoutParams()).bottomMargin;
    }
    
    private View getChildClosestToStart() {
        return ((RecyclerView.o)this).getChildAt(0);
    }
    
    private int getChildLeft(final View view) {
        return ((RecyclerView.o)this).getDecoratedLeft(view) - ((p)view.getLayoutParams()).leftMargin;
    }
    
    private int getChildRight(final View view) {
        return ((RecyclerView.o)this).getDecoratedRight(view) + ((p)view.getLayoutParams()).rightMargin;
    }
    
    private int getChildTop(final View view) {
        return ((RecyclerView.o)this).getDecoratedTop(view) - ((p)view.getLayoutParams()).topMargin;
    }
    
    private int handleScrollingMainOrientation(int a, final v v, final a0 a2) {
        if (((RecyclerView.o)this).getChildCount() == 0 || a == 0) {
            return 0;
        }
        this.ensureOrientationHelper();
        final LayoutState mLayoutState = this.mLayoutState;
        int n = 1;
        LayoutState.access$502(mLayoutState, true);
        final boolean b = !this.isMainAxisDirectionHorizontal() && this.mIsRtl;
        Label_0079: {
            if (b) {
                if (a < 0) {
                    break Label_0079;
                }
            }
            else if (a > 0) {
                break Label_0079;
            }
            n = -1;
        }
        final int abs = Math.abs(a);
        this.updateLayoutState(n, abs);
        final int n2 = LayoutState.access$2000(this.mLayoutState) + this.fill(v, a2, this.mLayoutState);
        if (n2 < 0) {
            return 0;
        }
        if (b) {
            if (abs > n2) {
                a = -n * n2;
            }
        }
        else if (abs > n2) {
            a = n * n2;
        }
        this.mOrientationHelper.r(-a);
        LayoutState.access$2702(this.mLayoutState, a);
        return a;
    }
    
    private int handleScrollingSubOrientation(int n) {
        final int childCount = ((RecyclerView.o)this).getChildCount();
        boolean b = false;
        if (childCount != 0 && n != 0) {
            this.ensureOrientationHelper();
            final boolean mainAxisDirectionHorizontal = this.isMainAxisDirectionHorizontal();
            final View mParent = this.mParent;
            int n2;
            if (mainAxisDirectionHorizontal) {
                n2 = mParent.getWidth();
            }
            else {
                n2 = mParent.getHeight();
            }
            int n3;
            if (mainAxisDirectionHorizontal) {
                n3 = ((RecyclerView.o)this).getWidth();
            }
            else {
                n3 = ((RecyclerView.o)this).getHeight();
            }
            if (((RecyclerView.o)this).getLayoutDirection() == 1) {
                b = true;
            }
            if (b) {
                final int abs = Math.abs(n);
                if (n < 0) {
                    n = Math.min(n3 + AnchorInfo.access$2400(this.mAnchorInfo) - n2, abs);
                    return -n;
                }
                final int min = n;
                if (AnchorInfo.access$2400(this.mAnchorInfo) + n <= 0) {
                    return min;
                }
            }
            else {
                if (n > 0) {
                    return Math.min(n3 - AnchorInfo.access$2400(this.mAnchorInfo) - n2, n);
                }
                if (AnchorInfo.access$2400(this.mAnchorInfo) + n >= 0) {
                    return n;
                }
            }
            n = AnchorInfo.access$2400(this.mAnchorInfo);
            return -n;
        }
        return 0;
    }
    
    private static boolean isMeasurementUpToDate(final int n, int size, final int n2) {
        final int mode = View$MeasureSpec.getMode(size);
        size = View$MeasureSpec.getSize(size);
        boolean b = false;
        final boolean b2 = false;
        if (n2 > 0 && n != n2) {
            return false;
        }
        if (mode == Integer.MIN_VALUE) {
            if (size >= n) {
                b = true;
            }
            return b;
        }
        if (mode == 0) {
            return true;
        }
        if (mode != 1073741824) {
            return false;
        }
        boolean b3 = b2;
        if (size == n) {
            b3 = true;
        }
        return b3;
    }
    
    private boolean isViewVisible(final View view, final boolean b) {
        final int paddingLeft = ((RecyclerView.o)this).getPaddingLeft();
        final int paddingTop = ((RecyclerView.o)this).getPaddingTop();
        final int n = ((RecyclerView.o)this).getWidth() - ((RecyclerView.o)this).getPaddingRight();
        final int n2 = ((RecyclerView.o)this).getHeight() - ((RecyclerView.o)this).getPaddingBottom();
        final int childLeft = this.getChildLeft(view);
        final int childTop = this.getChildTop(view);
        final int childRight = this.getChildRight(view);
        final int childBottom = this.getChildBottom(view);
        final boolean b2 = true;
        final boolean b3 = true;
        final boolean b4 = paddingLeft <= childLeft && n >= childRight;
        final boolean b5 = childLeft >= n || childRight >= paddingLeft;
        final boolean b6 = paddingTop <= childTop && n2 >= childBottom;
        final boolean b7 = childTop >= n2 || childBottom >= paddingTop;
        if (b) {
            return b4 && b6 && b3;
        }
        return b5 && b7 && b2;
    }
    
    private int layoutFlexLine(final FlexLine flexLine, final LayoutState layoutState) {
        if (this.isMainAxisDirectionHorizontal()) {
            return this.layoutFlexLineMainAxisHorizontal(flexLine, layoutState);
        }
        return this.layoutFlexLineMainAxisVertical(flexLine, layoutState);
    }
    
    private int layoutFlexLineMainAxisHorizontal(final FlexLine flexLine, final LayoutState layoutState) {
        final int paddingLeft = ((RecyclerView.o)this).getPaddingLeft();
        final int paddingRight = ((RecyclerView.o)this).getPaddingRight();
        final int width = ((RecyclerView.o)this).getWidth();
        int access$1000 = LayoutState.access$1000(layoutState);
        if (LayoutState.access$2300(layoutState) == -1) {
            access$1000 -= flexLine.mCrossSize;
        }
        final int access$1001 = LayoutState.access$2200(layoutState);
        final int mJustifyContent = this.mJustifyContent;
        float n = 0.0f;
        float n2 = 0.0f;
        float a = 0.0f;
        Label_0387: {
            if (mJustifyContent != 0) {
                if (mJustifyContent == 1) {
                    final int mMainSize = flexLine.mMainSize;
                    n = (float)(width - mMainSize + paddingRight);
                    n2 = (float)(mMainSize - paddingLeft);
                    a = 0.0f;
                    break Label_0387;
                }
                if (mJustifyContent != 2) {
                    if (mJustifyContent == 3) {
                        final float n3 = (float)paddingLeft;
                        final int mItemCount = flexLine.mItemCount;
                        float n4;
                        if (mItemCount != 1) {
                            n4 = (float)(mItemCount - 1);
                        }
                        else {
                            n4 = 1.0f;
                        }
                        a = (width - flexLine.mMainSize) / n4;
                        final float n5 = (float)(width - paddingRight);
                        n = n3;
                        n2 = n5;
                        break Label_0387;
                    }
                    if (mJustifyContent == 4) {
                        final int mItemCount2 = flexLine.mItemCount;
                        if (mItemCount2 != 0) {
                            a = (width - flexLine.mMainSize) / (float)mItemCount2;
                        }
                        else {
                            a = 0.0f;
                        }
                        final float n6 = (float)paddingLeft;
                        final float n7 = a / 2.0f;
                        n = n6 + n7;
                        n2 = width - paddingRight - n7;
                        break Label_0387;
                    }
                    if (mJustifyContent == 5) {
                        final int mItemCount3 = flexLine.mItemCount;
                        if (mItemCount3 != 0) {
                            a = (width - flexLine.mMainSize) / (float)(mItemCount3 + 1);
                        }
                        else {
                            a = 0.0f;
                        }
                        n = paddingLeft + a;
                        n2 = width - paddingRight - a;
                        break Label_0387;
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Invalid justifyContent is set: ");
                    sb.append(this.mJustifyContent);
                    throw new IllegalStateException(sb.toString());
                }
                else {
                    final float n8 = (float)paddingLeft;
                    final int mMainSize2 = flexLine.mMainSize;
                    n = n8 + (width - mMainSize2) / 2.0f;
                    n2 = width - paddingRight - (width - mMainSize2) / 2.0f;
                }
            }
            else {
                n = (float)paddingLeft;
                n2 = (float)(width - paddingRight);
            }
            a = 0.0f;
        }
        final float n9 = n - AnchorInfo.access$2400(this.mAnchorInfo);
        float n10 = n2 - AnchorInfo.access$2400(this.mAnchorInfo);
        final float max = Math.max(a, 0.0f);
        final int itemCount = flexLine.getItemCount();
        int n11 = 0;
        int i = access$1001;
        float n12 = n9;
        while (i < access$1001 + itemCount) {
            final View flexItem = this.getFlexItemAt(i);
            if (flexItem != null) {
                if (LayoutState.access$2300(layoutState) == 1) {
                    ((RecyclerView.o)this).calculateItemDecorationsForChild(flexItem, FlexboxLayoutManager.TEMP_RECT);
                    ((RecyclerView.o)this).addView(flexItem);
                }
                else {
                    ((RecyclerView.o)this).calculateItemDecorationsForChild(flexItem, FlexboxLayoutManager.TEMP_RECT);
                    ((RecyclerView.o)this).addView(flexItem, n11);
                    ++n11;
                }
                final FlexboxHelper mFlexboxHelper = this.mFlexboxHelper;
                final long n13 = mFlexboxHelper.mMeasureSpecCache[i];
                final int lowerInt = mFlexboxHelper.extractLowerInt(n13);
                final int higherInt = this.mFlexboxHelper.extractHigherInt(n13);
                final LayoutParams layoutParams = (LayoutParams)flexItem.getLayoutParams();
                if (this.shouldMeasureChild(flexItem, lowerInt, higherInt, layoutParams)) {
                    flexItem.measure(lowerInt, higherInt);
                }
                final float n14 = n12 + (layoutParams.leftMargin + ((RecyclerView.o)this).getLeftDecorationWidth(flexItem));
                final float n15 = n10 - (layoutParams.rightMargin + ((RecyclerView.o)this).getRightDecorationWidth(flexItem));
                final int n16 = access$1000 + ((RecyclerView.o)this).getTopDecorationHeight(flexItem);
                FlexboxHelper flexboxHelper;
                int round;
                int round2;
                int n17;
                if (this.mIsRtl) {
                    flexboxHelper = this.mFlexboxHelper;
                    round = Math.round(n15) - flexItem.getMeasuredWidth();
                    round2 = Math.round(n15);
                    n17 = n16 + flexItem.getMeasuredHeight();
                }
                else {
                    flexboxHelper = this.mFlexboxHelper;
                    round = Math.round(n14);
                    round2 = Math.round(n14) + flexItem.getMeasuredWidth();
                    n17 = n16 + flexItem.getMeasuredHeight();
                }
                flexboxHelper.layoutSingleChildHorizontal(flexItem, flexLine, round, n16, round2, n17);
                final float n18 = (float)(flexItem.getMeasuredWidth() + layoutParams.rightMargin + ((RecyclerView.o)this).getRightDecorationWidth(flexItem));
                final float n19 = (float)(flexItem.getMeasuredWidth() + layoutParams.leftMargin + ((RecyclerView.o)this).getLeftDecorationWidth(flexItem));
                n12 = n14 + (n18 + max);
                n10 = n15 - (n19 + max);
            }
            ++i;
        }
        LayoutState.access$1512(layoutState, LayoutState.access$2300(this.mLayoutState));
        return flexLine.getCrossSize();
    }
    
    private int layoutFlexLineMainAxisVertical(final FlexLine flexLine, final LayoutState layoutState) {
        final int paddingTop = ((RecyclerView.o)this).getPaddingTop();
        final int paddingBottom = ((RecyclerView.o)this).getPaddingBottom();
        final int height = ((RecyclerView.o)this).getHeight();
        final int access$1000 = LayoutState.access$1000(layoutState);
        final int access$1001 = LayoutState.access$1000(layoutState);
        int n = access$1000;
        int n2 = access$1001;
        if (LayoutState.access$2300(layoutState) == -1) {
            final int mCrossSize = flexLine.mCrossSize;
            n = access$1000 - mCrossSize;
            n2 = access$1001 + mCrossSize;
        }
        final int access$1002 = LayoutState.access$2200(layoutState);
        final int mJustifyContent = this.mJustifyContent;
        float n3 = 0.0f;
        float n4 = 0.0f;
        float a = 0.0f;
        Label_0411: {
            if (mJustifyContent != 0) {
                if (mJustifyContent == 1) {
                    final int mMainSize = flexLine.mMainSize;
                    n3 = (float)(height - mMainSize + paddingBottom);
                    n4 = (float)(mMainSize - paddingTop);
                    a = 0.0f;
                    break Label_0411;
                }
                if (mJustifyContent != 2) {
                    if (mJustifyContent == 3) {
                        final float n5 = (float)paddingTop;
                        final int mItemCount = flexLine.mItemCount;
                        float n6;
                        if (mItemCount != 1) {
                            n6 = (float)(mItemCount - 1);
                        }
                        else {
                            n6 = 1.0f;
                        }
                        a = (height - flexLine.mMainSize) / n6;
                        final float n7 = (float)(height - paddingBottom);
                        n3 = n5;
                        n4 = n7;
                        break Label_0411;
                    }
                    if (mJustifyContent == 4) {
                        final int mItemCount2 = flexLine.mItemCount;
                        if (mItemCount2 != 0) {
                            a = (height - flexLine.mMainSize) / (float)mItemCount2;
                        }
                        else {
                            a = 0.0f;
                        }
                        final float n8 = (float)paddingTop;
                        final float n9 = a / 2.0f;
                        n3 = n8 + n9;
                        n4 = height - paddingBottom - n9;
                        break Label_0411;
                    }
                    if (mJustifyContent == 5) {
                        final int mItemCount3 = flexLine.mItemCount;
                        if (mItemCount3 != 0) {
                            a = (height - flexLine.mMainSize) / (float)(mItemCount3 + 1);
                        }
                        else {
                            a = 0.0f;
                        }
                        n3 = paddingTop + a;
                        n4 = height - paddingBottom - a;
                        break Label_0411;
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Invalid justifyContent is set: ");
                    sb.append(this.mJustifyContent);
                    throw new IllegalStateException(sb.toString());
                }
                else {
                    final float n10 = (float)paddingTop;
                    final int mMainSize2 = flexLine.mMainSize;
                    n3 = n10 + (height - mMainSize2) / 2.0f;
                    n4 = height - paddingBottom - (height - mMainSize2) / 2.0f;
                }
            }
            else {
                n3 = (float)paddingTop;
                n4 = (float)(height - paddingBottom);
            }
            a = 0.0f;
        }
        final float n11 = n3 - AnchorInfo.access$2400(this.mAnchorInfo);
        float n12 = n4 - AnchorInfo.access$2400(this.mAnchorInfo);
        final float max = Math.max(a, 0.0f);
        final int itemCount = flexLine.getItemCount();
        int n13 = 0;
        int i = access$1002;
        float n14 = n11;
        while (i < access$1002 + itemCount) {
            final View flexItem = this.getFlexItemAt(i);
            if (flexItem != null) {
                final FlexboxHelper mFlexboxHelper = this.mFlexboxHelper;
                final long n15 = mFlexboxHelper.mMeasureSpecCache[i];
                final int lowerInt = mFlexboxHelper.extractLowerInt(n15);
                final int higherInt = this.mFlexboxHelper.extractHigherInt(n15);
                final LayoutParams layoutParams = (LayoutParams)flexItem.getLayoutParams();
                if (this.shouldMeasureChild(flexItem, lowerInt, higherInt, layoutParams)) {
                    flexItem.measure(lowerInt, higherInt);
                }
                final float n16 = n14 + (layoutParams.topMargin + ((RecyclerView.o)this).getTopDecorationHeight(flexItem));
                final float n17 = n12 - (layoutParams.rightMargin + ((RecyclerView.o)this).getBottomDecorationHeight(flexItem));
                if (LayoutState.access$2300(layoutState) == 1) {
                    ((RecyclerView.o)this).calculateItemDecorationsForChild(flexItem, FlexboxLayoutManager.TEMP_RECT);
                    ((RecyclerView.o)this).addView(flexItem);
                }
                else {
                    ((RecyclerView.o)this).calculateItemDecorationsForChild(flexItem, FlexboxLayoutManager.TEMP_RECT);
                    ((RecyclerView.o)this).addView(flexItem, n13);
                    ++n13;
                }
                final int n18 = n + ((RecyclerView.o)this).getLeftDecorationWidth(flexItem);
                final int n19 = n2 - ((RecyclerView.o)this).getRightDecorationWidth(flexItem);
                final boolean mIsRtl = this.mIsRtl;
                FlexboxHelper flexboxHelper;
                int n21;
                int n23;
                int n24;
                int n25;
                if (mIsRtl) {
                    if (this.mFromBottomToTop) {
                        flexboxHelper = this.mFlexboxHelper;
                        final int n20 = n19 - flexItem.getMeasuredWidth();
                        final int round = Math.round(n17);
                        final int measuredHeight = flexItem.getMeasuredHeight();
                        n21 = Math.round(n17);
                        final int n22 = round - measuredHeight;
                        n23 = n19;
                        n24 = n20;
                        n25 = n22;
                    }
                    else {
                        flexboxHelper = this.mFlexboxHelper;
                        final int n26 = n19 - flexItem.getMeasuredWidth();
                        n25 = Math.round(n16);
                        n21 = Math.round(n16) + flexItem.getMeasuredHeight();
                        n23 = n19;
                        n24 = n26;
                    }
                }
                else {
                    if (this.mFromBottomToTop) {
                        flexboxHelper = this.mFlexboxHelper;
                        n25 = Math.round(n17) - flexItem.getMeasuredHeight();
                        n23 = n18 + flexItem.getMeasuredWidth();
                        n21 = Math.round(n17);
                    }
                    else {
                        flexboxHelper = this.mFlexboxHelper;
                        n25 = Math.round(n16);
                        n23 = n18 + flexItem.getMeasuredWidth();
                        n21 = Math.round(n16) + flexItem.getMeasuredHeight();
                    }
                    n24 = n18;
                }
                flexboxHelper.layoutSingleChildVertical(flexItem, flexLine, mIsRtl, n24, n25, n23, n21);
                final float n27 = (float)(flexItem.getMeasuredHeight() + layoutParams.topMargin + ((RecyclerView.o)this).getBottomDecorationHeight(flexItem));
                final float n28 = (float)(flexItem.getMeasuredHeight() + layoutParams.bottomMargin + ((RecyclerView.o)this).getTopDecorationHeight(flexItem));
                n14 = n16 + (n27 + max);
                n12 = n17 - (n28 + max);
            }
            ++i;
        }
        LayoutState.access$1512(layoutState, LayoutState.access$2300(this.mLayoutState));
        return flexLine.getCrossSize();
    }
    
    private void recycleByLayoutState(final v v, final LayoutState layoutState) {
        if (!LayoutState.access$500(layoutState)) {
            return;
        }
        if (LayoutState.access$2300(layoutState) == -1) {
            this.recycleFlexLinesFromEnd(v, layoutState);
        }
        else {
            this.recycleFlexLinesFromStart(v, layoutState);
        }
    }
    
    private void recycleChildren(final v v, final int n, int i) {
        while (i >= n) {
            ((RecyclerView.o)this).removeAndRecycleViewAt(i, v);
            --i;
        }
    }
    
    private void recycleFlexLinesFromEnd(final v v, final LayoutState layoutState) {
        if (LayoutState.access$2000(layoutState) < 0) {
            return;
        }
        int childCount = ((RecyclerView.o)this).getChildCount();
        if (childCount == 0) {
            return;
        }
        final int n = childCount - 1;
        final View child = ((RecyclerView.o)this).getChildAt(n);
        if (child == null) {
            return;
        }
        int n2 = this.mFlexboxHelper.mIndexToFlexLine[((RecyclerView.o)this).getPosition(child)];
        if (n2 == -1) {
            return;
        }
        FlexLine flexLine = this.mFlexLines.get(n2);
        int n3 = n;
        int n4;
        while (true) {
            n4 = childCount;
            if (n3 < 0) {
                break;
            }
            final View child2 = ((RecyclerView.o)this).getChildAt(n3);
            int n5;
            FlexLine flexLine2;
            if (child2 == null) {
                n5 = n2;
                flexLine2 = flexLine;
            }
            else {
                n4 = childCount;
                if (!this.canViewBeRecycledFromEnd(child2, LayoutState.access$2000(layoutState))) {
                    break;
                }
                n5 = n2;
                flexLine2 = flexLine;
                if (flexLine.mFirstIndex == ((RecyclerView.o)this).getPosition(child2)) {
                    if (n2 <= 0) {
                        n4 = n3;
                        break;
                    }
                    n5 = n2 + LayoutState.access$2300(layoutState);
                    flexLine2 = this.mFlexLines.get(n5);
                    childCount = n3;
                }
            }
            --n3;
            n2 = n5;
            flexLine = flexLine2;
        }
        this.recycleChildren(v, n4, n);
    }
    
    private void recycleFlexLinesFromStart(final v v, final LayoutState layoutState) {
        if (LayoutState.access$2000(layoutState) < 0) {
            return;
        }
        final int childCount = ((RecyclerView.o)this).getChildCount();
        if (childCount == 0) {
            return;
        }
        final View child = ((RecyclerView.o)this).getChildAt(0);
        if (child == null) {
            return;
        }
        int n = this.mFlexboxHelper.mIndexToFlexLine[((RecyclerView.o)this).getPosition(child)];
        int n2 = -1;
        if (n == -1) {
            return;
        }
        FlexLine flexLine = this.mFlexLines.get(n);
        int n3 = 0;
        int n4;
        while (true) {
            n4 = n2;
            if (n3 >= childCount) {
                break;
            }
            final View child2 = ((RecyclerView.o)this).getChildAt(n3);
            int n5;
            FlexLine flexLine2;
            if (child2 == null) {
                n5 = n;
                flexLine2 = flexLine;
            }
            else {
                n4 = n2;
                if (!this.canViewBeRecycledFromStart(child2, LayoutState.access$2000(layoutState))) {
                    break;
                }
                n5 = n;
                flexLine2 = flexLine;
                if (flexLine.mLastIndex == ((RecyclerView.o)this).getPosition(child2)) {
                    if (n >= this.mFlexLines.size() - 1) {
                        n4 = n3;
                        break;
                    }
                    n5 = n + LayoutState.access$2300(layoutState);
                    flexLine2 = this.mFlexLines.get(n5);
                    n2 = n3;
                }
            }
            ++n3;
            n = n5;
            flexLine = flexLine2;
        }
        this.recycleChildren(v, 0, n4);
    }
    
    private void resolveInfiniteAmount() {
        int n;
        if (this.isMainAxisDirectionHorizontal()) {
            n = ((RecyclerView.o)this).getHeightMode();
        }
        else {
            n = ((RecyclerView.o)this).getWidthMode();
        }
        LayoutState.access$1102(this.mLayoutState, n == 0 || n == Integer.MIN_VALUE);
    }
    
    private void resolveLayoutDirection() {
        final int layoutDirection = ((RecyclerView.o)this).getLayoutDirection();
        final int mFlexDirection = this.mFlexDirection;
        boolean mIsRtl = false;
        final boolean b = false;
        while (true) {
            Label_0159: {
                boolean mFromBottomToTop;
                if (mFlexDirection != 0) {
                    if (mFlexDirection != 1) {
                        if (mFlexDirection != 2) {
                            if (mFlexDirection == 3) {
                                if (layoutDirection == 1) {
                                    mIsRtl = true;
                                }
                                this.mIsRtl = mIsRtl;
                                if (this.mFlexWrap == 2) {
                                    this.mIsRtl = (mIsRtl ^ true);
                                }
                                this.mFromBottomToTop = true;
                                return;
                            }
                            this.mIsRtl = false;
                            mFromBottomToTop = b;
                        }
                        else {
                            final boolean mIsRtl2 = layoutDirection == 1;
                            this.mIsRtl = mIsRtl2;
                            mFromBottomToTop = b;
                            if (this.mFlexWrap == 2) {
                                this.mIsRtl = (mIsRtl2 ^ true);
                                mFromBottomToTop = b;
                            }
                        }
                    }
                    else {
                        this.mIsRtl = (layoutDirection != 1);
                        mFromBottomToTop = b;
                        if (this.mFlexWrap == 2) {
                            break Label_0159;
                        }
                    }
                }
                else {
                    this.mIsRtl = (layoutDirection == 1);
                    mFromBottomToTop = b;
                    if (this.mFlexWrap == 2) {
                        break Label_0159;
                    }
                }
                this.mFromBottomToTop = mFromBottomToTop;
                return;
            }
            boolean mFromBottomToTop = true;
            continue;
        }
    }
    
    private boolean shouldMeasureChild(final View view, final int n, final int n2, final p p4) {
        return view.isLayoutRequested() || !((RecyclerView.o)this).isMeasurementCacheEnabled() || !isMeasurementUpToDate(view.getWidth(), n, p4.width) || !isMeasurementUpToDate(view.getHeight(), n2, p4.height);
    }
    
    private boolean updateAnchorFromChildren(final a0 a0, final AnchorInfo anchorInfo) {
        final int childCount = ((RecyclerView.o)this).getChildCount();
        boolean b = false;
        if (childCount == 0) {
            return false;
        }
        View view;
        if (AnchorInfo.access$900(anchorInfo)) {
            view = this.findLastReferenceChild(a0.b());
        }
        else {
            view = this.findFirstReferenceChild(a0.b());
        }
        if (view != null) {
            anchorInfo.assignFromView(view);
            if (!a0.e() && ((RecyclerView.o)this).supportsPredictiveItemAnimations()) {
                if (this.mOrientationHelper.g(view) >= this.mOrientationHelper.i() || this.mOrientationHelper.d(view) < this.mOrientationHelper.m()) {
                    b = true;
                }
                if (b) {
                    int n;
                    if (AnchorInfo.access$900(anchorInfo)) {
                        n = this.mOrientationHelper.i();
                    }
                    else {
                        n = this.mOrientationHelper.m();
                    }
                    AnchorInfo.access$1702(anchorInfo, n);
                }
            }
            return true;
        }
        return false;
    }
    
    private boolean updateAnchorFromPendingState(final a0 a0, final AnchorInfo anchorInfo, final SavedState savedState) {
        final boolean e = a0.e();
        boolean b = false;
        if (!e) {
            final int mPendingScrollPosition = this.mPendingScrollPosition;
            if (mPendingScrollPosition != -1) {
                if (mPendingScrollPosition >= 0 && mPendingScrollPosition < a0.b()) {
                    AnchorInfo.access$1302(anchorInfo, this.mPendingScrollPosition);
                    AnchorInfo.access$1402(anchorInfo, this.mFlexboxHelper.mIndexToFlexLine[AnchorInfo.access$1300(anchorInfo)]);
                    final SavedState mPendingSavedState = this.mPendingSavedState;
                    if (mPendingSavedState != null && mPendingSavedState.hasValidAnchor(a0.b())) {
                        AnchorInfo.access$1702(anchorInfo, this.mOrientationHelper.m() + SavedState.access$300(savedState));
                        AnchorInfo.access$1802(anchorInfo, true);
                        AnchorInfo.access$1402(anchorInfo, -1);
                        return true;
                    }
                    if (this.mPendingScrollPositionOffset == Integer.MIN_VALUE) {
                        final View viewByPosition = ((RecyclerView.o)this).findViewByPosition(this.mPendingScrollPosition);
                        if (viewByPosition != null) {
                            if (this.mOrientationHelper.e(viewByPosition) > this.mOrientationHelper.n()) {
                                anchorInfo.assignCoordinateFromPadding();
                                return true;
                            }
                            if (this.mOrientationHelper.g(viewByPosition) - this.mOrientationHelper.m() < 0) {
                                AnchorInfo.access$1702(anchorInfo, this.mOrientationHelper.m());
                                AnchorInfo.access$902(anchorInfo, false);
                                return true;
                            }
                            if (this.mOrientationHelper.i() - this.mOrientationHelper.d(viewByPosition) < 0) {
                                AnchorInfo.access$1702(anchorInfo, this.mOrientationHelper.i());
                                AnchorInfo.access$902(anchorInfo, true);
                                return true;
                            }
                            int g;
                            if (AnchorInfo.access$900(anchorInfo)) {
                                g = this.mOrientationHelper.d(viewByPosition) + this.mOrientationHelper.o();
                            }
                            else {
                                g = this.mOrientationHelper.g(viewByPosition);
                            }
                            AnchorInfo.access$1702(anchorInfo, g);
                        }
                        else {
                            if (((RecyclerView.o)this).getChildCount() > 0) {
                                final View child = ((RecyclerView.o)this).getChildAt(0);
                                if (child != null) {
                                    if (this.mPendingScrollPosition < ((RecyclerView.o)this).getPosition(child)) {
                                        b = true;
                                    }
                                    AnchorInfo.access$902(anchorInfo, b);
                                }
                            }
                            anchorInfo.assignCoordinateFromPadding();
                        }
                        return true;
                    }
                    int n;
                    if (!this.isMainAxisDirectionHorizontal() && this.mIsRtl) {
                        n = this.mPendingScrollPositionOffset - this.mOrientationHelper.j();
                    }
                    else {
                        n = this.mOrientationHelper.m() + this.mPendingScrollPositionOffset;
                    }
                    AnchorInfo.access$1702(anchorInfo, n);
                    return true;
                }
                else {
                    this.mPendingScrollPosition = -1;
                    this.mPendingScrollPositionOffset = Integer.MIN_VALUE;
                }
            }
        }
        return false;
    }
    
    private void updateAnchorInfoForLayout(final a0 a0, final AnchorInfo anchorInfo) {
        if (this.updateAnchorFromPendingState(a0, anchorInfo, this.mPendingSavedState)) {
            return;
        }
        if (this.updateAnchorFromChildren(a0, anchorInfo)) {
            return;
        }
        anchorInfo.assignCoordinateFromPadding();
        AnchorInfo.access$1302(anchorInfo, 0);
        AnchorInfo.access$1402(anchorInfo, 0);
    }
    
    private void updateDirtyPosition(int n) {
        if (n >= this.findLastVisibleItemPosition()) {
            return;
        }
        final int childCount = ((RecyclerView.o)this).getChildCount();
        this.mFlexboxHelper.ensureMeasureSpecCache(childCount);
        this.mFlexboxHelper.ensureMeasuredSizeCache(childCount);
        this.mFlexboxHelper.ensureIndexToFlexLine(childCount);
        if (n >= this.mFlexboxHelper.mIndexToFlexLine.length) {
            return;
        }
        this.mDirtyPosition = n;
        final View childClosestToStart = this.getChildClosestToStart();
        if (childClosestToStart == null) {
            return;
        }
        this.mPendingScrollPosition = ((RecyclerView.o)this).getPosition(childClosestToStart);
        if (!this.isMainAxisDirectionHorizontal() && this.mIsRtl) {
            n = this.mOrientationHelper.d(childClosestToStart) + this.mOrientationHelper.j();
        }
        else {
            n = this.mOrientationHelper.g(childClosestToStart) - this.mOrientationHelper.m();
        }
        this.mPendingScrollPositionOffset = n;
    }
    
    private void updateFlexLines(int n) {
        final int measureSpec = View$MeasureSpec.makeMeasureSpec(((RecyclerView.o)this).getWidth(), ((RecyclerView.o)this).getWidthMode());
        final int measureSpec2 = View$MeasureSpec.makeMeasureSpec(((RecyclerView.o)this).getHeight(), ((RecyclerView.o)this).getHeightMode());
        final int width = ((RecyclerView.o)this).getWidth();
        final int height = ((RecyclerView.o)this).getHeight();
        final boolean mainAxisDirectionHorizontal = this.isMainAxisDirectionHorizontal();
        final int n2 = 1;
        int n3 = 1;
        int n5 = 0;
        Label_0171: {
            int n4;
            if (mainAxisDirectionHorizontal) {
                final int mLastWidth = this.mLastWidth;
                if (mLastWidth == Integer.MIN_VALUE || mLastWidth == width) {
                    n3 = 0;
                }
                n4 = n3;
                if (LayoutState.access$1100(this.mLayoutState)) {
                    n5 = this.mContext.getResources().getDisplayMetrics().heightPixels;
                    break Label_0171;
                }
            }
            else {
                final int mLastHeight = this.mLastHeight;
                if (mLastHeight != Integer.MIN_VALUE && mLastHeight != height) {
                    n3 = n2;
                }
                else {
                    n3 = 0;
                }
                n4 = n3;
                if (LayoutState.access$1100(this.mLayoutState)) {
                    n5 = this.mContext.getResources().getDisplayMetrics().widthPixels;
                    break Label_0171;
                }
            }
            final int access$1200 = LayoutState.access$1200(this.mLayoutState);
            n3 = n4;
            n5 = access$1200;
        }
        this.mLastWidth = width;
        this.mLastHeight = height;
        final int mDirtyPosition = this.mDirtyPosition;
        if (mDirtyPosition == -1 && (this.mPendingScrollPosition != -1 || n3 != 0)) {
            if (AnchorInfo.access$900(this.mAnchorInfo)) {
                return;
            }
            this.mFlexLines.clear();
            this.mFlexLinesResult.reset();
            final boolean mainAxisDirectionHorizontal2 = this.isMainAxisDirectionHorizontal();
            final FlexboxHelper mFlexboxHelper = this.mFlexboxHelper;
            final FlexboxHelper.FlexLinesResult mFlexLinesResult = this.mFlexLinesResult;
            if (mainAxisDirectionHorizontal2) {
                mFlexboxHelper.calculateHorizontalFlexLinesToIndex(mFlexLinesResult, measureSpec, measureSpec2, n5, AnchorInfo.access$1300(this.mAnchorInfo), this.mFlexLines);
            }
            else {
                mFlexboxHelper.calculateVerticalFlexLinesToIndex(mFlexLinesResult, measureSpec, measureSpec2, n5, AnchorInfo.access$1300(this.mAnchorInfo), this.mFlexLines);
            }
            this.mFlexLines = this.mFlexLinesResult.mFlexLines;
            this.mFlexboxHelper.determineMainSize(measureSpec, measureSpec2);
            this.mFlexboxHelper.stretchViews();
            final AnchorInfo mAnchorInfo = this.mAnchorInfo;
            AnchorInfo.access$1402(mAnchorInfo, this.mFlexboxHelper.mIndexToFlexLine[AnchorInfo.access$1300(mAnchorInfo)]);
            LayoutState.access$1502(this.mLayoutState, AnchorInfo.access$1400(this.mAnchorInfo));
        }
        else {
            int n6;
            if (mDirtyPosition != -1) {
                n6 = Math.min(mDirtyPosition, AnchorInfo.access$1300(this.mAnchorInfo));
            }
            else {
                n6 = AnchorInfo.access$1300(this.mAnchorInfo);
            }
            this.mFlexLinesResult.reset();
            Label_0621: {
                FlexboxHelper flexboxHelper;
                FlexboxHelper.FlexLinesResult flexLinesResult;
                List<FlexLine> list;
                int n7;
                int n8;
                if (this.isMainAxisDirectionHorizontal()) {
                    if (this.mFlexLines.size() <= 0) {
                        this.mFlexboxHelper.ensureIndexToFlexLine(n);
                        this.mFlexboxHelper.calculateHorizontalFlexLines(this.mFlexLinesResult, measureSpec, measureSpec2, n5, 0, this.mFlexLines);
                        break Label_0621;
                    }
                    this.mFlexboxHelper.clearFlexLines(this.mFlexLines, n6);
                    flexboxHelper = this.mFlexboxHelper;
                    flexLinesResult = this.mFlexLinesResult;
                    n = AnchorInfo.access$1300(this.mAnchorInfo);
                    list = this.mFlexLines;
                    n7 = measureSpec;
                    n8 = measureSpec2;
                }
                else {
                    if (this.mFlexLines.size() <= 0) {
                        this.mFlexboxHelper.ensureIndexToFlexLine(n);
                        this.mFlexboxHelper.calculateVerticalFlexLines(this.mFlexLinesResult, measureSpec, measureSpec2, n5, 0, this.mFlexLines);
                        break Label_0621;
                    }
                    this.mFlexboxHelper.clearFlexLines(this.mFlexLines, n6);
                    flexboxHelper = this.mFlexboxHelper;
                    flexLinesResult = this.mFlexLinesResult;
                    n = AnchorInfo.access$1300(this.mAnchorInfo);
                    list = this.mFlexLines;
                    n7 = measureSpec2;
                    n8 = measureSpec;
                }
                flexboxHelper.calculateFlexLines(flexLinesResult, n7, n8, n5, n6, n, list);
            }
            this.mFlexLines = this.mFlexLinesResult.mFlexLines;
            this.mFlexboxHelper.determineMainSize(measureSpec, measureSpec2, n6);
            this.mFlexboxHelper.stretchViews(n6);
        }
    }
    
    private void updateLayoutState(int position, final int n) {
        LayoutState.access$2302(this.mLayoutState, position);
        final boolean mainAxisDirectionHorizontal = this.isMainAxisDirectionHorizontal();
        final int measureSpec = View$MeasureSpec.makeMeasureSpec(((RecyclerView.o)this).getWidth(), ((RecyclerView.o)this).getWidthMode());
        final int measureSpec2 = View$MeasureSpec.makeMeasureSpec(((RecyclerView.o)this).getHeight(), ((RecyclerView.o)this).getHeightMode());
        final boolean b = !mainAxisDirectionHorizontal && this.mIsRtl;
        if (position == 1) {
            final View child = ((RecyclerView.o)this).getChildAt(((RecyclerView.o)this).getChildCount() - 1);
            if (child == null) {
                return;
            }
            LayoutState.access$1002(this.mLayoutState, this.mOrientationHelper.d(child));
            position = ((RecyclerView.o)this).getPosition(child);
            final View lastReferenceViewInLine = this.findLastReferenceViewInLine(child, this.mFlexLines.get(this.mFlexboxHelper.mIndexToFlexLine[position]));
            LayoutState.access$2502(this.mLayoutState, 1);
            final LayoutState mLayoutState = this.mLayoutState;
            LayoutState.access$2202(mLayoutState, position + LayoutState.access$2500(mLayoutState));
            if (this.mFlexboxHelper.mIndexToFlexLine.length <= LayoutState.access$2200(this.mLayoutState)) {
                LayoutState.access$1502(this.mLayoutState, -1);
            }
            else {
                final LayoutState mLayoutState2 = this.mLayoutState;
                LayoutState.access$1502(mLayoutState2, this.mFlexboxHelper.mIndexToFlexLine[LayoutState.access$2200(mLayoutState2)]);
            }
            if (b) {
                LayoutState.access$1002(this.mLayoutState, this.mOrientationHelper.g(lastReferenceViewInLine));
                LayoutState.access$2002(this.mLayoutState, -this.mOrientationHelper.g(lastReferenceViewInLine) + this.mOrientationHelper.m());
                final LayoutState mLayoutState3 = this.mLayoutState;
                LayoutState.access$2002(mLayoutState3, Math.max(LayoutState.access$2000(mLayoutState3), 0));
            }
            else {
                LayoutState.access$1002(this.mLayoutState, this.mOrientationHelper.d(lastReferenceViewInLine));
                LayoutState.access$2002(this.mLayoutState, this.mOrientationHelper.d(lastReferenceViewInLine) - this.mOrientationHelper.i());
            }
            if ((LayoutState.access$1500(this.mLayoutState) == -1 || LayoutState.access$1500(this.mLayoutState) > this.mFlexLines.size() - 1) && LayoutState.access$2200(this.mLayoutState) <= this.getFlexItemCount()) {
                position = n - LayoutState.access$2000(this.mLayoutState);
                this.mFlexLinesResult.reset();
                if (position > 0) {
                    final FlexboxHelper mFlexboxHelper = this.mFlexboxHelper;
                    final FlexboxHelper.FlexLinesResult mFlexLinesResult = this.mFlexLinesResult;
                    final int access$2200 = LayoutState.access$2200(this.mLayoutState);
                    final List<FlexLine> mFlexLines = this.mFlexLines;
                    if (mainAxisDirectionHorizontal) {
                        mFlexboxHelper.calculateHorizontalFlexLines(mFlexLinesResult, measureSpec, measureSpec2, position, access$2200, mFlexLines);
                    }
                    else {
                        mFlexboxHelper.calculateVerticalFlexLines(mFlexLinesResult, measureSpec, measureSpec2, position, access$2200, mFlexLines);
                    }
                    this.mFlexboxHelper.determineMainSize(measureSpec, measureSpec2, LayoutState.access$2200(this.mLayoutState));
                    this.mFlexboxHelper.stretchViews(LayoutState.access$2200(this.mLayoutState));
                }
            }
        }
        else {
            final View child2 = ((RecyclerView.o)this).getChildAt(0);
            if (child2 == null) {
                return;
            }
            LayoutState.access$1002(this.mLayoutState, this.mOrientationHelper.g(child2));
            final int position2 = ((RecyclerView.o)this).getPosition(child2);
            position = this.mFlexboxHelper.mIndexToFlexLine[position2];
            final View firstReferenceViewInLine = this.findFirstReferenceViewInLine(child2, this.mFlexLines.get(position));
            LayoutState.access$2502(this.mLayoutState, 1);
            if ((position = this.mFlexboxHelper.mIndexToFlexLine[position2]) == -1) {
                position = 0;
            }
            if (position > 0) {
                LayoutState.access$2202(this.mLayoutState, position2 - this.mFlexLines.get(position - 1).getItemCount());
            }
            else {
                LayoutState.access$2202(this.mLayoutState, -1);
            }
            final LayoutState mLayoutState4 = this.mLayoutState;
            if (position > 0) {
                --position;
            }
            else {
                position = 0;
            }
            LayoutState.access$1502(mLayoutState4, position);
            final LayoutState mLayoutState5 = this.mLayoutState;
            final androidx.recyclerview.widget.i mOrientationHelper = this.mOrientationHelper;
            if (b) {
                LayoutState.access$1002(mLayoutState5, mOrientationHelper.d(firstReferenceViewInLine));
                LayoutState.access$2002(this.mLayoutState, this.mOrientationHelper.d(firstReferenceViewInLine) - this.mOrientationHelper.i());
                final LayoutState mLayoutState6 = this.mLayoutState;
                LayoutState.access$2002(mLayoutState6, Math.max(LayoutState.access$2000(mLayoutState6), 0));
            }
            else {
                LayoutState.access$1002(mLayoutState5, mOrientationHelper.g(firstReferenceViewInLine));
                LayoutState.access$2002(this.mLayoutState, -this.mOrientationHelper.g(firstReferenceViewInLine) + this.mOrientationHelper.m());
            }
        }
        final LayoutState mLayoutState7 = this.mLayoutState;
        LayoutState.access$1202(mLayoutState7, n - LayoutState.access$2000(mLayoutState7));
    }
    
    private void updateLayoutStateToFillEnd(final AnchorInfo anchorInfo, final boolean b, final boolean b2) {
        if (b2) {
            this.resolveInfiniteAmount();
        }
        else {
            LayoutState.access$1102(this.mLayoutState, false);
        }
        LayoutState layoutState;
        int n;
        int n2;
        if (!this.isMainAxisDirectionHorizontal() && this.mIsRtl) {
            layoutState = this.mLayoutState;
            n = AnchorInfo.access$1700(anchorInfo);
            n2 = ((RecyclerView.o)this).getPaddingRight();
        }
        else {
            layoutState = this.mLayoutState;
            n = this.mOrientationHelper.i();
            n2 = AnchorInfo.access$1700(anchorInfo);
        }
        LayoutState.access$1202(layoutState, n - n2);
        LayoutState.access$2202(this.mLayoutState, AnchorInfo.access$1300(anchorInfo));
        LayoutState.access$2502(this.mLayoutState, 1);
        LayoutState.access$2302(this.mLayoutState, 1);
        LayoutState.access$1002(this.mLayoutState, AnchorInfo.access$1700(anchorInfo));
        LayoutState.access$2002(this.mLayoutState, Integer.MIN_VALUE);
        LayoutState.access$1502(this.mLayoutState, AnchorInfo.access$1400(anchorInfo));
        if (b && this.mFlexLines.size() > 1 && AnchorInfo.access$1400(anchorInfo) >= 0 && AnchorInfo.access$1400(anchorInfo) < this.mFlexLines.size() - 1) {
            final FlexLine flexLine = this.mFlexLines.get(AnchorInfo.access$1400(anchorInfo));
            LayoutState.access$1508(this.mLayoutState);
            LayoutState.access$2212(this.mLayoutState, flexLine.getItemCount());
        }
    }
    
    private void updateLayoutStateToFillStart(final AnchorInfo anchorInfo, final boolean b, final boolean b2) {
        if (b2) {
            this.resolveInfiniteAmount();
        }
        else {
            LayoutState.access$1102(this.mLayoutState, false);
        }
        LayoutState layoutState;
        int access$1700;
        if (!this.isMainAxisDirectionHorizontal() && this.mIsRtl) {
            layoutState = this.mLayoutState;
            access$1700 = this.mParent.getWidth() - AnchorInfo.access$1700(anchorInfo);
        }
        else {
            layoutState = this.mLayoutState;
            access$1700 = AnchorInfo.access$1700(anchorInfo);
        }
        LayoutState.access$1202(layoutState, access$1700 - this.mOrientationHelper.m());
        LayoutState.access$2202(this.mLayoutState, AnchorInfo.access$1300(anchorInfo));
        LayoutState.access$2502(this.mLayoutState, 1);
        LayoutState.access$2302(this.mLayoutState, -1);
        LayoutState.access$1002(this.mLayoutState, AnchorInfo.access$1700(anchorInfo));
        LayoutState.access$2002(this.mLayoutState, Integer.MIN_VALUE);
        LayoutState.access$1502(this.mLayoutState, AnchorInfo.access$1400(anchorInfo));
        if (b && AnchorInfo.access$1400(anchorInfo) > 0 && this.mFlexLines.size() > AnchorInfo.access$1400(anchorInfo)) {
            final FlexLine flexLine = this.mFlexLines.get(AnchorInfo.access$1400(anchorInfo));
            LayoutState.access$1510(this.mLayoutState);
            LayoutState.access$2220(this.mLayoutState, flexLine.getItemCount());
        }
    }
    
    @Override
    public boolean canScrollHorizontally() {
        if (this.mFlexWrap == 0) {
            return this.isMainAxisDirectionHorizontal();
        }
        if (this.isMainAxisDirectionHorizontal()) {
            final int width = ((RecyclerView.o)this).getWidth();
            final View mParent = this.mParent;
            final boolean b = false;
            int width2;
            if (mParent != null) {
                width2 = mParent.getWidth();
            }
            else {
                width2 = 0;
            }
            if (width <= width2) {
                return b;
            }
        }
        return true;
    }
    
    @Override
    public boolean canScrollVertically() {
        final int mFlexWrap = this.mFlexWrap;
        final boolean b = true;
        if (mFlexWrap == 0) {
            return this.isMainAxisDirectionHorizontal() ^ true;
        }
        boolean b2 = b;
        if (!this.isMainAxisDirectionHorizontal()) {
            final int height = ((RecyclerView.o)this).getHeight();
            final View mParent = this.mParent;
            int height2;
            if (mParent != null) {
                height2 = mParent.getHeight();
            }
            else {
                height2 = 0;
            }
            b2 = (height > height2 && b);
        }
        return b2;
    }
    
    @Override
    public boolean checkLayoutParams(final p p) {
        return p instanceof LayoutParams;
    }
    
    @Override
    public int computeHorizontalScrollExtent(final a0 a0) {
        return this.computeScrollExtent(a0);
    }
    
    @Override
    public int computeHorizontalScrollOffset(final a0 a0) {
        return this.computeScrollOffset(a0);
    }
    
    @Override
    public int computeHorizontalScrollRange(final a0 a0) {
        return this.computeScrollRange(a0);
    }
    
    @Override
    public PointF computeScrollVectorForPosition(int n) {
        if (((RecyclerView.o)this).getChildCount() == 0) {
            return null;
        }
        final View child = ((RecyclerView.o)this).getChildAt(0);
        if (child == null) {
            return null;
        }
        if (n < ((RecyclerView.o)this).getPosition(child)) {
            n = -1;
        }
        else {
            n = 1;
        }
        if (this.isMainAxisDirectionHorizontal()) {
            return new PointF(0.0f, (float)n);
        }
        return new PointF((float)n, 0.0f);
    }
    
    @Override
    public int computeVerticalScrollExtent(final a0 a0) {
        return this.computeScrollExtent(a0);
    }
    
    @Override
    public int computeVerticalScrollOffset(final a0 a0) {
        return this.computeScrollOffset(a0);
    }
    
    @Override
    public int computeVerticalScrollRange(final a0 a0) {
        return this.computeScrollRange(a0);
    }
    
    public int findFirstCompletelyVisibleItemPosition() {
        final View oneVisibleChild = this.findOneVisibleChild(0, ((RecyclerView.o)this).getChildCount(), true);
        int position;
        if (oneVisibleChild == null) {
            position = -1;
        }
        else {
            position = ((RecyclerView.o)this).getPosition(oneVisibleChild);
        }
        return position;
    }
    
    public int findFirstVisibleItemPosition() {
        final View oneVisibleChild = this.findOneVisibleChild(0, ((RecyclerView.o)this).getChildCount(), false);
        int position;
        if (oneVisibleChild == null) {
            position = -1;
        }
        else {
            position = ((RecyclerView.o)this).getPosition(oneVisibleChild);
        }
        return position;
    }
    
    public int findLastCompletelyVisibleItemPosition() {
        final int childCount = ((RecyclerView.o)this).getChildCount();
        int position = -1;
        final View oneVisibleChild = this.findOneVisibleChild(childCount - 1, -1, true);
        if (oneVisibleChild != null) {
            position = ((RecyclerView.o)this).getPosition(oneVisibleChild);
        }
        return position;
    }
    
    public int findLastVisibleItemPosition() {
        final int childCount = ((RecyclerView.o)this).getChildCount();
        int position = -1;
        final View oneVisibleChild = this.findOneVisibleChild(childCount - 1, -1, false);
        if (oneVisibleChild != null) {
            position = ((RecyclerView.o)this).getPosition(oneVisibleChild);
        }
        return position;
    }
    
    @Override
    public p generateDefaultLayoutParams() {
        return new LayoutParams(-2, -2);
    }
    
    @Override
    public p generateLayoutParams(final Context context, final AttributeSet set) {
        return new LayoutParams(context, set);
    }
    
    @Override
    public int getAlignContent() {
        return 5;
    }
    
    @Override
    public int getAlignItems() {
        return this.mAlignItems;
    }
    
    @Override
    public int getChildHeightMeasureSpec(final int n, final int n2, final int n3) {
        return RecyclerView.o.getChildMeasureSpec(((RecyclerView.o)this).getHeight(), ((RecyclerView.o)this).getHeightMode(), n2, n3, this.canScrollVertically());
    }
    
    @Override
    public int getChildWidthMeasureSpec(final int n, final int n2, final int n3) {
        return RecyclerView.o.getChildMeasureSpec(((RecyclerView.o)this).getWidth(), ((RecyclerView.o)this).getWidthMode(), n2, n3, this.canScrollHorizontally());
    }
    
    @Override
    public int getDecorationLengthCrossAxis(final View view) {
        int n;
        int n2;
        if (this.isMainAxisDirectionHorizontal()) {
            n = ((RecyclerView.o)this).getTopDecorationHeight(view);
            n2 = ((RecyclerView.o)this).getBottomDecorationHeight(view);
        }
        else {
            n = ((RecyclerView.o)this).getLeftDecorationWidth(view);
            n2 = ((RecyclerView.o)this).getRightDecorationWidth(view);
        }
        return n + n2;
    }
    
    @Override
    public int getDecorationLengthMainAxis(final View view, int n, int n2) {
        if (this.isMainAxisDirectionHorizontal()) {
            n2 = ((RecyclerView.o)this).getLeftDecorationWidth(view);
            n = ((RecyclerView.o)this).getRightDecorationWidth(view);
        }
        else {
            n2 = ((RecyclerView.o)this).getTopDecorationHeight(view);
            n = ((RecyclerView.o)this).getBottomDecorationHeight(view);
        }
        return n2 + n;
    }
    
    @Override
    public int getFlexDirection() {
        return this.mFlexDirection;
    }
    
    @Override
    public View getFlexItemAt(final int n) {
        final View view = (View)this.mViewCache.get(n);
        if (view != null) {
            return view;
        }
        return this.mRecycler.o(n);
    }
    
    @Override
    public int getFlexItemCount() {
        return this.mState.b();
    }
    
    @Override
    public List<FlexLine> getFlexLines() {
        final ArrayList list = new ArrayList(this.mFlexLines.size());
        for (int size = this.mFlexLines.size(), i = 0; i < size; ++i) {
            final FlexLine flexLine = this.mFlexLines.get(i);
            if (flexLine.getItemCount() != 0) {
                list.add(flexLine);
            }
        }
        return list;
    }
    
    @Override
    public List<FlexLine> getFlexLinesInternal() {
        return this.mFlexLines;
    }
    
    @Override
    public int getFlexWrap() {
        return this.mFlexWrap;
    }
    
    @Override
    public int getJustifyContent() {
        return this.mJustifyContent;
    }
    
    @Override
    public int getLargestMainSize() {
        final int size = this.mFlexLines.size();
        int i = 0;
        if (size == 0) {
            return 0;
        }
        final int size2 = this.mFlexLines.size();
        int max = Integer.MIN_VALUE;
        while (i < size2) {
            max = Math.max(max, this.mFlexLines.get(i).mMainSize);
            ++i;
        }
        return max;
    }
    
    @Override
    public int getMaxLine() {
        return this.mMaxLine;
    }
    
    public int getPositionToFlexLineIndex(final int n) {
        return this.mFlexboxHelper.mIndexToFlexLine[n];
    }
    
    public boolean getRecycleChildrenOnDetach() {
        return this.mRecycleChildrenOnDetach;
    }
    
    @Override
    public View getReorderedFlexItemAt(final int n) {
        return this.getFlexItemAt(n);
    }
    
    @Override
    public int getSumOfCrossSize() {
        final int size = this.mFlexLines.size();
        int i = 0;
        int n = 0;
        while (i < size) {
            n += this.mFlexLines.get(i).mCrossSize;
            ++i;
        }
        return n;
    }
    
    @Override
    public boolean isAutoMeasureEnabled() {
        return true;
    }
    
    public boolean isLayoutRtl() {
        return this.mIsRtl;
    }
    
    @Override
    public boolean isMainAxisDirectionHorizontal() {
        final int mFlexDirection = this.mFlexDirection;
        boolean b = true;
        if (mFlexDirection != 0) {
            b = (mFlexDirection == 1 && b);
        }
        return b;
    }
    
    @Override
    public void onAdapterChanged(final g g, final g g2) {
        ((RecyclerView.o)this).removeAllViews();
    }
    
    @Override
    public void onAttachedToWindow(final RecyclerView recyclerView) {
        super.onAttachedToWindow(recyclerView);
        this.mParent = (View)((View)recyclerView).getParent();
    }
    
    @Override
    public void onDetachedFromWindow(final RecyclerView recyclerView, final v v) {
        super.onDetachedFromWindow(recyclerView, v);
        if (this.mRecycleChildrenOnDetach) {
            ((RecyclerView.o)this).removeAndRecycleAllViews(v);
            v.c();
        }
    }
    
    @Override
    public void onItemsAdded(final RecyclerView recyclerView, final int n, final int n2) {
        super.onItemsAdded(recyclerView, n, n2);
        this.updateDirtyPosition(n);
    }
    
    @Override
    public void onItemsMoved(final RecyclerView recyclerView, final int a, final int b, final int n) {
        super.onItemsMoved(recyclerView, a, b, n);
        this.updateDirtyPosition(Math.min(a, b));
    }
    
    @Override
    public void onItemsRemoved(final RecyclerView recyclerView, final int n, final int n2) {
        super.onItemsRemoved(recyclerView, n, n2);
        this.updateDirtyPosition(n);
    }
    
    @Override
    public void onItemsUpdated(final RecyclerView recyclerView, final int n, final int n2) {
        super.onItemsUpdated(recyclerView, n, n2);
        this.updateDirtyPosition(n);
    }
    
    @Override
    public void onItemsUpdated(final RecyclerView recyclerView, final int n, final int n2, final Object o) {
        super.onItemsUpdated(recyclerView, n, n2, o);
        this.updateDirtyPosition(n);
    }
    
    @Override
    public void onLayoutChildren(final v mRecycler, final a0 mState) {
        this.mRecycler = mRecycler;
        this.mState = mState;
        final int b = mState.b();
        if (b == 0 && mState.e()) {
            return;
        }
        this.resolveLayoutDirection();
        this.ensureOrientationHelper();
        this.ensureLayoutState();
        this.mFlexboxHelper.ensureMeasureSpecCache(b);
        this.mFlexboxHelper.ensureMeasuredSizeCache(b);
        this.mFlexboxHelper.ensureIndexToFlexLine(b);
        LayoutState.access$502(this.mLayoutState, false);
        final SavedState mPendingSavedState = this.mPendingSavedState;
        if (mPendingSavedState != null && mPendingSavedState.hasValidAnchor(b)) {
            this.mPendingScrollPosition = SavedState.access$200(this.mPendingSavedState);
        }
        if (!AnchorInfo.access$700(this.mAnchorInfo) || this.mPendingScrollPosition != -1 || this.mPendingSavedState != null) {
            this.mAnchorInfo.reset();
            this.updateAnchorInfoForLayout(mState, this.mAnchorInfo);
            AnchorInfo.access$702(this.mAnchorInfo, true);
        }
        ((RecyclerView.o)this).detachAndScrapAttachedViews(mRecycler);
        if (AnchorInfo.access$900(this.mAnchorInfo)) {
            this.updateLayoutStateToFillStart(this.mAnchorInfo, false, true);
        }
        else {
            this.updateLayoutStateToFillEnd(this.mAnchorInfo, false, true);
        }
        this.updateFlexLines(b);
        this.fill(mRecycler, mState, this.mLayoutState);
        int n;
        int n2;
        if (AnchorInfo.access$900(this.mAnchorInfo)) {
            n = LayoutState.access$1000(this.mLayoutState);
            this.updateLayoutStateToFillEnd(this.mAnchorInfo, true, false);
            this.fill(mRecycler, mState, this.mLayoutState);
            n2 = LayoutState.access$1000(this.mLayoutState);
        }
        else {
            n2 = LayoutState.access$1000(this.mLayoutState);
            this.updateLayoutStateToFillStart(this.mAnchorInfo, true, false);
            this.fill(mRecycler, mState, this.mLayoutState);
            n = LayoutState.access$1000(this.mLayoutState);
        }
        if (((RecyclerView.o)this).getChildCount() > 0) {
            if (AnchorInfo.access$900(this.mAnchorInfo)) {
                this.fixLayoutStartGap(n + this.fixLayoutEndGap(n2, mRecycler, mState, true), mRecycler, mState, false);
            }
            else {
                this.fixLayoutEndGap(n2 + this.fixLayoutStartGap(n, mRecycler, mState, true), mRecycler, mState, false);
            }
        }
    }
    
    @Override
    public void onLayoutCompleted(final a0 a0) {
        super.onLayoutCompleted(a0);
        this.mPendingSavedState = null;
        this.mPendingScrollPosition = -1;
        this.mPendingScrollPositionOffset = Integer.MIN_VALUE;
        this.mDirtyPosition = -1;
        this.mAnchorInfo.reset();
        this.mViewCache.clear();
    }
    
    @Override
    public void onNewFlexItemAdded(final View view, int n, int n2, final FlexLine flexLine) {
        ((RecyclerView.o)this).calculateItemDecorationsForChild(view, FlexboxLayoutManager.TEMP_RECT);
        if (this.isMainAxisDirectionHorizontal()) {
            n2 = ((RecyclerView.o)this).getLeftDecorationWidth(view);
            n = ((RecyclerView.o)this).getRightDecorationWidth(view);
        }
        else {
            n2 = ((RecyclerView.o)this).getTopDecorationHeight(view);
            n = ((RecyclerView.o)this).getBottomDecorationHeight(view);
        }
        n += n2;
        flexLine.mMainSize += n;
        flexLine.mDividerLengthInMainSize += n;
    }
    
    @Override
    public void onNewFlexLineAdded(final FlexLine flexLine) {
    }
    
    @Override
    public void onRestoreInstanceState(final Parcelable parcelable) {
        if (parcelable instanceof SavedState) {
            this.mPendingSavedState = (SavedState)parcelable;
            ((RecyclerView.o)this).requestLayout();
        }
    }
    
    @Override
    public Parcelable onSaveInstanceState() {
        if (this.mPendingSavedState != null) {
            return (Parcelable)new SavedState(this.mPendingSavedState, null);
        }
        final SavedState savedState = new SavedState();
        if (((RecyclerView.o)this).getChildCount() > 0) {
            final View childClosestToStart = this.getChildClosestToStart();
            SavedState.access$202(savedState, ((RecyclerView.o)this).getPosition(childClosestToStart));
            SavedState.access$302(savedState, this.mOrientationHelper.g(childClosestToStart) - this.mOrientationHelper.m());
        }
        else {
            savedState.invalidateAnchor();
        }
        return (Parcelable)savedState;
    }
    
    @Override
    public int scrollHorizontallyBy(int n, final v v, final a0 a0) {
        if (this.isMainAxisDirectionHorizontal() && this.mFlexWrap != 0) {
            n = this.handleScrollingSubOrientation(n);
            AnchorInfo.access$2412(this.mAnchorInfo, n);
            this.mSubOrientationHelper.r(-n);
            return n;
        }
        n = this.handleScrollingMainOrientation(n, v, a0);
        this.mViewCache.clear();
        return n;
    }
    
    @Override
    public void scrollToPosition(final int mPendingScrollPosition) {
        this.mPendingScrollPosition = mPendingScrollPosition;
        this.mPendingScrollPositionOffset = Integer.MIN_VALUE;
        final SavedState mPendingSavedState = this.mPendingSavedState;
        if (mPendingSavedState != null) {
            mPendingSavedState.invalidateAnchor();
        }
        ((RecyclerView.o)this).requestLayout();
    }
    
    @Override
    public int scrollVerticallyBy(int n, final v v, final a0 a0) {
        if (!this.isMainAxisDirectionHorizontal() && (this.mFlexWrap != 0 || this.isMainAxisDirectionHorizontal())) {
            n = this.handleScrollingSubOrientation(n);
            AnchorInfo.access$2412(this.mAnchorInfo, n);
            this.mSubOrientationHelper.r(-n);
            return n;
        }
        n = this.handleScrollingMainOrientation(n, v, a0);
        this.mViewCache.clear();
        return n;
    }
    
    @Override
    public void setAlignContent(final int n) {
        throw new UnsupportedOperationException("Setting the alignContent in the FlexboxLayoutManager is not supported. Use FlexboxLayout if you need to use this attribute.");
    }
    
    @Override
    public void setAlignItems(final int mAlignItems) {
        final int mAlignItems2 = this.mAlignItems;
        if (mAlignItems2 != mAlignItems) {
            if (mAlignItems2 == 4 || mAlignItems == 4) {
                ((RecyclerView.o)this).removeAllViews();
                this.clearFlexLines();
            }
            this.mAlignItems = mAlignItems;
            ((RecyclerView.o)this).requestLayout();
        }
    }
    
    @Override
    public void setFlexDirection(final int mFlexDirection) {
        if (this.mFlexDirection != mFlexDirection) {
            ((RecyclerView.o)this).removeAllViews();
            this.mFlexDirection = mFlexDirection;
            this.mOrientationHelper = null;
            this.mSubOrientationHelper = null;
            this.clearFlexLines();
            ((RecyclerView.o)this).requestLayout();
        }
    }
    
    @Override
    public void setFlexLines(final List<FlexLine> mFlexLines) {
        this.mFlexLines = mFlexLines;
    }
    
    @Override
    public void setFlexWrap(final int mFlexWrap) {
        if (mFlexWrap != 2) {
            final int mFlexWrap2 = this.mFlexWrap;
            if (mFlexWrap2 != mFlexWrap) {
                if (mFlexWrap2 == 0 || mFlexWrap == 0) {
                    ((RecyclerView.o)this).removeAllViews();
                    this.clearFlexLines();
                }
                this.mFlexWrap = mFlexWrap;
                this.mOrientationHelper = null;
                this.mSubOrientationHelper = null;
                ((RecyclerView.o)this).requestLayout();
            }
            return;
        }
        throw new UnsupportedOperationException("wrap_reverse is not supported in FlexboxLayoutManager");
    }
    
    @Override
    public void setJustifyContent(final int mJustifyContent) {
        if (this.mJustifyContent != mJustifyContent) {
            this.mJustifyContent = mJustifyContent;
            ((RecyclerView.o)this).requestLayout();
        }
    }
    
    @Override
    public void setMaxLine(final int mMaxLine) {
        if (this.mMaxLine != mMaxLine) {
            this.mMaxLine = mMaxLine;
            ((RecyclerView.o)this).requestLayout();
        }
    }
    
    public void setRecycleChildrenOnDetach(final boolean mRecycleChildrenOnDetach) {
        this.mRecycleChildrenOnDetach = mRecycleChildrenOnDetach;
    }
    
    @Override
    public void smoothScrollToPosition(final RecyclerView recyclerView, final a0 a0, final int targetPosition) {
        final androidx.recyclerview.widget.g g = new androidx.recyclerview.widget.g(((View)recyclerView).getContext());
        ((RecyclerView.z)g).setTargetPosition(targetPosition);
        ((RecyclerView.o)this).startSmoothScroll(g);
    }
    
    @Override
    public void updateViewCache(final int n, final View view) {
        this.mViewCache.put(n, (Object)view);
    }
    
    public class AnchorInfo
    {
        static final boolean $assertionsDisabled = false;
        private boolean mAssignedFromSavedState;
        private int mCoordinate;
        private int mFlexLinePosition;
        private boolean mLayoutFromEnd;
        private int mPerpendicularCoordinate;
        private int mPosition;
        private boolean mValid;
        final FlexboxLayoutManager this$0;
        
        private AnchorInfo(final FlexboxLayoutManager this$0) {
            this.this$0 = this$0;
            this.mPerpendicularCoordinate = 0;
        }
        
        public static /* synthetic */ int access$1300(final AnchorInfo anchorInfo) {
            return anchorInfo.mPosition;
        }
        
        public static /* synthetic */ int access$1302(final AnchorInfo anchorInfo, final int mPosition) {
            return anchorInfo.mPosition = mPosition;
        }
        
        public static /* synthetic */ int access$1400(final AnchorInfo anchorInfo) {
            return anchorInfo.mFlexLinePosition;
        }
        
        public static /* synthetic */ int access$1402(final AnchorInfo anchorInfo, final int mFlexLinePosition) {
            return anchorInfo.mFlexLinePosition = mFlexLinePosition;
        }
        
        public static /* synthetic */ int access$1700(final AnchorInfo anchorInfo) {
            return anchorInfo.mCoordinate;
        }
        
        public static /* synthetic */ int access$1702(final AnchorInfo anchorInfo, final int mCoordinate) {
            return anchorInfo.mCoordinate = mCoordinate;
        }
        
        public static /* synthetic */ boolean access$1802(final AnchorInfo anchorInfo, final boolean mAssignedFromSavedState) {
            return anchorInfo.mAssignedFromSavedState = mAssignedFromSavedState;
        }
        
        public static /* synthetic */ int access$2400(final AnchorInfo anchorInfo) {
            return anchorInfo.mPerpendicularCoordinate;
        }
        
        public static /* synthetic */ int access$2402(final AnchorInfo anchorInfo, final int mPerpendicularCoordinate) {
            return anchorInfo.mPerpendicularCoordinate = mPerpendicularCoordinate;
        }
        
        public static /* synthetic */ int access$2412(final AnchorInfo anchorInfo, int mPerpendicularCoordinate) {
            mPerpendicularCoordinate += anchorInfo.mPerpendicularCoordinate;
            return anchorInfo.mPerpendicularCoordinate = mPerpendicularCoordinate;
        }
        
        public static /* synthetic */ boolean access$700(final AnchorInfo anchorInfo) {
            return anchorInfo.mValid;
        }
        
        public static /* synthetic */ boolean access$702(final AnchorInfo anchorInfo, final boolean mValid) {
            return anchorInfo.mValid = mValid;
        }
        
        public static /* synthetic */ boolean access$900(final AnchorInfo anchorInfo) {
            return anchorInfo.mLayoutFromEnd;
        }
        
        public static /* synthetic */ boolean access$902(final AnchorInfo anchorInfo, final boolean mLayoutFromEnd) {
            return anchorInfo.mLayoutFromEnd = mLayoutFromEnd;
        }
        
        private void assignCoordinateFromPadding() {
            int mCoordinate = 0;
            Label_0084: {
                if (!this.this$0.isMainAxisDirectionHorizontal() && FlexboxLayoutManager.access$3000(this.this$0)) {
                    if (!this.mLayoutFromEnd) {
                        mCoordinate = ((RecyclerView.o)this.this$0).getWidth() - FlexboxLayoutManager.access$3100(this.this$0).m();
                        break Label_0084;
                    }
                }
                else if (!this.mLayoutFromEnd) {
                    mCoordinate = FlexboxLayoutManager.access$3100(this.this$0).m();
                    break Label_0084;
                }
                mCoordinate = FlexboxLayoutManager.access$3100(this.this$0).i();
            }
            this.mCoordinate = mCoordinate;
        }
        
        private void assignFromView(final View view) {
            androidx.recyclerview.widget.i i;
            if (FlexboxLayoutManager.access$2800(this.this$0) == 0) {
                i = FlexboxLayoutManager.access$3200(this.this$0);
            }
            else {
                i = FlexboxLayoutManager.access$3100(this.this$0);
            }
            Label_0118: {
                int mCoordinate = 0;
                Label_0113: {
                    int n;
                    if (!this.this$0.isMainAxisDirectionHorizontal() && FlexboxLayoutManager.access$3000(this.this$0)) {
                        if (!this.mLayoutFromEnd) {
                            mCoordinate = i.d(view);
                            break Label_0113;
                        }
                        n = i.g(view);
                    }
                    else {
                        if (!this.mLayoutFromEnd) {
                            mCoordinate = i.g(view);
                            break Label_0113;
                        }
                        n = i.d(view);
                    }
                    this.mCoordinate = n + i.o();
                    break Label_0118;
                }
                this.mCoordinate = mCoordinate;
            }
            this.mPosition = ((RecyclerView.o)this.this$0).getPosition(view);
            final int n2 = 0;
            this.mAssignedFromSavedState = false;
            final int[] mIndexToFlexLine = FlexboxLayoutManager.access$3300(this.this$0).mIndexToFlexLine;
            int mPosition = this.mPosition;
            if (mPosition == -1) {
                mPosition = 0;
            }
            final int n3 = mIndexToFlexLine[mPosition];
            int mFlexLinePosition = n2;
            if (n3 != -1) {
                mFlexLinePosition = n3;
            }
            this.mFlexLinePosition = mFlexLinePosition;
            if (FlexboxLayoutManager.access$3400(this.this$0).size() > this.mFlexLinePosition) {
                this.mPosition = FlexboxLayoutManager.access$3400(this.this$0).get(this.mFlexLinePosition).mFirstIndex;
            }
        }
        
        private void reset() {
            this.mPosition = -1;
            this.mFlexLinePosition = -1;
            this.mCoordinate = Integer.MIN_VALUE;
            boolean mLayoutFromEnd = false;
            this.mValid = false;
            this.mAssignedFromSavedState = false;
            Label_0113: {
                if (this.this$0.isMainAxisDirectionHorizontal()) {
                    if (FlexboxLayoutManager.access$2800(this.this$0) == 0) {
                        if (FlexboxLayoutManager.access$2900(this.this$0) != 1) {
                            break Label_0113;
                        }
                    }
                    else if (FlexboxLayoutManager.access$2800(this.this$0) != 2) {
                        break Label_0113;
                    }
                }
                else if (FlexboxLayoutManager.access$2800(this.this$0) == 0) {
                    if (FlexboxLayoutManager.access$2900(this.this$0) != 3) {
                        break Label_0113;
                    }
                }
                else if (FlexboxLayoutManager.access$2800(this.this$0) != 2) {
                    break Label_0113;
                }
                mLayoutFromEnd = true;
            }
            this.mLayoutFromEnd = mLayoutFromEnd;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("AnchorInfo{mPosition=");
            sb.append(this.mPosition);
            sb.append(", mFlexLinePosition=");
            sb.append(this.mFlexLinePosition);
            sb.append(", mCoordinate=");
            sb.append(this.mCoordinate);
            sb.append(", mPerpendicularCoordinate=");
            sb.append(this.mPerpendicularCoordinate);
            sb.append(", mLayoutFromEnd=");
            sb.append(this.mLayoutFromEnd);
            sb.append(", mValid=");
            sb.append(this.mValid);
            sb.append(", mAssignedFromSavedState=");
            sb.append(this.mAssignedFromSavedState);
            sb.append('}');
            return sb.toString();
        }
    }
    
    public static class LayoutParams extends p implements FlexItem
    {
        public static final Parcelable$Creator<LayoutParams> CREATOR;
        private int mAlignSelf;
        private float mFlexBasisPercent;
        private float mFlexGrow;
        private float mFlexShrink;
        private int mMaxHeight;
        private int mMaxWidth;
        private int mMinHeight;
        private int mMinWidth;
        private boolean mWrapBefore;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$Creator<LayoutParams>() {
                public LayoutParams createFromParcel(final Parcel parcel) {
                    return new LayoutParams(parcel);
                }
                
                public LayoutParams[] newArray(final int n) {
                    return new LayoutParams[n];
                }
            };
        }
        
        public LayoutParams(final int n, final int n2) {
            super(n, n2);
            this.mFlexGrow = 0.0f;
            this.mFlexShrink = 1.0f;
            this.mAlignSelf = -1;
            this.mFlexBasisPercent = -1.0f;
            this.mMaxWidth = 16777215;
            this.mMaxHeight = 16777215;
        }
        
        public LayoutParams(final Context context, final AttributeSet set) {
            super(context, set);
            this.mFlexGrow = 0.0f;
            this.mFlexShrink = 1.0f;
            this.mAlignSelf = -1;
            this.mFlexBasisPercent = -1.0f;
            this.mMaxWidth = 16777215;
            this.mMaxHeight = 16777215;
        }
        
        public LayoutParams(final Parcel parcel) {
            super(-2, -2);
            this.mFlexGrow = 0.0f;
            this.mFlexShrink = 1.0f;
            this.mAlignSelf = -1;
            this.mFlexBasisPercent = -1.0f;
            this.mMaxWidth = 16777215;
            this.mMaxHeight = 16777215;
            this.mFlexGrow = parcel.readFloat();
            this.mFlexShrink = parcel.readFloat();
            this.mAlignSelf = parcel.readInt();
            this.mFlexBasisPercent = parcel.readFloat();
            this.mMinWidth = parcel.readInt();
            this.mMinHeight = parcel.readInt();
            this.mMaxWidth = parcel.readInt();
            this.mMaxHeight = parcel.readInt();
            this.mWrapBefore = (parcel.readByte() != 0);
            super.bottomMargin = parcel.readInt();
            super.leftMargin = parcel.readInt();
            super.rightMargin = parcel.readInt();
            super.topMargin = parcel.readInt();
            super.height = parcel.readInt();
            super.width = parcel.readInt();
        }
        
        public LayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
            super(viewGroup$LayoutParams);
            this.mFlexGrow = 0.0f;
            this.mFlexShrink = 1.0f;
            this.mAlignSelf = -1;
            this.mFlexBasisPercent = -1.0f;
            this.mMaxWidth = 16777215;
            this.mMaxHeight = 16777215;
        }
        
        public LayoutParams(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
            super(viewGroup$MarginLayoutParams);
            this.mFlexGrow = 0.0f;
            this.mFlexShrink = 1.0f;
            this.mAlignSelf = -1;
            this.mFlexBasisPercent = -1.0f;
            this.mMaxWidth = 16777215;
            this.mMaxHeight = 16777215;
        }
        
        public LayoutParams(final p p) {
            super(p);
            this.mFlexGrow = 0.0f;
            this.mFlexShrink = 1.0f;
            this.mAlignSelf = -1;
            this.mFlexBasisPercent = -1.0f;
            this.mMaxWidth = 16777215;
            this.mMaxHeight = 16777215;
        }
        
        public LayoutParams(final LayoutParams layoutParams) {
            super((RecyclerView.p)layoutParams);
            this.mFlexGrow = 0.0f;
            this.mFlexShrink = 1.0f;
            this.mAlignSelf = -1;
            this.mFlexBasisPercent = -1.0f;
            this.mMaxWidth = 16777215;
            this.mMaxHeight = 16777215;
            this.mFlexGrow = layoutParams.mFlexGrow;
            this.mFlexShrink = layoutParams.mFlexShrink;
            this.mAlignSelf = layoutParams.mAlignSelf;
            this.mFlexBasisPercent = layoutParams.mFlexBasisPercent;
            this.mMinWidth = layoutParams.mMinWidth;
            this.mMinHeight = layoutParams.mMinHeight;
            this.mMaxWidth = layoutParams.mMaxWidth;
            this.mMaxHeight = layoutParams.mMaxHeight;
            this.mWrapBefore = layoutParams.mWrapBefore;
        }
        
        public int describeContents() {
            return 0;
        }
        
        @Override
        public int getAlignSelf() {
            return this.mAlignSelf;
        }
        
        @Override
        public float getFlexBasisPercent() {
            return this.mFlexBasisPercent;
        }
        
        @Override
        public float getFlexGrow() {
            return this.mFlexGrow;
        }
        
        @Override
        public float getFlexShrink() {
            return this.mFlexShrink;
        }
        
        @Override
        public int getHeight() {
            return super.height;
        }
        
        @Override
        public int getMarginBottom() {
            return super.bottomMargin;
        }
        
        @Override
        public int getMarginLeft() {
            return super.leftMargin;
        }
        
        @Override
        public int getMarginRight() {
            return super.rightMargin;
        }
        
        @Override
        public int getMarginTop() {
            return super.topMargin;
        }
        
        @Override
        public int getMaxHeight() {
            return this.mMaxHeight;
        }
        
        @Override
        public int getMaxWidth() {
            return this.mMaxWidth;
        }
        
        @Override
        public int getMinHeight() {
            return this.mMinHeight;
        }
        
        @Override
        public int getMinWidth() {
            return this.mMinWidth;
        }
        
        @Override
        public int getOrder() {
            return 1;
        }
        
        @Override
        public int getWidth() {
            return super.width;
        }
        
        @Override
        public boolean isWrapBefore() {
            return this.mWrapBefore;
        }
        
        @Override
        public void setAlignSelf(final int mAlignSelf) {
            this.mAlignSelf = mAlignSelf;
        }
        
        @Override
        public void setFlexBasisPercent(final float mFlexBasisPercent) {
            this.mFlexBasisPercent = mFlexBasisPercent;
        }
        
        @Override
        public void setFlexGrow(final float mFlexGrow) {
            this.mFlexGrow = mFlexGrow;
        }
        
        @Override
        public void setFlexShrink(final float mFlexShrink) {
            this.mFlexShrink = mFlexShrink;
        }
        
        @Override
        public void setHeight(final int height) {
            super.height = height;
        }
        
        @Override
        public void setMaxHeight(final int mMaxHeight) {
            this.mMaxHeight = mMaxHeight;
        }
        
        @Override
        public void setMaxWidth(final int mMaxWidth) {
            this.mMaxWidth = mMaxWidth;
        }
        
        @Override
        public void setMinHeight(final int mMinHeight) {
            this.mMinHeight = mMinHeight;
        }
        
        @Override
        public void setMinWidth(final int mMinWidth) {
            this.mMinWidth = mMinWidth;
        }
        
        @Override
        public void setOrder(final int n) {
            throw new UnsupportedOperationException("Setting the order in the FlexboxLayoutManager is not supported. Use FlexboxLayout if you need to reorder using the attribute.");
        }
        
        @Override
        public void setWidth(final int width) {
            super.width = width;
        }
        
        @Override
        public void setWrapBefore(final boolean mWrapBefore) {
            this.mWrapBefore = mWrapBefore;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            parcel.writeFloat(this.mFlexGrow);
            parcel.writeFloat(this.mFlexShrink);
            parcel.writeInt(this.mAlignSelf);
            parcel.writeFloat(this.mFlexBasisPercent);
            parcel.writeInt(this.mMinWidth);
            parcel.writeInt(this.mMinHeight);
            parcel.writeInt(this.mMaxWidth);
            parcel.writeInt(this.mMaxHeight);
            parcel.writeByte((byte)(byte)(this.mWrapBefore ? 1 : 0));
            parcel.writeInt(super.bottomMargin);
            parcel.writeInt(super.leftMargin);
            parcel.writeInt(super.rightMargin);
            parcel.writeInt(super.topMargin);
            parcel.writeInt(super.height);
            parcel.writeInt(super.width);
        }
    }
    
    public static class LayoutState
    {
        private static final int ITEM_DIRECTION_TAIL = 1;
        private static final int LAYOUT_END = 1;
        private static final int LAYOUT_START = -1;
        private static final int SCROLLING_OFFSET_NaN = Integer.MIN_VALUE;
        private int mAvailable;
        private int mFlexLinePosition;
        private boolean mInfinite;
        private int mItemDirection;
        private int mLastScrollDelta;
        private int mLayoutDirection;
        private int mOffset;
        private int mPosition;
        private int mScrollingOffset;
        private boolean mShouldRecycle;
        
        private LayoutState() {
            this.mItemDirection = 1;
            this.mLayoutDirection = 1;
        }
        
        public static /* synthetic */ int access$1000(final LayoutState layoutState) {
            return layoutState.mOffset;
        }
        
        public static /* synthetic */ int access$1002(final LayoutState layoutState, final int mOffset) {
            return layoutState.mOffset = mOffset;
        }
        
        public static /* synthetic */ int access$1012(final LayoutState layoutState, int mOffset) {
            mOffset += layoutState.mOffset;
            return layoutState.mOffset = mOffset;
        }
        
        public static /* synthetic */ int access$1020(final LayoutState layoutState, int mOffset) {
            mOffset = layoutState.mOffset - mOffset;
            return layoutState.mOffset = mOffset;
        }
        
        public static /* synthetic */ boolean access$1100(final LayoutState layoutState) {
            return layoutState.mInfinite;
        }
        
        public static /* synthetic */ boolean access$1102(final LayoutState layoutState, final boolean mInfinite) {
            return layoutState.mInfinite = mInfinite;
        }
        
        public static /* synthetic */ int access$1200(final LayoutState layoutState) {
            return layoutState.mAvailable;
        }
        
        public static /* synthetic */ int access$1202(final LayoutState layoutState, final int mAvailable) {
            return layoutState.mAvailable = mAvailable;
        }
        
        public static /* synthetic */ int access$1220(final LayoutState layoutState, int mAvailable) {
            mAvailable = layoutState.mAvailable - mAvailable;
            return layoutState.mAvailable = mAvailable;
        }
        
        public static /* synthetic */ int access$1500(final LayoutState layoutState) {
            return layoutState.mFlexLinePosition;
        }
        
        public static /* synthetic */ int access$1502(final LayoutState layoutState, final int mFlexLinePosition) {
            return layoutState.mFlexLinePosition = mFlexLinePosition;
        }
        
        public static /* synthetic */ int access$1508(final LayoutState layoutState) {
            return layoutState.mFlexLinePosition++;
        }
        
        public static /* synthetic */ int access$1510(final LayoutState layoutState) {
            return layoutState.mFlexLinePosition--;
        }
        
        public static /* synthetic */ int access$1512(final LayoutState layoutState, int mFlexLinePosition) {
            mFlexLinePosition += layoutState.mFlexLinePosition;
            return layoutState.mFlexLinePosition = mFlexLinePosition;
        }
        
        public static /* synthetic */ int access$2000(final LayoutState layoutState) {
            return layoutState.mScrollingOffset;
        }
        
        public static /* synthetic */ int access$2002(final LayoutState layoutState, final int mScrollingOffset) {
            return layoutState.mScrollingOffset = mScrollingOffset;
        }
        
        public static /* synthetic */ int access$2012(final LayoutState layoutState, int mScrollingOffset) {
            mScrollingOffset += layoutState.mScrollingOffset;
            return layoutState.mScrollingOffset = mScrollingOffset;
        }
        
        public static /* synthetic */ int access$2200(final LayoutState layoutState) {
            return layoutState.mPosition;
        }
        
        public static /* synthetic */ int access$2202(final LayoutState layoutState, final int mPosition) {
            return layoutState.mPosition = mPosition;
        }
        
        public static /* synthetic */ int access$2212(final LayoutState layoutState, int mPosition) {
            mPosition += layoutState.mPosition;
            return layoutState.mPosition = mPosition;
        }
        
        public static /* synthetic */ int access$2220(final LayoutState layoutState, int mPosition) {
            mPosition = layoutState.mPosition - mPosition;
            return layoutState.mPosition = mPosition;
        }
        
        public static /* synthetic */ int access$2300(final LayoutState layoutState) {
            return layoutState.mLayoutDirection;
        }
        
        public static /* synthetic */ int access$2302(final LayoutState layoutState, final int mLayoutDirection) {
            return layoutState.mLayoutDirection = mLayoutDirection;
        }
        
        public static /* synthetic */ int access$2500(final LayoutState layoutState) {
            return layoutState.mItemDirection;
        }
        
        public static /* synthetic */ int access$2502(final LayoutState layoutState, final int mItemDirection) {
            return layoutState.mItemDirection = mItemDirection;
        }
        
        public static /* synthetic */ int access$2702(final LayoutState layoutState, final int mLastScrollDelta) {
            return layoutState.mLastScrollDelta = mLastScrollDelta;
        }
        
        public static /* synthetic */ boolean access$500(final LayoutState layoutState) {
            return layoutState.mShouldRecycle;
        }
        
        public static /* synthetic */ boolean access$502(final LayoutState layoutState, final boolean mShouldRecycle) {
            return layoutState.mShouldRecycle = mShouldRecycle;
        }
        
        private boolean hasMore(final a0 a0, final List<FlexLine> list) {
            final int mPosition = this.mPosition;
            if (mPosition >= 0 && mPosition < a0.b()) {
                final int mFlexLinePosition = this.mFlexLinePosition;
                if (mFlexLinePosition >= 0 && mFlexLinePosition < list.size()) {
                    return true;
                }
            }
            return false;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("LayoutState{mAvailable=");
            sb.append(this.mAvailable);
            sb.append(", mFlexLinePosition=");
            sb.append(this.mFlexLinePosition);
            sb.append(", mPosition=");
            sb.append(this.mPosition);
            sb.append(", mOffset=");
            sb.append(this.mOffset);
            sb.append(", mScrollingOffset=");
            sb.append(this.mScrollingOffset);
            sb.append(", mLastScrollDelta=");
            sb.append(this.mLastScrollDelta);
            sb.append(", mItemDirection=");
            sb.append(this.mItemDirection);
            sb.append(", mLayoutDirection=");
            sb.append(this.mLayoutDirection);
            sb.append('}');
            return sb.toString();
        }
    }
    
    public static class SavedState implements Parcelable
    {
        public static final Parcelable$Creator<SavedState> CREATOR;
        private int mAnchorOffset;
        private int mAnchorPosition;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$Creator<SavedState>() {
                public SavedState createFromParcel(final Parcel parcel) {
                    return new SavedState(parcel, null);
                }
                
                public SavedState[] newArray(final int n) {
                    return new SavedState[n];
                }
            };
        }
        
        public SavedState() {
        }
        
        private SavedState(final Parcel parcel) {
            this.mAnchorPosition = parcel.readInt();
            this.mAnchorOffset = parcel.readInt();
        }
        
        private SavedState(final SavedState savedState) {
            this.mAnchorPosition = savedState.mAnchorPosition;
            this.mAnchorOffset = savedState.mAnchorOffset;
        }
        
        public static /* synthetic */ int access$200(final SavedState savedState) {
            return savedState.mAnchorPosition;
        }
        
        public static /* synthetic */ int access$202(final SavedState savedState, final int mAnchorPosition) {
            return savedState.mAnchorPosition = mAnchorPosition;
        }
        
        public static /* synthetic */ int access$300(final SavedState savedState) {
            return savedState.mAnchorOffset;
        }
        
        public static /* synthetic */ int access$302(final SavedState savedState, final int mAnchorOffset) {
            return savedState.mAnchorOffset = mAnchorOffset;
        }
        
        private boolean hasValidAnchor(final int n) {
            final int mAnchorPosition = this.mAnchorPosition;
            return mAnchorPosition >= 0 && mAnchorPosition < n;
        }
        
        private void invalidateAnchor() {
            this.mAnchorPosition = -1;
        }
        
        public int describeContents() {
            return 0;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("SavedState{mAnchorPosition=");
            sb.append(this.mAnchorPosition);
            sb.append(", mAnchorOffset=");
            sb.append(this.mAnchorOffset);
            sb.append('}');
            return sb.toString();
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            parcel.writeInt(this.mAnchorPosition);
            parcel.writeInt(this.mAnchorOffset);
        }
    }
}

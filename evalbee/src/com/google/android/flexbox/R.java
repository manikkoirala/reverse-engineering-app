// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.flexbox;

public final class R
{
    private R() {
    }
    
    public static final class attr
    {
        public static final int alignContent = 2130968624;
        public static final int alignItems = 2130968625;
        public static final int alpha = 2130968627;
        public static final int dividerDrawable = 2130968966;
        public static final int dividerDrawableHorizontal = 2130968967;
        public static final int dividerDrawableVertical = 2130968968;
        public static final int fastScrollEnabled = 2130969054;
        public static final int fastScrollHorizontalThumbDrawable = 2130969055;
        public static final int fastScrollHorizontalTrackDrawable = 2130969056;
        public static final int fastScrollVerticalThumbDrawable = 2130969057;
        public static final int fastScrollVerticalTrackDrawable = 2130969058;
        public static final int flexDirection = 2130969060;
        public static final int flexWrap = 2130969061;
        public static final int font = 2130969096;
        public static final int fontProviderAuthority = 2130969098;
        public static final int fontProviderCerts = 2130969099;
        public static final int fontProviderFetchStrategy = 2130969100;
        public static final int fontProviderFetchTimeout = 2130969101;
        public static final int fontProviderPackage = 2130969102;
        public static final int fontProviderQuery = 2130969103;
        public static final int fontStyle = 2130969105;
        public static final int fontVariationSettings = 2130969106;
        public static final int fontWeight = 2130969107;
        public static final int justifyContent = 2130969200;
        public static final int layoutManager = 2130969214;
        public static final int layout_alignSelf = 2130969215;
        public static final int layout_flexBasisPercent = 2130969270;
        public static final int layout_flexGrow = 2130969271;
        public static final int layout_flexShrink = 2130969272;
        public static final int layout_maxHeight = 2130969283;
        public static final int layout_maxWidth = 2130969284;
        public static final int layout_minHeight = 2130969285;
        public static final int layout_minWidth = 2130969286;
        public static final int layout_order = 2130969288;
        public static final int layout_wrapBefore = 2130969292;
        public static final int maxLine = 2130969377;
        public static final int recyclerViewStyle = 2130969522;
        public static final int reverseLayout = 2130969528;
        public static final int showDivider = 2130969565;
        public static final int showDividerHorizontal = 2130969566;
        public static final int showDividerVertical = 2130969567;
        public static final int spanCount = 2130969588;
        public static final int stackFromEnd = 2130969599;
        public static final int ttcIndex = 2130969814;
        
        private attr() {
        }
    }
    
    public static final class color
    {
        public static final int androidx_core_ripple_material_light = 2131099675;
        public static final int androidx_core_secondary_text_default_material_light = 2131099676;
        public static final int notification_action_color_filter = 2131100452;
        public static final int notification_icon_bg_color = 2131100453;
        public static final int ripple_material_light = 2131100464;
        public static final int secondary_text_default_material_light = 2131100466;
        
        private color() {
        }
    }
    
    public static final class dimen
    {
        public static final int compat_button_inset_horizontal_material = 2131165272;
        public static final int compat_button_inset_vertical_material = 2131165273;
        public static final int compat_button_padding_horizontal_material = 2131165274;
        public static final int compat_button_padding_vertical_material = 2131165275;
        public static final int compat_control_corner_material = 2131165276;
        public static final int compat_notification_large_icon_max_height = 2131165277;
        public static final int compat_notification_large_icon_max_width = 2131165278;
        public static final int fastscroll_default_thickness = 2131165330;
        public static final int fastscroll_margin = 2131165331;
        public static final int fastscroll_minimum_range = 2131165332;
        public static final int item_touch_helper_max_drag_scroll_per_frame = 2131165340;
        public static final int item_touch_helper_swipe_escape_max_velocity = 2131165341;
        public static final int item_touch_helper_swipe_escape_velocity = 2131165342;
        public static final int notification_action_icon_size = 2131165959;
        public static final int notification_action_text_size = 2131165960;
        public static final int notification_big_circle_margin = 2131165961;
        public static final int notification_content_margin_start = 2131165962;
        public static final int notification_large_icon_height = 2131165963;
        public static final int notification_large_icon_width = 2131165964;
        public static final int notification_main_column_padding_top = 2131165965;
        public static final int notification_media_narrow_margin = 2131165966;
        public static final int notification_right_icon_size = 2131165967;
        public static final int notification_right_side_padding_top = 2131165968;
        public static final int notification_small_icon_background_padding = 2131165969;
        public static final int notification_small_icon_size_as_large = 2131165970;
        public static final int notification_subtext_size = 2131165971;
        public static final int notification_top_pad = 2131165972;
        public static final int notification_top_pad_large_text = 2131165973;
        
        private dimen() {
        }
    }
    
    public static final class drawable
    {
        public static final int notification_action_background = 2131231070;
        public static final int notification_bg = 2131231071;
        public static final int notification_bg_low = 2131231072;
        public static final int notification_bg_low_normal = 2131231073;
        public static final int notification_bg_low_pressed = 2131231074;
        public static final int notification_bg_normal = 2131231075;
        public static final int notification_bg_normal_pressed = 2131231076;
        public static final int notification_icon_background = 2131231077;
        public static final int notification_template_icon_bg = 2131231078;
        public static final int notification_template_icon_low_bg = 2131231079;
        public static final int notification_tile_bg = 2131231080;
        public static final int notify_panel_notification_icon_bg = 2131231081;
        
        private drawable() {
        }
    }
    
    public static final class id
    {
        public static final int accessibility_action_clickable_span = 2131296271;
        public static final int accessibility_custom_action_0 = 2131296272;
        public static final int accessibility_custom_action_1 = 2131296273;
        public static final int accessibility_custom_action_10 = 2131296274;
        public static final int accessibility_custom_action_11 = 2131296275;
        public static final int accessibility_custom_action_12 = 2131296276;
        public static final int accessibility_custom_action_13 = 2131296277;
        public static final int accessibility_custom_action_14 = 2131296278;
        public static final int accessibility_custom_action_15 = 2131296279;
        public static final int accessibility_custom_action_16 = 2131296280;
        public static final int accessibility_custom_action_17 = 2131296281;
        public static final int accessibility_custom_action_18 = 2131296282;
        public static final int accessibility_custom_action_19 = 2131296283;
        public static final int accessibility_custom_action_2 = 2131296284;
        public static final int accessibility_custom_action_20 = 2131296285;
        public static final int accessibility_custom_action_21 = 2131296286;
        public static final int accessibility_custom_action_22 = 2131296287;
        public static final int accessibility_custom_action_23 = 2131296288;
        public static final int accessibility_custom_action_24 = 2131296289;
        public static final int accessibility_custom_action_25 = 2131296290;
        public static final int accessibility_custom_action_26 = 2131296291;
        public static final int accessibility_custom_action_27 = 2131296292;
        public static final int accessibility_custom_action_28 = 2131296293;
        public static final int accessibility_custom_action_29 = 2131296294;
        public static final int accessibility_custom_action_3 = 2131296295;
        public static final int accessibility_custom_action_30 = 2131296296;
        public static final int accessibility_custom_action_31 = 2131296297;
        public static final int accessibility_custom_action_4 = 2131296298;
        public static final int accessibility_custom_action_5 = 2131296299;
        public static final int accessibility_custom_action_6 = 2131296300;
        public static final int accessibility_custom_action_7 = 2131296301;
        public static final int accessibility_custom_action_8 = 2131296302;
        public static final int accessibility_custom_action_9 = 2131296303;
        public static final int action_container = 2131296315;
        public static final int action_divider = 2131296320;
        public static final int action_image = 2131296327;
        public static final int action_text = 2131296341;
        public static final int actions = 2131296346;
        public static final int async = 2131296366;
        public static final int auto = 2131296367;
        public static final int baseline = 2131296372;
        public static final int blocking = 2131296376;
        public static final int center = 2131296491;
        public static final int chronometer = 2131296516;
        public static final int column = 2131296524;
        public static final int column_reverse = 2131296525;
        public static final int dialog_button = 2131296555;
        public static final int flex_end = 2131296621;
        public static final int flex_start = 2131296622;
        public static final int forever = 2131296625;
        public static final int icon = 2131296643;
        public static final int icon_group = 2131296644;
        public static final int info = 2131296702;
        public static final int italic = 2131296705;
        public static final int item_touch_helper_previous_elevation = 2131296712;
        public static final int line1 = 2131296824;
        public static final int line3 = 2131296825;
        public static final int normal = 2131296929;
        public static final int notification_background = 2131296931;
        public static final int notification_main_column = 2131296932;
        public static final int notification_main_column_container = 2131296933;
        public static final int nowrap = 2131296934;
        public static final int right_icon = 2131297019;
        public static final int right_side = 2131297020;
        public static final int row = 2131297022;
        public static final int row_reverse = 2131297024;
        public static final int space_around = 2131297068;
        public static final int space_between = 2131297069;
        public static final int space_evenly = 2131297070;
        public static final int stretch = 2131297129;
        public static final int tag_accessibility_actions = 2131297150;
        public static final int tag_accessibility_clickable_spans = 2131297151;
        public static final int tag_accessibility_heading = 2131297152;
        public static final int tag_accessibility_pane_title = 2131297153;
        public static final int tag_screen_reader_focusable = 2131297157;
        public static final int tag_transition_group = 2131297159;
        public static final int tag_unhandled_key_event_manager = 2131297160;
        public static final int tag_unhandled_key_listeners = 2131297161;
        public static final int text = 2131297169;
        public static final int text2 = 2131297170;
        public static final int time = 2131297329;
        public static final int title = 2131297330;
        public static final int wrap = 2131297415;
        public static final int wrap_reverse = 2131297418;
        
        private id() {
        }
    }
    
    public static final class integer
    {
        public static final int status_bar_notification_info_maxnum = 2131361860;
        
        private integer() {
        }
    }
    
    public static final class layout
    {
        public static final int custom_dialog = 2131492940;
        public static final int notification_action = 2131493084;
        public static final int notification_action_tombstone = 2131493085;
        public static final int notification_template_custom_big = 2131493092;
        public static final int notification_template_icon_group = 2131493093;
        public static final int notification_template_part_chronometer = 2131493097;
        public static final int notification_template_part_time = 2131493098;
        
        private layout() {
        }
    }
    
    public static final class string
    {
        public static final int status_bar_notification_info_overflow = 2131886849;
        
        private string() {
        }
    }
    
    public static final class style
    {
        public static final int TextAppearance_Compat_Notification = 2131952102;
        public static final int TextAppearance_Compat_Notification_Info = 2131952103;
        public static final int TextAppearance_Compat_Notification_Line2 = 2131952105;
        public static final int TextAppearance_Compat_Notification_Time = 2131952108;
        public static final int TextAppearance_Compat_Notification_Title = 2131952110;
        public static final int Widget_Compat_NotificationActionContainer = 2131952479;
        public static final int Widget_Compat_NotificationActionText = 2131952480;
        
        private style() {
        }
    }
    
    public static final class styleable
    {
        public static final int[] ColorStateListItem;
        public static final int ColorStateListItem_alpha = 3;
        public static final int ColorStateListItem_android_alpha = 1;
        public static final int ColorStateListItem_android_color = 0;
        public static final int ColorStateListItem_android_lStar = 2;
        public static final int ColorStateListItem_lStar = 4;
        public static final int[] FlexboxLayout;
        public static final int[] FlexboxLayout_Layout;
        public static final int FlexboxLayout_Layout_layout_alignSelf = 0;
        public static final int FlexboxLayout_Layout_layout_flexBasisPercent = 1;
        public static final int FlexboxLayout_Layout_layout_flexGrow = 2;
        public static final int FlexboxLayout_Layout_layout_flexShrink = 3;
        public static final int FlexboxLayout_Layout_layout_maxHeight = 4;
        public static final int FlexboxLayout_Layout_layout_maxWidth = 5;
        public static final int FlexboxLayout_Layout_layout_minHeight = 6;
        public static final int FlexboxLayout_Layout_layout_minWidth = 7;
        public static final int FlexboxLayout_Layout_layout_order = 8;
        public static final int FlexboxLayout_Layout_layout_wrapBefore = 9;
        public static final int FlexboxLayout_alignContent = 0;
        public static final int FlexboxLayout_alignItems = 1;
        public static final int FlexboxLayout_dividerDrawable = 2;
        public static final int FlexboxLayout_dividerDrawableHorizontal = 3;
        public static final int FlexboxLayout_dividerDrawableVertical = 4;
        public static final int FlexboxLayout_flexDirection = 5;
        public static final int FlexboxLayout_flexWrap = 6;
        public static final int FlexboxLayout_justifyContent = 7;
        public static final int FlexboxLayout_maxLine = 8;
        public static final int FlexboxLayout_showDivider = 9;
        public static final int FlexboxLayout_showDividerHorizontal = 10;
        public static final int FlexboxLayout_showDividerVertical = 11;
        public static final int[] FontFamily;
        public static final int[] FontFamilyFont;
        public static final int FontFamilyFont_android_font = 0;
        public static final int FontFamilyFont_android_fontStyle = 2;
        public static final int FontFamilyFont_android_fontVariationSettings = 4;
        public static final int FontFamilyFont_android_fontWeight = 1;
        public static final int FontFamilyFont_android_ttcIndex = 3;
        public static final int FontFamilyFont_font = 5;
        public static final int FontFamilyFont_fontStyle = 6;
        public static final int FontFamilyFont_fontVariationSettings = 7;
        public static final int FontFamilyFont_fontWeight = 8;
        public static final int FontFamilyFont_ttcIndex = 9;
        public static final int FontFamily_fontProviderAuthority = 0;
        public static final int FontFamily_fontProviderCerts = 1;
        public static final int FontFamily_fontProviderFetchStrategy = 2;
        public static final int FontFamily_fontProviderFetchTimeout = 3;
        public static final int FontFamily_fontProviderPackage = 4;
        public static final int FontFamily_fontProviderQuery = 5;
        public static final int FontFamily_fontProviderSystemFontFamily = 6;
        public static final int[] GradientColor;
        public static final int[] GradientColorItem;
        public static final int GradientColorItem_android_color = 0;
        public static final int GradientColorItem_android_offset = 1;
        public static final int GradientColor_android_centerColor = 7;
        public static final int GradientColor_android_centerX = 3;
        public static final int GradientColor_android_centerY = 4;
        public static final int GradientColor_android_endColor = 1;
        public static final int GradientColor_android_endX = 10;
        public static final int GradientColor_android_endY = 11;
        public static final int GradientColor_android_gradientRadius = 5;
        public static final int GradientColor_android_startColor = 0;
        public static final int GradientColor_android_startX = 8;
        public static final int GradientColor_android_startY = 9;
        public static final int GradientColor_android_tileMode = 6;
        public static final int GradientColor_android_type = 2;
        public static final int[] RecyclerView;
        public static final int RecyclerView_android_clipToPadding = 1;
        public static final int RecyclerView_android_descendantFocusability = 2;
        public static final int RecyclerView_android_orientation = 0;
        public static final int RecyclerView_fastScrollEnabled = 3;
        public static final int RecyclerView_fastScrollHorizontalThumbDrawable = 4;
        public static final int RecyclerView_fastScrollHorizontalTrackDrawable = 5;
        public static final int RecyclerView_fastScrollVerticalThumbDrawable = 6;
        public static final int RecyclerView_fastScrollVerticalTrackDrawable = 7;
        public static final int RecyclerView_layoutManager = 8;
        public static final int RecyclerView_reverseLayout = 9;
        public static final int RecyclerView_spanCount = 10;
        public static final int RecyclerView_stackFromEnd = 11;
        
        static {
            ColorStateListItem = new int[] { 16843173, 16843551, 16844359, 2130968627, 2130969204 };
            FlexboxLayout = new int[] { 2130968624, 2130968625, 2130968966, 2130968967, 2130968968, 2130969060, 2130969061, 2130969200, 2130969377, 2130969565, 2130969566, 2130969567 };
            FlexboxLayout_Layout = new int[] { 2130969215, 2130969270, 2130969271, 2130969272, 2130969283, 2130969284, 2130969285, 2130969286, 2130969288, 2130969292 };
            FontFamily = new int[] { 2130969098, 2130969099, 2130969100, 2130969101, 2130969102, 2130969103, 2130969104 };
            FontFamilyFont = new int[] { 16844082, 16844083, 16844095, 16844143, 16844144, 2130969096, 2130969105, 2130969106, 2130969107, 2130969814 };
            GradientColor = new int[] { 16843165, 16843166, 16843169, 16843170, 16843171, 16843172, 16843265, 16843275, 16844048, 16844049, 16844050, 16844051 };
            GradientColorItem = new int[] { 16843173, 16844052 };
            RecyclerView = new int[] { 16842948, 16842987, 16842993, 2130969054, 2130969055, 2130969056, 2130969057, 2130969058, 2130969214, 2130969528, 2130969588, 2130969599 };
        }
        
        private styleable() {
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.flexbox;

import android.graphics.Rect;
import java.util.List;
import android.view.View;
import android.graphics.Canvas;
import android.content.res.TypedArray;
import android.content.Context;
import android.graphics.drawable.Drawable;
import androidx.recyclerview.widget.RecyclerView;

public class FlexboxItemDecoration extends n
{
    public static final int BOTH = 3;
    public static final int HORIZONTAL = 1;
    private static final int[] LIST_DIVIDER_ATTRS;
    public static final int VERTICAL = 2;
    private Drawable mDrawable;
    private int mOrientation;
    
    static {
        LIST_DIVIDER_ATTRS = new int[] { 16843284 };
    }
    
    public FlexboxItemDecoration(final Context context) {
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(FlexboxItemDecoration.LIST_DIVIDER_ATTRS);
        this.mDrawable = obtainStyledAttributes.getDrawable(0);
        obtainStyledAttributes.recycle();
        this.setOrientation(3);
    }
    
    private void drawHorizontalDecorations(final Canvas canvas, final RecyclerView recyclerView) {
        if (!this.needsHorizontalDecoration()) {
            return;
        }
        final FlexboxLayoutManager flexboxLayoutManager = (FlexboxLayoutManager)recyclerView.getLayoutManager();
        final int flexDirection = flexboxLayoutManager.getFlexDirection();
        final int left = ((View)recyclerView).getLeft();
        final int paddingLeft = ((View)recyclerView).getPaddingLeft();
        final int right = ((View)recyclerView).getRight();
        final int paddingRight = ((View)recyclerView).getPaddingRight();
        for (int childCount = recyclerView.getChildCount(), i = 0; i < childCount; ++i) {
            final View child = recyclerView.getChildAt(i);
            final p p2 = (p)child.getLayoutParams();
            int n;
            int n2;
            if (flexDirection == 3) {
                n = child.getBottom() + p2.bottomMargin;
                n2 = this.mDrawable.getIntrinsicHeight() + n;
            }
            else {
                n2 = child.getTop() - p2.topMargin;
                n = n2 - this.mDrawable.getIntrinsicHeight();
            }
            int min = 0;
            int max = 0;
            Label_0257: {
                if (flexboxLayoutManager.isMainAxisDirectionHorizontal()) {
                    if (flexboxLayoutManager.isLayoutRtl()) {
                        min = Math.min(child.getRight() + p2.rightMargin + this.mDrawable.getIntrinsicWidth(), right + paddingRight);
                        max = child.getLeft() - p2.leftMargin;
                        break Label_0257;
                    }
                    max = Math.max(child.getLeft() - p2.leftMargin - this.mDrawable.getIntrinsicWidth(), left - paddingLeft);
                }
                else {
                    max = child.getLeft() - p2.leftMargin;
                }
                min = child.getRight() + p2.rightMargin;
            }
            this.mDrawable.setBounds(max, n, min, n2);
            this.mDrawable.draw(canvas);
        }
    }
    
    private void drawVerticalDecorations(final Canvas canvas, final RecyclerView recyclerView) {
        if (!this.needsVerticalDecoration()) {
            return;
        }
        final FlexboxLayoutManager flexboxLayoutManager = (FlexboxLayoutManager)recyclerView.getLayoutManager();
        final int top = ((View)recyclerView).getTop();
        final int paddingTop = ((View)recyclerView).getPaddingTop();
        final int bottom = ((View)recyclerView).getBottom();
        final int paddingBottom = ((View)recyclerView).getPaddingBottom();
        final int childCount = recyclerView.getChildCount();
        final int flexDirection = flexboxLayoutManager.getFlexDirection();
        for (int i = 0; i < childCount; ++i) {
            final View child = recyclerView.getChildAt(i);
            final p p2 = (p)child.getLayoutParams();
            int n;
            int n2;
            if (flexboxLayoutManager.isLayoutRtl()) {
                n = child.getRight() + p2.rightMargin;
                n2 = this.mDrawable.getIntrinsicWidth() + n;
            }
            else {
                n2 = child.getLeft() - p2.leftMargin;
                n = n2 - this.mDrawable.getIntrinsicWidth();
            }
            int max = 0;
            int min = 0;
            Label_0260: {
                if (flexboxLayoutManager.isMainAxisDirectionHorizontal()) {
                    max = child.getTop() - p2.topMargin;
                }
                else {
                    if (flexDirection == 3) {
                        min = Math.min(child.getBottom() + p2.bottomMargin + this.mDrawable.getIntrinsicHeight(), bottom + paddingBottom);
                        max = child.getTop() - p2.topMargin;
                        break Label_0260;
                    }
                    max = Math.max(child.getTop() - p2.topMargin - this.mDrawable.getIntrinsicHeight(), top - paddingTop);
                }
                min = child.getBottom() + p2.bottomMargin;
            }
            this.mDrawable.setBounds(n, max, n2, min);
            this.mDrawable.draw(canvas);
        }
    }
    
    private boolean isFirstItemInLine(final int n, final List<FlexLine> list, final FlexboxLayoutManager flexboxLayoutManager) {
        final int positionToFlexLineIndex = flexboxLayoutManager.getPositionToFlexLineIndex(n);
        boolean b = true;
        if (positionToFlexLineIndex != -1 && positionToFlexLineIndex < flexboxLayoutManager.getFlexLinesInternal().size() && flexboxLayoutManager.getFlexLinesInternal().get(positionToFlexLineIndex).mFirstIndex == n) {
            return true;
        }
        if (n == 0) {
            return true;
        }
        if (list.size() == 0) {
            return false;
        }
        if (list.get(list.size() - 1).mLastIndex != n - 1) {
            b = false;
        }
        return b;
    }
    
    private boolean needsHorizontalDecoration() {
        final int mOrientation = this.mOrientation;
        boolean b = true;
        if ((mOrientation & 0x1) <= 0) {
            b = false;
        }
        return b;
    }
    
    private boolean needsVerticalDecoration() {
        return (this.mOrientation & 0x2) > 0;
    }
    
    private void setOffsetAlongCrossAxis(final Rect rect, final int n, final FlexboxLayoutManager flexboxLayoutManager, final List<FlexLine> list) {
        if (list.size() == 0) {
            return;
        }
        if (flexboxLayoutManager.getPositionToFlexLineIndex(n) == 0) {
            return;
        }
        if (flexboxLayoutManager.isMainAxisDirectionHorizontal()) {
            if (!this.needsHorizontalDecoration()) {
                rect.top = 0;
                rect.bottom = 0;
                return;
            }
            rect.top = this.mDrawable.getIntrinsicHeight();
            rect.bottom = 0;
        }
        else {
            if (!this.needsVerticalDecoration()) {
                return;
            }
            if (flexboxLayoutManager.isLayoutRtl()) {
                rect.right = this.mDrawable.getIntrinsicWidth();
                rect.left = 0;
            }
            else {
                rect.left = this.mDrawable.getIntrinsicWidth();
                rect.right = 0;
            }
        }
    }
    
    private void setOffsetAlongMainAxis(final Rect rect, final int n, final FlexboxLayoutManager flexboxLayoutManager, final List<FlexLine> list, final int n2) {
        if (this.isFirstItemInLine(n, list, flexboxLayoutManager)) {
            return;
        }
        if (flexboxLayoutManager.isMainAxisDirectionHorizontal()) {
            if (!this.needsVerticalDecoration()) {
                rect.left = 0;
                rect.right = 0;
                return;
            }
            if (flexboxLayoutManager.isLayoutRtl()) {
                rect.right = this.mDrawable.getIntrinsicWidth();
                rect.left = 0;
            }
            else {
                rect.left = this.mDrawable.getIntrinsicWidth();
                rect.right = 0;
            }
        }
        else {
            if (!this.needsHorizontalDecoration()) {
                rect.top = 0;
                rect.bottom = 0;
                return;
            }
            if (n2 == 3) {
                rect.bottom = this.mDrawable.getIntrinsicHeight();
                rect.top = 0;
            }
            else {
                rect.top = this.mDrawable.getIntrinsicHeight();
                rect.bottom = 0;
            }
        }
    }
    
    @Override
    public void getItemOffsets(final Rect rect, final View view, final RecyclerView recyclerView, final a0 a0) {
        final int childAdapterPosition = recyclerView.getChildAdapterPosition(view);
        if (childAdapterPosition == 0) {
            return;
        }
        if (!this.needsHorizontalDecoration() && !this.needsVerticalDecoration()) {
            rect.set(0, 0, 0, 0);
            return;
        }
        final FlexboxLayoutManager flexboxLayoutManager = (FlexboxLayoutManager)recyclerView.getLayoutManager();
        final List<FlexLine> flexLines = flexboxLayoutManager.getFlexLines();
        this.setOffsetAlongMainAxis(rect, childAdapterPosition, flexboxLayoutManager, flexLines, flexboxLayoutManager.getFlexDirection());
        this.setOffsetAlongCrossAxis(rect, childAdapterPosition, flexboxLayoutManager, flexLines);
    }
    
    @Override
    public void onDraw(final Canvas canvas, final RecyclerView recyclerView, final a0 a0) {
        this.drawHorizontalDecorations(canvas, recyclerView);
        this.drawVerticalDecorations(canvas, recyclerView);
    }
    
    public void setDrawable(final Drawable mDrawable) {
        if (mDrawable != null) {
            this.mDrawable = mDrawable;
            return;
        }
        throw new IllegalArgumentException("Drawable cannot be null.");
    }
    
    public void setOrientation(final int mOrientation) {
        this.mOrientation = mOrientation;
    }
}

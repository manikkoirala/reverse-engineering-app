// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

public @interface DevicePublicKeyStringDef {
    public static final String DIRECT = "direct";
    public static final String INDIRECT = "indirect";
    public static final String NONE = "none";
}

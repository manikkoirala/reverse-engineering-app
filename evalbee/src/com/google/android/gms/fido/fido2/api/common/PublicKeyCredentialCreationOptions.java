// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import java.util.Collection;
import java.util.Arrays;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.SafeParcelableSerializer;
import com.google.android.gms.common.internal.Preconditions;
import java.util.List;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

@Class(creator = "PublicKeyCredentialCreationOptionsCreator")
@Reserved({ 1 })
public class PublicKeyCredentialCreationOptions extends RequestOptions
{
    public static final Parcelable$Creator<PublicKeyCredentialCreationOptions> CREATOR;
    @Field(getter = "getRp", id = 2)
    private final PublicKeyCredentialRpEntity zza;
    @Field(getter = "getUser", id = 3)
    private final PublicKeyCredentialUserEntity zzb;
    @Field(getter = "getChallenge", id = 4)
    private final byte[] zzc;
    @Field(getter = "getParameters", id = 5)
    private final List zzd;
    @Field(getter = "getTimeoutSeconds", id = 6)
    private final Double zze;
    @Field(getter = "getExcludeList", id = 7)
    private final List zzf;
    @Field(getter = "getAuthenticatorSelection", id = 8)
    private final AuthenticatorSelectionCriteria zzg;
    @Field(getter = "getRequestId", id = 9)
    private final Integer zzh;
    @Field(getter = "getTokenBinding", id = 10)
    private final TokenBinding zzi;
    @Field(getter = "getAttestationConveyancePreferenceAsString", id = 11, type = "java.lang.String")
    private final AttestationConveyancePreference zzj;
    @Field(getter = "getAuthenticationExtensions", id = 12)
    private final AuthenticationExtensions zzk;
    
    static {
        CREATOR = (Parcelable$Creator)new zzak();
    }
    
    @Constructor
    public PublicKeyCredentialCreationOptions(@Param(id = 2) final PublicKeyCredentialRpEntity publicKeyCredentialRpEntity, @Param(id = 3) final PublicKeyCredentialUserEntity publicKeyCredentialUserEntity, @Param(id = 4) final byte[] array, @Param(id = 5) final List list, @Param(id = 6) final Double zze, @Param(id = 7) final List zzf, @Param(id = 8) final AuthenticatorSelectionCriteria zzg, @Param(id = 9) final Integer zzh, @Param(id = 10) final TokenBinding zzi, @Param(id = 11) final String s, @Param(id = 12) final AuthenticationExtensions zzk) {
        this.zza = Preconditions.checkNotNull(publicKeyCredentialRpEntity);
        this.zzb = Preconditions.checkNotNull(publicKeyCredentialUserEntity);
        this.zzc = Preconditions.checkNotNull(array);
        this.zzd = Preconditions.checkNotNull(list);
        this.zze = zze;
        this.zzf = zzf;
        this.zzg = zzg;
        this.zzh = zzh;
        this.zzi = zzi;
        Label_0111: {
            if (s != null) {
                try {
                    this.zzj = AttestationConveyancePreference.fromString(s);
                    break Label_0111;
                }
                catch (final AttestationConveyancePreference.UnsupportedAttestationConveyancePreferenceException cause) {
                    throw new IllegalArgumentException(cause);
                }
            }
            this.zzj = null;
        }
        this.zzk = zzk;
    }
    
    public static PublicKeyCredentialCreationOptions deserializeFromBytes(final byte[] array) {
        return SafeParcelableSerializer.deserializeFromBytes(array, PublicKeyCredentialCreationOptions.CREATOR);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof PublicKeyCredentialCreationOptions)) {
            return false;
        }
        final PublicKeyCredentialCreationOptions publicKeyCredentialCreationOptions = (PublicKeyCredentialCreationOptions)o;
        if (Objects.equal(this.zza, publicKeyCredentialCreationOptions.zza) && Objects.equal(this.zzb, publicKeyCredentialCreationOptions.zzb) && Arrays.equals(this.zzc, publicKeyCredentialCreationOptions.zzc) && Objects.equal(this.zze, publicKeyCredentialCreationOptions.zze) && this.zzd.containsAll(publicKeyCredentialCreationOptions.zzd) && publicKeyCredentialCreationOptions.zzd.containsAll(this.zzd)) {
            final List zzf = this.zzf;
            if (zzf != null || publicKeyCredentialCreationOptions.zzf != null) {
                if (zzf == null) {
                    return false;
                }
                final List zzf2 = publicKeyCredentialCreationOptions.zzf;
                if (zzf2 == null || !zzf.containsAll(zzf2) || !publicKeyCredentialCreationOptions.zzf.containsAll(this.zzf)) {
                    return false;
                }
            }
            if (Objects.equal(this.zzg, publicKeyCredentialCreationOptions.zzg) && Objects.equal(this.zzh, publicKeyCredentialCreationOptions.zzh) && Objects.equal(this.zzi, publicKeyCredentialCreationOptions.zzi) && Objects.equal(this.zzj, publicKeyCredentialCreationOptions.zzj) && Objects.equal(this.zzk, publicKeyCredentialCreationOptions.zzk)) {
                return true;
            }
        }
        return false;
    }
    
    public AttestationConveyancePreference getAttestationConveyancePreference() {
        return this.zzj;
    }
    
    public String getAttestationConveyancePreferenceAsString() {
        final AttestationConveyancePreference zzj = this.zzj;
        if (zzj == null) {
            return null;
        }
        return zzj.toString();
    }
    
    @Override
    public AuthenticationExtensions getAuthenticationExtensions() {
        return this.zzk;
    }
    
    public AuthenticatorSelectionCriteria getAuthenticatorSelection() {
        return this.zzg;
    }
    
    @Override
    public byte[] getChallenge() {
        return this.zzc;
    }
    
    public List<PublicKeyCredentialDescriptor> getExcludeList() {
        return this.zzf;
    }
    
    public List<PublicKeyCredentialParameters> getParameters() {
        return this.zzd;
    }
    
    @Override
    public Integer getRequestId() {
        return this.zzh;
    }
    
    public PublicKeyCredentialRpEntity getRp() {
        return this.zza;
    }
    
    @Override
    public Double getTimeoutSeconds() {
        return this.zze;
    }
    
    @Override
    public TokenBinding getTokenBinding() {
        return this.zzi;
    }
    
    public PublicKeyCredentialUserEntity getUser() {
        return this.zzb;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zza, this.zzb, Arrays.hashCode(this.zzc), this.zzd, this.zze, this.zzf, this.zzg, this.zzh, this.zzi, this.zzj, this.zzk);
    }
    
    @Override
    public byte[] serializeToBytes() {
        return SafeParcelableSerializer.serializeToBytes(this);
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeParcelable(parcel, 2, (Parcelable)this.getRp(), n, false);
        SafeParcelWriter.writeParcelable(parcel, 3, (Parcelable)this.getUser(), n, false);
        SafeParcelWriter.writeByteArray(parcel, 4, this.getChallenge(), false);
        SafeParcelWriter.writeTypedList(parcel, 5, this.getParameters(), false);
        SafeParcelWriter.writeDoubleObject(parcel, 6, this.getTimeoutSeconds(), false);
        SafeParcelWriter.writeTypedList(parcel, 7, this.getExcludeList(), false);
        SafeParcelWriter.writeParcelable(parcel, 8, (Parcelable)this.getAuthenticatorSelection(), n, false);
        SafeParcelWriter.writeIntegerObject(parcel, 9, this.getRequestId(), false);
        SafeParcelWriter.writeParcelable(parcel, 10, (Parcelable)this.getTokenBinding(), n, false);
        SafeParcelWriter.writeString(parcel, 11, this.getAttestationConveyancePreferenceAsString(), false);
        SafeParcelWriter.writeParcelable(parcel, 12, (Parcelable)this.getAuthenticationExtensions(), n, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public static final class Builder
    {
        private PublicKeyCredentialRpEntity zza;
        private PublicKeyCredentialUserEntity zzb;
        private byte[] zzc;
        private List zzd;
        private Double zze;
        private List zzf;
        private AuthenticatorSelectionCriteria zzg;
        private Integer zzh;
        private TokenBinding zzi;
        private AttestationConveyancePreference zzj;
        private AuthenticationExtensions zzk;
        
        public PublicKeyCredentialCreationOptions build() {
            final PublicKeyCredentialRpEntity zza = this.zza;
            final PublicKeyCredentialUserEntity zzb = this.zzb;
            final byte[] zzc = this.zzc;
            final List zzd = this.zzd;
            final Double zze = this.zze;
            final List zzf = this.zzf;
            final AuthenticatorSelectionCriteria zzg = this.zzg;
            final Integer zzh = this.zzh;
            final TokenBinding zzi = this.zzi;
            final AttestationConveyancePreference zzj = this.zzj;
            String string;
            if (zzj == null) {
                string = null;
            }
            else {
                string = zzj.toString();
            }
            return new PublicKeyCredentialCreationOptions(zza, zzb, zzc, zzd, zze, zzf, zzg, zzh, zzi, string, this.zzk);
        }
        
        public Builder setAttestationConveyancePreference(final AttestationConveyancePreference zzj) {
            this.zzj = zzj;
            return this;
        }
        
        public Builder setAuthenticationExtensions(final AuthenticationExtensions zzk) {
            this.zzk = zzk;
            return this;
        }
        
        public Builder setAuthenticatorSelection(final AuthenticatorSelectionCriteria zzg) {
            this.zzg = zzg;
            return this;
        }
        
        public Builder setChallenge(final byte[] array) {
            this.zzc = Preconditions.checkNotNull(array);
            return this;
        }
        
        public Builder setExcludeList(final List<PublicKeyCredentialDescriptor> zzf) {
            this.zzf = zzf;
            return this;
        }
        
        public Builder setParameters(final List<PublicKeyCredentialParameters> list) {
            this.zzd = Preconditions.checkNotNull(list);
            return this;
        }
        
        public Builder setRequestId(final Integer zzh) {
            this.zzh = zzh;
            return this;
        }
        
        public Builder setRp(final PublicKeyCredentialRpEntity publicKeyCredentialRpEntity) {
            this.zza = Preconditions.checkNotNull(publicKeyCredentialRpEntity);
            return this;
        }
        
        public Builder setTimeoutSeconds(final Double zze) {
            this.zze = zze;
            return this;
        }
        
        public Builder setTokenBinding(final TokenBinding zzi) {
            this.zzi = zzi;
            return this;
        }
        
        public Builder setUser(final PublicKeyCredentialUserEntity publicKeyCredentialUserEntity) {
            this.zzb = Preconditions.checkNotNull(publicKeyCredentialUserEntity);
            return this;
        }
    }
}

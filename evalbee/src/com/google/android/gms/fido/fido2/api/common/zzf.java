// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import java.util.Arrays;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "AuthenticationExtensionsDevicePublicKeyOutputsCreator")
public final class zzf extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zzf> CREATOR;
    @Field(getter = "getSignature", id = 1)
    private final byte[] zza;
    @Field(getter = "getAuthenticatorOutput", id = 2)
    private final byte[] zzb;
    
    static {
        CREATOR = (Parcelable$Creator)new zzg();
    }
    
    @Constructor
    public zzf(@Param(id = 1) final byte[] zza, @Param(id = 2) final byte[] zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (!(o instanceof zzf)) {
            return false;
        }
        final zzf zzf = (zzf)o;
        return Arrays.equals(this.zza, zzf.zza) && Arrays.equals(this.zzb, zzf.zzb);
    }
    
    @Override
    public final int hashCode() {
        return Objects.hashCode(this.zza, this.zzb);
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeByteArray(parcel, 1, this.zza, false);
        SafeParcelWriter.writeByteArray(parcel, 2, this.zzb, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

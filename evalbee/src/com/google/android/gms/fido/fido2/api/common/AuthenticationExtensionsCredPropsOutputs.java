// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "AuthenticationExtensionsCredPropsOutputsCreator")
public class AuthenticationExtensionsCredPropsOutputs extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<AuthenticationExtensionsCredPropsOutputs> CREATOR;
    @Field(getter = "getIsDiscoverableCredential", id = 1)
    private final boolean zza;
    
    static {
        CREATOR = (Parcelable$Creator)new zze();
    }
    
    @Constructor
    public AuthenticationExtensionsCredPropsOutputs(@Param(id = 1) final boolean zza) {
        this.zza = zza;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof AuthenticationExtensionsCredPropsOutputs && this.zza == ((AuthenticationExtensionsCredPropsOutputs)o).zza;
    }
    
    public boolean getIsDiscoverableCredential() {
        return this.zza;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zza);
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeBoolean(parcel, 1, this.getIsDiscoverableCredential());
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2;

import com.google.android.gms.common.Feature;
import com.google.android.gms.fido.fido2.api.common.PublicKeyCredentialRequestOptions;
import android.app.PendingIntent;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.common.api.internal.RemoteCall;
import com.google.android.gms.common.api.internal.TaskApiCall;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.fido.fido2.api.common.PublicKeyCredentialCreationOptions;
import android.content.Context;
import com.google.android.gms.common.api.internal.StatusExceptionMapper;
import com.google.android.gms.common.api.internal.ApiExceptionMapper;
import android.app.Activity;
import com.google.android.gms.internal.fido.zzo;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApi;

public class Fido2ApiClient extends GoogleApi<Api.ApiOptions.NoOptions>
{
    private static final Api.ClientKey zza;
    private static final Api zzb;
    
    static {
        zzb = new Api("Fido.FIDO2_API", (Api.AbstractClientBuilder<C, O>)new zzo(), zza = new Api.ClientKey());
    }
    
    @Deprecated
    public Fido2ApiClient(final Activity activity) {
        super(activity, Fido2ApiClient.zzb, (Api.ApiOptions)Api.ApiOptions.NO_OPTIONS, new ApiExceptionMapper());
    }
    
    @Deprecated
    public Fido2ApiClient(final Context context) {
        super(context, Fido2ApiClient.zzb, (Api.ApiOptions)Api.ApiOptions.NO_OPTIONS, new ApiExceptionMapper());
    }
    
    @Deprecated
    public Task<Fido2PendingIntent> getRegisterIntent(final PublicKeyCredentialCreationOptions publicKeyCredentialCreationOptions) {
        return this.doRead((TaskApiCall<Api.AnyClient, Fido2PendingIntent>)TaskApiCall.builder().setMethodKey(5409).run(new zzd(this, publicKeyCredentialCreationOptions)).build());
    }
    
    public Task<PendingIntent> getRegisterPendingIntent(final PublicKeyCredentialCreationOptions publicKeyCredentialCreationOptions) {
        return this.doRead((TaskApiCall<Api.AnyClient, PendingIntent>)TaskApiCall.builder().run(new zzc(this, publicKeyCredentialCreationOptions)).setMethodKey(5407).build());
    }
    
    @Deprecated
    public Task<Fido2PendingIntent> getSignIntent(final PublicKeyCredentialRequestOptions publicKeyCredentialRequestOptions) {
        return this.doRead((TaskApiCall<Api.AnyClient, Fido2PendingIntent>)TaskApiCall.builder().setMethodKey(5410).run(new zza(this, publicKeyCredentialRequestOptions)).build());
    }
    
    public Task<PendingIntent> getSignPendingIntent(final PublicKeyCredentialRequestOptions publicKeyCredentialRequestOptions) {
        return this.doRead((TaskApiCall<Api.AnyClient, PendingIntent>)TaskApiCall.builder().run(new zze(this, publicKeyCredentialRequestOptions)).setMethodKey(5408).build());
    }
    
    public Task<Boolean> isUserVerifyingPlatformAuthenticatorAvailable() {
        return this.doRead((TaskApiCall<Api.AnyClient, Boolean>)TaskApiCall.builder().run(new zzb(this)).setFeatures(com.google.android.gms.fido.zza.zzh).setMethodKey(5411).build());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "AuthenticationExtensionsCreator")
@Reserved({ 1 })
public class AuthenticationExtensions extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<AuthenticationExtensions> CREATOR;
    @Field(getter = "getFidoAppIdExtension", id = 2)
    private final FidoAppIdExtension zza;
    @Field(getter = "getCableAuthenticationExtension", id = 3)
    private final zzs zzb;
    @Field(getter = "getUserVerificationMethodExtension", id = 4)
    private final UserVerificationMethodExtension zzc;
    @Field(getter = "getGoogleMultiAssertionExtension", id = 5)
    private final zzz zzd;
    @Field(getter = "getGoogleSessionIdExtension", id = 6)
    private final zzab zze;
    @Field(getter = "getGoogleSilentVerificationExtension", id = 7)
    private final zzad zzf;
    @Field(getter = "getDevicePublicKeyExtension", id = 8)
    private final zzu zzg;
    @Field(getter = "getGoogleTunnelServerIdExtension", id = 9)
    private final zzag zzh;
    @Field(getter = "getGoogleThirdPartyPaymentExtension", id = 10)
    private final GoogleThirdPartyPaymentExtension zzi;
    @Field(getter = "getPrfExtension", id = 11)
    private final zzai zzj;
    
    static {
        CREATOR = (Parcelable$Creator)new zzd();
    }
    
    @Constructor
    public AuthenticationExtensions(@Param(id = 2) final FidoAppIdExtension zza, @Param(id = 3) final zzs zzb, @Param(id = 4) final UserVerificationMethodExtension zzc, @Param(id = 5) final zzz zzd, @Param(id = 6) final zzab zze, @Param(id = 7) final zzad zzf, @Param(id = 8) final zzu zzg, @Param(id = 9) final zzag zzh, @Param(id = 10) final GoogleThirdPartyPaymentExtension zzi, @Param(id = 11) final zzai zzj) {
        this.zza = zza;
        this.zzc = zzc;
        this.zzb = zzb;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
        this.zzg = zzg;
        this.zzh = zzh;
        this.zzi = zzi;
        this.zzj = zzj;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof AuthenticationExtensions)) {
            return false;
        }
        final AuthenticationExtensions authenticationExtensions = (AuthenticationExtensions)o;
        return Objects.equal(this.zza, authenticationExtensions.zza) && Objects.equal(this.zzb, authenticationExtensions.zzb) && Objects.equal(this.zzc, authenticationExtensions.zzc) && Objects.equal(this.zzd, authenticationExtensions.zzd) && Objects.equal(this.zze, authenticationExtensions.zze) && Objects.equal(this.zzf, authenticationExtensions.zzf) && Objects.equal(this.zzg, authenticationExtensions.zzg) && Objects.equal(this.zzh, authenticationExtensions.zzh) && Objects.equal(this.zzi, authenticationExtensions.zzi) && Objects.equal(this.zzj, authenticationExtensions.zzj);
    }
    
    public FidoAppIdExtension getFidoAppIdExtension() {
        return this.zza;
    }
    
    public UserVerificationMethodExtension getUserVerificationMethodExtension() {
        return this.zzc;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zza, this.zzb, this.zzc, this.zzd, this.zze, this.zzf, this.zzg, this.zzh, this.zzi, this.zzj);
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeParcelable(parcel, 2, (Parcelable)this.getFidoAppIdExtension(), n, false);
        SafeParcelWriter.writeParcelable(parcel, 3, (Parcelable)this.zzb, n, false);
        SafeParcelWriter.writeParcelable(parcel, 4, (Parcelable)this.getUserVerificationMethodExtension(), n, false);
        SafeParcelWriter.writeParcelable(parcel, 5, (Parcelable)this.zzd, n, false);
        SafeParcelWriter.writeParcelable(parcel, 6, (Parcelable)this.zze, n, false);
        SafeParcelWriter.writeParcelable(parcel, 7, (Parcelable)this.zzf, n, false);
        SafeParcelWriter.writeParcelable(parcel, 8, (Parcelable)this.zzg, n, false);
        SafeParcelWriter.writeParcelable(parcel, 9, (Parcelable)this.zzh, n, false);
        SafeParcelWriter.writeParcelable(parcel, 10, (Parcelable)this.zzi, n, false);
        SafeParcelWriter.writeParcelable(parcel, 11, (Parcelable)this.zzj, n, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public final zzs zza() {
        return this.zzb;
    }
    
    public final zzu zzb() {
        return this.zzg;
    }
    
    public final zzz zzc() {
        return this.zzd;
    }
    
    public final zzab zzd() {
        return this.zze;
    }
    
    public final zzad zze() {
        return this.zzf;
    }
    
    public final GoogleThirdPartyPaymentExtension zzf() {
        return this.zzi;
    }
    
    public final zzag zzg() {
        return this.zzh;
    }
    
    public final zzai zzh() {
        return this.zzj;
    }
    
    public static final class Builder
    {
        private FidoAppIdExtension zza;
        private UserVerificationMethodExtension zzb;
        private zzs zzc;
        private zzz zzd;
        private zzab zze;
        private zzad zzf;
        private zzu zzg;
        private zzag zzh;
        private GoogleThirdPartyPaymentExtension zzi;
        private zzai zzj;
        
        public Builder() {
        }
        
        public Builder(final AuthenticationExtensions authenticationExtensions) {
            if (authenticationExtensions != null) {
                this.zza = authenticationExtensions.getFidoAppIdExtension();
                this.zzb = authenticationExtensions.getUserVerificationMethodExtension();
                this.zzc = authenticationExtensions.zza();
                this.zzd = authenticationExtensions.zzc();
                this.zze = authenticationExtensions.zzd();
                this.zzf = authenticationExtensions.zze();
                this.zzg = authenticationExtensions.zzb();
                this.zzh = authenticationExtensions.zzg();
                this.zzi = authenticationExtensions.zzf();
                this.zzj = authenticationExtensions.zzh();
            }
        }
        
        public AuthenticationExtensions build() {
            return new AuthenticationExtensions(this.zza, this.zzc, this.zzb, this.zzd, this.zze, this.zzf, this.zzg, this.zzh, this.zzi, this.zzj);
        }
        
        public Builder setFido2Extension(final FidoAppIdExtension zza) {
            this.zza = zza;
            return this;
        }
        
        public Builder setGoogleThirdPartyPaymentExtension(final GoogleThirdPartyPaymentExtension zzi) {
            this.zzi = zzi;
            return this;
        }
        
        public Builder setUserVerificationMethodExtension(final UserVerificationMethodExtension zzb) {
            this.zzb = zzb;
            return this;
        }
    }
}

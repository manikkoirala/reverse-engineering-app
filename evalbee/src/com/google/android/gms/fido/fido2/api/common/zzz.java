// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "GoogleMultiAssertionExtensionCreator")
public final class zzz extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zzz> CREATOR;
    @Field(getter = "getRequestForMultiAssertion", id = 1)
    private final boolean zza;
    
    static {
        CREATOR = (Parcelable$Creator)new zzaa();
    }
    
    @Constructor
    public zzz(@Param(id = 1) final boolean b) {
        this.zza = Preconditions.checkNotNull(b);
    }
    
    @Override
    public final boolean equals(final Object o) {
        return o instanceof zzz && this.zza == ((zzz)o).zza;
    }
    
    @Override
    public final int hashCode() {
        return Objects.hashCode(this.zza);
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeBoolean(parcel, 1, this.zza);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

public enum RSAAlgorithm implements Algorithm
{
    @Deprecated
    LEGACY_RS1("LEGACY_RS1", 3, -262), 
    PS256("PS256", 4, -37), 
    PS384("PS384", 5, -38), 
    PS512("PS512", 6, -39), 
    RS1("RS1", 7, -65535), 
    RS256("RS256", 0, -257), 
    RS384("RS384", 1, -258), 
    RS512("RS512", 2, -259);
    
    private static final RSAAlgorithm[] zza;
    private final int zzb;
    
    private RSAAlgorithm(final String name, final int ordinal, final int zzb) {
        this.zzb = zzb;
    }
    
    @Override
    public int getAlgoValue() {
        return this.zzb;
    }
}

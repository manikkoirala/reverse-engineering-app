// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;
import android.os.Parcelable;

public class COSEAlgorithmIdentifier implements Parcelable
{
    public static final Parcelable$Creator<COSEAlgorithmIdentifier> CREATOR;
    private final Algorithm zza;
    
    static {
        CREATOR = (Parcelable$Creator)new zzp();
    }
    
    public COSEAlgorithmIdentifier(final Algorithm algorithm) {
        this.zza = Preconditions.checkNotNull(algorithm);
    }
    
    public static COSEAlgorithmIdentifier fromCoseValue(final int n) {
        if (n != RSAAlgorithm.LEGACY_RS1.getAlgoValue()) {
            final RSAAlgorithm[] values = RSAAlgorithm.values();
            final int length = values.length;
            final int n2 = 0;
            for (final Algorithm rs1 : values) {
                if (((RSAAlgorithm)rs1).getAlgoValue() == n) {
                    return new COSEAlgorithmIdentifier(rs1);
                }
            }
            final EC2Algorithm[] values2 = EC2Algorithm.values();
            for (int length2 = values2.length, j = n2; j < length2; ++j) {
                final Algorithm rs1 = values2[j];
                if (((EC2Algorithm)rs1).getAlgoValue() == n) {
                    return new COSEAlgorithmIdentifier(rs1);
                }
            }
            throw new UnsupportedAlgorithmIdentifierException(n);
        }
        Algorithm rs1 = RSAAlgorithm.RS1;
        return new COSEAlgorithmIdentifier(rs1);
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof COSEAlgorithmIdentifier && this.zza.getAlgoValue() == ((COSEAlgorithmIdentifier)o).zza.getAlgoValue();
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zza);
    }
    
    public int toCoseValue() {
        return this.zza.getAlgoValue();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeInt(this.zza.getAlgoValue());
    }
    
    public static class UnsupportedAlgorithmIdentifierException extends Exception
    {
        public UnsupportedAlgorithmIdentifierException(final int i) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Algorithm with COSE value ");
            sb.append(i);
            sb.append(" not supported");
            super(sb.toString());
        }
    }
}

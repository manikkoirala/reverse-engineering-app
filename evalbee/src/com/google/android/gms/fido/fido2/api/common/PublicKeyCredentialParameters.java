// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "PublicKeyCredentialParametersCreator")
@Reserved({ 1 })
public class PublicKeyCredentialParameters extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<PublicKeyCredentialParameters> CREATOR;
    @Field(getter = "getTypeAsString", id = 2, type = "java.lang.String")
    private final PublicKeyCredentialType zza;
    @Field(getter = "getAlgorithmIdAsInteger", id = 3, type = "java.lang.Integer")
    private final COSEAlgorithmIdentifier zzb;
    
    static {
        CREATOR = (Parcelable$Creator)new zzan();
    }
    
    @Constructor
    public PublicKeyCredentialParameters(@Param(id = 2) final String s, @Param(id = 3) final int i) {
        Preconditions.checkNotNull(s);
        try {
            this.zza = PublicKeyCredentialType.fromString(s);
            Preconditions.checkNotNull(i);
            try {
                this.zzb = COSEAlgorithmIdentifier.fromCoseValue(i);
            }
            catch (final COSEAlgorithmIdentifier.UnsupportedAlgorithmIdentifierException cause) {
                throw new IllegalArgumentException(cause);
            }
        }
        catch (final PublicKeyCredentialType.UnsupportedPublicKeyCredTypeException cause2) {
            throw new IllegalArgumentException(cause2);
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof PublicKeyCredentialParameters)) {
            return false;
        }
        final PublicKeyCredentialParameters publicKeyCredentialParameters = (PublicKeyCredentialParameters)o;
        return this.zza.equals(publicKeyCredentialParameters.zza) && this.zzb.equals(publicKeyCredentialParameters.zzb);
    }
    
    public COSEAlgorithmIdentifier getAlgorithm() {
        return this.zzb;
    }
    
    public int getAlgorithmIdAsInteger() {
        return this.zzb.toCoseValue();
    }
    
    public PublicKeyCredentialType getType() {
        return this.zza;
    }
    
    public String getTypeAsString() {
        return this.zza.toString();
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zza, this.zzb);
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 2, this.getTypeAsString(), false);
        SafeParcelWriter.writeIntegerObject(parcel, 3, this.getAlgorithmIdAsInteger(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

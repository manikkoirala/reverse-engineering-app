// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2;

import com.google.android.gms.common.api.internal.TaskUtil;
import com.google.android.gms.internal.fido.zzi;
import android.app.PendingIntent;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.internal.fido.zzl;

final class zzt extends zzl
{
    final TaskCompletionSource zza;
    
    public zzt(final Fido2PrivilegedApiClient fido2PrivilegedApiClient, final TaskCompletionSource zza) {
        this.zza = zza;
    }
    
    public final void zzb(final Status status, final PendingIntent pendingIntent) {
        TaskUtil.setResultOrApiException(status, new zzi(pendingIntent), (com.google.android.gms.tasks.TaskCompletionSource<zzi>)this.zza);
    }
}

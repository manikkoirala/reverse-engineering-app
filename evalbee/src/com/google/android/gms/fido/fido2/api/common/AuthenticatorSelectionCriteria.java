// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "AuthenticatorSelectionCriteriaCreator")
@Reserved({ 1 })
public class AuthenticatorSelectionCriteria extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<AuthenticatorSelectionCriteria> CREATOR;
    @Field(getter = "getAttachmentAsString", id = 2, type = "java.lang.String")
    private final Attachment zza;
    @Field(getter = "getRequireResidentKey", id = 3)
    private final Boolean zzb;
    @Field(getter = "getRequireUserVerificationAsString", id = 4, type = "java.lang.String")
    private final zzay zzc;
    @Field(getter = "getResidentKeyRequirementAsString", id = 5, type = "java.lang.String")
    private final ResidentKeyRequirement zzd;
    
    static {
        CREATOR = (Parcelable$Creator)new zzm();
    }
    
    @Constructor
    public AuthenticatorSelectionCriteria(@Param(id = 2) String ex, @Param(id = 3) final Boolean zzb, @Param(id = 4) final String s, @Param(id = 5) final String s2) {
        final ResidentKeyRequirement.UnsupportedResidentKeyRequirementException ex2 = null;
        Label_0021: {
            if (ex == null) {
                ex = null;
                break Label_0021;
            }
            try {
                ex = (ResidentKeyRequirement.UnsupportedResidentKeyRequirementException)Attachment.fromString((String)ex);
                this.zza = (Attachment)ex;
                this.zzb = zzb;
                if (s == null) {
                    ex = null;
                }
                else {
                    ex = (ResidentKeyRequirement.UnsupportedResidentKeyRequirementException)zzay.zza(s);
                }
                this.zzc = (zzay)ex;
                if (s2 == null) {
                    ex = ex2;
                }
                else {
                    ex = (ResidentKeyRequirement.UnsupportedResidentKeyRequirementException)ResidentKeyRequirement.fromString(s2);
                }
                this.zzd = (ResidentKeyRequirement)ex;
                return;
            }
            catch (final ResidentKeyRequirement.UnsupportedResidentKeyRequirementException ex) {}
            catch (final zzax ex) {}
            catch (final Attachment.UnsupportedAttachmentException ex3) {}
        }
        throw new IllegalArgumentException(ex);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof AuthenticatorSelectionCriteria)) {
            return false;
        }
        final AuthenticatorSelectionCriteria authenticatorSelectionCriteria = (AuthenticatorSelectionCriteria)o;
        return Objects.equal(this.zza, authenticatorSelectionCriteria.zza) && Objects.equal(this.zzb, authenticatorSelectionCriteria.zzb) && Objects.equal(this.zzc, authenticatorSelectionCriteria.zzc) && Objects.equal(this.zzd, authenticatorSelectionCriteria.zzd);
    }
    
    public Attachment getAttachment() {
        return this.zza;
    }
    
    public String getAttachmentAsString() {
        final Attachment zza = this.zza;
        if (zza == null) {
            return null;
        }
        return zza.toString();
    }
    
    public Boolean getRequireResidentKey() {
        return this.zzb;
    }
    
    public ResidentKeyRequirement getResidentKeyRequirement() {
        return this.zzd;
    }
    
    public String getResidentKeyRequirementAsString() {
        final ResidentKeyRequirement zzd = this.zzd;
        if (zzd == null) {
            return null;
        }
        return zzd.toString();
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zza, this.zzb, this.zzc, this.zzd);
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 2, this.getAttachmentAsString(), false);
        SafeParcelWriter.writeBooleanObject(parcel, 3, this.getRequireResidentKey(), false);
        final zzay zzc = this.zzc;
        String string;
        if (zzc == null) {
            string = null;
        }
        else {
            string = zzc.toString();
        }
        SafeParcelWriter.writeString(parcel, 4, string, false);
        SafeParcelWriter.writeString(parcel, 5, this.getResidentKeyRequirementAsString(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public static class Builder
    {
        private Attachment zza;
        private Boolean zzb;
        private ResidentKeyRequirement zzc;
        
        public AuthenticatorSelectionCriteria build() {
            final Attachment zza = this.zza;
            String string;
            if (zza == null) {
                string = null;
            }
            else {
                string = zza.toString();
            }
            final Boolean zzb = this.zzb;
            final ResidentKeyRequirement zzc = this.zzc;
            String string2;
            if (zzc == null) {
                string2 = null;
            }
            else {
                string2 = zzc.toString();
            }
            return new AuthenticatorSelectionCriteria(string, zzb, null, string2);
        }
        
        public Builder setAttachment(final Attachment zza) {
            this.zza = zza;
            return this;
        }
        
        public Builder setRequireResidentKey(final Boolean zzb) {
            this.zzb = zzb;
            return this;
        }
        
        public Builder setResidentKeyRequirement(final ResidentKeyRequirement zzc) {
            this.zzc = zzc;
            return this;
        }
    }
}

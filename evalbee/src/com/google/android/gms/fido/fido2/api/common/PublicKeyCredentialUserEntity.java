// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import java.util.Arrays;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "PublicKeyCredentialUserEntityCreator")
@Reserved({ 1 })
public class PublicKeyCredentialUserEntity extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<PublicKeyCredentialUserEntity> CREATOR;
    @Field(getter = "getId", id = 2)
    private final byte[] zza;
    @Field(getter = "getName", id = 3)
    private final String zzb;
    @Field(getter = "getIcon", id = 4)
    private final String zzc;
    @Field(getter = "getDisplayName", id = 5)
    private final String zzd;
    
    static {
        CREATOR = (Parcelable$Creator)new zzar();
    }
    
    @Constructor
    public PublicKeyCredentialUserEntity(@Param(id = 2) final byte[] array, @Param(id = 3) final String s, @Param(id = 4) final String zzc, @Param(id = 5) final String s2) {
        this.zza = Preconditions.checkNotNull(array);
        this.zzb = Preconditions.checkNotNull(s);
        this.zzc = zzc;
        this.zzd = Preconditions.checkNotNull(s2);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof PublicKeyCredentialUserEntity)) {
            return false;
        }
        final PublicKeyCredentialUserEntity publicKeyCredentialUserEntity = (PublicKeyCredentialUserEntity)o;
        return Arrays.equals(this.zza, publicKeyCredentialUserEntity.zza) && Objects.equal(this.zzb, publicKeyCredentialUserEntity.zzb) && Objects.equal(this.zzc, publicKeyCredentialUserEntity.zzc) && Objects.equal(this.zzd, publicKeyCredentialUserEntity.zzd);
    }
    
    public String getDisplayName() {
        return this.zzd;
    }
    
    public String getIcon() {
        return this.zzc;
    }
    
    public byte[] getId() {
        return this.zza;
    }
    
    public String getName() {
        return this.zzb;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zza, this.zzb, this.zzc, this.zzd);
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeByteArray(parcel, 2, this.getId(), false);
        SafeParcelWriter.writeString(parcel, 3, this.getName(), false);
        SafeParcelWriter.writeString(parcel, 4, this.getIcon(), false);
        SafeParcelWriter.writeString(parcel, 5, this.getDisplayName(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

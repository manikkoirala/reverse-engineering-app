// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import android.os.Parcel;
import android.os.Parcelable$Creator;
import android.os.Parcelable;

public enum ResidentKeyRequirement implements Parcelable
{
    public static final Parcelable$Creator<ResidentKeyRequirement> CREATOR;
    
    RESIDENT_KEY_DISCOURAGED("RESIDENT_KEY_DISCOURAGED", 0, "discouraged"), 
    RESIDENT_KEY_PREFERRED("RESIDENT_KEY_PREFERRED", 1, "preferred"), 
    RESIDENT_KEY_REQUIRED("RESIDENT_KEY_REQUIRED", 2, "required");
    
    private static final ResidentKeyRequirement[] zza;
    private final String zzb;
    
    static {
        CREATOR = (Parcelable$Creator)new zzas();
    }
    
    private ResidentKeyRequirement(final String name, final int ordinal, final String zzb) {
        this.zzb = zzb;
    }
    
    public static ResidentKeyRequirement fromString(final String s) {
        for (final ResidentKeyRequirement residentKeyRequirement : values()) {
            if (s.equals(residentKeyRequirement.zzb)) {
                return residentKeyRequirement;
            }
        }
        throw new UnsupportedResidentKeyRequirementException(s);
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public String toString() {
        return this.zzb;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeString(this.zzb);
    }
    
    public static class UnsupportedResidentKeyRequirementException extends Exception
    {
        public UnsupportedResidentKeyRequirementException(final String s) {
            super(String.format("Resident key requirement %s not supported", s));
        }
    }
}

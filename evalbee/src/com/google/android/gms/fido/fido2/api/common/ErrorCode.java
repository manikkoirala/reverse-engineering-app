// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import java.util.Locale;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import android.os.Parcelable;

public enum ErrorCode implements Parcelable
{
    ABORT_ERR("ABORT_ERR", 4, 20), 
    ATTESTATION_NOT_PRIVATE_ERR("ATTESTATION_NOT_PRIVATE_ERR", 11, 36), 
    CONSTRAINT_ERR("CONSTRAINT_ERR", 8, 29);
    
    public static final Parcelable$Creator<ErrorCode> CREATOR;
    
    DATA_ERR("DATA_ERR", 9, 30), 
    ENCODING_ERR("ENCODING_ERR", 6, 27), 
    INVALID_STATE_ERR("INVALID_STATE_ERR", 1, 11), 
    NETWORK_ERR("NETWORK_ERR", 3, 19), 
    NOT_ALLOWED_ERR("NOT_ALLOWED_ERR", 10, 35), 
    NOT_SUPPORTED_ERR("NOT_SUPPORTED_ERR", 0, 9), 
    SECURITY_ERR("SECURITY_ERR", 2, 18), 
    TIMEOUT_ERR("TIMEOUT_ERR", 5, 23), 
    UNKNOWN_ERR("UNKNOWN_ERR", 7, 28);
    
    private static final ErrorCode[] zza;
    private final int zzb;
    
    static {
        CREATOR = (Parcelable$Creator)new zzw();
    }
    
    private ErrorCode(final String name, final int ordinal, final int zzb) {
        this.zzb = zzb;
    }
    
    public static ErrorCode toErrorCode(final int n) {
        for (final ErrorCode errorCode : values()) {
            if (n == errorCode.zzb) {
                return errorCode;
            }
        }
        throw new UnsupportedErrorCodeException(n);
    }
    
    public int describeContents() {
        return 0;
    }
    
    public int getCode() {
        return this.zzb;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeInt(this.zzb);
    }
    
    public static class UnsupportedErrorCodeException extends Exception
    {
        public UnsupportedErrorCodeException(final int i) {
            super(String.format(Locale.US, "Error code %d is not supported", i));
        }
    }
}

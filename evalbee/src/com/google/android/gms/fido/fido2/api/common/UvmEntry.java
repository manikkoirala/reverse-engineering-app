// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "UvmEntryCreator")
public class UvmEntry extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<UvmEntry> CREATOR;
    @Field(getter = "getUserVerificationMethod", id = 1)
    private final int zza;
    @Field(getter = "getKeyProtectionType", id = 2)
    private final short zzb;
    @Field(getter = "getMatcherProtectionType", id = 3)
    private final short zzc;
    
    static {
        CREATOR = (Parcelable$Creator)new zzba();
    }
    
    @Constructor
    public UvmEntry(@Param(id = 1) final int zza, @Param(id = 2) final short zzb, @Param(id = 3) final short zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof UvmEntry)) {
            return false;
        }
        final UvmEntry uvmEntry = (UvmEntry)o;
        return this.zza == uvmEntry.zza && this.zzb == uvmEntry.zzb && this.zzc == uvmEntry.zzc;
    }
    
    public short getKeyProtectionType() {
        return this.zzb;
    }
    
    public short getMatcherProtectionType() {
        return this.zzc;
    }
    
    public int getUserVerificationMethod() {
        return this.zza;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zza, this.zzb, this.zzc);
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.getUserVerificationMethod());
        SafeParcelWriter.writeShort(parcel, 2, this.getKeyProtectionType());
        SafeParcelWriter.writeShort(parcel, 3, this.getMatcherProtectionType());
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public static final class Builder
    {
        private int zza;
        private short zzb;
        private short zzc;
        
        public UvmEntry build() {
            return new UvmEntry(this.zza, this.zzb, this.zzc);
        }
        
        public Builder setKeyProtectionType(final short zzb) {
            this.zzb = zzb;
            return this;
        }
        
        public Builder setMatcherProtectionType(final short zzc) {
            this.zzc = zzc;
            return this;
        }
        
        public Builder setUserVerificationMethod(final int zza) {
            this.zza = zza;
            return this;
        }
    }
}

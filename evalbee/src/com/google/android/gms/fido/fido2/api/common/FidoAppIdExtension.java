// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "FidoAppIdExtensionCreator")
@Reserved({ 1 })
public class FidoAppIdExtension extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<FidoAppIdExtension> CREATOR;
    @Field(getter = "getAppId", id = 2)
    private final String zza;
    
    static {
        CREATOR = (Parcelable$Creator)new zzx();
    }
    
    @Constructor
    public FidoAppIdExtension(@Param(id = 2) final String s) {
        this.zza = Preconditions.checkNotNull(s);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof FidoAppIdExtension && this.zza.equals(((FidoAppIdExtension)o).zza);
    }
    
    public String getAppId() {
        return this.zza;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zza);
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 2, this.getAppId(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

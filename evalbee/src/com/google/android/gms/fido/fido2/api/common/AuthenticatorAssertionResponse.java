// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.internal.fido.zzaj;
import com.google.android.gms.internal.fido.zzbf;
import com.google.android.gms.internal.fido.zzak;
import com.google.android.gms.common.internal.Objects;
import java.util.Arrays;
import com.google.android.gms.common.internal.safeparcel.SafeParcelableSerializer;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

@Class(creator = "AuthenticatorAssertionResponseCreator")
@Reserved({ 1 })
public class AuthenticatorAssertionResponse extends AuthenticatorResponse
{
    public static final Parcelable$Creator<AuthenticatorAssertionResponse> CREATOR;
    @Field(getter = "getKeyHandle", id = 2)
    private final byte[] zza;
    @Field(getter = "getClientDataJSON", id = 3)
    private final byte[] zzb;
    @Field(getter = "getAuthenticatorData", id = 4)
    private final byte[] zzc;
    @Field(getter = "getSignature", id = 5)
    private final byte[] zzd;
    @Field(getter = "getUserHandle", id = 6)
    private final byte[] zze;
    
    static {
        CREATOR = (Parcelable$Creator)new zzj();
    }
    
    @Constructor
    public AuthenticatorAssertionResponse(@Param(id = 2) final byte[] array, @Param(id = 3) final byte[] array2, @Param(id = 4) final byte[] array3, @Param(id = 5) final byte[] array4, @Param(id = 6) final byte[] zze) {
        this.zza = Preconditions.checkNotNull(array);
        this.zzb = Preconditions.checkNotNull(array2);
        this.zzc = Preconditions.checkNotNull(array3);
        this.zzd = Preconditions.checkNotNull(array4);
        this.zze = zze;
    }
    
    public static AuthenticatorAssertionResponse deserializeFromBytes(final byte[] array) {
        return SafeParcelableSerializer.deserializeFromBytes(array, AuthenticatorAssertionResponse.CREATOR);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof AuthenticatorAssertionResponse)) {
            return false;
        }
        final AuthenticatorAssertionResponse authenticatorAssertionResponse = (AuthenticatorAssertionResponse)o;
        return Arrays.equals(this.zza, authenticatorAssertionResponse.zza) && Arrays.equals(this.zzb, authenticatorAssertionResponse.zzb) && Arrays.equals(this.zzc, authenticatorAssertionResponse.zzc) && Arrays.equals(this.zzd, authenticatorAssertionResponse.zzd) && Arrays.equals(this.zze, authenticatorAssertionResponse.zze);
    }
    
    public byte[] getAuthenticatorData() {
        return this.zzc;
    }
    
    @Override
    public byte[] getClientDataJSON() {
        return this.zzb;
    }
    
    @Deprecated
    public byte[] getKeyHandle() {
        return this.zza;
    }
    
    public byte[] getSignature() {
        return this.zzd;
    }
    
    public byte[] getUserHandle() {
        return this.zze;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(Arrays.hashCode(this.zza), Arrays.hashCode(this.zzb), Arrays.hashCode(this.zzc), Arrays.hashCode(this.zzd), Arrays.hashCode(this.zze));
    }
    
    @Override
    public byte[] serializeToBytes() {
        return SafeParcelableSerializer.serializeToBytes(this);
    }
    
    @Override
    public String toString() {
        final zzaj zza = zzak.zza((Object)this);
        final zzbf zzd = zzbf.zzd();
        final byte[] zza2 = this.zza;
        zza.zzb("keyHandle", (Object)zzd.zze(zza2, 0, zza2.length));
        final zzbf zzd2 = zzbf.zzd();
        final byte[] zzb = this.zzb;
        zza.zzb("clientDataJSON", (Object)zzd2.zze(zzb, 0, zzb.length));
        final zzbf zzd3 = zzbf.zzd();
        final byte[] zzc = this.zzc;
        zza.zzb("authenticatorData", (Object)zzd3.zze(zzc, 0, zzc.length));
        final zzbf zzd4 = zzbf.zzd();
        final byte[] zzd5 = this.zzd;
        zza.zzb("signature", (Object)zzd4.zze(zzd5, 0, zzd5.length));
        final byte[] zze = this.zze;
        if (zze != null) {
            zza.zzb("userHandle", (Object)zzbf.zzd().zze(zze, 0, zze.length));
        }
        return zza.toString();
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeByteArray(parcel, 2, this.getKeyHandle(), false);
        SafeParcelWriter.writeByteArray(parcel, 3, this.getClientDataJSON(), false);
        SafeParcelWriter.writeByteArray(parcel, 4, this.getAuthenticatorData(), false);
        SafeParcelWriter.writeByteArray(parcel, 5, this.getSignature(), false);
        SafeParcelWriter.writeByteArray(parcel, 6, this.getUserHandle(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

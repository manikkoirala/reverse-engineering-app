// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import android.os.Parcel;
import android.os.Parcelable$Creator;
import android.os.Parcelable;

public enum Attachment implements Parcelable
{
    public static final Parcelable$Creator<Attachment> CREATOR;
    
    CROSS_PLATFORM("CROSS_PLATFORM", 1, "cross-platform"), 
    PLATFORM("PLATFORM", 0, "platform");
    
    private static final Attachment[] zza;
    private final String zzb;
    
    static {
        CREATOR = (Parcelable$Creator)new zza();
    }
    
    private Attachment(final String name, final int ordinal, final String zzb) {
        this.zzb = zzb;
    }
    
    public static Attachment fromString(final String s) {
        for (final Attachment attachment : values()) {
            if (s.equals(attachment.zzb)) {
                return attachment;
            }
        }
        throw new UnsupportedAttachmentException(s);
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public String toString() {
        return this.zzb;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeString(this.zzb);
    }
    
    public static class UnsupportedAttachmentException extends Exception
    {
        public UnsupportedAttachmentException(final String s) {
            super(String.format("Attachment %s not supported", s));
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.internal.fido.zzd;

final class zzj extends zzd
{
    final TaskCompletionSource zza;
    
    public zzj(final Fido2ApiClient fido2ApiClient, final TaskCompletionSource zza) {
        this.zza = zza;
    }
    
    public final void zzb(final boolean b) {
        this.zza.setResult((Object)b);
    }
    
    public final void zzc(final Status status) {
        this.zza.trySetException((Exception)new ApiException(status));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

public enum EC2Algorithm implements Algorithm
{
    ED25519("ED25519", 2, -8), 
    ED256("ED256", 0, -260), 
    ED512("ED512", 1, -261), 
    ES256("ES256", 3, -7), 
    ES384("ES384", 4, -35), 
    ES512("ES512", 5, -36);
    
    private static final EC2Algorithm[] zza;
    private final int zzb;
    
    private EC2Algorithm(final String name, final int ordinal, final int zzb) {
        this.zzb = zzb;
    }
    
    @Override
    public int getAlgoValue() {
        return this.zzb;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "GoogleSessionIdExtensionCreator")
public final class zzab extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zzab> CREATOR;
    @Field(getter = "getSessionId", id = 1)
    private final long zza;
    
    static {
        CREATOR = (Parcelable$Creator)new zzac();
    }
    
    @Constructor
    public zzab(@Param(id = 1) final long l) {
        this.zza = Preconditions.checkNotNull(l);
    }
    
    @Override
    public final boolean equals(final Object o) {
        return o instanceof zzab && this.zza == ((zzab)o).zza;
    }
    
    @Override
    public final int hashCode() {
        return Objects.hashCode(this.zza);
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeLong(parcel, 1, this.zza);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

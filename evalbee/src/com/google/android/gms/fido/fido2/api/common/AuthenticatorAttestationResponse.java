// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.internal.fido.zzaj;
import com.google.android.gms.internal.fido.zzbf;
import com.google.android.gms.internal.fido.zzak;
import com.google.android.gms.common.internal.Objects;
import java.util.Arrays;
import com.google.android.gms.common.internal.safeparcel.SafeParcelableSerializer;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

@Class(creator = "AuthenticatorAttestationResponseCreator")
@Reserved({ 1 })
public class AuthenticatorAttestationResponse extends AuthenticatorResponse
{
    public static final Parcelable$Creator<AuthenticatorAttestationResponse> CREATOR;
    @Field(getter = "getKeyHandle", id = 2)
    private final byte[] zza;
    @Field(getter = "getClientDataJSON", id = 3)
    private final byte[] zzb;
    @Field(getter = "getAttestationObject", id = 4)
    private final byte[] zzc;
    @Field(getter = "getTransports", id = 5)
    private final String[] zzd;
    
    static {
        CREATOR = (Parcelable$Creator)new zzk();
    }
    
    @Constructor
    public AuthenticatorAttestationResponse(@Param(id = 2) final byte[] array, @Param(id = 3) final byte[] array2, @Param(id = 4) final byte[] array3, @Param(id = 5) final String[] array4) {
        this.zza = Preconditions.checkNotNull(array);
        this.zzb = Preconditions.checkNotNull(array2);
        this.zzc = Preconditions.checkNotNull(array3);
        this.zzd = Preconditions.checkNotNull(array4);
    }
    
    public static AuthenticatorAttestationResponse deserializeFromBytes(final byte[] array) {
        return SafeParcelableSerializer.deserializeFromBytes(array, AuthenticatorAttestationResponse.CREATOR);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof AuthenticatorAttestationResponse)) {
            return false;
        }
        final AuthenticatorAttestationResponse authenticatorAttestationResponse = (AuthenticatorAttestationResponse)o;
        return Arrays.equals(this.zza, authenticatorAttestationResponse.zza) && Arrays.equals(this.zzb, authenticatorAttestationResponse.zzb) && Arrays.equals(this.zzc, authenticatorAttestationResponse.zzc);
    }
    
    public byte[] getAttestationObject() {
        return this.zzc;
    }
    
    @Override
    public byte[] getClientDataJSON() {
        return this.zzb;
    }
    
    @Deprecated
    public byte[] getKeyHandle() {
        return this.zza;
    }
    
    public String[] getTransports() {
        return this.zzd;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(Arrays.hashCode(this.zza), Arrays.hashCode(this.zzb), Arrays.hashCode(this.zzc));
    }
    
    @Override
    public byte[] serializeToBytes() {
        return SafeParcelableSerializer.serializeToBytes(this);
    }
    
    @Override
    public String toString() {
        final zzaj zza = zzak.zza((Object)this);
        final zzbf zzd = zzbf.zzd();
        final byte[] zza2 = this.zza;
        zza.zzb("keyHandle", (Object)zzd.zze(zza2, 0, zza2.length));
        final zzbf zzd2 = zzbf.zzd();
        final byte[] zzb = this.zzb;
        zza.zzb("clientDataJSON", (Object)zzd2.zze(zzb, 0, zzb.length));
        final zzbf zzd3 = zzbf.zzd();
        final byte[] zzc = this.zzc;
        zza.zzb("attestationObject", (Object)zzd3.zze(zzc, 0, zzc.length));
        zza.zzb("transports", (Object)Arrays.toString(this.zzd));
        return zza.toString();
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeByteArray(parcel, 2, this.getKeyHandle(), false);
        SafeParcelWriter.writeByteArray(parcel, 3, this.getClientDataJSON(), false);
        SafeParcelWriter.writeByteArray(parcel, 4, this.getAttestationObject(), false);
        SafeParcelWriter.writeStringArray(parcel, 5, this.getTransports(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

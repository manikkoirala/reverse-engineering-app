// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import java.util.Arrays;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.SafeParcelableSerializer;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "FidoCredentialDetailsCreator")
public class FidoCredentialDetails extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<FidoCredentialDetails> CREATOR;
    @Field(getter = "getUserName", id = 1)
    private final String zza;
    @Field(getter = "getUserDisplayName", id = 2)
    private final String zzb;
    @Field(getter = "getUserId", id = 3)
    private final byte[] zzc;
    @Field(getter = "getCredentialId", id = 4)
    private final byte[] zzd;
    @Field(getter = "getIsDiscoverable", id = 5)
    private final boolean zze;
    @Field(getter = "getIsPaymentCredential", id = 6)
    private final boolean zzf;
    
    static {
        CREATOR = (Parcelable$Creator)new zzy();
    }
    
    @Constructor
    public FidoCredentialDetails(@Param(id = 1) final String zza, @Param(id = 2) final String zzb, @Param(id = 3) final byte[] zzc, @Param(id = 4) final byte[] zzd, @Param(id = 5) final boolean zze, @Param(id = 6) final boolean zzf) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
    }
    
    public static FidoCredentialDetails deserializeFromBytes(final byte[] array) {
        return SafeParcelableSerializer.deserializeFromBytes(array, FidoCredentialDetails.CREATOR);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof FidoCredentialDetails)) {
            return false;
        }
        final FidoCredentialDetails fidoCredentialDetails = (FidoCredentialDetails)o;
        return Objects.equal(this.zza, fidoCredentialDetails.zza) && Objects.equal(this.zzb, fidoCredentialDetails.zzb) && Arrays.equals(this.zzc, fidoCredentialDetails.zzc) && Arrays.equals(this.zzd, fidoCredentialDetails.zzd) && this.zze == fidoCredentialDetails.zze && this.zzf == fidoCredentialDetails.zzf;
    }
    
    public byte[] getCredentialId() {
        return this.zzd;
    }
    
    public boolean getIsDiscoverable() {
        return this.zze;
    }
    
    public boolean getIsPaymentCredential() {
        return this.zzf;
    }
    
    public String getUserDisplayName() {
        return this.zzb;
    }
    
    public byte[] getUserId() {
        return this.zzc;
    }
    
    public String getUserName() {
        return this.zza;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zza, this.zzb, this.zzc, this.zzd, this.zze, this.zzf);
    }
    
    public byte[] serializeToBytes() {
        return SafeParcelableSerializer.serializeToBytes(this);
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.getUserName(), false);
        SafeParcelWriter.writeString(parcel, 2, this.getUserDisplayName(), false);
        SafeParcelWriter.writeByteArray(parcel, 3, this.getUserId(), false);
        SafeParcelWriter.writeByteArray(parcel, 4, this.getCredentialId(), false);
        SafeParcelWriter.writeBoolean(parcel, 5, this.getIsDiscoverable());
        SafeParcelWriter.writeBoolean(parcel, 6, this.getIsPaymentCredential());
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

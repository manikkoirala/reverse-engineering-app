// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2;

import android.app.Activity;

@Deprecated
public interface Fido2PendingIntent
{
    boolean hasPendingIntent();
    
    void launchPendingIntent(final Activity p0, final int p1);
}

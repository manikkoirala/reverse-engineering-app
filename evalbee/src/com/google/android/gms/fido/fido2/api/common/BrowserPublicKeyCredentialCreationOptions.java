// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.SafeParcelableSerializer;
import com.google.android.gms.common.internal.Preconditions;
import android.net.Uri;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

@Class(creator = "BrowserPublicKeyCredentialCreationOptionsCreator")
@Reserved({ 1 })
public class BrowserPublicKeyCredentialCreationOptions extends BrowserRequestOptions
{
    public static final Parcelable$Creator<BrowserPublicKeyCredentialCreationOptions> CREATOR;
    @Field(getter = "getPublicKeyCredentialCreationOptions", id = 2)
    private final PublicKeyCredentialCreationOptions zza;
    @Field(getter = "getOrigin", id = 3)
    private final Uri zzb;
    @Field(getter = "getClientDataHash", id = 4)
    private final byte[] zzc;
    
    static {
        CREATOR = (Parcelable$Creator)new zzn();
    }
    
    @Constructor
    public BrowserPublicKeyCredentialCreationOptions(@Param(id = 2) final PublicKeyCredentialCreationOptions publicKeyCredentialCreationOptions, @Param(id = 3) final Uri zzb, @Param(id = 4) final byte[] zzc) {
        this.zza = Preconditions.checkNotNull(publicKeyCredentialCreationOptions);
        zzc(zzb);
        this.zzb = zzb;
        zzd(zzc);
        this.zzc = zzc;
    }
    
    public static BrowserPublicKeyCredentialCreationOptions deserializeFromBytes(final byte[] array) {
        return SafeParcelableSerializer.deserializeFromBytes(array, BrowserPublicKeyCredentialCreationOptions.CREATOR);
    }
    
    private static Uri zzc(final Uri uri) {
        Preconditions.checkNotNull(uri);
        final String scheme = uri.getScheme();
        final boolean b = true;
        Preconditions.checkArgument(scheme != null, (Object)"origin scheme must be non-empty");
        Preconditions.checkArgument(uri.getAuthority() != null && b, (Object)"origin authority must be non-empty");
        return uri;
    }
    
    private static byte[] zzd(final byte[] array) {
        boolean b = true;
        if (array != null) {
            b = (array.length == 32 && b);
        }
        Preconditions.checkArgument(b, (Object)"clientDataHash must be 32 bytes long");
        return array;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof BrowserPublicKeyCredentialCreationOptions)) {
            return false;
        }
        final BrowserPublicKeyCredentialCreationOptions browserPublicKeyCredentialCreationOptions = (BrowserPublicKeyCredentialCreationOptions)o;
        return Objects.equal(this.zza, browserPublicKeyCredentialCreationOptions.zza) && Objects.equal(this.zzb, browserPublicKeyCredentialCreationOptions.zzb);
    }
    
    @Override
    public AuthenticationExtensions getAuthenticationExtensions() {
        return this.zza.getAuthenticationExtensions();
    }
    
    @Override
    public byte[] getChallenge() {
        return this.zza.getChallenge();
    }
    
    @Override
    public byte[] getClientDataHash() {
        return this.zzc;
    }
    
    @Override
    public Uri getOrigin() {
        return this.zzb;
    }
    
    public PublicKeyCredentialCreationOptions getPublicKeyCredentialCreationOptions() {
        return this.zza;
    }
    
    @Override
    public Integer getRequestId() {
        return this.zza.getRequestId();
    }
    
    @Override
    public Double getTimeoutSeconds() {
        return this.zza.getTimeoutSeconds();
    }
    
    @Override
    public TokenBinding getTokenBinding() {
        return this.zza.getTokenBinding();
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zza, this.zzb);
    }
    
    @Override
    public byte[] serializeToBytes() {
        return SafeParcelableSerializer.serializeToBytes(this);
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeParcelable(parcel, 2, (Parcelable)this.getPublicKeyCredentialCreationOptions(), n, false);
        SafeParcelWriter.writeParcelable(parcel, 3, (Parcelable)this.getOrigin(), n, false);
        SafeParcelWriter.writeByteArray(parcel, 4, this.getClientDataHash(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public static final class Builder
    {
        private PublicKeyCredentialCreationOptions zza;
        private Uri zzb;
        private byte[] zzc;
        
        public BrowserPublicKeyCredentialCreationOptions build() {
            return new BrowserPublicKeyCredentialCreationOptions(this.zza, this.zzb, this.zzc);
        }
        
        public Builder setClientDataHash(final byte[] zzc) {
            BrowserPublicKeyCredentialCreationOptions.zzb(zzc);
            this.zzc = zzc;
            return this;
        }
        
        public Builder setOrigin(final Uri zzb) {
            BrowserPublicKeyCredentialCreationOptions.zza(zzb);
            this.zzb = zzb;
            return this;
        }
        
        public Builder setPublicKeyCredentialCreationOptions(final PublicKeyCredentialCreationOptions zza) {
            this.zza = zza;
            return this;
        }
    }
}

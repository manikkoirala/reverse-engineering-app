// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import com.google.android.gms.internal.fido.zzam;
import java.util.ArrayList;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import java.util.HashSet;
import java.util.Collection;
import java.util.List;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "UvmEntriesCreator")
public class UvmEntries extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<UvmEntries> CREATOR;
    @Field(getter = "getUvmEntryList", id = 1)
    private final List zza;
    
    static {
        CREATOR = (Parcelable$Creator)new zzaz();
    }
    
    @Constructor
    public UvmEntries(@Param(id = 1) final List zza) {
        this.zza = zza;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof UvmEntries;
        final boolean b2 = false;
        if (!b) {
            return false;
        }
        final UvmEntries uvmEntries = (UvmEntries)o;
        final List zza = this.zza;
        if (zza != null || uvmEntries.zza != null) {
            boolean b3 = b2;
            if (zza == null) {
                return b3;
            }
            final List zza2 = uvmEntries.zza;
            b3 = b2;
            if (zza2 == null) {
                return b3;
            }
            b3 = b2;
            if (!zza.containsAll(zza2)) {
                return b3;
            }
            b3 = b2;
            if (!uvmEntries.zza.containsAll(this.zza)) {
                return b3;
            }
        }
        return true;
    }
    
    public List<UvmEntry> getUvmEntryList() {
        return this.zza;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(new HashSet(this.zza));
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeTypedList(parcel, 1, this.getUvmEntryList(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public static final class Builder
    {
        private final List zza;
        
        public Builder() {
            this.zza = new ArrayList();
        }
        
        public Builder addAll(final List<UvmEntry> list) {
            zzam.zzc(this.zza.size() + list.size() <= 3);
            this.zza.addAll(list);
            return this;
        }
        
        public Builder addUvmEntry(final UvmEntry uvmEntry) {
            if (this.zza.size() < 3) {
                this.zza.add(uvmEntry);
                return this;
            }
            throw new IllegalStateException();
        }
        
        public UvmEntries build() {
            return new UvmEntries(this.zza);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import android.os.Parcel;
import android.os.Parcelable$Creator;
import android.os.Parcelable;

public enum PublicKeyCredentialType implements Parcelable
{
    public static final Parcelable$Creator<PublicKeyCredentialType> CREATOR;
    
    PUBLIC_KEY("PUBLIC_KEY", 0, "public-key");
    
    private static final PublicKeyCredentialType[] zza;
    private final String zzb;
    
    static {
        CREATOR = (Parcelable$Creator)new zzaq();
    }
    
    private PublicKeyCredentialType(final String s, final int n, final String s2) {
        this.zzb = "public-key";
    }
    
    public static PublicKeyCredentialType fromString(final String s) {
        for (final PublicKeyCredentialType publicKeyCredentialType : values()) {
            if (s.equals(publicKeyCredentialType.zzb)) {
                return publicKeyCredentialType;
            }
        }
        throw new UnsupportedPublicKeyCredTypeException(String.format("PublicKeyCredentialType %s not supported", s));
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public String toString() {
        return this.zzb;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeString(this.zzb);
    }
    
    public static class UnsupportedPublicKeyCredTypeException extends Exception
    {
        public UnsupportedPublicKeyCredTypeException(final String message) {
            super(message);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import java.util.Arrays;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.SafeParcelableSerializer;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "PublicKeyCredentialCreator")
public class PublicKeyCredential extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<PublicKeyCredential> CREATOR;
    @Field(getter = "getId", id = 1)
    private final String zza;
    @Field(getter = "getType", id = 2)
    private final String zzb;
    @Field(getter = "getRawId", id = 3)
    private final byte[] zzc;
    @Field(getter = "getRegisterResponse", id = 4)
    private final AuthenticatorAttestationResponse zzd;
    @Field(getter = "getSignResponse", id = 5)
    private final AuthenticatorAssertionResponse zze;
    @Field(getter = "getErrorResponse", id = 6)
    private final AuthenticatorErrorResponse zzf;
    @Field(getter = "getClientExtensionResults", id = 7)
    private final AuthenticationExtensionsClientOutputs zzg;
    @Field(getter = "getAuthenticatorAttachment", id = 8)
    private final String zzh;
    
    static {
        CREATOR = (Parcelable$Creator)new zzal();
    }
    
    @Constructor
    public PublicKeyCredential(@Param(id = 1) final String zza, @Param(id = 2) final String zzb, @Param(id = 3) final byte[] zzc, @Param(id = 4) final AuthenticatorAttestationResponse zzd, @Param(id = 5) final AuthenticatorAssertionResponse zze, @Param(id = 6) final AuthenticatorErrorResponse zzf, @Param(id = 7) final AuthenticationExtensionsClientOutputs zzg, @Param(id = 8) final String zzh) {
        final boolean b = true;
        boolean b2 = false;
        Label_0070: {
            if (zzd != null && zze == null) {
                b2 = b;
                if (zzf == null) {
                    break Label_0070;
                }
            }
            if (zzd == null && zze != null) {
                b2 = b;
                if (zzf == null) {
                    break Label_0070;
                }
            }
            b2 = (zzd == null && zze == null && zzf != null && b);
        }
        Preconditions.checkArgument(b2);
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
        this.zzg = zzg;
        this.zzh = zzh;
    }
    
    public static PublicKeyCredential deserializeFromBytes(final byte[] array) {
        return SafeParcelableSerializer.deserializeFromBytes(array, PublicKeyCredential.CREATOR);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof PublicKeyCredential)) {
            return false;
        }
        final PublicKeyCredential publicKeyCredential = (PublicKeyCredential)o;
        return Objects.equal(this.zza, publicKeyCredential.zza) && Objects.equal(this.zzb, publicKeyCredential.zzb) && Arrays.equals(this.zzc, publicKeyCredential.zzc) && Objects.equal(this.zzd, publicKeyCredential.zzd) && Objects.equal(this.zze, publicKeyCredential.zze) && Objects.equal(this.zzf, publicKeyCredential.zzf) && Objects.equal(this.zzg, publicKeyCredential.zzg) && Objects.equal(this.zzh, publicKeyCredential.zzh);
    }
    
    public String getAuthenticatorAttachment() {
        return this.zzh;
    }
    
    public AuthenticationExtensionsClientOutputs getClientExtensionResults() {
        return this.zzg;
    }
    
    public String getId() {
        return this.zza;
    }
    
    public byte[] getRawId() {
        return this.zzc;
    }
    
    public AuthenticatorResponse getResponse() {
        final AuthenticatorAttestationResponse zzd = this.zzd;
        if (zzd != null) {
            return zzd;
        }
        final AuthenticatorAssertionResponse zze = this.zze;
        if (zze != null) {
            return zze;
        }
        final AuthenticatorErrorResponse zzf = this.zzf;
        if (zzf != null) {
            return zzf;
        }
        throw new IllegalStateException("No response set.");
    }
    
    public String getType() {
        return this.zzb;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zza, this.zzb, this.zzc, this.zze, this.zzd, this.zzf, this.zzg, this.zzh);
    }
    
    public byte[] serializeToBytes() {
        return SafeParcelableSerializer.serializeToBytes(this);
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.getId(), false);
        SafeParcelWriter.writeString(parcel, 2, this.getType(), false);
        SafeParcelWriter.writeByteArray(parcel, 3, this.getRawId(), false);
        SafeParcelWriter.writeParcelable(parcel, 4, (Parcelable)this.zzd, n, false);
        SafeParcelWriter.writeParcelable(parcel, 5, (Parcelable)this.zze, n, false);
        SafeParcelWriter.writeParcelable(parcel, 6, (Parcelable)this.zzf, n, false);
        SafeParcelWriter.writeParcelable(parcel, 7, (Parcelable)this.getClientExtensionResults(), n, false);
        SafeParcelWriter.writeString(parcel, 8, this.getAuthenticatorAttachment(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public static class Builder
    {
        private String zza;
        private byte[] zzb;
        private AuthenticatorResponse zzc;
        private AuthenticationExtensionsClientOutputs zzd;
        private String zze;
        
        public PublicKeyCredential build() {
            final AuthenticatorResponse zzc = this.zzc;
            final boolean b = zzc instanceof AuthenticatorAttestationResponse;
            AuthenticatorErrorResponse authenticatorErrorResponse = null;
            AuthenticatorAttestationResponse authenticatorAttestationResponse;
            if (b) {
                authenticatorAttestationResponse = (AuthenticatorAttestationResponse)zzc;
            }
            else {
                authenticatorAttestationResponse = null;
            }
            AuthenticatorAssertionResponse authenticatorAssertionResponse;
            if (zzc instanceof AuthenticatorAssertionResponse) {
                authenticatorAssertionResponse = (AuthenticatorAssertionResponse)zzc;
            }
            else {
                authenticatorAssertionResponse = null;
            }
            if (zzc instanceof AuthenticatorErrorResponse) {
                authenticatorErrorResponse = (AuthenticatorErrorResponse)zzc;
            }
            return new PublicKeyCredential(this.zza, PublicKeyCredentialType.PUBLIC_KEY.toString(), this.zzb, authenticatorAttestationResponse, authenticatorAssertionResponse, authenticatorErrorResponse, this.zzd, this.zze);
        }
        
        public Builder setAuthenticationExtensionsClientOutputs(final AuthenticationExtensionsClientOutputs zzd) {
            this.zzd = zzd;
            return this;
        }
        
        public Builder setAuthenticatorAttachment(final String zze) {
            this.zze = zze;
            return this;
        }
        
        public Builder setId(final String zza) {
            this.zza = zza;
            return this;
        }
        
        public Builder setRawId(final byte[] zzb) {
            this.zzb = zzb;
            return this;
        }
        
        public Builder setResponse(final AuthenticatorResponse zzc) {
            this.zzc = zzc;
            return this;
        }
    }
}

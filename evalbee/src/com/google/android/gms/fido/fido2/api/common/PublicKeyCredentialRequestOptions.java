// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import java.util.Collection;
import com.google.android.gms.common.internal.Objects;
import java.util.Arrays;
import com.google.android.gms.common.internal.safeparcel.SafeParcelableSerializer;
import com.google.android.gms.common.internal.Preconditions;
import java.util.List;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

@Class(creator = "PublicKeyCredentialRequestOptionsCreator")
@Reserved({ 1 })
public class PublicKeyCredentialRequestOptions extends RequestOptions
{
    public static final Parcelable$Creator<PublicKeyCredentialRequestOptions> CREATOR;
    @Field(getter = "getChallenge", id = 2)
    private final byte[] zza;
    @Field(getter = "getTimeoutSeconds", id = 3)
    private final Double zzb;
    @Field(getter = "getRpId", id = 4)
    private final String zzc;
    @Field(getter = "getAllowList", id = 5)
    private final List zzd;
    @Field(getter = "getRequestId", id = 6)
    private final Integer zze;
    @Field(getter = "getTokenBinding", id = 7)
    private final TokenBinding zzf;
    @Field(getter = "getUserVerificationAsString", id = 8, type = "java.lang.String")
    private final zzay zzg;
    @Field(getter = "getAuthenticationExtensions", id = 9)
    private final AuthenticationExtensions zzh;
    @Field(getter = "getLongRequestId", id = 10)
    private final Long zzi;
    
    static {
        CREATOR = (Parcelable$Creator)new zzao();
    }
    
    @Constructor
    public PublicKeyCredentialRequestOptions(@Param(id = 2) final byte[] array, @Param(id = 3) final Double zzb, @Param(id = 4) final String s, @Param(id = 5) final List zzd, @Param(id = 6) final Integer zze, @Param(id = 7) final TokenBinding zzf, @Param(id = 8) final String s2, @Param(id = 9) final AuthenticationExtensions zzh, @Param(id = 10) final Long zzi) {
        this.zza = Preconditions.checkNotNull(array);
        this.zzb = zzb;
        this.zzc = Preconditions.checkNotNull(s);
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
        this.zzi = zzi;
        Label_0087: {
            if (s2 != null) {
                try {
                    this.zzg = zzay.zza(s2);
                    break Label_0087;
                }
                catch (final zzax cause) {
                    throw new IllegalArgumentException(cause);
                }
            }
            this.zzg = null;
        }
        this.zzh = zzh;
    }
    
    public static PublicKeyCredentialRequestOptions deserializeFromBytes(final byte[] array) {
        return SafeParcelableSerializer.deserializeFromBytes(array, PublicKeyCredentialRequestOptions.CREATOR);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof PublicKeyCredentialRequestOptions)) {
            return false;
        }
        final PublicKeyCredentialRequestOptions publicKeyCredentialRequestOptions = (PublicKeyCredentialRequestOptions)o;
        if (Arrays.equals(this.zza, publicKeyCredentialRequestOptions.zza) && Objects.equal(this.zzb, publicKeyCredentialRequestOptions.zzb) && Objects.equal(this.zzc, publicKeyCredentialRequestOptions.zzc)) {
            final List zzd = this.zzd;
            if (zzd != null || publicKeyCredentialRequestOptions.zzd != null) {
                if (zzd == null) {
                    return false;
                }
                final List zzd2 = publicKeyCredentialRequestOptions.zzd;
                if (zzd2 == null || !zzd.containsAll(zzd2) || !publicKeyCredentialRequestOptions.zzd.containsAll(this.zzd)) {
                    return false;
                }
            }
            if (Objects.equal(this.zze, publicKeyCredentialRequestOptions.zze) && Objects.equal(this.zzf, publicKeyCredentialRequestOptions.zzf) && Objects.equal(this.zzg, publicKeyCredentialRequestOptions.zzg) && Objects.equal(this.zzh, publicKeyCredentialRequestOptions.zzh) && Objects.equal(this.zzi, publicKeyCredentialRequestOptions.zzi)) {
                return true;
            }
        }
        return false;
    }
    
    public List<PublicKeyCredentialDescriptor> getAllowList() {
        return this.zzd;
    }
    
    @Override
    public AuthenticationExtensions getAuthenticationExtensions() {
        return this.zzh;
    }
    
    @Override
    public byte[] getChallenge() {
        return this.zza;
    }
    
    @Override
    public Integer getRequestId() {
        return this.zze;
    }
    
    public String getRpId() {
        return this.zzc;
    }
    
    @Override
    public Double getTimeoutSeconds() {
        return this.zzb;
    }
    
    @Override
    public TokenBinding getTokenBinding() {
        return this.zzf;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(Arrays.hashCode(this.zza), this.zzb, this.zzc, this.zzd, this.zze, this.zzf, this.zzg, this.zzh, this.zzi);
    }
    
    @Override
    public byte[] serializeToBytes() {
        return SafeParcelableSerializer.serializeToBytes(this);
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeByteArray(parcel, 2, this.getChallenge(), false);
        SafeParcelWriter.writeDoubleObject(parcel, 3, this.getTimeoutSeconds(), false);
        SafeParcelWriter.writeString(parcel, 4, this.getRpId(), false);
        SafeParcelWriter.writeTypedList(parcel, 5, this.getAllowList(), false);
        SafeParcelWriter.writeIntegerObject(parcel, 6, this.getRequestId(), false);
        SafeParcelWriter.writeParcelable(parcel, 7, (Parcelable)this.getTokenBinding(), n, false);
        final zzay zzg = this.zzg;
        String string;
        if (zzg == null) {
            string = null;
        }
        else {
            string = zzg.toString();
        }
        SafeParcelWriter.writeString(parcel, 8, string, false);
        SafeParcelWriter.writeParcelable(parcel, 9, (Parcelable)this.getAuthenticationExtensions(), n, false);
        SafeParcelWriter.writeLongObject(parcel, 10, this.zzi, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public final zzay zza() {
        return this.zzg;
    }
    
    public static final class Builder
    {
        private byte[] zza;
        private Double zzb;
        private String zzc;
        private List zzd;
        private Integer zze;
        private TokenBinding zzf;
        private zzay zzg;
        private AuthenticationExtensions zzh;
        
        public Builder() {
        }
        
        public Builder(final PublicKeyCredentialRequestOptions publicKeyCredentialRequestOptions) {
            if (publicKeyCredentialRequestOptions != null) {
                this.zza = publicKeyCredentialRequestOptions.getChallenge();
                this.zzb = publicKeyCredentialRequestOptions.getTimeoutSeconds();
                this.zzc = publicKeyCredentialRequestOptions.getRpId();
                this.zzd = publicKeyCredentialRequestOptions.getAllowList();
                this.zze = publicKeyCredentialRequestOptions.getRequestId();
                this.zzf = publicKeyCredentialRequestOptions.getTokenBinding();
                this.zzg = publicKeyCredentialRequestOptions.zza();
                this.zzh = publicKeyCredentialRequestOptions.getAuthenticationExtensions();
            }
        }
        
        public PublicKeyCredentialRequestOptions build() {
            final byte[] zza = this.zza;
            final Double zzb = this.zzb;
            final String zzc = this.zzc;
            final List zzd = this.zzd;
            final Integer zze = this.zze;
            final TokenBinding zzf = this.zzf;
            final zzay zzg = this.zzg;
            String string;
            if (zzg == null) {
                string = null;
            }
            else {
                string = zzg.toString();
            }
            return new PublicKeyCredentialRequestOptions(zza, zzb, zzc, zzd, zze, zzf, string, this.zzh, null);
        }
        
        public Builder setAllowList(final List<PublicKeyCredentialDescriptor> zzd) {
            this.zzd = zzd;
            return this;
        }
        
        public Builder setAuthenticationExtensions(final AuthenticationExtensions zzh) {
            this.zzh = zzh;
            return this;
        }
        
        public Builder setChallenge(final byte[] array) {
            this.zza = Preconditions.checkNotNull(array);
            return this;
        }
        
        public Builder setRequestId(final Integer zze) {
            this.zze = zze;
            return this;
        }
        
        public Builder setRpId(final String s) {
            this.zzc = Preconditions.checkNotNull(s);
            return this;
        }
        
        public Builder setTimeoutSeconds(final Double zzb) {
            this.zzb = zzb;
            return this;
        }
        
        public Builder setTokenBinding(final TokenBinding zzf) {
            this.zzf = zzf;
            return this;
        }
    }
}

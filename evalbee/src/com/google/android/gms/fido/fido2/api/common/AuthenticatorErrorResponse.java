// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.internal.fido.zzaj;
import com.google.android.gms.internal.fido.zzak;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.SafeParcelableSerializer;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

@Class(creator = "AuthenticatorErrorResponseCreator")
@Reserved({ 1 })
public class AuthenticatorErrorResponse extends AuthenticatorResponse
{
    public static final Parcelable$Creator<AuthenticatorErrorResponse> CREATOR;
    @Field(getter = "getErrorCodeAsInt", id = 2, type = "int")
    private final ErrorCode zza;
    @Field(getter = "getErrorMessage", id = 3)
    private final String zzb;
    @Field(defaultValue = "0", getter = "getInternalErrorCode", id = 4, type = "int")
    private final int zzc;
    
    static {
        CREATOR = (Parcelable$Creator)new zzl();
    }
    
    @Constructor
    public AuthenticatorErrorResponse(@Param(id = 2) final int n, @Param(id = 3) final String zzb, @Param(id = 4) final int zzc) {
        try {
            this.zza = ErrorCode.toErrorCode(n);
            this.zzb = zzb;
            this.zzc = zzc;
        }
        catch (final ErrorCode.UnsupportedErrorCodeException cause) {
            throw new IllegalArgumentException(cause);
        }
    }
    
    public static AuthenticatorErrorResponse deserializeFromBytes(final byte[] array) {
        return SafeParcelableSerializer.deserializeFromBytes(array, AuthenticatorErrorResponse.CREATOR);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof AuthenticatorErrorResponse)) {
            return false;
        }
        final AuthenticatorErrorResponse authenticatorErrorResponse = (AuthenticatorErrorResponse)o;
        return Objects.equal(this.zza, authenticatorErrorResponse.zza) && Objects.equal(this.zzb, authenticatorErrorResponse.zzb) && Objects.equal(this.zzc, authenticatorErrorResponse.zzc);
    }
    
    @Override
    public byte[] getClientDataJSON() {
        throw new UnsupportedOperationException();
    }
    
    public ErrorCode getErrorCode() {
        return this.zza;
    }
    
    public int getErrorCodeAsInt() {
        return this.zza.getCode();
    }
    
    public String getErrorMessage() {
        return this.zzb;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zza, this.zzb, this.zzc);
    }
    
    @Override
    public byte[] serializeToBytes() {
        return SafeParcelableSerializer.serializeToBytes(this);
    }
    
    @Override
    public String toString() {
        final zzaj zza = zzak.zza((Object)this);
        zza.zza("errorCode", this.zza.getCode());
        final String zzb = this.zzb;
        if (zzb != null) {
            zza.zzb("errorMessage", (Object)zzb);
        }
        return zza.toString();
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 2, this.getErrorCodeAsInt());
        SafeParcelWriter.writeString(parcel, 3, this.getErrorMessage(), false);
        SafeParcelWriter.writeInt(parcel, 4, this.zzc);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

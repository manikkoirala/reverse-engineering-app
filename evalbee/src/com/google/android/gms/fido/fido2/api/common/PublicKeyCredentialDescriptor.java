// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import java.util.Collection;
import java.util.Arrays;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.fido.common.Transport;
import com.google.android.gms.internal.fido.zzh;
import java.util.List;
import com.google.android.gms.internal.fido.zzau;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "PublicKeyCredentialDescriptorCreator")
@Reserved({ 1 })
public class PublicKeyCredentialDescriptor extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<PublicKeyCredentialDescriptor> CREATOR;
    private static final zzau zza;
    @Field(getter = "getTypeAsString", id = 2, type = "java.lang.String")
    private final PublicKeyCredentialType zzb;
    @Field(getter = "getId", id = 3)
    private final byte[] zzc;
    @Field(getter = "getTransports", id = 4)
    private final List zzd;
    
    static {
        zza = zzau.zzi((Object)zzh.zza, (Object)zzh.zzb);
        CREATOR = (Parcelable$Creator)new zzam();
    }
    
    @Constructor
    public PublicKeyCredentialDescriptor(@Param(id = 2) final String s, @Param(id = 3) final byte[] array, @Param(id = 4) final List<Transport> zzd) {
        Preconditions.checkNotNull(s);
        try {
            this.zzb = PublicKeyCredentialType.fromString(s);
            this.zzc = Preconditions.checkNotNull(array);
            this.zzd = zzd;
        }
        catch (final PublicKeyCredentialType.UnsupportedPublicKeyCredTypeException cause) {
            throw new IllegalArgumentException(cause);
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof PublicKeyCredentialDescriptor)) {
            return false;
        }
        final PublicKeyCredentialDescriptor publicKeyCredentialDescriptor = (PublicKeyCredentialDescriptor)o;
        if (!this.zzb.equals(publicKeyCredentialDescriptor.zzb)) {
            return false;
        }
        if (!Arrays.equals(this.zzc, publicKeyCredentialDescriptor.zzc)) {
            return false;
        }
        final List zzd = this.zzd;
        if (zzd == null && publicKeyCredentialDescriptor.zzd == null) {
            return true;
        }
        if (zzd != null) {
            final List zzd2 = publicKeyCredentialDescriptor.zzd;
            if (zzd2 != null) {
                if (zzd.containsAll(zzd2) && publicKeyCredentialDescriptor.zzd.containsAll(this.zzd)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public byte[] getId() {
        return this.zzc;
    }
    
    public List<Transport> getTransports() {
        return this.zzd;
    }
    
    public PublicKeyCredentialType getType() {
        return this.zzb;
    }
    
    public String getTypeAsString() {
        return this.zzb.toString();
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zzb, Arrays.hashCode(this.zzc), this.zzd);
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 2, this.getTypeAsString(), false);
        SafeParcelWriter.writeByteArray(parcel, 3, this.getId(), false);
        SafeParcelWriter.writeTypedList(parcel, 4, this.getTransports(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public static class UnsupportedPubKeyCredDescriptorException extends Exception
    {
        public UnsupportedPubKeyCredDescriptorException(final String message) {
            super(message);
        }
        
        public UnsupportedPubKeyCredDescriptorException(final String message, final Throwable cause) {
            super(message, cause);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import android.os.Parcel;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.ShowFirstParty;
import android.os.Parcelable;

@ShowFirstParty
public enum zzay implements Parcelable
{
    public static final Parcelable$Creator<zzay> CREATOR;
    
    zza("USER_VERIFICATION_REQUIRED", 0, "required"), 
    zzb("USER_VERIFICATION_PREFERRED", 1, "preferred"), 
    zzc("USER_VERIFICATION_DISCOURAGED", 2, "discouraged");
    
    private static final zzay[] zzd;
    private final String zze;
    
    static {
        CREATOR = (Parcelable$Creator)new zzaw();
    }
    
    private zzay(final String name, final int ordinal, final String zze) {
        this.zze = zze;
    }
    
    public static zzay zza(final String s) {
        for (final zzay zzay : values()) {
            if (s.equals(zzay.zze)) {
                return zzay;
            }
        }
        throw new zzax(s);
    }
    
    public final int describeContents() {
        return 0;
    }
    
    @Override
    public final String toString() {
        return this.zze;
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeString(this.zze);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import java.util.Arrays;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "CableAuthenticationDataCreator")
public final class zzq extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zzq> CREATOR;
    @Field(getter = "getVersion", id = 1)
    private final long zza;
    @Field(getter = "getClientEid", id = 2)
    private final byte[] zzb;
    @Field(getter = "getAuthenticatorEid", id = 3)
    private final byte[] zzc;
    @Field(getter = "getSessionPreKey", id = 4)
    private final byte[] zzd;
    
    static {
        CREATOR = (Parcelable$Creator)new zzr();
    }
    
    @Constructor
    public zzq(@Param(id = 1) final long zza, @Param(id = 2) final byte[] array, @Param(id = 3) final byte[] array2, @Param(id = 4) final byte[] array3) {
        this.zza = zza;
        this.zzb = Preconditions.checkNotNull(array);
        this.zzc = Preconditions.checkNotNull(array2);
        this.zzd = Preconditions.checkNotNull(array3);
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (!(o instanceof zzq)) {
            return false;
        }
        final zzq zzq = (zzq)o;
        return this.zza == zzq.zza && Arrays.equals(this.zzb, zzq.zzb) && Arrays.equals(this.zzc, zzq.zzc) && Arrays.equals(this.zzd, zzq.zzd);
    }
    
    @Override
    public final int hashCode() {
        return Objects.hashCode(this.zza, this.zzb, this.zzc, this.zzd);
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeLong(parcel, 1, this.zza);
        SafeParcelWriter.writeByteArray(parcel, 2, this.zzb, false);
        SafeParcelWriter.writeByteArray(parcel, 3, this.zzc, false);
        SafeParcelWriter.writeByteArray(parcel, 4, this.zzd, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

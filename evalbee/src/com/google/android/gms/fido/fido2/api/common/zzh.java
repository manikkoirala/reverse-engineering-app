// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import java.util.Arrays;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "AuthenticationExtensionsPrfOutputsCreator")
public final class zzh extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zzh> CREATOR;
    @Field(getter = "getSupported", id = 1)
    private final boolean zza;
    @Field(getter = "getOutputs", id = 2)
    private final byte[] zzb;
    
    static {
        CREATOR = (Parcelable$Creator)new zzi();
    }
    
    @Constructor
    public zzh(@Param(id = 1) final boolean zza, @Param(id = 2) final byte[] zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (!(o instanceof zzh)) {
            return false;
        }
        final zzh zzh = (zzh)o;
        return this.zza == zzh.zza && Arrays.equals(this.zzb, zzh.zzb);
    }
    
    @Override
    public final int hashCode() {
        return Objects.hashCode(this.zza, this.zzb);
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeBoolean(parcel, 1, this.zza);
        SafeParcelWriter.writeByteArray(parcel, 2, this.zzb, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

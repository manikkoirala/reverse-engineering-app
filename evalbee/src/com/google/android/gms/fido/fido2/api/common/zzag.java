// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "GoogleTunnelServerIdExtensionCreator")
public final class zzag extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zzag> CREATOR;
    @Field(getter = "getTunnelServerId", id = 1)
    private final String zza;
    
    static {
        CREATOR = (Parcelable$Creator)new zzah();
    }
    
    @Constructor
    public zzag(@Param(id = 1) final String s) {
        this.zza = Preconditions.checkNotNull(s);
    }
    
    @Override
    public final boolean equals(final Object o) {
        return o instanceof zzag && this.zza.equals(((zzag)o).zza);
    }
    
    @Override
    public final int hashCode() {
        return Objects.hashCode(this.zza);
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.zza, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

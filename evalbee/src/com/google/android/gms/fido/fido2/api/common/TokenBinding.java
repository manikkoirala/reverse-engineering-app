// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Arrays;
import com.google.android.gms.internal.fido.zzal;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "TokenBindingCreator")
@Reserved({ 1 })
public class TokenBinding extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<TokenBinding> CREATOR;
    public static final TokenBinding NOT_SUPPORTED;
    public static final TokenBinding SUPPORTED;
    @Field(getter = "getTokenBindingStatusAsString", id = 2, type = "java.lang.String")
    private final TokenBindingStatus zza;
    @Field(getter = "getTokenBindingId", id = 3)
    private final String zzb;
    
    static {
        CREATOR = (Parcelable$Creator)new zzau();
        SUPPORTED = new TokenBinding(TokenBindingStatus.SUPPORTED.toString(), null);
        NOT_SUPPORTED = new TokenBinding(TokenBindingStatus.NOT_SUPPORTED.toString(), null);
    }
    
    public TokenBinding(final String s) {
        this(TokenBindingStatus.PRESENT.toString(), Preconditions.checkNotNull(s));
    }
    
    @Constructor
    public TokenBinding(@Param(id = 2) final String s, @Param(id = 3) final String zzb) {
        Preconditions.checkNotNull(s);
        try {
            this.zza = TokenBindingStatus.fromString(s);
            this.zzb = zzb;
        }
        catch (final UnsupportedTokenBindingStatusException cause) {
            throw new IllegalArgumentException(cause);
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof TokenBinding)) {
            return false;
        }
        final TokenBinding tokenBinding = (TokenBinding)o;
        return zzal.zza((Object)this.zza, (Object)tokenBinding.zza) && zzal.zza((Object)this.zzb, (Object)tokenBinding.zzb);
    }
    
    public String getTokenBindingId() {
        return this.zzb;
    }
    
    public String getTokenBindingStatusAsString() {
        return this.zza.toString();
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] { this.zza, this.zzb });
    }
    
    public JSONObject toJsonObject() {
        try {
            return new JSONObject().put("status", (Object)this.zza).put("id", (Object)this.zzb);
        }
        catch (final JSONException cause) {
            throw new RuntimeException((Throwable)cause);
        }
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 2, this.getTokenBindingStatusAsString(), false);
        SafeParcelWriter.writeString(parcel, 3, this.getTokenBindingId(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public enum TokenBindingStatus implements Parcelable
    {
        public static final Parcelable$Creator<TokenBindingStatus> CREATOR;
        
        NOT_SUPPORTED("NOT_SUPPORTED", 2, "not-supported"), 
        PRESENT("PRESENT", 0, "present"), 
        SUPPORTED("SUPPORTED", 1, "supported");
        
        private static final TokenBindingStatus[] zza;
        private final String zzb;
        
        static {
            CREATOR = (Parcelable$Creator)new zzat();
        }
        
        private TokenBindingStatus(final String name, final int ordinal, final String zzb) {
            this.zzb = zzb;
        }
        
        public static TokenBindingStatus fromString(final String s) {
            for (final TokenBindingStatus tokenBindingStatus : values()) {
                if (s.equals(tokenBindingStatus.zzb)) {
                    return tokenBindingStatus;
                }
            }
            throw new UnsupportedTokenBindingStatusException(s);
        }
        
        public int describeContents() {
            return 0;
        }
        
        @Override
        public String toString() {
            return this.zzb;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            parcel.writeString(this.zzb);
        }
    }
    
    public static class UnsupportedTokenBindingStatusException extends Exception
    {
        public UnsupportedTokenBindingStatusException(final String s) {
            super(String.format("TokenBindingStatus %s not supported", s));
        }
    }
}

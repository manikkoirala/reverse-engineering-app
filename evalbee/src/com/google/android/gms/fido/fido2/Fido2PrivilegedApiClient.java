// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2;

import com.google.android.gms.fido.zza;
import com.google.android.gms.common.Feature;
import com.google.android.gms.fido.fido2.api.common.BrowserPublicKeyCredentialRequestOptions;
import android.app.PendingIntent;
import com.google.android.gms.fido.fido2.api.common.BrowserPublicKeyCredentialCreationOptions;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.common.api.internal.RemoteCall;
import com.google.android.gms.common.api.internal.TaskApiCall;
import com.google.android.gms.fido.fido2.api.common.FidoCredentialDetails;
import java.util.List;
import com.google.android.gms.tasks.Task;
import android.content.Context;
import com.google.android.gms.common.api.internal.StatusExceptionMapper;
import com.google.android.gms.common.api.internal.ApiExceptionMapper;
import android.app.Activity;
import com.google.android.gms.internal.fido.zzj;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApi;

public class Fido2PrivilegedApiClient extends GoogleApi<Api.ApiOptions.NoOptions>
{
    private static final Api.ClientKey zza;
    private static final Api zzb;
    
    static {
        zzb = new Api("Fido.FIDO2_PRIVILEGED_API", (Api.AbstractClientBuilder<C, O>)new zzj(), zza = new Api.ClientKey());
    }
    
    @Deprecated
    public Fido2PrivilegedApiClient(final Activity activity) {
        super(activity, Fido2PrivilegedApiClient.zzb, (Api.ApiOptions)Api.ApiOptions.NO_OPTIONS, new ApiExceptionMapper());
    }
    
    @Deprecated
    public Fido2PrivilegedApiClient(final Context context) {
        super(context, Fido2PrivilegedApiClient.zzb, (Api.ApiOptions)Api.ApiOptions.NO_OPTIONS, new ApiExceptionMapper());
    }
    
    public Task<List<FidoCredentialDetails>> getCredentialList(final String s) {
        return this.doRead((TaskApiCall<Api.AnyClient, List<FidoCredentialDetails>>)TaskApiCall.builder().run(new zzk(this, s)).setMethodKey(5430).build());
    }
    
    @Deprecated
    public Task<Fido2PendingIntent> getRegisterIntent(final BrowserPublicKeyCredentialCreationOptions browserPublicKeyCredentialCreationOptions) {
        return this.doRead((TaskApiCall<Api.AnyClient, Fido2PendingIntent>)TaskApiCall.builder().setMethodKey(5414).run(new zzo(this, browserPublicKeyCredentialCreationOptions)).build());
    }
    
    public Task<PendingIntent> getRegisterPendingIntent(final BrowserPublicKeyCredentialCreationOptions browserPublicKeyCredentialCreationOptions) {
        return this.doRead((TaskApiCall<Api.AnyClient, PendingIntent>)TaskApiCall.builder().run(new zzl(this, browserPublicKeyCredentialCreationOptions)).setMethodKey(5412).build());
    }
    
    @Deprecated
    public Task<Fido2PendingIntent> getSignIntent(final BrowserPublicKeyCredentialRequestOptions browserPublicKeyCredentialRequestOptions) {
        return this.doRead((TaskApiCall<Api.AnyClient, Fido2PendingIntent>)TaskApiCall.builder().setMethodKey(5415).run(new zzn(this, browserPublicKeyCredentialRequestOptions)).build());
    }
    
    public Task<PendingIntent> getSignPendingIntent(final BrowserPublicKeyCredentialRequestOptions browserPublicKeyCredentialRequestOptions) {
        return this.doRead((TaskApiCall<Api.AnyClient, PendingIntent>)TaskApiCall.builder().run(new zzm(this, browserPublicKeyCredentialRequestOptions)).setMethodKey(5413).build());
    }
    
    public Task<Boolean> isUserVerifyingPlatformAuthenticatorAvailable() {
        return this.doRead((TaskApiCall<Api.AnyClient, Boolean>)TaskApiCall.builder().run(new zzp(this)).setFeatures(com.google.android.gms.fido.zza.zzh).setMethodKey(5416).build());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2;

import com.google.android.gms.common.api.internal.TaskUtil;
import android.app.PendingIntent;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.internal.fido.zzq;

final class zzg extends zzq
{
    final TaskCompletionSource zza;
    
    public zzg(final Fido2ApiClient fido2ApiClient, final TaskCompletionSource zza) {
        this.zza = zza;
    }
    
    public final void zzb(final Status status, final PendingIntent pendingIntent) {
        TaskUtil.setResultOrApiException(status, pendingIntent, (com.google.android.gms.tasks.TaskCompletionSource<PendingIntent>)this.zza);
    }
}

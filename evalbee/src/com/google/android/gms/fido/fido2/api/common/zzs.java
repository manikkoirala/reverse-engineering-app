// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import java.util.HashSet;
import java.util.Collection;
import com.google.android.gms.common.internal.Preconditions;
import java.util.List;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "CableAuthenticationExtensionCreator")
public final class zzs extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zzs> CREATOR;
    @Field(getter = "getCableAuthentication", id = 1)
    private final List zza;
    
    static {
        CREATOR = (Parcelable$Creator)new zzt();
    }
    
    @Constructor
    public zzs(@Param(id = 1) final List list) {
        this.zza = Preconditions.checkNotNull(list);
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (!(o instanceof zzs)) {
            return false;
        }
        final zzs zzs = (zzs)o;
        return this.zza.containsAll(zzs.zza) && zzs.zza.containsAll(this.zza);
    }
    
    @Override
    public final int hashCode() {
        return Objects.hashCode(new HashSet(this.zza));
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeTypedList(parcel, 1, (List<Parcelable>)this.zza, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

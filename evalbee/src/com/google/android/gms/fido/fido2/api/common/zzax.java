// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

public final class zzax extends Exception
{
    public zzax(final String s) {
        super(String.format("User verification requirement %s not supported", s));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.SafeParcelableSerializer;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "AuthenticationExtensionsClientOutputsCreator")
public class AuthenticationExtensionsClientOutputs extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<AuthenticationExtensionsClientOutputs> CREATOR;
    @Field(getter = "getUvmEntries", id = 1)
    private final UvmEntries zza;
    @Field(getter = "getDevicePubKey", id = 2)
    private final zzf zzb;
    @Field(getter = "getCredProps", id = 3)
    private final AuthenticationExtensionsCredPropsOutputs zzc;
    @Field(getter = "getPrf", id = 4)
    private final zzh zzd;
    
    static {
        CREATOR = (Parcelable$Creator)new zzc();
    }
    
    @Constructor
    public AuthenticationExtensionsClientOutputs(@Param(id = 1) final UvmEntries zza, @Param(id = 2) final zzf zzb, @Param(id = 3) final AuthenticationExtensionsCredPropsOutputs zzc, @Param(id = 4) final zzh zzd) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
    }
    
    public static AuthenticationExtensionsClientOutputs deserializeFromBytes(final byte[] array) {
        return SafeParcelableSerializer.deserializeFromBytes(array, AuthenticationExtensionsClientOutputs.CREATOR);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof AuthenticationExtensionsClientOutputs)) {
            return false;
        }
        final AuthenticationExtensionsClientOutputs authenticationExtensionsClientOutputs = (AuthenticationExtensionsClientOutputs)o;
        return Objects.equal(this.zza, authenticationExtensionsClientOutputs.zza) && Objects.equal(this.zzb, authenticationExtensionsClientOutputs.zzb) && Objects.equal(this.zzc, authenticationExtensionsClientOutputs.zzc) && Objects.equal(this.zzd, authenticationExtensionsClientOutputs.zzd);
    }
    
    public AuthenticationExtensionsCredPropsOutputs getCredProps() {
        return this.zzc;
    }
    
    public UvmEntries getUvmEntries() {
        return this.zza;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zza, this.zzb, this.zzc, this.zzd);
    }
    
    public byte[] serializeToBytes() {
        return SafeParcelableSerializer.serializeToBytes(this);
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeParcelable(parcel, 1, (Parcelable)this.getUvmEntries(), n, false);
        SafeParcelWriter.writeParcelable(parcel, 2, (Parcelable)this.zzb, n, false);
        SafeParcelWriter.writeParcelable(parcel, 3, (Parcelable)this.getCredProps(), n, false);
        SafeParcelWriter.writeParcelable(parcel, 4, (Parcelable)this.zzd, n, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public static final class Builder
    {
        private UvmEntries zza;
        private AuthenticationExtensionsCredPropsOutputs zzb;
        
        public AuthenticationExtensionsClientOutputs build() {
            return new AuthenticationExtensionsClientOutputs(this.zza, null, this.zzb, null);
        }
        
        public Builder setCredProps(final AuthenticationExtensionsCredPropsOutputs zzb) {
            this.zzb = zzb;
            return this;
        }
        
        public Builder setUserVerificationMethodEntries(final UvmEntries zza) {
            this.zza = zza;
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.safeparcel.SafeParcelableSerializer;
import com.google.android.gms.common.internal.Preconditions;
import android.net.Uri;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

@Class(creator = "BrowserPublicKeyCredentialRequestOptionsCreator")
@Reserved({ 1 })
public class BrowserPublicKeyCredentialRequestOptions extends BrowserRequestOptions
{
    public static final Parcelable$Creator<BrowserPublicKeyCredentialRequestOptions> CREATOR;
    @Field(getter = "getPublicKeyCredentialRequestOptions", id = 2)
    private final PublicKeyCredentialRequestOptions zza;
    @Field(getter = "getOrigin", id = 3)
    private final Uri zzb;
    @Field(getter = "getClientDataHash", id = 4)
    private final byte[] zzc;
    
    static {
        CREATOR = (Parcelable$Creator)new zzo();
    }
    
    @Constructor
    public BrowserPublicKeyCredentialRequestOptions(@Param(id = 2) final PublicKeyCredentialRequestOptions publicKeyCredentialRequestOptions, @Param(id = 3) final Uri zzb, @Param(id = 4) final byte[] zzc) {
        this.zza = Preconditions.checkNotNull(publicKeyCredentialRequestOptions);
        zzc(zzb);
        this.zzb = zzb;
        zzd(zzc);
        this.zzc = zzc;
    }
    
    public static BrowserPublicKeyCredentialRequestOptions deserializeFromBytes(final byte[] array) {
        return SafeParcelableSerializer.deserializeFromBytes(array, BrowserPublicKeyCredentialRequestOptions.CREATOR);
    }
    
    private static Uri zzc(final Uri uri) {
        Preconditions.checkNotNull(uri);
        final String scheme = uri.getScheme();
        final boolean b = true;
        Preconditions.checkArgument(scheme != null, (Object)"origin scheme must be non-empty");
        Preconditions.checkArgument(uri.getAuthority() != null && b, (Object)"origin authority must be non-empty");
        return uri;
    }
    
    private static byte[] zzd(final byte[] array) {
        boolean b = true;
        if (array != null) {
            b = (array.length == 32 && b);
        }
        Preconditions.checkArgument(b, (Object)"clientDataHash must be 32 bytes long");
        return array;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof BrowserPublicKeyCredentialRequestOptions)) {
            return false;
        }
        final BrowserPublicKeyCredentialRequestOptions browserPublicKeyCredentialRequestOptions = (BrowserPublicKeyCredentialRequestOptions)o;
        return Objects.equal(this.zza, browserPublicKeyCredentialRequestOptions.zza) && Objects.equal(this.zzb, browserPublicKeyCredentialRequestOptions.zzb);
    }
    
    @Override
    public AuthenticationExtensions getAuthenticationExtensions() {
        return this.zza.getAuthenticationExtensions();
    }
    
    @Override
    public byte[] getChallenge() {
        return this.zza.getChallenge();
    }
    
    @Override
    public byte[] getClientDataHash() {
        return this.zzc;
    }
    
    @Override
    public Uri getOrigin() {
        return this.zzb;
    }
    
    public PublicKeyCredentialRequestOptions getPublicKeyCredentialRequestOptions() {
        return this.zza;
    }
    
    @Override
    public Integer getRequestId() {
        return this.zza.getRequestId();
    }
    
    @Override
    public Double getTimeoutSeconds() {
        return this.zza.getTimeoutSeconds();
    }
    
    @Override
    public TokenBinding getTokenBinding() {
        return this.zza.getTokenBinding();
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zza, this.zzb);
    }
    
    @Override
    public byte[] serializeToBytes() {
        return SafeParcelableSerializer.serializeToBytes(this);
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeParcelable(parcel, 2, (Parcelable)this.getPublicKeyCredentialRequestOptions(), n, false);
        SafeParcelWriter.writeParcelable(parcel, 3, (Parcelable)this.getOrigin(), n, false);
        SafeParcelWriter.writeByteArray(parcel, 4, this.getClientDataHash(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public static final class Builder
    {
        private PublicKeyCredentialRequestOptions zza;
        private Uri zzb;
        private byte[] zzc;
        
        public BrowserPublicKeyCredentialRequestOptions build() {
            return new BrowserPublicKeyCredentialRequestOptions(this.zza, this.zzb, this.zzc);
        }
        
        public Builder setClientDataHash(final byte[] zzc) {
            BrowserPublicKeyCredentialRequestOptions.zzb(zzc);
            this.zzc = zzc;
            return this;
        }
        
        public Builder setOrigin(final Uri zzb) {
            BrowserPublicKeyCredentialRequestOptions.zza(zzb);
            this.zzb = zzb;
            return this;
        }
        
        public Builder setPublicKeyCredentialRequestOptions(final PublicKeyCredentialRequestOptions publicKeyCredentialRequestOptions) {
            this.zza = Preconditions.checkNotNull(publicKeyCredentialRequestOptions);
            return this;
        }
    }
}

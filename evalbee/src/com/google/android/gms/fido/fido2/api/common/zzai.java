// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import java.util.Arrays;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "PrfExtensionCreator")
public final class zzai extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zzai> CREATOR;
    @Field(getter = "getEvaluationPoints", id = 1)
    private final byte[][] zza;
    
    static {
        CREATOR = (Parcelable$Creator)new zzaj();
    }
    
    @Constructor
    public zzai(@Param(id = 1) final byte[][] zza) {
        Preconditions.checkArgument(zza != null);
        Preconditions.checkArgument(0x1 == ((zza.length & 0x1) ^ 0x1));
        for (int i = 0; i < zza.length; i += 2) {
            Preconditions.checkArgument(i == 0 || zza[i] != null);
            final int n = i + 1;
            Preconditions.checkArgument(zza[n] != null);
            final int length = zza[n].length;
            Preconditions.checkArgument(length == 32 || length == 64);
        }
        this.zza = zza;
    }
    
    @Override
    public final boolean equals(final Object o) {
        return o instanceof zzai && Arrays.deepEquals(this.zza, ((zzai)o).zza);
    }
    
    @Override
    public final int hashCode() {
        final byte[][] zza = this.zza;
        final int length = zza.length;
        int i = 0;
        int n = 0;
        while (i < length) {
            n ^= Objects.hashCode(zza[i]);
            ++i;
        }
        return n;
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeByteArrayArray(parcel, 1, this.zza, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

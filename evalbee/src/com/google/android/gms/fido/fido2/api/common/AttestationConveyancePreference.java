// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import android.os.Parcel;
import android.os.Parcelable$Creator;
import android.os.Parcelable;

public enum AttestationConveyancePreference implements Parcelable
{
    public static final Parcelable$Creator<AttestationConveyancePreference> CREATOR;
    
    DIRECT("DIRECT", 2, "direct"), 
    INDIRECT("INDIRECT", 1, "indirect"), 
    NONE("NONE", 0, "none");
    
    private static final AttestationConveyancePreference[] zza;
    private final String zzb;
    
    static {
        CREATOR = (Parcelable$Creator)new zzb();
    }
    
    private AttestationConveyancePreference(final String name, final int ordinal, final String zzb) {
        this.zzb = zzb;
    }
    
    public static AttestationConveyancePreference fromString(final String s) {
        for (final AttestationConveyancePreference attestationConveyancePreference : values()) {
            if (s.equals(attestationConveyancePreference.zzb)) {
                return attestationConveyancePreference;
            }
        }
        throw new UnsupportedAttestationConveyancePreferenceException(s);
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public String toString() {
        return this.zzb;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeString(this.zzb);
    }
    
    public static class UnsupportedAttestationConveyancePreferenceException extends Exception
    {
        public UnsupportedAttestationConveyancePreferenceException(final String s) {
            super(String.format("Attestation conveyance preference %s not supported", s));
        }
    }
}

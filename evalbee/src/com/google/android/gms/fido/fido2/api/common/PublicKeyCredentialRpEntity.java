// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.fido2.api.common;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "PublicKeyCredentialRpEntityCreator")
@Reserved({ 1 })
public class PublicKeyCredentialRpEntity extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<PublicKeyCredentialRpEntity> CREATOR;
    @Field(getter = "getId", id = 2)
    private final String zza;
    @Field(getter = "getName", id = 3)
    private final String zzb;
    @Field(getter = "getIcon", id = 4)
    private final String zzc;
    
    static {
        CREATOR = (Parcelable$Creator)new zzap();
    }
    
    @Constructor
    public PublicKeyCredentialRpEntity(@Param(id = 2) final String s, @Param(id = 3) final String s2, @Param(id = 4) final String zzc) {
        this.zza = Preconditions.checkNotNull(s);
        this.zzb = Preconditions.checkNotNull(s2);
        this.zzc = zzc;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof PublicKeyCredentialRpEntity)) {
            return false;
        }
        final PublicKeyCredentialRpEntity publicKeyCredentialRpEntity = (PublicKeyCredentialRpEntity)o;
        return Objects.equal(this.zza, publicKeyCredentialRpEntity.zza) && Objects.equal(this.zzb, publicKeyCredentialRpEntity.zzb) && Objects.equal(this.zzc, publicKeyCredentialRpEntity.zzc);
    }
    
    public String getIcon() {
        return this.zzc;
    }
    
    public String getId() {
        return this.zza;
    }
    
    public String getName() {
        return this.zzb;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zza, this.zzb, this.zzc);
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 2, this.getId(), false);
        SafeParcelWriter.writeString(parcel, 3, this.getName(), false);
        SafeParcelWriter.writeString(parcel, 4, this.getIcon(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

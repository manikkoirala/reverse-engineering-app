// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido;

import com.google.android.gms.common.Feature;

public final class zza
{
    public static final Feature zza;
    public static final Feature zzb;
    public static final Feature zzc;
    public static final Feature zzd;
    public static final Feature zze;
    public static final Feature zzf;
    public static final Feature zzg;
    public static final Feature zzh;
    public static final Feature zzi;
    public static final Feature zzj;
    public static final Feature zzk;
    public static final Feature zzl;
    public static final Feature zzm;
    public static final Feature zzn;
    public static final Feature zzo;
    public static final Feature zzp;
    public static final Feature zzq;
    public static final Feature zzr;
    public static final Feature zzs;
    public static final Feature[] zzt;
    
    static {
        zzt = new Feature[] { zza = new Feature("cancel_target_direct_transfer", 1L), zzb = new Feature("delete_credential", 1L), zzc = new Feature("delete_device_public_key", 1L), zzd = new Feature("get_or_generate_device_public_key", 1L), zze = new Feature("get_passkeys", 1L), zzf = new Feature("update_passkey", 1L), zzg = new Feature("is_user_verifying_platform_authenticator_available_for_credential", 1L), zzh = new Feature("is_user_verifying_platform_authenticator_available", 1L), zzi = new Feature("privileged_api_list_credentials", 2L), zzj = new Feature("start_target_direct_transfer", 1L), zzk = new Feature("zero_party_api_register", 3L), zzl = new Feature("zero_party_api_sign", 3L), zzm = new Feature("zero_party_api_list_discoverable_credentials", 2L), zzn = new Feature("zero_party_api_authenticate_passkey", 1L), zzo = new Feature("zero_party_api_register_passkey", 1L), zzp = new Feature("zero_party_api_get_hybrid_client_registration_pending_intent", 1L), zzq = new Feature("zero_party_api_get_hybrid_client_sign_pending_intent", 1L), zzr = new Feature("get_browser_hybrid_client_sign_pending_intent", 1L), zzs = new Feature("get_browser_hybrid_client_registration_pending_intent", 1L) };
    }
}

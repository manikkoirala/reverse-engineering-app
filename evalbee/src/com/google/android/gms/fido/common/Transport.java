// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.common;

import android.os.Parcel;
import java.util.Collection;
import java.util.ArrayList;
import android.util.Log;
import java.util.HashSet;
import java.util.List;
import org.json.JSONArray;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.ReflectedParcelable;

public enum Transport implements ReflectedParcelable
{
    BLUETOOTH_CLASSIC("BLUETOOTH_CLASSIC", 0, "bt"), 
    BLUETOOTH_LOW_ENERGY("BLUETOOTH_LOW_ENERGY", 1, "ble");
    
    public static final Parcelable$Creator<Transport> CREATOR;
    
    HYBRID("HYBRID", 5, "cable"), 
    INTERNAL("INTERNAL", 4, "internal"), 
    NFC("NFC", 2, "nfc"), 
    USB("USB", 3, "usb");
    
    private static final Transport[] zza;
    private final String zzb;
    
    static {
        CREATOR = (Parcelable$Creator)new zza();
    }
    
    private Transport(final String name, final int ordinal, final String zzb) {
        this.zzb = zzb;
    }
    
    public static Transport fromString(final String s) {
        for (final Transport transport : values()) {
            if (s.equals(transport.zzb)) {
                return transport;
            }
        }
        if (s.equals("hybrid")) {
            return Transport.HYBRID;
        }
        throw new UnsupportedTransportException(String.format("Transport %s not supported", s));
    }
    
    public static List<Transport> parseTransports(final JSONArray jsonArray) {
        if (jsonArray == null) {
            return null;
        }
        final HashSet c = new HashSet(jsonArray.length());
        for (int i = 0; i < jsonArray.length(); ++i) {
            final String string = jsonArray.getString(i);
            if (string != null && !string.isEmpty()) {
                try {
                    c.add(fromString(string));
                }
                catch (final UnsupportedTransportException ex) {
                    Log.w("Transport", "Ignoring unrecognized transport ".concat(string));
                }
            }
        }
        return new ArrayList<Transport>(c);
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public String toString() {
        return this.zzb;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeString(this.zzb);
    }
    
    public static class UnsupportedTransportException extends Exception
    {
        public UnsupportedTransportException(final String message) {
            super(message);
        }
    }
}

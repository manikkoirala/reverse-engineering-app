// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.u2f.api.common;

import org.json.JSONException;
import org.json.JSONObject;
import com.google.android.gms.common.internal.Preconditions;

@Deprecated
public class ClientData
{
    public static final String KEY_CHALLENGE = "challenge";
    public static final String KEY_CID_PUBKEY = "cid_pubkey";
    public static final String KEY_ORIGIN = "origin";
    public static final String KEY_TYPE = "typ";
    public static final String TYPE_FINISH_ENROLLMENT = "navigator.id.finishEnrollment";
    public static final String TYPE_GET_ASSERTION = "navigator.id.getAssertion";
    private final String zza;
    private final String zzb;
    private final String zzc;
    private final ChannelIdValue zzd;
    
    public ClientData(final String s, final String s2, final String s3, final ChannelIdValue channelIdValue) {
        this.zza = Preconditions.checkNotNull(s);
        this.zzb = Preconditions.checkNotNull(s2);
        this.zzc = Preconditions.checkNotNull(s3);
        this.zzd = Preconditions.checkNotNull(channelIdValue);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ClientData)) {
            return false;
        }
        final ClientData clientData = (ClientData)o;
        return this.zza.equals(clientData.zza) && this.zzb.equals(clientData.zzb) && this.zzc.equals(clientData.zzc) && this.zzd.equals(clientData.zzd);
    }
    
    @Override
    public int hashCode() {
        return (((this.zza.hashCode() + 31) * 31 + this.zzb.hashCode()) * 31 + this.zzc.hashCode()) * 31 + this.zzd.hashCode();
    }
    
    public String toJsonString() {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("typ", (Object)this.zza);
            jsonObject.put("challenge", (Object)this.zzb);
            jsonObject.put("origin", (Object)this.zzc);
            final ChannelIdValue.ChannelIdValueType absent = ChannelIdValue.ChannelIdValueType.ABSENT;
            final int ordinal = this.zzd.getType().ordinal();
            Object o;
            if (ordinal != 1) {
                if (ordinal != 2) {
                    return jsonObject.toString();
                }
                o = this.zzd.getObjectValue();
            }
            else {
                o = this.zzd.getStringValue();
            }
            jsonObject.put("cid_pubkey", o);
            return jsonObject.toString();
        }
        catch (final JSONException cause) {
            throw new RuntimeException((Throwable)cause);
        }
    }
    
    public static class Builder implements Cloneable
    {
        private String zza;
        private String zzb;
        private String zzc;
        private ChannelIdValue zzd;
        
        public Builder() {
            this.zzd = ChannelIdValue.ABSENT;
        }
        
        public Builder(final String zza, final String zzb, final String zzc, final ChannelIdValue zzd) {
            this.zza = zza;
            this.zzb = zzb;
            this.zzc = zzc;
            this.zzd = zzd;
        }
        
        public static Builder newInstance() {
            return new Builder();
        }
        
        public ClientData build() {
            return new ClientData(this.zza, this.zzb, this.zzc, this.zzd);
        }
        
        public Builder clone() {
            return new Builder(this.zza, this.zzb, this.zzc, this.zzd);
        }
        
        public Builder setChallenge(final String zzb) {
            this.zzb = zzb;
            return this;
        }
        
        public Builder setChannelId(final ChannelIdValue zzd) {
            this.zzd = zzd;
            return this;
        }
        
        public Builder setOrigin(final String zzc) {
            this.zzc = zzc;
            return this;
        }
        
        public Builder setType(final String zza) {
            this.zza = zza;
            return this;
        }
    }
}

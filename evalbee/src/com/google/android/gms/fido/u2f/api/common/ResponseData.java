// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.u2f.api.common;

import org.json.JSONObject;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Deprecated
public abstract class ResponseData extends AbstractSafeParcelable implements ReflectedParcelable
{
    public abstract JSONObject toJsonObject();
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.u2f.api.common;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.internal.fido.zzaj;
import com.google.android.gms.internal.fido.zzbf;
import com.google.android.gms.internal.fido.zzak;
import org.json.JSONException;
import android.util.Base64;
import org.json.JSONObject;
import com.google.android.gms.common.internal.Objects;
import java.util.Arrays;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.VisibleForTesting;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

@Deprecated
@Class(creator = "SignResponseDataCreator")
@Reserved({ 1 })
public class SignResponseData extends ResponseData
{
    public static final Parcelable$Creator<SignResponseData> CREATOR;
    @VisibleForTesting
    public static final String JSON_RESPONSE_DATA_CLIENT_DATA = "clientData";
    @VisibleForTesting
    public static final String JSON_RESPONSE_DATA_KEY_HANDLE = "keyHandle";
    @VisibleForTesting
    public static final String JSON_RESPONSE_DATA_SIGNATURE_DATA = "signatureData";
    @Field(getter = "getKeyHandle", id = 2)
    private final byte[] zza;
    @Field(getter = "getClientDataString", id = 3)
    private final String zzb;
    @Field(getter = "getSignatureData", id = 4)
    private final byte[] zzc;
    @Field(getter = "getApplication", id = 5)
    private final byte[] zzd;
    
    static {
        CREATOR = (Parcelable$Creator)new zzl();
    }
    
    @Deprecated
    public SignResponseData(final byte[] array, final String s, final byte[] array2) {
        this(array, s, array2, new byte[0]);
    }
    
    @Constructor
    public SignResponseData(@Param(id = 2) final byte[] array, @Param(id = 3) final String s, @Param(id = 4) final byte[] array2, @Param(id = 5) final byte[] array3) {
        this.zza = Preconditions.checkNotNull(array);
        this.zzb = Preconditions.checkNotNull(s);
        this.zzc = Preconditions.checkNotNull(array2);
        this.zzd = Preconditions.checkNotNull(array3);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof SignResponseData)) {
            return false;
        }
        final SignResponseData signResponseData = (SignResponseData)o;
        return Arrays.equals(this.zza, signResponseData.zza) && Objects.equal(this.zzb, signResponseData.zzb) && Arrays.equals(this.zzc, signResponseData.zzc) && Arrays.equals(this.zzd, signResponseData.zzd);
    }
    
    public String getClientDataString() {
        return this.zzb;
    }
    
    public byte[] getKeyHandle() {
        return this.zza;
    }
    
    public byte[] getSignatureData() {
        return this.zzc;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(Arrays.hashCode(this.zza), this.zzb, Arrays.hashCode(this.zzc), Arrays.hashCode(this.zzd));
    }
    
    @Override
    public JSONObject toJsonObject() {
        try {
            final JSONObject jsonObject = new JSONObject();
            jsonObject.put("keyHandle", (Object)Base64.encodeToString(this.zza, 11));
            jsonObject.put("clientData", (Object)Base64.encodeToString(this.zzb.getBytes(), 11));
            jsonObject.put("signatureData", (Object)Base64.encodeToString(this.zzc, 11));
            return jsonObject;
        }
        catch (final JSONException cause) {
            throw new RuntimeException((Throwable)cause);
        }
    }
    
    @Override
    public String toString() {
        final zzaj zza = zzak.zza((Object)this);
        final zzbf zzd = zzbf.zzd();
        final byte[] zza2 = this.zza;
        zza.zzb("keyHandle", (Object)zzd.zze(zza2, 0, zza2.length));
        zza.zzb("clientDataString", (Object)this.zzb);
        final zzbf zzd2 = zzbf.zzd();
        final byte[] zzc = this.zzc;
        zza.zzb("signatureData", (Object)zzd2.zze(zzc, 0, zzc.length));
        final zzbf zzd3 = zzbf.zzd();
        final byte[] zzd4 = this.zzd;
        zza.zzb("application", (Object)zzd3.zze(zzd4, 0, zzd4.length));
        return zza.toString();
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeByteArray(parcel, 2, this.getKeyHandle(), false);
        SafeParcelWriter.writeString(parcel, 3, this.getClientDataString(), false);
        SafeParcelWriter.writeByteArray(parcel, 4, this.getSignatureData(), false);
        SafeParcelWriter.writeByteArray(parcel, 5, this.zzd, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.u2f;

import com.google.android.gms.fido.u2f.api.common.SignRequestParams;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.common.api.internal.RemoteCall;
import com.google.android.gms.common.api.internal.TaskApiCall;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.fido.u2f.api.common.RegisterRequestParams;
import android.content.Context;
import com.google.android.gms.common.api.internal.StatusExceptionMapper;
import com.google.android.gms.common.api.internal.ApiExceptionMapper;
import android.app.Activity;
import com.google.android.gms.internal.fido.zzx;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApi;

@Deprecated
public class U2fApiClient extends GoogleApi<Api.ApiOptions.NoOptions>
{
    private static final Api.ClientKey zza;
    private static final Api zzb;
    
    static {
        zzb = new Api("Fido.U2F_API", (Api.AbstractClientBuilder<C, O>)new zzx(), zza = new Api.ClientKey());
    }
    
    public U2fApiClient(final Activity activity) {
        super(activity, U2fApiClient.zzb, (Api.ApiOptions)Api.ApiOptions.NO_OPTIONS, new ApiExceptionMapper());
    }
    
    public U2fApiClient(final Context context) {
        super(context, U2fApiClient.zzb, (Api.ApiOptions)Api.ApiOptions.NO_OPTIONS, new ApiExceptionMapper());
    }
    
    public Task<U2fPendingIntent> getRegisterIntent(final RegisterRequestParams registerRequestParams) {
        return this.doRead((TaskApiCall<Api.AnyClient, U2fPendingIntent>)TaskApiCall.builder().setMethodKey(5424).run(new zzb(this, registerRequestParams)).build());
    }
    
    public Task<U2fPendingIntent> getSignIntent(final SignRequestParams signRequestParams) {
        return this.doRead((TaskApiCall<Api.AnyClient, U2fPendingIntent>)TaskApiCall.builder().setMethodKey(5425).run(new zza(this, signRequestParams)).build());
    }
}

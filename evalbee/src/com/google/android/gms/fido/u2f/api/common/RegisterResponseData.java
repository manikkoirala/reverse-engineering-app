// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.u2f.api.common;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.internal.fido.zzaj;
import com.google.android.gms.internal.fido.zzbf;
import com.google.android.gms.internal.fido.zzak;
import org.json.JSONException;
import android.util.Base64;
import org.json.JSONObject;
import java.util.Arrays;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

@Deprecated
@Class(creator = "RegisterResponseDataCreator")
@Reserved({ 1 })
public class RegisterResponseData extends ResponseData
{
    public static final Parcelable$Creator<RegisterResponseData> CREATOR;
    @Field(getter = "getRegisterData", id = 2)
    private final byte[] zza;
    @Field(getter = "getProtocolVersionAsString", id = 3, type = "java.lang.String")
    private final ProtocolVersion zzb;
    @Field(getter = "getClientDataString", id = 4)
    private final String zzc;
    
    static {
        CREATOR = (Parcelable$Creator)new zzi();
    }
    
    public RegisterResponseData(final byte[] array) {
        this.zza = Preconditions.checkNotNull(array);
        this.zzb = ProtocolVersion.V1;
        this.zzc = null;
    }
    
    public RegisterResponseData(final byte[] array, final ProtocolVersion protocolVersion, final String s) {
        this.zza = Preconditions.checkNotNull(array);
        this.zzb = Preconditions.checkNotNull(protocolVersion);
        final ProtocolVersion unknown = ProtocolVersion.UNKNOWN;
        final boolean b = true;
        Preconditions.checkArgument(protocolVersion != unknown);
        String zzc;
        if (protocolVersion == ProtocolVersion.V1) {
            Preconditions.checkArgument(s == null && b);
            zzc = null;
        }
        else {
            zzc = Preconditions.checkNotNull(s);
        }
        this.zzc = zzc;
    }
    
    @Constructor
    public RegisterResponseData(@Param(id = 2) final byte[] zza, @Param(id = 3) final String s, @Param(id = 4) final String zzc) {
        this.zza = zza;
        try {
            this.zzb = ProtocolVersion.fromString(s);
            this.zzc = zzc;
        }
        catch (final ProtocolVersion.UnsupportedProtocolException cause) {
            throw new IllegalArgumentException(cause);
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof RegisterResponseData)) {
            return false;
        }
        final RegisterResponseData registerResponseData = (RegisterResponseData)o;
        return Objects.equal(this.zzb, registerResponseData.zzb) && Arrays.equals(this.zza, registerResponseData.zza) && Objects.equal(this.zzc, registerResponseData.zzc);
    }
    
    public String getClientDataString() {
        return this.zzc;
    }
    
    public ProtocolVersion getProtocolVersion() {
        return this.zzb;
    }
    
    public byte[] getRegisterData() {
        return this.zza;
    }
    
    public int getVersionCode() {
        final ProtocolVersion unknown = ProtocolVersion.UNKNOWN;
        final int ordinal = this.zzb.ordinal();
        int n = 1;
        if (ordinal != 1) {
            n = 2;
            if (ordinal != 2) {
                return -1;
            }
        }
        return n;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zzb, Arrays.hashCode(this.zza), this.zzc);
    }
    
    @Override
    public JSONObject toJsonObject() {
        try {
            final JSONObject jsonObject = new JSONObject();
            jsonObject.put("registrationData", (Object)Base64.encodeToString(this.zza, 11));
            jsonObject.put("version", (Object)this.zzb.toString());
            final String zzc = this.zzc;
            if (zzc != null) {
                jsonObject.put("clientData", (Object)Base64.encodeToString(zzc.getBytes(), 11));
            }
            return jsonObject;
        }
        catch (final JSONException cause) {
            throw new RuntimeException((Throwable)cause);
        }
    }
    
    @Override
    public String toString() {
        final zzaj zza = zzak.zza((Object)this);
        zza.zzb("protocolVersion", (Object)this.zzb);
        final zzbf zzd = zzbf.zzd();
        final byte[] zza2 = this.zza;
        zza.zzb("registerData", (Object)zzd.zze(zza2, 0, zza2.length));
        final String zzc = this.zzc;
        if (zzc != null) {
            zza.zzb("clientDataString", (Object)zzc);
        }
        return zza.toString();
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeByteArray(parcel, 2, this.getRegisterData(), false);
        SafeParcelWriter.writeString(parcel, 3, this.zzb.toString(), false);
        SafeParcelWriter.writeString(parcel, 4, this.getClientDataString(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

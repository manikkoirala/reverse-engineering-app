// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.u2f.api.messagebased;

@Deprecated
public enum ResponseType
{
    REGISTER("REGISTER", 0, "u2f_register_response"), 
    SIGN("SIGN", 1, "u2f_sign_response");
    
    private static final ResponseType[] zza;
    private final String zzb;
    
    private ResponseType(final String name, final int ordinal, final String zzb) {
        this.zzb = zzb;
    }
    
    public static ResponseType getResponseTypeForRequestType(final RequestType requestType) {
        if (requestType == null) {
            throw new RequestType.UnsupportedRequestTypeException((String)null);
        }
        final int ordinal = requestType.ordinal();
        if (ordinal == 0) {
            return ResponseType.REGISTER;
        }
        if (ordinal == 1) {
            return ResponseType.SIGN;
        }
        throw new RequestType.UnsupportedRequestTypeException(requestType.toString());
    }
    
    @Override
    public String toString() {
        return this.zzb;
    }
}

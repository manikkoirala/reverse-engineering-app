// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.u2f.api.messagebased;

@Deprecated
public enum RequestType
{
    REGISTER("REGISTER", 0, "u2f_register_request"), 
    SIGN("SIGN", 1, "u2f_sign_request");
    
    private static final RequestType[] zza;
    private final String zzb;
    
    private RequestType(final String name, final int ordinal, final String zzb) {
        this.zzb = zzb;
    }
    
    public static RequestType fromString(final String s) {
        for (final RequestType requestType : values()) {
            if (s.equals(requestType.zzb)) {
                return requestType;
            }
        }
        throw new UnsupportedRequestTypeException(s);
    }
    
    @Override
    public String toString() {
        return this.zzb;
    }
    
    public static class UnsupportedRequestTypeException extends Exception
    {
        public UnsupportedRequestTypeException(final String obj) {
            super("Unsupported request type ".concat(String.valueOf(obj)));
        }
    }
}

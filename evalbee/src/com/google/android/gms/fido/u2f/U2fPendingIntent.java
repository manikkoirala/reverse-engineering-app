// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.u2f;

import android.app.Activity;

@Deprecated
public interface U2fPendingIntent
{
    boolean hasPendingIntent();
    
    void launchPendingIntent(final Activity p0, final int p1);
}

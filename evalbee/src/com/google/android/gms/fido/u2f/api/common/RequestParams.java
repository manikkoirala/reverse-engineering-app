// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.u2f.api.common;

import java.util.List;
import android.net.Uri;
import java.util.Set;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Deprecated
public abstract class RequestParams extends AbstractSafeParcelable implements ReflectedParcelable
{
    public abstract Set<Uri> getAllAppIds();
    
    public abstract Uri getAppId();
    
    public abstract ChannelIdValue getChannelIdValue();
    
    public abstract String getDisplayHint();
    
    public abstract List<RegisteredKey> getRegisteredKeys();
    
    public abstract Integer getRequestId();
    
    public abstract Double getTimeoutSeconds();
}

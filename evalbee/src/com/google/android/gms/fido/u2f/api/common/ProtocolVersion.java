// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.u2f.api.common;

import android.os.Parcel;
import java.io.UnsupportedEncodingException;
import android.os.Parcelable$Creator;
import android.os.Parcelable;

@Deprecated
public enum ProtocolVersion implements Parcelable
{
    public static final Parcelable$Creator<ProtocolVersion> CREATOR;
    
    UNKNOWN("UNKNOWN", 0, "UNKNOWN"), 
    V1("V1", 1, "U2F_V1"), 
    V2("V2", 2, "U2F_V2");
    
    private static final ProtocolVersion[] zza;
    private final String zzb;
    
    static {
        CREATOR = (Parcelable$Creator)new zzf();
    }
    
    private ProtocolVersion(final String name, final int ordinal, final String zzb) {
        this.zzb = zzb;
    }
    
    public static ProtocolVersion fromBytes(final byte[] bytes) {
        try {
            return fromString(new String(bytes, "UTF-8"));
        }
        catch (final UnsupportedEncodingException cause) {
            throw new RuntimeException(cause);
        }
    }
    
    public static ProtocolVersion fromString(final String s) {
        if (s == null) {
            return ProtocolVersion.UNKNOWN;
        }
        for (final ProtocolVersion protocolVersion : values()) {
            if (s.equals(protocolVersion.zzb)) {
                return protocolVersion;
            }
        }
        throw new UnsupportedProtocolException(s);
    }
    
    public int describeContents() {
        return 0;
    }
    
    public boolean isCompatible(final ProtocolVersion obj) {
        final ProtocolVersion unknown = ProtocolVersion.UNKNOWN;
        return this.equals(unknown) || obj.equals(unknown) || this.equals(obj);
    }
    
    @Override
    public String toString() {
        return this.zzb;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeString(this.zzb);
    }
    
    public static class UnsupportedProtocolException extends Exception
    {
        public UnsupportedProtocolException(final String s) {
            super(String.format("Protocol version %s not supported", s));
        }
    }
}

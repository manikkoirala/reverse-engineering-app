// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.u2f.api.common;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import org.json.JSONException;
import org.json.JSONObject;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Deprecated
@Class(creator = "ChannelIdValueCreator")
@Reserved({ 1 })
public class ChannelIdValue extends AbstractSafeParcelable
{
    public static final ChannelIdValue ABSENT;
    public static final Parcelable$Creator<ChannelIdValue> CREATOR;
    public static final ChannelIdValue UNAVAILABLE;
    public static final ChannelIdValue UNUSED;
    @Field(getter = "getTypeAsInt", id = 2, type = "int")
    private final ChannelIdValueType zza;
    @Field(getter = "getStringValue", id = 3)
    private final String zzb;
    @Field(getter = "getObjectValueAsString", id = 4)
    private final String zzc;
    
    static {
        CREATOR = (Parcelable$Creator)new zzb();
        ABSENT = new ChannelIdValue();
        UNAVAILABLE = new ChannelIdValue("unavailable");
        UNUSED = new ChannelIdValue("unused");
    }
    
    private ChannelIdValue() {
        this.zza = ChannelIdValueType.ABSENT;
        this.zzc = null;
        this.zzb = null;
    }
    
    @Constructor
    public ChannelIdValue(@Param(id = 2) final int n, @Param(id = 3) final String zzb, @Param(id = 4) final String zzc) {
        try {
            this.zza = toChannelIdValueType(n);
            this.zzb = zzb;
            this.zzc = zzc;
        }
        catch (final UnsupportedChannelIdValueTypeException cause) {
            throw new IllegalArgumentException(cause);
        }
    }
    
    private ChannelIdValue(final String s) {
        this.zzb = Preconditions.checkNotNull(s);
        this.zza = ChannelIdValueType.STRING;
        this.zzc = null;
    }
    
    public ChannelIdValue(final JSONObject jsonObject) {
        this.zzc = Preconditions.checkNotNull(jsonObject.toString());
        this.zza = ChannelIdValueType.OBJECT;
        this.zzb = null;
    }
    
    public static ChannelIdValueType toChannelIdValueType(final int n) {
        for (final ChannelIdValueType channelIdValueType : ChannelIdValueType.values()) {
            if (n == ChannelIdValueType.zza(channelIdValueType)) {
                return channelIdValueType;
            }
        }
        throw new UnsupportedChannelIdValueTypeException(n);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ChannelIdValue)) {
            return false;
        }
        final ChannelIdValue channelIdValue = (ChannelIdValue)o;
        if (!this.zza.equals(channelIdValue.zza)) {
            return false;
        }
        final int ordinal = this.zza.ordinal();
        if (ordinal != 0) {
            String s;
            String anObject;
            if (ordinal != 1) {
                if (ordinal != 2) {
                    return false;
                }
                s = this.zzc;
                anObject = channelIdValue.zzc;
            }
            else {
                s = this.zzb;
                anObject = channelIdValue.zzb;
            }
            return s.equals(anObject);
        }
        return true;
    }
    
    public JSONObject getObjectValue() {
        if (this.zzc == null) {
            return null;
        }
        try {
            return new JSONObject(this.zzc);
        }
        catch (final JSONException cause) {
            throw new RuntimeException((Throwable)cause);
        }
    }
    
    public String getObjectValueAsString() {
        return this.zzc;
    }
    
    public String getStringValue() {
        return this.zzb;
    }
    
    public ChannelIdValueType getType() {
        return this.zza;
    }
    
    public int getTypeAsInt() {
        return ChannelIdValueType.zza(this.zza);
    }
    
    @Override
    public int hashCode() {
        final int n = this.zza.hashCode() + 31;
        final int ordinal = this.zza.ordinal();
        int n2;
        String s;
        if (ordinal != 1) {
            if (ordinal != 2) {
                return n;
            }
            n2 = n * 31;
            s = this.zzc;
        }
        else {
            n2 = n * 31;
            s = this.zzb;
        }
        return n2 + s.hashCode();
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 2, this.getTypeAsInt());
        SafeParcelWriter.writeString(parcel, 3, this.getStringValue(), false);
        SafeParcelWriter.writeString(parcel, 4, this.getObjectValueAsString(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public enum ChannelIdValueType implements Parcelable
    {
        ABSENT("ABSENT", 0, 0);
        
        public static final Parcelable$Creator<ChannelIdValueType> CREATOR;
        
        OBJECT("OBJECT", 2, 2), 
        STRING("STRING", 1, 1);
        
        private static final ChannelIdValueType[] zza;
        private final int zzb;
        
        static {
            CREATOR = (Parcelable$Creator)new zza();
        }
        
        private ChannelIdValueType(final String name, final int ordinal, final int zzb) {
            this.zzb = zzb;
        }
        
        public int describeContents() {
            return 0;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            parcel.writeInt(this.zzb);
        }
    }
    
    public static class UnsupportedChannelIdValueTypeException extends Exception
    {
        public UnsupportedChannelIdValueTypeException(final int i) {
            super(String.format("ChannelIdValueType %s not supported", i));
        }
    }
}

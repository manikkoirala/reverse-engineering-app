// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.u2f.api.common;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import java.util.Collection;
import java.util.Arrays;
import com.google.android.gms.common.internal.Objects;
import java.util.Iterator;
import java.util.HashSet;
import com.google.android.gms.common.internal.Preconditions;
import java.util.Set;
import java.util.List;
import android.net.Uri;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

@Deprecated
@Class(creator = "SignRequestParamsCreator")
@Reserved({ 1 })
public class SignRequestParams extends RequestParams
{
    public static final Parcelable$Creator<SignRequestParams> CREATOR;
    public static final int MAX_DISPLAY_HINT_LENGTH = 80;
    @Field(getter = "getRequestId", id = 2)
    private final Integer zza;
    @Field(getter = "getTimeoutSeconds", id = 3)
    private final Double zzb;
    @Field(getter = "getAppId", id = 4)
    private final Uri zzc;
    @Field(getter = "getDefaultSignChallenge", id = 5)
    private final byte[] zzd;
    @Field(getter = "getRegisteredKeys", id = 6)
    private final List zze;
    @Field(getter = "getChannelIdValue", id = 7)
    private final ChannelIdValue zzf;
    @Field(getter = "getDisplayHint", id = 8)
    private final String zzg;
    private final Set zzh;
    
    static {
        CREATOR = (Parcelable$Creator)new zzk();
    }
    
    @Constructor
    public SignRequestParams(@Param(id = 2) final Integer zza, @Param(id = 3) final Double zzb, @Param(id = 4) final Uri zzc, @Param(id = 5) final byte[] zzd, @Param(id = 6) final List zze, @Param(id = 7) final ChannelIdValue zzf, @Param(id = 8) final String zzg) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        final boolean b = false;
        Preconditions.checkArgument(zze != null && !zze.isEmpty(), (Object)"registeredKeys must not be null or empty");
        this.zze = zze;
        this.zzf = zzf;
        final HashSet zzh = new HashSet();
        if (zzc != null) {
            zzh.add(zzc);
        }
        for (final RegisteredKey registeredKey : zze) {
            Preconditions.checkArgument(registeredKey.getAppId() != null || zzc != null, (Object)"registered key has null appId and no request appId is provided");
            registeredKey.getChallengeValue();
            Preconditions.checkArgument(true, (Object)"register request has null challenge and no default challenge isprovided");
            if (registeredKey.getAppId() != null) {
                zzh.add(Uri.parse(registeredKey.getAppId()));
            }
        }
        this.zzh = zzh;
        boolean b2 = false;
        Label_0213: {
            if (zzg != null) {
                b2 = b;
                if (zzg.length() > 80) {
                    break Label_0213;
                }
            }
            b2 = true;
        }
        Preconditions.checkArgument(b2, (Object)"Display Hint cannot be longer than 80 characters");
        this.zzg = zzg;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SignRequestParams)) {
            return false;
        }
        final SignRequestParams signRequestParams = (SignRequestParams)o;
        return Objects.equal(this.zza, signRequestParams.zza) && Objects.equal(this.zzb, signRequestParams.zzb) && Objects.equal(this.zzc, signRequestParams.zzc) && Arrays.equals(this.zzd, signRequestParams.zzd) && this.zze.containsAll(signRequestParams.zze) && signRequestParams.zze.containsAll(this.zze) && Objects.equal(this.zzf, signRequestParams.zzf) && Objects.equal(this.zzg, signRequestParams.zzg);
    }
    
    @Override
    public Set<Uri> getAllAppIds() {
        return this.zzh;
    }
    
    @Override
    public Uri getAppId() {
        return this.zzc;
    }
    
    @Override
    public ChannelIdValue getChannelIdValue() {
        return this.zzf;
    }
    
    public byte[] getDefaultSignChallenge() {
        return this.zzd;
    }
    
    @Override
    public String getDisplayHint() {
        return this.zzg;
    }
    
    @Override
    public List<RegisteredKey> getRegisteredKeys() {
        return this.zze;
    }
    
    @Override
    public Integer getRequestId() {
        return this.zza;
    }
    
    @Override
    public Double getTimeoutSeconds() {
        return this.zzb;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zza, this.zzc, this.zzb, this.zze, this.zzf, this.zzg, Arrays.hashCode(this.zzd));
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeIntegerObject(parcel, 2, this.getRequestId(), false);
        SafeParcelWriter.writeDoubleObject(parcel, 3, this.getTimeoutSeconds(), false);
        SafeParcelWriter.writeParcelable(parcel, 4, (Parcelable)this.getAppId(), n, false);
        SafeParcelWriter.writeByteArray(parcel, 5, this.getDefaultSignChallenge(), false);
        SafeParcelWriter.writeTypedList(parcel, 6, this.getRegisteredKeys(), false);
        SafeParcelWriter.writeParcelable(parcel, 7, (Parcelable)this.getChannelIdValue(), n, false);
        SafeParcelWriter.writeString(parcel, 8, this.getDisplayHint(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public static final class Builder
    {
        Integer zza;
        Double zzb;
        Uri zzc;
        byte[] zzd;
        List zze;
        ChannelIdValue zzf;
        String zzg;
        
        public SignRequestParams build() {
            return new SignRequestParams(this.zza, this.zzb, this.zzc, this.zzd, this.zze, this.zzf, this.zzg);
        }
        
        public Builder setAppId(final Uri zzc) {
            this.zzc = zzc;
            return this;
        }
        
        public Builder setChannelIdValue(final ChannelIdValue zzf) {
            this.zzf = zzf;
            return this;
        }
        
        public Builder setDefaultSignChallenge(final byte[] zzd) {
            this.zzd = zzd;
            return this;
        }
        
        public Builder setDisplayHint(final String zzg) {
            this.zzg = zzg;
            return this;
        }
        
        public Builder setRegisteredKeys(final List<RegisteredKey> zze) {
            this.zze = zze;
            return this;
        }
        
        public Builder setRequestId(final Integer zza) {
            this.zza = zza;
            return this;
        }
        
        public Builder setTimeoutSeconds(final Double zzb) {
            this.zzb = zzb;
            return this;
        }
    }
}

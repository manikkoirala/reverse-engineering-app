// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.u2f.api.common;

import android.os.Parcel;
import android.os.Parcelable$Creator;
import android.os.Parcelable;

@Deprecated
public enum ErrorCode implements Parcelable
{
    BAD_REQUEST("BAD_REQUEST", 2, 2), 
    CONFIGURATION_UNSUPPORTED("CONFIGURATION_UNSUPPORTED", 3, 3);
    
    public static final Parcelable$Creator<ErrorCode> CREATOR;
    
    DEVICE_INELIGIBLE("DEVICE_INELIGIBLE", 4, 4), 
    OK("OK", 0, 0), 
    OTHER_ERROR("OTHER_ERROR", 1, 1), 
    TIMEOUT("TIMEOUT", 5, 5);
    
    private static final String zza;
    private static final ErrorCode[] zzb;
    private final int zzc;
    
    static {
        zza = ErrorCode.class.getSimpleName();
        CREATOR = (Parcelable$Creator)new zzc();
    }
    
    private ErrorCode(final String name, final int ordinal, final int zzc) {
        this.zzc = zzc;
    }
    
    public static ErrorCode toErrorCode(final int n) {
        for (final ErrorCode errorCode : values()) {
            if (n == errorCode.zzc) {
                return errorCode;
            }
        }
        return ErrorCode.OTHER_ERROR;
    }
    
    public int describeContents() {
        return 0;
    }
    
    public int getCode() {
        return this.zzc;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeInt(this.zzc);
    }
}

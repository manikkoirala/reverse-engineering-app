// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.u2f.api.common;

import java.util.Iterator;
import org.json.JSONArray;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.util.Base64Utils;
import com.google.android.gms.common.internal.Objects;
import java.util.Collection;
import java.util.Arrays;
import org.json.JSONException;
import android.util.Base64;
import org.json.JSONObject;
import com.google.android.gms.fido.common.Transport;
import java.util.List;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Deprecated
@Class(creator = "KeyHandleCreator")
public class KeyHandle extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<KeyHandle> CREATOR;
    @VersionField(getter = "getVersionCode", id = 1)
    private final int zza;
    @Field(getter = "getBytes", id = 2)
    private final byte[] zzb;
    @Field(getter = "getProtocolVersionAsString", id = 3, type = "java.lang.String")
    private final ProtocolVersion zzc;
    @Field(getter = "getTransports", id = 4)
    private final List zzd;
    
    static {
        CREATOR = (Parcelable$Creator)new zze();
    }
    
    @Constructor
    public KeyHandle(@Param(id = 1) final int zza, @Param(id = 2) final byte[] zzb, @Param(id = 3) final String s, @Param(id = 4) final List zzd) {
        this.zza = zza;
        this.zzb = zzb;
        try {
            this.zzc = ProtocolVersion.fromString(s);
            this.zzd = zzd;
        }
        catch (final ProtocolVersion.UnsupportedProtocolException cause) {
            throw new IllegalArgumentException(cause);
        }
    }
    
    public KeyHandle(final byte[] zzb, final ProtocolVersion zzc, final List<Transport> zzd) {
        this.zza = 1;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
    }
    
    public static KeyHandle parseFromJson(final JSONObject jsonObject) {
        final boolean has = jsonObject.has("version");
        final List<Transport> list = null;
        String string;
        if (has) {
            string = jsonObject.getString("version");
        }
        else {
            string = null;
        }
        try {
            final ProtocolVersion fromString = ProtocolVersion.fromString(string);
            final String string2 = jsonObject.getString("keyHandle");
            try {
                final byte[] decode = Base64.decode(string2, 8);
                List<Transport> transports;
                if (!jsonObject.has("transports")) {
                    transports = list;
                }
                else {
                    transports = Transport.parseTransports(jsonObject.getJSONArray("transports"));
                }
                return new KeyHandle(decode, fromString, transports);
            }
            catch (final IllegalArgumentException ex) {
                throw new JSONException(ex.toString());
            }
        }
        catch (final ProtocolVersion.UnsupportedProtocolException ex2) {
            throw new JSONException(ex2.toString());
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof KeyHandle)) {
            return false;
        }
        final KeyHandle keyHandle = (KeyHandle)o;
        if (!Arrays.equals(this.zzb, keyHandle.zzb)) {
            return false;
        }
        if (!this.zzc.equals(keyHandle.zzc)) {
            return false;
        }
        final List zzd = this.zzd;
        if (zzd == null && keyHandle.zzd == null) {
            return true;
        }
        if (zzd != null) {
            final List zzd2 = keyHandle.zzd;
            if (zzd2 != null) {
                if (zzd.containsAll(zzd2) && keyHandle.zzd.containsAll(this.zzd)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public byte[] getBytes() {
        return this.zzb;
    }
    
    public ProtocolVersion getProtocolVersion() {
        return this.zzc;
    }
    
    public List<Transport> getTransports() {
        return this.zzd;
    }
    
    public int getVersionCode() {
        return this.zza;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(Arrays.hashCode(this.zzb), this.zzc, this.zzd);
    }
    
    public JSONObject toJson() {
        return this.zza();
    }
    
    @Override
    public String toString() {
        final List zzd = this.zzd;
        String string;
        if (zzd == null) {
            string = "null";
        }
        else {
            string = zzd.toString();
        }
        return String.format("{keyHandle: %s, version: %s, transports: %s}", Base64Utils.encode(this.zzb), this.zzc, string);
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.getVersionCode());
        SafeParcelWriter.writeByteArray(parcel, 2, this.getBytes(), false);
        SafeParcelWriter.writeString(parcel, 3, this.zzc.toString(), false);
        SafeParcelWriter.writeTypedList(parcel, 4, this.getTransports(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public final JSONObject zza() {
        final JSONObject jsonObject = new JSONObject();
        try {
            final byte[] zzb = this.zzb;
            if (zzb != null) {
                jsonObject.put("keyHandle", (Object)Base64.encodeToString(zzb, 11));
            }
            final ProtocolVersion zzc = this.zzc;
            if (zzc != null) {
                jsonObject.put("version", (Object)zzc.toString());
            }
            if (this.zzd != null) {
                final JSONArray jsonArray = new JSONArray();
                final Iterator iterator = this.zzd.iterator();
                while (iterator.hasNext()) {
                    jsonArray.put((Object)((Transport)iterator.next()).toString());
                }
                jsonObject.put("transports", (Object)jsonArray);
            }
            return jsonObject;
        }
        catch (final JSONException cause) {
            throw new RuntimeException((Throwable)cause);
        }
    }
}

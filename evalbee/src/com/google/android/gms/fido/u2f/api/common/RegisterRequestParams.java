// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.u2f.api.common;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import java.util.Collection;
import com.google.android.gms.common.internal.Objects;
import java.util.Iterator;
import java.util.HashSet;
import com.google.android.gms.common.internal.Preconditions;
import java.util.Set;
import java.util.List;
import android.net.Uri;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

@Deprecated
@Class(creator = "RegisterRequestParamsCreator")
@Reserved({ 1 })
public class RegisterRequestParams extends RequestParams
{
    public static final Parcelable$Creator<RegisterRequestParams> CREATOR;
    public static final int MAX_DISPLAY_HINT_LENGTH = 80;
    @Field(getter = "getRequestId", id = 2)
    private final Integer zza;
    @Field(getter = "getTimeoutSeconds", id = 3)
    private final Double zzb;
    @Field(getter = "getAppId", id = 4)
    private final Uri zzc;
    @Field(getter = "getRegisterRequests", id = 5)
    private final List zzd;
    @Field(getter = "getRegisteredKeys", id = 6)
    private final List zze;
    @Field(getter = "getChannelIdValue", id = 7)
    private final ChannelIdValue zzf;
    @Field(getter = "getDisplayHint", id = 8)
    private final String zzg;
    private Set zzh;
    
    static {
        CREATOR = (Parcelable$Creator)new zzh();
    }
    
    @Constructor
    public RegisterRequestParams(@Param(id = 2) final Integer zza, @Param(id = 3) final Double zzb, @Param(id = 4) final Uri zzc, @Param(id = 5) final List zzd, @Param(id = 6) final List zze, @Param(id = 7) final ChannelIdValue zzf, @Param(id = 8) final String zzg) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        final boolean b = false;
        Preconditions.checkArgument(zzd != null && !zzd.isEmpty(), (Object)"empty list of register requests is provided");
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
        final HashSet zzh = new HashSet();
        if (zzc != null) {
            zzh.add(zzc);
        }
        for (final RegisterRequest registerRequest : zzd) {
            Preconditions.checkArgument(zzc != null || registerRequest.getAppId() != null, (Object)"register request has null appId and no request appId is provided");
            if (registerRequest.getAppId() != null) {
                zzh.add(Uri.parse(registerRequest.getAppId()));
            }
        }
        for (final RegisteredKey registeredKey : zze) {
            Preconditions.checkArgument(zzc != null || registeredKey.getAppId() != null, (Object)"registered key has null appId and no request appId is provided");
            if (registeredKey.getAppId() != null) {
                zzh.add(Uri.parse(registeredKey.getAppId()));
            }
        }
        this.zzh = zzh;
        boolean b2 = false;
        Label_0286: {
            if (zzg != null) {
                b2 = b;
                if (zzg.length() > 80) {
                    break Label_0286;
                }
            }
            b2 = true;
        }
        Preconditions.checkArgument(b2, (Object)"Display Hint cannot be longer than 80 characters");
        this.zzg = zzg;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RegisterRequestParams)) {
            return false;
        }
        final RegisterRequestParams registerRequestParams = (RegisterRequestParams)o;
        if (Objects.equal(this.zza, registerRequestParams.zza) && Objects.equal(this.zzb, registerRequestParams.zzb) && Objects.equal(this.zzc, registerRequestParams.zzc) && Objects.equal(this.zzd, registerRequestParams.zzd)) {
            final List zze = this.zze;
            if (zze != null || registerRequestParams.zze != null) {
                if (zze == null) {
                    return false;
                }
                final List zze2 = registerRequestParams.zze;
                if (zze2 == null || !zze.containsAll(zze2) || !registerRequestParams.zze.containsAll(this.zze)) {
                    return false;
                }
            }
            if (Objects.equal(this.zzf, registerRequestParams.zzf) && Objects.equal(this.zzg, registerRequestParams.zzg)) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public Set<Uri> getAllAppIds() {
        return this.zzh;
    }
    
    @Override
    public Uri getAppId() {
        return this.zzc;
    }
    
    @Override
    public ChannelIdValue getChannelIdValue() {
        return this.zzf;
    }
    
    @Override
    public String getDisplayHint() {
        return this.zzg;
    }
    
    public List<RegisterRequest> getRegisterRequests() {
        return this.zzd;
    }
    
    @Override
    public List<RegisteredKey> getRegisteredKeys() {
        return this.zze;
    }
    
    @Override
    public Integer getRequestId() {
        return this.zza;
    }
    
    @Override
    public Double getTimeoutSeconds() {
        return this.zzb;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zza, this.zzc, this.zzb, this.zzd, this.zze, this.zzf, this.zzg);
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeIntegerObject(parcel, 2, this.getRequestId(), false);
        SafeParcelWriter.writeDoubleObject(parcel, 3, this.getTimeoutSeconds(), false);
        SafeParcelWriter.writeParcelable(parcel, 4, (Parcelable)this.getAppId(), n, false);
        SafeParcelWriter.writeTypedList(parcel, 5, this.getRegisterRequests(), false);
        SafeParcelWriter.writeTypedList(parcel, 6, this.getRegisteredKeys(), false);
        SafeParcelWriter.writeParcelable(parcel, 7, (Parcelable)this.getChannelIdValue(), n, false);
        SafeParcelWriter.writeString(parcel, 8, this.getDisplayHint(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public static final class Builder
    {
        Integer zza;
        Double zzb;
        Uri zzc;
        List zzd;
        List zze;
        ChannelIdValue zzf;
        String zzg;
        
        public RegisterRequestParams build() {
            return new RegisterRequestParams(this.zza, this.zzb, this.zzc, this.zzd, this.zze, this.zzf, this.zzg);
        }
        
        public Builder setAppId(final Uri zzc) {
            this.zzc = zzc;
            return this;
        }
        
        public Builder setChannelIdValue(final ChannelIdValue zzf) {
            this.zzf = zzf;
            return this;
        }
        
        public Builder setDisplayHint(final String zzg) {
            this.zzg = zzg;
            return this;
        }
        
        public Builder setRegisterRequests(final List<RegisterRequest> zzd) {
            this.zzd = zzd;
            return this;
        }
        
        public Builder setRegisteredKeys(final List<RegisteredKey> zze) {
            this.zze = zze;
            return this;
        }
        
        public Builder setRequestId(final Integer zza) {
            this.zza = zza;
            return this;
        }
        
        public Builder setTimeoutSeconds(final Double zzb) {
            this.zzb = zzb;
            return this;
        }
    }
}

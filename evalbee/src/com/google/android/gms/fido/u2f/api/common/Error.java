// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.u2f.api.common;

import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;
import com.google.android.gms.common.util.VisibleForTesting;

@Deprecated
public class Error
{
    @VisibleForTesting
    public static final String JSON_ERROR_CODE = "errorCode";
    @VisibleForTesting
    public static final String JSON_ERROR_MESSAGE = "errorMessage";
    private final ErrorCode zza;
    private final String zzb;
    
    public Error(final ErrorCode zza) {
        this.zza = zza;
        this.zzb = null;
    }
    
    public Error(final ErrorCode zza, final String zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public ErrorCode getErrorCode() {
        return this.zza;
    }
    
    public String getErrorMessage() {
        return this.zzb;
    }
    
    public JSONObject toJsonObject() {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("errorCode", this.zza.getCode());
            final String zzb = this.zzb;
            if (zzb != null) {
                jsonObject.put("errorMessage", (Object)zzb);
            }
            return jsonObject;
        }
        catch (final JSONException cause) {
            throw new RuntimeException((Throwable)cause);
        }
    }
    
    @Override
    public String toString() {
        if (this.zzb == null) {
            return String.format(Locale.ENGLISH, "{errorCode: %d}", this.zza.getCode());
        }
        return String.format(Locale.ENGLISH, "{errorCode: %d, errorMessage: %s}", this.zza.getCode(), this.zzb);
    }
}

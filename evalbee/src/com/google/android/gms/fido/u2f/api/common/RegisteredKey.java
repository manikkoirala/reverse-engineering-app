// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.u2f.api.common;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import android.util.Base64;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Deprecated
@Class(creator = "RegisteredKeyCreator")
@Reserved({ 1 })
public class RegisteredKey extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<RegisteredKey> CREATOR;
    @Field(getter = "getChallengeValue", id = 3)
    String zza;
    @Field(getter = "getKeyHandle", id = 2)
    private final KeyHandle zzb;
    @Field(getter = "getAppId", id = 4)
    private final String zzc;
    
    static {
        CREATOR = (Parcelable$Creator)new zzj();
    }
    
    public RegisteredKey(final KeyHandle keyHandle) {
        this(keyHandle, null, null);
    }
    
    @Constructor
    public RegisteredKey(@Param(id = 2) final KeyHandle keyHandle, @Param(id = 3) final String zza, @Param(id = 4) final String zzc) {
        this.zzb = Preconditions.checkNotNull(keyHandle);
        this.zza = zza;
        this.zzc = zzc;
    }
    
    public static RegisteredKey parseFromJson(final JSONObject jsonObject) {
        final boolean has = jsonObject.has("challenge");
        String string = null;
        String string2;
        if (has) {
            string2 = jsonObject.getString("challenge");
        }
        else {
            string2 = null;
        }
        final KeyHandle fromJson = KeyHandle.parseFromJson(jsonObject);
        if (jsonObject.has("appId")) {
            string = jsonObject.getString("appId");
        }
        return new RegisteredKey(fromJson, string2, string);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RegisteredKey)) {
            return false;
        }
        final RegisteredKey registeredKey = (RegisteredKey)o;
        final String zza = this.zza;
        if (zza == null) {
            if (registeredKey.zza != null) {
                return false;
            }
        }
        else if (!zza.equals(registeredKey.zza)) {
            return false;
        }
        if (!this.zzb.equals(registeredKey.zzb)) {
            return false;
        }
        final String zzc = this.zzc;
        final String zzc2 = registeredKey.zzc;
        if (zzc == null) {
            if (zzc2 != null) {
                return false;
            }
        }
        else if (!zzc.equals(zzc2)) {
            return false;
        }
        return true;
    }
    
    public String getAppId() {
        return this.zzc;
    }
    
    public String getChallengeValue() {
        return this.zza;
    }
    
    public KeyHandle getKeyHandle() {
        return this.zzb;
    }
    
    @Override
    public int hashCode() {
        final String zza = this.zza;
        int hashCode = 0;
        int hashCode2;
        if (zza == null) {
            hashCode2 = 0;
        }
        else {
            hashCode2 = zza.hashCode();
        }
        final int hashCode3 = this.zzb.hashCode();
        final String zzc = this.zzc;
        if (zzc != null) {
            hashCode = zzc.hashCode();
        }
        return ((hashCode2 + 31) * 31 + hashCode3) * 31 + hashCode;
    }
    
    public JSONObject toJson() {
        final JSONObject jsonObject = new JSONObject();
        try {
            final String zza = this.zza;
            if (zza != null) {
                jsonObject.put("challenge", (Object)zza);
            }
            final JSONObject zza2 = this.zzb.zza();
            final Iterator keys = zza2.keys();
            while (keys.hasNext()) {
                final String s = keys.next();
                jsonObject.put(s, zza2.get(s));
            }
            final String zzc = this.zzc;
            if (zzc != null) {
                jsonObject.put("appId", (Object)zzc);
            }
            return jsonObject;
        }
        catch (final JSONException cause) {
            throw new RuntimeException((Throwable)cause);
        }
    }
    
    @Override
    public String toString() {
        try {
            final JSONObject jsonObject = new JSONObject();
            jsonObject.put("keyHandle", (Object)Base64.encodeToString(this.zzb.getBytes(), 11));
            if (this.zzb.getProtocolVersion() != ProtocolVersion.UNKNOWN) {
                jsonObject.put("version", (Object)this.zzb.getProtocolVersion().toString());
            }
            if (this.zzb.getTransports() != null) {
                jsonObject.put("transports", (Object)this.zzb.getTransports().toString());
            }
            final String zza = this.zza;
            if (zza != null) {
                jsonObject.put("challenge", (Object)zza);
            }
            final String zzc = this.zzc;
            if (zzc != null) {
                jsonObject.put("appId", (Object)zzc);
            }
            return jsonObject.toString();
        }
        catch (final JSONException cause) {
            throw new RuntimeException((Throwable)cause);
        }
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeParcelable(parcel, 2, (Parcelable)this.getKeyHandle(), n, false);
        SafeParcelWriter.writeString(parcel, 3, this.getChallengeValue(), false);
        SafeParcelWriter.writeString(parcel, 4, this.getAppId(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

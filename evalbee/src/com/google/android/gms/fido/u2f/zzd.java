// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.u2f;

import com.google.android.gms.common.api.internal.TaskUtil;
import com.google.android.gms.internal.fido.zzt;
import android.app.PendingIntent;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.internal.fido.zzu;

final class zzd extends zzu
{
    final TaskCompletionSource zza;
    
    public zzd(final U2fApiClient u2fApiClient, final TaskCompletionSource zza) {
        this.zza = zza;
    }
    
    public final void zzb(final Status status, final PendingIntent pendingIntent) {
        TaskUtil.setResultOrApiException(status, new zzt(pendingIntent), (com.google.android.gms.tasks.TaskCompletionSource<zzt>)this.zza);
    }
}

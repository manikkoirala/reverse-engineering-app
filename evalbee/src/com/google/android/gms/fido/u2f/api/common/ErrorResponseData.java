// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.u2f.api.common;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.internal.fido.zzaj;
import com.google.android.gms.internal.fido.zzak;
import org.json.JSONException;
import org.json.JSONObject;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.VisibleForTesting;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

@Deprecated
@Class(creator = "ErrorResponseDataCreator")
@Reserved({ 1 })
public class ErrorResponseData extends ResponseData
{
    public static final Parcelable$Creator<ErrorResponseData> CREATOR;
    @VisibleForTesting
    public static final String JSON_ERROR_CODE = "errorCode";
    @VisibleForTesting
    public static final String JSON_ERROR_MESSAGE = "errorMessage";
    @Field(getter = "getErrorCodeAsInt", id = 2, type = "int")
    private final ErrorCode zza;
    @Field(getter = "getErrorMessage", id = 3)
    private final String zzb;
    
    static {
        CREATOR = (Parcelable$Creator)new zzd();
    }
    
    @Constructor
    public ErrorResponseData(@Param(id = 2) final int n, @Param(id = 3) final String zzb) {
        this.zza = ErrorCode.toErrorCode(n);
        this.zzb = zzb;
    }
    
    public ErrorResponseData(final ErrorCode errorCode) {
        this.zza = Preconditions.checkNotNull(errorCode);
        this.zzb = null;
    }
    
    public ErrorResponseData(final ErrorCode errorCode, final String zzb) {
        this.zza = Preconditions.checkNotNull(errorCode);
        this.zzb = zzb;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof ErrorResponseData)) {
            return false;
        }
        final ErrorResponseData errorResponseData = (ErrorResponseData)o;
        return Objects.equal(this.zza, errorResponseData.zza) && Objects.equal(this.zzb, errorResponseData.zzb);
    }
    
    public ErrorCode getErrorCode() {
        return this.zza;
    }
    
    public int getErrorCodeAsInt() {
        return this.zza.getCode();
    }
    
    public String getErrorMessage() {
        return this.zzb;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zza, this.zzb);
    }
    
    @Override
    public final JSONObject toJsonObject() {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("errorCode", this.zza.getCode());
            final String zzb = this.zzb;
            if (zzb != null) {
                jsonObject.put("errorMessage", (Object)zzb);
            }
            return jsonObject;
        }
        catch (final JSONException cause) {
            throw new RuntimeException((Throwable)cause);
        }
    }
    
    @Override
    public String toString() {
        final zzaj zza = zzak.zza((Object)this);
        zza.zza("errorCode", this.zza.getCode());
        final String zzb = this.zzb;
        if (zzb != null) {
            zza.zzb("errorMessage", (Object)zzb);
        }
        return zza.toString();
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 2, this.getErrorCodeAsInt());
        SafeParcelWriter.writeString(parcel, 3, this.getErrorMessage(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

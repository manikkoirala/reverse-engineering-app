// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.fido.u2f.api.common;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import java.util.Arrays;
import org.json.JSONException;
import android.util.Base64;
import org.json.JSONObject;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Deprecated
@Class(creator = "RegisterRequestCreator")
public class RegisterRequest extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<RegisterRequest> CREATOR;
    public static final int U2F_V1_CHALLENGE_BYTE_LENGTH = 65;
    @VersionField(getter = "getVersionCode", id = 1)
    private final int zza;
    @Field(getter = "getProtocolVersionAsString", id = 2, type = "java.lang.String")
    private final ProtocolVersion zzb;
    @Field(getter = "getChallengeValue", id = 3)
    private final byte[] zzc;
    @Field(getter = "getAppId", id = 4)
    private final String zzd;
    
    static {
        CREATOR = (Parcelable$Creator)new zzg();
    }
    
    @Constructor
    public RegisterRequest(@Param(id = 1) final int zza, @Param(id = 2) final String s, @Param(id = 3) final byte[] zzc, @Param(id = 4) final String zzd) {
        this.zza = zza;
        try {
            this.zzb = ProtocolVersion.fromString(s);
            this.zzc = zzc;
            this.zzd = zzd;
        }
        catch (final ProtocolVersion.UnsupportedProtocolException cause) {
            throw new IllegalArgumentException(cause);
        }
    }
    
    public RegisterRequest(final ProtocolVersion protocolVersion, final byte[] array, final String zzd) {
        boolean b = true;
        this.zza = 1;
        this.zzb = Preconditions.checkNotNull(protocolVersion);
        this.zzc = Preconditions.checkNotNull(array);
        if (protocolVersion == ProtocolVersion.V1) {
            if (array.length != 65) {
                b = false;
            }
            Preconditions.checkArgument(b, (Object)"invalid challengeValue length for V1");
        }
        this.zzd = zzd;
    }
    
    public static RegisterRequest parseFromJson(final JSONObject jsonObject) {
        final boolean has = jsonObject.has("version");
        final String s = null;
        String string;
        if (has) {
            string = jsonObject.getString("version");
        }
        else {
            string = null;
        }
        try {
            final ProtocolVersion fromString = ProtocolVersion.fromString(string);
            final String string2 = jsonObject.getString("challenge");
            try {
                final byte[] decode = Base64.decode(string2, 8);
                String string3 = s;
                if (jsonObject.has("appId")) {
                    string3 = jsonObject.getString("appId");
                }
                try {
                    return new RegisterRequest(fromString, decode, string3);
                }
                catch (final IllegalArgumentException ex) {
                    throw new JSONException(ex.getMessage());
                }
            }
            catch (final IllegalArgumentException ex2) {
                throw new JSONException(ex2.toString());
            }
        }
        catch (final ProtocolVersion.UnsupportedProtocolException ex3) {
            throw new JSONException(ex3.toString());
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RegisterRequest)) {
            return false;
        }
        final RegisterRequest registerRequest = (RegisterRequest)o;
        if (!Arrays.equals(this.zzc, registerRequest.zzc)) {
            return false;
        }
        if (this.zzb != registerRequest.zzb) {
            return false;
        }
        final String zzd = this.zzd;
        final String zzd2 = registerRequest.zzd;
        if (zzd == null) {
            if (zzd2 != null) {
                return false;
            }
        }
        else if (!zzd.equals(zzd2)) {
            return false;
        }
        return true;
    }
    
    public String getAppId() {
        return this.zzd;
    }
    
    public byte[] getChallengeValue() {
        return this.zzc;
    }
    
    public ProtocolVersion getProtocolVersion() {
        return this.zzb;
    }
    
    public int getVersionCode() {
        return this.zza;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = Arrays.hashCode(this.zzc);
        final int hashCode2 = this.zzb.hashCode();
        final String zzd = this.zzd;
        int hashCode3;
        if (zzd == null) {
            hashCode3 = 0;
        }
        else {
            hashCode3 = zzd.hashCode();
        }
        return ((hashCode + 31) * 31 + hashCode2) * 31 + hashCode3;
    }
    
    public JSONObject toJson() {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("version", (Object)this.zzb.toString());
            jsonObject.put("challenge", (Object)Base64.encodeToString(this.zzc, 11));
            final String zzd = this.zzd;
            if (zzd != null) {
                jsonObject.put("appId", (Object)zzd);
            }
            return jsonObject;
        }
        catch (final JSONException cause) {
            throw new RuntimeException((Throwable)cause);
        }
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.getVersionCode());
        SafeParcelWriter.writeString(parcel, 2, this.zzb.toString(), false);
        SafeParcelWriter.writeByteArray(parcel, 3, this.getChallengeValue(), false);
        SafeParcelWriter.writeString(parcel, 4, this.getAppId(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.dynamic;

import android.os.Bundle;
import android.app.Activity;

final class zab implements zah
{
    final Activity zaa;
    final Bundle zab;
    final Bundle zac;
    final DeferredLifecycleHelper zad;
    
    public zab(final DeferredLifecycleHelper zad, final Activity zaa, final Bundle zab, final Bundle zac) {
        this.zad = zad;
        this.zaa = zaa;
        this.zab = zab;
        this.zac = zac;
    }
    
    @Override
    public final int zaa() {
        return 0;
    }
    
    @Override
    public final void zab(final LifecycleDelegate lifecycleDelegate) {
        DeferredLifecycleHelper.zaa((DeferredLifecycleHelper<LifecycleDelegate>)this.zad).onInflate(this.zaa, this.zab, this.zac);
    }
}

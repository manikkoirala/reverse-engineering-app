// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.dynamic;

import android.os.Bundle;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

final class zad implements zah
{
    final FrameLayout zaa;
    final LayoutInflater zab;
    final ViewGroup zac;
    final Bundle zad;
    final DeferredLifecycleHelper zae;
    
    public zad(final DeferredLifecycleHelper zae, final FrameLayout zaa, final LayoutInflater zab, final ViewGroup zac, final Bundle zad) {
        this.zae = zae;
        this.zaa = zaa;
        this.zab = zab;
        this.zac = zac;
        this.zad = zad;
    }
    
    @Override
    public final int zaa() {
        return 2;
    }
    
    @Override
    public final void zab(final LifecycleDelegate lifecycleDelegate) {
        ((ViewGroup)this.zaa).removeAllViews();
        ((ViewGroup)this.zaa).addView(DeferredLifecycleHelper.zaa((DeferredLifecycleHelper<LifecycleDelegate>)this.zae).onCreateView(this.zab, this.zac, this.zad));
    }
}

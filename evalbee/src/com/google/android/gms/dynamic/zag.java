// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.dynamic;

final class zag implements zah
{
    final DeferredLifecycleHelper zaa;
    
    public zag(final DeferredLifecycleHelper zaa) {
        this.zaa = zaa;
    }
    
    @Override
    public final int zaa() {
        return 5;
    }
    
    @Override
    public final void zab(final LifecycleDelegate lifecycleDelegate) {
        DeferredLifecycleHelper.zaa((DeferredLifecycleHelper<LifecycleDelegate>)this.zaa).onResume();
    }
}

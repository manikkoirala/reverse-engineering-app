// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.dynamic;

import android.os.Bundle;

final class zac implements zah
{
    final Bundle zaa;
    final DeferredLifecycleHelper zab;
    
    public zac(final DeferredLifecycleHelper zab, final Bundle zaa) {
        this.zab = zab;
        this.zaa = zaa;
    }
    
    @Override
    public final int zaa() {
        return 1;
    }
    
    @Override
    public final void zab(final LifecycleDelegate lifecycleDelegate) {
        DeferredLifecycleHelper.zaa((DeferredLifecycleHelper<LifecycleDelegate>)this.zab).onCreate(this.zaa);
    }
}

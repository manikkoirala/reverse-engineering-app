// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.dynamic;

import android.content.ActivityNotFoundException;
import android.util.Log;
import android.view.View;
import android.content.Intent;
import android.content.Context;
import android.view.View$OnClickListener;

final class zae implements View$OnClickListener
{
    final Context zaa;
    final Intent zab;
    
    public zae(final Context zaa, final Intent zab) {
        this.zaa = zaa;
        this.zab = zab;
    }
    
    public final void onClick(final View view) {
        try {
            this.zaa.startActivity(this.zab);
        }
        catch (final ActivityNotFoundException ex) {
            Log.e("DeferredLifecycleHelper", "Failed to start resolution intent", (Throwable)ex);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.dynamic;

import android.os.Parcelable;
import com.google.android.gms.internal.common.zzc;
import android.os.Parcel;
import android.os.IBinder;
import com.google.android.gms.internal.common.zzb;
import android.content.Intent;
import android.os.Bundle;
import android.os.IInterface;

public interface IFragmentWrapper extends IInterface
{
    boolean zzA();
    
    int zzb();
    
    int zzc();
    
    Bundle zzd();
    
    IFragmentWrapper zze();
    
    IFragmentWrapper zzf();
    
    IObjectWrapper zzg();
    
    IObjectWrapper zzh();
    
    IObjectWrapper zzi();
    
    String zzj();
    
    void zzk(final IObjectWrapper p0);
    
    void zzl(final boolean p0);
    
    void zzm(final boolean p0);
    
    void zzn(final boolean p0);
    
    void zzo(final boolean p0);
    
    void zzp(final Intent p0);
    
    void zzq(final Intent p0, final int p1);
    
    void zzr(final IObjectWrapper p0);
    
    boolean zzs();
    
    boolean zzt();
    
    boolean zzu();
    
    boolean zzv();
    
    boolean zzw();
    
    boolean zzx();
    
    boolean zzy();
    
    boolean zzz();
    
    public abstract static class Stub extends zzb implements IFragmentWrapper
    {
        public Stub() {
            super("com.google.android.gms.dynamic.IFragmentWrapper");
        }
        
        public static IFragmentWrapper asInterface(final IBinder binder) {
            if (binder == null) {
                return null;
            }
            final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.dynamic.IFragmentWrapper");
            if (queryLocalInterface instanceof IFragmentWrapper) {
                return (IFragmentWrapper)queryLocalInterface;
            }
            return new zza(binder);
        }
        
        public final boolean zza(int n, final Parcel parcel, final Parcel parcel2, int zza) {
            Object o = null;
            Label_0531: {
                Label_0497: {
                    Label_0493: {
                        Label_0454: {
                            switch (n) {
                                default: {
                                    return false;
                                }
                                case 27: {
                                    final IObjectWrapper interface1 = IObjectWrapper.Stub.asInterface(parcel.readStrongBinder());
                                    zzc.zzb(parcel);
                                    this.zzr(interface1);
                                    break;
                                }
                                case 26: {
                                    final Intent intent = (Intent)zzc.zza(parcel, Intent.CREATOR);
                                    n = parcel.readInt();
                                    zzc.zzb(parcel);
                                    this.zzq(intent, n);
                                    break;
                                }
                                case 25: {
                                    final Intent intent2 = (Intent)zzc.zza(parcel, Intent.CREATOR);
                                    zzc.zzb(parcel);
                                    this.zzp(intent2);
                                    break;
                                }
                                case 24: {
                                    final boolean zzf = zzc.zzf(parcel);
                                    zzc.zzb(parcel);
                                    this.zzo(zzf);
                                    break;
                                }
                                case 23: {
                                    final boolean zzf2 = zzc.zzf(parcel);
                                    zzc.zzb(parcel);
                                    this.zzn(zzf2);
                                    break;
                                }
                                case 22: {
                                    final boolean zzf3 = zzc.zzf(parcel);
                                    zzc.zzb(parcel);
                                    this.zzm(zzf3);
                                    break;
                                }
                                case 21: {
                                    final boolean zzf4 = zzc.zzf(parcel);
                                    zzc.zzb(parcel);
                                    this.zzl(zzf4);
                                    break;
                                }
                                case 20: {
                                    final IObjectWrapper interface2 = IObjectWrapper.Stub.asInterface(parcel.readStrongBinder());
                                    zzc.zzb(parcel);
                                    this.zzk(interface2);
                                    break;
                                }
                                case 19: {
                                    n = (this.zzA() ? 1 : 0);
                                    break Label_0454;
                                }
                                case 18: {
                                    n = (this.zzz() ? 1 : 0);
                                    break Label_0454;
                                }
                                case 17: {
                                    n = (this.zzy() ? 1 : 0);
                                    break Label_0454;
                                }
                                case 16: {
                                    n = (this.zzx() ? 1 : 0);
                                    break Label_0454;
                                }
                                case 15: {
                                    n = (this.zzw() ? 1 : 0);
                                    break Label_0454;
                                }
                                case 14: {
                                    n = (this.zzv() ? 1 : 0);
                                    break Label_0454;
                                }
                                case 13: {
                                    n = (this.zzu() ? 1 : 0);
                                    break Label_0454;
                                }
                                case 12: {
                                    o = this.zzi();
                                    break Label_0531;
                                }
                                case 11: {
                                    n = (this.zzt() ? 1 : 0);
                                    break Label_0454;
                                }
                                case 10: {
                                    n = this.zzc();
                                    break Label_0493;
                                }
                                case 9: {
                                    o = this.zzf();
                                    break Label_0531;
                                }
                                case 8: {
                                    final String zzj = this.zzj();
                                    parcel2.writeNoException();
                                    parcel2.writeString(zzj);
                                    return true;
                                }
                                case 7: {
                                    n = (this.zzs() ? 1 : 0);
                                    break Label_0454;
                                }
                                case 6: {
                                    o = this.zzh();
                                    break Label_0531;
                                }
                                case 5: {
                                    o = this.zze();
                                    break Label_0531;
                                }
                                case 4: {
                                    n = this.zzb();
                                    break Label_0493;
                                }
                                case 3: {
                                    final Bundle zzd = this.zzd();
                                    parcel2.writeNoException();
                                    zzc.zzd(parcel2, (Parcelable)zzd);
                                    return true;
                                }
                                case 2: {
                                    o = this.zzg();
                                    break Label_0531;
                                }
                            }
                            parcel2.writeNoException();
                            return true;
                        }
                        parcel2.writeNoException();
                        zza = zzc.zza;
                        break Label_0497;
                    }
                    parcel2.writeNoException();
                }
                parcel2.writeInt(n);
                return true;
            }
            parcel2.writeNoException();
            zzc.zze(parcel2, (IInterface)o);
            return true;
        }
    }
}

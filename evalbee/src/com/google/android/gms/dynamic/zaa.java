// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.dynamic;

import java.util.Iterator;
import android.os.Bundle;

final class zaa implements OnDelegateCreatedListener
{
    final DeferredLifecycleHelper zaa;
    
    public zaa(final DeferredLifecycleHelper zaa) {
        this.zaa = zaa;
    }
    
    @Override
    public final void onDelegateCreated(final LifecycleDelegate lifecycleDelegate) {
        DeferredLifecycleHelper.zac((DeferredLifecycleHelper<LifecycleDelegate>)this.zaa, lifecycleDelegate);
        final Iterator iterator = DeferredLifecycleHelper.zab((DeferredLifecycleHelper<LifecycleDelegate>)this.zaa).iterator();
        while (iterator.hasNext()) {
            ((zah)iterator.next()).zab(DeferredLifecycleHelper.zaa((DeferredLifecycleHelper<LifecycleDelegate>)this.zaa));
        }
        DeferredLifecycleHelper.zab((DeferredLifecycleHelper<LifecycleDelegate>)this.zaa).clear();
        DeferredLifecycleHelper.zad((DeferredLifecycleHelper<LifecycleDelegate>)this.zaa, (Bundle)null);
    }
}

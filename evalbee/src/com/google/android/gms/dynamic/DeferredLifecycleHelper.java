// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.dynamic;

import android.app.Activity;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.content.Intent;
import android.content.Context;
import android.view.View$OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.view.View;
import android.view.ViewGroup$LayoutParams;
import android.widget.FrameLayout$LayoutParams;
import android.widget.LinearLayout;
import com.google.android.gms.common.internal.zac;
import com.google.android.gms.common.GoogleApiAvailability;
import android.widget.FrameLayout;
import java.util.LinkedList;
import android.os.Bundle;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public abstract class DeferredLifecycleHelper<T extends LifecycleDelegate>
{
    private T zaa;
    private Bundle zab;
    private LinkedList<zah> zac;
    private final OnDelegateCreatedListener<T> zad;
    
    @KeepForSdk
    public DeferredLifecycleHelper() {
        this.zad = new zaa(this);
    }
    
    @KeepForSdk
    public static void showGooglePlayUnavailableMessage(final FrameLayout frameLayout) {
        final GoogleApiAvailability instance = GoogleApiAvailability.getInstance();
        final Context context = ((View)frameLayout).getContext();
        final int googlePlayServicesAvailable = instance.isGooglePlayServicesAvailable(context);
        final String zad = zac.zad(context, googlePlayServicesAvailable);
        final String zac = com.google.android.gms.common.internal.zac.zac(context, googlePlayServicesAvailable);
        final LinearLayout linearLayout = new LinearLayout(((View)frameLayout).getContext());
        linearLayout.setOrientation(1);
        ((View)linearLayout).setLayoutParams((ViewGroup$LayoutParams)new FrameLayout$LayoutParams(-2, -2));
        ((ViewGroup)frameLayout).addView((View)linearLayout);
        final TextView textView = new TextView(((View)frameLayout).getContext());
        ((View)textView).setLayoutParams((ViewGroup$LayoutParams)new FrameLayout$LayoutParams(-2, -2));
        textView.setText((CharSequence)zad);
        ((ViewGroup)linearLayout).addView((View)textView);
        final Intent errorResolutionIntent = instance.getErrorResolutionIntent(context, googlePlayServicesAvailable, null);
        if (errorResolutionIntent != null) {
            final Button button = new Button(context);
            ((View)button).setId(16908313);
            ((View)button).setLayoutParams((ViewGroup$LayoutParams)new FrameLayout$LayoutParams(-2, -2));
            ((TextView)button).setText((CharSequence)zac);
            ((ViewGroup)linearLayout).addView((View)button);
            ((View)button).setOnClickListener((View$OnClickListener)new zae(context, errorResolutionIntent));
        }
    }
    
    private final void zae(final int n) {
        while (!this.zac.isEmpty() && this.zac.getLast().zaa() >= n) {
            this.zac.removeLast();
        }
    }
    
    private final void zaf(final Bundle bundle, final zah e) {
        final LifecycleDelegate zaa = this.zaa;
        if (zaa != null) {
            e.zab(zaa);
            return;
        }
        if (this.zac == null) {
            this.zac = new LinkedList<zah>();
        }
        this.zac.add(e);
        if (bundle != null) {
            final Bundle zab = this.zab;
            if (zab == null) {
                this.zab = (Bundle)bundle.clone();
            }
            else {
                zab.putAll(bundle);
            }
        }
        this.createDelegate(this.zad);
    }
    
    @KeepForSdk
    public abstract void createDelegate(final OnDelegateCreatedListener<T> p0);
    
    @KeepForSdk
    public T getDelegate() {
        return this.zaa;
    }
    
    @KeepForSdk
    public void handleGooglePlayUnavailable(final FrameLayout frameLayout) {
        showGooglePlayUnavailableMessage(frameLayout);
    }
    
    @KeepForSdk
    public void onCreate(final Bundle bundle) {
        this.zaf(bundle, new com.google.android.gms.dynamic.zac(this, bundle));
    }
    
    @KeepForSdk
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final FrameLayout frameLayout = new FrameLayout(layoutInflater.getContext());
        this.zaf(bundle, new zad(this, frameLayout, layoutInflater, viewGroup, bundle));
        if (this.zaa == null) {
            this.handleGooglePlayUnavailable(frameLayout);
        }
        return (View)frameLayout;
    }
    
    @KeepForSdk
    public void onDestroy() {
        final LifecycleDelegate zaa = this.zaa;
        if (zaa != null) {
            zaa.onDestroy();
            return;
        }
        this.zae(1);
    }
    
    @KeepForSdk
    public void onDestroyView() {
        final LifecycleDelegate zaa = this.zaa;
        if (zaa != null) {
            zaa.onDestroyView();
            return;
        }
        this.zae(2);
    }
    
    @KeepForSdk
    public void onInflate(final Activity activity, final Bundle bundle, final Bundle bundle2) {
        this.zaf(bundle2, new zab(this, activity, bundle, bundle2));
    }
    
    @KeepForSdk
    public void onLowMemory() {
        final LifecycleDelegate zaa = this.zaa;
        if (zaa != null) {
            zaa.onLowMemory();
        }
    }
    
    @KeepForSdk
    public void onPause() {
        final LifecycleDelegate zaa = this.zaa;
        if (zaa != null) {
            zaa.onPause();
            return;
        }
        this.zae(5);
    }
    
    @KeepForSdk
    public void onResume() {
        this.zaf(null, new zag(this));
    }
    
    @KeepForSdk
    public void onSaveInstanceState(final Bundle bundle) {
        final LifecycleDelegate zaa = this.zaa;
        if (zaa != null) {
            zaa.onSaveInstanceState(bundle);
            return;
        }
        final Bundle zab = this.zab;
        if (zab != null) {
            bundle.putAll(zab);
        }
    }
    
    @KeepForSdk
    public void onStart() {
        this.zaf(null, new zaf(this));
    }
    
    @KeepForSdk
    public void onStop() {
        final LifecycleDelegate zaa = this.zaa;
        if (zaa != null) {
            zaa.onStop();
            return;
        }
        this.zae(4);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.dynamic;

import android.content.Intent;
import com.google.android.gms.common.internal.Preconditions;
import android.view.View;
import android.os.Bundle;
import android.app.Fragment;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public final class FragmentWrapper extends Stub
{
    private final Fragment zza;
    
    private FragmentWrapper(final Fragment zza) {
        this.zza = zza;
    }
    
    @KeepForSdk
    public static FragmentWrapper wrap(final Fragment fragment) {
        if (fragment != null) {
            return new FragmentWrapper(fragment);
        }
        return null;
    }
    
    public final boolean zzA() {
        return this.zza.isVisible();
    }
    
    public final int zzb() {
        return this.zza.getId();
    }
    
    public final int zzc() {
        return this.zza.getTargetRequestCode();
    }
    
    public final Bundle zzd() {
        return this.zza.getArguments();
    }
    
    public final IFragmentWrapper zze() {
        return wrap(this.zza.getParentFragment());
    }
    
    public final IFragmentWrapper zzf() {
        return wrap(this.zza.getTargetFragment());
    }
    
    public final IObjectWrapper zzg() {
        return ObjectWrapper.wrap(this.zza.getActivity());
    }
    
    public final IObjectWrapper zzh() {
        return ObjectWrapper.wrap(this.zza.getResources());
    }
    
    public final IObjectWrapper zzi() {
        return ObjectWrapper.wrap(this.zza.getView());
    }
    
    public final String zzj() {
        return this.zza.getTag();
    }
    
    public final void zzk(final IObjectWrapper objectWrapper) {
        final View view = ObjectWrapper.unwrap(objectWrapper);
        final Fragment zza = this.zza;
        Preconditions.checkNotNull(view);
        zza.registerForContextMenu(view);
    }
    
    public final void zzl(final boolean hasOptionsMenu) {
        this.zza.setHasOptionsMenu(hasOptionsMenu);
    }
    
    public final void zzm(final boolean menuVisibility) {
        this.zza.setMenuVisibility(menuVisibility);
    }
    
    public final void zzn(final boolean retainInstance) {
        this.zza.setRetainInstance(retainInstance);
    }
    
    public final void zzo(final boolean userVisibleHint) {
        this.zza.setUserVisibleHint(userVisibleHint);
    }
    
    public final void zzp(final Intent intent) {
        this.zza.startActivity(intent);
    }
    
    public final void zzq(final Intent intent, final int n) {
        this.zza.startActivityForResult(intent, n);
    }
    
    public final void zzr(final IObjectWrapper objectWrapper) {
        final View view = ObjectWrapper.unwrap(objectWrapper);
        final Fragment zza = this.zza;
        Preconditions.checkNotNull(view);
        zza.unregisterForContextMenu(view);
    }
    
    public final boolean zzs() {
        return this.zza.getRetainInstance();
    }
    
    public final boolean zzt() {
        return this.zza.getUserVisibleHint();
    }
    
    public final boolean zzu() {
        return this.zza.isAdded();
    }
    
    public final boolean zzv() {
        return this.zza.isDetached();
    }
    
    public final boolean zzw() {
        return this.zza.isHidden();
    }
    
    public final boolean zzx() {
        return this.zza.isInLayout();
    }
    
    public final boolean zzy() {
        return this.zza.isRemoving();
    }
    
    public final boolean zzz() {
        return this.zza.isResumed();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.dynamic;

import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;

public final class zza extends com.google.android.gms.internal.common.zza implements IFragmentWrapper
{
    public zza(final IBinder binder) {
        super(binder, "com.google.android.gms.dynamic.IFragmentWrapper");
    }
    
    public final boolean zzA() {
        throw null;
    }
    
    public final int zzb() {
        throw null;
    }
    
    public final int zzc() {
        throw null;
    }
    
    public final Bundle zzd() {
        throw null;
    }
    
    public final IFragmentWrapper zze() {
        throw null;
    }
    
    public final IFragmentWrapper zzf() {
        throw null;
    }
    
    public final IObjectWrapper zzg() {
        throw null;
    }
    
    public final IObjectWrapper zzh() {
        throw null;
    }
    
    public final IObjectWrapper zzi() {
        throw null;
    }
    
    public final String zzj() {
        throw null;
    }
    
    public final void zzk(final IObjectWrapper objectWrapper) {
        throw null;
    }
    
    public final void zzl(final boolean b) {
        throw null;
    }
    
    public final void zzm(final boolean b) {
        throw null;
    }
    
    public final void zzn(final boolean b) {
        throw null;
    }
    
    public final void zzo(final boolean b) {
        throw null;
    }
    
    public final void zzp(final Intent intent) {
        throw null;
    }
    
    public final void zzq(final Intent intent, final int n) {
        throw null;
    }
    
    public final void zzr(final IObjectWrapper objectWrapper) {
        throw null;
    }
    
    public final boolean zzs() {
        throw null;
    }
    
    public final boolean zzt() {
        throw null;
    }
    
    public final boolean zzu() {
        throw null;
    }
    
    public final boolean zzv() {
        throw null;
    }
    
    public final boolean zzw() {
        throw null;
    }
    
    public final boolean zzx() {
        throw null;
    }
    
    public final boolean zzy() {
        throw null;
    }
    
    public final boolean zzz() {
        throw null;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads;

import android.view.ViewGroup$LayoutParams;
import android.view.View;
import com.google.android.gms.ads.internal.client.zzay;
import android.os.Bundle;
import com.google.android.gms.dynamic.ObjectWrapper;
import android.content.res.Configuration;
import android.content.Intent;
import android.os.RemoteException;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.internal.ads.zzbtp;
import com.google.android.gms.common.annotation.KeepForSdk;
import android.app.Activity;

@KeepForSdk
public final class AdActivity extends Activity
{
    @KeepForSdk
    public static final String CLASS_NAME = "com.google.android.gms.ads.AdActivity";
    private zzbtp zza;
    
    private final void zza() {
        final zzbtp zza = this.zza;
        if (zza != null) {
            try {
                zza.zzx();
            }
            catch (final RemoteException ex) {
                zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex);
            }
        }
    }
    
    public final void onActivityResult(final int n, final int n2, final Intent intent) {
        try {
            final zzbtp zza = this.zza;
            if (zza != null) {
                zza.zzh(n, n2, intent);
            }
        }
        catch (final Exception ex) {
            zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex);
        }
        super.onActivityResult(n, n2, intent);
    }
    
    public final void onBackPressed() {
        try {
            final zzbtp zza = this.zza;
            if (zza != null) {
                if (!zza.zzH()) {
                    return;
                }
            }
        }
        catch (final RemoteException ex) {
            zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex);
        }
        super.onBackPressed();
        try {
            final zzbtp zza2 = this.zza;
            if (zza2 != null) {
                zza2.zzi();
            }
        }
        catch (final RemoteException ex2) {
            zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex2);
        }
    }
    
    public final void onConfigurationChanged(final Configuration configuration) {
        super.onConfigurationChanged(configuration);
        try {
            final zzbtp zza = this.zza;
            if (zza != null) {
                zza.zzk(ObjectWrapper.wrap(configuration));
            }
        }
        catch (final RemoteException ex) {
            zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex);
        }
    }
    
    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        final zzbtp zzo = zzay.zza().zzo(this);
        this.zza = zzo;
        if (zzo != null) {
            try {
                zzo.zzl(bundle);
                return;
            }
            catch (final RemoteException ex) {}
        }
        else {
            bundle = null;
        }
        zzcbn.zzl("#007 Could not call remote method.", (Throwable)bundle);
        this.finish();
    }
    
    public final void onDestroy() {
        try {
            final zzbtp zza = this.zza;
            if (zza != null) {
                zza.zzm();
            }
        }
        catch (final RemoteException ex) {
            zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex);
        }
        super.onDestroy();
    }
    
    public final void onPause() {
        try {
            final zzbtp zza = this.zza;
            if (zza != null) {
                zza.zzo();
            }
        }
        catch (final RemoteException ex) {
            zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex);
            this.finish();
        }
        super.onPause();
    }
    
    public final void onRequestPermissionsResult(final int n, final String[] array, final int[] array2) {
        try {
            final zzbtp zza = this.zza;
            if (zza != null) {
                zza.zzp(n, array, array2);
            }
        }
        catch (final RemoteException ex) {
            zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex);
        }
    }
    
    public final void onRestart() {
        super.onRestart();
        try {
            final zzbtp zza = this.zza;
            if (zza != null) {
                zza.zzq();
            }
        }
        catch (final RemoteException ex) {
            zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex);
            this.finish();
        }
    }
    
    public final void onResume() {
        super.onResume();
        try {
            final zzbtp zza = this.zza;
            if (zza != null) {
                zza.zzr();
            }
        }
        catch (final RemoteException ex) {
            zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex);
            this.finish();
        }
    }
    
    public final void onSaveInstanceState(final Bundle bundle) {
        try {
            final zzbtp zza = this.zza;
            if (zza != null) {
                zza.zzs(bundle);
            }
        }
        catch (final RemoteException ex) {
            zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex);
            this.finish();
        }
        super.onSaveInstanceState(bundle);
    }
    
    public final void onStart() {
        super.onStart();
        try {
            final zzbtp zza = this.zza;
            if (zza != null) {
                zza.zzt();
            }
        }
        catch (final RemoteException ex) {
            zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex);
            this.finish();
        }
    }
    
    public final void onStop() {
        try {
            final zzbtp zza = this.zza;
            if (zza != null) {
                zza.zzu();
            }
        }
        catch (final RemoteException ex) {
            zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex);
            this.finish();
        }
        super.onStop();
    }
    
    public final void onUserLeaveHint() {
        super.onUserLeaveHint();
        try {
            final zzbtp zza = this.zza;
            if (zza != null) {
                zza.zzv();
            }
        }
        catch (final RemoteException ex) {
            zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex);
        }
    }
    
    public final void setContentView(final int contentView) {
        super.setContentView(contentView);
        this.zza();
    }
    
    public final void setContentView(final View contentView) {
        super.setContentView(contentView);
        this.zza();
    }
    
    public final void setContentView(final View view, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        super.setContentView(view, viewGroup$LayoutParams);
        this.zza();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.query;

import com.google.android.gms.common.annotation.KeepForSdk;
import android.os.Bundle;
import com.google.android.gms.ads.internal.client.zzdx;
import com.google.android.gms.internal.ads.zzbug;
import com.google.android.gms.internal.ads.zzcbc;
import com.google.android.gms.ads.internal.client.zzba;
import com.google.android.gms.internal.ads.zzbet;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdFormat;
import android.content.Context;
import com.google.android.gms.ads.internal.client.zzem;

public class QueryInfo
{
    private final zzem zza;
    
    public QueryInfo(final zzem zza) {
        this.zza = zza;
    }
    
    public static void generate(final Context context, final AdFormat adFormat, final AdRequest adRequest, final QueryInfoGenerationCallback queryInfoGenerationCallback) {
        zza(context, adFormat, adRequest, null, queryInfoGenerationCallback);
    }
    
    public static void generate(final Context context, final AdFormat adFormat, final AdRequest adRequest, final String s, final QueryInfoGenerationCallback queryInfoGenerationCallback) {
        Preconditions.checkNotNull(s, "AdUnitId cannot be null.");
        zza(context, adFormat, adRequest, s, queryInfoGenerationCallback);
    }
    
    private static void zza(final Context context, final AdFormat adFormat, final AdRequest adRequest, final String s, final QueryInfoGenerationCallback queryInfoGenerationCallback) {
        zzbdc.zza(context);
        if ((boolean)zzbet.zzk.zze() && (boolean)zzba.zzc().zza(zzbdc.zzkt)) {
            zzcbc.zzb.execute(new zza(context, adFormat, adRequest, s, queryInfoGenerationCallback));
            return;
        }
        zzdx zza;
        if (adRequest == null) {
            zza = null;
        }
        else {
            zza = adRequest.zza();
        }
        new zzbug(context, adFormat, zza, s).zzb(queryInfoGenerationCallback);
    }
    
    public String getQuery() {
        return this.zza.zzb();
    }
    
    @KeepForSdk
    public Bundle getQueryBundle() {
        return this.zza.zza();
    }
    
    @KeepForSdk
    public String getRequestId() {
        return this.zza.zzc();
    }
}

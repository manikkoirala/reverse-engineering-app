// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.query;

import android.net.Uri;
import java.util.List;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public abstract class UpdateImpressionUrlsCallback
{
    @KeepForSdk
    public void onFailure(final String s) {
    }
    
    @KeepForSdk
    public void onSuccess(final List<Uri> list) {
    }
}

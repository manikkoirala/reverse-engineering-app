// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.query;

import android.net.Uri;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public abstract class UpdateClickUrlCallback
{
    @KeepForSdk
    public void onFailure(final String s) {
    }
    
    @KeepForSdk
    public void onSuccess(final Uri uri) {
    }
}

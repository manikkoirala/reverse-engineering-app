// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.query;

import java.util.Map;
import android.view.View;
import com.google.android.gms.internal.ads.zzbul;
import android.view.MotionEvent;
import android.net.Uri;
import java.util.List;
import com.google.android.gms.internal.ads.zzbum;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public final class ReportingInfo
{
    private final zzbum zza = new zzbum(Builder.zza(builder));
    
    @KeepForSdk
    public void recordClick(final List<Uri> list) {
        this.zza.zza((List)list);
    }
    
    @KeepForSdk
    public void recordImpression(final List<Uri> list) {
        this.zza.zzb((List)list);
    }
    
    @KeepForSdk
    public void reportTouchEvent(final MotionEvent motionEvent) {
        this.zza.zzc(motionEvent);
    }
    
    @KeepForSdk
    public void updateClickUrl(final Uri uri, final UpdateClickUrlCallback updateClickUrlCallback) {
        this.zza.zzd(uri, updateClickUrlCallback);
    }
    
    @KeepForSdk
    public void updateImpressionUrls(final List<Uri> list, final UpdateImpressionUrlsCallback updateImpressionUrlsCallback) {
        this.zza.zze((List)list, updateImpressionUrlsCallback);
    }
    
    @KeepForSdk
    public static final class Builder
    {
        private final zzbul zza;
        
        @KeepForSdk
        public Builder(final View view) {
            (this.zza = new zzbul()).zzb(view);
        }
        
        @KeepForSdk
        public ReportingInfo build() {
            return new ReportingInfo(this, null);
        }
        
        @KeepForSdk
        public Builder setAssetViews(final Map<String, View> map) {
            this.zza.zzc((Map)map);
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads;

public enum AdFormat
{
    APP_OPEN_AD("APP_OPEN_AD", 6, 6), 
    BANNER("BANNER", 0, 0), 
    INTERSTITIAL("INTERSTITIAL", 1, 1), 
    NATIVE("NATIVE", 4, 4), 
    REWARDED("REWARDED", 2, 2), 
    REWARDED_INTERSTITIAL("REWARDED_INTERSTITIAL", 3, 3), 
    @Deprecated
    UNKNOWN("UNKNOWN", 5, 5);
    
    private static final AdFormat[] zza;
    private final int zzb;
    
    private AdFormat(final String name, final int ordinal, final int zzb) {
        this.zzb = zzb;
    }
    
    public int getValue() {
        return this.zzb;
    }
}

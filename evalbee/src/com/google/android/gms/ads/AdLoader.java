// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads;

import com.google.android.gms.ads.internal.client.zzfl;
import com.google.android.gms.internal.ads.zzbfw;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.AdManagerAdViewOptions;
import com.google.android.gms.ads.internal.client.zzbh;
import com.google.android.gms.ads.internal.client.zzg;
import com.google.android.gms.internal.ads.zzbiq;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.internal.ads.zzbhw;
import com.google.android.gms.internal.ads.zzbte;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.internal.ads.zzbin;
import com.google.android.gms.ads.formats.NativeCustomTemplateAd;
import com.google.android.gms.internal.ads.zzbtc;
import com.google.android.gms.ads.nativead.NativeCustomFormatAd;
import com.google.android.gms.internal.ads.zzbht;
import com.google.android.gms.ads.internal.client.zzq;
import com.google.android.gms.internal.ads.zzbip;
import com.google.android.gms.ads.formats.OnAdManagerAdViewLoadedListener;
import com.google.android.gms.ads.internal.client.zzeu;
import com.google.android.gms.internal.ads.zzbpr;
import com.google.android.gms.internal.ads.zzbpo;
import com.google.android.gms.ads.internal.client.zzay;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.ads.internal.client.zzbq;
import com.google.android.gms.ads.admanager.AdManagerAdRequest;
import android.os.RemoteException;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.internal.ads.zzcbc;
import com.google.android.gms.ads.internal.client.zzba;
import com.google.android.gms.internal.ads.zzbet;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.ads.internal.client.zzdx;
import com.google.android.gms.ads.internal.client.zzbn;
import android.content.Context;
import com.google.android.gms.ads.internal.client.zzp;

public class AdLoader
{
    private final zzp zza;
    private final Context zzb;
    private final zzbn zzc;
    
    public AdLoader(final Context zzb, final zzbn zzc, final zzp zza) {
        this.zzb = zzb;
        this.zzc = zzc;
        this.zza = zza;
    }
    
    private final void zzb(final zzdx zzdx) {
        zzbdc.zza(this.zzb);
        if (zzbet.zzc.zze()) {
            if (zzba.zzc().zza(zzbdc.zzkt)) {
                zzcbc.zzb.execute(new zza(this, zzdx));
                return;
            }
        }
        try {
            this.zzc.zzg(this.zza.zza(this.zzb, zzdx));
        }
        catch (final RemoteException ex) {
            zzcbn.zzh("Failed to load ad.", (Throwable)ex);
        }
    }
    
    public boolean isLoading() {
        try {
            return this.zzc.zzi();
        }
        catch (final RemoteException ex) {
            zzcbn.zzk("Failed to check if ad is loading.", (Throwable)ex);
            return false;
        }
    }
    
    public void loadAd(final AdRequest adRequest) {
        this.zzb(adRequest.zza);
    }
    
    public void loadAd(final AdManagerAdRequest adManagerAdRequest) {
        this.zzb(adManagerAdRequest.zza);
    }
    
    public void loadAds(final AdRequest adRequest, final int n) {
        final zzdx zza = adRequest.zza;
        try {
            this.zzc.zzh(this.zza.zza(this.zzb, zza), n);
        }
        catch (final RemoteException ex) {
            zzcbn.zzh("Failed to load ads.", (Throwable)ex);
        }
    }
    
    public static class Builder
    {
        private final Context zza;
        private final zzbq zzb;
        
        public Builder(final Context context, final String s) {
            final Context zza = Preconditions.checkNotNull(context, "context cannot be null");
            final zzbq zzc = zzay.zza().zzc(context, s, (zzbpr)new zzbpo());
            this.zza = zza;
            this.zzb = zzc;
        }
        
        public AdLoader build() {
            try {
                return new AdLoader(this.zza, this.zzb.zze(), zzp.zza);
            }
            catch (final RemoteException ex) {
                zzcbn.zzh("Failed to build AdLoader.", (Throwable)ex);
                return new AdLoader(this.zza, new zzeu().zzc(), zzp.zza);
            }
        }
        
        public Builder forAdManagerAdView(final OnAdManagerAdViewLoadedListener onAdManagerAdViewLoadedListener, final AdSize... array) {
            if (array != null && array.length > 0) {
                try {
                    this.zzb.zzj((zzbht)new zzbip(onAdManagerAdViewLoadedListener), new zzq(this.zza, array));
                }
                catch (final RemoteException ex) {
                    zzcbn.zzk("Failed to add Google Ad Manager banner ad listener", (Throwable)ex);
                }
                return this;
            }
            throw new IllegalArgumentException("The supported ad sizes must contain at least one valid ad size.");
        }
        
        public Builder forCustomFormatAd(final String s, final NativeCustomFormatAd.OnCustomFormatAdLoadedListener onCustomFormatAdLoadedListener, final NativeCustomFormatAd.OnCustomClickListener onCustomClickListener) {
            final zzbtc zzbtc = new zzbtc(onCustomFormatAdLoadedListener, onCustomClickListener);
            try {
                this.zzb.zzh(s, zzbtc.zzb(), zzbtc.zza());
            }
            catch (final RemoteException ex) {
                zzcbn.zzk("Failed to add custom format ad listener", (Throwable)ex);
            }
            return this;
        }
        
        @Deprecated
        public Builder forCustomTemplateAd(final String s, final NativeCustomTemplateAd.OnCustomTemplateAdLoadedListener onCustomTemplateAdLoadedListener, final NativeCustomTemplateAd.OnCustomClickListener onCustomClickListener) {
            final zzbin zzbin = new zzbin(onCustomTemplateAdLoadedListener, onCustomClickListener);
            try {
                this.zzb.zzh(s, zzbin.zze(), zzbin.zzd());
            }
            catch (final RemoteException ex) {
                zzcbn.zzk("Failed to add custom template ad listener", (Throwable)ex);
            }
            return this;
        }
        
        public Builder forNativeAd(final NativeAd.OnNativeAdLoadedListener onNativeAdLoadedListener) {
            try {
                this.zzb.zzk((zzbhw)new zzbte(onNativeAdLoadedListener));
            }
            catch (final RemoteException ex) {
                zzcbn.zzk("Failed to add google native ad listener", (Throwable)ex);
            }
            return this;
        }
        
        @Deprecated
        public Builder forUnifiedNativeAd(final UnifiedNativeAd.OnUnifiedNativeAdLoadedListener onUnifiedNativeAdLoadedListener) {
            try {
                this.zzb.zzk((zzbhw)new zzbiq(onUnifiedNativeAdLoadedListener));
            }
            catch (final RemoteException ex) {
                zzcbn.zzk("Failed to add google native ad listener", (Throwable)ex);
            }
            return this;
        }
        
        public Builder withAdListener(final AdListener adListener) {
            try {
                this.zzb.zzl(new zzg(adListener));
            }
            catch (final RemoteException ex) {
                zzcbn.zzk("Failed to set AdListener.", (Throwable)ex);
            }
            return this;
        }
        
        public Builder withAdManagerAdViewOptions(final AdManagerAdViewOptions adManagerAdViewOptions) {
            try {
                this.zzb.zzm(adManagerAdViewOptions);
            }
            catch (final RemoteException ex) {
                zzcbn.zzk("Failed to specify Ad Manager banner ad options", (Throwable)ex);
            }
            return this;
        }
        
        @Deprecated
        public Builder withNativeAdOptions(final NativeAdOptions nativeAdOptions) {
            try {
                this.zzb.zzo(new zzbfw(nativeAdOptions));
            }
            catch (final RemoteException ex) {
                zzcbn.zzk("Failed to specify native ad options", (Throwable)ex);
            }
            return this;
        }
        
        public Builder withNativeAdOptions(final com.google.android.gms.ads.nativead.NativeAdOptions nativeAdOptions) {
            try {
                final zzbq zzb = this.zzb;
                final boolean shouldReturnUrlsForImageAssets = nativeAdOptions.shouldReturnUrlsForImageAssets();
                final boolean shouldRequestMultipleImages = nativeAdOptions.shouldRequestMultipleImages();
                final int adChoicesPlacement = nativeAdOptions.getAdChoicesPlacement();
                zzfl zzfl;
                if (nativeAdOptions.getVideoOptions() != null) {
                    zzfl = new zzfl(nativeAdOptions.getVideoOptions());
                }
                else {
                    zzfl = null;
                }
                zzb.zzo(new zzbfw(4, shouldReturnUrlsForImageAssets, -1, shouldRequestMultipleImages, adChoicesPlacement, zzfl, nativeAdOptions.zzc(), nativeAdOptions.getMediaAspectRatio(), nativeAdOptions.zza(), nativeAdOptions.zzb(), nativeAdOptions.zzd() - 1));
            }
            catch (final RemoteException ex) {
                zzcbn.zzk("Failed to specify native ad options", (Throwable)ex);
            }
            return this;
        }
    }
}

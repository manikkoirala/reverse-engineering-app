// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.appopen;

import com.google.android.gms.ads.AdLoadCallback;
import android.app.Activity;
import com.google.android.gms.ads.ResponseInfo;
import com.google.android.gms.ads.OnPaidEventListener;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.admanager.AdManagerAdRequest;
import com.google.android.gms.internal.ads.zzaxr;
import com.google.android.gms.internal.ads.zzcbc;
import com.google.android.gms.ads.internal.client.zzba;
import com.google.android.gms.internal.ads.zzbet;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.ads.AdRequest;
import android.content.Context;

public abstract class AppOpenAd
{
    public static final int APP_OPEN_AD_ORIENTATION_LANDSCAPE = 2;
    public static final int APP_OPEN_AD_ORIENTATION_PORTRAIT = 1;
    
    @Deprecated
    public static void load(final Context context, final String s, final AdRequest adRequest, @AppOpenAdOrientation final int n, final AppOpenAdLoadCallback appOpenAdLoadCallback) {
        Preconditions.checkNotNull(context, "Context cannot be null.");
        Preconditions.checkNotNull(s, "adUnitId cannot be null.");
        Preconditions.checkNotNull(adRequest, "AdRequest cannot be null.");
        Preconditions.checkMainThread("#008 Must be called on the main UI thread.");
        zzbdc.zza(context);
        if ((boolean)zzbet.zzd.zze() && (boolean)zzba.zzc().zza(zzbdc.zzkt)) {
            zzcbc.zzb.execute(new zzb(context, s, adRequest, n, appOpenAdLoadCallback));
            return;
        }
        new zzaxr(context, s, adRequest.zza(), n, appOpenAdLoadCallback).zza();
    }
    
    public static void load(final Context context, final String s, final AdRequest adRequest, final AppOpenAdLoadCallback appOpenAdLoadCallback) {
        Preconditions.checkNotNull(context, "Context cannot be null.");
        Preconditions.checkNotNull(s, "adUnitId cannot be null.");
        Preconditions.checkNotNull(adRequest, "AdRequest cannot be null.");
        Preconditions.checkMainThread("#008 Must be called on the main UI thread.");
        zzbdc.zza(context);
        if ((boolean)zzbet.zzd.zze() && (boolean)zzba.zzc().zza(zzbdc.zzkt)) {
            zzcbc.zzb.execute(new zza(context, s, adRequest, appOpenAdLoadCallback));
            return;
        }
        new zzaxr(context, s, adRequest.zza(), 3, appOpenAdLoadCallback).zza();
    }
    
    @Deprecated
    public static void load(final Context context, final String s, final AdManagerAdRequest adManagerAdRequest, @AppOpenAdOrientation final int n, final AppOpenAdLoadCallback appOpenAdLoadCallback) {
        Preconditions.checkNotNull(context, "Context cannot be null.");
        Preconditions.checkNotNull(s, "adUnitId cannot be null.");
        Preconditions.checkNotNull(adManagerAdRequest, "AdManagerAdRequest cannot be null.");
        Preconditions.checkMainThread("#008 Must be called on the main UI thread.");
        zzbdc.zza(context);
        if ((boolean)zzbet.zzd.zze() && (boolean)zzba.zzc().zza(zzbdc.zzkt)) {
            zzcbc.zzb.execute(new zzc(context, s, adManagerAdRequest, n, appOpenAdLoadCallback));
            return;
        }
        new zzaxr(context, s, adManagerAdRequest.zza(), n, appOpenAdLoadCallback).zza();
    }
    
    public abstract String getAdUnitId();
    
    public abstract FullScreenContentCallback getFullScreenContentCallback();
    
    public abstract OnPaidEventListener getOnPaidEventListener();
    
    public abstract ResponseInfo getResponseInfo();
    
    public abstract void setFullScreenContentCallback(final FullScreenContentCallback p0);
    
    public abstract void setImmersiveMode(final boolean p0);
    
    public abstract void setOnPaidEventListener(final OnPaidEventListener p0);
    
    public abstract void show(final Activity p0);
    
    public abstract static class AppOpenAdLoadCallback extends AdLoadCallback<AppOpenAd>
    {
    }
    
    public @interface AppOpenAdOrientation {
    }
}

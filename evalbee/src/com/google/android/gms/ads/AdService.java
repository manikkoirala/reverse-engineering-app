// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads;

import android.os.RemoteException;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.internal.ads.zzbpr;
import android.content.Context;
import com.google.android.gms.internal.ads.zzbpo;
import com.google.android.gms.ads.internal.client.zzay;
import android.content.Intent;
import com.google.android.gms.common.annotation.KeepForSdk;
import android.app.IntentService;

@KeepForSdk
public class AdService extends IntentService
{
    @KeepForSdk
    public static final String CLASS_NAME = "com.google.android.gms.ads.AdService";
    
    public AdService() {
        super("AdService");
    }
    
    public final void onHandleIntent(final Intent intent) {
        try {
            zzay.zza().zzm((Context)this, (zzbpr)new zzbpo()).zze(intent);
        }
        catch (final RemoteException ex) {
            zzcbn.zzg("RemoteException calling handleNotificationIntent: ".concat(ex.toString()));
        }
    }
}

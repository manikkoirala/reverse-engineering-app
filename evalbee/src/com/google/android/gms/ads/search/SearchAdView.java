// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.search;

import com.google.android.gms.internal.ads.zzcbn;
import android.view.View;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdListener;
import android.util.AttributeSet;
import android.content.Context;
import org.checkerframework.checker.initialization.qual.NotOnlyInitialized;
import com.google.android.gms.ads.internal.client.zzea;
import android.view.ViewGroup;

public final class SearchAdView extends ViewGroup
{
    @NotOnlyInitialized
    private final zzea zza;
    
    public SearchAdView(final Context context) {
        super(context);
        this.zza = new zzea(this);
    }
    
    public SearchAdView(final Context context, final AttributeSet set) {
        super(context, set);
        this.zza = new zzea(this, set, false);
    }
    
    public SearchAdView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.zza = new zzea(this, set, false);
    }
    
    public void destroy() {
        this.zza.zzk();
    }
    
    public AdListener getAdListener() {
        return this.zza.zza();
    }
    
    public AdSize getAdSize() {
        return this.zza.zzb();
    }
    
    public String getAdUnitId() {
        return this.zza.zzj();
    }
    
    public void loadAd(final DynamicHeightSearchAdRequest dynamicHeightSearchAdRequest) {
        if (AdSize.SEARCH.equals(this.getAdSize())) {
            this.zza.zzm(dynamicHeightSearchAdRequest.zza());
            return;
        }
        throw new IllegalStateException("You must use AdSize.SEARCH for a DynamicHeightSearchAdRequest");
    }
    
    public void loadAd(final SearchAdRequest searchAdRequest) {
        this.zza.zzm(searchAdRequest.zza());
    }
    
    public void onLayout(final boolean b, int n, int n2, final int n3, final int n4) {
        final View child = this.getChildAt(0);
        if (child != null && child.getVisibility() != 8) {
            final int measuredWidth = child.getMeasuredWidth();
            final int measuredHeight = child.getMeasuredHeight();
            n = (n3 - n - measuredWidth) / 2;
            n2 = (n4 - n2 - measuredHeight) / 2;
            child.layout(n, n2, measuredWidth + n, measuredHeight + n2);
        }
    }
    
    public void onMeasure(final int n, final int n2) {
        int a = 0;
        final View child = this.getChildAt(0);
        int a2;
        if (child != null && child.getVisibility() != 8) {
            this.measureChild(child, n, n2);
            a = child.getMeasuredWidth();
            a2 = child.getMeasuredHeight();
        }
        else {
            AdSize adSize;
            try {
                adSize = this.getAdSize();
            }
            catch (final NullPointerException ex) {
                zzcbn.zzh("Unable to retrieve ad size.", (Throwable)ex);
                adSize = null;
            }
            if (adSize != null) {
                final Context context = ((View)this).getContext();
                a = adSize.getWidthInPixels(context);
                a2 = adSize.getHeightInPixels(context);
            }
            else {
                a2 = 0;
            }
        }
        ((View)this).setMeasuredDimension(View.resolveSize(Math.max(a, ((View)this).getSuggestedMinimumWidth()), n), View.resolveSize(Math.max(a2, ((View)this).getSuggestedMinimumHeight()), n2));
    }
    
    public void pause() {
        this.zza.zzn();
    }
    
    public void resume() {
        this.zza.zzp();
    }
    
    public void setAdListener(final AdListener adListener) {
        this.zza.zzr(adListener);
    }
    
    public void setAdSize(final AdSize adSize) {
        this.zza.zzs(adSize);
    }
    
    public void setAdUnitId(final String s) {
        this.zza.zzu(s);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.search;

import android.content.Context;
import com.google.android.gms.ads.mediation.MediationAdapter;
import com.google.android.gms.ads.mediation.NetworkExtras;
import android.location.Location;
import android.os.Bundle;
import com.google.android.gms.ads.mediation.customevent.CustomEvent;
import org.checkerframework.checker.initialization.qual.NotOnlyInitialized;
import com.google.android.gms.ads.internal.client.zzdx;

@Deprecated
public final class SearchAdRequest
{
    public static final int BORDER_TYPE_DASHED = 1;
    public static final int BORDER_TYPE_DOTTED = 2;
    public static final int BORDER_TYPE_NONE = 0;
    public static final int BORDER_TYPE_SOLID = 3;
    public static final int CALL_BUTTON_COLOR_DARK = 2;
    public static final int CALL_BUTTON_COLOR_LIGHT = 0;
    public static final int CALL_BUTTON_COLOR_MEDIUM = 1;
    public static final String DEVICE_ID_EMULATOR = "B3EEABB8EE11C2BE770B684D95219ECB";
    public static final int ERROR_CODE_INTERNAL_ERROR = 0;
    public static final int ERROR_CODE_INVALID_REQUEST = 1;
    public static final int ERROR_CODE_NETWORK_ERROR = 2;
    public static final int ERROR_CODE_NO_FILL = 3;
    @NotOnlyInitialized
    private final zzdx zza = new zzdx(com.google.android.gms.ads.search.zzb.zza(zzb), this);
    private final String zzb = com.google.android.gms.ads.search.zzb.zzf(zzb);
    
    @Deprecated
    public int getAnchorTextColor() {
        return 0;
    }
    
    @Deprecated
    public int getBackgroundColor() {
        return 0;
    }
    
    @Deprecated
    public int getBackgroundGradientBottom() {
        return 0;
    }
    
    @Deprecated
    public int getBackgroundGradientTop() {
        return 0;
    }
    
    @Deprecated
    public int getBorderColor() {
        return 0;
    }
    
    @Deprecated
    public int getBorderThickness() {
        return 0;
    }
    
    @Deprecated
    public int getBorderType() {
        return 0;
    }
    
    @Deprecated
    public int getCallButtonColor() {
        return 0;
    }
    
    @Deprecated
    public String getCustomChannels() {
        return null;
    }
    
    public <T extends CustomEvent> Bundle getCustomEventExtrasBundle(final Class<T> clazz) {
        return this.zza.zzd(clazz);
    }
    
    @Deprecated
    public int getDescriptionTextColor() {
        return 0;
    }
    
    @Deprecated
    public String getFontFace() {
        return null;
    }
    
    @Deprecated
    public int getHeaderTextColor() {
        return 0;
    }
    
    @Deprecated
    public int getHeaderTextSize() {
        return 0;
    }
    
    public Location getLocation() {
        return null;
    }
    
    @Deprecated
    public <T extends NetworkExtras> T getNetworkExtras(final Class<T> clazz) {
        return (T)this.zza.zzh(clazz);
    }
    
    public <T extends MediationAdapter> Bundle getNetworkExtrasBundle(final Class<T> clazz) {
        return this.zza.zzf(clazz);
    }
    
    public String getQuery() {
        return this.zzb;
    }
    
    public boolean isTestDevice(final Context context) {
        return this.zza.zzs(context);
    }
    
    public final zzdx zza() {
        return this.zza;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.search;

import android.os.BaseBundle;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.mediation.NetworkExtras;
import com.google.android.gms.ads.internal.client.zzdx;
import android.content.Context;
import com.google.android.gms.ads.mediation.MediationAdapter;
import android.os.Bundle;
import com.google.android.gms.ads.mediation.customevent.CustomEvent;

public final class DynamicHeightSearchAdRequest
{
    private final SearchAdRequest zza = new SearchAdRequest(Builder.zza(builder), null);
    
    public <T extends CustomEvent> Bundle getCustomEventExtrasBundle(final Class<T> clazz) {
        return this.zza.getCustomEventExtrasBundle(clazz);
    }
    
    public <T extends MediationAdapter> Bundle getNetworkExtrasBundle(final Class<T> clazz) {
        return this.zza.getNetworkExtrasBundle(clazz);
    }
    
    public String getQuery() {
        return this.zza.getQuery();
    }
    
    public boolean isTestDevice(final Context context) {
        return this.zza.isTestDevice(context);
    }
    
    public final zzdx zza() {
        return this.zza.zza();
    }
    
    public static final class Builder
    {
        private final zzb zza;
        private final Bundle zzb;
        
        public Builder() {
            this.zza = new zzb();
            this.zzb = new Bundle();
        }
        
        public Builder addCustomEventExtrasBundle(final Class<? extends CustomEvent> clazz, final Bundle bundle) {
            this.zza.zzb(clazz, bundle);
            return this;
        }
        
        public Builder addNetworkExtras(final NetworkExtras networkExtras) {
            this.zza.zzc(networkExtras);
            return this;
        }
        
        public Builder addNetworkExtrasBundle(final Class<? extends MediationAdapter> clazz, final Bundle bundle) {
            this.zza.zzd(clazz, bundle);
            return this;
        }
        
        public DynamicHeightSearchAdRequest build() {
            this.zza.zzd(AdMobAdapter.class, this.zzb);
            return new DynamicHeightSearchAdRequest(this, null);
        }
        
        public Builder setAdBorderSelectors(final String s) {
            ((BaseBundle)this.zzb).putString("csa_adBorderSelectors", s);
            return this;
        }
        
        public Builder setAdTest(final boolean b) {
            String s;
            if (!b) {
                s = "off";
            }
            else {
                s = "on";
            }
            ((BaseBundle)this.zzb).putString("csa_adtest", s);
            return this;
        }
        
        public Builder setAdjustableLineHeight(final int i) {
            ((BaseBundle)this.zzb).putString("csa_adjustableLineHeight", Integer.toString(i));
            return this;
        }
        
        public Builder setAdvancedOptionValue(final String s, final String s2) {
            ((BaseBundle)this.zzb).putString(s, s2);
            return this;
        }
        
        public Builder setAttributionSpacingBelow(final int i) {
            ((BaseBundle)this.zzb).putString("csa_attributionSpacingBelow", Integer.toString(i));
            return this;
        }
        
        public Builder setBorderSelections(final String s) {
            ((BaseBundle)this.zzb).putString("csa_borderSelections", s);
            return this;
        }
        
        public Builder setChannel(final String s) {
            ((BaseBundle)this.zzb).putString("csa_channel", s);
            return this;
        }
        
        public Builder setColorAdBorder(final String s) {
            ((BaseBundle)this.zzb).putString("csa_colorAdBorder", s);
            return this;
        }
        
        public Builder setColorAdSeparator(final String s) {
            ((BaseBundle)this.zzb).putString("csa_colorAdSeparator", s);
            return this;
        }
        
        public Builder setColorAnnotation(final String s) {
            ((BaseBundle)this.zzb).putString("csa_colorAnnotation", s);
            return this;
        }
        
        public Builder setColorAttribution(final String s) {
            ((BaseBundle)this.zzb).putString("csa_colorAttribution", s);
            return this;
        }
        
        public Builder setColorBackground(final String s) {
            ((BaseBundle)this.zzb).putString("csa_colorBackground", s);
            return this;
        }
        
        public Builder setColorBorder(final String s) {
            ((BaseBundle)this.zzb).putString("csa_colorBorder", s);
            return this;
        }
        
        public Builder setColorDomainLink(final String s) {
            ((BaseBundle)this.zzb).putString("csa_colorDomainLink", s);
            return this;
        }
        
        public Builder setColorText(final String s) {
            ((BaseBundle)this.zzb).putString("csa_colorText", s);
            return this;
        }
        
        public Builder setColorTitleLink(final String s) {
            ((BaseBundle)this.zzb).putString("csa_colorTitleLink", s);
            return this;
        }
        
        public Builder setCssWidth(final int i) {
            ((BaseBundle)this.zzb).putString("csa_width", Integer.toString(i));
            return this;
        }
        
        public Builder setDetailedAttribution(final boolean b) {
            ((BaseBundle)this.zzb).putString("csa_detailedAttribution", Boolean.toString(b));
            return this;
        }
        
        public Builder setFontFamily(final String s) {
            ((BaseBundle)this.zzb).putString("csa_fontFamily", s);
            return this;
        }
        
        public Builder setFontFamilyAttribution(final String s) {
            ((BaseBundle)this.zzb).putString("csa_fontFamilyAttribution", s);
            return this;
        }
        
        public Builder setFontSizeAnnotation(final int i) {
            ((BaseBundle)this.zzb).putString("csa_fontSizeAnnotation", Integer.toString(i));
            return this;
        }
        
        public Builder setFontSizeAttribution(final int i) {
            ((BaseBundle)this.zzb).putString("csa_fontSizeAttribution", Integer.toString(i));
            return this;
        }
        
        public Builder setFontSizeDescription(final int i) {
            ((BaseBundle)this.zzb).putString("csa_fontSizeDescription", Integer.toString(i));
            return this;
        }
        
        public Builder setFontSizeDomainLink(final int i) {
            ((BaseBundle)this.zzb).putString("csa_fontSizeDomainLink", Integer.toString(i));
            return this;
        }
        
        public Builder setFontSizeTitle(final int i) {
            ((BaseBundle)this.zzb).putString("csa_fontSizeTitle", Integer.toString(i));
            return this;
        }
        
        public Builder setHostLanguage(final String s) {
            ((BaseBundle)this.zzb).putString("csa_hl", s);
            return this;
        }
        
        public Builder setIsClickToCallEnabled(final boolean b) {
            ((BaseBundle)this.zzb).putString("csa_clickToCall", Boolean.toString(b));
            return this;
        }
        
        public Builder setIsLocationEnabled(final boolean b) {
            ((BaseBundle)this.zzb).putString("csa_location", Boolean.toString(b));
            return this;
        }
        
        public Builder setIsPlusOnesEnabled(final boolean b) {
            ((BaseBundle)this.zzb).putString("csa_plusOnes", Boolean.toString(b));
            return this;
        }
        
        public Builder setIsSellerRatingsEnabled(final boolean b) {
            ((BaseBundle)this.zzb).putString("csa_sellerRatings", Boolean.toString(b));
            return this;
        }
        
        public Builder setIsSiteLinksEnabled(final boolean b) {
            ((BaseBundle)this.zzb).putString("csa_siteLinks", Boolean.toString(b));
            return this;
        }
        
        public Builder setIsTitleBold(final boolean b) {
            ((BaseBundle)this.zzb).putString("csa_titleBold", Boolean.toString(b));
            return this;
        }
        
        public Builder setIsTitleUnderlined(final boolean b) {
            ((BaseBundle)this.zzb).putString("csa_noTitleUnderline", Boolean.toString(b ^ true));
            return this;
        }
        
        public Builder setLocationColor(final String s) {
            ((BaseBundle)this.zzb).putString("csa_colorLocation", s);
            return this;
        }
        
        public Builder setLocationFontSize(final int i) {
            ((BaseBundle)this.zzb).putString("csa_fontSizeLocation", Integer.toString(i));
            return this;
        }
        
        public Builder setLongerHeadlines(final boolean b) {
            ((BaseBundle)this.zzb).putString("csa_longerHeadlines", Boolean.toString(b));
            return this;
        }
        
        public Builder setNumber(final int i) {
            ((BaseBundle)this.zzb).putString("csa_number", Integer.toString(i));
            return this;
        }
        
        public Builder setPage(final int i) {
            ((BaseBundle)this.zzb).putString("csa_adPage", Integer.toString(i));
            return this;
        }
        
        public Builder setQuery(final String s) {
            this.zza.zze(s);
            return this;
        }
        
        public Builder setStyleId(final String s) {
            ((BaseBundle)this.zzb).putString("csa_styleId", s);
            return this;
        }
        
        public Builder setVerticalSpacing(final int i) {
            ((BaseBundle)this.zzb).putString("csa_verticalSpacing", Integer.toString(i));
            return this;
        }
    }
}

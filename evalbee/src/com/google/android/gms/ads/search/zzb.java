// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.search;

import com.google.android.gms.ads.mediation.NetworkExtras;
import android.os.Bundle;
import com.google.android.gms.ads.internal.client.zzdw;

public final class zzb
{
    private final zzdw zza;
    private String zzb;
    
    public zzb() {
        this.zza = new zzdw();
    }
    
    public final zzb zzb(final Class clazz, final Bundle bundle) {
        this.zza.zzq(clazz, bundle);
        return this;
    }
    
    public final zzb zzc(final NetworkExtras networkExtras) {
        this.zza.zzu(networkExtras);
        return this;
    }
    
    public final zzb zzd(final Class clazz, final Bundle bundle) {
        this.zza.zzt(clazz, bundle);
        return this;
    }
    
    public final zzb zze(final String zzb) {
        this.zzb = zzb;
        return this;
    }
}

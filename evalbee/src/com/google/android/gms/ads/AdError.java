// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads;

import org.json.JSONObject;
import android.os.IBinder;
import com.google.android.gms.ads.internal.client.zze;
import org.json.JSONException;

public class AdError
{
    public static final String UNDEFINED_DOMAIN = "undefined";
    private final int zza;
    private final String zzb;
    private final String zzc;
    private final AdError zzd;
    
    public AdError(final int n, final String s, final String s2) {
        this(n, s, s2, null);
    }
    
    public AdError(final int zza, final String zzb, final String zzc, final AdError zzd) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
    }
    
    public AdError getCause() {
        return this.zzd;
    }
    
    public int getCode() {
        return this.zza;
    }
    
    public String getDomain() {
        return this.zzc;
    }
    
    public String getMessage() {
        return this.zzb;
    }
    
    @Override
    public String toString() {
        String string;
        try {
            string = this.zzb().toString(2);
        }
        catch (final JSONException ex) {
            string = "Error forming toString output.";
        }
        return string;
    }
    
    public final zze zza() {
        final AdError zzd = this.zzd;
        zze zze;
        if (zzd == null) {
            zze = null;
        }
        else {
            zze = new zze(zzd.zza, zzd.zzb, zzd.zzc, null, null);
        }
        return new zze(this.zza, this.zzb, this.zzc, zze, null);
    }
    
    public JSONObject zzb() {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("Code", this.zza);
        jsonObject.put("Message", (Object)this.zzb);
        jsonObject.put("Domain", (Object)this.zzc);
        final AdError zzd = this.zzd;
        Object zzb;
        if (zzd == null) {
            zzb = "null";
        }
        else {
            zzb = zzd.zzb();
        }
        jsonObject.put("Cause", zzb);
        return jsonObject;
    }
}

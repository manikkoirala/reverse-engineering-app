// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.Parcelable$Creator;
import com.google.android.gms.internal.ads.zzavi;
import android.os.Parcel;
import com.google.android.gms.internal.ads.zzavh;

public abstract class zzcz extends zzavh implements zzda
{
    public zzcz() {
        super("com.google.android.gms.ads.internal.client.IOnAdInspectorClosedListener");
    }
    
    public final boolean zzbK(final int n, final Parcel parcel, final Parcel parcel2, final int n2) {
        if (n == 1) {
            final zze zze = (zze)zzavi.zza(parcel, (Parcelable$Creator)com.google.android.gms.ads.internal.client.zze.CREATOR);
            zzavi.zzc(parcel);
            this.zze(zze);
            parcel2.writeNoException();
            return true;
        }
        return false;
    }
}

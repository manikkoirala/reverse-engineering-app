// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import android.graphics.Rect;
import java.util.Iterator;
import com.google.android.gms.internal.ads.zzful;
import com.google.android.gms.internal.ads.zzftk;
import com.google.android.gms.internal.ads.zzfdu;
import android.view.ViewParent;
import android.widget.AdapterView;
import android.widget.ScrollView;
import com.google.android.gms.ads.internal.zzt;
import android.widget.ImageView$ScaleType;
import java.util.Map;
import org.json.JSONException;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.ads.internal.client.zzay;
import org.json.JSONObject;
import android.content.Context;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.ads.internal.client.zzba;
import android.view.WindowManager$LayoutParams;
import android.graphics.Point;
import android.view.View;
import android.view.MotionEvent;

public final class zzbz
{
    public static Point zza(final MotionEvent motionEvent, final View view) {
        final int[] zzj = zzj(view);
        return new Point((int)motionEvent.getRawX() - zzj[0], (int)motionEvent.getRawY() - zzj[1]);
    }
    
    public static WindowManager$LayoutParams zzb() {
        final WindowManager$LayoutParams windowManager$LayoutParams = new WindowManager$LayoutParams(-2, -2, 0, 0, -2);
        windowManager$LayoutParams.flags = (int)zzba.zzc().zza(zzbdc.zzhS);
        windowManager$LayoutParams.type = 2;
        windowManager$LayoutParams.gravity = 8388659;
        return windowManager$LayoutParams;
    }
    
    public static JSONObject zzc(final String s, final Context context, final Point point, final Point point2) {
        JSONObject jsonObject = null;
        final Object o = null;
        JSONObject jsonObject3;
        try {
            final JSONObject jsonObject2 = new JSONObject();
            try {
                jsonObject = new JSONObject();
                Object o2;
                try {
                    jsonObject.put("x", zzay.zzb().zzb(context, point2.x));
                    jsonObject.put("y", zzay.zzb().zzb(context, point2.y));
                    jsonObject.put("start_x", zzay.zzb().zzb(context, point.x));
                    jsonObject.put("start_y", zzay.zzb().zzb(context, point.y));
                    o2 = jsonObject;
                }
                catch (final JSONException ex) {
                    zzcbn.zzh("Error occurred while putting signals into JSON object.", (Throwable)ex);
                    o2 = o;
                }
                jsonObject2.put("click_point", o2);
                jsonObject2.put("asset_id", (Object)s);
            }
            catch (final Exception ex2) {
                jsonObject3 = jsonObject2;
            }
        }
        catch (final Exception ex2) {
            jsonObject3 = jsonObject;
        }
        final Exception ex2;
        zzcbn.zzh("Error occurred while grabbing click signals.", (Throwable)ex2);
        return jsonObject3;
    }
    
    public static JSONObject zzd(final Context p0, final Map p1, final Map p2, final View p3, final ImageView$ScaleType p4) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     2: astore          14
        //     4: ldc             "relative_to"
        //     6: astore          15
        //     8: new             Lorg/json/JSONObject;
        //    11: dup            
        //    12: invokespecial   org/json/JSONObject.<init>:()V
        //    15: astore          11
        //    17: aload           11
        //    19: astore          13
        //    21: aload_1        
        //    22: ifnull          837
        //    25: aload_3        
        //    26: ifnonnull       36
        //    29: aload           11
        //    31: astore          13
        //    33: goto            837
        //    36: aload_3        
        //    37: invokestatic    com/google/android/gms/ads/internal/util/zzbz.zzj:(Landroid/view/View;)[I
        //    40: astore          16
        //    42: aload_1        
        //    43: invokeinterface java/util/Map.entrySet:()Ljava/util/Set;
        //    48: invokeinterface java/util/Set.iterator:()Ljava/util/Iterator;
        //    53: astore          12
        //    55: aload           11
        //    57: astore_1       
        //    58: aload           15
        //    60: astore_3       
        //    61: aload           14
        //    63: astore          11
        //    65: aload_1        
        //    66: astore          13
        //    68: aload           12
        //    70: invokeinterface java/util/Iterator.hasNext:()Z
        //    75: ifeq            837
        //    78: aload           12
        //    80: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //    85: checkcast       Ljava/util/Map$Entry;
        //    88: astore          15
        //    90: aload           15
        //    92: invokeinterface java/util/Map$Entry.getValue:()Ljava/lang/Object;
        //    97: checkcast       Ljava/lang/ref/WeakReference;
        //   100: invokevirtual   java/lang/ref/Reference.get:()Ljava/lang/Object;
        //   103: checkcast       Landroid/view/View;
        //   106: astore          17
        //   108: aload           17
        //   110: ifnull          65
        //   113: aload           17
        //   115: invokestatic    com/google/android/gms/ads/internal/util/zzbz.zzj:(Landroid/view/View;)[I
        //   118: astore          18
        //   120: new             Lorg/json/JSONObject;
        //   123: dup            
        //   124: invokespecial   org/json/JSONObject.<init>:()V
        //   127: astore          14
        //   129: new             Lorg/json/JSONObject;
        //   132: dup            
        //   133: invokespecial   org/json/JSONObject.<init>:()V
        //   136: astore          13
        //   138: aload           17
        //   140: invokevirtual   android/view/View.getMeasuredWidth:()I
        //   143: istore          8
        //   145: aload           13
        //   147: ldc             "width"
        //   149: invokestatic    com/google/android/gms/ads/internal/client/zzay.zzb:()Lcom/google/android/gms/internal/ads/zzcbg;
        //   152: aload_0        
        //   153: iload           8
        //   155: invokevirtual   com/google/android/gms/internal/ads/zzcbg.zzb:(Landroid/content/Context;I)I
        //   158: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //   161: pop            
        //   162: aload           17
        //   164: invokevirtual   android/view/View.getMeasuredHeight:()I
        //   167: istore          8
        //   169: aload           13
        //   171: ldc             "height"
        //   173: invokestatic    com/google/android/gms/ads/internal/client/zzay.zzb:()Lcom/google/android/gms/internal/ads/zzcbg;
        //   176: aload_0        
        //   177: iload           8
        //   179: invokevirtual   com/google/android/gms/internal/ads/zzcbg.zzb:(Landroid/content/Context;I)I
        //   182: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //   185: pop            
        //   186: aload           18
        //   188: iconst_0       
        //   189: iaload         
        //   190: istore          8
        //   192: aload           16
        //   194: iconst_0       
        //   195: iaload         
        //   196: istore          9
        //   198: aload           13
        //   200: ldc             "x"
        //   202: invokestatic    com/google/android/gms/ads/internal/client/zzay.zzb:()Lcom/google/android/gms/internal/ads/zzcbg;
        //   205: aload_0        
        //   206: iload           8
        //   208: iload           9
        //   210: isub           
        //   211: invokevirtual   com/google/android/gms/internal/ads/zzcbg.zzb:(Landroid/content/Context;I)I
        //   214: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //   217: pop            
        //   218: aload           18
        //   220: iconst_1       
        //   221: iaload         
        //   222: istore          8
        //   224: aload           16
        //   226: iconst_1       
        //   227: iaload         
        //   228: istore          9
        //   230: aload           13
        //   232: ldc             "y"
        //   234: invokestatic    com/google/android/gms/ads/internal/client/zzay.zzb:()Lcom/google/android/gms/internal/ads/zzcbg;
        //   237: aload_0        
        //   238: iload           8
        //   240: iload           9
        //   242: isub           
        //   243: invokevirtual   com/google/android/gms/internal/ads/zzcbg.zzb:(Landroid/content/Context;I)I
        //   246: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //   249: pop            
        //   250: aload           13
        //   252: aload_3        
        //   253: aload           11
        //   255: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //   258: pop            
        //   259: aload           14
        //   261: ldc             "frame"
        //   263: aload           13
        //   265: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //   268: pop            
        //   269: new             Landroid/graphics/Rect;
        //   272: astore          13
        //   274: aload           13
        //   276: invokespecial   android/graphics/Rect.<init>:()V
        //   279: aload           17
        //   281: aload           13
        //   283: invokevirtual   android/view/View.getLocalVisibleRect:(Landroid/graphics/Rect;)Z
        //   286: ifeq            300
        //   289: aload_0        
        //   290: aload           13
        //   292: invokestatic    com/google/android/gms/ads/internal/util/zzbz.zzk:(Landroid/content/Context;Landroid/graphics/Rect;)Lorg/json/JSONObject;
        //   295: astore          13
        //   297: goto            401
        //   300: new             Lorg/json/JSONObject;
        //   303: astore          13
        //   305: aload           13
        //   307: invokespecial   org/json/JSONObject.<init>:()V
        //   310: aload           13
        //   312: ldc             "width"
        //   314: iconst_0       
        //   315: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //   318: pop            
        //   319: aload           13
        //   321: ldc             "height"
        //   323: iconst_0       
        //   324: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //   327: pop            
        //   328: aload           18
        //   330: iconst_0       
        //   331: iaload         
        //   332: istore          9
        //   334: aload           16
        //   336: iconst_0       
        //   337: iaload         
        //   338: istore          8
        //   340: aload           13
        //   342: ldc             "x"
        //   344: invokestatic    com/google/android/gms/ads/internal/client/zzay.zzb:()Lcom/google/android/gms/internal/ads/zzcbg;
        //   347: aload_0        
        //   348: iload           9
        //   350: iload           8
        //   352: isub           
        //   353: invokevirtual   com/google/android/gms/internal/ads/zzcbg.zzb:(Landroid/content/Context;I)I
        //   356: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //   359: pop            
        //   360: aload           18
        //   362: iconst_1       
        //   363: iaload         
        //   364: istore          8
        //   366: aload           16
        //   368: iconst_1       
        //   369: iaload         
        //   370: istore          9
        //   372: aload           13
        //   374: ldc             "y"
        //   376: invokestatic    com/google/android/gms/ads/internal/client/zzay.zzb:()Lcom/google/android/gms/internal/ads/zzcbg;
        //   379: aload_0        
        //   380: iload           8
        //   382: iload           9
        //   384: isub           
        //   385: invokevirtual   com/google/android/gms/internal/ads/zzcbg.zzb:(Landroid/content/Context;I)I
        //   388: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //   391: pop            
        //   392: aload           13
        //   394: aload_3        
        //   395: aload           11
        //   397: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //   400: pop            
        //   401: aload           14
        //   403: ldc             "visible_bounds"
        //   405: aload           13
        //   407: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //   410: pop            
        //   411: aload           15
        //   413: invokeinterface java/util/Map$Entry.getKey:()Ljava/lang/Object;
        //   418: checkcast       Ljava/lang/String;
        //   421: ldc             "3010"
        //   423: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   426: ifeq            680
        //   429: getstatic       com/google/android/gms/internal/ads/zzbdc.zzhM:Lcom/google/android/gms/internal/ads/zzbcu;
        //   432: astore          13
        //   434: invokestatic    com/google/android/gms/ads/internal/client/zzba.zzc:()Lcom/google/android/gms/internal/ads/zzbda;
        //   437: aload           13
        //   439: invokevirtual   com/google/android/gms/internal/ads/zzbda.zza:(Lcom/google/android/gms/internal/ads/zzbcu;)Ljava/lang/Object;
        //   442: checkcast       Ljava/lang/Boolean;
        //   445: invokevirtual   java/lang/Boolean.booleanValue:()Z
        //   448: ifeq            467
        //   451: aload           14
        //   453: ldc             "mediaview_graphics_matrix"
        //   455: aload           17
        //   457: invokevirtual   android/view/View.getMatrix:()Landroid/graphics/Matrix;
        //   460: invokevirtual   android/graphics/Matrix.toShortString:()Ljava/lang/String;
        //   463: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //   466: pop            
        //   467: getstatic       com/google/android/gms/internal/ads/zzbdc.zzhN:Lcom/google/android/gms/internal/ads/zzbcu;
        //   470: astore          13
        //   472: invokestatic    com/google/android/gms/ads/internal/client/zzba.zzc:()Lcom/google/android/gms/internal/ads/zzbda;
        //   475: aload           13
        //   477: invokevirtual   com/google/android/gms/internal/ads/zzbda.zza:(Lcom/google/android/gms/internal/ads/zzbcu;)Ljava/lang/Object;
        //   480: checkcast       Ljava/lang/Boolean;
        //   483: invokevirtual   java/lang/Boolean.booleanValue:()Z
        //   486: ifeq            532
        //   489: aload           17
        //   491: invokevirtual   android/view/View.getLayoutParams:()Landroid/view/ViewGroup$LayoutParams;
        //   494: astore          13
        //   496: aload           14
        //   498: ldc             "view_width_layout_type"
        //   500: aload           13
        //   502: getfield        android/view/ViewGroup$LayoutParams.width:I
        //   505: invokestatic    com/google/android/gms/ads/internal/util/zzbz.zzl:(I)I
        //   508: iconst_1       
        //   509: isub           
        //   510: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //   513: pop            
        //   514: aload           14
        //   516: ldc             "view_height_layout_type"
        //   518: aload           13
        //   520: getfield        android/view/ViewGroup$LayoutParams.height:I
        //   523: invokestatic    com/google/android/gms/ads/internal/util/zzbz.zzl:(I)I
        //   526: iconst_1       
        //   527: isub           
        //   528: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //   531: pop            
        //   532: getstatic       com/google/android/gms/internal/ads/zzbdc.zzhO:Lcom/google/android/gms/internal/ads/zzbcu;
        //   535: astore          13
        //   537: invokestatic    com/google/android/gms/ads/internal/client/zzba.zzc:()Lcom/google/android/gms/internal/ads/zzbda;
        //   540: aload           13
        //   542: invokevirtual   com/google/android/gms/internal/ads/zzbda.zza:(Lcom/google/android/gms/internal/ads/zzbcu;)Ljava/lang/Object;
        //   545: checkcast       Ljava/lang/Boolean;
        //   548: invokevirtual   java/lang/Boolean.booleanValue:()Z
        //   551: ifeq            639
        //   554: new             Ljava/util/ArrayList;
        //   557: astore          18
        //   559: aload           18
        //   561: invokespecial   java/util/ArrayList.<init>:()V
        //   564: aload           18
        //   566: aload           17
        //   568: invokevirtual   android/view/View.getId:()I
        //   571: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //   574: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //   577: pop            
        //   578: aload           17
        //   580: invokevirtual   android/view/View.getParent:()Landroid/view/ViewParent;
        //   583: astore          13
        //   585: aload           13
        //   587: instanceof      Landroid/view/View;
        //   590: ifeq            622
        //   593: aload           18
        //   595: aload           13
        //   597: checkcast       Landroid/view/View;
        //   600: invokevirtual   android/view/View.getId:()I
        //   603: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //   606: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //   609: pop            
        //   610: aload           13
        //   612: invokeinterface android/view/ViewParent.getParent:()Landroid/view/ViewParent;
        //   617: astore          13
        //   619: goto            585
        //   622: aload           14
        //   624: ldc_w           "view_path"
        //   627: ldc_w           "/"
        //   630: aload           18
        //   632: invokestatic    android/text/TextUtils.join:(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;
        //   635: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //   638: pop            
        //   639: getstatic       com/google/android/gms/internal/ads/zzbdc.zzhP:Lcom/google/android/gms/internal/ads/zzbcu;
        //   642: astore          13
        //   644: invokestatic    com/google/android/gms/ads/internal/client/zzba.zzc:()Lcom/google/android/gms/internal/ads/zzbda;
        //   647: aload           13
        //   649: invokevirtual   com/google/android/gms/internal/ads/zzbda.zza:(Lcom/google/android/gms/internal/ads/zzbcu;)Ljava/lang/Object;
        //   652: checkcast       Ljava/lang/Boolean;
        //   655: invokevirtual   java/lang/Boolean.booleanValue:()Z
        //   658: ifeq            680
        //   661: aload           4
        //   663: ifnull          680
        //   666: aload           14
        //   668: ldc_w           "mediaview_scale_type"
        //   671: aload           4
        //   673: invokevirtual   java/lang/Enum.ordinal:()I
        //   676: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //   679: pop            
        //   680: aload           17
        //   682: instanceof      Landroid/widget/TextView;
        //   685: ifeq            749
        //   688: aload           17
        //   690: checkcast       Landroid/widget/TextView;
        //   693: astore          13
        //   695: aload           14
        //   697: ldc_w           "text_color"
        //   700: aload           13
        //   702: invokevirtual   android/widget/TextView.getCurrentTextColor:()I
        //   705: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //   708: pop            
        //   709: aload           13
        //   711: invokevirtual   android/widget/TextView.getTextSize:()F
        //   714: fstore          7
        //   716: fload           7
        //   718: f2d            
        //   719: dstore          5
        //   721: aload           14
        //   723: ldc_w           "font_size"
        //   726: dload           5
        //   728: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;D)Lorg/json/JSONObject;
        //   731: pop            
        //   732: aload           14
        //   734: ldc_w           "text"
        //   737: aload           13
        //   739: invokevirtual   android/widget/TextView.getText:()Ljava/lang/CharSequence;
        //   742: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //   745: pop            
        //   746: goto            749
        //   749: aload_2        
        //   750: ifnull          783
        //   753: aload_2        
        //   754: aload           15
        //   756: invokeinterface java/util/Map$Entry.getKey:()Ljava/lang/Object;
        //   761: invokeinterface java/util/Map.containsKey:(Ljava/lang/Object;)Z
        //   766: ifeq            783
        //   769: aload           17
        //   771: invokevirtual   android/view/View.isClickable:()Z
        //   774: ifeq            783
        //   777: iconst_1       
        //   778: istore          10
        //   780: goto            786
        //   783: iconst_0       
        //   784: istore          10
        //   786: aload           14
        //   788: ldc_w           "is_clickable"
        //   791: iload           10
        //   793: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Z)Lorg/json/JSONObject;
        //   796: pop            
        //   797: aload           15
        //   799: invokeinterface java/util/Map$Entry.getKey:()Ljava/lang/Object;
        //   804: checkcast       Ljava/lang/String;
        //   807: astore          13
        //   809: aload_1        
        //   810: aload           13
        //   812: aload           14
        //   814: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //   817: pop            
        //   818: goto            834
        //   821: astore          13
        //   823: goto            828
        //   826: astore          13
        //   828: ldc_w           "Unable to get asset views information"
        //   831: invokestatic    com/google/android/gms/internal/ads/zzcbn.zzj:(Ljava/lang/String;)V
        //   834: goto            65
        //   837: aload           13
        //   839: areturn        
        //   840: astore          13
        //   842: goto            823
        //   845: astore          13
        //   847: goto            828
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                    
        //  -----  -----  -----  -----  ------------------------
        //  138    145    826    828    Lorg/json/JSONException;
        //  145    186    821    823    Lorg/json/JSONException;
        //  198    218    821    823    Lorg/json/JSONException;
        //  230    297    821    823    Lorg/json/JSONException;
        //  300    328    821    823    Lorg/json/JSONException;
        //  340    360    821    823    Lorg/json/JSONException;
        //  372    401    821    823    Lorg/json/JSONException;
        //  401    467    821    823    Lorg/json/JSONException;
        //  467    532    821    823    Lorg/json/JSONException;
        //  532    585    821    823    Lorg/json/JSONException;
        //  585    619    821    823    Lorg/json/JSONException;
        //  622    639    821    823    Lorg/json/JSONException;
        //  639    661    821    823    Lorg/json/JSONException;
        //  666    680    821    823    Lorg/json/JSONException;
        //  680    716    821    823    Lorg/json/JSONException;
        //  721    746    840    845    Lorg/json/JSONException;
        //  753    777    840    845    Lorg/json/JSONException;
        //  786    809    840    845    Lorg/json/JSONException;
        //  809    818    845    850    Lorg/json/JSONException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 379 out of bounds for length 379
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:248)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:372)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:459)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static JSONObject zze(final Context context, final View view) {
        final JSONObject jsonObject = new JSONObject();
        if (view != null) {
            try {
                zzt.zzp();
                jsonObject.put("can_show_on_lock_screen", com.google.android.gms.ads.internal.util.zzt.zzn(view));
                zzt.zzp();
                jsonObject.put("is_keyguard_locked", com.google.android.gms.ads.internal.util.zzt.zzC(context));
            }
            catch (final JSONException ex) {
                zzcbn.zzj("Unable to get lock screen information");
            }
        }
        return jsonObject;
    }
    
    public static JSONObject zzf(final View view) {
        final JSONObject jsonObject = new JSONObject();
        if (view == null) {
            return jsonObject;
        }
        try {
            final boolean booleanValue = (boolean)zzba.zzc().zza(zzbdc.zzhL);
            boolean b = false;
            Label_0081: {
                if (booleanValue) {
                    zzt.zzp();
                    ViewParent viewParent;
                    for (viewParent = view.getParent(); viewParent != null && !(viewParent instanceof ScrollView); viewParent = viewParent.getParent()) {}
                    if (viewParent == null) {
                        break Label_0081;
                    }
                }
                else {
                    zzt.zzp();
                    ViewParent viewParent2;
                    for (viewParent2 = view.getParent(); viewParent2 != null && !(viewParent2 instanceof AdapterView); viewParent2 = viewParent2.getParent()) {}
                    int positionForView;
                    if (viewParent2 == null) {
                        positionForView = -1;
                    }
                    else {
                        positionForView = ((AdapterView)viewParent2).getPositionForView(view);
                    }
                    if (positionForView == -1) {
                        break Label_0081;
                    }
                }
                b = true;
            }
            jsonObject.put("contained_in_scroll_view", b);
            return jsonObject;
        }
        catch (final Exception ex) {
            return jsonObject;
        }
    }
    
    public static JSONObject zzg(final Context p0, final View p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: invokespecial   org/json/JSONObject.<init>:()V
        //     7: astore          5
        //     9: aload_1        
        //    10: ifnonnull       16
        //    13: goto            647
        //    16: aload_1        
        //    17: invokestatic    com/google/android/gms/ads/internal/util/zzbz.zzj:(Landroid/view/View;)[I
        //    20: astore          6
        //    22: iconst_2       
        //    23: newarray        I
        //    25: astore          7
        //    27: aload           7
        //    29: iconst_0       
        //    30: aload_1        
        //    31: invokevirtual   android/view/View.getMeasuredWidth:()I
        //    34: iastore        
        //    35: aload           7
        //    37: iconst_1       
        //    38: aload_1        
        //    39: invokevirtual   android/view/View.getMeasuredHeight:()I
        //    42: iastore        
        //    43: aload_1        
        //    44: invokevirtual   android/view/View.getParent:()Landroid/view/ViewParent;
        //    47: astore          4
        //    49: aload           4
        //    51: instanceof      Landroid/view/ViewGroup;
        //    54: ifeq            108
        //    57: aload           4
        //    59: checkcast       Landroid/view/ViewGroup;
        //    62: astore          8
        //    64: aload           7
        //    66: iconst_0       
        //    67: aload           8
        //    69: invokevirtual   android/view/View.getMeasuredWidth:()I
        //    72: aload           7
        //    74: iconst_0       
        //    75: iaload         
        //    76: invokestatic    java/lang/Math.min:(II)I
        //    79: iastore        
        //    80: aload           7
        //    82: iconst_1       
        //    83: aload           8
        //    85: invokevirtual   android/view/View.getMeasuredHeight:()I
        //    88: aload           7
        //    90: iconst_1       
        //    91: iaload         
        //    92: invokestatic    java/lang/Math.min:(II)I
        //    95: iastore        
        //    96: aload           4
        //    98: invokeinterface android/view/ViewParent.getParent:()Landroid/view/ViewParent;
        //   103: astore          4
        //   105: goto            49
        //   108: new             Lorg/json/JSONObject;
        //   111: astore          4
        //   113: aload           4
        //   115: invokespecial   org/json/JSONObject.<init>:()V
        //   118: aload_1        
        //   119: invokevirtual   android/view/View.getMeasuredWidth:()I
        //   122: istore_2       
        //   123: aload           4
        //   125: ldc             "width"
        //   127: invokestatic    com/google/android/gms/ads/internal/client/zzay.zzb:()Lcom/google/android/gms/internal/ads/zzcbg;
        //   130: aload_0        
        //   131: iload_2        
        //   132: invokevirtual   com/google/android/gms/internal/ads/zzcbg.zzb:(Landroid/content/Context;I)I
        //   135: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //   138: pop            
        //   139: aload_1        
        //   140: invokevirtual   android/view/View.getMeasuredHeight:()I
        //   143: istore_2       
        //   144: aload           4
        //   146: ldc             "height"
        //   148: invokestatic    com/google/android/gms/ads/internal/client/zzay.zzb:()Lcom/google/android/gms/internal/ads/zzcbg;
        //   151: aload_0        
        //   152: iload_2        
        //   153: invokevirtual   com/google/android/gms/internal/ads/zzcbg.zzb:(Landroid/content/Context;I)I
        //   156: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //   159: pop            
        //   160: aload           6
        //   162: iconst_0       
        //   163: iaload         
        //   164: istore_2       
        //   165: aload           4
        //   167: ldc             "x"
        //   169: invokestatic    com/google/android/gms/ads/internal/client/zzay.zzb:()Lcom/google/android/gms/internal/ads/zzcbg;
        //   172: aload_0        
        //   173: iload_2        
        //   174: invokevirtual   com/google/android/gms/internal/ads/zzcbg.zzb:(Landroid/content/Context;I)I
        //   177: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //   180: pop            
        //   181: aload           6
        //   183: iconst_1       
        //   184: iaload         
        //   185: istore_2       
        //   186: aload           4
        //   188: ldc             "y"
        //   190: invokestatic    com/google/android/gms/ads/internal/client/zzay.zzb:()Lcom/google/android/gms/internal/ads/zzcbg;
        //   193: aload_0        
        //   194: iload_2        
        //   195: invokevirtual   com/google/android/gms/internal/ads/zzcbg.zzb:(Landroid/content/Context;I)I
        //   198: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //   201: pop            
        //   202: aload           7
        //   204: iconst_0       
        //   205: iaload         
        //   206: istore_2       
        //   207: aload           4
        //   209: ldc_w           "maximum_visible_width"
        //   212: invokestatic    com/google/android/gms/ads/internal/client/zzay.zzb:()Lcom/google/android/gms/internal/ads/zzcbg;
        //   215: aload_0        
        //   216: iload_2        
        //   217: invokevirtual   com/google/android/gms/internal/ads/zzcbg.zzb:(Landroid/content/Context;I)I
        //   220: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //   223: pop            
        //   224: aload           7
        //   226: iconst_1       
        //   227: iaload         
        //   228: istore_2       
        //   229: aload           4
        //   231: ldc_w           "maximum_visible_height"
        //   234: invokestatic    com/google/android/gms/ads/internal/client/zzay.zzb:()Lcom/google/android/gms/internal/ads/zzcbg;
        //   237: aload_0        
        //   238: iload_2        
        //   239: invokevirtual   com/google/android/gms/internal/ads/zzcbg.zzb:(Landroid/content/Context;I)I
        //   242: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //   245: pop            
        //   246: aload           4
        //   248: ldc             "relative_to"
        //   250: ldc_w           "window"
        //   253: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //   256: pop            
        //   257: aload           5
        //   259: ldc             "frame"
        //   261: aload           4
        //   263: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //   266: pop            
        //   267: new             Landroid/graphics/Rect;
        //   270: astore          4
        //   272: aload           4
        //   274: invokespecial   android/graphics/Rect.<init>:()V
        //   277: aload_1        
        //   278: aload           4
        //   280: invokevirtual   android/view/View.getGlobalVisibleRect:(Landroid/graphics/Rect;)Z
        //   283: ifeq            296
        //   286: aload_0        
        //   287: aload           4
        //   289: invokestatic    com/google/android/gms/ads/internal/util/zzbz.zzk:(Landroid/content/Context;Landroid/graphics/Rect;)Lorg/json/JSONObject;
        //   292: astore_0       
        //   293: goto            380
        //   296: new             Lorg/json/JSONObject;
        //   299: astore          4
        //   301: aload           4
        //   303: invokespecial   org/json/JSONObject.<init>:()V
        //   306: aload           4
        //   308: ldc             "width"
        //   310: iconst_0       
        //   311: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //   314: pop            
        //   315: aload           4
        //   317: ldc             "height"
        //   319: iconst_0       
        //   320: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //   323: pop            
        //   324: aload           6
        //   326: iconst_0       
        //   327: iaload         
        //   328: istore_2       
        //   329: aload           4
        //   331: ldc             "x"
        //   333: invokestatic    com/google/android/gms/ads/internal/client/zzay.zzb:()Lcom/google/android/gms/internal/ads/zzcbg;
        //   336: aload_0        
        //   337: iload_2        
        //   338: invokevirtual   com/google/android/gms/internal/ads/zzcbg.zzb:(Landroid/content/Context;I)I
        //   341: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //   344: pop            
        //   345: aload           6
        //   347: iconst_1       
        //   348: iaload         
        //   349: istore_2       
        //   350: aload           4
        //   352: ldc             "y"
        //   354: invokestatic    com/google/android/gms/ads/internal/client/zzay.zzb:()Lcom/google/android/gms/internal/ads/zzcbg;
        //   357: aload_0        
        //   358: iload_2        
        //   359: invokevirtual   com/google/android/gms/internal/ads/zzcbg.zzb:(Landroid/content/Context;I)I
        //   362: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //   365: pop            
        //   366: aload           4
        //   368: ldc             "relative_to"
        //   370: ldc_w           "window"
        //   373: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //   376: pop            
        //   377: aload           4
        //   379: astore_0       
        //   380: aload           5
        //   382: ldc             "visible_bounds"
        //   384: aload_0        
        //   385: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //   388: pop            
        //   389: goto            399
        //   392: astore_0       
        //   393: ldc_w           "Unable to get native ad view bounding box"
        //   396: invokestatic    com/google/android/gms/internal/ads/zzcbn.zzj:(Ljava/lang/String;)V
        //   399: getstatic       com/google/android/gms/internal/ads/zzbdc.zzgk:Lcom/google/android/gms/internal/ads/zzbcu;
        //   402: astore_0       
        //   403: invokestatic    com/google/android/gms/ads/internal/client/zzba.zzc:()Lcom/google/android/gms/internal/ads/zzbda;
        //   406: aload_0        
        //   407: invokevirtual   com/google/android/gms/internal/ads/zzbda.zza:(Lcom/google/android/gms/internal/ads/zzbcu;)Ljava/lang/Object;
        //   410: checkcast       Ljava/lang/Boolean;
        //   413: invokevirtual   java/lang/Boolean.booleanValue:()Z
        //   416: ifeq            578
        //   419: aload_1        
        //   420: invokevirtual   android/view/View.getParent:()Landroid/view/ViewParent;
        //   423: astore_0       
        //   424: aload_0        
        //   425: ifnull          473
        //   428: aload_0        
        //   429: invokevirtual   java/lang/Object.getClass:()Ljava/lang/Class;
        //   432: ldc_w           "getTemplateTypeName"
        //   435: iconst_0       
        //   436: anewarray       Ljava/lang/Class;
        //   439: invokevirtual   java/lang/Class.getMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //   442: aload_0        
        //   443: iconst_0       
        //   444: anewarray       Ljava/lang/Object;
        //   447: invokevirtual   java/lang/reflect/Method.invoke:(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
        //   450: checkcast       Ljava/lang/String;
        //   453: astore_0       
        //   454: goto            477
        //   457: astore_0       
        //   458: goto            466
        //   461: astore_0       
        //   462: goto            466
        //   465: astore_0       
        //   466: ldc_w           "Cannot access method getTemplateTypeName: "
        //   469: aload_0        
        //   470: invokestatic    com/google/android/gms/internal/ads/zzcbn.zzh:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   473: ldc_w           ""
        //   476: astore_0       
        //   477: aload_0        
        //   478: invokevirtual   java/lang/String.hashCode:()I
        //   481: istore_2       
        //   482: iload_2        
        //   483: ldc_w           -2066603854
        //   486: if_icmpeq       514
        //   489: iload_2        
        //   490: ldc_w           2019754500
        //   493: if_icmpeq       499
        //   496: goto            529
        //   499: aload_0        
        //   500: ldc_w           "medium_template"
        //   503: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   506: ifeq            529
        //   509: iconst_1       
        //   510: istore_2       
        //   511: goto            531
        //   514: aload_0        
        //   515: ldc_w           "small_template"
        //   518: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   521: ifeq            529
        //   524: iconst_0       
        //   525: istore_2       
        //   526: goto            531
        //   529: iconst_m1      
        //   530: istore_2       
        //   531: iconst_1       
        //   532: istore_3       
        //   533: iload_2        
        //   534: ifeq            557
        //   537: iload_2        
        //   538: iconst_1       
        //   539: if_icmpeq       555
        //   542: aload           5
        //   544: ldc_w           "native_template_type"
        //   547: iconst_0       
        //   548: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //   551: pop            
        //   552: goto            578
        //   555: iconst_2       
        //   556: istore_3       
        //   557: aload           5
        //   559: ldc_w           "native_template_type"
        //   562: iload_3        
        //   563: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //   566: pop            
        //   567: goto            578
        //   570: astore_0       
        //   571: ldc_w           "Could not log native template signal to JSON"
        //   574: aload_0        
        //   575: invokestatic    com/google/android/gms/internal/ads/zzcbn.zzh:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   578: getstatic       com/google/android/gms/internal/ads/zzbdc.zzhN:Lcom/google/android/gms/internal/ads/zzbcu;
        //   581: astore_0       
        //   582: invokestatic    com/google/android/gms/ads/internal/client/zzba.zzc:()Lcom/google/android/gms/internal/ads/zzbda;
        //   585: aload_0        
        //   586: invokevirtual   com/google/android/gms/internal/ads/zzbda.zza:(Lcom/google/android/gms/internal/ads/zzbcu;)Ljava/lang/Object;
        //   589: checkcast       Ljava/lang/Boolean;
        //   592: invokevirtual   java/lang/Boolean.booleanValue:()Z
        //   595: ifeq            647
        //   598: aload_1        
        //   599: invokevirtual   android/view/View.getLayoutParams:()Landroid/view/ViewGroup$LayoutParams;
        //   602: astore_0       
        //   603: aload           5
        //   605: ldc             "view_width_layout_type"
        //   607: aload_0        
        //   608: getfield        android/view/ViewGroup$LayoutParams.width:I
        //   611: invokestatic    com/google/android/gms/ads/internal/util/zzbz.zzl:(I)I
        //   614: iconst_1       
        //   615: isub           
        //   616: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //   619: pop            
        //   620: aload           5
        //   622: ldc             "view_height_layout_type"
        //   624: aload_0        
        //   625: getfield        android/view/ViewGroup$LayoutParams.height:I
        //   628: invokestatic    com/google/android/gms/ads/internal/util/zzbz.zzl:(I)I
        //   631: iconst_1       
        //   632: isub           
        //   633: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //   636: pop            
        //   637: goto            647
        //   640: astore_0       
        //   641: ldc_w           "Unable to get native ad view layout types"
        //   644: invokestatic    com/google/android/gms/ads/internal/util/zze.zza:(Ljava/lang/String;)V
        //   647: aload           5
        //   649: areturn        
        //   650: astore_0       
        //   651: goto            473
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                         
        //  -----  -----  -----  -----  ---------------------------------------------
        //  16     49     392    399    Ljava/lang/Exception;
        //  49     105    392    399    Ljava/lang/Exception;
        //  108    160    392    399    Ljava/lang/Exception;
        //  165    181    392    399    Ljava/lang/Exception;
        //  186    202    392    399    Ljava/lang/Exception;
        //  207    224    392    399    Ljava/lang/Exception;
        //  229    293    392    399    Ljava/lang/Exception;
        //  296    324    392    399    Ljava/lang/Exception;
        //  329    345    392    399    Ljava/lang/Exception;
        //  350    377    392    399    Ljava/lang/Exception;
        //  380    389    392    399    Ljava/lang/Exception;
        //  428    454    650    654    Ljava/lang/NoSuchMethodException;
        //  428    454    465    466    Ljava/lang/SecurityException;
        //  428    454    461    465    Ljava/lang/IllegalAccessException;
        //  428    454    457    461    Ljava/lang/reflect/InvocationTargetException;
        //  477    482    570    578    Lorg/json/JSONException;
        //  542    552    570    578    Lorg/json/JSONException;
        //  557    567    570    578    Lorg/json/JSONException;
        //  598    637    640    647    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 329 out of bounds for length 329
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:248)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:372)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:459)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static boolean zzh(final Context context, final zzfdu zzfdu) {
        if (!zzfdu.zzO) {
            return false;
        }
        if (zzba.zzc().zza(zzbdc.zzhQ)) {
            return (boolean)zzba.zzc().zza(zzbdc.zzhT);
        }
        final String s = (String)zzba.zzc().zza(zzbdc.zzhR);
        if (!s.isEmpty()) {
            if (context != null) {
                final String packageName = context.getPackageName();
                final Iterator iterator = zzful.zzc(zzftk.zzc(';')).zzd((CharSequence)s).iterator();
                while (iterator.hasNext()) {
                    if (((String)iterator.next()).equals(packageName)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    public static boolean zzi(final int n) {
        return !(boolean)zzba.zzc().zza(zzbdc.zzdl) || (boolean)zzba.zzc().zza(zzbdc.zzdm) || n <= 15299999;
    }
    
    public static int[] zzj(final View view) {
        final int[] array = new int[2];
        if (view != null) {
            view.getLocationOnScreen(array);
        }
        return array;
    }
    
    private static JSONObject zzk(final Context context, final Rect rect) {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("width", zzay.zzb().zzb(context, rect.right - rect.left));
        jsonObject.put("height", zzay.zzb().zzb(context, rect.bottom - rect.top));
        jsonObject.put("x", zzay.zzb().zzb(context, rect.left));
        jsonObject.put("y", zzay.zzb().zzb(context, rect.top));
        jsonObject.put("relative_to", (Object)"self");
        return jsonObject;
    }
    
    private static int zzl(final int n) {
        if (n == -2) {
            return 4;
        }
        if (n != -1) {
            return 2;
        }
        return 3;
    }
}

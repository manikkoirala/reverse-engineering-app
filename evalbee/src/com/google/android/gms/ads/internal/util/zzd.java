// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import com.google.android.gms.internal.ads.zzccd;
import com.google.android.gms.internal.ads.zzcbn;
import android.provider.Settings$Global;
import com.google.android.gms.internal.ads.zzber;
import com.google.android.gms.internal.ads.zzcbm;
import android.content.Context;

public final class zzd
{
    public static void zza(final Context context) {
        final int zza = zzcbm.zza;
        if (!(boolean)zzber.zza.zze()) {
            return;
        }
        try {
            if (Settings$Global.getInt(context.getContentResolver(), "development_settings_enabled", 0) != 0 && !zzcbm.zzl()) {
                final ik0 zzb = new zzc(context).zzb();
                zzcbn.zzi("Updating ad debug logging enablement.");
                zzccd.zza(zzb, "AdDebugLogUpdater.updateEnablement");
            }
        }
        catch (final Exception ex) {
            zzcbn.zzk("Fail to determine debug setting.", (Throwable)ex);
        }
    }
}

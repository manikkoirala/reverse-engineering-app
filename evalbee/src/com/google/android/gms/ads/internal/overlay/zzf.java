// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.overlay;

final class zzf extends Exception
{
    public zzf(final String message) {
        super(message);
    }
    
    public zzf(final String message, final Throwable cause) {
        super(message, cause);
    }
}

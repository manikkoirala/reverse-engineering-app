// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.ads.zzbmh;
import com.google.android.gms.internal.ads.zzbpr;
import com.google.android.gms.dynamic.IObjectWrapper;
import java.util.List;
import android.os.IInterface;

public interface zzco extends IInterface
{
    float zze();
    
    String zzf();
    
    List zzg();
    
    void zzh(final String p0);
    
    void zzi();
    
    void zzj(final boolean p0);
    
    void zzk();
    
    void zzl(final String p0, final IObjectWrapper p1);
    
    void zzm(final zzda p0);
    
    void zzn(final IObjectWrapper p0, final String p1);
    
    void zzo(final zzbpr p0);
    
    void zzp(final boolean p0);
    
    void zzq(final float p0);
    
    void zzr(final String p0);
    
    void zzs(final zzbmh p0);
    
    void zzt(final String p0);
    
    void zzu(final zzff p0);
    
    boolean zzv();
}

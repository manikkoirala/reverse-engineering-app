// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.ads.zzcae;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.RemoteException;
import com.google.android.gms.internal.ads.zzcbq;
import com.google.android.gms.internal.ads.zzcbp;
import com.google.android.gms.internal.ads.zzcbr;
import com.google.android.gms.internal.ads.zzcah;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.internal.ads.zzbpr;
import android.content.Context;

final class zzae extends zzax
{
    final Context zza;
    final zzbpr zzb;
    
    public zzae(final zzaw zzaw, final Context zza, final zzbpr zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
}

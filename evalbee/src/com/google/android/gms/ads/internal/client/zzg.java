// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.AdListener;

public final class zzg extends zzbg
{
    private final AdListener zza;
    
    public zzg(final AdListener zza) {
        this.zza = zza;
    }
    
    public final AdListener zzb() {
        return this.zza;
    }
    
    public final void zzc() {
        final AdListener zza = this.zza;
        if (zza != null) {
            zza.onAdClicked();
        }
    }
    
    public final void zzd() {
        final AdListener zza = this.zza;
        if (zza != null) {
            zza.onAdClosed();
        }
    }
    
    public final void zze(final int n) {
    }
    
    public final void zzf(final zze zze) {
        final AdListener zza = this.zza;
        if (zza != null) {
            zza.onAdFailedToLoad(zze.zzb());
        }
    }
    
    public final void zzg() {
        final AdListener zza = this.zza;
        if (zza != null) {
            zza.onAdImpression();
        }
    }
    
    public final void zzh() {
    }
    
    public final void zzi() {
        final AdListener zza = this.zza;
        if (zza != null) {
            zza.onAdLoaded();
        }
    }
    
    public final void zzj() {
        final AdListener zza = this.zza;
        if (zza != null) {
            zza.onAdOpened();
        }
    }
    
    public final void zzk() {
        final AdListener zza = this.zza;
        if (zza != null) {
            zza.onAdSwipeGestureClicked();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.IInterface;
import com.google.android.gms.internal.ads.zzavi;
import com.google.android.gms.internal.ads.zzbpr;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IBinder;
import com.google.android.gms.internal.ads.zzavg;

public final class zzbv extends zzavg
{
    public zzbv(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IAdManagerCreator");
    }
    
    public final IBinder zze(final IObjectWrapper objectWrapper, final zzq zzq, final String s, final zzbpr zzbpr, final int n, final int n2) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)objectWrapper);
        zzavi.zzd(zza, (Parcelable)zzq);
        zza.writeString(s);
        zzavi.zzf(zza, (IInterface)zzbpr);
        zza.writeInt(234310000);
        zza.writeInt(n2);
        final Parcel zzbh = this.zzbh(2, zza);
        final IBinder strongBinder = zzbh.readStrongBinder();
        zzbh.recycle();
        return strongBinder;
    }
}

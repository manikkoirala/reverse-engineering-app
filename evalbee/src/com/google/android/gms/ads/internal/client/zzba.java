// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.ads.zzbda;
import com.google.android.gms.internal.ads.zzbcw;
import com.google.android.gms.internal.ads.zzbcv;

public final class zzba
{
    private static final zzba zza;
    private final zzbcv zzb;
    private final zzbcw zzc;
    private final zzbda zzd;
    
    static {
        zza = new zzba();
    }
    
    public zzba() {
        final zzbcv zzb = new zzbcv();
        final zzbcw zzc = new zzbcw();
        final zzbda zzd = new zzbda();
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
    }
    
    public static zzbcv zza() {
        return zzba.zza.zzb;
    }
    
    public static zzbcw zzb() {
        return zzba.zza.zzc;
    }
    
    public static zzbda zzc() {
        return zzba.zza.zzd;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.overlay;

import android.widget.ImageView;
import android.view.ViewGroup;
import android.graphics.drawable.Drawable;
import android.content.res.Resources;
import android.widget.ImageView$ScaleType;
import android.content.res.Resources$NotFoundException;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.ads.impl.R;
import com.google.android.gms.ads.internal.zzt;
import android.text.TextUtils;
import com.google.android.gms.common.util.PlatformVersion;
import android.animation.Animator$AnimatorListener;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.ads.internal.client.zzba;
import android.view.ViewGroup$LayoutParams;
import android.view.View;
import android.widget.FrameLayout$LayoutParams;
import com.google.android.gms.internal.ads.zzcbg;
import com.google.android.gms.ads.internal.client.zzay;
import android.content.Context;
import android.widget.ImageButton;
import android.view.View$OnClickListener;
import android.widget.FrameLayout;

public final class zzr extends FrameLayout implements View$OnClickListener
{
    private final ImageButton zza;
    private final zzad zzb;
    
    public zzr(final Context context, final zzq zzq, final zzad zzb) {
        super(context);
        this.zzb = zzb;
        ((View)this).setOnClickListener((View$OnClickListener)this);
        final ImageButton zza = new ImageButton(context);
        this.zza = zza;
        this.zzc();
        ((View)zza).setBackgroundColor(0);
        ((View)zza).setOnClickListener((View$OnClickListener)this);
        zzay.zzb();
        final int zzx = zzcbg.zzx(context, zzq.zza);
        zzay.zzb();
        final int zzx2 = zzcbg.zzx(context, 0);
        zzay.zzb();
        final int zzx3 = zzcbg.zzx(context, zzq.zzb);
        zzay.zzb();
        ((View)zza).setPadding(zzx, zzx2, zzx3, zzcbg.zzx(context, zzq.zzc));
        ((View)zza).setContentDescription((CharSequence)"Interstitial close button");
        zzay.zzb();
        final int zzx4 = zzcbg.zzx(context, zzq.zzd + zzq.zza + zzq.zzb);
        zzay.zzb();
        ((ViewGroup)this).addView((View)zza, (ViewGroup$LayoutParams)new FrameLayout$LayoutParams(zzx4, zzcbg.zzx(context, zzq.zzd + zzq.zzc), 17));
        final long longValue = (long)zzba.zzc().zza(zzbdc.zzbb);
        if (longValue <= 0L) {
            return;
        }
        Object listener;
        if (zzba.zzc().zza(zzbdc.zzbc)) {
            listener = new zzp(this);
        }
        else {
            listener = null;
        }
        ((View)zza).setAlpha(0.0f);
        ((View)zza).animate().alpha(1.0f).setDuration(longValue).setListener((Animator$AnimatorListener)listener);
    }
    
    private final void zzc() {
        final String anObject = (String)zzba.zzc().zza(zzbdc.zzba);
        if (!PlatformVersion.isAtLeastLollipop() || TextUtils.isEmpty((CharSequence)anObject) || "default".equals(anObject)) {
            ((ImageView)this.zza).setImageResource(17301527);
            return;
        }
        final Resources zze = zzt.zzo().zze();
        if (zze == null) {
            ((ImageView)this.zza).setImageResource(17301527);
            return;
        }
        final Drawable drawable = null;
        Drawable drawable2 = null;
        Label_0109: {
            try {
                int n;
                if ("white".equals(anObject)) {
                    n = R.drawable.admob_close_button_white_circle_black_cross;
                }
                else {
                    drawable2 = drawable;
                    if (!"black".equals(anObject)) {
                        break Label_0109;
                    }
                    n = R.drawable.admob_close_button_black_circle_white_cross;
                }
                drawable2 = zze.getDrawable(n);
            }
            catch (final Resources$NotFoundException ex) {
                zzcbn.zze("Close button resource not found, falling back to default.");
                drawable2 = drawable;
            }
        }
        final ImageButton zza = this.zza;
        if (drawable2 == null) {
            ((ImageView)zza).setImageResource(17301527);
            return;
        }
        ((ImageView)zza).setImageDrawable(drawable2);
        ((ImageView)this.zza).setScaleType(ImageView$ScaleType.CENTER);
    }
    
    public final void onClick(final View view) {
        final zzad zzb = this.zzb;
        if (zzb != null) {
            zzb.zzj();
        }
    }
    
    public final void zzb(final boolean b) {
        if (b) {
            ((View)this.zza).setVisibility(8);
            if ((long)zzba.zzc().zza(zzbdc.zzbb) > 0L) {
                ((View)this.zza).animate().cancel();
                ((View)this.zza).clearAnimation();
            }
            return;
        }
        ((View)this.zza).setVisibility(0);
    }
}

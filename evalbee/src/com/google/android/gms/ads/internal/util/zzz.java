// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.ads.internal.client.zzba;
import android.content.Context;

public final class zzz extends zzy
{
    @Override
    public final int zzn(final Context context) {
        if (zzba.zzc().zza(zzbdc.zziq)) {
            return 0;
        }
        return super.zzn(context);
    }
}

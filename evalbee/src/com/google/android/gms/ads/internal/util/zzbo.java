// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import com.google.android.gms.internal.ads.zzanj;
import com.google.android.gms.internal.ads.zzccf;
import com.google.android.gms.internal.ads.zzane;

final class zzbo implements zzane
{
    final zzccf zza;
    
    public zzbo(final zzccf zza) {
        this.zza = zza;
    }
    
    public final void zza(final zzanj zzanj) {
        this.zza.zzd((Throwable)zzanj);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.Parcelable$Creator;
import com.google.android.gms.internal.ads.zzavi;
import android.os.Parcel;
import com.google.android.gms.internal.ads.zzavh;

public abstract class zzbm extends zzavh implements zzbn
{
    public zzbm() {
        super("com.google.android.gms.ads.internal.client.IAdLoader");
    }
    
    public final boolean zzbK(int n, final Parcel parcel, final Parcel parcel2, int zza) {
        Label_0137: {
            if (n != 1) {
                String s;
                if (n != 2) {
                    if (n == 3) {
                        n = (this.zzi() ? 1 : 0);
                        parcel2.writeNoException();
                        zza = zzavi.zza;
                        parcel2.writeInt(n);
                        return true;
                    }
                    if (n != 4) {
                        if (n != 5) {
                            return false;
                        }
                        final zzl zzl = (zzl)zzavi.zza(parcel, (Parcelable$Creator)com.google.android.gms.ads.internal.client.zzl.CREATOR);
                        n = parcel.readInt();
                        zzavi.zzc(parcel);
                        this.zzh(zzl, n);
                        break Label_0137;
                    }
                    else {
                        s = this.zzf();
                    }
                }
                else {
                    s = this.zze();
                }
                parcel2.writeNoException();
                parcel2.writeString(s);
                return true;
            }
            final zzl zzl2 = (zzl)zzavi.zza(parcel, (Parcelable$Creator)zzl.CREATOR);
            zzavi.zzc(parcel);
            this.zzg(zzl2);
        }
        parcel2.writeNoException();
        return true;
    }
}

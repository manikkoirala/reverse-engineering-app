// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.ads.search.SearchAdRequest;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "SearchAdRequestParcelCreator")
@Reserved({ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 })
public final class zzfh extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zzfh> CREATOR;
    @Field(id = 15)
    public final String zza;
    
    static {
        CREATOR = (Parcelable$Creator)new zzfi();
    }
    
    public zzfh(final SearchAdRequest searchAdRequest) {
        this.zza = searchAdRequest.getQuery();
    }
    
    @Constructor
    public zzfh(@Param(id = 15) final String zza) {
        this.zza = zza;
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        final String zza = this.zza;
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 15, zza, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

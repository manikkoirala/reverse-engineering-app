// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.ads.zzavi;
import android.os.Parcel;
import com.google.android.gms.internal.ads.zzavh;

public abstract class zzds extends zzavh implements zzdt
{
    public zzds() {
        super("com.google.android.gms.ads.internal.client.IVideoLifecycleCallbacks");
    }
    
    public final boolean zzbK(final int n, final Parcel parcel, final Parcel parcel2, final int n2) {
        if (n != 1) {
            if (n != 2) {
                if (n != 3) {
                    if (n != 4) {
                        if (n != 5) {
                            return false;
                        }
                        final boolean zzg = zzavi.zzg(parcel);
                        zzavi.zzc(parcel);
                        this.zzf(zzg);
                    }
                    else {
                        this.zze();
                    }
                }
                else {
                    this.zzg();
                }
            }
            else {
                this.zzh();
            }
        }
        else {
            this.zzi();
        }
        parcel2.writeNoException();
        return true;
    }
}

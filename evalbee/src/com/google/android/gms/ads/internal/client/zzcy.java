// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ads.zzavi;
import android.os.IBinder;
import com.google.android.gms.internal.ads.zzavg;

public final class zzcy extends zzavg implements zzda
{
    public zzcy(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IOnAdInspectorClosedListener");
    }
    
    public final void zze(final zze zze) {
        final Parcel zza = this.zza();
        zzavi.zzd(zza, (Parcelable)zze);
        this.zzbi(1, zza);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.Parcel;
import android.os.IInterface;
import com.google.android.gms.internal.ads.zzavi;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IBinder;
import com.google.android.gms.internal.ads.zzavg;

public final class zzdh extends zzavg implements zzdj
{
    public zzdh(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IOutOfContextTester");
    }
    
    public final void zze(final String s, final IObjectWrapper objectWrapper, final IObjectWrapper objectWrapper2) {
        final Parcel zza = this.zza();
        zza.writeString(s);
        zzavi.zzf(zza, (IInterface)objectWrapper);
        zzavi.zzf(zza, (IInterface)objectWrapper2);
        this.zzbi(1, zza);
    }
}

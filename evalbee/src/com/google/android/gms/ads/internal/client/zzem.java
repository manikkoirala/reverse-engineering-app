// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import org.json.JSONException;
import org.json.JSONObject;
import android.text.TextUtils;
import android.os.Bundle;

public final class zzem
{
    private final String zza;
    private final Bundle zzb;
    private final String zzc;
    
    public zzem(final String zza, final Bundle zzb, final String zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    public final Bundle zza() {
        return this.zzb;
    }
    
    public final String zzb() {
        return this.zza;
    }
    
    public final String zzc() {
        if (TextUtils.isEmpty((CharSequence)this.zzc)) {
            return "";
        }
        try {
            return new JSONObject(this.zzc).optString("request_id", "");
        }
        catch (final JSONException ex) {
            return "";
        }
    }
}

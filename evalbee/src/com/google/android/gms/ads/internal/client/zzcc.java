// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.ads.zzcad;
import com.google.android.gms.internal.ads.zzcae;
import com.google.android.gms.internal.ads.zzbxi;
import com.google.android.gms.internal.ads.zzbxj;
import com.google.android.gms.internal.ads.zzbwt;
import com.google.android.gms.internal.ads.zzbto;
import com.google.android.gms.internal.ads.zzbtp;
import com.google.android.gms.internal.ads.zzbth;
import com.google.android.gms.internal.ads.zzbti;
import com.google.android.gms.internal.ads.zzbld;
import com.google.android.gms.internal.ads.zzble;
import com.google.android.gms.internal.ads.zzblb;
import com.google.android.gms.internal.ads.zzbgr;
import com.google.android.gms.internal.ads.zzbgs;
import com.google.android.gms.internal.ads.zzbgl;
import com.google.android.gms.internal.ads.zzbgm;
import android.os.Parcelable;
import android.os.Parcel;
import android.os.IInterface;
import com.google.android.gms.internal.ads.zzavi;
import com.google.android.gms.internal.ads.zzbpr;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IBinder;
import com.google.android.gms.internal.ads.zzavg;

public final class zzcc extends zzavg implements zzce
{
    public zzcc(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IClientApi");
    }
    
    public final zzbq zzb(final IObjectWrapper objectWrapper, final String s, final zzbpr zzbpr, final int n) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)objectWrapper);
        zza.writeString(s);
        zzavi.zzf(zza, (IInterface)zzbpr);
        zza.writeInt(234310000);
        final Parcel zzbh = this.zzbh(3, zza);
        final IBinder strongBinder = zzbh.readStrongBinder();
        zzbq zzbq;
        if (strongBinder == null) {
            zzbq = null;
        }
        else {
            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdLoaderBuilder");
            if (queryLocalInterface instanceof zzbq) {
                zzbq = (zzbq)queryLocalInterface;
            }
            else {
                zzbq = new zzbo(strongBinder);
            }
        }
        zzbh.recycle();
        return zzbq;
    }
    
    public final zzbu zzc(final IObjectWrapper objectWrapper, final zzq zzq, final String s, final zzbpr zzbpr, final int n) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)objectWrapper);
        zzavi.zzd(zza, (Parcelable)zzq);
        zza.writeString(s);
        zzavi.zzf(zza, (IInterface)zzbpr);
        zza.writeInt(234310000);
        final Parcel zzbh = this.zzbh(13, zza);
        final IBinder strongBinder = zzbh.readStrongBinder();
        zzbu zzbu;
        if (strongBinder == null) {
            zzbu = null;
        }
        else {
            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdManager");
            if (queryLocalInterface instanceof zzbu) {
                zzbu = (zzbu)queryLocalInterface;
            }
            else {
                zzbu = new zzbs(strongBinder);
            }
        }
        zzbh.recycle();
        return zzbu;
    }
    
    public final zzbu zzd(final IObjectWrapper objectWrapper, final zzq zzq, final String s, final zzbpr zzbpr, final int n) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)objectWrapper);
        zzavi.zzd(zza, (Parcelable)zzq);
        zza.writeString(s);
        zzavi.zzf(zza, (IInterface)zzbpr);
        zza.writeInt(234310000);
        final Parcel zzbh = this.zzbh(1, zza);
        final IBinder strongBinder = zzbh.readStrongBinder();
        zzbu zzbu;
        if (strongBinder == null) {
            zzbu = null;
        }
        else {
            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdManager");
            if (queryLocalInterface instanceof zzbu) {
                zzbu = (zzbu)queryLocalInterface;
            }
            else {
                zzbu = new zzbs(strongBinder);
            }
        }
        zzbh.recycle();
        return zzbu;
    }
    
    public final zzbu zze(final IObjectWrapper objectWrapper, final zzq zzq, final String s, final zzbpr zzbpr, final int n) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)objectWrapper);
        zzavi.zzd(zza, (Parcelable)zzq);
        zza.writeString(s);
        zzavi.zzf(zza, (IInterface)zzbpr);
        zza.writeInt(234310000);
        final Parcel zzbh = this.zzbh(2, zza);
        final IBinder strongBinder = zzbh.readStrongBinder();
        zzbu zzbu;
        if (strongBinder == null) {
            zzbu = null;
        }
        else {
            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdManager");
            if (queryLocalInterface instanceof zzbu) {
                zzbu = (zzbu)queryLocalInterface;
            }
            else {
                zzbu = new zzbs(strongBinder);
            }
        }
        zzbh.recycle();
        return zzbu;
    }
    
    public final zzbu zzf(final IObjectWrapper objectWrapper, final zzq zzq, final String s, final int n) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)objectWrapper);
        zzavi.zzd(zza, (Parcelable)zzq);
        zza.writeString(s);
        zza.writeInt(234310000);
        final Parcel zzbh = this.zzbh(10, zza);
        final IBinder strongBinder = zzbh.readStrongBinder();
        zzbu zzbu;
        if (strongBinder == null) {
            zzbu = null;
        }
        else {
            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdManager");
            if (queryLocalInterface instanceof zzbu) {
                zzbu = (zzbu)queryLocalInterface;
            }
            else {
                zzbu = new zzbs(strongBinder);
            }
        }
        zzbh.recycle();
        return zzbu;
    }
    
    public final zzco zzg(final IObjectWrapper objectWrapper, final int n) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)objectWrapper);
        zza.writeInt(234310000);
        final Parcel zzbh = this.zzbh(9, zza);
        final IBinder strongBinder = zzbh.readStrongBinder();
        zzco zzco;
        if (strongBinder == null) {
            zzco = null;
        }
        else {
            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IMobileAdsSettingManager");
            if (queryLocalInterface instanceof zzco) {
                zzco = (zzco)queryLocalInterface;
            }
            else {
                zzco = new zzcm(strongBinder);
            }
        }
        zzbh.recycle();
        return zzco;
    }
    
    public final zzdj zzh(final IObjectWrapper objectWrapper, final zzbpr zzbpr, final int n) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)objectWrapper);
        zzavi.zzf(zza, (IInterface)zzbpr);
        zza.writeInt(234310000);
        final Parcel zzbh = this.zzbh(17, zza);
        final IBinder strongBinder = zzbh.readStrongBinder();
        zzdj zzdj;
        if (strongBinder == null) {
            zzdj = null;
        }
        else {
            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IOutOfContextTester");
            if (queryLocalInterface instanceof zzdj) {
                zzdj = (zzdj)queryLocalInterface;
            }
            else {
                zzdj = new zzdh(strongBinder);
            }
        }
        zzbh.recycle();
        return zzdj;
    }
    
    public final zzbgm zzi(final IObjectWrapper objectWrapper, final IObjectWrapper objectWrapper2) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)objectWrapper);
        zzavi.zzf(zza, (IInterface)objectWrapper2);
        final Parcel zzbh = this.zzbh(5, zza);
        final zzbgm zzbF = zzbgl.zzbF(zzbh.readStrongBinder());
        zzbh.recycle();
        return zzbF;
    }
    
    public final zzbgs zzj(final IObjectWrapper objectWrapper, final IObjectWrapper objectWrapper2, final IObjectWrapper objectWrapper3) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)objectWrapper);
        zzavi.zzf(zza, (IInterface)objectWrapper2);
        zzavi.zzf(zza, (IInterface)objectWrapper3);
        final Parcel zzbh = this.zzbh(11, zza);
        final zzbgs zze = zzbgr.zze(zzbh.readStrongBinder());
        zzbh.recycle();
        return zze;
    }
    
    public final zzble zzk(final IObjectWrapper objectWrapper, final zzbpr zzbpr, final int n, final zzblb zzblb) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)objectWrapper);
        zzavi.zzf(zza, (IInterface)zzbpr);
        zza.writeInt(234310000);
        zzavi.zzf(zza, (IInterface)zzblb);
        final Parcel zzbh = this.zzbh(16, zza);
        final zzble zzb = zzbld.zzb(zzbh.readStrongBinder());
        zzbh.recycle();
        return zzb;
    }
    
    public final zzbti zzl(final IObjectWrapper objectWrapper, final zzbpr zzbpr, final int n) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)objectWrapper);
        zzavi.zzf(zza, (IInterface)zzbpr);
        zza.writeInt(234310000);
        final Parcel zzbh = this.zzbh(15, zza);
        final zzbti zzb = zzbth.zzb(zzbh.readStrongBinder());
        zzbh.recycle();
        return zzb;
    }
    
    public final zzbtp zzm(final IObjectWrapper objectWrapper) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)objectWrapper);
        final Parcel zzbh = this.zzbh(8, zza);
        final zzbtp zzI = zzbto.zzI(zzbh.readStrongBinder());
        zzbh.recycle();
        return zzI;
    }
    
    public final zzbwt zzn(final IObjectWrapper objectWrapper, final zzbpr zzbpr, final int n) {
        throw null;
    }
    
    public final zzbxj zzo(final IObjectWrapper objectWrapper, final String s, final zzbpr zzbpr, final int n) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)objectWrapper);
        zza.writeString(s);
        zzavi.zzf(zza, (IInterface)zzbpr);
        zza.writeInt(234310000);
        final Parcel zzbh = this.zzbh(12, zza);
        final zzbxj zzq = zzbxi.zzq(zzbh.readStrongBinder());
        zzbh.recycle();
        return zzq;
    }
    
    public final zzcae zzp(final IObjectWrapper objectWrapper, final zzbpr zzbpr, final int n) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)objectWrapper);
        zzavi.zzf(zza, (IInterface)zzbpr);
        zza.writeInt(234310000);
        final Parcel zzbh = this.zzbh(14, zza);
        final zzcae zzb = zzcad.zzb(zzbh.readStrongBinder());
        zzbh.recycle();
        return zzb;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.internal.ads.zzbet;
import com.google.android.gms.internal.ads.zzawe;
import com.google.android.gms.internal.ads.zzbpr;
import com.google.android.gms.dynamic.ObjectWrapper;
import android.view.View;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.ads.ResponseInfo;
import com.google.android.gms.ads.zzb;
import android.os.RemoteException;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.internal.ads.zzcbg;
import android.content.Context;
import android.util.AttributeSet;
import com.google.android.gms.ads.OnPaidEventListener;
import org.checkerframework.checker.initialization.qual.NotOnlyInitialized;
import android.view.ViewGroup;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.admanager.AppEventListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.VideoController;
import java.util.concurrent.atomic.AtomicBoolean;
import com.google.android.gms.internal.ads.zzbpo;

public final class zzea
{
    final zzaz zza;
    private final zzbpo zzb;
    private final zzp zzc;
    private final AtomicBoolean zzd;
    private final VideoController zze;
    private zza zzf;
    private AdListener zzg;
    private AdSize[] zzh;
    private AppEventListener zzi;
    private zzbu zzj;
    private VideoOptions zzk;
    private String zzl;
    @NotOnlyInitialized
    private final ViewGroup zzm;
    private int zzn;
    private boolean zzo;
    private OnPaidEventListener zzp;
    
    public zzea(final ViewGroup viewGroup) {
        this(viewGroup, null, false, com.google.android.gms.ads.internal.client.zzp.zza, null, 0);
    }
    
    public zzea(final ViewGroup viewGroup, final int n) {
        this(viewGroup, null, false, com.google.android.gms.ads.internal.client.zzp.zza, null, n);
    }
    
    public zzea(final ViewGroup viewGroup, final AttributeSet set, final boolean b) {
        this(viewGroup, set, b, com.google.android.gms.ads.internal.client.zzp.zza, null, 0);
    }
    
    public zzea(final ViewGroup viewGroup, final AttributeSet set, final boolean b, final int n) {
        this(viewGroup, set, b, com.google.android.gms.ads.internal.client.zzp.zza, null, n);
    }
    
    public zzea(final ViewGroup zzm, final AttributeSet set, final boolean b, final zzp zzc, zzbu context, int zzn) {
        this.zzb = new zzbpo();
        this.zze = new VideoController();
        this.zza = new zzdz(this);
        this.zzm = zzm;
        this.zzc = zzc;
        this.zzj = null;
        this.zzd = new AtomicBoolean(false);
        this.zzn = zzn;
        if (set != null) {
            context = (zzbu)((View)zzm).getContext();
            try {
                final zzy zzy = new zzy((Context)context, set);
                this.zzh = zzy.zzb(b);
                this.zzl = zzy.zza();
                if (((View)zzm).isInEditMode()) {
                    final zzcbg zzb = zzay.zzb();
                    final AdSize adSize = this.zzh[0];
                    zzn = this.zzn;
                    zzq zze;
                    if (adSize.equals(AdSize.INVALID)) {
                        zze = zzq.zze();
                    }
                    else {
                        zze = new zzq((Context)context, adSize);
                        zze.zzj = zzD(zzn);
                    }
                    zzb.zzm(zzm, zze, "Ads by Google");
                }
            }
            catch (final IllegalArgumentException ex) {
                zzay.zzb().zzl(zzm, new zzq((Context)context, AdSize.BANNER), ex.getMessage(), ex.getMessage());
            }
        }
    }
    
    private static zzq zzC(final Context context, final AdSize[] array, final int n) {
        for (int length = array.length, i = 0; i < length; ++i) {
            if (array[i].equals(AdSize.INVALID)) {
                return zzq.zze();
            }
        }
        final zzq zzq = new zzq(context, array);
        zzq.zzj = zzD(n);
        return zzq;
    }
    
    private static boolean zzD(final int n) {
        return n == 1;
    }
    
    public final boolean zzA() {
        try {
            final zzbu zzj = this.zzj;
            if (zzj != null) {
                return zzj.zzY();
            }
        }
        catch (final RemoteException ex) {
            zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex);
        }
        return false;
    }
    
    public final AdSize[] zzB() {
        return this.zzh;
    }
    
    public final AdListener zza() {
        return this.zzg;
    }
    
    public final AdSize zzb() {
        try {
            final zzbu zzj = this.zzj;
            if (zzj != null) {
                final zzq zzg = zzj.zzg();
                if (zzg != null) {
                    return com.google.android.gms.ads.zzb.zzc(zzg.zze, zzg.zzb, zzg.zza);
                }
            }
        }
        catch (final RemoteException ex) {
            zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex);
        }
        final AdSize[] zzh = this.zzh;
        if (zzh != null) {
            return zzh[0];
        }
        return null;
    }
    
    public final OnPaidEventListener zzc() {
        return this.zzp;
    }
    
    public final ResponseInfo zzd() {
        final zzdn zzdn = null;
        zzdn zzk;
        try {
            final zzbu zzj = this.zzj;
            zzk = zzdn;
            if (zzj != null) {
                zzk = zzj.zzk();
            }
        }
        catch (final RemoteException ex) {
            zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex);
            zzk = zzdn;
        }
        return ResponseInfo.zza(zzk);
    }
    
    public final VideoController zzf() {
        return this.zze;
    }
    
    public final VideoOptions zzg() {
        return this.zzk;
    }
    
    public final AppEventListener zzh() {
        return this.zzi;
    }
    
    public final zzdq zzi() {
        final zzbu zzj = this.zzj;
        if (zzj != null) {
            try {
                return zzj.zzl();
            }
            catch (final RemoteException ex) {
                zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex);
            }
        }
        return null;
    }
    
    public final String zzj() {
        if (this.zzl == null) {
            final zzbu zzj = this.zzj;
            if (zzj != null) {
                try {
                    this.zzl = zzj.zzr();
                }
                catch (final RemoteException ex) {
                    zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex);
                }
            }
        }
        return this.zzl;
    }
    
    public final void zzk() {
        try {
            final zzbu zzj = this.zzj;
            if (zzj != null) {
                zzj.zzx();
            }
        }
        catch (final RemoteException ex) {
            zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex);
        }
    }
    
    public final void zzm(final zzdx zzdx) {
        try {
            if (this.zzj == null) {
                if (this.zzh == null || this.zzl == null) {
                    throw new IllegalStateException("The ad size and ad unit ID must be set before loadAd is called.");
                }
                final Context context = ((View)this.zzm).getContext();
                final zzq zzC = zzC(context, this.zzh, this.zzn);
                Object o;
                if ("search_v2".equals(zzC.zza)) {
                    o = new zzal(zzay.zza(), context, zzC, this.zzl).zzd(context, false);
                }
                else {
                    o = new zzaj(zzay.zza(), context, zzC, this.zzl, (zzbpr)this.zzb).zzd(context, false);
                }
                (this.zzj = (zzbu)o).zzD(new zzg(this.zza));
                final zza zzf = this.zzf;
                if (zzf != null) {
                    this.zzj.zzC(new com.google.android.gms.ads.internal.client.zzb(zzf));
                }
                final AppEventListener zzi = this.zzi;
                if (zzi != null) {
                    this.zzj.zzG((zzcb)new zzawe(zzi));
                }
                if (this.zzk != null) {
                    this.zzj.zzU(new zzfl(this.zzk));
                }
                this.zzj.zzP(new zzfe(this.zzp));
                this.zzj.zzN(this.zzo);
                final zzbu zzj = this.zzj;
                if (zzj != null) {
                    try {
                        final IObjectWrapper zzn = zzj.zzn();
                        if (zzn != null) {
                            if ((boolean)zzbet.zzf.zze() && (boolean)zzba.zzc().zza(zzbdc.zzkt)) {
                                zzcbg.zza.post((Runnable)new zzdy(this, zzn));
                            }
                            else {
                                this.zzm.addView((View)ObjectWrapper.unwrap(zzn));
                            }
                        }
                    }
                    catch (final RemoteException ex) {
                        zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex);
                    }
                }
            }
            final zzbu zzj2 = this.zzj;
            zzj2.getClass();
            zzj2.zzaa(this.zzc.zza(((View)this.zzm).getContext(), zzdx));
        }
        catch (final RemoteException ex2) {
            zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex2);
        }
    }
    
    public final void zzn() {
        try {
            final zzbu zzj = this.zzj;
            if (zzj != null) {
                zzj.zzz();
            }
        }
        catch (final RemoteException ex) {
            zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex);
        }
    }
    
    public final void zzo() {
        if (this.zzd.getAndSet(true)) {
            return;
        }
        try {
            final zzbu zzj = this.zzj;
            if (zzj != null) {
                zzj.zzA();
            }
        }
        catch (final RemoteException ex) {
            zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex);
        }
    }
    
    public final void zzp() {
        try {
            final zzbu zzj = this.zzj;
            if (zzj != null) {
                zzj.zzB();
            }
        }
        catch (final RemoteException ex) {
            zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex);
        }
    }
    
    public final void zzq(final zza zzf) {
        try {
            this.zzf = zzf;
            final zzbu zzj = this.zzj;
            if (zzj != null) {
                zzbe zzbe;
                if (zzf != null) {
                    zzbe = new com.google.android.gms.ads.internal.client.zzb(zzf);
                }
                else {
                    zzbe = null;
                }
                zzj.zzC(zzbe);
            }
        }
        catch (final RemoteException ex) {
            zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex);
        }
    }
    
    public final void zzr(final AdListener zzg) {
        this.zzg = zzg;
        this.zza.zza(zzg);
    }
    
    public final void zzs(final AdSize... array) {
        if (this.zzh == null) {
            this.zzt(array);
            return;
        }
        throw new IllegalStateException("The ad size can only be set once on AdView.");
    }
    
    public final void zzt(final AdSize... zzh) {
        this.zzh = zzh;
        try {
            final zzbu zzj = this.zzj;
            if (zzj != null) {
                zzj.zzF(zzC(((View)this.zzm).getContext(), this.zzh, this.zzn));
            }
        }
        catch (final RemoteException ex) {
            zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex);
        }
        ((View)this.zzm).requestLayout();
    }
    
    public final void zzu(final String zzl) {
        if (this.zzl == null) {
            this.zzl = zzl;
            return;
        }
        throw new IllegalStateException("The ad unit ID can only be set once on AdView.");
    }
    
    public final void zzv(final AppEventListener zzi) {
        try {
            this.zzi = zzi;
            final zzbu zzj = this.zzj;
            if (zzj != null) {
                Object o;
                if (zzi != null) {
                    o = new zzawe(zzi);
                }
                else {
                    o = null;
                }
                zzj.zzG((zzcb)o);
            }
        }
        catch (final RemoteException ex) {
            zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex);
        }
    }
    
    public final void zzw(final boolean zzo) {
        this.zzo = zzo;
        try {
            final zzbu zzj = this.zzj;
            if (zzj != null) {
                zzj.zzN(zzo);
            }
        }
        catch (final RemoteException ex) {
            zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex);
        }
    }
    
    public final void zzx(final OnPaidEventListener zzp) {
        try {
            this.zzp = zzp;
            final zzbu zzj = this.zzj;
            if (zzj != null) {
                zzj.zzP(new zzfe(zzp));
            }
        }
        catch (final RemoteException ex) {
            zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex);
        }
    }
    
    public final void zzy(final VideoOptions zzk) {
        this.zzk = zzk;
        try {
            final zzbu zzj = this.zzj;
            if (zzj != null) {
                zzfl zzfl;
                if (zzk == null) {
                    zzfl = null;
                }
                else {
                    zzfl = new zzfl(zzk);
                }
                zzj.zzU(zzfl);
            }
        }
        catch (final RemoteException ex) {
            zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex);
        }
    }
    
    public final boolean zzz(final zzbu zzj) {
        try {
            final IObjectWrapper zzn = zzj.zzn();
            if (zzn != null) {
                if (((View)ObjectWrapper.unwrap(zzn)).getParent() == null) {
                    this.zzm.addView((View)ObjectWrapper.unwrap(zzn));
                    this.zzj = zzj;
                    return true;
                }
            }
            return false;
        }
        catch (final RemoteException ex) {
            zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex);
            return false;
        }
    }
}

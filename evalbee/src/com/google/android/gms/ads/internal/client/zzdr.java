// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.Parcel;
import com.google.android.gms.internal.ads.zzavi;
import android.os.IBinder;
import com.google.android.gms.internal.ads.zzavg;

public final class zzdr extends zzavg implements zzdt
{
    public zzdr(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IVideoLifecycleCallbacks");
    }
    
    public final void zze() {
        this.zzbi(4, this.zza());
    }
    
    public final void zzf(final boolean b) {
        final Parcel zza = this.zza();
        final int zza2 = zzavi.zza;
        zza.writeInt((int)(b ? 1 : 0));
        this.zzbi(5, zza);
    }
    
    public final void zzg() {
        this.zzbi(3, this.zza());
    }
    
    public final void zzh() {
        this.zzbi(2, this.zza());
    }
    
    public final void zzi() {
        this.zzbi(1, this.zza());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal;

import android.view.MotionEvent;
import android.view.View;
import android.view.View$OnTouchListener;

final class zzn implements View$OnTouchListener
{
    final zzs zza;
    
    public zzn(final zzs zza) {
        this.zza = zza;
    }
    
    public final boolean onTouch(final View view, final MotionEvent motionEvent) {
        final zzs zza = this.zza;
        if (zzs.zzf(zza) != null) {
            zzs.zzf(zza).zzd(motionEvent);
        }
        return false;
    }
}

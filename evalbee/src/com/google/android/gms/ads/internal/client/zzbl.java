// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ads.zzavi;
import android.os.IBinder;
import com.google.android.gms.internal.ads.zzavg;

public final class zzbl extends zzavg implements zzbn
{
    public zzbl(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IAdLoader");
    }
    
    public final String zze() {
        throw null;
    }
    
    public final String zzf() {
        throw null;
    }
    
    public final void zzg(final zzl zzl) {
        final Parcel zza = this.zza();
        zzavi.zzd(zza, (Parcelable)zzl);
        this.zzbi(1, zza);
    }
    
    public final void zzh(final zzl zzl, final int n) {
        final Parcel zza = this.zza();
        zzavi.zzd(zza, (Parcelable)zzl);
        zza.writeInt(n);
        this.zzbi(5, zza);
    }
    
    public final boolean zzi() {
        final Parcel zzbh = this.zzbh(3, this.zza());
        final boolean zzg = zzavi.zzg(zzbh);
        zzbh.recycle();
        return zzg;
    }
}

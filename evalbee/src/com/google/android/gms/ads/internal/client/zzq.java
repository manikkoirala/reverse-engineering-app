// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import android.view.Display;
import android.util.DisplayMetrics;
import com.google.android.gms.internal.ads.zzcbg;
import android.view.WindowManager;
import com.google.android.gms.ads.zzb;
import com.google.android.gms.ads.AdSize;
import android.content.Context;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "AdSizeParcelCreator")
@Reserved({ 1 })
public final class zzq extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zzq> CREATOR;
    @Field(id = 2)
    public final String zza;
    @Field(id = 3)
    public final int zzb;
    @Field(id = 4)
    public final int zzc;
    @Field(id = 5)
    public final boolean zzd;
    @Field(id = 6)
    public final int zze;
    @Field(id = 7)
    public final int zzf;
    @Field(id = 8)
    public final zzq[] zzg;
    @Field(id = 9)
    public final boolean zzh;
    @Field(id = 10)
    public final boolean zzi;
    @Field(id = 11)
    public boolean zzj;
    @Field(id = 12)
    public boolean zzk;
    @Field(id = 13)
    public boolean zzl;
    @Field(id = 14)
    public boolean zzm;
    @Field(id = 15)
    public boolean zzn;
    @Field(id = 16)
    public boolean zzo;
    
    static {
        CREATOR = (Parcelable$Creator)new zzr();
    }
    
    public zzq() {
        this("interstitial_mb", 0, 0, true, 0, 0, null, false, false, false, false, false, false, false, false);
    }
    
    public zzq(final Context context, final AdSize adSize) {
        this(context, new AdSize[] { adSize });
    }
    
    public zzq(final Context context, final AdSize[] array) {
        final AdSize adSize = array[0];
        this.zzd = false;
        final boolean fluid = adSize.isFluid();
        this.zzi = fluid;
        this.zzm = com.google.android.gms.ads.zzb.zzf(adSize);
        this.zzn = com.google.android.gms.ads.zzb.zzg(adSize);
        final boolean zzh = com.google.android.gms.ads.zzb.zzh(adSize);
        this.zzo = zzh;
        int zzb;
        if (fluid) {
            final AdSize banner = AdSize.BANNER;
            this.zze = banner.getWidth();
            zzb = banner.getHeight();
        }
        else if (this.zzn) {
            this.zze = adSize.getWidth();
            zzb = com.google.android.gms.ads.zzb.zza(adSize);
        }
        else if (zzh) {
            this.zze = adSize.getWidth();
            zzb = com.google.android.gms.ads.zzb.zzb(adSize);
        }
        else {
            this.zze = adSize.getWidth();
            zzb = adSize.getHeight();
        }
        this.zzb = zzb;
        final boolean b = this.zze == -1;
        final boolean b2 = zzb == -2;
        final DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int zze;
        if (b) {
            zzay.zzb();
            int widthPixels4 = 0;
            Label_0417: {
                if (context.getResources().getConfiguration().orientation == 2) {
                    final DisplayMetrics displayMetrics2 = context.getResources().getDisplayMetrics();
                    if ((int)(displayMetrics2.heightPixels / displayMetrics2.density) < 600) {
                        zzay.zzb();
                        final DisplayMetrics displayMetrics3 = context.getResources().getDisplayMetrics();
                        final WindowManager windowManager = (WindowManager)context.getSystemService("window");
                        if (windowManager != null) {
                            final Display defaultDisplay = windowManager.getDefaultDisplay();
                            defaultDisplay.getRealMetrics(displayMetrics3);
                            final int heightPixels = displayMetrics3.heightPixels;
                            final int widthPixels = displayMetrics3.widthPixels;
                            defaultDisplay.getMetrics(displayMetrics3);
                            final int heightPixels2 = displayMetrics3.heightPixels;
                            final int widthPixels2 = displayMetrics3.widthPixels;
                            if (heightPixels2 == heightPixels && widthPixels2 == widthPixels) {
                                final int widthPixels3 = displayMetrics.widthPixels;
                                zzay.zzb();
                                final int identifier = context.getResources().getIdentifier("navigation_bar_width", "dimen", "android");
                                int dimensionPixelSize;
                                if (identifier > 0) {
                                    dimensionPixelSize = context.getResources().getDimensionPixelSize(identifier);
                                }
                                else {
                                    dimensionPixelSize = 0;
                                }
                                widthPixels4 = widthPixels3 - dimensionPixelSize;
                                break Label_0417;
                            }
                        }
                    }
                }
                widthPixels4 = displayMetrics.widthPixels;
            }
            this.zzf = widthPixels4;
            final double n = widthPixels4 / displayMetrics.density;
            final int n2 = zze = (int)n;
            if (n - n2 >= 0.01) {
                zze = n2 + 1;
            }
        }
        else {
            zze = this.zze;
            zzay.zzb();
            this.zzf = zzcbg.zzp(displayMetrics, this.zze);
        }
        int i;
        if (b2) {
            i = zzf(displayMetrics);
        }
        else {
            i = this.zzb;
        }
        zzay.zzb();
        this.zzc = zzcbg.zzp(displayMetrics, i);
        String zza;
        if (!b && !b2) {
            if (!this.zzn && !this.zzo) {
                if (fluid) {
                    zza = "320x50_mb";
                }
                else {
                    zza = adSize.toString();
                }
            }
            else {
                final int zze2 = this.zze;
                final int zzb2 = this.zzb;
                final StringBuilder sb = new StringBuilder();
                sb.append(zze2);
                sb.append("x");
                sb.append(zzb2);
                sb.append("_as");
                zza = sb.toString();
            }
        }
        else {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(zze);
            sb2.append("x");
            sb2.append(i);
            sb2.append("_as");
            zza = sb2.toString();
        }
        this.zza = zza;
        final int length = array.length;
        if (length > 1) {
            this.zzg = new zzq[length];
            for (int j = 0; j < array.length; ++j) {
                this.zzg[j] = new zzq(context, array[j]);
            }
        }
        else {
            this.zzg = null;
        }
        this.zzh = false;
        this.zzj = false;
    }
    
    @Constructor
    public zzq(@Param(id = 2) final String zza, @Param(id = 3) final int zzb, @Param(id = 4) final int zzc, @Param(id = 5) final boolean zzd, @Param(id = 6) final int zze, @Param(id = 7) final int zzf, @Param(id = 8) final zzq[] zzg, @Param(id = 9) final boolean zzh, @Param(id = 10) final boolean zzi, @Param(id = 11) final boolean zzj, @Param(id = 12) final boolean zzk, @Param(id = 13) final boolean zzl, @Param(id = 14) final boolean zzm, @Param(id = 15) final boolean zzn, @Param(id = 16) final boolean zzo) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
        this.zzg = zzg;
        this.zzh = zzh;
        this.zzi = zzi;
        this.zzj = zzj;
        this.zzk = zzk;
        this.zzl = zzl;
        this.zzm = zzm;
        this.zzn = zzn;
        this.zzo = zzo;
    }
    
    public static int zza(final DisplayMetrics displayMetrics) {
        return (int)(zzf(displayMetrics) * displayMetrics.density);
    }
    
    public static zzq zzb() {
        return new zzq("interstitial_mb", 0, 0, false, 0, 0, null, false, false, false, false, true, false, false, false);
    }
    
    public static zzq zzc() {
        return new zzq("320x50_mb", 0, 0, false, 0, 0, null, true, false, false, false, false, false, false, false);
    }
    
    public static zzq zzd() {
        return new zzq("reward_mb", 0, 0, true, 0, 0, null, false, false, false, false, false, false, false, false);
    }
    
    public static zzq zze() {
        return new zzq("invalid", 0, 0, false, 0, 0, null, false, false, false, true, false, false, false, false);
    }
    
    private static int zzf(final DisplayMetrics displayMetrics) {
        final int n = (int)(displayMetrics.heightPixels / displayMetrics.density);
        if (n <= 400) {
            return 32;
        }
        if (n <= 720) {
            return 50;
        }
        return 90;
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final String zza = this.zza;
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 2, zza, false);
        SafeParcelWriter.writeInt(parcel, 3, this.zzb);
        SafeParcelWriter.writeInt(parcel, 4, this.zzc);
        SafeParcelWriter.writeBoolean(parcel, 5, this.zzd);
        SafeParcelWriter.writeInt(parcel, 6, this.zze);
        SafeParcelWriter.writeInt(parcel, 7, this.zzf);
        SafeParcelWriter.writeTypedArray(parcel, 8, this.zzg, n, false);
        SafeParcelWriter.writeBoolean(parcel, 9, this.zzh);
        SafeParcelWriter.writeBoolean(parcel, 10, this.zzi);
        SafeParcelWriter.writeBoolean(parcel, 11, this.zzj);
        SafeParcelWriter.writeBoolean(parcel, 12, this.zzk);
        SafeParcelWriter.writeBoolean(parcel, 13, this.zzl);
        SafeParcelWriter.writeBoolean(parcel, 14, this.zzm);
        SafeParcelWriter.writeBoolean(parcel, 15, this.zzn);
        SafeParcelWriter.writeBoolean(parcel, 16, this.zzo);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

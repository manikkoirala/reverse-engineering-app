// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import android.media.AudioManager;
import android.content.Context;

public final class zzac
{
    private boolean zza;
    private float zzb;
    
    public zzac() {
        this.zza = false;
        this.zzb = 1.0f;
    }
    
    public static float zzb(final Context context) {
        final AudioManager audioManager = (AudioManager)context.getSystemService("audio");
        if (audioManager != null) {
            final int streamMaxVolume = audioManager.getStreamMaxVolume(3);
            final int streamVolume = audioManager.getStreamVolume(3);
            if (streamMaxVolume != 0) {
                return streamVolume / (float)streamMaxVolume;
            }
        }
        return 0.0f;
    }
    
    private final boolean zzf() {
        synchronized (this) {
            final float zzb = this.zzb;
            monitorexit(this);
            return zzb >= 0.0f;
        }
    }
    
    public final float zza() {
        synchronized (this) {
            if (this.zzf()) {
                return this.zzb;
            }
            return 1.0f;
        }
    }
    
    public final void zzc(final boolean zza) {
        synchronized (this) {
            this.zza = zza;
        }
    }
    
    public final void zzd(final float zzb) {
        synchronized (this) {
            this.zzb = zzb;
        }
    }
    
    public final boolean zze() {
        synchronized (this) {
            return this.zza;
        }
    }
}

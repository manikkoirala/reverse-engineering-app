// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import android.os.BaseBundle;
import java.util.Map;
import java.io.Writer;
import java.io.StringWriter;
import com.google.android.gms.internal.ads.zzfea;
import java.io.IOException;
import android.util.JsonWriter;
import org.json.JSONException;
import android.util.JsonToken;
import android.util.JsonReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import com.google.android.gms.internal.ads.zzcbn;
import android.os.Parcelable;
import org.json.JSONArray;
import android.os.Bundle;
import org.json.JSONObject;

public final class zzbw
{
    public static final zzbv zza;
    
    static {
        zza = new zzbu();
    }
    
    public static Bundle zza(final JSONObject jsonObject) {
        if (jsonObject == null) {
            return null;
        }
        final Iterator keys = jsonObject.keys();
        final Bundle bundle = new Bundle();
        while (keys.hasNext()) {
            final String s = keys.next();
            final Object opt = jsonObject.opt(s);
            if (opt != null) {
                if (opt instanceof Boolean) {
                    ((BaseBundle)bundle).putBoolean(s, (boolean)opt);
                }
                else if (opt instanceof Double) {
                    ((BaseBundle)bundle).putDouble(s, (double)opt);
                }
                else if (opt instanceof Integer) {
                    ((BaseBundle)bundle).putInt(s, (int)opt);
                }
                else if (opt instanceof Long) {
                    ((BaseBundle)bundle).putLong(s, (long)opt);
                }
                else if (opt instanceof String) {
                    ((BaseBundle)bundle).putString(s, (String)opt);
                }
                else {
                    String s3 = null;
                    Label_0611: {
                        String str;
                        String s2;
                        if (opt instanceof JSONArray) {
                            final JSONArray jsonArray = (JSONArray)opt;
                            if (jsonArray.length() == 0) {
                                continue;
                            }
                            final int length = jsonArray.length();
                            final int n = 0;
                            final int n2 = 0;
                            final int n3 = 0;
                            final int n4 = 0;
                            Object opt2 = null;
                            for (int n5 = 0; opt2 == null && n5 < length; ++n5) {
                                if (!jsonArray.isNull(n5)) {
                                    opt2 = jsonArray.opt(n5);
                                }
                                else {
                                    opt2 = null;
                                }
                            }
                            if (opt2 == null) {
                                str = String.valueOf(s);
                                s2 = "Expected JSONArray with at least 1 non-null element for key:";
                            }
                            else {
                                if (opt2 instanceof JSONObject) {
                                    final Bundle[] array = new Bundle[length];
                                    for (int i = n4; i < length; ++i) {
                                        Bundle zza;
                                        if (!jsonArray.isNull(i)) {
                                            zza = zza(jsonArray.optJSONObject(i));
                                        }
                                        else {
                                            zza = null;
                                        }
                                        array[i] = zza;
                                    }
                                    bundle.putParcelableArray(s, (Parcelable[])array);
                                    continue;
                                }
                                if (opt2 instanceof Number) {
                                    final double[] array2 = new double[jsonArray.length()];
                                    for (int j = n; j < length; ++j) {
                                        array2[j] = jsonArray.optDouble(j);
                                    }
                                    ((BaseBundle)bundle).putDoubleArray(s, array2);
                                    continue;
                                }
                                if (opt2 instanceof CharSequence) {
                                    final String[] array3 = new String[length];
                                    for (int k = n2; k < length; ++k) {
                                        String optString;
                                        if (!jsonArray.isNull(k)) {
                                            optString = jsonArray.optString(k);
                                        }
                                        else {
                                            optString = null;
                                        }
                                        array3[k] = optString;
                                    }
                                    ((BaseBundle)bundle).putStringArray(s, array3);
                                    continue;
                                }
                                if (opt2 instanceof Boolean) {
                                    final boolean[] array4 = new boolean[length];
                                    for (int l = n3; l < length; ++l) {
                                        array4[l] = jsonArray.optBoolean(l);
                                    }
                                    ((BaseBundle)bundle).putBooleanArray(s, array4);
                                    continue;
                                }
                                s3 = String.format("JSONArray with unsupported type %s for key:%s", opt2.getClass().getCanonicalName(), s);
                                break Label_0611;
                            }
                        }
                        else {
                            if (opt instanceof JSONObject) {
                                bundle.putBundle(s, zza((JSONObject)opt));
                                continue;
                            }
                            str = String.valueOf(s);
                            s2 = "Unsupported type for key:";
                        }
                        s3 = s2.concat(str);
                    }
                    zzcbn.zzj(s3);
                }
            }
        }
        return bundle;
    }
    
    public static String zzb(final String s, final JSONObject jsonObject, final String... array) {
        final JSONObject zzm = zzm(jsonObject, array);
        if (zzm == null) {
            return "";
        }
        return zzm.optString(array[0], "");
    }
    
    public static List zzc(final JSONArray jsonArray, final List list) {
        List list2 = list;
        if (list == null) {
            list2 = new ArrayList();
        }
        if (jsonArray != null) {
            for (int i = 0; i < jsonArray.length(); ++i) {
                list2.add(jsonArray.getString(i));
            }
        }
        return list2;
    }
    
    public static List zzd(final JsonReader jsonReader) {
        final ArrayList list = new ArrayList();
        jsonReader.beginArray();
        while (jsonReader.hasNext()) {
            list.add(jsonReader.nextString());
        }
        jsonReader.endArray();
        return list;
    }
    
    public static JSONArray zze(final JsonReader jsonReader) {
        final JSONArray jsonArray = new JSONArray();
        jsonReader.beginArray();
        while (jsonReader.hasNext()) {
            final JsonToken peek = jsonReader.peek();
            Object o;
            if (JsonToken.BEGIN_ARRAY.equals(peek)) {
                o = zze(jsonReader);
            }
            else if (JsonToken.BEGIN_OBJECT.equals(peek)) {
                o = zzh(jsonReader);
            }
            else {
                if (JsonToken.BOOLEAN.equals(peek)) {
                    jsonArray.put(jsonReader.nextBoolean());
                    continue;
                }
                if (JsonToken.NUMBER.equals(peek)) {
                    jsonArray.put(jsonReader.nextDouble());
                    continue;
                }
                if (!JsonToken.STRING.equals(peek)) {
                    throw new IllegalStateException("unexpected json token: ".concat(String.valueOf(peek)));
                }
                o = jsonReader.nextString();
            }
            jsonArray.put(o);
        }
        jsonReader.endArray();
        return jsonArray;
    }
    
    public static JSONObject zzf(JSONObject jsonObject, final String s) {
        try {
            jsonObject = jsonObject.getJSONObject(s);
        }
        catch (final JSONException ex) {
            final JSONObject jsonObject2 = new JSONObject();
            jsonObject.put(s, (Object)jsonObject2);
            jsonObject = jsonObject2;
        }
        return jsonObject;
    }
    
    public static JSONObject zzg(JSONObject zzm, final String... array) {
        zzm = zzm(zzm, array);
        if (zzm == null) {
            return null;
        }
        return zzm.optJSONObject(array[1]);
    }
    
    public static JSONObject zzh(final JsonReader jsonReader) {
        final JSONObject jsonObject = new JSONObject();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            final String nextName = jsonReader.nextName();
            final JsonToken peek = jsonReader.peek();
            Object o;
            if (JsonToken.BEGIN_ARRAY.equals(peek)) {
                o = zze(jsonReader);
            }
            else if (JsonToken.BEGIN_OBJECT.equals(peek)) {
                o = zzh(jsonReader);
            }
            else {
                if (JsonToken.BOOLEAN.equals(peek)) {
                    jsonObject.put(nextName, jsonReader.nextBoolean());
                    continue;
                }
                if (JsonToken.NUMBER.equals(peek)) {
                    jsonObject.put(nextName, jsonReader.nextDouble());
                    continue;
                }
                if (!JsonToken.STRING.equals(peek)) {
                    throw new IllegalStateException("unexpected json token: ".concat(String.valueOf(peek)));
                }
                o = jsonReader.nextString();
            }
            jsonObject.put(nextName, o);
        }
        jsonReader.endObject();
        return jsonObject;
    }
    
    public static void zzi(final JsonWriter jsonWriter, final JSONArray jsonArray) {
        try {
            jsonWriter.beginArray();
            for (int i = 0; i < jsonArray.length(); ++i) {
                final Object value = jsonArray.get(i);
                if (value instanceof String) {
                    jsonWriter.value((String)value);
                }
                else if (value instanceof Number) {
                    jsonWriter.value((Number)value);
                }
                else if (value instanceof Boolean) {
                    jsonWriter.value((boolean)value);
                }
                else if (value instanceof JSONObject) {
                    zzj(jsonWriter, (JSONObject)value);
                }
                else {
                    if (!(value instanceof JSONArray)) {
                        final String value2 = String.valueOf(value);
                        final StringBuilder sb = new StringBuilder();
                        sb.append("unable to write field: ");
                        sb.append(value2);
                        throw new JSONException(sb.toString());
                    }
                    zzi(jsonWriter, (JSONArray)value);
                }
            }
            jsonWriter.endArray();
        }
        catch (final JSONException cause) {
            throw new IOException((Throwable)cause);
        }
    }
    
    public static void zzj(final JsonWriter jsonWriter, final JSONObject jsonObject) {
        try {
            jsonWriter.beginObject();
            final Iterator keys = jsonObject.keys();
            while (keys.hasNext()) {
                final String s = keys.next();
                final Object value = jsonObject.get(s);
                if (value instanceof String) {
                    jsonWriter.name(s).value((String)value);
                }
                else if (value instanceof Number) {
                    jsonWriter.name(s).value((Number)value);
                }
                else if (value instanceof Boolean) {
                    jsonWriter.name(s).value((boolean)value);
                }
                else if (value instanceof JSONObject) {
                    zzj(jsonWriter.name(s), (JSONObject)value);
                }
                else {
                    if (!(value instanceof JSONArray)) {
                        final String value2 = String.valueOf(value);
                        final StringBuilder sb = new StringBuilder();
                        sb.append("unable to write field: ");
                        sb.append(value2);
                        throw new JSONException(sb.toString());
                    }
                    zzi(jsonWriter.name(s), (JSONArray)value);
                }
            }
            jsonWriter.endObject();
        }
        catch (final JSONException cause) {
            throw new IOException((Throwable)cause);
        }
    }
    
    public static boolean zzk(final boolean b, JSONObject zzm, final String... array) {
        zzm = zzm(zzm, array);
        return zzm != null && zzm.optBoolean(array[array.length - 1], false);
    }
    
    public static String zzl(final zzfea zzfea) {
        final String s = null;
        String string;
        if (zzfea == null) {
            string = s;
        }
        else {
            final StringWriter stringWriter = new StringWriter();
            try {
                final JsonWriter jsonWriter = new JsonWriter((Writer)stringWriter);
                zzn(jsonWriter, zzfea);
                jsonWriter.close();
                string = stringWriter.toString();
            }
            catch (final IOException ex) {
                zzcbn.zzh("Error when writing JSON.", (Throwable)ex);
                string = s;
            }
        }
        return string;
    }
    
    private static JSONObject zzm(JSONObject optJSONObject, final String[] array) {
        for (int i = 0; i < array.length - 1; ++i) {
            if (optJSONObject == null) {
                return null;
            }
            optJSONObject = optJSONObject.optJSONObject(array[i]);
        }
        return optJSONObject;
    }
    
    private static void zzn(final JsonWriter jsonWriter, final Object o) {
        if (o == null) {
            jsonWriter.nullValue();
            return;
        }
        if (o instanceof Number) {
            jsonWriter.value((Number)o);
            return;
        }
        if (o instanceof Boolean) {
            jsonWriter.value((boolean)o);
            return;
        }
        if (o instanceof String) {
            jsonWriter.value((String)o);
            return;
        }
        if (o instanceof zzfea) {
            zzj(jsonWriter, ((zzfea)o).zzd);
            return;
        }
        if (o instanceof Map) {
            jsonWriter.beginObject();
            for (final Map.Entry<Object, V> entry : ((Map)o).entrySet()) {
                final String key = entry.getKey();
                if (key instanceof String) {
                    zzn(jsonWriter.name((String)key), entry.getValue());
                }
            }
            jsonWriter.endObject();
            return;
        }
        if (o instanceof List) {
            jsonWriter.beginArray();
            final Iterator iterator2 = ((List)o).iterator();
            while (iterator2.hasNext()) {
                zzn(jsonWriter, iterator2.next());
            }
            jsonWriter.endArray();
            return;
        }
        jsonWriter.nullValue();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import android.graphics.drawable.Drawable;
import android.os.RemoteException;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.internal.ads.zzbhc;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.internal.ads.zzbgf;
import com.google.android.gms.ads.MediaContent;

public final class zzep implements MediaContent
{
    private final zzbgf zza;
    private final VideoController zzb;
    private final zzbhc zzc;
    
    public zzep(final zzbgf zza, final zzbhc zzc) {
        this.zzb = new VideoController();
        this.zza = zza;
        this.zzc = zzc;
    }
    
    @Override
    public final float getAspectRatio() {
        try {
            return this.zza.zze();
        }
        catch (final RemoteException ex) {
            zzcbn.zzh("", (Throwable)ex);
            return 0.0f;
        }
    }
    
    @Override
    public final float getCurrentTime() {
        try {
            return this.zza.zzf();
        }
        catch (final RemoteException ex) {
            zzcbn.zzh("", (Throwable)ex);
            return 0.0f;
        }
    }
    
    @Override
    public final float getDuration() {
        try {
            return this.zza.zzg();
        }
        catch (final RemoteException ex) {
            zzcbn.zzh("", (Throwable)ex);
            return 0.0f;
        }
    }
    
    @Override
    public final Drawable getMainImage() {
        try {
            final IObjectWrapper zzi = this.zza.zzi();
            if (zzi != null) {
                return (Drawable)ObjectWrapper.unwrap(zzi);
            }
        }
        catch (final RemoteException ex) {
            zzcbn.zzh("", (Throwable)ex);
        }
        return null;
    }
    
    @Override
    public final VideoController getVideoController() {
        try {
            if (this.zza.zzh() != null) {
                this.zzb.zzb(this.zza.zzh());
            }
        }
        catch (final RemoteException ex) {
            zzcbn.zzh("Exception occurred while getting video controller", (Throwable)ex);
        }
        return this.zzb;
    }
    
    @Override
    public final boolean hasVideoContent() {
        try {
            return this.zza.zzl();
        }
        catch (final RemoteException ex) {
            zzcbn.zzh("", (Throwable)ex);
            return false;
        }
    }
    
    @Override
    public final void setMainImage(final Drawable drawable) {
        try {
            this.zza.zzj(ObjectWrapper.wrap(drawable));
        }
        catch (final RemoteException ex) {
            zzcbn.zzh("", (Throwable)ex);
        }
    }
    
    @Override
    public final zzbhc zza() {
        return this.zzc;
    }
    
    @Override
    public final boolean zzb() {
        try {
            return this.zza.zzk();
        }
        catch (final RemoteException ex) {
            zzcbn.zzh("", (Throwable)ex);
            return false;
        }
    }
    
    public final zzbgf zzc() {
        return this.zza;
    }
}

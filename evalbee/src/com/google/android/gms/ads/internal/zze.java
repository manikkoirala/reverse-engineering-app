// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.ads.zzgbl;
import android.content.pm.PackageInfo;
import android.content.pm.ApplicationInfo;
import com.google.android.gms.internal.ads.zzbcu;
import com.google.android.gms.internal.ads.zzbok;
import com.google.android.gms.internal.ads.zzboo;
import com.google.android.gms.internal.ads.zzbou;
import com.google.android.gms.internal.ads.zzccd;
import java.util.concurrent.Executor;
import com.google.android.gms.internal.ads.zzgai;
import com.google.android.gms.internal.ads.zzgbb;
import com.google.android.gms.internal.ads.zzcca;
import com.google.android.gms.internal.ads.zzfjw;
import android.content.pm.PackageManager$NameNotFoundException;
import com.google.android.gms.common.wrappers.Wrappers;
import org.json.JSONObject;
import com.google.android.gms.internal.ads.zzbom;
import com.google.android.gms.internal.ads.zzbon;
import com.google.android.gms.internal.ads.zzbor;
import com.google.android.gms.internal.ads.zzfjv;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.ads.internal.client.zzba;
import android.text.TextUtils;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.internal.ads.zzcaq;
import com.google.android.gms.internal.ads.zzfkk;
import com.google.android.gms.internal.ads.zzcbt;
import android.content.Context;

public final class zze
{
    private Context zza;
    private long zzb;
    
    public zze() {
        this.zzb = 0L;
    }
    
    public final void zza(final Context context, final zzcbt zzcbt, final String s, final Runnable runnable, final zzfkk zzfkk) {
        this.zzb(context, zzcbt, true, null, s, null, runnable, zzfkk);
    }
    
    public final void zzb(final Context context, final zzcbt zzcbt, final boolean b, zzcaq zza, final String s, final String s2, final Runnable runnable, final zzfkk zzfkk) {
        if (zzt.zzB().elapsedRealtime() - this.zzb < 5000L) {
            zzcbn.zzj("Not retrying to fetch app settings");
            return;
        }
        this.zzb = zzt.zzB().elapsedRealtime();
        if (zza != null) {
            if (!TextUtils.isEmpty((CharSequence)zza.zzc())) {
                if (zzt.zzB().currentTimeMillis() - zza.zza() <= (long)zzba.zzc().zza(zzbdc.zzdV) && zza.zzi()) {
                    return;
                }
            }
        }
        if (context == null) {
            zzcbn.zzj("Context not provided to fetch application settings");
            return;
        }
        if (TextUtils.isEmpty((CharSequence)s) && TextUtils.isEmpty((CharSequence)s2)) {
            zzcbn.zzj("App settings could not be fetched. Required parameters missing");
            return;
        }
        Context applicationContext;
        if ((applicationContext = context.getApplicationContext()) == null) {
            applicationContext = context;
        }
        this.zza = applicationContext;
        zza = (zzcaq)zzfjv.zza(context, 4);
        ((zzfjw)zza).zzh();
        final zzbou zza2 = zzt.zzf().zza(this.zza, zzcbt, zzfkk);
        final zzboo zza3 = zzbor.zza;
        final zzbok zza4 = zza2.zza("google.afma.config.fetchAppSettings", (zzbon)zza3, (zzbom)zza3);
        try {
            final JSONObject jsonObject = new JSONObject();
            if (!TextUtils.isEmpty((CharSequence)s)) {
                jsonObject.put("app_id", (Object)s);
            }
            else if (!TextUtils.isEmpty((CharSequence)s2)) {
                jsonObject.put("ad_unit_id", (Object)s2);
            }
            jsonObject.put("is_init", b);
            jsonObject.put("pn", (Object)context.getPackageName());
            final zzbcu zza5 = zzbdc.zza;
            jsonObject.put("experiment_ids", (Object)TextUtils.join((CharSequence)",", (Iterable)zzba.zza().zza()));
            jsonObject.put("js", (Object)zzcbt.zza);
            try {
                final ApplicationInfo applicationInfo = this.zza.getApplicationInfo();
                if (applicationInfo != null) {
                    final PackageInfo packageInfo = Wrappers.packageManager(context).getPackageInfo(applicationInfo.packageName, 0);
                    if (packageInfo != null) {
                        jsonObject.put("version", packageInfo.versionCode);
                    }
                }
            }
            catch (final PackageManager$NameNotFoundException ex) {
                com.google.android.gms.ads.internal.util.zze.zza("Error fetching PackageInfo.");
            }
            final ik0 zzb = zza4.zzb((Object)jsonObject);
            final zzd zzd = new zzd(zzfkk, (zzfjw)zza);
            final zzgbl zzf = zzcca.zzf;
            final ik0 zzn = zzgbb.zzn(zzb, (zzgai)zzd, (Executor)zzf);
            if (runnable != null) {
                zzb.addListener(runnable, (Executor)zzf);
            }
            zzccd.zza(zzn, "ConfigLoader.maybeFetchNewAppSettings");
        }
        catch (final Exception ex2) {
            zzcbn.zzh("Error requesting application settings", (Throwable)ex2);
            ((zzfjw)zza).zzg((Throwable)ex2);
            ((zzfjw)zza).zzf(false);
            zzfkk.zzb(((zzfjw)zza).zzl());
        }
    }
    
    public final void zzc(final Context context, final zzcbt zzcbt, final String s, final zzcaq zzcaq, final zzfkk zzfkk) {
        String zzb;
        if (zzcaq != null) {
            zzb = zzcaq.zzb();
        }
        else {
            zzb = null;
        }
        this.zzb(context, zzcbt, false, zzcaq, zzb, s, null, zzfkk);
    }
}

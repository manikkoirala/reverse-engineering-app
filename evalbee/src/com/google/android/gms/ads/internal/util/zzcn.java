// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import android.util.Range;
import android.media.MediaCodecInfo$VideoCapabilities;
import android.media.MediaCodecInfo$CodecProfileLevel;
import android.media.MediaCodecInfo$CodecCapabilities;
import java.util.Iterator;
import android.media.MediaCodecInfo;
import java.util.ArrayList;
import java.util.Arrays;
import android.media.MediaCodecList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class zzcn
{
    private static final Map zza;
    private static List zzb;
    private static final Object zzc;
    
    static {
        zza = new HashMap();
        zzc = new Object();
    }
    
    public static List zza(final String s) {
        synchronized (zzcn.zzc) {
            final Map zza = zzcn.zza;
            if (zza.containsKey(s)) {
                return (List)zza.get(s);
            }
            try {
                synchronized (zzcn.zzc) {
                    if (zzcn.zzb == null) {
                        zzcn.zzb = Arrays.asList(new MediaCodecList(0).getCodecInfos());
                    }
                    monitorexit(zzcn.zzc);
                    final ArrayList<HashMap> list = new ArrayList<HashMap>();
                    for (final MediaCodecInfo mediaCodecInfo : zzcn.zzb) {
                        if (!mediaCodecInfo.isEncoder() && Arrays.asList(mediaCodecInfo.getSupportedTypes()).contains(s)) {
                            final HashMap e = new HashMap();
                            e.put("codecName", mediaCodecInfo.getName());
                            final MediaCodecInfo$CodecCapabilities capabilitiesForType = mediaCodecInfo.getCapabilitiesForType(s);
                            final ArrayList<Integer[]> list2 = new ArrayList<Integer[]>();
                            for (final MediaCodecInfo$CodecProfileLevel mediaCodecInfo$CodecProfileLevel : capabilitiesForType.profileLevels) {
                                list2.add(new Integer[] { mediaCodecInfo$CodecProfileLevel.profile, mediaCodecInfo$CodecProfileLevel.level });
                            }
                            e.put("profileLevels", list2);
                            final MediaCodecInfo$VideoCapabilities videoCapabilities = capabilitiesForType.getVideoCapabilities();
                            e.put("bitRatesBps", zzb(videoCapabilities.getBitrateRange()));
                            e.put("widthAlignment", videoCapabilities.getWidthAlignment());
                            e.put("heightAlignment", videoCapabilities.getHeightAlignment());
                            e.put("frameRates", zzb(videoCapabilities.getSupportedFrameRates()));
                            e.put("widths", zzb(videoCapabilities.getSupportedWidths()));
                            e.put("heights", zzb(videoCapabilities.getSupportedHeights()));
                            e.put("instancesLimit", capabilitiesForType.getMaxSupportedInstances());
                            list.add(e);
                        }
                    }
                    zzcn.zza.put(s, list);
                    return list;
                }
            }
            catch (final LinkageError zza) {}
            catch (final RuntimeException ex) {}
            final HashMap e2 = new HashMap();
            e2.put("error", zza.getClass().getSimpleName());
            final ArrayList<HashMap> list3 = new ArrayList<HashMap>();
            list3.add(e2);
            zzcn.zza.put(s, list3);
            return list3;
        }
    }
    
    private static Integer[] zzb(final Range range) {
        return new Integer[] { (Integer)range.getLower(), (Integer)range.getUpper() };
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import android.content.Intent;
import android.content.Context;
import android.content.BroadcastReceiver;

final class zzcl extends BroadcastReceiver
{
    final zzcm zza;
    
    public zzcl(final zzcm zza) {
        this.zza = zza;
    }
    
    public final void onReceive(final Context context, final Intent intent) {
        zzcm.zza(this.zza, context, intent);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.Parcel;
import android.os.IInterface;
import com.google.android.gms.internal.ads.zzavi;
import com.google.android.gms.internal.ads.zzbpr;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IBinder;
import com.google.android.gms.internal.ads.zzavg;

public final class zzbr extends zzavg
{
    public zzbr(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IAdLoaderBuilderCreator");
    }
    
    public final IBinder zze(final IObjectWrapper objectWrapper, final String s, final zzbpr zzbpr, final int n) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)objectWrapper);
        zza.writeString(s);
        zzavi.zzf(zza, (IInterface)zzbpr);
        zza.writeInt(234310000);
        final Parcel zzbh = this.zzbh(1, zza);
        final IBinder strongBinder = zzbh.readStrongBinder();
        zzbh.recycle();
        return strongBinder;
    }
}

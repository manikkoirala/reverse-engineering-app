// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.ads.zzavi;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.Parcel;
import com.google.android.gms.internal.ads.zzavh;

public abstract class zzdi extends zzavh implements zzdj
{
    public zzdi() {
        super("com.google.android.gms.ads.internal.client.IOutOfContextTester");
    }
    
    public final boolean zzbK(final int n, final Parcel parcel, final Parcel parcel2, final int n2) {
        if (n == 1) {
            final String string = parcel.readString();
            final IObjectWrapper interface1 = IObjectWrapper.Stub.asInterface(parcel.readStrongBinder());
            final IObjectWrapper interface2 = IObjectWrapper.Stub.asInterface(parcel.readStrongBinder());
            zzavi.zzc(parcel);
            this.zze(string, interface1, interface2);
            parcel2.writeNoException();
            return true;
        }
        return false;
    }
}

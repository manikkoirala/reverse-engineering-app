// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.overlay;

import com.google.android.gms.internal.ads.zzbcu;
import com.google.android.gms.ads.internal.client.zzba;
import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.internal.ads.zzbdc;
import android.content.ActivityNotFoundException;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.ads.internal.zzt;
import com.google.android.gms.ads.internal.util.zze;
import android.content.Intent;
import android.content.Context;

public final class zza
{
    public static final boolean zza(final Context context, final Intent intent, final zzz zzz, final zzx zzx, final boolean b) {
        if (b) {
            return zzc(context, intent.getData(), zzz, zzx);
        }
        try {
            final String uri = intent.toURI();
            final StringBuilder sb = new StringBuilder();
            sb.append("Launching an intent: ");
            sb.append(uri);
            zze.zza(sb.toString());
            zzt.zzp();
            com.google.android.gms.ads.internal.util.zzt.zzS(context, intent);
            if (zzz != null) {
                zzz.zzg();
            }
            if (zzx != null) {
                zzx.zza(true);
            }
            return true;
        }
        catch (final ActivityNotFoundException ex) {
            zzcbn.zzj(((Throwable)ex).getMessage());
            if (zzx != null) {
                zzx.zza(false);
            }
            return false;
        }
    }
    
    public static final boolean zzb(final Context context, final zzc zzc, final zzz zzz, final zzx zzx) {
        final int n = 0;
        String concat = null;
        Label_0010: {
            if (zzc != null) {
                zzbdc.zza(context);
                Intent zzh = zzc.zzh;
                if (zzh == null) {
                    final Intent intent = new Intent();
                    if (TextUtils.isEmpty((CharSequence)zzc.zzb)) {
                        concat = "Open GMSG did not contain a URL.";
                        break Label_0010;
                    }
                    if (!TextUtils.isEmpty((CharSequence)zzc.zzc)) {
                        intent.setDataAndType(Uri.parse(zzc.zzb), zzc.zzc);
                    }
                    else {
                        intent.setData(Uri.parse(zzc.zzb));
                    }
                    intent.setAction("android.intent.action.VIEW");
                    if (!TextUtils.isEmpty((CharSequence)zzc.zzd)) {
                        intent.setPackage(zzc.zzd);
                    }
                    if (!TextUtils.isEmpty((CharSequence)zzc.zze)) {
                        final String[] split = zzc.zze.split("/", 2);
                        if (split.length < 2) {
                            concat = "Could not parse component name from open GMSG: ".concat(String.valueOf(zzc.zze));
                            break Label_0010;
                        }
                        intent.setClassName(split[0], split[1]);
                    }
                    final String zzf = zzc.zzf;
                    if (!TextUtils.isEmpty((CharSequence)zzf)) {
                        int int1;
                        try {
                            int1 = Integer.parseInt(zzf);
                        }
                        catch (final NumberFormatException ex) {
                            zzcbn.zzj("Could not parse intent flags.");
                            int1 = n;
                        }
                        intent.addFlags(int1);
                    }
                    if (zzba.zzc().zza(zzbdc.zzes)) {
                        intent.addFlags(268435456);
                        intent.putExtra("android.support.customtabs.extra.user_opt_out", true);
                        zzh = intent;
                    }
                    else {
                        final zzbcu zzer = zzbdc.zzer;
                        zzh = intent;
                        if (zzba.zzc().zza(zzer)) {
                            zzt.zzp();
                            com.google.android.gms.ads.internal.util.zzt.zzo(context, intent);
                            zzh = intent;
                        }
                    }
                }
                return zza(context, zzh, zzz, zzx, zzc.zzj);
            }
            concat = "No intent data for launcher overlay.";
        }
        zzcbn.zzj(concat);
        return false;
    }
    
    private static final boolean zzc(final Context context, final Uri uri, final zzz zzz, final zzx zzx) {
        int zzm;
        try {
            final int n = zzm = zzt.zzp().zzm(context, uri);
            if (zzz != null) {
                zzz.zzg();
                zzm = n;
            }
        }
        catch (final ActivityNotFoundException ex) {
            zzcbn.zzj(((Throwable)ex).getMessage());
            zzm = 6;
        }
        if (zzx != null) {
            zzx.zzb(zzm);
        }
        return zzm == 5;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "LiteSdkVersionsParcelCreator")
public final class zzen extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zzen> CREATOR;
    @Field(getter = "getAdsDynamiteVersion", id = 1)
    private final int zza;
    @Field(getter = "getSdkVersionLite", id = 2)
    private final int zzb;
    @Field(getter = "getGranularVersion", id = 3)
    private final String zzc;
    
    static {
        CREATOR = (Parcelable$Creator)new zzeo();
    }
    
    public zzen() {
        this(234310600, 234310000, "22.6.0");
    }
    
    @Constructor
    public zzen(@Param(id = 1) final int zza, @Param(id = 2) final int zzb, @Param(id = 3) final String zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.zza);
        SafeParcelWriter.writeInt(parcel, 2, this.zzb);
        SafeParcelWriter.writeString(parcel, 3, this.zzc, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public final int zza() {
        return this.zzb;
    }
    
    public final String zzb() {
        return this.zzc;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.formats.PublisherAdViewOptions;
import com.google.android.gms.internal.ads.zzbfw;
import com.google.android.gms.internal.ads.zzbmm;
import com.google.android.gms.ads.formats.AdManagerAdViewOptions;
import com.google.android.gms.internal.ads.zzbhw;
import android.os.Parcelable;
import com.google.android.gms.internal.ads.zzbht;
import com.google.android.gms.internal.ads.zzbmv;
import com.google.android.gms.internal.ads.zzavi;
import com.google.android.gms.internal.ads.zzbhm;
import com.google.android.gms.internal.ads.zzbhp;
import com.google.android.gms.internal.ads.zzbhj;
import com.google.android.gms.internal.ads.zzbhg;
import android.os.IInterface;
import android.os.Parcel;
import android.os.IBinder;
import com.google.android.gms.internal.ads.zzavg;

public final class zzbo extends zzavg implements zzbq
{
    public zzbo(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IAdLoaderBuilder");
    }
    
    public final zzbn zze() {
        final Parcel zzbh = this.zzbh(1, this.zza());
        final IBinder strongBinder = zzbh.readStrongBinder();
        zzbn zzbn;
        if (strongBinder == null) {
            zzbn = null;
        }
        else {
            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdLoader");
            if (queryLocalInterface instanceof zzbn) {
                zzbn = (zzbn)queryLocalInterface;
            }
            else {
                zzbn = new zzbl(strongBinder);
            }
        }
        zzbh.recycle();
        return zzbn;
    }
    
    public final void zzf(final zzbhg zzbhg) {
        throw null;
    }
    
    public final void zzg(final zzbhj zzbhj) {
        throw null;
    }
    
    public final void zzh(final String s, final zzbhp zzbhp, final zzbhm zzbhm) {
        final Parcel zza = this.zza();
        zza.writeString(s);
        zzavi.zzf(zza, (IInterface)zzbhp);
        zzavi.zzf(zza, (IInterface)zzbhm);
        this.zzbi(5, zza);
    }
    
    public final void zzi(final zzbmv zzbmv) {
        throw null;
    }
    
    public final void zzj(final zzbht zzbht, final zzq zzq) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)zzbht);
        zzavi.zzd(zza, (Parcelable)zzq);
        this.zzbi(8, zza);
    }
    
    public final void zzk(final zzbhw zzbhw) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)zzbhw);
        this.zzbi(10, zza);
    }
    
    public final void zzl(final zzbh zzbh) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)zzbh);
        this.zzbi(2, zza);
    }
    
    public final void zzm(final AdManagerAdViewOptions adManagerAdViewOptions) {
        final Parcel zza = this.zza();
        zzavi.zzd(zza, (Parcelable)adManagerAdViewOptions);
        this.zzbi(15, zza);
    }
    
    public final void zzn(final zzbmm zzbmm) {
        throw null;
    }
    
    public final void zzo(final zzbfw zzbfw) {
        final Parcel zza = this.zza();
        zzavi.zzd(zza, (Parcelable)zzbfw);
        this.zzbi(6, zza);
    }
    
    public final void zzp(final PublisherAdViewOptions publisherAdViewOptions) {
        throw null;
    }
    
    public final void zzq(final zzcf zzcf) {
        throw null;
    }
}

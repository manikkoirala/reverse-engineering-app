// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.database.Cursor;
import android.content.ContentValues;
import android.net.Uri;
import android.os.BaseBundle;
import android.text.TextUtils;
import com.google.android.gms.internal.ads.zzbpk;
import com.google.android.gms.internal.ads.zzcbn;
import android.content.pm.PackageManager$NameNotFoundException;
import com.google.android.gms.common.wrappers.Wrappers;
import android.content.pm.ProviderInfo;
import android.content.Context;
import android.content.ContentProvider;

public final class zzek extends ContentProvider
{
    public final void attachInfo(final Context context, final ProviderInfo providerInfo) {
        final BaseBundle baseBundle = null;
        Object metaData = null;
        Label_0051: {
            String s;
            try {
                metaData = Wrappers.packageManager(context).getApplicationInfo(context.getPackageName(), 128).metaData;
                break Label_0051;
            }
            catch (final PackageManager$NameNotFoundException ex) {
                s = "Failed to load metadata: Package name not found.";
            }
            catch (final NullPointerException ex) {
                s = "Failed to load metadata: Null pointer exception.";
            }
            final PackageManager$NameNotFoundException ex;
            zzcbn.zzh(s, (Throwable)ex);
            metaData = baseBundle;
        }
        final zzbpk zza = zzbpk.zza();
        Label_0186: {
            if (metaData == null) {
                zzcbn.zzg("Metadata was null.");
                break Label_0186;
            }
            try {
                final String str = (String)((BaseBundle)metaData).get("com.google.android.gms.ads.APPLICATION_ID");
                try {
                    final Boolean b = (Boolean)((BaseBundle)metaData).get("com.google.android.gms.ads.DELAY_APP_MEASUREMENT_INIT");
                    try {
                        final String obj = (String)((BaseBundle)metaData).get("com.google.android.gms.ads.INTEGRATION_MANAGER");
                        if (str != null) {
                            if (!str.matches("^ca-app-pub-[0-9]{16}~[0-9]{10}$")) {
                                throw new IllegalStateException("\n\n******************************************************************************\n* Invalid application ID. Follow instructions here:                          *\n* https://googlemobileadssdk.page.link/admob-android-update-manifest         *\n* to find your app ID.                                                       *\n* Google Ad Manager publishers should follow instructions here:              *\n* https://googlemobileadssdk.page.link/ad-manager-android-update-manifest.   *\n******************************************************************************\n\n");
                            }
                            zzcbn.zze("Publisher provided Google AdMob App ID in manifest: ".concat(str));
                            if (b == null || !b) {
                                zza.zzb(context, str);
                            }
                        }
                        else {
                            if (TextUtils.isEmpty((CharSequence)obj)) {
                                throw new IllegalStateException("\n\n******************************************************************************\n* The Google Mobile Ads SDK was initialized incorrectly. AdMob publishers    *\n* should follow the instructions here:                                       *\n* https://googlemobileadssdk.page.link/admob-android-update-manifest         *\n* to add a valid App ID inside the AndroidManifest.                          *\n* Google Ad Manager publishers should follow instructions here:              *\n* https://googlemobileadssdk.page.link/ad-manager-android-update-manifest.   *\n******************************************************************************\n\n");
                            }
                            zzcbn.zze("The Google Mobile Ads SDK is integrated by ".concat(String.valueOf(obj)));
                        }
                        if (metaData != null) {
                            final boolean boolean1 = ((BaseBundle)metaData).getBoolean("com.google.android.gms.ads.flag.OPTIMIZE_INITIALIZATION", false);
                            final boolean boolean2 = ((BaseBundle)metaData).getBoolean("com.google.android.gms.ads.flag.OPTIMIZE_AD_LOADING", false);
                            if (boolean1) {
                                zzcbn.zze("com.google.android.gms.ads.flag.OPTIMIZE_INITIALIZATION is enabled");
                            }
                            if (boolean2) {
                                zzcbn.zze("com.google.android.gms.ads.flag.OPTIMIZE_AD_LOADING is enabled");
                            }
                        }
                        super.attachInfo(context, providerInfo);
                    }
                    catch (final ClassCastException cause) {
                        throw new IllegalStateException("The com.google.android.gms.ads.INTEGRATION_MANAGER metadata must have a String value.", cause);
                    }
                }
                catch (final ClassCastException cause2) {
                    throw new IllegalStateException("The com.google.android.gms.ads.DELAY_APP_MEASUREMENT_INIT metadata must have a boolean value.", cause2);
                }
            }
            catch (final ClassCastException cause3) {
                throw new IllegalStateException("The com.google.android.gms.ads.APPLICATION_ID metadata must have a String value.", cause3);
            }
        }
    }
    
    public final int delete(final Uri uri, final String s, final String[] array) {
        return 0;
    }
    
    public final String getType(final Uri uri) {
        return null;
    }
    
    public final Uri insert(final Uri uri, final ContentValues contentValues) {
        return null;
    }
    
    public final boolean onCreate() {
        return false;
    }
    
    public final Cursor query(final Uri uri, final String[] array, final String s, final String[] array2, final String s2) {
        return null;
    }
    
    public final int update(final Uri uri, final ContentValues contentValues, final String s, final String[] array) {
        return 0;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import androidx.work.b;
import com.google.android.gms.ads.internal.offline.buffering.OfflineNotificationPoster;
import com.google.android.gms.internal.ads.zzcbn;
import androidx.work.NetworkType;
import com.google.android.gms.ads.internal.offline.buffering.OfflinePingSender;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.dynamic.IObjectWrapper;
import androidx.work.WorkManager;
import androidx.work.a;
import android.content.Context;
import com.google.android.apps.common.proguard.UsedByReflection;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public class WorkManagerUtil extends zzbs
{
    @UsedByReflection("This class must be instantiated reflectively so that the default class loader can be used.")
    public WorkManagerUtil() {
    }
    
    private static void zzb(final Context context) {
        try {
            WorkManager.e(context.getApplicationContext(), new a.a().a());
        }
        catch (final IllegalStateException ex) {}
    }
    
    public final void zze(final IObjectWrapper objectWrapper) {
        final Context context = ObjectWrapper.unwrap(objectWrapper);
        zzb(context);
        try {
            final WorkManager d = WorkManager.d(context);
            d.a("offline_ping_sender_work");
            d.b(((n92.a)((n92.a)((n92.a)new t11.a(OfflinePingSender.class)).i(new zk.a().b(NetworkType.CONNECTED).a())).a("offline_ping_sender_work")).b());
        }
        catch (final IllegalStateException ex) {
            zzcbn.zzk("Failed to instantiate WorkManager.", (Throwable)ex);
        }
    }
    
    public final boolean zzf(final IObjectWrapper objectWrapper, final String s, final String s2) {
        final Context context = ObjectWrapper.unwrap(objectWrapper);
        zzb(context);
        final t11 t11 = (t11)((n92.a)((n92.a)((n92.a)((n92.a)new t11.a(OfflineNotificationPoster.class)).i(new zk.a().b(NetworkType.CONNECTED).a())).k(new b.a().e("uri", s).e("gws_query_id", s2).a())).a("offline_notification_work")).b();
        try {
            WorkManager.d(context).b(t11);
            return true;
        }
        catch (final IllegalStateException ex) {
            zzcbn.zzk("Failed to instantiate WorkManager.", (Throwable)ex);
            return false;
        }
    }
}

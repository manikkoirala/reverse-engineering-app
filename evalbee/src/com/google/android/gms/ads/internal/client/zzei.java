// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import com.google.android.gms.internal.ads.zzbmg;

final class zzei extends zzbmg
{
    final zzej zza;
    
    public final void zzb(final List list) {
        synchronized (zzej.zzg(this.zza)) {
            final zzej zza = this.zza;
            int i = 0;
            zzej.zzk(zza, false);
            zzej.zzj(this.zza, true);
            final ArrayList list2 = new ArrayList(zzej.zzi(this.zza));
            zzej.zzi(this.zza).clear();
            monitorexit(zzej.zzg(this.zza));
            final InitializationStatus zzd = zzej.zzd(list);
            while (i < list2.size()) {
                ((OnInitializationCompleteListener)list2.get(i)).onInitializationComplete(zzd);
                ++i;
            }
        }
    }
}

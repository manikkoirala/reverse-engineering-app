// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.MuteThisAdListener;

public final class zzct extends zzcr
{
    private final MuteThisAdListener zza;
    
    public zzct(final MuteThisAdListener zza) {
        this.zza = zza;
    }
    
    public final void zze() {
        this.zza.onAdMuted();
    }
}

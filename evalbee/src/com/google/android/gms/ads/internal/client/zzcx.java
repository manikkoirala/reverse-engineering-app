// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.ads.MuteThisAdReason;

public final class zzcx implements MuteThisAdReason
{
    private final String zza;
    private final zzcw zzb;
    
    public zzcx(final zzcw zzb) {
        this.zzb = zzb;
        String zze;
        try {
            zze = zzb.zze();
        }
        catch (final RemoteException ex) {
            zzcbn.zzh("", (Throwable)ex);
            zze = null;
        }
        this.zza = zze;
    }
    
    @Override
    public final String getDescription() {
        return this.zza;
    }
    
    @Override
    public final String toString() {
        return this.zza;
    }
    
    public final zzcw zza() {
        return this.zzb;
    }
}

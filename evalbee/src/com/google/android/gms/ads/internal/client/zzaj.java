// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.internal.ads.zzbpr;
import android.content.Context;

final class zzaj extends zzax
{
    final Context zza;
    final zzq zzb;
    final String zzc;
    final zzbpr zzd;
    final zzaw zze;
    
    public zzaj(final zzaw zze, final Context zza, final zzq zzb, final String zzc, final zzbpr zzd) {
        this.zze = zze;
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
    }
}

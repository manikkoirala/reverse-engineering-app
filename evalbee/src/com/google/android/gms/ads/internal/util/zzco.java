// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import com.google.android.gms.internal.ads.zzccn;
import com.google.android.gms.ads.internal.zzt;
import android.view.Window;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver$OnScrollChangedListener;
import android.view.ViewTreeObserver$OnGlobalLayoutListener;
import android.app.Activity;
import android.view.View;

public final class zzco
{
    private final View zza;
    private Activity zzb;
    private boolean zzc;
    private boolean zzd;
    private boolean zze;
    private final ViewTreeObserver$OnGlobalLayoutListener zzf;
    
    public zzco(final Activity zzb, final View zza, final ViewTreeObserver$OnGlobalLayoutListener zzf, final ViewTreeObserver$OnScrollChangedListener viewTreeObserver$OnScrollChangedListener) {
        this.zzb = zzb;
        this.zza = zza;
        this.zzf = zzf;
    }
    
    private static ViewTreeObserver zzf(final Activity activity) {
        final Window window = activity.getWindow();
        if (window != null) {
            final View decorView = window.getDecorView();
            if (decorView != null) {
                return decorView.getViewTreeObserver();
            }
        }
        return null;
    }
    
    private final void zzg() {
        if (!this.zzc) {
            final Activity zzb = this.zzb;
            if (zzb != null) {
                final ViewTreeObserver$OnGlobalLayoutListener zzf = this.zzf;
                final ViewTreeObserver zzf2 = zzf(zzb);
                if (zzf2 != null) {
                    zzf2.addOnGlobalLayoutListener(zzf);
                }
            }
            final View zza = this.zza;
            final ViewTreeObserver$OnGlobalLayoutListener zzf3 = this.zzf;
            zzt.zzx();
            zzccn.zza(zza, zzf3);
            this.zzc = true;
        }
    }
    
    private final void zzh() {
        final Activity zzb = this.zzb;
        if (zzb != null) {
            if (this.zzc) {
                final ViewTreeObserver$OnGlobalLayoutListener zzf = this.zzf;
                final ViewTreeObserver zzf2 = zzf(zzb);
                if (zzf2 != null) {
                    zzf2.removeOnGlobalLayoutListener(zzf);
                }
                this.zzc = false;
            }
        }
    }
    
    public final void zza() {
        this.zze = false;
        this.zzh();
    }
    
    public final void zzb() {
        this.zze = true;
        if (this.zzd) {
            this.zzg();
        }
    }
    
    public final void zzc() {
        this.zzd = true;
        if (this.zze) {
            this.zzg();
        }
    }
    
    public final void zzd() {
        this.zzd = false;
        this.zzh();
    }
    
    public final void zze(final Activity zzb) {
        this.zzb = zzb;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.ads.zzfow;

final class zzh implements zzfow
{
    final zzi zza;
    
    public zzh(final zzi zza) {
        this.zza = zza;
    }
    
    public final void zza(final int n, final long n2) {
        zzi.zza(this.zza).zzd(n, System.currentTimeMillis() - n2);
    }
    
    public final void zzb(final int n, final long n2, final String s) {
        zzi.zza(this.zza).zze(n, System.currentTimeMillis() - n2, s);
    }
}

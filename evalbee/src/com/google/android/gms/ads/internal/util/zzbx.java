// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.ads.zzfqv;
import android.os.Looper;
import android.os.Handler;
import android.os.HandlerThread;

public final class zzbx
{
    private HandlerThread zza;
    private Handler zzb;
    private int zzc;
    private final Object zzd;
    
    public zzbx() {
        this.zza = null;
        this.zzb = null;
        this.zzc = 0;
        this.zzd = new Object();
    }
    
    public final Handler zza() {
        return this.zzb;
    }
    
    public final Looper zzb() {
        synchronized (this.zzd) {
            if (this.zzc == 0) {
                if (this.zza == null) {
                    zze.zza("Starting the looper thread.");
                    ((Thread)(this.zza = new HandlerThread("LooperProvider"))).start();
                    this.zzb = (Handler)new zzfqv(this.zza.getLooper());
                    zze.zza("Looper thread started.");
                }
                else {
                    zze.zza("Resuming the looper thread");
                    this.zzd.notifyAll();
                }
            }
            else {
                Preconditions.checkNotNull(this.zza, "Invalid state: handlerThread should already been initialized.");
            }
            ++this.zzc;
            return this.zza.getLooper();
        }
    }
}

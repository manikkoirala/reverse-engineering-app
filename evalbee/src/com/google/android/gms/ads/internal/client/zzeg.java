// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.OnAdInspectorClosedListener;
import com.google.android.gms.ads.AdInspectorError;

final class zzeg extends zzcz
{
    private zzeg() {
    }
    
    public final void zze(final zze zze) {
        final OnAdInspectorClosedListener zzb = zzej.zzb(zzej.zzf());
        if (zzb != null) {
            AdInspectorError adInspectorError;
            if (zze == null) {
                adInspectorError = null;
            }
            else {
                adInspectorError = new AdInspectorError(zze.zza, zze.zzb, zze.zzc);
            }
            zzb.onAdInspectorClosed(adInspectorError);
        }
    }
}

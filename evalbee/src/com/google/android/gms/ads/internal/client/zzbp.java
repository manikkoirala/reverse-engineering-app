// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.ads.zzbhg;
import com.google.android.gms.internal.ads.zzbhj;
import com.google.android.gms.internal.ads.zzbhm;
import com.google.android.gms.internal.ads.zzbhp;
import android.os.IBinder;
import com.google.android.gms.internal.ads.zzbht;
import com.google.android.gms.internal.ads.zzbhw;
import com.google.android.gms.internal.ads.zzbmv;
import android.os.IInterface;
import com.google.android.gms.internal.ads.zzbhf;
import com.google.android.gms.internal.ads.zzbhi;
import com.google.android.gms.internal.ads.zzbhl;
import com.google.android.gms.internal.ads.zzbho;
import com.google.android.gms.internal.ads.zzbfw;
import com.google.android.gms.internal.ads.zzbhs;
import com.google.android.gms.ads.formats.PublisherAdViewOptions;
import com.google.android.gms.internal.ads.zzbhv;
import com.google.android.gms.internal.ads.zzbmm;
import com.google.android.gms.internal.ads.zzbmu;
import android.os.Parcelable$Creator;
import com.google.android.gms.internal.ads.zzavi;
import com.google.android.gms.ads.formats.AdManagerAdViewOptions;
import android.os.Parcel;
import com.google.android.gms.internal.ads.zzavh;

public abstract class zzbp extends zzavh implements zzbq
{
    public zzbp() {
        super("com.google.android.gms.ads.internal.client.IAdLoaderBuilder");
    }
    
    public final boolean zzbK(final int n, final Parcel parcel, final Parcel parcel2, final int n2) {
        final zzbh zzbh = null;
        zzcf zzcf = null;
        switch (n) {
            default: {
                return false;
            }
            case 15: {
                final AdManagerAdViewOptions adManagerAdViewOptions = (AdManagerAdViewOptions)zzavi.zza(parcel, (Parcelable$Creator)AdManagerAdViewOptions.CREATOR);
                zzavi.zzc(parcel);
                this.zzm(adManagerAdViewOptions);
                break;
            }
            case 14: {
                final zzbmv zzb = zzbmu.zzb(parcel.readStrongBinder());
                zzavi.zzc(parcel);
                this.zzi(zzb);
                break;
            }
            case 13: {
                final zzbmm zzbmm = (zzbmm)zzavi.zza(parcel, com.google.android.gms.internal.ads.zzbmm.CREATOR);
                zzavi.zzc(parcel);
                this.zzn(zzbmm);
                break;
            }
            case 10: {
                final zzbhw zzb2 = zzbhv.zzb(parcel.readStrongBinder());
                zzavi.zzc(parcel);
                this.zzk(zzb2);
                break;
            }
            case 9: {
                final PublisherAdViewOptions publisherAdViewOptions = (PublisherAdViewOptions)zzavi.zza(parcel, (Parcelable$Creator)PublisherAdViewOptions.CREATOR);
                zzavi.zzc(parcel);
                this.zzp(publisherAdViewOptions);
                break;
            }
            case 8: {
                final zzbht zzb3 = zzbhs.zzb(parcel.readStrongBinder());
                final zzq zzq = (zzq)zzavi.zza(parcel, (Parcelable$Creator)com.google.android.gms.ads.internal.client.zzq.CREATOR);
                zzavi.zzc(parcel);
                this.zzj(zzb3, zzq);
                break;
            }
            case 7: {
                final IBinder strongBinder = parcel.readStrongBinder();
                if (strongBinder != null) {
                    final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.ICorrelationIdProvider");
                    if (queryLocalInterface instanceof zzcf) {
                        zzcf = (zzcf)queryLocalInterface;
                    }
                    else {
                        zzcf = new zzcf(strongBinder);
                    }
                }
                zzavi.zzc(parcel);
                this.zzq(zzcf);
                break;
            }
            case 6: {
                final zzbfw zzbfw = (zzbfw)zzavi.zza(parcel, com.google.android.gms.internal.ads.zzbfw.CREATOR);
                zzavi.zzc(parcel);
                this.zzo(zzbfw);
                break;
            }
            case 5: {
                final String string = parcel.readString();
                final zzbhp zzb4 = zzbho.zzb(parcel.readStrongBinder());
                final zzbhm zzb5 = zzbhl.zzb(parcel.readStrongBinder());
                zzavi.zzc(parcel);
                this.zzh(string, zzb4, zzb5);
                break;
            }
            case 4: {
                final zzbhj zzb6 = zzbhi.zzb(parcel.readStrongBinder());
                zzavi.zzc(parcel);
                this.zzg(zzb6);
                break;
            }
            case 3: {
                final zzbhg zzb7 = zzbhf.zzb(parcel.readStrongBinder());
                zzavi.zzc(parcel);
                this.zzf(zzb7);
                break;
            }
            case 2: {
                final IBinder strongBinder2 = parcel.readStrongBinder();
                zzbh zzbh2;
                if (strongBinder2 == null) {
                    zzbh2 = zzbh;
                }
                else {
                    final IInterface queryLocalInterface2 = strongBinder2.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdListener");
                    if (queryLocalInterface2 instanceof zzbh) {
                        zzbh2 = (zzbh)queryLocalInterface2;
                    }
                    else {
                        zzbh2 = new zzbf(strongBinder2);
                    }
                }
                zzavi.zzc(parcel);
                this.zzl(zzbh2);
                break;
            }
            case 1: {
                final zzbn zze = this.zze();
                parcel2.writeNoException();
                zzavi.zzf(parcel2, (IInterface)zze);
                return true;
            }
        }
        parcel2.writeNoException();
        return true;
    }
}

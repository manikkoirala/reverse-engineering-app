// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

public final class zzel extends zzcv
{
    private final String zza;
    private final String zzb;
    
    public zzel(final String zza, final String zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public final String zze() {
        return this.zza;
    }
    
    public final String zzf() {
        return this.zzb;
    }
}

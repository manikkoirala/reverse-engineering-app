// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.ads.zzbus;
import com.google.android.gms.internal.ads.zzcbq;
import android.os.RemoteException;
import com.google.android.gms.internal.ads.zzbgl;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.ads.zzcbp;
import com.google.android.gms.internal.ads.zzcbr;
import com.google.android.gms.internal.ads.zzbgp;
import com.google.android.gms.internal.ads.zzbcu;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.dynamic.ObjectWrapper;
import android.content.Context;
import android.widget.FrameLayout;

final class zzas extends zzax
{
    final FrameLayout zza;
    final FrameLayout zzb;
    final Context zzc;
    final zzaw zzd;
    
    public zzas(final zzaw zzd, final FrameLayout zza, final FrameLayout zzb, final Context zzc) {
        this.zzd = zzd;
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
}

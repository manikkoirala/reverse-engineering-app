// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import com.google.android.gms.internal.ads.zzami;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.internal.ads.zzane;
import com.google.android.gms.internal.ads.zzanf;
import com.google.android.gms.internal.ads.zzcbm;
import com.google.android.gms.internal.ads.zzana;
import java.util.Map;
import com.google.android.gms.internal.ads.zzccf;
import com.google.android.gms.internal.ads.zzano;
import com.google.android.gms.internal.ads.zzaog;
import com.google.android.gms.ads.internal.client.zzba;
import com.google.android.gms.common.util.ClientLibraryUtils;
import com.google.android.gms.internal.ads.zzbdc;
import android.content.Context;
import com.google.android.gms.internal.ads.zzand;

public final class zzbq
{
    @Deprecated
    public static final zzbl zza;
    private static zzand zzb;
    private static final Object zzc;
    
    static {
        zzc = new Object();
        zza = new zzbi();
    }
    
    public zzbq(final Context context) {
        Context applicationContext = context;
        if (context.getApplicationContext() != null) {
            applicationContext = context.getApplicationContext();
        }
        synchronized (zzbq.zzc) {
            if (zzbq.zzb == null) {
                zzbdc.zza(applicationContext);
                zzand zzb;
                if (!ClientLibraryUtils.isPackageSide() && (boolean)zzba.zzc().zza(zzbdc.zzeo)) {
                    zzb = zzaz.zzb(applicationContext);
                }
                else {
                    zzb = zzaog.zza(applicationContext, (zzano)null);
                }
                zzbq.zzb = zzb;
            }
        }
    }
    
    public final ik0 zza(final String s) {
        final zzccf zzccf = new zzccf();
        zzbq.zzb.zza((zzana)new zzbp(s, null, zzccf));
        return (ik0)zzccf;
    }
    
    public final ik0 zzb(final int n, final String s, Map zzbk, final byte[] array) {
        final zzbn zzbn = new zzbn(null);
        final zzbj zzbj = new zzbj(this, s, zzbn);
        final zzcbm zzcbm = new zzcbm((String)null);
        zzbk = new zzbk(this, n, s, (zzanf)zzbn, (zzane)zzbj, array, (Map)zzbk, zzcbm);
        if (com.google.android.gms.internal.ads.zzcbm.zzk()) {
            try {
                zzcbm.zzd(s, "GET", ((zzana)zzbk).zzl(), ((zzana)zzbk).zzx());
            }
            catch (final zzami zzami) {
                zzcbn.zzj(((Throwable)zzami).getMessage());
            }
        }
        zzbq.zzb.zza((zzana)zzbk);
        return (ik0)zzbn;
    }
}

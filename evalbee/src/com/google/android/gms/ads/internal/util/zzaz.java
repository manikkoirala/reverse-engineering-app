// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import com.google.android.gms.internal.ads.zzblz;
import com.google.android.gms.internal.ads.zzcbg;
import com.google.android.gms.ads.internal.client.zzay;
import java.util.regex.Pattern;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.ads.internal.client.zzba;
import com.google.android.gms.internal.ads.zzamw;
import com.google.android.gms.internal.ads.zzana;
import com.google.android.gms.internal.ads.zzamt;
import com.google.android.gms.internal.ads.zzamk;
import com.google.android.gms.internal.ads.zzaoa;
import com.google.android.gms.internal.ads.zzanw;
import java.io.File;
import com.google.android.gms.internal.ads.zzand;
import com.google.android.gms.internal.ads.zzano;
import android.content.Context;
import com.google.android.gms.internal.ads.zzanp;

public final class zzaz extends zzanp
{
    private final Context zzc;
    
    private zzaz(final Context zzc, final zzano zzano) {
        super(zzano);
        this.zzc = zzc;
    }
    
    public static zzand zzb(final Context context) {
        final zzand zzand = new zzand((zzamk)new zzanw(new File(context.getCacheDir(), "admob_volley"), 20971520), (zzamt)new zzaz(context, (zzano)new zzaoa()), 4);
        zzand.zzd();
        return zzand;
    }
    
    public final zzamw zza(final zzana zzana) {
        if (zzana.zza() == 0 && Pattern.matches((String)zzba.zzc().zza(zzbdc.zzep), zzana.zzk())) {
            final Context zzc = this.zzc;
            zzay.zzb();
            if (zzcbg.zzs(zzc, 13400000)) {
                final zzamw zza = new zzblz(this.zzc).zza(zzana);
                if (zza != null) {
                    zze.zza("Got gmscore asset response: ".concat(String.valueOf(zzana.zzk())));
                    return zza;
                }
                zze.zza("Failed to get gmscore asset response: ".concat(String.valueOf(zzana.zzk())));
            }
        }
        return super.zza(zzana);
    }
}

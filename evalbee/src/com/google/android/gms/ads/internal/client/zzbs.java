// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.Parcelable$Creator;
import android.os.Bundle;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.ads.zzbww;
import com.google.android.gms.internal.ads.zzbub;
import com.google.android.gms.internal.ads.zzbea;
import com.google.android.gms.internal.ads.zzbty;
import com.google.android.gms.internal.ads.zzaxm;
import android.os.Parcelable;
import android.os.Parcel;
import android.os.IInterface;
import com.google.android.gms.internal.ads.zzavi;
import android.os.IBinder;
import com.google.android.gms.internal.ads.zzavg;

public final class zzbs extends zzavg implements zzbu
{
    public zzbs(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IAdManager");
    }
    
    public final void zzA() {
        this.zzbi(11, this.zza());
    }
    
    public final void zzB() {
        this.zzbi(6, this.zza());
    }
    
    public final void zzC(final zzbe zzbe) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)zzbe);
        this.zzbi(20, zza);
    }
    
    public final void zzD(final zzbh zzbh) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)zzbh);
        this.zzbi(7, zza);
    }
    
    public final void zzE(final zzby zzby) {
        throw null;
    }
    
    public final void zzF(final zzq zzq) {
        final Parcel zza = this.zza();
        zzavi.zzd(zza, (Parcelable)zzq);
        this.zzbi(13, zza);
    }
    
    public final void zzG(final zzcb zzcb) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)zzcb);
        this.zzbi(8, zza);
    }
    
    public final void zzH(final zzaxm zzaxm) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)zzaxm);
        this.zzbi(40, zza);
    }
    
    public final void zzI(final zzw zzw) {
        final Parcel zza = this.zza();
        zzavi.zzd(zza, (Parcelable)zzw);
        this.zzbi(39, zza);
    }
    
    public final void zzJ(final zzci zzci) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)zzci);
        this.zzbi(45, zza);
    }
    
    public final void zzK(final zzdu zzdu) {
        throw null;
    }
    
    public final void zzL(final boolean b) {
        final Parcel zza = this.zza();
        final int zza2 = zzavi.zza;
        zza.writeInt((int)(b ? 1 : 0));
        this.zzbi(34, zza);
    }
    
    public final void zzM(final zzbty zzbty) {
        throw null;
    }
    
    public final void zzN(final boolean b) {
        final Parcel zza = this.zza();
        final int zza2 = zzavi.zza;
        zza.writeInt((int)(b ? 1 : 0));
        this.zzbi(22, zza);
    }
    
    public final void zzO(final zzbea zzbea) {
        throw null;
    }
    
    public final void zzP(final zzdg zzdg) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)zzdg);
        this.zzbi(42, zza);
    }
    
    public final void zzQ(final zzbub zzbub, final String s) {
        throw null;
    }
    
    public final void zzR(final String s) {
        throw null;
    }
    
    public final void zzS(final zzbww zzbww) {
        throw null;
    }
    
    public final void zzT(final String s) {
        throw null;
    }
    
    public final void zzU(final zzfl zzfl) {
        final Parcel zza = this.zza();
        zzavi.zzd(zza, (Parcelable)zzfl);
        this.zzbi(29, zza);
    }
    
    public final void zzW(final IObjectWrapper objectWrapper) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)objectWrapper);
        this.zzbi(44, zza);
    }
    
    public final void zzX() {
        throw null;
    }
    
    public final boolean zzY() {
        final Parcel zzbh = this.zzbh(23, this.zza());
        final boolean zzg = zzavi.zzg(zzbh);
        zzbh.recycle();
        return zzg;
    }
    
    public final boolean zzZ() {
        throw null;
    }
    
    public final boolean zzaa(final zzl zzl) {
        final Parcel zza = this.zza();
        zzavi.zzd(zza, (Parcelable)zzl);
        final Parcel zzbh = this.zzbh(4, zza);
        final boolean zzg = zzavi.zzg(zzbh);
        zzbh.recycle();
        return zzg;
    }
    
    public final void zzab(final zzcf zzcf) {
        throw null;
    }
    
    public final Bundle zzd() {
        throw null;
    }
    
    public final zzq zzg() {
        final Parcel zzbh = this.zzbh(12, this.zza());
        final zzq zzq = (zzq)zzavi.zza(zzbh, (Parcelable$Creator)com.google.android.gms.ads.internal.client.zzq.CREATOR);
        zzbh.recycle();
        return zzq;
    }
    
    public final zzbh zzi() {
        final Parcel zzbh = this.zzbh(33, this.zza());
        final IBinder strongBinder = zzbh.readStrongBinder();
        zzbh zzbh2;
        if (strongBinder == null) {
            zzbh2 = null;
        }
        else {
            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdListener");
            if (queryLocalInterface instanceof zzbh) {
                zzbh2 = (zzbh)queryLocalInterface;
            }
            else {
                zzbh2 = new zzbf(strongBinder);
            }
        }
        zzbh.recycle();
        return zzbh2;
    }
    
    public final zzcb zzj() {
        final Parcel zzbh = this.zzbh(32, this.zza());
        final IBinder strongBinder = zzbh.readStrongBinder();
        zzcb zzcb;
        if (strongBinder == null) {
            zzcb = null;
        }
        else {
            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAppEventListener");
            if (queryLocalInterface instanceof zzcb) {
                zzcb = (zzcb)queryLocalInterface;
            }
            else {
                zzcb = new zzbz(strongBinder);
            }
        }
        zzbh.recycle();
        return zzcb;
    }
    
    public final zzdn zzk() {
        final Parcel zzbh = this.zzbh(41, this.zza());
        final IBinder strongBinder = zzbh.readStrongBinder();
        zzdn zzdn;
        if (strongBinder == null) {
            zzdn = null;
        }
        else {
            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IResponseInfo");
            if (queryLocalInterface instanceof zzdn) {
                zzdn = (zzdn)queryLocalInterface;
            }
            else {
                zzdn = new zzdl(strongBinder);
            }
        }
        zzbh.recycle();
        return zzdn;
    }
    
    public final zzdq zzl() {
        final Parcel zzbh = this.zzbh(26, this.zza());
        final IBinder strongBinder = zzbh.readStrongBinder();
        zzdq zzdq;
        if (strongBinder == null) {
            zzdq = null;
        }
        else {
            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IVideoController");
            if (queryLocalInterface instanceof zzdq) {
                zzdq = (zzdq)queryLocalInterface;
            }
            else {
                zzdq = new zzdo(strongBinder);
            }
        }
        zzbh.recycle();
        return zzdq;
    }
    
    public final IObjectWrapper zzn() {
        final Parcel zzbh = this.zzbh(1, this.zza());
        final IObjectWrapper interface1 = IObjectWrapper.Stub.asInterface(zzbh.readStrongBinder());
        zzbh.recycle();
        return interface1;
    }
    
    public final String zzr() {
        final Parcel zzbh = this.zzbh(31, this.zza());
        final String string = zzbh.readString();
        zzbh.recycle();
        return string;
    }
    
    public final String zzs() {
        throw null;
    }
    
    public final String zzt() {
        throw null;
    }
    
    public final void zzx() {
        this.zzbi(2, this.zza());
    }
    
    public final void zzy(final zzl zzl, final zzbk zzbk) {
        final Parcel zza = this.zza();
        zzavi.zzd(zza, (Parcelable)zzl);
        zzavi.zzf(zza, (IInterface)zzbk);
        this.zzbi(43, zza);
    }
    
    public final void zzz() {
        this.zzbi(5, this.zza());
    }
}

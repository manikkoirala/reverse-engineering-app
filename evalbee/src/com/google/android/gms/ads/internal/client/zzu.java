// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import android.os.Bundle;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "AdapterResponseInfoParcelCreator")
public final class zzu extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zzu> CREATOR;
    @Field(id = 1)
    public final String zza;
    @Field(id = 2)
    public long zzb;
    @Field(id = 3)
    public zze zzc;
    @Field(id = 4)
    public final Bundle zzd;
    @Field(id = 5)
    public final String zze;
    @Field(id = 6)
    public final String zzf;
    @Field(id = 7)
    public final String zzg;
    @Field(id = 8)
    public final String zzh;
    
    static {
        CREATOR = (Parcelable$Creator)new zzv();
    }
    
    @Constructor
    public zzu(@Param(id = 1) final String zza, @Param(id = 2) final long zzb, @Param(id = 3) final zze zzc, @Param(id = 4) final Bundle zzd, @Param(id = 5) final String zze, @Param(id = 6) final String zzf, @Param(id = 7) final String zzg, @Param(id = 8) final String zzh) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
        this.zzg = zzg;
        this.zzh = zzh;
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final String zza = this.zza;
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, zza, false);
        SafeParcelWriter.writeLong(parcel, 2, this.zzb);
        SafeParcelWriter.writeParcelable(parcel, 3, (Parcelable)this.zzc, n, false);
        SafeParcelWriter.writeBundle(parcel, 4, this.zzd, false);
        SafeParcelWriter.writeString(parcel, 5, this.zze, false);
        SafeParcelWriter.writeString(parcel, 6, this.zzf, false);
        SafeParcelWriter.writeString(parcel, 7, this.zzg, false);
        SafeParcelWriter.writeString(parcel, 8, this.zzh, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.ads.zzbus;
import com.google.android.gms.internal.ads.zzcbq;
import android.os.RemoteException;
import com.google.android.gms.internal.ads.zzbto;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.ads.zzcbp;
import com.google.android.gms.internal.ads.zzcbr;
import com.google.android.gms.internal.ads.zzbts;
import com.google.android.gms.internal.ads.zzbcu;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.dynamic.ObjectWrapper;
import android.content.Context;
import android.app.Activity;

final class zzaa extends zzax
{
    final Activity zza;
    final zzaw zzb;
    
    public zzaa(final zzaw zzb, final Activity zza) {
        this.zzb = zzb;
        this.zza = zza;
    }
}

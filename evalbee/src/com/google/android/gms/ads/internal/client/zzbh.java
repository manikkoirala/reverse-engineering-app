// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.IInterface;

public interface zzbh extends IInterface
{
    void zzc();
    
    void zzd();
    
    void zze(final int p0);
    
    void zzf(final zze p0);
    
    void zzg();
    
    void zzh();
    
    void zzi();
    
    void zzj();
    
    void zzk();
}

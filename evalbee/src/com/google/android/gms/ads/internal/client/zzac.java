// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.ads.zzbus;
import com.google.android.gms.internal.ads.zzcbq;
import android.os.RemoteException;
import com.google.android.gms.internal.ads.zzcbp;
import com.google.android.gms.internal.ads.zzcbr;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.internal.ads.zzbpr;
import android.content.Context;

final class zzac extends zzax
{
    final Context zza;
    final zzbpr zzb;
    
    public zzac(final zzaw zzaw, final Context zza, final zzbpr zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
}

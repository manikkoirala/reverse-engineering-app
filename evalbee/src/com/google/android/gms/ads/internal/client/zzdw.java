// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.BaseBundle;
import com.google.android.gms.ads.mediation.NetworkExtras;
import com.google.android.gms.common.internal.Preconditions;
import java.util.Iterator;
import com.google.android.gms.internal.ads.zzcbn;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.HashMap;
import android.os.Bundle;
import java.util.HashSet;

public final class zzdw
{
    private final HashSet zza;
    private final Bundle zzb;
    private final HashMap zzc;
    private final HashSet zzd;
    private final Bundle zze;
    private final HashSet zzf;
    private Date zzg;
    private String zzh;
    private final List zzi;
    private int zzj;
    private String zzk;
    private String zzl;
    private int zzm;
    private boolean zzn;
    private String zzo;
    private int zzp;
    
    public zzdw() {
        this.zza = new HashSet();
        this.zzb = new Bundle();
        this.zzc = new HashMap();
        this.zzd = new HashSet();
        this.zze = new Bundle();
        this.zzf = new HashSet();
        this.zzi = new ArrayList();
        this.zzj = -1;
        this.zzm = -1;
        this.zzp = 60000;
    }
    
    @Deprecated
    public final void zzA(final int zzj) {
        this.zzj = zzj;
    }
    
    public final void zzB(final int zzp) {
        this.zzp = zzp;
    }
    
    @Deprecated
    public final void zzC(final boolean zzn) {
        this.zzn = zzn;
    }
    
    public final void zzD(final List list) {
        this.zzi.clear();
        for (final String s : list) {
            if (TextUtils.isEmpty((CharSequence)s)) {
                zzcbn.zzj("neighboring content URL should not be null or empty");
            }
            else {
                this.zzi.add(s);
            }
        }
    }
    
    public final void zzE(final String zzk) {
        this.zzk = zzk;
    }
    
    public final void zzF(final String zzl) {
        this.zzl = zzl;
    }
    
    @Deprecated
    public final void zzG(final boolean zzm) {
        this.zzm = (zzm ? 1 : 0);
    }
    
    public final void zzp(final String e) {
        this.zzf.add(e);
    }
    
    public final void zzq(final Class clazz, final Bundle bundle) {
        if (this.zzb.getBundle("com.google.android.gms.ads.mediation.customevent.CustomEventAdapter") == null) {
            this.zzb.putBundle("com.google.android.gms.ads.mediation.customevent.CustomEventAdapter", new Bundle());
        }
        final Bundle bundle2 = this.zzb.getBundle("com.google.android.gms.ads.mediation.customevent.CustomEventAdapter");
        Preconditions.checkNotNull(bundle2);
        bundle2.putBundle(clazz.getName(), bundle);
    }
    
    public final void zzr(final String s, final String s2) {
        ((BaseBundle)this.zze).putString(s, s2);
    }
    
    public final void zzs(final String e) {
        this.zza.add(e);
    }
    
    public final void zzt(final Class clazz, final Bundle bundle) {
        this.zzb.putBundle(clazz.getName(), bundle);
    }
    
    @Deprecated
    public final void zzu(final NetworkExtras value) {
        this.zzc.put(value.getClass(), value);
    }
    
    public final void zzv(final String e) {
        this.zzd.add(e);
    }
    
    public final void zzw(final String s) {
        this.zzd.remove("B3EEABB8EE11C2BE770B684D95219ECB");
    }
    
    public final void zzx(final String zzo) {
        this.zzo = zzo;
    }
    
    @Deprecated
    public final void zzy(final Date zzg) {
        this.zzg = zzg;
    }
    
    public final void zzz(final String zzh) {
        this.zzh = zzh;
    }
}

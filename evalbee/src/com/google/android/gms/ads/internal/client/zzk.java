// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.internal.ads.zzbus;
import com.google.android.gms.internal.ads.zzcbq;
import android.os.RemoteException;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.internal.ads.zzcbp;
import com.google.android.gms.internal.ads.zzcbr;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.internal.ads.zzbpr;
import android.content.Context;
import android.os.IInterface;
import android.os.IBinder;
import com.google.android.gms.internal.ads.zzbuu;
import com.google.android.gms.dynamic.RemoteCreator;

public final class zzk extends RemoteCreator
{
    private zzbuu zza;
    
    public zzk() {
        super("com.google.android.gms.ads.AdManagerCreatorImpl");
    }
    
    public final zzbu zza(Context zze, zzq queryLocalInterface, final String s, final zzbpr zzbpr, final int n) {
        zzbdc.zza((Context)zze);
        final boolean booleanValue = (boolean)zzba.zzc().zza(zzbdc.zzjX);
        final RemoteCreatorException ex = null;
        if (booleanValue) {
            try {
                final IBinder zze2 = ((zzbv)zzcbr.zzb((Context)zze, "com.google.android.gms.ads.ChimeraAdManagerCreatorImpl", (zzcbp)zzj.zza)).zze(ObjectWrapper.wrap(zze), (zzq)queryLocalInterface, s, zzbpr, 234310000, n);
                if (zze2 == null) {
                    zze = ex;
                    return (zzbu)zze;
                }
                queryLocalInterface = (NullPointerException)zze2.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdManager");
                if (queryLocalInterface instanceof zzbu) {
                    queryLocalInterface = (NullPointerException)(zze = (RemoteCreatorException)queryLocalInterface);
                    return (zzbu)zze;
                }
                queryLocalInterface = (NullPointerException)(zze = (RemoteCreatorException)new zzbs(zze2));
                return (zzbu)zze;
            }
            catch (final NullPointerException queryLocalInterface) {}
            catch (final RemoteException queryLocalInterface) {}
            catch (final zzcbq zzcbq) {}
            (this.zza = zzbus.zza((Context)zze)).zzf((Throwable)queryLocalInterface, "AdManagerCreator.newAdManagerByDynamiteLoader");
            zzcbn.zzl("#007 Could not call remote method.", (Throwable)queryLocalInterface);
            zze = ex;
            return (zzbu)zze;
        }
        try {
            zze = (RemoteCreatorException)this.getRemoteCreatorInstance((Context)zze).zze(ObjectWrapper.wrap(zze), (zzq)queryLocalInterface, s, zzbpr, 234310000, n);
            if (zze == null) {
                zze = ex;
            }
            else {
                final IInterface queryLocalInterface2 = ((IBinder)zze).queryLocalInterface("com.google.android.gms.ads.internal.client.IAdManager");
                if (queryLocalInterface2 instanceof zzbu) {
                    zze = (RemoteCreatorException)queryLocalInterface2;
                }
                else {
                    zze = (RemoteCreatorException)new zzbs((IBinder)zze);
                }
            }
            return (zzbu)zze;
        }
        catch (final RemoteCreatorException zze) {}
        catch (final RemoteException ex2) {}
        zzcbn.zzf("Could not create remote AdManager.", (Throwable)zze);
        return null;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.ads.zzcbn;
import android.os.RemoteException;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.internal.ads.zzbpr;
import android.content.Context;
import android.os.IInterface;
import android.os.IBinder;
import com.google.android.gms.dynamic.RemoteCreator;

public final class zzi extends RemoteCreator
{
    public zzi() {
        super("com.google.android.gms.ads.AdLoaderBuilderCreatorImpl");
    }
    
    public final zzbq zza(Context zze, final String s, final zzbpr zzbpr) {
        final RemoteCreatorException ex = null;
        try {
            zze = (RemoteCreatorException)this.getRemoteCreatorInstance((Context)zze).zze(ObjectWrapper.wrap(zze), s, zzbpr, 234310000);
            if (zze == null) {
                zze = ex;
            }
            else {
                final IInterface queryLocalInterface = ((IBinder)zze).queryLocalInterface("com.google.android.gms.ads.internal.client.IAdLoaderBuilder");
                if (queryLocalInterface instanceof zzbq) {
                    zze = (RemoteCreatorException)queryLocalInterface;
                }
                else {
                    zze = (RemoteCreatorException)new zzbo((IBinder)zze);
                }
            }
            return (zzbq)zze;
        }
        catch (final RemoteCreatorException zze) {}
        catch (final RemoteException ex2) {}
        zzcbn.zzk("Could not create remote builder for AdLoader.", (Throwable)zze);
        return null;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.overlay;

import com.google.android.gms.internal.ads.zzdge;
import android.app.Activity;
import com.google.android.gms.common.util.PlatformVersion;
import android.os.Parcelable;
import android.os.Bundle;
import android.content.Intent;
import com.google.android.gms.ads.internal.zzt;
import android.content.Context;

public final class zzm
{
    public static final void zza(final Context context, final AdOverlayInfoParcel adOverlayInfoParcel, final boolean b) {
        if (adOverlayInfoParcel.zzk == 4 && adOverlayInfoParcel.zzc == null) {
            final com.google.android.gms.ads.internal.client.zza zzb = adOverlayInfoParcel.zzb;
            if (zzb != null) {
                zzb.onAdClicked();
            }
            final zzdge zzu = adOverlayInfoParcel.zzu;
            if (zzu != null) {
                zzu.zzbL();
            }
            final Activity zzi = adOverlayInfoParcel.zzd.zzi();
            final zzc zza = adOverlayInfoParcel.zza;
            Object o = context;
            if (zza != null) {
                o = context;
                if (zza.zzj) {
                    o = context;
                    if (zzi != null) {
                        o = zzi;
                    }
                }
            }
            zzt.zzh();
            final zzc zza2 = adOverlayInfoParcel.zza;
            final zzz zzi2 = adOverlayInfoParcel.zzi;
            zzx zzi3;
            if (zza2 != null) {
                zzi3 = zza2.zzi;
            }
            else {
                zzi3 = null;
            }
            com.google.android.gms.ads.internal.overlay.zza.zzb((Context)o, zza2, zzi2, zzi3);
            return;
        }
        final Intent intent = new Intent();
        intent.setClassName(context, "com.google.android.gms.ads.AdActivity");
        intent.putExtra("com.google.android.gms.ads.internal.overlay.useClientJar", adOverlayInfoParcel.zzm.zzd);
        intent.putExtra("shouldCallOnOverlayOpened", b);
        final Bundle bundle = new Bundle(1);
        bundle.putParcelable("com.google.android.gms.ads.inernal.overlay.AdOverlayInfo", (Parcelable)adOverlayInfoParcel);
        intent.putExtra("com.google.android.gms.ads.inernal.overlay.AdOverlayInfo", bundle);
        if (!PlatformVersion.isAtLeastLollipop()) {
            intent.addFlags(524288);
        }
        if (!(context instanceof Activity)) {
            intent.addFlags(268435456);
        }
        zzt.zzp();
        com.google.android.gms.ads.internal.util.zzt.zzS(context, intent);
    }
}

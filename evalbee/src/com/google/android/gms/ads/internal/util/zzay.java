// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import android.os.Handler;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import com.google.android.gms.ads.internal.client.zzda;
import com.google.android.gms.internal.ads.zzdwm;
import java.io.FileOutputStream;
import android.net.Uri$Builder;
import java.util.UUID;
import java.io.IOException;
import java.io.InputStream;
import com.google.android.gms.common.util.IOUtils;
import android.text.TextUtils;
import android.net.Uri;
import java.util.concurrent.TimeoutException;
import com.google.android.gms.internal.ads.zzcbn;
import java.util.concurrent.TimeUnit;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.ads.internal.client.zzba;
import java.util.Map;
import com.google.android.gms.ads.internal.zzt;
import java.util.HashMap;
import android.content.Context;
import com.google.android.gms.internal.ads.zzdwn;

public final class zzay
{
    protected String zza;
    private final Object zzb;
    private String zzc;
    private String zzd;
    private boolean zze;
    private boolean zzf;
    private zzdwn zzg;
    
    public zzay() {
        this.zzb = new Object();
        this.zzc = "";
        this.zzd = "";
        this.zze = false;
        this.zzf = false;
        this.zza = "";
    }
    
    public static final String zzo(final Context context, String s, String s2) {
        final HashMap hashMap = new HashMap();
        hashMap.put("User-Agent", zzt.zzp().zzc(context, s2));
        final ik0 zzb = new zzbq(context).zzb(0, s, hashMap, null);
        try {
            return (String)zzb.get((int)zzba.zzc().zza(zzbdc.zzeF), TimeUnit.MILLISECONDS);
        }
        catch (final Exception ex) {
            zzcbn.zzh("Error retrieving a response from: ".concat(String.valueOf(s)), (Throwable)ex);
            return null;
        }
        catch (final InterruptedException ex2) {
            s = String.valueOf(s);
            s2 = "Interrupted while retrieving a response from: ";
        }
        catch (final TimeoutException ex2) {
            s = String.valueOf(s);
            s2 = "Timeout while retrieving a response from: ";
        }
        final InterruptedException ex2;
        zzcbn.zzh(s2.concat(s), (Throwable)ex2);
        zzb.cancel(true);
        return null;
    }
    
    private final Uri zzp(final Context context, String zzc, final String s, final String s2) {
        final Uri$Builder buildUpon = Uri.parse(zzc).buildUpon();
        synchronized (this.zzb) {
            if (TextUtils.isEmpty((CharSequence)this.zzc)) {
                zzt.zzp();
                try {
                    zzc = new String(IOUtils.readInputStreamFully(context.openFileInput("debug_signals_id.txt"), true), "UTF-8");
                }
                catch (final IOException ex) {
                    zzcbn.zze("Error reading from internal storage.");
                    zzc = "";
                }
                this.zzc = zzc;
                if (TextUtils.isEmpty((CharSequence)zzc)) {
                    zzt.zzp();
                    this.zzc = UUID.randomUUID().toString();
                    zzt.zzp();
                    zzc = this.zzc;
                    try {
                        final FileOutputStream openFileOutput = context.openFileOutput("debug_signals_id.txt", 0);
                        openFileOutput.write(zzc.getBytes("UTF-8"));
                        openFileOutput.close();
                    }
                    catch (final Exception ex2) {
                        zzcbn.zzh("Error writing to file in internal storage.", (Throwable)ex2);
                    }
                }
            }
            final String zzc2 = this.zzc;
            monitorexit(this.zzb);
            buildUpon.appendQueryParameter("linkedDeviceId", zzc2);
            buildUpon.appendQueryParameter("adSlotPath", s);
            buildUpon.appendQueryParameter("afmaVersion", s2);
            return buildUpon.build();
        }
    }
    
    public final zzdwn zza() {
        return this.zzg;
    }
    
    public final String zzb() {
        synchronized (this.zzb) {
            return this.zzd;
        }
    }
    
    public final void zzc(final Context context) {
        if (zzba.zzc().zza(zzbdc.zziY)) {
            final zzdwn zzg = this.zzg;
            if (zzg != null) {
                zzg.zzh((zzda)new zzav(this, context), zzdwm.zzd);
            }
        }
    }
    
    public final void zzd(final Context context, final String s, final String s2) {
        zzt.zzp();
        com.google.android.gms.ads.internal.util.zzt.zzT(context, this.zzp(context, (String)zzba.zzc().zza(zzbdc.zzeB), s, s2));
    }
    
    public final void zze(final Context context, final String s, final String s2, final String s3) {
        final Uri$Builder buildUpon = this.zzp(context, (String)zzba.zzc().zza(zzbdc.zzeE), s3, s).buildUpon();
        buildUpon.appendQueryParameter("debugData", s2);
        zzt.zzp();
        com.google.android.gms.ads.internal.util.zzt.zzK(context, s, buildUpon.build().toString());
    }
    
    public final void zzf(final boolean zzf) {
        synchronized (this.zzb) {
            this.zzf = zzf;
            if (zzba.zzc().zza(zzbdc.zziY)) {
                zzt.zzo().zzi().zzB(zzf);
                final zzdwn zzg = this.zzg;
                if (zzg != null) {
                    zzg.zzk(zzf);
                }
            }
        }
    }
    
    public final void zzg(final zzdwn zzg) {
        this.zzg = zzg;
    }
    
    public final void zzh(final boolean zze) {
        synchronized (this.zzb) {
            this.zze = zze;
        }
    }
    
    public final void zzi(final Context context, final String s, final boolean b, final boolean b2) {
        if (!(context instanceof Activity)) {
            zzcbn.zzi("Can not create dialog without Activity Context");
            return;
        }
        ((Handler)com.google.android.gms.ads.internal.util.zzt.zza).post((Runnable)new zzax(this, context, s, b, b2));
    }
    
    public final boolean zzj(final Context context, String s, String trim) {
        final String zzo = zzo(context, this.zzp(context, (String)zzba.zzc().zza(zzbdc.zzeD), s, trim).toString(), trim);
        if (TextUtils.isEmpty((CharSequence)zzo)) {
            zzcbn.zze("Not linked for debug signals.");
            return false;
        }
        trim = zzo.trim();
        try {
            final boolean equals = "1".equals(new JSONObject(trim).optString("debug_mode"));
            this.zzf(equals);
            if (zzba.zzc().zza(zzbdc.zziY)) {
                final zzg zzi = zzt.zzo().zzi();
                if (!equals) {
                    s = "";
                }
                zzi.zzA(s);
            }
            return equals;
        }
        catch (final JSONException ex) {
            zzcbn.zzk("Fail to get debug mode response json.", (Throwable)ex);
            return false;
        }
    }
    
    public final boolean zzk(final Context context, String s, final String s2) {
        final String zzo = zzo(context, this.zzp(context, (String)zzba.zzc().zza(zzbdc.zzeC), s, s2).toString(), s2);
        if (TextUtils.isEmpty((CharSequence)zzo)) {
            zzcbn.zze("Not linked for in app preview.");
            return false;
        }
        final String trim = zzo.trim();
        try {
            final JSONObject jsonObject = new JSONObject(trim);
            final String optString = jsonObject.optString("gct");
            this.zza = jsonObject.optString("status");
            if (zzba.zzc().zza(zzbdc.zziY)) {
                final boolean b = "0".equals(this.zza) || "2".equals(this.zza);
                this.zzf(b);
                final zzg zzi = zzt.zzo().zzi();
                if (!b) {
                    s = "";
                }
                zzi.zzA(s);
            }
            synchronized (this.zzb) {
                this.zzd = optString;
                return true;
            }
        }
        catch (final JSONException ex) {
            zzcbn.zzk("Fail to get in app preview response json.", (Throwable)ex);
            return false;
        }
    }
    
    public final boolean zzl() {
        synchronized (this.zzb) {
            return this.zzf;
        }
    }
    
    public final boolean zzm() {
        synchronized (this.zzb) {
            return this.zze;
        }
    }
    
    public final boolean zzn(final Context context, final String s, final String s2, final String s3) {
        if (!TextUtils.isEmpty((CharSequence)s2) && this.zzm()) {
            zzcbn.zze("Sending troubleshooting signals to the server.");
            this.zze(context, s, s2, s3);
            return true;
        }
        return false;
    }
}

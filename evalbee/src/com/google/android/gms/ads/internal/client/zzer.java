// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import com.google.android.gms.internal.ads.zzcbn;

final class zzer implements Runnable
{
    final zzet zza;
    
    public zzer(final zzet zza) {
        this.zza = zza;
    }
    
    @Override
    public final void run() {
        final zzeu zza = this.zza.zza;
        if (zzeu.zzb(zza) != null) {
            try {
                zzeu.zzb(zza).zze(1);
            }
            catch (final RemoteException ex) {
                zzcbn.zzk("Could not notify onAdFailedToLoad event.", (Throwable)ex);
            }
        }
    }
}

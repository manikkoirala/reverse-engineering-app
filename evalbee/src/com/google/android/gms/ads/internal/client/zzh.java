// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.AdLoadCallback;

public final class zzh extends zzbj
{
    private final AdLoadCallback zza;
    private final Object zzb;
    
    public zzh(final AdLoadCallback zza, final Object zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public final void zzb(final zze zze) {
        final AdLoadCallback zza = this.zza;
        if (zza != null) {
            zza.onAdFailedToLoad(zze.zzb());
        }
    }
    
    public final void zzc() {
        final AdLoadCallback zza = this.zza;
        if (zza != null) {
            final Object zzb = this.zzb;
            if (zzb != null) {
                zza.onAdLoaded(zzb);
            }
        }
    }
}

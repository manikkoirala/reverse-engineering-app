// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.ads.zzcbn;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.TimeUnit;
import com.google.android.gms.internal.ads.zzasi;
import android.os.AsyncTask;

final class zzq extends AsyncTask
{
    final zzs zza;
    
    public final String zza(Void... zza) {
        try {
            zza = (TimeoutException)this.zza;
            zzs.zzv((zzs)zza, (zzasi)zzs.zzu((zzs)zza).get(1000L, TimeUnit.MILLISECONDS));
            return this.zza.zzp();
        }
        catch (final TimeoutException zza) {}
        catch (final ExecutionException zza) {}
        catch (final InterruptedException ex) {}
        zzcbn.zzk("", (Throwable)zza);
        return this.zza.zzp();
    }
}

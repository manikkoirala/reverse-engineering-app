// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import com.google.android.gms.internal.ads.zzcbm;
import com.google.android.gms.internal.ads.zzcbn;
import java.io.IOException;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import android.content.Context;

final class zzc extends zzb
{
    private final Context zza;
    
    public zzc(final Context zza) {
        this.zza = zza;
    }
    
    @Override
    public final void zza() {
        boolean isAdIdFakeForDebugLogging = false;
        Label_0032: {
            try {
                isAdIdFakeForDebugLogging = AdvertisingIdClient.getIsAdIdFakeForDebugLogging(this.zza);
                break Label_0032;
            }
            catch (final GooglePlayServicesRepairableException ex) {}
            catch (final GooglePlayServicesNotAvailableException ex) {}
            catch (final IllegalStateException ex) {}
            catch (final IOException ex2) {}
            final GooglePlayServicesRepairableException ex;
            zzcbn.zzh("Fail to get isAdIdFakeForDebugLogging", (Throwable)ex);
            isAdIdFakeForDebugLogging = false;
        }
        zzcbm.zzj(isAdIdFakeForDebugLogging);
        final StringBuilder sb = new StringBuilder();
        sb.append("Update ad debug logging enablement as ");
        sb.append(isAdIdFakeForDebugLogging);
        zzcbn.zzj(sb.toString());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.ads.zzcdo;
import com.google.android.gms.internal.ads.zzcet;
import com.google.android.gms.internal.ads.zzcdd;

public final class zza
{
    public final zzcdd zza;
    public final zzcet zzb;
    
    public zza(final zzcet zzb, final zzcdd zza) {
        this.zzb = zzb;
        this.zza = zza;
    }
    
    public static zza zza() {
        return new zza(new zzcet(), (zzcdd)new zzcdo());
    }
}

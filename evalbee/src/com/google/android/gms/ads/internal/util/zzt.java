// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import android.view.ViewGroup;
import android.os.BaseBundle;
import com.google.android.gms.internal.ads.zzhed;
import com.google.android.gms.internal.ads.zzbeb;
import com.google.android.gms.internal.ads.zzbed;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import com.google.android.gms.internal.ads.zzcbf;
import com.google.android.gms.internal.ads.zzcbg;
import android.content.SharedPreferences$OnSharedPreferenceChangeListener;
import android.content.SharedPreferences;
import java.net.HttpURLConnection;
import java.util.concurrent.Callable;
import com.google.android.gms.internal.ads.zzgbb;
import com.google.android.gms.internal.ads.zzbwo;
import android.os.IInterface;
import android.content.res.Resources;
import com.google.android.gms.ads.impl.R;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.util.concurrent.TimeUnit;
import android.webkit.WebResourceResponse;
import org.json.JSONArray;
import org.json.JSONException;
import java.util.HashSet;
import org.json.JSONObject;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.os.Build;
import android.os.Build$VERSION;
import android.os.IBinder;
import android.view.WindowManager$LayoutParams;
import com.google.android.gms.internal.ads.zzbcu;
import com.google.android.gms.internal.ads.zzful;
import com.google.android.gms.internal.ads.zzftk;
import java.util.regex.PatternSyntaxException;
import java.util.regex.Pattern;
import android.content.pm.PackageManager$NameNotFoundException;
import com.google.android.gms.common.wrappers.Wrappers;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import com.google.android.gms.ads.internal.client.zzay;
import java.util.HashMap;
import java.util.Map;
import android.net.Uri;
import java.io.InputStreamReader;
import com.google.android.gms.common.util.CrashUtils;
import com.google.android.gms.internal.ads.zzbfe;
import java.util.ArrayList;
import android.app.AlertDialog$Builder;
import com.google.android.gms.internal.ads.zzfdy;
import java.util.Locale;
import com.google.android.gms.internal.ads.zzfdu;
import com.google.android.gms.internal.ads.zzcgm;
import com.google.android.gms.internal.ads.zzchs;
import com.google.android.gms.ads.nativead.NativeAdView;
import com.google.android.gms.ads.formats.zzg;
import com.google.android.gms.internal.ads.zzdqx;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.graphics.Point;
import android.graphics.Rect;
import android.app.Activity;
import android.os.RemoteException;
import android.text.TextUtils;
import java.util.Iterator;
import java.util.List;
import android.os.PowerManager;
import android.app.ActivityManager$RunningAppProcessInfo;
import android.app.ActivityManager;
import android.os.Process;
import com.google.android.gms.internal.ads.zzcbn;
import android.app.KeyguardManager;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.ads.internal.client.zzba;
import com.google.android.gms.internal.ads.zzcbm;
import com.google.android.gms.common.util.DeviceProperties;
import android.content.Context;
import java.util.concurrent.Executors;
import android.os.Bundle;
import android.os.Looper;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import com.google.android.gms.internal.ads.zzfqv;

public final class zzt
{
    public static final zzfqv zza;
    private final AtomicReference zzb;
    private final AtomicReference zzc;
    private final AtomicReference zzd;
    private final AtomicBoolean zze;
    private boolean zzf;
    private final Object zzg;
    private String zzh;
    private boolean zzi;
    private boolean zzj;
    private final Executor zzk;
    
    static {
        zza = new zzf(Looper.getMainLooper());
    }
    
    public zzt() {
        this.zzb = new AtomicReference(null);
        this.zzc = new AtomicReference(null);
        this.zzd = new AtomicReference((V)new Bundle());
        this.zze = new AtomicBoolean();
        this.zzf = true;
        this.zzg = new Object();
        this.zzi = false;
        this.zzj = false;
        this.zzk = Executors.newSingleThreadExecutor();
    }
    
    public static final boolean zzA(final Context context) {
        boolean bstar;
        try {
            bstar = DeviceProperties.isBstar(context);
        }
        catch (final NoSuchMethodError noSuchMethodError) {
            bstar = false;
        }
        return bstar;
    }
    
    public static final boolean zzB(final String s) {
        if (!zzcbm.zzk()) {
            return false;
        }
        if (!(boolean)zzba.zzc().zza(zzbdc.zzeP)) {
            return false;
        }
        final String s2 = (String)zzba.zzc().zza(zzbdc.zzeR);
        if (!s2.isEmpty()) {
            final String[] split = s2.split(";");
            for (int length = split.length, i = 0; i < length; ++i) {
                if (split[i].equals(s)) {
                    return false;
                }
            }
        }
        final String s3 = (String)zzba.zzc().zza(zzbdc.zzeQ);
        if (s3.isEmpty()) {
            return true;
        }
        final String[] split2 = s3.split(";");
        for (int length2 = split2.length, j = 0; j < length2; ++j) {
            if (split2[j].equals(s)) {
                return true;
            }
        }
        return false;
    }
    
    public static final boolean zzC(final Context context) {
        if (context == null) {
            return false;
        }
        final KeyguardManager zzW = zzW(context);
        return zzW != null && zzW.isKeyguardLocked();
    }
    
    public static final boolean zzD(final Context context) {
        try {
            context.getClassLoader().loadClass("com.google.android.gms.ads.internal.ClientApi");
            return false;
        }
        catch (final ClassNotFoundException ex) {
            return true;
        }
        finally {
            final Throwable t;
            zzcbn.zzh("Error loading class.", t);
            com.google.android.gms.ads.internal.zzt.zzo().zzw(t, "AdUtil.isLiteSdk");
            return false;
        }
    }
    
    public static final boolean zzE() {
        final int myUid = Process.myUid();
        return myUid == 0 || myUid == 1000;
    }
    
    public static final boolean zzF(final Context context) {
        try {
            final ActivityManager activityManager = (ActivityManager)context.getSystemService("activity");
            final KeyguardManager keyguardManager = (KeyguardManager)context.getSystemService("keyguard");
            if (activityManager != null) {
                if (keyguardManager != null) {
                    final List runningAppProcesses = activityManager.getRunningAppProcesses();
                    if (runningAppProcesses == null) {
                        return false;
                    }
                    for (final ActivityManager$RunningAppProcessInfo activityManager$RunningAppProcessInfo : runningAppProcesses) {
                        if (Process.myPid() == activityManager$RunningAppProcessInfo.pid) {
                            if (activityManager$RunningAppProcessInfo.importance != 100 || keyguardManager.inKeyguardRestrictedInputMode()) {
                                break;
                            }
                            final PowerManager powerManager = (PowerManager)context.getSystemService("power");
                            if (powerManager == null) {
                                break;
                            }
                            if (powerManager.isScreenOn()) {
                                return false;
                            }
                            break;
                        }
                    }
                    return true;
                }
            }
            return false;
        }
        finally {
            return false;
        }
    }
    
    public static final boolean zzG(final Context context) {
        try {
            final Bundle zzX = zzX(context);
            final String string = ((BaseBundle)zzX).getString("com.google.android.gms.ads.INTEGRATION_MANAGER");
            if (TextUtils.isEmpty((CharSequence)zzY(zzX)) && !TextUtils.isEmpty((CharSequence)string)) {
                return true;
            }
            return false;
        }
        catch (final RemoteException ex) {
            return false;
        }
    }
    
    public static final boolean zzH(final Context context) {
        if (!(context instanceof Activity)) {
            return false;
        }
        final Window window = ((Activity)context).getWindow();
        if (window != null) {
            if (window.getDecorView() != null) {
                final Rect rect = new Rect();
                final Rect rect2 = new Rect();
                window.getDecorView().getGlobalVisibleRect(rect, (Point)null);
                window.getDecorView().getWindowVisibleDisplayFrame(rect2);
                if (rect.bottom != 0 && rect2.bottom != 0 && rect.top == rect2.top) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public static final void zzI(final View view, final int i, final MotionEvent motionEvent) {
        final int[] array = new int[2];
        final Rect rect = new Rect();
        try {
            final String packageName = view.getContext().getPackageName();
            View child = view;
            if (view instanceof zzdqx) {
                child = ((ViewGroup)view).getChildAt(0);
            }
            String s;
            boolean b;
            if (!(child instanceof zzg) && !(child instanceof NativeAdView)) {
                s = "UNKNOWN";
                b = false;
            }
            else {
                s = "NATIVE";
                b = true;
            }
            int width;
            int height;
            if (child.getLocalVisibleRect(rect)) {
                width = rect.width();
                height = rect.height();
            }
            else {
                height = 0;
                width = 0;
            }
            com.google.android.gms.ads.internal.zzt.zzp();
            final long zzv = zzv(child);
            child.getLocationOnScreen(array);
            final int j = array[0];
            final int k = array[1];
            final boolean b2 = child instanceof zzchs;
            final String s2 = "none";
            String zzb = null;
            Label_0240: {
                if (b2) {
                    final zzfdy zzP = ((zzchs)child).zzP();
                    if (zzP != null) {
                        zzb = zzP.zzb;
                        final int hashCode = child.hashCode();
                        final StringBuilder sb = new StringBuilder();
                        sb.append(zzb);
                        sb.append(":");
                        sb.append(hashCode);
                        child.setContentDescription((CharSequence)sb.toString());
                        break Label_0240;
                    }
                }
                zzb = "none";
            }
            String zza = s;
            int zzf = b ? 1 : 0;
            String zzF = s2;
            if (child instanceof zzcgm) {
                final zzfdu zzD = ((zzcgm)child).zzD();
                zza = s;
                zzf = (b ? 1 : 0);
                zzF = s2;
                if (zzD != null) {
                    zza = zzfdu.zza(zzD.zzb);
                    zzf = zzD.zzf;
                    zzF = zzD.zzF;
                }
            }
            zzcbn.zzi(String.format(Locale.US, "<Ad hashCode=%d, package=%s, adNetCls=%s, gwsQueryId=%s, format=%s, impType=%d, class=%s, x=%d, y=%d, width=%d, height=%d, vWidth=%d, vHeight=%d, alpha=%d, state=%s>", child.hashCode(), packageName, zzF, zzb, zza, zzf, ((zzcgm)child).getClass().getName(), j, k, child.getWidth(), child.getHeight(), width, height, zzv, Integer.toString(i, 2)));
        }
        catch (final Exception ex) {
            zzcbn.zzh("Failure getting view location.", (Throwable)ex);
        }
    }
    
    public static final AlertDialog$Builder zzJ(final Context context) {
        return new AlertDialog$Builder(context, com.google.android.gms.ads.internal.zzt.zzq().zza());
    }
    
    public static final void zzK(final Context context, final String s, final String s2) {
        final ArrayList list = new ArrayList();
        list.add(s2);
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            new zzca(context, s, (String)iterator.next()).zzb();
        }
    }
    
    public static final void zzL(final Context context, final Throwable t) {
        if (context == null) {
            return;
        }
        try {
            if (zzbfe.zzb.zze()) {
                CrashUtils.addDynamiteErrorToDropBox(context, t);
            }
        }
        catch (final IllegalStateException ex) {}
    }
    
    public static final String zzM(final InputStreamReader inputStreamReader) {
        final StringBuilder sb = new StringBuilder(8192);
        final char[] array = new char[2048];
        while (true) {
            final int read = inputStreamReader.read(array);
            if (read == -1) {
                break;
            }
            sb.append(array, 0, read);
        }
        return sb.toString();
    }
    
    public static final int zzN(final String s) {
        try {
            return Integer.parseInt(s);
        }
        catch (final NumberFormatException ex) {
            zzcbn.zzj("Could not parse value:".concat(ex.toString()));
            return 0;
        }
    }
    
    public static final Map zzO(final Uri uri) {
        if (uri == null) {
            return null;
        }
        final HashMap hashMap = new HashMap();
        for (final String key : uri.getQueryParameterNames()) {
            if (!TextUtils.isEmpty((CharSequence)key)) {
                hashMap.put(key, uri.getQueryParameter(key));
            }
        }
        return hashMap;
    }
    
    public static final int[] zzP(final Activity activity) {
        final Window window = activity.getWindow();
        if (window != null) {
            final View viewById = window.findViewById(16908290);
            if (viewById != null) {
                return new int[] { viewById.getWidth(), viewById.getHeight() };
            }
        }
        return zzt();
    }
    
    public static final int[] zzQ(final Activity activity) {
        final Window window = activity.getWindow();
        if (window != null) {
            final View viewById = window.findViewById(16908290);
            if (viewById != null) {
                final int[] zzt = { viewById.getTop(), viewById.getBottom() };
                return new int[] { zzay.zzb().zzb((Context)activity, zzt[0]), zzay.zzb().zzb((Context)activity, zzt[1]) };
            }
        }
        final int[] zzt = zzt();
        return new int[] { zzay.zzb().zzb((Context)activity, zzt[0]), zzay.zzb().zzb((Context)activity, zzt[1]) };
    }
    
    public static final boolean zzR(final View view, final PowerManager powerManager, final KeyguardManager keyguardManager) {
        final boolean zzf = com.google.android.gms.ads.internal.zzt.zzp().zzf;
        boolean b = true;
        boolean b2 = false;
        Label_0047: {
            if (!zzf) {
                if (keyguardManager != null) {
                    if (keyguardManager.inKeyguardRestrictedInputMode()) {
                        if (!zzn(view)) {
                            b2 = false;
                            break Label_0047;
                        }
                    }
                }
            }
            b2 = true;
        }
        final long zzv = zzv(view);
        if (view.getVisibility() == 0 && view.isShown() && (powerManager == null || powerManager.isScreenOn()) && b2 && (!(boolean)zzba.zzc().zza(zzbdc.zzbj) || view.getLocalVisibleRect(new Rect()) || view.getGlobalVisibleRect(new Rect()))) {
            if (!(boolean)zzba.zzc().zza(zzbdc.zzjS)) {
                return b;
            }
            if (zzv >= (int)zzba.zzc().zza(zzbdc.zzjU)) {
                return true;
            }
        }
        b = false;
        return b;
    }
    
    public static final void zzS(final Context context, final Intent intent) {
        if (zzba.zzc().zza(zzbdc.zzkk)) {
            try {
                zzaa(context, intent);
                return;
            }
            catch (final SecurityException ex) {
                zzcbn.zzk("", (Throwable)ex);
                com.google.android.gms.ads.internal.zzt.zzo().zzw((Throwable)ex, "AdUtil.startActivityWithUnknownContext");
                return;
            }
        }
        zzaa(context, intent);
    }
    
    public static final void zzT(final Context context, final Uri uri) {
        try {
            final Intent intent = new Intent("android.intent.action.VIEW", uri);
            final Bundle bundle = new Bundle();
            intent.putExtras(bundle);
            zzo(context, intent);
            ((BaseBundle)bundle).putString("com.android.browser.application_id", context.getPackageName());
            context.startActivity(intent);
            final String string = uri.toString();
            final StringBuilder sb = new StringBuilder();
            sb.append("Opening ");
            sb.append(string);
            sb.append(" in a new browser.");
            zzcbn.zze(sb.toString());
        }
        catch (final ActivityNotFoundException ex) {
            zzcbn.zzh("No browser is found.", (Throwable)ex);
        }
    }
    
    public static final int[] zzU(final Activity activity) {
        final int[] zzP = zzP(activity);
        return new int[] { zzay.zzb().zzb((Context)activity, zzP[0]), zzay.zzb().zzb((Context)activity, zzP[1]) };
    }
    
    public static final boolean zzV(final View view, final Context context) {
        final Context applicationContext = context.getApplicationContext();
        PowerManager powerManager;
        if (applicationContext != null) {
            powerManager = (PowerManager)applicationContext.getSystemService("power");
        }
        else {
            powerManager = null;
        }
        return zzR(view, powerManager, zzW(context));
    }
    
    private static KeyguardManager zzW(final Context context) {
        final Object systemService = context.getSystemService("keyguard");
        if (systemService != null && systemService instanceof KeyguardManager) {
            return (KeyguardManager)systemService;
        }
        return null;
    }
    
    private static Bundle zzX(Context metaData) {
        try {
            metaData = (NullPointerException)Wrappers.packageManager((Context)metaData).getApplicationInfo(((Context)metaData).getPackageName(), 128).metaData;
            return (Bundle)metaData;
        }
        catch (final NullPointerException metaData) {}
        catch (final PackageManager$NameNotFoundException ex) {}
        zze.zzb("Error getting metadata", metaData);
        return null;
    }
    
    private static String zzY(final Bundle bundle) {
        if (bundle != null) {
            final String string = ((BaseBundle)bundle).getString("com.google.android.gms.ads.APPLICATION_ID");
            if (!TextUtils.isEmpty((CharSequence)string) && (string.matches("^ca-app-pub-[0-9]{16}~[0-9]{10}$") || string.matches("^/\\d+~.+$"))) {
                return string;
            }
        }
        return "";
    }
    
    private static boolean zzZ(final String s, final AtomicReference atomicReference, final String regex) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return false;
        }
        try {
            final Pattern pattern = atomicReference.get();
            if (pattern != null) {
                final Pattern compile = pattern;
                if (regex.equals(pattern.pattern())) {
                    return compile.matcher(s).matches();
                }
            }
            final Pattern compile = Pattern.compile(regex);
            atomicReference.set(compile);
            return compile.matcher(s).matches();
        }
        catch (final PatternSyntaxException ex) {
            return false;
        }
    }
    
    public static int zza(final int i) {
        if (i >= 5000) {
            return i;
        }
        if (i > 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("HTTP timeout too low: ");
            sb.append(i);
            sb.append(" milliseconds. Reverting to default timeout: 60000 milliseconds.");
            zzcbn.zzj(sb.toString());
        }
        return 60000;
    }
    
    private static final void zzaa(final Context context, final Intent intent) {
        try {
            context.startActivity(intent);
        }
        finally {
            intent.addFlags(268435456);
            context.startActivity(intent);
        }
    }
    
    public static List zzd() {
        final zzbcu zza = zzbdc.zza;
        final List zzb = zzba.zza().zzb();
        final ArrayList list = new ArrayList();
        final Iterator iterator = zzb.iterator();
        while (iterator.hasNext()) {
            for (final String s : zzful.zzc(zzftk.zzc(',')).zzd((CharSequence)iterator.next())) {
                try {
                    list.add(Long.valueOf(s));
                }
                catch (final NumberFormatException ex) {
                    zze.zza("Experiment ID is not a number");
                }
            }
        }
        return list;
    }
    
    public static final boolean zzn(View rootView) {
        rootView = rootView.getRootView();
        final WindowManager$LayoutParams windowManager$LayoutParams = null;
        Activity activity = null;
        Label_0033: {
            if (rootView != null) {
                final Context context = rootView.getContext();
                if (context instanceof Activity) {
                    activity = (Activity)context;
                    break Label_0033;
                }
            }
            activity = null;
        }
        if (activity == null) {
            return false;
        }
        final Window window = activity.getWindow();
        WindowManager$LayoutParams attributes;
        if (window == null) {
            attributes = windowManager$LayoutParams;
        }
        else {
            attributes = window.getAttributes();
        }
        return attributes != null && (attributes.flags & 0x80000) != 0x0;
    }
    
    public static final void zzo(final Context context, final Intent intent) {
        if (intent == null) {
            return;
        }
        Bundle extras;
        if (intent.getExtras() != null) {
            extras = intent.getExtras();
        }
        else {
            extras = new Bundle();
        }
        extras.putBinder("android.support.customtabs.extra.SESSION", (IBinder)null);
        ((BaseBundle)extras).putString("com.android.browser.application_id", context.getPackageName());
        intent.putExtras(extras);
    }
    
    public static final String zzp(final Context context) {
        Context applicationContext = context;
        if (context.getApplicationContext() != null) {
            applicationContext = context.getApplicationContext();
        }
        return zzY(zzX(applicationContext));
    }
    
    public static final String zzq() {
        final StringBuilder sb = new StringBuilder(256);
        sb.append("Mozilla/5.0 (Linux; U; Android");
        final String release = Build$VERSION.RELEASE;
        if (release != null) {
            sb.append(" ");
            sb.append(release);
        }
        sb.append("; ");
        sb.append(Locale.getDefault());
        final String device = Build.DEVICE;
        if (device != null) {
            sb.append("; ");
            sb.append(device);
            final String display = Build.DISPLAY;
            if (display != null) {
                sb.append(" Build/");
                sb.append(display);
            }
        }
        sb.append(") AppleWebKit/533 Version/4.0 Safari/533");
        return sb.toString();
    }
    
    public static final String zzr() {
        final String manufacturer = Build.MANUFACTURER;
        final String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return model;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(manufacturer);
        sb.append(" ");
        sb.append(model);
        return sb.toString();
    }
    
    public static final DisplayMetrics zzs(final WindowManager windowManager) {
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics;
    }
    
    public static final int[] zzt() {
        return new int[] { 0, 0 };
    }
    
    public static final Map zzu(final String s) {
        final HashMap hashMap = new HashMap();
        try {
            final JSONObject jsonObject = new JSONObject(s);
            final Iterator keys = jsonObject.keys();
            while (keys.hasNext()) {
                final String s2 = keys.next();
                final HashSet set = new HashSet();
                final JSONArray optJSONArray = jsonObject.optJSONArray(s2);
                if (optJSONArray != null) {
                    for (int i = 0; i < optJSONArray.length(); ++i) {
                        final String optString = optJSONArray.optString(i);
                        if (optString != null) {
                            set.add(optString);
                        }
                    }
                    hashMap.put(s2, set);
                }
            }
            return hashMap;
        }
        catch (final JSONException ex) {
            com.google.android.gms.ads.internal.zzt.zzo().zzw((Throwable)ex, "AdUtil.getMapOfFileNamesToKeysFromJsonString");
            return hashMap;
        }
    }
    
    public static final long zzv(View parent) {
        float a = Float.MAX_VALUE;
        float min;
        float n;
        do {
            final boolean b = parent instanceof View;
            n = 0.0f;
            min = a;
            if (!b) {
                break;
            }
            parent = parent;
            min = Math.min(a, ((View)parent).getAlpha());
            parent = ((View)parent).getParent();
            a = min;
        } while (min > 0.0f);
        if (min < 0.0f) {
            min = n;
        }
        return Math.round(min * 100.0f);
    }
    
    public static final WebResourceResponse zzw(Context ex, final String s, final String s2) {
        try {
            final HashMap hashMap = new HashMap();
            hashMap.put("User-Agent", com.google.android.gms.ads.internal.zzt.zzp().zzc((Context)ex, s));
            hashMap.put("Cache-Control", "max-stale=3600");
            ex = (TimeoutException)new zzbq((Context)ex).zzb(0, s2, hashMap, null).get(60L, TimeUnit.SECONDS);
            if (ex != null) {
                ex = (TimeoutException)new WebResourceResponse("application/javascript", "UTF-8", (InputStream)new ByteArrayInputStream(((String)ex).getBytes("UTF-8")));
                return (WebResourceResponse)ex;
            }
            return null;
        }
        catch (final TimeoutException ex) {}
        catch (final InterruptedException ex) {}
        catch (final ExecutionException ex) {}
        catch (final IOException ex2) {}
        zzcbn.zzk("Could not fetch MRAID JS.", (Throwable)ex);
        return null;
    }
    
    public static final String zzx() {
        final Resources zze = com.google.android.gms.ads.internal.zzt.zzo().zze();
        String string;
        if (zze != null) {
            string = zze.getString(R.string.s7);
        }
        else {
            string = "Test Ad";
        }
        return string;
    }
    
    public static final zzbt zzy(final Context context) {
        final zzbt zzbt = null;
        try {
            final Object instance = context.getClassLoader().loadClass("com.google.android.gms.ads.internal.util.WorkManagerUtil").getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
            if (!(instance instanceof IBinder)) {
                zzcbn.zzg("Instantiated WorkManagerUtil not instance of IBinder.");
                return null;
            }
            final IBinder binder = (IBinder)instance;
            zzbt zzbt2;
            if (binder == null) {
                zzbt2 = zzbt;
            }
            else {
                final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.ads.internal.util.IWorkManagerUtil");
                if (queryLocalInterface instanceof zzbt) {
                    zzbt2 = (zzbt)queryLocalInterface;
                }
                else {
                    zzbt2 = new zzbr(binder);
                }
            }
            return zzbt2;
        }
        catch (final Exception ex) {
            com.google.android.gms.ads.internal.zzt.zzo().zzw((Throwable)ex, "Failed to instantiate WorkManagerUtil");
            return null;
        }
    }
    
    public static final boolean zzz(Context zza, final String s) {
        zza = zzbwo.zza(zza);
        return Wrappers.packageManager(zza).checkPermission(s, zza.getPackageName()) == 0;
    }
    
    public final ik0 zzb(final Uri uri) {
        return zzgbb.zzj((Callable)new zzn(uri), this.zzk);
    }
    
    public final String zzc(final Context p0, final String p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/google/android/gms/ads/internal/util/zzt.zzg:Ljava/lang/Object;
        //     4: astore          4
        //     6: aload           4
        //     8: monitorenter   
        //     9: aload_0        
        //    10: getfield        com/google/android/gms/ads/internal/util/zzt.zzh:Ljava/lang/String;
        //    13: astore_3       
        //    14: aload_3        
        //    15: ifnull          23
        //    18: aload           4
        //    20: monitorexit    
        //    21: aload_3        
        //    22: areturn        
        //    23: aload_2        
        //    24: ifnonnull       36
        //    27: invokestatic    com/google/android/gms/ads/internal/util/zzt.zzq:()Ljava/lang/String;
        //    30: astore_1       
        //    31: aload           4
        //    33: monitorexit    
        //    34: aload_1        
        //    35: areturn        
        //    36: invokestatic    com/google/android/gms/ads/internal/util/zzck.zza:()Lcom/google/android/gms/ads/internal/util/zzck;
        //    39: astore          5
        //    41: aload           5
        //    43: getfield        com/google/android/gms/ads/internal/util/zzck.zza:Ljava/lang/String;
        //    46: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //    49: ifne            55
        //    52: goto            116
        //    55: invokestatic    com/google/android/gms/common/util/ClientLibraryUtils.isPackageSide:()Z
        //    58: ifeq            84
        //    61: new             Lcom/google/android/gms/ads/internal/util/zzci;
        //    64: astore_3       
        //    65: aload_3        
        //    66: aload_1        
        //    67: invokespecial   com/google/android/gms/ads/internal/util/zzci.<init>:(Landroid/content/Context;)V
        //    70: aload_1        
        //    71: aload_3        
        //    72: invokestatic    com/google/android/gms/ads/internal/util/zzch.zza:(Landroid/content/Context;Ljava/util/concurrent/Callable;)Ljava/lang/Object;
        //    75: astore_3       
        //    76: aload_3        
        //    77: checkcast       Ljava/lang/String;
        //    80: astore_3       
        //    81: goto            110
        //    84: aload_1        
        //    85: invokestatic    com/google/android/gms/common/GooglePlayServicesUtilLight.getRemoteContext:(Landroid/content/Context;)Landroid/content/Context;
        //    88: astore          6
        //    90: new             Lcom/google/android/gms/ads/internal/util/zzcj;
        //    93: astore_3       
        //    94: aload_3        
        //    95: aload           6
        //    97: aload_1        
        //    98: invokespecial   com/google/android/gms/ads/internal/util/zzcj.<init>:(Landroid/content/Context;Landroid/content/Context;)V
        //   101: aload_1        
        //   102: aload_3        
        //   103: invokestatic    com/google/android/gms/ads/internal/util/zzch.zza:(Landroid/content/Context;Ljava/util/concurrent/Callable;)Ljava/lang/Object;
        //   106: astore_3       
        //   107: goto            76
        //   110: aload           5
        //   112: aload_3        
        //   113: putfield        com/google/android/gms/ads/internal/util/zzck.zza:Ljava/lang/String;
        //   116: aload_0        
        //   117: aload           5
        //   119: getfield        com/google/android/gms/ads/internal/util/zzck.zza:Ljava/lang/String;
        //   122: putfield        com/google/android/gms/ads/internal/util/zzt.zzh:Ljava/lang/String;
        //   125: aload_0        
        //   126: getfield        com/google/android/gms/ads/internal/util/zzt.zzh:Ljava/lang/String;
        //   129: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //   132: ifeq            143
        //   135: aload_0        
        //   136: aload_1        
        //   137: invokestatic    android/webkit/WebSettings.getDefaultUserAgent:(Landroid/content/Context;)Ljava/lang/String;
        //   140: putfield        com/google/android/gms/ads/internal/util/zzt.zzh:Ljava/lang/String;
        //   143: aload_0        
        //   144: getfield        com/google/android/gms/ads/internal/util/zzt.zzh:Ljava/lang/String;
        //   147: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //   150: ifeq            160
        //   153: aload_0        
        //   154: invokestatic    com/google/android/gms/ads/internal/util/zzt.zzq:()Ljava/lang/String;
        //   157: putfield        com/google/android/gms/ads/internal/util/zzt.zzh:Ljava/lang/String;
        //   160: aload_0        
        //   161: getfield        com/google/android/gms/ads/internal/util/zzt.zzh:Ljava/lang/String;
        //   164: astore_3       
        //   165: new             Ljava/lang/StringBuilder;
        //   168: astore          5
        //   170: aload           5
        //   172: invokespecial   java/lang/StringBuilder.<init>:()V
        //   175: aload           5
        //   177: aload_3        
        //   178: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   181: pop            
        //   182: aload           5
        //   184: ldc_w           " (Mobile; "
        //   187: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   190: pop            
        //   191: aload           5
        //   193: aload_2        
        //   194: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   197: pop            
        //   198: aload_0        
        //   199: aload           5
        //   201: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   204: putfield        com/google/android/gms/ads/internal/util/zzt.zzh:Ljava/lang/String;
        //   207: aload_1        
        //   208: invokestatic    com/google/android/gms/common/wrappers/Wrappers.packageManager:(Landroid/content/Context;)Lcom/google/android/gms/common/wrappers/PackageManagerWrapper;
        //   211: invokevirtual   com/google/android/gms/common/wrappers/PackageManagerWrapper.isCallerInstantApp:()Z
        //   214: ifeq            266
        //   217: aload_0        
        //   218: getfield        com/google/android/gms/ads/internal/util/zzt.zzh:Ljava/lang/String;
        //   221: astore_2       
        //   222: new             Ljava/lang/StringBuilder;
        //   225: astore_1       
        //   226: aload_1        
        //   227: invokespecial   java/lang/StringBuilder.<init>:()V
        //   230: aload_1        
        //   231: aload_2        
        //   232: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   235: pop            
        //   236: aload_1        
        //   237: ldc_w           ";aia"
        //   240: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   243: pop            
        //   244: aload_0        
        //   245: aload_1        
        //   246: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   249: putfield        com/google/android/gms/ads/internal/util/zzt.zzh:Ljava/lang/String;
        //   252: goto            266
        //   255: astore_1       
        //   256: invokestatic    com/google/android/gms/ads/internal/zzt.zzo:()Lcom/google/android/gms/internal/ads/zzcaw;
        //   259: aload_1        
        //   260: ldc_w           "AdUtil.getUserAgent"
        //   263: invokevirtual   com/google/android/gms/internal/ads/zzcaw.zzw:(Ljava/lang/Throwable;Ljava/lang/String;)V
        //   266: aload_0        
        //   267: getfield        com/google/android/gms/ads/internal/util/zzt.zzh:Ljava/lang/String;
        //   270: astore_2       
        //   271: new             Ljava/lang/StringBuilder;
        //   274: astore_1       
        //   275: aload_1        
        //   276: invokespecial   java/lang/StringBuilder.<init>:()V
        //   279: aload_1        
        //   280: aload_2        
        //   281: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   284: pop            
        //   285: aload_1        
        //   286: ldc_w           ")"
        //   289: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   292: pop            
        //   293: aload_1        
        //   294: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   297: astore_1       
        //   298: aload_0        
        //   299: aload_1        
        //   300: putfield        com/google/android/gms/ads/internal/util/zzt.zzh:Ljava/lang/String;
        //   303: aload           4
        //   305: monitorexit    
        //   306: aload_1        
        //   307: areturn        
        //   308: astore_1       
        //   309: aload           4
        //   311: monitorexit    
        //   312: aload_1        
        //   313: athrow         
        //   314: astore_3       
        //   315: goto            125
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  9      14     308    314    Any
        //  18     21     308    314    Any
        //  27     34     308    314    Any
        //  36     52     314    318    Ljava/lang/Exception;
        //  36     52     308    314    Any
        //  55     76     314    318    Ljava/lang/Exception;
        //  55     76     308    314    Any
        //  76     81     314    318    Ljava/lang/Exception;
        //  76     81     308    314    Any
        //  84     107    314    318    Ljava/lang/Exception;
        //  84     107    308    314    Any
        //  110    116    314    318    Ljava/lang/Exception;
        //  110    116    308    314    Any
        //  116    125    314    318    Ljava/lang/Exception;
        //  116    125    308    314    Any
        //  125    143    308    314    Any
        //  143    160    308    314    Any
        //  160    207    308    314    Any
        //  207    252    255    266    Ljava/lang/Exception;
        //  207    252    308    314    Any
        //  256    266    308    314    Any
        //  266    306    308    314    Any
        //  309    312    308    314    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0036:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public final void zzf(final Context context, final String s, final boolean b, final HttpURLConnection httpURLConnection, final boolean b2, int zza) {
        zza = zza(zza);
        final StringBuilder sb = new StringBuilder();
        sb.append("HTTP timeout: ");
        sb.append(zza);
        sb.append(" milliseconds.");
        zzcbn.zzi(sb.toString());
        httpURLConnection.setConnectTimeout(zza);
        httpURLConnection.setInstanceFollowRedirects(false);
        httpURLConnection.setReadTimeout(zza);
        if (TextUtils.isEmpty((CharSequence)httpURLConnection.getRequestProperty("User-Agent"))) {
            httpURLConnection.setRequestProperty("User-Agent", this.zzc(context, s));
        }
        httpURLConnection.setUseCaches(false);
    }
    
    public final void zzh(final Context context, final String s, String s2, final Bundle bundle, final boolean b) {
        com.google.android.gms.ads.internal.zzt.zzp();
        ((BaseBundle)bundle).putString("device", zzr());
        final zzbcu zza = zzbdc.zza;
        ((BaseBundle)bundle).putString("eids", TextUtils.join((CharSequence)",", (Iterable)zzba.zza().zza()));
        if (((BaseBundle)bundle).isEmpty()) {
            zzcbn.zze("Empty or null bundle.");
        }
        else {
            s2 = (String)zzba.zzc().zza(zzbdc.zzjP);
            if (!this.zze.getAndSet(true)) {
                this.zzd.set(zzad.zza(context, s2, (SharedPreferences$OnSharedPreferenceChangeListener)new zzm(this, context, s2)));
            }
            bundle.putAll((Bundle)this.zzd.get());
        }
        zzay.zzb();
        zzcbg.zzw(context, s, "gmob-apps", bundle, true, (zzcbf)new zzl(context, s));
    }
    
    public final boolean zzi(final String s) {
        return zzZ(s, this.zzb, (String)zzba.zzc().zza(zzbdc.zzab));
    }
    
    public final boolean zzj(final String s) {
        return zzZ(s, this.zzc, (String)zzba.zzc().zza(zzbdc.zzac));
    }
    
    public final boolean zzk(final Context context) {
        if (this.zzj) {
            return false;
        }
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.google.android.ads.intent.DEBUG_LOGGING_ENABLEMENT_CHANGED");
        zzbdc.zza(context);
        if ((boolean)zzba.zzc().zza(zzbdc.zzkj) && Build$VERSION.SDK_INT >= 33) {
            ze2.a(context.getApplicationContext(), (BroadcastReceiver)new zzq(this, null), intentFilter, 4);
        }
        else {
            context.getApplicationContext().registerReceiver((BroadcastReceiver)new zzq(this, null), intentFilter);
        }
        return this.zzj = true;
    }
    
    public final boolean zzl(final Context context) {
        if (this.zzi) {
            return false;
        }
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.USER_PRESENT");
        intentFilter.addAction("android.intent.action.SCREEN_OFF");
        zzbdc.zza(context);
        if ((boolean)zzba.zzc().zza(zzbdc.zzkj) && Build$VERSION.SDK_INT >= 33) {
            ze2.a(context.getApplicationContext(), (BroadcastReceiver)new zzs(this, null), intentFilter, 4);
        }
        else {
            context.getApplicationContext().registerReceiver((BroadcastReceiver)new zzs(this, null), intentFilter);
        }
        return this.zzi = true;
    }
    
    public final int zzm(final Context context, final Uri data) {
        if (context == null) {
            com.google.android.gms.ads.internal.util.zze.zza("Trying to open chrome custom tab on a null context");
            return 3;
        }
        int n;
        if (!(context instanceof Activity)) {
            com.google.android.gms.ads.internal.util.zze.zza("Chrome Custom Tabs can only work with Activity context.");
            n = 2;
        }
        else {
            n = 0;
        }
        final zzbcu zzeu = zzbdc.zzeu;
        final Boolean b = (Boolean)zzba.zzc().zza(zzeu);
        final zzbcu zzev = zzbdc.zzev;
        if (b.equals(zzba.zzc().zza(zzev))) {
            n = 9;
        }
        if (n != 0) {
            final Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(data);
            intent.addFlags(268435456);
            context.startActivity(intent);
            return n;
        }
        if (zzba.zzc().zza(zzeu)) {
            final zzbed zzbed = new zzbed();
            zzbed.zze((zzbeb)new zzo(this, zzbed, context, data));
            zzbed.zzb((Activity)context);
        }
        if (zzba.zzc().zza(zzev)) {
            final to a = new to.d().a();
            a.a.setPackage(zzhed.zza(context));
            a.a(context, data);
        }
        return 5;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.Parcel;
import android.os.IInterface;
import com.google.android.gms.internal.ads.zzavi;
import com.google.android.gms.internal.ads.zzbpr;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IBinder;
import com.google.android.gms.internal.ads.zzavg;

public final class zzdk extends zzavg
{
    public zzdk(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IOutOfContextTesterCreator");
    }
    
    public final zzdj zze(final IObjectWrapper objectWrapper, final zzbpr zzbpr, final int n) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)objectWrapper);
        zzavi.zzf(zza, (IInterface)zzbpr);
        zza.writeInt(234310000);
        final Parcel zzbh = this.zzbh(1, zza);
        final IBinder strongBinder = zzbh.readStrongBinder();
        zzdj zzdj;
        if (strongBinder == null) {
            zzdj = null;
        }
        else {
            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IOutOfContextTester");
            if (queryLocalInterface instanceof zzdj) {
                zzdj = (zzdj)queryLocalInterface;
            }
            else {
                zzdj = new zzdh(strongBinder);
            }
        }
        zzbh.recycle();
        return zzdj;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import com.google.android.gms.internal.ads.zzbey;
import java.util.Iterator;
import android.util.Log;
import com.google.android.gms.internal.ads.zzcbn;

public final class zze extends zzcbn
{
    public static void zza(String s) {
        if (zzc()) {
            if (s != null && s.length() > 4000) {
                final Iterator iterator = zzcbn.zza.zzd((CharSequence)s).iterator();
                int n = 1;
                while (iterator.hasNext()) {
                    s = (String)iterator.next();
                    if (n != 0) {
                        Log.v("Ads", s);
                    }
                    else {
                        Log.v("Ads-cont", s);
                    }
                    n = 0;
                }
            }
            else {
                Log.v("Ads", s);
            }
        }
    }
    
    public static void zzb(final String s, final Throwable t) {
        if (zzc()) {
            Log.v("Ads", s, t);
        }
    }
    
    public static boolean zzc() {
        return zzcbn.zzm(2) && (boolean)zzbey.zza.zze();
    }
}

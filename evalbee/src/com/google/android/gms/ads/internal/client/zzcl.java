// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.ads.zzbpr;
import android.os.IInterface;

public interface zzcl extends IInterface
{
    zzbpr getAdapterCreator();
    
    zzen getLiteSdkVersion();
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import android.media.AudioManager;
import android.telephony.TelephonyManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.app.Activity;
import com.google.android.gms.internal.ads.zzchx;
import com.google.android.gms.internal.ads.zzchc;
import com.google.android.gms.internal.ads.zzefa;
import com.google.android.gms.internal.ads.zzayp;
import com.google.android.gms.internal.ads.zzcgv;
import android.webkit.WebResourceResponse;
import java.io.InputStream;
import java.util.Map;
import com.google.android.gms.internal.ads.zzcbn;
import android.webkit.CookieSyncManager;
import com.google.android.gms.ads.internal.zzt;
import android.webkit.CookieManager;
import android.content.Context;

public class zzab
{
    private zzab() {
    }
    
    public static zzab zzo(final int n) {
        if (n >= 30) {
            return new zzz();
        }
        if (n >= 28) {
            return new zzy();
        }
        if (n >= 26) {
            return new zzw();
        }
        if (n >= 24) {
            return new zzv();
        }
        if (n >= 21) {
            return new zzu();
        }
        return new zzab();
    }
    
    public int zza() {
        return 1;
    }
    
    public CookieManager zzb(final Context context) {
        zzt.zzp();
        if (com.google.android.gms.ads.internal.util.zzt.zzE()) {
            return null;
        }
        try {
            CookieSyncManager.createInstance(context);
            return CookieManager.getInstance();
        }
        finally {
            final Throwable t;
            zzcbn.zzh("Failed to obtain CookieManager.", t);
            zzt.zzo().zzw(t, "ApiLevelUtil.getCookieManager");
            return null;
        }
    }
    
    public WebResourceResponse zzc(final String s, final String s2, final int n, final String s3, final Map map, final InputStream inputStream) {
        return new WebResourceResponse(s, s2, inputStream);
    }
    
    public zzchc zzd(final zzcgv zzcgv, final zzayp zzayp, final boolean b, final zzefa zzefa) {
        return (zzchc)new zzchx(zzcgv, zzayp, b, zzefa);
    }
    
    public boolean zze(final Activity activity, final Configuration configuration) {
        return false;
    }
    
    public Intent zzg(final Activity activity) {
        final Intent intent = new Intent();
        intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
        intent.putExtra("app_package", ((Context)activity).getPackageName());
        intent.putExtra("app_uid", ((Context)activity).getApplicationInfo().uid);
        return intent;
    }
    
    public void zzh(final Context context, final String s, final String s2) {
    }
    
    public boolean zzi(final Context context, final String s) {
        return false;
    }
    
    public int zzj(final Context context, final TelephonyManager telephonyManager) {
        return 1001;
    }
    
    public int zzk(final AudioManager audioManager) {
        return 0;
    }
    
    public void zzl(final Activity activity) {
    }
    
    public int zzn(final Context context) {
        return ((TelephonyManager)context.getSystemService("phone")).getNetworkType();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import android.app.Dialog;
import android.app.AlertDialog$Builder;
import android.content.DialogInterface$OnClickListener;
import com.google.android.gms.ads.internal.zzt;
import android.content.Context;

final class zzax implements Runnable
{
    final Context zza;
    final String zzb;
    final boolean zzc;
    final boolean zzd;
    
    public zzax(final zzay zzay, final Context zza, final String zzb, final boolean zzc, final boolean zzd) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
    }
    
    @Override
    public final void run() {
        zzt.zzp();
        final AlertDialog$Builder zzJ = com.google.android.gms.ads.internal.util.zzt.zzJ(this.zza);
        zzJ.setMessage((CharSequence)this.zzb);
        String title;
        if (this.zzc) {
            title = "Error";
        }
        else {
            title = "Info";
        }
        zzJ.setTitle((CharSequence)title);
        if (this.zzd) {
            zzJ.setNeutralButton((CharSequence)"Dismiss", (DialogInterface$OnClickListener)null);
        }
        else {
            zzJ.setPositiveButton((CharSequence)"Learn More", (DialogInterface$OnClickListener)new zzaw(this));
            zzJ.setNegativeButton((CharSequence)"Dismiss", (DialogInterface$OnClickListener)null);
        }
        ((Dialog)zzJ.create()).show();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import java.util.Locale;
import com.google.android.gms.ads.AdInspectorError;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.internal.ads.zzfun;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.internal.ads.zzbpk;
import java.util.Iterator;
import java.util.Map;
import com.google.android.gms.internal.ads.zzbmj;
import com.google.android.gms.internal.ads.zzbmi;
import com.google.android.gms.ads.initialization.AdapterStatus;
import com.google.android.gms.internal.ads.zzbma;
import java.util.HashMap;
import com.google.android.gms.ads.initialization.InitializationStatus;
import java.util.List;
import android.os.RemoteException;
import com.google.android.gms.internal.ads.zzcbn;
import android.content.Context;
import com.google.android.gms.ads.RequestConfiguration;
import com.google.android.gms.ads.OnAdInspectorClosedListener;
import java.util.ArrayList;

public final class zzej
{
    private static zzej zza;
    private final Object zzb;
    private final ArrayList zzc;
    private boolean zzd;
    private boolean zze;
    private final Object zzf;
    private zzco zzg;
    private OnAdInspectorClosedListener zzh;
    private RequestConfiguration zzi;
    
    private zzej() {
        this.zzb = new Object();
        this.zzd = false;
        this.zze = false;
        this.zzf = new Object();
        this.zzh = null;
        this.zzi = new RequestConfiguration.Builder().build();
        this.zzc = new ArrayList();
    }
    
    private final void zzA(final Context context) {
        if (this.zzg == null) {
            this.zzg = (zzco)new zzaq(zzay.zza(), context).zzd(context, false);
        }
    }
    
    private final void zzB(final RequestConfiguration requestConfiguration) {
        try {
            this.zzg.zzu(new zzff(requestConfiguration));
        }
        catch (final RemoteException ex) {
            zzcbn.zzh("Unable to set request configuration parcel.", (Throwable)ex);
        }
    }
    
    public static zzej zzf() {
        synchronized (zzej.class) {
            if (zzej.zza == null) {
                zzej.zza = new zzej();
            }
            return zzej.zza;
        }
    }
    
    private static InitializationStatus zzy(final List list) {
        final HashMap hashMap = new HashMap();
        for (final zzbma zzbma : list) {
            final String zza = zzbma.zza;
            AdapterStatus.State state;
            if (zzbma.zzb) {
                state = AdapterStatus.State.READY;
            }
            else {
                state = AdapterStatus.State.NOT_READY;
            }
            hashMap.put(zza, new zzbmi(state, zzbma.zzd, zzbma.zzc));
        }
        return (InitializationStatus)new zzbmj((Map)hashMap);
    }
    
    private final void zzz(final Context context, final String s) {
        try {
            zzbpk.zza().zzb(context, (String)null);
            this.zzg.zzk();
            this.zzg.zzl(null, ObjectWrapper.wrap((Object)null));
        }
        catch (final RemoteException ex) {
            zzcbn.zzk("MobileAdsSettingManager initialization failed", (Throwable)ex);
        }
    }
    
    public final float zza() {
        synchronized (this.zzf) {
            final zzco zzg = this.zzg;
            float zze = 1.0f;
            if (zzg == null) {
                return 1.0f;
            }
            try {
                zze = zzg.zze();
            }
            catch (final RemoteException ex) {
                zzcbn.zzh("Unable to get app volume.", (Throwable)ex);
            }
            return zze;
        }
    }
    
    public final RequestConfiguration zzc() {
        return this.zzi;
    }
    
    public final InitializationStatus zze() {
        synchronized (this.zzf) {
            Preconditions.checkState(this.zzg != null, (Object)"MobileAds.initialize() must be called prior to getting initialization status.");
            try {
                return zzy(this.zzg.zzg());
            }
            catch (final RemoteException ex) {
                zzcbn.zzg("Unable to get Initialization status.");
                return new zzeb(this);
            }
        }
    }
    
    public final String zzh() {
        synchronized (this.zzf) {
            Preconditions.checkState(this.zzg != null, (Object)"MobileAds.initialize() must be called prior to getting version string.");
            try {
                return zzfun.zzc(this.zzg.zzf());
            }
            catch (final RemoteException ex) {
                zzcbn.zzh("Unable to get internal version.", (Throwable)ex);
                return "";
            }
        }
    }
    
    public final void zzl(final Context context) {
        synchronized (this.zzf) {
            this.zzA(context);
            try {
                this.zzg.zzi();
            }
            catch (final RemoteException ex) {
                zzcbn.zzg("Unable to disable mediation adapter initialization.");
            }
        }
    }
    
    public final void zzm(final Context p0, final String p1, final OnInitializationCompleteListener p2) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/google/android/gms/ads/internal/client/zzej.zzb:Ljava/lang/Object;
        //     4: astore_2       
        //     5: aload_2        
        //     6: monitorenter   
        //     7: aload_0        
        //     8: getfield        com/google/android/gms/ads/internal/client/zzej.zzd:Z
        //    11: ifeq            30
        //    14: aload_3        
        //    15: ifnull          27
        //    18: aload_0        
        //    19: getfield        com/google/android/gms/ads/internal/client/zzej.zzc:Ljava/util/ArrayList;
        //    22: aload_3        
        //    23: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //    26: pop            
        //    27: aload_2        
        //    28: monitorexit    
        //    29: return         
        //    30: aload_0        
        //    31: getfield        com/google/android/gms/ads/internal/client/zzej.zze:Z
        //    34: ifeq            54
        //    37: aload_3        
        //    38: ifnull          51
        //    41: aload_3        
        //    42: aload_0        
        //    43: invokevirtual   com/google/android/gms/ads/internal/client/zzej.zze:()Lcom/google/android/gms/ads/initialization/InitializationStatus;
        //    46: invokeinterface com/google/android/gms/ads/initialization/OnInitializationCompleteListener.onInitializationComplete:(Lcom/google/android/gms/ads/initialization/InitializationStatus;)V
        //    51: aload_2        
        //    52: monitorexit    
        //    53: return         
        //    54: aload_0        
        //    55: iconst_1       
        //    56: putfield        com/google/android/gms/ads/internal/client/zzej.zzd:Z
        //    59: aload_3        
        //    60: ifnull          72
        //    63: aload_0        
        //    64: getfield        com/google/android/gms/ads/internal/client/zzej.zzc:Ljava/util/ArrayList;
        //    67: aload_3        
        //    68: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //    71: pop            
        //    72: aload_2        
        //    73: monitorexit    
        //    74: aload_1        
        //    75: ifnull          334
        //    78: aload_0        
        //    79: getfield        com/google/android/gms/ads/internal/client/zzej.zzf:Ljava/lang/Object;
        //    82: astore_2       
        //    83: aload_2        
        //    84: monitorenter   
        //    85: aload_0        
        //    86: aload_1        
        //    87: invokespecial   com/google/android/gms/ads/internal/client/zzej.zzA:(Landroid/content/Context;)V
        //    90: aload_0        
        //    91: getfield        com/google/android/gms/ads/internal/client/zzej.zzg:Lcom/google/android/gms/ads/internal/client/zzco;
        //    94: astore_3       
        //    95: new             Lcom/google/android/gms/ads/internal/client/zzei;
        //    98: astore          4
        //   100: aload           4
        //   102: aload_0        
        //   103: aconst_null    
        //   104: invokespecial   com/google/android/gms/ads/internal/client/zzei.<init>:(Lcom/google/android/gms/ads/internal/client/zzej;Lcom/google/android/gms/ads/internal/client/zzeh;)V
        //   107: aload_3        
        //   108: aload           4
        //   110: invokeinterface com/google/android/gms/ads/internal/client/zzco.zzs:(Lcom/google/android/gms/internal/ads/zzbmh;)V
        //   115: aload_0        
        //   116: getfield        com/google/android/gms/ads/internal/client/zzej.zzg:Lcom/google/android/gms/ads/internal/client/zzco;
        //   119: astore_3       
        //   120: new             Lcom/google/android/gms/internal/ads/zzbpo;
        //   123: astore          4
        //   125: aload           4
        //   127: invokespecial   com/google/android/gms/internal/ads/zzbpo.<init>:()V
        //   130: aload_3        
        //   131: aload           4
        //   133: invokeinterface com/google/android/gms/ads/internal/client/zzco.zzo:(Lcom/google/android/gms/internal/ads/zzbpr;)V
        //   138: aload_0        
        //   139: getfield        com/google/android/gms/ads/internal/client/zzej.zzi:Lcom/google/android/gms/ads/RequestConfiguration;
        //   142: invokevirtual   com/google/android/gms/ads/RequestConfiguration.getTagForChildDirectedTreatment:()I
        //   145: iconst_m1      
        //   146: if_icmpne       160
        //   149: aload_0        
        //   150: getfield        com/google/android/gms/ads/internal/client/zzej.zzi:Lcom/google/android/gms/ads/RequestConfiguration;
        //   153: invokevirtual   com/google/android/gms/ads/RequestConfiguration.getTagForUnderAgeOfConsent:()I
        //   156: iconst_m1      
        //   157: if_icmpeq       182
        //   160: aload_0        
        //   161: aload_0        
        //   162: getfield        com/google/android/gms/ads/internal/client/zzej.zzi:Lcom/google/android/gms/ads/RequestConfiguration;
        //   165: invokespecial   com/google/android/gms/ads/internal/client/zzej.zzB:(Lcom/google/android/gms/ads/RequestConfiguration;)V
        //   168: goto            182
        //   171: astore_1       
        //   172: goto            330
        //   175: astore_3       
        //   176: ldc             "MobileAdsSettingManager initialization failed"
        //   178: aload_3        
        //   179: invokestatic    com/google/android/gms/internal/ads/zzcbn.zzk:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   182: aload_1        
        //   183: invokestatic    com/google/android/gms/internal/ads/zzbdc.zza:(Landroid/content/Context;)V
        //   186: getstatic       com/google/android/gms/internal/ads/zzbet.zza:Lcom/google/android/gms/internal/ads/zzbeh;
        //   189: invokevirtual   com/google/android/gms/internal/ads/zzbeh.zze:()Ljava/lang/Object;
        //   192: checkcast       Ljava/lang/Boolean;
        //   195: invokevirtual   java/lang/Boolean.booleanValue:()Z
        //   198: ifeq            253
        //   201: getstatic       com/google/android/gms/internal/ads/zzbdc.zzks:Lcom/google/android/gms/internal/ads/zzbcu;
        //   204: astore_3       
        //   205: invokestatic    com/google/android/gms/ads/internal/client/zzba.zzc:()Lcom/google/android/gms/internal/ads/zzbda;
        //   208: aload_3        
        //   209: invokevirtual   com/google/android/gms/internal/ads/zzbda.zza:(Lcom/google/android/gms/internal/ads/zzbcu;)Ljava/lang/Object;
        //   212: checkcast       Ljava/lang/Boolean;
        //   215: invokevirtual   java/lang/Boolean.booleanValue:()Z
        //   218: ifeq            253
        //   221: ldc_w           "Initializing on bg thread"
        //   224: invokestatic    com/google/android/gms/internal/ads/zzcbn.zze:(Ljava/lang/String;)V
        //   227: getstatic       com/google/android/gms/internal/ads/zzcbc.zza:Ljava/util/concurrent/ThreadPoolExecutor;
        //   230: astore_3       
        //   231: new             Lcom/google/android/gms/ads/internal/client/zzec;
        //   234: astore          4
        //   236: aload           4
        //   238: aload_0        
        //   239: aload_1        
        //   240: aconst_null    
        //   241: invokespecial   com/google/android/gms/ads/internal/client/zzec.<init>:(Lcom/google/android/gms/ads/internal/client/zzej;Landroid/content/Context;Ljava/lang/String;)V
        //   244: aload_3        
        //   245: aload           4
        //   247: invokevirtual   java/util/concurrent/ThreadPoolExecutor.execute:(Ljava/lang/Runnable;)V
        //   250: goto            327
        //   253: getstatic       com/google/android/gms/internal/ads/zzbet.zzb:Lcom/google/android/gms/internal/ads/zzbeh;
        //   256: invokevirtual   com/google/android/gms/internal/ads/zzbeh.zze:()Ljava/lang/Object;
        //   259: checkcast       Ljava/lang/Boolean;
        //   262: invokevirtual   java/lang/Boolean.booleanValue:()Z
        //   265: ifeq            315
        //   268: getstatic       com/google/android/gms/internal/ads/zzbdc.zzks:Lcom/google/android/gms/internal/ads/zzbcu;
        //   271: astore_3       
        //   272: invokestatic    com/google/android/gms/ads/internal/client/zzba.zzc:()Lcom/google/android/gms/internal/ads/zzbda;
        //   275: aload_3        
        //   276: invokevirtual   com/google/android/gms/internal/ads/zzbda.zza:(Lcom/google/android/gms/internal/ads/zzbcu;)Ljava/lang/Object;
        //   279: checkcast       Ljava/lang/Boolean;
        //   282: invokevirtual   java/lang/Boolean.booleanValue:()Z
        //   285: ifeq            315
        //   288: getstatic       com/google/android/gms/internal/ads/zzcbc.zzb:Ljava/util/concurrent/ExecutorService;
        //   291: astore          4
        //   293: new             Lcom/google/android/gms/ads/internal/client/zzed;
        //   296: astore_3       
        //   297: aload_3        
        //   298: aload_0        
        //   299: aload_1        
        //   300: aconst_null    
        //   301: invokespecial   com/google/android/gms/ads/internal/client/zzed.<init>:(Lcom/google/android/gms/ads/internal/client/zzej;Landroid/content/Context;Ljava/lang/String;)V
        //   304: aload           4
        //   306: aload_3        
        //   307: invokeinterface java/util/concurrent/Executor.execute:(Ljava/lang/Runnable;)V
        //   312: goto            327
        //   315: ldc_w           "Initializing on calling thread"
        //   318: invokestatic    com/google/android/gms/internal/ads/zzcbn.zze:(Ljava/lang/String;)V
        //   321: aload_0        
        //   322: aload_1        
        //   323: aconst_null    
        //   324: invokespecial   com/google/android/gms/ads/internal/client/zzej.zzz:(Landroid/content/Context;Ljava/lang/String;)V
        //   327: aload_2        
        //   328: monitorexit    
        //   329: return         
        //   330: aload_2        
        //   331: monitorexit    
        //   332: aload_1        
        //   333: athrow         
        //   334: new             Ljava/lang/IllegalArgumentException;
        //   337: dup            
        //   338: ldc_w           "Context cannot be null."
        //   341: invokespecial   java/lang/IllegalArgumentException.<init>:(Ljava/lang/String;)V
        //   344: athrow         
        //   345: astore_1       
        //   346: aload_2        
        //   347: monitorexit    
        //   348: aload_1        
        //   349: athrow         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                        
        //  -----  -----  -----  -----  ----------------------------
        //  7      14     345    350    Any
        //  18     27     345    350    Any
        //  27     29     345    350    Any
        //  30     37     345    350    Any
        //  41     51     345    350    Any
        //  51     53     345    350    Any
        //  54     59     345    350    Any
        //  63     72     345    350    Any
        //  72     74     345    350    Any
        //  85     160    175    182    Landroid/os/RemoteException;
        //  85     160    171    334    Any
        //  160    168    175    182    Landroid/os/RemoteException;
        //  160    168    171    334    Any
        //  176    182    171    334    Any
        //  182    250    171    334    Any
        //  253    312    171    334    Any
        //  315    327    171    334    Any
        //  327    329    171    334    Any
        //  330    332    171    334    Any
        //  346    348    345    350    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException
        //     at com.strobel.decompiler.languages.java.ast.NameVariables.generateNameForVariable(NameVariables.java:252)
        //     at com.strobel.decompiler.languages.java.ast.NameVariables.assignNamesToVariables(NameVariables.java:175)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.nameVariables(AstMethodBodyBuilder.java:1482)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.populateVariables(AstMethodBodyBuilder.java:1408)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:210)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public final void zzp(final Context context, final OnAdInspectorClosedListener zzh) {
        synchronized (this.zzf) {
            this.zzA(context);
            this.zzh = zzh;
            try {
                this.zzg.zzm(new zzeg(null));
            }
            catch (final RemoteException ex) {
                zzcbn.zzg("Unable to open the ad inspector.");
                if (zzh != null) {
                    zzh.onAdInspectorClosed(new AdInspectorError(0, "Ad inspector had an internal error.", "com.google.android.gms.ads"));
                }
            }
        }
    }
    
    public final void zzq(final Context context, final String s) {
        synchronized (this.zzf) {
            Preconditions.checkState(this.zzg != null, (Object)"MobileAds.initialize() must be called prior to opening debug menu.");
            try {
                this.zzg.zzn(ObjectWrapper.wrap(context), s);
            }
            catch (final RemoteException ex) {
                zzcbn.zzh("Unable to open debug menu.", (Throwable)ex);
            }
        }
    }
    
    public final void zzr(final boolean b) {
        synchronized (this.zzf) {
            Preconditions.checkState(this.zzg != null, (Object)"MobileAds.initialize() must be called prior to enable/disable the publisher first-party ID.");
            try {
                this.zzg.zzj(b);
            }
            catch (final RemoteException cause) {
                String str;
                if (b) {
                    str = "enable";
                }
                else {
                    str = "disable";
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Unable to ");
                sb.append(str);
                sb.append(" the publisher first-party ID.");
                zzcbn.zzh(sb.toString(), (Throwable)cause);
                if (((Throwable)cause).getMessage() != null) {
                    if (((Throwable)cause).getMessage().toLowerCase(Locale.ROOT).contains("paid")) {
                        throw new IllegalStateException((Throwable)cause);
                    }
                }
            }
        }
    }
    
    public final void zzs(final Class clazz) {
        final Object zzf = this.zzf;
        monitorenter(zzf);
        try {
            try {
                this.zzg.zzh(clazz.getCanonicalName());
            }
            finally {
                monitorexit(zzf);
                monitorexit(zzf);
            }
        }
        catch (final RemoteException ex) {}
    }
    
    public final void zzt(final boolean b) {
        synchronized (this.zzf) {
            Preconditions.checkState(this.zzg != null, (Object)"MobileAds.initialize() must be called prior to setting app muted state.");
            try {
                this.zzg.zzp(b);
            }
            catch (final RemoteException ex) {
                zzcbn.zzh("Unable to set app mute state.", (Throwable)ex);
            }
        }
    }
    
    public final void zzu(final float n) {
        final boolean b = true;
        Preconditions.checkArgument(n >= 0.0f && n <= 1.0f, (Object)"The app volume must be a value between 0 and 1 inclusive.");
        synchronized (this.zzf) {
            Preconditions.checkState(this.zzg != null && b, (Object)"MobileAds.initialize() must be called prior to setting the app volume.");
            try {
                this.zzg.zzq(n);
            }
            catch (final RemoteException ex) {
                zzcbn.zzh("Unable to set app volume.", (Throwable)ex);
            }
        }
    }
    
    public final void zzv(final String s) {
        synchronized (this.zzf) {
            Preconditions.checkState(this.zzg != null, (Object)"MobileAds.initialize() must be called prior to setting the plugin.");
            try {
                this.zzg.zzt(s);
            }
            catch (final RemoteException ex) {
                zzcbn.zzh("Unable to set plugin.", (Throwable)ex);
            }
        }
    }
    
    public final void zzw(final RequestConfiguration zzi) {
        Preconditions.checkArgument(zzi != null, (Object)"Null passed to setRequestConfiguration.");
        synchronized (this.zzf) {
            final RequestConfiguration zzi2 = this.zzi;
            this.zzi = zzi;
            if (this.zzg == null) {
                return;
            }
            if (zzi2.getTagForChildDirectedTreatment() != zzi.getTagForChildDirectedTreatment() || zzi2.getTagForUnderAgeOfConsent() != zzi.getTagForUnderAgeOfConsent()) {
                this.zzB(zzi);
            }
        }
    }
    
    public final boolean zzx() {
        synchronized (this.zzf) {
            final zzco zzg = this.zzg;
            boolean zzv = false;
            if (zzg == null) {
                return false;
            }
            try {
                zzv = zzg.zzv();
            }
            catch (final RemoteException ex) {
                zzcbn.zzh("Unable to get app mute state.", (Throwable)ex);
            }
            return zzv;
        }
    }
}

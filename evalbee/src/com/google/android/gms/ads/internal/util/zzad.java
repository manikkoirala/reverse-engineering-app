// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import android.os.BaseBundle;
import android.content.SharedPreferences;
import java.util.List;
import org.json.JSONObject;
import com.google.android.gms.internal.ads.zzful;
import com.google.android.gms.internal.ads.zzftk;
import org.json.JSONException;
import com.google.android.gms.internal.ads.zzcbn;
import org.json.JSONArray;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.os.Bundle;
import android.content.SharedPreferences$OnSharedPreferenceChangeListener;
import android.content.Context;

public final class zzad
{
    public static Bundle zza(final Context context, final String s, final SharedPreferences$OnSharedPreferenceChangeListener sharedPreferences$OnSharedPreferenceChangeListener) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return Bundle.EMPTY;
        }
        PreferenceManager.getDefaultSharedPreferences(context).registerOnSharedPreferenceChangeListener(sharedPreferences$OnSharedPreferenceChangeListener);
        return zzb(context, s);
    }
    
    public static Bundle zzb(final Context context, final String s) {
        JSONArray jsonArray = null;
        Label_0034: {
            Label_0007: {
                if (!TextUtils.isEmpty((CharSequence)s)) {
                    try {
                        jsonArray = new JSONArray(s);
                    }
                    catch (final JSONException ex) {
                        zzcbn.zzf("JSON parsing error", (Throwable)ex);
                        break Label_0007;
                    }
                    break Label_0034;
                }
            }
            jsonArray = null;
        }
        if (jsonArray == null) {
            return Bundle.EMPTY;
        }
        final Bundle bundle = new Bundle();
        for (int i = 0; i < jsonArray.length(); ++i) {
            final JSONObject optJSONObject = jsonArray.optJSONObject(i);
            final String optString = optJSONObject.optString("bk");
            final String optString2 = optJSONObject.optString("sk");
            final int optInt = optJSONObject.optInt("type", -1);
            int n;
            if (optInt != 0) {
                if (optInt != 1) {
                    if (optInt != 2) {
                        n = 0;
                    }
                    else {
                        n = 3;
                    }
                }
                else {
                    n = 2;
                }
            }
            else {
                n = 1;
            }
            if (!TextUtils.isEmpty((CharSequence)optString) && !TextUtils.isEmpty((CharSequence)optString2)) {
                if (n != 0) {
                    final List zzf = zzful.zzc(zzftk.zzc('/')).zzf((CharSequence)optString2);
                    Object value;
                    if (zzf.size() <= 2 && !zzf.isEmpty()) {
                        SharedPreferences sharedPreferences;
                        Object o;
                        if (zzf.size() == 1) {
                            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                            o = zzf.get(0);
                        }
                        else {
                            sharedPreferences = context.getSharedPreferences((String)zzf.get(0), 0);
                            o = zzf.get(1);
                        }
                        value = sharedPreferences.getAll().get(o);
                    }
                    else {
                        value = null;
                    }
                    if (value != null) {
                        if (--n != 0) {
                            if (n != 1) {
                                if (value instanceof Boolean) {
                                    ((BaseBundle)bundle).putBoolean(optString, (boolean)value);
                                }
                            }
                            else if (value instanceof Integer) {
                                ((BaseBundle)bundle).putInt(optString, (int)value);
                            }
                            else if (value instanceof Long) {
                                ((BaseBundle)bundle).putLong(optString, (long)value);
                            }
                            else if (value instanceof Float) {
                                bundle.putFloat(optString, (float)value);
                            }
                        }
                        else if (value instanceof String) {
                            ((BaseBundle)bundle).putString(optString, (String)value);
                        }
                    }
                }
            }
        }
        return bundle;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IInterface;

public interface zzbt extends IInterface
{
    void zze(final IObjectWrapper p0);
    
    boolean zzf(final IObjectWrapper p0, final String p1, final String p2);
}

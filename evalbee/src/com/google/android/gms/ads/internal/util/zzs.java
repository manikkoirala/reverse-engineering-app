// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import android.content.Intent;
import android.content.Context;
import android.content.BroadcastReceiver;

final class zzs extends BroadcastReceiver
{
    final zzt zza;
    
    public final void onReceive(final Context context, final Intent intent) {
        zzt zzt;
        boolean b;
        if ("android.intent.action.USER_PRESENT".equals(intent.getAction())) {
            zzt = this.zza;
            b = true;
        }
        else {
            if (!"android.intent.action.SCREEN_OFF".equals(intent.getAction())) {
                return;
            }
            zzt = this.zza;
            b = false;
        }
        com.google.android.gms.ads.internal.util.zzt.zze(zzt, b);
    }
}

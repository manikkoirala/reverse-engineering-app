// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.ads.zzbpo;
import com.google.android.gms.internal.ads.zzbpr;
import android.content.Context;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public class LiteSdkInfo extends zzck
{
    public LiteSdkInfo(final Context context) {
    }
    
    public zzbpr getAdapterCreator() {
        return (zzbpr)new zzbpo();
    }
    
    public zzen getLiteSdkVersion() {
        return new zzen(234310600, 234310000, "22.6.0");
    }
}

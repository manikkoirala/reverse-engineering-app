// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.overlay;

import com.google.android.gms.internal.ads.zzfsf;
import com.google.android.gms.internal.ads.zzfsg;

final class zzv implements zzfsg
{
    final zzw zza;
    
    public zzv(final zzw zza) {
        this.zza = zza;
    }
    
    public final void zza(final zzfsf zzfsf) {
        this.zza.zzi(zzfsf);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.Parcelable$Creator;
import com.google.android.gms.internal.ads.zzavi;
import android.os.Parcel;
import android.os.IInterface;
import android.os.IBinder;
import com.google.android.gms.internal.ads.zzavh;

public abstract class zzdf extends zzavh implements zzdg
{
    public zzdf() {
        super("com.google.android.gms.ads.internal.client.IOnPaidEventListener");
    }
    
    public static zzdg zzb(final IBinder binder) {
        if (binder == null) {
            return null;
        }
        final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.ads.internal.client.IOnPaidEventListener");
        if (queryLocalInterface instanceof zzdg) {
            return (zzdg)queryLocalInterface;
        }
        return new zzde(binder);
    }
    
    public final boolean zzbK(int zzf, final Parcel parcel, final Parcel parcel2, int zza) {
        if (zzf != 1) {
            if (zzf != 2) {
                return false;
            }
            zzf = (this.zzf() ? 1 : 0);
            parcel2.writeNoException();
            zza = zzavi.zza;
            parcel2.writeInt(zzf);
        }
        else {
            final zzs zzs = (zzs)zzavi.zza(parcel, (Parcelable$Creator)com.google.android.gms.ads.internal.client.zzs.CREATOR);
            zzavi.zzc(parcel);
            this.zze(zzs);
            parcel2.writeNoException();
        }
        return true;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.ads.zzcae;
import com.google.android.gms.internal.ads.zzbxj;
import com.google.android.gms.internal.ads.zzbwt;
import com.google.android.gms.internal.ads.zzbtp;
import com.google.android.gms.internal.ads.zzbti;
import com.google.android.gms.internal.ads.zzble;
import com.google.android.gms.internal.ads.zzblb;
import com.google.android.gms.internal.ads.zzbgs;
import com.google.android.gms.internal.ads.zzbgm;
import com.google.android.gms.internal.ads.zzbpr;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IInterface;

public interface zzce extends IInterface
{
    zzbq zzb(final IObjectWrapper p0, final String p1, final zzbpr p2, final int p3);
    
    zzbu zzc(final IObjectWrapper p0, final zzq p1, final String p2, final zzbpr p3, final int p4);
    
    zzbu zzd(final IObjectWrapper p0, final zzq p1, final String p2, final zzbpr p3, final int p4);
    
    zzbu zze(final IObjectWrapper p0, final zzq p1, final String p2, final zzbpr p3, final int p4);
    
    zzbu zzf(final IObjectWrapper p0, final zzq p1, final String p2, final int p3);
    
    zzco zzg(final IObjectWrapper p0, final int p1);
    
    zzdj zzh(final IObjectWrapper p0, final zzbpr p1, final int p2);
    
    zzbgm zzi(final IObjectWrapper p0, final IObjectWrapper p1);
    
    zzbgs zzj(final IObjectWrapper p0, final IObjectWrapper p1, final IObjectWrapper p2);
    
    zzble zzk(final IObjectWrapper p0, final zzbpr p1, final int p2, final zzblb p3);
    
    zzbti zzl(final IObjectWrapper p0, final zzbpr p1, final int p2);
    
    zzbtp zzm(final IObjectWrapper p0);
    
    zzbwt zzn(final IObjectWrapper p0, final zzbpr p1, final int p2);
    
    zzbxj zzo(final IObjectWrapper p0, final String p1, final zzbpr p2, final int p3);
    
    zzcae zzp(final IObjectWrapper p0, final zzbpr p1, final int p2);
}

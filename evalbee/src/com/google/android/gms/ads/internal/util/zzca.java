// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import com.google.android.gms.ads.internal.zzt;
import android.content.Context;
import com.google.android.gms.internal.ads.zzcbs;

public final class zzca extends zzb
{
    private final zzcbs zza;
    private final String zzb;
    
    public zzca(final Context context, final String s, final String zzb) {
        final String zzc = zzt.zzp().zzc(context, s);
        this.zza = new zzcbs(zzc);
        this.zzb = zzb;
    }
    
    @Override
    public final void zza() {
        this.zza.zza(this.zzb);
    }
}

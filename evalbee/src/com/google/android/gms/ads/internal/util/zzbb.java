// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.ads.internal.client.zze;
import com.google.android.gms.internal.ads.zzfun;
import com.google.android.gms.internal.ads.zzffr;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "ExceptionParcelCreator")
public final class zzbb extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zzbb> CREATOR;
    @Field(id = 1)
    public final String zza;
    @Field(id = 2)
    public final int zzb;
    
    static {
        CREATOR = (Parcelable$Creator)new zzbc();
    }
    
    @Constructor
    public zzbb(@Param(id = 1) final String s, @Param(id = 2) final int zzb) {
        String zza = s;
        if (s == null) {
            zza = "";
        }
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public static zzbb zzb(final Throwable t) {
        final zze zza = zzffr.zza(t);
        String s;
        if (zzfun.zzd(t.getMessage())) {
            s = zza.zzb;
        }
        else {
            s = t.getMessage();
        }
        return new zzbb(s, zza.zza);
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        final String zza = this.zza;
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, zza, false);
        SafeParcelWriter.writeInt(parcel, 2, this.zzb);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public final zzba zza() {
        return new zzba(this.zza, this.zzb);
    }
}

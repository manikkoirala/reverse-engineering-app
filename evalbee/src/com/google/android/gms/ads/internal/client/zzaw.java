// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.BaseBundle;
import com.google.android.gms.internal.ads.zzcae;
import com.google.android.gms.internal.ads.zzbxj;
import android.content.Intent;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.internal.ads.zzbtp;
import android.app.Activity;
import com.google.android.gms.internal.ads.zzbti;
import com.google.android.gms.internal.ads.zzble;
import com.google.android.gms.ads.h5.OnH5AdsEventListener;
import com.google.android.gms.internal.ads.zzbgs;
import java.util.HashMap;
import android.view.View;
import com.google.android.gms.internal.ads.zzbgm;
import android.widget.FrameLayout;
import com.google.android.gms.internal.ads.zzbpr;
import android.os.Bundle;
import android.content.Context;
import com.google.android.gms.internal.ads.zzbuu;
import com.google.android.gms.internal.ads.zzbii;
import com.google.android.gms.internal.ads.zzbtm;
import com.google.android.gms.internal.ads.zzbxv;
import com.google.android.gms.internal.ads.zzbih;

public final class zzaw
{
    private final zzk zza;
    private final zzi zzb;
    private final zzeq zzc;
    private final zzbih zzd;
    private final zzbxv zze;
    private final zzbtm zzf;
    private final zzbii zzg;
    private zzbuu zzh;
    
    public zzaw(final zzk zza, final zzi zzb, final zzeq zzc, final zzbih zzd, final zzbxv zze, final zzbtm zzf, final zzbii zzg) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
        this.zzg = zzg;
    }
    
    public final zzbq zzc(final Context context, final String s, final zzbpr zzbpr) {
        return (zzbq)new zzao(this, context, s, zzbpr).zzd(context, false);
    }
    
    public final zzbu zzd(final Context context, final zzq zzq, final String s, final zzbpr zzbpr) {
        return (zzbu)new zzak(this, context, zzq, s, zzbpr).zzd(context, false);
    }
    
    public final zzbu zze(final Context context, final zzq zzq, final String s, final zzbpr zzbpr) {
        return (zzbu)new zzam(this, context, zzq, s, zzbpr).zzd(context, false);
    }
    
    public final zzdj zzf(final Context context, final zzbpr zzbpr) {
        return (zzdj)new zzac(this, context, zzbpr).zzd(context, false);
    }
    
    public final zzbgm zzh(final Context context, final FrameLayout frameLayout, final FrameLayout frameLayout2) {
        return (zzbgm)new zzas(this, frameLayout, frameLayout2, context).zzd(context, false);
    }
    
    public final zzbgs zzi(final View view, final HashMap hashMap, final HashMap hashMap2) {
        return (zzbgs)new zzau(this, view, hashMap, hashMap2).zzd(view.getContext(), false);
    }
    
    public final zzble zzl(final Context context, final zzbpr zzbpr, final OnH5AdsEventListener onH5AdsEventListener) {
        return (zzble)new zzai(this, context, zzbpr, onH5AdsEventListener).zzd(context, false);
    }
    
    public final zzbti zzm(final Context context, final zzbpr zzbpr) {
        return (zzbti)new zzag(this, context, zzbpr).zzd(context, false);
    }
    
    public final zzbtp zzo(final Activity activity) {
        final zzaa zzaa = new zzaa(this, activity);
        final Intent intent = activity.getIntent();
        final boolean hasExtra = intent.hasExtra("com.google.android.gms.ads.internal.overlay.useClientJar");
        boolean booleanExtra = false;
        if (!hasExtra) {
            zzcbn.zzg("useClientJar flag not found in activity intent extras.");
        }
        else {
            booleanExtra = intent.getBooleanExtra("com.google.android.gms.ads.internal.overlay.useClientJar", false);
        }
        return (zzbtp)zzaa.zzd((Context)activity, booleanExtra);
    }
    
    public final zzbxj zzq(final Context context, final String s, final zzbpr zzbpr) {
        return (zzbxj)new zzav(this, context, s, zzbpr).zzd(context, false);
    }
    
    public final zzcae zzr(final Context context, final zzbpr zzbpr) {
        return (zzcae)new zzae(this, context, zzbpr).zzd(context, false);
    }
}

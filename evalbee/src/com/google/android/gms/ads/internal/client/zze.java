// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.IInterface;
import com.google.android.gms.ads.ResponseInfo;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.AdError;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import android.os.IBinder;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "AdErrorParcelCreator")
public final class zze extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zze> CREATOR;
    @Field(id = 1)
    public final int zza;
    @Field(id = 2)
    public final String zzb;
    @Field(id = 3)
    public final String zzc;
    @Field(id = 4)
    public zze zzd;
    @Field(id = 5, type = "android.os.IBinder")
    public IBinder zze;
    
    static {
        CREATOR = (Parcelable$Creator)new zzf();
    }
    
    @Constructor
    public zze(@Param(id = 1) final int zza, @Param(id = 2) final String zzb, @Param(id = 3) final String zzc, @Param(id = 4) final zze zzd, @Param(id = 5) final IBinder zze) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = this.zza;
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, zza);
        SafeParcelWriter.writeString(parcel, 2, this.zzb, false);
        SafeParcelWriter.writeString(parcel, 3, this.zzc, false);
        SafeParcelWriter.writeParcelable(parcel, 4, (Parcelable)this.zzd, n, false);
        SafeParcelWriter.writeIBinder(parcel, 5, this.zze, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public final AdError zza() {
        final zze zzd = this.zzd;
        AdError adError;
        if (zzd == null) {
            adError = null;
        }
        else {
            adError = new AdError(zzd.zza, zzd.zzb, zzd.zzc);
        }
        return new AdError(this.zza, this.zzb, this.zzc, adError);
    }
    
    public final LoadAdError zzb() {
        final zze zzd = this.zzd;
        zzdn zzdn = null;
        AdError adError;
        if (zzd == null) {
            adError = null;
        }
        else {
            adError = new AdError(zzd.zza, zzd.zzb, zzd.zzc);
        }
        final int zza = this.zza;
        final String zzb = this.zzb;
        final String zzc = this.zzc;
        final IBinder zze = this.zze;
        if (zze != null) {
            final IInterface queryLocalInterface = zze.queryLocalInterface("com.google.android.gms.ads.internal.client.IResponseInfo");
            if (queryLocalInterface instanceof zzdn) {
                zzdn = (zzdn)queryLocalInterface;
            }
            else {
                zzdn = new zzdl(zze);
            }
        }
        return new LoadAdError(zza, zzb, zzc, adError, ResponseInfo.zza(zzdn));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ads.zzavi;
import android.os.IBinder;
import com.google.android.gms.internal.ads.zzavg;

public final class zzcg extends zzavg implements zzci
{
    public zzcg(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IFullScreenContentCallback");
    }
    
    public final void zzb() {
        this.zzbi(5, this.zza());
    }
    
    public final void zzc() {
        this.zzbi(3, this.zza());
    }
    
    public final void zzd(final zze zze) {
        final Parcel zza = this.zza();
        zzavi.zzd(zza, (Parcelable)zze);
        this.zzbi(1, zza);
    }
    
    public final void zze() {
        this.zzbi(4, this.zza());
    }
    
    public final void zzf() {
        this.zzbi(2, this.zza());
    }
}

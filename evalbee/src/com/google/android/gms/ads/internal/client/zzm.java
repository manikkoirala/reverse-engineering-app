// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.location.Location;
import com.google.android.gms.ads.RequestConfiguration;
import java.util.ArrayList;
import java.util.List;
import android.os.Bundle;

public final class zzm
{
    private Bundle zza;
    private List zzb;
    private boolean zzc;
    private int zzd;
    private final Bundle zze;
    private final Bundle zzf;
    private final List zzg;
    private int zzh;
    private String zzi;
    private final List zzj;
    private int zzk;
    private final int zzl;
    
    public zzm() {
        this.zza = new Bundle();
        this.zzb = new ArrayList();
        this.zzc = false;
        this.zzd = -1;
        this.zze = new Bundle();
        this.zzf = new Bundle();
        this.zzg = new ArrayList();
        this.zzh = -1;
        this.zzi = null;
        this.zzj = new ArrayList();
        this.zzk = 60000;
        this.zzl = RequestConfiguration.PublisherPrivacyPersonalizationState.DEFAULT.getValue();
    }
    
    public final zzl zza() {
        return new zzl(8, -1L, this.zza, -1, this.zzb, this.zzc, this.zzd, false, null, null, null, null, this.zze, this.zzf, this.zzg, null, null, false, null, this.zzh, this.zzi, this.zzj, this.zzk, null, this.zzl);
    }
    
    public final zzm zzb(final Bundle zza) {
        this.zza = zza;
        return this;
    }
    
    public final zzm zzc(final int zzk) {
        this.zzk = zzk;
        return this;
    }
    
    public final zzm zzd(final boolean zzc) {
        this.zzc = zzc;
        return this;
    }
    
    public final zzm zze(final List zzb) {
        this.zzb = zzb;
        return this;
    }
    
    public final zzm zzf(final String zzi) {
        this.zzi = zzi;
        return this;
    }
    
    public final zzm zzg(final int zzd) {
        this.zzd = zzd;
        return this;
    }
    
    public final zzm zzh(final int zzh) {
        this.zzh = zzh;
        return this;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "IconAdOptionsParcelCreator")
@Reserved({ 1 })
public final class zzdu extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zzdu> CREATOR;
    @Field(id = 2)
    public final int zza;
    
    static {
        CREATOR = (Parcelable$Creator)new zzdv();
    }
    
    @Constructor
    public zzdu(@Param(id = 2) final int zza) {
        this.zza = zza;
    }
    
    public final void writeToParcel(final Parcel parcel, int zza) {
        zza = this.zza;
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 2, zza);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

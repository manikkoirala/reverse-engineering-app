// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.IBinder;
import com.google.android.gms.internal.ads.zzbus;
import com.google.android.gms.internal.ads.zzcbq;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.ads.zzcbp;
import com.google.android.gms.internal.ads.zzcbr;
import com.google.android.gms.internal.ads.zzbcu;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.dynamic.ObjectWrapper;
import android.content.Context;

final class zzaq extends zzax
{
    final Context zza;
    final zzaw zzb;
    
    public zzaq(final zzaw zzb, final Context zza) {
        this.zzb = zzb;
        this.zza = zza;
    }
}

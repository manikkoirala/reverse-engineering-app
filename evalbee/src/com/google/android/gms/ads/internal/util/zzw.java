// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import com.google.android.gms.ads.internal.zzt;
import android.telephony.TelephonyManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.app.Activity;

public class zzw extends zzv
{
    @Override
    public final Intent zzg(final Activity activity) {
        final Intent intent = new Intent();
        intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
        intent.putExtra("android.provider.extra.APP_PACKAGE", ((Context)activity).getPackageName());
        return intent;
    }
    
    @Override
    public final void zzh(final Context context, final String s, final String s2) {
        final NotificationChannel a = eh.a("offline_notification_channel", "AdMob Offline Notifications", 2);
        sj2.a(a, false);
        fh.a((NotificationManager)context.getSystemService((Class)NotificationManager.class), a);
    }
    
    @Override
    public final boolean zzi(final Context context, final String s) {
        final NotificationChannel a = pj2.a((NotificationManager)context.getSystemService((Class)NotificationManager.class), "offline_notification_channel");
        return a != null && qj2.a(a) == 0;
    }
    
    @Override
    public final int zzj(final Context context, final TelephonyManager telephonyManager) {
        zzt.zzp();
        if (com.google.android.gms.ads.internal.util.zzt.zzz(context, "android.permission.ACCESS_NETWORK_STATE") && rj2.a(telephonyManager)) {
            return 2;
        }
        return 1;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import java.util.List;
import android.os.Parcelable;
import com.google.android.gms.internal.ads.zzavi;
import android.os.Parcel;
import android.os.IInterface;
import android.os.IBinder;
import com.google.android.gms.internal.ads.zzavh;

public abstract class zzdm extends zzavh implements zzdn
{
    public zzdm() {
        super("com.google.android.gms.ads.internal.client.IResponseInfo");
    }
    
    public static zzdn zzb(final IBinder binder) {
        if (binder == null) {
            return null;
        }
        final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.ads.internal.client.IResponseInfo");
        if (queryLocalInterface instanceof zzdn) {
            return (zzdn)queryLocalInterface;
        }
        return new zzdl(binder);
    }
    
    public final boolean zzbK(final int n, final Parcel parcel, final Parcel parcel2, final int n2) {
        String s = null;
        Label_0117: {
            Object o = null;
            switch (n) {
                default: {
                    return false;
                }
                case 6: {
                    s = this.zzh();
                    break Label_0117;
                }
                case 5: {
                    o = this.zze();
                    break;
                }
                case 4: {
                    o = this.zzf();
                    break;
                }
                case 3: {
                    final List zzj = this.zzj();
                    parcel2.writeNoException();
                    parcel2.writeTypedList(zzj);
                    return true;
                }
                case 2: {
                    s = this.zzi();
                    break Label_0117;
                }
                case 1: {
                    s = this.zzg();
                    break Label_0117;
                }
            }
            parcel2.writeNoException();
            zzavi.zze(parcel2, (Parcelable)o);
            return true;
        }
        parcel2.writeNoException();
        parcel2.writeString(s);
        return true;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.ads.zzbty;
import com.google.android.gms.internal.ads.zzbub;
import com.google.android.gms.internal.ads.zzbea;
import com.google.android.gms.internal.ads.zzbww;
import com.google.android.gms.internal.ads.zzaxm;
import android.os.Parcelable;
import com.google.android.gms.internal.ads.zzbtx;
import com.google.android.gms.internal.ads.zzbua;
import com.google.android.gms.internal.ads.zzbdz;
import com.google.android.gms.internal.ads.zzbwv;
import com.google.android.gms.internal.ads.zzaxl;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.Parcelable$Creator;
import com.google.android.gms.internal.ads.zzavi;
import android.os.Parcel;
import android.os.IInterface;
import android.os.IBinder;
import com.google.android.gms.internal.ads.zzavh;

public abstract class zzbt extends zzavh implements zzbu
{
    public zzbt() {
        super("com.google.android.gms.ads.internal.client.IAdManager");
    }
    
    public static zzbu zzac(final IBinder binder) {
        if (binder == null) {
            return null;
        }
        final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdManager");
        if (queryLocalInterface instanceof zzbu) {
            return (zzbu)queryLocalInterface;
        }
        return new zzbs(binder);
    }
    
    public final boolean zzbK(int n, final Parcel parcel, final Parcel parcel2, int zza) {
        zzbk zzbk = null;
        final zzdg zzdg = null;
        final zzby zzby = null;
        final zzcf zzcf = null;
        final zzbe zzbe = null;
        final zzcb zzcb = null;
        final zzbh zzbh = null;
        final zzci zzci = null;
        Object o = null;
        Label_1392: {
            Label_1364: {
                Label_1355: {
                    Object o2 = null;
                    Label_1120: {
                        String s = null;
                        while (true) {
                            switch (n) {
                                default: {
                                    return false;
                                }
                                case 41: {
                                    o = this.zzk();
                                    break Label_1392;
                                }
                                case 37: {
                                    o2 = this.zzd();
                                    break Label_1120;
                                }
                                case 35: {
                                    s = this.zzt();
                                    break;
                                }
                                case 33: {
                                    o = this.zzi();
                                    break Label_1392;
                                }
                                case 32: {
                                    o = this.zzj();
                                    break Label_1392;
                                }
                                case 31: {
                                    s = this.zzr();
                                    break;
                                }
                                case 26: {
                                    o = this.zzl();
                                    break Label_1392;
                                }
                                case 23: {
                                    n = (this.zzY() ? 1 : 0);
                                    break Label_1355;
                                }
                                case 18: {
                                    s = this.zzs();
                                    break;
                                }
                                case 12: {
                                    o2 = this.zzg();
                                    break Label_1120;
                                }
                                case 4: {
                                    final zzl zzl = (zzl)zzavi.zza(parcel, (Parcelable$Creator)com.google.android.gms.ads.internal.client.zzl.CREATOR);
                                    zzavi.zzc(parcel);
                                    n = (this.zzaa(zzl) ? 1 : 0);
                                    parcel2.writeNoException();
                                    break Label_1364;
                                }
                                case 3: {
                                    n = (this.zzZ() ? 1 : 0);
                                    break Label_1355;
                                }
                                case 10: {
                                    parcel2.writeNoException();
                                    return true;
                                }
                                case 45: {
                                    final IBinder strongBinder = parcel.readStrongBinder();
                                    zzci zzci2;
                                    if (strongBinder == null) {
                                        zzci2 = zzci;
                                    }
                                    else {
                                        final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IFullScreenContentCallback");
                                        if (queryLocalInterface instanceof zzci) {
                                            zzci2 = (zzci)queryLocalInterface;
                                        }
                                        else {
                                            zzci2 = new zzcg(strongBinder);
                                        }
                                    }
                                    zzavi.zzc(parcel);
                                    this.zzJ(zzci2);
                                    continue;
                                }
                                case 44: {
                                    final IObjectWrapper interface1 = IObjectWrapper.Stub.asInterface(parcel.readStrongBinder());
                                    zzavi.zzc(parcel);
                                    this.zzW(interface1);
                                    continue;
                                }
                                case 43: {
                                    final zzl zzl2 = (zzl)zzavi.zza(parcel, (Parcelable$Creator)zzl.CREATOR);
                                    final IBinder strongBinder2 = parcel.readStrongBinder();
                                    if (strongBinder2 != null) {
                                        final IInterface queryLocalInterface2 = strongBinder2.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdLoadCallback");
                                        if (queryLocalInterface2 instanceof zzbk) {
                                            zzbk = (zzbk)queryLocalInterface2;
                                        }
                                        else {
                                            zzbk = new zzbi(strongBinder2);
                                        }
                                    }
                                    zzavi.zzc(parcel);
                                    this.zzy(zzl2, zzbk);
                                    continue;
                                }
                                case 42: {
                                    final IBinder strongBinder3 = parcel.readStrongBinder();
                                    zzdg zzdg2;
                                    if (strongBinder3 == null) {
                                        zzdg2 = zzdg;
                                    }
                                    else {
                                        final IInterface queryLocalInterface3 = strongBinder3.queryLocalInterface("com.google.android.gms.ads.internal.client.IOnPaidEventListener");
                                        if (queryLocalInterface3 instanceof zzdg) {
                                            zzdg2 = (zzdg)queryLocalInterface3;
                                        }
                                        else {
                                            zzdg2 = new zzde(strongBinder3);
                                        }
                                    }
                                    zzavi.zzc(parcel);
                                    this.zzP(zzdg2);
                                    continue;
                                }
                                case 40: {
                                    final zzaxm zze = zzaxl.zze(parcel.readStrongBinder());
                                    zzavi.zzc(parcel);
                                    this.zzH(zze);
                                    continue;
                                }
                                case 39: {
                                    final zzw zzw = (zzw)zzavi.zza(parcel, (Parcelable$Creator)com.google.android.gms.ads.internal.client.zzw.CREATOR);
                                    zzavi.zzc(parcel);
                                    this.zzI(zzw);
                                    continue;
                                }
                                case 38: {
                                    final String string = parcel.readString();
                                    zzavi.zzc(parcel);
                                    this.zzR(string);
                                    continue;
                                }
                                case 36: {
                                    final IBinder strongBinder4 = parcel.readStrongBinder();
                                    zzby zzby2;
                                    if (strongBinder4 == null) {
                                        zzby2 = zzby;
                                    }
                                    else {
                                        final IInterface queryLocalInterface4 = strongBinder4.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdMetadataListener");
                                        if (queryLocalInterface4 instanceof zzby) {
                                            zzby2 = (zzby)queryLocalInterface4;
                                        }
                                        else {
                                            zzby2 = new zzbw(strongBinder4);
                                        }
                                    }
                                    zzavi.zzc(parcel);
                                    this.zzE(zzby2);
                                    continue;
                                }
                                case 34: {
                                    final boolean zzg = zzavi.zzg(parcel);
                                    zzavi.zzc(parcel);
                                    this.zzL(zzg);
                                    continue;
                                }
                                case 30: {
                                    final zzdu zzdu = (zzdu)zzavi.zza(parcel, (Parcelable$Creator)com.google.android.gms.ads.internal.client.zzdu.CREATOR);
                                    zzavi.zzc(parcel);
                                    this.zzK(zzdu);
                                    continue;
                                }
                                case 29: {
                                    final zzfl zzfl = (zzfl)zzavi.zza(parcel, (Parcelable$Creator)com.google.android.gms.ads.internal.client.zzfl.CREATOR);
                                    zzavi.zzc(parcel);
                                    this.zzU(zzfl);
                                    continue;
                                }
                                case 25: {
                                    final String string2 = parcel.readString();
                                    zzavi.zzc(parcel);
                                    this.zzT(string2);
                                    continue;
                                }
                                case 24: {
                                    final zzbww zzb = zzbwv.zzb(parcel.readStrongBinder());
                                    zzavi.zzc(parcel);
                                    this.zzS(zzb);
                                    continue;
                                }
                                case 22: {
                                    final boolean zzg2 = zzavi.zzg(parcel);
                                    zzavi.zzc(parcel);
                                    this.zzN(zzg2);
                                    continue;
                                }
                                case 21: {
                                    final IBinder strongBinder5 = parcel.readStrongBinder();
                                    zzcf zzcf2;
                                    if (strongBinder5 == null) {
                                        zzcf2 = zzcf;
                                    }
                                    else {
                                        final IInterface queryLocalInterface5 = strongBinder5.queryLocalInterface("com.google.android.gms.ads.internal.client.ICorrelationIdProvider");
                                        if (queryLocalInterface5 instanceof zzcf) {
                                            zzcf2 = (zzcf)queryLocalInterface5;
                                        }
                                        else {
                                            zzcf2 = new zzcf(strongBinder5);
                                        }
                                    }
                                    zzavi.zzc(parcel);
                                    this.zzab(zzcf2);
                                    continue;
                                }
                                case 20: {
                                    final IBinder strongBinder6 = parcel.readStrongBinder();
                                    zzbe zzbe2;
                                    if (strongBinder6 == null) {
                                        zzbe2 = zzbe;
                                    }
                                    else {
                                        final IInterface queryLocalInterface6 = strongBinder6.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdClickListener");
                                        if (queryLocalInterface6 instanceof zzbe) {
                                            zzbe2 = (zzbe)queryLocalInterface6;
                                        }
                                        else {
                                            zzbe2 = new zzbc(strongBinder6);
                                        }
                                    }
                                    zzavi.zzc(parcel);
                                    this.zzC(zzbe2);
                                    continue;
                                }
                                case 19: {
                                    final zzbea zzb2 = zzbdz.zzb(parcel.readStrongBinder());
                                    zzavi.zzc(parcel);
                                    this.zzO(zzb2);
                                    continue;
                                }
                                case 15: {
                                    final zzbub zzb3 = zzbua.zzb(parcel.readStrongBinder());
                                    final String string3 = parcel.readString();
                                    zzavi.zzc(parcel);
                                    this.zzQ(zzb3, string3);
                                    continue;
                                }
                                case 14: {
                                    final zzbty zzb4 = zzbtx.zzb(parcel.readStrongBinder());
                                    zzavi.zzc(parcel);
                                    this.zzM(zzb4);
                                    continue;
                                }
                                case 13: {
                                    final zzq zzq = (zzq)zzavi.zza(parcel, (Parcelable$Creator)com.google.android.gms.ads.internal.client.zzq.CREATOR);
                                    zzavi.zzc(parcel);
                                    this.zzF(zzq);
                                    continue;
                                }
                                case 11: {
                                    this.zzA();
                                    continue;
                                }
                                case 9: {
                                    this.zzX();
                                    continue;
                                }
                                case 8: {
                                    final IBinder strongBinder7 = parcel.readStrongBinder();
                                    zzcb zzcb2;
                                    if (strongBinder7 == null) {
                                        zzcb2 = zzcb;
                                    }
                                    else {
                                        final IInterface queryLocalInterface7 = strongBinder7.queryLocalInterface("com.google.android.gms.ads.internal.client.IAppEventListener");
                                        if (queryLocalInterface7 instanceof zzcb) {
                                            zzcb2 = (zzcb)queryLocalInterface7;
                                        }
                                        else {
                                            zzcb2 = new zzbz(strongBinder7);
                                        }
                                    }
                                    zzavi.zzc(parcel);
                                    this.zzG(zzcb2);
                                    continue;
                                }
                                case 7: {
                                    final IBinder strongBinder8 = parcel.readStrongBinder();
                                    zzbh zzbh2;
                                    if (strongBinder8 == null) {
                                        zzbh2 = zzbh;
                                    }
                                    else {
                                        final IInterface queryLocalInterface8 = strongBinder8.queryLocalInterface("com.google.android.gms.ads.internal.client.IAdListener");
                                        if (queryLocalInterface8 instanceof zzbh) {
                                            zzbh2 = (zzbh)queryLocalInterface8;
                                        }
                                        else {
                                            zzbh2 = new zzbf(strongBinder8);
                                        }
                                    }
                                    zzavi.zzc(parcel);
                                    this.zzD(zzbh2);
                                    continue;
                                }
                                case 6: {
                                    this.zzB();
                                    continue;
                                }
                                case 5: {
                                    this.zzz();
                                    continue;
                                }
                                case 2: {
                                    this.zzx();
                                    continue;
                                }
                                case 1: {
                                    o = this.zzn();
                                    break Label_1392;
                                }
                            }
                            break;
                        }
                        parcel2.writeNoException();
                        parcel2.writeString(s);
                        return true;
                    }
                    parcel2.writeNoException();
                    zzavi.zze(parcel2, (Parcelable)o2);
                    return true;
                }
                parcel2.writeNoException();
                zza = zzavi.zza;
            }
            parcel2.writeInt(n);
            return true;
        }
        parcel2.writeNoException();
        zzavi.zzf(parcel2, (IInterface)o);
        return true;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import com.google.android.gms.common.internal.Objects;

public final class zzbe
{
    public final String zza;
    public final double zzb;
    public final double zzc;
    public final double zzd;
    public final int zze;
    
    public zzbe(final String zza, final double zzc, final double zzb, final double zzd, final int zze) {
        this.zza = zza;
        this.zzc = zzc;
        this.zzb = zzb;
        this.zzd = zzd;
        this.zze = zze;
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (!(o instanceof zzbe)) {
            return false;
        }
        final zzbe zzbe = (zzbe)o;
        return Objects.equal(this.zza, zzbe.zza) && this.zzb == zzbe.zzb && this.zzc == zzbe.zzc && this.zze == zzbe.zze && Double.compare(this.zzd, zzbe.zzd) == 0;
    }
    
    @Override
    public final int hashCode() {
        return Objects.hashCode(this.zza, this.zzb, this.zzc, this.zzd, this.zze);
    }
    
    @Override
    public final String toString() {
        return Objects.toStringHelper(this).add("name", this.zza).add("minBound", this.zzc).add("maxBound", this.zzb).add("percent", this.zzd).add("count", this.zze).toString();
    }
}

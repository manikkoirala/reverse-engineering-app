// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.ads.zzbpr;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.ads.zzcbg;
import android.os.RemoteException;
import com.google.android.gms.internal.ads.zzcbn;
import java.util.List;
import java.util.Collections;
import com.google.android.gms.internal.ads.zzbmh;

public final class zzey extends zzcn
{
    private zzbmh zza;
    
    public final float zze() {
        return 1.0f;
    }
    
    public final String zzf() {
        return "";
    }
    
    public final List zzg() {
        return Collections.emptyList();
    }
    
    public final void zzh(final String s) {
    }
    
    public final void zzi() {
    }
    
    public final void zzj(final boolean b) {
    }
    
    public final void zzk() {
        zzcbn.zzg("The initialization is not processed because MobileAdsSettingsManager is not created successfully.");
        zzcbg.zza.post((Runnable)new zzex(this));
    }
    
    public final void zzl(final String s, final IObjectWrapper objectWrapper) {
    }
    
    public final void zzm(final zzda zzda) {
    }
    
    public final void zzn(final IObjectWrapper objectWrapper, final String s) {
    }
    
    public final void zzo(final zzbpr zzbpr) {
    }
    
    public final void zzp(final boolean b) {
    }
    
    public final void zzq(final float n) {
    }
    
    public final void zzr(final String s) {
    }
    
    public final void zzs(final zzbmh zza) {
        this.zza = zza;
    }
    
    public final void zzt(final String s) {
    }
    
    public final void zzu(final zzff zzff) {
    }
    
    public final boolean zzv() {
        return false;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

final class zza implements Runnable
{
    final zzb zza;
    
    public zza(final zzb zza) {
        this.zza = zza;
    }
    
    @Override
    public final void run() {
        zzb.zzc(this.zza, Thread.currentThread());
        this.zza.zza();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.internal.ads.zzanj;
import com.google.android.gms.internal.ads.zzane;

final class zzbj implements zzane
{
    final String zza;
    final zzbn zzb;
    
    public zzbj(final zzbq zzbq, final String zza, final zzbn zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public final void zza(final zzanj zzanj) {
        final String string = zzanj.toString();
        final StringBuilder sb = new StringBuilder();
        sb.append("Failed to load URL: ");
        sb.append(this.zza);
        sb.append("\n");
        sb.append(string);
        zzcbn.zzj(sb.toString());
        this.zzb.zza(null);
    }
}

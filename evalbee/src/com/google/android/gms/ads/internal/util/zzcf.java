// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import java.util.Iterator;
import com.google.android.gms.ads.internal.zzt;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.ads.internal.client.zzba;
import android.content.SharedPreferences;
import android.content.SharedPreferences$OnSharedPreferenceChangeListener;
import android.preference.PreferenceManager;
import java.util.ArrayList;
import java.util.HashMap;
import android.content.Context;
import java.util.List;
import java.util.Map;

public final class zzcf
{
    private final Map zza;
    private final List zzb;
    private final Context zzc;
    
    public zzcf(final Context zzc) {
        this.zza = new HashMap();
        this.zzb = new ArrayList();
        this.zzc = zzc;
    }
    
    public final void zzb(final String s) {
        synchronized (this) {
            if (!this.zza.containsKey(s)) {
                SharedPreferences sharedPreferences;
                if (s != "__default__" && (s == null || !s.equals("__default__"))) {
                    sharedPreferences = this.zzc.getSharedPreferences(s, 0);
                }
                else {
                    sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.zzc);
                }
                final zzce zzce = new zzce(this, s);
                this.zza.put(s, zzce);
                sharedPreferences.registerOnSharedPreferenceChangeListener((SharedPreferences$OnSharedPreferenceChangeListener)zzce);
            }
        }
    }
    
    public final void zzc() {
        if (!(boolean)zzba.zzc().zza(zzbdc.zzjJ)) {
            return;
        }
        zzt.zzp();
        final Map zzu = com.google.android.gms.ads.internal.util.zzt.zzu((String)zzba.zzc().zza(zzbdc.zzjN));
        final Iterator iterator = zzu.keySet().iterator();
        while (iterator.hasNext()) {
            this.zzb((String)iterator.next());
        }
        this.zzd(new zzcd(zzu));
    }
    
    public final void zzd(final zzcd zzcd) {
        synchronized (this) {
            this.zzb.add(zzcd);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import java.util.concurrent.Executor;
import android.app.Dialog;
import android.view.WindowManager$BadTokenException;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.ads.internal.client.zzba;
import android.view.MotionEvent;
import java.util.Iterator;
import java.util.Map;
import android.net.Uri;
import com.google.android.gms.internal.ads.zzcca;
import android.net.Uri$Builder;
import android.text.TextUtils;
import com.google.android.gms.internal.ads.zzcbn;
import android.app.Activity;
import android.content.Intent;
import android.content.DialogInterface;
import com.google.android.gms.internal.ads.zzgbl;
import android.app.AlertDialog$Builder;
import android.content.DialogInterface$OnCancelListener;
import android.content.DialogInterface$OnClickListener;
import java.util.concurrent.atomic.AtomicInteger;
import com.google.android.gms.internal.ads.zzdwj;
import java.util.List;
import java.util.ArrayList;
import com.google.android.gms.ads.internal.zzt;
import android.view.ViewConfiguration;
import android.os.Handler;
import android.graphics.PointF;
import com.google.android.gms.internal.ads.zzdwn;
import android.content.Context;

public final class zzau
{
    private final Context zza;
    private final zzdwn zzb;
    private String zzc;
    private String zzd;
    private String zze;
    private String zzf;
    private int zzg;
    private int zzh;
    private PointF zzi;
    private PointF zzj;
    private Handler zzk;
    private Runnable zzl;
    
    public zzau(final Context zza) {
        this.zzg = 0;
        this.zzl = new zzah(this);
        this.zza = zza;
        this.zzh = ViewConfiguration.get(zza).getScaledTouchSlop();
        zzt.zzt().zzb();
        this.zzk = zzt.zzt().zza();
        this.zzb = zzt.zzs().zza();
    }
    
    public zzau(final Context context, final String zzc) {
        this(context);
        this.zzc = zzc;
    }
    
    private final void zzs(final Context context) {
        final ArrayList list = new ArrayList();
        int zzu = zzu(list, "None", true);
        final int zzu2 = zzu(list, "Shake", true);
        final int zzu3 = zzu(list, "Flick", true);
        final zzdwj zza = zzdwj.zza;
        final int ordinal = ((Enum)this.zzb.zza()).ordinal();
        if (ordinal != 1) {
            if (ordinal == 2) {
                zzu = zzu3;
            }
        }
        else {
            zzu = zzu2;
        }
        zzt.zzp();
        final AlertDialog$Builder zzJ = com.google.android.gms.ads.internal.util.zzt.zzJ(context);
        final AtomicInteger atomicInteger = new AtomicInteger(zzu);
        zzJ.setTitle((CharSequence)"Setup gesture");
        zzJ.setSingleChoiceItems((CharSequence[])list.toArray(new String[0]), zzu, (DialogInterface$OnClickListener)new zzap(atomicInteger));
        zzJ.setNegativeButton((CharSequence)"Dismiss", (DialogInterface$OnClickListener)new zzaq(this));
        zzJ.setPositiveButton((CharSequence)"Save", (DialogInterface$OnClickListener)new zzar(this, atomicInteger, zzu, zzu2, zzu3));
        zzJ.setOnCancelListener((DialogInterface$OnCancelListener)new zzas(this));
        ((Dialog)zzJ.create()).show();
    }
    
    private final boolean zzt(final float n, final float n2, final float n3, final float n4) {
        return Math.abs(this.zzi.x - n) < this.zzh && Math.abs(this.zzi.y - n2) < this.zzh && Math.abs(this.zzj.x - n3) < this.zzh && Math.abs(this.zzj.y - n4) < this.zzh;
    }
    
    private static final int zzu(final List list, final String s, final boolean b) {
        if (!b) {
            return -1;
        }
        list.add(s);
        return list.size() - 1;
    }
    
    @Override
    public final String toString() {
        final StringBuilder sb = new StringBuilder(100);
        sb.append("{Dialog: ");
        sb.append(this.zzc);
        sb.append(",DebugSignal: ");
        sb.append(this.zzf);
        sb.append(",AFMA Version: ");
        sb.append(this.zze);
        sb.append(",Ad Unit ID: ");
        sb.append(this.zzd);
        sb.append("}");
        return sb.toString();
    }
    
    public final void zzm(final MotionEvent motionEvent) {
        final int actionMasked = motionEvent.getActionMasked();
        final int historySize = motionEvent.getHistorySize();
        final int pointerCount = motionEvent.getPointerCount();
        if (actionMasked == 0) {
            this.zzg = 0;
            this.zzi = new PointF(motionEvent.getX(0), motionEvent.getY(0));
            return;
        }
        final int zzg = this.zzg;
        if (zzg != -1) {
            if (zzg == 0) {
                if (actionMasked == 5) {
                    this.zzg = 5;
                    this.zzj = new PointF(motionEvent.getX(1), motionEvent.getY(1));
                    this.zzk.postDelayed(this.zzl, (long)zzba.zzc().zza(zzbdc.zzeA));
                }
            }
            else if (zzg == 5) {
                if (pointerCount == 2) {
                    if (actionMasked != 2) {
                        return;
                    }
                    int i = 0;
                    boolean b = false;
                    while (i < historySize) {
                        b |= (this.zzt(motionEvent.getHistoricalX(0, i), motionEvent.getHistoricalY(0, i), motionEvent.getHistoricalX(1, i), motionEvent.getHistoricalY(1, i)) ^ true);
                        ++i;
                    }
                    if (this.zzt(motionEvent.getX(), motionEvent.getY(), motionEvent.getX(1), motionEvent.getY(1)) && !b) {
                        return;
                    }
                }
                this.zzg = -1;
                this.zzk.removeCallbacks(this.zzl);
            }
        }
    }
    
    public final void zzn(final String zzd) {
        this.zzd = zzd;
    }
    
    public final void zzo(final String zze) {
        this.zze = zze;
    }
    
    public final void zzp(final String zzc) {
        this.zzc = zzc;
    }
    
    public final void zzq(final String zzf) {
        this.zzf = zzf;
    }
    
    public final void zzr() {
        try {
            if (!(this.zza instanceof Activity)) {
                zzcbn.zzi("Can not create dialog without Activity Context");
                return;
            }
            final boolean empty = TextUtils.isEmpty((CharSequence)zzt.zzs().zzb());
            String s = "Creative preview (enabled)";
            if (empty) {
                s = "Creative preview";
            }
            final boolean zzm = zzt.zzs().zzm();
            String s2 = "Troubleshooting (enabled)";
            if (!zzm) {
                s2 = "Troubleshooting";
            }
            final ArrayList list = new ArrayList();
            final int zzu = zzu(list, "Ad information", true);
            final int zzu2 = zzu(list, s, true);
            final int zzu3 = zzu(list, s2, true);
            final boolean booleanValue = (boolean)zzba.zzc().zza(zzbdc.zziY);
            final int zzu4 = zzu(list, "Open ad inspector", booleanValue);
            final int zzu5 = zzu(list, "Ad inspector settings", booleanValue);
            zzt.zzp();
            final AlertDialog$Builder zzJ = com.google.android.gms.ads.internal.util.zzt.zzJ(this.zza);
            zzJ.setTitle((CharSequence)"Select a debug mode").setItems((CharSequence[])list.toArray(new String[0]), (DialogInterface$OnClickListener)new zzao(this, zzu, zzu2, zzu3, zzu4, zzu5));
            ((Dialog)zzJ.create()).show();
        }
        catch (final WindowManager$BadTokenException ex) {
            com.google.android.gms.ads.internal.util.zze.zzb("", (Throwable)ex);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal;

import java.util.Iterator;
import android.net.Uri;
import android.text.TextUtils;
import java.util.Map;
import java.util.List;
import java.util.Collections;
import com.google.android.gms.internal.ads.zzbvg;
import com.google.android.gms.internal.ads.zzbyo;
import android.content.Context;

public final class zzb
{
    private final Context zza;
    private boolean zzb;
    private final zzbyo zzc;
    private final zzbvg zzd;
    
    public zzb(final Context zza, final zzbyo zzc, final zzbvg zzbvg) {
        this.zza = zza;
        this.zzc = zzc;
        this.zzd = new zzbvg(false, (List)Collections.emptyList());
    }
    
    private final boolean zzd() {
        final zzbyo zzc = this.zzc;
        return (zzc != null && zzc.zza().zzf) || this.zzd.zza;
    }
    
    public final void zza() {
        this.zzb = true;
    }
    
    public final void zzb(final String s) {
        if (this.zzd()) {
            String s2;
            if ((s2 = s) == null) {
                s2 = "";
            }
            final zzbyo zzc = this.zzc;
            if (zzc != null) {
                zzc.zzd(s2, (Map)null, 3);
                return;
            }
            final zzbvg zzd = this.zzd;
            if (zzd.zza) {
                final List zzb = zzd.zzb;
                if (zzb != null) {
                    for (final String s3 : zzb) {
                        if (!TextUtils.isEmpty((CharSequence)s3)) {
                            final String replace = s3.replace("{NAVIGATION_URL}", Uri.encode(s2));
                            final Context zza = this.zza;
                            zzt.zzp();
                            com.google.android.gms.ads.internal.util.zzt.zzK(zza, "", replace);
                        }
                    }
                }
            }
        }
    }
    
    public final boolean zzc() {
        return !this.zzd() || this.zzb;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.ads.zzavi;
import android.os.IInterface;
import android.os.Parcel;
import android.os.IBinder;
import com.google.android.gms.internal.ads.zzavg;

public final class zzdo extends zzavg implements zzdq
{
    public zzdo(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IVideoController");
    }
    
    public final float zze() {
        throw null;
    }
    
    public final float zzf() {
        throw null;
    }
    
    public final float zzg() {
        throw null;
    }
    
    public final int zzh() {
        final Parcel zzbh = this.zzbh(5, this.zza());
        final int int1 = zzbh.readInt();
        zzbh.recycle();
        return int1;
    }
    
    public final zzdt zzi() {
        final Parcel zzbh = this.zzbh(11, this.zza());
        final IBinder strongBinder = zzbh.readStrongBinder();
        zzdt zzdt;
        if (strongBinder == null) {
            zzdt = null;
        }
        else {
            final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IVideoLifecycleCallbacks");
            if (queryLocalInterface instanceof zzdt) {
                zzdt = (zzdt)queryLocalInterface;
            }
            else {
                zzdt = new zzdr(strongBinder);
            }
        }
        zzbh.recycle();
        return zzdt;
    }
    
    public final void zzj(final boolean b) {
        final Parcel zza = this.zza();
        final int zza2 = zzavi.zza;
        zza.writeInt((int)(b ? 1 : 0));
        this.zzbi(3, zza);
    }
    
    public final void zzk() {
        this.zzbi(2, this.zza());
    }
    
    public final void zzl() {
        this.zzbi(1, this.zza());
    }
    
    public final void zzm(final zzdt zzdt) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)zzdt);
        this.zzbi(8, zza);
    }
    
    public final void zzn() {
        this.zzbi(13, this.zza());
    }
    
    public final boolean zzo() {
        final Parcel zzbh = this.zzbh(12, this.zza());
        final boolean zzg = zzavi.zzg(zzbh);
        zzbh.recycle();
        return zzg;
    }
    
    public final boolean zzp() {
        final Parcel zzbh = this.zzbh(10, this.zza());
        final boolean zzg = zzavi.zzg(zzbh);
        zzbh.recycle();
        return zzg;
    }
    
    public final boolean zzq() {
        final Parcel zzbh = this.zzbh(4, this.zza());
        final boolean zzg = zzavi.zzg(zzbh);
        zzbh.recycle();
        return zzg;
    }
}

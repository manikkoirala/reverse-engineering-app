// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import java.util.concurrent.Executor;
import java.util.Iterator;
import android.os.Looper;
import com.google.android.gms.internal.ads.zzben;
import android.security.NetworkSecurityPolicy;
import com.google.android.gms.common.util.PlatformVersion;
import android.content.Context;
import org.json.JSONException;
import com.google.android.gms.ads.internal.zzt;
import org.json.JSONArray;
import android.text.TextUtils;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.ads.internal.client.zzba;
import com.google.android.gms.internal.ads.zzcca;
import com.google.android.gms.internal.ads.zzcbn;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.TimeUnit;
import java.util.Collections;
import java.util.ArrayList;
import org.json.JSONObject;
import java.util.Set;
import com.google.android.gms.internal.ads.zzcaq;
import android.content.SharedPreferences$Editor;
import android.content.SharedPreferences;
import com.google.android.gms.internal.ads.zzawp;
import java.util.List;

public final class zzj implements zzg
{
    private boolean zzA;
    private String zzB;
    private int zzC;
    private int zzD;
    private long zzE;
    private final Object zza;
    private boolean zzb;
    private final List zzc;
    private ik0 zzd;
    private zzawp zze;
    private SharedPreferences zzf;
    private SharedPreferences$Editor zzg;
    private boolean zzh;
    private String zzi;
    private String zzj;
    private boolean zzk;
    private String zzl;
    private String zzm;
    private String zzn;
    private int zzo;
    private zzcaq zzp;
    private long zzq;
    private long zzr;
    private int zzs;
    private int zzt;
    private Set zzu;
    private JSONObject zzv;
    private boolean zzw;
    private boolean zzx;
    private String zzy;
    private String zzz;
    
    public zzj() {
        this.zza = new Object();
        this.zzc = new ArrayList();
        this.zze = null;
        this.zzh = true;
        this.zzk = true;
        this.zzl = "-1";
        this.zzm = "-1";
        this.zzn = "-1";
        this.zzo = -1;
        this.zzp = new zzcaq("", 0L);
        this.zzq = 0L;
        this.zzr = 0L;
        this.zzs = -1;
        this.zzt = 0;
        this.zzu = Collections.emptySet();
        this.zzv = new JSONObject();
        this.zzw = true;
        this.zzx = true;
        this.zzy = null;
        this.zzz = "";
        this.zzA = false;
        this.zzB = "";
        this.zzC = -1;
        this.zzD = -1;
        this.zzE = 0L;
    }
    
    private final void zzT() {
        final ik0 zzd = this.zzd;
        if (zzd != null) {
            if (!zzd.isDone()) {
                try {
                    this.zzd.get(1L, TimeUnit.SECONDS);
                }
                catch (final TimeoutException zzd) {
                    goto Label_0045;
                }
                catch (final ExecutionException zzd) {
                    goto Label_0045;
                }
                catch (final CancellationException ex) {}
                catch (final InterruptedException ex2) {
                    Thread.currentThread().interrupt();
                    zzcbn.zzk("Interrupted while waiting for preferences loaded.", (Throwable)ex2);
                }
            }
        }
    }
    
    private final void zzU() {
        ((Executor)zzcca.zza).execute(new zzh(this));
    }
    
    @Override
    public final void zzA(final String s) {
        if (!(boolean)zzba.zzc().zza(zzbdc.zziY)) {
            return;
        }
        this.zzT();
        synchronized (this.zza) {
            if (this.zzB.equals(s)) {
                return;
            }
            this.zzB = s;
            final SharedPreferences$Editor zzg = this.zzg;
            if (zzg != null) {
                zzg.putString("linked_ad_unit", s);
                this.zzg.apply();
            }
            this.zzU();
        }
    }
    
    @Override
    public final void zzB(final boolean zzA) {
        if (!(boolean)zzba.zzc().zza(zzbdc.zziY)) {
            return;
        }
        this.zzT();
        synchronized (this.zza) {
            if (this.zzA == zzA) {
                return;
            }
            this.zzA = zzA;
            final SharedPreferences$Editor zzg = this.zzg;
            if (zzg != null) {
                zzg.putBoolean("linked_device", zzA);
                this.zzg.apply();
            }
            this.zzU();
        }
    }
    
    @Override
    public final void zzC(final String zzy) {
        this.zzT();
        synchronized (this.zza) {
            if (TextUtils.equals((CharSequence)this.zzy, (CharSequence)zzy)) {
                return;
            }
            this.zzy = zzy;
            final SharedPreferences$Editor zzg = this.zzg;
            if (zzg != null) {
                zzg.putString("display_cutout", zzy);
                this.zzg.apply();
            }
            this.zzU();
        }
    }
    
    @Override
    public final void zzD(final long zzr) {
        this.zzT();
        synchronized (this.zza) {
            if (this.zzr == zzr) {
                return;
            }
            this.zzr = zzr;
            final SharedPreferences$Editor zzg = this.zzg;
            if (zzg != null) {
                zzg.putLong("first_ad_req_time_ms", zzr);
                this.zzg.apply();
            }
            this.zzU();
        }
    }
    
    @Override
    public final void zzE(final int zzo) {
        this.zzT();
        synchronized (this.zza) {
            this.zzo = zzo;
            final SharedPreferences$Editor zzg = this.zzg;
            if (zzg != null) {
                if (zzo == -1) {
                    zzg.remove("gad_has_consent_for_cookies");
                }
                else {
                    zzg.putInt("gad_has_consent_for_cookies", zzo);
                }
                this.zzg.apply();
            }
            this.zzU();
        }
    }
    
    @Override
    public final void zzF(final String s, final String zzl) {
        this.zzT();
        synchronized (this.zza) {
            final int hashCode = s.hashCode();
            int n = 0;
            Label_0089: {
                if (hashCode != -2004976699) {
                    if (hashCode != 83641339) {
                        if (hashCode == 1218895378) {
                            if (s.equals("IABTCF_TCString")) {
                                n = 1;
                                break Label_0089;
                            }
                        }
                    }
                    else if (s.equals("IABTCF_gdprApplies")) {
                        n = 0;
                        break Label_0089;
                    }
                }
                else if (s.equals("IABTCF_PurposeConsents")) {
                    n = 2;
                    break Label_0089;
                }
                n = -1;
            }
            if (n != 0) {
                if (n != 1) {
                    if (n != 2) {
                        return;
                    }
                    this.zzn = zzl;
                }
                else {
                    this.zzm = zzl;
                }
            }
            else {
                this.zzl = zzl;
            }
            if (this.zzg != null) {
                if (zzl.equals("-1")) {
                    this.zzg.remove(s);
                }
                else {
                    this.zzg.putString(s, zzl);
                }
                this.zzg.apply();
            }
            this.zzU();
        }
    }
    
    @Override
    public final void zzG(final String s) {
        if (!(boolean)zzba.zzc().zza(zzbdc.zziJ)) {
            return;
        }
        this.zzT();
        synchronized (this.zza) {
            if (this.zzz.equals(s)) {
                return;
            }
            this.zzz = s;
            final SharedPreferences$Editor zzg = this.zzg;
            if (zzg != null) {
                zzg.putString("inspector_info", s);
                this.zzg.apply();
            }
            this.zzU();
        }
    }
    
    @Override
    public final void zzH(final boolean zzk) {
        this.zzT();
        synchronized (this.zza) {
            if (zzk == this.zzk) {
                return;
            }
            this.zzk = zzk;
            final SharedPreferences$Editor zzg = this.zzg;
            if (zzg != null) {
                zzg.putBoolean("gad_idless", zzk);
                this.zzg.apply();
            }
            this.zzU();
        }
    }
    
    @Override
    public final void zzI(final boolean b) {
        this.zzT();
        synchronized (this.zza) {
            final long currentTimeMillis = System.currentTimeMillis();
            final long longValue = (long)zzba.zzc().zza(zzbdc.zzjL);
            final SharedPreferences$Editor zzg = this.zzg;
            if (zzg != null) {
                zzg.putBoolean("is_topics_ad_personalization_allowed", b);
                this.zzg.putLong("topics_consent_expiry_time_ms", currentTimeMillis + longValue);
                this.zzg.apply();
            }
            this.zzU();
        }
    }
    
    @Override
    public final void zzJ(final String s, final String s2, final boolean b) {
        this.zzT();
        synchronized (this.zza) {
            JSONArray optJSONArray;
            if ((optJSONArray = this.zzv.optJSONArray(s)) == null) {
                optJSONArray = new JSONArray();
            }
            final int length = optJSONArray.length();
            int n = 0;
            int n2;
            while (true) {
                n2 = length;
                if (n >= optJSONArray.length()) {
                    break;
                }
                final JSONObject optJSONObject = optJSONArray.optJSONObject(n);
                if (optJSONObject == null) {
                    return;
                }
                if (s2.equals(optJSONObject.optString("template_id"))) {
                    if (b && optJSONObject.optBoolean("uses_media_view", false)) {
                        return;
                    }
                    n2 = n;
                    break;
                }
                else {
                    ++n;
                }
            }
            try {
                final JSONObject jsonObject = new JSONObject();
                jsonObject.put("template_id", (Object)s2);
                jsonObject.put("uses_media_view", b);
                jsonObject.put("timestamp_ms", com.google.android.gms.ads.internal.zzt.zzB().currentTimeMillis());
                optJSONArray.put(n2, (Object)jsonObject);
                this.zzv.put(s, (Object)optJSONArray);
            }
            catch (final JSONException ex) {
                zzcbn.zzk("Could not update native advanced settings", (Throwable)ex);
            }
            final SharedPreferences$Editor zzg = this.zzg;
            if (zzg != null) {
                zzg.putString("native_advanced_settings", this.zzv.toString());
                this.zzg.apply();
            }
            this.zzU();
        }
    }
    
    @Override
    public final void zzK(final int zzs) {
        this.zzT();
        synchronized (this.zza) {
            if (this.zzs == zzs) {
                return;
            }
            this.zzs = zzs;
            final SharedPreferences$Editor zzg = this.zzg;
            if (zzg != null) {
                zzg.putInt("request_in_session_count", zzs);
                this.zzg.apply();
            }
            this.zzU();
        }
    }
    
    @Override
    public final void zzL(final int zzD) {
        this.zzT();
        synchronized (this.zza) {
            if (this.zzD == zzD) {
                return;
            }
            this.zzD = zzD;
            final SharedPreferences$Editor zzg = this.zzg;
            if (zzg != null) {
                zzg.putInt("sd_app_measure_npa", zzD);
                this.zzg.apply();
            }
            this.zzU();
        }
    }
    
    @Override
    public final void zzM(final long zzE) {
        this.zzT();
        synchronized (this.zza) {
            if (this.zzE == zzE) {
                return;
            }
            this.zzE = zzE;
            final SharedPreferences$Editor zzg = this.zzg;
            if (zzg != null) {
                zzg.putLong("sd_app_measure_npa_ts", zzE);
                this.zzg.apply();
            }
            this.zzU();
        }
    }
    
    @Override
    public final boolean zzN() {
        this.zzT();
        synchronized (this.zza) {
            return this.zzw;
        }
    }
    
    @Override
    public final boolean zzO() {
        this.zzT();
        synchronized (this.zza) {
            return this.zzx;
        }
    }
    
    @Override
    public final boolean zzP() {
        this.zzT();
        synchronized (this.zza) {
            return this.zzA;
        }
    }
    
    @Override
    public final boolean zzQ() {
        if (!(boolean)zzba.zzc().zza(zzbdc.zzat)) {
            return false;
        }
        this.zzT();
        synchronized (this.zza) {
            return this.zzk;
        }
    }
    
    @Override
    public final boolean zzR() {
        this.zzT();
        synchronized (this.zza) {
            final SharedPreferences zzf = this.zzf;
            final boolean b = false;
            if (zzf == null) {
                return false;
            }
            if (zzf.getLong("topics_consent_expiry_time_ms", 0L) < System.currentTimeMillis()) {
                return false;
            }
            boolean b2 = b;
            if (this.zzf.getBoolean("is_topics_ad_personalization_allowed", false)) {
                b2 = b;
                if (!this.zzk) {
                    b2 = true;
                }
            }
            return b2;
        }
    }
    
    @Override
    public final int zza() {
        this.zzT();
        synchronized (this.zza) {
            return this.zzt;
        }
    }
    
    @Override
    public final int zzb() {
        this.zzT();
        synchronized (this.zza) {
            return this.zzo;
        }
    }
    
    @Override
    public final int zzc() {
        this.zzT();
        synchronized (this.zza) {
            return this.zzs;
        }
    }
    
    @Override
    public final long zzd() {
        this.zzT();
        synchronized (this.zza) {
            return this.zzq;
        }
    }
    
    @Override
    public final long zze() {
        this.zzT();
        synchronized (this.zza) {
            return this.zzr;
        }
    }
    
    @Override
    public final long zzf() {
        this.zzT();
        synchronized (this.zza) {
            return this.zzE;
        }
    }
    
    @Override
    public final zzawp zzg() {
        if (!this.zzb) {
            return null;
        }
        if (this.zzN() && this.zzO()) {
            return null;
        }
        if (!(boolean)zzben.zzb.zze()) {
            return null;
        }
        synchronized (this.zza) {
            if (Looper.getMainLooper() == null) {
                return null;
            }
            if (this.zze == null) {
                this.zze = new zzawp();
            }
            this.zze.zze();
            zzcbn.zzi("start fetching content...");
            return this.zze;
        }
    }
    
    @Override
    public final zzcaq zzh() {
        this.zzT();
        synchronized (this.zza) {
            if ((boolean)zzba.zzc().zza(zzbdc.zzkQ) && this.zzp.zzj()) {
                final Iterator iterator = this.zzc.iterator();
                while (iterator.hasNext()) {
                    ((Runnable)iterator.next()).run();
                }
            }
            return this.zzp;
        }
    }
    
    @Override
    public final zzcaq zzi() {
        synchronized (this.zza) {
            return this.zzp;
        }
    }
    
    @Override
    public final String zzj() {
        this.zzT();
        synchronized (this.zza) {
            return this.zzi;
        }
    }
    
    @Override
    public final String zzk() {
        this.zzT();
        synchronized (this.zza) {
            return this.zzj;
        }
    }
    
    @Override
    public final String zzl() {
        this.zzT();
        synchronized (this.zza) {
            return this.zzB;
        }
    }
    
    @Override
    public final String zzm() {
        this.zzT();
        synchronized (this.zza) {
            return this.zzy;
        }
    }
    
    @Override
    public final String zzn(String s) {
        this.zzT();
        synchronized (this.zza) {
            final int hashCode = s.hashCode();
            int n = 0;
            Label_0087: {
                if (hashCode != -2004976699) {
                    if (hashCode != 83641339) {
                        if (hashCode == 1218895378) {
                            if (s.equals("IABTCF_TCString")) {
                                n = 1;
                                break Label_0087;
                            }
                        }
                    }
                    else if (s.equals("IABTCF_gdprApplies")) {
                        n = 0;
                        break Label_0087;
                    }
                }
                else if (s.equals("IABTCF_PurposeConsents")) {
                    n = 2;
                    break Label_0087;
                }
                n = -1;
            }
            if (n == 0) {
                s = this.zzl;
                return s;
            }
            if (n == 1) {
                s = this.zzm;
                return s;
            }
            if (n != 2) {
                return null;
            }
            s = this.zzn;
            return s;
        }
    }
    
    @Override
    public final String zzo() {
        this.zzT();
        synchronized (this.zza) {
            return this.zzz;
        }
    }
    
    @Override
    public final JSONObject zzp() {
        this.zzT();
        synchronized (this.zza) {
            return this.zzv;
        }
    }
    
    @Override
    public final void zzq(final Runnable runnable) {
        this.zzc.add(runnable);
    }
    
    @Override
    public final void zzr(final Context context) {
        synchronized (this.zza) {
            if (this.zzf != null) {
                return;
            }
            monitorexit(this.zza);
            this.zzd = zzcca.zza.zza((Runnable)new zzi(this, context, "admob"));
            this.zzb = true;
        }
    }
    
    @Override
    public final void zzs() {
        this.zzT();
        synchronized (this.zza) {
            this.zzv = new JSONObject();
            final SharedPreferences$Editor zzg = this.zzg;
            if (zzg != null) {
                zzg.remove("native_advanced_settings");
                this.zzg.apply();
            }
            this.zzU();
        }
    }
    
    @Override
    public final void zzt(final long zzq) {
        this.zzT();
        synchronized (this.zza) {
            if (this.zzq == zzq) {
                return;
            }
            this.zzq = zzq;
            final SharedPreferences$Editor zzg = this.zzg;
            if (zzg != null) {
                zzg.putLong("app_last_background_time_ms", zzq);
                this.zzg.apply();
            }
            this.zzU();
        }
    }
    
    @Override
    public final void zzu(final String s) {
        this.zzT();
        synchronized (this.zza) {
            final long currentTimeMillis = com.google.android.gms.ads.internal.zzt.zzB().currentTimeMillis();
            if (s != null && !s.equals(this.zzp.zzc())) {
                this.zzp = new zzcaq(s, currentTimeMillis);
                final SharedPreferences$Editor zzg = this.zzg;
                if (zzg != null) {
                    zzg.putString("app_settings_json", s);
                    this.zzg.putLong("app_settings_last_update_ms", currentTimeMillis);
                    this.zzg.apply();
                }
                this.zzU();
                final Iterator iterator = this.zzc.iterator();
                while (iterator.hasNext()) {
                    ((Runnable)iterator.next()).run();
                }
                return;
            }
            this.zzp.zzg(currentTimeMillis);
        }
    }
    
    @Override
    public final void zzv(final int zzt) {
        this.zzT();
        synchronized (this.zza) {
            if (this.zzt == zzt) {
                return;
            }
            this.zzt = zzt;
            final SharedPreferences$Editor zzg = this.zzg;
            if (zzg != null) {
                zzg.putInt("version_code", zzt);
                this.zzg.apply();
            }
            this.zzU();
        }
    }
    
    @Override
    public final void zzw(final String zzi) {
        this.zzT();
        synchronized (this.zza) {
            if (zzi.equals(this.zzi)) {
                return;
            }
            this.zzi = zzi;
            final SharedPreferences$Editor zzg = this.zzg;
            if (zzg != null) {
                zzg.putString("content_url_hashes", zzi);
                this.zzg.apply();
            }
            this.zzU();
        }
    }
    
    @Override
    public final void zzx(final boolean zzw) {
        this.zzT();
        synchronized (this.zza) {
            if (this.zzw == zzw) {
                return;
            }
            this.zzw = zzw;
            final SharedPreferences$Editor zzg = this.zzg;
            if (zzg != null) {
                zzg.putBoolean("content_url_opted_out", zzw);
                this.zzg.apply();
            }
            this.zzU();
        }
    }
    
    @Override
    public final void zzy(final String zzj) {
        this.zzT();
        synchronized (this.zza) {
            if (zzj.equals(this.zzj)) {
                return;
            }
            this.zzj = zzj;
            final SharedPreferences$Editor zzg = this.zzg;
            if (zzg != null) {
                zzg.putString("content_vertical_hashes", zzj);
                this.zzg.apply();
            }
            this.zzU();
        }
    }
    
    @Override
    public final void zzz(final boolean zzx) {
        this.zzT();
        synchronized (this.zza) {
            if (this.zzx == zzx) {
                return;
            }
            this.zzx = zzx;
            final SharedPreferences$Editor zzg = this.zzg;
            if (zzg != null) {
                zzg.putBoolean("content_vertical_opted_out", zzx);
                this.zzg.apply();
            }
            this.zzU();
        }
    }
}

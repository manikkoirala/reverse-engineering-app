// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ads.zzavi;
import android.os.IBinder;
import com.google.android.gms.internal.ads.zzavg;

public final class zzbi extends zzavg implements zzbk
{
    public zzbi(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IAdLoadCallback");
    }
    
    public final void zzb(final zze zze) {
        final Parcel zza = this.zza();
        zzavi.zzd(zza, (Parcelable)zze);
        this.zzbi(2, zza);
    }
    
    public final void zzc() {
        this.zzbi(1, this.zza());
    }
}

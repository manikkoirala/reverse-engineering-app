// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import com.google.android.gms.internal.ads.zzchz;
import com.google.android.gms.internal.ads.zzchc;
import com.google.android.gms.internal.ads.zzefa;
import com.google.android.gms.internal.ads.zzayp;
import com.google.android.gms.internal.ads.zzcgv;
import android.webkit.WebResourceResponse;
import java.io.InputStream;
import java.util.Map;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.ads.internal.zzt;
import android.webkit.CookieManager;
import android.content.Context;

public class zzu extends zzab
{
    public zzu() {
        super(null);
    }
    
    @Override
    public final int zza() {
        return 16974374;
    }
    
    @Override
    public final CookieManager zzb(final Context context) {
        zzt.zzp();
        if (com.google.android.gms.ads.internal.util.zzt.zzE()) {
            return null;
        }
        try {
            return CookieManager.getInstance();
        }
        finally {
            final Throwable t;
            zzcbn.zzh("Failed to obtain CookieManager.", t);
            zzt.zzo().zzv(t, "ApiLevelUtil.getCookieManager");
            return null;
        }
    }
    
    @Override
    public final WebResourceResponse zzc(final String s, final String s2, final int n, final String s3, final Map map, final InputStream inputStream) {
        return new WebResourceResponse(s, s2, n, s3, map, inputStream);
    }
    
    @Override
    public final zzchc zzd(final zzcgv zzcgv, final zzayp zzayp, final boolean b, final zzefa zzefa) {
        return (zzchc)new zzchz(zzcgv, zzayp, b, zzefa);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.ads.zzcbn;
import android.os.RemoteException;
import com.google.android.gms.dynamic.ObjectWrapper;
import android.content.Context;
import android.os.IInterface;
import android.os.IBinder;
import com.google.android.gms.dynamic.RemoteCreator;

public final class zzeq extends RemoteCreator
{
    public zzeq() {
        super("com.google.android.gms.ads.MobileAdsSettingManagerCreatorImpl");
    }
    
    public final zzco zza(Context zze) {
        final RemoteCreatorException ex = null;
        try {
            zze = (RemoteCreatorException)this.getRemoteCreatorInstance((Context)zze).zze(ObjectWrapper.wrap(zze), 234310000);
            if (zze == null) {
                zze = ex;
            }
            else {
                final IInterface queryLocalInterface = ((IBinder)zze).queryLocalInterface("com.google.android.gms.ads.internal.client.IMobileAdsSettingManager");
                if (queryLocalInterface instanceof zzco) {
                    zze = (RemoteCreatorException)queryLocalInterface;
                }
                else {
                    zze = (RemoteCreatorException)new zzcm((IBinder)zze);
                }
            }
            return (zzco)zze;
        }
        catch (final RemoteCreatorException zze) {}
        catch (final RemoteException ex2) {}
        zzcbn.zzk("Could not get remote MobileAdsSettingManager.", (Throwable)zze);
        return null;
    }
}

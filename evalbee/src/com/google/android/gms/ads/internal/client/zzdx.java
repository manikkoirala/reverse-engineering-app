// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.RequestConfiguration;
import com.google.android.gms.internal.ads.zzcbg;
import android.content.Context;
import java.util.Collection;
import java.util.ArrayList;
import com.google.android.gms.ads.mediation.NetworkExtras;
import java.util.Collections;
import org.checkerframework.checker.initialization.qual.NotOnlyInitialized;
import com.google.android.gms.ads.search.SearchAdRequest;
import java.util.Map;
import android.os.Bundle;
import java.util.Set;
import java.util.List;
import java.util.Date;

public final class zzdx
{
    private final Date zza;
    private final String zzb;
    private final List zzc;
    private final int zzd;
    private final Set zze;
    private final Bundle zzf;
    private final Map zzg;
    private final String zzh;
    private final String zzi;
    @NotOnlyInitialized
    private final SearchAdRequest zzj;
    private final int zzk;
    private final Set zzl;
    private final Bundle zzm;
    private final Set zzn;
    private final boolean zzo;
    private final String zzp;
    private final int zzq;
    
    public zzdx(final zzdw zzdw, final SearchAdRequest zzj) {
        this.zza = zzdw.zzj(zzdw);
        this.zzb = zzdw.zzg(zzdw);
        this.zzc = zzdw.zzo(zzdw);
        this.zzd = zzdw.zza(zzdw);
        this.zze = Collections.unmodifiableSet((Set<?>)zzdw.zzm(zzdw));
        this.zzf = zzdw.zze(zzdw);
        this.zzg = Collections.unmodifiableMap((Map<?, ?>)zzdw.zzk(zzdw));
        this.zzh = zzdw.zzh(zzdw);
        this.zzi = zzdw.zzi(zzdw);
        this.zzj = zzj;
        this.zzk = zzdw.zzc(zzdw);
        this.zzl = Collections.unmodifiableSet((Set<?>)zzdw.zzn(zzdw));
        this.zzm = zzdw.zzd(zzdw);
        this.zzn = Collections.unmodifiableSet((Set<?>)zzdw.zzl(zzdw));
        this.zzo = zzdw.zzH(zzdw);
        this.zzp = zzdw.zzf(zzdw);
        this.zzq = zzdw.zzb(zzdw);
    }
    
    @Deprecated
    public final int zza() {
        return this.zzd;
    }
    
    public final int zzb() {
        return this.zzq;
    }
    
    public final int zzc() {
        return this.zzk;
    }
    
    public final Bundle zzd(final Class clazz) {
        final Bundle bundle = this.zzf.getBundle("com.google.android.gms.ads.mediation.customevent.CustomEventAdapter");
        if (bundle != null) {
            return bundle.getBundle(clazz.getName());
        }
        return null;
    }
    
    public final Bundle zze() {
        return this.zzm;
    }
    
    public final Bundle zzf(final Class clazz) {
        return this.zzf.getBundle(clazz.getName());
    }
    
    public final Bundle zzg() {
        return this.zzf;
    }
    
    @Deprecated
    public final NetworkExtras zzh(final Class clazz) {
        return this.zzg.get(clazz);
    }
    
    public final SearchAdRequest zzi() {
        return this.zzj;
    }
    
    public final String zzj() {
        return this.zzp;
    }
    
    public final String zzk() {
        return this.zzb;
    }
    
    public final String zzl() {
        return this.zzh;
    }
    
    public final String zzm() {
        return this.zzi;
    }
    
    @Deprecated
    public final Date zzn() {
        return this.zza;
    }
    
    public final List zzo() {
        return new ArrayList(this.zzc);
    }
    
    public final Set zzp() {
        return this.zzn;
    }
    
    public final Set zzq() {
        return this.zze;
    }
    
    @Deprecated
    public final boolean zzr() {
        return this.zzo;
    }
    
    public final boolean zzs(final Context context) {
        final RequestConfiguration zzc = zzej.zzf().zzc();
        zzay.zzb();
        final Set zzl = this.zzl;
        final String zzy = zzcbg.zzy(context);
        return zzl.contains(zzy) || zzc.getTestDeviceIds().contains(zzy);
    }
}

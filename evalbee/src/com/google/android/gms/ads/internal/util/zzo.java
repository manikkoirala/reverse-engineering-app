// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import android.app.Activity;
import com.google.android.gms.internal.ads.zzhed;
import android.net.Uri;
import android.content.Context;
import com.google.android.gms.internal.ads.zzbed;
import com.google.android.gms.internal.ads.zzbeb;

final class zzo implements zzbeb
{
    final zzbed zza;
    final Context zzb;
    final Uri zzc;
    
    public zzo(final zzt zzt, final zzbed zza, final Context zzb, final Uri zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    public final void zza() {
        final to a = new to.d(this.zza.zza()).a();
        a.a.setPackage(zzhed.zza(this.zzb));
        a.a(this.zzb, this.zzc);
        this.zza.zzf((Activity)this.zzb);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.ads.zzcbg;
import com.google.android.gms.internal.ads.zzcbn;

final class zzet extends zzbm
{
    final zzeu zza;
    
    public final String zze() {
        return null;
    }
    
    public final String zzf() {
        return null;
    }
    
    public final void zzg(final zzl zzl) {
        this.zzh(zzl, 1);
    }
    
    public final void zzh(final zzl zzl, final int n) {
        zzcbn.zzg("This app is using a lightweight version of the Google Mobile Ads SDK that requires the latest Google Play services to be installed, but Google Play services is either missing or out of date.");
        zzcbg.zza.post((Runnable)new zzer(this));
    }
    
    public final boolean zzi() {
        return false;
    }
}

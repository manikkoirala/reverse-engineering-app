// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import com.google.android.gms.internal.ads.zzavi;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.Parcel;
import com.google.android.gms.internal.ads.zzavh;

public abstract class zzbs extends zzavh implements zzbt
{
    public zzbs() {
        super("com.google.android.gms.ads.internal.util.IWorkManagerUtil");
    }
    
    public final boolean zzbK(int zzf, final Parcel parcel, final Parcel parcel2, final int n) {
        if (zzf != 1) {
            if (zzf != 2) {
                return false;
            }
            final IObjectWrapper interface1 = IObjectWrapper.Stub.asInterface(parcel.readStrongBinder());
            zzavi.zzc(parcel);
            this.zze(interface1);
            parcel2.writeNoException();
        }
        else {
            final IObjectWrapper interface2 = IObjectWrapper.Stub.asInterface(parcel.readStrongBinder());
            final String string = parcel.readString();
            final String string2 = parcel.readString();
            zzavi.zzc(parcel);
            zzf = (this.zzf(interface2, string, string2) ? 1 : 0);
            parcel2.writeNoException();
            parcel2.writeInt(zzf);
        }
        return true;
    }
}

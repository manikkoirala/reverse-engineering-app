// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.rewarded.OnAdMetadataChangedListener;

public final class zzfd extends zzdc
{
    private final OnAdMetadataChangedListener zza;
    
    public zzfd(final OnAdMetadataChangedListener zza) {
        this.zza = zza;
    }
    
    public final void zze() {
        final OnAdMetadataChangedListener zza = this.zza;
        if (zza != null) {
            zza.onAdMetadataChanged();
        }
    }
}

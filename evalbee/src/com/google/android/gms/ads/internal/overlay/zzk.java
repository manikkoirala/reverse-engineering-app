// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.overlay;

import android.os.Handler;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.BitmapDrawable;
import android.renderscript.Allocation;
import android.renderscript.ScriptIntrinsicBlur;
import android.renderscript.Element;
import android.content.Context;
import android.renderscript.RenderScript;
import android.graphics.Bitmap;
import com.google.android.gms.ads.internal.zzt;
import com.google.android.gms.ads.internal.util.zzb;

final class zzk extends zzb
{
    final zzl zza;
    
    @Override
    public final void zza() {
        final Bitmap zza = zzt.zzu().zza(this.zza.zzc.zzo.zzf);
        if (zza != null) {
            zzt.zzp();
            final zzl zza2 = this.zza;
            final com.google.android.gms.ads.internal.zzj zzo = zza2.zzc.zzo;
            final boolean zzd = zzo.zzd;
            final float zze = zzo.zze;
            final Activity zzb = zza2.zzb;
            BitmapDrawable bitmapDrawable;
            if (zzd && zze > 0.0f && zze <= 25.0f) {
                try {
                    final Bitmap scaledBitmap = Bitmap.createScaledBitmap(zza, zza.getWidth(), zza.getHeight(), false);
                    final Bitmap bitmap = Bitmap.createBitmap(scaledBitmap);
                    final RenderScript create = RenderScript.create((Context)zzb);
                    final ScriptIntrinsicBlur create2 = ScriptIntrinsicBlur.create(create, Element.U8_4(create));
                    final Allocation fromBitmap = Allocation.createFromBitmap(create, scaledBitmap);
                    final Allocation fromBitmap2 = Allocation.createFromBitmap(create, bitmap);
                    create2.setRadius(zze);
                    create2.setInput(fromBitmap);
                    create2.forEach(fromBitmap2);
                    fromBitmap2.copyTo(bitmap);
                    bitmapDrawable = new BitmapDrawable(((Context)zzb).getResources(), bitmap);
                }
                catch (final RuntimeException ex) {
                    bitmapDrawable = new BitmapDrawable(((Context)zzb).getResources(), zza);
                }
            }
            else {
                bitmapDrawable = new BitmapDrawable(((Context)zzb).getResources(), zza);
            }
            ((Handler)com.google.android.gms.ads.internal.util.zzt.zza).post((Runnable)new zzi(this, (Drawable)bitmapDrawable));
        }
    }
}

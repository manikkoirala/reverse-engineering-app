// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.Parcel;
import com.google.android.gms.internal.ads.zzavh;

public abstract class zzbd extends zzavh implements zzbe
{
    public zzbd() {
        super("com.google.android.gms.ads.internal.client.IAdClickListener");
    }
    
    public final boolean zzbK(final int n, final Parcel parcel, final Parcel parcel2, final int n2) {
        if (n == 1) {
            this.zzb();
            parcel2.writeNoException();
            return true;
        }
        return false;
    }
}

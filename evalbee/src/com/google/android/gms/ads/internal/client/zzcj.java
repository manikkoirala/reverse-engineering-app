// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.Parcelable$Creator;
import com.google.android.gms.internal.ads.zzavi;
import android.os.Parcel;
import com.google.android.gms.internal.ads.zzbpq;
import com.google.android.gms.internal.ads.zzbpr;
import android.os.IBinder;
import com.google.android.gms.internal.ads.zzavg;

public final class zzcj extends zzavg implements zzcl
{
    public zzcj(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.ILiteSdkInfo");
    }
    
    public final zzbpr getAdapterCreator() {
        final Parcel zzbh = this.zzbh(2, this.zza());
        final zzbpr zzf = zzbpq.zzf(zzbh.readStrongBinder());
        zzbh.recycle();
        return zzf;
    }
    
    public final zzen getLiteSdkVersion() {
        final Parcel zzbh = this.zzbh(1, this.zza());
        final zzen zzen = (zzen)zzavi.zza(zzbh, (Parcelable$Creator)com.google.android.gms.ads.internal.client.zzen.CREATOR);
        zzbh.recycle();
        return zzen;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import android.os.Build$VERSION;
import com.google.android.gms.ads.internal.client.zzba;
import com.google.android.gms.internal.ads.zzbdc;
import java.util.Iterator;
import android.content.IntentFilter;
import java.util.ArrayList;
import android.content.Intent;
import java.util.WeakHashMap;
import android.content.Context;
import java.util.Map;
import android.content.BroadcastReceiver;

public final class zzcm
{
    private final BroadcastReceiver zza;
    private final Map zzb;
    private boolean zzc;
    private boolean zzd;
    private Context zze;
    
    public zzcm() {
        this.zzc = false;
        this.zzb = new WeakHashMap();
        this.zza = new zzcl(this);
    }
    
    private final void zze(final Context context, final Intent intent) {
        synchronized (this) {
            final ArrayList list = new ArrayList();
            for (final Map.Entry<K, IntentFilter> entry : this.zzb.entrySet()) {
                if (entry.getValue().hasAction(intent.getAction())) {
                    list.add(entry.getKey());
                }
            }
            for (int size = list.size(), i = 0; i < size; ++i) {
                ((BroadcastReceiver)list.get(i)).onReceive(context, intent);
            }
        }
    }
    
    public final void zzb(final Context zze) {
        synchronized (this) {
            if (this.zzc) {
                return;
            }
            if ((this.zze = zze.getApplicationContext()) == null) {
                this.zze = zze;
            }
            zzbdc.zza(this.zze);
            this.zzd = (boolean)zzba.zzc().zza(zzbdc.zzdG);
            final IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.SCREEN_ON");
            intentFilter.addAction("android.intent.action.SCREEN_OFF");
            intentFilter.addAction("android.intent.action.USER_PRESENT");
            if ((boolean)zzba.zzc().zza(zzbdc.zzkj) && Build$VERSION.SDK_INT >= 33) {
                ze2.a(this.zze, this.zza, intentFilter, 4);
            }
            else {
                this.zze.registerReceiver(this.zza, intentFilter);
            }
            this.zzc = true;
        }
    }
    
    public final void zzc(final Context context, final BroadcastReceiver broadcastReceiver, final IntentFilter intentFilter) {
        synchronized (this) {
            if (this.zzd) {
                this.zzb.put(broadcastReceiver, intentFilter);
                return;
            }
            zzbdc.zza(context);
            if ((boolean)zzba.zzc().zza(zzbdc.zzkj) && Build$VERSION.SDK_INT >= 33) {
                ze2.a(context, broadcastReceiver, intentFilter, 4);
                return;
            }
            context.registerReceiver(broadcastReceiver, intentFilter);
        }
    }
    
    public final void zzd(final Context context, final BroadcastReceiver broadcastReceiver) {
        synchronized (this) {
            if (this.zzd) {
                this.zzb.remove(broadcastReceiver);
                return;
            }
            context.unregisterReceiver(broadcastReceiver);
        }
    }
}

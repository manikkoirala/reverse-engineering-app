// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.overlay;

import android.os.BaseBundle;
import com.google.android.gms.internal.ads.zzbmx;
import android.os.Handler;
import com.google.android.gms.internal.ads.zzbss;
import com.google.android.gms.internal.ads.zzcyu;
import android.os.Bundle;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.content.Intent;
import com.google.android.gms.internal.ads.zzbti;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.util.Map;
import java.util.Collections;
import com.google.android.gms.internal.ads.zzfqv;
import com.google.android.gms.internal.ads.zzefb;
import android.view.ViewParent;
import com.google.android.gms.internal.ads.zzbiv;
import com.google.android.gms.internal.ads.zzbit;
import com.google.android.gms.internal.ads.zzcbt;
import com.google.android.gms.internal.ads.zzcik;
import com.google.android.gms.internal.ads.zzcii;
import com.google.android.gms.ads.internal.zzb;
import android.os.RemoteException;
import com.google.android.gms.internal.ads.zzefc;
import android.view.ViewGroup$LayoutParams;
import android.widget.RelativeLayout$LayoutParams;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import com.google.android.gms.internal.ads.zzcig;
import com.google.android.gms.internal.ads.zzcpo;
import com.google.android.gms.internal.ads.zzbkp;
import com.google.android.gms.internal.ads.zzbkv;
import com.google.android.gms.internal.ads.zzdge;
import com.google.android.gms.internal.ads.zzbkw;
import com.google.android.gms.internal.ads.zzfje;
import com.google.android.gms.internal.ads.zzdtp;
import com.google.android.gms.internal.ads.zzfla;
import com.google.android.gms.internal.ads.zzeep;
import com.google.android.gms.internal.ads.zzbyo;
import com.google.android.gms.internal.ads.zzbst;
import com.google.android.gms.internal.ads.zzbkf;
import com.google.android.gms.ads.internal.client.zza;
import com.google.android.gms.internal.ads.zzefa;
import com.google.android.gms.internal.ads.zzfdy;
import com.google.android.gms.internal.ads.zzfdu;
import com.google.android.gms.internal.ads.zzbdu;
import com.google.android.gms.internal.ads.zzbee;
import com.google.android.gms.internal.ads.zzasi;
import com.google.android.gms.internal.ads.zzchh;
import com.google.android.gms.internal.ads.zzayp;
import com.google.android.gms.internal.ads.zzcbn;
import android.content.Context;
import android.os.Build$VERSION;
import com.google.android.gms.internal.ads.zzflf;
import android.view.View;
import android.view.Window;
import com.google.android.gms.ads.internal.zzj;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.ads.internal.client.zzba;
import com.google.android.gms.ads.internal.zzt;
import android.content.res.Configuration;
import android.graphics.Color;
import android.widget.TextView;
import android.webkit.WebChromeClient$CustomViewCallback;
import android.widget.FrameLayout;
import com.google.android.gms.internal.ads.zzcgv;
import android.app.Activity;
import com.google.android.gms.internal.ads.zzbto;

public class zzl extends zzbto implements zzad
{
    static final int zza;
    protected final Activity zzb;
    AdOverlayInfoParcel zzc;
    zzcgv zzd;
    zzh zze;
    zzr zzf;
    boolean zzg;
    FrameLayout zzh;
    WebChromeClient$CustomViewCallback zzi;
    boolean zzj;
    boolean zzk;
    zzg zzl;
    boolean zzm;
    int zzn;
    private final Object zzo;
    private Runnable zzp;
    private boolean zzq;
    private boolean zzr;
    private boolean zzs;
    private boolean zzt;
    private boolean zzu;
    private TextView zzv;
    
    static {
        zza = Color.argb(0, 0, 0, 0);
    }
    
    public zzl(final Activity zzb) {
        this.zzg = false;
        this.zzj = false;
        this.zzk = false;
        this.zzm = false;
        this.zzn = 1;
        this.zzo = new Object();
        this.zzs = false;
        this.zzt = false;
        this.zzu = true;
        this.zzb = zzb;
    }
    
    private final void zzJ(final Configuration configuration) {
        final AdOverlayInfoParcel zzc = this.zzc;
        final int n = 1;
        final int n2 = 0;
        boolean b = false;
        Label_0044: {
            if (zzc != null) {
                final zzj zzo = zzc.zzo;
                if (zzo != null && zzo.zzb) {
                    b = true;
                    break Label_0044;
                }
            }
            b = false;
        }
        final boolean zze = com.google.android.gms.ads.internal.zzt.zzq().zze(this.zzb, configuration);
        int n3;
        int n4;
        if ((!this.zzk || b || (boolean)zzba.zzc().zza(zzbdc.zzaE)) && (!zze || (boolean)zzba.zzc().zza(zzbdc.zzaD))) {
            final AdOverlayInfoParcel zzc2 = this.zzc;
            n3 = n;
            n4 = n2;
            if (zzc2 != null) {
                final zzj zzo2 = zzc2.zzo;
                n3 = n;
                n4 = n2;
                if (zzo2 != null) {
                    n3 = n;
                    n4 = n2;
                    if (zzo2.zzg) {
                        n4 = 1;
                        n3 = n;
                    }
                }
            }
        }
        else {
            n3 = 0;
            n4 = n2;
        }
        final Window window = this.zzb.getWindow();
        if (zzba.zzc().zza(zzbdc.zzbd)) {
            final View decorView = window.getDecorView();
            int systemUiVisibility;
            if (n3 != 0) {
                if (n4 != 0) {
                    systemUiVisibility = 5894;
                }
                else {
                    systemUiVisibility = 5380;
                }
            }
            else {
                systemUiVisibility = 256;
            }
            decorView.setSystemUiVisibility(systemUiVisibility);
            return;
        }
        if (n3 != 0) {
            window.addFlags(1024);
            window.clearFlags(2048);
            if (n4 != 0) {
                window.getDecorView().setSystemUiVisibility(4098);
            }
            return;
        }
        window.addFlags(2048);
        window.clearFlags(1024);
    }
    
    private static final void zzK(final zzflf zzflf, final View view) {
        if (zzflf != null && view != null) {
            zzt.zzA().zzh(zzflf, view);
        }
    }
    
    public final void zzA(final int requestedOrientation) {
        if (((Context)this.zzb).getApplicationInfo().targetSdkVersion >= (int)zzba.zzc().zza(zzbdc.zzfX) && ((Context)this.zzb).getApplicationInfo().targetSdkVersion <= (int)zzba.zzc().zza(zzbdc.zzfY)) {
            final int sdk_INT = Build$VERSION.SDK_INT;
            if (sdk_INT >= (int)zzba.zzc().zza(zzbdc.zzfZ)) {
                if (sdk_INT <= (int)zzba.zzc().zza(zzbdc.zzga)) {
                    return;
                }
            }
        }
        try {
            this.zzb.setRequestedOrientation(requestedOrientation);
        }
        finally {
            final Throwable t;
            com.google.android.gms.ads.internal.zzt.zzo().zzv(t, "AdOverlay.setRequestedOrientation");
        }
    }
    
    public final void zzB(final boolean b) {
        zzg zzg;
        int backgroundColor;
        if (b) {
            zzg = this.zzl;
            backgroundColor = 0;
        }
        else {
            zzg = this.zzl;
            backgroundColor = -16777216;
        }
        ((View)zzg).setBackgroundColor(backgroundColor);
    }
    
    public final void zzC(final View view, final WebChromeClient$CustomViewCallback zzi) {
        ((View)(this.zzh = new FrameLayout((Context)this.zzb))).setBackgroundColor(-16777216);
        ((ViewGroup)this.zzh).addView(view, -1, -1);
        this.zzb.setContentView((View)this.zzh);
        this.zzr = true;
        this.zzi = zzi;
        this.zzg = true;
    }
    
    public final void zzD(final boolean b) {
        if (!this.zzr) {
            this.zzb.requestWindowFeature(1);
        }
        final Window window = this.zzb.getWindow();
        if (window == null) {
            throw new zzf("Invalid activity, no window available.");
        }
        final zzcgv zzd = this.zzc.zzd;
        final zzb zzb = null;
        zzcii zzN;
        if (zzd != null) {
            zzN = zzd.zzN();
        }
        else {
            zzN = null;
        }
        final boolean b2 = zzN != null && zzN.zzK();
        this.zzm = false;
        boolean b3 = false;
        Label_0170: {
            Label_0168: {
                if (b2) {
                    final int zzj = this.zzc.zzj;
                    Label_0134: {
                        Label_0132: {
                            if (zzj == 6) {
                                if (((Context)this.zzb).getResources().getConfiguration().orientation != 1) {
                                    break Label_0132;
                                }
                            }
                            else {
                                if (zzj != 7) {
                                    break Label_0168;
                                }
                                if (((Context)this.zzb).getResources().getConfiguration().orientation != 2) {
                                    break Label_0132;
                                }
                            }
                            b3 = true;
                            break Label_0134;
                        }
                        b3 = false;
                    }
                    this.zzm = b3;
                    break Label_0170;
                }
            }
            b3 = false;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Delay onShow to next orientation change: ");
        sb.append(b3);
        zzcbn.zze(sb.toString());
        this.zzA(this.zzc.zzj);
        window.setFlags(16777216, 16777216);
        zzcbn.zze("Hardware acceleration on the AdActivity window enabled.");
        zzg zzg;
        int zza;
        if (!this.zzk) {
            zzg = this.zzl;
            zza = -16777216;
        }
        else {
            zzg = this.zzl;
            zza = com.google.android.gms.ads.internal.overlay.zzl.zza;
        }
        ((View)zzg).setBackgroundColor(zza);
        this.zzb.setContentView((View)this.zzl);
        this.zzr = true;
        Label_0698: {
            if (b) {
                try {
                    com.google.android.gms.ads.internal.zzt.zzz();
                    final Activity zzb2 = this.zzb;
                    final zzcgv zzd2 = this.zzc.zzd;
                    zzcik zzO;
                    if (zzd2 != null) {
                        zzO = zzd2.zzO();
                    }
                    else {
                        zzO = null;
                    }
                    final zzcgv zzd3 = this.zzc.zzd;
                    String zzS;
                    if (zzd3 != null) {
                        zzS = zzd3.zzS();
                    }
                    else {
                        zzS = null;
                    }
                    final AdOverlayInfoParcel zzc = this.zzc;
                    final zzcbt zzm = zzc.zzm;
                    final zzcgv zzd4 = zzc.zzd;
                    com.google.android.gms.ads.internal.zza zzj2;
                    if (zzd4 != null) {
                        zzj2 = zzd4.zzj();
                    }
                    else {
                        zzj2 = null;
                    }
                    final zzcgv zza2 = zzchh.zza((Context)zzb2, zzO, zzS, true, b2, (zzasi)null, (zzbee)null, zzm, (zzbdu)null, (com.google.android.gms.ads.internal.zzl)null, zzj2, zzayp.zza(), (zzfdu)null, (zzfdy)null, (zzefa)null);
                    this.zzd = zza2;
                    final zzcii zzN2 = zza2.zzN();
                    final AdOverlayInfoParcel zzc2 = this.zzc;
                    final zzbit zzp = zzc2.zzp;
                    final zzbiv zze = zzc2.zze;
                    final zzz zzi = zzc2.zzi;
                    final zzcgv zzd5 = zzc2.zzd;
                    zzb zzd6 = zzb;
                    if (zzd5 != null) {
                        zzd6 = zzd5.zzN().zzd();
                    }
                    zzN2.zzM((zza)null, zzp, (zzo)null, zze, zzi, true, (zzbkf)null, zzd6, (zzbst)null, (zzbyo)null, (zzeep)null, (zzfla)null, (zzdtp)null, (zzfje)null, (zzbkw)null, (zzdge)null, (zzbkv)null, (zzbkp)null, (zzcpo)null);
                    this.zzd.zzN().zzA((zzcig)new zze(this));
                    final AdOverlayInfoParcel zzc3 = this.zzc;
                    final String zzl = zzc3.zzl;
                    if (zzl != null) {
                        this.zzd.loadUrl(zzl);
                    }
                    else {
                        final String zzh = zzc3.zzh;
                        if (zzh == null) {
                            throw new zzf("No URL or HTML to display in ad overlay.");
                        }
                        this.zzd.loadDataWithBaseURL(zzc3.zzf, zzh, "text/html", "UTF-8", (String)null);
                    }
                    final zzcgv zzd7 = this.zzc.zzd;
                    if (zzd7 != null) {
                        zzd7.zzar(this);
                    }
                    break Label_0698;
                }
                catch (final Exception ex) {
                    zzcbn.zzh("Error obtaining webview.", (Throwable)ex);
                    throw new zzf("Could not obtain webview for the overlay.", ex);
                }
            }
            (this.zzd = this.zzc.zzd).zzak((Context)this.zzb);
        }
        if (this.zzc.zzw) {
            CookieManager.getInstance().setAcceptThirdPartyCookies(this.zzd.zzG(), false);
        }
        this.zzd.zzaf(this);
        final zzcgv zzd8 = this.zzc.zzd;
        if (zzd8 != null) {
            zzK(zzd8.zzQ(), (View)this.zzl);
        }
        if (this.zzc.zzk != 5) {
            final ViewParent parent = this.zzd.getParent();
            if (parent != null && parent instanceof ViewGroup) {
                ((ViewGroup)parent).removeView(this.zzd.zzF());
            }
            if (this.zzk) {
                this.zzd.zzaj();
            }
            if (this.zzc.zzw) {
                ((View)(this.zzv = new TextView((Context)this.zzb))).setId(View.generateViewId());
                this.zzd.zzF().setId(View.generateViewId());
                this.zzv.setTextAppearance((Context)this.zzb, 16973894);
                this.zzv.setMinHeight(50);
                final RelativeLayout$LayoutParams relativeLayout$LayoutParams = new RelativeLayout$LayoutParams(-2, -2);
                relativeLayout$LayoutParams.addRule(10);
                relativeLayout$LayoutParams.addRule(9);
                this.zzv.setGravity(8388627);
                ((ViewGroup)this.zzl).addView((View)this.zzv, (ViewGroup$LayoutParams)relativeLayout$LayoutParams);
                final RelativeLayout$LayoutParams relativeLayout$LayoutParams2 = new RelativeLayout$LayoutParams(-2, -2);
                relativeLayout$LayoutParams2.addRule(3, ((View)this.zzv).getId());
                relativeLayout$LayoutParams2.addRule(12);
                ((ViewGroup)this.zzl).addView(this.zzd.zzF(), (ViewGroup$LayoutParams)relativeLayout$LayoutParams2);
            }
            else {
                ((ViewGroup)this.zzl).addView(this.zzd.zzF(), -1, -1);
            }
        }
        if (!b && !this.zzm) {
            this.zze();
        }
        if (this.zzc.zzk != 5) {
            this.zzw(b2);
            if (this.zzd.zzaw()) {
                this.zzy(b2, true);
            }
            return;
        }
        final Activity zzb3 = this.zzb;
        final zzefb zze2 = zzefc.zze();
        zze2.zza(zzb3);
        zze2.zzb(this);
        zze2.zzc(this.zzc.zzq);
        zze2.zzd(this.zzc.zzr);
        final zzefc zze3 = zze2.zze();
        try {
            this.zzf(zze3);
            return;
        }
        catch (final RemoteException zze3) {}
        catch (final zzf zzf) {}
        throw new zzf(((Throwable)zze3).getMessage(), (Throwable)zze3);
    }
    
    public final void zzE() {
        synchronized (this.zzo) {
            this.zzq = true;
            final Runnable zzp = this.zzp;
            if (zzp != null) {
                final zzfqv zza = com.google.android.gms.ads.internal.util.zzt.zza;
                ((Handler)zza).removeCallbacks(zzp);
                ((Handler)zza).post(this.zzp);
            }
        }
    }
    
    public final void zzF() {
        if (this.zzb.isFinishing()) {
            if (!this.zzs) {
                this.zzs = true;
                final zzcgv zzd = this.zzd;
                if (zzd != null) {
                    zzd.zzW(this.zzn - 1);
                    synchronized (this.zzo) {
                        if (!this.zzq && this.zzd.zzax()) {
                            if ((boolean)zzba.zzc().zza(zzbdc.zzeH) && !this.zzt) {
                                final AdOverlayInfoParcel zzc = this.zzc;
                                if (zzc != null) {
                                    final zzo zzc2 = zzc.zzc;
                                    if (zzc2 != null) {
                                        zzc2.zzbv();
                                    }
                                }
                            }
                            final zzd zzp = new zzd(this);
                            this.zzp = zzp;
                            ((Handler)com.google.android.gms.ads.internal.util.zzt.zza).postDelayed((Runnable)zzp, (long)zzba.zzc().zza(zzbdc.zzaW));
                            return;
                        }
                    }
                }
                this.zzc();
            }
        }
    }
    
    public final void zzG(final String text) {
        final TextView zzv = this.zzv;
        if (zzv != null) {
            zzv.setText((CharSequence)text);
        }
    }
    
    public final boolean zzH() {
        this.zzn = 1;
        if (this.zzd == null) {
            return true;
        }
        if ((boolean)zzba.zzc().zza(zzbdc.zziJ) && this.zzd.canGoBack()) {
            this.zzd.goBack();
            return false;
        }
        final boolean zzaC = this.zzd.zzaC();
        if (!zzaC) {
            ((zzbmx)this.zzd).zzd("onbackblocked", (Map)Collections.emptyMap());
        }
        return zzaC;
    }
    
    public final void zzb() {
        this.zzn = 3;
        this.zzb.finish();
        final AdOverlayInfoParcel zzc = this.zzc;
        if (zzc != null && zzc.zzk == 5) {
            this.zzb.overridePendingTransition(0, 0);
        }
    }
    
    public final void zzc() {
        if (!this.zzt) {
            this.zzt = true;
            final zzcgv zzd = this.zzd;
            if (zzd != null) {
                ((ViewGroup)this.zzl).removeView(zzd.zzF());
                final zzh zze = this.zze;
                if (zze != null) {
                    this.zzd.zzak(zze.zzd);
                    this.zzd.zzan(false);
                    final ViewGroup zzc = this.zze.zzc;
                    final View zzF = this.zzd.zzF();
                    final zzh zze2 = this.zze;
                    zzc.addView(zzF, zze2.zza, zze2.zzb);
                    this.zze = null;
                }
                else if (((Context)this.zzb).getApplicationContext() != null) {
                    this.zzd.zzak(((Context)this.zzb).getApplicationContext());
                }
                this.zzd = null;
            }
            final AdOverlayInfoParcel zzc2 = this.zzc;
            if (zzc2 != null) {
                final zzo zzc3 = zzc2.zzc;
                if (zzc3 != null) {
                    zzc3.zzbz(this.zzn);
                }
            }
            final AdOverlayInfoParcel zzc4 = this.zzc;
            if (zzc4 != null) {
                final zzcgv zzd2 = zzc4.zzd;
                if (zzd2 != null) {
                    zzK(zzd2.zzQ(), this.zzc.zzd.zzF());
                }
            }
        }
    }
    
    public final void zzd() {
        this.zzl.zzb = true;
    }
    
    public final void zze() {
        this.zzd.zzX();
    }
    
    public final void zzf(final zzefc zzefc) {
        final AdOverlayInfoParcel zzc = this.zzc;
        if (zzc != null) {
            final zzbti zzv = zzc.zzv;
            if (zzv != null) {
                zzv.zzg(ObjectWrapper.wrap(zzefc));
                return;
            }
        }
        throw new zzf("noioou");
    }
    
    public final void zzg() {
        final AdOverlayInfoParcel zzc = this.zzc;
        if (zzc != null && this.zzg) {
            this.zzA(zzc.zzj);
        }
        if (this.zzh != null) {
            this.zzb.setContentView((View)this.zzl);
            this.zzr = true;
            ((ViewGroup)this.zzh).removeAllViews();
            this.zzh = null;
        }
        final WebChromeClient$CustomViewCallback zzi = this.zzi;
        if (zzi != null) {
            zzi.onCustomViewHidden();
            this.zzi = null;
        }
        this.zzg = false;
    }
    
    public final void zzh(final int n, final int n2, final Intent intent) {
    }
    
    public final void zzi() {
        this.zzn = 1;
    }
    
    public final void zzj() {
        this.zzn = 2;
        this.zzb.finish();
    }
    
    public final void zzk(final IObjectWrapper objectWrapper) {
        this.zzJ(ObjectWrapper.unwrap(objectWrapper));
    }
    
    public void zzl(final Bundle bundle) {
        if (!this.zzr) {
            this.zzb.requestWindowFeature(1);
        }
        this.zzj = (bundle != null && ((BaseBundle)bundle).getBoolean("com.google.android.gms.ads.internal.overlay.hasResumed", false));
        try {
            final AdOverlayInfoParcel zza = AdOverlayInfoParcel.zza(this.zzb.getIntent());
            this.zzc = zza;
            if (zza == null) {
                throw new zzf("Could not get info for ad overlay.");
            }
            if (zza.zzw) {
                if (Build$VERSION.SDK_INT >= 28) {
                    fg2.a(this.zzb, true);
                }
                else {
                    this.zzb.getWindow().addFlags(524288);
                }
            }
            if (this.zzc.zzm.zzc > 7500000) {
                this.zzn = 4;
            }
            if (this.zzb.getIntent() != null) {
                this.zzu = this.zzb.getIntent().getBooleanExtra("shouldCallOnOverlayOpened", true);
            }
            final AdOverlayInfoParcel zzc = this.zzc;
            final zzj zzo = zzc.zzo;
            Label_0249: {
                if (zzo != null) {
                    final boolean zza2 = zzo.zza;
                    this.zzk = zza2;
                    if (!zza2) {
                        break Label_0249;
                    }
                }
                else {
                    if (zzc.zzk != 5) {
                        this.zzk = false;
                        break Label_0249;
                    }
                    this.zzk = true;
                }
                if (zzc.zzk != 5 && zzo.zzf != -1) {
                    new zzk(this, null).zzb();
                }
            }
            if (bundle == null) {
                if (this.zzu) {
                    final zzcyu zzt = this.zzc.zzt;
                    if (zzt != null) {
                        zzt.zze();
                    }
                    final zzo zzc2 = this.zzc.zzc;
                    if (zzc2 != null) {
                        zzc2.zzbw();
                    }
                }
                final AdOverlayInfoParcel zzc3 = this.zzc;
                if (zzc3.zzk != 1) {
                    final zza zzb = zzc3.zzb;
                    if (zzb != null) {
                        zzb.onAdClicked();
                    }
                    final zzdge zzu = this.zzc.zzu;
                    if (zzu != null) {
                        zzu.zzbL();
                    }
                }
            }
            final Activity zzb2 = this.zzb;
            final AdOverlayInfoParcel zzc4 = this.zzc;
            ((View)(this.zzl = new zzg((Context)zzb2, zzc4.zzn, zzc4.zzm.zza, zzc4.zzs))).setId(1000);
            com.google.android.gms.ads.internal.zzt.zzq().zzl(this.zzb);
            final AdOverlayInfoParcel zzc5 = this.zzc;
            final int zzk = zzc5.zzk;
            if (zzk == 1) {
                this.zzD(false);
                return;
            }
            if (zzk == 2) {
                this.zze = new zzh(zzc5.zzd);
                this.zzD(false);
                return;
            }
            if (zzk == 3) {
                this.zzD(true);
                return;
            }
            if (zzk == 5) {
                this.zzD(false);
                return;
            }
            throw new zzf("Could not determine ad overlay type.");
        }
        catch (final zzf zzf) {
            zzcbn.zzj(zzf.getMessage());
            this.zzn = 4;
            this.zzb.finish();
        }
    }
    
    public final void zzm() {
        final zzcgv zzd = this.zzd;
        while (true) {
            if (zzd == null) {
                break Label_0022;
            }
            try {
                ((ViewGroup)this.zzl).removeView(zzd.zzF());
                this.zzF();
            }
            catch (final NullPointerException ex) {
                continue;
            }
            break;
        }
    }
    
    public final void zzn() {
        if (this.zzm) {
            this.zzm = false;
            this.zze();
        }
    }
    
    public final void zzo() {
        this.zzg();
        final AdOverlayInfoParcel zzc = this.zzc;
        if (zzc != null) {
            final zzo zzc2 = zzc.zzc;
            if (zzc2 != null) {
                zzc2.zzbp();
            }
        }
        if (!(boolean)zzba.zzc().zza(zzbdc.zzeJ) && this.zzd != null && (!this.zzb.isFinishing() || this.zze == null)) {
            this.zzd.onPause();
        }
        this.zzF();
    }
    
    public final void zzp(final int n, final String[] array, final int[] array2) {
        if (n != 12345) {
            return;
        }
        final Activity zzb = this.zzb;
        final zzefb zze = zzefc.zze();
        zze.zza(zzb);
        zzl zzl;
        if (this.zzc.zzk == 5) {
            zzl = this;
        }
        else {
            zzl = null;
        }
        zze.zzb(zzl);
        final zzefc zze2 = zze.zze();
        try {
            this.zzc.zzv.zzf(array, array2, ObjectWrapper.wrap(zze2));
        }
        catch (final RemoteException ex) {}
    }
    
    public final void zzq() {
    }
    
    public final void zzr() {
        final AdOverlayInfoParcel zzc = this.zzc;
        if (zzc != null) {
            final zzo zzc2 = zzc.zzc;
            if (zzc2 != null) {
                zzc2.zzbM();
            }
        }
        this.zzJ(((Context)this.zzb).getResources().getConfiguration());
        if (!(boolean)zzba.zzc().zza(zzbdc.zzeJ)) {
            final zzcgv zzd = this.zzd;
            if (zzd != null && !zzd.zzaz()) {
                this.zzd.onResume();
                return;
            }
            zzcbn.zzj("The webview does not exist. Ignoring action.");
        }
    }
    
    public final void zzs(final Bundle bundle) {
        ((BaseBundle)bundle).putBoolean("com.google.android.gms.ads.internal.overlay.hasResumed", this.zzj);
    }
    
    public final void zzt() {
        if (zzba.zzc().zza(zzbdc.zzeJ)) {
            final zzcgv zzd = this.zzd;
            if (zzd != null && !zzd.zzaz()) {
                this.zzd.onResume();
                return;
            }
            zzcbn.zzj("The webview does not exist. Ignoring action.");
        }
    }
    
    public final void zzu() {
        if ((boolean)zzba.zzc().zza(zzbdc.zzeJ) && this.zzd != null && (!this.zzb.isFinishing() || this.zze == null)) {
            this.zzd.onPause();
        }
        this.zzF();
    }
    
    public final void zzv() {
        final AdOverlayInfoParcel zzc = this.zzc;
        if (zzc != null) {
            final zzo zzc2 = zzc.zzc;
            if (zzc2 != null) {
                zzc2.zzby();
            }
        }
    }
    
    public final void zzw(final boolean b) {
        final int intValue = (int)zzba.zzc().zza(zzbdc.zzeM);
        final boolean booleanValue = (boolean)zzba.zzc().zza(zzbdc.zzaZ);
        final int n = 0;
        final boolean b2 = booleanValue || b;
        final zzq zzq = new zzq();
        zzq.zzd = 50;
        int zza;
        if (!b2) {
            zza = 0;
        }
        else {
            zza = intValue;
        }
        zzq.zza = zza;
        int zzb = n;
        if (!b2) {
            zzb = intValue;
        }
        zzq.zzb = zzb;
        zzq.zzc = intValue;
        this.zzf = new zzr((Context)this.zzb, zzq, this);
        final RelativeLayout$LayoutParams relativeLayout$LayoutParams = new RelativeLayout$LayoutParams(-2, -2);
        relativeLayout$LayoutParams.addRule(10);
        final boolean zzw = this.zzc.zzw;
        int n2 = 11;
        if (zzw && this.zzd != null) {
            relativeLayout$LayoutParams.addRule(11);
            relativeLayout$LayoutParams.addRule(2, this.zzd.zzF().getId());
        }
        else {
            if (!b2) {
                n2 = 9;
            }
            relativeLayout$LayoutParams.addRule(n2);
        }
        this.zzy(b, this.zzc.zzg);
        ((ViewGroup)this.zzl).addView((View)this.zzf, (ViewGroup$LayoutParams)relativeLayout$LayoutParams);
    }
    
    public final void zzx() {
        this.zzr = true;
    }
    
    public final void zzy(final boolean b, final boolean b2) {
        final boolean booleanValue = (boolean)zzba.zzc().zza(zzbdc.zzaX);
        final boolean b3 = true;
        boolean b4 = false;
        Label_0067: {
            if (booleanValue) {
                final AdOverlayInfoParcel zzc = this.zzc;
                if (zzc != null) {
                    final zzj zzo = zzc.zzo;
                    if (zzo != null && zzo.zzh) {
                        b4 = true;
                        break Label_0067;
                    }
                }
            }
            b4 = false;
        }
        boolean b5 = false;
        Label_0129: {
            if (zzba.zzc().zza(zzbdc.zzaY)) {
                final AdOverlayInfoParcel zzc2 = this.zzc;
                if (zzc2 != null) {
                    final zzj zzo2 = zzc2.zzo;
                    if (zzo2 != null && zzo2.zzi) {
                        b5 = true;
                        break Label_0129;
                    }
                }
            }
            b5 = false;
        }
        if (b && b2 && b4 && !b5) {
            new zzbss(this.zzd, "useCustomClose").zzg("Custom close has been disabled for interstitial ads in this ad slot.");
        }
        final zzr zzf = this.zzf;
        if (zzf != null) {
            boolean b6 = b3;
            if (!b5) {
                b6 = (b2 && !b4 && b3);
            }
            zzf.zzb(b6);
        }
    }
    
    public final void zzz() {
        ((ViewGroup)this.zzl).removeView((View)this.zzf);
        this.zzw(true);
    }
}

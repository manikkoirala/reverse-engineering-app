// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

public final class zzba extends Exception
{
    private final int zza;
    
    public zzba(final String message, final int zza) {
        super(message);
        this.zza = zza;
    }
    
    public final int zza() {
        return this.zza;
    }
}

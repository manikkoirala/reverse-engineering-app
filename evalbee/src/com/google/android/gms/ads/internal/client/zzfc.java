// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.ads.zzbxr;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.ads.zzbxx;
import com.google.android.gms.internal.ads.zzbxm;
import com.google.android.gms.internal.ads.zzbxg;
import android.os.Bundle;
import com.google.android.gms.internal.ads.zzcbg;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.internal.ads.zzbxq;
import com.google.android.gms.internal.ads.zzbxi;

public final class zzfc extends zzbxi
{
    private static void zzr(final zzbxq zzbxq) {
        zzcbn.zzg("This app is using a lightweight version of the Google Mobile Ads SDK that requires the latest Google Play services to be installed, but Google Play services is either missing or out of date.");
        zzcbg.zza.post((Runnable)new zzfb(zzbxq));
    }
    
    public final Bundle zzb() {
        return new Bundle();
    }
    
    public final zzdn zzc() {
        return null;
    }
    
    public final zzbxg zzd() {
        return null;
    }
    
    public final String zze() {
        return "";
    }
    
    public final void zzf(final zzl zzl, final zzbxq zzbxq) {
        zzr(zzbxq);
    }
    
    public final void zzg(final zzl zzl, final zzbxq zzbxq) {
        zzr(zzbxq);
    }
    
    public final void zzh(final boolean b) {
    }
    
    public final void zzi(final zzdd zzdd) {
    }
    
    public final void zzj(final zzdg zzdg) {
    }
    
    public final void zzk(final zzbxm zzbxm) {
    }
    
    public final void zzl(final zzbxx zzbxx) {
    }
    
    public final void zzm(final IObjectWrapper objectWrapper) {
    }
    
    public final void zzn(final IObjectWrapper objectWrapper, final boolean b) {
    }
    
    public final boolean zzo() {
        return false;
    }
    
    public final void zzp(final zzbxr zzbxr) {
    }
}

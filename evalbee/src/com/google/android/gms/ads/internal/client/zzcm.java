// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.Parcelable;
import com.google.android.gms.internal.ads.zzbmh;
import com.google.android.gms.internal.ads.zzbpr;
import android.os.IInterface;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.ads.zzavi;
import java.util.ArrayList;
import com.google.android.gms.internal.ads.zzbma;
import java.util.List;
import android.os.Parcel;
import android.os.IBinder;
import com.google.android.gms.internal.ads.zzavg;

public final class zzcm extends zzavg implements zzco
{
    public zzcm(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IMobileAdsSettingManager");
    }
    
    public final float zze() {
        final Parcel zzbh = this.zzbh(7, this.zza());
        final float float1 = zzbh.readFloat();
        zzbh.recycle();
        return float1;
    }
    
    public final String zzf() {
        final Parcel zzbh = this.zzbh(9, this.zza());
        final String string = zzbh.readString();
        zzbh.recycle();
        return string;
    }
    
    public final List zzg() {
        final Parcel zzbh = this.zzbh(13, this.zza());
        final ArrayList typedArrayList = zzbh.createTypedArrayList(zzbma.CREATOR);
        zzbh.recycle();
        return typedArrayList;
    }
    
    public final void zzh(final String s) {
        final Parcel zza = this.zza();
        zza.writeString(s);
        this.zzbi(10, zza);
    }
    
    public final void zzi() {
        this.zzbi(15, this.zza());
    }
    
    public final void zzj(final boolean b) {
        final Parcel zza = this.zza();
        final int zza2 = zzavi.zza;
        zza.writeInt((int)(b ? 1 : 0));
        this.zzbi(17, zza);
    }
    
    public final void zzk() {
        this.zzbi(1, this.zza());
    }
    
    public final void zzl(final String s, final IObjectWrapper objectWrapper) {
        final Parcel zza = this.zza();
        zza.writeString((String)null);
        zzavi.zzf(zza, (IInterface)objectWrapper);
        this.zzbi(6, zza);
    }
    
    public final void zzm(final zzda zzda) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)zzda);
        this.zzbi(16, zza);
    }
    
    public final void zzn(final IObjectWrapper objectWrapper, final String s) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)objectWrapper);
        zza.writeString(s);
        this.zzbi(5, zza);
    }
    
    public final void zzo(final zzbpr zzbpr) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)zzbpr);
        this.zzbi(11, zza);
    }
    
    public final void zzp(final boolean b) {
        final Parcel zza = this.zza();
        final int zza2 = zzavi.zza;
        zza.writeInt((int)(b ? 1 : 0));
        this.zzbi(4, zza);
    }
    
    public final void zzq(final float n) {
        final Parcel zza = this.zza();
        zza.writeFloat(n);
        this.zzbi(2, zza);
    }
    
    public final void zzr(final String s) {
        throw null;
    }
    
    public final void zzs(final zzbmh zzbmh) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)zzbmh);
        this.zzbi(12, zza);
    }
    
    public final void zzt(final String s) {
        final Parcel zza = this.zza();
        zza.writeString(s);
        this.zzbi(18, zza);
    }
    
    public final void zzu(final zzff zzff) {
        final Parcel zza = this.zza();
        zzavi.zzd(zza, (Parcelable)zzff);
        this.zzbi(14, zza);
    }
    
    public final boolean zzv() {
        final Parcel zzbh = this.zzbh(8, this.zza());
        final boolean zzg = zzavi.zzg(zzbh);
        zzbh.recycle();
        return zzg;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.RequestConfiguration;
import com.google.android.gms.ads.search.SearchAdRequest;
import android.os.Bundle;
import java.util.Set;
import java.util.Date;
import android.location.Location;
import java.util.Comparator;
import java.util.Arrays;
import com.google.android.gms.internal.ads.zzcbg;
import com.google.ads.mediation.admob.AdMobAdapter;
import java.util.List;
import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import android.content.Context;

public final class zzp
{
    public static final zzp zza;
    
    static {
        zza = new zzp();
    }
    
    public final zzl zza(Context applicationContext, final zzdx zzdx) {
        final Date zzn = zzdx.zzn();
        long time;
        if (zzn != null) {
            time = zzn.getTime();
        }
        else {
            time = -1L;
        }
        final String zzk = zzdx.zzk();
        final int zza = zzdx.zza();
        final Set zzq = zzdx.zzq();
        List<Object> unmodifiableList;
        if (!zzq.isEmpty()) {
            unmodifiableList = Collections.unmodifiableList((List<?>)new ArrayList<Object>(zzq));
        }
        else {
            unmodifiableList = null;
        }
        final boolean zzs = zzdx.zzs(applicationContext);
        final Bundle zzf = zzdx.zzf(AdMobAdapter.class);
        final String zzl = zzdx.zzl();
        final SearchAdRequest zzi = zzdx.zzi();
        zzfh zzfh;
        if (zzi != null) {
            zzfh = new zzfh(zzi);
        }
        else {
            zzfh = null;
        }
        applicationContext = applicationContext.getApplicationContext();
        String zzq2;
        if (applicationContext != null) {
            final String packageName = applicationContext.getPackageName();
            zzay.zzb();
            zzq2 = zzcbg.zzq(Thread.currentThread().getStackTrace(), packageName);
        }
        else {
            zzq2 = null;
        }
        final boolean zzr = zzdx.zzr();
        final RequestConfiguration zzc = zzej.zzf().zzc();
        return new zzl(8, time, zzf, zza, unmodifiableList, zzs, Math.max(zzdx.zzc(), zzc.getTagForChildDirectedTreatment()), false, zzl, zzfh, null, zzk, zzdx.zzg(), zzdx.zze(), Collections.unmodifiableList((List<?>)new ArrayList<Object>(zzdx.zzp())), zzdx.zzm(), zzq2, zzr, null, zzc.getTagForUnderAgeOfConsent(), (String)Collections.max((Collection<?>)Arrays.asList(null, zzc.getMaxAdContentRating()), (Comparator<? super Object>)zzo.zza), zzdx.zzo(), zzdx.zzb(), zzdx.zzj(), zzc.getPublisherPrivacyPersonalizationState().getValue());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.IBinder;
import com.google.android.gms.internal.ads.zzavg;

public final class zzdb extends zzavg implements zzdd
{
    public zzdb(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IOnAdMetadataChangedListener");
    }
    
    public final void zze() {
        this.zzbi(1, this.zza());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import android.content.Context;
import org.json.JSONObject;
import com.google.android.gms.internal.ads.zzcaq;
import com.google.android.gms.internal.ads.zzawp;

public interface zzg
{
    void zzA(final String p0);
    
    void zzB(final boolean p0);
    
    void zzC(final String p0);
    
    void zzD(final long p0);
    
    void zzE(final int p0);
    
    void zzF(final String p0, final String p1);
    
    void zzG(final String p0);
    
    void zzH(final boolean p0);
    
    void zzI(final boolean p0);
    
    void zzJ(final String p0, final String p1, final boolean p2);
    
    void zzK(final int p0);
    
    void zzL(final int p0);
    
    void zzM(final long p0);
    
    boolean zzN();
    
    boolean zzO();
    
    boolean zzP();
    
    boolean zzQ();
    
    boolean zzR();
    
    int zza();
    
    int zzb();
    
    int zzc();
    
    long zzd();
    
    long zze();
    
    long zzf();
    
    zzawp zzg();
    
    zzcaq zzh();
    
    zzcaq zzi();
    
    String zzj();
    
    String zzk();
    
    String zzl();
    
    String zzm();
    
    String zzn(final String p0);
    
    String zzo();
    
    JSONObject zzp();
    
    void zzq(final Runnable p0);
    
    void zzr(final Context p0);
    
    void zzs();
    
    void zzt(final long p0);
    
    void zzu(final String p0);
    
    void zzv(final int p0);
    
    void zzw(final String p0);
    
    void zzx(final boolean p0);
    
    void zzy(final String p0);
    
    void zzz(final boolean p0);
}

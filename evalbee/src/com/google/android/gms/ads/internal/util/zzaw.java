// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import android.net.Uri;
import com.google.android.gms.ads.internal.zzt;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;

final class zzaw implements DialogInterface$OnClickListener
{
    final zzax zza;
    
    public zzaw(final zzax zza) {
        this.zza = zza;
    }
    
    public final void onClick(final DialogInterface dialogInterface, final int n) {
        zzt.zzp();
        com.google.android.gms.ads.internal.util.zzt.zzT(this.zza.zza, Uri.parse("https://support.google.com/dfp_premium/answer/7160685#push"));
    }
}

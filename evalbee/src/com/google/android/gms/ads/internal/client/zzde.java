// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.ads.zzavi;
import android.os.IBinder;
import com.google.android.gms.internal.ads.zzavg;

public final class zzde extends zzavg implements zzdg
{
    public zzde(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IOnPaidEventListener");
    }
    
    public final void zze(final zzs zzs) {
        final Parcel zza = this.zza();
        zzavi.zzd(zza, (Parcelable)zzs);
        this.zzbi(1, zza);
    }
    
    public final boolean zzf() {
        final Parcel zzbh = this.zzbh(2, this.zza());
        final boolean zzg = zzavi.zzg(zzbh);
        zzbh.recycle();
        return zzg;
    }
}

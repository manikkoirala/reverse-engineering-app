// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal;

import android.os.RemoteException;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.ads.internal.client.zze;
import com.google.android.gms.internal.ads.zzffr;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

final class zzm extends WebViewClient
{
    final zzs zza;
    
    public zzm(final zzs zza) {
        this.zza = zza;
    }
    
    public final void onReceivedError(final WebView webView, final WebResourceRequest webResourceRequest, final WebResourceError webResourceError) {
        final zzs zza = this.zza;
        if (zzs.zzh(zza) != null) {
            try {
                zzs.zzh(zza).zzf(zzffr.zzd(1, (String)null, (zze)null));
            }
            catch (final RemoteException ex) {
                zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex);
            }
        }
        final zzs zza2 = this.zza;
        if (zzs.zzh(zza2) != null) {
            try {
                zzs.zzh(zza2).zze(0);
            }
            catch (final RemoteException ex2) {
                zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex2);
            }
        }
    }
    
    public final boolean shouldOverrideUrlLoading(final WebView webView, final String s) {
        if (s.startsWith(this.zza.zzq())) {
            return false;
        }
        if (s.startsWith("gmsg://noAdLoaded")) {
            final zzs zza = this.zza;
            if (zzs.zzh(zza) != null) {
                try {
                    zzs.zzh(zza).zzf(zzffr.zzd(3, (String)null, (zze)null));
                }
                catch (final RemoteException ex) {
                    zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex);
                }
            }
            final zzs zza2 = this.zza;
            if (zzs.zzh(zza2) != null) {
                try {
                    zzs.zzh(zza2).zze(3);
                }
                catch (final RemoteException ex2) {
                    zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex2);
                }
            }
            this.zza.zzV(0);
            return true;
        }
        if (s.startsWith("gmsg://scriptLoadFailed")) {
            final zzs zza3 = this.zza;
            if (zzs.zzh(zza3) != null) {
                try {
                    zzs.zzh(zza3).zzf(zzffr.zzd(1, (String)null, (zze)null));
                }
                catch (final RemoteException ex3) {
                    zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex3);
                }
            }
            final zzs zza4 = this.zza;
            if (zzs.zzh(zza4) != null) {
                try {
                    zzs.zzh(zza4).zze(0);
                }
                catch (final RemoteException ex4) {
                    zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex4);
                }
            }
            this.zza.zzV(0);
            return true;
        }
        if (s.startsWith("gmsg://adResized")) {
            final zzs zza5 = this.zza;
            if (zzs.zzh(zza5) != null) {
                try {
                    zzs.zzh(zza5).zzi();
                }
                catch (final RemoteException ex5) {
                    zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex5);
                }
            }
            this.zza.zzV(this.zza.zzb(s));
            return true;
        }
        if (s.startsWith("gmsg://")) {
            return true;
        }
        final zzs zza6 = this.zza;
        if (zzs.zzh(zza6) != null) {
            try {
                zzs.zzh(zza6).zzc();
                zzs.zzh(this.zza).zzh();
            }
            catch (final RemoteException ex6) {
                zzcbn.zzl("#007 Could not call remote method.", (Throwable)ex6);
            }
        }
        zzs.zzw(this.zza, zzs.zzo(this.zza, s));
        return true;
    }
}

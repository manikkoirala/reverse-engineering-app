// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.overlay;

import android.view.MotionEvent;
import android.content.Context;
import com.google.android.gms.ads.internal.util.zzau;
import android.widget.RelativeLayout;

final class zzg extends RelativeLayout
{
    final zzau zza;
    boolean zzb;
    
    public zzg(final Context context, final String s, final String s2, final String s3) {
        super(context);
        final zzau zza = new zzau(context, s);
        (this.zza = zza).zzo(s2);
        zza.zzn(s3);
    }
    
    public final boolean onInterceptTouchEvent(final MotionEvent motionEvent) {
        if (!this.zzb) {
            this.zza.zzm(motionEvent);
        }
        return false;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.overlay;

import android.view.ViewParent;
import com.google.android.gms.internal.ads.zzcgv;
import android.content.Context;
import android.view.ViewGroup;
import android.view.ViewGroup$LayoutParams;

public final class zzh
{
    public final int zza;
    public final ViewGroup$LayoutParams zzb;
    public final ViewGroup zzc;
    public final Context zzd;
    
    public zzh(final zzcgv zzcgv) {
        this.zzb = zzcgv.getLayoutParams();
        final ViewParent parent = zzcgv.getParent();
        this.zzd = zzcgv.zzE();
        if (parent != null && parent instanceof ViewGroup) {
            final ViewGroup zzc = (ViewGroup)parent;
            this.zzc = zzc;
            this.zza = zzc.indexOfChild(zzcgv.zzF());
            zzc.removeView(zzcgv.zzF());
            zzcgv.zzan(true);
            return;
        }
        throw new zzf("Could not get the parent of the WebView for an overlay.");
    }
}

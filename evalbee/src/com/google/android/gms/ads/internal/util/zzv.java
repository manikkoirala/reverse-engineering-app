// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import android.util.DisplayMetrics;
import com.google.android.gms.ads.internal.zzt;
import android.view.WindowManager;
import android.content.Context;
import com.google.android.gms.internal.ads.zzcbg;
import com.google.android.gms.ads.internal.client.zzay;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.ads.internal.client.zzba;
import android.content.res.Configuration;
import android.app.Activity;

public class zzv extends zzu
{
    public static final boolean zzf(final int n, final int n2, final int n3) {
        return Math.abs(n - n2) <= n3;
    }
    
    @Override
    public final boolean zze(final Activity activity, final Configuration configuration) {
        final boolean booleanValue = (boolean)zzba.zzc().zza(zzbdc.zzeI);
        boolean b = false;
        if (!booleanValue) {
            return false;
        }
        if (zzba.zzc().zza(zzbdc.zzeK)) {
            return activity.isInMultiWindowMode();
        }
        zzay.zzb();
        final int zzx = zzcbg.zzx((Context)activity, configuration.screenHeightDp);
        final int zzx2 = zzcbg.zzx((Context)activity, configuration.screenWidthDp);
        final WindowManager windowManager = (WindowManager)((Context)activity).getApplicationContext().getSystemService("window");
        zzt.zzp();
        final DisplayMetrics zzs = com.google.android.gms.ads.internal.util.zzt.zzs(windowManager);
        final int heightPixels = zzs.heightPixels;
        final int widthPixels = zzs.widthPixels;
        final int identifier = ((Context)activity).getResources().getIdentifier("status_bar_height", "dimen", "android");
        int dimensionPixelSize;
        if (identifier > 0) {
            dimensionPixelSize = ((Context)activity).getResources().getDimensionPixelSize(identifier);
        }
        else {
            dimensionPixelSize = 0;
        }
        final int n = (int)Math.round(((Context)activity).getResources().getDisplayMetrics().density + 0.5) * (int)zzba.zzc().zza(zzbdc.zzeG);
        if (zzf(heightPixels, zzx + dimensionPixelSize, n)) {
            if (!zzf(widthPixels, zzx2, n)) {
                return true;
            }
        }
        else {
            b = true;
        }
        return b;
    }
}

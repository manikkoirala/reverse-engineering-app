// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.IBinder;
import com.google.android.gms.internal.ads.zzavg;

public final class zzcq extends zzavg implements zzcs
{
    public zzcq(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IMuteThisAdListener");
    }
    
    public final void zze() {
        this.zzbi(1, this.zza());
    }
}

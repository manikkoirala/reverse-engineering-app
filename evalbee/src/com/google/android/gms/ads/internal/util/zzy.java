// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import android.view.View$OnApplyWindowInsetsListener;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.ads.internal.client.zzba;
import android.media.AudioManager;
import android.view.WindowManager$LayoutParams;
import android.view.Window;
import java.util.Iterator;
import android.view.DisplayCutout;
import android.text.TextUtils;
import java.util.Locale;
import android.graphics.Rect;
import com.google.android.gms.ads.internal.zzt;
import android.view.WindowInsets;
import android.view.View;
import android.app.Activity;

public class zzy extends zzw
{
    private static final void zzp(final boolean b, final Activity activity) {
        final Window window = activity.getWindow();
        final WindowManager$LayoutParams attributes = window.getAttributes();
        final int a = dk2.a(attributes);
        int n = 1;
        if (!b) {
            n = 2;
        }
        if (n != a) {
            ek2.a(attributes, n);
            window.setAttributes(attributes);
        }
    }
    
    @Override
    public final int zzk(final AudioManager audioManager) {
        return gk2.a(audioManager, 3);
    }
    
    @Override
    public final void zzl(final Activity activity) {
        if ((boolean)zzba.zzc().zza(zzbdc.zzbe) && zzt.zzo().zzi().zzm() == null && !activity.isInMultiWindowMode()) {
            zzp(true, activity);
            activity.getWindow().getDecorView().setOnApplyWindowInsetsListener((View$OnApplyWindowInsetsListener)new zzx(this, activity));
        }
    }
}

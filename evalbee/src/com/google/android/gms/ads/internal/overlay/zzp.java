// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.overlay;

import android.view.View;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

final class zzp extends AnimatorListenerAdapter
{
    final zzr zza;
    
    public zzp(final zzr zza) {
        this.zza = zza;
    }
    
    private final void zza(final boolean b) {
        ((View)this.zza).setEnabled(b);
        ((View)zzr.zza(this.zza)).setEnabled(b);
    }
    
    public final void onAnimationCancel(final Animator animator) {
        this.zza(true);
    }
    
    public final void onAnimationEnd(final Animator animator) {
        this.zza(true);
    }
    
    public final void onAnimationStart(final Animator animator) {
        this.zza(false);
    }
}

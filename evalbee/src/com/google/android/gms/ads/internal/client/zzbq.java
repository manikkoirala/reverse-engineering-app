// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.formats.PublisherAdViewOptions;
import com.google.android.gms.internal.ads.zzbfw;
import com.google.android.gms.internal.ads.zzbmm;
import com.google.android.gms.ads.formats.AdManagerAdViewOptions;
import com.google.android.gms.internal.ads.zzbhw;
import com.google.android.gms.internal.ads.zzbht;
import com.google.android.gms.internal.ads.zzbmv;
import com.google.android.gms.internal.ads.zzbhm;
import com.google.android.gms.internal.ads.zzbhp;
import com.google.android.gms.internal.ads.zzbhj;
import com.google.android.gms.internal.ads.zzbhg;
import android.os.IInterface;

public interface zzbq extends IInterface
{
    zzbn zze();
    
    void zzf(final zzbhg p0);
    
    void zzg(final zzbhj p0);
    
    void zzh(final String p0, final zzbhp p1, final zzbhm p2);
    
    void zzi(final zzbmv p0);
    
    void zzj(final zzbht p0, final zzq p1);
    
    void zzk(final zzbhw p0);
    
    void zzl(final zzbh p0);
    
    void zzm(final AdManagerAdViewOptions p0);
    
    void zzn(final zzbmm p0);
    
    void zzo(final zzbfw p0);
    
    void zzp(final PublisherAdViewOptions p0);
    
    void zzq(final zzcf p0);
}

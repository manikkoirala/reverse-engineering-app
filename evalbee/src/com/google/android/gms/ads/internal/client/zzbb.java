// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.FullScreenContentCallback;

public final class zzbb extends zzch
{
    private final FullScreenContentCallback zza;
    
    public zzbb(final FullScreenContentCallback zza) {
        this.zza = zza;
    }
    
    public final void zzb() {
        final FullScreenContentCallback zza = this.zza;
        if (zza != null) {
            zza.onAdClicked();
        }
    }
    
    public final void zzc() {
        final FullScreenContentCallback zza = this.zza;
        if (zza != null) {
            zza.onAdDismissedFullScreenContent();
        }
    }
    
    public final void zzd(final zze zze) {
        final FullScreenContentCallback zza = this.zza;
        if (zza != null) {
            zza.onAdFailedToShowFullScreenContent(zze.zza());
        }
    }
    
    public final void zze() {
        final FullScreenContentCallback zza = this.zza;
        if (zza != null) {
            zza.onAdImpression();
        }
    }
    
    public final void zzf() {
        final FullScreenContentCallback zza = this.zza;
        if (zza != null) {
            zza.onAdShowedFullScreenContent();
        }
    }
}

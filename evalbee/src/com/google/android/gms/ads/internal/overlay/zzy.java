// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.overlay;

import android.os.BaseBundle;
import com.google.android.gms.internal.ads.zzdge;
import android.content.Context;
import com.google.android.gms.ads.internal.zzt;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.ads.internal.client.zzba;
import android.os.Bundle;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.content.Intent;
import android.app.Activity;
import com.google.android.gms.internal.ads.zzbto;

public final class zzy extends zzbto
{
    private final AdOverlayInfoParcel zza;
    private final Activity zzb;
    private boolean zzc;
    private boolean zzd;
    private boolean zze;
    
    public zzy(final Activity zzb, final AdOverlayInfoParcel zza) {
        this.zzc = false;
        this.zzd = false;
        this.zze = false;
        this.zza = zza;
        this.zzb = zzb;
    }
    
    private final void zzb() {
        synchronized (this) {
            if (!this.zzd) {
                final zzo zzc = this.zza.zzc;
                if (zzc != null) {
                    zzc.zzbz(4);
                }
                this.zzd = true;
            }
        }
    }
    
    public final boolean zzH() {
        return false;
    }
    
    public final void zzh(final int n, final int n2, final Intent intent) {
    }
    
    public final void zzi() {
    }
    
    public final void zzk(final IObjectWrapper objectWrapper) {
    }
    
    public final void zzl(final Bundle bundle) {
        if ((boolean)zzba.zzc().zza(zzbdc.zziH) && !this.zze) {
            this.zzb.requestWindowFeature(1);
        }
        int n = 0;
        if (bundle != null) {
            n = n;
            if (((BaseBundle)bundle).getBoolean("com.google.android.gms.ads.internal.overlay.hasResumed", false)) {
                n = 1;
            }
        }
        final AdOverlayInfoParcel zza = this.zza;
        if (zza != null) {
            if (n == 0) {
                if (bundle == null) {
                    final com.google.android.gms.ads.internal.client.zza zzb = zza.zzb;
                    if (zzb != null) {
                        zzb.onAdClicked();
                    }
                    final zzdge zzu = this.zza.zzu;
                    if (zzu != null) {
                        zzu.zzbL();
                    }
                    if (this.zzb.getIntent() != null && this.zzb.getIntent().getBooleanExtra("shouldCallOnOverlayOpened", true)) {
                        final zzo zzc = this.zza.zzc;
                        if (zzc != null) {
                            zzc.zzbw();
                        }
                    }
                }
                final Activity zzb2 = this.zzb;
                final AdOverlayInfoParcel zza2 = this.zza;
                zzt.zzh();
                final zzc zza3 = zza2.zza;
                if (com.google.android.gms.ads.internal.overlay.zza.zzb((Context)zzb2, zza3, zza2.zzi, zza3.zzi)) {
                    return;
                }
            }
        }
        this.zzb.finish();
    }
    
    public final void zzm() {
        if (this.zzb.isFinishing()) {
            this.zzb();
        }
    }
    
    public final void zzo() {
        final zzo zzc = this.zza.zzc;
        if (zzc != null) {
            zzc.zzbp();
        }
        if (this.zzb.isFinishing()) {
            this.zzb();
        }
    }
    
    public final void zzp(final int n, final String[] array, final int[] array2) {
    }
    
    public final void zzq() {
    }
    
    public final void zzr() {
        if (this.zzc) {
            this.zzb.finish();
            return;
        }
        this.zzc = true;
        final zzo zzc = this.zza.zzc;
        if (zzc != null) {
            zzc.zzbM();
        }
    }
    
    public final void zzs(final Bundle bundle) {
        ((BaseBundle)bundle).putBoolean("com.google.android.gms.ads.internal.overlay.hasResumed", this.zzc);
    }
    
    public final void zzt() {
    }
    
    public final void zzu() {
        if (this.zzb.isFinishing()) {
            this.zzb();
        }
    }
    
    public final void zzv() {
        final zzo zzc = this.zza.zzc;
        if (zzc != null) {
            zzc.zzby();
        }
    }
    
    public final void zzx() {
        this.zze = true;
    }
}

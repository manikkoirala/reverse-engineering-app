// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.ads.appopen.AppOpenAd;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "AppOpenAdOptionsParcelCreator")
@Reserved({ 1 })
public final class zzw extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zzw> CREATOR;
    @AppOpenAd.AppOpenAdOrientation
    @Field(id = 2)
    public final int zza;
    
    static {
        CREATOR = (Parcelable$Creator)new zzx();
    }
    
    @Constructor
    public zzw(@AppOpenAd.AppOpenAdOrientation @Param(id = 2) final int zza) {
        this.zza = zza;
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        final int zza = this.zza;
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 2, zza);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.ads.zzbpr;
import com.google.android.gms.internal.ads.zzbmh;
import java.util.List;
import android.os.IInterface;
import android.os.IBinder;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.ads.zzbpq;
import com.google.android.gms.internal.ads.zzbmg;
import android.os.Parcelable$Creator;
import com.google.android.gms.internal.ads.zzavi;
import android.os.Parcel;
import com.google.android.gms.internal.ads.zzavh;

public abstract class zzcn extends zzavh implements zzco
{
    public zzcn() {
        super("com.google.android.gms.ads.internal.client.IMobileAdsSettingManager");
    }
    
    public final boolean zzbK(int zzv, final Parcel parcel, final Parcel parcel2, int zza) {
        switch (zzv) {
            default: {
                return false;
            }
            case 18: {
                final String string = parcel.readString();
                zzavi.zzc(parcel);
                this.zzt(string);
                break;
            }
            case 17: {
                final boolean zzg = zzavi.zzg(parcel);
                zzavi.zzc(parcel);
                this.zzj(zzg);
                break;
            }
            case 16: {
                final IBinder strongBinder = parcel.readStrongBinder();
                zzda zzda;
                if (strongBinder == null) {
                    zzda = null;
                }
                else {
                    final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IOnAdInspectorClosedListener");
                    if (queryLocalInterface instanceof zzda) {
                        zzda = (zzda)queryLocalInterface;
                    }
                    else {
                        zzda = new zzcy(strongBinder);
                    }
                }
                zzavi.zzc(parcel);
                this.zzm(zzda);
                break;
            }
            case 15: {
                this.zzi();
                break;
            }
            case 14: {
                final zzff zzff = (zzff)zzavi.zza(parcel, (Parcelable$Creator)com.google.android.gms.ads.internal.client.zzff.CREATOR);
                zzavi.zzc(parcel);
                this.zzu(zzff);
                break;
            }
            case 13: {
                final List zzg2 = this.zzg();
                parcel2.writeNoException();
                parcel2.writeTypedList(zzg2);
                return true;
            }
            case 12: {
                final zzbmh zzc = zzbmg.zzc(parcel.readStrongBinder());
                zzavi.zzc(parcel);
                this.zzs(zzc);
                break;
            }
            case 11: {
                final zzbpr zzf = zzbpq.zzf(parcel.readStrongBinder());
                zzavi.zzc(parcel);
                this.zzo(zzf);
                break;
            }
            case 10: {
                final String string2 = parcel.readString();
                zzavi.zzc(parcel);
                this.zzh(string2);
                break;
            }
            case 9: {
                final String zzf2 = this.zzf();
                parcel2.writeNoException();
                parcel2.writeString(zzf2);
                return true;
            }
            case 8: {
                zzv = (this.zzv() ? 1 : 0);
                parcel2.writeNoException();
                zza = zzavi.zza;
                parcel2.writeInt(zzv);
                return true;
            }
            case 7: {
                final float zze = this.zze();
                parcel2.writeNoException();
                parcel2.writeFloat(zze);
                return true;
            }
            case 6: {
                final String string3 = parcel.readString();
                final IObjectWrapper interface1 = IObjectWrapper.Stub.asInterface(parcel.readStrongBinder());
                zzavi.zzc(parcel);
                this.zzl(string3, interface1);
                break;
            }
            case 5: {
                final IObjectWrapper interface2 = IObjectWrapper.Stub.asInterface(parcel.readStrongBinder());
                final String string4 = parcel.readString();
                zzavi.zzc(parcel);
                this.zzn(interface2, string4);
                break;
            }
            case 4: {
                final boolean zzg3 = zzavi.zzg(parcel);
                zzavi.zzc(parcel);
                this.zzp(zzg3);
                break;
            }
            case 3: {
                final String string5 = parcel.readString();
                zzavi.zzc(parcel);
                this.zzr(string5);
                break;
            }
            case 2: {
                final float float1 = parcel.readFloat();
                zzavi.zzc(parcel);
                this.zzq(float1);
                break;
            }
            case 1: {
                this.zzk();
                break;
            }
        }
        parcel2.writeNoException();
        return true;
    }
}

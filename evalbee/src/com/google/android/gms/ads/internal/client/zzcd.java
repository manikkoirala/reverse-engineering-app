// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.ads.zzblb;
import com.google.android.gms.internal.ads.zzbpr;
import android.os.IInterface;
import android.os.Parcelable$Creator;
import com.google.android.gms.internal.ads.zzbla;
import com.google.android.gms.internal.ads.zzavi;
import com.google.android.gms.internal.ads.zzbpq;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.Parcel;
import com.google.android.gms.internal.ads.zzavh;

public abstract class zzcd extends zzavh implements zzce
{
    public zzcd() {
        super("com.google.android.gms.ads.internal.client.IClientApi");
    }
    
    public final boolean zzbK(int n, final Parcel parcel, final Parcel parcel2, final int n2) {
        Object o = null;
        switch (n) {
            default: {
                return false;
            }
            case 17: {
                final IObjectWrapper interface1 = IObjectWrapper.Stub.asInterface(parcel.readStrongBinder());
                final zzbpr zzf = zzbpq.zzf(parcel.readStrongBinder());
                n = parcel.readInt();
                zzavi.zzc(parcel);
                o = this.zzh(interface1, zzf, n);
                break;
            }
            case 16: {
                final IObjectWrapper interface2 = IObjectWrapper.Stub.asInterface(parcel.readStrongBinder());
                final zzbpr zzf2 = zzbpq.zzf(parcel.readStrongBinder());
                n = parcel.readInt();
                final zzblb zzc = zzbla.zzc(parcel.readStrongBinder());
                zzavi.zzc(parcel);
                o = this.zzk(interface2, zzf2, n, zzc);
                break;
            }
            case 15: {
                final IObjectWrapper interface3 = IObjectWrapper.Stub.asInterface(parcel.readStrongBinder());
                final zzbpr zzf3 = zzbpq.zzf(parcel.readStrongBinder());
                n = parcel.readInt();
                zzavi.zzc(parcel);
                o = this.zzl(interface3, zzf3, n);
                break;
            }
            case 14: {
                final IObjectWrapper interface4 = IObjectWrapper.Stub.asInterface(parcel.readStrongBinder());
                final zzbpr zzf4 = zzbpq.zzf(parcel.readStrongBinder());
                n = parcel.readInt();
                zzavi.zzc(parcel);
                o = this.zzp(interface4, zzf4, n);
                break;
            }
            case 13: {
                final IObjectWrapper interface5 = IObjectWrapper.Stub.asInterface(parcel.readStrongBinder());
                final zzq zzq = (zzq)zzavi.zza(parcel, (Parcelable$Creator)com.google.android.gms.ads.internal.client.zzq.CREATOR);
                final String string = parcel.readString();
                final zzbpr zzf5 = zzbpq.zzf(parcel.readStrongBinder());
                n = parcel.readInt();
                zzavi.zzc(parcel);
                o = this.zzc(interface5, zzq, string, zzf5, n);
                break;
            }
            case 12: {
                final IObjectWrapper interface6 = IObjectWrapper.Stub.asInterface(parcel.readStrongBinder());
                final String string2 = parcel.readString();
                final zzbpr zzf6 = zzbpq.zzf(parcel.readStrongBinder());
                n = parcel.readInt();
                zzavi.zzc(parcel);
                o = this.zzo(interface6, string2, zzf6, n);
                break;
            }
            case 11: {
                final IObjectWrapper interface7 = IObjectWrapper.Stub.asInterface(parcel.readStrongBinder());
                final IObjectWrapper interface8 = IObjectWrapper.Stub.asInterface(parcel.readStrongBinder());
                final IObjectWrapper interface9 = IObjectWrapper.Stub.asInterface(parcel.readStrongBinder());
                zzavi.zzc(parcel);
                o = this.zzj(interface7, interface8, interface9);
                break;
            }
            case 10: {
                final IObjectWrapper interface10 = IObjectWrapper.Stub.asInterface(parcel.readStrongBinder());
                final zzq zzq2 = (zzq)zzavi.zza(parcel, (Parcelable$Creator)zzq.CREATOR);
                final String string3 = parcel.readString();
                n = parcel.readInt();
                zzavi.zzc(parcel);
                o = this.zzf(interface10, zzq2, string3, n);
                break;
            }
            case 9: {
                final IObjectWrapper interface11 = IObjectWrapper.Stub.asInterface(parcel.readStrongBinder());
                n = parcel.readInt();
                zzavi.zzc(parcel);
                o = this.zzg(interface11, n);
                break;
            }
            case 8: {
                final IObjectWrapper interface12 = IObjectWrapper.Stub.asInterface(parcel.readStrongBinder());
                zzavi.zzc(parcel);
                o = this.zzm(interface12);
                break;
            }
            case 6: {
                final IObjectWrapper interface13 = IObjectWrapper.Stub.asInterface(parcel.readStrongBinder());
                final zzbpr zzf7 = zzbpq.zzf(parcel.readStrongBinder());
                n = parcel.readInt();
                zzavi.zzc(parcel);
                o = this.zzn(interface13, zzf7, n);
                break;
            }
            case 5: {
                final IObjectWrapper interface14 = IObjectWrapper.Stub.asInterface(parcel.readStrongBinder());
                final IObjectWrapper interface15 = IObjectWrapper.Stub.asInterface(parcel.readStrongBinder());
                zzavi.zzc(parcel);
                o = this.zzi(interface14, interface15);
                break;
            }
            case 4:
            case 7: {
                IObjectWrapper.Stub.asInterface(parcel.readStrongBinder());
                zzavi.zzc(parcel);
                parcel2.writeNoException();
                zzavi.zzf(parcel2, (IInterface)null);
                return true;
            }
            case 3: {
                final IObjectWrapper interface16 = IObjectWrapper.Stub.asInterface(parcel.readStrongBinder());
                final String string4 = parcel.readString();
                final zzbpr zzf8 = zzbpq.zzf(parcel.readStrongBinder());
                n = parcel.readInt();
                zzavi.zzc(parcel);
                o = this.zzb(interface16, string4, zzf8, n);
                break;
            }
            case 2: {
                final IObjectWrapper interface17 = IObjectWrapper.Stub.asInterface(parcel.readStrongBinder());
                final zzq zzq3 = (zzq)zzavi.zza(parcel, (Parcelable$Creator)zzq.CREATOR);
                final String string5 = parcel.readString();
                final zzbpr zzf9 = zzbpq.zzf(parcel.readStrongBinder());
                n = parcel.readInt();
                zzavi.zzc(parcel);
                o = this.zze(interface17, zzq3, string5, zzf9, n);
                break;
            }
            case 1: {
                final IObjectWrapper interface18 = IObjectWrapper.Stub.asInterface(parcel.readStrongBinder());
                final zzq zzq4 = (zzq)zzavi.zza(parcel, (Parcelable$Creator)zzq.CREATOR);
                final String string6 = parcel.readString();
                final zzbpr zzf10 = zzbpq.zzf(parcel.readStrongBinder());
                n = parcel.readInt();
                zzavi.zzc(parcel);
                o = this.zzd(interface18, zzq4, string6, zzf10, n);
                break;
            }
        }
        parcel2.writeNoException();
        zzavi.zzf(parcel2, (IInterface)o);
        return true;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.overlay;

import com.google.android.gms.internal.ads.zzbmx;
import java.util.concurrent.Executor;
import com.google.android.gms.ads.internal.zzt;
import com.google.android.gms.internal.ads.zzfru;
import com.google.android.gms.internal.ads.zzftc;
import com.google.android.gms.internal.ads.zzfsd;
import com.google.android.gms.internal.ads.zzfsf;
import com.google.android.gms.internal.ads.zzcca;
import com.google.android.gms.internal.ads.zzfrr;
import com.google.android.gms.internal.ads.zzfrs;
import com.google.android.gms.ads.internal.util.zze;
import java.util.Map;
import java.util.HashMap;
import android.content.Context;
import com.google.android.gms.internal.ads.zzfsh;
import android.text.TextUtils;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.ads.internal.client.zzba;
import com.google.android.gms.internal.ads.zzfsi;
import com.google.android.gms.internal.ads.zzfsg;
import com.google.android.gms.internal.ads.zzfrt;
import com.google.android.gms.internal.ads.zzcgv;

public final class zzw
{
    private String zza;
    private String zzb;
    private zzcgv zzc;
    private zzfrt zzd;
    private boolean zze;
    private zzfsg zzf;
    
    public zzw() {
        this.zzc = null;
        this.zze = false;
        this.zza = null;
        this.zzd = null;
        this.zzb = null;
    }
    
    private final zzfsi zzl() {
        final zzfsh zzc = zzfsi.zzc();
        if ((boolean)zzba.zzc().zza(zzbdc.zzkD) && !TextUtils.isEmpty((CharSequence)this.zzb)) {
            zzc.zza(this.zzb);
        }
        else {
            final String zza = this.zza;
            if (zza != null) {
                zzc.zzb(zza);
            }
            else {
                this.zzf("Missing session token and/or appId", "onLMDupdate");
            }
        }
        return zzc.zzc();
    }
    
    private final void zzm() {
        if (this.zzf == null) {
            this.zzf = (zzfsg)new zzv(this);
        }
    }
    
    public final void zza(final zzcgv zzc, final Context context) {
        synchronized (this) {
            this.zzc = zzc;
            if (!this.zzk(context)) {
                this.zzf("Unable to bind", "on_play_store_bind");
                return;
            }
            final HashMap hashMap = new HashMap();
            hashMap.put("action", "fetch_completed");
            this.zze("on_play_store_bind", hashMap);
        }
    }
    
    public final void zzb() {
        if (this.zze) {
            final zzfrt zzd = this.zzd;
            if (zzd != null) {
                zzd.zza(this.zzl(), this.zzf);
                this.zzd("onLMDOverlayCollapse");
                return;
            }
        }
        com.google.android.gms.ads.internal.util.zze.zza("LastMileDelivery not connected");
    }
    
    public final void zzc() {
        if (this.zze) {
            final zzfrt zzd = this.zzd;
            if (zzd != null) {
                final zzfrr zzc = zzfrs.zzc();
                if ((boolean)zzba.zzc().zza(zzbdc.zzkD) && !TextUtils.isEmpty((CharSequence)this.zzb)) {
                    zzc.zza(this.zzb);
                }
                else {
                    final String zza = this.zza;
                    if (zza != null) {
                        zzc.zzb(zza);
                    }
                    else {
                        this.zzf("Missing session token and/or appId", "onLMDupdate");
                    }
                }
                zzd.zzb(zzc.zzc(), this.zzf);
                return;
            }
        }
        com.google.android.gms.ads.internal.util.zze.zza("LastMileDelivery not connected");
    }
    
    public final void zzd(final String s) {
        this.zze(s, new HashMap());
    }
    
    public final void zze(final String s, final Map map) {
        ((Executor)zzcca.zze).execute(new zzu(this, s, map));
    }
    
    public final void zzf(final String s, final String s2) {
        com.google.android.gms.ads.internal.util.zze.zza(s);
        if (this.zzc != null) {
            final HashMap hashMap = new HashMap();
            hashMap.put("message", s);
            hashMap.put("action", s2);
            this.zze("onError", hashMap);
        }
    }
    
    public final void zzg() {
        if (this.zze) {
            final zzfrt zzd = this.zzd;
            if (zzd != null) {
                zzd.zzc(this.zzl(), this.zzf);
                this.zzd("onLMDOverlayExpand");
                return;
            }
        }
        com.google.android.gms.ads.internal.util.zze.zza("LastMileDelivery not connected");
    }
    
    public final void zzi(final zzfsf zzfsf) {
        if (!TextUtils.isEmpty((CharSequence)zzfsf.zzb()) && !(boolean)zzba.zzc().zza(zzbdc.zzkD)) {
            this.zza = zzfsf.zzb();
        }
        switch (zzfsf.zza()) {
            default: {
                return;
            }
            case 8160:
            case 8161:
            case 8162: {
                final HashMap hashMap = new HashMap();
                hashMap.put("error", String.valueOf(zzfsf.zza()));
                this.zze("onLMDOverlayFailedToOpen", hashMap);
                return;
            }
            case 8157: {
                this.zza = null;
                this.zzb = null;
                this.zze = false;
                return;
            }
            case 8155: {
                this.zzd("onLMDOverlayClose");
                return;
            }
            case 8153: {
                this.zzd("onLMDOverlayClicked");
                return;
            }
            case 8152: {
                this.zzd("onLMDOverlayOpened");
            }
        }
    }
    
    public final void zzj(final zzcgv zzc, final zzfsd zzfsd) {
        if (zzc == null) {
            this.zzf("adWebview missing", "onLMDShow");
            return;
        }
        this.zzc = zzc;
        if (!this.zze && !this.zzk(zzc.getContext())) {
            this.zzf("LMDOverlay not bound", "on_play_store_bind");
            return;
        }
        if (zzba.zzc().zza(zzbdc.zzkD)) {
            this.zzb = zzfsd.zzg();
        }
        this.zzm();
        final zzfrt zzd = this.zzd;
        if (zzd != null) {
            zzd.zzd(zzfsd, this.zzf);
        }
    }
    
    public final boolean zzk(final Context context) {
        synchronized (this) {
            if (!zzftc.zza(context)) {
                return false;
            }
            try {
                this.zzd = zzfru.zza(context);
            }
            catch (final NullPointerException ex) {
                com.google.android.gms.ads.internal.util.zze.zza("Error connecting LMD Overlay service");
                zzt.zzo().zzw((Throwable)ex, "LastMileDeliveryOverlay.bindLastMileDeliveryService");
            }
            if (this.zzd == null) {
                return this.zze = false;
            }
            this.zzm();
            return this.zze = true;
        }
    }
}

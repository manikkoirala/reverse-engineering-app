// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import com.google.android.gms.ads.internal.zzt;
import android.os.Message;
import android.os.Looper;
import com.google.android.gms.internal.ads.zzfqv;

public final class zzf extends zzfqv
{
    public zzf(final Looper looper) {
        super(looper);
    }
    
    public final void handleMessage(final Message message) {
        try {
            super.handleMessage(message);
        }
        catch (final Exception ex) {
            zzt.zzo().zzw((Throwable)ex, "AdMobHandler.handleMessage");
        }
    }
    
    public final void zza(final Message message) {
        try {
            super.zza(message);
        }
        finally {
            zzt.zzp();
            final Throwable t;
            com.google.android.gms.ads.internal.util.zzt.zzL(zzt.zzo().zzd(), t);
        }
    }
}

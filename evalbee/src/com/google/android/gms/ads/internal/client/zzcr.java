// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.Parcel;
import android.os.IInterface;
import android.os.IBinder;
import com.google.android.gms.internal.ads.zzavh;

public abstract class zzcr extends zzavh implements zzcs
{
    public zzcr() {
        super("com.google.android.gms.ads.internal.client.IMuteThisAdListener");
    }
    
    public static zzcs zzb(final IBinder binder) {
        if (binder == null) {
            return null;
        }
        final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.ads.internal.client.IMuteThisAdListener");
        if (queryLocalInterface instanceof zzcs) {
            return (zzcs)queryLocalInterface;
        }
        return new zzcq(binder);
    }
    
    public final boolean zzbK(final int n, final Parcel parcel, final Parcel parcel2, final int n2) {
        if (n == 1) {
            this.zze();
            parcel2.writeNoException();
            return true;
        }
        return false;
    }
}

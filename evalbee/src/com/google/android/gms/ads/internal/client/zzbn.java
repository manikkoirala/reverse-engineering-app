// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.IInterface;

public interface zzbn extends IInterface
{
    String zze();
    
    String zzf();
    
    void zzg(final zzl p0);
    
    void zzh(final zzl p0, final int p1);
    
    boolean zzi();
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.LoadAdError;

final class zzdz extends zzaz
{
    final zzea zza;
    
    public zzdz(final zzea zza) {
        this.zza = zza;
    }
    
    @Override
    public final void onAdFailedToLoad(final LoadAdError loadAdError) {
        final zzea zza = this.zza;
        zzea.zze(zza).zzb(zza.zzi());
        super.onAdFailedToLoad(loadAdError);
    }
    
    @Override
    public final void onAdLoaded() {
        final zzea zza = this.zza;
        zzea.zze(zza).zzb(zza.zzi());
        super.onAdLoaded();
    }
}

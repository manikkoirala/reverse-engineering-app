// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.RemoteException;
import com.google.android.gms.internal.ads.zzcbn;

final class zzev implements Runnable
{
    final zzew zza;
    
    public zzev(final zzew zza) {
        this.zza = zza;
    }
    
    @Override
    public final void run() {
        final zzew zza = this.zza;
        if (zzew.zzb(zza) != null) {
            try {
                zzew.zzb(zza).zze(1);
            }
            catch (final RemoteException ex) {
                zzcbn.zzk("Could not notify onAdFailedToLoad event.", (Throwable)ex);
            }
        }
    }
}

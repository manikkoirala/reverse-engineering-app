// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import com.google.android.gms.internal.ads.zzanx;
import com.google.android.gms.internal.ads.zzang;
import com.google.android.gms.internal.ads.zzamw;
import com.google.android.gms.internal.ads.zzane;
import java.util.Map;
import com.google.android.gms.internal.ads.zzcbm;
import com.google.android.gms.internal.ads.zzccf;
import com.google.android.gms.internal.ads.zzana;

public final class zzbp extends zzana
{
    private final zzccf zza;
    private final zzcbm zzb;
    
    public zzbp(final String s, final Map map, final zzccf zza) {
        super(0, s, (zzane)new zzbo(zza));
        this.zza = zza;
        (this.zzb = new zzcbm((String)null)).zzd(s, "GET", (Map)null, (byte[])null);
    }
    
    public final zzang zzh(final zzamw zzamw) {
        return zzang.zzb((Object)zzamw, zzanx.zzb(zzamw));
    }
}

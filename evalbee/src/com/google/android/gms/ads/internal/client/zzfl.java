// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.ads.VideoOptions;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "VideoOptionsParcelCreator")
@Reserved({ 1 })
public final class zzfl extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zzfl> CREATOR;
    @Field(id = 2)
    public final boolean zza;
    @Field(id = 3)
    public final boolean zzb;
    @Field(id = 4)
    public final boolean zzc;
    
    static {
        CREATOR = (Parcelable$Creator)new zzfm();
    }
    
    public zzfl(final VideoOptions videoOptions) {
        this(videoOptions.getStartMuted(), videoOptions.getCustomControlsRequested(), videoOptions.getClickToExpandRequested());
    }
    
    @Constructor
    public zzfl(@Param(id = 2) final boolean zza, @Param(id = 3) final boolean zzb, @Param(id = 4) final boolean zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        final boolean zza = this.zza;
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeBoolean(parcel, 2, zza);
        SafeParcelWriter.writeBoolean(parcel, 3, this.zzb);
        SafeParcelWriter.writeBoolean(parcel, 4, this.zzc);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

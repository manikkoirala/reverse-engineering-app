// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.Parcelable$Creator;
import com.google.android.gms.internal.ads.zzavi;
import android.os.Parcel;
import com.google.android.gms.internal.ads.zzavh;

public abstract class zzbg extends zzavh implements zzbh
{
    public zzbg() {
        super("com.google.android.gms.ads.internal.client.IAdListener");
    }
    
    public final boolean zzbK(int int1, final Parcel parcel, final Parcel parcel2, final int n) {
        while (true) {
            switch (int1) {
                default: {
                    return false;
                }
                case 3: {
                    parcel2.writeNoException();
                    return true;
                }
                case 9: {
                    this.zzk();
                    continue;
                }
                case 8: {
                    final zze zze = (zze)zzavi.zza(parcel, (Parcelable$Creator)com.google.android.gms.ads.internal.client.zze.CREATOR);
                    zzavi.zzc(parcel);
                    this.zzf(zze);
                    continue;
                }
                case 7: {
                    this.zzg();
                    continue;
                }
                case 6: {
                    this.zzc();
                    continue;
                }
                case 5: {
                    this.zzj();
                    continue;
                }
                case 4: {
                    this.zzi();
                    continue;
                }
                case 2: {
                    int1 = parcel.readInt();
                    zzavi.zzc(parcel);
                    this.zze(int1);
                    continue;
                }
                case 1: {
                    this.zzd();
                    continue;
                }
            }
            break;
        }
    }
}

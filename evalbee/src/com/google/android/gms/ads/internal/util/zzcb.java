// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import com.google.android.gms.ads.internal.zzt;

public final class zzcb
{
    private long zza;
    private long zzb;
    private final Object zzc;
    
    public zzcb(final long zza) {
        this.zzb = Long.MIN_VALUE;
        this.zzc = new Object();
        this.zza = zza;
    }
    
    public final void zza(final long zza) {
        synchronized (this.zzc) {
            this.zza = zza;
        }
    }
    
    public final boolean zzb() {
        synchronized (this.zzc) {
            final long elapsedRealtime = zzt.zzB().elapsedRealtime();
            if (this.zzb + this.zza > elapsedRealtime) {
                return false;
            }
            this.zzb = elapsedRealtime;
            return true;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.IInterface;

public interface zzdq extends IInterface
{
    float zze();
    
    float zzf();
    
    float zzg();
    
    int zzh();
    
    zzdt zzi();
    
    void zzj(final boolean p0);
    
    void zzk();
    
    void zzl();
    
    void zzm(final zzdt p0);
    
    void zzn();
    
    boolean zzo();
    
    boolean zzp();
    
    boolean zzq();
}

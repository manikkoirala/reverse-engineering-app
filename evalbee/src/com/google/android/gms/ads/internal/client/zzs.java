// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import org.json.JSONObject;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "AdValueParcelCreator")
public final class zzs extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zzs> CREATOR;
    @Field(id = 1)
    public final int zza;
    @Field(id = 2)
    public final int zzb;
    @Field(id = 3)
    public final String zzc;
    @Field(id = 4)
    public final long zzd;
    
    static {
        CREATOR = (Parcelable$Creator)new zzt();
    }
    
    @Constructor
    public zzs(@Param(id = 1) final int zza, @Param(id = 2) final int zzb, @Param(id = 3) final String zzc, @Param(id = 4) final long zzd) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
    }
    
    public static zzs zza(final JSONObject jsonObject) {
        return new zzs(jsonObject.getInt("type_num"), jsonObject.getInt("precision_num"), jsonObject.getString("currency"), jsonObject.getLong("value"));
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        final int zza = this.zza;
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, zza);
        SafeParcelWriter.writeInt(parcel, 2, this.zzb);
        SafeParcelWriter.writeString(parcel, 3, this.zzc, false);
        SafeParcelWriter.writeLong(parcel, 4, this.zzd);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

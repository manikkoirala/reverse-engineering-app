// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import com.google.android.gms.internal.ads.zzcca;

public abstract class zzb
{
    private final Runnable zza;
    private volatile Thread zzb;
    
    public zzb() {
        this.zza = new zza(this);
    }
    
    public abstract void zza();
    
    public ik0 zzb() {
        return zzcca.zza.zza(this.zza);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import java.util.ArrayList;
import java.util.List;

public final class zzbf
{
    private final List zza;
    private final List zzb;
    private final List zzc;
    
    public zzbf() {
        this.zza = new ArrayList();
        this.zzb = new ArrayList();
        this.zzc = new ArrayList();
    }
    
    public final zzbf zza(final String s, final double d, final double d2) {
        int i;
        for (i = 0; i < this.zza.size(); ++i) {
            final double doubleValue = this.zzc.get(i);
            final double doubleValue2 = this.zzb.get(i);
            if (d < doubleValue) {
                break;
            }
            if (doubleValue == d && d2 < doubleValue2) {
                break;
            }
        }
        this.zza.add(i, s);
        this.zzc.add(i, d);
        this.zzb.add(i, d2);
        return this;
    }
    
    public final zzbh zzb() {
        return new zzbh(this, null);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import android.content.SharedPreferences$Editor;
import com.google.android.gms.common.util.SharedPreferencesUtils;
import com.google.android.gms.common.util.ClientLibraryUtils;
import com.google.android.gms.common.GooglePlayServicesUtilLight;
import android.webkit.WebSettings;
import android.content.Context;

public final class zzck
{
    private static zzck zzb;
    String zza;
    
    private zzck() {
    }
    
    public static zzck zza() {
        if (zzck.zzb == null) {
            zzck.zzb = new zzck();
        }
        return zzck.zzb;
    }
    
    public final void zzb(final Context context) {
        zze.zza("Updating user agent.");
        final String defaultUserAgent = WebSettings.getDefaultUserAgent(context);
        if (!defaultUserAgent.equals(this.zza)) {
            Context remoteContext = GooglePlayServicesUtilLight.getRemoteContext(context);
            Label_0088: {
                if (!ClientLibraryUtils.isPackageSide()) {
                    if (remoteContext != null) {
                        break Label_0088;
                    }
                    remoteContext = null;
                }
                final SharedPreferences$Editor putString = context.getSharedPreferences("admob_user_agent", 0).edit().putString("user_agent", WebSettings.getDefaultUserAgent(context));
                if (remoteContext == null) {
                    putString.apply();
                }
                else {
                    SharedPreferencesUtils.publishWorldReadableSharedPreferences(context, putString, "admob_user_agent");
                }
            }
            this.zza = defaultUserAgent;
        }
        zze.zza("User agent is updated.");
    }
}

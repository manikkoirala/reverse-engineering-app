// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.ads.formats.ShouldDelayBannerRenderingListener;
import com.google.android.gms.internal.ads.zzbhy;

public final class zzfj extends zzbhy
{
    private final ShouldDelayBannerRenderingListener zza;
    
    public zzfj(final ShouldDelayBannerRenderingListener zza) {
        this.zza = zza;
    }
    
    public final boolean zzb(final IObjectWrapper objectWrapper) {
        return this.zza.shouldDelayBannerRendering(ObjectWrapper.unwrap(objectWrapper));
    }
}

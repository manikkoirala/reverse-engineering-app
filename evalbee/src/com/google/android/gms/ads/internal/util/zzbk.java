// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import java.util.Collections;
import com.google.android.gms.internal.ads.zzane;
import com.google.android.gms.internal.ads.zzanf;
import com.google.android.gms.internal.ads.zzcbm;
import java.util.Map;
import com.google.android.gms.internal.ads.zzaoe;

final class zzbk extends zzaoe
{
    final byte[] zza;
    final Map zzb;
    final zzcbm zzc;
    
    public zzbk(final zzbq zzbq, final int n, final String s, final zzanf zzanf, final zzane zzane, final byte[] zza, final Map zzb, final zzcbm zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        super(n, s, zzanf, zzane);
    }
    
    public final Map zzl() {
        Map<Object, Object> map;
        if ((map = this.zzb) == null) {
            map = Collections.emptyMap();
        }
        return map;
    }
    
    public final byte[] zzx() {
        byte[] zza;
        if ((zza = this.zza) == null) {
            zza = null;
        }
        return zza;
    }
    
    public final void zzz(final String s) {
        this.zzc.zzg(s);
        super.zzz(s);
    }
}

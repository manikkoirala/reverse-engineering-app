// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import android.os.Parcel;
import android.os.IInterface;
import com.google.android.gms.internal.ads.zzavi;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IBinder;
import com.google.android.gms.internal.ads.zzavg;

public final class zzbr extends zzavg implements zzbt
{
    public zzbr(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.util.IWorkManagerUtil");
    }
    
    public final void zze(final IObjectWrapper objectWrapper) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)objectWrapper);
        this.zzbi(2, zza);
    }
    
    public final boolean zzf(final IObjectWrapper objectWrapper, final String s, final String s2) {
        final Parcel zza = this.zza();
        zzavi.zzf(zza, (IInterface)objectWrapper);
        zza.writeString(s);
        zza.writeString(s2);
        final Parcel zzbh = this.zzbh(1, zza);
        final boolean zzg = zzavi.zzg(zzbh);
        zzbh.recycle();
        return zzg;
    }
}

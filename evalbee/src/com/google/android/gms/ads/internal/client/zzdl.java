// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcelable$Creator;
import android.os.Parcel;
import com.google.android.gms.internal.ads.zzavi;
import android.os.Bundle;
import android.os.IBinder;
import com.google.android.gms.internal.ads.zzavg;

public final class zzdl extends zzavg implements zzdn
{
    public zzdl(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IResponseInfo");
    }
    
    public final Bundle zze() {
        final Parcel zzbh = this.zzbh(5, this.zza());
        final Bundle bundle = (Bundle)zzavi.zza(zzbh, Bundle.CREATOR);
        zzbh.recycle();
        return bundle;
    }
    
    public final zzu zzf() {
        final Parcel zzbh = this.zzbh(4, this.zza());
        final zzu zzu = (zzu)zzavi.zza(zzbh, (Parcelable$Creator)com.google.android.gms.ads.internal.client.zzu.CREATOR);
        zzbh.recycle();
        return zzu;
    }
    
    public final String zzg() {
        final Parcel zzbh = this.zzbh(1, this.zza());
        final String string = zzbh.readString();
        zzbh.recycle();
        return string;
    }
    
    public final String zzh() {
        final Parcel zzbh = this.zzbh(6, this.zza());
        final String string = zzbh.readString();
        zzbh.recycle();
        return string;
    }
    
    public final String zzi() {
        final Parcel zzbh = this.zzbh(2, this.zza());
        final String string = zzbh.readString();
        zzbh.recycle();
        return string;
    }
    
    public final List zzj() {
        final Parcel zzbh = this.zzbh(3, this.zza());
        final ArrayList typedArrayList = zzbh.createTypedArrayList((Parcelable$Creator)zzu.CREATOR);
        zzbh.recycle();
        return typedArrayList;
    }
}

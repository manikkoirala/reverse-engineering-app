// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal;

import com.google.android.gms.ads.internal.client.zzbk;
import java.util.Iterator;
import java.util.Map;
import com.google.android.gms.internal.ads.zzbej;
import android.net.Uri$Builder;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.ads.internal.client.zzdq;
import com.google.android.gms.ads.internal.client.zzdn;
import android.os.Bundle;
import com.google.android.gms.internal.ads.zzcbg;
import com.google.android.gms.ads.internal.client.zzay;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.client.zzcf;
import com.google.android.gms.ads.internal.client.zzl;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.view.ViewGroup$LayoutParams;
import com.google.android.gms.ads.internal.client.zzfl;
import com.google.android.gms.internal.ads.zzbww;
import com.google.android.gms.internal.ads.zzbub;
import com.google.android.gms.ads.internal.client.zzdg;
import com.google.android.gms.internal.ads.zzbea;
import com.google.android.gms.internal.ads.zzbty;
import com.google.android.gms.ads.internal.client.zzdu;
import com.google.android.gms.ads.internal.client.zzci;
import com.google.android.gms.ads.internal.client.zzw;
import com.google.android.gms.internal.ads.zzaxm;
import com.google.android.gms.ads.internal.client.zzcb;
import com.google.android.gms.ads.internal.client.zzby;
import com.google.android.gms.ads.internal.client.zzbe;
import com.google.android.gms.common.internal.Preconditions;
import android.content.Intent;
import com.google.android.gms.internal.ads.zzasj;
import com.google.android.gms.internal.ads.zzcbn;
import android.app.Activity;
import android.view.View;
import android.net.Uri;
import android.view.View$OnTouchListener;
import android.webkit.WebViewClient;
import java.util.concurrent.Callable;
import com.google.android.gms.internal.ads.zzcca;
import android.os.AsyncTask;
import com.google.android.gms.internal.ads.zzasi;
import com.google.android.gms.ads.internal.client.zzbh;
import android.webkit.WebView;
import android.content.Context;
import java.util.concurrent.Future;
import com.google.android.gms.ads.internal.client.zzq;
import com.google.android.gms.internal.ads.zzcbt;
import com.google.android.gms.ads.internal.client.zzbt;

public final class zzs extends zzbt
{
    private final zzcbt zza;
    private final zzq zzb;
    private final Future zzc;
    private final Context zzd;
    private final zzr zze;
    private WebView zzf;
    private zzbh zzg;
    private zzasi zzh;
    private AsyncTask zzi;
    
    public zzs(final Context zzd, final zzq zzb, final String s, final zzcbt zza) {
        this.zzd = zzd;
        this.zza = zza;
        this.zzb = zzb;
        this.zzf = new WebView(zzd);
        this.zzc = zzcca.zza.zzb((Callable)new zzo(this));
        this.zze = new zzr(zzd, s);
        this.zzV(0);
        ((View)this.zzf).setVerticalScrollBarEnabled(false);
        this.zzf.getSettings().setJavaScriptEnabled(true);
        this.zzf.setWebViewClient((WebViewClient)new zzm(this));
        ((View)this.zzf).setOnTouchListener((View$OnTouchListener)new zzn(this));
    }
    
    public final void zzA() {
        throw new IllegalStateException("Unused method");
    }
    
    public final void zzB() {
        Preconditions.checkMainThread("resume must be called on the main UI thread.");
    }
    
    public final void zzC(final zzbe zzbe) {
        throw new IllegalStateException("Unused method");
    }
    
    public final void zzD(final zzbh zzg) {
        this.zzg = zzg;
    }
    
    public final void zzE(final zzby zzby) {
        throw new IllegalStateException("Unused method");
    }
    
    public final void zzF(final zzq zzq) {
        throw new IllegalStateException("AdSize must be set before initialization");
    }
    
    public final void zzG(final zzcb zzcb) {
        throw new IllegalStateException("Unused method");
    }
    
    public final void zzH(final zzaxm zzaxm) {
        throw new IllegalStateException("Unused method");
    }
    
    public final void zzI(final zzw zzw) {
        throw new IllegalStateException("Unused method");
    }
    
    public final void zzJ(final zzci zzci) {
    }
    
    public final void zzK(final zzdu zzdu) {
        throw new IllegalStateException("Unused method");
    }
    
    public final void zzL(final boolean b) {
        throw new IllegalStateException("Unused method");
    }
    
    public final void zzM(final zzbty zzbty) {
        throw new IllegalStateException("Unused method");
    }
    
    public final void zzN(final boolean b) {
    }
    
    public final void zzO(final zzbea zzbea) {
        throw new IllegalStateException("Unused method");
    }
    
    public final void zzP(final zzdg zzdg) {
    }
    
    public final void zzQ(final zzbub zzbub, final String s) {
        throw new IllegalStateException("Unused method");
    }
    
    public final void zzR(final String s) {
        throw new IllegalStateException("Unused method");
    }
    
    public final void zzS(final zzbww zzbww) {
        throw new IllegalStateException("Unused method");
    }
    
    public final void zzT(final String s) {
        throw new IllegalStateException("Unused method");
    }
    
    public final void zzU(final zzfl zzfl) {
        throw new IllegalStateException("Unused method");
    }
    
    public final void zzV(final int n) {
        if (this.zzf == null) {
            return;
        }
        this.zzf.setLayoutParams(new ViewGroup$LayoutParams(-1, n));
    }
    
    public final void zzW(final IObjectWrapper objectWrapper) {
    }
    
    public final void zzX() {
        throw new IllegalStateException("Unused method");
    }
    
    public final boolean zzY() {
        return false;
    }
    
    public final boolean zzZ() {
        return false;
    }
    
    public final boolean zzaa(final zzl zzl) {
        Preconditions.checkNotNull(this.zzf, "This Search Ad has already been torn down");
        this.zze.zzf(zzl, this.zza);
        this.zzi = new com.google.android.gms.ads.internal.zzq(this, null).execute((Object[])new Void[0]);
        return true;
    }
    
    public final void zzab(final zzcf zzcf) {
        throw new IllegalStateException("Unused method");
    }
    
    public final int zzb(String queryParameter) {
        queryParameter = Uri.parse(queryParameter).getQueryParameter("height");
        if (TextUtils.isEmpty((CharSequence)queryParameter)) {
            return 0;
        }
        try {
            zzay.zzb();
            return zzcbg.zzx(this.zzd, Integer.parseInt(queryParameter));
        }
        catch (final NumberFormatException ex) {
            return 0;
        }
    }
    
    public final Bundle zzd() {
        throw new IllegalStateException("Unused method");
    }
    
    public final zzq zzg() {
        return this.zzb;
    }
    
    public final zzbh zzi() {
        throw new IllegalStateException("getIAdListener not implemented");
    }
    
    public final zzcb zzj() {
        throw new IllegalStateException("getIAppEventListener not implemented");
    }
    
    public final zzdn zzk() {
        return null;
    }
    
    public final zzdq zzl() {
        return null;
    }
    
    public final IObjectWrapper zzn() {
        Preconditions.checkMainThread("getAdFrame must be called on the main UI thread.");
        return ObjectWrapper.wrap(this.zzf);
    }
    
    public final String zzp() {
        final Uri$Builder uri$Builder = new Uri$Builder();
        uri$Builder.scheme("https://").appendEncodedPath((String)zzbej.zzd.zze());
        uri$Builder.appendQueryParameter("query", this.zze.zzd());
        uri$Builder.appendQueryParameter("pubId", this.zze.zzc());
        uri$Builder.appendQueryParameter("mappver", this.zze.zza());
        final Map zze = this.zze.zze();
        for (final String s : zze.keySet()) {
            uri$Builder.appendQueryParameter(s, (String)zze.get(s));
        }
        final Uri build = uri$Builder.build();
        final zzasi zzh = this.zzh;
        Uri zzb = build;
        if (zzh != null) {
            try {
                zzb = zzh.zzb(build, this.zzd);
            }
            catch (final zzasj zzasj) {
                zzcbn.zzk("Unable to process ad data", (Throwable)zzasj);
                zzb = build;
            }
        }
        final String zzq = this.zzq();
        final String encodedQuery = zzb.getEncodedQuery();
        final StringBuilder sb = new StringBuilder();
        sb.append(zzq);
        sb.append("#");
        sb.append(encodedQuery);
        return sb.toString();
    }
    
    public final String zzq() {
        String zzb;
        if (TextUtils.isEmpty((CharSequence)(zzb = this.zze.zzb()))) {
            zzb = "www.google.com";
        }
        final String str = (String)zzbej.zzd.zze();
        final StringBuilder sb = new StringBuilder();
        sb.append("https://");
        sb.append(zzb);
        sb.append(str);
        return sb.toString();
    }
    
    public final String zzr() {
        throw new IllegalStateException("getAdUnitId not implemented");
    }
    
    public final String zzs() {
        return null;
    }
    
    public final String zzt() {
        return null;
    }
    
    public final void zzx() {
        Preconditions.checkMainThread("destroy must be called on the main UI thread.");
        this.zzi.cancel(true);
        this.zzc.cancel(true);
        this.zzf.destroy();
        this.zzf = null;
    }
    
    public final void zzy(final zzl zzl, final zzbk zzbk) {
    }
    
    public final void zzz() {
        Preconditions.checkMainThread("pause must be called on the main UI thread.");
    }
}

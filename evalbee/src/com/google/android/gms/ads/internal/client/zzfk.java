// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.VideoController;

public final class zzfk extends zzds
{
    private final VideoController.VideoLifecycleCallbacks zza;
    
    public zzfk(final VideoController.VideoLifecycleCallbacks zza) {
        this.zza = zza;
    }
    
    public final void zze() {
        this.zza.onVideoEnd();
    }
    
    public final void zzf(final boolean b) {
        this.zza.onVideoMute(b);
    }
    
    public final void zzg() {
        this.zza.onVideoPause();
    }
    
    public final void zzh() {
        this.zza.onVideoPlay();
    }
    
    public final void zzi() {
        this.zza.onVideoStart();
    }
}

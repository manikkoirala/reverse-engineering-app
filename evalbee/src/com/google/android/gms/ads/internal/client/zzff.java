// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.ads.RequestConfiguration;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "RequestConfigurationParcelCreator")
public final class zzff extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zzff> CREATOR;
    @Field(id = 1)
    public final int zza;
    @Field(id = 2)
    public final int zzb;
    
    static {
        CREATOR = (Parcelable$Creator)new zzfg();
    }
    
    @Constructor
    public zzff(@Param(id = 1) final int zza, @Param(id = 2) final int zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public zzff(final RequestConfiguration requestConfiguration) {
        this.zza = requestConfiguration.getTagForChildDirectedTreatment();
        this.zzb = requestConfiguration.getTagForUnderAgeOfConsent();
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        final int zza = this.zza;
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, zza);
        SafeParcelWriter.writeInt(parcel, 2, this.zzb);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

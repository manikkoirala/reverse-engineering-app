// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.Parcel;
import android.os.IInterface;
import android.os.IBinder;
import com.google.android.gms.internal.ads.zzavh;

public abstract class zzcv extends zzavh implements zzcw
{
    public zzcv() {
        super("com.google.android.gms.ads.internal.client.IMuteThisAdReason");
    }
    
    public static zzcw zzb(final IBinder binder) {
        if (binder == null) {
            return null;
        }
        final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.ads.internal.client.IMuteThisAdReason");
        if (queryLocalInterface instanceof zzcw) {
            return (zzcw)queryLocalInterface;
        }
        return new zzcu(binder);
    }
    
    public final boolean zzbK(final int n, final Parcel parcel, final Parcel parcel2, final int n2) {
        String s;
        if (n != 1) {
            if (n != 2) {
                return false;
            }
            s = this.zzf();
        }
        else {
            s = this.zze();
        }
        parcel2.writeNoException();
        parcel2.writeString(s);
        return true;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.offline.buffering;

import android.os.RemoteException;
import androidx.work.c;
import com.google.android.gms.internal.ads.zzbpr;
import com.google.android.gms.internal.ads.zzbpo;
import com.google.android.gms.ads.internal.client.zzay;
import androidx.work.WorkerParameters;
import android.content.Context;
import com.google.android.gms.internal.ads.zzbti;
import com.google.android.gms.common.annotation.KeepForSdk;
import androidx.work.Worker;

@KeepForSdk
public class OfflinePingSender extends Worker
{
    private final zzbti zza;
    
    public OfflinePingSender(final Context context, final WorkerParameters workerParameters) {
        super(context, workerParameters);
        this.zza = zzay.zza().zzm(context, (zzbpr)new zzbpo());
    }
    
    @Override
    public final a doWork() {
        try {
            this.zza.zzh();
            return a.c();
        }
        catch (final RemoteException ex) {
            return a.a();
        }
    }
}

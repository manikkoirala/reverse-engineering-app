// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.Parcelable;
import com.google.android.gms.internal.ads.zzavi;
import android.os.Parcel;
import android.os.IBinder;
import com.google.android.gms.internal.ads.zzavg;

public final class zzbf extends zzavg implements zzbh
{
    public zzbf(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IAdListener");
    }
    
    public final void zzc() {
        this.zzbi(6, this.zza());
    }
    
    public final void zzd() {
        this.zzbi(1, this.zza());
    }
    
    public final void zze(final int n) {
        final Parcel zza = this.zza();
        zza.writeInt(n);
        this.zzbi(2, zza);
    }
    
    public final void zzf(final zze zze) {
        final Parcel zza = this.zza();
        zzavi.zzd(zza, (Parcelable)zze);
        this.zzbi(8, zza);
    }
    
    public final void zzg() {
        this.zzbi(7, this.zza());
    }
    
    public final void zzh() {
        this.zzbi(3, this.zza());
    }
    
    public final void zzi() {
        this.zzbi(4, this.zza());
    }
    
    public final void zzj() {
        this.zzbi(5, this.zza());
    }
    
    public final void zzk() {
        this.zzbi(9, this.zza());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.overlay;

import android.os.IInterface;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import android.os.Bundle;
import android.content.Intent;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IBinder;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.ads.internal.client.zzba;
import com.google.android.gms.internal.ads.zzbti;
import com.google.android.gms.internal.ads.zzdge;
import com.google.android.gms.internal.ads.zzcyu;
import com.google.android.gms.internal.ads.zzbit;
import com.google.android.gms.ads.internal.zzj;
import com.google.android.gms.internal.ads.zzcbt;
import com.google.android.gms.internal.ads.zzbiv;
import com.google.android.gms.internal.ads.zzcgv;
import com.google.android.gms.ads.internal.client.zza;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "AdOverlayInfoCreator")
@Reserved({ 1 })
public final class AdOverlayInfoParcel extends AbstractSafeParcelable implements ReflectedParcelable
{
    public static final Parcelable$Creator<AdOverlayInfoParcel> CREATOR;
    @Field(id = 2)
    public final zzc zza;
    @Field(getter = "getAdClickListenerAsBinder", id = 3, type = "android.os.IBinder")
    public final zza zzb;
    @Field(getter = "getAdOverlayListenerAsBinder", id = 4, type = "android.os.IBinder")
    public final zzo zzc;
    @Field(getter = "getAdWebViewAsBinder", id = 5, type = "android.os.IBinder")
    public final zzcgv zzd;
    @Field(getter = "getAppEventGmsgListenerAsBinder", id = 6, type = "android.os.IBinder")
    public final zzbiv zze;
    @Field(id = 7)
    public final String zzf;
    @Field(id = 8)
    public final boolean zzg;
    @Field(id = 9)
    public final String zzh;
    @Field(getter = "getLeaveApplicationListenerAsBinder", id = 10, type = "android.os.IBinder")
    public final zzz zzi;
    @Field(id = 11)
    public final int zzj;
    @Field(id = 12)
    public final int zzk;
    @Field(id = 13)
    public final String zzl;
    @Field(id = 14)
    public final zzcbt zzm;
    @Field(id = 16)
    public final String zzn;
    @Field(id = 17)
    public final zzj zzo;
    @Field(getter = "getAdMetadataGmsgListenerAsBinder", id = 18, type = "android.os.IBinder")
    public final zzbit zzp;
    @Field(id = 19)
    public final String zzq;
    @Field(id = 24)
    public final String zzr;
    @Field(id = 25)
    public final String zzs;
    @Field(getter = "getAdFailedToShowEventEmitterAsBinder", id = 26, type = "android.os.IBinder")
    public final zzcyu zzt;
    @Field(getter = "getPhysicalClickListenerAsBinder", id = 27, type = "android.os.IBinder")
    public final zzdge zzu;
    @Field(getter = "getOfflineUtilsAsBinder", id = 28, type = "android.os.IBinder")
    public final zzbti zzv;
    @Field(id = 29)
    public final boolean zzw;
    
    static {
        CREATOR = (Parcelable$Creator)new zzn();
    }
    
    public AdOverlayInfoParcel(final zza zza, final zzo zzc, final zzz zzz, final zzcgv zzd, final int zzj, final zzcbt zzm, final String zzn, final zzj zzo, final String zzf, final String zzh, final String zzs, final zzcyu zzt, final zzbti zzv) {
        this.zza = null;
        this.zzb = null;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zzp = null;
        this.zze = null;
        this.zzg = false;
        if (zzba.zzc().zza(zzbdc.zzaH)) {
            this.zzf = null;
            this.zzh = null;
        }
        else {
            this.zzf = zzf;
            this.zzh = zzh;
        }
        this.zzi = null;
        this.zzj = zzj;
        this.zzk = 1;
        this.zzl = null;
        this.zzm = zzm;
        this.zzn = zzn;
        this.zzo = zzo;
        this.zzq = null;
        this.zzr = null;
        this.zzs = zzs;
        this.zzt = zzt;
        this.zzu = null;
        this.zzv = zzv;
        this.zzw = false;
    }
    
    public AdOverlayInfoParcel(final zza zzb, final zzo zzc, final zzz zzi, final zzcgv zzd, final boolean zzg, final int zzj, final zzcbt zzm, final zzdge zzu, final zzbti zzv) {
        this.zza = null;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zzp = null;
        this.zze = null;
        this.zzf = null;
        this.zzg = zzg;
        this.zzh = null;
        this.zzi = zzi;
        this.zzj = zzj;
        this.zzk = 2;
        this.zzl = null;
        this.zzm = zzm;
        this.zzn = null;
        this.zzo = null;
        this.zzq = null;
        this.zzr = null;
        this.zzs = null;
        this.zzt = null;
        this.zzu = zzu;
        this.zzv = zzv;
        this.zzw = false;
    }
    
    public AdOverlayInfoParcel(final zza zzb, final zzo zzc, final zzbit zzp, final zzbiv zze, final zzz zzi, final zzcgv zzd, final boolean zzg, final int zzj, final String zzl, final zzcbt zzm, final zzdge zzu, final zzbti zzv, final boolean zzw) {
        this.zza = null;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zzp = zzp;
        this.zze = zze;
        this.zzf = null;
        this.zzg = zzg;
        this.zzh = null;
        this.zzi = zzi;
        this.zzj = zzj;
        this.zzk = 3;
        this.zzl = zzl;
        this.zzm = zzm;
        this.zzn = null;
        this.zzo = null;
        this.zzq = null;
        this.zzr = null;
        this.zzs = null;
        this.zzt = null;
        this.zzu = zzu;
        this.zzv = zzv;
        this.zzw = zzw;
    }
    
    public AdOverlayInfoParcel(final zza zzb, final zzo zzc, final zzbit zzp, final zzbiv zze, final zzz zzi, final zzcgv zzd, final boolean zzg, final int zzj, final String zzh, final String zzf, final zzcbt zzm, final zzdge zzu, final zzbti zzv) {
        this.zza = null;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zzp = zzp;
        this.zze = zze;
        this.zzf = zzf;
        this.zzg = zzg;
        this.zzh = zzh;
        this.zzi = zzi;
        this.zzj = zzj;
        this.zzk = 3;
        this.zzl = null;
        this.zzm = zzm;
        this.zzn = null;
        this.zzo = null;
        this.zzq = null;
        this.zzr = null;
        this.zzs = null;
        this.zzt = null;
        this.zzu = zzu;
        this.zzv = zzv;
        this.zzw = false;
    }
    
    @Constructor
    public AdOverlayInfoParcel(@Param(id = 2) final zzc zza, @Param(id = 3) final IBinder binder, @Param(id = 4) final IBinder binder2, @Param(id = 5) final IBinder binder3, @Param(id = 6) final IBinder binder4, @Param(id = 7) final String zzf, @Param(id = 8) final boolean zzg, @Param(id = 9) final String zzh, @Param(id = 10) final IBinder binder5, @Param(id = 11) final int zzj, @Param(id = 12) final int zzk, @Param(id = 13) final String zzl, @Param(id = 14) final zzcbt zzm, @Param(id = 16) final String zzn, @Param(id = 17) final zzj zzo, @Param(id = 18) final IBinder binder6, @Param(id = 19) final String zzq, @Param(id = 24) final String zzr, @Param(id = 25) final String zzs, @Param(id = 26) final IBinder binder7, @Param(id = 27) final IBinder binder8, @Param(id = 28) final IBinder binder9, @Param(id = 29) final boolean zzw) {
        this.zza = zza;
        this.zzb = ObjectWrapper.unwrap(IObjectWrapper.Stub.asInterface(binder));
        this.zzc = ObjectWrapper.unwrap(IObjectWrapper.Stub.asInterface(binder2));
        this.zzd = ObjectWrapper.unwrap(IObjectWrapper.Stub.asInterface(binder3));
        this.zzp = ObjectWrapper.unwrap(IObjectWrapper.Stub.asInterface(binder6));
        this.zze = ObjectWrapper.unwrap(IObjectWrapper.Stub.asInterface(binder4));
        this.zzf = zzf;
        this.zzg = zzg;
        this.zzh = zzh;
        this.zzi = ObjectWrapper.unwrap(IObjectWrapper.Stub.asInterface(binder5));
        this.zzj = zzj;
        this.zzk = zzk;
        this.zzl = zzl;
        this.zzm = zzm;
        this.zzn = zzn;
        this.zzo = zzo;
        this.zzq = zzq;
        this.zzr = zzr;
        this.zzs = zzs;
        this.zzt = ObjectWrapper.unwrap(IObjectWrapper.Stub.asInterface(binder7));
        this.zzu = ObjectWrapper.unwrap(IObjectWrapper.Stub.asInterface(binder8));
        this.zzv = ObjectWrapper.unwrap(IObjectWrapper.Stub.asInterface(binder9));
        this.zzw = zzw;
    }
    
    public AdOverlayInfoParcel(final zzc zza, final zza zzb, final zzo zzc, final zzz zzi, final zzcbt zzm, final zzcgv zzd, final zzdge zzu) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zzp = null;
        this.zze = null;
        this.zzf = null;
        this.zzg = false;
        this.zzh = null;
        this.zzi = zzi;
        this.zzj = -1;
        this.zzk = 4;
        this.zzl = null;
        this.zzm = zzm;
        this.zzn = null;
        this.zzo = null;
        this.zzq = null;
        this.zzr = null;
        this.zzs = null;
        this.zzt = null;
        this.zzu = zzu;
        this.zzv = null;
        this.zzw = false;
    }
    
    public AdOverlayInfoParcel(final zzo zzc, final zzcgv zzd, final int n, final zzcbt zzm) {
        this.zzc = zzc;
        this.zzd = zzd;
        this.zzj = 1;
        this.zzm = zzm;
        this.zza = null;
        this.zzb = null;
        this.zzp = null;
        this.zze = null;
        this.zzf = null;
        this.zzg = false;
        this.zzh = null;
        this.zzi = null;
        this.zzk = 1;
        this.zzl = null;
        this.zzn = null;
        this.zzo = null;
        this.zzq = null;
        this.zzr = null;
        this.zzs = null;
        this.zzt = null;
        this.zzu = null;
        this.zzv = null;
        this.zzw = false;
    }
    
    public AdOverlayInfoParcel(final zzcgv zzd, final zzcbt zzm, final String zzq, final String zzr, final int n, final zzbti zzv) {
        this.zza = null;
        this.zzb = null;
        this.zzc = null;
        this.zzd = zzd;
        this.zzp = null;
        this.zze = null;
        this.zzf = null;
        this.zzg = false;
        this.zzh = null;
        this.zzi = null;
        this.zzj = 14;
        this.zzk = 5;
        this.zzl = null;
        this.zzm = zzm;
        this.zzn = null;
        this.zzo = null;
        this.zzq = zzq;
        this.zzr = zzr;
        this.zzs = null;
        this.zzt = null;
        this.zzu = null;
        this.zzv = zzv;
        this.zzw = false;
    }
    
    public static AdOverlayInfoParcel zza(final Intent intent) {
        try {
            final Bundle bundleExtra = intent.getBundleExtra("com.google.android.gms.ads.inernal.overlay.AdOverlayInfo");
            bundleExtra.setClassLoader(AdOverlayInfoParcel.class.getClassLoader());
            return (AdOverlayInfoParcel)bundleExtra.getParcelable("com.google.android.gms.ads.inernal.overlay.AdOverlayInfo");
        }
        catch (final Exception ex) {
            return null;
        }
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final zzc zza = this.zza;
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeParcelable(parcel, 2, (Parcelable)zza, n, false);
        SafeParcelWriter.writeIBinder(parcel, 3, ((IInterface)ObjectWrapper.wrap(this.zzb)).asBinder(), false);
        SafeParcelWriter.writeIBinder(parcel, 4, ((IInterface)ObjectWrapper.wrap(this.zzc)).asBinder(), false);
        SafeParcelWriter.writeIBinder(parcel, 5, ((IInterface)ObjectWrapper.wrap(this.zzd)).asBinder(), false);
        SafeParcelWriter.writeIBinder(parcel, 6, ((IInterface)ObjectWrapper.wrap(this.zze)).asBinder(), false);
        SafeParcelWriter.writeString(parcel, 7, this.zzf, false);
        SafeParcelWriter.writeBoolean(parcel, 8, this.zzg);
        SafeParcelWriter.writeString(parcel, 9, this.zzh, false);
        SafeParcelWriter.writeIBinder(parcel, 10, ((IInterface)ObjectWrapper.wrap(this.zzi)).asBinder(), false);
        SafeParcelWriter.writeInt(parcel, 11, this.zzj);
        SafeParcelWriter.writeInt(parcel, 12, this.zzk);
        SafeParcelWriter.writeString(parcel, 13, this.zzl, false);
        SafeParcelWriter.writeParcelable(parcel, 14, (Parcelable)this.zzm, n, false);
        SafeParcelWriter.writeString(parcel, 16, this.zzn, false);
        SafeParcelWriter.writeParcelable(parcel, 17, (Parcelable)this.zzo, n, false);
        SafeParcelWriter.writeIBinder(parcel, 18, ((IInterface)ObjectWrapper.wrap(this.zzp)).asBinder(), false);
        SafeParcelWriter.writeString(parcel, 19, this.zzq, false);
        SafeParcelWriter.writeString(parcel, 24, this.zzr, false);
        SafeParcelWriter.writeString(parcel, 25, this.zzs, false);
        SafeParcelWriter.writeIBinder(parcel, 26, ((IInterface)ObjectWrapper.wrap(this.zzt)).asBinder(), false);
        SafeParcelWriter.writeIBinder(parcel, 27, ((IInterface)ObjectWrapper.wrap(this.zzu)).asBinder(), false);
        SafeParcelWriter.writeIBinder(parcel, 28, ((IInterface)ObjectWrapper.wrap(this.zzv)).asBinder(), false);
        SafeParcelWriter.writeBoolean(parcel, 29, this.zzw);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

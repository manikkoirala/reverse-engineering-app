// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal;

import com.google.android.gms.internal.ads.zzcae;
import com.google.android.gms.internal.ads.zzbxj;
import com.google.android.gms.internal.ads.zzfdn;
import com.google.android.gms.internal.ads.zzbwt;
import com.google.android.gms.internal.ads.zzbto;
import com.google.android.gms.ads.internal.overlay.zzae;
import com.google.android.gms.ads.internal.overlay.zzaf;
import com.google.android.gms.ads.internal.overlay.zzy;
import com.google.android.gms.ads.internal.overlay.zzac;
import com.google.android.gms.ads.internal.overlay.zzt;
import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import android.app.Activity;
import com.google.android.gms.internal.ads.zzbtp;
import com.google.android.gms.internal.ads.zzbti;
import com.google.android.gms.internal.ads.zzduy;
import com.google.android.gms.internal.ads.zzble;
import com.google.android.gms.internal.ads.zzblb;
import com.google.android.gms.internal.ads.zzdle;
import java.util.HashMap;
import android.view.View;
import com.google.android.gms.internal.ads.zzbgs;
import com.google.android.gms.internal.ads.zzdlg;
import android.widget.FrameLayout;
import com.google.android.gms.internal.ads.zzbgm;
import com.google.android.gms.ads.internal.client.zzdj;
import com.google.android.gms.ads.internal.client.zzco;
import com.google.android.gms.internal.ads.zzcbt;
import com.google.android.gms.internal.ads.zzfbz;
import com.google.android.gms.internal.ads.zzfai;
import com.google.android.gms.internal.ads.zzeyv;
import com.google.android.gms.internal.ads.zzeyu;
import com.google.android.gms.ads.internal.client.zzew;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.ads.internal.client.zzba;
import com.google.android.gms.ads.internal.client.zzbu;
import com.google.android.gms.ads.internal.client.zzq;
import com.google.android.gms.internal.ads.zzemp;
import com.google.android.gms.internal.ads.zzciq;
import com.google.android.gms.dynamic.ObjectWrapper;
import android.content.Context;
import com.google.android.gms.ads.internal.client.zzbq;
import com.google.android.gms.internal.ads.zzbpr;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.ads.internal.client.zzcd;

public class ClientApi extends zzcd
{
    @KeepForSdk
    public ClientApi() {
    }
    
    public final zzbq zzb(final IObjectWrapper objectWrapper, final String s, final zzbpr zzbpr, final int n) {
        final Context context = ObjectWrapper.unwrap(objectWrapper);
        return (zzbq)new zzemp(zzciq.zzb(context, zzbpr, n), context, s);
    }
    
    public final zzbu zzc(final IObjectWrapper objectWrapper, final zzq zzq, final String s, final zzbpr zzbpr, final int n) {
        final Context context = ObjectWrapper.unwrap(objectWrapper);
        final zzeyu zzs = zzciq.zzb(context, zzbpr, n).zzs();
        zzs.zza(s);
        zzs.zzb(context);
        final zzeyv zzc = zzs.zzc();
        if (n >= (int)zzba.zzc().zza(zzbdc.zzfg)) {
            return (zzbu)zzc.zza();
        }
        return new zzew();
    }
    
    public final zzbu zzd(final IObjectWrapper objectWrapper, final zzq zzq, final String s, final zzbpr zzbpr, final int n) {
        final Context context = ObjectWrapper.unwrap(objectWrapper);
        final zzfai zzt = zzciq.zzb(context, zzbpr, n).zzt();
        zzt.zzc(context);
        zzt.zza(zzq);
        zzt.zzb(s);
        return (zzbu)zzt.zzd().zza();
    }
    
    public final zzbu zze(final IObjectWrapper objectWrapper, final zzq zzq, final String s, final zzbpr zzbpr, final int n) {
        final Context context = ObjectWrapper.unwrap(objectWrapper);
        final zzfbz zzu = zzciq.zzb(context, zzbpr, n).zzu();
        zzu.zzc(context);
        zzu.zza(zzq);
        zzu.zzb(s);
        return (zzbu)zzu.zzd().zza();
    }
    
    public final zzbu zzf(final IObjectWrapper objectWrapper, final zzq zzq, final String s, final int n) {
        return new zzs(ObjectWrapper.unwrap(objectWrapper), zzq, s, new zzcbt(234310000, n, true, false));
    }
    
    public final zzco zzg(final IObjectWrapper objectWrapper, final int n) {
        return (zzco)zzciq.zzb((Context)ObjectWrapper.unwrap(objectWrapper), (zzbpr)null, n).zzc();
    }
    
    public final zzdj zzh(final IObjectWrapper objectWrapper, final zzbpr zzbpr, final int n) {
        return (zzdj)zzciq.zzb((Context)ObjectWrapper.unwrap(objectWrapper), zzbpr, n).zzl();
    }
    
    public final zzbgm zzi(final IObjectWrapper objectWrapper, final IObjectWrapper objectWrapper2) {
        return (zzbgm)new zzdlg((FrameLayout)ObjectWrapper.unwrap(objectWrapper), (FrameLayout)ObjectWrapper.unwrap(objectWrapper2), 234310000);
    }
    
    public final zzbgs zzj(final IObjectWrapper objectWrapper, final IObjectWrapper objectWrapper2, final IObjectWrapper objectWrapper3) {
        return (zzbgs)new zzdle((View)ObjectWrapper.unwrap(objectWrapper), (HashMap)ObjectWrapper.unwrap(objectWrapper2), (HashMap)ObjectWrapper.unwrap(objectWrapper3));
    }
    
    public final zzble zzk(final IObjectWrapper objectWrapper, final zzbpr zzbpr, final int n, final zzblb zzblb) {
        final Context context = ObjectWrapper.unwrap(objectWrapper);
        final zzduy zzj = zzciq.zzb(context, zzbpr, n).zzj();
        zzj.zzb(context);
        zzj.zza(zzblb);
        return (zzble)zzj.zzc().zzd();
    }
    
    public final zzbti zzl(final IObjectWrapper objectWrapper, final zzbpr zzbpr, final int n) {
        return (zzbti)zzciq.zzb((Context)ObjectWrapper.unwrap(objectWrapper), zzbpr, n).zzm();
    }
    
    public final zzbtp zzm(final IObjectWrapper objectWrapper) {
        final Activity activity = ObjectWrapper.unwrap(objectWrapper);
        final AdOverlayInfoParcel zza = AdOverlayInfoParcel.zza(activity.getIntent());
        zzbto zzbto;
        if (zza == null) {
            zzbto = new zzt(activity);
        }
        else {
            final int zzk = zza.zzk;
            if (zzk != 1) {
                if (zzk != 2) {
                    if (zzk != 3) {
                        if (zzk != 4) {
                            if (zzk != 5) {
                                zzbto = new zzt(activity);
                            }
                            else {
                                zzbto = new zzac(activity);
                            }
                        }
                        else {
                            zzbto = new zzy(activity, zza);
                        }
                    }
                    else {
                        zzbto = new zzaf(activity);
                    }
                }
                else {
                    zzbto = new zzae(activity);
                }
            }
            else {
                zzbto = new com.google.android.gms.ads.internal.overlay.zzs(activity);
            }
        }
        return (zzbtp)zzbto;
    }
    
    public final zzbwt zzn(final IObjectWrapper objectWrapper, final zzbpr zzbpr, final int n) {
        final Context context = ObjectWrapper.unwrap(objectWrapper);
        final zzfdn zzv = zzciq.zzb(context, zzbpr, n).zzv();
        zzv.zzb(context);
        return (zzbwt)zzv.zzc().zzb();
    }
    
    public final zzbxj zzo(final IObjectWrapper objectWrapper, final String s, final zzbpr zzbpr, final int n) {
        final Context context = ObjectWrapper.unwrap(objectWrapper);
        final zzfdn zzv = zzciq.zzb(context, zzbpr, n).zzv();
        zzv.zzb(context);
        zzv.zza(s);
        return (zzbxj)zzv.zzc().zza();
    }
    
    public final zzcae zzp(final IObjectWrapper objectWrapper, final zzbpr zzbpr, final int n) {
        return (zzcae)zzciq.zzb((Context)ObjectWrapper.unwrap(objectWrapper), zzbpr, n).zzp();
    }
}

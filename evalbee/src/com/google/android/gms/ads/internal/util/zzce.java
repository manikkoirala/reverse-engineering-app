// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import java.util.Map;
import java.util.Iterator;
import com.google.android.gms.ads.internal.zzt;
import java.util.Set;
import android.content.SharedPreferences;
import android.content.SharedPreferences$OnSharedPreferenceChangeListener;

final class zzce implements SharedPreferences$OnSharedPreferenceChangeListener
{
    final zzcf zza;
    private final String zzb;
    
    public zzce(final zzcf zza, final String zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public final void onSharedPreferenceChanged(final SharedPreferences sharedPreferences, final String s) {
        synchronized (this.zza) {
            for (final zzcd zzcd : zzcf.zza(this.zza)) {
                final String zzb = this.zzb;
                final Map zza = zzcd.zza;
                if (zza.containsKey(zzb) && ((Set)zza.get(zzb)).contains(s)) {
                    zzt.zzo().zzi().zzI(false);
                }
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.AdValue;
import com.google.android.gms.ads.OnPaidEventListener;

public final class zzfe extends zzdf
{
    private final OnPaidEventListener zza;
    
    public zzfe(final OnPaidEventListener zza) {
        this.zza = zza;
    }
    
    public final void zze(final zzs zzs) {
        final OnPaidEventListener zza = this.zza;
        if (zza != null) {
            zza.onPaidEvent(AdValue.zza(zzs.zzb, zzs.zzc, zzs.zzd));
        }
    }
    
    public final boolean zzf() {
        return this.zza == null;
    }
}

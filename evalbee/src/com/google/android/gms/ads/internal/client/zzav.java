// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.ads.zzbxv;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.internal.ads.zzbpr;
import android.content.Context;

final class zzav extends zzax
{
    final Context zza;
    final String zzb;
    final zzbpr zzc;
    final zzaw zzd;
    
    public zzav(final zzaw zzd, final Context zza, final String zzb, final zzbpr zzc) {
        this.zzd = zzd;
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.BaseBundle;
import android.os.Bundle;
import com.google.android.gms.internal.ads.zzbfe;
import com.google.android.gms.internal.ads.zzbeq;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.dynamite.DynamiteModule;
import com.google.android.gms.internal.ads.zzcbg;
import android.content.Context;
import android.os.RemoteException;
import android.os.IInterface;
import com.google.android.gms.internal.ads.zzcbn;
import android.os.IBinder;

abstract class zzax
{
    private static final zzce zza;
    
    static {
        zzce zza2 = null;
        try {
            final Object instance = zzaw.class.getClassLoader().loadClass("com.google.android.gms.ads.internal.ClientApi").getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
            if (!(instance instanceof IBinder)) {
                zzcbn.zzj("ClientApi class is not an instance of IBinder.");
            }
            else {
                final IBinder binder = (IBinder)instance;
                if (binder != null) {
                    final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.ads.internal.client.IClientApi");
                    if (queryLocalInterface instanceof zzce) {
                        zza2 = (zzce)queryLocalInterface;
                    }
                    else {
                        zza2 = new zzcc(binder);
                    }
                }
            }
        }
        catch (final Exception ex) {
            zzcbn.zzj("Failed to instantiate ClientApi class.");
        }
        zza = zza2;
    }
    
    public zzax() {
    }
    
    private final Object zze() {
        final zzce zza = zzax.zza;
        if (zza != null) {
            try {
                return this.zzb(zza);
            }
            catch (final RemoteException ex) {
                zzcbn.zzk("Cannot invoke local loader using ClientApi class.", (Throwable)ex);
                return null;
            }
        }
        zzcbn.zzj("ClientApi class cannot be loaded.");
        return null;
    }
    
    private final Object zzf() {
        try {
            return this.zzc();
        }
        catch (final RemoteException ex) {
            zzcbn.zzk("Cannot invoke remote loader.", (Throwable)ex);
            return null;
        }
    }
    
    public abstract Object zza();
    
    public abstract Object zzb(final zzce p0);
    
    public abstract Object zzc();
    
    public final Object zzd(final Context context, final boolean b) {
        boolean b2 = b;
        if (!b) {
            zzay.zzb();
            b2 = b;
            if (!zzcbg.zzs(context, 12451000)) {
                zzcbn.zze("Google Play Services is not available.");
                b2 = true;
            }
        }
        final int localVersion = DynamiteModule.getLocalVersion(context, "com.google.android.gms.ads.dynamite");
        final int remoteVersion = DynamiteModule.getRemoteVersion(context, "com.google.android.gms.ads.dynamite");
        final int n = 0;
        final boolean b3 = localVersion <= remoteVersion;
        zzbdc.zza(context);
        int n3;
        int n4;
        if (zzbeq.zza.zze()) {
            final int n2 = 0;
            n3 = n;
            n4 = n2;
        }
        else if (zzbeq.zzb.zze()) {
            n4 = 1;
            n3 = 1;
        }
        else {
            n3 = ((b2 | (b3 ^ true)) ? 1 : 0);
            n4 = 0;
        }
        Object o;
        if (n3 != 0) {
            final Object zze = this.zze();
            if ((o = zze) == null) {
                o = zze;
                if (n4 == 0) {
                    o = this.zzf();
                }
            }
        }
        else {
            final Object zzf = this.zzf();
            if (zzf == null && zzay.zze().nextInt(((Long)zzbfe.zza.zze()).intValue()) == 0) {
                final Bundle bundle = new Bundle();
                ((BaseBundle)bundle).putString("action", "dynamite_load");
                ((BaseBundle)bundle).putInt("is_missing", 1);
                zzay.zzb().zzn(context, zzay.zzc().zza, "gmob-apps", bundle, true);
            }
            if (zzf == null) {
                o = this.zze();
            }
            else {
                o = zzf;
            }
        }
        Object zza;
        if ((zza = o) == null) {
            zza = this.zza();
        }
        return zza;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.internal.ads.zzcbo;
import java.util.ArrayList;
import android.location.Location;
import java.util.List;
import android.os.Bundle;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "AdRequestParcelCreator")
public final class zzl extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zzl> CREATOR;
    @Field(id = 1)
    public final int zza;
    @Deprecated
    @Field(id = 2)
    public final long zzb;
    @Field(id = 3)
    public final Bundle zzc;
    @Deprecated
    @Field(id = 4)
    public final int zzd;
    @Field(id = 5)
    public final List zze;
    @Field(id = 6)
    public final boolean zzf;
    @Field(id = 7)
    public final int zzg;
    @Field(id = 8)
    public final boolean zzh;
    @Field(id = 9)
    public final String zzi;
    @Field(id = 10)
    public final zzfh zzj;
    @Field(id = 11)
    public final Location zzk;
    @Field(id = 12)
    public final String zzl;
    @Field(id = 13)
    public final Bundle zzm;
    @Field(id = 14)
    public final Bundle zzn;
    @Field(id = 15)
    public final List zzo;
    @Field(id = 16)
    public final String zzp;
    @Field(id = 17)
    public final String zzq;
    @Deprecated
    @Field(id = 18)
    public final boolean zzr;
    @Field(id = 19)
    public final zzc zzs;
    @Field(id = 20)
    public final int zzt;
    @Field(id = 21)
    public final String zzu;
    @Field(id = 22)
    public final List zzv;
    @Field(id = 23)
    public final int zzw;
    @Field(id = 24)
    public final String zzx;
    @Field(id = 25)
    public final int zzy;
    
    static {
        CREATOR = (Parcelable$Creator)new zzn();
    }
    
    @Constructor
    public zzl(@Param(id = 1) final int zza, @Param(id = 2) final long zzb, @Param(id = 3) Bundle bundle, @Param(id = 4) final int zzd, @Param(id = 5) final List zze, @Param(id = 6) final boolean zzf, @Param(id = 7) final int zzg, @Param(id = 8) final boolean zzh, @Param(id = 9) final String zzi, @Param(id = 10) final zzfh zzj, @Param(id = 11) final Location zzk, @Param(id = 12) final String zzl, @Param(id = 13) final Bundle bundle2, @Param(id = 14) final Bundle zzn, @Param(id = 15) final List zzo, @Param(id = 16) final String zzp, @Param(id = 17) final String zzq, @Param(id = 18) final boolean zzr, @Param(id = 19) final zzc zzs, @Param(id = 20) final int zzt, @Param(id = 21) final String zzu, @Param(id = 22) final List list, @Param(id = 23) final int zzw, @Param(id = 24) final String zzx, @Param(id = 25) final int zzy) {
        this.zza = zza;
        this.zzb = zzb;
        if (bundle == null) {
            bundle = new Bundle();
        }
        this.zzc = bundle;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
        this.zzg = zzg;
        this.zzh = zzh;
        this.zzi = zzi;
        this.zzj = zzj;
        this.zzk = zzk;
        this.zzl = zzl;
        if (bundle2 == null) {
            bundle = new Bundle();
        }
        else {
            bundle = bundle2;
        }
        this.zzm = bundle;
        this.zzn = zzn;
        this.zzo = zzo;
        this.zzp = zzp;
        this.zzq = zzq;
        this.zzr = zzr;
        this.zzs = zzs;
        this.zzt = zzt;
        this.zzu = zzu;
        List zzv;
        if (list == null) {
            zzv = new ArrayList();
        }
        else {
            zzv = list;
        }
        this.zzv = zzv;
        this.zzw = zzw;
        this.zzx = zzx;
        this.zzy = zzy;
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (!(o instanceof zzl)) {
            return false;
        }
        final zzl zzl = (zzl)o;
        return this.zza == zzl.zza && this.zzb == zzl.zzb && zzcbo.zza(this.zzc, zzl.zzc) && this.zzd == zzl.zzd && Objects.equal(this.zze, zzl.zze) && this.zzf == zzl.zzf && this.zzg == zzl.zzg && this.zzh == zzl.zzh && Objects.equal(this.zzi, zzl.zzi) && Objects.equal(this.zzj, zzl.zzj) && Objects.equal(this.zzk, zzl.zzk) && Objects.equal(this.zzl, zzl.zzl) && zzcbo.zza(this.zzm, zzl.zzm) && zzcbo.zza(this.zzn, zzl.zzn) && Objects.equal(this.zzo, zzl.zzo) && Objects.equal(this.zzp, zzl.zzp) && Objects.equal(this.zzq, zzl.zzq) && this.zzr == zzl.zzr && this.zzt == zzl.zzt && Objects.equal(this.zzu, zzl.zzu) && Objects.equal(this.zzv, zzl.zzv) && this.zzw == zzl.zzw && Objects.equal(this.zzx, zzl.zzx) && this.zzy == zzl.zzy;
    }
    
    @Override
    public final int hashCode() {
        return Objects.hashCode(this.zza, this.zzb, this.zzc, this.zzd, this.zze, this.zzf, this.zzg, this.zzh, this.zzi, this.zzj, this.zzk, this.zzl, this.zzm, this.zzn, this.zzo, this.zzp, this.zzq, this.zzr, this.zzt, this.zzu, this.zzv, this.zzw, this.zzx, this.zzy);
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int zza = this.zza;
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, zza);
        SafeParcelWriter.writeLong(parcel, 2, this.zzb);
        SafeParcelWriter.writeBundle(parcel, 3, this.zzc, false);
        SafeParcelWriter.writeInt(parcel, 4, this.zzd);
        SafeParcelWriter.writeStringList(parcel, 5, this.zze, false);
        SafeParcelWriter.writeBoolean(parcel, 6, this.zzf);
        SafeParcelWriter.writeInt(parcel, 7, this.zzg);
        SafeParcelWriter.writeBoolean(parcel, 8, this.zzh);
        SafeParcelWriter.writeString(parcel, 9, this.zzi, false);
        SafeParcelWriter.writeParcelable(parcel, 10, (Parcelable)this.zzj, n, false);
        SafeParcelWriter.writeParcelable(parcel, 11, (Parcelable)this.zzk, n, false);
        SafeParcelWriter.writeString(parcel, 12, this.zzl, false);
        SafeParcelWriter.writeBundle(parcel, 13, this.zzm, false);
        SafeParcelWriter.writeBundle(parcel, 14, this.zzn, false);
        SafeParcelWriter.writeStringList(parcel, 15, this.zzo, false);
        SafeParcelWriter.writeString(parcel, 16, this.zzp, false);
        SafeParcelWriter.writeString(parcel, 17, this.zzq, false);
        SafeParcelWriter.writeBoolean(parcel, 18, this.zzr);
        SafeParcelWriter.writeParcelable(parcel, 19, (Parcelable)this.zzs, n, false);
        SafeParcelWriter.writeInt(parcel, 20, this.zzt);
        SafeParcelWriter.writeString(parcel, 21, this.zzu, false);
        SafeParcelWriter.writeStringList(parcel, 22, this.zzv, false);
        SafeParcelWriter.writeInt(parcel, 23, this.zzw);
        SafeParcelWriter.writeString(parcel, 24, this.zzx, false);
        SafeParcelWriter.writeInt(parcel, 25, this.zzy);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

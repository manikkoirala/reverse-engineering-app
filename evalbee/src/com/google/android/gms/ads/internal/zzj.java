// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "InterstitialAdParameterParcelCreator")
@Reserved({ 1 })
public final class zzj extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zzj> CREATOR;
    @Field(id = 2)
    public final boolean zza;
    @Field(id = 3)
    public final boolean zzb;
    @Field(id = 4)
    public final String zzc;
    @Field(id = 5)
    public final boolean zzd;
    @Field(id = 6)
    public final float zze;
    @Field(id = 7)
    public final int zzf;
    @Field(id = 8)
    public final boolean zzg;
    @Field(id = 9)
    public final boolean zzh;
    @Field(id = 10)
    public final boolean zzi;
    
    static {
        CREATOR = (Parcelable$Creator)new zzk();
    }
    
    @Constructor
    public zzj(@Param(id = 2) final boolean zza, @Param(id = 3) final boolean zzb, @Param(id = 4) final String zzc, @Param(id = 5) final boolean zzd, @Param(id = 6) final float zze, @Param(id = 7) final int zzf, @Param(id = 8) final boolean zzg, @Param(id = 9) final boolean zzh, @Param(id = 10) final boolean zzi) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
        this.zzg = zzg;
        this.zzh = zzh;
        this.zzi = zzi;
    }
    
    public zzj(final boolean b, final boolean b2, final boolean b3, final float n, final int n2, final boolean b4, final boolean b5, final boolean b6) {
        this(b, b2, null, b3, n, -1, b4, b5, b6);
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        final boolean zza = this.zza;
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeBoolean(parcel, 2, zza);
        SafeParcelWriter.writeBoolean(parcel, 3, this.zzb);
        SafeParcelWriter.writeString(parcel, 4, this.zzc, false);
        SafeParcelWriter.writeBoolean(parcel, 5, this.zzd);
        SafeParcelWriter.writeFloat(parcel, 6, this.zze);
        SafeParcelWriter.writeInt(parcel, 7, this.zzf);
        SafeParcelWriter.writeBoolean(parcel, 8, this.zzg);
        SafeParcelWriter.writeBoolean(parcel, 9, this.zzh);
        SafeParcelWriter.writeBoolean(parcel, 10, this.zzi);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import android.graphics.Bitmap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.Map;

public final class zzby
{
    final Map zza;
    private final AtomicInteger zzb;
    
    public zzby() {
        this.zza = new ConcurrentHashMap();
        this.zzb = new AtomicInteger(0);
    }
    
    public final Bitmap zza(final Integer n) {
        return this.zza.get(n);
    }
}

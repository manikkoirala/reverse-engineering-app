// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import java.util.ArrayList;
import java.util.List;

public final class zzbh
{
    private final String[] zza;
    private final double[] zzb;
    private final double[] zzc;
    private final int[] zzd;
    private int zze;
    
    private static final double[] zzc(final List list) {
        final int size = list.size();
        final double[] array = new double[size];
        for (int i = 0; i < size; ++i) {
            array[i] = (double)list.get(i);
        }
        return array;
    }
    
    public final List zza() {
        final ArrayList list = new ArrayList(this.zza.length);
        int n = 0;
        while (true) {
            final String[] zza = this.zza;
            if (n >= zza.length) {
                break;
            }
            final String s = zza[n];
            final double[] zzc = this.zzc;
            final double[] zzb = this.zzb;
            final int[] zzd = this.zzd;
            final double n2 = zzc[n];
            final double n3 = zzb[n];
            final int n4 = zzd[n];
            list.add(new zzbe(s, n2, n3, n4 / (double)this.zze, n4));
            ++n;
        }
        return list;
    }
    
    public final void zzb(final double n) {
        ++this.zze;
        int n2 = 0;
        while (true) {
            final double[] zzc = this.zzc;
            if (n2 >= zzc.length) {
                break;
            }
            final double n3 = zzc[n2];
            if (n3 <= n && n < this.zzb[n2]) {
                final int[] zzd = this.zzd;
                ++zzd[n2];
            }
            if (n < n3) {
                break;
            }
            ++n2;
        }
    }
}

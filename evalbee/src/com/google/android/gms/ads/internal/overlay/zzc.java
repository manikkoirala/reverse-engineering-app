// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.overlay;

import android.os.IInterface;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IBinder;
import com.google.android.gms.dynamic.ObjectWrapper;
import android.content.Intent;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "AdLauncherIntentInfoCreator")
@Reserved({ 1 })
public final class zzc extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zzc> CREATOR;
    @Field(id = 2)
    public final String zza;
    @Field(id = 3)
    public final String zzb;
    @Field(id = 4)
    public final String zzc;
    @Field(id = 5)
    public final String zzd;
    @Field(id = 6)
    public final String zze;
    @Field(id = 7)
    public final String zzf;
    @Field(id = 8)
    public final String zzg;
    @Field(id = 9)
    public final Intent zzh;
    @Field(getter = "getLaunchIntentListenerAsBinder", id = 10, type = "android.os.IBinder")
    public final zzx zzi;
    @Field(id = 11)
    public final boolean zzj;
    
    static {
        CREATOR = (Parcelable$Creator)new zzb();
    }
    
    public zzc(final Intent intent, final zzx zzx) {
        this(null, null, null, null, null, null, null, intent, ((IInterface)ObjectWrapper.wrap(zzx)).asBinder(), false);
    }
    
    @Constructor
    public zzc(@Param(id = 2) final String zza, @Param(id = 3) final String zzb, @Param(id = 4) final String zzc, @Param(id = 5) final String zzd, @Param(id = 6) final String zze, @Param(id = 7) final String zzf, @Param(id = 8) final String zzg, @Param(id = 9) final Intent zzh, @Param(id = 10) final IBinder binder, @Param(id = 11) final boolean zzj) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
        this.zzg = zzg;
        this.zzh = zzh;
        this.zzi = ObjectWrapper.unwrap(IObjectWrapper.Stub.asInterface(binder));
        this.zzj = zzj;
    }
    
    public zzc(final String s, final String s2, final String s3, final String s4, final String s5, final String s6, final String s7, final zzx zzx) {
        this(s, s2, s3, s4, s5, s6, s7, null, ((IInterface)ObjectWrapper.wrap(zzx)).asBinder(), false);
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final String zza = this.zza;
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 2, zza, false);
        SafeParcelWriter.writeString(parcel, 3, this.zzb, false);
        SafeParcelWriter.writeString(parcel, 4, this.zzc, false);
        SafeParcelWriter.writeString(parcel, 5, this.zzd, false);
        SafeParcelWriter.writeString(parcel, 6, this.zze, false);
        SafeParcelWriter.writeString(parcel, 7, this.zzf, false);
        SafeParcelWriter.writeString(parcel, 8, this.zzg, false);
        SafeParcelWriter.writeParcelable(parcel, 9, (Parcelable)this.zzh, n, false);
        SafeParcelWriter.writeIBinder(parcel, 10, ((IInterface)ObjectWrapper.wrap(this.zzi)).asBinder(), false);
        SafeParcelWriter.writeBoolean(parcel, 11, this.zzj);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

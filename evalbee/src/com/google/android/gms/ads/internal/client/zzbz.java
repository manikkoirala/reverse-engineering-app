// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.Parcel;
import android.os.IBinder;
import com.google.android.gms.internal.ads.zzavg;

public final class zzbz extends zzavg implements zzcb
{
    public zzbz(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IAppEventListener");
    }
    
    public final void zzc(final String s, final String s2) {
        final Parcel zza = this.zza();
        zza.writeString(s);
        zza.writeString(s2);
        this.zzbi(1, zza);
    }
}

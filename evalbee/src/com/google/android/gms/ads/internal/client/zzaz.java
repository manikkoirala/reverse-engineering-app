// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.AdListener;

public class zzaz extends AdListener
{
    private final Object zza;
    private AdListener zzb;
    
    public zzaz() {
        this.zza = new Object();
    }
    
    @Override
    public final void onAdClicked() {
        synchronized (this.zza) {
            final AdListener zzb = this.zzb;
            if (zzb != null) {
                zzb.onAdClicked();
            }
        }
    }
    
    @Override
    public final void onAdClosed() {
        synchronized (this.zza) {
            final AdListener zzb = this.zzb;
            if (zzb != null) {
                zzb.onAdClosed();
            }
        }
    }
    
    @Override
    public void onAdFailedToLoad(final LoadAdError loadAdError) {
        synchronized (this.zza) {
            final AdListener zzb = this.zzb;
            if (zzb != null) {
                zzb.onAdFailedToLoad(loadAdError);
            }
        }
    }
    
    @Override
    public final void onAdImpression() {
        synchronized (this.zza) {
            final AdListener zzb = this.zzb;
            if (zzb != null) {
                zzb.onAdImpression();
            }
        }
    }
    
    @Override
    public void onAdLoaded() {
        synchronized (this.zza) {
            final AdListener zzb = this.zzb;
            if (zzb != null) {
                zzb.onAdLoaded();
            }
        }
    }
    
    @Override
    public final void onAdOpened() {
        synchronized (this.zza) {
            final AdListener zzb = this.zzb;
            if (zzb != null) {
                zzb.onAdOpened();
            }
        }
    }
    
    public final void zza(final AdListener zzb) {
        synchronized (this.zza) {
            this.zzb = zzb;
        }
    }
}

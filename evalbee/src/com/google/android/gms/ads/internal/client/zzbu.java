// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.Bundle;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.ads.zzbww;
import com.google.android.gms.internal.ads.zzbub;
import com.google.android.gms.internal.ads.zzbea;
import com.google.android.gms.internal.ads.zzbty;
import com.google.android.gms.internal.ads.zzaxm;
import android.os.IInterface;

public interface zzbu extends IInterface
{
    void zzA();
    
    void zzB();
    
    void zzC(final zzbe p0);
    
    void zzD(final zzbh p0);
    
    void zzE(final zzby p0);
    
    void zzF(final zzq p0);
    
    void zzG(final zzcb p0);
    
    void zzH(final zzaxm p0);
    
    void zzI(final zzw p0);
    
    void zzJ(final zzci p0);
    
    void zzK(final zzdu p0);
    
    void zzL(final boolean p0);
    
    void zzM(final zzbty p0);
    
    void zzN(final boolean p0);
    
    void zzO(final zzbea p0);
    
    void zzP(final zzdg p0);
    
    void zzQ(final zzbub p0, final String p1);
    
    void zzR(final String p0);
    
    void zzS(final zzbww p0);
    
    void zzT(final String p0);
    
    void zzU(final zzfl p0);
    
    void zzW(final IObjectWrapper p0);
    
    void zzX();
    
    boolean zzY();
    
    boolean zzZ();
    
    boolean zzaa(final zzl p0);
    
    void zzab(final zzcf p0);
    
    Bundle zzd();
    
    zzq zzg();
    
    zzbh zzi();
    
    zzcb zzj();
    
    zzdn zzk();
    
    zzdq zzl();
    
    IObjectWrapper zzn();
    
    String zzr();
    
    String zzs();
    
    String zzt();
    
    void zzx();
    
    void zzy(final zzl p0, final zzbk p1);
    
    void zzz();
}

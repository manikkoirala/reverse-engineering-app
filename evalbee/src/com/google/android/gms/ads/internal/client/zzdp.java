// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.ads.zzavi;
import android.os.Parcel;
import android.os.IInterface;
import android.os.IBinder;
import com.google.android.gms.internal.ads.zzavh;

public abstract class zzdp extends zzavh implements zzdq
{
    public zzdp() {
        super("com.google.android.gms.ads.internal.client.IVideoController");
    }
    
    public static zzdq zzb(final IBinder binder) {
        if (binder == null) {
            return null;
        }
        final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.ads.internal.client.IVideoController");
        if (queryLocalInterface instanceof zzdq) {
            return (zzdq)queryLocalInterface;
        }
        return new zzdo(binder);
    }
    
    public final boolean zzbK(int n, final Parcel parcel, final Parcel parcel2, int zza) {
        Label_0307: {
            Label_0263: {
                Label_0254: {
                    float n2 = 0.0f;
                    switch (n) {
                        default: {
                            return false;
                        }
                        case 13: {
                            this.zzn();
                            break Label_0307;
                        }
                        case 12: {
                            n = (this.zzo() ? 1 : 0);
                            break Label_0254;
                        }
                        case 11: {
                            final zzdt zzi = this.zzi();
                            parcel2.writeNoException();
                            zzavi.zzf(parcel2, (IInterface)zzi);
                            return true;
                        }
                        case 10: {
                            n = (this.zzp() ? 1 : 0);
                            break Label_0254;
                        }
                        case 9: {
                            n2 = this.zze();
                            break;
                        }
                        case 8: {
                            final IBinder strongBinder = parcel.readStrongBinder();
                            zzdt zzdt;
                            if (strongBinder == null) {
                                zzdt = null;
                            }
                            else {
                                final IInterface queryLocalInterface = strongBinder.queryLocalInterface("com.google.android.gms.ads.internal.client.IVideoLifecycleCallbacks");
                                if (queryLocalInterface instanceof zzdt) {
                                    zzdt = (zzdt)queryLocalInterface;
                                }
                                else {
                                    zzdt = new zzdr(strongBinder);
                                }
                            }
                            zzavi.zzc(parcel);
                            this.zzm(zzdt);
                            break Label_0307;
                        }
                        case 7: {
                            n2 = this.zzf();
                            break;
                        }
                        case 6: {
                            n2 = this.zzg();
                            break;
                        }
                        case 5: {
                            n = this.zzh();
                            parcel2.writeNoException();
                            break Label_0263;
                        }
                        case 4: {
                            n = (this.zzq() ? 1 : 0);
                            break Label_0254;
                        }
                        case 3: {
                            final boolean zzg = zzavi.zzg(parcel);
                            zzavi.zzc(parcel);
                            this.zzj(zzg);
                            break Label_0307;
                        }
                        case 2: {
                            this.zzk();
                            break Label_0307;
                        }
                        case 1: {
                            this.zzl();
                            break Label_0307;
                        }
                    }
                    parcel2.writeNoException();
                    parcel2.writeFloat(n2);
                    return true;
                }
                parcel2.writeNoException();
                zza = zzavi.zza;
            }
            parcel2.writeInt(n);
            return true;
        }
        parcel2.writeNoException();
        return true;
    }
}

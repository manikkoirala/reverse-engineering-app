// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.ads.zzbpr;
import com.google.android.gms.dynamic.ObjectWrapper;
import android.content.Context;

final class zzal extends zzax
{
    final Context zza;
    final zzq zzb;
    final String zzc;
    final zzaw zzd;
    
    public zzal(final zzaw zzd, final Context zza, final zzq zzb, final String zzc) {
        this.zzd = zzd;
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
}

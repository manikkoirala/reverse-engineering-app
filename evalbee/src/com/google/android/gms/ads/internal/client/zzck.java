// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.ads.zzbpr;
import android.os.Parcelable;
import com.google.android.gms.internal.ads.zzavi;
import android.os.Parcel;
import android.os.IInterface;
import android.os.IBinder;
import com.google.android.gms.internal.ads.zzavh;

public abstract class zzck extends zzavh implements zzcl
{
    public zzck() {
        super("com.google.android.gms.ads.internal.client.ILiteSdkInfo");
    }
    
    public static zzcl asInterface(final IBinder binder) {
        if (binder == null) {
            return null;
        }
        final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.ads.internal.client.ILiteSdkInfo");
        if (queryLocalInterface instanceof zzcl) {
            return (zzcl)queryLocalInterface;
        }
        return new zzcj(binder);
    }
    
    public final boolean zzbK(final int n, final Parcel parcel, final Parcel parcel2, final int n2) {
        if (n != 1) {
            if (n != 2) {
                return false;
            }
            final zzbpr adapterCreator = this.getAdapterCreator();
            parcel2.writeNoException();
            zzavi.zzf(parcel2, (IInterface)adapterCreator);
        }
        else {
            final zzen liteSdkVersion = this.getLiteSdkVersion();
            parcel2.writeNoException();
            zzavi.zze(parcel2, (Parcelable)liteSdkVersion);
        }
        return true;
    }
}

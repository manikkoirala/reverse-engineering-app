// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal;

import android.app.Activity;
import android.view.View;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.internal.ads.zzfow;
import com.google.android.gms.internal.ads.zzfpp;
import com.google.android.gms.internal.ads.zzfov;
import com.google.android.gms.internal.ads.zzasb;
import com.google.android.gms.internal.ads.zzash;
import java.util.Iterator;
import android.view.MotionEvent;
import java.util.concurrent.ExecutorService;
import com.google.android.gms.internal.ads.zzcca;
import com.google.android.gms.internal.ads.zzcbg;
import com.google.android.gms.ads.internal.client.zzay;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.ads.internal.client.zzba;
import java.util.concurrent.Executors;
import java.util.Vector;
import com.google.android.gms.internal.ads.zzcbt;
import android.content.Context;
import com.google.android.gms.internal.ads.zzfnt;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicReference;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import com.google.android.gms.internal.ads.zzase;

public final class zzi implements Runnable, zzase
{
    protected boolean zza;
    final CountDownLatch zzb;
    private final List zzc;
    private final AtomicReference zzd;
    private final AtomicReference zze;
    private final boolean zzf;
    private final boolean zzg;
    private final Executor zzh;
    private final zzfnt zzi;
    private Context zzj;
    private final Context zzk;
    private zzcbt zzl;
    private final zzcbt zzm;
    private final boolean zzn;
    private int zzo;
    
    public zzi(final Context context, final zzcbt zzcbt) {
        this.zzc = new Vector();
        this.zzd = new AtomicReference();
        this.zze = new AtomicReference();
        this.zzb = new CountDownLatch(1);
        this.zzj = context;
        this.zzk = context;
        this.zzl = zzcbt;
        this.zzm = zzcbt;
        final ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
        this.zzh = cachedThreadPool;
        final boolean booleanValue = (boolean)zzba.zzc().zza(zzbdc.zzch);
        this.zzn = booleanValue;
        this.zzi = zzfnt.zza(context, (Executor)cachedThreadPool, booleanValue);
        this.zzf = (boolean)zzba.zzc().zza(zzbdc.zzcd);
        this.zzg = (boolean)zzba.zzc().zza(zzbdc.zzci);
        if (zzba.zzc().zza(zzbdc.zzcg)) {
            this.zzo = 2;
        }
        else {
            this.zzo = 1;
        }
        if (!(boolean)zzba.zzc().zza(zzbdc.zzdk)) {
            this.zza = this.zzc();
        }
        if (!(boolean)zzba.zzc().zza(zzbdc.zzdd)) {
            zzay.zzb();
            if (!zzcbg.zzu()) {
                this.run();
                return;
            }
        }
        ((Executor)zzcca.zza).execute(this);
    }
    
    private final zzase zzj() {
        AtomicReference atomicReference;
        if (this.zzi() == 2) {
            atomicReference = this.zze;
        }
        else {
            atomicReference = this.zzd;
        }
        return (zzase)atomicReference.get();
    }
    
    private final void zzm() {
        final List zzc = this.zzc;
        final zzase zzj = this.zzj();
        if (!zzc.isEmpty()) {
            if (zzj != null) {
                for (final Object[] array : this.zzc) {
                    final int length = array.length;
                    if (length == 1) {
                        zzj.zzk((MotionEvent)array[0]);
                    }
                    else {
                        if (length != 3) {
                            continue;
                        }
                        zzj.zzl((int)array[0], (int)array[1], (int)array[2]);
                    }
                }
                this.zzc.clear();
            }
        }
    }
    
    private final void zzp(final boolean b) {
        this.zzd.set(zzash.zzu(this.zzl.zza, zzq(this.zzj), b, this.zzo));
    }
    
    private static final Context zzq(final Context context) {
        final Context applicationContext = context.getApplicationContext();
        if (applicationContext == null) {
            return context;
        }
        return applicationContext;
    }
    
    @Override
    public final void run() {
        try {
            if (zzba.zzc().zza(zzbdc.zzdk)) {
                this.zza = this.zzc();
            }
            final boolean zzd = this.zzl.zzd;
            final boolean booleanValue = (boolean)zzba.zzc().zza(zzbdc.zzaV);
            boolean b2;
            final boolean b = b2 = false;
            if (!booleanValue) {
                b2 = b;
                if (zzd) {
                    b2 = true;
                }
            }
            if (this.zzi() == 1) {
                this.zzp(b2);
                if (this.zzo == 2) {
                    this.zzh.execute(new zzg(this, b2));
                }
            }
            else {
                final long currentTimeMillis = System.currentTimeMillis();
                try {
                    final zzasb zza = zzasb.zza(this.zzl.zza, zzq(this.zzj), b2, this.zzn);
                    this.zze.set(zza);
                    if (this.zzg && !zza.zzr()) {
                        this.zzo = 1;
                        this.zzp(b2);
                    }
                }
                catch (final NullPointerException ex) {
                    this.zzo = 1;
                    this.zzp(b2);
                    this.zzi.zzc(2031, System.currentTimeMillis() - currentTimeMillis, (Exception)ex);
                }
            }
        }
        finally {
            this.zzb.countDown();
            this.zzj = null;
            this.zzl = null;
        }
    }
    
    public final boolean zzc() {
        return new zzfpp(this.zzj, zzfov.zzb(this.zzj, this.zzi), (zzfow)new zzh(this), (boolean)zzba.zzc().zza(zzbdc.zzce)).zzd(1);
    }
    
    public final boolean zzd() {
        try {
            this.zzb.await();
            return true;
        }
        catch (final InterruptedException ex) {
            zzcbn.zzk("Interrupted during GADSignals creation.", (Throwable)ex);
            return false;
        }
    }
    
    public final String zze(final Context context, final String s, final View view) {
        return this.zzf(context, s, view, null);
    }
    
    public final String zzf(final Context context, final String s, final View view, final Activity activity) {
        if (this.zzd()) {
            final zzase zzj = this.zzj();
            if (zzba.zzc().zza(zzbdc.zzjW)) {
                zzt.zzp();
                com.google.android.gms.ads.internal.util.zzt.zzI(view, 4, null);
            }
            if (zzj != null) {
                this.zzm();
                return zzj.zzf(zzq(context), s, view, activity);
            }
        }
        return "";
    }
    
    public final String zzg(final Context context) {
        if (this.zzd()) {
            final zzase zzj = this.zzj();
            if (zzj != null) {
                this.zzm();
                return zzj.zzg(zzq(context));
            }
        }
        return "";
    }
    
    public final String zzh(final Context context, final View view, final Activity activity) {
        if (zzba.zzc().zza(zzbdc.zzjV)) {
            if (this.zzd()) {
                final zzase zzj = this.zzj();
                if (zzba.zzc().zza(zzbdc.zzjW)) {
                    zzt.zzp();
                    com.google.android.gms.ads.internal.util.zzt.zzI(view, 2, null);
                }
                if (zzj != null) {
                    return zzj.zzh(context, view, activity);
                }
            }
        }
        else {
            final zzase zzj2 = this.zzj();
            if (zzba.zzc().zza(zzbdc.zzjW)) {
                zzt.zzp();
                com.google.android.gms.ads.internal.util.zzt.zzI(view, 2, null);
            }
            if (zzj2 != null) {
                return zzj2.zzh(context, view, activity);
            }
        }
        return "";
    }
    
    public final int zzi() {
        if (this.zzf && !this.zza) {
            return 1;
        }
        return this.zzo;
    }
    
    public final void zzk(final MotionEvent motionEvent) {
        final zzase zzj = this.zzj();
        if (zzj != null) {
            this.zzm();
            zzj.zzk(motionEvent);
            return;
        }
        this.zzc.add(new Object[] { motionEvent });
    }
    
    public final void zzl(final int i, final int j, final int k) {
        final zzase zzj = this.zzj();
        if (zzj != null) {
            this.zzm();
            zzj.zzl(i, j, k);
            return;
        }
        this.zzc.add(new Object[] { i, j, k });
    }
    
    public final void zzn(final StackTraceElement[] array) {
        if (this.zzd()) {
            final zzase zzj = this.zzj();
            if (zzj != null) {
                zzj.zzn(array);
            }
        }
    }
    
    public final void zzo(final View view) {
        final zzase zzj = this.zzj();
        if (zzj != null) {
            zzj.zzo(view);
        }
    }
}

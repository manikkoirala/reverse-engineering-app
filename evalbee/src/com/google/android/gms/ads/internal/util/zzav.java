// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.util;

import com.google.android.gms.ads.internal.client.zze;
import android.content.Context;
import com.google.android.gms.ads.internal.client.zzcz;

final class zzav extends zzcz
{
    final Context zza;
    final zzay zzb;
    
    public zzav(final zzay zzb, final Context zza) {
        this.zzb = zzb;
        this.zza = zza;
    }
    
    public final void zze(final zze zze) {
        if (zze == null) {
            return;
        }
        this.zzb.zzi(this.zza, zze.zzb, true, true);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal;

import android.os.BaseBundle;
import java.util.Iterator;
import android.os.Bundle;
import com.google.android.gms.ads.internal.util.zzad;
import com.google.android.gms.internal.ads.zzbej;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.internal.ads.zzcbt;
import com.google.android.gms.ads.internal.client.zzl;
import android.content.pm.PackageManager$NameNotFoundException;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.common.wrappers.Wrappers;
import java.util.TreeMap;
import java.util.Map;
import android.content.Context;

final class zzr
{
    private final Context zza;
    private final String zzb;
    private final Map zzc;
    private String zzd;
    private String zze;
    private final String zzf;
    
    public zzr(final Context context, String packageName) {
        this.zza = context.getApplicationContext();
        this.zzb = packageName;
        this.zzc = new TreeMap();
        packageName = context.getPackageName();
        String zzf;
        try {
            final String versionName = Wrappers.packageManager(context).getPackageInfo(context.getPackageName(), 0).versionName;
            final StringBuilder sb = new StringBuilder();
            sb.append(packageName);
            sb.append("-");
            sb.append(versionName);
            zzf = sb.toString();
        }
        catch (final PackageManager$NameNotFoundException ex) {
            zzcbn.zzh("Unable to get package version name for reporting", (Throwable)ex);
            zzf = String.valueOf(packageName).concat("-missing");
        }
        this.zzf = zzf;
    }
    
    public final String zza() {
        return this.zzf;
    }
    
    public final String zzb() {
        return this.zze;
    }
    
    public final String zzc() {
        return this.zzb;
    }
    
    public final String zzd() {
        return this.zzd;
    }
    
    public final Map zze() {
        return this.zzc;
    }
    
    public final void zzf(final zzl zzl, final zzcbt zzcbt) {
        this.zzd = zzl.zzj.zza;
        final Bundle zzm = zzl.zzm;
        Object bundle;
        if (zzm != null) {
            bundle = zzm.getBundle(AdMobAdapter.class.getName());
        }
        else {
            bundle = null;
        }
        if (bundle != null) {
            final String s = (String)zzbej.zzc.zze();
            for (final String anObject : ((BaseBundle)bundle).keySet()) {
                if (s.equals(anObject)) {
                    this.zze = ((BaseBundle)bundle).getString(anObject);
                }
                else {
                    if (!anObject.startsWith("csa_")) {
                        continue;
                    }
                    this.zzc.put(anObject.substring(4), ((BaseBundle)bundle).getString(anObject));
                }
            }
            this.zzc.put("SDKVersion", zzcbt.zza);
            if (zzbej.zza.zze()) {
                final Bundle zzb = zzad.zzb(this.zza, (String)zzbej.zzb.zze());
                for (final String s2 : ((BaseBundle)zzb).keySet()) {
                    this.zzc.put(s2, ((BaseBundle)zzb).get(s2).toString());
                }
            }
        }
    }
}

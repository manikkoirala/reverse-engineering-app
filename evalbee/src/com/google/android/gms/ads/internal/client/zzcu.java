// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import android.os.Parcel;
import android.os.IBinder;
import com.google.android.gms.internal.ads.zzavg;

public final class zzcu extends zzavg implements zzcw
{
    public zzcu(final IBinder binder) {
        super(binder, "com.google.android.gms.ads.internal.client.IMuteThisAdReason");
    }
    
    public final String zze() {
        final Parcel zzbh = this.zzbh(1, this.zza());
        final String string = zzbh.readString();
        zzbh.recycle();
        return string;
    }
    
    public final String zzf() {
        final Parcel zzbh = this.zzbh(2, this.zza());
        final String string = zzbh.readString();
        zzbh.recycle();
        return string;
    }
}

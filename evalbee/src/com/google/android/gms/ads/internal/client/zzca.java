// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.internal.client;

import com.google.android.gms.internal.ads.zzavi;
import android.os.Parcel;
import android.os.IInterface;
import android.os.IBinder;
import com.google.android.gms.internal.ads.zzavh;

public abstract class zzca extends zzavh implements zzcb
{
    public zzca() {
        super("com.google.android.gms.ads.internal.client.IAppEventListener");
    }
    
    public static zzcb zzd(final IBinder binder) {
        final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.ads.internal.client.IAppEventListener");
        if (queryLocalInterface instanceof zzcb) {
            return (zzcb)queryLocalInterface;
        }
        return new zzbz(binder);
    }
    
    public final boolean zzbK(final int n, final Parcel parcel, final Parcel parcel2, final int n2) {
        if (n == 1) {
            final String string = parcel.readString();
            final String string2 = parcel.readString();
            zzavi.zzc(parcel);
            this.zzc(string, string2);
            parcel2.writeNoException();
            return true;
        }
        return false;
    }
}

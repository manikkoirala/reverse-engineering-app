// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

public final class AdValue
{
    private final int zza;
    private final String zzb;
    private final long zzc;
    
    private AdValue(final int zza, final String zzb, final long zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    public static AdValue zza(final int n, final String s, final long n2) {
        return new AdValue(n, s, n2);
    }
    
    public String getCurrencyCode() {
        return this.zzb;
    }
    
    public int getPrecisionType() {
        return this.zza;
    }
    
    public long getValueMicros() {
        return this.zzc;
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface PrecisionType {
        public static final int ESTIMATED = 1;
        public static final int PRECISE = 3;
        public static final int PUBLISHER_PROVIDED = 2;
        public static final int UNKNOWN = 0;
    }
}

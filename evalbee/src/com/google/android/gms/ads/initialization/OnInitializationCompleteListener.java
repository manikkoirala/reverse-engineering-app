// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.initialization;

public interface OnInitializationCompleteListener
{
    void onInitializationComplete(final InitializationStatus p0);
}

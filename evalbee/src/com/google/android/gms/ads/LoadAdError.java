// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads;

import org.json.JSONObject;
import org.json.JSONException;

public final class LoadAdError extends AdError
{
    private final ResponseInfo zza;
    
    public LoadAdError(final int n, final String s, final String s2, final AdError adError, final ResponseInfo zza) {
        super(n, s, s2, adError);
        this.zza = zza;
    }
    
    public ResponseInfo getResponseInfo() {
        return this.zza;
    }
    
    @Override
    public String toString() {
        String string;
        try {
            string = this.zzb().toString(2);
        }
        catch (final JSONException ex) {
            string = "Error forming toString output.";
        }
        return string;
    }
    
    @Override
    public final JSONObject zzb() {
        final JSONObject zzb = super.zzb();
        final ResponseInfo responseInfo = this.getResponseInfo();
        Object zzd;
        if (responseInfo == null) {
            zzd = "null";
        }
        else {
            zzd = responseInfo.zzd();
        }
        zzb.put("Response Info", zzd);
        return zzb;
    }
}

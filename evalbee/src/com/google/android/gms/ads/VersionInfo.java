// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads;

import java.util.Locale;

public class VersionInfo
{
    protected final int zza;
    protected final int zzb;
    protected final int zzc;
    
    public VersionInfo(final int zza, final int zzb, final int zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    public int getMajorVersion() {
        return this.zza;
    }
    
    public int getMicroVersion() {
        return this.zzc;
    }
    
    public int getMinorVersion() {
        return this.zzb;
    }
    
    @Override
    public String toString() {
        return String.format(Locale.US, "%d.%d.%d", this.zza, this.zzb, this.zzc);
    }
}

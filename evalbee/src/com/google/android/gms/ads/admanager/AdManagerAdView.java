// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.admanager;

import android.view.View;
import com.google.android.gms.ads.internal.client.zzbu;
import com.google.android.gms.internal.ads.zzbus;
import com.google.android.gms.internal.ads.zzcbc;
import com.google.android.gms.ads.internal.client.zzba;
import com.google.android.gms.internal.ads.zzbet;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.AdSize;
import android.util.AttributeSet;
import com.google.android.gms.common.internal.Preconditions;
import android.content.Context;
import com.google.android.gms.ads.BaseAdView;

public final class AdManagerAdView extends BaseAdView
{
    public AdManagerAdView(final Context context) {
        super(context, 0);
        Preconditions.checkNotNull(context, "Context cannot be null");
    }
    
    public AdManagerAdView(final Context context, final AttributeSet set) {
        super(context, set, true);
        Preconditions.checkNotNull(context, "Context cannot be null");
    }
    
    public AdManagerAdView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n, 0, true);
        Preconditions.checkNotNull(context, "Context cannot be null");
    }
    
    public AdSize[] getAdSizes() {
        return super.zza.zzB();
    }
    
    public AppEventListener getAppEventListener() {
        return super.zza.zzh();
    }
    
    public VideoController getVideoController() {
        return super.zza.zzf();
    }
    
    public VideoOptions getVideoOptions() {
        return super.zza.zzg();
    }
    
    public void loadAd(final AdManagerAdRequest adManagerAdRequest) {
        Preconditions.checkMainThread("#008 Must be called on the main UI thread.");
        zzbdc.zza(((View)this).getContext());
        if ((boolean)zzbet.zzf.zze() && (boolean)zzba.zzc().zza(zzbdc.zzkt)) {
            zzcbc.zzb.execute(new zzb(this, adManagerAdRequest));
            return;
        }
        super.zza.zzm(adManagerAdRequest.zza());
    }
    
    public void recordManualImpression() {
        super.zza.zzo();
    }
    
    public void setAdSizes(final AdSize... array) {
        if (array != null && array.length > 0) {
            super.zza.zzt(array);
            return;
        }
        throw new IllegalArgumentException("The supported ad sizes must contain at least one valid ad size.");
    }
    
    public void setAppEventListener(final AppEventListener appEventListener) {
        super.zza.zzv(appEventListener);
    }
    
    public void setManualImpressionsEnabled(final boolean b) {
        super.zza.zzw(b);
    }
    
    public void setVideoOptions(final VideoOptions videoOptions) {
        super.zza.zzy(videoOptions);
    }
    
    public final boolean zzb(final zzbu zzbu) {
        return super.zza.zzz(zzbu);
    }
}

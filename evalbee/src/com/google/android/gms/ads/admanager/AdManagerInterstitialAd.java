// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.admanager;

import com.google.android.gms.ads.AdLoadCallback;
import com.google.android.gms.internal.ads.zzbmw;
import com.google.android.gms.internal.ads.zzcbc;
import com.google.android.gms.ads.internal.client.zzba;
import com.google.android.gms.internal.ads.zzbet;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.common.internal.Preconditions;
import android.content.Context;
import com.google.android.gms.ads.interstitial.InterstitialAd;

public abstract class AdManagerInterstitialAd extends InterstitialAd
{
    public static void load(final Context context, final String s, final AdManagerAdRequest adManagerAdRequest, final AdManagerInterstitialAdLoadCallback adManagerInterstitialAdLoadCallback) {
        Preconditions.checkNotNull(context, "Context cannot be null.");
        Preconditions.checkNotNull(s, "AdUnitId cannot be null.");
        Preconditions.checkNotNull(adManagerAdRequest, "AdManagerAdRequest cannot be null.");
        Preconditions.checkNotNull(adManagerInterstitialAdLoadCallback, "LoadCallback cannot be null.");
        Preconditions.checkMainThread("#008 Must be called on the main UI thread.");
        zzbdc.zza(context);
        if ((boolean)zzbet.zzi.zze() && (boolean)zzba.zzc().zza(zzbdc.zzkt)) {
            zzcbc.zzb.execute(new zzc(context, s, adManagerAdRequest, adManagerInterstitialAdLoadCallback));
            return;
        }
        new zzbmw(context, s).zza(adManagerAdRequest.zza(), (AdLoadCallback)adManagerInterstitialAdLoadCallback);
    }
    
    public abstract AppEventListener getAppEventListener();
    
    public abstract void setAppEventListener(final AppEventListener p0);
}

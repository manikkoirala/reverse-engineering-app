// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.admanager;

import android.text.TextUtils;
import java.util.List;
import android.os.Bundle;
import com.google.android.gms.ads.AdRequest;

public final class AdManagerAdRequest extends AdRequest
{
    @Override
    public Bundle getCustomTargeting() {
        return super.zza.zze();
    }
    
    public String getPublisherProvidedId() {
        return super.zza.zzl();
    }
    
    public static final class Builder extends AdRequest.Builder
    {
        public Builder addCategoryExclusion(final String s) {
            super.zza.zzp(s);
            return this;
        }
        
        public Builder addCustomTargeting(final String s, final String s2) {
            super.zza.zzr(s, s2);
            return this;
        }
        
        public Builder addCustomTargeting(final String s, final List<String> list) {
            if (list != null) {
                super.zza.zzr(s, TextUtils.join((CharSequence)",", (Iterable)list));
            }
            return this;
        }
        
        public AdManagerAdRequest build() {
            return new AdManagerAdRequest(this, null);
        }
        
        public Builder setPublisherProvidedId(final String s) {
            super.zza.zzE(s);
            return this;
        }
    }
}

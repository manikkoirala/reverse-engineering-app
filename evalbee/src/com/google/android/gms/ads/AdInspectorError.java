// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

public final class AdInspectorError extends AdError
{
    public static final int ERROR_CODE_ALREADY_OPEN = 3;
    public static final int ERROR_CODE_FAILED_TO_LOAD = 1;
    public static final int ERROR_CODE_INTERNAL_ERROR = 0;
    public static final int ERROR_CODE_NOT_IN_TEST_MODE = 2;
    
    public AdInspectorError(final int n, final String s, final String s2) {
        super(n, s, s2);
    }
    
    @Override
    public int getCode() {
        return super.getCode();
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface AdInspectorErrorCode {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads;

public final class zzb
{
    public static int zza(final AdSize adSize) {
        return adSize.zzb();
    }
    
    public static int zzb(final AdSize adSize) {
        return adSize.zza();
    }
    
    public static AdSize zzc(final int n, final int n2, final String s) {
        return new AdSize(n, n2, s);
    }
    
    public static AdSize zzd(final int n, final int n2) {
        final AdSize adSize = new AdSize(n, n2);
        adSize.zze(true);
        adSize.zzc(n2);
        return adSize;
    }
    
    public static AdSize zze(final int n, final int n2) {
        final AdSize adSize = new AdSize(n, n2);
        adSize.zzf(true);
        adSize.zzd(n2);
        return adSize;
    }
    
    public static boolean zzf(final AdSize adSize) {
        return adSize.zzg();
    }
    
    public static boolean zzg(final AdSize adSize) {
        return adSize.zzh();
    }
    
    public static boolean zzh(final AdSize adSize) {
        return adSize.zzi();
    }
}

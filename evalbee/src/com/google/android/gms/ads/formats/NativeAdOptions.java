// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.formats;

import com.google.android.gms.ads.VideoOptions;

@Deprecated
public final class NativeAdOptions
{
    public static final int ADCHOICES_BOTTOM_LEFT = 3;
    public static final int ADCHOICES_BOTTOM_RIGHT = 2;
    public static final int ADCHOICES_TOP_LEFT = 0;
    public static final int ADCHOICES_TOP_RIGHT = 1;
    public static final int NATIVE_MEDIA_ASPECT_RATIO_ANY = 1;
    public static final int NATIVE_MEDIA_ASPECT_RATIO_LANDSCAPE = 2;
    public static final int NATIVE_MEDIA_ASPECT_RATIO_PORTRAIT = 3;
    public static final int NATIVE_MEDIA_ASPECT_RATIO_SQUARE = 4;
    public static final int NATIVE_MEDIA_ASPECT_RATIO_UNKNOWN = 0;
    @Deprecated
    public static final int ORIENTATION_ANY = 0;
    @Deprecated
    public static final int ORIENTATION_LANDSCAPE = 2;
    @Deprecated
    public static final int ORIENTATION_PORTRAIT = 1;
    private final boolean zza = Builder.zzf(builder);
    private final int zzb = Builder.zzb(builder);
    private final int zzc = Builder.zzc(builder);
    private final boolean zzd = Builder.zze(builder);
    private final int zze = Builder.zza(builder);
    private final VideoOptions zzf = Builder.zzd(builder);
    private final boolean zzg = Builder.zzg(builder);
    
    public int getAdChoicesPlacement() {
        return this.zze;
    }
    
    @Deprecated
    public int getImageOrientation() {
        return this.zzb;
    }
    
    public int getMediaAspectRatio() {
        return this.zzc;
    }
    
    public VideoOptions getVideoOptions() {
        return this.zzf;
    }
    
    public boolean shouldRequestMultipleImages() {
        return this.zzd;
    }
    
    public boolean shouldReturnUrlsForImageAssets() {
        return this.zza;
    }
    
    public final boolean zza() {
        return this.zzg;
    }
    
    public @interface AdChoicesPlacement {
    }
    
    public static final class Builder
    {
        private boolean zza;
        private int zzb;
        private int zzc;
        private boolean zzd;
        private VideoOptions zze;
        private int zzf;
        private boolean zzg;
        
        public Builder() {
            this.zza = false;
            this.zzb = -1;
            this.zzc = 0;
            this.zzd = false;
            this.zzf = 1;
            this.zzg = false;
        }
        
        public NativeAdOptions build() {
            return new NativeAdOptions(this, null);
        }
        
        public Builder setAdChoicesPlacement(@AdChoicesPlacement final int zzf) {
            this.zzf = zzf;
            return this;
        }
        
        @Deprecated
        public Builder setImageOrientation(final int zzb) {
            this.zzb = zzb;
            return this;
        }
        
        public Builder setMediaAspectRatio(@NativeMediaAspectRatio final int zzc) {
            this.zzc = zzc;
            return this;
        }
        
        public Builder setRequestCustomMuteThisAd(final boolean zzg) {
            this.zzg = zzg;
            return this;
        }
        
        public Builder setRequestMultipleImages(final boolean zzd) {
            this.zzd = zzd;
            return this;
        }
        
        public Builder setReturnUrlsForImageAssets(final boolean zza) {
            this.zza = zza;
            return this;
        }
        
        public Builder setVideoOptions(final VideoOptions zze) {
            this.zze = zze;
            return this;
        }
    }
    
    public @interface NativeMediaAspectRatio {
    }
}

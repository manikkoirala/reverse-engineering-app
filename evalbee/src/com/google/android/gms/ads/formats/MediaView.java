// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.formats;

import android.util.AttributeSet;
import android.content.Context;
import android.widget.ImageView$ScaleType;
import com.google.android.gms.ads.MediaContent;
import android.widget.FrameLayout;

@Deprecated
public class MediaView extends FrameLayout
{
    private MediaContent zza;
    private ImageView$ScaleType zzb;
    
    public MediaView(final Context context) {
        super(context);
    }
    
    public MediaView(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    public MediaView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    public MediaView(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
    }
    
    public void setImageScaleType(final ImageView$ScaleType zzb) {
        this.zzb = zzb;
    }
    
    public void setMediaContent(final MediaContent zza) {
        this.zza = zza;
    }
}

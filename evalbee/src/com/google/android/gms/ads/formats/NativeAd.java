// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.formats;

import android.net.Uri;
import android.graphics.drawable.Drawable;
import java.util.List;
import android.os.Bundle;

@Deprecated
public abstract class NativeAd
{
    public static final String ASSET_ADCHOICES_CONTAINER_VIEW = "1098";
    
    @Deprecated
    public abstract void performClick(final Bundle p0);
    
    @Deprecated
    public abstract boolean recordImpression(final Bundle p0);
    
    @Deprecated
    public abstract void reportTouchEvent(final Bundle p0);
    
    @Deprecated
    public abstract static class AdChoicesInfo
    {
        public abstract List<Image> getImages();
        
        public abstract CharSequence getText();
    }
    
    @Deprecated
    public abstract static class Image
    {
        public abstract Drawable getDrawable();
        
        public abstract double getScale();
        
        public abstract Uri getUri();
        
        public int zza() {
            return -1;
        }
        
        public int zzb() {
            return -1;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.formats;

import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.internal.ads.zzbhy;
import com.google.android.gms.internal.ads.zzbhz;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.ads.internal.client.zzfj;
import android.os.IBinder;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "AdManagerAdViewOptionsCreator")
public final class AdManagerAdViewOptions extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<AdManagerAdViewOptions> CREATOR;
    @Field(getter = "getManualImpressionsEnabled", id = 1)
    private final boolean zza = Builder.zzb(builder);
    @Field(getter = "getDelayedBannerAdListenerBinder", id = 2)
    private final IBinder zzb;
    
    static {
        CREATOR = (Parcelable$Creator)new zzc();
    }
    
    @Constructor
    public AdManagerAdViewOptions(@Param(id = 1) final boolean zza, @Param(id = 2) final IBinder zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public boolean getManualImpressionsEnabled() {
        return this.zza;
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeBoolean(parcel, 1, this.getManualImpressionsEnabled());
        SafeParcelWriter.writeIBinder(parcel, 2, this.zzb, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public final zzbhz zza() {
        final IBinder zzb = this.zzb;
        if (zzb == null) {
            return null;
        }
        return zzbhy.zzc(zzb);
    }
    
    public static final class Builder
    {
        private boolean zza;
        private ShouldDelayBannerRenderingListener zzb;
        
        public Builder() {
            this.zza = false;
        }
        
        public AdManagerAdViewOptions build() {
            return new AdManagerAdViewOptions(this, null);
        }
        
        public Builder setManualImpressionsEnabled(final boolean zza) {
            this.zza = zza;
            return this;
        }
        
        @Deprecated
        @KeepForSdk
        public Builder setShouldDelayBannerRenderingListener(final ShouldDelayBannerRenderingListener zzb) {
            this.zzb = zzb;
            return this;
        }
    }
}

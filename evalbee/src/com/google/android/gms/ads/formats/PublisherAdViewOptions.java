// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.formats;

import android.os.IInterface;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.internal.ads.zzbhy;
import com.google.android.gms.internal.ads.zzbhz;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.ads.internal.client.zzca;
import android.os.IBinder;
import com.google.android.gms.ads.internal.client.zzcb;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Deprecated
@Class(creator = "PublisherAdViewOptionsCreator")
public final class PublisherAdViewOptions extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<PublisherAdViewOptions> CREATOR;
    @Field(getter = "getManualImpressionsEnabled", id = 1)
    private final boolean zza;
    @Field(getter = "getAppEventListenerBinder", id = 2, type = "android.os.IBinder")
    private final zzcb zzb;
    @Field(getter = "getDelayedBannerAdListenerBinder", id = 3)
    private final IBinder zzc;
    
    static {
        CREATOR = (Parcelable$Creator)new zzf();
    }
    
    @Constructor
    public PublisherAdViewOptions(@Param(id = 1) final boolean zza, @Param(id = 2) final IBinder binder, @Param(id = 3) final IBinder zzc) {
        this.zza = zza;
        zzcb zzd;
        if (binder != null) {
            zzd = zzca.zzd(binder);
        }
        else {
            zzd = null;
        }
        this.zzb = zzd;
        this.zzc = zzc;
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeBoolean(parcel, 1, this.zza);
        final zzcb zzb = this.zzb;
        IBinder binder;
        if (zzb == null) {
            binder = null;
        }
        else {
            binder = ((IInterface)zzb).asBinder();
        }
        SafeParcelWriter.writeIBinder(parcel, 2, binder, false);
        SafeParcelWriter.writeIBinder(parcel, 3, this.zzc, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public final zzcb zza() {
        return this.zzb;
    }
    
    public final zzbhz zzb() {
        final IBinder zzc = this.zzc;
        if (zzc == null) {
            return null;
        }
        return zzbhy.zzc(zzc);
    }
    
    public final boolean zzc() {
        return this.zza;
    }
    
    @Deprecated
    public static final class Builder
    {
        private ShouldDelayBannerRenderingListener zza;
        
        @KeepForSdk
        public Builder setShouldDelayBannerRenderingListener(final ShouldDelayBannerRenderingListener zza) {
            this.zza = zza;
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads;

import java.util.Iterator;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.ads.internal.client.zzba;
import java.util.List;
import android.content.Context;

public class MediationUtils
{
    protected static final double MIN_HEIGHT_RATIO = 0.7;
    protected static final double MIN_WIDTH_RATIO = 0.5;
    
    public static AdSize findClosestSize(final Context context, AdSize adSize, final List<AdSize> list) {
        final AdSize adSize2 = null;
        final AdSize adSize3 = null;
        AdSize adSize4 = adSize2;
        if (list != null) {
            if (adSize == null) {
                adSize4 = adSize2;
            }
            else {
                AdSize adSize5 = adSize;
                if (!adSize.zzh()) {
                    adSize5 = adSize;
                    if (!adSize.zzi()) {
                        final float density = context.getResources().getDisplayMetrics().density;
                        adSize5 = new AdSize(Math.round(adSize.getWidthInPixels(context) / density), Math.round(adSize.getHeightInPixels(context) / density));
                    }
                }
                final Iterator<AdSize> iterator = list.iterator();
                AdSize adSize6 = adSize3;
                while (true) {
                    adSize4 = adSize6;
                    if (!iterator.hasNext()) {
                        break;
                    }
                    adSize = iterator.next();
                    if (adSize == null) {
                        continue;
                    }
                    final int width = adSize5.getWidth();
                    final int width2 = adSize.getWidth();
                    final int height = adSize5.getHeight();
                    final double n = width;
                    final double n2 = width2;
                    final int height2 = adSize.getHeight();
                    if (n * 0.5 > n2 || width < width2) {
                        continue;
                    }
                    if (adSize5.zzi()) {
                        final int zza = adSize5.zza();
                        if ((int)zzba.zzc().zza(zzbdc.zzhJ) > width2 || (int)zzba.zzc().zza(zzbdc.zzhK) > height2 || zza < height2) {
                            continue;
                        }
                    }
                    else if (adSize5.zzh()) {
                        if (adSize5.zzb() < height2) {
                            continue;
                        }
                    }
                    else {
                        if (height * 0.7 > height2) {
                            continue;
                        }
                        if (height < height2) {
                            continue;
                        }
                    }
                    if (adSize6 != null) {
                        if (adSize6.getWidth() * adSize6.getHeight() > adSize.getWidth() * adSize.getHeight()) {
                            continue;
                        }
                    }
                    adSize6 = adSize;
                }
            }
        }
        return adSize4;
    }
}

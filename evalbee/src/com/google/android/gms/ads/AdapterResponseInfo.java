// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads;

import android.os.BaseBundle;
import java.util.Iterator;
import org.json.JSONObject;
import org.json.JSONException;
import android.os.Bundle;
import com.google.android.gms.ads.internal.client.zze;
import com.google.android.gms.ads.internal.client.zzu;

public final class AdapterResponseInfo
{
    private final zzu zza;
    private final AdError zzb;
    
    private AdapterResponseInfo(final zzu zza) {
        this.zza = zza;
        final zze zzc = zza.zzc;
        AdError zza2;
        if (zzc == null) {
            zza2 = null;
        }
        else {
            zza2 = zzc.zza();
        }
        this.zzb = zza2;
    }
    
    public static AdapterResponseInfo zza(final zzu zzu) {
        if (zzu != null) {
            return new AdapterResponseInfo(zzu);
        }
        return null;
    }
    
    public AdError getAdError() {
        return this.zzb;
    }
    
    public String getAdSourceId() {
        return this.zza.zzf;
    }
    
    public String getAdSourceInstanceId() {
        return this.zza.zzh;
    }
    
    public String getAdSourceInstanceName() {
        return this.zza.zzg;
    }
    
    public String getAdSourceName() {
        return this.zza.zze;
    }
    
    public String getAdapterClassName() {
        return this.zza.zza;
    }
    
    public Bundle getCredentials() {
        return this.zza.zzd;
    }
    
    public long getLatencyMillis() {
        return this.zza.zzb;
    }
    
    @Override
    public String toString() {
        String string;
        try {
            string = this.zzb().toString(2);
        }
        catch (final JSONException ex) {
            string = "Error forming toString output.";
        }
        return string;
    }
    
    public final JSONObject zzb() {
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("Adapter", (Object)this.zza.zza);
        jsonObject.put("Latency", this.zza.zzb);
        final String adSourceName = this.getAdSourceName();
        if (adSourceName == null) {
            jsonObject.put("Ad Source Name", (Object)"null");
        }
        else {
            jsonObject.put("Ad Source Name", (Object)adSourceName);
        }
        final String adSourceId = this.getAdSourceId();
        if (adSourceId == null) {
            jsonObject.put("Ad Source ID", (Object)"null");
        }
        else {
            jsonObject.put("Ad Source ID", (Object)adSourceId);
        }
        final String adSourceInstanceName = this.getAdSourceInstanceName();
        if (adSourceInstanceName == null) {
            jsonObject.put("Ad Source Instance Name", (Object)"null");
        }
        else {
            jsonObject.put("Ad Source Instance Name", (Object)adSourceInstanceName);
        }
        final String adSourceInstanceId = this.getAdSourceInstanceId();
        if (adSourceInstanceId == null) {
            jsonObject.put("Ad Source Instance ID", (Object)"null");
        }
        else {
            jsonObject.put("Ad Source Instance ID", (Object)adSourceInstanceId);
        }
        final JSONObject jsonObject2 = new JSONObject();
        for (final String s : ((BaseBundle)this.zza.zzd).keySet()) {
            jsonObject2.put(s, ((BaseBundle)this.zza.zzd).get(s));
        }
        jsonObject.put("Credentials", (Object)jsonObject2);
        final AdError zzb = this.zzb;
        if (zzb == null) {
            jsonObject.put("Ad Error", (Object)"null");
        }
        else {
            jsonObject.put("Ad Error", (Object)zzb.zzb());
        }
        return jsonObject;
    }
}

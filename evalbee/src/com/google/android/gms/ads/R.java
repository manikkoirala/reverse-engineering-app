// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads;

public final class R
{
    private R() {
    }
    
    public static final class attr
    {
        public static final int adSize = 2130968616;
        public static final int adSizes = 2130968617;
        public static final int adUnitId = 2130968618;
        
        private attr() {
        }
    }
    
    public static final class id
    {
        public static final int layout = 2131296717;
        
        private id() {
        }
    }
    
    public static final class layout
    {
        public static final int admob_empty_layout = 2131492937;
        
        private layout() {
        }
    }
    
    public static final class style
    {
        public static final int Theme_IAPTheme = 2131952215;
        
        private style() {
        }
    }
    
    public static final class styleable
    {
        public static final int[] AdsAttrs;
        public static final int AdsAttrs_adSize = 0;
        public static final int AdsAttrs_adSizes = 1;
        public static final int AdsAttrs_adUnitId = 2;
        
        static {
            AdsAttrs = new int[] { 2130968616, 2130968617, 2130968618 };
        }
        
        private styleable() {
        }
    }
    
    public static final class xml
    {
        public static final int gma_ad_services_config = 2132082691;
        
        private xml() {
        }
    }
}

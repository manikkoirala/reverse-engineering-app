// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads;

import android.content.Intent;
import com.google.android.gms.ads.internal.client.zzdj;
import android.os.RemoteException;
import com.google.android.gms.dynamic.ObjectWrapper;
import android.widget.LinearLayout;
import com.google.android.gms.internal.ads.zzbpr;
import android.content.Context;
import com.google.android.gms.internal.ads.zzbpo;
import com.google.android.gms.ads.internal.client.zzay;
import android.os.Bundle;
import com.google.android.gms.common.annotation.KeepForSdk;
import android.app.Activity;

@KeepForSdk
public final class OutOfContextTestingActivity extends Activity
{
    @KeepForSdk
    public static final String AD_UNIT_KEY = "adUnit";
    @KeepForSdk
    public static final String CLASS_NAME = "com.google.android.gms.ads.OutOfContextTestingActivity";
    
    public final void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final zzdj zzf = zzay.zza().zzf((Context)this, (zzbpr)new zzbpo());
        if (zzf == null) {
            this.finish();
            return;
        }
        this.setContentView(R.layout.admob_empty_layout);
        final LinearLayout linearLayout = (LinearLayout)this.findViewById(R.id.layout);
        final Intent intent = this.getIntent();
        if (intent == null) {
            this.finish();
            return;
        }
        final String stringExtra = intent.getStringExtra("adUnit");
        if (stringExtra == null) {
            this.finish();
            return;
        }
        try {
            zzf.zze(stringExtra, ObjectWrapper.wrap(this), ObjectWrapper.wrap(linearLayout));
        }
        catch (final RemoteException ex) {
            this.finish();
        }
    }
}

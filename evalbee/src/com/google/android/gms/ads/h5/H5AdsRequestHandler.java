// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.h5;

import android.content.Context;
import com.google.android.gms.internal.ads.zzbli;

public final class H5AdsRequestHandler
{
    private final zzbli zza;
    
    public H5AdsRequestHandler(final Context context, final OnH5AdsEventListener onH5AdsEventListener) {
        this.zza = new zzbli(context, onH5AdsEventListener);
    }
    
    public void clearAdObjects() {
        this.zza.zza();
    }
    
    public boolean handleH5AdsRequest(final String s) {
        return this.zza.zzb(s);
    }
    
    public boolean shouldInterceptRequest(final String s) {
        return zzbli.zzc(s);
    }
}

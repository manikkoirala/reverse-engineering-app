// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.h5;

import android.webkit.WebViewClient;
import android.webkit.WebView;
import android.content.Context;
import com.google.android.gms.internal.ads.zzblk;
import com.google.android.gms.internal.ads.zzbkx;

public final class H5AdsWebViewClient extends zzbkx
{
    private final zzblk zza;
    
    public H5AdsWebViewClient(final Context context, final WebView webView) {
        this.zza = new zzblk(context, webView);
    }
    
    public void clearAdObjects() {
        this.zza.zza();
    }
    
    public WebViewClient getDelegate() {
        return (WebViewClient)this.zza;
    }
    
    public WebViewClient getDelegateWebViewClient() {
        return this.zza.getDelegate();
    }
    
    public void setDelegateWebViewClient(final WebViewClient webViewClient) {
        this.zza.zzb(webViewClient);
    }
}

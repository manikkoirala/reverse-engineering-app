// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads;

import com.google.android.gms.ads.internal.client.zzay;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;
import android.os.Bundle;
import java.util.Iterator;
import android.os.RemoteException;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.ads.internal.client.zzu;
import java.util.ArrayList;
import java.util.List;
import com.google.android.gms.ads.internal.client.zzdn;

public final class ResponseInfo
{
    private final zzdn zza;
    private final List zzb;
    private AdapterResponseInfo zzc;
    
    private ResponseInfo(zzdn zza) {
        this.zza = zza;
        this.zzb = new ArrayList();
        if (zza != null) {
            try {
                final List zzj = zza.zzj();
                if (zzj != null) {
                    final Iterator iterator = zzj.iterator();
                    while (iterator.hasNext()) {
                        final AdapterResponseInfo zza2 = AdapterResponseInfo.zza((zzu)iterator.next());
                        if (zza2 != null) {
                            this.zzb.add(zza2);
                        }
                    }
                }
            }
            catch (final RemoteException ex) {
                zzcbn.zzh("Could not forward getAdapterResponseInfo to ResponseInfo.", (Throwable)ex);
            }
        }
        zza = this.zza;
        if (zza == null) {
            return;
        }
        try {
            final zzu zzf = zza.zzf();
            if (zzf != null) {
                this.zzc = AdapterResponseInfo.zza(zzf);
            }
        }
        catch (final RemoteException ex2) {
            zzcbn.zzh("Could not forward getLoadedAdapterResponse to ResponseInfo.", (Throwable)ex2);
        }
    }
    
    public static ResponseInfo zza(final zzdn zzdn) {
        if (zzdn != null) {
            return new ResponseInfo(zzdn);
        }
        return null;
    }
    
    public static ResponseInfo zzb(final zzdn zzdn) {
        return new ResponseInfo(zzdn);
    }
    
    public List<AdapterResponseInfo> getAdapterResponses() {
        return this.zzb;
    }
    
    public AdapterResponseInfo getLoadedAdapterResponseInfo() {
        return this.zzc;
    }
    
    public String getMediationAdapterClassName() {
        try {
            final zzdn zza = this.zza;
            if (zza != null) {
                return zza.zzg();
            }
        }
        catch (final RemoteException ex) {
            zzcbn.zzh("Could not forward getMediationAdapterClassName to ResponseInfo.", (Throwable)ex);
        }
        return null;
    }
    
    public Bundle getResponseExtras() {
        try {
            final zzdn zza = this.zza;
            if (zza != null) {
                return zza.zze();
            }
        }
        catch (final RemoteException ex) {
            zzcbn.zzh("Could not forward getResponseExtras to ResponseInfo.", (Throwable)ex);
        }
        return new Bundle();
    }
    
    public String getResponseId() {
        try {
            final zzdn zza = this.zza;
            if (zza != null) {
                return zza.zzi();
            }
        }
        catch (final RemoteException ex) {
            zzcbn.zzh("Could not forward getResponseId to ResponseInfo.", (Throwable)ex);
        }
        return null;
    }
    
    @Override
    public String toString() {
        String string;
        try {
            string = this.zzd().toString(2);
        }
        catch (final JSONException ex) {
            string = "Error forming toString output.";
        }
        return string;
    }
    
    public final zzdn zzc() {
        return this.zza;
    }
    
    public final JSONObject zzd() {
        final JSONObject jsonObject = new JSONObject();
        final String responseId = this.getResponseId();
        if (responseId == null) {
            jsonObject.put("Response ID", (Object)"null");
        }
        else {
            jsonObject.put("Response ID", (Object)responseId);
        }
        final String mediationAdapterClassName = this.getMediationAdapterClassName();
        if (mediationAdapterClassName == null) {
            jsonObject.put("Mediation Adapter Class Name", (Object)"null");
        }
        else {
            jsonObject.put("Mediation Adapter Class Name", (Object)mediationAdapterClassName);
        }
        final JSONArray jsonArray = new JSONArray();
        final Iterator iterator = this.zzb.iterator();
        while (iterator.hasNext()) {
            jsonArray.put((Object)((AdapterResponseInfo)iterator.next()).zzb());
        }
        jsonObject.put("Adapter Responses", (Object)jsonArray);
        final AdapterResponseInfo zzc = this.zzc;
        if (zzc != null) {
            jsonObject.put("Loaded Adapter Response", (Object)zzc.zzb());
        }
        final Bundle responseExtras = this.getResponseExtras();
        if (responseExtras != null) {
            jsonObject.put("Response Extras", (Object)zzay.zzb().zzh(responseExtras));
        }
        return jsonObject;
    }
}

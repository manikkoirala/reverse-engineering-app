// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads;

import com.google.android.gms.internal.ads.zzbhc;
import android.graphics.drawable.Drawable;

public interface MediaContent
{
    float getAspectRatio();
    
    float getCurrentTime();
    
    float getDuration();
    
    Drawable getMainImage();
    
    VideoController getVideoController();
    
    boolean hasVideoContent();
    
    void setMainImage(final Drawable p0);
    
    zzbhc zza();
    
    boolean zzb();
}

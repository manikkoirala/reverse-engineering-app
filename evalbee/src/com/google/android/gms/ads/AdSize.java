// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads;

import android.os.Parcelable$Creator;
import android.util.DisplayMetrics;
import com.google.android.gms.ads.internal.client.zzq;
import com.google.android.gms.ads.internal.client.zzay;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.internal.ads.zzcbg;
import android.content.Context;

public final class AdSize
{
    public static final int AUTO_HEIGHT = -2;
    public static final AdSize BANNER;
    public static final AdSize FLUID;
    public static final AdSize FULL_BANNER;
    public static final int FULL_WIDTH = -1;
    public static final AdSize INVALID;
    public static final AdSize LARGE_BANNER;
    public static final AdSize LEADERBOARD;
    public static final AdSize MEDIUM_RECTANGLE;
    public static final AdSize SEARCH;
    @Deprecated
    public static final AdSize SMART_BANNER;
    public static final AdSize WIDE_SKYSCRAPER;
    public static final AdSize zza;
    private final int zzb;
    private final int zzc;
    private final String zzd;
    private boolean zze;
    private boolean zzf;
    private int zzg;
    private boolean zzh;
    private int zzi;
    
    static {
        BANNER = new AdSize(320, 50, "320x50_mb");
        FULL_BANNER = new AdSize(468, 60, "468x60_as");
        LARGE_BANNER = new AdSize(320, 100, "320x100_as");
        LEADERBOARD = new AdSize(728, 90, "728x90_as");
        MEDIUM_RECTANGLE = new AdSize(300, 250, "300x250_as");
        WIDE_SKYSCRAPER = new AdSize(160, 600, "160x600_as");
        SMART_BANNER = new AdSize(-1, -2, "smart_banner");
        FLUID = new AdSize(-3, -4, "fluid");
        INVALID = new AdSize(0, 0, "invalid");
        zza = new AdSize(50, 50, "50x50_mb");
        SEARCH = new AdSize(-3, 0, "search_v2");
    }
    
    public AdSize(final int i, final int j) {
        String value;
        if (i == -1) {
            value = "FULL";
        }
        else {
            value = String.valueOf(i);
        }
        String value2;
        if (j == -2) {
            value2 = "AUTO";
        }
        else {
            value2 = String.valueOf(j);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(value);
        sb.append("x");
        sb.append(value2);
        sb.append("_as");
        this(i, j, sb.toString());
    }
    
    public AdSize(final int n, final int n2, final String zzd) {
        if (n < 0 && n != -1 && n != -3) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid width for AdSize: ");
            sb.append(n);
            throw new IllegalArgumentException(sb.toString());
        }
        if (n2 < 0 && n2 != -2 && n2 != -4) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Invalid height for AdSize: ");
            sb2.append(n2);
            throw new IllegalArgumentException(sb2.toString());
        }
        this.zzb = n;
        this.zzc = n2;
        this.zzd = zzd;
    }
    
    public static AdSize getCurrentOrientationAnchoredAdaptiveBannerAdSize(final Context context, final int n) {
        final AdSize zzc = zzcbg.zzc(context, n, 50, 0);
        zzc.zze = true;
        return zzc;
    }
    
    public static AdSize getCurrentOrientationInlineAdaptiveBannerAdSize(final Context context, final int n) {
        final int zza = zzcbg.zza(context, 0);
        if (zza == -1) {
            return AdSize.INVALID;
        }
        final AdSize adSize = new AdSize(n, 0);
        adSize.zzg = zza;
        adSize.zzf = true;
        return adSize;
    }
    
    public static AdSize getCurrentOrientationInterscrollerAdSize(final Context context, final int n) {
        return zzj(n, zzcbg.zza(context, 0));
    }
    
    public static AdSize getInlineAdaptiveBannerAdSize(final int n, final int n2) {
        final AdSize adSize = new AdSize(n, 0);
        adSize.zzg = n2;
        adSize.zzf = true;
        if (n2 < 32) {
            final StringBuilder sb = new StringBuilder();
            sb.append("The maximum height set for the inline adaptive ad size was ");
            sb.append(n2);
            sb.append(" dp, which is below the minimum recommended value of 32 dp.");
            zzcbn.zzj(sb.toString());
        }
        return adSize;
    }
    
    public static AdSize getLandscapeAnchoredAdaptiveBannerAdSize(final Context context, final int n) {
        final AdSize zzc = zzcbg.zzc(context, n, 50, 2);
        zzc.zze = true;
        return zzc;
    }
    
    public static AdSize getLandscapeInlineAdaptiveBannerAdSize(final Context context, final int n) {
        final int zza = zzcbg.zza(context, 2);
        final AdSize adSize = new AdSize(n, 0);
        if (zza == -1) {
            return AdSize.INVALID;
        }
        adSize.zzg = zza;
        adSize.zzf = true;
        return adSize;
    }
    
    public static AdSize getLandscapeInterscrollerAdSize(final Context context, final int n) {
        return zzj(n, zzcbg.zza(context, 2));
    }
    
    public static AdSize getPortraitAnchoredAdaptiveBannerAdSize(final Context context, final int n) {
        final AdSize zzc = zzcbg.zzc(context, n, 50, 1);
        zzc.zze = true;
        return zzc;
    }
    
    public static AdSize getPortraitInlineAdaptiveBannerAdSize(final Context context, final int n) {
        final int zza = zzcbg.zza(context, 1);
        final AdSize adSize = new AdSize(n, 0);
        if (zza == -1) {
            return AdSize.INVALID;
        }
        adSize.zzg = zza;
        adSize.zzf = true;
        return adSize;
    }
    
    public static AdSize getPortraitInterscrollerAdSize(final Context context, final int n) {
        return zzj(n, zzcbg.zza(context, 1));
    }
    
    private static AdSize zzj(final int n, final int zzi) {
        if (zzi == -1) {
            return AdSize.INVALID;
        }
        final AdSize adSize = new AdSize(n, 0);
        adSize.zzi = zzi;
        adSize.zzh = true;
        return adSize;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        if (!(o instanceof AdSize)) {
            return false;
        }
        final AdSize adSize = (AdSize)o;
        return this.zzb == adSize.zzb && this.zzc == adSize.zzc && this.zzd.equals(adSize.zzd);
    }
    
    public int getHeight() {
        return this.zzc;
    }
    
    public int getHeightInPixels(final Context context) {
        final int zzc = this.zzc;
        if (zzc == -4 || zzc == -3) {
            return -1;
        }
        if (zzc != -2) {
            zzay.zzb();
            return zzcbg.zzx(context, zzc);
        }
        return zzq.zza(context.getResources().getDisplayMetrics());
    }
    
    public int getWidth() {
        return this.zzb;
    }
    
    public int getWidthInPixels(final Context context) {
        final int zzb = this.zzb;
        if (zzb == -3) {
            return -1;
        }
        if (zzb != -1) {
            zzay.zzb();
            return zzcbg.zzx(context, zzb);
        }
        final DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        final Parcelable$Creator<zzq> creator = zzq.CREATOR;
        return displayMetrics.widthPixels;
    }
    
    @Override
    public int hashCode() {
        return this.zzd.hashCode();
    }
    
    public boolean isAutoHeight() {
        return this.zzc == -2;
    }
    
    public boolean isFluid() {
        return this.zzb == -3 && this.zzc == -4;
    }
    
    public boolean isFullWidth() {
        return this.zzb == -1;
    }
    
    @Override
    public String toString() {
        return this.zzd;
    }
    
    public final int zza() {
        return this.zzi;
    }
    
    public final int zzb() {
        return this.zzg;
    }
    
    public final void zzc(final int zzg) {
        this.zzg = zzg;
    }
    
    public final void zzd(final int zzi) {
        this.zzi = zzi;
    }
    
    public final void zze(final boolean b) {
        this.zzf = true;
    }
    
    public final void zzf(final boolean b) {
        this.zzh = true;
    }
    
    public final boolean zzg() {
        return this.zze;
    }
    
    public final boolean zzh() {
        return this.zzf;
    }
    
    public final boolean zzi() {
        return this.zzh;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.mediation.rtb;

import com.google.android.gms.ads.mediation.MediationConfiguration;
import com.google.android.gms.ads.AdSize;
import android.os.Bundle;
import java.util.List;
import android.content.Context;

public class RtbSignalData
{
    private final Context zza;
    private final List zzb;
    private final Bundle zzc;
    private final AdSize zzd;
    
    public RtbSignalData(final Context zza, final List<MediationConfiguration> zzb, final Bundle zzc, final AdSize zzd) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
    }
    
    public AdSize getAdSize() {
        return this.zzd;
    }
    
    @Deprecated
    public MediationConfiguration getConfiguration() {
        final List zzb = this.zzb;
        if (zzb != null && zzb.size() > 0) {
            return this.zzb.get(0);
        }
        return null;
    }
    
    public List<MediationConfiguration> getConfigurations() {
        return this.zzb;
    }
    
    public Context getContext() {
        return this.zza;
    }
    
    public Bundle getNetworkExtras() {
        return this.zzc;
    }
}

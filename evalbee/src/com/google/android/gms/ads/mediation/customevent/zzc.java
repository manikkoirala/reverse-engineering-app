// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.mediation.customevent;

import com.google.android.gms.ads.mediation.UnifiedNativeAdMapper;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.mediation.MediationNativeAdapter;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.ads.mediation.MediationNativeListener;

final class zzc implements CustomEventNativeListener
{
    private final CustomEventAdapter zza;
    private final MediationNativeListener zzb;
    
    public zzc(final CustomEventAdapter zza, final MediationNativeListener zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    @Override
    public final void onAdClicked() {
        zzcbn.zze("Custom event adapter called onAdClicked.");
        this.zzb.onAdClicked(this.zza);
    }
    
    @Override
    public final void onAdClosed() {
        zzcbn.zze("Custom event adapter called onAdClosed.");
        this.zzb.onAdClosed(this.zza);
    }
    
    @Override
    public final void onAdFailedToLoad(final int n) {
        zzcbn.zze("Custom event adapter called onAdFailedToLoad.");
        this.zzb.onAdFailedToLoad(this.zza, n);
    }
    
    @Override
    public final void onAdFailedToLoad(final AdError adError) {
        zzcbn.zze("Custom event adapter called onAdFailedToLoad.");
        this.zzb.onAdFailedToLoad(this.zza, adError);
    }
    
    @Override
    public final void onAdImpression() {
        zzcbn.zze("Custom event adapter called onAdImpression.");
        this.zzb.onAdImpression(this.zza);
    }
    
    @Override
    public final void onAdLeftApplication() {
        zzcbn.zze("Custom event adapter called onAdLeftApplication.");
        this.zzb.onAdLeftApplication(this.zza);
    }
    
    @Override
    public final void onAdLoaded(final UnifiedNativeAdMapper unifiedNativeAdMapper) {
        zzcbn.zze("Custom event adapter called onAdLoaded.");
        this.zzb.onAdLoaded(this.zza, unifiedNativeAdMapper);
    }
    
    @Override
    public final void onAdOpened() {
        zzcbn.zze("Custom event adapter called onAdOpened.");
        this.zzb.onAdOpened(this.zza);
    }
}

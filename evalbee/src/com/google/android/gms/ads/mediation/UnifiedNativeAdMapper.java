// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.mediation;

import java.util.Map;
import android.os.Bundle;
import android.view.View;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.formats.NativeAd;
import java.util.List;

public abstract class UnifiedNativeAdMapper
{
    private String zza;
    private List zzb;
    private String zzc;
    private NativeAd.Image zzd;
    private String zze;
    private String zzf;
    private Double zzg;
    private String zzh;
    private String zzi;
    private VideoController zzj;
    private boolean zzk;
    private View zzl;
    private View zzm;
    private Object zzn;
    private Bundle zzo;
    private boolean zzp;
    private boolean zzq;
    private float zzr;
    
    public UnifiedNativeAdMapper() {
        this.zzo = new Bundle();
    }
    
    public View getAdChoicesContent() {
        return this.zzl;
    }
    
    public final String getAdvertiser() {
        return this.zzf;
    }
    
    public final String getBody() {
        return this.zzc;
    }
    
    public final String getCallToAction() {
        return this.zze;
    }
    
    public float getCurrentTime() {
        return 0.0f;
    }
    
    public float getDuration() {
        return 0.0f;
    }
    
    public final Bundle getExtras() {
        return this.zzo;
    }
    
    public final String getHeadline() {
        return this.zza;
    }
    
    public final NativeAd.Image getIcon() {
        return this.zzd;
    }
    
    public final List<NativeAd.Image> getImages() {
        return this.zzb;
    }
    
    public float getMediaContentAspectRatio() {
        return this.zzr;
    }
    
    public final boolean getOverrideClickHandling() {
        return this.zzq;
    }
    
    public final boolean getOverrideImpressionRecording() {
        return this.zzp;
    }
    
    public final String getPrice() {
        return this.zzi;
    }
    
    public final Double getStarRating() {
        return this.zzg;
    }
    
    public final String getStore() {
        return this.zzh;
    }
    
    public void handleClick(final View view) {
    }
    
    public boolean hasVideoContent() {
        return this.zzk;
    }
    
    public void recordImpression() {
    }
    
    public void setAdChoicesContent(final View zzl) {
        this.zzl = zzl;
    }
    
    public final void setAdvertiser(final String zzf) {
        this.zzf = zzf;
    }
    
    public final void setBody(final String zzc) {
        this.zzc = zzc;
    }
    
    public final void setCallToAction(final String zze) {
        this.zze = zze;
    }
    
    public final void setExtras(final Bundle zzo) {
        this.zzo = zzo;
    }
    
    public void setHasVideoContent(final boolean zzk) {
        this.zzk = zzk;
    }
    
    public final void setHeadline(final String zza) {
        this.zza = zza;
    }
    
    public final void setIcon(final NativeAd.Image zzd) {
        this.zzd = zzd;
    }
    
    public final void setImages(final List<NativeAd.Image> zzb) {
        this.zzb = zzb;
    }
    
    public void setMediaContentAspectRatio(final float zzr) {
        this.zzr = zzr;
    }
    
    public void setMediaView(final View zzm) {
        this.zzm = zzm;
    }
    
    public final void setOverrideClickHandling(final boolean zzq) {
        this.zzq = zzq;
    }
    
    public final void setOverrideImpressionRecording(final boolean zzp) {
        this.zzp = zzp;
    }
    
    public final void setPrice(final String zzi) {
        this.zzi = zzi;
    }
    
    public final void setStarRating(final Double zzg) {
        this.zzg = zzg;
    }
    
    public final void setStore(final String zzh) {
        this.zzh = zzh;
    }
    
    public void trackViews(final View view, final Map<String, View> map, final Map<String, View> map2) {
    }
    
    public void untrackView(final View view) {
    }
    
    public final View zza() {
        return this.zzm;
    }
    
    public final VideoController zzb() {
        return this.zzj;
    }
    
    public final Object zzc() {
        return this.zzn;
    }
    
    public final void zzd(final Object zzn) {
        this.zzn = zzn;
    }
    
    public final void zze(final VideoController zzj) {
        this.zzj = zzj;
    }
}

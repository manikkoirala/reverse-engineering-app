// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.mediation.customevent;

import android.view.View;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.mediation.MediationBannerAdapter;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.ads.mediation.MediationBannerListener;

final class zza implements CustomEventBannerListener
{
    private final CustomEventAdapter zza;
    private final MediationBannerListener zzb;
    
    public zza(final CustomEventAdapter zza, final MediationBannerListener zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    @Override
    public final void onAdClicked() {
        zzcbn.zze("Custom event adapter called onAdClicked.");
        this.zzb.onAdClicked(this.zza);
    }
    
    @Override
    public final void onAdClosed() {
        zzcbn.zze("Custom event adapter called onAdClosed.");
        this.zzb.onAdClosed(this.zza);
    }
    
    @Override
    public final void onAdFailedToLoad(final int n) {
        zzcbn.zze("Custom event adapter called onAdFailedToLoad.");
        this.zzb.onAdFailedToLoad(this.zza, n);
    }
    
    @Override
    public final void onAdFailedToLoad(final AdError adError) {
        zzcbn.zze("Custom event adapter called onAdFailedToLoad.");
        this.zzb.onAdFailedToLoad(this.zza, adError);
    }
    
    @Override
    public final void onAdLeftApplication() {
        zzcbn.zze("Custom event adapter called onAdLeftApplication.");
        this.zzb.onAdLeftApplication(this.zza);
    }
    
    @Override
    public final void onAdLoaded(final View view) {
        zzcbn.zze("Custom event adapter called onAdLoaded.");
        CustomEventAdapter.zza(this.zza, view);
        this.zzb.onAdLoaded(this.zza);
    }
    
    @Override
    public final void onAdOpened() {
        zzcbn.zze("Custom event adapter called onAdOpened.");
        this.zzb.onAdOpened(this.zza);
    }
}

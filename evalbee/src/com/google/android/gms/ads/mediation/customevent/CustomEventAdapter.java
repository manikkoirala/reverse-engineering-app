// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.mediation.customevent;

import android.os.BaseBundle;
import com.google.android.gms.ads.mediation.NativeMediationAdRequest;
import com.google.android.gms.ads.mediation.MediationNativeListener;
import com.google.android.gms.ads.mediation.MediationInterstitialListener;
import com.google.android.gms.ads.mediation.MediationAdRequest;
import com.google.android.gms.ads.AdSize;
import android.os.Bundle;
import com.google.android.gms.ads.mediation.MediationBannerListener;
import android.content.Context;
import com.google.android.gms.internal.ads.zzcbn;
import android.view.View;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.annotation.KeepForSdkWithMembers;
import com.google.android.gms.ads.mediation.MediationNativeAdapter;
import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.ads.mediation.MediationBannerAdapter;

@KeepForSdkWithMembers
@KeepName
public final class CustomEventAdapter implements MediationBannerAdapter, MediationInterstitialAdapter, MediationNativeAdapter
{
    static final AdError zza;
    CustomEventBanner zzb;
    CustomEventInterstitial zzc;
    CustomEventNative zzd;
    private View zze;
    
    static {
        zza = new AdError(0, "Could not instantiate custom event adapter", "com.google.android.gms.ads");
    }
    
    private static Object zzb(final Class clazz, final String s) {
        s.getClass();
        try {
            return clazz.cast(Class.forName(s).getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]));
        }
        finally {
            final Throwable t;
            final String message = t.getMessage();
            final StringBuilder sb = new StringBuilder();
            sb.append("Could not instantiate custom event adapter: ");
            sb.append(s);
            sb.append(". ");
            sb.append(message);
            zzcbn.zzj(sb.toString());
            return null;
        }
    }
    
    @Override
    public View getBannerView() {
        return this.zze;
    }
    
    @Override
    public void onDestroy() {
        final CustomEventBanner zzb = this.zzb;
        if (zzb != null) {
            zzb.onDestroy();
        }
        final CustomEventInterstitial zzc = this.zzc;
        if (zzc != null) {
            zzc.onDestroy();
        }
        final CustomEventNative zzd = this.zzd;
        if (zzd != null) {
            zzd.onDestroy();
        }
    }
    
    @Override
    public void onPause() {
        final CustomEventBanner zzb = this.zzb;
        if (zzb != null) {
            zzb.onPause();
        }
        final CustomEventInterstitial zzc = this.zzc;
        if (zzc != null) {
            zzc.onPause();
        }
        final CustomEventNative zzd = this.zzd;
        if (zzd != null) {
            zzd.onPause();
        }
    }
    
    @Override
    public void onResume() {
        final CustomEventBanner zzb = this.zzb;
        if (zzb != null) {
            zzb.onResume();
        }
        final CustomEventInterstitial zzc = this.zzc;
        if (zzc != null) {
            zzc.onResume();
        }
        final CustomEventNative zzd = this.zzd;
        if (zzd != null) {
            zzd.onResume();
        }
    }
    
    @Override
    public void requestBannerAd(final Context context, final MediationBannerListener mediationBannerListener, final Bundle bundle, final AdSize adSize, final MediationAdRequest mediationAdRequest, Bundle bundle2) {
        final CustomEventBanner zzb = (CustomEventBanner)zzb(CustomEventBanner.class, ((BaseBundle)bundle).getString("class_name"));
        this.zzb = zzb;
        if (zzb == null) {
            mediationBannerListener.onAdFailedToLoad(this, CustomEventAdapter.zza);
            return;
        }
        if (bundle2 == null) {
            bundle2 = null;
        }
        else {
            bundle2 = bundle2.getBundle(((BaseBundle)bundle).getString("class_name"));
        }
        final CustomEventBanner zzb2 = this.zzb;
        zzb2.getClass();
        zzb2.requestBannerAd(context, new zza(this, mediationBannerListener), ((BaseBundle)bundle).getString("parameter"), adSize, mediationAdRequest, bundle2);
    }
    
    @Override
    public void requestInterstitialAd(final Context context, final MediationInterstitialListener mediationInterstitialListener, final Bundle bundle, final MediationAdRequest mediationAdRequest, Bundle bundle2) {
        final CustomEventInterstitial zzc = (CustomEventInterstitial)zzb(CustomEventInterstitial.class, ((BaseBundle)bundle).getString("class_name"));
        this.zzc = zzc;
        if (zzc == null) {
            mediationInterstitialListener.onAdFailedToLoad(this, CustomEventAdapter.zza);
            return;
        }
        if (bundle2 == null) {
            bundle2 = null;
        }
        else {
            bundle2 = bundle2.getBundle(((BaseBundle)bundle).getString("class_name"));
        }
        final CustomEventInterstitial zzc2 = this.zzc;
        zzc2.getClass();
        zzc2.requestInterstitialAd(context, new zzb(this, this, mediationInterstitialListener), ((BaseBundle)bundle).getString("parameter"), mediationAdRequest, bundle2);
    }
    
    @Override
    public void requestNativeAd(final Context context, final MediationNativeListener mediationNativeListener, final Bundle bundle, final NativeMediationAdRequest nativeMediationAdRequest, Bundle bundle2) {
        final CustomEventNative zzd = (CustomEventNative)zzb(CustomEventNative.class, ((BaseBundle)bundle).getString("class_name"));
        this.zzd = zzd;
        if (zzd == null) {
            mediationNativeListener.onAdFailedToLoad(this, CustomEventAdapter.zza);
            return;
        }
        if (bundle2 == null) {
            bundle2 = null;
        }
        else {
            bundle2 = bundle2.getBundle(((BaseBundle)bundle).getString("class_name"));
        }
        final CustomEventNative zzd2 = this.zzd;
        zzd2.getClass();
        zzd2.requestNativeAd(context, new zzc(this, mediationNativeListener), ((BaseBundle)bundle).getString("parameter"), nativeMediationAdRequest, bundle2);
    }
    
    @Override
    public void showInterstitial() {
        final CustomEventInterstitial zzc = this.zzc;
        if (zzc != null) {
            zzc.showInterstitial();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.mediation.customevent;

import com.google.android.gms.ads.AdError;

@Deprecated
public interface CustomEventListener
{
    void onAdClicked();
    
    void onAdClosed();
    
    @Deprecated
    void onAdFailedToLoad(final int p0);
    
    void onAdFailedToLoad(final AdError p0);
    
    void onAdLeftApplication();
    
    void onAdOpened();
}

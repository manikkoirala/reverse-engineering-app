// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.mediation;

import android.os.Bundle;
import com.google.android.gms.ads.AdFormat;

public class MediationConfiguration
{
    public static final String CUSTOM_EVENT_SERVER_PARAMETER_FIELD = "parameter";
    private final AdFormat zza;
    private final Bundle zzb;
    
    public MediationConfiguration(final AdFormat zza, final Bundle zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public AdFormat getFormat() {
        return this.zza;
    }
    
    public Bundle getServerParameters() {
        return this.zzb;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.mediation;

import com.google.android.gms.ads.AdError;

public interface MediationAdLoadCallback<MediationAdT, MediationAdCallbackT>
{
    void onFailure(final AdError p0);
    
    @Deprecated
    void onFailure(final String p0);
    
    MediationAdCallbackT onSuccess(final MediationAdT p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.mediation;

public interface InitializationCompleteCallback
{
    void onInitializationFailed(final String p0);
    
    void onInitializationSucceeded();
}

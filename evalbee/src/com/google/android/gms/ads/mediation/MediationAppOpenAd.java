// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.mediation;

import android.content.Context;

public interface MediationAppOpenAd
{
    void showAd(final Context p0);
}

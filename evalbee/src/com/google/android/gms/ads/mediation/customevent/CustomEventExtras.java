// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.mediation.customevent;

import java.util.HashMap;

@Deprecated
public final class CustomEventExtras
{
    private final HashMap zza;
    
    public CustomEventExtras() {
        this.zza = new HashMap();
    }
    
    public Object getExtra(final String key) {
        return this.zza.get(key);
    }
    
    public void setExtra(final String key, final Object value) {
        this.zza.put(key, value);
    }
}

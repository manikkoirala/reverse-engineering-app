// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.mediation;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.location.Location;
import android.content.Context;
import android.os.Bundle;

public class MediationAdConfiguration
{
    public static final int TAG_FOR_CHILD_DIRECTED_TREATMENT_FALSE = 0;
    public static final int TAG_FOR_CHILD_DIRECTED_TREATMENT_TRUE = 1;
    public static final int TAG_FOR_CHILD_DIRECTED_TREATMENT_UNSPECIFIED = -1;
    private final String zza;
    private final Bundle zzb;
    private final Bundle zzc;
    private final Context zzd;
    private final boolean zze;
    private final int zzf;
    private final int zzg;
    private final String zzh;
    private final String zzi;
    
    public MediationAdConfiguration(final Context zzd, final String zza, final Bundle zzb, final Bundle zzc, final boolean zze, final Location location, final int zzf, final int zzg, final String zzh, final String zzi) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
        this.zzg = zzg;
        this.zzh = zzh;
        this.zzi = zzi;
    }
    
    public String getBidResponse() {
        return this.zza;
    }
    
    public Context getContext() {
        return this.zzd;
    }
    
    public String getMaxAdContentRating() {
        return this.zzh;
    }
    
    public Bundle getMediationExtras() {
        return this.zzc;
    }
    
    public Bundle getServerParameters() {
        return this.zzb;
    }
    
    public String getWatermark() {
        return this.zzi;
    }
    
    public boolean isTestRequest() {
        return this.zze;
    }
    
    public int taggedForChildDirectedTreatment() {
        return this.zzf;
    }
    
    public int taggedForUnderAgeTreatment() {
        return this.zzg;
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface TagForChildDirectedTreatment {
    }
}

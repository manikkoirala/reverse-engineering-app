// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.mediation;

import com.google.android.gms.ads.nativead.NativeAdOptions;
import android.location.Location;
import android.os.Bundle;
import android.content.Context;
import com.google.android.gms.internal.ads.zzbfw;

public class MediationNativeAdConfiguration extends MediationAdConfiguration
{
    private final zzbfw zza;
    
    public MediationNativeAdConfiguration(final Context context, final String s, final Bundle bundle, final Bundle bundle2, final boolean b, final Location location, final int n, final int n2, final String s2, final String s3, final zzbfw zza) {
        super(context, s, bundle, bundle2, b, location, n, n2, s2, s3);
        this.zza = zza;
    }
    
    public NativeAdOptions getNativeAdOptions() {
        return zzbfw.zza(this.zza);
    }
}

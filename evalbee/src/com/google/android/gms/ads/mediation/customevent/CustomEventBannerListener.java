// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.mediation.customevent;

import android.view.View;

@Deprecated
public interface CustomEventBannerListener extends CustomEventListener
{
    void onAdLoaded(final View p0);
}

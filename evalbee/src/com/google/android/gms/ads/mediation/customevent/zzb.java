// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.mediation.customevent;

import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.mediation.MediationInterstitialAdapter;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.ads.mediation.MediationInterstitialListener;

final class zzb implements CustomEventInterstitialListener
{
    final CustomEventAdapter zza;
    private final CustomEventAdapter zzb;
    private final MediationInterstitialListener zzc;
    
    public zzb(final CustomEventAdapter zza, final CustomEventAdapter zzb, final MediationInterstitialListener zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    @Override
    public final void onAdClicked() {
        zzcbn.zze("Custom event adapter called onAdClicked.");
        this.zzc.onAdClicked(this.zzb);
    }
    
    @Override
    public final void onAdClosed() {
        zzcbn.zze("Custom event adapter called onAdClosed.");
        this.zzc.onAdClosed(this.zzb);
    }
    
    @Override
    public final void onAdFailedToLoad(final int n) {
        zzcbn.zze("Custom event adapter called onFailedToReceiveAd.");
        this.zzc.onAdFailedToLoad(this.zzb, n);
    }
    
    @Override
    public final void onAdFailedToLoad(final AdError adError) {
        zzcbn.zze("Custom event adapter called onFailedToReceiveAd.");
        this.zzc.onAdFailedToLoad(this.zzb, adError);
    }
    
    @Override
    public final void onAdLeftApplication() {
        zzcbn.zze("Custom event adapter called onAdLeftApplication.");
        this.zzc.onAdLeftApplication(this.zzb);
    }
    
    @Override
    public final void onAdLoaded() {
        zzcbn.zze("Custom event adapter called onReceivedAd.");
        this.zzc.onAdLoaded(this.zza);
    }
    
    @Override
    public final void onAdOpened() {
        zzcbn.zze("Custom event adapter called onAdOpened.");
        this.zzc.onAdOpened(this.zzb);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.mediation.rtb;

import com.google.android.gms.ads.AdError;

public interface SignalCallbacks
{
    void onFailure(final AdError p0);
    
    @Deprecated
    void onFailure(final String p0);
    
    void onSuccess(final String p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.mediation;

import android.location.Location;
import java.util.Set;
import java.util.Date;

@Deprecated
public interface MediationAdRequest
{
    public static final int TAG_FOR_CHILD_DIRECTED_TREATMENT_FALSE = 0;
    public static final int TAG_FOR_CHILD_DIRECTED_TREATMENT_TRUE = 1;
    public static final int TAG_FOR_CHILD_DIRECTED_TREATMENT_UNSPECIFIED = -1;
    
    @Deprecated
    Date getBirthday();
    
    @Deprecated
    int getGender();
    
    Set<String> getKeywords();
    
    Location getLocation();
    
    @Deprecated
    boolean isDesignedForFamilies();
    
    boolean isTesting();
    
    int taggedForChildDirectedTreatment();
}

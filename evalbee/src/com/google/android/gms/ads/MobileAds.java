// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads;

import android.view.View;
import com.google.android.gms.internal.ads.zzcae;
import android.os.RemoteException;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.internal.ads.zzbug;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.common.internal.Preconditions;
import android.webkit.WebView;
import com.google.android.gms.ads.mediation.rtb.RtbAdapter;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import android.text.TextUtils;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.internal.client.zzej;
import android.content.Context;

public class MobileAds
{
    public static final String ERROR_DOMAIN = "com.google.android.gms.ads";
    
    private MobileAds() {
    }
    
    public static void disableMediationAdapterInitialization(final Context context) {
        zzej.zzf().zzl(context);
    }
    
    @Deprecated
    public static void enableSameAppKey(final boolean b) {
        zzej.zzf().zzr(b);
    }
    
    public static InitializationStatus getInitializationStatus() {
        return zzej.zzf().zze();
    }
    
    @KeepForSdk
    private static String getInternalVersion() {
        return zzej.zzf().zzh();
    }
    
    public static RequestConfiguration getRequestConfiguration() {
        return zzej.zzf().zzc();
    }
    
    public static VersionInfo getVersion() {
        zzej.zzf();
        final String[] split = TextUtils.split("22.6.0", "\\.");
        VersionInfo versionInfo;
        if (split.length != 3) {
            versionInfo = new VersionInfo(0, 0, 0);
        }
        else {
            try {
                versionInfo = new VersionInfo(Integer.parseInt(split[0]), Integer.parseInt(split[1]), Integer.parseInt(split[2]));
            }
            catch (final NumberFormatException ex) {
                versionInfo = new VersionInfo(0, 0, 0);
            }
        }
        return versionInfo;
    }
    
    public static void initialize(final Context context) {
        zzej.zzf().zzm(context, null, null);
    }
    
    public static void initialize(final Context context, final OnInitializationCompleteListener onInitializationCompleteListener) {
        zzej.zzf().zzm(context, null, onInitializationCompleteListener);
    }
    
    public static void openAdInspector(final Context context, final OnAdInspectorClosedListener onAdInspectorClosedListener) {
        zzej.zzf().zzp(context, onAdInspectorClosedListener);
    }
    
    public static void openDebugMenu(final Context context, final String s) {
        zzej.zzf().zzq(context, s);
    }
    
    public static void putPublisherFirstPartyIdEnabled(final boolean b) {
        zzej.zzf().zzr(b);
    }
    
    @KeepForSdk
    public static void registerRtbAdapter(final Class<? extends RtbAdapter> clazz) {
        zzej.zzf().zzs(clazz);
    }
    
    public static void registerWebView(final WebView webView) {
        zzej.zzf();
        Preconditions.checkMainThread("#008 Must be called on the main UI thread.");
        if (webView == null) {
            zzcbn.zzg("The webview to be registered cannot be null.");
            return;
        }
        final zzcae zza = zzbug.zza(((View)webView).getContext());
        if (zza == null) {
            zzcbn.zzj("Internal error, query info generator is null.");
            return;
        }
        try {
            zza.zzi(ObjectWrapper.wrap(webView));
        }
        catch (final RemoteException ex) {
            zzcbn.zzh("", (Throwable)ex);
        }
    }
    
    public static void setAppMuted(final boolean b) {
        zzej.zzf().zzt(b);
    }
    
    public static void setAppVolume(final float n) {
        zzej.zzf().zzu(n);
    }
    
    @KeepForSdk
    private static void setPlugin(final String s) {
        zzej.zzf().zzv(s);
    }
    
    public static void setRequestConfiguration(final RequestConfiguration requestConfiguration) {
        zzej.zzf().zzw(requestConfiguration);
    }
}

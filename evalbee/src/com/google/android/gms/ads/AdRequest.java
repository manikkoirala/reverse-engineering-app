// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads;

import android.os.BaseBundle;
import java.util.Date;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.common.internal.Preconditions;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.internal.client.zzdw;
import android.content.Context;
import com.google.android.gms.ads.mediation.MediationExtrasReceiver;
import java.util.List;
import java.util.Set;
import android.os.Bundle;
import com.google.android.gms.ads.mediation.customevent.CustomEvent;
import com.google.android.gms.ads.search.SearchAdRequest;
import com.google.android.gms.ads.internal.client.zzdx;

public class AdRequest
{
    public static final String DEVICE_ID_EMULATOR = "B3EEABB8EE11C2BE770B684D95219ECB";
    public static final int ERROR_CODE_APP_ID_MISSING = 8;
    public static final int ERROR_CODE_INTERNAL_ERROR = 0;
    public static final int ERROR_CODE_INVALID_AD_STRING = 11;
    public static final int ERROR_CODE_INVALID_REQUEST = 1;
    public static final int ERROR_CODE_MEDIATION_NO_FILL = 9;
    public static final int ERROR_CODE_NETWORK_ERROR = 2;
    public static final int ERROR_CODE_NO_FILL = 3;
    public static final int ERROR_CODE_REQUEST_ID_MISMATCH = 10;
    public static final int GENDER_FEMALE = 2;
    public static final int GENDER_MALE = 1;
    public static final int GENDER_UNKNOWN = 0;
    public static final int MAX_CONTENT_URL_LENGTH = 512;
    protected final zzdx zza;
    
    public AdRequest(final Builder builder) {
        this.zza = new zzdx(builder.zza, null);
    }
    
    public String getAdString() {
        return this.zza.zzj();
    }
    
    public String getContentUrl() {
        return this.zza.zzk();
    }
    
    @Deprecated
    public <T extends CustomEvent> Bundle getCustomEventExtrasBundle(final Class<T> clazz) {
        return this.zza.zzd(clazz);
    }
    
    public Bundle getCustomTargeting() {
        return this.zza.zze();
    }
    
    public Set<String> getKeywords() {
        return this.zza.zzq();
    }
    
    public List<String> getNeighboringContentUrls() {
        return this.zza.zzo();
    }
    
    public <T extends MediationExtrasReceiver> Bundle getNetworkExtrasBundle(final Class<T> clazz) {
        return this.zza.zzf(clazz);
    }
    
    public String getRequestAgent() {
        return this.zza.zzm();
    }
    
    public boolean isTestDevice(final Context context) {
        return this.zza.zzs(context);
    }
    
    public final zzdx zza() {
        return this.zza;
    }
    
    public static class Builder
    {
        protected final zzdw zza;
        
        public Builder() {
            (this.zza = new zzdw()).zzv("B3EEABB8EE11C2BE770B684D95219ECB");
        }
        
        @Deprecated
        public Builder addCustomEventExtrasBundle(final Class<? extends CustomEvent> clazz, final Bundle bundle) {
            this.zza.zzq(clazz, bundle);
            return this;
        }
        
        public Builder addKeyword(final String s) {
            this.zza.zzs(s);
            return this;
        }
        
        public Builder addNetworkExtrasBundle(final Class<? extends MediationExtrasReceiver> clazz, final Bundle bundle) {
            this.zza.zzt(clazz, bundle);
            if (clazz.equals(AdMobAdapter.class) && ((BaseBundle)bundle).getBoolean("_emulatorLiveAds")) {
                this.zza.zzw("B3EEABB8EE11C2BE770B684D95219ECB");
            }
            return this;
        }
        
        public AdRequest build() {
            return new AdRequest(this);
        }
        
        public Builder setAdString(final String s) {
            this.zza.zzx(s);
            return this;
        }
        
        public Builder setContentUrl(final String s) {
            Preconditions.checkNotNull(s, "Content URL must be non-null.");
            Preconditions.checkNotEmpty(s, "Content URL must be non-empty.");
            Preconditions.checkArgument(s.length() <= 512, "Content URL must not exceed %d in length.  Provided length was %d.", 512, s.length());
            this.zza.zzz(s);
            return this;
        }
        
        public Builder setHttpTimeoutMillis(final int n) {
            this.zza.zzB(n);
            return this;
        }
        
        public Builder setNeighboringContentUrls(final List<String> list) {
            if (list == null) {
                zzcbn.zzj("neighboring content URLs list should not be null");
                return this;
            }
            this.zza.zzD(list);
            return this;
        }
        
        public Builder setRequestAgent(final String s) {
            this.zza.zzF(s);
            return this;
        }
        
        @Deprecated
        public final Builder zza(final String s) {
            this.zza.zzv(s);
            return this;
        }
        
        @Deprecated
        public final Builder zzb(final Date date) {
            this.zza.zzy(date);
            return this;
        }
        
        @Deprecated
        public final Builder zzc(final int n) {
            this.zza.zzA(n);
            return this;
        }
        
        @Deprecated
        public final Builder zzd(final boolean b) {
            this.zza.zzC(b);
            return this;
        }
        
        @Deprecated
        public final Builder zze(final boolean b) {
            this.zza.zzG(b);
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads;

import com.google.android.gms.ads.admanager.AppEventListener;
import com.google.android.gms.ads.internal.client.zza;
import com.google.android.gms.internal.ads.zzcbn;
import android.view.View;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.ads.zzcbc;
import com.google.android.gms.ads.internal.client.zzba;
import com.google.android.gms.internal.ads.zzbet;
import com.google.android.gms.internal.ads.zzbdc;
import android.util.AttributeSet;
import android.content.Context;
import org.checkerframework.checker.initialization.qual.NotOnlyInitialized;
import com.google.android.gms.ads.internal.client.zzea;
import android.view.ViewGroup;

public abstract class BaseAdView extends ViewGroup
{
    @NotOnlyInitialized
    protected final zzea zza;
    
    public BaseAdView(final Context context, final int n) {
        super(context);
        this.zza = new zzea(this, n);
    }
    
    public BaseAdView(final Context context, final AttributeSet set, final int n) {
        super(context, set);
        this.zza = new zzea(this, set, false, n);
    }
    
    public BaseAdView(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n);
        this.zza = new zzea(this, set, false, n2);
    }
    
    public BaseAdView(final Context context, final AttributeSet set, final int n, final int n2, final boolean b) {
        super(context, set, n);
        this.zza = new zzea(this, set, b, n2);
    }
    
    public BaseAdView(final Context context, final AttributeSet set, final boolean b) {
        super(context, set);
        this.zza = new zzea(this, set, b);
    }
    
    public void destroy() {
        zzbdc.zza(((View)this).getContext());
        if ((boolean)zzbet.zze.zze() && (boolean)zzba.zzc().zza(zzbdc.zzkq)) {
            zzcbc.zzb.execute(new zzd(this));
            return;
        }
        this.zza.zzk();
    }
    
    public AdListener getAdListener() {
        return this.zza.zza();
    }
    
    public AdSize getAdSize() {
        return this.zza.zzb();
    }
    
    public String getAdUnitId() {
        return this.zza.zzj();
    }
    
    public OnPaidEventListener getOnPaidEventListener() {
        return this.zza.zzc();
    }
    
    public ResponseInfo getResponseInfo() {
        return this.zza.zzd();
    }
    
    public boolean isLoading() {
        return this.zza.zzA();
    }
    
    public void loadAd(final AdRequest adRequest) {
        Preconditions.checkMainThread("#008 Must be called on the main UI thread.");
        zzbdc.zza(((View)this).getContext());
        if ((boolean)zzbet.zzf.zze() && (boolean)zzba.zzc().zza(zzbdc.zzkt)) {
            zzcbc.zzb.execute(new zzf(this, adRequest));
            return;
        }
        this.zza.zzm(adRequest.zza);
    }
    
    public void onLayout(final boolean b, int n, int n2, final int n3, final int n4) {
        final View child = this.getChildAt(0);
        if (child != null && child.getVisibility() != 8) {
            final int measuredWidth = child.getMeasuredWidth();
            final int measuredHeight = child.getMeasuredHeight();
            n = (n3 - n - measuredWidth) / 2;
            n2 = (n4 - n2 - measuredHeight) / 2;
            child.layout(n, n2, measuredWidth + n, measuredHeight + n2);
        }
    }
    
    public void onMeasure(final int n, final int n2) {
        int a = 0;
        final View child = this.getChildAt(0);
        int a2;
        if (child != null && child.getVisibility() != 8) {
            this.measureChild(child, n, n2);
            a = child.getMeasuredWidth();
            a2 = child.getMeasuredHeight();
        }
        else {
            AdSize adSize;
            try {
                adSize = this.getAdSize();
            }
            catch (final NullPointerException ex) {
                zzcbn.zzh("Unable to retrieve ad size.", (Throwable)ex);
                adSize = null;
            }
            if (adSize != null) {
                final Context context = ((View)this).getContext();
                a = adSize.getWidthInPixels(context);
                a2 = adSize.getHeightInPixels(context);
            }
            else {
                a2 = 0;
            }
        }
        ((View)this).setMeasuredDimension(View.resolveSize(Math.max(a, ((View)this).getSuggestedMinimumWidth()), n), View.resolveSize(Math.max(a2, ((View)this).getSuggestedMinimumHeight()), n2));
    }
    
    public void pause() {
        zzbdc.zza(((View)this).getContext());
        if ((boolean)zzbet.zzg.zze() && (boolean)zzba.zzc().zza(zzbdc.zzkr)) {
            zzcbc.zzb.execute(new zze(this));
            return;
        }
        this.zza.zzn();
    }
    
    public void resume() {
        zzbdc.zza(((View)this).getContext());
        if ((boolean)zzbet.zzh.zze() && (boolean)zzba.zzc().zza(zzbdc.zzkp)) {
            zzcbc.zzb.execute(new zzc(this));
            return;
        }
        this.zza.zzp();
    }
    
    public void setAdListener(final AdListener adListener) {
        this.zza.zzr(adListener);
        if (adListener == null) {
            this.zza.zzq(null);
            return;
        }
        if (adListener instanceof zza) {
            this.zza.zzq((zza)adListener);
        }
        if (adListener instanceof AppEventListener) {
            this.zza.zzv((AppEventListener)adListener);
        }
    }
    
    public void setAdSize(final AdSize adSize) {
        this.zza.zzs(adSize);
    }
    
    public void setAdUnitId(final String s) {
        this.zza.zzu(s);
    }
    
    public void setOnPaidEventListener(final OnPaidEventListener onPaidEventListener) {
        this.zza.zzx(onPaidEventListener);
    }
}

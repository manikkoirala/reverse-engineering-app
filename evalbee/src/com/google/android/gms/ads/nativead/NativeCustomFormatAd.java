// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.nativead;

import android.view.View;
import com.google.android.gms.ads.MediaContent;
import java.util.List;

public interface NativeCustomFormatAd
{
    public static final String ASSET_NAME_VIDEO = "_videoMediaView";
    
    void destroy();
    
    List<String> getAvailableAssetNames();
    
    String getCustomFormatId();
    
    DisplayOpenMeasurement getDisplayOpenMeasurement();
    
    NativeAd.Image getImage(final String p0);
    
    MediaContent getMediaContent();
    
    CharSequence getText(final String p0);
    
    void performClick(final String p0);
    
    void recordImpression();
    
    public interface DisplayOpenMeasurement
    {
        void setView(final View p0);
        
        boolean start();
    }
    
    public interface OnCustomClickListener
    {
        void onCustomClick(final NativeCustomFormatAd p0, final String p1);
    }
    
    public interface OnCustomFormatAdLoadedListener
    {
        void onCustomFormatAdLoaded(final NativeCustomFormatAd p0);
    }
}

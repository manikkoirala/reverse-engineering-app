// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.nativead;

import android.view.ViewGroup;
import com.google.android.gms.internal.ads.zzbhc;
import android.os.RemoteException;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.dynamic.ObjectWrapper;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.ImageView$ScaleType;
import com.google.android.gms.ads.MediaContent;
import android.widget.FrameLayout;

public class MediaView extends FrameLayout
{
    private MediaContent zza;
    private boolean zzb;
    private ImageView$ScaleType zzc;
    private boolean zzd;
    private zzb zze;
    private zzc zzf;
    
    public MediaView(final Context context) {
        super(context);
    }
    
    public MediaView(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    public MediaView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    public MediaView(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
    }
    
    public MediaContent getMediaContent() {
        return this.zza;
    }
    
    public void setImageScaleType(final ImageView$ScaleType zzc) {
        this.zzd = true;
        this.zzc = zzc;
        final zzc zzf = this.zzf;
        if (zzf != null) {
            zzf.zza.zzc(zzc);
        }
    }
    
    public void setMediaContent(final MediaContent zza) {
        this.zzb = true;
        this.zza = zza;
        final zzb zze = this.zze;
        if (zze != null) {
            zze.zza.zzb(zza);
        }
        if (zza == null) {
            return;
        }
        try {
            final zzbhc zza2 = zza.zza();
            if (zza2 != null) {
                Label_0092: {
                    boolean b;
                    if (zza.hasVideoContent()) {
                        b = zza2.zzs(ObjectWrapper.wrap(this));
                    }
                    else {
                        if (!zza.zzb()) {
                            break Label_0092;
                        }
                        b = zza2.zzr(ObjectWrapper.wrap(this));
                    }
                    if (b) {
                        return;
                    }
                }
                ((ViewGroup)this).removeAllViews();
            }
        }
        catch (final RemoteException ex) {
            ((ViewGroup)this).removeAllViews();
            zzcbn.zzh("", (Throwable)ex);
        }
    }
    
    public final void zza(final zzb zze) {
        synchronized (this) {
            this.zze = zze;
            if (this.zzb) {
                zze.zza.zzb(this.zza);
            }
        }
    }
    
    public final void zzb(final zzc zzf) {
        synchronized (this) {
            this.zzf = zzf;
            if (this.zzd) {
                zzf.zza.zzc(this.zzc);
            }
        }
    }
}

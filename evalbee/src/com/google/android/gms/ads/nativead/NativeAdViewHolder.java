// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.nativead;

import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.RemoteException;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.util.HashMap;
import com.google.android.gms.ads.internal.client.zzay;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.common.internal.Preconditions;
import java.util.Map;
import android.view.View;
import java.lang.ref.WeakReference;
import org.checkerframework.checker.initialization.qual.NotOnlyInitialized;
import com.google.android.gms.internal.ads.zzbgs;
import java.util.WeakHashMap;

public final class NativeAdViewHolder
{
    public static final WeakHashMap zza;
    @NotOnlyInitialized
    private zzbgs zzb;
    private WeakReference zzc;
    
    static {
        zza = new WeakHashMap();
    }
    
    public NativeAdViewHolder(final View referent, final Map<String, View> map, final Map<String, View> map2) {
        Preconditions.checkNotNull(referent, "ContainerView must not be null");
        if (referent instanceof NativeAdView) {
            zzcbn.zzg("The provided containerView is of type of NativeAdView, which cannot be usedwith NativeAdViewHolder.");
            return;
        }
        final WeakHashMap zza = NativeAdViewHolder.zza;
        if (zza.get(referent) != null) {
            zzcbn.zzg("The provided containerView is already in use with another NativeAdViewHolder.");
            return;
        }
        zza.put(referent, this);
        this.zzc = new WeakReference((T)referent);
        this.zzb = zzay.zza().zzi(referent, zza(map), zza(map2));
    }
    
    private static final HashMap zza(final Map m) {
        if (m == null) {
            return new HashMap();
        }
        return new HashMap(m);
    }
    
    public final void setClickConfirmingView(final View view) {
        try {
            this.zzb.zzb(ObjectWrapper.wrap(view));
        }
        catch (final RemoteException ex) {
            zzcbn.zzh("Unable to call setClickConfirmingView on delegate", (Throwable)ex);
        }
    }
    
    public void setNativeAd(final NativeAd nativeAd) {
        final Object zza = nativeAd.zza();
        final WeakReference zzc = this.zzc;
        View view;
        if (zzc != null) {
            view = (View)zzc.get();
        }
        else {
            view = null;
        }
        if (view == null) {
            zzcbn.zzj("NativeAdViewHolder.setNativeAd containerView doesn't exist, returning");
            return;
        }
        final WeakHashMap zza2 = NativeAdViewHolder.zza;
        if (!zza2.containsKey(view)) {
            zza2.put(view, this);
        }
        final zzbgs zzb = this.zzb;
        if (zzb != null) {
            try {
                zzb.zzc((IObjectWrapper)zza);
            }
            catch (final RemoteException ex) {
                zzcbn.zzh("Unable to call setNativeAd on delegate", (Throwable)ex);
            }
        }
    }
    
    public void unregisterNativeAd() {
        final zzbgs zzb = this.zzb;
        if (zzb != null) {
            try {
                zzb.zzd();
            }
            catch (final RemoteException ex) {
                zzcbn.zzh("Unable to call unregisterNativeAd on delegate", (Throwable)ex);
            }
        }
        final WeakReference zzc = this.zzc;
        View key;
        if (zzc != null) {
            key = (View)zzc.get();
        }
        else {
            key = null;
        }
        if (key != null) {
            NativeAdViewHolder.zza.remove(key);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.nativead;

import android.view.ViewGroup;
import android.widget.ImageView$ScaleType;
import com.google.android.gms.internal.ads.zzbgf;
import com.google.android.gms.ads.internal.client.zzep;
import com.google.android.gms.ads.MediaContent;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.ads.internal.client.zzba;
import android.view.MotionEvent;
import android.os.RemoteException;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.dynamic.ObjectWrapper;
import org.checkerframework.checker.nullness.qual.RequiresNonNull;
import com.google.android.gms.ads.internal.client.zzay;
import android.view.View;
import android.view.ViewGroup$LayoutParams;
import android.widget.FrameLayout$LayoutParams;
import android.util.AttributeSet;
import android.content.Context;
import com.google.android.gms.internal.ads.zzbgm;
import org.checkerframework.checker.initialization.qual.NotOnlyInitialized;
import android.widget.FrameLayout;

public final class NativeAdView extends FrameLayout
{
    @NotOnlyInitialized
    private final FrameLayout zza;
    @NotOnlyInitialized
    private final zzbgm zzb;
    
    public NativeAdView(final Context context) {
        super(context);
        this.zza = this.zzd(context);
        this.zzb = this.zze();
    }
    
    public NativeAdView(final Context context, final AttributeSet set) {
        super(context, set);
        this.zza = this.zzd(context);
        this.zzb = this.zze();
    }
    
    public NativeAdView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.zza = this.zzd(context);
        this.zzb = this.zze();
    }
    
    public NativeAdView(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.zza = this.zzd(context);
        this.zzb = this.zze();
    }
    
    private final FrameLayout zzd(final Context context) {
        final FrameLayout frameLayout = new FrameLayout(context);
        ((View)frameLayout).setLayoutParams((ViewGroup$LayoutParams)new FrameLayout$LayoutParams(-1, -1));
        ((ViewGroup)this).addView((View)frameLayout);
        return frameLayout;
    }
    
    @RequiresNonNull({ "overlayFrame" })
    private final zzbgm zze() {
        if (((View)this).isInEditMode()) {
            return null;
        }
        final FrameLayout zza = this.zza;
        return zzay.zza().zzh(((View)zza).getContext(), this, zza);
    }
    
    private final void zzf(final String s, final View view) {
        final zzbgm zzb = this.zzb;
        if (zzb == null) {
            return;
        }
        try {
            zzb.zzbA(s, ObjectWrapper.wrap(view));
        }
        catch (final RemoteException ex) {
            zzcbn.zzh("Unable to call setAssetView on delegate", (Throwable)ex);
        }
    }
    
    public final void addView(final View view, final int n, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        super.addView(view, n, viewGroup$LayoutParams);
        super.bringChildToFront((View)this.zza);
    }
    
    public final void bringChildToFront(final View view) {
        super.bringChildToFront(view);
        final FrameLayout zza = this.zza;
        if (zza != view) {
            super.bringChildToFront((View)zza);
        }
    }
    
    public void destroy() {
        final zzbgm zzb = this.zzb;
        if (zzb == null) {
            return;
        }
        try {
            zzb.zzc();
        }
        catch (final RemoteException ex) {
            zzcbn.zzh("Unable to destroy native ad view", (Throwable)ex);
        }
    }
    
    public final boolean dispatchTouchEvent(final MotionEvent motionEvent) {
        if (this.zzb != null && (boolean)zzba.zzc().zza(zzbdc.zzkG)) {
            try {
                this.zzb.zzd(ObjectWrapper.wrap(motionEvent));
            }
            catch (final RemoteException ex) {
                zzcbn.zzh("Unable to call handleTouchEvent on delegate", (Throwable)ex);
            }
        }
        return super.dispatchTouchEvent(motionEvent);
    }
    
    public AdChoicesView getAdChoicesView() {
        final View zza = this.zza("3011");
        if (zza instanceof AdChoicesView) {
            return (AdChoicesView)zza;
        }
        return null;
    }
    
    public final View getAdvertiserView() {
        return this.zza("3005");
    }
    
    public final View getBodyView() {
        return this.zza("3004");
    }
    
    public final View getCallToActionView() {
        return this.zza("3002");
    }
    
    public final View getHeadlineView() {
        return this.zza("3001");
    }
    
    public final View getIconView() {
        return this.zza("3003");
    }
    
    public final View getImageView() {
        return this.zza("3008");
    }
    
    public final MediaView getMediaView() {
        final View zza = this.zza("3010");
        if (zza instanceof MediaView) {
            return (MediaView)zza;
        }
        if (zza != null) {
            zzcbn.zze("View is not an instance of MediaView");
        }
        return null;
    }
    
    public final View getPriceView() {
        return this.zza("3007");
    }
    
    public final View getStarRatingView() {
        return this.zza("3009");
    }
    
    public final View getStoreView() {
        return this.zza("3006");
    }
    
    public final void onVisibilityChanged(final View view, final int n) {
        super.onVisibilityChanged(view, n);
        final zzbgm zzb = this.zzb;
        if (zzb == null) {
            return;
        }
        try {
            zzb.zze(ObjectWrapper.wrap(view), n);
        }
        catch (final RemoteException ex) {
            zzcbn.zzh("Unable to call onVisibilityChanged on delegate", (Throwable)ex);
        }
    }
    
    public final void removeAllViews() {
        super.removeAllViews();
        super.addView((View)this.zza);
    }
    
    public final void removeView(final View view) {
        if (this.zza == view) {
            return;
        }
        super.removeView(view);
    }
    
    public void setAdChoicesView(final AdChoicesView adChoicesView) {
        this.zzf("3011", (View)adChoicesView);
    }
    
    public final void setAdvertiserView(final View view) {
        this.zzf("3005", view);
    }
    
    public final void setBodyView(final View view) {
        this.zzf("3004", view);
    }
    
    public final void setCallToActionView(final View view) {
        this.zzf("3002", view);
    }
    
    public final void setClickConfirmingView(final View view) {
        final zzbgm zzb = this.zzb;
        if (zzb == null) {
            return;
        }
        try {
            zzb.zzbB(ObjectWrapper.wrap(view));
        }
        catch (final RemoteException ex) {
            zzcbn.zzh("Unable to call setClickConfirmingView on delegate", (Throwable)ex);
        }
    }
    
    public final void setHeadlineView(final View view) {
        this.zzf("3001", view);
    }
    
    public final void setIconView(final View view) {
        this.zzf("3003", view);
    }
    
    public final void setImageView(final View view) {
        this.zzf("3008", view);
    }
    
    public final void setMediaView(final MediaView mediaView) {
        this.zzf("3010", (View)mediaView);
        if (mediaView == null) {
            return;
        }
        mediaView.zza(new zzb(this));
        mediaView.zzb(new zzc(this));
    }
    
    public void setNativeAd(final NativeAd nativeAd) {
        final zzbgm zzb = this.zzb;
        if (zzb == null) {
            return;
        }
        try {
            zzb.zzbE((IObjectWrapper)nativeAd.zza());
        }
        catch (final RemoteException ex) {
            zzcbn.zzh("Unable to call setNativeAd on delegate", (Throwable)ex);
        }
    }
    
    public final void setPriceView(final View view) {
        this.zzf("3007", view);
    }
    
    public final void setStarRatingView(final View view) {
        this.zzf("3009", view);
    }
    
    public final void setStoreView(final View view) {
        this.zzf("3006", view);
    }
    
    public final View zza(final String s) {
        final zzbgm zzb = this.zzb;
        if (zzb != null) {
            try {
                final IObjectWrapper zzb2 = zzb.zzb(s);
                if (zzb2 != null) {
                    return (View)ObjectWrapper.unwrap(zzb2);
                }
            }
            catch (final RemoteException ex) {
                zzcbn.zzh("Unable to call getAssetView on delegate", (Throwable)ex);
            }
        }
        return null;
    }
}

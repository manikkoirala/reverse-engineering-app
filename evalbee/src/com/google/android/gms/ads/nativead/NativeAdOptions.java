// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.nativead;

import com.google.android.gms.ads.VideoOptions;

public final class NativeAdOptions
{
    public static final int ADCHOICES_BOTTOM_LEFT = 3;
    public static final int ADCHOICES_BOTTOM_RIGHT = 2;
    public static final int ADCHOICES_TOP_LEFT = 0;
    public static final int ADCHOICES_TOP_RIGHT = 1;
    public static final int NATIVE_MEDIA_ASPECT_RATIO_ANY = 1;
    public static final int NATIVE_MEDIA_ASPECT_RATIO_LANDSCAPE = 2;
    public static final int NATIVE_MEDIA_ASPECT_RATIO_PORTRAIT = 3;
    public static final int NATIVE_MEDIA_ASPECT_RATIO_SQUARE = 4;
    public static final int NATIVE_MEDIA_ASPECT_RATIO_UNKNOWN = 0;
    public static final int SWIPE_GESTURE_DIRECTION_DOWN = 8;
    public static final int SWIPE_GESTURE_DIRECTION_LEFT = 2;
    public static final int SWIPE_GESTURE_DIRECTION_RIGHT = 1;
    public static final int SWIPE_GESTURE_DIRECTION_UP = 4;
    private final boolean zza = Builder.zzg(builder);
    private final int zzb = Builder.zzc(builder);
    private final boolean zzc = Builder.zzf(builder);
    private final int zzd = Builder.zza(builder);
    private final VideoOptions zze = Builder.zzd(builder);
    private final boolean zzf = Builder.zzh(builder);
    private final boolean zzg = Builder.zze(builder);
    private final int zzh = Builder.zzb(builder);
    private final int zzi = Builder.zzj(builder);
    
    public int getAdChoicesPlacement() {
        return this.zzd;
    }
    
    public int getMediaAspectRatio() {
        return this.zzb;
    }
    
    public VideoOptions getVideoOptions() {
        return this.zze;
    }
    
    public boolean shouldRequestMultipleImages() {
        return this.zzc;
    }
    
    public boolean shouldReturnUrlsForImageAssets() {
        return this.zza;
    }
    
    public final int zza() {
        return this.zzh;
    }
    
    public final boolean zzb() {
        return this.zzg;
    }
    
    public final boolean zzc() {
        return this.zzf;
    }
    
    public final int zzd() {
        return this.zzi;
    }
    
    public @interface AdChoicesPlacement {
    }
    
    public static final class Builder
    {
        private boolean zza;
        private int zzb;
        private boolean zzc;
        private VideoOptions zzd;
        private int zze;
        private boolean zzf;
        private boolean zzg;
        private int zzh;
        private int zzi;
        
        public Builder() {
            this.zza = false;
            this.zzb = 0;
            this.zzc = false;
            this.zze = 1;
            this.zzf = false;
            this.zzg = false;
            this.zzh = 0;
            this.zzi = 1;
        }
        
        public NativeAdOptions build() {
            return new NativeAdOptions(this, null);
        }
        
        public Builder enableCustomClickGestureDirection(@SwipeGestureDirection final int zzh, final boolean zzg) {
            this.zzg = zzg;
            this.zzh = zzh;
            return this;
        }
        
        public Builder setAdChoicesPlacement(@AdChoicesPlacement final int zze) {
            this.zze = zze;
            return this;
        }
        
        public Builder setMediaAspectRatio(@NativeMediaAspectRatio final int zzb) {
            this.zzb = zzb;
            return this;
        }
        
        public Builder setRequestCustomMuteThisAd(final boolean zzf) {
            this.zzf = zzf;
            return this;
        }
        
        public Builder setRequestMultipleImages(final boolean zzc) {
            this.zzc = zzc;
            return this;
        }
        
        public Builder setReturnUrlsForImageAssets(final boolean zza) {
            this.zza = zza;
            return this;
        }
        
        public Builder setVideoOptions(final VideoOptions zzd) {
            this.zzd = zzd;
            return this;
        }
        
        public final Builder zzi(final int zzi) {
            this.zzi = zzi;
            return this;
        }
    }
    
    public @interface NativeMediaAspectRatio {
    }
    
    public @interface SwipeGestureDirection {
    }
}

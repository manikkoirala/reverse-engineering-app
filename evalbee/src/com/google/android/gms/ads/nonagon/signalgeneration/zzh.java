// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.nonagon.signalgeneration;

import com.google.android.gms.internal.ads.zzfkh;
import com.google.android.gms.internal.ads.zzdtk;

public abstract class zzh
{
    public abstract zzdtk zza();
    
    public abstract zzfkh zzb();
    
    public abstract ik0 zzc();
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.nonagon.signalgeneration;

import java.util.HashSet;
import java.util.Set;
import java.util.Locale;
import com.google.android.gms.internal.ads.zzayz;

public final class zzae
{
    private final String zza = zzac.zzc(zzac);
    
    public final zzayz zza() {
        final String zza = this.zza;
        int n = 0;
        Label_0113: {
            switch (zza.hashCode()) {
                case 1951953708: {
                    if (zza.equals("BANNER")) {
                        n = 0;
                        break Label_0113;
                    }
                    break;
                }
                case 543046670: {
                    if (zza.equals("REWARDED")) {
                        n = 3;
                        break Label_0113;
                    }
                    break;
                }
                case -1372958932: {
                    if (zza.equals("INTERSTITIAL")) {
                        n = 1;
                        break Label_0113;
                    }
                    break;
                }
                case -1999289321: {
                    if (zza.equals("NATIVE")) {
                        n = 2;
                        break Label_0113;
                    }
                    break;
                }
            }
            n = -1;
        }
        if (n == 0) {
            return zzayz.zzb;
        }
        if (n == 1) {
            return zzayz.zzd;
        }
        if (n == 2) {
            return zzayz.zzg;
        }
        if (n != 3) {
            return zzayz.zza;
        }
        return zzayz.zzh;
    }
    
    public final String zzb() {
        return this.zza.toLowerCase(Locale.ROOT);
    }
    
    public final Set zzc() {
        final HashSet set = new HashSet();
        set.add(this.zza.toLowerCase(Locale.ROOT));
        return set;
    }
}

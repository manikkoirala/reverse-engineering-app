// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.nonagon.signalgeneration;

import com.google.android.gms.internal.ads.zzcxr;

public interface zzg
{
    zzg zza(final zzcxr p0);
    
    zzg zzb(final zzae p0);
    
    zzh zzc();
}

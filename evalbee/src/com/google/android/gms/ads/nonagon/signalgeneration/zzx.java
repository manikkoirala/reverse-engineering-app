// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.nonagon.signalgeneration;

import com.google.android.gms.internal.ads.zzfla;
import java.util.Iterator;
import com.google.android.gms.internal.ads.zzfkh;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.ads.internal.client.zzba;
import android.net.Uri;
import java.util.List;
import java.util.ArrayList;
import android.os.RemoteException;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.internal.ads.zzbue;
import com.google.android.gms.internal.ads.zzgax;

final class zzx implements zzgax
{
    final zzbue zza;
    final boolean zzb;
    final zzaa zzc;
    
    public zzx(final zzaa zzc, final zzbue zza, final boolean zzb) {
        this.zzc = zzc;
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public final void zza(final Throwable t) {
        try {
            final zzbue zza = this.zza;
            final String message = t.getMessage();
            final StringBuilder sb = new StringBuilder();
            sb.append("Internal error: ");
            sb.append(message);
            zza.zze(sb.toString());
        }
        catch (final RemoteException ex) {
            zzcbn.zzh("", (Throwable)ex);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.nonagon.signalgeneration;

import java.util.concurrent.Executor;
import android.os.BaseBundle;
import java.util.concurrent.Future;
import com.google.android.gms.internal.ads.zzfkh;
import com.google.android.gms.internal.ads.zzasj;
import android.net.Uri;
import android.webkit.CookieManager;
import org.json.JSONException;
import android.view.MotionEvent;
import org.json.JSONObject;
import android.text.TextUtils;
import android.app.Activity;
import com.google.android.gms.ads.query.QueryInfo;
import com.google.android.gms.ads.mediation.MediationExtrasReceiver;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdFormat;
import com.google.android.gms.ads.query.QueryInfoGenerationCallback;
import android.os.Bundle;
import java.util.UUID;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.Callable;
import com.google.android.gms.common.annotation.KeepForSdk;
import android.webkit.JavascriptInterface;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.internal.ads.zzdtk;
import android.util.Pair;
import android.view.View;
import com.google.android.gms.ads.internal.zzt;
import com.google.android.gms.ads.internal.client.zzba;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.internal.ads.zzcca;
import com.google.android.gms.internal.ads.zzfla;
import com.google.android.gms.internal.ads.zzgbl;
import com.google.android.gms.internal.ads.zzdtu;
import com.google.android.gms.internal.ads.zzasi;
import android.webkit.WebView;
import android.content.Context;

final class TaggingLibraryJsInterface
{
    private final Context zza;
    private final WebView zzb;
    private final zzasi zzc;
    private final int zzd;
    private final zzdtu zze;
    private final boolean zzf;
    private final zzgbl zzg;
    private final zzfla zzh;
    
    public TaggingLibraryJsInterface(final WebView zzb, final zzasi zzc, final zzdtu zze, final zzfla zzh) {
        this.zzg = zzcca.zze;
        this.zzb = zzb;
        final Context context = ((View)zzb).getContext();
        this.zza = context;
        this.zzc = zzc;
        this.zze = zze;
        zzbdc.zza(context);
        this.zzd = (int)zzba.zzc().zza(zzbdc.zzjm);
        this.zzf = (boolean)zzba.zzc().zza(zzbdc.zzjn);
        this.zzh = zzh;
    }
    
    @JavascriptInterface
    @KeepForSdk
    public String getClickSignals(final String s) {
        try {
            final long currentTimeMillis = zzt.zzB().currentTimeMillis();
            final String zze = this.zzc.zzc().zze(this.zza, s, (View)this.zzb);
            if (this.zzf) {
                com.google.android.gms.ads.nonagon.signalgeneration.zzf.zzc(this.zze, null, "csg", new Pair((Object)"clat", (Object)String.valueOf(zzt.zzB().currentTimeMillis() - currentTimeMillis)));
            }
            return zze;
        }
        catch (final RuntimeException ex) {
            zzcbn.zzh("Exception getting click signals. ", (Throwable)ex);
            zzt.zzo().zzw((Throwable)ex, "TaggingLibraryJsInterface.getClickSignals");
            return "";
        }
    }
    
    @JavascriptInterface
    @KeepForSdk
    public String getClickSignalsWithTimeout(String zzb, int min) {
        if (min <= 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid timeout for getting click signals. Timeout=");
            sb.append(min);
            zzcbn.zzg(sb.toString());
            return "";
        }
        min = Math.min(min, this.zzd);
        zzb = (ExecutionException)zzcca.zza.zzb((Callable)new zzap(this, (String)zzb));
        final long n = min;
        try {
            zzb = (ExecutionException)((Future<Object>)zzb).get(n, TimeUnit.MILLISECONDS);
            return (String)zzb;
        }
        catch (final ExecutionException zzb) {}
        catch (final TimeoutException zzb) {}
        catch (final InterruptedException ex) {}
        zzcbn.zzh("Exception getting click signals with timeout. ", (Throwable)zzb);
        zzt.zzo().zzw((Throwable)zzb, "TaggingLibraryJsInterface.getClickSignalsWithTimeout");
        if (zzb instanceof TimeoutException) {
            return "17";
        }
        return "";
    }
    
    @JavascriptInterface
    @KeepForSdk
    public String getQueryInfo() {
        zzt.zzp();
        final String string = UUID.randomUUID().toString();
        final Bundle bundle = new Bundle();
        ((BaseBundle)bundle).putString("query_info_type", "requester_type_6");
        final zzar zzar = new zzar(this, string);
        if (zzba.zzc().zza(zzbdc.zzjp)) {
            ((Executor)this.zzg).execute(new zzaq(this, bundle, zzar));
        }
        else {
            final Context zza = this.zza;
            final AdFormat banner = AdFormat.BANNER;
            final AdRequest.Builder builder = new AdRequest.Builder();
            builder.addNetworkExtrasBundle(AdMobAdapter.class, bundle);
            QueryInfo.generate(zza, banner, builder.build(), zzar);
        }
        return string;
    }
    
    @JavascriptInterface
    @KeepForSdk
    public String getViewSignals() {
        try {
            final long currentTimeMillis = zzt.zzB().currentTimeMillis();
            final String zzh = this.zzc.zzc().zzh(this.zza, (View)this.zzb, (Activity)null);
            if (this.zzf) {
                com.google.android.gms.ads.nonagon.signalgeneration.zzf.zzc(this.zze, null, "vsg", new Pair((Object)"vlat", (Object)String.valueOf(zzt.zzB().currentTimeMillis() - currentTimeMillis)));
            }
            return zzh;
        }
        catch (final RuntimeException ex) {
            zzcbn.zzh("Exception getting view signals. ", (Throwable)ex);
            zzt.zzo().zzw((Throwable)ex, "TaggingLibraryJsInterface.getViewSignals");
            return "";
        }
    }
    
    @JavascriptInterface
    @KeepForSdk
    public String getViewSignalsWithTimeout(int min) {
        if (min <= 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid timeout for getting view signals. Timeout=");
            sb.append(min);
            zzcbn.zzg(sb.toString());
            return "";
        }
        min = Math.min(min, this.zzd);
        Object zzb = zzcca.zza.zzb((Callable)new zzan(this));
        final long n = min;
        try {
            zzb = ((Future<String>)zzb).get(n, TimeUnit.MILLISECONDS);
            return (String)zzb;
        }
        catch (final ExecutionException zzb) {}
        catch (final TimeoutException zzb) {}
        catch (final InterruptedException ex) {}
        zzcbn.zzh("Exception getting view signals with timeout. ", (Throwable)zzb);
        zzt.zzo().zzw((Throwable)zzb, "TaggingLibraryJsInterface.getViewSignalsWithTimeout");
        if (zzb instanceof TimeoutException) {
            return "17";
        }
        return "";
    }
    
    @JavascriptInterface
    @KeepForSdk
    public void recordClick(final String s) {
        if (zzba.zzc().zza(zzbdc.zzjr)) {
            if (!TextUtils.isEmpty((CharSequence)s)) {
                ((Executor)zzcca.zza).execute(new zzao(this, s));
            }
        }
    }
    
    @JavascriptInterface
    @KeepForSdk
    public void reportTouchEvent(String obtain) {
        try {
            final JSONObject jsonObject = new JSONObject((String)obtain);
            final int int1 = jsonObject.getInt("x");
            final int int2 = jsonObject.getInt("y");
            final int int3 = jsonObject.getInt("duration_ms");
            final float n = (float)jsonObject.getDouble("force");
            final int int4 = jsonObject.getInt("type");
            int n2;
            if (int4 != 0) {
                n2 = 1;
                if (int4 != 1) {
                    n2 = 2;
                    if (int4 != 2) {
                        n2 = 3;
                        if (int4 != 3) {
                            n2 = -1;
                        }
                    }
                }
            }
            else {
                n2 = 0;
            }
            obtain = (JSONException)MotionEvent.obtain(0L, (long)int3, n2, (float)int1, (float)int2, n, 1.0f, 0, 1.0f, 1.0f, 0, 0);
            try {
                this.zzc.zzd((MotionEvent)obtain);
                return;
            }
            catch (final JSONException obtain) {}
            catch (final RuntimeException obtain) {}
        }
        catch (final JSONException obtain) {}
        catch (final RuntimeException ex) {}
        zzcbn.zzh("Failed to parse the touch string. ", (Throwable)obtain);
        zzt.zzo().zzw((Throwable)obtain, "TaggingLibraryJsInterface.reportTouchEvent");
    }
}

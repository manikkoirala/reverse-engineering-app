// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.nonagon.signalgeneration;

import com.google.android.gms.internal.ads.zzdtx;
import java.util.concurrent.Executor;
import android.os.BaseBundle;
import com.google.android.gms.ads.internal.client.zzc;
import com.google.android.gms.internal.ads.zzfeq;
import java.util.Map;
import com.google.android.gms.internal.ads.zzcca;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.ads.internal.client.zzba;
import android.util.Pair;
import com.google.android.gms.internal.ads.zzdtk;
import com.google.android.gms.internal.ads.zzdtu;
import android.os.Bundle;
import com.google.android.gms.ads.internal.client.zzl;
import android.text.TextUtils;

public final class zzf
{
    public static String zza(final String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return "unspecified";
        }
        int n = 0;
        Label_0198: {
            switch (s.hashCode()) {
                case 1743582870: {
                    if (s.equals("requester_type_8")) {
                        n = 8;
                        break Label_0198;
                    }
                    break;
                }
                case 1743582869: {
                    if (s.equals("requester_type_7")) {
                        n = 7;
                        break Label_0198;
                    }
                    break;
                }
                case 1743582868: {
                    if (s.equals("requester_type_6")) {
                        n = 6;
                        break Label_0198;
                    }
                    break;
                }
                case 1743582867: {
                    if (s.equals("requester_type_5")) {
                        n = 5;
                        break Label_0198;
                    }
                    break;
                }
                case 1743582866: {
                    if (s.equals("requester_type_4")) {
                        n = 4;
                        break Label_0198;
                    }
                    break;
                }
                case 1743582865: {
                    if (s.equals("requester_type_3")) {
                        n = 3;
                        break Label_0198;
                    }
                    break;
                }
                case 1743582864: {
                    if (s.equals("requester_type_2")) {
                        n = 2;
                        break Label_0198;
                    }
                    break;
                }
                case 1743582863: {
                    if (s.equals("requester_type_1")) {
                        n = 1;
                        break Label_0198;
                    }
                    break;
                }
                case 1743582862: {
                    if (s.equals("requester_type_0")) {
                        n = 0;
                        break Label_0198;
                    }
                    break;
                }
            }
            n = -1;
        }
        switch (n) {
            default: {
                return s;
            }
            case 8: {
                return "8";
            }
            case 7: {
                return "7";
            }
            case 6: {
                return "6";
            }
            case 5: {
                return "5";
            }
            case 4: {
                return "4";
            }
            case 3: {
                return "3";
            }
            case 2: {
                return "2";
            }
            case 1: {
                return "1";
            }
            case 0: {
                return "0";
            }
        }
    }
    
    public static String zzb(final zzl zzl) {
        if (zzl != null) {
            final Bundle zzc = zzl.zzc;
            if (zzc != null) {
                return ((BaseBundle)zzc).getString("query_info_type");
            }
        }
        return "unspecified";
    }
    
    public static void zzc(final zzdtu zzdtu, final zzdtk zzdtk, final String s, final Pair... array) {
        if (!(boolean)zzba.zzc().zza(zzbdc.zzgZ)) {
            return;
        }
        ((Executor)zzcca.zza).execute(new zze(zzdtu, zzdtk, s, array));
    }
    
    public static void zzd(final zzdtu zzdtu, final zzdtk zzdtk, final String s, final Pair... array) {
        Map map;
        if (zzdtk == null) {
            map = ((zzdtx)zzdtu).zzc();
        }
        else {
            map = zzdtk.zza();
        }
        zzf(map, "action", s);
        for (final Pair pair : array) {
            zzf(map, (String)pair.first, (String)pair.second);
        }
        ((zzdtx)zzdtu).zzf(map);
    }
    
    public static int zze(final zzfeq zzfeq) {
        if (zzfeq.zzq) {
            return 2;
        }
        final zzl zzd = zzfeq.zzd;
        final zzc zzs = zzd.zzs;
        if (zzs == null && zzd.zzx == null) {
            return 1;
        }
        if (zzs != null && zzd.zzx != null) {
            return 5;
        }
        if (zzs != null) {
            return 3;
        }
        return 4;
    }
    
    private static void zzf(final Map map, final String s, final String s2) {
        if (!TextUtils.isEmpty((CharSequence)s)) {
            if (!TextUtils.isEmpty((CharSequence)s2)) {
                map.put(s, s2);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.nonagon.signalgeneration;

import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.ads.internal.client.zzba;
import com.google.android.gms.internal.ads.zzcbn;
import android.util.Pair;
import com.google.android.gms.ads.internal.zzt;
import com.google.android.gms.internal.ads.zzgax;

final class zzz implements zzgax
{
    final zzaa zza;
    
    public zzz(final zzaa zza) {
        this.zza = zza;
    }
    
    public final void zza(final Throwable t) {
        zzt.zzo().zzw(t, "SignalGeneratorImpl.initializeWebViewForSignalCollection");
        final zzaa zza = this.zza;
        zzf.zzc(zzaa.zzp(zza), zzaa.zzo(zza), "sgf", new Pair((Object)"sgf_reason", (Object)t.getMessage()), new Pair((Object)"sgi_rn", (Object)Integer.toString(zzaa.zzE(this.zza).get())));
        zzcbn.zzh("Failed to initialize webview for loading SDKCore. ", t);
        if ((boolean)zzba.zzc().zza(zzbdc.zzjs) && !zzaa.zzD(this.zza).get() && zzaa.zzE(this.zza).getAndIncrement() < (int)zzba.zzc().zza(zzbdc.zzjt)) {
            zzaa.zzI(this.zza);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.nonagon.signalgeneration;

import com.google.android.gms.internal.ads.zzfkh;
import android.os.RemoteException;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.internal.ads.zzbeo;
import android.util.Pair;
import com.google.android.gms.ads.internal.zzt;
import com.google.android.gms.internal.ads.zzfjw;
import com.google.android.gms.internal.ads.zzcab;
import com.google.android.gms.internal.ads.zzcai;
import com.google.android.gms.internal.ads.zzgax;

final class zzw implements zzgax
{
    final ik0 zza;
    final zzcai zzb;
    final zzcab zzc;
    final zzfjw zzd;
    final long zze;
    final zzaa zzf;
    
    public zzw(final zzaa zzf, final ik0 zza, final zzcai zzb, final zzcab zzc, final zzfjw zzd, final long zze) {
        this.zzf = zzf;
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
    }
    
    public final void zza(final Throwable t) {
        final long currentTimeMillis = zzt.zzB().currentTimeMillis();
        final long zze = this.zze;
        final String message = t.getMessage();
        zzt.zzo().zzw(t, "SignalGeneratorImpl.generateSignals");
        final zzaa zzf = this.zzf;
        com.google.android.gms.ads.nonagon.signalgeneration.zzf.zzc(zzaa.zzp(zzf), zzaa.zzo(zzf), "sgf", new Pair((Object)"sgf_reason", (Object)message), new Pair((Object)"tqgt", (Object)String.valueOf(currentTimeMillis - zze)));
        final zzfkh zzr = zzaa.zzr(this.zza, this.zzb);
        if ((boolean)zzbeo.zze.zze() && zzr != null) {
            final zzfjw zzd = this.zzd;
            zzd.zzg(t);
            zzd.zzf(false);
            zzr.zza(zzd);
            zzr.zzg();
        }
        try {
            String string;
            if ("Unknown format is no longer supported.".equals(message)) {
                string = message;
            }
            else {
                final StringBuilder sb = new StringBuilder();
                sb.append("Internal error. ");
                sb.append(message);
                string = sb.toString();
            }
            this.zzc.zzb(string);
        }
        catch (final RemoteException ex) {
            zzcbn.zzh("", (Throwable)ex);
        }
    }
}

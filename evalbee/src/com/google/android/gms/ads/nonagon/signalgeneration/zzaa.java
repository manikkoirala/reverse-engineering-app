// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.nonagon.signalgeneration;

import org.json.JSONObject;
import android.widget.ImageView$ScaleType;
import com.google.android.gms.internal.ads.zzasj;
import com.google.android.gms.ads.internal.util.zzbz;
import android.view.MotionEvent;
import android.webkit.WebView;
import com.google.android.gms.internal.ads.zzbcu;
import com.google.android.gms.internal.ads.zzfjw;
import com.google.android.gms.internal.ads.zzfjv;
import com.google.android.gms.internal.ads.zzcab;
import android.app.Activity;
import com.google.android.gms.dynamic.ObjectWrapper;
import android.view.View;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import com.google.android.gms.internal.ads.zzbeo;
import com.google.android.gms.internal.ads.zzfkk;
import com.google.android.gms.internal.ads.zzfkh;
import com.google.android.gms.internal.ads.zzcai;
import com.google.android.gms.internal.ads.zzfun;
import java.util.concurrent.Callable;
import android.os.RemoteException;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.internal.ads.zzbue;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.ads.zzgax;
import com.google.android.gms.ads.AdFormat;
import com.google.android.gms.internal.ads.zzgah;
import com.google.android.gms.internal.ads.zzftn;
import java.util.concurrent.TimeUnit;
import com.google.android.gms.internal.ads.zzgas;
import java.util.concurrent.Executor;
import com.google.android.gms.internal.ads.zzgai;
import com.google.android.gms.internal.ads.zzgbb;
import com.google.android.gms.internal.ads.zzdpj;
import com.google.android.gms.internal.ads.zzddw;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.internal.client.zzm;
import com.google.android.gms.internal.ads.zzcxp;
import com.google.android.gms.internal.ads.zzfeo;
import com.google.android.gms.ads.internal.client.zzl;
import com.google.android.gms.ads.internal.client.zzq;
import android.text.TextUtils;
import com.google.android.gms.internal.ads.zzcca;
import java.util.Iterator;
import android.net.Uri;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.ads.internal.client.zzba;
import java.util.Map;
import java.util.Collections;
import java.util.WeakHashMap;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Arrays;
import com.google.android.gms.internal.ads.zzfla;
import com.google.android.gms.internal.ads.zzdtu;
import java.util.Set;
import android.graphics.Point;
import com.google.android.gms.internal.ads.zzbun;
import java.util.concurrent.ScheduledExecutorService;
import com.google.android.gms.internal.ads.zzgbl;
import com.google.android.gms.internal.ads.zzdtk;
import com.google.android.gms.internal.ads.zzffn;
import com.google.android.gms.internal.ads.zzasi;
import android.content.Context;
import com.google.android.gms.internal.ads.zzciq;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicBoolean;
import com.google.android.gms.internal.ads.zzcbt;
import java.util.List;
import com.google.android.gms.internal.ads.zzcad;

public final class zzaa extends zzcad
{
    protected static final List zza;
    protected static final List zzb;
    protected static final List zzc;
    protected static final List zzd;
    public static final int zze = 0;
    private final zzcbt zzA;
    private String zzB;
    private final String zzC;
    private final List zzD;
    private final List zzE;
    private final List zzF;
    private final List zzG;
    private final AtomicBoolean zzH;
    private final AtomicBoolean zzI;
    private final AtomicInteger zzJ;
    private final zzciq zzf;
    private Context zzg;
    private final zzasi zzh;
    private final zzffn zzi;
    private zzdtk zzj;
    private final zzgbl zzk;
    private final ScheduledExecutorService zzl;
    private zzbun zzm;
    private Point zzn;
    private Point zzo;
    private final Set zzp;
    private final zzc zzq;
    private final zzdtu zzr;
    private final zzfla zzs;
    private final boolean zzt;
    private final boolean zzu;
    private final boolean zzv;
    private final boolean zzw;
    private final String zzx;
    private final String zzy;
    private final AtomicInteger zzz;
    
    static {
        zza = new ArrayList(Arrays.asList("/aclk", "/pcs/click", "/dbm/clk"));
        zzb = new ArrayList(Arrays.asList(".doubleclick.net", ".googleadservices.com"));
        zzc = new ArrayList(Arrays.asList("/pagead/adview", "/pcs/view", "/pagead/conversion", "/dbm/ad"));
        zzd = new ArrayList(Arrays.asList(".doubleclick.net", ".googleadservices.com", ".googlesyndication.com"));
    }
    
    public zzaa(final zzciq zzf, final Context zzg, final zzasi zzh, final zzffn zzi, final zzgbl zzk, final ScheduledExecutorService zzl, final zzdtu zzr, final zzfla zzs, final zzcbt zzA) {
        this.zzj = null;
        this.zzn = new Point();
        this.zzo = new Point();
        this.zzp = Collections.newSetFromMap(new WeakHashMap<Object, Boolean>());
        this.zzz = new AtomicInteger(0);
        this.zzH = new AtomicBoolean(false);
        this.zzI = new AtomicBoolean(false);
        this.zzJ = new AtomicInteger(0);
        this.zzf = zzf;
        this.zzg = zzg;
        this.zzh = zzh;
        this.zzi = zzi;
        this.zzk = zzk;
        this.zzl = zzl;
        this.zzq = zzf.zzn();
        this.zzr = zzr;
        this.zzs = zzs;
        this.zzA = zzA;
        this.zzt = (boolean)zzba.zzc().zza(zzbdc.zzhl);
        this.zzu = (boolean)zzba.zzc().zza(zzbdc.zzhk);
        this.zzv = (boolean)zzba.zzc().zza(zzbdc.zzhn);
        this.zzw = (boolean)zzba.zzc().zza(zzbdc.zzhp);
        this.zzx = (String)zzba.zzc().zza(zzbdc.zzho);
        this.zzy = (String)zzba.zzc().zza(zzbdc.zzhq);
        this.zzC = (String)zzba.zzc().zza(zzbdc.zzhr);
        List zzG;
        if (zzba.zzc().zza(zzbdc.zzhs)) {
            this.zzD = zzac((String)zzba.zzc().zza(zzbdc.zzht));
            this.zzE = zzac((String)zzba.zzc().zza(zzbdc.zzhu));
            this.zzF = zzac((String)zzba.zzc().zza(zzbdc.zzhv));
            zzG = zzac((String)zzba.zzc().zza(zzbdc.zzhw));
        }
        else {
            this.zzD = zzaa.zza;
            this.zzE = zzaa.zzb;
            this.zzF = zzaa.zzc;
            zzG = zzaa.zzd;
        }
        this.zzG = zzG;
    }
    
    private final zzh zzT(final Context context, final String s, final String s2, final zzq zzq, final zzl zzl) {
        final zzfeo zzfeo = new zzfeo();
        if ("REWARDED".equals(s2)) {
            zzfeo.zzo().zza(2);
        }
        else if ("REWARDED_INTERSTITIAL".equals(s2)) {
            zzfeo.zzo().zza(3);
        }
        final zzg zzo = this.zzf.zzo();
        final zzcxp zzcxp = new zzcxp();
        zzcxp.zze(context);
        String s3;
        if ((s3 = s) == null) {
            s3 = "adUnitId";
        }
        zzfeo.zzs(s3);
        zzl zza;
        if ((zza = zzl) == null) {
            zza = new zzm().zza();
        }
        zzfeo.zzE(zza);
        zzq zzq2;
        if ((zzq2 = zzq) == null) {
            int n = 0;
            Label_0270: {
                switch (s2.hashCode()) {
                    case 1951953708: {
                        if (s2.equals("BANNER")) {
                            n = 0;
                            break Label_0270;
                        }
                        break;
                    }
                    case 1854800829: {
                        if (s2.equals("REWARDED_INTERSTITIAL")) {
                            n = 2;
                            break Label_0270;
                        }
                        break;
                    }
                    case 543046670: {
                        if (s2.equals("REWARDED")) {
                            n = 1;
                            break Label_0270;
                        }
                        break;
                    }
                    case -428325382: {
                        if (s2.equals("APP_OPEN_AD")) {
                            n = 4;
                            break Label_0270;
                        }
                        break;
                    }
                    case -1999289321: {
                        if (s2.equals("NATIVE")) {
                            n = 3;
                            break Label_0270;
                        }
                        break;
                    }
                }
                n = -1;
            }
            if (n != 0) {
                if (n != 1 && n != 2) {
                    if (n != 3) {
                        if (n != 4) {
                            zzq2 = new zzq();
                        }
                        else {
                            zzq2 = zzq.zzb();
                        }
                    }
                    else {
                        zzq2 = zzq.zzc();
                    }
                }
                else {
                    zzq2 = zzq.zzd();
                }
            }
            else {
                zzq2 = new zzq(context, AdSize.BANNER);
            }
        }
        zzfeo.zzr(zzq2);
        zzfeo.zzx(true);
        zzcxp.zzi(zzfeo.zzG());
        zzo.zza(zzcxp.zzj());
        final zzac zzac = new zzac();
        zzac.zza(s2);
        zzo.zzb(new zzae(zzac, null));
        new zzddw();
        final zzh zzc = zzo.zzc();
        this.zzj = zzc.zza();
        return zzc;
    }
    
    private final ik0 zzU(final String s) {
        final zzdpj[] array = { null };
        final ik0 zzn = zzgbb.zzn(this.zzi.zza(), (zzgai)new com.google.android.gms.ads.nonagon.signalgeneration.zzl(this, array, s), (Executor)this.zzk);
        zzn.addListener(new com.google.android.gms.ads.nonagon.signalgeneration.zzm(this, array), (Executor)this.zzk);
        return zzgbb.zze(zzgbb.zzm((ik0)zzgbb.zzo((ik0)zzgas.zzu(zzn), (long)(int)zzba.zzc().zza(zzbdc.zzhE), TimeUnit.MILLISECONDS, this.zzl), (zzftn)com.google.android.gms.ads.nonagon.signalgeneration.zzs.zza, (Executor)this.zzk), (Class)Exception.class, (zzftn)com.google.android.gms.ads.nonagon.signalgeneration.zzt.zza, (Executor)this.zzk);
    }
    
    private final void zzV() {
        ik0 ik0;
        if (zzba.zzc().zza(zzbdc.zzko)) {
            ik0 = zzgbb.zzk((zzgah)new zzi(this), (Executor)zzcca.zza);
        }
        else {
            ik0 = this.zzT(this.zzg, null, AdFormat.BANNER.name(), null, null).zzc();
        }
        zzgbb.zzr(ik0, (zzgax)new zzz(this), this.zzf.zzB());
    }
    
    private final void zzW() {
        if ((boolean)zzba.zzc().zza(zzbdc.zzjl) && !(boolean)zzba.zzc().zza(zzbdc.zzjo) && (!(boolean)zzba.zzc().zza(zzbdc.zzjs) || !this.zzH.getAndSet(true))) {
            this.zzV();
        }
    }
    
    private final void zzX(final List obj, final IObjectWrapper objectWrapper, final zzbue zzbue, final boolean b) {
        if (!(boolean)zzba.zzc().zza(zzbdc.zzhD)) {
            zzcbn.zzj("The updating URL feature is not enabled.");
            try {
                zzbue.zze("The updating URL feature is not enabled.");
                return;
            }
            catch (final RemoteException ex) {
                zzcbn.zzh("", (Throwable)ex);
                return;
            }
        }
        final Iterator iterator = obj.iterator();
        int n = 0;
        while (iterator.hasNext()) {
            if (this.zzQ((Uri)iterator.next())) {
                ++n;
            }
        }
        if (n > 1) {
            zzcbn.zzj("Multiple google urls found: ".concat(String.valueOf(obj)));
        }
        final ArrayList list = new ArrayList();
        for (final Uri obj2 : obj) {
            ik0 ik0;
            if (!this.zzQ(obj2)) {
                zzcbn.zzj("Not a Google URL: ".concat(String.valueOf(obj2)));
                ik0 = zzgbb.zzh((Object)obj2);
            }
            else {
                ik0 = this.zzk.zzb((Callable)new zzn(this, obj2, objectWrapper));
                if (this.zzaa()) {
                    ik0 = zzgbb.zzn(ik0, (zzgai)new zzo(this), (Executor)this.zzk);
                }
                else {
                    zzcbn.zzi("Asset view map is empty.");
                }
            }
            list.add(ik0);
        }
        zzgbb.zzr(zzgbb.zzd((Iterable)list), (zzgax)new zzy(this, zzbue, b), this.zzf.zzB());
    }
    
    private final void zzY(final List list, final IObjectWrapper objectWrapper, final zzbue zzbue, final boolean b) {
        if (!(boolean)zzba.zzc().zza(zzbdc.zzhD)) {
            try {
                zzbue.zze("The updating URL feature is not enabled.");
                return;
            }
            catch (final RemoteException ex) {
                zzcbn.zzh("", (Throwable)ex);
                return;
            }
        }
        ik0 ik0 = this.zzk.zzb((Callable)new zzu(this, list, objectWrapper));
        if (this.zzaa()) {
            ik0 = zzgbb.zzn(ik0, (zzgai)new zzv(this), (Executor)this.zzk);
        }
        else {
            zzcbn.zzi("Asset view map is empty.");
        }
        zzgbb.zzr(ik0, (zzgax)new zzx(this, zzbue, b), this.zzf.zzB());
    }
    
    private static boolean zzZ(final Uri uri, final List list, final List list2) {
        final String host = uri.getHost();
        final String path = uri.getPath();
        if (host != null) {
            if (path != null) {
                final Iterator iterator = list.iterator();
                while (iterator.hasNext()) {
                    if (path.contains((CharSequence)iterator.next())) {
                        final Iterator iterator2 = list2.iterator();
                        while (iterator2.hasNext()) {
                            if (host.endsWith((String)iterator2.next())) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }
    
    private final boolean zzaa() {
        final zzbun zzm = this.zzm;
        if (zzm != null) {
            final Map zzb = zzm.zzb;
            if (zzb != null && !zzb.isEmpty()) {
                return true;
            }
        }
        return false;
    }
    
    private static final Uri zzab(final Uri uri, final String str, final String str2) {
        final String string = uri.toString();
        int n;
        if ((n = string.indexOf("&adurl=")) == -1) {
            n = string.indexOf("?adurl=");
        }
        if (n != -1) {
            ++n;
            final StringBuilder sb = new StringBuilder(string.substring(0, n));
            sb.append(str);
            sb.append("=");
            sb.append(str2);
            sb.append("&");
            sb.append(string.substring(n));
            return Uri.parse(sb.toString());
        }
        return uri.buildUpon().appendQueryParameter(str, str2).build();
    }
    
    private static final List zzac(final String s) {
        final String[] split = TextUtils.split(s, ",");
        final ArrayList list = new ArrayList();
        for (final String s2 : split) {
            if (!zzfun.zzd(s2)) {
                list.add(s2);
            }
        }
        return list;
    }
    
    public final boolean zzQ(final Uri uri) {
        return zzZ(uri, this.zzD, this.zzE);
    }
    
    public final boolean zzR(final Uri uri) {
        return zzZ(uri, this.zzF, this.zzG);
    }
    
    public final void zze(final IObjectWrapper objectWrapper, final zzcai zzcai, final zzcab zzcab) {
        final Context zzg = ObjectWrapper.unwrap(objectWrapper);
        this.zzg = zzg;
        final zzfjw zza = zzfjv.zza(zzg, 22);
        zza.zzh();
        ik0 zzg2 = null;
        ik0 zzg3 = null;
        Label_0249: {
            if (AdFormat.UNKNOWN.name().equals(zzcai.zzb)) {
                List<String> list = new ArrayList<String>();
                final zzbcu zzhC = zzbdc.zzhC;
                if (!((String)zzba.zzc().zza(zzhC)).isEmpty()) {
                    list = Arrays.asList(((String)zzba.zzc().zza(zzhC)).split(","));
                }
                if (list.contains(com.google.android.gms.ads.nonagon.signalgeneration.zzf.zzb(zzcai.zzd))) {
                    zzg2 = zzgbb.zzg((Throwable)new IllegalArgumentException("Unknown format is no longer supported."));
                    zzg3 = zzgbb.zzg((Throwable)new IllegalArgumentException("Unknown format is no longer supported."));
                    break Label_0249;
                }
            }
            ik0 ik0;
            ik0 ik2;
            if (zzba.zzc().zza(zzbdc.zzko)) {
                final zzgbl zza2 = zzcca.zza;
                ik0 = zza2.zzb((Callable)new com.google.android.gms.ads.nonagon.signalgeneration.zzq(this, zzcai));
                ik2 = zzgbb.zzn(ik0, (zzgai)com.google.android.gms.ads.nonagon.signalgeneration.zzr.zza, (Executor)zza2);
            }
            else {
                final zzh zzT = this.zzT(this.zzg, zzcai.zza, zzcai.zzb, zzcai.zzc, zzcai.zzd);
                ik0 = zzgbb.zzh((Object)zzT);
                ik2 = zzT.zzc();
            }
            zzg3 = ik2;
            zzg2 = ik0;
        }
        zzgbb.zzr(zzg3, (zzgax)new zzw(this, zzg2, zzcai, zzcab, zza, com.google.android.gms.ads.internal.zzt.zzB().currentTimeMillis()), this.zzf.zzB());
    }
    
    public final void zzf(final zzbun zzm) {
        this.zzm = zzm;
        this.zzi.zzc(1);
    }
    
    public final void zzg(final List list, final IObjectWrapper objectWrapper, final zzbue zzbue) {
        this.zzX(list, objectWrapper, zzbue, true);
    }
    
    public final void zzh(final List list, final IObjectWrapper objectWrapper, final zzbue zzbue) {
        this.zzY(list, objectWrapper, zzbue, true);
    }
    
    public final void zzi(final IObjectWrapper objectWrapper) {
        if (zzba.zzc().zza(zzbdc.zzjk)) {
            final zzbcu zzhB = zzbdc.zzhB;
            if (!(boolean)zzba.zzc().zza(zzhB)) {
                this.zzW();
            }
            final WebView webView = ObjectWrapper.unwrap(objectWrapper);
            if (webView == null) {
                zzcbn.zzg("The webView cannot be null.");
                return;
            }
            if (this.zzp.contains(webView)) {
                zzcbn.zzi("This webview has already been registered.");
                return;
            }
            this.zzp.add(webView);
            webView.addJavascriptInterface((Object)new TaggingLibraryJsInterface(webView, this.zzh, this.zzr, this.zzs), "gmaSdk");
            if (zzba.zzc().zza(zzbdc.zzju)) {
                com.google.android.gms.ads.internal.zzt.zzo().zzs();
            }
            if (zzba.zzc().zza(zzhB)) {
                this.zzW();
            }
        }
    }
    
    public final void zzj(final IObjectWrapper objectWrapper) {
        if (!(boolean)zzba.zzc().zza(zzbdc.zzhD)) {
            return;
        }
        final MotionEvent motionEvent = ObjectWrapper.unwrap(objectWrapper);
        final zzbun zzm = this.zzm;
        View zza;
        if (zzm == null) {
            zza = null;
        }
        else {
            zza = zzm.zza;
        }
        this.zzn = zzbz.zza(motionEvent, zza);
        if (motionEvent.getAction() == 0) {
            this.zzo = this.zzn;
        }
        final MotionEvent obtain = MotionEvent.obtain(motionEvent);
        final Point zzn = this.zzn;
        obtain.setLocation((float)zzn.x, (float)zzn.y);
        this.zzh.zzd(obtain);
        obtain.recycle();
    }
    
    public final void zzk(final List list, final IObjectWrapper objectWrapper, final zzbue zzbue) {
        this.zzX(list, objectWrapper, zzbue, false);
    }
    
    public final void zzl(final List list, final IObjectWrapper objectWrapper, final zzbue zzbue) {
        this.zzY(list, objectWrapper, zzbue, false);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.nonagon.signalgeneration;

import com.google.android.gms.internal.ads.zzgbl;
import android.content.Context;
import com.google.android.gms.internal.ads.zzcji;
import com.google.android.gms.internal.ads.zzfla;
import com.google.android.gms.internal.ads.zzdtu;
import java.util.concurrent.ScheduledExecutorService;
import com.google.android.gms.internal.ads.zzhdx;
import com.google.android.gms.internal.ads.zzcca;
import com.google.android.gms.internal.ads.zzffn;
import com.google.android.gms.internal.ads.zzasi;
import com.google.android.gms.internal.ads.zzciw;
import com.google.android.gms.internal.ads.zzciq;
import com.google.android.gms.internal.ads.zzhec;
import com.google.android.gms.internal.ads.zzhdp;

public final class zzab implements zzhdp
{
    private final zzhec zza;
    private final zzhec zzb;
    private final zzhec zzc;
    private final zzhec zzd;
    private final zzhec zze;
    private final zzhec zzf;
    private final zzhec zzg;
    private final zzhec zzh;
    private final zzhec zzi;
    
    public zzab(final zzhec zza, final zzhec zzb, final zzhec zzc, final zzhec zzd, final zzhec zze, final zzhec zzf, final zzhec zzg, final zzhec zzh, final zzhec zzi) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
        this.zzg = zzg;
        this.zzh = zzh;
        this.zzi = zzi;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.nonagon.signalgeneration;

import com.google.android.gms.internal.ads.zzgbb;
import com.google.android.gms.internal.ads.zzbwa;
import com.google.android.gms.internal.ads.zzdyl;
import java.util.concurrent.Executor;
import com.google.android.gms.internal.ads.zzgai;

public final class zzak implements zzgai
{
    private final Executor zza;
    private final zzdyl zzb;
    
    public zzak(final Executor zza, final zzdyl zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
}

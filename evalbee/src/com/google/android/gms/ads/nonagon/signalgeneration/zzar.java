// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.nonagon.signalgeneration;

import org.json.JSONException;
import org.json.JSONObject;
import com.google.android.gms.ads.query.QueryInfo;
import android.webkit.ValueCallback;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.ads.query.QueryInfoGenerationCallback;

final class zzar extends QueryInfoGenerationCallback
{
    final String zza;
    final TaggingLibraryJsInterface zzb;
    
    public zzar(final TaggingLibraryJsInterface zzb, final String zza) {
        this.zzb = zzb;
        this.zza = zza;
    }
    
    @Override
    public final void onFailure(String format) {
        zzcbn.zzj("Failed to generate query info for the tagging library, error: ".concat(String.valueOf(format)));
        format = String.format("window.postMessage({'paw_id': '%1$s', 'error': '%2$s'}, '*');", this.zza, format);
        TaggingLibraryJsInterface.zza(this.zzb).evaluateJavascript(format, (ValueCallback)null);
    }
    
    @Override
    public final void onSuccess(QueryInfo queryInfo) {
        final String query = queryInfo.getQuery();
        try {
            final JSONObject jsonObject = new JSONObject();
            jsonObject.put("paw_id", (Object)this.zza);
            jsonObject.put("signal", (Object)query);
            queryInfo = (QueryInfo)String.format("window.postMessage(%1$s, '*');", jsonObject);
        }
        catch (final JSONException ex) {
            queryInfo = (QueryInfo)String.format("window.postMessage({'paw_id': '%1$s', 'signal': '%2$s'}, '*');", this.zza, queryInfo.getQuery());
        }
        TaggingLibraryJsInterface.zza(this.zzb).evaluateJavascript((String)queryInfo, (ValueCallback)null);
    }
}

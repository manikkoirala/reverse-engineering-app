// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.nonagon.signalgeneration;

import com.google.android.gms.internal.ads.zzfim;
import java.util.concurrent.TimeUnit;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.ads.internal.client.zzba;
import com.google.android.gms.internal.ads.zzgai;
import com.google.android.gms.internal.ads.zzcxm;
import com.google.android.gms.internal.ads.zzfio;
import com.google.android.gms.internal.ads.zzfiu;
import com.google.android.gms.internal.ads.zzhec;
import com.google.android.gms.internal.ads.zzhdp;

public final class zzai implements zzhdp
{
    private final zzhec zza;
    private final zzhec zzb;
    private final zzhec zzc;
    
    public zzai(final zzhec zza, final zzhec zzb, final zzhec zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
}

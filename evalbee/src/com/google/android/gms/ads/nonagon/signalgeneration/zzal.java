// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.nonagon.signalgeneration;

import com.google.android.gms.internal.ads.zzgbl;
import java.util.concurrent.Executor;
import com.google.android.gms.internal.ads.zzdym;
import com.google.android.gms.internal.ads.zzhdx;
import com.google.android.gms.internal.ads.zzcca;
import com.google.android.gms.internal.ads.zzhec;
import com.google.android.gms.internal.ads.zzhdp;

public final class zzal implements zzhdp
{
    private final zzhec zza;
    private final zzhec zzb;
    
    public zzal(final zzhec zza, final zzhec zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public final zzak zza() {
        final zzgbl zza = zzcca.zza;
        zzhdx.zzb((Object)zza);
        return new zzak((Executor)zza, ((zzdym)this.zzb).zza());
    }
}

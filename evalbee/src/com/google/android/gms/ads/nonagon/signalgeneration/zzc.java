// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.nonagon.signalgeneration;

import com.google.android.gms.internal.ads.zzdtx;
import java.util.concurrent.Executor;
import android.text.TextUtils;
import java.util.Iterator;
import java.util.Set;
import java.util.ConcurrentModificationException;
import com.google.android.gms.ads.internal.zzt;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.concurrent.ConcurrentHashMap;
import android.util.Pair;
import com.google.android.gms.internal.ads.zzcca;
import com.google.android.gms.internal.ads.zzdtk;
import java.util.Collections;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.ads.internal.client.zzba;
import com.google.android.gms.internal.ads.zzdtu;
import java.util.ArrayDeque;
import java.util.Map;

public final class zzc
{
    private final int zza;
    private final long zzb;
    private final boolean zzc;
    private final boolean zzd;
    private final Map zze;
    private final ArrayDeque zzf;
    private final ArrayDeque zzg;
    private final zzdtu zzh;
    private Map zzi;
    
    public zzc(final zzdtu zzh) {
        this.zzf = new ArrayDeque();
        this.zzg = new ArrayDeque();
        this.zzh = zzh;
        this.zza = (int)zzba.zzc().zza(zzbdc.zzgX);
        this.zzb = (long)zzba.zzc().zza(zzbdc.zzgY);
        this.zzc = (boolean)zzba.zzc().zza(zzbdc.zzhd);
        this.zzd = (boolean)zzba.zzc().zza(zzbdc.zzhb);
        this.zze = Collections.synchronizedMap((Map<Object, Object>)new zzb(this));
    }
    
    private final void zzg(final zzdtk zzdtk) {
        synchronized (this) {
            if (!this.zzc) {
                return;
            }
            final ArrayDeque zzg = this.zzg;
            final ArrayDeque clone = zzg.clone();
            zzg.clear();
            final ArrayDeque zzf = this.zzf;
            final ArrayDeque clone2 = zzf.clone();
            zzf.clear();
            ((Executor)zzcca.zza).execute(new zza(this, zzdtk, clone, clone2));
        }
    }
    
    private final void zzh(final zzdtk zzdtk, final ArrayDeque arrayDeque, final String s) {
        while (!arrayDeque.isEmpty()) {
            final Pair pair = arrayDeque.poll();
            (this.zzi = new ConcurrentHashMap(zzdtk.zza())).put("action", "ev");
            this.zzi.put("e_r", s);
            this.zzi.put("e_id", pair.first);
            if (this.zzd) {
                final String s2 = (String)pair.second;
                Pair pair2;
                try {
                    final JSONObject jsonObject = new JSONObject(s2);
                    pair2 = new Pair((Object)com.google.android.gms.ads.nonagon.signalgeneration.zzf.zza(jsonObject.getJSONObject("extras").getString("query_info_type")), (Object)jsonObject.getString("request_agent"));
                }
                catch (final JSONException ex) {
                    pair2 = new Pair((Object)"", (Object)"");
                }
                zzj(this.zzi, "e_type", (String)pair2.first);
                zzj(this.zzi, "e_agent", (String)pair2.second);
            }
            ((zzdtx)this.zzh).zzf(this.zzi);
        }
    }
    
    private final void zzi() {
        synchronized (this) {
            final long currentTimeMillis = zzt.zzB().currentTimeMillis();
            final Set entrySet = this.zze.entrySet();
            try {
                final Iterator iterator = entrySet.iterator();
                while (iterator.hasNext()) {
                    final Map.Entry<K, Pair> entry = (Map.Entry<K, Pair>)iterator.next();
                    if (currentTimeMillis - (long)entry.getValue().first <= this.zzb) {
                        break;
                    }
                    this.zzg.add(new Pair((Object)entry.getKey(), (Object)entry.getValue().second));
                    iterator.remove();
                }
            }
            catch (final ConcurrentModificationException ex) {
                zzt.zzo().zzw((Throwable)ex, "QueryJsonMap.removeExpiredEntries");
            }
        }
    }
    
    private static final void zzj(final Map map, final String s, final String s2) {
        if (!TextUtils.isEmpty((CharSequence)s2)) {
            map.put(s, s2);
        }
    }
    
    public final String zzb(final String s, final zzdtk zzdtk) {
        synchronized (this) {
            final Pair pair = this.zze.get(s);
            zzdtk.zza().put("rid", s);
            if (pair != null) {
                final String s2 = (String)pair.second;
                this.zze.remove(s);
                zzdtk.zza().put("mhit", "true");
                return s2;
            }
            zzdtk.zza().put("mhit", "false");
            return null;
        }
    }
    
    public final void zzd(final String s, final String s2, final zzdtk zzdtk) {
        synchronized (this) {
            this.zze.put(s, new Pair((Object)zzt.zzB().currentTimeMillis(), (Object)s2));
            this.zzi();
            this.zzg(zzdtk);
        }
    }
    
    public final void zzf(final String s) {
        synchronized (this) {
            this.zze.remove(s);
        }
    }
}

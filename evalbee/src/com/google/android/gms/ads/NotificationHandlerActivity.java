// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads;

import com.google.android.gms.internal.ads.zzbti;
import android.os.RemoteException;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.internal.ads.zzbpr;
import android.content.Context;
import com.google.android.gms.internal.ads.zzbpo;
import com.google.android.gms.ads.internal.client.zzay;
import android.os.Bundle;
import com.google.android.gms.common.annotation.KeepForSdk;
import android.app.Activity;

@KeepForSdk
public final class NotificationHandlerActivity extends Activity
{
    @KeepForSdk
    public static final String CLASS_NAME = "com.google.android.gms.ads.NotificationHandlerActivity";
    
    public final void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        try {
            final zzbti zzm = zzay.zza().zzm((Context)this, (zzbpr)new zzbpo());
            if (zzm == null) {
                zzcbn.zzg("OfflineUtils is null");
                return;
            }
            zzm.zze(this.getIntent());
        }
        catch (final RemoteException ex) {
            zzcbn.zzg("RemoteException calling handleNotificationIntent: ".concat(ex.toString()));
        }
    }
    
    public final void onResume() {
        super.onResume();
        this.finish();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads;

import com.google.android.gms.ads.internal.client.zzfl;

public final class VideoOptions
{
    private final boolean zza = Builder.zzc(builder);
    private final boolean zzb = Builder.zzb(builder);
    private final boolean zzc = Builder.zza(builder);
    
    public VideoOptions(final zzfl zzfl) {
        this.zza = zzfl.zza;
        this.zzb = zzfl.zzb;
        this.zzc = zzfl.zzc;
    }
    
    public boolean getClickToExpandRequested() {
        return this.zzc;
    }
    
    public boolean getCustomControlsRequested() {
        return this.zzb;
    }
    
    public boolean getStartMuted() {
        return this.zza;
    }
    
    public static final class Builder
    {
        private boolean zza;
        private boolean zzb;
        private boolean zzc;
        
        public Builder() {
            this.zza = true;
            this.zzb = false;
            this.zzc = false;
        }
        
        public VideoOptions build() {
            return new VideoOptions(this, null);
        }
        
        public Builder setClickToExpandRequested(final boolean zzc) {
            this.zzc = zzc;
            return this;
        }
        
        public Builder setCustomControlsRequested(final boolean zzb) {
            this.zzb = zzb;
            return this;
        }
        
        public Builder setStartMuted(final boolean zza) {
            this.zza = zza;
            return this;
        }
    }
}

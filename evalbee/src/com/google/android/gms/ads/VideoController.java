// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads;

import com.google.android.gms.ads.internal.client.zzdt;
import com.google.android.gms.ads.internal.client.zzfk;
import android.os.RemoteException;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.ads.internal.client.zzdq;
import com.google.android.gms.common.annotation.KeepForSdk;

public final class VideoController
{
    @KeepForSdk
    public static final int PLAYBACK_STATE_ENDED = 3;
    @KeepForSdk
    public static final int PLAYBACK_STATE_PAUSED = 2;
    @KeepForSdk
    public static final int PLAYBACK_STATE_PLAYING = 1;
    @KeepForSdk
    public static final int PLAYBACK_STATE_READY = 5;
    @KeepForSdk
    public static final int PLAYBACK_STATE_UNKNOWN = 0;
    private final Object zza;
    private zzdq zzb;
    private VideoLifecycleCallbacks zzc;
    
    public VideoController() {
        this.zza = new Object();
    }
    
    @KeepForSdk
    public int getPlaybackState() {
        synchronized (this.zza) {
            final zzdq zzb = this.zzb;
            if (zzb == null) {
                return 0;
            }
            try {
                return zzb.zzh();
            }
            catch (final RemoteException ex) {
                zzcbn.zzh("Unable to call getPlaybackState on video controller.", (Throwable)ex);
                return 0;
            }
        }
    }
    
    public VideoLifecycleCallbacks getVideoLifecycleCallbacks() {
        synchronized (this.zza) {
            return this.zzc;
        }
    }
    
    public boolean hasVideoContent() {
        synchronized (this.zza) {
            return this.zzb != null;
        }
    }
    
    public boolean isClickToExpandEnabled() {
        synchronized (this.zza) {
            final zzdq zzb = this.zzb;
            if (zzb == null) {
                return false;
            }
            try {
                return zzb.zzo();
            }
            catch (final RemoteException ex) {
                zzcbn.zzh("Unable to call isClickToExpandEnabled.", (Throwable)ex);
                return false;
            }
        }
    }
    
    public boolean isCustomControlsEnabled() {
        synchronized (this.zza) {
            final zzdq zzb = this.zzb;
            if (zzb == null) {
                return false;
            }
            try {
                return zzb.zzp();
            }
            catch (final RemoteException ex) {
                zzcbn.zzh("Unable to call isUsingCustomPlayerControls.", (Throwable)ex);
                return false;
            }
        }
    }
    
    public boolean isMuted() {
        synchronized (this.zza) {
            final zzdq zzb = this.zzb;
            if (zzb == null) {
                return true;
            }
            try {
                return zzb.zzq();
            }
            catch (final RemoteException ex) {
                zzcbn.zzh("Unable to call isMuted on video controller.", (Throwable)ex);
                return true;
            }
        }
    }
    
    public void mute(final boolean b) {
        synchronized (this.zza) {
            final zzdq zzb = this.zzb;
            if (zzb == null) {
                return;
            }
            try {
                zzb.zzj(b);
            }
            catch (final RemoteException ex) {
                zzcbn.zzh("Unable to call mute on video controller.", (Throwable)ex);
            }
        }
    }
    
    public void pause() {
        synchronized (this.zza) {
            final zzdq zzb = this.zzb;
            if (zzb == null) {
                return;
            }
            try {
                zzb.zzk();
            }
            catch (final RemoteException ex) {
                zzcbn.zzh("Unable to call pause on video controller.", (Throwable)ex);
            }
        }
    }
    
    public void play() {
        synchronized (this.zza) {
            final zzdq zzb = this.zzb;
            if (zzb == null) {
                return;
            }
            try {
                zzb.zzl();
            }
            catch (final RemoteException ex) {
                zzcbn.zzh("Unable to call play on video controller.", (Throwable)ex);
            }
        }
    }
    
    public void setVideoLifecycleCallbacks(final VideoLifecycleCallbacks zzc) {
        synchronized (this.zza) {
            this.zzc = zzc;
            final zzdq zzb = this.zzb;
            if (zzb == null) {
                return;
            }
            Label_0042: {
                if (zzc == null) {
                    final zzdt zzdt = null;
                    break Label_0042;
                }
                try {
                    final zzdt zzdt = new zzfk(zzc);
                    zzb.zzm(zzdt);
                }
                catch (final RemoteException ex) {
                    zzcbn.zzh("Unable to call setVideoLifecycleCallbacks on video controller.", (Throwable)ex);
                }
            }
        }
    }
    
    public void stop() {
        synchronized (this.zza) {
            final zzdq zzb = this.zzb;
            if (zzb == null) {
                return;
            }
            try {
                zzb.zzn();
            }
            catch (final RemoteException ex) {
                zzcbn.zzh("Unable to call stop on video controller.", (Throwable)ex);
            }
        }
    }
    
    public final zzdq zza() {
        synchronized (this.zza) {
            return this.zzb;
        }
    }
    
    public final void zzb(final zzdq zzb) {
        synchronized (this.zza) {
            this.zzb = zzb;
            final VideoLifecycleCallbacks zzc = this.zzc;
            if (zzc != null) {
                this.setVideoLifecycleCallbacks(zzc);
            }
        }
    }
    
    public abstract static class VideoLifecycleCallbacks
    {
        public void onVideoEnd() {
        }
        
        public void onVideoMute(final boolean b) {
        }
        
        public void onVideoPause() {
        }
        
        public void onVideoPlay() {
        }
        
        public void onVideoStart() {
        }
    }
}

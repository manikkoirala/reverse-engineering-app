// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.rewarded;

public class ServerSideVerificationOptions
{
    private final String zza = Builder.zzb(builder);
    private final String zzb = Builder.zza(builder);
    
    public String getCustomData() {
        return this.zzb;
    }
    
    public String getUserId() {
        return this.zza;
    }
    
    public static final class Builder
    {
        private String zza;
        private String zzb;
        
        public Builder() {
            this.zza = "";
            this.zzb = "";
        }
        
        public ServerSideVerificationOptions build() {
            return new ServerSideVerificationOptions(this, null);
        }
        
        public Builder setCustomData(final String zzb) {
            this.zzb = zzb;
            return this;
        }
        
        public Builder setUserId(final String zza) {
            this.zza = zza;
            return this;
        }
    }
}

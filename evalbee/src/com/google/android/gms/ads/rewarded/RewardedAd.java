// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.rewarded;

import com.google.android.gms.ads.OnUserEarnedRewardListener;
import android.app.Activity;
import com.google.android.gms.ads.ResponseInfo;
import com.google.android.gms.ads.OnPaidEventListener;
import com.google.android.gms.ads.FullScreenContentCallback;
import android.os.Bundle;
import com.google.android.gms.ads.admanager.AdManagerAdRequest;
import com.google.android.gms.internal.ads.zzbxs;
import com.google.android.gms.internal.ads.zzcbn;
import com.google.android.gms.internal.ads.zzcbc;
import com.google.android.gms.ads.internal.client.zzba;
import com.google.android.gms.internal.ads.zzbet;
import com.google.android.gms.internal.ads.zzbdc;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.ads.AdRequest;
import android.content.Context;

public abstract class RewardedAd
{
    public static void load(final Context context, final String s, final AdRequest adRequest, final RewardedAdLoadCallback rewardedAdLoadCallback) {
        Preconditions.checkNotNull(context, "Context cannot be null.");
        Preconditions.checkNotNull(s, "AdUnitId cannot be null.");
        Preconditions.checkNotNull(adRequest, "AdRequest cannot be null.");
        Preconditions.checkNotNull(rewardedAdLoadCallback, "LoadCallback cannot be null.");
        Preconditions.checkMainThread("#008 Must be called on the main UI thread.");
        zzbdc.zza(context);
        if ((boolean)zzbet.zzl.zze() && (boolean)zzba.zzc().zza(zzbdc.zzkt)) {
            zzcbc.zzb.execute(new zzc(context, s, adRequest, rewardedAdLoadCallback));
            return;
        }
        zzcbn.zze("Loading on UI thread");
        new zzbxs(context, s).zza(adRequest.zza(), rewardedAdLoadCallback);
    }
    
    public static void load(final Context context, final String s, final AdManagerAdRequest adManagerAdRequest, final RewardedAdLoadCallback rewardedAdLoadCallback) {
        Preconditions.checkNotNull(context, "Context cannot be null.");
        Preconditions.checkNotNull(s, "AdUnitId cannot be null.");
        Preconditions.checkNotNull(adManagerAdRequest, "AdManagerAdRequest cannot be null.");
        Preconditions.checkNotNull(rewardedAdLoadCallback, "LoadCallback cannot be null.");
        Preconditions.checkMainThread("#008 Must be called on the main UI thread.");
        zzbdc.zza(context);
        if ((boolean)zzbet.zzl.zze() && (boolean)zzba.zzc().zza(zzbdc.zzkt)) {
            zzcbn.zze("Loading on background thread");
            zzcbc.zzb.execute(new zzb(context, s, adManagerAdRequest, rewardedAdLoadCallback));
            return;
        }
        zzcbn.zze("Loading on UI thread");
        new zzbxs(context, s).zza(adManagerAdRequest.zza(), rewardedAdLoadCallback);
    }
    
    public abstract Bundle getAdMetadata();
    
    public abstract String getAdUnitId();
    
    public abstract FullScreenContentCallback getFullScreenContentCallback();
    
    public abstract OnAdMetadataChangedListener getOnAdMetadataChangedListener();
    
    public abstract OnPaidEventListener getOnPaidEventListener();
    
    public abstract ResponseInfo getResponseInfo();
    
    public abstract RewardItem getRewardItem();
    
    public abstract void setFullScreenContentCallback(final FullScreenContentCallback p0);
    
    public abstract void setImmersiveMode(final boolean p0);
    
    public abstract void setOnAdMetadataChangedListener(final OnAdMetadataChangedListener p0);
    
    public abstract void setOnPaidEventListener(final OnPaidEventListener p0);
    
    public abstract void setServerSideVerificationOptions(final ServerSideVerificationOptions p0);
    
    public abstract void show(final Activity p0, final OnUserEarnedRewardListener p1);
}

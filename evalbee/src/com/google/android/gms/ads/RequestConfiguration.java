// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import com.google.android.gms.internal.ads.zzcbn;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RequestConfiguration
{
    public static final String MAX_AD_CONTENT_RATING_G = "G";
    public static final String MAX_AD_CONTENT_RATING_MA = "MA";
    public static final String MAX_AD_CONTENT_RATING_PG = "PG";
    public static final String MAX_AD_CONTENT_RATING_T = "T";
    public static final String MAX_AD_CONTENT_RATING_UNSPECIFIED = "";
    public static final int TAG_FOR_CHILD_DIRECTED_TREATMENT_FALSE = 0;
    public static final int TAG_FOR_CHILD_DIRECTED_TREATMENT_TRUE = 1;
    public static final int TAG_FOR_CHILD_DIRECTED_TREATMENT_UNSPECIFIED = -1;
    public static final int TAG_FOR_UNDER_AGE_OF_CONSENT_FALSE = 0;
    public static final int TAG_FOR_UNDER_AGE_OF_CONSENT_TRUE = 1;
    public static final int TAG_FOR_UNDER_AGE_OF_CONSENT_UNSPECIFIED = -1;
    public static final List zza;
    private final int zzb = zzb;
    private final int zzc = zzc;
    private final String zzd = zzd;
    private final List zze = zze;
    private final PublisherPrivacyPersonalizationState zzf = zzf;
    
    static {
        zza = Arrays.asList("MA", "T", "PG", "G");
    }
    
    public String getMaxAdContentRating() {
        String zzd;
        if ((zzd = this.zzd) == null) {
            zzd = "";
        }
        return zzd;
    }
    
    public PublisherPrivacyPersonalizationState getPublisherPrivacyPersonalizationState() {
        return this.zzf;
    }
    
    public int getTagForChildDirectedTreatment() {
        return this.zzb;
    }
    
    public int getTagForUnderAgeOfConsent() {
        return this.zzc;
    }
    
    public List<String> getTestDeviceIds() {
        return new ArrayList<String>(this.zze);
    }
    
    public Builder toBuilder() {
        final Builder builder = new Builder();
        builder.setTagForChildDirectedTreatment(this.zzb);
        builder.setTagForUnderAgeOfConsent(this.zzc);
        builder.setMaxAdContentRating(this.zzd);
        builder.setTestDeviceIds(this.zze);
        return builder;
    }
    
    public static class Builder
    {
        private int zza;
        private int zzb;
        private String zzc;
        private final List zzd;
        private PublisherPrivacyPersonalizationState zze;
        
        public Builder() {
            this.zza = -1;
            this.zzb = -1;
            this.zzc = null;
            this.zzd = new ArrayList();
            this.zze = PublisherPrivacyPersonalizationState.DEFAULT;
        }
        
        public RequestConfiguration build() {
            return new RequestConfiguration(this.zza, this.zzb, this.zzc, this.zzd, this.zze, null);
        }
        
        public Builder setMaxAdContentRating(final String s) {
            String zzc;
            if (s != null && !"".equals(s)) {
                zzc = s;
                if (!"G".equals(s)) {
                    zzc = s;
                    if (!"PG".equals(s)) {
                        zzc = s;
                        if (!"T".equals(s)) {
                            if (!"MA".equals(s)) {
                                zzcbn.zzj("Invalid value passed to setMaxAdContentRating: ".concat(s));
                                return this;
                            }
                            zzc = s;
                        }
                    }
                }
            }
            else {
                zzc = null;
            }
            this.zzc = zzc;
            return this;
        }
        
        public Builder setPublisherPrivacyPersonalizationState(final PublisherPrivacyPersonalizationState zze) {
            this.zze = zze;
            return this;
        }
        
        public Builder setTagForChildDirectedTreatment(final int n) {
            if (n != -1 && n != 0 && n != 1) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid value passed to setTagForChildDirectedTreatment: ");
                sb.append(n);
                zzcbn.zzj(sb.toString());
            }
            else {
                this.zza = n;
            }
            return this;
        }
        
        public Builder setTagForUnderAgeOfConsent(final int n) {
            if (n != -1 && n != 0 && n != 1) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid value passed to setTagForUnderAgeOfConsent: ");
                sb.append(n);
                zzcbn.zzj(sb.toString());
            }
            else {
                this.zzb = n;
            }
            return this;
        }
        
        public Builder setTestDeviceIds(final List<String> list) {
            this.zzd.clear();
            if (list != null) {
                this.zzd.addAll(list);
            }
            return this;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface MaxAdContentRating {
    }
    
    public enum PublisherPrivacyPersonalizationState
    {
        DEFAULT("DEFAULT", 0, 0), 
        DISABLED("DISABLED", 2, 2), 
        ENABLED("ENABLED", 1, 1);
        
        private static final PublisherPrivacyPersonalizationState[] zza;
        private final int zzb;
        
        private PublisherPrivacyPersonalizationState(final String name, final int ordinal, final int zzb) {
            this.zzb = zzb;
        }
        
        public int getValue() {
            return this.zzb;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface TagForChildDirectedTreatment {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface TagForUnderAgeOfConsent {
    }
}

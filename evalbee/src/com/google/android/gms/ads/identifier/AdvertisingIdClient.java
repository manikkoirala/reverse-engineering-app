// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.identifier;

import com.google.android.gms.common.annotation.KeepForSdkWithMembers;
import java.util.Map;
import java.util.HashMap;
import android.content.pm.PackageManager$NameNotFoundException;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.internal.ads_identifier.zze;
import java.util.concurrent.TimeUnit;
import android.content.Intent;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import android.content.ServiceConnection;
import com.google.android.gms.common.stats.ConnectionTracker;
import com.google.android.gms.common.internal.ShowFirstParty;
import android.os.RemoteException;
import android.util.Log;
import java.io.IOException;
import android.os.SystemClock;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.common.internal.Preconditions;
import android.content.Context;
import com.google.android.gms.internal.ads_identifier.zzf;
import com.google.android.gms.common.BlockingServiceConnection;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public class AdvertisingIdClient
{
    BlockingServiceConnection zza;
    zzf zzb;
    boolean zzc;
    final Object zzd;
    zzb zze;
    final long zzf;
    private final Context zzg;
    
    @KeepForSdk
    public AdvertisingIdClient(final Context context) {
        this(context, 30000L, false, false);
    }
    
    @VisibleForTesting
    public AdvertisingIdClient(final Context context, final long zzf, final boolean b, final boolean b2) {
        this.zzd = new Object();
        Preconditions.checkNotNull(context);
        Context zzg = context;
        if (b) {
            final Context applicationContext = context.getApplicationContext();
            zzg = context;
            if (applicationContext != null) {
                zzg = applicationContext;
            }
        }
        this.zzg = zzg;
        this.zzc = false;
        this.zzf = zzf;
    }
    
    @KeepForSdk
    public static Info getAdvertisingIdInfo(Context context) {
        context = (Context)new AdvertisingIdClient(context, -1L, true, false);
        try {
            final long elapsedRealtime = SystemClock.elapsedRealtime();
            ((AdvertisingIdClient)context).zzb(false);
            final Info zzd = ((AdvertisingIdClient)context).zzd(-1);
            ((AdvertisingIdClient)context).zzc(zzd, true, 0.0f, SystemClock.elapsedRealtime() - elapsedRealtime, "", null);
            ((AdvertisingIdClient)context).zza();
            return zzd;
        }
        finally {
            try {
                final Throwable t;
                ((AdvertisingIdClient)context).zzc(null, true, 0.0f, -1L, "", t);
            }
            finally {
                ((AdvertisingIdClient)context).zza();
            }
        }
    }
    
    @KeepForSdk
    public static boolean getIsAdIdFakeForDebugLogging(Context context) {
        context = (Context)new AdvertisingIdClient(context, -1L, false, false);
        try {
            ((AdvertisingIdClient)context).zzb(false);
            Preconditions.checkNotMainThread("Calling this from your main thread can lead to deadlock");
            synchronized (context) {
                Label_0116: {
                    if (!((AdvertisingIdClient)context).zzc) {
                        Object zzd = ((AdvertisingIdClient)context).zzd;
                        synchronized (zzd) {
                            final zzb zze = ((AdvertisingIdClient)context).zze;
                            if (zze != null && zze.zzb) {
                                monitorexit(zzd);
                                try {
                                    ((AdvertisingIdClient)context).zzb(false);
                                    if (((AdvertisingIdClient)context).zzc) {
                                        break Label_0116;
                                    }
                                    zzd = new IOException("AdvertisingIdClient cannot reconnect.");
                                    throw zzd;
                                }
                                catch (final Exception cause) {
                                    zzd = new IOException("AdvertisingIdClient cannot reconnect.", cause);
                                    throw zzd;
                                }
                            }
                            throw new IOException("AdvertisingIdClient is not connected.");
                        }
                    }
                }
                Preconditions.checkNotNull(((AdvertisingIdClient)context).zza);
                Preconditions.checkNotNull(((AdvertisingIdClient)context).zzb);
                try {
                    final boolean zzd2 = ((AdvertisingIdClient)context).zzb.zzd();
                    monitorexit(context);
                    ((AdvertisingIdClient)context).zze();
                    return zzd2;
                }
                catch (final RemoteException ex) {
                    Log.i("AdvertisingIdClient", "GMS remote exception ", (Throwable)ex);
                    throw new IOException("Remote exception");
                }
            }
        }
        finally {
            ((AdvertisingIdClient)context).zza();
        }
    }
    
    @KeepForSdk
    @ShowFirstParty
    public static void setShouldSkipGmsCoreVersionCheck(final boolean b) {
    }
    
    private final Info zzd(final int n) {
        Preconditions.checkNotMainThread("Calling this from your main thread can lead to deadlock");
        synchronized (this) {
            Label_0097: {
                if (!this.zzc) {
                    Object zzd = this.zzd;
                    synchronized (zzd) {
                        final zzb zze = this.zze;
                        if (zze != null && zze.zzb) {
                            monitorexit(zzd);
                            try {
                                this.zzb(false);
                                if (this.zzc) {
                                    break Label_0097;
                                }
                                zzd = new IOException("AdvertisingIdClient cannot reconnect.");
                                throw zzd;
                            }
                            catch (final Exception cause) {
                                zzd = new IOException("AdvertisingIdClient cannot reconnect.", cause);
                                throw zzd;
                            }
                        }
                        throw new IOException("AdvertisingIdClient is not connected.");
                    }
                }
            }
            Preconditions.checkNotNull(this.zza);
            Preconditions.checkNotNull(this.zzb);
            try {
                final Info info = new Info(this.zzb.zzc(), this.zzb.zze(true));
                monitorexit(this);
                this.zze();
                return info;
            }
            catch (final RemoteException ex) {
                Log.i("AdvertisingIdClient", "GMS remote exception ", (Throwable)ex);
                throw new IOException("Remote exception");
            }
        }
    }
    
    private final void zze() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/google/android/gms/ads/identifier/AdvertisingIdClient.zzd:Ljava/lang/Object;
        //     4: astore_3       
        //     5: aload_3        
        //     6: monitorenter   
        //     7: aload_0        
        //     8: getfield        com/google/android/gms/ads/identifier/AdvertisingIdClient.zze:Lcom/google/android/gms/ads/identifier/zzb;
        //    11: astore          4
        //    13: aload           4
        //    15: ifnull          33
        //    18: aload           4
        //    20: getfield        com/google/android/gms/ads/identifier/zzb.zza:Ljava/util/concurrent/CountDownLatch;
        //    23: invokevirtual   java/util/concurrent/CountDownLatch.countDown:()V
        //    26: aload_0        
        //    27: getfield        com/google/android/gms/ads/identifier/AdvertisingIdClient.zze:Lcom/google/android/gms/ads/identifier/zzb;
        //    30: invokevirtual   java/lang/Thread.join:()V
        //    33: aload_0        
        //    34: getfield        com/google/android/gms/ads/identifier/AdvertisingIdClient.zzf:J
        //    37: lstore_1       
        //    38: lload_1        
        //    39: lconst_0       
        //    40: lcmp           
        //    41: ifle            62
        //    44: new             Lcom/google/android/gms/ads/identifier/zzb;
        //    47: astore          4
        //    49: aload           4
        //    51: aload_0        
        //    52: lload_1        
        //    53: invokespecial   com/google/android/gms/ads/identifier/zzb.<init>:(Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;J)V
        //    56: aload_0        
        //    57: aload           4
        //    59: putfield        com/google/android/gms/ads/identifier/AdvertisingIdClient.zze:Lcom/google/android/gms/ads/identifier/zzb;
        //    62: aload_3        
        //    63: monitorexit    
        //    64: return         
        //    65: astore          4
        //    67: aload_3        
        //    68: monitorexit    
        //    69: aload           4
        //    71: athrow         
        //    72: astore          4
        //    74: goto            33
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                            
        //  -----  -----  -----  -----  --------------------------------
        //  7      13     65     72     Any
        //  18     26     65     72     Any
        //  26     33     72     77     Ljava/lang/InterruptedException;
        //  26     33     65     72     Any
        //  33     38     65     72     Any
        //  44     62     65     72     Any
        //  62     64     65     72     Any
        //  67     69     65     72     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0033:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public final void finalize() {
        this.zza();
        super.finalize();
    }
    
    @KeepForSdk
    public Info getInfo() {
        return this.zzd(-1);
    }
    
    @KeepForSdk
    public void start() {
        this.zzb(true);
    }
    
    public final void zza() {
        Preconditions.checkNotMainThread("Calling this from your main thread can lead to deadlock");
        synchronized (this) {
            if (this.zzg != null && this.zza != null) {
                try {
                    if (this.zzc) {
                        ConnectionTracker.getInstance().unbindService(this.zzg, (ServiceConnection)this.zza);
                    }
                }
                finally {
                    final Throwable t;
                    Log.i("AdvertisingIdClient", "AdvertisingIdClient unbindService failed.", t);
                }
                this.zzc = false;
                this.zzb = null;
                this.zza = null;
            }
        }
    }
    
    @VisibleForTesting
    public final void zzb(final boolean b) {
        Preconditions.checkNotMainThread("Calling this from your main thread can lead to deadlock");
        synchronized (this) {
            if (this.zzc) {
                this.zza();
            }
            final Context zzg = this.zzg;
            try {
                zzg.getPackageManager().getPackageInfo("com.android.vending", 0);
                final int googlePlayServicesAvailable = GoogleApiAvailabilityLight.getInstance().isGooglePlayServicesAvailable(zzg, 12451000);
                if (googlePlayServicesAvailable != 0 && googlePlayServicesAvailable != 2) {
                    throw new IOException("Google Play services not available");
                }
                final BlockingServiceConnection zza = new BlockingServiceConnection();
                final Intent intent = new Intent("com.google.android.gms.ads.identifier.service.START");
                intent.setPackage("com.google.android.gms");
                try {
                    if (ConnectionTracker.getInstance().bindService(zzg, intent, (ServiceConnection)zza, 1)) {
                        this.zza = zza;
                        try {
                            this.zzb = com.google.android.gms.internal.ads_identifier.zze.zza(zza.getServiceWithTimeout(10000L, TimeUnit.MILLISECONDS));
                            this.zzc = true;
                            if (b) {
                                this.zze();
                            }
                            return;
                        }
                        catch (final InterruptedException ex) {
                            final IOException ex2 = new IOException("Interrupted exception");
                        }
                    }
                    throw new IOException("Connection failure");
                }
                finally {
                    final Throwable cause;
                    throw new IOException(cause);
                }
            }
            catch (final PackageManager$NameNotFoundException ex3) {
                throw new GooglePlayServicesNotAvailableException(9);
            }
        }
    }
    
    @VisibleForTesting
    public final boolean zzc(final Info info, final boolean b, final float n, final long i, String s, final Throwable t) {
        if (Math.random() <= 0.0) {
            final HashMap hashMap = new HashMap();
            s = "1";
            hashMap.put("app_context", "1");
            if (info != null) {
                if (!info.isLimitAdTrackingEnabled()) {
                    s = "0";
                }
                hashMap.put("limit_ad_tracking", s);
                final String id = info.getId();
                if (id != null) {
                    hashMap.put("ad_id_size", Integer.toString(id.length()));
                }
            }
            if (t != null) {
                hashMap.put("error", t.getClass().getName());
            }
            hashMap.put("tag", "AdvertisingIdClient");
            hashMap.put("time_spent", Long.toString(i));
            new zza(this, hashMap).start();
            return true;
        }
        return false;
    }
    
    @KeepForSdkWithMembers
    public static final class Info
    {
        private final String zza;
        private final boolean zzb;
        
        @Deprecated
        public Info(final String zza, final boolean zzb) {
            this.zza = zza;
            this.zzb = zzb;
        }
        
        public String getId() {
            return this.zza;
        }
        
        public boolean isLimitAdTrackingEnabled() {
            return this.zzb;
        }
        
        @Override
        public String toString() {
            final String zza = this.zza;
            final boolean zzb = this.zzb;
            final StringBuilder sb = new StringBuilder(String.valueOf(zza).length() + 7);
            sb.append("{");
            sb.append(zza);
            sb.append("}");
            sb.append(zzb);
            return sb.toString();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.ads.impl;

public final class R
{
    private R() {
    }
    
    public static final class drawable
    {
        public static final int admob_close_button_black_circle_white_cross = 2131230839;
        public static final int admob_close_button_white_circle_black_cross = 2131230840;
        
        private drawable() {
        }
    }
    
    public static final class string
    {
        public static final int native_body = 2131886650;
        public static final int native_headline = 2131886651;
        public static final int native_media_view = 2131886652;
        public static final int notifications_permission_confirm = 2131886668;
        public static final int notifications_permission_decline = 2131886669;
        public static final int notifications_permission_title = 2131886670;
        public static final int offline_notification_text = 2131886672;
        public static final int offline_notification_title = 2131886673;
        public static final int offline_opt_in_confirm = 2131886674;
        public static final int offline_opt_in_confirmation = 2131886675;
        public static final int offline_opt_in_decline = 2131886676;
        public static final int offline_opt_in_message = 2131886677;
        public static final int offline_opt_in_title = 2131886678;
        public static final int s1 = 2131886752;
        public static final int s2 = 2131886753;
        public static final int s3 = 2131886754;
        public static final int s4 = 2131886755;
        public static final int s5 = 2131886756;
        public static final int s6 = 2131886757;
        public static final int s7 = 2131886758;
        public static final int watermark_label_prefix = 2131886899;
        
        private string() {
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.actions;

public class SearchIntents
{
    public static final String ACTION_SEARCH = "com.google.android.gms.actions.SEARCH_ACTION";
    public static final String EXTRA_QUERY = "query";
    
    private SearchIntents() {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.base;

public final class R
{
    private R() {
    }
    
    public static final class attr
    {
        public static final int buttonSize = 2130968731;
        public static final int circleCrop = 2130968792;
        public static final int colorScheme = 2130968866;
        public static final int imageAspectRatio = 2130969150;
        public static final int imageAspectRatioAdjust = 2130969151;
        public static final int scopeUris = 2130969535;
        
        private attr() {
        }
    }
    
    public static final class color
    {
        public static final int common_google_signin_btn_text_dark = 2131099741;
        public static final int common_google_signin_btn_text_dark_default = 2131099742;
        public static final int common_google_signin_btn_text_dark_disabled = 2131099743;
        public static final int common_google_signin_btn_text_dark_focused = 2131099744;
        public static final int common_google_signin_btn_text_dark_pressed = 2131099745;
        public static final int common_google_signin_btn_text_light = 2131099746;
        public static final int common_google_signin_btn_text_light_default = 2131099747;
        public static final int common_google_signin_btn_text_light_disabled = 2131099748;
        public static final int common_google_signin_btn_text_light_focused = 2131099749;
        public static final int common_google_signin_btn_text_light_pressed = 2131099750;
        public static final int common_google_signin_btn_tint = 2131099751;
        
        private color() {
        }
    }
    
    public static final class drawable
    {
        public static final int common_full_open_on_phone = 2131230875;
        public static final int common_google_signin_btn_icon_dark = 2131230876;
        public static final int common_google_signin_btn_icon_dark_focused = 2131230877;
        public static final int common_google_signin_btn_icon_dark_normal = 2131230878;
        public static final int common_google_signin_btn_icon_dark_normal_background = 2131230879;
        public static final int common_google_signin_btn_icon_disabled = 2131230880;
        public static final int common_google_signin_btn_icon_light = 2131230881;
        public static final int common_google_signin_btn_icon_light_focused = 2131230882;
        public static final int common_google_signin_btn_icon_light_normal = 2131230883;
        public static final int common_google_signin_btn_icon_light_normal_background = 2131230884;
        public static final int common_google_signin_btn_text_dark = 2131230885;
        public static final int common_google_signin_btn_text_dark_focused = 2131230886;
        public static final int common_google_signin_btn_text_dark_normal = 2131230887;
        public static final int common_google_signin_btn_text_dark_normal_background = 2131230888;
        public static final int common_google_signin_btn_text_disabled = 2131230889;
        public static final int common_google_signin_btn_text_light = 2131230890;
        public static final int common_google_signin_btn_text_light_focused = 2131230891;
        public static final int common_google_signin_btn_text_light_normal = 2131230892;
        public static final int common_google_signin_btn_text_light_normal_background = 2131230893;
        public static final int googleg_disabled_color_18 = 2131230901;
        public static final int googleg_standard_color_18 = 2131230902;
        
        private drawable() {
        }
    }
    
    public static final class id
    {
        public static final int adjust_height = 2131296349;
        public static final int adjust_width = 2131296350;
        public static final int auto = 2131296367;
        public static final int dark = 2131296542;
        public static final int icon_only = 2131296645;
        public static final int light = 2131296823;
        public static final int none = 2131296928;
        public static final int standard = 2131297120;
        public static final int wide = 2131297411;
        
        private id() {
        }
    }
    
    public static final class string
    {
        public static final int common_google_play_services_enable_button = 2131886181;
        public static final int common_google_play_services_enable_text = 2131886182;
        public static final int common_google_play_services_enable_title = 2131886183;
        public static final int common_google_play_services_install_button = 2131886184;
        public static final int common_google_play_services_install_text = 2131886185;
        public static final int common_google_play_services_install_title = 2131886186;
        public static final int common_google_play_services_notification_channel_name = 2131886187;
        public static final int common_google_play_services_notification_ticker = 2131886188;
        public static final int common_google_play_services_unsupported_text = 2131886190;
        public static final int common_google_play_services_update_button = 2131886191;
        public static final int common_google_play_services_update_text = 2131886192;
        public static final int common_google_play_services_update_title = 2131886193;
        public static final int common_google_play_services_updating_text = 2131886194;
        public static final int common_google_play_services_wear_update_text = 2131886195;
        public static final int common_open_on_phone = 2131886196;
        public static final int common_signin_button_text = 2131886197;
        public static final int common_signin_button_text_long = 2131886198;
        
        private string() {
        }
    }
    
    public static final class styleable
    {
        public static final int[] LoadingImageView;
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
        public static final int[] SignInButton;
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;
        
        static {
            LoadingImageView = new int[] { 2130968792, 2130969150, 2130969151 };
            SignInButton = new int[] { 2130968731, 2130968866, 2130969535 };
        }
        
        private styleable() {
        }
    }
}

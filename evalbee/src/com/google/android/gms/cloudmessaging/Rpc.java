// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.cloudmessaging;

import android.os.BaseBundle;
import com.google.android.gms.tasks.SuccessContinuation;
import com.google.android.gms.tasks.Continuation;
import java.io.IOException;
import com.google.android.gms.internal.cloudmessaging.zza;
import com.google.android.gms.tasks.OnCompleteListener;
import java.util.concurrent.ScheduledFuture;
import android.os.RemoteException;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.regex.Matcher;
import android.os.Parcelable;
import android.util.Log;
import android.content.Intent;
import android.os.Message;
import com.google.android.gms.tasks.Tasks;
import com.google.android.gms.tasks.Task;
import android.os.Bundle;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import android.os.Handler;
import android.os.Looper;
import android.os.Messenger;
import java.util.concurrent.ScheduledExecutorService;
import android.content.Context;
import java.util.regex.Pattern;
import java.util.concurrent.Executor;
import android.app.PendingIntent;

public class Rpc
{
    private static int zza;
    private static PendingIntent zzb;
    private static final Executor zzc;
    private static final Pattern zzd;
    private final co1 zze;
    private final Context zzf;
    private final zzv zzg;
    private final ScheduledExecutorService zzh;
    private final Messenger zzi;
    private Messenger zzj;
    private zze zzk;
    
    static {
        zzc = zzy.zza;
        zzd = Pattern.compile("\\|ID\\|([^|]+)\\|:?+(.*)");
    }
    
    public Rpc(final Context zzf) {
        this.zze = new co1();
        this.zzf = zzf;
        this.zzg = new zzv(zzf);
        this.zzi = new Messenger((Handler)new zzad(this, Looper.getMainLooper()));
        final ScheduledThreadPoolExecutor zzh = new ScheduledThreadPoolExecutor(1);
        zzh.setKeepAliveTime(60L, TimeUnit.SECONDS);
        zzh.allowCoreThreadTimeOut(true);
        this.zzh = zzh;
    }
    
    private final Task zze(final Bundle bundle) {
        final String zzf = zzf();
        final TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        Object action = this.zze;
        synchronized (action) {
            this.zze.put(zzf, taskCompletionSource);
            monitorexit(action);
            final Intent obj = new Intent();
            obj.setPackage("com.google.android.gms");
            if (this.zzg.zzb() == 2) {
                action = "com.google.iid.TOKEN_REQUEST";
            }
            else {
                action = "com.google.android.c2dm.intent.REGISTER";
            }
            obj.setAction((String)action);
            obj.putExtras(bundle);
            zzg(this.zzf, obj);
            final StringBuilder sb = new StringBuilder();
            sb.append("|ID|");
            sb.append(zzf);
            sb.append("|");
            obj.putExtra("kid", sb.toString());
            if (Log.isLoggable("Rpc", 3)) {
                Log.d("Rpc", "Sending ".concat(String.valueOf(obj.getExtras())));
            }
            obj.putExtra("google.messenger", (Parcelable)this.zzi);
            Label_0284: {
                if (this.zzj != null || this.zzk != null) {
                    action = Message.obtain();
                    ((Message)action).obj = obj;
                    try {
                        final Messenger zzj = this.zzj;
                        if (zzj != null) {
                            zzj.send((Message)action);
                            break Label_0284;
                        }
                        this.zzk.zzb((Message)action);
                        break Label_0284;
                    }
                    catch (final RemoteException ex) {
                        if (Log.isLoggable("Rpc", 3)) {
                            Log.d("Rpc", "Messenger failed, fallback to startService");
                        }
                    }
                }
                if (this.zzg.zzb() == 2) {
                    this.zzf.sendBroadcast(obj);
                }
                else {
                    this.zzf.startService(obj);
                }
            }
            taskCompletionSource.getTask().addOnCompleteListener(Rpc.zzc, (OnCompleteListener)new zzac(this, zzf, this.zzh.schedule(new zzab(taskCompletionSource), 30L, TimeUnit.SECONDS)));
            return taskCompletionSource.getTask();
        }
    }
    
    private static String zzf() {
        synchronized (Rpc.class) {
            final int zza = Rpc.zza;
            Rpc.zza = zza + 1;
            return Integer.toString(zza);
        }
    }
    
    private static void zzg(final Context context, final Intent intent) {
        synchronized (Rpc.class) {
            if (Rpc.zzb == null) {
                final Intent intent2 = new Intent();
                intent2.setPackage("com.google.example.invalidpackage");
                Rpc.zzb = PendingIntent.getBroadcast(context, 0, intent2, com.google.android.gms.internal.cloudmessaging.zza.zza);
            }
            intent.putExtra("app", (Parcelable)Rpc.zzb);
        }
    }
    
    private final void zzh(final String str, final Bundle result) {
        synchronized (this.zze) {
            final TaskCompletionSource taskCompletionSource = (TaskCompletionSource)this.zze.remove(str);
            if (taskCompletionSource == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Missing callback for ");
                sb.append(str);
                Log.w("Rpc", sb.toString());
                return;
            }
            taskCompletionSource.setResult((Object)result);
        }
    }
    
    private static boolean zzi(final Bundle bundle) {
        return bundle != null && ((BaseBundle)bundle).containsKey("google.messenger");
    }
    
    public Task<Void> messageHandled(final CloudMessage cloudMessage) {
        if (this.zzg.zza() >= 233700000) {
            final Bundle bundle = new Bundle();
            ((BaseBundle)bundle).putString("google.message_id", cloudMessage.getMessageId());
            final Integer zza = cloudMessage.zza();
            if (zza != null) {
                ((BaseBundle)bundle).putInt("google.product_id", (int)zza);
            }
            return (Task<Void>)zzu.zzb(this.zzf).zzc(3, bundle);
        }
        return (Task<Void>)Tasks.forException((Exception)new IOException("SERVICE_NOT_AVAILABLE"));
    }
    
    public Task<Bundle> send(final Bundle bundle) {
        if (this.zzg.zza() < 12000000) {
            Task task;
            if (this.zzg.zzb() != 0) {
                task = this.zze(bundle).continueWithTask(Rpc.zzc, (Continuation)new zzz(this, bundle));
            }
            else {
                task = Tasks.forException((Exception)new IOException("MISSING_INSTANCEID_SERVICE"));
            }
            return (Task<Bundle>)task;
        }
        return (Task<Bundle>)zzu.zzb(this.zzf).zzd(1, bundle).continueWith(Rpc.zzc, (Continuation)zzaa.zza);
    }
}

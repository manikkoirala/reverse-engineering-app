// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.cloudmessaging;

import java.util.List;
import android.content.pm.PackageManager;
import android.content.Intent;
import com.google.android.gms.common.util.PlatformVersion;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager$NameNotFoundException;
import android.util.Log;
import com.google.android.gms.common.wrappers.Wrappers;
import android.content.Context;

public final class zzv
{
    private final Context zza;
    private int zzb;
    private int zzc;
    
    public zzv(final Context zza) {
        this.zzc = 0;
        this.zza = zza;
    }
    
    public final int zza() {
        synchronized (this) {
            if (this.zzb == 0) {
                PackageInfo packageInfo;
                try {
                    packageInfo = Wrappers.packageManager(this.zza).getPackageInfo("com.google.android.gms", 0);
                }
                catch (final PackageManager$NameNotFoundException ex) {
                    Log.w("Metadata", "Failed to find package ".concat(ex.toString()));
                    packageInfo = null;
                }
                if (packageInfo != null) {
                    this.zzb = packageInfo.versionCode;
                }
            }
            return this.zzb;
        }
    }
    
    public final int zzb() {
        synchronized (this) {
            final int zzc = this.zzc;
            if (zzc != 0) {
                return zzc;
            }
            final Context zza = this.zza;
            final PackageManager packageManager = zza.getPackageManager();
            if (Wrappers.packageManager(zza).checkPermission("com.google.android.c2dm.permission.SEND", "com.google.android.gms") == -1) {
                Log.e("Metadata", "Google Play services missing or without correct permission.");
                return 0;
            }
            final boolean atLeastO = PlatformVersion.isAtLeastO();
            int zzc2 = 1;
            final int n = 1;
            if (!atLeastO) {
                final Intent intent = new Intent("com.google.android.c2dm.intent.REGISTER");
                intent.setPackage("com.google.android.gms");
                final List queryIntentServices = packageManager.queryIntentServices(intent, 0);
                if (queryIntentServices != null && !queryIntentServices.isEmpty()) {
                    final int n2 = n;
                    return this.zzc = n2;
                }
            }
            final Intent intent2 = new Intent("com.google.iid.TOKEN_REQUEST");
            intent2.setPackage("com.google.android.gms");
            final List queryBroadcastReceivers = packageManager.queryBroadcastReceivers(intent2, 0);
            if (queryBroadcastReceivers == null || queryBroadcastReceivers.isEmpty()) {
                Log.w("Metadata", "Failed to resolve IID implementation package, falling back");
                if (PlatformVersion.isAtLeastO()) {
                    zzc2 = 2;
                }
                return this.zzc = zzc2;
            }
            final int n2 = 2;
            return this.zzc = n2;
        }
    }
}

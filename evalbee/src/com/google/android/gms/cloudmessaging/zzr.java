// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.cloudmessaging;

import android.util.Log;
import android.os.Bundle;
import com.google.android.gms.tasks.TaskCompletionSource;

abstract class zzr
{
    final int zza;
    final TaskCompletionSource zzb;
    final int zzc;
    final Bundle zzd;
    
    public zzr(final int zza, final int zzc, final Bundle zzd) {
        this.zzb = new TaskCompletionSource();
        this.zza = zza;
        this.zzc = zzc;
        this.zzd = zzd;
    }
    
    @Override
    public final String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Request { what=");
        sb.append(this.zzc);
        sb.append(" id=");
        sb.append(this.zza);
        sb.append(" oneWay=");
        sb.append(this.zzb());
        sb.append("}");
        return sb.toString();
    }
    
    public abstract void zza(final Bundle p0);
    
    public abstract boolean zzb();
    
    public final void zzc(final zzs exception) {
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            final String string = this.toString();
            final String string2 = exception.toString();
            final StringBuilder sb = new StringBuilder();
            sb.append("Failing ");
            sb.append(string);
            sb.append(" with ");
            sb.append(string2);
            Log.d("MessengerIpcClient", sb.toString());
        }
        this.zzb.setException((Exception)exception);
    }
    
    public final void zzd(final Object o) {
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            final String string = this.toString();
            final String value = String.valueOf(o);
            final StringBuilder sb = new StringBuilder();
            sb.append("Finishing ");
            sb.append(string);
            sb.append(" with ");
            sb.append(value);
            Log.d("MessengerIpcClient", sb.toString());
        }
        this.zzb.setResult(o);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.cloudmessaging;

import android.os.BaseBundle;
import android.os.Parcelable;
import android.content.BroadcastReceiver$PendingResult;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.Executors;
import com.google.android.gms.common.util.concurrent.NamedThreadFactory;
import com.google.android.gms.internal.cloudmessaging.zze;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executor;
import android.app.PendingIntent$CanceledException;
import android.app.PendingIntent;
import com.google.android.gms.tasks.Task;
import android.util.Log;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.TimeUnit;
import android.os.Bundle;
import com.google.android.gms.tasks.Tasks;
import android.text.TextUtils;
import android.content.Intent;
import android.content.Context;
import java.lang.ref.SoftReference;
import android.content.BroadcastReceiver;

public abstract class CloudMessagingReceiver extends BroadcastReceiver
{
    private static SoftReference zza;
    
    private final int zzb(final Context ex, final Intent intent) {
        if (intent.getExtras() == null) {
            return 500;
        }
        final CloudMessage cloudMessage = new CloudMessage(intent);
        Task task;
        if (TextUtils.isEmpty((CharSequence)cloudMessage.getMessageId())) {
            task = Tasks.forResult((Object)null);
        }
        else {
            final Bundle bundle = new Bundle();
            ((BaseBundle)bundle).putString("google.message_id", cloudMessage.getMessageId());
            final Integer zza = cloudMessage.zza();
            if (zza != null) {
                ((BaseBundle)bundle).putInt("google.product_id", (int)zza);
            }
            ((BaseBundle)bundle).putBoolean("supports_message_handled", true);
            task = zzu.zzb((Context)ex).zzc(2, bundle);
        }
        final int onMessageReceive = this.onMessageReceive((Context)ex, cloudMessage);
        try {
            Tasks.await(task, TimeUnit.SECONDS.toMillis(1L), TimeUnit.MILLISECONDS);
            return onMessageReceive;
        }
        catch (final TimeoutException ex) {}
        catch (final InterruptedException ex) {}
        catch (final ExecutionException ex2) {}
        Log.w("CloudMessagingReceiver", "Message ack failed: ".concat(ex.toString()));
        return onMessageReceive;
    }
    
    private final int zzc(final Context context, final Intent intent) {
        final PendingIntent pendingIntent = (PendingIntent)intent.getParcelableExtra("pending_intent");
        if (pendingIntent != null) {
            try {
                pendingIntent.send();
            }
            catch (final PendingIntent$CanceledException ex) {
                Log.e("CloudMessagingReceiver", "Notification pending intent canceled");
            }
        }
        Bundle extras = intent.getExtras();
        if (extras != null) {
            extras.remove("pending_intent");
        }
        else {
            extras = new Bundle();
        }
        final String action = intent.getAction();
        if (action != "com.google.firebase.messaging.NOTIFICATION_DISMISS" && (action == null || !action.equals("com.google.firebase.messaging.NOTIFICATION_DISMISS"))) {
            Log.e("CloudMessagingReceiver", "Unknown notification action");
            return 500;
        }
        this.onNotificationDismissed(context, extras);
        return -1;
    }
    
    public Executor getBroadcastExecutor() {
        synchronized (CloudMessagingReceiver.class) {
            final SoftReference zza = CloudMessagingReceiver.zza;
            ExecutorService executorService;
            if (zza != null) {
                executorService = zza.get();
            }
            else {
                executorService = null;
            }
            ExecutorService unconfigurableExecutorService = executorService;
            if (executorService == null) {
                zze.zza();
                unconfigurableExecutorService = Executors.unconfigurableExecutorService(Executors.newCachedThreadPool(new NamedThreadFactory("firebase-iid-executor")));
                CloudMessagingReceiver.zza = new SoftReference(unconfigurableExecutorService);
            }
            return unconfigurableExecutorService;
        }
    }
    
    public abstract int onMessageReceive(final Context p0, final CloudMessage p1);
    
    public void onNotificationDismissed(final Context context, final Bundle bundle) {
    }
    
    public final void onReceive(final Context context, final Intent intent) {
        if (intent == null) {
            return;
        }
        this.getBroadcastExecutor().execute(new zzf(this, intent, context, this.isOrderedBroadcast(), this.goAsync()));
    }
    
    public static final class IntentActionKeys
    {
        public static final String NOTIFICATION_DISMISS = "com.google.firebase.messaging.NOTIFICATION_DISMISS";
        public static final String NOTIFICATION_OPEN = "com.google.firebase.messaging.NOTIFICATION_OPEN";
        
        private IntentActionKeys() {
        }
    }
    
    public static final class IntentKeys
    {
        public static final String PENDING_INTENT = "pending_intent";
        public static final String WRAPPED_INTENT = "wrapped_intent";
        
        private IntentKeys() {
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.cloudmessaging;

import android.os.BaseBundle;
import android.os.Bundle;

final class zzq extends zzr
{
    public zzq(final int n, final int n2, final Bundle bundle) {
        super(n, n2, bundle);
    }
    
    @Override
    public final void zza(final Bundle bundle) {
        if (((BaseBundle)bundle).getBoolean("ack", false)) {
            this.zzd(null);
            return;
        }
        this.zzc(new zzs(4, "Invalid response to one way request", null));
    }
    
    @Override
    public final boolean zzb() {
        return true;
    }
}

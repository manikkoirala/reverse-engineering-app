// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.cloudmessaging;

import android.os.Parcel;
import android.os.IBinder;
import android.os.Binder;
import android.os.Message;
import android.os.IInterface;

interface IMessengerCompat extends IInterface
{
    public static final String DESCRIPTOR = "com.google.android.gms.iid.IMessengerCompat";
    public static final int TRANSACTION_SEND = 1;
    
    void send(final Message p0);
    
    public static class Impl extends Binder implements IMessengerCompat
    {
        public IBinder asBinder() {
            throw null;
        }
        
        public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) {
            throw null;
        }
        
        public void send(final Message message) {
            throw null;
        }
    }
    
    public static class Proxy implements IMessengerCompat
    {
        private final IBinder zza;
        
        public Proxy(final IBinder zza) {
            this.zza = zza;
        }
        
        public IBinder asBinder() {
            return this.zza;
        }
        
        @Override
        public void send(final Message message) {
            final Parcel obtain = Parcel.obtain();
            obtain.writeInterfaceToken("com.google.android.gms.iid.IMessengerCompat");
            obtain.writeInt(1);
            message.writeToParcel(obtain, 0);
            try {
                this.zza.transact(1, obtain, (Parcel)null, 1);
            }
            finally {
                obtain.recycle();
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.cloudmessaging;

import java.util.concurrent.TimeUnit;
import android.content.Intent;
import com.google.android.gms.common.internal.Preconditions;
import java.util.Iterator;
import com.google.android.gms.common.stats.ConnectionTracker;
import android.util.Log;
import android.os.IBinder;
import android.content.ComponentName;
import java.util.ArrayDeque;
import android.os.Handler;
import android.os.Handler$Callback;
import com.google.android.gms.internal.cloudmessaging.zzf;
import android.os.Looper;
import android.util.SparseArray;
import java.util.Queue;
import android.os.Messenger;
import android.content.ServiceConnection;

final class zzn implements ServiceConnection
{
    int zza = 0;
    final Messenger zzb = new Messenger((Handler)new zzf(Looper.getMainLooper(), (Handler$Callback)new zzk(this)));
    zzp zzc;
    final Queue zzd = new ArrayDeque();
    final SparseArray zze = new SparseArray();
    final zzu zzf;
    
    public final void onServiceConnected(final ComponentName componentName, final IBinder binder) {
        if (Log.isLoggable("MessengerIpcClient", 2)) {
            Log.v("MessengerIpcClient", "Service connected");
        }
        zzu.zze(this.zzf).execute(new zzg(this, binder));
    }
    
    public final void onServiceDisconnected(final ComponentName componentName) {
        if (Log.isLoggable("MessengerIpcClient", 2)) {
            Log.v("MessengerIpcClient", "Service disconnected");
        }
        zzu.zze(this.zzf).execute(new zzj(this));
    }
    
    public final void zza(final int n, final String s) {
        synchronized (this) {
            this.zzb(n, s, null);
        }
    }
    
    public final void zzb(int i, final String obj, final Throwable t) {
        synchronized (this) {
            if (Log.isLoggable("MessengerIpcClient", 3)) {
                Log.d("MessengerIpcClient", "Disconnected: ".concat(String.valueOf(obj)));
            }
            final int zza = this.zza;
            if (zza == 0) {
                throw new IllegalStateException();
            }
            if (zza == 1 || zza == 2) {
                if (Log.isLoggable("MessengerIpcClient", 2)) {
                    Log.v("MessengerIpcClient", "Unbinding service");
                }
                this.zza = 4;
                ConnectionTracker.getInstance().unbindService(zzu.zza(this.zzf), (ServiceConnection)this);
                final zzs zzs = new zzs(i, obj, t);
                final Iterator iterator = this.zzd.iterator();
                while (iterator.hasNext()) {
                    ((zzr)iterator.next()).zzc(zzs);
                }
                this.zzd.clear();
                for (i = 0; i < this.zze.size(); ++i) {
                    ((zzr)this.zze.valueAt(i)).zzc(zzs);
                }
                this.zze.clear();
                return;
            }
            if (zza != 3) {
                return;
            }
            this.zza = 4;
        }
    }
    
    public final void zzc() {
        zzu.zze(this.zzf).execute(new zzh(this));
    }
    
    public final void zzd() {
        synchronized (this) {
            if (this.zza == 1) {
                this.zza(1, "Timed out while binding");
            }
        }
    }
    
    public final void zze(final int i) {
        synchronized (this) {
            final zzr zzr = (zzr)this.zze.get(i);
            if (zzr != null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Timing out request: ");
                sb.append(i);
                Log.w("MessengerIpcClient", sb.toString());
                this.zze.remove(i);
                zzr.zzc(new zzs(3, "Timed out waiting for response", null));
                this.zzf();
            }
        }
    }
    
    public final void zzf() {
        synchronized (this) {
            if (this.zza == 2 && this.zzd.isEmpty() && this.zze.size() == 0) {
                if (Log.isLoggable("MessengerIpcClient", 2)) {
                    Log.v("MessengerIpcClient", "Finished handling requests, unbinding");
                }
                this.zza = 3;
                ConnectionTracker.getInstance().unbindService(zzu.zza(this.zzf), (ServiceConnection)this);
            }
        }
    }
    
    public final boolean zzg(final zzr zzr) {
        synchronized (this) {
            final int zza = this.zza;
            if (zza == 0) {
                this.zzd.add(zzr);
                Preconditions.checkState(this.zza == 0);
                if (Log.isLoggable("MessengerIpcClient", 2)) {
                    Log.v("MessengerIpcClient", "Starting bind to GmsCore");
                }
                this.zza = 1;
                final Intent intent = new Intent("com.google.android.c2dm.intent.REGISTER");
                intent.setPackage("com.google.android.gms");
                try {
                    if (!ConnectionTracker.getInstance().bindService(zzu.zza(this.zzf), intent, (ServiceConnection)this, 1)) {
                        this.zza(0, "Unable to bind to service");
                    }
                    else {
                        zzu.zze(this.zzf).schedule(new zzi(this), 30L, TimeUnit.SECONDS);
                    }
                }
                catch (final SecurityException ex) {
                    this.zzb(0, "Unable to bind to service", ex);
                }
                return true;
            }
            if (zza == 1) {
                this.zzd.add(zzr);
                return true;
            }
            if (zza != 2) {
                return false;
            }
            this.zzd.add(zzr);
            this.zzc();
            return true;
        }
    }
}

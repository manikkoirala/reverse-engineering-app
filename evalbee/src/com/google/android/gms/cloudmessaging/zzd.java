// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.cloudmessaging;

import android.util.Log;

public final class zzd extends ClassLoader
{
    public final Class loadClass(final String name, final boolean resolve) {
        if (name != "com.google.android.gms.iid.MessengerCompat" && (name == null || !name.equals("com.google.android.gms.iid.MessengerCompat"))) {
            return super.loadClass(name, resolve);
        }
        if (Log.isLoggable("CloudMessengerCompat", 3)) {
            Log.d("CloudMessengerCompat", "Using renamed FirebaseIidMessengerCompat class");
        }
        return zze.class;
    }
}

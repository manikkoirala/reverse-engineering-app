// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.cloudmessaging;

import android.os.BaseBundle;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import android.util.Log;
import java.util.Iterator;
import android.os.Bundle;
import java.util.Map;
import android.content.Intent;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "CloudMessageCreator")
public final class CloudMessage extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<CloudMessage> CREATOR;
    public static final int PRIORITY_HIGH = 1;
    public static final int PRIORITY_NORMAL = 2;
    public static final int PRIORITY_UNKNOWN = 0;
    @Field(id = 1)
    final Intent zza;
    private Map zzb;
    
    static {
        CREATOR = (Parcelable$Creator)new zzb();
    }
    
    @Constructor
    public CloudMessage(@Param(id = 1) final Intent zza) {
        this.zza = zza;
    }
    
    private static int zzb(final String s) {
        if (zza.zza(s, "high")) {
            return 1;
        }
        if (zza.zza(s, "normal")) {
            return 2;
        }
        return 0;
    }
    
    public String getCollapseKey() {
        return this.zza.getStringExtra("collapse_key");
    }
    
    public Map<String, String> getData() {
        synchronized (this) {
            if (this.zzb == null) {
                final Bundle extras = this.zza.getExtras();
                final r8 zzb = new r8();
                if (extras != null) {
                    for (final String s : ((BaseBundle)extras).keySet()) {
                        final Object value = ((BaseBundle)extras).get(s);
                        if (value instanceof String) {
                            final String s2 = (String)value;
                            if (s.startsWith("google.") || s.equals("from") || s.equals("message_type") || s.equals("collapse_key")) {
                                continue;
                            }
                            zzb.put(s, s2);
                        }
                    }
                }
                this.zzb = zzb;
            }
            return this.zzb;
        }
    }
    
    public String getFrom() {
        return this.zza.getStringExtra("from");
    }
    
    public Intent getIntent() {
        return this.zza;
    }
    
    public String getMessageId() {
        String s;
        if ((s = this.zza.getStringExtra("google.message_id")) == null) {
            s = this.zza.getStringExtra("message_id");
        }
        return s;
    }
    
    public String getMessageType() {
        return this.zza.getStringExtra("message_type");
    }
    
    public int getOriginalPriority() {
        String s;
        if ((s = this.zza.getStringExtra("google.original_priority")) == null) {
            s = this.zza.getStringExtra("google.priority");
        }
        return zzb(s);
    }
    
    public int getPriority() {
        String s;
        if ((s = this.zza.getStringExtra("google.delivered_priority")) == null) {
            if (com.google.android.gms.cloudmessaging.zza.zza(this.zza.getStringExtra("google.priority_reduced"), "1")) {
                return 2;
            }
            s = this.zza.getStringExtra("google.priority");
        }
        return zzb(s);
    }
    
    public byte[] getRawData() {
        return this.zza.getByteArrayExtra("rawData");
    }
    
    public String getSenderId() {
        return this.zza.getStringExtra("google.c.sender.id");
    }
    
    public long getSentTime() {
        final Bundle extras = this.zza.getExtras();
        Object value;
        if (extras != null) {
            value = ((BaseBundle)extras).get("google.sent_time");
        }
        else {
            value = null;
        }
        if (value instanceof Long) {
            return (long)value;
        }
        if (value instanceof String) {
            try {
                return Long.parseLong((String)value);
            }
            catch (final NumberFormatException ex) {
                Log.w("CloudMessage", "Invalid sent time: ".concat(String.valueOf(value)));
            }
        }
        return 0L;
    }
    
    public String getTo() {
        return this.zza.getStringExtra("google.to");
    }
    
    public int getTtl() {
        final Bundle extras = this.zza.getExtras();
        Object value;
        if (extras != null) {
            value = ((BaseBundle)extras).get("google.ttl");
        }
        else {
            value = null;
        }
        if (value instanceof Integer) {
            return (int)value;
        }
        if (value instanceof String) {
            try {
                return Integer.parseInt((String)value);
            }
            catch (final NumberFormatException ex) {
                Log.w("CloudMessage", "Invalid TTL: ".concat(String.valueOf(value)));
            }
        }
        return 0;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeParcelable(parcel, 1, (Parcelable)this.zza, n, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public final Integer zza() {
        if (this.zza.hasExtra("google.product_id")) {
            return this.zza.getIntExtra("google.product_id", 0);
        }
        return null;
    }
    
    @Retention(RetentionPolicy.SOURCE)
    @Target({ ElementType.TYPE_PARAMETER, ElementType.TYPE_USE })
    public @interface MessagePriority {
    }
}

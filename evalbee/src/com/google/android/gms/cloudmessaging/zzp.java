// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.cloudmessaging;

import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.os.IBinder;
import android.os.Messenger;

final class zzp
{
    private final Messenger zza;
    private final zze zzb;
    
    public zzp(final IBinder binder) {
        final String interfaceDescriptor = binder.getInterfaceDescriptor();
        if (zzo.zza(interfaceDescriptor, "android.os.IMessenger")) {
            this.zza = new Messenger(binder);
            this.zzb = null;
            return;
        }
        if (zzo.zza(interfaceDescriptor, "com.google.android.gms.iid.IMessengerCompat")) {
            this.zzb = new zze(binder);
            this.zza = null;
            return;
        }
        Log.w("MessengerIpcClient", "Invalid interface descriptor: ".concat(String.valueOf(interfaceDescriptor)));
        throw new RemoteException();
    }
    
    public final void zza(final Message message) {
        final Messenger zza = this.zza;
        if (zza != null) {
            zza.send(message);
            return;
        }
        final zze zzb = this.zzb;
        if (zzb != null) {
            zzb.zzb(message);
            return;
        }
        throw new IllegalStateException("Both messengers are null");
    }
}

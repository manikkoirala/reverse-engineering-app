// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.cloudmessaging;

import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.tasks.Task;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.Executors;
import com.google.android.gms.common.util.concurrent.NamedThreadFactory;
import com.google.android.gms.internal.cloudmessaging.zze;
import java.util.concurrent.ScheduledExecutorService;
import android.content.Context;

public final class zzu
{
    private static zzu zza;
    private final Context zzb;
    private final ScheduledExecutorService zzc;
    private zzn zzd;
    private int zze;
    
    public zzu(final Context context, final ScheduledExecutorService zzc) {
        this.zzd = new zzn(this, null);
        this.zze = 1;
        this.zzc = zzc;
        this.zzb = context.getApplicationContext();
    }
    
    public static zzu zzb(final Context context) {
        synchronized (zzu.class) {
            if (zzu.zza == null) {
                zze.zza();
                zzu.zza = new zzu(context, Executors.unconfigurableScheduledExecutorService(Executors.newScheduledThreadPool(1, new NamedThreadFactory("MessengerIpcClient"))));
            }
            return zzu.zza;
        }
    }
    
    private final int zzf() {
        synchronized (this) {
            return this.zze++;
        }
    }
    
    private final Task zzg(final zzr zzr) {
        synchronized (this) {
            if (Log.isLoggable("MessengerIpcClient", 3)) {
                Log.d("MessengerIpcClient", "Queueing ".concat(zzr.toString()));
            }
            if (!this.zzd.zzg(zzr)) {
                (this.zzd = new zzn(this, null)).zzg(zzr);
            }
            return zzr.zzb.getTask();
        }
    }
    
    public final Task zzc(final int n, final Bundle bundle) {
        return this.zzg(new zzq(this.zzf(), n, bundle));
    }
    
    public final Task zzd(final int n, final Bundle bundle) {
        return this.zzg(new zzt(this.zzf(), 1, bundle));
    }
}

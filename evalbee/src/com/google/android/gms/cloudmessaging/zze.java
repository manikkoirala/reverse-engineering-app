// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.cloudmessaging;

import android.os.IInterface;
import android.os.Message;
import android.os.Parcel;
import android.os.IBinder;
import android.os.Messenger;
import android.os.Parcelable$Creator;
import android.os.Parcelable;

public final class zze implements Parcelable
{
    public static final Parcelable$Creator<zze> CREATOR;
    Messenger zza;
    IMessengerCompat zzb;
    
    static {
        CREATOR = (Parcelable$Creator)new zzc();
    }
    
    public zze(final IBinder binder) {
        this.zza = new Messenger(binder);
    }
    
    public final int describeContents() {
        return 0;
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (o == null) {
            return false;
        }
        try {
            return this.zza().equals(((zze)o).zza());
        }
        catch (final ClassCastException ex) {
            return false;
        }
    }
    
    @Override
    public final int hashCode() {
        return this.zza().hashCode();
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final Messenger zza = this.zza;
        IBinder binder;
        if (zza != null) {
            binder = zza.getBinder();
        }
        else {
            binder = ((IInterface)this.zzb).asBinder();
        }
        parcel.writeStrongBinder(binder);
    }
    
    public final IBinder zza() {
        final Messenger zza = this.zza;
        IBinder binder;
        if (zza != null) {
            binder = zza.getBinder();
        }
        else {
            binder = ((IInterface)this.zzb).asBinder();
        }
        return binder;
    }
    
    public final void zzb(final Message message) {
        final Messenger zza = this.zza;
        if (zza != null) {
            zza.send(message);
            return;
        }
        this.zzb.send(message);
    }
}

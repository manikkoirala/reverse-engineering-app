// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.cloudmessaging;

import android.os.Message;
import android.os.Looper;
import com.google.android.gms.internal.cloudmessaging.zzf;

final class zzad extends zzf
{
    final Rpc zza;
    
    public zzad(final Rpc zza, final Looper looper) {
        this.zza = zza;
        super(looper);
    }
    
    public final void handleMessage(final Message message) {
        Rpc.zzc(this.zza, message);
    }
}

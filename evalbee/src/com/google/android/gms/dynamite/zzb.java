// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.dynamite;

import android.util.Log;
import android.os.Looper;

public final class zzb
{
    private static ClassLoader zza;
    private static Thread zzb;
    
    public static ClassLoader zza() {
        synchronized (zzb.class) {
            if (com.google.android.gms.dynamite.zzb.zza == null) {
                final Thread zzb = com.google.android.gms.dynamite.zzb.zzb;
                final ThreadGroup threadGroup = null;
                Label_0416: {
                    Object threadGroup2;
                    Object contextClassLoader = null;
                    if ((threadGroup2 = zzb) == null) {
                        threadGroup2 = Looper.getMainLooper().getThread().getThreadGroup();
                        Label_0317: {
                            if (threadGroup2 == null) {
                                contextClassLoader = null;
                                break Label_0317;
                            }
                            monitorenter(Void.class);
                            try {
                                Label_0310: {
                                    try {
                                        final int activeGroupCount = ((ThreadGroup)threadGroup2).activeGroupCount();
                                        final ThreadGroup[] list = new ThreadGroup[activeGroupCount];
                                        ((ThreadGroup)threadGroup2).enumerate(list);
                                        final int n = 0;
                                        for (final ThreadGroup threadGroup3 : list) {
                                            if ("dynamiteLoader".equals(threadGroup3.getName())) {
                                                ThreadGroup threadGroup4 = threadGroup3;
                                                if (threadGroup3 == null) {
                                                    threadGroup4 = new ThreadGroup((ThreadGroup)threadGroup2, "dynamiteLoader");
                                                }
                                                final int activeCount = threadGroup4.activeCount();
                                                threadGroup2 = new Thread[activeCount];
                                                threadGroup4.enumerate((Thread[])threadGroup2);
                                                int j = n;
                                                while (true) {
                                                    while (j < activeCount) {
                                                        Object o = threadGroup2[j];
                                                        if ("GmsDynamite".equals(((Thread)o).getName())) {
                                                            threadGroup2 = o;
                                                            if (o != null) {
                                                                break Label_0310;
                                                            }
                                                            try {
                                                                threadGroup2 = new zza(threadGroup4, "GmsDynamite");
                                                                try {
                                                                    ((Thread)threadGroup2).setContextClassLoader(null);
                                                                    ((Thread)threadGroup2).start();
                                                                }
                                                                catch (final SecurityException ex) {
                                                                    o = threadGroup2;
                                                                    threadGroup2 = ex;
                                                                }
                                                            }
                                                            catch (final SecurityException threadGroup2) {}
                                                        }
                                                        else {
                                                            ++j;
                                                        }
                                                    }
                                                    Object o = null;
                                                    continue;
                                                }
                                            }
                                        }
                                        ThreadGroup threadGroup3 = null;
                                    }
                                    finally {
                                        monitorexit(Void.class);
                                        final String message = ((Throwable)threadGroup2).getMessage();
                                        final StringBuilder sb = new StringBuilder();
                                        sb.append("Failed to enumerate thread/threadgroup ");
                                        sb.append(message);
                                        Log.w("DynamiteLoaderV2CL", sb.toString());
                                        final Object o;
                                        threadGroup2 = o;
                                        break Label_0310;
                                        contextClassLoader = threadGroup;
                                        break Label_0416;
                                        monitorexit(Void.class);
                                        contextClassLoader = threadGroup2;
                                        com.google.android.gms.dynamite.zzb.zzb = (Thread)contextClassLoader;
                                        threadGroup2 = contextClassLoader;
                                        iftrue(Label_0344:)(contextClassLoader != null);
                                    }
                                }
                            }
                            catch (final SecurityException ex2) {}
                        }
                    }
                    Label_0344: {
                        monitorenter(threadGroup2);
                    }
                    try {
                        try {
                            contextClassLoader = com.google.android.gms.dynamite.zzb.zzb.getContextClassLoader();
                        }
                        finally {
                            monitorexit(threadGroup2);
                            monitorexit(threadGroup2);
                            com.google.android.gms.dynamite.zzb.zza = (ClassLoader)contextClassLoader;
                        }
                    }
                    catch (final SecurityException ex3) {}
                }
            }
            return com.google.android.gms.dynamite.zzb.zza;
        }
    }
}

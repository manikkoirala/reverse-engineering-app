// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.dynamite;

import android.content.Context;

final class zze implements IVersions
{
    public zze() {
    }
    
    @Override
    public final int zza(final Context context, final String s) {
        return DynamiteModule.getLocalVersion(context, s);
    }
    
    @Override
    public final int zzb(final Context context, final String s, final boolean b) {
        return DynamiteModule.zza(context, s, b);
    }
}

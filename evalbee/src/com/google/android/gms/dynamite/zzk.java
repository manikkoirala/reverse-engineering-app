// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.dynamite;

import android.content.Context;

final class zzk implements VersionPolicy
{
    public zzk() {
    }
    
    @Override
    public final SelectionResult selectModule(final Context context, final String s, final IVersions versions) {
        final SelectionResult selectionResult = new SelectionResult();
        selectionResult.localVersion = versions.zza(context, s);
        final int zzb = versions.zzb(context, s, true);
        selectionResult.remoteVersion = zzb;
        int localVersion;
        if ((localVersion = selectionResult.localVersion) == 0) {
            localVersion = 0;
            if (zzb == 0) {
                selectionResult.selection = 0;
                return selectionResult;
            }
        }
        if (zzb >= localVersion) {
            selectionResult.selection = 1;
        }
        else {
            selectionResult.selection = -1;
        }
        return selectionResult;
    }
}

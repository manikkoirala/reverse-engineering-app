// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.dynamite;

import android.os.Parcel;
import android.os.IInterface;
import com.google.android.gms.internal.common.zzc;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IBinder;
import com.google.android.gms.internal.common.zza;

public final class zzr extends zza
{
    public zzr(final IBinder binder) {
        super(binder, "com.google.android.gms.dynamite.IDynamiteLoaderV2");
    }
    
    public final IObjectWrapper zze(IObjectWrapper interface1, final String s, final int n, final IObjectWrapper objectWrapper) {
        final Parcel zza = this.zza();
        zzc.zze(zza, (IInterface)interface1);
        zza.writeString(s);
        zza.writeInt(n);
        zzc.zze(zza, (IInterface)objectWrapper);
        final Parcel zzB = this.zzB(2, zza);
        interface1 = IObjectWrapper.Stub.asInterface(zzB.readStrongBinder());
        zzB.recycle();
        return interface1;
    }
    
    public final IObjectWrapper zzf(final IObjectWrapper objectWrapper, final String s, final int n, final IObjectWrapper objectWrapper2) {
        final Parcel zza = this.zza();
        zzc.zze(zza, (IInterface)objectWrapper);
        zza.writeString(s);
        zza.writeInt(n);
        zzc.zze(zza, (IInterface)objectWrapper2);
        final Parcel zzB = this.zzB(3, zza);
        final IObjectWrapper interface1 = IObjectWrapper.Stub.asInterface(zzB.readStrongBinder());
        zzB.recycle();
        return interface1;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.dynamite;

import android.content.Context;

final class zzg implements VersionPolicy
{
    public zzg() {
    }
    
    @Override
    public final SelectionResult selectModule(final Context context, final String s, final IVersions versions) {
        final SelectionResult selectionResult = new SelectionResult();
        final int zza = versions.zza(context, s);
        selectionResult.localVersion = zza;
        if (zza != 0) {
            selectionResult.selection = -1;
        }
        else if ((selectionResult.remoteVersion = versions.zzb(context, s, true)) != 0) {
            selectionResult.selection = 1;
        }
        return selectionResult;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.dynamite;

import com.google.android.gms.common.util.DynamiteApi;
import android.os.IInterface;
import android.content.pm.ApplicationInfo;
import android.content.pm.ProviderInfo;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import android.database.Cursor;
import java.lang.reflect.InvocationTargetException;
import android.os.IBinder;
import com.google.errorprone.annotations.ResultIgnorabilityUnspecified;
import java.lang.reflect.Field;
import android.util.Log;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import android.content.Context;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public final class DynamiteModule
{
    @KeepForSdk
    public static final int LOCAL = -1;
    @KeepForSdk
    public static final int NONE = 0;
    @KeepForSdk
    public static final int NO_SELECTION = 0;
    @KeepForSdk
    public static final VersionPolicy PREFER_HIGHEST_OR_LOCAL_VERSION;
    @KeepForSdk
    public static final VersionPolicy PREFER_HIGHEST_OR_LOCAL_VERSION_NO_FORCE_STAGING;
    @KeepForSdk
    public static final VersionPolicy PREFER_HIGHEST_OR_REMOTE_VERSION;
    @KeepForSdk
    public static final VersionPolicy PREFER_LOCAL;
    @KeepForSdk
    public static final VersionPolicy PREFER_REMOTE;
    @KeepForSdk
    public static final VersionPolicy PREFER_REMOTE_VERSION_NO_FORCE_STAGING;
    @KeepForSdk
    public static final int REMOTE = 1;
    public static final VersionPolicy zza;
    private static Boolean zzb;
    private static String zzc;
    private static boolean zzd = false;
    private static int zze = -1;
    private static Boolean zzf;
    private static final ThreadLocal zzg;
    private static final ThreadLocal zzh;
    private static final IVersions zzi;
    private static zzq zzk;
    private static zzr zzl;
    private final Context zzj;
    
    static {
        zzg = new ThreadLocal();
        zzh = new zzd();
        zzi = (IVersions)new zze();
        PREFER_REMOTE = (VersionPolicy)new zzf();
        PREFER_LOCAL = (VersionPolicy)new zzg();
        PREFER_REMOTE_VERSION_NO_FORCE_STAGING = (VersionPolicy)new zzh();
        PREFER_HIGHEST_OR_LOCAL_VERSION = (VersionPolicy)new zzi();
        PREFER_HIGHEST_OR_LOCAL_VERSION_NO_FORCE_STAGING = (VersionPolicy)new zzj();
        PREFER_HIGHEST_OR_REMOTE_VERSION = (VersionPolicy)new zzk();
        zza = (VersionPolicy)new zzl();
    }
    
    private DynamiteModule(final Context zzj) {
        Preconditions.checkNotNull(zzj);
        this.zzj = zzj;
    }
    
    @KeepForSdk
    public static int getLocalVersion(final Context context, final String str) {
        try {
            final ClassLoader classLoader = context.getApplicationContext().getClassLoader();
            final StringBuilder sb = new StringBuilder();
            sb.append("com.google.android.gms.dynamite.descriptors.");
            sb.append(str);
            sb.append(".ModuleDescriptor");
            final Class<?> loadClass = classLoader.loadClass(sb.toString());
            final Field declaredField = loadClass.getDeclaredField("MODULE_ID");
            final Field declaredField2 = loadClass.getDeclaredField("MODULE_VERSION");
            if (!Objects.equal(declaredField.get(null), str)) {
                final String value = String.valueOf(declaredField.get(null));
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Module descriptor id '");
                sb2.append(value);
                sb2.append("' didn't match expected id '");
                sb2.append(str);
                sb2.append("'");
                Log.e("DynamiteModule", sb2.toString());
                return 0;
            }
            return declaredField2.getInt(null);
        }
        catch (final Exception ex) {
            Log.e("DynamiteModule", "Failed to load module descriptor class: ".concat(String.valueOf(ex.getMessage())));
        }
        catch (final ClassNotFoundException ex2) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Local module descriptor class for ");
            sb3.append(str);
            sb3.append(" not found.");
            Log.w("DynamiteModule", sb3.toString());
        }
        return 0;
    }
    
    @KeepForSdk
    public static int getRemoteVersion(final Context context, final String s) {
        return zza(context, s, false);
    }
    
    @ResultIgnorabilityUnspecified
    @KeepForSdk
    public static DynamiteModule load(final Context p0, final VersionPolicy p1, final String p2) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokevirtual   android/content/Context.getApplicationContext:()Landroid/content/Context;
        //     4: astore          8
        //     6: aload           8
        //     8: ifnull          2067
        //    11: getstatic       com/google/android/gms/dynamite/DynamiteModule.zzg:Ljava/lang/ThreadLocal;
        //    14: astore          16
        //    16: aload           16
        //    18: invokevirtual   java/lang/ThreadLocal.get:()Ljava/lang/Object;
        //    21: checkcast       Lcom/google/android/gms/dynamite/zzn;
        //    24: astore          13
        //    26: new             Lcom/google/android/gms/dynamite/zzn;
        //    29: dup            
        //    30: aconst_null    
        //    31: invokespecial   com/google/android/gms/dynamite/zzn.<init>:(Lcom/google/android/gms/dynamite/zzm;)V
        //    34: astore          14
        //    36: aload           16
        //    38: aload           14
        //    40: invokevirtual   java/lang/ThreadLocal.set:(Ljava/lang/Object;)V
        //    43: getstatic       com/google/android/gms/dynamite/DynamiteModule.zzh:Ljava/lang/ThreadLocal;
        //    46: astore          17
        //    48: aload           17
        //    50: invokevirtual   java/lang/ThreadLocal.get:()Ljava/lang/Object;
        //    53: checkcast       Ljava/lang/Long;
        //    56: invokevirtual   java/lang/Long.longValue:()J
        //    59: lstore          6
        //    61: aload           17
        //    63: invokestatic    android/os/SystemClock.elapsedRealtime:()J
        //    66: invokestatic    java/lang/Long.valueOf:(J)Ljava/lang/Long;
        //    69: invokevirtual   java/lang/ThreadLocal.set:(Ljava/lang/Object;)V
        //    72: aload_1        
        //    73: aload_0        
        //    74: aload_2        
        //    75: getstatic       com/google/android/gms/dynamite/DynamiteModule.zzi:Lcom/google/android/gms/dynamite/DynamiteModule$VersionPolicy$IVersions;
        //    78: invokeinterface com/google/android/gms/dynamite/DynamiteModule$VersionPolicy.selectModule:(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/dynamite/DynamiteModule$VersionPolicy$IVersions;)Lcom/google/android/gms/dynamite/DynamiteModule$VersionPolicy$SelectionResult;
        //    83: astore          15
        //    85: aload           15
        //    87: getfield        com/google/android/gms/dynamite/DynamiteModule$VersionPolicy$SelectionResult.localVersion:I
        //    90: istore          4
        //    92: aload           15
        //    94: getfield        com/google/android/gms/dynamite/DynamiteModule$VersionPolicy$SelectionResult.remoteVersion:I
        //    97: istore_3       
        //    98: new             Ljava/lang/StringBuilder;
        //   101: astore          9
        //   103: aload           9
        //   105: invokespecial   java/lang/StringBuilder.<init>:()V
        //   108: aload           9
        //   110: ldc_w           "Considering local module "
        //   113: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   116: pop            
        //   117: aload           9
        //   119: aload_2        
        //   120: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   123: pop            
        //   124: aload           9
        //   126: ldc_w           ":"
        //   129: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   132: pop            
        //   133: aload           9
        //   135: iload           4
        //   137: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   140: pop            
        //   141: aload           9
        //   143: ldc_w           " and remote module "
        //   146: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   149: pop            
        //   150: aload           9
        //   152: aload_2        
        //   153: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   156: pop            
        //   157: aload           9
        //   159: ldc_w           ":"
        //   162: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   165: pop            
        //   166: aload           9
        //   168: iload_3        
        //   169: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   172: pop            
        //   173: ldc             "DynamiteModule"
        //   175: aload           9
        //   177: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   180: invokestatic    android/util/Log.i:(Ljava/lang/String;Ljava/lang/String;)I
        //   183: pop            
        //   184: aload           15
        //   186: getfield        com/google/android/gms/dynamite/DynamiteModule$VersionPolicy$SelectionResult.selection:I
        //   189: istore          4
        //   191: iload           4
        //   193: ifeq            1926
        //   196: iload           4
        //   198: istore_3       
        //   199: iload           4
        //   201: iconst_m1      
        //   202: if_icmpne       215
        //   205: aload           15
        //   207: getfield        com/google/android/gms/dynamite/DynamiteModule$VersionPolicy$SelectionResult.localVersion:I
        //   210: ifeq            1926
        //   213: iconst_m1      
        //   214: istore_3       
        //   215: iload_3        
        //   216: iconst_1       
        //   217: if_icmpne       228
        //   220: aload           15
        //   222: getfield        com/google/android/gms/dynamite/DynamiteModule$VersionPolicy$SelectionResult.remoteVersion:I
        //   225: ifeq            1926
        //   228: iload_3        
        //   229: iconst_m1      
        //   230: if_icmpne       290
        //   233: aload           8
        //   235: aload_2        
        //   236: invokestatic    com/google/android/gms/dynamite/DynamiteModule.zzc:(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/dynamite/DynamiteModule;
        //   239: astore_0       
        //   240: lload           6
        //   242: lconst_0       
        //   243: lcmp           
        //   244: ifne            255
        //   247: aload           17
        //   249: invokevirtual   java/lang/ThreadLocal.remove:()V
        //   252: goto            265
        //   255: aload           17
        //   257: lload           6
        //   259: invokestatic    java/lang/Long.valueOf:(J)Ljava/lang/Long;
        //   262: invokevirtual   java/lang/ThreadLocal.set:(Ljava/lang/Object;)V
        //   265: aload           14
        //   267: getfield        com/google/android/gms/dynamite/zzn.zza:Landroid/database/Cursor;
        //   270: astore_1       
        //   271: aload_1        
        //   272: ifnull          281
        //   275: aload_1        
        //   276: invokeinterface android/database/Cursor.close:()V
        //   281: aload           16
        //   283: aload           13
        //   285: invokevirtual   java/lang/ThreadLocal.set:(Ljava/lang/Object;)V
        //   288: aload_0        
        //   289: areturn        
        //   290: iload_3        
        //   291: iconst_1       
        //   292: if_icmpne       1889
        //   295: aload           15
        //   297: getfield        com/google/android/gms/dynamite/DynamiteModule$VersionPolicy$SelectionResult.remoteVersion:I
        //   300: istore          4
        //   302: ldc             Lcom/google/android/gms/dynamite/DynamiteModule;.class
        //   304: monitorenter   
        //   305: aload_0        
        //   306: invokestatic    com/google/android/gms/dynamite/DynamiteModule.zzf:(Landroid/content/Context;)Z
        //   309: ifeq            1543
        //   312: getstatic       com/google/android/gms/dynamite/DynamiteModule.zzb:Ljava/lang/Boolean;
        //   315: astore          9
        //   317: ldc             Lcom/google/android/gms/dynamite/DynamiteModule;.class
        //   319: monitorexit    
        //   320: aload           9
        //   322: ifnull          1486
        //   325: aload           9
        //   327: invokevirtual   java/lang/Boolean.booleanValue:()Z
        //   330: ifeq            870
        //   333: new             Ljava/lang/StringBuilder;
        //   336: astore          9
        //   338: aload           9
        //   340: invokespecial   java/lang/StringBuilder.<init>:()V
        //   343: aload           9
        //   345: ldc_w           "Selected remote version of "
        //   348: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   351: pop            
        //   352: aload           9
        //   354: aload_2        
        //   355: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   358: pop            
        //   359: aload           9
        //   361: ldc_w           ", version >= "
        //   364: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   367: pop            
        //   368: aload           9
        //   370: iload           4
        //   372: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   375: pop            
        //   376: ldc             "DynamiteModule"
        //   378: aload           9
        //   380: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   383: invokestatic    android/util/Log.i:(Ljava/lang/String;Ljava/lang/String;)I
        //   386: pop            
        //   387: ldc             Lcom/google/android/gms/dynamite/DynamiteModule;.class
        //   389: monitorenter   
        //   390: getstatic       com/google/android/gms/dynamite/DynamiteModule.zzl:Lcom/google/android/gms/dynamite/zzr;
        //   393: astore          12
        //   395: ldc             Lcom/google/android/gms/dynamite/DynamiteModule;.class
        //   397: monitorexit    
        //   398: aload           12
        //   400: ifnull          788
        //   403: aload           16
        //   405: invokevirtual   java/lang/ThreadLocal.get:()Ljava/lang/Object;
        //   408: checkcast       Lcom/google/android/gms/dynamite/zzn;
        //   411: astore          9
        //   413: aload           9
        //   415: ifnull          731
        //   418: aload           9
        //   420: getfield        com/google/android/gms/dynamite/zzn.zza:Landroid/database/Cursor;
        //   423: ifnull          731
        //   426: aload_0        
        //   427: invokevirtual   android/content/Context.getApplicationContext:()Landroid/content/Context;
        //   430: astore          18
        //   432: aload           9
        //   434: getfield        com/google/android/gms/dynamite/zzn.zza:Landroid/database/Cursor;
        //   437: astore          19
        //   439: aconst_null    
        //   440: invokestatic    com/google/android/gms/dynamic/ObjectWrapper.wrap:(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;
        //   443: pop            
        //   444: ldc             Lcom/google/android/gms/dynamite/DynamiteModule;.class
        //   446: monitorenter   
        //   447: aload           8
        //   449: astore          11
        //   451: getstatic       com/google/android/gms/dynamite/DynamiteModule.zze:I
        //   454: iconst_2       
        //   455: if_icmplt       464
        //   458: iconst_1       
        //   459: istore          5
        //   461: goto            467
        //   464: iconst_0       
        //   465: istore          5
        //   467: ldc             Lcom/google/android/gms/dynamite/DynamiteModule;.class
        //   469: monitorexit    
        //   470: aload           11
        //   472: astore          8
        //   474: aload           11
        //   476: astore          9
        //   478: aload           11
        //   480: astore          10
        //   482: iload           5
        //   484: invokestatic    java/lang/Boolean.valueOf:(Z)Ljava/lang/Boolean;
        //   487: invokevirtual   java/lang/Boolean.booleanValue:()Z
        //   490: ifeq            549
        //   493: aload           11
        //   495: astore          8
        //   497: aload           11
        //   499: astore          9
        //   501: aload           11
        //   503: astore          10
        //   505: ldc             "DynamiteModule"
        //   507: ldc_w           "Dynamite loader version >= 2, using loadModule2NoCrashUtils"
        //   510: invokestatic    android/util/Log.v:(Ljava/lang/String;Ljava/lang/String;)I
        //   513: pop            
        //   514: aload           11
        //   516: astore          8
        //   518: aload           11
        //   520: astore          9
        //   522: aload           11
        //   524: astore          10
        //   526: aload           12
        //   528: aload           18
        //   530: invokestatic    com/google/android/gms/dynamic/ObjectWrapper.wrap:(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;
        //   533: aload_2        
        //   534: iload           4
        //   536: aload           19
        //   538: invokestatic    com/google/android/gms/dynamic/ObjectWrapper.wrap:(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;
        //   541: invokevirtual   com/google/android/gms/dynamite/zzr.zzf:(Lcom/google/android/gms/dynamic/IObjectWrapper;Ljava/lang/String;ILcom/google/android/gms/dynamic/IObjectWrapper;)Lcom/google/android/gms/dynamic/IObjectWrapper;
        //   544: astore          12
        //   546: goto            602
        //   549: aload           11
        //   551: astore          8
        //   553: aload           11
        //   555: astore          9
        //   557: aload           11
        //   559: astore          10
        //   561: ldc             "DynamiteModule"
        //   563: ldc_w           "Dynamite loader version < 2, falling back to loadModule2"
        //   566: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;)I
        //   569: pop            
        //   570: aload           11
        //   572: astore          8
        //   574: aload           11
        //   576: astore          9
        //   578: aload           11
        //   580: astore          10
        //   582: aload           12
        //   584: aload           18
        //   586: invokestatic    com/google/android/gms/dynamic/ObjectWrapper.wrap:(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;
        //   589: aload_2        
        //   590: iload           4
        //   592: aload           19
        //   594: invokestatic    com/google/android/gms/dynamic/ObjectWrapper.wrap:(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;
        //   597: invokevirtual   com/google/android/gms/dynamite/zzr.zze:(Lcom/google/android/gms/dynamic/IObjectWrapper;Ljava/lang/String;ILcom/google/android/gms/dynamic/IObjectWrapper;)Lcom/google/android/gms/dynamic/IObjectWrapper;
        //   600: astore          12
        //   602: aload           11
        //   604: astore          8
        //   606: aload           11
        //   608: astore          9
        //   610: aload           11
        //   612: astore          10
        //   614: aload           12
        //   616: invokestatic    com/google/android/gms/dynamic/ObjectWrapper.unwrap:(Lcom/google/android/gms/dynamic/IObjectWrapper;)Ljava/lang/Object;
        //   619: checkcast       Landroid/content/Context;
        //   622: astore          12
        //   624: aload           12
        //   626: ifnull          658
        //   629: aload           11
        //   631: astore          8
        //   633: aload           11
        //   635: astore          9
        //   637: aload           11
        //   639: astore          10
        //   641: new             Lcom/google/android/gms/dynamite/DynamiteModule;
        //   644: dup            
        //   645: aload           12
        //   647: invokespecial   com/google/android/gms/dynamite/DynamiteModule.<init>:(Landroid/content/Context;)V
        //   650: astore          11
        //   652: aload           11
        //   654: astore_0       
        //   655: goto            1330
        //   658: aload           11
        //   660: astore          8
        //   662: aload           11
        //   664: astore          9
        //   666: aload           11
        //   668: astore          10
        //   670: new             Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //   673: astore          12
        //   675: aload           11
        //   677: astore          8
        //   679: aload           11
        //   681: astore          9
        //   683: aload           11
        //   685: astore          10
        //   687: aload           12
        //   689: ldc_w           "Failed to get module context"
        //   692: aconst_null    
        //   693: invokespecial   com/google/android/gms/dynamite/DynamiteModule$LoadingException.<init>:(Ljava/lang/String;Lcom/google/android/gms/dynamite/zzp;)V
        //   696: aload           11
        //   698: astore          8
        //   700: aload           11
        //   702: astore          9
        //   704: aload           11
        //   706: astore          10
        //   708: aload           12
        //   710: athrow         
        //   711: astore          12
        //   713: ldc             Lcom/google/android/gms/dynamite/DynamiteModule;.class
        //   715: monitorexit    
        //   716: aload           11
        //   718: astore          8
        //   720: aload           11
        //   722: astore          9
        //   724: aload           11
        //   726: astore          10
        //   728: aload           12
        //   730: athrow         
        //   731: aload           8
        //   733: astore          11
        //   735: aload           11
        //   737: astore          8
        //   739: aload           11
        //   741: astore          9
        //   743: aload           11
        //   745: astore          10
        //   747: new             Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //   750: astore          12
        //   752: aload           11
        //   754: astore          8
        //   756: aload           11
        //   758: astore          9
        //   760: aload           11
        //   762: astore          10
        //   764: aload           12
        //   766: ldc_w           "No result cursor"
        //   769: aconst_null    
        //   770: invokespecial   com/google/android/gms/dynamite/DynamiteModule$LoadingException.<init>:(Ljava/lang/String;Lcom/google/android/gms/dynamite/zzp;)V
        //   773: aload           11
        //   775: astore          8
        //   777: aload           11
        //   779: astore          9
        //   781: aload           11
        //   783: astore          10
        //   785: aload           12
        //   787: athrow         
        //   788: aload           8
        //   790: astore          11
        //   792: aload           11
        //   794: astore          8
        //   796: aload           11
        //   798: astore          9
        //   800: aload           11
        //   802: astore          10
        //   804: new             Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //   807: astore          12
        //   809: aload           11
        //   811: astore          8
        //   813: aload           11
        //   815: astore          9
        //   817: aload           11
        //   819: astore          10
        //   821: aload           12
        //   823: ldc_w           "DynamiteLoaderV2 was not cached."
        //   826: aconst_null    
        //   827: invokespecial   com/google/android/gms/dynamite/DynamiteModule$LoadingException.<init>:(Ljava/lang/String;Lcom/google/android/gms/dynamite/zzp;)V
        //   830: aload           11
        //   832: astore          8
        //   834: aload           11
        //   836: astore          9
        //   838: aload           11
        //   840: astore          10
        //   842: aload           12
        //   844: athrow         
        //   845: astore          11
        //   847: aload           8
        //   849: astore          10
        //   851: ldc             Lcom/google/android/gms/dynamite/DynamiteModule;.class
        //   853: monitorexit    
        //   854: aload           10
        //   856: astore          8
        //   858: aload           10
        //   860: astore          9
        //   862: aload           11
        //   864: athrow         
        //   865: astore          11
        //   867: goto            851
        //   870: aload           8
        //   872: astore          11
        //   874: aload           11
        //   876: astore          8
        //   878: aload           11
        //   880: astore          9
        //   882: aload           11
        //   884: astore          10
        //   886: new             Ljava/lang/StringBuilder;
        //   889: astore          12
        //   891: aload           11
        //   893: astore          8
        //   895: aload           11
        //   897: astore          9
        //   899: aload           11
        //   901: astore          10
        //   903: aload           12
        //   905: invokespecial   java/lang/StringBuilder.<init>:()V
        //   908: aload           11
        //   910: astore          8
        //   912: aload           11
        //   914: astore          9
        //   916: aload           11
        //   918: astore          10
        //   920: aload           12
        //   922: ldc_w           "Selected remote version of "
        //   925: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   928: pop            
        //   929: aload           11
        //   931: astore          8
        //   933: aload           11
        //   935: astore          9
        //   937: aload           11
        //   939: astore          10
        //   941: aload           12
        //   943: aload_2        
        //   944: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   947: pop            
        //   948: aload           11
        //   950: astore          8
        //   952: aload           11
        //   954: astore          9
        //   956: aload           11
        //   958: astore          10
        //   960: aload           12
        //   962: ldc_w           ", version >= "
        //   965: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   968: pop            
        //   969: aload           11
        //   971: astore          8
        //   973: aload           11
        //   975: astore          9
        //   977: aload           11
        //   979: astore          10
        //   981: aload           12
        //   983: iload           4
        //   985: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   988: pop            
        //   989: aload           11
        //   991: astore          8
        //   993: aload           11
        //   995: astore          9
        //   997: aload           11
        //   999: astore          10
        //  1001: ldc             "DynamiteModule"
        //  1003: aload           12
        //  1005: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //  1008: invokestatic    android/util/Log.i:(Ljava/lang/String;Ljava/lang/String;)I
        //  1011: pop            
        //  1012: aload           11
        //  1014: astore          8
        //  1016: aload           11
        //  1018: astore          9
        //  1020: aload           11
        //  1022: astore          10
        //  1024: aload_0        
        //  1025: invokestatic    com/google/android/gms/dynamite/DynamiteModule.zzg:(Landroid/content/Context;)Lcom/google/android/gms/dynamite/zzq;
        //  1028: astore          18
        //  1030: aload           18
        //  1032: ifnull          1433
        //  1035: aload           11
        //  1037: astore          8
        //  1039: aload           11
        //  1041: astore          9
        //  1043: aload           11
        //  1045: astore          10
        //  1047: aload           18
        //  1049: invokevirtual   com/google/android/gms/dynamite/zzq.zze:()I
        //  1052: istore_3       
        //  1053: iload_3        
        //  1054: iconst_3       
        //  1055: if_icmplt       1175
        //  1058: aload           11
        //  1060: astore          8
        //  1062: aload           11
        //  1064: astore          9
        //  1066: aload           11
        //  1068: astore          10
        //  1070: aload           16
        //  1072: invokevirtual   java/lang/ThreadLocal.get:()Ljava/lang/Object;
        //  1075: checkcast       Lcom/google/android/gms/dynamite/zzn;
        //  1078: astore          12
        //  1080: aload           12
        //  1082: ifnull          1122
        //  1085: aload           11
        //  1087: astore          8
        //  1089: aload           11
        //  1091: astore          9
        //  1093: aload           11
        //  1095: astore          10
        //  1097: aload           18
        //  1099: aload_0        
        //  1100: invokestatic    com/google/android/gms/dynamic/ObjectWrapper.wrap:(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;
        //  1103: aload_2        
        //  1104: iload           4
        //  1106: aload           12
        //  1108: getfield        com/google/android/gms/dynamite/zzn.zza:Landroid/database/Cursor;
        //  1111: invokestatic    com/google/android/gms/dynamic/ObjectWrapper.wrap:(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;
        //  1114: invokevirtual   com/google/android/gms/dynamite/zzq.zzi:(Lcom/google/android/gms/dynamic/IObjectWrapper;Ljava/lang/String;ILcom/google/android/gms/dynamic/IObjectWrapper;)Lcom/google/android/gms/dynamic/IObjectWrapper;
        //  1117: astore          12
        //  1119: goto            1277
        //  1122: aload           11
        //  1124: astore          8
        //  1126: aload           11
        //  1128: astore          9
        //  1130: aload           11
        //  1132: astore          10
        //  1134: new             Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1137: astore          12
        //  1139: aload           11
        //  1141: astore          8
        //  1143: aload           11
        //  1145: astore          9
        //  1147: aload           11
        //  1149: astore          10
        //  1151: aload           12
        //  1153: ldc_w           "No cached result cursor holder"
        //  1156: aconst_null    
        //  1157: invokespecial   com/google/android/gms/dynamite/DynamiteModule$LoadingException.<init>:(Ljava/lang/String;Lcom/google/android/gms/dynamite/zzp;)V
        //  1160: aload           11
        //  1162: astore          8
        //  1164: aload           11
        //  1166: astore          9
        //  1168: aload           11
        //  1170: astore          10
        //  1172: aload           12
        //  1174: athrow         
        //  1175: iload_3        
        //  1176: iconst_2       
        //  1177: if_icmpne       1230
        //  1180: aload           11
        //  1182: astore          8
        //  1184: aload           11
        //  1186: astore          9
        //  1188: aload           11
        //  1190: astore          10
        //  1192: ldc             "DynamiteModule"
        //  1194: ldc_w           "IDynamite loader version = 2"
        //  1197: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;)I
        //  1200: pop            
        //  1201: aload           11
        //  1203: astore          8
        //  1205: aload           11
        //  1207: astore          9
        //  1209: aload           11
        //  1211: astore          10
        //  1213: aload           18
        //  1215: aload_0        
        //  1216: invokestatic    com/google/android/gms/dynamic/ObjectWrapper.wrap:(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;
        //  1219: aload_2        
        //  1220: iload           4
        //  1222: invokevirtual   com/google/android/gms/dynamite/zzq.zzj:(Lcom/google/android/gms/dynamic/IObjectWrapper;Ljava/lang/String;I)Lcom/google/android/gms/dynamic/IObjectWrapper;
        //  1225: astore          12
        //  1227: goto            1277
        //  1230: aload           11
        //  1232: astore          8
        //  1234: aload           11
        //  1236: astore          9
        //  1238: aload           11
        //  1240: astore          10
        //  1242: ldc             "DynamiteModule"
        //  1244: ldc_w           "Dynamite loader version < 2, falling back to createModuleContext"
        //  1247: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;)I
        //  1250: pop            
        //  1251: aload           11
        //  1253: astore          8
        //  1255: aload           11
        //  1257: astore          9
        //  1259: aload           11
        //  1261: astore          10
        //  1263: aload           18
        //  1265: aload_0        
        //  1266: invokestatic    com/google/android/gms/dynamic/ObjectWrapper.wrap:(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;
        //  1269: aload_2        
        //  1270: iload           4
        //  1272: invokevirtual   com/google/android/gms/dynamite/zzq.zzh:(Lcom/google/android/gms/dynamic/IObjectWrapper;Ljava/lang/String;I)Lcom/google/android/gms/dynamic/IObjectWrapper;
        //  1275: astore          12
        //  1277: aload           11
        //  1279: astore          8
        //  1281: aload           11
        //  1283: astore          9
        //  1285: aload           11
        //  1287: astore          10
        //  1289: aload           12
        //  1291: invokestatic    com/google/android/gms/dynamic/ObjectWrapper.unwrap:(Lcom/google/android/gms/dynamic/IObjectWrapper;)Ljava/lang/Object;
        //  1294: astore          12
        //  1296: aload           12
        //  1298: ifnull          1380
        //  1301: aload           11
        //  1303: astore          8
        //  1305: aload           11
        //  1307: astore          9
        //  1309: aload           11
        //  1311: astore          10
        //  1313: new             Lcom/google/android/gms/dynamite/DynamiteModule;
        //  1316: dup            
        //  1317: aload           12
        //  1319: checkcast       Landroid/content/Context;
        //  1322: invokespecial   com/google/android/gms/dynamite/DynamiteModule.<init>:(Landroid/content/Context;)V
        //  1325: astore          11
        //  1327: aload           11
        //  1329: astore_0       
        //  1330: lload           6
        //  1332: lconst_0       
        //  1333: lcmp           
        //  1334: ifne            1345
        //  1337: aload           17
        //  1339: invokevirtual   java/lang/ThreadLocal.remove:()V
        //  1342: goto            1355
        //  1345: aload           17
        //  1347: lload           6
        //  1349: invokestatic    java/lang/Long.valueOf:(J)Ljava/lang/Long;
        //  1352: invokevirtual   java/lang/ThreadLocal.set:(Ljava/lang/Object;)V
        //  1355: aload           14
        //  1357: getfield        com/google/android/gms/dynamite/zzn.zza:Landroid/database/Cursor;
        //  1360: astore_1       
        //  1361: aload_1        
        //  1362: ifnull          1371
        //  1365: aload_1        
        //  1366: invokeinterface android/database/Cursor.close:()V
        //  1371: aload           16
        //  1373: aload           13
        //  1375: invokevirtual   java/lang/ThreadLocal.set:(Ljava/lang/Object;)V
        //  1378: aload_0        
        //  1379: areturn        
        //  1380: aload           11
        //  1382: astore          8
        //  1384: aload           11
        //  1386: astore          9
        //  1388: aload           11
        //  1390: astore          10
        //  1392: new             Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1395: astore          12
        //  1397: aload           11
        //  1399: astore          8
        //  1401: aload           11
        //  1403: astore          9
        //  1405: aload           11
        //  1407: astore          10
        //  1409: aload           12
        //  1411: ldc_w           "Failed to load remote module."
        //  1414: aconst_null    
        //  1415: invokespecial   com/google/android/gms/dynamite/DynamiteModule$LoadingException.<init>:(Ljava/lang/String;Lcom/google/android/gms/dynamite/zzp;)V
        //  1418: aload           11
        //  1420: astore          8
        //  1422: aload           11
        //  1424: astore          9
        //  1426: aload           11
        //  1428: astore          10
        //  1430: aload           12
        //  1432: athrow         
        //  1433: aload           11
        //  1435: astore          8
        //  1437: aload           11
        //  1439: astore          9
        //  1441: aload           11
        //  1443: astore          10
        //  1445: new             Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1448: astore          12
        //  1450: aload           11
        //  1452: astore          8
        //  1454: aload           11
        //  1456: astore          9
        //  1458: aload           11
        //  1460: astore          10
        //  1462: aload           12
        //  1464: ldc_w           "Failed to create IDynamiteLoader."
        //  1467: aconst_null    
        //  1468: invokespecial   com/google/android/gms/dynamite/DynamiteModule$LoadingException.<init>:(Ljava/lang/String;Lcom/google/android/gms/dynamite/zzp;)V
        //  1471: aload           11
        //  1473: astore          8
        //  1475: aload           11
        //  1477: astore          9
        //  1479: aload           11
        //  1481: astore          10
        //  1483: aload           12
        //  1485: athrow         
        //  1486: aload           8
        //  1488: astore          11
        //  1490: aload           11
        //  1492: astore          8
        //  1494: aload           11
        //  1496: astore          9
        //  1498: aload           11
        //  1500: astore          10
        //  1502: new             Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1505: astore          12
        //  1507: aload           11
        //  1509: astore          8
        //  1511: aload           11
        //  1513: astore          9
        //  1515: aload           11
        //  1517: astore          10
        //  1519: aload           12
        //  1521: ldc_w           "Failed to determine which loading route to use."
        //  1524: aconst_null    
        //  1525: invokespecial   com/google/android/gms/dynamite/DynamiteModule$LoadingException.<init>:(Ljava/lang/String;Lcom/google/android/gms/dynamite/zzp;)V
        //  1528: aload           11
        //  1530: astore          8
        //  1532: aload           11
        //  1534: astore          9
        //  1536: aload           11
        //  1538: astore          10
        //  1540: aload           12
        //  1542: athrow         
        //  1543: aload           8
        //  1545: astore          9
        //  1547: aload           9
        //  1549: astore          8
        //  1551: new             Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1554: astore          10
        //  1556: aload           9
        //  1558: astore          8
        //  1560: aload           10
        //  1562: ldc_w           "Remote loading disabled"
        //  1565: aconst_null    
        //  1566: invokespecial   com/google/android/gms/dynamite/DynamiteModule$LoadingException.<init>:(Ljava/lang/String;Lcom/google/android/gms/dynamite/zzp;)V
        //  1569: aload           9
        //  1571: astore          8
        //  1573: aload           10
        //  1575: athrow         
        //  1576: astore          11
        //  1578: aload           8
        //  1580: astore          10
        //  1582: goto            1591
        //  1585: astore          11
        //  1587: aload           8
        //  1589: astore          10
        //  1591: aload           10
        //  1593: astore          8
        //  1595: ldc             Lcom/google/android/gms/dynamite/DynamiteModule;.class
        //  1597: monitorexit    
        //  1598: aload           10
        //  1600: astore          8
        //  1602: aload           10
        //  1604: astore          9
        //  1606: aload           11
        //  1608: athrow         
        //  1609: astore          10
        //  1611: aload           8
        //  1613: astore          9
        //  1615: goto            1638
        //  1618: astore          10
        //  1620: aload           9
        //  1622: astore          8
        //  1624: goto            1682
        //  1627: astore          9
        //  1629: goto            1691
        //  1632: astore          10
        //  1634: aload           8
        //  1636: astore          9
        //  1638: aload           9
        //  1640: astore          8
        //  1642: aload_0        
        //  1643: aload           10
        //  1645: invokestatic    com/google/android/gms/common/util/CrashUtils.addDynamiteErrorToDropBox:(Landroid/content/Context;Ljava/lang/Throwable;)Z
        //  1648: pop            
        //  1649: aload           9
        //  1651: astore          8
        //  1653: new             Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1656: astore          11
        //  1658: aload           9
        //  1660: astore          8
        //  1662: aload           11
        //  1664: ldc_w           "Failed to load remote module."
        //  1667: aload           10
        //  1669: aconst_null    
        //  1670: invokespecial   com/google/android/gms/dynamite/DynamiteModule$LoadingException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;Lcom/google/android/gms/dynamite/zzp;)V
        //  1673: aload           9
        //  1675: astore          8
        //  1677: aload           11
        //  1679: athrow         
        //  1680: astore          10
        //  1682: aload           10
        //  1684: athrow         
        //  1685: astore          9
        //  1687: aload           8
        //  1689: astore          10
        //  1691: aload           10
        //  1693: astore          8
        //  1695: new             Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1698: astore          11
        //  1700: aload           10
        //  1702: astore          8
        //  1704: aload           11
        //  1706: ldc_w           "Failed to load remote module."
        //  1709: aload           9
        //  1711: aconst_null    
        //  1712: invokespecial   com/google/android/gms/dynamite/DynamiteModule$LoadingException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;Lcom/google/android/gms/dynamite/zzp;)V
        //  1715: aload           10
        //  1717: astore          8
        //  1719: aload           11
        //  1721: athrow         
        //  1722: astore          9
        //  1724: goto            1729
        //  1727: astore          9
        //  1729: aload           9
        //  1731: invokevirtual   java/lang/Throwable.getMessage:()Ljava/lang/String;
        //  1734: astore          11
        //  1736: new             Ljava/lang/StringBuilder;
        //  1739: astore          10
        //  1741: aload           10
        //  1743: invokespecial   java/lang/StringBuilder.<init>:()V
        //  1746: aload           10
        //  1748: ldc_w           "Failed to load remote module: "
        //  1751: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1754: pop            
        //  1755: aload           10
        //  1757: aload           11
        //  1759: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1762: pop            
        //  1763: ldc             "DynamiteModule"
        //  1765: aload           10
        //  1767: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //  1770: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;)I
        //  1773: pop            
        //  1774: aload           15
        //  1776: getfield        com/google/android/gms/dynamite/DynamiteModule$VersionPolicy$SelectionResult.localVersion:I
        //  1779: istore_3       
        //  1780: iload_3        
        //  1781: ifeq            1873
        //  1784: new             Lcom/google/android/gms/dynamite/zzo;
        //  1787: astore          10
        //  1789: aload           10
        //  1791: iload_3        
        //  1792: iconst_0       
        //  1793: invokespecial   com/google/android/gms/dynamite/zzo.<init>:(II)V
        //  1796: aload_1        
        //  1797: aload_0        
        //  1798: aload_2        
        //  1799: aload           10
        //  1801: invokeinterface com/google/android/gms/dynamite/DynamiteModule$VersionPolicy.selectModule:(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/dynamite/DynamiteModule$VersionPolicy$IVersions;)Lcom/google/android/gms/dynamite/DynamiteModule$VersionPolicy$SelectionResult;
        //  1806: getfield        com/google/android/gms/dynamite/DynamiteModule$VersionPolicy$SelectionResult.selection:I
        //  1809: iconst_m1      
        //  1810: if_icmpne       1873
        //  1813: aload           8
        //  1815: aload_2        
        //  1816: invokestatic    com/google/android/gms/dynamite/DynamiteModule.zzc:(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/dynamite/DynamiteModule;
        //  1819: astore_0       
        //  1820: lload           6
        //  1822: lconst_0       
        //  1823: lcmp           
        //  1824: ifne            1836
        //  1827: getstatic       com/google/android/gms/dynamite/DynamiteModule.zzh:Ljava/lang/ThreadLocal;
        //  1830: invokevirtual   java/lang/ThreadLocal.remove:()V
        //  1833: goto            1847
        //  1836: getstatic       com/google/android/gms/dynamite/DynamiteModule.zzh:Ljava/lang/ThreadLocal;
        //  1839: lload           6
        //  1841: invokestatic    java/lang/Long.valueOf:(J)Ljava/lang/Long;
        //  1844: invokevirtual   java/lang/ThreadLocal.set:(Ljava/lang/Object;)V
        //  1847: aload           14
        //  1849: getfield        com/google/android/gms/dynamite/zzn.zza:Landroid/database/Cursor;
        //  1852: astore_1       
        //  1853: aload_1        
        //  1854: ifnull          1863
        //  1857: aload_1        
        //  1858: invokeinterface android/database/Cursor.close:()V
        //  1863: getstatic       com/google/android/gms/dynamite/DynamiteModule.zzg:Ljava/lang/ThreadLocal;
        //  1866: aload           13
        //  1868: invokevirtual   java/lang/ThreadLocal.set:(Ljava/lang/Object;)V
        //  1871: aload_0        
        //  1872: areturn        
        //  1873: new             Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1876: astore_0       
        //  1877: aload_0        
        //  1878: ldc_w           "Remote load failed. No local fallback found."
        //  1881: aload           9
        //  1883: aconst_null    
        //  1884: invokespecial   com/google/android/gms/dynamite/DynamiteModule$LoadingException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;Lcom/google/android/gms/dynamite/zzp;)V
        //  1887: aload_0        
        //  1888: athrow         
        //  1889: new             Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1892: astore_1       
        //  1893: new             Ljava/lang/StringBuilder;
        //  1896: astore_0       
        //  1897: aload_0        
        //  1898: invokespecial   java/lang/StringBuilder.<init>:()V
        //  1901: aload_0        
        //  1902: ldc_w           "VersionPolicy returned invalid code:"
        //  1905: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1908: pop            
        //  1909: aload_0        
        //  1910: iload_3        
        //  1911: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //  1914: pop            
        //  1915: aload_1        
        //  1916: aload_0        
        //  1917: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //  1920: aconst_null    
        //  1921: invokespecial   com/google/android/gms/dynamite/DynamiteModule$LoadingException.<init>:(Ljava/lang/String;Lcom/google/android/gms/dynamite/zzp;)V
        //  1924: aload_1        
        //  1925: athrow         
        //  1926: new             Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1929: astore_0       
        //  1930: aload           15
        //  1932: getfield        com/google/android/gms/dynamite/DynamiteModule$VersionPolicy$SelectionResult.localVersion:I
        //  1935: istore_3       
        //  1936: aload           15
        //  1938: getfield        com/google/android/gms/dynamite/DynamiteModule$VersionPolicy$SelectionResult.remoteVersion:I
        //  1941: istore          4
        //  1943: new             Ljava/lang/StringBuilder;
        //  1946: astore_1       
        //  1947: aload_1        
        //  1948: invokespecial   java/lang/StringBuilder.<init>:()V
        //  1951: aload_1        
        //  1952: ldc_w           "No acceptable module "
        //  1955: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1958: pop            
        //  1959: aload_1        
        //  1960: aload_2        
        //  1961: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1964: pop            
        //  1965: aload_1        
        //  1966: ldc_w           " found. Local version is "
        //  1969: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1972: pop            
        //  1973: aload_1        
        //  1974: iload_3        
        //  1975: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //  1978: pop            
        //  1979: aload_1        
        //  1980: ldc_w           " and remote version is "
        //  1983: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1986: pop            
        //  1987: aload_1        
        //  1988: iload           4
        //  1990: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //  1993: pop            
        //  1994: aload_1        
        //  1995: ldc_w           "."
        //  1998: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  2001: pop            
        //  2002: aload_0        
        //  2003: aload_1        
        //  2004: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //  2007: aconst_null    
        //  2008: invokespecial   com/google/android/gms/dynamite/DynamiteModule$LoadingException.<init>:(Ljava/lang/String;Lcom/google/android/gms/dynamite/zzp;)V
        //  2011: aload_0        
        //  2012: athrow         
        //  2013: astore_0       
        //  2014: lload           6
        //  2016: lconst_0       
        //  2017: lcmp           
        //  2018: ifne            2030
        //  2021: getstatic       com/google/android/gms/dynamite/DynamiteModule.zzh:Ljava/lang/ThreadLocal;
        //  2024: invokevirtual   java/lang/ThreadLocal.remove:()V
        //  2027: goto            2041
        //  2030: getstatic       com/google/android/gms/dynamite/DynamiteModule.zzh:Ljava/lang/ThreadLocal;
        //  2033: lload           6
        //  2035: invokestatic    java/lang/Long.valueOf:(J)Ljava/lang/Long;
        //  2038: invokevirtual   java/lang/ThreadLocal.set:(Ljava/lang/Object;)V
        //  2041: aload           14
        //  2043: getfield        com/google/android/gms/dynamite/zzn.zza:Landroid/database/Cursor;
        //  2046: astore_1       
        //  2047: aload_1        
        //  2048: ifnull          2057
        //  2051: aload_1        
        //  2052: invokeinterface android/database/Cursor.close:()V
        //  2057: getstatic       com/google/android/gms/dynamite/DynamiteModule.zzg:Ljava/lang/ThreadLocal;
        //  2060: aload           13
        //  2062: invokevirtual   java/lang/ThreadLocal.set:(Ljava/lang/Object;)V
        //  2065: aload_0        
        //  2066: athrow         
        //  2067: new             Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  2070: dup            
        //  2071: ldc_w           "null application Context"
        //  2074: aconst_null    
        //  2075: invokespecial   com/google/android/gms/dynamite/DynamiteModule$LoadingException.<init>:(Ljava/lang/String;Lcom/google/android/gms/dynamite/zzp;)V
        //  2078: athrow         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                                             
        //  -----  -----  -----  -----  -----------------------------------------------------------------
        //  61     191    2013   2067   Any
        //  205    213    2013   2067   Any
        //  220    228    2013   2067   Any
        //  233    240    2013   2067   Any
        //  295    302    1727   1729   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  295    302    2013   2067   Any
        //  302    305    1685   1691   Landroid/os/RemoteException;
        //  302    305    1680   1682   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  302    305    1632   1638   Any
        //  305    320    1585   1591   Any
        //  325    390    1685   1691   Landroid/os/RemoteException;
        //  325    390    1680   1682   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  325    390    1632   1638   Any
        //  390    398    845    870    Any
        //  403    413    1685   1691   Landroid/os/RemoteException;
        //  403    413    1680   1682   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  403    413    1632   1638   Any
        //  418    447    1685   1691   Landroid/os/RemoteException;
        //  418    447    1680   1682   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  418    447    1632   1638   Any
        //  451    458    711    731    Any
        //  467    470    711    731    Any
        //  482    493    1627   1632   Landroid/os/RemoteException;
        //  482    493    1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  482    493    1609   1618   Any
        //  505    514    1627   1632   Landroid/os/RemoteException;
        //  505    514    1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  505    514    1609   1618   Any
        //  526    546    1627   1632   Landroid/os/RemoteException;
        //  526    546    1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  526    546    1609   1618   Any
        //  561    570    1627   1632   Landroid/os/RemoteException;
        //  561    570    1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  561    570    1609   1618   Any
        //  582    602    1627   1632   Landroid/os/RemoteException;
        //  582    602    1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  582    602    1609   1618   Any
        //  614    624    1627   1632   Landroid/os/RemoteException;
        //  614    624    1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  614    624    1609   1618   Any
        //  641    652    1627   1632   Landroid/os/RemoteException;
        //  641    652    1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  641    652    1609   1618   Any
        //  670    675    1627   1632   Landroid/os/RemoteException;
        //  670    675    1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  670    675    1609   1618   Any
        //  687    696    1627   1632   Landroid/os/RemoteException;
        //  687    696    1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  687    696    1609   1618   Any
        //  708    711    1627   1632   Landroid/os/RemoteException;
        //  708    711    1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  708    711    1609   1618   Any
        //  713    716    711    731    Any
        //  728    731    1627   1632   Landroid/os/RemoteException;
        //  728    731    1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  728    731    1609   1618   Any
        //  747    752    1627   1632   Landroid/os/RemoteException;
        //  747    752    1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  747    752    1609   1618   Any
        //  764    773    1627   1632   Landroid/os/RemoteException;
        //  764    773    1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  764    773    1609   1618   Any
        //  785    788    1627   1632   Landroid/os/RemoteException;
        //  785    788    1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  785    788    1609   1618   Any
        //  804    809    1627   1632   Landroid/os/RemoteException;
        //  804    809    1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  804    809    1609   1618   Any
        //  821    830    1627   1632   Landroid/os/RemoteException;
        //  821    830    1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  821    830    1609   1618   Any
        //  842    845    1627   1632   Landroid/os/RemoteException;
        //  842    845    1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  842    845    1609   1618   Any
        //  851    854    865    870    Any
        //  862    865    1627   1632   Landroid/os/RemoteException;
        //  862    865    1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  862    865    1609   1618   Any
        //  886    891    1627   1632   Landroid/os/RemoteException;
        //  886    891    1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  886    891    1609   1618   Any
        //  903    908    1627   1632   Landroid/os/RemoteException;
        //  903    908    1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  903    908    1609   1618   Any
        //  920    929    1627   1632   Landroid/os/RemoteException;
        //  920    929    1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  920    929    1609   1618   Any
        //  941    948    1627   1632   Landroid/os/RemoteException;
        //  941    948    1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  941    948    1609   1618   Any
        //  960    969    1627   1632   Landroid/os/RemoteException;
        //  960    969    1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  960    969    1609   1618   Any
        //  981    989    1627   1632   Landroid/os/RemoteException;
        //  981    989    1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  981    989    1609   1618   Any
        //  1001   1012   1627   1632   Landroid/os/RemoteException;
        //  1001   1012   1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1001   1012   1609   1618   Any
        //  1024   1030   1627   1632   Landroid/os/RemoteException;
        //  1024   1030   1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1024   1030   1609   1618   Any
        //  1047   1053   1627   1632   Landroid/os/RemoteException;
        //  1047   1053   1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1047   1053   1609   1618   Any
        //  1070   1080   1627   1632   Landroid/os/RemoteException;
        //  1070   1080   1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1070   1080   1609   1618   Any
        //  1097   1119   1627   1632   Landroid/os/RemoteException;
        //  1097   1119   1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1097   1119   1609   1618   Any
        //  1134   1139   1627   1632   Landroid/os/RemoteException;
        //  1134   1139   1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1134   1139   1609   1618   Any
        //  1151   1160   1627   1632   Landroid/os/RemoteException;
        //  1151   1160   1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1151   1160   1609   1618   Any
        //  1172   1175   1627   1632   Landroid/os/RemoteException;
        //  1172   1175   1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1172   1175   1609   1618   Any
        //  1192   1201   1627   1632   Landroid/os/RemoteException;
        //  1192   1201   1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1192   1201   1609   1618   Any
        //  1213   1227   1627   1632   Landroid/os/RemoteException;
        //  1213   1227   1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1213   1227   1609   1618   Any
        //  1242   1251   1627   1632   Landroid/os/RemoteException;
        //  1242   1251   1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1242   1251   1609   1618   Any
        //  1263   1277   1627   1632   Landroid/os/RemoteException;
        //  1263   1277   1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1263   1277   1609   1618   Any
        //  1289   1296   1627   1632   Landroid/os/RemoteException;
        //  1289   1296   1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1289   1296   1609   1618   Any
        //  1313   1327   1627   1632   Landroid/os/RemoteException;
        //  1313   1327   1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1313   1327   1609   1618   Any
        //  1392   1397   1627   1632   Landroid/os/RemoteException;
        //  1392   1397   1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1392   1397   1609   1618   Any
        //  1409   1418   1627   1632   Landroid/os/RemoteException;
        //  1409   1418   1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1409   1418   1609   1618   Any
        //  1430   1433   1627   1632   Landroid/os/RemoteException;
        //  1430   1433   1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1430   1433   1609   1618   Any
        //  1445   1450   1627   1632   Landroid/os/RemoteException;
        //  1445   1450   1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1445   1450   1609   1618   Any
        //  1462   1471   1627   1632   Landroid/os/RemoteException;
        //  1462   1471   1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1462   1471   1609   1618   Any
        //  1483   1486   1627   1632   Landroid/os/RemoteException;
        //  1483   1486   1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1483   1486   1609   1618   Any
        //  1502   1507   1627   1632   Landroid/os/RemoteException;
        //  1502   1507   1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1502   1507   1609   1618   Any
        //  1519   1528   1627   1632   Landroid/os/RemoteException;
        //  1519   1528   1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1519   1528   1609   1618   Any
        //  1540   1543   1627   1632   Landroid/os/RemoteException;
        //  1540   1543   1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1540   1543   1609   1618   Any
        //  1551   1556   1576   1585   Any
        //  1560   1569   1576   1585   Any
        //  1573   1576   1576   1585   Any
        //  1595   1598   1576   1585   Any
        //  1606   1609   1627   1632   Landroid/os/RemoteException;
        //  1606   1609   1618   1627   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1606   1609   1609   1618   Any
        //  1642   1649   1722   1727   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1642   1649   2013   2067   Any
        //  1653   1658   1722   1727   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1653   1658   2013   2067   Any
        //  1662   1673   1722   1727   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1662   1673   2013   2067   Any
        //  1677   1680   1722   1727   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1677   1680   2013   2067   Any
        //  1682   1685   1722   1727   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1682   1685   2013   2067   Any
        //  1695   1700   1722   1727   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1695   1700   2013   2067   Any
        //  1704   1715   1722   1727   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1704   1715   2013   2067   Any
        //  1719   1722   1722   1727   Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  1719   1722   2013   2067   Any
        //  1729   1780   2013   2067   Any
        //  1784   1820   2013   2067   Any
        //  1873   1889   2013   2067   Any
        //  1889   1926   2013   2067   Any
        //  1926   2013   2013   2067   Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0549:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static int zza(final Context p0, final String p1, final boolean p2) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     2: monitorenter   
        //     3: getstatic       com/google/android/gms/dynamite/DynamiteModule.zzb:Ljava/lang/Boolean;
        //     6: astore          10
        //     8: aconst_null    
        //     9: astore          8
        //    11: aconst_null    
        //    12: astore          9
        //    14: aconst_null    
        //    15: astore          7
        //    17: iconst_0       
        //    18: istore          4
        //    20: aload           10
        //    22: astore          6
        //    24: aload           10
        //    26: ifnonnull       392
        //    29: aload_0        
        //    30: invokevirtual   android/content/Context.getApplicationContext:()Landroid/content/Context;
        //    33: invokevirtual   android/content/Context.getClassLoader:()Ljava/lang/ClassLoader;
        //    36: ldc             Lcom/google/android/gms/dynamite/DynamiteModule$DynamiteLoaderClassLoader;.class
        //    38: invokevirtual   java/lang/Class.getName:()Ljava/lang/String;
        //    41: invokevirtual   java/lang/ClassLoader.loadClass:(Ljava/lang/String;)Ljava/lang/Class;
        //    44: ldc_w           "sClassLoader"
        //    47: invokevirtual   java/lang/Class.getDeclaredField:(Ljava/lang/String;)Ljava/lang/reflect/Field;
        //    50: astore          11
        //    52: aload           11
        //    54: invokevirtual   java/lang/reflect/Field.getDeclaringClass:()Ljava/lang/Class;
        //    57: astore          10
        //    59: aload           10
        //    61: monitorenter   
        //    62: aload           11
        //    64: aconst_null    
        //    65: invokevirtual   java/lang/reflect/Field.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //    68: checkcast       Ljava/lang/ClassLoader;
        //    71: astore          6
        //    73: aload           6
        //    75: invokestatic    java/lang/ClassLoader.getSystemClassLoader:()Ljava/lang/ClassLoader;
        //    78: if_acmpne       89
        //    81: getstatic       java/lang/Boolean.FALSE:Ljava/lang/Boolean;
        //    84: astore          6
        //    86: goto            311
        //    89: aload           6
        //    91: ifnull          107
        //    94: aload           6
        //    96: invokestatic    com/google/android/gms/dynamite/DynamiteModule.zzd:(Ljava/lang/ClassLoader;)V
        //    99: getstatic       java/lang/Boolean.TRUE:Ljava/lang/Boolean;
        //   102: astore          6
        //   104: goto            311
        //   107: aload_0        
        //   108: invokestatic    com/google/android/gms/dynamite/DynamiteModule.zzf:(Landroid/content/Context;)Z
        //   111: ifne            122
        //   114: aload           10
        //   116: monitorexit    
        //   117: ldc             Lcom/google/android/gms/dynamite/DynamiteModule;.class
        //   119: monitorexit    
        //   120: iconst_0       
        //   121: ireturn        
        //   122: getstatic       com/google/android/gms/dynamite/DynamiteModule.zzd:Z
        //   125: ifne            299
        //   128: getstatic       java/lang/Boolean.TRUE:Ljava/lang/Boolean;
        //   131: astore          12
        //   133: aload           12
        //   135: aconst_null    
        //   136: invokevirtual   java/lang/Boolean.equals:(Ljava/lang/Object;)Z
        //   139: istore          5
        //   141: iload           5
        //   143: ifeq            149
        //   146: goto            299
        //   149: aload_0        
        //   150: aload_1        
        //   151: iload_2        
        //   152: iconst_1       
        //   153: invokestatic    com/google/android/gms/dynamite/DynamiteModule.zzb:(Landroid/content/Context;Ljava/lang/String;ZZ)I
        //   156: istore_3       
        //   157: getstatic       com/google/android/gms/dynamite/DynamiteModule.zzc:Ljava/lang/String;
        //   160: astore          6
        //   162: aload           6
        //   164: ifnull          277
        //   167: aload           6
        //   169: invokevirtual   java/lang/String.isEmpty:()Z
        //   172: ifeq            178
        //   175: goto            277
        //   178: invokestatic    com/google/android/gms/dynamite/zzb.zza:()Ljava/lang/ClassLoader;
        //   181: astore          6
        //   183: aload           6
        //   185: ifnull          191
        //   188: goto            251
        //   191: getstatic       android/os/Build$VERSION.SDK_INT:I
        //   194: bipush          29
        //   196: if_icmplt       226
        //   199: invokestatic    vv.a:()V
        //   202: getstatic       com/google/android/gms/dynamite/DynamiteModule.zzc:Ljava/lang/String;
        //   205: astore          6
        //   207: aload           6
        //   209: invokestatic    com/google/android/gms/common/internal/Preconditions.checkNotNull:(Ljava/lang/Object;)Ljava/lang/Object;
        //   212: pop            
        //   213: aload           6
        //   215: invokestatic    java/lang/ClassLoader.getSystemClassLoader:()Ljava/lang/ClassLoader;
        //   218: invokestatic    uv.a:(Ljava/lang/String;Ljava/lang/ClassLoader;)Ldalvik/system/DelegateLastClassLoader;
        //   221: astore          6
        //   223: goto            251
        //   226: getstatic       com/google/android/gms/dynamite/DynamiteModule.zzc:Ljava/lang/String;
        //   229: astore          6
        //   231: aload           6
        //   233: invokestatic    com/google/android/gms/common/internal/Preconditions.checkNotNull:(Ljava/lang/Object;)Ljava/lang/Object;
        //   236: pop            
        //   237: new             Lcom/google/android/gms/dynamite/zzc;
        //   240: dup            
        //   241: aload           6
        //   243: invokestatic    java/lang/ClassLoader.getSystemClassLoader:()Ljava/lang/ClassLoader;
        //   246: invokespecial   com/google/android/gms/dynamite/zzc.<init>:(Ljava/lang/String;Ljava/lang/ClassLoader;)V
        //   249: astore          6
        //   251: aload           6
        //   253: invokestatic    com/google/android/gms/dynamite/DynamiteModule.zzd:(Ljava/lang/ClassLoader;)V
        //   256: aload           11
        //   258: aconst_null    
        //   259: aload           6
        //   261: invokevirtual   java/lang/reflect/Field.set:(Ljava/lang/Object;Ljava/lang/Object;)V
        //   264: aload           12
        //   266: putstatic       com/google/android/gms/dynamite/DynamiteModule.zzb:Ljava/lang/Boolean;
        //   269: aload           10
        //   271: monitorexit    
        //   272: ldc             Lcom/google/android/gms/dynamite/DynamiteModule;.class
        //   274: monitorexit    
        //   275: iload_3        
        //   276: ireturn        
        //   277: aload           10
        //   279: monitorexit    
        //   280: ldc             Lcom/google/android/gms/dynamite/DynamiteModule;.class
        //   282: monitorexit    
        //   283: iload_3        
        //   284: ireturn        
        //   285: astore          6
        //   287: aload           11
        //   289: aconst_null    
        //   290: invokestatic    java/lang/ClassLoader.getSystemClassLoader:()Ljava/lang/ClassLoader;
        //   293: invokevirtual   java/lang/reflect/Field.set:(Ljava/lang/Object;Ljava/lang/Object;)V
        //   296: goto            81
        //   299: aload           11
        //   301: aconst_null    
        //   302: invokestatic    java/lang/ClassLoader.getSystemClassLoader:()Ljava/lang/ClassLoader;
        //   305: invokevirtual   java/lang/reflect/Field.set:(Ljava/lang/Object;Ljava/lang/Object;)V
        //   308: goto            81
        //   311: aload           10
        //   313: monitorexit    
        //   314: goto            387
        //   317: astore          6
        //   319: aload           10
        //   321: monitorexit    
        //   322: aload           6
        //   324: athrow         
        //   325: astore          6
        //   327: goto            337
        //   330: astore          6
        //   332: goto            337
        //   335: astore          6
        //   337: aload           6
        //   339: invokevirtual   java/lang/Object.toString:()Ljava/lang/String;
        //   342: astore          6
        //   344: new             Ljava/lang/StringBuilder;
        //   347: astore          10
        //   349: aload           10
        //   351: invokespecial   java/lang/StringBuilder.<init>:()V
        //   354: aload           10
        //   356: ldc_w           "Failed to load module via V2: "
        //   359: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   362: pop            
        //   363: aload           10
        //   365: aload           6
        //   367: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   370: pop            
        //   371: ldc             "DynamiteModule"
        //   373: aload           10
        //   375: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   378: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;)I
        //   381: pop            
        //   382: getstatic       java/lang/Boolean.FALSE:Ljava/lang/Boolean;
        //   385: astore          6
        //   387: aload           6
        //   389: putstatic       com/google/android/gms/dynamite/DynamiteModule.zzb:Ljava/lang/Boolean;
        //   392: ldc             Lcom/google/android/gms/dynamite/DynamiteModule;.class
        //   394: monitorexit    
        //   395: aload           6
        //   397: invokevirtual   java/lang/Boolean.booleanValue:()Z
        //   400: istore          5
        //   402: iload           5
        //   404: ifeq            462
        //   407: aload_0        
        //   408: aload_1        
        //   409: iload_2        
        //   410: iconst_0       
        //   411: invokestatic    com/google/android/gms/dynamite/DynamiteModule.zzb:(Landroid/content/Context;Ljava/lang/String;ZZ)I
        //   414: istore_3       
        //   415: iload_3        
        //   416: ireturn        
        //   417: astore_1       
        //   418: aload_1        
        //   419: invokevirtual   java/lang/Throwable.getMessage:()Ljava/lang/String;
        //   422: astore_1       
        //   423: new             Ljava/lang/StringBuilder;
        //   426: astore          6
        //   428: aload           6
        //   430: invokespecial   java/lang/StringBuilder.<init>:()V
        //   433: aload           6
        //   435: ldc_w           "Failed to retrieve remote module version: "
        //   438: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   441: pop            
        //   442: aload           6
        //   444: aload_1        
        //   445: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   448: pop            
        //   449: ldc             "DynamiteModule"
        //   451: aload           6
        //   453: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   456: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;)I
        //   459: pop            
        //   460: iconst_0       
        //   461: ireturn        
        //   462: aload_0        
        //   463: invokestatic    com/google/android/gms/dynamite/DynamiteModule.zzg:(Landroid/content/Context;)Lcom/google/android/gms/dynamite/zzq;
        //   466: astore          10
        //   468: aload           10
        //   470: ifnonnull       479
        //   473: iload           4
        //   475: istore_3       
        //   476: goto            840
        //   479: aload           9
        //   481: astore          6
        //   483: aload           10
        //   485: invokevirtual   com/google/android/gms/dynamite/zzq.zze:()I
        //   488: istore_3       
        //   489: iload_3        
        //   490: iconst_3       
        //   491: if_icmplt       684
        //   494: aload           9
        //   496: astore          6
        //   498: getstatic       com/google/android/gms/dynamite/DynamiteModule.zzg:Ljava/lang/ThreadLocal;
        //   501: invokevirtual   java/lang/ThreadLocal.get:()Ljava/lang/Object;
        //   504: checkcast       Lcom/google/android/gms/dynamite/zzn;
        //   507: astore          11
        //   509: aload           11
        //   511: ifnull          546
        //   514: aload           9
        //   516: astore          6
        //   518: aload           11
        //   520: getfield        com/google/android/gms/dynamite/zzn.zza:Landroid/database/Cursor;
        //   523: astore          11
        //   525: aload           11
        //   527: ifnull          546
        //   530: aload           9
        //   532: astore          6
        //   534: aload           11
        //   536: iconst_0       
        //   537: invokeinterface android/database/Cursor.getInt:(I)I
        //   542: istore_3       
        //   543: goto            840
        //   546: aload           9
        //   548: astore          6
        //   550: aload           10
        //   552: aload_0        
        //   553: invokestatic    com/google/android/gms/dynamic/ObjectWrapper.wrap:(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;
        //   556: aload_1        
        //   557: iload_2        
        //   558: getstatic       com/google/android/gms/dynamite/DynamiteModule.zzh:Ljava/lang/ThreadLocal;
        //   561: invokevirtual   java/lang/ThreadLocal.get:()Ljava/lang/Object;
        //   564: checkcast       Ljava/lang/Long;
        //   567: invokevirtual   java/lang/Long.longValue:()J
        //   570: invokevirtual   com/google/android/gms/dynamite/zzq.zzk:(Lcom/google/android/gms/dynamic/IObjectWrapper;Ljava/lang/String;ZJ)Lcom/google/android/gms/dynamic/IObjectWrapper;
        //   573: invokestatic    com/google/android/gms/dynamic/ObjectWrapper.unwrap:(Lcom/google/android/gms/dynamic/IObjectWrapper;)Ljava/lang/Object;
        //   576: checkcast       Landroid/database/Cursor;
        //   579: astore_1       
        //   580: aload_1        
        //   581: ifnull          636
        //   584: aload_1        
        //   585: invokeinterface android/database/Cursor.moveToFirst:()Z
        //   590: ifne            596
        //   593: goto            636
        //   596: aload_1        
        //   597: iconst_0       
        //   598: invokeinterface android/database/Cursor.getInt:(I)I
        //   603: istore_3       
        //   604: iload_3        
        //   605: ifle            623
        //   608: aload_1        
        //   609: invokestatic    com/google/android/gms/dynamite/DynamiteModule.zze:(Landroid/database/Cursor;)Z
        //   612: istore_2       
        //   613: iload_2        
        //   614: ifeq            623
        //   617: aload           7
        //   619: astore_1       
        //   620: goto            623
        //   623: aload_1        
        //   624: ifnull          633
        //   627: aload_1        
        //   628: invokeinterface android/database/Cursor.close:()V
        //   633: goto            840
        //   636: ldc             "DynamiteModule"
        //   638: ldc_w           "Failed to retrieve remote module version."
        //   641: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;)I
        //   644: pop            
        //   645: iload           4
        //   647: istore_3       
        //   648: aload_1        
        //   649: ifnull          840
        //   652: aload_1        
        //   653: invokeinterface android/database/Cursor.close:()V
        //   658: iload           4
        //   660: istore_3       
        //   661: goto            840
        //   664: astore          7
        //   666: aload_1        
        //   667: astore          6
        //   669: aload           7
        //   671: astore_1       
        //   672: goto            846
        //   675: astore          6
        //   677: aload           6
        //   679: astore          7
        //   681: goto            761
        //   684: iload_3        
        //   685: iconst_2       
        //   686: if_icmpne       721
        //   689: aload           9
        //   691: astore          6
        //   693: ldc             "DynamiteModule"
        //   695: ldc_w           "IDynamite loader version = 2, no high precision latency measurement."
        //   698: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;)I
        //   701: pop            
        //   702: aload           9
        //   704: astore          6
        //   706: aload           10
        //   708: aload_0        
        //   709: invokestatic    com/google/android/gms/dynamic/ObjectWrapper.wrap:(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;
        //   712: aload_1        
        //   713: iload_2        
        //   714: invokevirtual   com/google/android/gms/dynamite/zzq.zzg:(Lcom/google/android/gms/dynamic/IObjectWrapper;Ljava/lang/String;Z)I
        //   717: istore_3       
        //   718: goto            840
        //   721: aload           9
        //   723: astore          6
        //   725: ldc             "DynamiteModule"
        //   727: ldc_w           "IDynamite loader version < 2, falling back to getModuleVersion2"
        //   730: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;)I
        //   733: pop            
        //   734: aload           9
        //   736: astore          6
        //   738: aload           10
        //   740: aload_0        
        //   741: invokestatic    com/google/android/gms/dynamic/ObjectWrapper.wrap:(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/IObjectWrapper;
        //   744: aload_1        
        //   745: iload_2        
        //   746: invokevirtual   com/google/android/gms/dynamite/zzq.zzf:(Lcom/google/android/gms/dynamic/IObjectWrapper;Ljava/lang/String;Z)I
        //   749: istore_3       
        //   750: goto            840
        //   753: goto            846
        //   756: astore          7
        //   758: aload           8
        //   760: astore_1       
        //   761: aload_1        
        //   762: astore          6
        //   764: aload           7
        //   766: invokevirtual   java/lang/Throwable.getMessage:()Ljava/lang/String;
        //   769: astore          8
        //   771: aload_1        
        //   772: astore          6
        //   774: new             Ljava/lang/StringBuilder;
        //   777: astore          7
        //   779: aload_1        
        //   780: astore          6
        //   782: aload           7
        //   784: invokespecial   java/lang/StringBuilder.<init>:()V
        //   787: aload_1        
        //   788: astore          6
        //   790: aload           7
        //   792: ldc_w           "Failed to retrieve remote module version: "
        //   795: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   798: pop            
        //   799: aload_1        
        //   800: astore          6
        //   802: aload           7
        //   804: aload           8
        //   806: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   809: pop            
        //   810: aload_1        
        //   811: astore          6
        //   813: ldc             "DynamiteModule"
        //   815: aload           7
        //   817: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   820: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;)I
        //   823: pop            
        //   824: iload           4
        //   826: istore_3       
        //   827: aload_1        
        //   828: ifnull          840
        //   831: aload_1        
        //   832: invokeinterface android/database/Cursor.close:()V
        //   837: iload           4
        //   839: istore_3       
        //   840: iload_3        
        //   841: ireturn        
        //   842: astore_1       
        //   843: goto            753
        //   846: aload           6
        //   848: ifnull          858
        //   851: aload           6
        //   853: invokeinterface android/database/Cursor.close:()V
        //   858: aload_1        
        //   859: athrow         
        //   860: astore_1       
        //   861: ldc             Lcom/google/android/gms/dynamite/DynamiteModule;.class
        //   863: monitorexit    
        //   864: aload_1        
        //   865: athrow         
        //   866: astore_1       
        //   867: aload_0        
        //   868: aload_1        
        //   869: invokestatic    com/google/android/gms/common/util/CrashUtils.addDynamiteErrorToDropBox:(Landroid/content/Context;Ljava/lang/Throwable;)Z
        //   872: pop            
        //   873: aload_1        
        //   874: athrow         
        //   875: astore          6
        //   877: goto            99
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                                             
        //  -----  -----  -----  -----  -----------------------------------------------------------------
        //  0      3      866    875    Any
        //  3      8      860    866    Any
        //  29     62     335    337    Ljava/lang/ClassNotFoundException;
        //  29     62     330    335    Ljava/lang/IllegalAccessException;
        //  29     62     325    330    Ljava/lang/NoSuchFieldException;
        //  29     62     860    866    Any
        //  62     81     317    325    Any
        //  81     86     317    325    Any
        //  94     99     875    880    Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  94     99     317    325    Any
        //  99     104    317    325    Any
        //  107    117    317    325    Any
        //  117    120    860    866    Any
        //  122    141    317    325    Any
        //  149    162    285    299    Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  149    162    317    325    Any
        //  167    175    285    299    Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  167    175    317    325    Any
        //  178    183    285    299    Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  178    183    317    325    Any
        //  191    223    285    299    Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  191    223    317    325    Any
        //  226    251    285    299    Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  226    251    317    325    Any
        //  251    269    285    299    Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  251    269    317    325    Any
        //  269    272    317    325    Any
        //  272    275    860    866    Any
        //  277    280    317    325    Any
        //  280    283    860    866    Any
        //  287    296    317    325    Any
        //  299    308    317    325    Any
        //  311    314    317    325    Any
        //  319    322    317    325    Any
        //  322    325    335    337    Ljava/lang/ClassNotFoundException;
        //  322    325    330    335    Ljava/lang/IllegalAccessException;
        //  322    325    325    330    Ljava/lang/NoSuchFieldException;
        //  322    325    860    866    Any
        //  337    387    860    866    Any
        //  387    392    860    866    Any
        //  392    395    860    866    Any
        //  395    402    866    875    Any
        //  407    415    417    462    Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //  407    415    866    875    Any
        //  418    460    866    875    Any
        //  462    468    866    875    Any
        //  483    489    756    761    Landroid/os/RemoteException;
        //  483    489    842    756    Any
        //  498    509    756    761    Landroid/os/RemoteException;
        //  498    509    842    756    Any
        //  518    525    756    761    Landroid/os/RemoteException;
        //  518    525    842    756    Any
        //  534    543    756    761    Landroid/os/RemoteException;
        //  534    543    842    756    Any
        //  550    580    756    761    Landroid/os/RemoteException;
        //  550    580    842    756    Any
        //  584    593    675    684    Landroid/os/RemoteException;
        //  584    593    664    675    Any
        //  596    604    675    684    Landroid/os/RemoteException;
        //  596    604    664    675    Any
        //  608    613    675    684    Landroid/os/RemoteException;
        //  608    613    664    675    Any
        //  627    633    866    875    Any
        //  636    645    675    684    Landroid/os/RemoteException;
        //  636    645    664    675    Any
        //  652    658    866    875    Any
        //  693    702    756    761    Landroid/os/RemoteException;
        //  693    702    842    756    Any
        //  706    718    756    761    Landroid/os/RemoteException;
        //  706    718    842    756    Any
        //  725    734    756    761    Landroid/os/RemoteException;
        //  725    734    842    756    Any
        //  738    750    756    761    Landroid/os/RemoteException;
        //  738    750    842    756    Any
        //  764    771    842    756    Any
        //  774    779    842    756    Any
        //  782    787    842    756    Any
        //  790    799    842    756    Any
        //  802    810    842    756    Any
        //  813    824    842    756    Any
        //  831    837    866    875    Any
        //  851    858    866    875    Any
        //  858    860    866    875    Any
        //  861    864    860    866    Any
        //  864    866    866    875    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0099:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private static int zzb(final Context p0, final String p1, final boolean p2, final boolean p3) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore          10
        //     3: getstatic       com/google/android/gms/dynamite/DynamiteModule.zzh:Ljava/lang/ThreadLocal;
        //     6: invokevirtual   java/lang/ThreadLocal.get:()Ljava/lang/Object;
        //     9: checkcast       Ljava/lang/Long;
        //    12: invokevirtual   java/lang/Long.longValue:()J
        //    15: lstore          8
        //    17: aload_0        
        //    18: invokevirtual   android/content/Context.getContentResolver:()Landroid/content/ContentResolver;
        //    21: astore          11
        //    23: ldc_w           "api_force_staging"
        //    26: astore_0       
        //    27: iconst_1       
        //    28: istore          7
        //    30: iconst_1       
        //    31: iload_2        
        //    32: if_icmpeq       39
        //    35: ldc_w           "api"
        //    38: astore_0       
        //    39: new             Landroid/net/Uri$Builder;
        //    42: astore          12
        //    44: aload           12
        //    46: invokespecial   android/net/Uri$Builder.<init>:()V
        //    49: aload           11
        //    51: aload           12
        //    53: ldc_w           "content"
        //    56: invokevirtual   android/net/Uri$Builder.scheme:(Ljava/lang/String;)Landroid/net/Uri$Builder;
        //    59: ldc_w           "com.google.android.gms.chimera"
        //    62: invokevirtual   android/net/Uri$Builder.authority:(Ljava/lang/String;)Landroid/net/Uri$Builder;
        //    65: aload_0        
        //    66: invokevirtual   android/net/Uri$Builder.path:(Ljava/lang/String;)Landroid/net/Uri$Builder;
        //    69: aload_1        
        //    70: invokevirtual   android/net/Uri$Builder.appendPath:(Ljava/lang/String;)Landroid/net/Uri$Builder;
        //    73: ldc_w           "requestStartTime"
        //    76: lload           8
        //    78: invokestatic    java/lang/String.valueOf:(J)Ljava/lang/String;
        //    81: invokevirtual   android/net/Uri$Builder.appendQueryParameter:(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;
        //    84: invokevirtual   android/net/Uri$Builder.build:()Landroid/net/Uri;
        //    87: aconst_null    
        //    88: aconst_null    
        //    89: aconst_null    
        //    90: aconst_null    
        //    91: invokevirtual   android/content/ContentResolver.query:(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
        //    94: astore          11
        //    96: aload           11
        //    98: ifnull          349
        //   101: aload           11
        //   103: astore          10
        //   105: aload           11
        //   107: astore_0       
        //   108: aload           11
        //   110: invokeinterface android/database/Cursor.moveToFirst:()Z
        //   115: ifeq            349
        //   118: iconst_0       
        //   119: istore          6
        //   121: iconst_0       
        //   122: istore_2       
        //   123: aload           11
        //   125: astore          10
        //   127: aload           11
        //   129: astore_0       
        //   130: aload           11
        //   132: iconst_0       
        //   133: invokeinterface android/database/Cursor.getInt:(I)I
        //   138: istore          4
        //   140: aload           11
        //   142: astore_1       
        //   143: iload           4
        //   145: ifle            288
        //   148: aload           11
        //   150: astore          10
        //   152: aload           11
        //   154: astore_0       
        //   155: ldc             Lcom/google/android/gms/dynamite/DynamiteModule;.class
        //   157: monitorenter   
        //   158: aload           11
        //   160: iconst_2       
        //   161: invokeinterface android/database/Cursor.getString:(I)Ljava/lang/String;
        //   166: putstatic       com/google/android/gms/dynamite/DynamiteModule.zzc:Ljava/lang/String;
        //   169: aload           11
        //   171: ldc_w           "loaderVersion"
        //   174: invokeinterface android/database/Cursor.getColumnIndex:(Ljava/lang/String;)I
        //   179: istore          5
        //   181: iload           5
        //   183: iflt            198
        //   186: aload           11
        //   188: iload           5
        //   190: invokeinterface android/database/Cursor.getInt:(I)I
        //   195: putstatic       com/google/android/gms/dynamite/DynamiteModule.zze:I
        //   198: aload           11
        //   200: ldc_w           "disableStandaloneDynamiteLoader2"
        //   203: invokeinterface android/database/Cursor.getColumnIndex:(Ljava/lang/String;)I
        //   208: istore          5
        //   210: iload           5
        //   212: iflt            239
        //   215: aload           11
        //   217: iload           5
        //   219: invokeinterface android/database/Cursor.getInt:(I)I
        //   224: ifeq            233
        //   227: iload           7
        //   229: istore_2       
        //   230: goto            235
        //   233: iconst_0       
        //   234: istore_2       
        //   235: iload_2        
        //   236: putstatic       com/google/android/gms/dynamite/DynamiteModule.zzd:Z
        //   239: ldc             Lcom/google/android/gms/dynamite/DynamiteModule;.class
        //   241: monitorexit    
        //   242: aload           11
        //   244: astore          10
        //   246: aload           11
        //   248: astore_0       
        //   249: aload           11
        //   251: invokestatic    com/google/android/gms/dynamite/DynamiteModule.zze:(Landroid/database/Cursor;)Z
        //   254: istore          7
        //   256: aload           11
        //   258: astore_1       
        //   259: iload_2        
        //   260: istore          6
        //   262: iload           7
        //   264: ifeq            288
        //   267: aconst_null    
        //   268: astore_1       
        //   269: iload_2        
        //   270: istore          6
        //   272: goto            288
        //   275: astore_1       
        //   276: ldc             Lcom/google/android/gms/dynamite/DynamiteModule;.class
        //   278: monitorexit    
        //   279: aload           11
        //   281: astore          10
        //   283: aload           11
        //   285: astore_0       
        //   286: aload_1        
        //   287: athrow         
        //   288: iload_3        
        //   289: ifeq            336
        //   292: iload           6
        //   294: ifne            300
        //   297: goto            336
        //   300: aload_1        
        //   301: astore          10
        //   303: aload_1        
        //   304: astore_0       
        //   305: new             Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //   308: astore          11
        //   310: aload_1        
        //   311: astore          10
        //   313: aload_1        
        //   314: astore_0       
        //   315: aload           11
        //   317: ldc_w           "forcing fallback to container DynamiteLoader impl"
        //   320: aconst_null    
        //   321: invokespecial   com/google/android/gms/dynamite/DynamiteModule$LoadingException.<init>:(Ljava/lang/String;Lcom/google/android/gms/dynamite/zzp;)V
        //   324: aload_1        
        //   325: astore          10
        //   327: aload_1        
        //   328: astore_0       
        //   329: aload           11
        //   331: athrow         
        //   332: astore_1       
        //   333: goto            410
        //   336: aload_1        
        //   337: ifnull          346
        //   340: aload_1        
        //   341: invokeinterface android/database/Cursor.close:()V
        //   346: iload           4
        //   348: ireturn        
        //   349: aload           11
        //   351: astore          10
        //   353: aload           11
        //   355: astore_0       
        //   356: ldc             "DynamiteModule"
        //   358: ldc_w           "Failed to retrieve remote module version."
        //   361: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;)I
        //   364: pop            
        //   365: aload           11
        //   367: astore          10
        //   369: aload           11
        //   371: astore_0       
        //   372: new             Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //   375: astore_1       
        //   376: aload           11
        //   378: astore          10
        //   380: aload           11
        //   382: astore_0       
        //   383: aload_1        
        //   384: ldc_w           "Failed to connect to dynamite module ContentResolver."
        //   387: aconst_null    
        //   388: invokespecial   com/google/android/gms/dynamite/DynamiteModule$LoadingException.<init>:(Ljava/lang/String;Lcom/google/android/gms/dynamite/zzp;)V
        //   391: aload           11
        //   393: astore          10
        //   395: aload           11
        //   397: astore_0       
        //   398: aload_1        
        //   399: athrow         
        //   400: astore_0       
        //   401: aload_0        
        //   402: astore_1       
        //   403: goto            506
        //   406: astore_1       
        //   407: aconst_null    
        //   408: astore          10
        //   410: aload           10
        //   412: astore_0       
        //   413: aload_1        
        //   414: instanceof      Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //   417: ifeq            425
        //   420: aload           10
        //   422: astore_0       
        //   423: aload_1        
        //   424: athrow         
        //   425: aload           10
        //   427: astore_0       
        //   428: new             Lcom/google/android/gms/dynamite/DynamiteModule$LoadingException;
        //   431: astore          12
        //   433: aload           10
        //   435: astore_0       
        //   436: aload_1        
        //   437: invokevirtual   java/lang/Throwable.getMessage:()Ljava/lang/String;
        //   440: astore          13
        //   442: aload           10
        //   444: astore_0       
        //   445: new             Ljava/lang/StringBuilder;
        //   448: astore          11
        //   450: aload           10
        //   452: astore_0       
        //   453: aload           11
        //   455: invokespecial   java/lang/StringBuilder.<init>:()V
        //   458: aload           10
        //   460: astore_0       
        //   461: aload           11
        //   463: ldc_w           "V2 version check failed: "
        //   466: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   469: pop            
        //   470: aload           10
        //   472: astore_0       
        //   473: aload           11
        //   475: aload           13
        //   477: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   480: pop            
        //   481: aload           10
        //   483: astore_0       
        //   484: aload           12
        //   486: aload           11
        //   488: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   491: aload_1        
        //   492: aconst_null    
        //   493: invokespecial   com/google/android/gms/dynamite/DynamiteModule$LoadingException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;Lcom/google/android/gms/dynamite/zzp;)V
        //   496: aload           10
        //   498: astore_0       
        //   499: aload           12
        //   501: athrow         
        //   502: astore_1       
        //   503: aload_0        
        //   504: astore          10
        //   506: aload           10
        //   508: ifnull          518
        //   511: aload           10
        //   513: invokeinterface android/database/Cursor.close:()V
        //   518: aload_1        
        //   519: athrow         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  3      23     406    410    Ljava/lang/Exception;
        //  3      23     400    406    Any
        //  39     96     406    410    Ljava/lang/Exception;
        //  39     96     400    406    Any
        //  108    118    332    336    Ljava/lang/Exception;
        //  108    118    502    506    Any
        //  130    140    332    336    Ljava/lang/Exception;
        //  130    140    502    506    Any
        //  155    158    332    336    Ljava/lang/Exception;
        //  155    158    502    506    Any
        //  158    181    275    288    Any
        //  186    198    275    288    Any
        //  198    210    275    288    Any
        //  215    227    275    288    Any
        //  235    239    275    288    Any
        //  239    242    275    288    Any
        //  249    256    332    336    Ljava/lang/Exception;
        //  249    256    502    506    Any
        //  276    279    275    288    Any
        //  286    288    332    336    Ljava/lang/Exception;
        //  286    288    502    506    Any
        //  305    310    332    336    Ljava/lang/Exception;
        //  305    310    502    506    Any
        //  315    324    332    336    Ljava/lang/Exception;
        //  315    324    502    506    Any
        //  329    332    332    336    Ljava/lang/Exception;
        //  329    332    502    506    Any
        //  356    365    332    336    Ljava/lang/Exception;
        //  356    365    502    506    Any
        //  372    376    332    336    Ljava/lang/Exception;
        //  372    376    502    506    Any
        //  383    391    332    336    Ljava/lang/Exception;
        //  383    391    502    506    Any
        //  398    400    332    336    Ljava/lang/Exception;
        //  398    400    502    506    Any
        //  413    420    502    506    Any
        //  423    425    502    506    Any
        //  428    433    502    506    Any
        //  436    442    502    506    Any
        //  445    450    502    506    Any
        //  453    458    502    506    Any
        //  461    470    502    506    Any
        //  473    481    502    506    Any
        //  484    496    502    506    Any
        //  499    502    502    506    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0198:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private static DynamiteModule zzc(final Context context, final String obj) {
        Log.i("DynamiteModule", "Selected local version of ".concat(String.valueOf(obj)));
        return new DynamiteModule(context);
    }
    
    private static void zzd(ClassLoader queryLocalInterface) {
        try {
            final IBinder binder = (IBinder)((ClassLoader)queryLocalInterface).loadClass("com.google.android.gms.dynamiteloader.DynamiteLoaderV2").getConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
            if (binder == null) {
                queryLocalInterface = null;
            }
            else {
                queryLocalInterface = (NoSuchMethodException)binder.queryLocalInterface("com.google.android.gms.dynamite.IDynamiteLoaderV2");
                if (queryLocalInterface instanceof zzr) {
                    queryLocalInterface = queryLocalInterface;
                }
                else {
                    queryLocalInterface = (NoSuchMethodException)new zzr(binder);
                }
            }
            DynamiteModule.zzl = (zzr)queryLocalInterface;
            return;
        }
        catch (final NoSuchMethodException queryLocalInterface) {}
        catch (final InvocationTargetException queryLocalInterface) {}
        catch (final InstantiationException queryLocalInterface) {}
        catch (final IllegalAccessException queryLocalInterface) {}
        catch (final ClassNotFoundException ex) {}
        throw new LoadingException("Failed to instantiate dynamite loader", queryLocalInterface, null);
    }
    
    private static boolean zze(final Cursor zza) {
        final zzn zzn = DynamiteModule.zzg.get();
        if (zzn != null && zzn.zza == null) {
            zzn.zza = zza;
            return true;
        }
        return false;
    }
    
    private static boolean zzf(final Context context) {
        final Boolean true = Boolean.TRUE;
        if (true.equals(null)) {
            return true;
        }
        if (true.equals(DynamiteModule.zzf)) {
            return true;
        }
        final Boolean zzf = DynamiteModule.zzf;
        boolean b = false;
        final boolean b2 = false;
        if (zzf == null) {
            final ProviderInfo resolveContentProvider = context.getPackageManager().resolveContentProvider("com.google.android.gms.chimera", 0);
            boolean b3 = b2;
            if (GoogleApiAvailabilityLight.getInstance().isGooglePlayServicesAvailable(context, 10000000) == 0) {
                b3 = b2;
                if (resolveContentProvider != null) {
                    b3 = b2;
                    if ("com.google.android.gms".equals(resolveContentProvider.packageName)) {
                        b3 = true;
                    }
                }
            }
            final boolean booleanValue = DynamiteModule.zzf = b3;
            if (b = booleanValue) {
                final ApplicationInfo applicationInfo = resolveContentProvider.applicationInfo;
                b = booleanValue;
                if (applicationInfo != null) {
                    b = booleanValue;
                    if ((applicationInfo.flags & 0x81) == 0x0) {
                        Log.i("DynamiteModule", "Non-system-image GmsCore APK, forcing V1");
                        DynamiteModule.zzd = true;
                        b = booleanValue;
                    }
                }
            }
        }
        if (!b) {
            Log.e("DynamiteModule", "Invalid GmsCore APK, remote loading disabled.");
        }
        return b;
    }
    
    private static zzq zzg(final Context context) {
        synchronized (DynamiteModule.class) {
            final zzq zzk = DynamiteModule.zzk;
            if (zzk != null) {
                return zzk;
            }
            try {
                final IBinder binder = (IBinder)context.createPackageContext("com.google.android.gms", 3).getClassLoader().loadClass("com.google.android.gms.chimera.container.DynamiteLoaderImpl").newInstance();
                zzq zzk2;
                if (binder == null) {
                    zzk2 = null;
                }
                else {
                    final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.dynamite.IDynamiteLoader");
                    if (queryLocalInterface instanceof zzq) {
                        zzk2 = (zzq)queryLocalInterface;
                    }
                    else {
                        zzk2 = new zzq(binder);
                    }
                }
                if (zzk2 != null) {
                    return DynamiteModule.zzk = zzk2;
                }
            }
            catch (final Exception ex) {
                final String message = ex.getMessage();
                final StringBuilder sb = new StringBuilder();
                sb.append("Failed to load IDynamiteLoader from GmsCore: ");
                sb.append(message);
                Log.e("DynamiteModule", sb.toString());
            }
            return null;
        }
    }
    
    @ResultIgnorabilityUnspecified
    @KeepForSdk
    public Context getModuleContext() {
        return this.zzj;
    }
    
    @KeepForSdk
    public IBinder instantiate(final String s) {
        IBinder binder = null;
        try {
            binder = (IBinder)this.zzj.getClassLoader().loadClass(s).newInstance();
            return binder;
        }
        catch (final IllegalAccessException binder) {}
        catch (final InstantiationException binder) {}
        catch (final ClassNotFoundException ex) {}
        throw new LoadingException("Failed to instantiate module class: ".concat(String.valueOf(s)), (Throwable)binder, null);
    }
    
    @DynamiteApi
    public static class DynamiteLoaderClassLoader
    {
        public static ClassLoader sClassLoader;
    }
    
    @KeepForSdk
    public static class LoadingException extends Exception
    {
    }
    
    public interface VersionPolicy
    {
        @KeepForSdk
        SelectionResult selectModule(final Context p0, final String p1, final IVersions p2);
        
        @KeepForSdk
        public interface IVersions
        {
            int zza(final Context p0, final String p1);
            
            int zzb(final Context p0, final String p1, final boolean p2);
        }
        
        @KeepForSdk
        public static class SelectionResult
        {
            @KeepForSdk
            public int localVersion;
            @KeepForSdk
            public int remoteVersion;
            @KeepForSdk
            public int selection;
            
            public SelectionResult() {
                this.localVersion = 0;
                this.remoteVersion = 0;
                this.selection = 0;
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.dynamite;

import android.content.Context;

final class zzf implements VersionPolicy
{
    public zzf() {
    }
    
    @Override
    public final SelectionResult selectModule(final Context context, final String s, final IVersions versions) {
        final SelectionResult selectionResult = new SelectionResult();
        final int zzb = versions.zzb(context, s, true);
        selectionResult.remoteVersion = zzb;
        if (zzb != 0) {
            selectionResult.selection = 1;
        }
        else if ((selectionResult.localVersion = versions.zza(context, s)) != 0) {
            selectionResult.selection = -1;
        }
        return selectionResult;
    }
}

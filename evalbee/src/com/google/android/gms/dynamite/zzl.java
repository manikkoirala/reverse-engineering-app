// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.dynamite;

import android.content.Context;

final class zzl implements VersionPolicy
{
    public zzl() {
    }
    
    @Override
    public final SelectionResult selectModule(final Context context, final String s, final IVersions versions) {
        final SelectionResult selectionResult = new SelectionResult();
        final int zza = versions.zza(context, s);
        selectionResult.localVersion = zza;
        int n = 0;
        int remoteVersion;
        if (zza != 0) {
            remoteVersion = versions.zzb(context, s, false);
        }
        else {
            remoteVersion = versions.zzb(context, s, true);
        }
        selectionResult.remoteVersion = remoteVersion;
        final int localVersion = selectionResult.localVersion;
        if (localVersion == 0) {
            if (remoteVersion == 0) {
                selectionResult.selection = 0;
                return selectionResult;
            }
        }
        else {
            n = localVersion;
        }
        if (remoteVersion >= n) {
            selectionResult.selection = 1;
        }
        else {
            selectionResult.selection = -1;
        }
        return selectionResult;
    }
}

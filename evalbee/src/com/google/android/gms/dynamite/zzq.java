// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.dynamite;

import android.os.IInterface;
import com.google.android.gms.internal.common.zzc;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.Parcel;
import android.os.IBinder;
import com.google.android.gms.internal.common.zza;

public final class zzq extends zza
{
    public zzq(final IBinder binder) {
        super(binder, "com.google.android.gms.dynamite.IDynamiteLoader");
    }
    
    public final int zze() {
        final Parcel zzB = this.zzB(6, this.zza());
        final int int1 = zzB.readInt();
        zzB.recycle();
        return int1;
    }
    
    public final int zzf(final IObjectWrapper objectWrapper, final String s, final boolean b) {
        final Parcel zza = this.zza();
        zzc.zze(zza, (IInterface)objectWrapper);
        zza.writeString(s);
        zza.writeInt((int)(b ? 1 : 0));
        final Parcel zzB = this.zzB(3, zza);
        final int int1 = zzB.readInt();
        zzB.recycle();
        return int1;
    }
    
    public final int zzg(final IObjectWrapper objectWrapper, final String s, final boolean b) {
        final Parcel zza = this.zza();
        zzc.zze(zza, (IInterface)objectWrapper);
        zza.writeString(s);
        zza.writeInt((int)(b ? 1 : 0));
        final Parcel zzB = this.zzB(5, zza);
        final int int1 = zzB.readInt();
        zzB.recycle();
        return int1;
    }
    
    public final IObjectWrapper zzh(final IObjectWrapper objectWrapper, final String s, final int n) {
        final Parcel zza = this.zza();
        zzc.zze(zza, (IInterface)objectWrapper);
        zza.writeString(s);
        zza.writeInt(n);
        final Parcel zzB = this.zzB(2, zza);
        final IObjectWrapper interface1 = IObjectWrapper.Stub.asInterface(zzB.readStrongBinder());
        zzB.recycle();
        return interface1;
    }
    
    public final IObjectWrapper zzi(final IObjectWrapper objectWrapper, final String s, final int n, final IObjectWrapper objectWrapper2) {
        final Parcel zza = this.zza();
        zzc.zze(zza, (IInterface)objectWrapper);
        zza.writeString(s);
        zza.writeInt(n);
        zzc.zze(zza, (IInterface)objectWrapper2);
        final Parcel zzB = this.zzB(8, zza);
        final IObjectWrapper interface1 = IObjectWrapper.Stub.asInterface(zzB.readStrongBinder());
        zzB.recycle();
        return interface1;
    }
    
    public final IObjectWrapper zzj(final IObjectWrapper objectWrapper, final String s, final int n) {
        final Parcel zza = this.zza();
        zzc.zze(zza, (IInterface)objectWrapper);
        zza.writeString(s);
        zza.writeInt(n);
        final Parcel zzB = this.zzB(4, zza);
        final IObjectWrapper interface1 = IObjectWrapper.Stub.asInterface(zzB.readStrongBinder());
        zzB.recycle();
        return interface1;
    }
    
    public final IObjectWrapper zzk(final IObjectWrapper objectWrapper, final String s, final boolean b, final long n) {
        final Parcel zza = this.zza();
        zzc.zze(zza, (IInterface)objectWrapper);
        zza.writeString(s);
        zza.writeInt((int)(b ? 1 : 0));
        zza.writeLong(n);
        final Parcel zzB = this.zzB(7, zza);
        final IObjectWrapper interface1 = IObjectWrapper.Stub.asInterface(zzB.readStrongBinder());
        zzB.recycle();
        return interface1;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.dynamite;

import android.content.Context;

final class zzo implements IVersions
{
    private final int zza;
    
    public zzo(final int zza, final int n) {
        this.zza = zza;
    }
    
    @Override
    public final int zza(final Context context, final String s) {
        return this.zza;
    }
    
    @Override
    public final int zzb(final Context context, final String s, final boolean b) {
        return 0;
    }
}

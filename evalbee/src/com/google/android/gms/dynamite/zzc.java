// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.dynamite;

import dalvik.system.PathClassLoader;

final class zzc extends PathClassLoader
{
    public zzc(final String s, final ClassLoader classLoader) {
        super(s, classLoader);
    }
    
    public final Class loadClass(final String name, final boolean b) {
        Label_0026: {
            if (name.startsWith("java.") || name.startsWith("android.")) {
                break Label_0026;
            }
            try {
                return ((ClassLoader)this).findClass(name);
                return super.loadClass(name, b);
            }
            catch (final ClassNotFoundException ex) {
                return super.loadClass(name, b);
            }
        }
    }
}

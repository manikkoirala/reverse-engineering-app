// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.appset;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

public class AppSetIdInfo
{
    public static final int SCOPE_APP = 1;
    public static final int SCOPE_DEVELOPER = 2;
    private final String zza;
    private final int zzb;
    
    public AppSetIdInfo(final String zza, final int zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public String getId() {
        return this.zza;
    }
    
    public int getScope() {
        return this.zzb;
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface Scope {
    }
}

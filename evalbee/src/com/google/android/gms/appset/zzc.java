// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.appset;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "AppSetInfoParcelCreator")
public final class zzc extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zzc> CREATOR;
    @Field(getter = "getId", id = 1)
    private final String zza;
    @Field(getter = "getScope", id = 2)
    private final int zzb;
    
    static {
        CREATOR = (Parcelable$Creator)new zzd();
    }
    
    @Constructor
    public zzc(@Param(id = 1) final String zza, @Param(id = 2) final int zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.zza, false);
        SafeParcelWriter.writeInt(parcel, 2, this.zzb);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public final int zza() {
        return this.zzb;
    }
    
    public final String zzb() {
        return this.zza;
    }
}

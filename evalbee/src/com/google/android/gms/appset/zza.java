// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.appset;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "AppSetIdRequestParamsCreator")
public final class zza extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zza> CREATOR;
    @Field(getter = "getVersion", id = 1)
    private final String zza;
    @Field(getter = "getClientAppPackageName", id = 2)
    private final String zzb;
    
    static {
        CREATOR = (Parcelable$Creator)new zzb();
    }
    
    @Constructor
    public zza(@Param(id = 1) final String zza, @Param(id = 2) final String zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.zza, false);
        SafeParcelWriter.writeString(parcel, 2, this.zzb, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.util.concurrent;

import com.google.android.gms.common.internal.Preconditions;
import java.util.concurrent.Executors;
import com.google.android.gms.common.annotation.KeepForSdk;
import java.util.concurrent.ThreadFactory;

@KeepForSdk
public class NamedThreadFactory implements ThreadFactory
{
    private final String zza;
    private final ThreadFactory zzb;
    
    @KeepForSdk
    public NamedThreadFactory(final String zza) {
        this.zzb = Executors.defaultThreadFactory();
        Preconditions.checkNotNull(zza, "Name must not be null");
        this.zza = zza;
    }
    
    @Override
    public final Thread newThread(final Runnable runnable) {
        final Thread thread = this.zzb.newThread(new zza(runnable, 0));
        thread.setName(this.zza);
        return thread;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.util;

import org.checkerframework.checker.nullness.qual.EnsuresNonNullIf;
import android.text.TextUtils;
import java.util.regex.Pattern;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public class Strings
{
    private static final Pattern zza;
    
    static {
        zza = Pattern.compile("\\$\\{(.*?)\\}");
    }
    
    private Strings() {
    }
    
    @KeepForSdk
    public static String emptyToNull(final String s) {
        String s2 = s;
        if (TextUtils.isEmpty((CharSequence)s)) {
            s2 = null;
        }
        return s2;
    }
    
    @EnsuresNonNullIf(expression = { "#1" }, result = false)
    @KeepForSdk
    public static boolean isEmptyOrWhitespace(final String s) {
        return s == null || s.trim().isEmpty();
    }
}

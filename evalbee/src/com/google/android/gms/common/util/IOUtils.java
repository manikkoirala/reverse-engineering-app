// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.util;

import com.google.android.gms.common.internal.Preconditions;
import java.io.ByteArrayOutputStream;
import com.google.errorprone.annotations.ResultIgnorabilityUnspecified;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.Closeable;
import java.io.IOException;
import android.os.ParcelFileDescriptor;
import com.google.android.gms.common.internal.ShowFirstParty;
import com.google.android.gms.common.annotation.KeepForSdk;

@Deprecated
@KeepForSdk
@ShowFirstParty
public final class IOUtils
{
    private IOUtils() {
    }
    
    @KeepForSdk
    public static void closeQuietly(final ParcelFileDescriptor parcelFileDescriptor) {
        if (parcelFileDescriptor == null) {
            return;
        }
        try {
            parcelFileDescriptor.close();
        }
        catch (final IOException ex) {}
    }
    
    @KeepForSdk
    public static void closeQuietly(final Closeable closeable) {
        if (closeable == null) {
            return;
        }
        try {
            closeable.close();
        }
        catch (final IOException ex) {}
    }
    
    @ResultIgnorabilityUnspecified
    @Deprecated
    @KeepForSdk
    public static long copyStream(final InputStream inputStream, final OutputStream outputStream) {
        return copyStream(inputStream, outputStream, false, 1024);
    }
    
    @ResultIgnorabilityUnspecified
    @Deprecated
    @KeepForSdk
    public static long copyStream(final InputStream inputStream, final OutputStream outputStream, final boolean b, final int len) {
        final byte[] array = new byte[len];
        long n = 0L;
        try {
            while (true) {
                final int read = inputStream.read(array, 0, len);
                if (read == -1) {
                    break;
                }
                n += read;
                outputStream.write(array, 0, read);
            }
            if (b) {
                closeQuietly(inputStream);
                closeQuietly(outputStream);
            }
            return n;
        }
        finally {
            if (b) {
                closeQuietly(inputStream);
                closeQuietly(outputStream);
            }
        }
    }
    
    @KeepForSdk
    public static boolean isGzipByteBuffer(final byte[] array) {
        return array.length > 1 && ((array[1] & 0xFF) << 8 | (array[0] & 0xFF)) == 0x8B1F;
    }
    
    @Deprecated
    @KeepForSdk
    public static byte[] readInputStreamFully(final InputStream inputStream) {
        return readInputStreamFully(inputStream, true);
    }
    
    @Deprecated
    @KeepForSdk
    public static byte[] readInputStreamFully(final InputStream inputStream, final boolean b) {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        copyStream(inputStream, byteArrayOutputStream, b, 1024);
        return byteArrayOutputStream.toByteArray();
    }
    
    @Deprecated
    @KeepForSdk
    public static byte[] toByteArray(final InputStream inputStream) {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Preconditions.checkNotNull(inputStream);
        Preconditions.checkNotNull(byteArrayOutputStream);
        final byte[] array = new byte[4096];
        while (true) {
            final int read = inputStream.read(array);
            if (read == -1) {
                break;
            }
            byteArrayOutputStream.write(array, 0, read);
        }
        return byteArrayOutputStream.toByteArray();
    }
}

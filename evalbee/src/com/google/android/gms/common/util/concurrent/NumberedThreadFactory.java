// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.util.concurrent;

import com.google.android.gms.common.internal.Preconditions;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import com.google.android.gms.common.annotation.KeepForSdk;
import java.util.concurrent.ThreadFactory;

@KeepForSdk
public class NumberedThreadFactory implements ThreadFactory
{
    private final String zza;
    private final AtomicInteger zzb;
    private final ThreadFactory zzc;
    
    @KeepForSdk
    public NumberedThreadFactory(final String zza) {
        this.zzb = new AtomicInteger();
        this.zzc = Executors.defaultThreadFactory();
        Preconditions.checkNotNull(zza, "Name must not be null");
        this.zza = zza;
    }
    
    @Override
    public final Thread newThread(final Runnable runnable) {
        final Thread thread = this.zzc.newThread(new zza(runnable, 0));
        final String zza = this.zza;
        final int andIncrement = this.zzb.getAndIncrement();
        final StringBuilder sb = new StringBuilder();
        sb.append(zza);
        sb.append("[");
        sb.append(andIncrement);
        sb.append("]");
        thread.setName(sb.toString());
        return thread;
    }
}

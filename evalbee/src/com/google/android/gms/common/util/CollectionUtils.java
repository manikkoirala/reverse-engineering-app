// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.util;

import java.util.HashSet;
import java.util.HashMap;
import java.util.Set;
import java.util.Map;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Collection;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public final class CollectionUtils
{
    private CollectionUtils() {
    }
    
    @KeepForSdk
    public static boolean isEmpty(final Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }
    
    @Deprecated
    @KeepForSdk
    public static <T> List<T> listOf() {
        return Collections.emptyList();
    }
    
    @Deprecated
    @KeepForSdk
    public static <T> List<T> listOf(final T o) {
        return Collections.singletonList(o);
    }
    
    @Deprecated
    @KeepForSdk
    public static <T> List<T> listOf(final T... a) {
        final int length = a.length;
        if (length == 0) {
            return Collections.emptyList();
        }
        if (length != 1) {
            return Collections.unmodifiableList((List<? extends T>)Arrays.asList((T[])a));
        }
        return Collections.singletonList(a[0]);
    }
    
    @KeepForSdk
    public static <K, V> Map<K, V> mapOf(final K k, final V v, final K i, final V v2, final K j, final V v3) {
        final Map zza = zza(3, false);
        zza.put(k, v);
        zza.put(i, v2);
        zza.put(j, v3);
        return (Map<K, V>)Collections.unmodifiableMap((Map<?, ?>)zza);
    }
    
    @KeepForSdk
    public static <K, V> Map<K, V> mapOf(final K k, final V v, final K i, final V v2, final K j, final V v3, final K l, final V v4, final K m, final V v5, final K k2, final V v6) {
        final Map zza = zza(6, false);
        zza.put(k, v);
        zza.put(i, v2);
        zza.put(j, v3);
        zza.put(l, v4);
        zza.put(m, v5);
        zza.put(k2, v6);
        return (Map<K, V>)Collections.unmodifiableMap((Map<?, ?>)zza);
    }
    
    @KeepForSdk
    public static <K, V> Map<K, V> mapOfKeyValueArrays(final K[] array, final V[] array2) {
        final int length = array.length;
        final int length2 = array2.length;
        if (length != length2) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Key and values array lengths not equal: ");
            sb.append(length);
            sb.append(" != ");
            sb.append(length2);
            throw new IllegalArgumentException(sb.toString());
        }
        if (length == 0) {
            return Collections.emptyMap();
        }
        int i = 0;
        if (length != 1) {
            final Map zza = zza(length, false);
            while (i < array.length) {
                zza.put(array[i], array2[i]);
                ++i;
            }
            return (Map<K, V>)Collections.unmodifiableMap((Map<?, ?>)zza);
        }
        return Collections.singletonMap(array[0], array2[0]);
    }
    
    @KeepForSdk
    public static <T> Set<T> mutableSetOfWithSize(final int n) {
        Set zzb;
        if (n == 0) {
            zzb = new s8();
        }
        else {
            zzb = zzb(n, true);
        }
        return zzb;
    }
    
    @Deprecated
    @KeepForSdk
    public static <T> Set<T> setOf(final T t, final T t2, final T t3) {
        final Set zzb = zzb(3, false);
        zzb.add(t);
        zzb.add(t2);
        zzb.add(t3);
        return (Set<T>)Collections.unmodifiableSet((Set<?>)zzb);
    }
    
    @Deprecated
    @KeepForSdk
    public static <T> Set<T> setOf(final T... elements) {
        final int length = elements.length;
        if (length == 0) {
            return Collections.emptySet();
        }
        if (length == 1) {
            return Collections.singleton(elements[0]);
        }
        if (length == 2) {
            final T t = elements[0];
            final T t2 = elements[1];
            final Set zzb = zzb(2, false);
            zzb.add(t);
            zzb.add(t2);
            return (Set<T>)Collections.unmodifiableSet((Set<?>)zzb);
        }
        if (length == 3) {
            return setOf(elements[0], elements[1], elements[2]);
        }
        if (length != 4) {
            final Set zzb2 = zzb(length, false);
            Collections.addAll(zzb2, elements);
            return (Set<T>)Collections.unmodifiableSet((Set<?>)zzb2);
        }
        final T t3 = elements[0];
        final T t4 = elements[1];
        final T t5 = elements[2];
        final T t6 = elements[3];
        final Set zzb3 = zzb(4, false);
        zzb3.add(t3);
        zzb3.add(t4);
        zzb3.add(t5);
        zzb3.add(t6);
        return (Set<T>)Collections.unmodifiableSet((Set<?>)zzb3);
    }
    
    private static Map zza(final int initialCapacity, final boolean b) {
        Map map;
        if (initialCapacity <= 256) {
            map = new r8(initialCapacity);
        }
        else {
            map = new HashMap(initialCapacity, 1.0f);
        }
        return map;
    }
    
    private static Set zzb(final int initialCapacity, final boolean b) {
        int n;
        if (!b) {
            n = 256;
        }
        else {
            n = 128;
        }
        Collection collection;
        if (initialCapacity <= n) {
            collection = new s8(initialCapacity);
        }
        else {
            float loadFactor;
            if (!b) {
                loadFactor = 1.0f;
            }
            else {
                loadFactor = 0.75f;
            }
            collection = new HashSet(initialCapacity, loadFactor);
        }
        return (Set)collection;
    }
}

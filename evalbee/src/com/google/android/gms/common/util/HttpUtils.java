// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Iterator;
import com.google.android.gms.internal.common.zzx;
import com.google.android.gms.internal.common.zzo;
import java.util.HashMap;
import java.util.Collections;
import java.util.Map;
import java.net.URI;
import java.util.regex.Pattern;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public class HttpUtils
{
    private static final Pattern zza;
    private static final Pattern zzb;
    private static final Pattern zzc;
    
    static {
        zza = Pattern.compile("^(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}$");
        zzb = Pattern.compile("^(?:[0-9a-fA-F]{1,4}:){7}[0-9a-fA-F]{1,4}$");
        zzc = Pattern.compile("^((?:[0-9A-Fa-f]{1,4}(?::[0-9A-Fa-f]{1,4})*)?)::((?:[0-9A-Fa-f]{1,4}(?::[0-9A-Fa-f]{1,4})*)?)$");
    }
    
    private HttpUtils() {
    }
    
    @KeepForSdk
    public static Map<String, String> parse(final URI uri, final String s) {
        final Map<Object, Object> emptyMap = Collections.emptyMap();
        final String rawQuery = uri.getRawQuery();
        Map<Object, Object> map = emptyMap;
        if (rawQuery != null) {
            map = emptyMap;
            if (rawQuery.length() > 0) {
                final HashMap hashMap = new HashMap();
                final zzx zzc = zzx.zzc(zzo.zzb('='));
                final Iterator iterator = zzx.zzc(zzo.zzb('&')).zzb().zzd((CharSequence)rawQuery).iterator();
                while (true) {
                    map = hashMap;
                    if (!iterator.hasNext()) {
                        break;
                    }
                    final List zzf = zzc.zzf((CharSequence)iterator.next());
                    if (zzf.isEmpty() || zzf.size() > 2) {
                        throw new IllegalArgumentException("bad parameter");
                    }
                    final String zza = zza(zzf.get(0), s);
                    String zza2;
                    if (zzf.size() == 2) {
                        zza2 = zza(zzf.get(1), s);
                    }
                    else {
                        zza2 = null;
                    }
                    hashMap.put(zza, zza2);
                }
            }
        }
        return (Map<String, String>)map;
    }
    
    private static String zza(String decode, final String s) {
        String enc = s;
        if (s == null) {
            enc = "ISO-8859-1";
        }
        try {
            decode = URLDecoder.decode(decode, enc);
            return decode;
        }
        catch (final UnsupportedEncodingException cause) {
            throw new IllegalArgumentException(cause);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.util;

import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.api.Scope;
import java.util.Set;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public final class ScopeUtil
{
    private ScopeUtil() {
    }
    
    @KeepForSdk
    public static String[] toScopeString(final Set<Scope> set) {
        Preconditions.checkNotNull(set, "scopes can't be null.");
        final Scope[] array = set.toArray(new Scope[set.size()]);
        Preconditions.checkNotNull(array, "scopes can't be null.");
        final String[] array2 = new String[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = array[i].getScopeUri();
        }
        return array2;
    }
}

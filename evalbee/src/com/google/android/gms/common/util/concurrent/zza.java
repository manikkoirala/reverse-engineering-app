// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.util.concurrent;

import android.os.Process;

final class zza implements Runnable
{
    private final Runnable zza;
    
    public zza(final Runnable zza, final int n) {
        this.zza = zza;
    }
    
    @Override
    public final void run() {
        Process.setThreadPriority(0);
        this.zza.run();
    }
}

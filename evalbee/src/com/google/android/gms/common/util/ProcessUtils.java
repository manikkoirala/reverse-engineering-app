// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.util;

import android.os.StrictMode$ThreadPolicy;
import java.io.IOException;
import com.google.android.gms.common.internal.Preconditions;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.FileReader;
import android.os.StrictMode;
import android.os.Process;
import android.os.Build$VERSION;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public class ProcessUtils
{
    private static String zza;
    private static int zzb;
    
    private ProcessUtils() {
    }
    
    @KeepForSdk
    public static String getMyProcessName() {
        if (ProcessUtils.zza != null) {
            goto Label_0181;
        }
        if (Build$VERSION.SDK_INT >= 28) {
            ProcessUtils.zza = l81.a();
            goto Label_0181;
        }
        int zzb;
        if ((zzb = ProcessUtils.zzb) == 0) {
            zzb = (ProcessUtils.zzb = Process.myPid());
        }
        String s = null;
        if (zzb <= 0) {
            goto Label_0177;
        }
        try {
            final StringBuilder sb = new StringBuilder();
            sb.append("/proc/");
            sb.append(zzb);
            sb.append("/cmdline");
            final String string = sb.toString();
            final StrictMode$ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
            try {
                final BufferedReader bufferedReader = new BufferedReader(new FileReader(string));
                StrictMode.setThreadPolicy(allowThreadDiskReads);
                try {
                    s = bufferedReader.readLine();
                    Preconditions.checkNotNull(s);
                    s = s.trim();
                }
                catch (final IOException s) {}
            }
            finally {}
        }
        catch (final IOException ex) {}
    }
}

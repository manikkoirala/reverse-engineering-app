// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.util;

import java.util.regex.Matcher;
import android.text.TextUtils;
import java.util.regex.Pattern;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public final class JsonUtils
{
    private static final Pattern zza;
    private static final Pattern zzb;
    
    static {
        zza = Pattern.compile("\\\\.");
        zzb = Pattern.compile("[\\\\\"/\b\f\n\r\t]");
    }
    
    private JsonUtils() {
    }
    
    @KeepForSdk
    public static boolean areJsonValuesEquivalent(final Object p0, final Object p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: ifnonnull       13
        //     4: aload_1        
        //     5: ifnull          11
        //     8: goto            13
        //    11: iconst_1       
        //    12: ireturn        
        //    13: aload_0        
        //    14: ifnull          210
        //    17: aload_1        
        //    18: ifnonnull       24
        //    21: goto            210
        //    24: aload_0        
        //    25: instanceof      Lorg/json/JSONObject;
        //    28: ifeq            129
        //    31: aload_1        
        //    32: instanceof      Lorg/json/JSONObject;
        //    35: ifeq            129
        //    38: aload_0        
        //    39: checkcast       Lorg/json/JSONObject;
        //    42: astore_0       
        //    43: aload_1        
        //    44: checkcast       Lorg/json/JSONObject;
        //    47: astore          4
        //    49: aload_0        
        //    50: invokevirtual   org/json/JSONObject.length:()I
        //    53: aload           4
        //    55: invokevirtual   org/json/JSONObject.length:()I
        //    58: if_icmpeq       63
        //    61: iconst_0       
        //    62: ireturn        
        //    63: aload_0        
        //    64: invokevirtual   org/json/JSONObject.keys:()Ljava/util/Iterator;
        //    67: astore          5
        //    69: aload           5
        //    71: invokeinterface java/util/Iterator.hasNext:()Z
        //    76: ifeq            127
        //    79: aload           5
        //    81: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //    86: checkcast       Ljava/lang/String;
        //    89: astore_1       
        //    90: aload           4
        //    92: aload_1        
        //    93: invokevirtual   org/json/JSONObject.has:(Ljava/lang/String;)Z
        //    96: ifne            101
        //    99: iconst_0       
        //   100: ireturn        
        //   101: aload_1        
        //   102: invokestatic    com/google/android/gms/common/internal/Preconditions.checkNotNull:(Ljava/lang/Object;)Ljava/lang/Object;
        //   105: pop            
        //   106: aload_0        
        //   107: aload_1        
        //   108: invokevirtual   org/json/JSONObject.get:(Ljava/lang/String;)Ljava/lang/Object;
        //   111: aload           4
        //   113: aload_1        
        //   114: invokevirtual   org/json/JSONObject.get:(Ljava/lang/String;)Ljava/lang/Object;
        //   117: invokestatic    com/google/android/gms/common/util/JsonUtils.areJsonValuesEquivalent:(Ljava/lang/Object;Ljava/lang/Object;)Z
        //   120: istore_3       
        //   121: iload_3        
        //   122: ifne            69
        //   125: iconst_0       
        //   126: ireturn        
        //   127: iconst_1       
        //   128: ireturn        
        //   129: aload_0        
        //   130: instanceof      Lorg/json/JSONArray;
        //   133: ifeq            204
        //   136: aload_1        
        //   137: instanceof      Lorg/json/JSONArray;
        //   140: ifeq            204
        //   143: aload_0        
        //   144: checkcast       Lorg/json/JSONArray;
        //   147: astore_0       
        //   148: aload_1        
        //   149: checkcast       Lorg/json/JSONArray;
        //   152: astore_1       
        //   153: aload_0        
        //   154: invokevirtual   org/json/JSONArray.length:()I
        //   157: aload_1        
        //   158: invokevirtual   org/json/JSONArray.length:()I
        //   161: if_icmpne       202
        //   164: iconst_0       
        //   165: istore_2       
        //   166: iload_2        
        //   167: aload_0        
        //   168: invokevirtual   org/json/JSONArray.length:()I
        //   171: if_icmpge       200
        //   174: aload_0        
        //   175: iload_2        
        //   176: invokevirtual   org/json/JSONArray.get:(I)Ljava/lang/Object;
        //   179: aload_1        
        //   180: iload_2        
        //   181: invokevirtual   org/json/JSONArray.get:(I)Ljava/lang/Object;
        //   184: invokestatic    com/google/android/gms/common/util/JsonUtils.areJsonValuesEquivalent:(Ljava/lang/Object;Ljava/lang/Object;)Z
        //   187: istore_3       
        //   188: iload_3        
        //   189: ifeq            198
        //   192: iinc            2, 1
        //   195: goto            166
        //   198: iconst_0       
        //   199: ireturn        
        //   200: iconst_1       
        //   201: ireturn        
        //   202: iconst_0       
        //   203: ireturn        
        //   204: aload_0        
        //   205: aload_1        
        //   206: invokevirtual   java/lang/Object.equals:(Ljava/lang/Object;)Z
        //   209: ireturn        
        //   210: iconst_0       
        //   211: ireturn        
        //   212: astore_0       
        //   213: goto            125
        //   216: astore_0       
        //   217: goto            198
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                    
        //  -----  -----  -----  -----  ------------------------
        //  101    121    212    216    Lorg/json/JSONException;
        //  174    188    216    220    Lorg/json/JSONException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0198:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @KeepForSdk
    public static String escapeString(final String input) {
        String string = input;
        if (!TextUtils.isEmpty((CharSequence)input)) {
            final Matcher matcher = JsonUtils.zzb.matcher(input);
            StringBuffer sb = null;
            while (matcher.find()) {
                StringBuffer sb2;
                if ((sb2 = sb) == null) {
                    sb2 = new StringBuffer();
                }
                final char char1 = matcher.group().charAt(0);
                String replacement;
                if (char1 != '\f') {
                    if (char1 != '\r') {
                        if (char1 != '\"') {
                            if (char1 != '/') {
                                if (char1 != '\\') {
                                    switch (char1) {
                                        default: {
                                            sb = sb2;
                                            continue;
                                        }
                                        case 10: {
                                            replacement = "\\\\n";
                                            break;
                                        }
                                        case 9: {
                                            replacement = "\\\\t";
                                            break;
                                        }
                                        case 8: {
                                            replacement = "\\\\b";
                                            break;
                                        }
                                    }
                                }
                                else {
                                    replacement = "\\\\\\\\";
                                }
                            }
                            else {
                                replacement = "\\\\/";
                            }
                        }
                        else {
                            replacement = "\\\\\\\"";
                        }
                    }
                    else {
                        replacement = "\\\\r";
                    }
                }
                else {
                    replacement = "\\\\f";
                }
                matcher.appendReplacement(sb2, replacement);
                sb = sb2;
            }
            if (sb == null) {
                return input;
            }
            matcher.appendTail(sb);
            string = sb.toString();
        }
        return string;
    }
    
    @KeepForSdk
    public static String unescapeString(String replacement) {
        String string = replacement;
        if (!TextUtils.isEmpty((CharSequence)replacement)) {
            final String zza = zzc.zza(replacement);
            final Matcher matcher = JsonUtils.zza.matcher(zza);
            StringBuffer sb = null;
            while (matcher.find()) {
                StringBuffer sb2;
                if ((sb2 = sb) == null) {
                    sb2 = new StringBuffer();
                }
                final char char1 = matcher.group().charAt(1);
                if (char1 != '\"') {
                    if (char1 != '/') {
                        if (char1 != '\\') {
                            if (char1 != 'b') {
                                if (char1 != 'f') {
                                    if (char1 != 'n') {
                                        if (char1 != 'r') {
                                            if (char1 != 't') {
                                                throw new IllegalStateException("Found an escaped character that should never be.");
                                            }
                                            replacement = "\t";
                                        }
                                        else {
                                            replacement = "\r";
                                        }
                                    }
                                    else {
                                        replacement = "\n";
                                    }
                                }
                                else {
                                    replacement = "\f";
                                }
                            }
                            else {
                                replacement = "\b";
                            }
                        }
                        else {
                            replacement = "\\\\";
                        }
                    }
                    else {
                        replacement = "/";
                    }
                }
                else {
                    replacement = "\"";
                }
                matcher.appendReplacement(sb2, replacement);
                sb = sb2;
            }
            if (sb == null) {
                return zza;
            }
            matcher.appendTail(sb);
            string = sb.toString();
        }
        return string;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.util;

import java.util.ArrayList;
import java.util.List;
import com.google.android.gms.common.internal.Preconditions;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager$NameNotFoundException;
import com.google.android.gms.common.wrappers.Wrappers;
import android.content.Context;
import android.util.Log;
import android.os.WorkSource;
import java.lang.reflect.Method;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public class WorkSourceUtil
{
    private static final int zza;
    private static final Method zzb;
    private static final Method zzc;
    private static final Method zzd;
    private static final Method zze;
    private static final Method zzf;
    private static final Method zzg;
    private static final Method zzh;
    private static final Method zzi;
    private static Boolean zzj;
    
    static {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: putstatic       com/google/android/gms/common/util/WorkSourceUtil.zza:I
        //     6: ldc             Landroid/os/WorkSource;.class
        //     8: ldc             "add"
        //    10: iconst_1       
        //    11: anewarray       Ljava/lang/Class;
        //    14: dup            
        //    15: iconst_0       
        //    16: getstatic       java/lang/Integer.TYPE:Ljava/lang/Class;
        //    19: aastore        
        //    20: invokevirtual   java/lang/Class.getMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //    23: astore_0       
        //    24: goto            30
        //    27: astore_0       
        //    28: aconst_null    
        //    29: astore_0       
        //    30: aload_0        
        //    31: putstatic       com/google/android/gms/common/util/WorkSourceUtil.zzb:Ljava/lang/reflect/Method;
        //    34: invokestatic    com/google/android/gms/common/util/PlatformVersion.isAtLeastJellyBeanMR2:()Z
        //    37: ifeq            66
        //    40: ldc             Landroid/os/WorkSource;.class
        //    42: ldc             "add"
        //    44: iconst_2       
        //    45: anewarray       Ljava/lang/Class;
        //    48: dup            
        //    49: iconst_0       
        //    50: getstatic       java/lang/Integer.TYPE:Ljava/lang/Class;
        //    53: aastore        
        //    54: dup            
        //    55: iconst_1       
        //    56: ldc             Ljava/lang/String;.class
        //    58: aastore        
        //    59: invokevirtual   java/lang/Class.getMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //    62: astore_0       
        //    63: goto            68
        //    66: aconst_null    
        //    67: astore_0       
        //    68: aload_0        
        //    69: putstatic       com/google/android/gms/common/util/WorkSourceUtil.zzc:Ljava/lang/reflect/Method;
        //    72: ldc             Landroid/os/WorkSource;.class
        //    74: ldc             "size"
        //    76: iconst_0       
        //    77: anewarray       Ljava/lang/Class;
        //    80: invokevirtual   java/lang/Class.getMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //    83: astore_0       
        //    84: goto            90
        //    87: astore_0       
        //    88: aconst_null    
        //    89: astore_0       
        //    90: aload_0        
        //    91: putstatic       com/google/android/gms/common/util/WorkSourceUtil.zzd:Ljava/lang/reflect/Method;
        //    94: ldc             Landroid/os/WorkSource;.class
        //    96: ldc             "get"
        //    98: iconst_1       
        //    99: anewarray       Ljava/lang/Class;
        //   102: dup            
        //   103: iconst_0       
        //   104: getstatic       java/lang/Integer.TYPE:Ljava/lang/Class;
        //   107: aastore        
        //   108: invokevirtual   java/lang/Class.getMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //   111: astore_0       
        //   112: goto            118
        //   115: astore_0       
        //   116: aconst_null    
        //   117: astore_0       
        //   118: aload_0        
        //   119: putstatic       com/google/android/gms/common/util/WorkSourceUtil.zze:Ljava/lang/reflect/Method;
        //   122: invokestatic    com/google/android/gms/common/util/PlatformVersion.isAtLeastJellyBeanMR2:()Z
        //   125: ifeq            149
        //   128: ldc             Landroid/os/WorkSource;.class
        //   130: ldc             "getName"
        //   132: iconst_1       
        //   133: anewarray       Ljava/lang/Class;
        //   136: dup            
        //   137: iconst_0       
        //   138: getstatic       java/lang/Integer.TYPE:Ljava/lang/Class;
        //   141: aastore        
        //   142: invokevirtual   java/lang/Class.getMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //   145: astore_0       
        //   146: goto            151
        //   149: aconst_null    
        //   150: astore_0       
        //   151: aload_0        
        //   152: putstatic       com/google/android/gms/common/util/WorkSourceUtil.zzf:Ljava/lang/reflect/Method;
        //   155: invokestatic    com/google/android/gms/common/util/PlatformVersion.isAtLeastP:()Z
        //   158: ifeq            186
        //   161: ldc             Landroid/os/WorkSource;.class
        //   163: ldc             "createWorkChain"
        //   165: iconst_0       
        //   166: anewarray       Ljava/lang/Class;
        //   169: invokevirtual   java/lang/Class.getMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //   172: astore_0       
        //   173: goto            188
        //   176: astore_0       
        //   177: ldc             "WorkSourceUtil"
        //   179: ldc             "Missing WorkChain API createWorkChain"
        //   181: aload_0        
        //   182: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   185: pop            
        //   186: aconst_null    
        //   187: astore_0       
        //   188: aload_0        
        //   189: putstatic       com/google/android/gms/common/util/WorkSourceUtil.zzg:Ljava/lang/reflect/Method;
        //   192: invokestatic    com/google/android/gms/common/util/PlatformVersion.isAtLeastP:()Z
        //   195: ifeq            237
        //   198: ldc             "android.os.WorkSource$WorkChain"
        //   200: invokestatic    java/lang/Class.forName:(Ljava/lang/String;)Ljava/lang/Class;
        //   203: ldc             "addNode"
        //   205: iconst_2       
        //   206: anewarray       Ljava/lang/Class;
        //   209: dup            
        //   210: iconst_0       
        //   211: getstatic       java/lang/Integer.TYPE:Ljava/lang/Class;
        //   214: aastore        
        //   215: dup            
        //   216: iconst_1       
        //   217: ldc             Ljava/lang/String;.class
        //   219: aastore        
        //   220: invokevirtual   java/lang/Class.getMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //   223: astore_0       
        //   224: goto            239
        //   227: astore_0       
        //   228: ldc             "WorkSourceUtil"
        //   230: ldc             "Missing WorkChain class"
        //   232: aload_0        
        //   233: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   236: pop            
        //   237: aconst_null    
        //   238: astore_0       
        //   239: aload_0        
        //   240: putstatic       com/google/android/gms/common/util/WorkSourceUtil.zzh:Ljava/lang/reflect/Method;
        //   243: invokestatic    com/google/android/gms/common/util/PlatformVersion.isAtLeastP:()Z
        //   246: ifeq            269
        //   249: ldc             Landroid/os/WorkSource;.class
        //   251: ldc             "isEmpty"
        //   253: iconst_0       
        //   254: anewarray       Ljava/lang/Class;
        //   257: invokevirtual   java/lang/Class.getMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //   260: astore_0       
        //   261: aload_0        
        //   262: iconst_1       
        //   263: invokevirtual   java/lang/reflect/AccessibleObject.setAccessible:(Z)V
        //   266: goto            271
        //   269: aconst_null    
        //   270: astore_0       
        //   271: aload_0        
        //   272: putstatic       com/google/android/gms/common/util/WorkSourceUtil.zzi:Ljava/lang/reflect/Method;
        //   275: aconst_null    
        //   276: putstatic       com/google/android/gms/common/util/WorkSourceUtil.zzj:Ljava/lang/Boolean;
        //   279: return         
        //   280: astore_0       
        //   281: goto            66
        //   284: astore_0       
        //   285: goto            149
        //   288: astore_0       
        //   289: goto            269
        //   292: astore_1       
        //   293: goto            271
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  6      24     27     30     Ljava/lang/Exception;
        //  40     63     280    284    Ljava/lang/Exception;
        //  72     84     87     90     Ljava/lang/Exception;
        //  94     112    115    118    Ljava/lang/Exception;
        //  128    146    284    288    Ljava/lang/Exception;
        //  161    173    176    186    Ljava/lang/Exception;
        //  198    224    227    237    Ljava/lang/Exception;
        //  249    261    288    292    Ljava/lang/Exception;
        //  261    266    292    296    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 158 out of bounds for length 158
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:248)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:372)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:459)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private WorkSourceUtil() {
    }
    
    @KeepForSdk
    public static void add(final WorkSource workSource, final int n, final String s) {
        final Method zzc = WorkSourceUtil.zzc;
        if (zzc != null) {
            String s2;
            if ((s2 = s) == null) {
                s2 = "";
            }
            try {
                zzc.invoke(workSource, n, s2);
                return;
            }
            catch (final Exception ex) {
                Log.wtf("WorkSourceUtil", "Unable to assign blame through WorkSource", (Throwable)ex);
                return;
            }
        }
        final Method zzb = WorkSourceUtil.zzb;
        if (zzb != null) {
            try {
                zzb.invoke(workSource, n);
            }
            catch (final Exception ex2) {
                Log.wtf("WorkSourceUtil", "Unable to assign blame through WorkSource", (Throwable)ex2);
            }
        }
    }
    
    @KeepForSdk
    public static WorkSource fromPackage(final Context context, final String str) {
        if (context != null && context.getPackageManager() != null && str != null) {
            while (true) {
                try {
                    final ApplicationInfo applicationInfo = Wrappers.packageManager(context).getApplicationInfo(str, 0);
                    if (applicationInfo == null) {
                        final String s = "Could not get applicationInfo from package: ";
                        Log.e("WorkSourceUtil", s.concat(str));
                        return null;
                    }
                    final int uid = applicationInfo.uid;
                    final WorkSource workSource = new WorkSource();
                    add(workSource, uid, str);
                    return workSource;
                }
                catch (final PackageManager$NameNotFoundException ex) {
                    final String s = "Could not find package: ";
                    continue;
                }
                break;
            }
        }
        return null;
    }
    
    @KeepForSdk
    public static WorkSource fromPackageAndModuleExperimentalPi(Context obj, final String str, final String s) {
        if (obj == null || obj.getPackageManager() == null || s == null || str == null) {
            Log.w("WorkSourceUtil", "Unexpected null arguments");
            return null;
        }
        int uid = -1;
        Label_0067: {
            String s2;
            try {
                final ApplicationInfo applicationInfo = Wrappers.packageManager(obj).getApplicationInfo(str, 0);
                if (applicationInfo != null) {
                    uid = applicationInfo.uid;
                    break Label_0067;
                }
                s2 = "Could not get applicationInfo from package: ";
            }
            catch (final PackageManager$NameNotFoundException ex) {
                s2 = "Could not find package: ";
            }
            Log.e("WorkSourceUtil", s2.concat(str));
        }
        if (uid < 0) {
            return null;
        }
        obj = (Context)new WorkSource();
        final Method zzg = WorkSourceUtil.zzg;
        if (zzg != null) {
            final Method zzh = WorkSourceUtil.zzh;
            if (zzh != null) {
                try {
                    final Object invoke = zzg.invoke(obj, new Object[0]);
                    final int zza = WorkSourceUtil.zza;
                    if (uid != zza) {
                        zzh.invoke(invoke, uid, str);
                    }
                    zzh.invoke(invoke, zza, s);
                }
                catch (final Exception ex2) {
                    Log.w("WorkSourceUtil", "Unable to assign chained blame through WorkSource", (Throwable)ex2);
                }
                return (WorkSource)obj;
            }
        }
        add((WorkSource)obj, uid, str);
        return (WorkSource)obj;
    }
    
    @KeepForSdk
    public static int get(final WorkSource obj, int intValue) {
        final Method zze = WorkSourceUtil.zze;
        if (zze != null) {
            try {
                final Object invoke = zze.invoke(obj, intValue);
                Preconditions.checkNotNull(invoke);
                intValue = (int)invoke;
                return intValue;
            }
            catch (final Exception ex) {
                Log.wtf("WorkSourceUtil", "Unable to assign blame through WorkSource", (Throwable)ex);
            }
        }
        return 0;
    }
    
    @KeepForSdk
    public static String getName(final WorkSource obj, final int i) {
        final Method zzf = WorkSourceUtil.zzf;
        if (zzf != null) {
            try {
                return (String)zzf.invoke(obj, i);
            }
            catch (final Exception ex) {
                Log.wtf("WorkSourceUtil", "Unable to assign blame through WorkSource", (Throwable)ex);
            }
        }
        return null;
    }
    
    @KeepForSdk
    public static List<String> getNames(final WorkSource workSource) {
        final ArrayList list = new ArrayList();
        int i = 0;
        int size;
        if (workSource == null) {
            size = 0;
        }
        else {
            size = size(workSource);
        }
        if (size != 0) {
            while (i < size) {
                final String name = getName(workSource, i);
                if (!Strings.isEmptyOrWhitespace(name)) {
                    Preconditions.checkNotNull(name);
                    list.add(name);
                }
                ++i;
            }
        }
        return list;
    }
    
    @KeepForSdk
    public static boolean hasWorkSourcePermission(final Context context) {
        synchronized (WorkSourceUtil.class) {
            final Boolean zzj = WorkSourceUtil.zzj;
            if (zzj != null) {
                return zzj;
            }
            boolean b = false;
            if (context == null) {
                return false;
            }
            if (sl.checkSelfPermission(context, "android.permission.UPDATE_DEVICE_STATS") == 0) {
                b = true;
            }
            return WorkSourceUtil.zzj = b;
        }
    }
    
    @KeepForSdk
    public static boolean isEmpty(final WorkSource obj) {
        final Method zzi = WorkSourceUtil.zzi;
        if (zzi != null) {
            try {
                final Object invoke = zzi.invoke(obj, new Object[0]);
                Preconditions.checkNotNull(invoke);
                return (boolean)invoke;
            }
            catch (final Exception ex) {
                Log.e("WorkSourceUtil", "Unable to check WorkSource emptiness", (Throwable)ex);
            }
        }
        return size(obj) == 0;
    }
    
    @KeepForSdk
    public static int size(final WorkSource obj) {
        final Method zzd = WorkSourceUtil.zzd;
        if (zzd != null) {
            try {
                final Object invoke = zzd.invoke(obj, new Object[0]);
                Preconditions.checkNotNull(invoke);
                return (int)invoke;
            }
            catch (final Exception ex) {
                Log.wtf("WorkSourceUtil", "Unable to assign blame through WorkSource", (Throwable)ex);
            }
        }
        return 0;
    }
}

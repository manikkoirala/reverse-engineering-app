// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.util;

import com.google.android.gms.common.internal.ShowFirstParty;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
@ShowFirstParty
public class Hex
{
    private static final char[] zza;
    private static final char[] zzb;
    
    static {
        zza = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
        zzb = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
    }
    
    @KeepForSdk
    public static String bytesToStringLowercase(final byte[] array) {
        final int length = array.length;
        final char[] value = new char[length + length];
        int i = 0;
        int n = 0;
        while (i < array.length) {
            final int n2 = array[i] & 0xFF;
            final int n3 = n + 1;
            final char[] zzb = Hex.zzb;
            value[n] = zzb[n2 >>> 4];
            value[n3] = zzb[n2 & 0xF];
            n = n3 + 1;
            ++i;
        }
        return new String(value);
    }
    
    @KeepForSdk
    public static String bytesToStringUppercase(final byte[] array) {
        return bytesToStringUppercase(array, false);
    }
    
    @KeepForSdk
    public static String bytesToStringUppercase(final byte[] array, final boolean b) {
        final int length = array.length;
        final StringBuilder sb = new StringBuilder(length + length);
        for (int n = 0; n < length && (!b || n != length - 1 || (array[n] & 0xFF) != 0x0); ++n) {
            final char[] zza = Hex.zza;
            sb.append(zza[(array[n] & 0xF0) >>> 4]);
            sb.append(zza[array[n] & 0xF]);
        }
        return sb.toString();
    }
    
    @KeepForSdk
    public static byte[] stringToBytes(final String s) {
        final int length = s.length();
        if (length % 2 == 0) {
            final byte[] array = new byte[length / 2];
            int endIndex;
            for (int i = 0; i < length; i = endIndex) {
                final int n = i / 2;
                endIndex = i + 2;
                array[n] = (byte)Integer.parseInt(s.substring(i, endIndex), 16);
            }
            return array;
        }
        throw new IllegalArgumentException("Hex string has odd number of characters");
    }
}

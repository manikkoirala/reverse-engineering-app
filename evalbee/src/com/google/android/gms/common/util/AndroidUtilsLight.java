// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.util;

import java.security.NoSuchAlgorithmException;
import java.security.MessageDigest;
import android.content.pm.Signature;
import android.content.pm.PackageInfo;
import com.google.android.gms.common.wrappers.Wrappers;
import android.content.Context;
import com.google.android.gms.common.internal.ShowFirstParty;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
@ShowFirstParty
public class AndroidUtilsLight
{
    private static volatile int zza = -1;
    
    @Deprecated
    @KeepForSdk
    public static byte[] getPackageCertificateHashBytes(final Context context, final String s) {
        final PackageInfo packageInfo = Wrappers.packageManager(context).getPackageInfo(s, 64);
        final Signature[] signatures = packageInfo.signatures;
        if (signatures != null && signatures.length == 1) {
            final MessageDigest zza = zza("SHA1");
            if (zza != null) {
                return zza.digest(packageInfo.signatures[0].toByteArray());
            }
        }
        return null;
    }
    
    public static MessageDigest zza(final String algorithm) {
        int n = 0;
    Label_0021_Outer:
        while (true) {
            Label_0027: {
                if (n >= 2) {
                    break Label_0027;
                }
                while (true) {
                    try {
                        final MessageDigest instance = MessageDigest.getInstance(algorithm);
                        if (instance == null) {
                            ++n;
                            continue Label_0021_Outer;
                        }
                        return instance;
                        return null;
                    }
                    catch (final NoSuchAlgorithmException ex) {
                        continue;
                    }
                    break;
                }
            }
        }
    }
}

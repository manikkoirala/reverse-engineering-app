// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.util;

import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public class MurmurHash3
{
    private MurmurHash3() {
    }
    
    @KeepForSdk
    public static int murmurhash3_x86_32(final byte[] array, int n, final int n2, int n3) {
        int n4 = n;
        int n5;
        while (true) {
            n5 = (n2 & 0xFFFFFFFC) + n;
            if (n4 >= n5) {
                break;
            }
            final int n6 = ((array[n4] & 0xFF) | (array[n4 + 1] & 0xFF) << 8 | (array[n4 + 2] & 0xFF) << 16 | array[n4 + 3] << 24) * -862048943;
            n3 ^= (n6 >>> 17 | n6 << 15) * 461845907;
            n3 = (n3 >>> 19 | n3 << 13) * 5 - 430675100;
            n4 += 4;
        }
        final int n7 = n2 & 0x3;
        n = 0;
        final int n8 = 0;
        Label_0200: {
            if (n7 != 1) {
                n = n8;
                if (n7 != 2) {
                    if (n7 != 3) {
                        break Label_0200;
                    }
                    n = (array[n5 + 2] & 0xFF) << 16;
                }
                n |= (array[n5 + 1] & 0xFF) << 8;
            }
            n = ((array[n5] & 0xFF) | n) * -862048943;
            n3 ^= (n >>> 17 | n << 15) * 461845907;
        }
        n = (n3 ^ n2);
        n = (n ^ n >>> 16) * -2048144789;
        n = (n ^ n >>> 13) * -1028477387;
        return n ^ n >>> 16;
    }
}

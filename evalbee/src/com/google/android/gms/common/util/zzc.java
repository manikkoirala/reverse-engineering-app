// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.util;

import java.util.regex.Matcher;
import android.text.TextUtils;
import java.util.regex.Pattern;

final class zzc
{
    private static final Pattern zza;
    
    static {
        zza = Pattern.compile("\\\\u[0-9a-fA-F]{4}");
    }
    
    public static String zza(final String s) {
        String string = s;
        if (!TextUtils.isEmpty((CharSequence)s)) {
            final Matcher matcher = zzc.zza.matcher(s);
            StringBuilder sb = null;
            int end = 0;
            while (matcher.find()) {
                StringBuilder sb2;
                if ((sb2 = sb) == null) {
                    sb2 = new StringBuilder();
                }
                int start;
                int n;
                for (n = (start = matcher.start()); start >= 0 && s.charAt(start) == '\\'; --start) {}
                sb = sb2;
                if ((n - start) % 2 != 0) {
                    final int int1 = Integer.parseInt(matcher.group().substring(2), 16);
                    sb2.append(s, end, matcher.start());
                    if (int1 == 92) {
                        sb2.append("\\\\");
                    }
                    else {
                        sb2.append(Character.toChars(int1));
                    }
                    end = matcher.end();
                    sb = sb2;
                }
            }
            if (sb == null) {
                return s;
            }
            if (end < matcher.regionEnd()) {
                sb.append(s, end, matcher.regionEnd());
            }
            string = sb.toString();
        }
        return string;
    }
}

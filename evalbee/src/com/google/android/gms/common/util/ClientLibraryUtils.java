// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.util;

import android.os.BaseBundle;
import android.content.pm.PackageManager$NameNotFoundException;
import com.google.android.gms.common.wrappers.Wrappers;
import android.os.Bundle;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.Context;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public class ClientLibraryUtils
{
    private ClientLibraryUtils() {
    }
    
    @KeepForSdk
    public static int getClientVersion(final Context context, final String s) {
        final PackageInfo packageInfo = getPackageInfo(context, s);
        if (packageInfo != null) {
            final ApplicationInfo applicationInfo = packageInfo.applicationInfo;
            if (applicationInfo != null) {
                final Bundle metaData = applicationInfo.metaData;
                if (metaData != null) {
                    return ((BaseBundle)metaData).getInt("com.google.android.gms.version", -1);
                }
            }
        }
        return -1;
    }
    
    @KeepForSdk
    public static PackageInfo getPackageInfo(final Context context, final String s) {
        try {
            return Wrappers.packageManager(context).getPackageInfo(s, 128);
        }
        catch (final PackageManager$NameNotFoundException ex) {
            return null;
        }
    }
    
    @KeepForSdk
    public static boolean isPackageSide() {
        return false;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.util;

import android.content.res.Configuration;
import com.google.android.apps.common.proguard.SideEffectFree;
import android.os.Build;
import com.google.android.gms.common.GooglePlayServicesUtilLight;
import android.content.res.Resources;
import android.content.pm.PackageManager;
import android.content.Context;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public final class DeviceProperties
{
    private static Boolean zza;
    private static Boolean zzb;
    private static Boolean zzc;
    private static Boolean zzd;
    private static Boolean zze;
    private static Boolean zzf;
    private static Boolean zzg;
    private static Boolean zzh;
    private static Boolean zzi;
    private static Boolean zzj;
    private static Boolean zzk;
    private static Boolean zzl;
    
    private DeviceProperties() {
    }
    
    @KeepForSdk
    public static boolean isAuto(final Context context) {
        final PackageManager packageManager = context.getPackageManager();
        if (DeviceProperties.zzi == null) {
            final boolean atLeastO = PlatformVersion.isAtLeastO();
            boolean b = false;
            if (atLeastO) {
                b = b;
                if (packageManager.hasSystemFeature("android.hardware.type.automotive")) {
                    b = true;
                }
            }
            DeviceProperties.zzi = b;
        }
        return DeviceProperties.zzi;
    }
    
    @KeepForSdk
    public static boolean isBstar(final Context context) {
        if (DeviceProperties.zzl == null) {
            final boolean atLeastR = PlatformVersion.isAtLeastR();
            boolean b = false;
            if (atLeastR) {
                b = b;
                if (context.getPackageManager().hasSystemFeature("com.google.android.play.feature.HPE_EXPERIENCE")) {
                    b = true;
                }
            }
            DeviceProperties.zzl = b;
        }
        return DeviceProperties.zzl;
    }
    
    @KeepForSdk
    public static boolean isLatchsky(final Context context) {
        if (DeviceProperties.zzf == null) {
            final PackageManager packageManager = context.getPackageManager();
            final boolean hasSystemFeature = packageManager.hasSystemFeature("com.google.android.feature.services_updater");
            boolean b = false;
            if (hasSystemFeature) {
                b = b;
                if (packageManager.hasSystemFeature("cn.google.services")) {
                    b = true;
                }
            }
            DeviceProperties.zzf = b;
        }
        return DeviceProperties.zzf;
    }
    
    @KeepForSdk
    public static boolean isPhone(final Context context) {
        if (DeviceProperties.zza == null) {
            final boolean tablet = isTablet(context);
            boolean b2;
            final boolean b = b2 = false;
            if (!tablet) {
                b2 = b;
                if (!isWearable(context)) {
                    b2 = b;
                    if (!zzb(context)) {
                        if (DeviceProperties.zzh == null) {
                            DeviceProperties.zzh = context.getPackageManager().hasSystemFeature("org.chromium.arc");
                        }
                        b2 = b;
                        if (!DeviceProperties.zzh) {
                            b2 = b;
                            if (!isAuto(context)) {
                                b2 = b;
                                if (!isTv(context)) {
                                    if (DeviceProperties.zzk == null) {
                                        DeviceProperties.zzk = context.getPackageManager().hasSystemFeature("com.google.android.feature.AMATI_EXPERIENCE");
                                    }
                                    b2 = b;
                                    if (!DeviceProperties.zzk) {
                                        b2 = b;
                                        if (!isBstar(context)) {
                                            b2 = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            DeviceProperties.zza = b2;
        }
        return DeviceProperties.zza;
    }
    
    @KeepForSdk
    public static boolean isSevenInchTablet(final Context context) {
        return zzc(context.getResources());
    }
    
    @KeepForSdk
    public static boolean isSidewinder(final Context context) {
        return zza(context);
    }
    
    @KeepForSdk
    public static boolean isTablet(final Context context) {
        return isTablet(context.getResources());
    }
    
    @KeepForSdk
    public static boolean isTablet(final Resources resources) {
        boolean b = false;
        if (resources == null) {
            return false;
        }
        if (DeviceProperties.zzb == null) {
            if ((resources.getConfiguration().screenLayout & 0xF) > 3 || zzc(resources)) {
                b = true;
            }
            DeviceProperties.zzb = b;
        }
        return DeviceProperties.zzb;
    }
    
    @KeepForSdk
    public static boolean isTv(final Context context) {
        final PackageManager packageManager = context.getPackageManager();
        if (DeviceProperties.zzj == null) {
            final boolean hasSystemFeature = packageManager.hasSystemFeature("com.google.android.tv");
            boolean b2;
            final boolean b = b2 = true;
            if (!hasSystemFeature) {
                b2 = b;
                if (!packageManager.hasSystemFeature("android.hardware.type.television")) {
                    b2 = (packageManager.hasSystemFeature("android.software.leanback") && b);
                }
            }
            DeviceProperties.zzj = b2;
        }
        return DeviceProperties.zzj;
    }
    
    @KeepForSdk
    public static boolean isUserBuild() {
        final int google_PLAY_SERVICES_VERSION_CODE = GooglePlayServicesUtilLight.GOOGLE_PLAY_SERVICES_VERSION_CODE;
        return "user".equals(Build.TYPE);
    }
    
    @SideEffectFree
    @KeepForSdk
    public static boolean isWearable(final Context context) {
        return zzd(context.getPackageManager());
    }
    
    @KeepForSdk
    public static boolean isWearableWithoutPlayStore(final Context context) {
        return (isWearable(context) && !PlatformVersion.isAtLeastN()) || (zza(context) && (!PlatformVersion.isAtLeastO() || PlatformVersion.isAtLeastR()));
    }
    
    public static boolean zza(final Context context) {
        if (DeviceProperties.zze == null) {
            final boolean atLeastLollipop = PlatformVersion.isAtLeastLollipop();
            boolean b = false;
            if (atLeastLollipop) {
                b = b;
                if (context.getPackageManager().hasSystemFeature("cn.google")) {
                    b = true;
                }
            }
            DeviceProperties.zze = b;
        }
        return DeviceProperties.zze;
    }
    
    public static boolean zzb(final Context context) {
        if (DeviceProperties.zzg == null) {
            final boolean hasSystemFeature = context.getPackageManager().hasSystemFeature("android.hardware.type.iot");
            boolean b = true;
            if (!hasSystemFeature) {
                b = (context.getPackageManager().hasSystemFeature("android.hardware.type.embedded") && b);
            }
            DeviceProperties.zzg = b;
        }
        return DeviceProperties.zzg;
    }
    
    public static boolean zzc(final Resources resources) {
        final boolean b = false;
        if (resources == null) {
            return false;
        }
        if (DeviceProperties.zzc == null) {
            final Configuration configuration = resources.getConfiguration();
            boolean b2 = b;
            if ((configuration.screenLayout & 0xF) <= 3) {
                b2 = b;
                if (configuration.smallestScreenWidthDp >= 600) {
                    b2 = true;
                }
            }
            DeviceProperties.zzc = b2;
        }
        return DeviceProperties.zzc;
    }
    
    @SideEffectFree
    public static boolean zzd(final PackageManager packageManager) {
        if (DeviceProperties.zzd == null) {
            final boolean atLeastKitKatWatch = PlatformVersion.isAtLeastKitKatWatch();
            boolean b = false;
            if (atLeastKitKatWatch) {
                b = b;
                if (packageManager.hasSystemFeature("android.hardware.type.watch")) {
                    b = true;
                }
            }
            DeviceProperties.zzd = b;
        }
        return DeviceProperties.zzd;
    }
}

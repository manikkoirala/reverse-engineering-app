// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.util;

import com.google.errorprone.annotations.ResultIgnorabilityUnspecified;
import android.util.Log;
import com.google.android.gms.common.internal.Preconditions;
import android.content.Context;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public final class CrashUtils
{
    private static final String[] zza;
    
    static {
        zza = new String[] { "android.", "com.android.", "dalvik.", "java.", "javax." };
    }
    
    @ResultIgnorabilityUnspecified
    @KeepForSdk
    public static boolean addDynamiteErrorToDropBox(final Context context, final Throwable t) {
        try {
            Preconditions.checkNotNull(context);
            Preconditions.checkNotNull(t);
        }
        catch (final Exception ex) {
            Log.e("CrashUtils", "Error adding exception to DropBox!", (Throwable)ex);
        }
        return false;
    }
}

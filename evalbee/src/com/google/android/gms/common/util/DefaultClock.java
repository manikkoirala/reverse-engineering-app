// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.util;

import android.os.SystemClock;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public class DefaultClock implements Clock
{
    private static final DefaultClock zza;
    
    static {
        zza = new DefaultClock();
    }
    
    private DefaultClock() {
    }
    
    @KeepForSdk
    public static Clock getInstance() {
        return DefaultClock.zza;
    }
    
    @Override
    public final long currentThreadTimeMillis() {
        return SystemClock.currentThreadTimeMillis();
    }
    
    @Override
    public final long currentTimeMillis() {
        return System.currentTimeMillis();
    }
    
    @Override
    public final long elapsedRealtime() {
        return SystemClock.elapsedRealtime();
    }
    
    @Override
    public final long nanoTime() {
        return System.nanoTime();
    }
}

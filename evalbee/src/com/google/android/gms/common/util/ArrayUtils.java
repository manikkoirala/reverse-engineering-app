// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.util;

import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import com.google.android.gms.common.internal.Objects;
import java.lang.reflect.Array;
import java.util.Arrays;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public final class ArrayUtils
{
    private ArrayUtils() {
    }
    
    @KeepForSdk
    public static <T> T[] concat(final T[]... array) {
        if (array.length != 0) {
            int i = 0;
            int newLength = 0;
            while (i < array.length) {
                newLength += array[i].length;
                ++i;
            }
            final T[] copy = Arrays.copyOf(array[0], newLength);
            int length = array[0].length;
            for (int j = 1; j < array.length; ++j) {
                final T[] array2 = array[j];
                final int length2 = array2.length;
                System.arraycopy(array2, 0, copy, length, length2);
                length += length2;
            }
            return copy;
        }
        return (T[])Array.newInstance(array.getClass(), 0);
    }
    
    @KeepForSdk
    public static byte[] concatByteArrays(final byte[]... array) {
        if (array.length != 0) {
            int i = 0;
            int newLength = 0;
            while (i < array.length) {
                newLength += array[i].length;
                ++i;
            }
            final byte[] copy = Arrays.copyOf(array[0], newLength);
            int length = array[0].length;
            for (int j = 1; j < array.length; ++j) {
                final byte[] array2 = array[j];
                final int length2 = array2.length;
                System.arraycopy(array2, 0, copy, length, length2);
                length += length2;
            }
            return copy;
        }
        return new byte[0];
    }
    
    @KeepForSdk
    public static boolean contains(final int[] array, final int n) {
        if (array != null) {
            for (int i = 0; i < array.length; ++i) {
                if (array[i] == n) {
                    return true;
                }
            }
        }
        return false;
    }
    
    @KeepForSdk
    public static <T> boolean contains(final T[] array, final T t) {
        int length;
        if (array != null) {
            length = array.length;
        }
        else {
            length = 0;
        }
        int i = 0;
        while (i < length) {
            if (Objects.equal(array[i], t)) {
                if (i >= 0) {
                    return true;
                }
                break;
            }
            else {
                ++i;
            }
        }
        return false;
    }
    
    @KeepForSdk
    public static <T> ArrayList<T> newArrayList() {
        return new ArrayList<T>();
    }
    
    @KeepForSdk
    public static <T> T[] removeAll(final T[] original, final T... array) {
        final T[] array2 = null;
        if (original == null) {
            return null;
        }
        if (array != null) {
            final int length = array.length;
            if (length != 0) {
                final Class<? extends Object[]> class1 = array.getClass();
                final int length2 = original.length;
                final Object[] original2 = (Object[])Array.newInstance(class1.getComponentType(), length2);
                int i = 0;
                int newLength;
                if (length == 1) {
                    int n = 0;
                    int n2 = 0;
                    while (true) {
                        newLength = n2;
                        if (n >= length2) {
                            break;
                        }
                        final T t = original[n];
                        int n3 = n2;
                        if (!Objects.equal(array[0], t)) {
                            original2[n2] = t;
                            n3 = n2 + 1;
                        }
                        ++n;
                        n2 = n3;
                    }
                }
                else {
                    int n4 = 0;
                    while (i < length2) {
                        final T t2 = original[i];
                        int n5 = n4;
                        if (!contains(array, t2)) {
                            original2[n4] = t2;
                            n5 = n4 + 1;
                        }
                        ++i;
                        n4 = n5;
                    }
                    newLength = n4;
                }
                T[] array3;
                if (original2 == null) {
                    array3 = array2;
                }
                else {
                    if (newLength != ((T[])original2).length) {
                        return Arrays.copyOf(original2, newLength);
                    }
                    array3 = (T[])original2;
                }
                return array3;
            }
        }
        return Arrays.copyOf(original, original.length);
    }
    
    @KeepForSdk
    public static <T> ArrayList<T> toArrayList(final T[] array) {
        final int length = array.length;
        final ArrayList list = new ArrayList<T>(length);
        for (int i = 0; i < length; ++i) {
            list.add(array[i]);
        }
        return (ArrayList<T>)list;
    }
    
    @KeepForSdk
    public static int[] toPrimitiveArray(final Collection<Integer> collection) {
        int n = 0;
        if (collection != null && !collection.isEmpty()) {
            final int[] array = new int[collection.size()];
            final Iterator iterator = collection.iterator();
            while (iterator.hasNext()) {
                array[n] = (int)iterator.next();
                ++n;
            }
            return array;
        }
        return new int[0];
    }
    
    @KeepForSdk
    public static Integer[] toWrapperArray(final int[] array) {
        if (array == null) {
            return null;
        }
        final int length = array.length;
        final Integer[] array2 = new Integer[length];
        for (int i = 0; i < length; ++i) {
            array2[i] = array[i];
        }
        return array2;
    }
    
    @KeepForSdk
    public static void writeArray(final StringBuilder sb, final double[] array) {
        for (int length = array.length, i = 0; i < length; ++i) {
            if (i != 0) {
                sb.append(",");
            }
            sb.append(Double.toString(array[i]));
        }
    }
    
    @KeepForSdk
    public static void writeArray(final StringBuilder sb, final float[] array) {
        for (int length = array.length, i = 0; i < length; ++i) {
            if (i != 0) {
                sb.append(",");
            }
            sb.append(Float.toString(array[i]));
        }
    }
    
    @KeepForSdk
    public static void writeArray(final StringBuilder sb, final int[] array) {
        for (int length = array.length, i = 0; i < length; ++i) {
            if (i != 0) {
                sb.append(",");
            }
            sb.append(Integer.toString(array[i]));
        }
    }
    
    @KeepForSdk
    public static void writeArray(final StringBuilder sb, final long[] array) {
        for (int length = array.length, i = 0; i < length; ++i) {
            if (i != 0) {
                sb.append(",");
            }
            sb.append(Long.toString(array[i]));
        }
    }
    
    @KeepForSdk
    public static <T> void writeArray(final StringBuilder sb, final T[] array) {
        for (int length = array.length, i = 0; i < length; ++i) {
            if (i != 0) {
                sb.append(",");
            }
            sb.append(array[i]);
        }
    }
    
    @KeepForSdk
    public static void writeArray(final StringBuilder sb, final boolean[] array) {
        for (int length = array.length, i = 0; i < length; ++i) {
            if (i != 0) {
                sb.append(",");
            }
            sb.append(Boolean.toString(array[i]));
        }
    }
    
    @KeepForSdk
    public static void writeStringArray(final StringBuilder sb, final String[] array) {
        for (int length = array.length, i = 0; i < length; ++i) {
            if (i != 0) {
                sb.append(",");
            }
            sb.append("\"");
            sb.append(array[i]);
            sb.append("\"");
        }
    }
}

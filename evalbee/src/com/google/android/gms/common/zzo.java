// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IBinder;
import android.content.Context;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "GoogleCertificatesLookupQueryCreator")
public final class zzo extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zzo> CREATOR;
    @Field(getter = "getCallingPackage", id = 1)
    private final String zza;
    @Field(getter = "getAllowTestKeys", id = 2)
    private final boolean zzb;
    @Field(defaultValue = "false", getter = "getIgnoreTestKeysOverride", id = 3)
    private final boolean zzc;
    @Field(getter = "getCallingContextBinder", id = 4, type = "android.os.IBinder")
    private final Context zzd;
    @Field(getter = "getIsChimeraPackage", id = 5)
    private final boolean zze;
    @Field(getter = "getIncludeHashesInErrorMessage", id = 6)
    private final boolean zzf;
    
    static {
        CREATOR = (Parcelable$Creator)new zzp();
    }
    
    @Constructor
    public zzo(@Param(id = 1) final String zza, @Param(id = 2) final boolean zzb, @Param(id = 3) final boolean zzc, @Param(id = 4) final IBinder binder, @Param(id = 5) final boolean zze, @Param(id = 6) final boolean zzf) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = ObjectWrapper.unwrap(IObjectWrapper.Stub.asInterface(binder));
        this.zze = zze;
        this.zzf = zzf;
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.zza, false);
        SafeParcelWriter.writeBoolean(parcel, 2, this.zzb);
        SafeParcelWriter.writeBoolean(parcel, 3, this.zzc);
        SafeParcelWriter.writeIBinder(parcel, 4, (IBinder)ObjectWrapper.wrap(this.zzd), false);
        SafeParcelWriter.writeBoolean(parcel, 5, this.zze);
        SafeParcelWriter.writeBoolean(parcel, 6, this.zzf);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

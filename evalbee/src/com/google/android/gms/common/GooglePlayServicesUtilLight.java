// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common;

import android.os.BaseBundle;
import java.util.Iterator;
import android.content.pm.PackageInstaller$SessionInfo;
import android.os.Bundle;
import android.os.UserManager;
import com.google.android.gms.common.util.PlatformVersion;
import com.google.android.gms.common.util.UidVerifier;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import com.google.android.gms.common.util.zza;
import com.google.android.gms.common.internal.zzah;
import com.google.android.gms.common.internal.HideFirstParty;
import com.google.errorprone.annotations.ResultIgnorabilityUnspecified;
import android.content.pm.PackageInfo;
import com.google.android.gms.common.util.DeviceProperties;
import com.google.android.gms.common.wrappers.Wrappers;
import android.content.res.Resources;
import android.app.PendingIntent;
import com.google.android.gms.common.util.ClientLibraryUtils;
import com.google.android.gms.common.internal.Preconditions;
import android.content.pm.PackageManager$NameNotFoundException;
import android.content.Intent;
import android.util.Log;
import android.app.NotificationManager;
import android.content.Context;
import com.google.android.gms.common.util.VisibleForTesting;
import java.util.concurrent.atomic.AtomicBoolean;
import com.google.android.gms.common.internal.ShowFirstParty;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
@ShowFirstParty
public class GooglePlayServicesUtilLight
{
    @KeepForSdk
    static final int GMS_AVAILABILITY_NOTIFICATION_ID = 10436;
    @KeepForSdk
    static final int GMS_GENERAL_ERROR_NOTIFICATION_ID = 39789;
    @KeepForSdk
    public static final String GOOGLE_PLAY_GAMES_PACKAGE = "com.google.android.play.games";
    @Deprecated
    @KeepForSdk
    public static final String GOOGLE_PLAY_SERVICES_PACKAGE = "com.google.android.gms";
    @Deprecated
    @KeepForSdk
    public static final int GOOGLE_PLAY_SERVICES_VERSION_CODE = 12451000;
    @KeepForSdk
    public static final String GOOGLE_PLAY_STORE_PACKAGE = "com.android.vending";
    @Deprecated
    @KeepForSdk
    static final AtomicBoolean sCanceledAvailabilityNotification;
    @VisibleForTesting
    static boolean zza = false;
    private static boolean zzb = false;
    private static final AtomicBoolean zzc;
    
    static {
        sCanceledAvailabilityNotification = new AtomicBoolean();
        zzc = new AtomicBoolean();
    }
    
    @KeepForSdk
    public GooglePlayServicesUtilLight() {
    }
    
    @Deprecated
    @KeepForSdk
    public static void cancelAvailabilityErrorNotifications(final Context context) {
        if (GooglePlayServicesUtilLight.sCanceledAvailabilityNotification.getAndSet(true)) {
            return;
        }
        try {
            final NotificationManager notificationManager = (NotificationManager)context.getSystemService("notification");
            if (notificationManager != null) {
                notificationManager.cancel(10436);
            }
        }
        catch (final SecurityException ex) {
            Log.d("GooglePlayServicesUtil", "Suppressing Security Exception %s in cancelAvailabilityErrorNotifications.", (Throwable)ex);
        }
    }
    
    @KeepForSdk
    @ShowFirstParty
    public static void enableUsingApkIndependentContext() {
        GooglePlayServicesUtilLight.zzc.set(true);
    }
    
    @Deprecated
    @KeepForSdk
    public static void ensurePlayServicesAvailable(final Context context, int googlePlayServicesAvailable) {
        googlePlayServicesAvailable = GoogleApiAvailabilityLight.getInstance().isGooglePlayServicesAvailable(context, googlePlayServicesAvailable);
        if (googlePlayServicesAvailable == 0) {
            return;
        }
        final Intent errorResolutionIntent = GoogleApiAvailabilityLight.getInstance().getErrorResolutionIntent(context, googlePlayServicesAvailable, "e");
        final StringBuilder sb = new StringBuilder();
        sb.append("GooglePlayServices not available due to error ");
        sb.append(googlePlayServicesAvailable);
        Log.e("GooglePlayServicesUtil", sb.toString());
        if (errorResolutionIntent == null) {
            throw new GooglePlayServicesNotAvailableException(googlePlayServicesAvailable);
        }
        throw new GooglePlayServicesRepairableException(googlePlayServicesAvailable, "Google Play Services not available", errorResolutionIntent);
    }
    
    @Deprecated
    @KeepForSdk
    @ShowFirstParty
    public static int getApkVersion(final Context context) {
        try {
            return context.getPackageManager().getPackageInfo("com.google.android.gms", 0).versionCode;
        }
        catch (final PackageManager$NameNotFoundException ex) {
            Log.w("GooglePlayServicesUtil", "Google Play services is missing.");
            return 0;
        }
    }
    
    @Deprecated
    @KeepForSdk
    @ShowFirstParty
    public static int getClientVersion(final Context context) {
        Preconditions.checkState(true);
        return ClientLibraryUtils.getClientVersion(context, context.getPackageName());
    }
    
    @Deprecated
    @KeepForSdk
    public static PendingIntent getErrorPendingIntent(final int n, final Context context, final int n2) {
        return GoogleApiAvailabilityLight.getInstance().getErrorResolutionPendingIntent(context, n, n2);
    }
    
    @Deprecated
    @KeepForSdk
    @VisibleForTesting
    public static String getErrorString(final int n) {
        return ConnectionResult.zza(n);
    }
    
    @Deprecated
    @KeepForSdk
    @ShowFirstParty
    public static Intent getGooglePlayServicesAvailabilityRecoveryIntent(final int n) {
        return GoogleApiAvailabilityLight.getInstance().getErrorResolutionIntent(null, n, null);
    }
    
    @KeepForSdk
    public static Context getRemoteContext(Context packageContext) {
        try {
            packageContext = packageContext.createPackageContext("com.google.android.gms", 3);
            return packageContext;
        }
        catch (final PackageManager$NameNotFoundException ex) {
            return null;
        }
    }
    
    @KeepForSdk
    public static Resources getRemoteResource(final Context context) {
        try {
            return context.getPackageManager().getResourcesForApplication("com.google.android.gms");
        }
        catch (final PackageManager$NameNotFoundException ex) {
            return null;
        }
    }
    
    @KeepForSdk
    @ShowFirstParty
    public static boolean honorsDebugCertificates(final Context context) {
        if (!GooglePlayServicesUtilLight.zza) {
            try {
                try {
                    final PackageInfo packageInfo = Wrappers.packageManager(context).getPackageInfo("com.google.android.gms", 64);
                    GoogleSignatureVerifier.getInstance(context);
                    if (packageInfo != null && !GoogleSignatureVerifier.zzb(packageInfo, false) && GoogleSignatureVerifier.zzb(packageInfo, true)) {
                        GooglePlayServicesUtilLight.zzb = true;
                    }
                    GooglePlayServicesUtilLight.zzb = false;
                }
                finally {}
            }
            catch (final PackageManager$NameNotFoundException ex) {
                Log.w("GooglePlayServicesUtil", "Cannot find Google Play services package name.", (Throwable)ex);
            }
            GooglePlayServicesUtilLight.zza = true;
            return GooglePlayServicesUtilLight.zzb || !DeviceProperties.isUserBuild();
            GooglePlayServicesUtilLight.zza = true;
        }
        return GooglePlayServicesUtilLight.zzb || !DeviceProperties.isUserBuild();
    }
    
    @ResultIgnorabilityUnspecified
    @Deprecated
    @KeepForSdk
    @HideFirstParty
    public static int isGooglePlayServicesAvailable(final Context context) {
        return isGooglePlayServicesAvailable(context, GooglePlayServicesUtilLight.GOOGLE_PLAY_SERVICES_VERSION_CODE);
    }
    
    @Deprecated
    @KeepForSdk
    public static int isGooglePlayServicesAvailable(final Context context, int i) {
        try {
            context.getResources().getString(R.string.common_google_play_services_unknown_issue);
        }
        finally {
            Log.e("GooglePlayServicesUtil", "The Google Play services resources were not found. Check your project configuration to ensure that the resources are included.");
        }
        if (!"com.google.android.gms".equals(context.getPackageName())) {
            if (!GooglePlayServicesUtilLight.zzc.get()) {
                final int zza = zzah.zza(context);
                if (zza == 0) {
                    throw new GooglePlayServicesMissingManifestValueException();
                }
                if (zza != GooglePlayServicesUtilLight.GOOGLE_PLAY_SERVICES_VERSION_CODE) {
                    throw new GooglePlayServicesIncorrectManifestValueException(zza);
                }
            }
        }
        final boolean wearableWithoutPlayStore = DeviceProperties.isWearableWithoutPlayStore(context);
        final int n = 1;
        final boolean b = !wearableWithoutPlayStore && !DeviceProperties.zzb(context);
        Preconditions.checkArgument(i >= 0);
        final String packageName = context.getPackageName();
        final PackageManager packageManager = context.getPackageManager();
        Label_0193: {
            while (true) {
                PackageInfo packageInfo = null;
                Block_9: {
                    if (!b) {
                        packageInfo = null;
                        break Block_9;
                    }
                    String s;
                    String str;
                    try {
                        packageInfo = packageManager.getPackageInfo("com.android.vending", 8256);
                        break Label_0193;
                    }
                    catch (final PackageManager$NameNotFoundException ex) {
                        s = String.valueOf(packageName);
                        str = " requires the Google Play Store, but it is missing.";
                    }
                    Log.w("GooglePlayServicesUtil", s.concat(str));
                    i = 9;
                    return i;
                }
                try {
                    final PackageInfo packageInfo2 = packageManager.getPackageInfo("com.google.android.gms", 64);
                    GoogleSignatureVerifier.getInstance(context);
                    if (!GoogleSignatureVerifier.zzb(packageInfo2, true)) {
                        final String s = String.valueOf(packageName);
                        final String str = " requires Google Play services, but their signature is invalid.";
                        continue;
                    }
                    if (b) {
                        Preconditions.checkNotNull(packageInfo);
                        if (!GoogleSignatureVerifier.zzb(packageInfo, true)) {
                            final String s = String.valueOf(packageName);
                            final String str = " requires Google Play Store, but its signature is invalid.";
                            continue;
                        }
                    }
                    if (b && packageInfo != null && !packageInfo.signatures[0].equals((Object)packageInfo2.signatures[0])) {
                        final String s = String.valueOf(packageName);
                        final String str = " requires Google Play Store, but its signature doesn't match that of Google Play services.";
                        continue;
                    }
                    if (com.google.android.gms.common.util.zza.zza(packageInfo2.versionCode) < com.google.android.gms.common.util.zza.zza(i)) {
                        final int versionCode = packageInfo2.versionCode;
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Google Play services out of date for ");
                        sb.append(packageName);
                        sb.append(".  Requires ");
                        sb.append(i);
                        sb.append(" but found ");
                        sb.append(versionCode);
                        Log.w("GooglePlayServicesUtil", sb.toString());
                        i = 2;
                    }
                    else {
                        ApplicationInfo applicationInfo;
                        if ((applicationInfo = packageInfo2.applicationInfo) == null) {
                            try {
                                applicationInfo = packageManager.getApplicationInfo("com.google.android.gms", 0);
                            }
                            catch (final PackageManager$NameNotFoundException ex2) {
                                Log.wtf("GooglePlayServicesUtil", String.valueOf(packageName).concat(" requires Google Play services, but they're missing when getting application info."), (Throwable)ex2);
                                i = n;
                                return i;
                            }
                        }
                        if (applicationInfo.enabled) {
                            return 0;
                        }
                        i = 3;
                    }
                }
                catch (final PackageManager$NameNotFoundException ex3) {
                    Log.w("GooglePlayServicesUtil", String.valueOf(packageName).concat(" requires Google Play services, but they are missing."));
                    i = n;
                }
                break;
            }
        }
        return i;
    }
    
    @Deprecated
    @KeepForSdk
    public static boolean isGooglePlayServicesUid(final Context context, final int n) {
        return UidVerifier.isGooglePlayServicesUid(context, n);
    }
    
    @Deprecated
    @KeepForSdk
    @ShowFirstParty
    public static boolean isPlayServicesPossiblyUpdating(final Context context, final int n) {
        return n == 18 || (n == 1 && zza(context, "com.google.android.gms"));
    }
    
    @Deprecated
    @KeepForSdk
    @ShowFirstParty
    public static boolean isPlayStorePossiblyUpdating(final Context context, final int n) {
        return n == 9 && zza(context, "com.android.vending");
    }
    
    @KeepForSdk
    public static boolean isRestrictedUserProfile(final Context context) {
        if (PlatformVersion.isAtLeastJellyBeanMR2()) {
            final Object systemService = context.getSystemService("user");
            Preconditions.checkNotNull(systemService);
            final Bundle applicationRestrictions = ((UserManager)systemService).getApplicationRestrictions(context.getPackageName());
            if (applicationRestrictions != null && "true".equals(((BaseBundle)applicationRestrictions).getString("restricted_profile"))) {
                return true;
            }
        }
        return false;
    }
    
    @Deprecated
    @KeepForSdk
    @ShowFirstParty
    @VisibleForTesting
    public static boolean isSidewinderDevice(final Context context) {
        return DeviceProperties.isSidewinder(context);
    }
    
    @Deprecated
    @KeepForSdk
    public static boolean isUserRecoverableError(final int n) {
        return n == 1 || n == 2 || n == 3 || n == 9;
    }
    
    @Deprecated
    @KeepForSdk
    public static boolean uidHasPackageName(final Context context, final int n, final String s) {
        return UidVerifier.uidHasPackageName(context, n, s);
    }
    
    public static boolean zza(final Context context, final String s) {
        final boolean equals = s.equals("com.google.android.gms");
        if (PlatformVersion.isAtLeastLollipop()) {
            try {
                final Iterator iterator = context.getPackageManager().getPackageInstaller().getAllSessions().iterator();
                while (iterator.hasNext()) {
                    if (s.equals(((PackageInstaller$SessionInfo)iterator.next()).getAppPackageName())) {
                        return true;
                    }
                }
            }
            catch (final Exception ex) {
                return false;
            }
        }
        final PackageManager packageManager = context.getPackageManager();
        try {
            final ApplicationInfo applicationInfo = packageManager.getApplicationInfo(s, 8192);
            if (equals) {
                return applicationInfo.enabled;
            }
            if (applicationInfo.enabled && !isRestrictedUserProfile(context)) {
                return true;
            }
            return false;
        }
        catch (final PackageManager$NameNotFoundException ex2) {
            return false;
        }
    }
}

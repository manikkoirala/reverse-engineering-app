// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common;

import android.content.ComponentName;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.TimeUnit;
import com.google.errorprone.annotations.ResultIgnorabilityUnspecified;
import com.google.android.gms.common.internal.Preconditions;
import android.os.IBinder;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.BlockingQueue;
import com.google.android.gms.common.annotation.KeepForSdk;
import android.content.ServiceConnection;

@KeepForSdk
public class BlockingServiceConnection implements ServiceConnection
{
    boolean zza;
    private final BlockingQueue zzb;
    
    public BlockingServiceConnection() {
        this.zza = false;
        this.zzb = new LinkedBlockingQueue();
    }
    
    @ResultIgnorabilityUnspecified
    @KeepForSdk
    public IBinder getService() {
        Preconditions.checkNotMainThread("BlockingServiceConnection.getService() called on main thread");
        if (!this.zza) {
            this.zza = true;
            return this.zzb.take();
        }
        throw new IllegalStateException("Cannot call get on this connection more than once");
    }
    
    @ResultIgnorabilityUnspecified
    @KeepForSdk
    public IBinder getServiceWithTimeout(final long n, final TimeUnit timeUnit) {
        Preconditions.checkNotMainThread("BlockingServiceConnection.getServiceWithTimeout() called on main thread");
        if (this.zza) {
            throw new IllegalStateException("Cannot call get on this connection more than once");
        }
        this.zza = true;
        final IBinder binder = this.zzb.poll(n, timeUnit);
        if (binder != null) {
            return binder;
        }
        throw new TimeoutException("Timed out waiting for the service connection");
    }
    
    public final void onServiceConnected(final ComponentName componentName, final IBinder binder) {
        this.zzb.add(binder);
    }
    
    public final void onServiceDisconnected(final ComponentName componentName) {
    }
}

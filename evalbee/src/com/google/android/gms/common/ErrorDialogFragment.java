// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.AlertDialog$Builder;
import android.content.Context;
import android.os.Bundle;
import android.content.DialogInterface;
import android.content.DialogInterface$OnDismissListener;
import com.google.android.gms.common.internal.Preconditions;
import android.content.DialogInterface$OnCancelListener;
import android.app.Dialog;
import android.app.DialogFragment;

public class ErrorDialogFragment extends DialogFragment
{
    private Dialog zaa;
    private DialogInterface$OnCancelListener zab;
    private Dialog zac;
    
    public static ErrorDialogFragment newInstance(final Dialog dialog) {
        return newInstance(dialog, null);
    }
    
    public static ErrorDialogFragment newInstance(Dialog zaa, final DialogInterface$OnCancelListener zab) {
        final ErrorDialogFragment errorDialogFragment = new ErrorDialogFragment();
        zaa = Preconditions.checkNotNull(zaa, "Cannot display null dialog");
        zaa.setOnCancelListener((DialogInterface$OnCancelListener)null);
        zaa.setOnDismissListener((DialogInterface$OnDismissListener)null);
        errorDialogFragment.zaa = zaa;
        if (zab != null) {
            errorDialogFragment.zab = zab;
        }
        return errorDialogFragment;
    }
    
    public void onCancel(final DialogInterface dialogInterface) {
        final DialogInterface$OnCancelListener zab = this.zab;
        if (zab != null) {
            zab.onCancel(dialogInterface);
        }
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        Dialog dialog;
        if ((dialog = this.zaa) == null) {
            this.setShowsDialog(false);
            if (this.zac == null) {
                this.zac = (Dialog)new AlertDialog$Builder((Context)Preconditions.checkNotNull((Context)((Fragment)this).getActivity())).create();
            }
            dialog = this.zac;
        }
        return dialog;
    }
    
    public void show(final FragmentManager fragmentManager, final String s) {
        super.show(fragmentManager, s);
    }
}

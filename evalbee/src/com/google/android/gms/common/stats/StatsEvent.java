// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.stats;

import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Deprecated
@KeepForSdk
public abstract class StatsEvent extends AbstractSafeParcelable implements ReflectedParcelable
{
    @Override
    public final String toString() {
        final long zzb = this.zzb();
        final int zza = this.zza();
        final String zzc = this.zzc();
        final StringBuilder sb = new StringBuilder();
        sb.append(zzb);
        sb.append("\t");
        sb.append(zza);
        sb.append("\t-1");
        sb.append(zzc);
        return sb.toString();
    }
    
    public abstract int zza();
    
    public abstract long zzb();
    
    public abstract String zzc();
    
    @KeepForSdk
    public interface Types
    {
        @KeepForSdk
        public static final int EVENT_TYPE_ACQUIRE_WAKE_LOCK = 7;
        @KeepForSdk
        public static final int EVENT_TYPE_RELEASE_WAKE_LOCK = 8;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.stats;

import com.google.errorprone.annotations.ResultIgnorabilityUnspecified;
import com.google.android.gms.common.util.PlatformVersion;
import com.google.android.gms.common.internal.zzt;
import android.content.ComponentName;
import android.content.pm.PackageManager$NameNotFoundException;
import android.util.Log;
import com.google.android.gms.common.wrappers.Wrappers;
import java.util.concurrent.Executor;
import android.content.Intent;
import java.util.NoSuchElementException;
import android.content.ServiceConnection;
import android.content.Context;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.VisibleForTesting;
import java.util.concurrent.ConcurrentHashMap;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public class ConnectionTracker
{
    private static final Object zzb;
    private static volatile ConnectionTracker zzc;
    @VisibleForTesting
    public final ConcurrentHashMap zza;
    
    static {
        zzb = new Object();
    }
    
    private ConnectionTracker() {
        this.zza = new ConcurrentHashMap();
    }
    
    @KeepForSdk
    public static ConnectionTracker getInstance() {
        if (ConnectionTracker.zzc == null) {
            synchronized (ConnectionTracker.zzb) {
                if (ConnectionTracker.zzc == null) {
                    ConnectionTracker.zzc = new ConnectionTracker();
                }
            }
        }
        final ConnectionTracker zzc = ConnectionTracker.zzc;
        Preconditions.checkNotNull(zzc);
        return zzc;
    }
    
    private static void zzb(final Context context, final ServiceConnection serviceConnection) {
        try {
            context.unbindService(serviceConnection);
        }
        catch (final IllegalArgumentException | IllegalStateException | NoSuchElementException ex) {}
    }
    
    private final boolean zzc(final Context context, final String s, final Intent intent, final ServiceConnection serviceConnection, final int n, boolean b, final Executor executor) {
        final ComponentName component = intent.getComponent();
        while (true) {
            if (component == null) {
                break Label_0062;
            }
            final String packageName = component.getPackageName();
            "com.google.android.gms".equals(packageName);
            try {
                if ((Wrappers.packageManager(context).getApplicationInfo(packageName, 0).flags & 0x200000) != 0x0) {
                    Log.w("ConnectionTracker", "Attempted to bind to a service in a STOPPED package.");
                    return false;
                }
                if (zzd(serviceConnection)) {
                    final ServiceConnection serviceConnection2 = this.zza.putIfAbsent(serviceConnection, serviceConnection);
                    if (serviceConnection2 != null && serviceConnection != serviceConnection2) {
                        Log.w("ConnectionTracker", String.format("Duplicate binding with the same ServiceConnection: %s, %s, %s.", serviceConnection, s, intent.getAction()));
                    }
                    try {
                        b = zze(context, intent, serviceConnection, n, executor);
                        if (b) {
                            return b;
                        }
                        return false;
                    }
                    finally {
                        this.zza.remove(serviceConnection, serviceConnection);
                    }
                }
                b = zze(context, intent, serviceConnection, n, executor);
                return b;
            }
            catch (final PackageManager$NameNotFoundException ex) {
                continue;
            }
            break;
        }
    }
    
    private static boolean zzd(final ServiceConnection serviceConnection) {
        return !(serviceConnection instanceof zzt);
    }
    
    private static final boolean zze(final Context context, final Intent intent, final ServiceConnection serviceConnection, final int n, final Executor executor) {
        if (PlatformVersion.isAtLeastQ() && executor != null) {
            return lk.a(context, intent, n, executor, serviceConnection);
        }
        return context.bindService(intent, serviceConnection, n);
    }
    
    @ResultIgnorabilityUnspecified
    @KeepForSdk
    public boolean bindService(final Context context, final Intent intent, final ServiceConnection serviceConnection, final int n) {
        return this.zzc(context, context.getClass().getName(), intent, serviceConnection, n, true, null);
    }
    
    @KeepForSdk
    public void unbindService(final Context context, final ServiceConnection key) {
        if (zzd(key) && this.zza.containsKey(key)) {
            try {
                zzb(context, this.zza.get(key));
                return;
            }
            finally {
                this.zza.remove(key);
            }
        }
        zzb(context, key);
    }
    
    @KeepForSdk
    public void unbindServiceSafe(final Context context, final ServiceConnection serviceConnection) {
        try {
            this.unbindService(context, serviceConnection);
        }
        catch (final IllegalArgumentException ex) {}
    }
    
    @ResultIgnorabilityUnspecified
    public final boolean zza(final Context context, final String s, final Intent intent, final ServiceConnection serviceConnection, final int n, final Executor executor) {
        return this.zzc(context, s, intent, serviceConnection, 4225, true, executor);
    }
}

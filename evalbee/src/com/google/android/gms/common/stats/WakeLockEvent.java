// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.stats;

import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import java.util.List;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.annotation.KeepForSdk;

@Deprecated
@KeepForSdk
@Class(creator = "WakeLockEventCreator")
public final class WakeLockEvent extends StatsEvent
{
    public static final Parcelable$Creator<WakeLockEvent> CREATOR;
    @VersionField(id = 1)
    final int zza;
    @Field(getter = "getTimeMillis", id = 2)
    private final long zzb;
    @Field(getter = "getEventType", id = 11)
    private final int zzc;
    @Field(getter = "getWakeLockName", id = 4)
    private final String zzd;
    @Field(getter = "getSecondaryWakeLockName", id = 10)
    private final String zze;
    @Field(getter = "getCodePackage", id = 17)
    private final String zzf;
    @Field(getter = "getWakeLockType", id = 5)
    private final int zzg;
    @Field(getter = "getCallingPackages", id = 6)
    private final List zzh;
    @Field(getter = "getEventKey", id = 12)
    private final String zzi;
    @Field(getter = "getElapsedRealtime", id = 8)
    private final long zzj;
    @Field(getter = "getDeviceState", id = 14)
    private final int zzk;
    @Field(getter = "getHostPackage", id = 13)
    private final String zzl;
    @Field(getter = "getBeginPowerPercentage", id = 15)
    private final float zzm;
    @Field(getter = "getTimeout", id = 16)
    private final long zzn;
    @Field(getter = "getAcquiredWithTimeout", id = 18)
    private final boolean zzo;
    
    static {
        CREATOR = (Parcelable$Creator)new zza();
    }
    
    @Constructor
    public WakeLockEvent(@Param(id = 1) final int zza, @Param(id = 2) final long zzb, @Param(id = 11) final int zzc, @Param(id = 4) final String zzd, @Param(id = 5) final int zzg, @Param(id = 6) final List zzh, @Param(id = 12) final String zzi, @Param(id = 8) final long zzj, @Param(id = 14) final int zzk, @Param(id = 10) final String zze, @Param(id = 13) final String zzl, @Param(id = 15) final float zzm, @Param(id = 16) final long zzn, @Param(id = 17) final String zzf, @Param(id = 18) final boolean zzo) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
        this.zzg = zzg;
        this.zzh = zzh;
        this.zzi = zzi;
        this.zzj = zzj;
        this.zzk = zzk;
        this.zzl = zzl;
        this.zzm = zzm;
        this.zzn = zzn;
        this.zzo = zzo;
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.zza);
        SafeParcelWriter.writeLong(parcel, 2, this.zzb);
        SafeParcelWriter.writeString(parcel, 4, this.zzd, false);
        SafeParcelWriter.writeInt(parcel, 5, this.zzg);
        SafeParcelWriter.writeStringList(parcel, 6, this.zzh, false);
        SafeParcelWriter.writeLong(parcel, 8, this.zzj);
        SafeParcelWriter.writeString(parcel, 10, this.zze, false);
        SafeParcelWriter.writeInt(parcel, 11, this.zzc);
        SafeParcelWriter.writeString(parcel, 12, this.zzi, false);
        SafeParcelWriter.writeString(parcel, 13, this.zzl, false);
        SafeParcelWriter.writeInt(parcel, 14, this.zzk);
        SafeParcelWriter.writeFloat(parcel, 15, this.zzm);
        SafeParcelWriter.writeLong(parcel, 16, this.zzn);
        SafeParcelWriter.writeString(parcel, 17, this.zzf, false);
        SafeParcelWriter.writeBoolean(parcel, 18, this.zzo);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    @Override
    public final int zza() {
        return this.zzc;
    }
    
    @Override
    public final long zzb() {
        return this.zzb;
    }
    
    @Override
    public final String zzc() {
        final List zzh = this.zzh;
        final String zzd = this.zzd;
        final int zzg = this.zzg;
        String str = "";
        String join;
        if (zzh == null) {
            join = "";
        }
        else {
            join = TextUtils.join((CharSequence)",", (Iterable)zzh);
        }
        final int zzk = this.zzk;
        String zze;
        if ((zze = this.zze) == null) {
            zze = "";
        }
        String zzl;
        if ((zzl = this.zzl) == null) {
            zzl = "";
        }
        final float zzm = this.zzm;
        final String zzf = this.zzf;
        if (zzf != null) {
            str = zzf;
        }
        final boolean zzo = this.zzo;
        final StringBuilder sb = new StringBuilder();
        sb.append("\t");
        sb.append(zzd);
        sb.append("\t");
        sb.append(zzg);
        sb.append("\t");
        sb.append(join);
        sb.append("\t");
        sb.append(zzk);
        sb.append("\t");
        sb.append(zze);
        sb.append("\t");
        sb.append(zzl);
        sb.append("\t");
        sb.append(zzm);
        sb.append("\t");
        sb.append(str);
        sb.append("\t");
        sb.append(zzo);
        return sb.toString();
    }
}

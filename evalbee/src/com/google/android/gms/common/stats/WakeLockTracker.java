// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.stats;

import java.util.List;
import android.content.Intent;
import android.content.Context;
import com.google.android.gms.common.annotation.KeepForSdk;

@Deprecated
@KeepForSdk
public class WakeLockTracker
{
    private static final WakeLockTracker zza;
    
    static {
        zza = new WakeLockTracker();
    }
    
    @KeepForSdk
    public static WakeLockTracker getInstance() {
        return WakeLockTracker.zza;
    }
    
    @KeepForSdk
    public void registerAcquireEvent(final Context context, final Intent intent, final String s, final String s2, final String s3, final int n, final String s4) {
    }
    
    @KeepForSdk
    public void registerDeadlineEvent(final Context context, final String s, final String s2, final String s3, final int n, final List<String> list, final boolean b, final long n2) {
    }
    
    @KeepForSdk
    public void registerEvent(final Context context, final String s, final int n, final String s2, final String s3, final String s4, final int n2, final List<String> list) {
    }
    
    @KeepForSdk
    public void registerEvent(final Context context, final String s, final int n, final String s2, final String s3, final String s4, final int n2, final List<String> list, final long n3) {
    }
    
    @KeepForSdk
    public void registerReleaseEvent(final Context context, final Intent intent) {
    }
}

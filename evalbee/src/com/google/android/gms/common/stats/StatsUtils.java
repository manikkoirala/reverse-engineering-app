// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.stats;

import android.text.TextUtils;
import android.os.Process;
import android.os.PowerManager$WakeLock;
import com.google.android.gms.common.annotation.KeepForSdk;

@Deprecated
@KeepForSdk
public class StatsUtils
{
    @KeepForSdk
    public static String getEventKey(final PowerManager$WakeLock powerManager$WakeLock, final String s) {
        final long n = Process.myPid();
        final long n2 = System.identityHashCode(powerManager$WakeLock);
        String obj = s;
        if (TextUtils.isEmpty((CharSequence)s)) {
            obj = "";
        }
        return String.valueOf(String.valueOf(n << 32 | n2)).concat(String.valueOf(obj));
    }
}

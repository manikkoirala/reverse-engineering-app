// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common;

public class PackageVerificationResult
{
    private final String zza;
    private final boolean zzb;
    private final String zzc;
    private final Throwable zzd;
    
    private PackageVerificationResult(final String zza, final int n, final boolean zzb, final String zzc, final Throwable zzd) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
    }
    
    public static PackageVerificationResult zza(final String s, final String s2, final Throwable t) {
        return new PackageVerificationResult(s, 1, false, s2, t);
    }
    
    public static PackageVerificationResult zzd(final String s, final int n) {
        return new PackageVerificationResult(s, n, true, null, null);
    }
    
    public final void zzb() {
        if (this.zzb) {
            return;
        }
        final String value = String.valueOf(this.zzc);
        final Throwable zzd = this.zzd;
        final String concat = "PackageVerificationRslt: ".concat(value);
        if (zzd != null) {
            throw new SecurityException(concat, zzd);
        }
        throw new SecurityException(concat);
    }
    
    public final boolean zzc() {
        return this.zzb;
    }
}

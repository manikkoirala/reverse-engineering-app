// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common;

import java.lang.ref.WeakReference;

abstract class zzl extends zzj
{
    private static final WeakReference zza;
    private WeakReference zzb;
    
    static {
        zza = new WeakReference(null);
    }
    
    public zzl(final byte[] array) {
        super(array);
        this.zzb = zzl.zza;
    }
    
    public abstract byte[] zzb();
    
    @Override
    public final byte[] zzf() {
        synchronized (this) {
            byte[] zzb;
            if ((zzb = (byte[])this.zzb.get()) == null) {
                zzb = this.zzb();
                this.zzb = new WeakReference(zzb);
            }
            return zzb;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.logging;

import java.util.Locale;
import android.util.Log;
import com.google.android.gms.common.internal.GmsLogger;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public class Logger
{
    private final String zza;
    private final String zzb;
    private final GmsLogger zzc;
    private final int zzd;
    
    @KeepForSdk
    public Logger(final String zza, final String... array) {
        final int length = array.length;
        String string;
        if (length == 0) {
            string = "";
        }
        else {
            final StringBuilder sb = new StringBuilder();
            sb.append('[');
            for (final String str : array) {
                if (sb.length() > 1) {
                    sb.append(",");
                }
                sb.append(str);
            }
            sb.append("] ");
            string = sb.toString();
        }
        this.zzb = string;
        this.zza = zza;
        this.zzc = new GmsLogger(zza);
        int zzd;
        for (zzd = 2; zzd <= 7 && !Log.isLoggable(this.zza, zzd); ++zzd) {}
        this.zzd = zzd;
    }
    
    @KeepForSdk
    public void d(final String s, final Object... array) {
        if (this.isLoggable(3)) {
            Log.d(this.zza, this.format(s, array));
        }
    }
    
    @KeepForSdk
    public void e(final String s, final Throwable t, final Object... array) {
        Log.e(this.zza, this.format(s, array), t);
    }
    
    @KeepForSdk
    public void e(final String s, final Object... array) {
        Log.e(this.zza, this.format(s, array));
    }
    
    @KeepForSdk
    public String format(final String format, final Object... args) {
        String format2 = format;
        if (args != null) {
            format2 = format;
            if (args.length > 0) {
                format2 = String.format(Locale.US, format, args);
            }
        }
        return this.zzb.concat(format2);
    }
    
    @KeepForSdk
    public String getTag() {
        return this.zza;
    }
    
    @KeepForSdk
    public void i(final String s, final Object... array) {
        Log.i(this.zza, this.format(s, array));
    }
    
    @KeepForSdk
    public boolean isLoggable(final int n) {
        return this.zzd <= n;
    }
    
    @KeepForSdk
    public void v(final String s, final Throwable t, final Object... array) {
        if (this.isLoggable(2)) {
            Log.v(this.zza, this.format(s, array), t);
        }
    }
    
    @KeepForSdk
    public void v(final String s, final Object... array) {
        if (this.isLoggable(2)) {
            Log.v(this.zza, this.format(s, array));
        }
    }
    
    @KeepForSdk
    public void w(final String s, final Object... array) {
        Log.w(this.zza, this.format(s, array));
    }
    
    @KeepForSdk
    public void wtf(final String s, final Throwable t, final Object... array) {
        Log.wtf(this.zza, this.format(s, array), t);
    }
    
    @KeepForSdk
    public void wtf(final Throwable t) {
        Log.wtf(this.zza, t);
    }
}

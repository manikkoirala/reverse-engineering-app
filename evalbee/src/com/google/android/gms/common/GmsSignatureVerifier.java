// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common;

import java.util.List;
import com.google.android.gms.internal.common.zzag;
import java.util.HashMap;
import com.google.android.gms.common.internal.ShowFirstParty;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.errorprone.annotations.RestrictedInheritance;

@RestrictedInheritance(allowedOnPath = ".*javatests/com/google/android/gmscore/integ/client/common/robolectric/.*", explanation = "Sub classing of GMS Core's APIs are restricted to testing fakes.", link = "go/gmscore-restrictedinheritance")
@KeepForSdk
@ShowFirstParty
public class GmsSignatureVerifier
{
    private static final zzab zza;
    private static final zzab zzb;
    private static final HashMap zzc;
    
    static {
        final zzz zzz = new zzz();
        zzz.zzd("com.google.android.gms");
        zzz.zza(204200000L);
        final zzl zzd = zzn.zzd;
        zzz.zzc((List)zzag.zzn((Object)zzd.zzf(), (Object)zzn.zzb.zzf()));
        final zzl zzc2 = zzn.zzc;
        zzz.zzb((List)zzag.zzn((Object)zzc2.zzf(), (Object)zzn.zza.zzf()));
        zza = zzz.zze();
        final zzz zzz2 = new zzz();
        zzz2.zzd("com.android.vending");
        zzz2.zza(82240000L);
        zzz2.zzc((List)zzag.zzm((Object)zzd.zzf()));
        zzz2.zzb((List)zzag.zzm((Object)zzc2.zzf()));
        zzb = zzz2.zze();
        zzc = new HashMap();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "GoogleCertificatesLookupResponseCreator")
public final class zzq extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zzq> CREATOR;
    @Field(getter = "getResult", id = 1)
    private final boolean zza;
    @Field(getter = "getErrorMessage", id = 2)
    private final String zzb;
    @Field(getter = "getStatusValue", id = 3)
    private final int zzc;
    @Field(getter = "getFirstPartyStatusValue", id = 4)
    private final int zzd;
    
    static {
        CREATOR = (Parcelable$Creator)new zzr();
    }
    
    @Constructor
    public zzq(@Param(id = 1) final boolean zza, @Param(id = 2) final String zzb, @Param(id = 3) final int n, @Param(id = 4) final int n2) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzy.zza(n) - 1;
        this.zzd = com.google.android.gms.common.zzd.zza(n2) - 1;
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeBoolean(parcel, 1, this.zza);
        SafeParcelWriter.writeString(parcel, 2, this.zzb, false);
        SafeParcelWriter.writeInt(parcel, 3, this.zzc);
        SafeParcelWriter.writeInt(parcel, 4, this.zzd);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public final String zza() {
        return this.zzb;
    }
    
    public final boolean zzb() {
        return this.zza;
    }
    
    public final int zzc() {
        return com.google.android.gms.common.zzd.zza(this.zzd);
    }
    
    public final int zzd() {
        return zzy.zza(this.zzc);
    }
}

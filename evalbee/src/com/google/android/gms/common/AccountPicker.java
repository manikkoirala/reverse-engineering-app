// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common;

import android.os.BaseBundle;
import java.util.Collection;
import java.util.List;
import com.google.errorprone.annotations.ResultIgnorabilityUnspecified;
import android.os.Parcelable;
import java.io.Serializable;
import com.google.android.gms.common.internal.Preconditions;
import android.content.Intent;
import android.os.Bundle;
import java.util.ArrayList;
import android.accounts.Account;

public final class AccountPicker
{
    private AccountPicker() {
    }
    
    @ResultIgnorabilityUnspecified
    @Deprecated
    public static Intent newChooseAccountIntent(final Account account, final ArrayList<Account> list, final String[] array, final boolean b, final String s, final String s2, final String[] array2, final Bundle bundle) {
        final Intent intent = new Intent();
        Preconditions.checkArgument(true, (Object)"We only support hostedDomain filter for account chip styled account picker");
        intent.setAction("com.google.android.gms.common.account.CHOOSE_ACCOUNT");
        intent.setPackage("com.google.android.gms");
        intent.putExtra("allowableAccounts", (Serializable)list);
        intent.putExtra("allowableAccountTypes", array);
        intent.putExtra("addAccountOptions", bundle);
        intent.putExtra("selectedAccount", (Parcelable)account);
        intent.putExtra("alwaysPromptForAccount", b);
        intent.putExtra("descriptionTextOverride", s);
        intent.putExtra("authTokenType", s2);
        intent.putExtra("addAccountRequiredFeatures", array2);
        intent.putExtra("setGmsCoreAccount", false);
        intent.putExtra("overrideTheme", 0);
        intent.putExtra("overrideCustomTheme", 0);
        intent.putExtra("hostedDomainFilter", (String)null);
        return intent;
    }
    
    public static Intent newChooseAccountIntent(final AccountChooserOptions accountChooserOptions) {
        final Intent intent = new Intent();
        AccountChooserOptions.zzD(accountChooserOptions);
        AccountChooserOptions.zze(accountChooserOptions);
        Preconditions.checkArgument(true, (Object)"We only support hostedDomain filter for account chip styled account picker");
        AccountChooserOptions.zzd(accountChooserOptions);
        Preconditions.checkArgument(true, (Object)"Consent is only valid for account chip styled account picker");
        AccountChooserOptions.zzB(accountChooserOptions);
        Preconditions.checkArgument(true, (Object)"Making the selected account non-clickable is only supported for the THEME_DAY_NIGHT_GOOGLE_MATERIAL2, THEME_LIGHT_GOOGLE_MATERIAL3, THEME_DARK_GOOGLE_MATERIAL3 or THEME_DAY_NIGHT_GOOGLE_MATERIAL3 themes");
        AccountChooserOptions.zzD(accountChooserOptions);
        intent.setAction("com.google.android.gms.common.account.CHOOSE_ACCOUNT");
        intent.setPackage("com.google.android.gms");
        intent.putExtra("allowableAccounts", (Serializable)AccountChooserOptions.zzi(accountChooserOptions));
        if (AccountChooserOptions.zzh(accountChooserOptions) != null) {
            intent.putExtra("allowableAccountTypes", (String[])AccountChooserOptions.zzh(accountChooserOptions).toArray(new String[0]));
        }
        intent.putExtra("addAccountOptions", AccountChooserOptions.zzc(accountChooserOptions));
        intent.putExtra("selectedAccount", (Parcelable)AccountChooserOptions.zzb(accountChooserOptions));
        AccountChooserOptions.zzB(accountChooserOptions);
        intent.putExtra("selectedAccountIsNotClickable", false);
        intent.putExtra("alwaysPromptForAccount", AccountChooserOptions.zzy(accountChooserOptions));
        intent.putExtra("descriptionTextOverride", AccountChooserOptions.zzg(accountChooserOptions));
        AccountChooserOptions.zzC(accountChooserOptions);
        intent.putExtra("setGmsCoreAccount", false);
        AccountChooserOptions.zzf(accountChooserOptions);
        intent.putExtra("realClientPackage", (String)null);
        AccountChooserOptions.zza(accountChooserOptions);
        intent.putExtra("overrideTheme", 0);
        AccountChooserOptions.zzD(accountChooserOptions);
        intent.putExtra("overrideCustomTheme", 0);
        AccountChooserOptions.zze(accountChooserOptions);
        intent.putExtra("hostedDomainFilter", (String)null);
        final Bundle bundle = new Bundle();
        AccountChooserOptions.zzD(accountChooserOptions);
        AccountChooserOptions.zzd(accountChooserOptions);
        AccountChooserOptions.zzz(accountChooserOptions);
        AccountChooserOptions.zzA(accountChooserOptions);
        if (!((BaseBundle)bundle).isEmpty()) {
            intent.putExtra("first_party_options_bundle", bundle);
        }
        return intent;
    }
    
    public static class AccountChooserOptions
    {
        private Account zza;
        private boolean zzb;
        private ArrayList zzc;
        private ArrayList zzd;
        private boolean zze;
        private String zzf;
        private Bundle zzg;
        private boolean zzh;
        private int zzi;
        private String zzj;
        private boolean zzk;
        private zza zzl;
        private String zzm;
        private boolean zzn;
        private boolean zzo;
        
        public static class Builder
        {
            private Account zza;
            private ArrayList zzb;
            private ArrayList zzc;
            private boolean zzd;
            private String zze;
            private Bundle zzf;
            
            public Builder() {
                this.zzd = false;
            }
            
            public AccountChooserOptions build() {
                Preconditions.checkArgument(true, (Object)"We only support hostedDomain filter for account chip styled account picker");
                Preconditions.checkArgument(true, (Object)"Consent is only valid for account chip styled account picker");
                final AccountChooserOptions accountChooserOptions = new AccountChooserOptions();
                AccountChooserOptions.zzj(accountChooserOptions, this.zzc);
                AccountChooserOptions.zzk(accountChooserOptions, this.zzb);
                AccountChooserOptions.zzl(accountChooserOptions, this.zzd);
                AccountChooserOptions.zzm(accountChooserOptions, (zza)null);
                AccountChooserOptions.zzp(accountChooserOptions, (String)null);
                AccountChooserOptions.zzq(accountChooserOptions, this.zzf);
                AccountChooserOptions.zzs(accountChooserOptions, this.zza);
                AccountChooserOptions.zzt(accountChooserOptions, false);
                AccountChooserOptions.zzu(accountChooserOptions, false);
                AccountChooserOptions.zzr(accountChooserOptions, (String)null);
                AccountChooserOptions.zzv(accountChooserOptions, 0);
                AccountChooserOptions.zzw(accountChooserOptions, this.zze);
                AccountChooserOptions.zzx(accountChooserOptions, false);
                AccountChooserOptions.zzn(accountChooserOptions, false);
                AccountChooserOptions.zzo(accountChooserOptions, false);
                return accountChooserOptions;
            }
            
            public Builder setAllowableAccounts(final List<Account> c) {
                ArrayList zzb;
                if (c == null) {
                    zzb = null;
                }
                else {
                    zzb = new ArrayList((Collection<? extends E>)c);
                }
                this.zzb = zzb;
                return this;
            }
            
            public Builder setAllowableAccountsTypes(final List<String> c) {
                ArrayList zzc;
                if (c == null) {
                    zzc = null;
                }
                else {
                    zzc = new ArrayList((Collection<? extends E>)c);
                }
                this.zzc = zzc;
                return this;
            }
            
            public Builder setAlwaysShowAccountPicker(final boolean zzd) {
                this.zzd = zzd;
                return this;
            }
            
            public Builder setOptionsForAddingAccount(final Bundle zzf) {
                this.zzf = zzf;
                return this;
            }
            
            public Builder setSelectedAccount(final Account zza) {
                this.zza = zza;
                return this;
            }
            
            public Builder setTitleOverrideText(final String zze) {
                this.zze = zze;
                return this;
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common;

import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.common.internal.zzaa;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.zzz;

abstract class zzj extends zzz
{
    private final int zza;
    
    public zzj(final byte[] a) {
        Preconditions.checkArgument(a.length == 25);
        this.zza = Arrays.hashCode(a);
    }
    
    public static byte[] zze(final String s) {
        try {
            return s.getBytes("ISO-8859-1");
        }
        catch (final UnsupportedEncodingException detailMessage) {
            throw new AssertionError((Object)detailMessage);
        }
    }
    
    public final boolean equals(final Object o) {
        if (o != null) {
            if (o instanceof zzaa) {
                try {
                    final zzaa zzaa = (zzaa)o;
                    if (zzaa.zzc() != this.zza) {
                        return false;
                    }
                    final IObjectWrapper zzd = zzaa.zzd();
                    return zzd != null && Arrays.equals(this.zzf(), (byte[])ObjectWrapper.unwrap(zzd));
                }
                catch (final RemoteException ex) {
                    Log.e("GoogleCertificates", "Failed to get Google certificates from remote", (Throwable)ex);
                }
            }
        }
        return false;
    }
    
    public final int hashCode() {
        return this.zza;
    }
    
    public final int zzc() {
        return this.zza;
    }
    
    public final IObjectWrapper zzd() {
        return ObjectWrapper.wrap(this.zzf());
    }
    
    public abstract byte[] zzf();
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.sqlite;

import android.database.AbstractCursor;
import com.google.errorprone.annotations.ResultIgnorabilityUnspecified;
import android.database.CursorWindow;
import android.database.Cursor;
import android.database.AbstractWindowedCursor;
import com.google.android.gms.common.annotation.KeepForSdk;
import android.database.CrossProcessCursor;

@KeepForSdk
public class CursorWrapper extends android.database.CursorWrapper implements CrossProcessCursor
{
    private AbstractWindowedCursor zza;
    
    @KeepForSdk
    public CursorWrapper(Cursor wrappedCursor) {
        super(wrappedCursor);
        for (int n = 0; n < 10 && wrappedCursor instanceof android.database.CursorWrapper; wrappedCursor = ((android.database.CursorWrapper)wrappedCursor).getWrappedCursor(), ++n) {}
        if (wrappedCursor instanceof AbstractWindowedCursor) {
            this.zza = (AbstractWindowedCursor)wrappedCursor;
            return;
        }
        throw new IllegalArgumentException("Unknown type: ".concat(wrappedCursor.getClass().getName()));
    }
    
    @KeepForSdk
    public void fillWindow(final int n, final CursorWindow cursorWindow) {
        ((AbstractCursor)this.zza).fillWindow(n, cursorWindow);
    }
    
    @ResultIgnorabilityUnspecified
    @KeepForSdk
    public CursorWindow getWindow() {
        return this.zza.getWindow();
    }
    
    @ResultIgnorabilityUnspecified
    public final boolean onMove(final int n, final int n2) {
        return ((AbstractCursor)this.zza).onMove(n, n2);
    }
    
    @KeepForSdk
    public void setWindow(final CursorWindow window) {
        this.zza.setWindow(window);
    }
}

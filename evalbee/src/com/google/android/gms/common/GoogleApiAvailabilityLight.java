// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common;

import android.net.Uri$Builder;
import com.google.android.gms.common.internal.HideFirstParty;
import com.google.errorprone.annotations.ResultIgnorabilityUnspecified;
import com.google.android.gms.internal.common.zzd;
import android.app.PendingIntent;
import android.content.pm.PackageManager$NameNotFoundException;
import com.google.android.gms.common.wrappers.Wrappers;
import android.text.TextUtils;
import com.google.android.gms.common.util.DeviceProperties;
import android.net.Uri;
import android.content.Intent;
import android.content.Context;
import com.google.android.gms.common.internal.ShowFirstParty;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
@ShowFirstParty
public class GoogleApiAvailabilityLight
{
    @KeepForSdk
    public static final String GOOGLE_PLAY_SERVICES_PACKAGE = "com.google.android.gms";
    @KeepForSdk
    public static final int GOOGLE_PLAY_SERVICES_VERSION_CODE;
    @KeepForSdk
    public static final String GOOGLE_PLAY_STORE_PACKAGE = "com.android.vending";
    @KeepForSdk
    static final String TRACKING_SOURCE_DIALOG = "d";
    @KeepForSdk
    static final String TRACKING_SOURCE_NOTIFICATION = "n";
    private static final GoogleApiAvailabilityLight zza;
    
    static {
        GOOGLE_PLAY_SERVICES_VERSION_CODE = GooglePlayServicesUtilLight.GOOGLE_PLAY_SERVICES_VERSION_CODE;
        zza = new GoogleApiAvailabilityLight();
    }
    
    @KeepForSdk
    public GoogleApiAvailabilityLight() {
    }
    
    @KeepForSdk
    public static GoogleApiAvailabilityLight getInstance() {
        return GoogleApiAvailabilityLight.zza;
    }
    
    @KeepForSdk
    public void cancelAvailabilityErrorNotifications(final Context context) {
        GooglePlayServicesUtilLight.cancelAvailabilityErrorNotifications(context);
    }
    
    @KeepForSdk
    @ShowFirstParty
    public int getApkVersion(final Context context) {
        return GooglePlayServicesUtilLight.getApkVersion(context);
    }
    
    @KeepForSdk
    @ShowFirstParty
    public int getClientVersion(final Context context) {
        return GooglePlayServicesUtilLight.getClientVersion(context);
    }
    
    @Deprecated
    @KeepForSdk
    @ShowFirstParty
    public Intent getErrorResolutionIntent(final int n) {
        return this.getErrorResolutionIntent(null, n, null);
    }
    
    @KeepForSdk
    @ShowFirstParty
    public Intent getErrorResolutionIntent(final Context context, final int n, String string) {
        if (n != 1 && n != 2) {
            if (n != 3) {
                return null;
            }
            final Uri fromParts = Uri.fromParts("package", "com.google.android.gms", (String)null);
            final Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
            intent.setData(fromParts);
            return intent;
        }
        else {
            if (context != null && DeviceProperties.isWearableWithoutPlayStore(context)) {
                final Intent intent2 = new Intent("com.google.android.clockwork.home.UPDATE_ANDROID_WEAR_ACTION");
                intent2.setPackage("com.google.android.wearable.app");
                return intent2;
            }
            Object appendQueryParameter = new StringBuilder();
            ((StringBuilder)appendQueryParameter).append("gcore_");
            ((StringBuilder)appendQueryParameter).append(GoogleApiAvailabilityLight.GOOGLE_PLAY_SERVICES_VERSION_CODE);
            ((StringBuilder)appendQueryParameter).append("-");
            if (!TextUtils.isEmpty((CharSequence)string)) {
                ((StringBuilder)appendQueryParameter).append(string);
            }
            ((StringBuilder)appendQueryParameter).append("-");
            if (context != null) {
                ((StringBuilder)appendQueryParameter).append(context.getPackageName());
            }
            ((StringBuilder)appendQueryParameter).append("-");
            while (true) {
                if (context == null) {
                    break Label_0180;
                }
                try {
                    ((StringBuilder)appendQueryParameter).append(Wrappers.packageManager(context).getPackageInfo(context.getPackageName(), 0).versionCode);
                    string = ((StringBuilder)appendQueryParameter).toString();
                    final Intent intent3 = new Intent("android.intent.action.VIEW");
                    appendQueryParameter = Uri.parse("market://details").buildUpon().appendQueryParameter("id", "com.google.android.gms");
                    if (!TextUtils.isEmpty((CharSequence)string)) {
                        ((Uri$Builder)appendQueryParameter).appendQueryParameter("pcampaignid", string);
                    }
                    intent3.setData(((Uri$Builder)appendQueryParameter).build());
                    intent3.setPackage("com.android.vending");
                    intent3.addFlags(524288);
                    return intent3;
                }
                catch (final PackageManager$NameNotFoundException ex) {
                    continue;
                }
                break;
            }
        }
    }
    
    @KeepForSdk
    public PendingIntent getErrorResolutionPendingIntent(final Context context, final int n, final int n2) {
        return this.getErrorResolutionPendingIntent(context, n, n2, null);
    }
    
    @KeepForSdk
    @ShowFirstParty
    public PendingIntent getErrorResolutionPendingIntent(final Context context, final int n, final int n2, final String s) {
        final Intent errorResolutionIntent = this.getErrorResolutionIntent(context, n, s);
        if (errorResolutionIntent == null) {
            return null;
        }
        return PendingIntent.getActivity(context, n2, errorResolutionIntent, zzd.zza | 0x8000000);
    }
    
    @KeepForSdk
    public String getErrorString(final int n) {
        return GooglePlayServicesUtilLight.getErrorString(n);
    }
    
    @ResultIgnorabilityUnspecified
    @KeepForSdk
    @HideFirstParty
    public int isGooglePlayServicesAvailable(final Context context) {
        return this.isGooglePlayServicesAvailable(context, GoogleApiAvailabilityLight.GOOGLE_PLAY_SERVICES_VERSION_CODE);
    }
    
    @KeepForSdk
    public int isGooglePlayServicesAvailable(final Context context, int googlePlayServicesAvailable) {
        googlePlayServicesAvailable = GooglePlayServicesUtilLight.isGooglePlayServicesAvailable(context, googlePlayServicesAvailable);
        if (GooglePlayServicesUtilLight.isPlayServicesPossiblyUpdating(context, googlePlayServicesAvailable)) {
            return 18;
        }
        return googlePlayServicesAvailable;
    }
    
    @KeepForSdk
    @ShowFirstParty
    public boolean isPlayServicesPossiblyUpdating(final Context context, final int n) {
        return GooglePlayServicesUtilLight.isPlayServicesPossiblyUpdating(context, n);
    }
    
    @KeepForSdk
    @ShowFirstParty
    public boolean isPlayStorePossiblyUpdating(final Context context, final int n) {
        return GooglePlayServicesUtilLight.isPlayStorePossiblyUpdating(context, n);
    }
    
    @KeepForSdk
    public boolean isUninstalledAppPossiblyUpdating(final Context context, final String s) {
        return GooglePlayServicesUtilLight.zza(context, s);
    }
    
    @KeepForSdk
    public boolean isUserResolvableError(final int n) {
        return GooglePlayServicesUtilLight.isUserRecoverableError(n);
    }
    
    @KeepForSdk
    public void verifyGooglePlayServicesIsAvailable(final Context context, final int n) {
        GooglePlayServicesUtilLight.ensurePlayServicesAvailable(context, n);
    }
}

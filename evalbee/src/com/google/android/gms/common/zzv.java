// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common;

import java.util.concurrent.Callable;

final class zzv extends zzx
{
    private final Callable zze = zze;
    
    @Override
    public final String zza() {
        try {
            return this.zze.call();
        }
        catch (final Exception cause) {
            throw new RuntimeException(cause);
        }
    }
}

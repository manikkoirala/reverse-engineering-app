// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common;

import java.util.Arrays;

final class zzk extends zzj
{
    private final byte[] zza;
    
    public zzk(final byte[] array) {
        super(Arrays.copyOfRange(array, 0, 25));
        this.zza = array;
    }
    
    @Override
    public final byte[] zzf() {
        return this.zza;
    }
}

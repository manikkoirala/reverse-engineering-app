// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@KeepForSdk
@Class(creator = "FeatureCreator")
public class Feature extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<Feature> CREATOR;
    @Field(getter = "getName", id = 1)
    private final String zza;
    @Deprecated
    @Field(getter = "getOldVersion", id = 2)
    private final int zzb;
    @Field(defaultValue = "-1", getter = "getVersion", id = 3)
    private final long zzc;
    
    static {
        CREATOR = (Parcelable$Creator)new zzc();
    }
    
    @Constructor
    public Feature(@Param(id = 1) final String zza, @Param(id = 2) final int zzb, @Param(id = 3) final long zzc) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    @KeepForSdk
    public Feature(final String zza, final long zzc) {
        this.zza = zza;
        this.zzc = zzc;
        this.zzb = -1;
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (o instanceof Feature) {
            final Feature feature = (Feature)o;
            if (((this.getName() != null && this.getName().equals(feature.getName())) || (this.getName() == null && feature.getName() == null)) && this.getVersion() == feature.getVersion()) {
                return true;
            }
        }
        return false;
    }
    
    @KeepForSdk
    public String getName() {
        return this.zza;
    }
    
    @KeepForSdk
    public long getVersion() {
        long zzc;
        if ((zzc = this.zzc) == -1L) {
            zzc = this.zzb;
        }
        return zzc;
    }
    
    @Override
    public final int hashCode() {
        return Objects.hashCode(this.getName(), this.getVersion());
    }
    
    @Override
    public final String toString() {
        final Objects.ToStringHelper stringHelper = Objects.toStringHelper(this);
        stringHelper.add("name", this.getName());
        stringHelper.add("version", this.getVersion());
        return stringHelper.toString();
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.getName(), false);
        SafeParcelWriter.writeInt(parcel, 2, this.zzb);
        SafeParcelWriter.writeLong(parcel, 3, this.getVersion());
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.server.response;

import android.os.BaseBundle;
import java.math.BigInteger;
import java.math.BigDecimal;
import android.os.Parcelable;
import java.util.List;
import java.util.ArrayList;
import com.google.android.gms.common.util.MapUtils;
import java.util.Set;
import java.io.Serializable;
import android.os.Bundle;
import com.google.android.gms.common.util.ArrayUtils;
import com.google.android.gms.common.util.Base64Utils;
import com.google.android.gms.common.util.JsonUtils;
import java.util.HashMap;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import android.util.SparseArray;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import java.util.Iterator;
import java.util.Map;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
@Class(creator = "SafeParcelResponseCreator")
@VisibleForTesting
public class SafeParcelResponse extends FastSafeParcelableJsonResponse
{
    @KeepForSdk
    public static final Parcelable$Creator<SafeParcelResponse> CREATOR;
    @VersionField(getter = "getVersionCode", id = 1)
    private final int zaa;
    @SafeParcelable.Field(getter = "getParcel", id = 2)
    private final Parcel zab;
    private final int zac;
    @SafeParcelable.Field(getter = "getFieldMappingDictionary", id = 3)
    private final zan zad;
    private final String zae;
    private int zaf;
    private int zag;
    
    static {
        CREATOR = (Parcelable$Creator)new zaq();
    }
    
    @Constructor
    public SafeParcelResponse(@Param(id = 1) final int zaa, @Param(id = 2) final Parcel parcel, @Param(id = 3) final zan zad) {
        this.zaa = zaa;
        this.zab = Preconditions.checkNotNull(parcel);
        this.zac = 2;
        this.zad = zad;
        String zaa2;
        if (zad == null) {
            zaa2 = null;
        }
        else {
            zaa2 = zad.zaa();
        }
        this.zae = zaa2;
        this.zaf = 2;
    }
    
    private SafeParcelResponse(final SafeParcelable safeParcelable, final zan zan, final String s) {
        this.zaa = 1;
        ((Parcelable)safeParcelable).writeToParcel(this.zab = Parcel.obtain(), 0);
        this.zac = 1;
        this.zad = Preconditions.checkNotNull(zan);
        this.zae = Preconditions.checkNotNull(s);
        this.zaf = 2;
    }
    
    public SafeParcelResponse(final zan zan, final String s) {
        this.zaa = 1;
        this.zab = Parcel.obtain();
        this.zac = 0;
        this.zad = Preconditions.checkNotNull(zan);
        this.zae = Preconditions.checkNotNull(s);
        this.zaf = 0;
    }
    
    @KeepForSdk
    public static <T extends FastJsonResponse & SafeParcelable> SafeParcelResponse from(final T t) {
        final String s = Preconditions.checkNotNull(t.getClass().getCanonicalName());
        final zan zan = new zan(t.getClass());
        zaF(zan, t);
        zan.zac();
        zan.zad();
        return new SafeParcelResponse(t, zan, s);
    }
    
    private static void zaF(final zan zan, FastJsonResponse fastJsonResponse) {
        final java.lang.Class<? extends FastJsonResponse> class1 = fastJsonResponse.getClass();
        if (!zan.zaf(class1)) {
            final Map<String, FastJsonResponse.Field<?, ?>> fieldMappings = fastJsonResponse.getFieldMappings();
            zan.zae(class1, fieldMappings);
            final Iterator<String> iterator = fieldMappings.keySet().iterator();
            while (iterator.hasNext()) {
                fastJsonResponse = (FastJsonResponse)fieldMappings.get(iterator.next());
                final java.lang.Class<? extends FastJsonResponse> zag = ((FastJsonResponse.Field)fastJsonResponse).zag;
                if (zag != null) {
                    try {
                        zaF(zan, (FastJsonResponse)zag.newInstance());
                        continue;
                    }
                    catch (final IllegalAccessException cause) {
                        final String value = String.valueOf(Preconditions.checkNotNull(((FastJsonResponse.Field)fastJsonResponse).zag).getCanonicalName());
                        String concat;
                        if (value.length() != 0) {
                            concat = "Could not access object of type ".concat(value);
                        }
                        else {
                            concat = new String("Could not access object of type ");
                        }
                        throw new IllegalStateException(concat, cause);
                    }
                    catch (final InstantiationException cause2) {
                        final String value2 = String.valueOf(Preconditions.checkNotNull(((FastJsonResponse.Field)fastJsonResponse).zag).getCanonicalName());
                        String concat2;
                        if (value2.length() != 0) {
                            concat2 = "Could not instantiate an object of type ".concat(value2);
                        }
                        else {
                            concat2 = new String("Could not instantiate an object of type ");
                        }
                        throw new IllegalStateException(concat2, cause2);
                    }
                    break;
                }
            }
        }
    }
    
    private final void zaG(final FastJsonResponse.Field<?, ?> field) {
        if (field.zaf == -1) {
            throw new IllegalStateException("Field does not have a valid safe parcelable field id.");
        }
        final Parcel zab = this.zab;
        if (zab == null) {
            throw new IllegalStateException("Internal Parcel object is null.");
        }
        final int zaf = this.zaf;
        if (zaf == 0) {
            this.zag = SafeParcelWriter.beginObjectHeader(zab);
            this.zaf = 1;
            return;
        }
        if (zaf == 1) {
            return;
        }
        throw new IllegalStateException("Attempted to parse JSON with a SafeParcelResponse object that is already filled with data.");
    }
    
    private final void zaH(final StringBuilder sb, final Map<String, FastJsonResponse.Field<?, ?>> map, final Parcel parcel) {
        final SparseArray sparseArray = new SparseArray();
        for (final Map.Entry<K, FastJsonResponse.Field> entry : map.entrySet()) {
            sparseArray.put(entry.getValue().getSafeParcelableFieldId(), (Object)entry);
        }
        sb.append('{');
        final int validateObjectHeader = SafeParcelReader.validateObjectHeader(parcel);
        int n = 0;
        while (parcel.dataPosition() < validateObjectHeader) {
            final int header = SafeParcelReader.readHeader(parcel);
            final Map.Entry entry2 = (Map.Entry)sparseArray.get(SafeParcelReader.getFieldId(header));
            if (entry2 != null) {
                if (n != 0) {
                    sb.append(",");
                }
                final String str = (String)entry2.getKey();
                final FastJsonResponse.Field field = (FastJsonResponse.Field)entry2.getValue();
                sb.append("\"");
                sb.append(str);
                sb.append("\":");
                Label_0504: {
                    if (field.zaj()) {
                        final int zac = field.zac;
                        Object o = null;
                        Label_0497: {
                            Serializable s = null;
                            switch (zac) {
                                default: {
                                    final StringBuilder sb2 = new StringBuilder(36);
                                    sb2.append("Unknown field out type = ");
                                    sb2.append(zac);
                                    throw new IllegalArgumentException(sb2.toString());
                                }
                                case 11: {
                                    throw new IllegalArgumentException("Method does not accept concrete type.");
                                }
                                case 10: {
                                    final Bundle bundle = SafeParcelReader.createBundle(parcel, header);
                                    final HashMap<String, String> hashMap = new HashMap<String, String>();
                                    for (final String key : ((BaseBundle)bundle).keySet()) {
                                        hashMap.put(key, Preconditions.checkNotNull(((BaseBundle)bundle).getString(key)));
                                    }
                                    o = FastJsonResponse.zaD((FastJsonResponse.Field<Object, Object>)field, hashMap);
                                    break Label_0497;
                                }
                                case 8:
                                case 9: {
                                    o = FastJsonResponse.zaD((FastJsonResponse.Field<Object, Object>)field, SafeParcelReader.createByteArray(parcel, header));
                                    break Label_0497;
                                }
                                case 7: {
                                    s = SafeParcelReader.createString(parcel, header);
                                    break;
                                }
                                case 6: {
                                    s = SafeParcelReader.readBoolean(parcel, header);
                                    break;
                                }
                                case 5: {
                                    s = SafeParcelReader.createBigDecimal(parcel, header);
                                    break;
                                }
                                case 4: {
                                    s = SafeParcelReader.readDouble(parcel, header);
                                    break;
                                }
                                case 3: {
                                    s = SafeParcelReader.readFloat(parcel, header);
                                    break;
                                }
                                case 2: {
                                    s = SafeParcelReader.readLong(parcel, header);
                                    break;
                                }
                                case 1: {
                                    s = SafeParcelReader.createBigInteger(parcel, header);
                                    break;
                                }
                                case 0: {
                                    s = SafeParcelReader.readInt(parcel, header);
                                    break;
                                }
                            }
                            o = FastJsonResponse.zaD((FastJsonResponse.Field<Object, Object>)field, s);
                        }
                        zaJ(sb, field, o);
                    }
                    else {
                        String str3 = null;
                        Label_0781: {
                            if (!field.zad) {
                                Number obj = null;
                                Label_1175: {
                                    String str4 = null;
                                    switch (field.zac) {
                                        default: {
                                            throw new IllegalStateException("Unknown field type out");
                                        }
                                        case 11: {
                                            final Parcel parcel2 = SafeParcelReader.createParcel(parcel, header);
                                            parcel2.setDataPosition(0);
                                            this.zaH(sb, field.zah(), parcel2);
                                            break Label_0504;
                                        }
                                        case 10: {
                                            final Bundle bundle2 = SafeParcelReader.createBundle(parcel, header);
                                            final Set keySet = ((BaseBundle)bundle2).keySet();
                                            sb.append("{");
                                            final Iterator iterator3 = keySet.iterator();
                                            int n2 = 1;
                                            while (iterator3.hasNext()) {
                                                final String str2 = (String)iterator3.next();
                                                if (n2 == 0) {
                                                    sb.append(",");
                                                }
                                                sb.append("\"");
                                                sb.append(str2);
                                                sb.append("\":\"");
                                                sb.append(JsonUtils.escapeString(((BaseBundle)bundle2).getString(str2)));
                                                sb.append("\"");
                                                n2 = 0;
                                            }
                                            str3 = "}";
                                            break Label_0781;
                                        }
                                        case 9: {
                                            final byte[] byteArray = SafeParcelReader.createByteArray(parcel, header);
                                            sb.append("\"");
                                            str4 = Base64Utils.encodeUrlSafe(byteArray);
                                            break;
                                        }
                                        case 8: {
                                            final byte[] byteArray2 = SafeParcelReader.createByteArray(parcel, header);
                                            sb.append("\"");
                                            str4 = Base64Utils.encode(byteArray2);
                                            break;
                                        }
                                        case 7: {
                                            final String string = SafeParcelReader.createString(parcel, header);
                                            sb.append("\"");
                                            str4 = JsonUtils.escapeString(string);
                                            break;
                                        }
                                        case 6: {
                                            sb.append(SafeParcelReader.readBoolean(parcel, header));
                                            break Label_0504;
                                        }
                                        case 5: {
                                            obj = SafeParcelReader.createBigDecimal(parcel, header);
                                            break Label_1175;
                                        }
                                        case 4: {
                                            sb.append(SafeParcelReader.readDouble(parcel, header));
                                            break Label_0504;
                                        }
                                        case 3: {
                                            sb.append(SafeParcelReader.readFloat(parcel, header));
                                            break Label_0504;
                                        }
                                        case 2: {
                                            sb.append(SafeParcelReader.readLong(parcel, header));
                                            break Label_0504;
                                        }
                                        case 1: {
                                            obj = SafeParcelReader.createBigInteger(parcel, header);
                                            break Label_1175;
                                        }
                                        case 0: {
                                            sb.append(SafeParcelReader.readInt(parcel, header));
                                            break Label_0504;
                                        }
                                    }
                                    sb.append(str4);
                                    sb.append("\"");
                                    break Label_0504;
                                }
                                sb.append(obj);
                                break Label_0504;
                            }
                            sb.append("[");
                            switch (field.zac) {
                                default: {
                                    throw new IllegalStateException("Unknown field type out.");
                                }
                                case 11: {
                                    final Parcel[] parcelArray = SafeParcelReader.createParcelArray(parcel, header);
                                    for (int length = parcelArray.length, i = 0; i < length; ++i) {
                                        if (i > 0) {
                                            sb.append(",");
                                        }
                                        parcelArray[i].setDataPosition(0);
                                        this.zaH(sb, field.zah(), parcelArray[i]);
                                    }
                                    break;
                                }
                                case 8:
                                case 9:
                                case 10: {
                                    throw new UnsupportedOperationException("List of type BASE64, BASE64_URL_SAFE, or STRING_MAP is not supported");
                                }
                                case 7: {
                                    ArrayUtils.writeStringArray(sb, SafeParcelReader.createStringArray(parcel, header));
                                    break;
                                }
                                case 6: {
                                    ArrayUtils.writeArray(sb, SafeParcelReader.createBooleanArray(parcel, header));
                                    break;
                                }
                                case 5: {
                                    ArrayUtils.writeArray(sb, SafeParcelReader.createBigDecimalArray(parcel, header));
                                    break;
                                }
                                case 4: {
                                    ArrayUtils.writeArray(sb, SafeParcelReader.createDoubleArray(parcel, header));
                                    break;
                                }
                                case 3: {
                                    ArrayUtils.writeArray(sb, SafeParcelReader.createFloatArray(parcel, header));
                                    break;
                                }
                                case 2: {
                                    ArrayUtils.writeArray(sb, SafeParcelReader.createLongArray(parcel, header));
                                    break;
                                }
                                case 1: {
                                    ArrayUtils.writeArray(sb, SafeParcelReader.createBigIntegerArray(parcel, header));
                                    break;
                                }
                                case 0: {
                                    ArrayUtils.writeArray(sb, SafeParcelReader.createIntArray(parcel, header));
                                    break;
                                }
                            }
                            str3 = "]";
                        }
                        sb.append(str3);
                    }
                }
                n = 1;
            }
        }
        if (parcel.dataPosition() == validateObjectHeader) {
            sb.append('}');
            return;
        }
        final StringBuilder sb3 = new StringBuilder(37);
        sb3.append("Overread allowed size end=");
        sb3.append(validateObjectHeader);
        throw new SafeParcelReader.ParseException(sb3.toString(), parcel);
    }
    
    private static final void zaI(StringBuilder sb, final int i, final Object obj) {
        switch (i) {
            default: {
                sb = new StringBuilder(26);
                sb.append("Unknown type = ");
                sb.append(i);
                throw new IllegalArgumentException(sb.toString());
            }
            case 11: {
                throw new IllegalArgumentException("Method does not accept concrete type.");
            }
            case 10: {
                MapUtils.writeStringMapToJson(sb, Preconditions.checkNotNull(obj));
                return;
            }
            case 9: {
                sb.append("\"");
                sb.append(Base64Utils.encodeUrlSafe((byte[])obj));
                sb.append("\"");
                return;
            }
            case 8: {
                sb.append("\"");
                sb.append(Base64Utils.encode((byte[])obj));
                sb.append("\"");
                return;
            }
            case 7: {
                sb.append("\"");
                sb.append(JsonUtils.escapeString(Preconditions.checkNotNull(obj).toString()));
                sb.append("\"");
                return;
            }
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6: {
                sb.append(obj);
            }
        }
    }
    
    private static final void zaJ(final StringBuilder sb, final FastJsonResponse.Field<?, ?> field, final Object o) {
        if (field.zab) {
            final ArrayList list = (ArrayList)o;
            sb.append("[");
            for (int size = list.size(), i = 0; i < size; ++i) {
                if (i != 0) {
                    sb.append(",");
                }
                zaI(sb, field.zaa, list.get(i));
            }
            sb.append("]");
            return;
        }
        zaI(sb, field.zaa, o);
    }
    
    @Override
    public final <T extends FastJsonResponse> void addConcreteTypeArrayInternal(final FastJsonResponse.Field field, final String s, final ArrayList<T> list) {
        this.zaG(field);
        final ArrayList list2 = new ArrayList();
        Preconditions.checkNotNull(list).size();
        for (int size = list.size(), i = 0; i < size; ++i) {
            list2.add(((SafeParcelResponse)list.get(i)).zaE());
        }
        SafeParcelWriter.writeParcelList(this.zab, field.getSafeParcelableFieldId(), list2, true);
    }
    
    @Override
    public final <T extends FastJsonResponse> void addConcreteTypeInternal(final FastJsonResponse.Field field, final String s, final T t) {
        this.zaG(field);
        SafeParcelWriter.writeParcel(this.zab, field.getSafeParcelableFieldId(), ((SafeParcelResponse)t).zaE(), true);
    }
    
    @Override
    public final Map<String, FastJsonResponse.Field<?, ?>> getFieldMappings() {
        final zan zad = this.zad;
        if (zad == null) {
            return null;
        }
        return zad.zab(Preconditions.checkNotNull(this.zae));
    }
    
    @Override
    public final Object getValueObject(final String s) {
        throw new UnsupportedOperationException("Converting to JSON does not require this method.");
    }
    
    @Override
    public final boolean isPrimitiveFieldSet(final String s) {
        throw new UnsupportedOperationException("Converting to JSON does not require this method.");
    }
    
    @Override
    public final void setBooleanInternal(final FastJsonResponse.Field<?, ?> field, final String s, final boolean b) {
        this.zaG(field);
        SafeParcelWriter.writeBoolean(this.zab, field.getSafeParcelableFieldId(), b);
    }
    
    @Override
    public final void setDecodedBytesInternal(final FastJsonResponse.Field<?, ?> field, final String s, final byte[] array) {
        this.zaG(field);
        SafeParcelWriter.writeByteArray(this.zab, field.getSafeParcelableFieldId(), array, true);
    }
    
    @Override
    public final void setIntegerInternal(final FastJsonResponse.Field<?, ?> field, final String s, final int n) {
        this.zaG(field);
        SafeParcelWriter.writeInt(this.zab, field.getSafeParcelableFieldId(), n);
    }
    
    @Override
    public final void setLongInternal(final FastJsonResponse.Field<?, ?> field, final String s, final long n) {
        this.zaG(field);
        SafeParcelWriter.writeLong(this.zab, field.getSafeParcelableFieldId(), n);
    }
    
    @Override
    public final void setStringInternal(final FastJsonResponse.Field<?, ?> field, final String s, final String s2) {
        this.zaG(field);
        SafeParcelWriter.writeString(this.zab, field.getSafeParcelableFieldId(), s2, true);
    }
    
    @Override
    public final void setStringMapInternal(final FastJsonResponse.Field<?, ?> field, final String s, final Map<String, String> map) {
        this.zaG(field);
        final Bundle bundle = new Bundle();
        for (final String s2 : Preconditions.checkNotNull(map).keySet()) {
            ((BaseBundle)bundle).putString(s2, (String)map.get(s2));
        }
        SafeParcelWriter.writeBundle(this.zab, field.getSafeParcelableFieldId(), bundle, true);
    }
    
    @Override
    public final void setStringsInternal(final FastJsonResponse.Field<?, ?> field, final String s, final ArrayList<String> list) {
        this.zaG(field);
        final int size = Preconditions.checkNotNull(list).size();
        final String[] array = new String[size];
        for (int i = 0; i < size; ++i) {
            array[i] = list.get(i);
        }
        SafeParcelWriter.writeStringArray(this.zab, field.getSafeParcelableFieldId(), array, true);
    }
    
    @Override
    public final String toString() {
        Preconditions.checkNotNull(this.zad, "Cannot convert to JSON on client side.");
        final Parcel zaE = this.zaE();
        zaE.setDataPosition(0);
        final StringBuilder sb = new StringBuilder(100);
        this.zaH(sb, Preconditions.checkNotNull(this.zad.zab(Preconditions.checkNotNull(this.zae))), zaE);
        return sb.toString();
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.zaa);
        SafeParcelWriter.writeParcel(parcel, 2, this.zaE(), false);
        Object zad;
        if (this.zac != 0) {
            zad = this.zad;
        }
        else {
            zad = null;
        }
        SafeParcelWriter.writeParcelable(parcel, 3, (Parcelable)zad, n, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public final Parcel zaE() {
        final int zaf = this.zaf;
        if (zaf != 0) {
            if (zaf != 1) {
                return this.zab;
            }
            SafeParcelWriter.finishObjectHeader(this.zab, this.zag);
        }
        else {
            final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(this.zab);
            this.zag = beginObjectHeader;
            SafeParcelWriter.finishObjectHeader(this.zab, beginObjectHeader);
        }
        this.zaf = 2;
        return this.zab;
    }
    
    @Override
    public final void zab(final FastJsonResponse.Field<?, ?> field, final String s, final BigDecimal bigDecimal) {
        this.zaG(field);
        SafeParcelWriter.writeBigDecimal(this.zab, field.getSafeParcelableFieldId(), bigDecimal, true);
    }
    
    @Override
    public final void zad(final FastJsonResponse.Field<?, ?> field, final String s, final ArrayList<BigDecimal> list) {
        this.zaG(field);
        final int size = Preconditions.checkNotNull(list).size();
        final BigDecimal[] array = new BigDecimal[size];
        for (int i = 0; i < size; ++i) {
            array[i] = list.get(i);
        }
        SafeParcelWriter.writeBigDecimalArray(this.zab, field.getSafeParcelableFieldId(), array, true);
    }
    
    @Override
    public final void zaf(final FastJsonResponse.Field<?, ?> field, final String s, final BigInteger bigInteger) {
        this.zaG(field);
        SafeParcelWriter.writeBigInteger(this.zab, field.getSafeParcelableFieldId(), bigInteger, true);
    }
    
    @Override
    public final void zah(final FastJsonResponse.Field<?, ?> field, final String s, final ArrayList<BigInteger> list) {
        this.zaG(field);
        final int size = Preconditions.checkNotNull(list).size();
        final BigInteger[] array = new BigInteger[size];
        for (int i = 0; i < size; ++i) {
            array[i] = list.get(i);
        }
        SafeParcelWriter.writeBigIntegerArray(this.zab, field.getSafeParcelableFieldId(), array, true);
    }
    
    @Override
    public final void zak(final FastJsonResponse.Field<?, ?> field, final String s, final ArrayList<Boolean> list) {
        this.zaG(field);
        final int size = Preconditions.checkNotNull(list).size();
        final boolean[] array = new boolean[size];
        for (int i = 0; i < size; ++i) {
            array[i] = list.get(i);
        }
        SafeParcelWriter.writeBooleanArray(this.zab, field.getSafeParcelableFieldId(), array, true);
    }
    
    @Override
    public final void zan(final FastJsonResponse.Field<?, ?> field, final String s, final double n) {
        this.zaG(field);
        SafeParcelWriter.writeDouble(this.zab, field.getSafeParcelableFieldId(), n);
    }
    
    @Override
    public final void zap(final FastJsonResponse.Field<?, ?> field, final String s, final ArrayList<Double> list) {
        this.zaG(field);
        final int size = Preconditions.checkNotNull(list).size();
        final double[] array = new double[size];
        for (int i = 0; i < size; ++i) {
            array[i] = list.get(i);
        }
        SafeParcelWriter.writeDoubleArray(this.zab, field.getSafeParcelableFieldId(), array, true);
    }
    
    @Override
    public final void zar(final FastJsonResponse.Field<?, ?> field, final String s, final float n) {
        this.zaG(field);
        SafeParcelWriter.writeFloat(this.zab, field.getSafeParcelableFieldId(), n);
    }
    
    @Override
    public final void zat(final FastJsonResponse.Field<?, ?> field, final String s, final ArrayList<Float> list) {
        this.zaG(field);
        final int size = Preconditions.checkNotNull(list).size();
        final float[] array = new float[size];
        for (int i = 0; i < size; ++i) {
            array[i] = list.get(i);
        }
        SafeParcelWriter.writeFloatArray(this.zab, field.getSafeParcelableFieldId(), array, true);
    }
    
    @Override
    public final void zaw(final FastJsonResponse.Field<?, ?> field, final String s, final ArrayList<Integer> list) {
        this.zaG(field);
        final int size = Preconditions.checkNotNull(list).size();
        final int[] array = new int[size];
        for (int i = 0; i < size; ++i) {
            array[i] = list.get(i);
        }
        SafeParcelWriter.writeIntArray(this.zab, field.getSafeParcelableFieldId(), array, true);
    }
    
    @Override
    public final void zaz(final FastJsonResponse.Field<?, ?> field, final String s, final ArrayList<Long> list) {
        this.zaG(field);
        final int size = Preconditions.checkNotNull(list).size();
        final long[] array = new long[size];
        for (int i = 0; i < size; ++i) {
            array[i] = list.get(i);
        }
        SafeParcelWriter.writeLongArray(this.zab, field.getSafeParcelableFieldId(), array, true);
    }
}

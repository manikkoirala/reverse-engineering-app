// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.server;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@KeepForSdk
@Class(creator = "FavaDiagnosticsEntityCreator")
public class FavaDiagnosticsEntity extends AbstractSafeParcelable implements ReflectedParcelable
{
    @KeepForSdk
    public static final Parcelable$Creator<FavaDiagnosticsEntity> CREATOR;
    @VersionField(id = 1)
    final int zaa;
    @Field(id = 2)
    public final String zab;
    @Field(id = 3)
    public final int zac;
    
    static {
        CREATOR = (Parcelable$Creator)new zaa();
    }
    
    @Constructor
    public FavaDiagnosticsEntity(@Param(id = 1) final int zaa, @Param(id = 2) final String zab, @Param(id = 3) final int zac) {
        this.zaa = zaa;
        this.zab = zab;
        this.zac = zac;
    }
    
    @KeepForSdk
    public FavaDiagnosticsEntity(final String zab, final int zac) {
        this.zaa = 1;
        this.zab = zab;
        this.zac = zac;
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.zaa);
        SafeParcelWriter.writeString(parcel, 2, this.zab, false);
        SafeParcelWriter.writeInt(parcel, 3, this.zac);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

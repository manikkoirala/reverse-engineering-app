// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.server.converter;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.server.response.FastJsonResponse;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "ConverterWrapperCreator")
public final class zaa extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zaa> CREATOR;
    @VersionField(id = 1)
    final int zaa;
    @Field(getter = "getStringToIntConverter", id = 2)
    private final StringToIntConverter zab;
    
    static {
        CREATOR = (Parcelable$Creator)new zab();
    }
    
    @Constructor
    public zaa(@Param(id = 1) final int zaa, @Param(id = 2) final StringToIntConverter zab) {
        this.zaa = zaa;
        this.zab = zab;
    }
    
    private zaa(final StringToIntConverter zab) {
        this.zaa = 1;
        this.zab = zab;
    }
    
    public static zaa zaa(final FastJsonResponse.FieldConverter<?, ?> fieldConverter) {
        if (fieldConverter instanceof StringToIntConverter) {
            return new zaa((StringToIntConverter)fieldConverter);
        }
        throw new IllegalArgumentException("Unsupported safe parcelable field converter class.");
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.zaa);
        SafeParcelWriter.writeParcelable(parcel, 2, (Parcelable)this.zab, n, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public final FastJsonResponse.FieldConverter<?, ?> zab() {
        final StringToIntConverter zab = this.zab;
        if (zab != null) {
            return zab;
        }
        throw new IllegalStateException("There was no converter wrapped in this ConverterWrapper.");
    }
}

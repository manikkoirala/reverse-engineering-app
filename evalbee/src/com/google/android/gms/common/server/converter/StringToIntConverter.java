// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.server.converter;

import java.util.Iterator;
import android.os.Parcelable;
import java.util.List;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import java.util.ArrayList;
import android.util.SparseArray;
import java.util.HashMap;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.server.response.FastJsonResponse;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@KeepForSdk
@Class(creator = "StringToIntConverterCreator")
public final class StringToIntConverter extends AbstractSafeParcelable implements FieldConverter<String, Integer>
{
    public static final Parcelable$Creator<StringToIntConverter> CREATOR;
    @VersionField(id = 1)
    final int zaa;
    private final HashMap<String, Integer> zab;
    private final SparseArray<String> zac;
    
    static {
        CREATOR = (Parcelable$Creator)new zad();
    }
    
    @KeepForSdk
    public StringToIntConverter() {
        this.zaa = 1;
        this.zab = new HashMap<String, Integer>();
        this.zac = (SparseArray<String>)new SparseArray();
    }
    
    @Constructor
    public StringToIntConverter(@Param(id = 1) int i, @Param(id = 2) final ArrayList<zac> list) {
        this.zaa = i;
        this.zab = new HashMap<String, Integer>();
        this.zac = (SparseArray<String>)new SparseArray();
        int size;
        zac zac;
        for (size = list.size(), i = 0; i < size; ++i) {
            zac = (zac)list.get(i);
            this.add(zac.zab, zac.zac);
        }
    }
    
    @KeepForSdk
    public StringToIntConverter add(final String key, final int i) {
        this.zab.put(key, i);
        this.zac.put(i, (Object)key);
        return this;
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.zaa);
        final ArrayList list = new ArrayList();
        for (final String key : this.zab.keySet()) {
            list.add(new zac(key, this.zab.get(key)));
        }
        SafeParcelWriter.writeTypedList(parcel, 2, (List<Parcelable>)list, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    @Override
    public final int zaa() {
        return 7;
    }
    
    @Override
    public final int zab() {
        return 0;
    }
}

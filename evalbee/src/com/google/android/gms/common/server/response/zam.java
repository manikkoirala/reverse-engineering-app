// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.server.response;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.ShowFirstParty;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@ShowFirstParty
@Class(creator = "FieldMapPairCreator")
public final class zam extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zam> CREATOR;
    @VersionField(id = 1)
    final int zaa;
    @Field(id = 2)
    final String zab;
    @Field(id = 3)
    final FastJsonResponse.Field<?, ?> zac;
    
    static {
        CREATOR = (Parcelable$Creator)new zak();
    }
    
    @Constructor
    public zam(@Param(id = 1) final int zaa, @Param(id = 2) final String zab, @Param(id = 3) final FastJsonResponse.Field<?, ?> zac) {
        this.zaa = zaa;
        this.zab = zab;
        this.zac = zac;
    }
    
    public zam(final String zab, final FastJsonResponse.Field<?, ?> zac) {
        this.zaa = 1;
        this.zab = zab;
        this.zac = zac;
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.zaa);
        SafeParcelWriter.writeString(parcel, 2, this.zab, false);
        SafeParcelWriter.writeParcelable(parcel, 3, (Parcelable)this.zac, n, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

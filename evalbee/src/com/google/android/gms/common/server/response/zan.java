// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.server.response;

import android.os.Parcelable;
import java.util.List;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import java.util.Iterator;
import com.google.android.gms.common.internal.Preconditions;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.ShowFirstParty;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@ShowFirstParty
@Class(creator = "FieldMappingDictionaryCreator")
public final class zan extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zan> CREATOR;
    @VersionField(id = 1)
    final int zaa;
    private final HashMap<String, Map<String, FastJsonResponse.Field<?, ?>>> zab;
    @Field(getter = "getRootClassName", id = 3)
    private final String zac;
    
    static {
        CREATOR = (Parcelable$Creator)new zao();
    }
    
    @Constructor
    public zan(@Param(id = 1) int i, @Param(id = 2) final ArrayList<zal> list, @Param(id = 3) final String s) {
        this.zaa = i;
        final HashMap zab = new HashMap();
        int size;
        zal zal;
        String zab2;
        HashMap<String, FastJsonResponse.Field<?, ?>> value;
        int size2;
        int j;
        zam zam;
        for (size = list.size(), i = 0; i < size; ++i) {
            zal = list.get(i);
            zab2 = zal.zab;
            value = new HashMap<String, FastJsonResponse.Field<?, ?>>();
            for (size2 = Preconditions.checkNotNull(zal.zac).size(), j = 0; j < size2; ++j) {
                zam = zal.zac.get(j);
                value.put(zam.zab, zam.zac);
            }
            zab.put(zab2, value);
        }
        this.zab = zab;
        this.zac = Preconditions.checkNotNull(s);
        this.zad();
    }
    
    public zan(final java.lang.Class<? extends FastJsonResponse> clazz) {
        this.zaa = 1;
        this.zab = new HashMap<String, Map<String, FastJsonResponse.Field<?, ?>>>();
        this.zac = Preconditions.checkNotNull(clazz.getCanonicalName());
    }
    
    @Override
    public final String toString() {
        final StringBuilder sb = new StringBuilder();
        for (final String s : this.zab.keySet()) {
            sb.append(s);
            sb.append(":\n");
            final Map map = this.zab.get(s);
            for (final String str : map.keySet()) {
                sb.append("  ");
                sb.append(str);
                sb.append(": ");
                sb.append(map.get(str));
            }
        }
        return sb.toString();
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.zaa);
        final ArrayList list = new ArrayList();
        for (final String key : this.zab.keySet()) {
            list.add(new zal(key, this.zab.get(key)));
        }
        SafeParcelWriter.writeTypedList(parcel, 2, (List<Parcelable>)list, false);
        SafeParcelWriter.writeString(parcel, 3, this.zac, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public final String zaa() {
        return this.zac;
    }
    
    public final Map<String, FastJsonResponse.Field<?, ?>> zab(final String key) {
        return this.zab.get(key);
    }
    
    public final void zac() {
        for (final String s : this.zab.keySet()) {
            final Map map = this.zab.get(s);
            final HashMap<String, FastJsonResponse.Field<?, ?>> value = new HashMap<String, FastJsonResponse.Field<?, ?>>();
            for (final String key : map.keySet()) {
                value.put(key, ((FastJsonResponse.Field)map.get(key)).zab());
            }
            this.zab.put(s, value);
        }
    }
    
    public final void zad() {
        final Iterator<String> iterator = this.zab.keySet().iterator();
        while (iterator.hasNext()) {
            final Map map = this.zab.get(iterator.next());
            final Iterator iterator2 = map.keySet().iterator();
            while (iterator2.hasNext()) {
                ((FastJsonResponse.Field)map.get(iterator2.next())).zai(this);
            }
        }
    }
    
    public final void zae(final java.lang.Class<? extends FastJsonResponse> clazz, final Map<String, FastJsonResponse.Field<?, ?>> value) {
        this.zab.put(Preconditions.checkNotNull(clazz.getCanonicalName()), value);
    }
    
    public final boolean zaf(final java.lang.Class<? extends FastJsonResponse> clazz) {
        return this.zab.containsKey(Preconditions.checkNotNull(clazz.getCanonicalName()));
    }
}

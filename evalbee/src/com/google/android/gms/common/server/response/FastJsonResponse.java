// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.server.response;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.server.converter.zaa;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import java.util.Iterator;
import com.google.android.gms.common.util.Base64Utils;
import com.google.android.gms.common.util.MapUtils;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import android.util.Log;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.JsonUtils;
import java.math.BigInteger;
import java.math.BigDecimal;
import com.google.android.gms.common.internal.ShowFirstParty;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
@ShowFirstParty
public abstract class FastJsonResponse
{
    public static final <O, I> I zaD(final Field<I, O> field, final Object o) {
        if (Field.zac((Field<Object, Object>)field) != null) {
            return field.zaf(o);
        }
        return (I)o;
    }
    
    private final <I, O> void zaE(final Field<I, O> field, final I n) {
        final String zae = field.zae;
        final O zae2 = field.zae(n);
        final int zac = field.zac;
        switch (zac) {
            default: {
                final StringBuilder sb = new StringBuilder(44);
                sb.append("Unsupported type for conversion: ");
                sb.append(zac);
                throw new IllegalStateException(sb.toString());
            }
            case 8:
            case 9: {
                if (zae2 != null) {
                    this.setDecodedBytesInternal(field, zae, (byte[])(Object)zae2);
                    return;
                }
                zaG(zae);
                return;
            }
            case 7: {
                this.setStringInternal(field, zae, (String)zae2);
                return;
            }
            case 6: {
                if (zae2 != null) {
                    this.setBooleanInternal(field, zae, (boolean)zae2);
                    return;
                }
                zaG(zae);
                return;
            }
            case 5: {
                this.zab(field, zae, (BigDecimal)zae2);
                return;
            }
            case 4: {
                if (zae2 != null) {
                    this.zan(field, zae, (double)zae2);
                    return;
                }
                zaG(zae);
                return;
            }
            case 2: {
                if (zae2 != null) {
                    this.setLongInternal(field, zae, (long)zae2);
                    return;
                }
                zaG(zae);
                return;
            }
            case 1: {
                this.zaf(field, zae, (BigInteger)zae2);
                return;
            }
            case 0: {
                if (zae2 != null) {
                    this.setIntegerInternal(field, zae, (int)zae2);
                    return;
                }
                zaG(zae);
            }
        }
    }
    
    private static final void zaF(final StringBuilder sb, final Field field, final Object o) {
        final int zaa = field.zaa;
        String string;
        if (zaa != 11) {
            if (zaa != 7) {
                sb.append(o);
                return;
            }
            string = "\"";
            sb.append("\"");
            sb.append(JsonUtils.escapeString((String)o));
        }
        else {
            final Class<? extends FastJsonResponse> zag = field.zag;
            Preconditions.checkNotNull(zag);
            string = ((FastJsonResponse)zag.cast(o)).toString();
        }
        sb.append(string);
    }
    
    private static final <O> void zaG(final String s) {
        if (Log.isLoggable("FastJsonResponse", 6)) {
            final StringBuilder sb = new StringBuilder(String.valueOf(s).length() + 58);
            sb.append("Output field (");
            sb.append(s);
            sb.append(") has a null value, but expected a primitive");
            Log.e("FastJsonResponse", sb.toString());
        }
    }
    
    @KeepForSdk
    public <T extends FastJsonResponse> void addConcreteTypeArrayInternal(final Field field, final String s, final ArrayList<T> list) {
        throw new UnsupportedOperationException("Concrete type array not supported");
    }
    
    @KeepForSdk
    public <T extends FastJsonResponse> void addConcreteTypeInternal(final Field field, final String s, final T t) {
        throw new UnsupportedOperationException("Concrete type not supported");
    }
    
    @KeepForSdk
    public abstract Map<String, Field<?, ?>> getFieldMappings();
    
    @KeepForSdk
    public Object getFieldValue(final Field field) {
        final String zae = field.zae;
        if (field.zag != null) {
            Preconditions.checkState(this.getValueObject(zae) == null, "Concrete field shouldn't be value object: %s", field.zae);
            try {
                final char upperCase = Character.toUpperCase(zae.charAt(0));
                final String substring = zae.substring(1);
                final StringBuilder sb = new StringBuilder(String.valueOf(substring).length() + 4);
                sb.append("get");
                sb.append(upperCase);
                sb.append(substring);
                return this.getClass().getMethod(sb.toString(), (Class<?>[])new Class[0]).invoke(this, new Object[0]);
            }
            catch (final Exception cause) {
                throw new RuntimeException(cause);
            }
        }
        return this.getValueObject(zae);
    }
    
    @KeepForSdk
    public abstract Object getValueObject(final String p0);
    
    @KeepForSdk
    public boolean isFieldSet(final Field field) {
        if (field.zac != 11) {
            return this.isPrimitiveFieldSet(field.zae);
        }
        if (field.zad) {
            throw new UnsupportedOperationException("Concrete type arrays not supported");
        }
        throw new UnsupportedOperationException("Concrete types not supported");
    }
    
    @KeepForSdk
    public abstract boolean isPrimitiveFieldSet(final String p0);
    
    @KeepForSdk
    public void setBooleanInternal(final Field<?, ?> field, final String s, final boolean b) {
        throw new UnsupportedOperationException("Boolean not supported");
    }
    
    @KeepForSdk
    public void setDecodedBytesInternal(final Field<?, ?> field, final String s, final byte[] array) {
        throw new UnsupportedOperationException("byte[] not supported");
    }
    
    @KeepForSdk
    public void setIntegerInternal(final Field<?, ?> field, final String s, final int n) {
        throw new UnsupportedOperationException("Integer not supported");
    }
    
    @KeepForSdk
    public void setLongInternal(final Field<?, ?> field, final String s, final long n) {
        throw new UnsupportedOperationException("Long not supported");
    }
    
    @KeepForSdk
    public void setStringInternal(final Field<?, ?> field, final String s, final String s2) {
        throw new UnsupportedOperationException("String not supported");
    }
    
    @KeepForSdk
    public void setStringMapInternal(final Field<?, ?> field, final String s, final Map<String, String> map) {
        throw new UnsupportedOperationException("String map not supported");
    }
    
    @KeepForSdk
    public void setStringsInternal(final Field<?, ?> field, final String s, final ArrayList<String> list) {
        throw new UnsupportedOperationException("String list not supported");
    }
    
    @KeepForSdk
    @Override
    public String toString() {
        final Map<String, Field<?, ?>> fieldMappings = this.getFieldMappings();
        final StringBuilder sb = new StringBuilder(100);
        for (final String str : fieldMappings.keySet()) {
            final Field field = fieldMappings.get(str);
            if (this.isFieldSet(field)) {
                final ArrayList zaD = zaD((Field<ArrayList, Object>)field, this.getFieldValue(field));
                if (sb.length() == 0) {
                    sb.append("{");
                }
                else {
                    sb.append(",");
                }
                sb.append("\"");
                sb.append(str);
                sb.append("\":");
                String str2 = null;
                Label_0148: {
                    if (zaD != null) {
                        String str3 = null;
                        switch (field.zac) {
                            default: {
                                if (field.zab) {
                                    final ArrayList list = zaD;
                                    sb.append("[");
                                    for (int size = list.size(), i = 0; i < size; ++i) {
                                        if (i > 0) {
                                            sb.append(",");
                                        }
                                        final Object value = list.get(i);
                                        if (value != null) {
                                            zaF(sb, field, value);
                                        }
                                    }
                                    str2 = "]";
                                    break Label_0148;
                                }
                                zaF(sb, field, zaD);
                                continue;
                            }
                            case 10: {
                                MapUtils.writeStringMapToJson(sb, (HashMap<String, String>)zaD);
                                continue;
                            }
                            case 9: {
                                sb.append("\"");
                                str3 = Base64Utils.encodeUrlSafe((byte[])(Object)zaD);
                                break;
                            }
                            case 8: {
                                sb.append("\"");
                                str3 = Base64Utils.encode((byte[])(Object)zaD);
                                break;
                            }
                        }
                        sb.append(str3);
                        sb.append("\"");
                        continue;
                    }
                    str2 = "null";
                }
                sb.append(str2);
            }
        }
        String str4;
        if (sb.length() > 0) {
            str4 = "}";
        }
        else {
            str4 = "{}";
        }
        sb.append(str4);
        return sb.toString();
    }
    
    public final <O> void zaA(final Field<String, O> field, final String s) {
        if (Field.zac((Field<Object, Object>)field) != null) {
            this.zaE((Field<String, Object>)field, s);
            return;
        }
        this.setStringInternal(field, field.zae, s);
    }
    
    public final <O> void zaB(final Field<Map<String, String>, O> field, final Map<String, String> map) {
        if (Field.zac((Field<Object, Object>)field) != null) {
            this.zaE((Field<Map<String, String>, Object>)field, map);
            return;
        }
        this.setStringMapInternal(field, field.zae, map);
    }
    
    public final <O> void zaC(final Field<ArrayList<String>, O> field, final ArrayList<String> list) {
        if (Field.zac((Field<Object, Object>)field) != null) {
            this.zaE((Field<ArrayList<String>, Object>)field, list);
            return;
        }
        this.setStringsInternal(field, field.zae, list);
    }
    
    public final <O> void zaa(final Field<BigDecimal, O> field, final BigDecimal bigDecimal) {
        if (Field.zac((Field<Object, Object>)field) != null) {
            this.zaE((Field<BigDecimal, Object>)field, bigDecimal);
            return;
        }
        this.zab(field, field.zae, bigDecimal);
    }
    
    public void zab(final Field<?, ?> field, final String s, final BigDecimal bigDecimal) {
        throw new UnsupportedOperationException("BigDecimal not supported");
    }
    
    public final <O> void zac(final Field<ArrayList<BigDecimal>, O> field, final ArrayList<BigDecimal> list) {
        if (Field.zac((Field<Object, Object>)field) != null) {
            this.zaE((Field<ArrayList<BigDecimal>, Object>)field, list);
            return;
        }
        this.zad(field, field.zae, list);
    }
    
    public void zad(final Field<?, ?> field, final String s, final ArrayList<BigDecimal> list) {
        throw new UnsupportedOperationException("BigDecimal list not supported");
    }
    
    public final <O> void zae(final Field<BigInteger, O> field, final BigInteger bigInteger) {
        if (Field.zac((Field<Object, Object>)field) != null) {
            this.zaE((Field<BigInteger, Object>)field, bigInteger);
            return;
        }
        this.zaf(field, field.zae, bigInteger);
    }
    
    public void zaf(final Field<?, ?> field, final String s, final BigInteger bigInteger) {
        throw new UnsupportedOperationException("BigInteger not supported");
    }
    
    public final <O> void zag(final Field<ArrayList<BigInteger>, O> field, final ArrayList<BigInteger> list) {
        if (Field.zac((Field<Object, Object>)field) != null) {
            this.zaE((Field<ArrayList<BigInteger>, Object>)field, list);
            return;
        }
        this.zah(field, field.zae, list);
    }
    
    public void zah(final Field<?, ?> field, final String s, final ArrayList<BigInteger> list) {
        throw new UnsupportedOperationException("BigInteger list not supported");
    }
    
    public final <O> void zai(final Field<Boolean, O> field, final boolean b) {
        if (Field.zac((Field<Object, Object>)field) != null) {
            this.zaE((Field<Boolean, Object>)field, b);
            return;
        }
        this.setBooleanInternal(field, field.zae, b);
    }
    
    public final <O> void zaj(final Field<ArrayList<Boolean>, O> field, final ArrayList<Boolean> list) {
        if (Field.zac((Field<Object, Object>)field) != null) {
            this.zaE((Field<ArrayList<Boolean>, Object>)field, list);
            return;
        }
        this.zak(field, field.zae, list);
    }
    
    public void zak(final Field<?, ?> field, final String s, final ArrayList<Boolean> list) {
        throw new UnsupportedOperationException("Boolean list not supported");
    }
    
    public final <O> void zal(final Field<byte[], O> field, final byte[] array) {
        if (Field.zac((Field<Object, Object>)field) != null) {
            this.zaE((Field<byte[], Object>)field, array);
            return;
        }
        this.setDecodedBytesInternal(field, field.zae, array);
    }
    
    public final <O> void zam(final Field<Double, O> field, final double d) {
        if (Field.zac((Field<Object, Object>)field) != null) {
            this.zaE((Field<Double, Object>)field, d);
            return;
        }
        this.zan(field, field.zae, d);
    }
    
    public void zan(final Field<?, ?> field, final String s, final double n) {
        throw new UnsupportedOperationException("Double not supported");
    }
    
    public final <O> void zao(final Field<ArrayList<Double>, O> field, final ArrayList<Double> list) {
        if (Field.zac((Field<Object, Object>)field) != null) {
            this.zaE((Field<ArrayList<Double>, Object>)field, list);
            return;
        }
        this.zap(field, field.zae, list);
    }
    
    public void zap(final Field<?, ?> field, final String s, final ArrayList<Double> list) {
        throw new UnsupportedOperationException("Double list not supported");
    }
    
    public final <O> void zaq(final Field<Float, O> field, final float f) {
        if (Field.zac((Field<Object, Object>)field) != null) {
            this.zaE((Field<Float, Object>)field, f);
            return;
        }
        this.zar(field, field.zae, f);
    }
    
    public void zar(final Field<?, ?> field, final String s, final float n) {
        throw new UnsupportedOperationException("Float not supported");
    }
    
    public final <O> void zas(final Field<ArrayList<Float>, O> field, final ArrayList<Float> list) {
        if (Field.zac((Field<Object, Object>)field) != null) {
            this.zaE((Field<ArrayList<Float>, Object>)field, list);
            return;
        }
        this.zat(field, field.zae, list);
    }
    
    public void zat(final Field<?, ?> field, final String s, final ArrayList<Float> list) {
        throw new UnsupportedOperationException("Float list not supported");
    }
    
    public final <O> void zau(final Field<Integer, O> field, final int i) {
        if (Field.zac((Field<Object, Object>)field) != null) {
            this.zaE((Field<Integer, Object>)field, i);
            return;
        }
        this.setIntegerInternal(field, field.zae, i);
    }
    
    public final <O> void zav(final Field<ArrayList<Integer>, O> field, final ArrayList<Integer> list) {
        if (Field.zac((Field<Object, Object>)field) != null) {
            this.zaE((Field<ArrayList<Integer>, Object>)field, list);
            return;
        }
        this.zaw(field, field.zae, list);
    }
    
    public void zaw(final Field<?, ?> field, final String s, final ArrayList<Integer> list) {
        throw new UnsupportedOperationException("Integer list not supported");
    }
    
    public final <O> void zax(final Field<Long, O> field, final long l) {
        if (Field.zac((Field<Object, Object>)field) != null) {
            this.zaE((Field<Long, Object>)field, l);
            return;
        }
        this.setLongInternal(field, field.zae, l);
    }
    
    public final <O> void zay(final Field<ArrayList<Long>, O> field, final ArrayList<Long> list) {
        if (Field.zac((Field<Object, Object>)field) != null) {
            this.zaE((Field<ArrayList<Long>, Object>)field, list);
            return;
        }
        this.zaz(field, field.zae, list);
    }
    
    public void zaz(final Field<?, ?> field, final String s, final ArrayList<Long> list) {
        throw new UnsupportedOperationException("Long list not supported");
    }
    
    @KeepForSdk
    @ShowFirstParty
    @Class(creator = "FieldCreator")
    @VisibleForTesting
    public static class Field<I, O> extends AbstractSafeParcelable
    {
        public static final zaj CREATOR;
        @SafeParcelable.Field(getter = "getTypeIn", id = 2)
        protected final int zaa;
        @SafeParcelable.Field(getter = "isTypeInArray", id = 3)
        protected final boolean zab;
        @SafeParcelable.Field(getter = "getTypeOut", id = 4)
        protected final int zac;
        @SafeParcelable.Field(getter = "isTypeOutArray", id = 5)
        protected final boolean zad;
        @SafeParcelable.Field(getter = "getOutputFieldName", id = 6)
        protected final String zae;
        @SafeParcelable.Field(getter = "getSafeParcelableFieldId", id = 7)
        protected final int zaf;
        protected final java.lang.Class<? extends FastJsonResponse> zag;
        @SafeParcelable.Field(getter = "getConcreteTypeName", id = 8)
        protected final String zah;
        @VersionField(getter = "getVersionCode", id = 1)
        private final int zai;
        private zan zaj;
        @SafeParcelable.Field(getter = "getWrappedConverter", id = 9, type = "com.google.android.gms.common.server.converter.ConverterWrapper")
        private FieldConverter<I, O> zak;
        
        static {
            CREATOR = new zaj();
        }
        
        @Constructor
        public Field(@Param(id = 1) final int zai, @Param(id = 2) final int zaa, @Param(id = 3) final boolean zab, @Param(id = 4) final int zac, @Param(id = 5) final boolean zad, @Param(id = 6) final String zae, @Param(id = 7) final int zaf, @Param(id = 8) final String zah, @Param(id = 9) final zaa zaa2) {
            this.zai = zai;
            this.zaa = zaa;
            this.zab = zab;
            this.zac = zac;
            this.zad = zad;
            this.zae = zae;
            this.zaf = zaf;
            if (zah == null) {
                this.zag = null;
                this.zah = null;
            }
            else {
                this.zag = SafeParcelResponse.class;
                this.zah = zah;
            }
            if (zaa2 == null) {
                this.zak = null;
                return;
            }
            this.zak = (FieldConverter<I, O>)zaa2.zab();
        }
        
        public Field(final int zaa, final boolean zab, final int zac, final boolean zad, String canonicalName, final int zaf, final java.lang.Class<? extends FastJsonResponse> zag, final FieldConverter<I, O> zak) {
            this.zai = 1;
            this.zaa = zaa;
            this.zab = zab;
            this.zac = zac;
            this.zad = zad;
            this.zae = canonicalName;
            this.zaf = zaf;
            this.zag = zag;
            if (zag == null) {
                canonicalName = null;
            }
            else {
                canonicalName = zag.getCanonicalName();
            }
            this.zah = canonicalName;
            this.zak = zak;
        }
        
        @KeepForSdk
        @VisibleForTesting
        public static Field<byte[], byte[]> forBase64(final String s, final int n) {
            return new Field<byte[], byte[]>(8, false, 8, false, s, n, null, null);
        }
        
        @KeepForSdk
        public static Field<Boolean, Boolean> forBoolean(final String s, final int n) {
            return new Field<Boolean, Boolean>(6, false, 6, false, s, n, null, null);
        }
        
        @KeepForSdk
        public static <T extends FastJsonResponse> Field<T, T> forConcreteType(final String s, final int n, final java.lang.Class<T> clazz) {
            return new Field<T, T>(11, false, 11, false, s, n, clazz, null);
        }
        
        @KeepForSdk
        public static <T extends FastJsonResponse> Field<ArrayList<T>, ArrayList<T>> forConcreteTypeArray(final String s, final int n, final java.lang.Class<T> clazz) {
            return new Field<ArrayList<T>, ArrayList<T>>(11, true, 11, true, s, n, clazz, null);
        }
        
        @KeepForSdk
        public static Field<Double, Double> forDouble(final String s, final int n) {
            return new Field<Double, Double>(4, false, 4, false, s, n, null, null);
        }
        
        @KeepForSdk
        public static Field<Float, Float> forFloat(final String s, final int n) {
            return new Field<Float, Float>(3, false, 3, false, s, n, null, null);
        }
        
        @KeepForSdk
        @VisibleForTesting
        public static Field<Integer, Integer> forInteger(final String s, final int n) {
            return new Field<Integer, Integer>(0, false, 0, false, s, n, null, null);
        }
        
        @KeepForSdk
        public static Field<Long, Long> forLong(final String s, final int n) {
            return new Field<Long, Long>(2, false, 2, false, s, n, null, null);
        }
        
        @KeepForSdk
        public static Field<String, String> forString(final String s, final int n) {
            return new Field<String, String>(7, false, 7, false, s, n, null, null);
        }
        
        @KeepForSdk
        public static Field<HashMap<String, String>, HashMap<String, String>> forStringMap(final String s, final int n) {
            return new Field<HashMap<String, String>, HashMap<String, String>>(10, false, 10, false, s, n, null, null);
        }
        
        @KeepForSdk
        public static Field<ArrayList<String>, ArrayList<String>> forStrings(final String s, final int n) {
            return new Field<ArrayList<String>, ArrayList<String>>(7, true, 7, true, s, n, null, null);
        }
        
        @KeepForSdk
        public static Field withConverter(final String s, final int n, final FieldConverter<?, ?> fieldConverter, final boolean b) {
            fieldConverter.zaa();
            fieldConverter.zab();
            return new Field(7, b, 0, false, s, n, null, (FieldConverter<I, O>)fieldConverter);
        }
        
        @KeepForSdk
        public int getSafeParcelableFieldId() {
            return this.zaf;
        }
        
        @Override
        public final String toString() {
            final Objects.ToStringHelper add = Objects.toStringHelper(this).add("versionCode", this.zai).add("typeIn", this.zaa).add("typeInArray", this.zab).add("typeOut", this.zac).add("typeOutArray", this.zad).add("outputFieldName", this.zae).add("safeParcelFieldId", this.zaf).add("concreteTypeName", this.zag());
            final java.lang.Class<? extends FastJsonResponse> zag = this.zag;
            if (zag != null) {
                add.add("concreteType.class", zag.getCanonicalName());
            }
            final FieldConverter<I, O> zak = this.zak;
            if (zak != null) {
                add.add("converterName", zak.getClass().getCanonicalName());
            }
            return add.toString();
        }
        
        public final void writeToParcel(final Parcel parcel, final int n) {
            final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
            SafeParcelWriter.writeInt(parcel, 1, this.zai);
            SafeParcelWriter.writeInt(parcel, 2, this.zaa);
            SafeParcelWriter.writeBoolean(parcel, 3, this.zab);
            SafeParcelWriter.writeInt(parcel, 4, this.zac);
            SafeParcelWriter.writeBoolean(parcel, 5, this.zad);
            SafeParcelWriter.writeString(parcel, 6, this.zae, false);
            SafeParcelWriter.writeInt(parcel, 7, this.getSafeParcelableFieldId());
            SafeParcelWriter.writeString(parcel, 8, this.zag(), false);
            SafeParcelWriter.writeParcelable(parcel, 9, (Parcelable)this.zaa(), n, false);
            SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
        }
        
        public final zaa zaa() {
            final FieldConverter<I, O> zak = this.zak;
            if (zak == null) {
                return null;
            }
            return com.google.android.gms.common.server.converter.zaa.zaa(zak);
        }
        
        public final Field<I, O> zab() {
            return new Field<I, O>(this.zai, this.zaa, this.zab, this.zac, this.zad, this.zae, this.zaf, this.zah, this.zaa());
        }
        
        public final FastJsonResponse zad() {
            Preconditions.checkNotNull(this.zag);
            final java.lang.Class<? extends FastJsonResponse> zag = this.zag;
            if (zag == SafeParcelResponse.class) {
                Preconditions.checkNotNull(this.zah);
                Preconditions.checkNotNull(this.zaj, "The field mapping dictionary must be set if the concrete type is a SafeParcelResponse object.");
                return new SafeParcelResponse(this.zaj, this.zah);
            }
            return zag.newInstance();
        }
        
        public final O zae(final I n) {
            Preconditions.checkNotNull(this.zak);
            return Preconditions.checkNotNull(this.zak.zac(n));
        }
        
        public final I zaf(final O o) {
            Preconditions.checkNotNull(this.zak);
            return this.zak.zad(o);
        }
        
        public final String zag() {
            String zah;
            if ((zah = this.zah) == null) {
                zah = null;
            }
            return zah;
        }
        
        public final Map<String, Field<?, ?>> zah() {
            Preconditions.checkNotNull(this.zah);
            Preconditions.checkNotNull(this.zaj);
            return Preconditions.checkNotNull(this.zaj.zab(this.zah));
        }
        
        public final void zai(final zan zaj) {
            this.zaj = zaj;
        }
        
        public final boolean zaj() {
            return this.zak != null;
        }
    }
    
    @ShowFirstParty
    public interface FieldConverter<I, O>
    {
        int zaa();
        
        int zab();
        
        O zac(final I p0);
        
        I zad(final O p0);
    }
}

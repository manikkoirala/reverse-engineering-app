// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.server.response;

import java.util.List;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import java.util.Iterator;
import java.util.Map;
import java.util.ArrayList;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.ShowFirstParty;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@ShowFirstParty
@Class(creator = "FieldMappingDictionaryEntryCreator")
public final class zal extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zal> CREATOR;
    @VersionField(id = 1)
    final int zaa;
    @Field(id = 2)
    final String zab;
    @Field(id = 3)
    final ArrayList<zam> zac;
    
    static {
        CREATOR = (Parcelable$Creator)new zap();
    }
    
    @Constructor
    public zal(@Param(id = 1) final int zaa, @Param(id = 2) final String zab, @Param(id = 3) final ArrayList<zam> zac) {
        this.zaa = zaa;
        this.zab = zab;
        this.zac = zac;
    }
    
    public zal(String zab, final Map<String, FastJsonResponse.Field<?, ?>> map) {
        this.zaa = 1;
        this.zab = zab;
        ArrayList<zam> zac;
        if (map == null) {
            zac = null;
        }
        else {
            final ArrayList list = new ArrayList();
            final Iterator<String> iterator = map.keySet().iterator();
            while (true) {
                zac = list;
                if (!iterator.hasNext()) {
                    break;
                }
                zab = iterator.next();
                list.add(new zam(zab, map.get(zab)));
            }
        }
        this.zac = zac;
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.zaa);
        SafeParcelWriter.writeString(parcel, 2, this.zab, false);
        SafeParcelWriter.writeTypedList(parcel, 3, this.zac, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

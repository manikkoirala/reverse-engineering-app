// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.server.response;

import java.io.IOException;
import android.util.Log;
import java.io.Reader;
import java.io.InputStreamReader;
import java.io.InputStream;
import com.google.android.gms.common.util.Base64Utils;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import com.google.android.gms.common.util.JsonUtils;
import java.io.BufferedReader;
import java.util.Stack;
import java.math.BigDecimal;
import java.math.BigInteger;
import com.google.android.gms.common.internal.ShowFirstParty;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
@ShowFirstParty
public class FastParser<T extends FastJsonResponse>
{
    private static final char[] zaa;
    private static final char[] zab;
    private static final char[] zac;
    private static final char[] zad;
    private static final char[] zae;
    private static final char[] zaf;
    private static final zai<Integer> zag;
    private static final zai<Long> zah;
    private static final zai<Float> zai;
    private static final zai<Double> zaj;
    private static final zai<Boolean> zak;
    private static final zai<String> zal;
    private static final zai<BigInteger> zam;
    private static final zai<BigDecimal> zan;
    private final char[] zao;
    private final char[] zap;
    private final char[] zaq;
    private final StringBuilder zar;
    private final StringBuilder zas;
    private final Stack<Integer> zat;
    
    static {
        zaa = new char[] { 'u', 'l', 'l' };
        zab = new char[] { 'r', 'u', 'e' };
        zac = new char[] { 'r', 'u', 'e', '\"' };
        zad = new char[] { 'a', 'l', 's', 'e' };
        zae = new char[] { 'a', 'l', 's', 'e', '\"' };
        zaf = new char[] { '\n' };
        zag = new zaa();
        zah = new zab();
        zai = new zac();
        zaj = new zad();
        zak = new zae();
        zal = new zaf();
        zam = new zag();
        zan = new zah();
    }
    
    public FastParser() {
        this.zao = new char[1];
        this.zap = new char[32];
        this.zaq = new char[1024];
        this.zar = new StringBuilder(32);
        this.zas = new StringBuilder(1024);
        this.zat = new Stack<Integer>();
    }
    
    private static final String zaA(final BufferedReader bufferedReader, final char[] str, final StringBuilder sb, final char[] array) {
        sb.setLength(0);
        bufferedReader.mark(str.length);
        int n = 0;
        int n2 = 0;
        Block_8: {
        Label_0084:
            while (true) {
                final int read = bufferedReader.read(str);
                if (read == -1) {
                    throw new ParseException("Unexpected EOF while parsing string");
                }
                for (final char ch : str) {
                    Label_0094: {
                        if (Character.isISOControl(ch)) {
                            if (array != null) {
                                for (int j = 0; j <= 0; ++j) {
                                    if (array[j] == ch) {
                                        break Label_0094;
                                    }
                                }
                                break Label_0084;
                            }
                            break Label_0084;
                        }
                    }
                    Label_0170: {
                        if (ch == '\"') {
                            if (n2 == 0) {
                                break Block_8;
                            }
                        }
                        else if (ch == '\\') {
                            n2 ^= 0x1;
                            n = 1;
                            break Label_0170;
                        }
                        n2 = 0;
                    }
                }
                sb.append(str, 0, read);
                bufferedReader.mark(str.length);
            }
            throw new ParseException("Unexpected control character while reading string");
        }
        final int i;
        sb.append(str, 0, i);
        bufferedReader.reset();
        bufferedReader.skip(i + 1);
        String s = sb.toString();
        if (n != 0) {
            s = JsonUtils.unescapeString(s);
        }
        return s;
    }
    
    private final char zai(final BufferedReader bufferedReader) {
        if (bufferedReader.read(this.zao) != -1) {
            while (Character.isWhitespace(this.zao[0])) {
                if (bufferedReader.read(this.zao) == -1) {
                    return '\0';
                }
            }
            return this.zao[0];
        }
        return '\0';
    }
    
    private final double zaj(final BufferedReader bufferedReader) {
        final int zam = this.zam(bufferedReader, this.zaq);
        if (zam == 0) {
            return 0.0;
        }
        return Double.parseDouble(new String(this.zaq, 0, zam));
    }
    
    private final float zak(final BufferedReader bufferedReader) {
        final int zam = this.zam(bufferedReader, this.zaq);
        if (zam == 0) {
            return 0.0f;
        }
        return Float.parseFloat(new String(this.zaq, 0, zam));
    }
    
    private final int zal(final BufferedReader bufferedReader) {
        final int zam = this.zam(bufferedReader, this.zaq);
        if (zam == 0) {
            return 0;
        }
        final char[] zaq = this.zaq;
        if (zam > 0) {
            final char c = zaq[0];
            int n;
            if (c == '-') {
                n = Integer.MIN_VALUE;
            }
            else {
                n = -2147483647;
            }
            int n2;
            if (c == '-') {
                n2 = 1;
            }
            else {
                n2 = 0;
            }
            int i;
            int n3;
            if (n2 < zam) {
                i = n2 + 1;
                final int digit = Character.digit(zaq[n2], 10);
                if (digit < 0) {
                    throw new ParseException("Unexpected non-digit character");
                }
                n3 = -digit;
            }
            else {
                n3 = 0;
                i = n2;
            }
            while (i < zam) {
                final int digit2 = Character.digit(zaq[i], 10);
                if (digit2 < 0) {
                    throw new ParseException("Unexpected non-digit character");
                }
                if (n3 < -214748364) {
                    throw new ParseException("Number too large");
                }
                final int n4 = n3 * 10;
                if (n4 < n + digit2) {
                    throw new ParseException("Number too large");
                }
                n3 = n4 - digit2;
                ++i;
            }
            if (n2 != 0) {
                if (i <= 1) {
                    throw new ParseException("No digits to parse");
                }
            }
            else {
                n3 = -n3;
            }
            return n3;
        }
        throw new ParseException("No number to parse");
    }
    
    private final int zam(final BufferedReader bufferedReader, final char[] array) {
        final char zai = this.zai(bufferedReader);
        if (zai == '\0') {
            throw new ParseException("Unexpected EOF");
        }
        if (zai == ',') {
            throw new ParseException("Missing value");
        }
        if (zai == 'n') {
            this.zax(bufferedReader, FastParser.zaa);
            return 0;
        }
        bufferedReader.mark(1024);
        int n2;
        if (zai == '\"') {
            int off = 0;
            int n = 0;
            while (true) {
                n2 = off;
                if (off >= 1024) {
                    break;
                }
                n2 = off;
                if (bufferedReader.read(array, off, 1) == -1) {
                    break;
                }
                final char ch = array[off];
                if (Character.isISOControl(ch)) {
                    throw new ParseException("Unexpected control character while reading string");
                }
                int n3 = 0;
                Label_0140: {
                    if (ch == '\"') {
                        if (n == 0) {
                            bufferedReader.reset();
                            bufferedReader.skip(off + 1);
                            return off;
                        }
                    }
                    else if (ch == '\\') {
                        n3 = (n ^ 0x1);
                        break Label_0140;
                    }
                    n3 = 0;
                }
                ++off;
                n = n3;
            }
        }
        else {
            array[0] = zai;
            int off2 = 1;
            while (true) {
                n2 = off2;
                if (off2 >= 1024) {
                    break;
                }
                n2 = off2;
                if (bufferedReader.read(array, off2, 1) == -1) {
                    break;
                }
                final char ch2 = array[off2];
                if (ch2 == '}' || ch2 == ',' || Character.isWhitespace(ch2) || array[off2] == ']') {
                    bufferedReader.reset();
                    bufferedReader.skip(off2 - 1);
                    array[off2] = '\0';
                    return off2;
                }
                ++off2;
            }
        }
        if (n2 == 1024) {
            throw new ParseException("Absurdly long value");
        }
        throw new ParseException("Unexpected EOF");
    }
    
    private final long zan(final BufferedReader bufferedReader) {
        final int zam = this.zam(bufferedReader, this.zaq);
        if (zam == 0) {
            return 0L;
        }
        final char[] zaq = this.zaq;
        if (zam > 0) {
            int n = 0;
            final char c = zaq[0];
            long n2;
            if (c == '-') {
                n2 = Long.MIN_VALUE;
            }
            else {
                n2 = -9223372036854775807L;
            }
            if (c == '-') {
                n = 1;
            }
            int i;
            long n3;
            if (n < zam) {
                i = n + 1;
                final int digit = Character.digit(zaq[n], 10);
                if (digit < 0) {
                    throw new ParseException("Unexpected non-digit character");
                }
                n3 = -digit;
            }
            else {
                n3 = 0L;
                i = n;
            }
            while (i < zam) {
                final int digit2 = Character.digit(zaq[i], 10);
                if (digit2 < 0) {
                    throw new ParseException("Unexpected non-digit character");
                }
                if (n3 < -922337203685477580L) {
                    throw new ParseException("Number too large");
                }
                final long n4 = n3 * 10L;
                final long n5 = digit2;
                if (n4 < n2 + n5) {
                    throw new ParseException("Number too large");
                }
                n3 = n4 - n5;
                ++i;
            }
            if (n != 0) {
                if (i <= 1) {
                    throw new ParseException("No digits to parse");
                }
            }
            else {
                n3 = -n3;
            }
            return n3;
        }
        throw new ParseException("No number to parse");
    }
    
    private final String zao(final BufferedReader bufferedReader) {
        return this.zap(bufferedReader, this.zap, this.zar, null);
    }
    
    private final String zap(final BufferedReader bufferedReader, final char[] array, final StringBuilder sb, final char[] array2) {
        final char zai = this.zai(bufferedReader);
        if (zai == '\"') {
            return zaA(bufferedReader, array, sb, array2);
        }
        if (zai == 'n') {
            this.zax(bufferedReader, FastParser.zaa);
            return null;
        }
        throw new ParseException("Expected string");
    }
    
    private final String zaq(final BufferedReader bufferedReader) {
        this.zat.push(2);
        final char zai = this.zai(bufferedReader);
        if (zai != '\"') {
            if (zai == ']') {
                this.zaw(2);
                this.zaw(1);
                this.zaw(5);
                return null;
            }
            if (zai == '}') {
                this.zaw(2);
                return null;
            }
            final StringBuilder sb = new StringBuilder(19);
            sb.append("Unexpected token: ");
            sb.append(zai);
            throw new ParseException(sb.toString());
        }
        else {
            this.zat.push(3);
            final String zaA = zaA(bufferedReader, this.zap, this.zar, null);
            this.zaw(3);
            if (this.zai(bufferedReader) == ':') {
                return zaA;
            }
            throw new ParseException("Expected key/value separator");
        }
    }
    
    private final String zar(final BufferedReader bufferedReader) {
        bufferedReader.mark(1024);
        final char zai = this.zai(bufferedReader);
        int i = 1;
        Label_0433: {
            if (zai != '\"') {
                if (zai == ',') {
                    throw new ParseException("Missing value");
                }
                if (zai != '[') {
                    if (zai != '{') {
                        bufferedReader.reset();
                        this.zam(bufferedReader, this.zaq);
                    }
                    else {
                        this.zat.push(1);
                        bufferedReader.mark(32);
                        final char zai2 = this.zai(bufferedReader);
                        if (zai2 != '}') {
                            if (zai2 != '\"') {
                                final StringBuilder sb = new StringBuilder(18);
                                sb.append("Unexpected token ");
                                sb.append(zai2);
                                throw new ParseException(sb.toString());
                            }
                            bufferedReader.reset();
                            this.zaq(bufferedReader);
                            while (this.zar(bufferedReader) != null) {}
                        }
                        this.zaw(1);
                    }
                }
                else {
                    this.zat.push(5);
                    bufferedReader.mark(32);
                    if (this.zai(bufferedReader) != ']') {
                        bufferedReader.reset();
                        int n = 0;
                        int n2 = 0;
                        while (i > 0) {
                            final char zai3 = this.zai(bufferedReader);
                            if (zai3 == '\0') {
                                throw new ParseException("Unexpected EOF while parsing array");
                            }
                            if (Character.isISOControl(zai3)) {
                                throw new ParseException("Unexpected control character while reading array");
                            }
                            int n3 = n;
                            int n4;
                            if ((n4 = zai3) == 34) {
                                n3 = n;
                                if (n2 == 0) {
                                    n3 = (n ^ 0x1);
                                }
                                n4 = 34;
                            }
                            int n5 = i;
                            int n6;
                            if ((n6 = n4) == 91) {
                                n5 = i;
                                if (n3 == 0) {
                                    n5 = i + 1;
                                }
                                n6 = 91;
                            }
                            i = n5;
                            if (n6 == 93) {
                                i = n5;
                                if (n3 == 0) {
                                    i = n5 - 1;
                                }
                            }
                            if (n6 == 92 && n3 != 0) {
                                n2 ^= 0x1;
                                n = n3;
                            }
                            else {
                                n2 = 0;
                                n = n3;
                            }
                        }
                    }
                    this.zaw(5);
                }
            }
            else {
                if (bufferedReader.read(this.zao) != -1) {
                    char c = this.zao[0];
                    int n7 = 0;
                    while (true) {
                        int n8 = c;
                        int n9 = n7;
                        if (c == '\"') {
                            if (n7 == 0) {
                                break Label_0433;
                            }
                            n8 = 34;
                            n9 = 1;
                        }
                        if (n8 == 92) {
                            n7 = (n9 ^ 0x1);
                        }
                        else {
                            n7 = 0;
                        }
                        if (bufferedReader.read(this.zao) == -1) {
                            throw new ParseException("Unexpected EOF while parsing string");
                        }
                        final char ch = this.zao[0];
                        if (Character.isISOControl(ch)) {
                            break;
                        }
                        c = ch;
                    }
                    throw new ParseException("Unexpected control character while reading string");
                }
                throw new ParseException("Unexpected EOF while parsing string");
            }
        }
        final char zai4 = this.zai(bufferedReader);
        if (zai4 == ',') {
            this.zaw(2);
            return this.zaq(bufferedReader);
        }
        if (zai4 == '}') {
            this.zaw(2);
            return null;
        }
        final StringBuilder sb2 = new StringBuilder(18);
        sb2.append("Unexpected token ");
        sb2.append(zai4);
        throw new ParseException(sb2.toString());
    }
    
    private final BigDecimal zas(final BufferedReader bufferedReader) {
        final int zam = this.zam(bufferedReader, this.zaq);
        if (zam == 0) {
            return null;
        }
        return new BigDecimal(new String(this.zaq, 0, zam));
    }
    
    private final BigInteger zat(final BufferedReader bufferedReader) {
        final int zam = this.zam(bufferedReader, this.zaq);
        if (zam == 0) {
            return null;
        }
        return new BigInteger(new String(this.zaq, 0, zam));
    }
    
    private final <O> ArrayList<O> zau(final BufferedReader bufferedReader, final zai<O> zai) {
        final char zai2 = this.zai(bufferedReader);
        if (zai2 == 'n') {
            this.zax(bufferedReader, FastParser.zaa);
            return null;
        }
        if (zai2 != '[') {
            throw new ParseException("Expected start of array");
        }
        this.zat.push(5);
        final ArrayList list = new ArrayList();
        while (true) {
            bufferedReader.mark(1024);
            final char zai3 = this.zai(bufferedReader);
            if (zai3 == '\0') {
                throw new ParseException("Unexpected EOF");
            }
            if (zai3 == ',') {
                continue;
            }
            if (zai3 == ']') {
                this.zaw(5);
                return list;
            }
            bufferedReader.reset();
            list.add(zai.zaa(this, bufferedReader));
        }
    }
    
    private final <T extends FastJsonResponse> ArrayList<T> zav(final BufferedReader bufferedReader, final FastJsonResponse.Field<?, ?> field) {
        final ArrayList list = new ArrayList();
        final char zai = this.zai(bufferedReader);
        if (zai == ']') {
            this.zaw(5);
            return list;
        }
        if (zai != 'n') {
            if (zai == '{') {
                Stack<Integer> stack = this.zat;
                while (true) {
                    stack.push(1);
                    try {
                        final FastJsonResponse zad = field.zad();
                        if (!this.zaz(bufferedReader, zad)) {
                            return list;
                        }
                        list.add(zad);
                        final char zai2 = this.zai(bufferedReader);
                        if (zai2 != ',') {
                            if (zai2 == ']') {
                                this.zaw(5);
                                return list;
                            }
                            final StringBuilder sb = new StringBuilder(19);
                            sb.append("Unexpected token: ");
                            sb.append(zai2);
                            throw new ParseException(sb.toString());
                        }
                        else {
                            if (this.zai(bufferedReader) == '{') {
                                stack = this.zat;
                                continue;
                            }
                            throw new ParseException("Expected start of next object in array");
                        }
                    }
                    catch (final IllegalAccessException ex) {
                        throw new ParseException("Error instantiating inner object", ex);
                    }
                    catch (final InstantiationException ex2) {
                        throw new ParseException("Error instantiating inner object", ex2);
                    }
                    break;
                }
            }
            final StringBuilder sb2 = new StringBuilder(19);
            sb2.append("Unexpected token: ");
            sb2.append(zai);
            throw new ParseException(sb2.toString());
        }
        this.zax(bufferedReader, FastParser.zaa);
        this.zaw(5);
        return null;
    }
    
    private final void zaw(final int n) {
        if (this.zat.isEmpty()) {
            final StringBuilder sb = new StringBuilder(46);
            sb.append("Expected state ");
            sb.append(n);
            sb.append(" but had empty stack");
            throw new ParseException(sb.toString());
        }
        final int intValue = this.zat.pop();
        if (intValue == n) {
            return;
        }
        final StringBuilder sb2 = new StringBuilder(46);
        sb2.append("Expected state ");
        sb2.append(n);
        sb2.append(" but had ");
        sb2.append(intValue);
        throw new ParseException(sb2.toString());
    }
    
    private final void zax(final BufferedReader bufferedReader, final char[] array) {
        int n = 0;
        while (true) {
            final int length = array.length;
            if (n >= length) {
                return;
            }
            final int read = bufferedReader.read(this.zap, 0, length - n);
            if (read == -1) {
                throw new ParseException("Unexpected EOF");
            }
            for (int i = 0; i < read; ++i) {
                if (array[i + n] != this.zap[i]) {
                    throw new ParseException("Unexpected character");
                }
            }
            n += read;
        }
    }
    
    private final boolean zay(final BufferedReader bufferedReader, final boolean b) {
        final char zai = this.zai(bufferedReader);
        if (zai != '\"') {
            if (zai == 'f') {
                char[] array;
                if (b) {
                    array = FastParser.zae;
                }
                else {
                    array = FastParser.zad;
                }
                this.zax(bufferedReader, array);
                return false;
            }
            if (zai == 'n') {
                this.zax(bufferedReader, FastParser.zaa);
                return false;
            }
            if (zai == 't') {
                char[] array2;
                if (b) {
                    array2 = FastParser.zac;
                }
                else {
                    array2 = FastParser.zab;
                }
                this.zax(bufferedReader, array2);
                return true;
            }
            final StringBuilder sb = new StringBuilder(19);
            sb.append("Unexpected token: ");
            sb.append(zai);
            throw new ParseException(sb.toString());
        }
        else {
            if (!b) {
                return this.zay(bufferedReader, true);
            }
            throw new ParseException("No boolean value found in string");
        }
    }
    
    private final boolean zaz(final BufferedReader bufferedReader, final FastJsonResponse fastJsonResponse) {
        final Map<String, FastJsonResponse.Field<?, ?>> fieldMappings = fastJsonResponse.getFieldMappings();
        String s = this.zaq(bufferedReader);
        final Integer value = 1;
        if (s != null) {
        Label_0478_Outer:
            while (s != null) {
                final FastJsonResponse.Field<Map<String, String>, Object> field = fieldMappings.get(s);
                if (field == null) {
                    s = this.zar(bufferedReader);
                }
                else {
                    this.zat.push(4);
                    final int zaa = field.zaa;
                    switch (zaa) {
                        default: {
                            final StringBuilder sb = new StringBuilder(30);
                            sb.append("Invalid field type ");
                            sb.append(zaa);
                            throw new ParseException(sb.toString());
                        }
                        case 11: {
                            if (field.zab) {
                                final char zai = this.zai(bufferedReader);
                                if (zai == 'n') {
                                    this.zax(bufferedReader, FastParser.zaa);
                                    fastJsonResponse.addConcreteTypeArrayInternal(field, field.zae, (ArrayList<FastJsonResponse>)null);
                                }
                                else {
                                    this.zat.push(5);
                                    if (zai != '[') {
                                        throw new ParseException("Expected array start");
                                    }
                                    fastJsonResponse.addConcreteTypeArrayInternal(field, field.zae, this.zav(bufferedReader, (FastJsonResponse.Field<?, ?>)field));
                                }
                            }
                            else {
                                final char zai2 = this.zai(bufferedReader);
                                if (zai2 != 'n') {
                                    this.zat.push(value);
                                    if (zai2 == '{') {
                                        try {
                                            final FastJsonResponse zad = field.zad();
                                            this.zaz(bufferedReader, zad);
                                            fastJsonResponse.addConcreteTypeInternal(field, field.zae, zad);
                                            break;
                                        }
                                        catch (final IllegalAccessException ex) {
                                            throw new ParseException("Error instantiating inner object", ex);
                                        }
                                        catch (final InstantiationException ex2) {
                                            throw new ParseException("Error instantiating inner object", ex2);
                                        }
                                    }
                                    throw new ParseException("Expected start of object");
                                }
                                this.zax(bufferedReader, FastParser.zaa);
                                fastJsonResponse.addConcreteTypeInternal(field, field.zae, (FastJsonResponse)null);
                            }
                            break;
                        }
                        case 10: {
                            final char zai3 = this.zai(bufferedReader);
                            HashMap<String, String> hashMap = null;
                            Label_0651: {
                                if (zai3 == 'n') {
                                    this.zax(bufferedReader, FastParser.zaa);
                                    hashMap = null;
                                }
                                else {
                                    if (zai3 == '{') {
                                        this.zat.push(value);
                                        hashMap = new HashMap<String, String>();
                                        String zaA = null;
                                        Block_15: {
                                            Block_13: {
                                                char zai5 = '\0';
                                                while (true) {
                                                    Block_17: {
                                                        while (true) {
                                                            final char zai4 = this.zai(bufferedReader);
                                                            if (zai4 == '\0') {
                                                                throw new ParseException("Unexpected EOF");
                                                            }
                                                            if (zai4 != '\"') {
                                                                if (zai4 != '}') {
                                                                    continue Label_0478_Outer;
                                                                }
                                                                break;
                                                            }
                                                            else {
                                                                zaA = zaA(bufferedReader, this.zap, this.zar, null);
                                                                if (this.zai(bufferedReader) != ':') {
                                                                    break Block_13;
                                                                }
                                                                if (this.zai(bufferedReader) != '\"') {
                                                                    break Block_15;
                                                                }
                                                                hashMap.put(zaA, zaA(bufferedReader, this.zap, this.zar, null));
                                                                zai5 = this.zai(bufferedReader);
                                                                if (zai5 != ',') {
                                                                    break Block_17;
                                                                }
                                                                continue Label_0478_Outer;
                                                            }
                                                        }
                                                        this.zaw(1);
                                                        break Label_0651;
                                                    }
                                                    if (zai5 == '}') {
                                                        continue;
                                                    }
                                                    break;
                                                }
                                                final StringBuilder sb2 = new StringBuilder(48);
                                                sb2.append("Unexpected character while parsing string map: ");
                                                sb2.append(zai5);
                                                throw new ParseException(sb2.toString());
                                            }
                                            final String value2 = String.valueOf(zaA);
                                            String concat;
                                            if (value2.length() != 0) {
                                                concat = "No map value found for key ".concat(value2);
                                            }
                                            else {
                                                concat = new String("No map value found for key ");
                                            }
                                            throw new ParseException(concat);
                                        }
                                        final String value3 = String.valueOf(zaA);
                                        String concat2;
                                        if (value3.length() != 0) {
                                            concat2 = "Expected String value for key ".concat(value3);
                                        }
                                        else {
                                            concat2 = new String("Expected String value for key ");
                                        }
                                        throw new ParseException(concat2);
                                    }
                                    throw new ParseException("Expected start of a map object");
                                }
                            }
                            fastJsonResponse.zaB(field, hashMap);
                            break;
                        }
                        case 9: {
                            fastJsonResponse.zal((FastJsonResponse.Field<byte[], Object>)field, Base64Utils.decodeUrlSafe(this.zap(bufferedReader, this.zaq, this.zas, FastParser.zaf)));
                            break;
                        }
                        case 8: {
                            fastJsonResponse.zal((FastJsonResponse.Field<byte[], Object>)field, Base64Utils.decode(this.zap(bufferedReader, this.zaq, this.zas, FastParser.zaf)));
                            break;
                        }
                        case 7: {
                            if (field.zab) {
                                fastJsonResponse.zaC((FastJsonResponse.Field<ArrayList<String>, Object>)field, this.zau(bufferedReader, FastParser.zal));
                                break;
                            }
                            fastJsonResponse.zaA((FastJsonResponse.Field<String, Object>)field, this.zao(bufferedReader));
                            break;
                        }
                        case 6: {
                            if (field.zab) {
                                fastJsonResponse.zaj((FastJsonResponse.Field<ArrayList<Boolean>, Object>)field, this.zau(bufferedReader, FastParser.zak));
                                break;
                            }
                            fastJsonResponse.zai((FastJsonResponse.Field<Boolean, Object>)field, this.zay(bufferedReader, false));
                            break;
                        }
                        case 5: {
                            if (field.zab) {
                                fastJsonResponse.zac((FastJsonResponse.Field<ArrayList<BigDecimal>, Object>)field, this.zau(bufferedReader, FastParser.zan));
                                break;
                            }
                            fastJsonResponse.zaa((FastJsonResponse.Field<BigDecimal, Object>)field, this.zas(bufferedReader));
                            break;
                        }
                        case 4: {
                            if (field.zab) {
                                fastJsonResponse.zao((FastJsonResponse.Field<ArrayList<Double>, Object>)field, this.zau(bufferedReader, FastParser.zaj));
                                break;
                            }
                            fastJsonResponse.zam((FastJsonResponse.Field<Double, Object>)field, this.zaj(bufferedReader));
                            break;
                        }
                        case 3: {
                            if (field.zab) {
                                fastJsonResponse.zas((FastJsonResponse.Field<ArrayList<Float>, Object>)field, this.zau(bufferedReader, FastParser.zai));
                                break;
                            }
                            fastJsonResponse.zaq((FastJsonResponse.Field<Float, Object>)field, this.zak(bufferedReader));
                            break;
                        }
                        case 2: {
                            if (field.zab) {
                                fastJsonResponse.zay((FastJsonResponse.Field<ArrayList<Long>, Object>)field, this.zau(bufferedReader, FastParser.zah));
                                break;
                            }
                            fastJsonResponse.zax((FastJsonResponse.Field<Long, Object>)field, this.zan(bufferedReader));
                            break;
                        }
                        case 1: {
                            if (field.zab) {
                                fastJsonResponse.zag((FastJsonResponse.Field<ArrayList<BigInteger>, Object>)field, this.zau(bufferedReader, FastParser.zam));
                                break;
                            }
                            fastJsonResponse.zae((FastJsonResponse.Field<BigInteger, Object>)field, this.zat(bufferedReader));
                            break;
                        }
                        case 0: {
                            if (field.zab) {
                                fastJsonResponse.zav((FastJsonResponse.Field<ArrayList<Integer>, Object>)field, this.zau(bufferedReader, FastParser.zag));
                                break;
                            }
                            fastJsonResponse.zau((FastJsonResponse.Field<Integer, Object>)field, this.zal(bufferedReader));
                            break;
                        }
                    }
                    this.zaw(4);
                    this.zaw(2);
                    final char zai6 = this.zai(bufferedReader);
                    if (zai6 != ',') {
                        if (zai6 != '}') {
                            final StringBuilder sb3 = new StringBuilder(55);
                            sb3.append("Expected end of object or field separator, but found: ");
                            sb3.append(zai6);
                            throw new ParseException(sb3.toString());
                        }
                        s = null;
                    }
                    else {
                        s = this.zaq(bufferedReader);
                    }
                }
            }
            this.zaw(1);
            return true;
        }
        this.zaw(1);
        return false;
    }
    
    @KeepForSdk
    public void parse(InputStream in, final T t) {
        in = (IOException)new BufferedReader(new InputStreamReader((InputStream)in), 1024);
        try {
            this.zat.push(0);
            final char zai = this.zai((BufferedReader)in);
            if (zai != '\0') {
                if (zai != '[') {
                    if (zai != '{') {
                        final StringBuilder sb = new StringBuilder(19);
                        sb.append("Unexpected token: ");
                        sb.append(zai);
                        throw new ParseException(sb.toString());
                    }
                    this.zat.push(1);
                    this.zaz((BufferedReader)in, t);
                }
                else {
                    this.zat.push(5);
                    final Map<String, FastJsonResponse.Field<?, ?>> fieldMappings = t.getFieldMappings();
                    if (fieldMappings.size() == 0) {
                        throw new ParseException("Object array response class must have a single Field");
                    }
                    final FastJsonResponse.Field field = ((Map.Entry<K, FastJsonResponse.Field>)fieldMappings.entrySet().iterator().next()).getValue();
                    t.addConcreteTypeArrayInternal(field, ((FastJsonResponse.Field)field).zae, this.zav((BufferedReader)in, field));
                }
                this.zaw(0);
                try {
                    ((BufferedReader)in).close();
                    return;
                }
                catch (final IOException in) {
                    Log.w("FastParser", "Failed to close reader while parsing.");
                    return;
                }
                throw new ParseException("Object array response class must have a single Field");
            }
            throw new ParseException("No data to parse");
        }
        catch (final IOException ex) {}
        finally {
            try {
                ((BufferedReader)in).close();
            }
            catch (final IOException ex2) {
                Log.w("FastParser", "Failed to close reader while parsing.");
            }
        }
    }
    
    @KeepForSdk
    @ShowFirstParty
    public static class ParseException extends Exception
    {
        public ParseException(final String message) {
            super(message);
        }
        
        public ParseException(final String s, final Throwable cause) {
            super("Error instantiating inner object", cause);
        }
        
        public ParseException(final Throwable cause) {
            super(cause);
        }
    }
}

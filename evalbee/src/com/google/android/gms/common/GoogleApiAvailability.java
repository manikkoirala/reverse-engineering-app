// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common;

import android.os.Handler;
import com.google.android.gms.internal.base.zal;
import com.google.android.gms.common.api.GoogleApiActivity;
import com.google.android.gms.common.wrappers.InstantApps;
import com.google.android.gms.common.api.internal.LifecycleFragment;
import android.app.Notification;
import android.app.NotificationChannel;
import android.content.res.Resources;
import com.google.android.gms.base.R;
import com.google.android.gms.common.util.DeviceProperties;
import android.app.FragmentManager;
import androidx.fragment.app.k;
import androidx.fragment.app.e;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import com.google.android.gms.common.api.internal.zabx;
import com.google.android.gms.common.api.internal.zabw;
import android.app.AlertDialog;
import android.view.View;
import android.util.AttributeSet;
import android.widget.ProgressBar;
import android.util.Log;
import android.content.DialogInterface$OnClickListener;
import com.google.android.gms.common.internal.zac;
import android.app.AlertDialog$Builder;
import android.util.TypedValue;
import android.app.NotificationManager;
import com.google.android.gms.common.util.PlatformVersion;
import com.google.android.gms.common.api.internal.zacc;
import com.google.android.gms.tasks.Tasks;
import com.google.android.gms.common.internal.HideFirstParty;
import android.app.PendingIntent;
import android.content.Intent;
import androidx.fragment.app.Fragment;
import com.google.android.gms.common.internal.zag;
import android.content.DialogInterface$OnCancelListener;
import android.app.Dialog;
import android.app.Activity;
import com.google.android.gms.common.internal.ShowFirstParty;
import com.google.android.gms.common.annotation.KeepForSdk;
import android.content.Context;
import com.google.android.gms.tasks.SuccessContinuation;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.internal.GoogleApiManager;
import java.util.Collection;
import java.util.Arrays;
import java.util.ArrayList;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.api.internal.ApiKey;
import java.util.Map;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.common.api.HasApiKey;

public class GoogleApiAvailability extends GoogleApiAvailabilityLight
{
    public static final String GOOGLE_PLAY_SERVICES_PACKAGE = "com.google.android.gms";
    public static final int GOOGLE_PLAY_SERVICES_VERSION_CODE;
    private static final Object zaa;
    private static final GoogleApiAvailability zab;
    private String zac;
    
    static {
        zaa = new Object();
        zab = new GoogleApiAvailability();
        GOOGLE_PLAY_SERVICES_VERSION_CODE = GoogleApiAvailabilityLight.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }
    
    public static GoogleApiAvailability getInstance() {
        return GoogleApiAvailability.zab;
    }
    
    public static final Task<Map<ApiKey<?>, String>> zai(final HasApiKey<?> hasApiKey, final HasApiKey<?>... a) {
        Preconditions.checkNotNull(hasApiKey, "Requested API must not be null.");
        for (int length = a.length, i = 0; i < length; ++i) {
            Preconditions.checkNotNull(a[i], "Requested API must not be null.");
        }
        final ArrayList list = new ArrayList(a.length + 1);
        list.add(hasApiKey);
        list.addAll(Arrays.asList(a));
        return GoogleApiManager.zal().zao(list);
    }
    
    public Task<Void> checkApiAvailability(final GoogleApi<?> googleApi, final GoogleApi<?>... array) {
        return (Task<Void>)zai(googleApi, (HasApiKey<?>[])array).onSuccessTask((SuccessContinuation)com.google.android.gms.common.zab.zaa);
    }
    
    public Task<Void> checkApiAvailability(final HasApiKey<?> hasApiKey, final HasApiKey<?>... array) {
        return (Task<Void>)zai(hasApiKey, array).onSuccessTask((SuccessContinuation)com.google.android.gms.common.zaa.zaa);
    }
    
    @KeepForSdk
    @ShowFirstParty
    @Override
    public int getClientVersion(final Context context) {
        return super.getClientVersion(context);
    }
    
    public Dialog getErrorDialog(final Activity activity, final int n, final int n2) {
        return this.getErrorDialog(activity, n, n2, null);
    }
    
    public Dialog getErrorDialog(final Activity activity, final int n, final int n2, final DialogInterface$OnCancelListener dialogInterface$OnCancelListener) {
        return this.zaa((Context)activity, n, zag.zab(activity, this.getErrorResolutionIntent((Context)activity, n, "d"), n2), dialogInterface$OnCancelListener);
    }
    
    public Dialog getErrorDialog(final Fragment fragment, final int n, final int n2) {
        return this.getErrorDialog(fragment, n, n2, null);
    }
    
    public Dialog getErrorDialog(final Fragment fragment, final int n, final int n2, final DialogInterface$OnCancelListener dialogInterface$OnCancelListener) {
        return this.zaa(fragment.requireContext(), n, zag.zac(fragment, this.getErrorResolutionIntent(fragment.requireContext(), n, "d"), n2), dialogInterface$OnCancelListener);
    }
    
    @KeepForSdk
    @ShowFirstParty
    @Override
    public Intent getErrorResolutionIntent(final Context context, final int n, final String s) {
        return super.getErrorResolutionIntent(context, n, s);
    }
    
    @Override
    public PendingIntent getErrorResolutionPendingIntent(final Context context, final int n, final int n2) {
        return super.getErrorResolutionPendingIntent(context, n, n2);
    }
    
    public PendingIntent getErrorResolutionPendingIntent(final Context context, final ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            return connectionResult.getResolution();
        }
        return this.getErrorResolutionPendingIntent(context, connectionResult.getErrorCode(), 0);
    }
    
    @Override
    public final String getErrorString(final int n) {
        return super.getErrorString(n);
    }
    
    @HideFirstParty
    @Override
    public int isGooglePlayServicesAvailable(final Context context) {
        return super.isGooglePlayServicesAvailable(context);
    }
    
    @KeepForSdk
    @ShowFirstParty
    @Override
    public int isGooglePlayServicesAvailable(final Context context, final int n) {
        return super.isGooglePlayServicesAvailable(context, n);
    }
    
    @Override
    public final boolean isUserResolvableError(final int n) {
        return super.isUserResolvableError(n);
    }
    
    public Task<Void> makeGooglePlayServicesAvailable(final Activity activity) {
        final int google_PLAY_SERVICES_VERSION_CODE = GoogleApiAvailability.GOOGLE_PLAY_SERVICES_VERSION_CODE;
        Preconditions.checkMainThread("makeGooglePlayServicesAvailable must be called from the main thread");
        final int googlePlayServicesAvailable = this.isGooglePlayServicesAvailable((Context)activity, google_PLAY_SERVICES_VERSION_CODE);
        Task task;
        if (googlePlayServicesAvailable == 0) {
            task = Tasks.forResult((Object)null);
        }
        else {
            final zacc zaa = zacc.zaa(activity);
            zaa.zah(new ConnectionResult(googlePlayServicesAvailable, null), 0);
            task = zaa.zad();
        }
        return (Task<Void>)task;
    }
    
    public void setDefaultNotificationChannelId(final Context context, final String zac) {
        if (PlatformVersion.isAtLeastO()) {
            Preconditions.checkNotNull(pj2.a((NotificationManager)Preconditions.checkNotNull(context.getSystemService("notification")), zac));
        }
        synchronized (GoogleApiAvailability.zaa) {
            this.zac = zac;
        }
    }
    
    public boolean showErrorDialogFragment(final Activity activity, final int n, final int n2) {
        return this.showErrorDialogFragment(activity, n, n2, null);
    }
    
    public boolean showErrorDialogFragment(final Activity activity, final int n, final int n2, final DialogInterface$OnCancelListener dialogInterface$OnCancelListener) {
        final Dialog errorDialog = this.getErrorDialog(activity, n, n2, dialogInterface$OnCancelListener);
        if (errorDialog == null) {
            return false;
        }
        this.zad(activity, errorDialog, "GooglePlayServicesErrorDialog", dialogInterface$OnCancelListener);
        return true;
    }
    
    public void showErrorNotification(final Context context, final int n) {
        this.zae(context, n, null, this.getErrorResolutionPendingIntent(context, n, 0, "n"));
    }
    
    public void showErrorNotification(final Context context, final ConnectionResult connectionResult) {
        this.zae(context, connectionResult.getErrorCode(), null, this.getErrorResolutionPendingIntent(context, connectionResult));
    }
    
    public final Dialog zaa(final Context context, final int i, final zag zag, final DialogInterface$OnCancelListener onCancelListener) {
        AlertDialog$Builder alertDialog$Builder = null;
        if (i == 0) {
            return null;
        }
        final TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(16843529, typedValue, true);
        if ("Theme.Dialog.Alert".equals(context.getResources().getResourceEntryName(typedValue.resourceId))) {
            alertDialog$Builder = new AlertDialog$Builder(context, 5);
        }
        AlertDialog$Builder alertDialog$Builder2;
        if ((alertDialog$Builder2 = alertDialog$Builder) == null) {
            alertDialog$Builder2 = new AlertDialog$Builder(context);
        }
        alertDialog$Builder2.setMessage((CharSequence)com.google.android.gms.common.internal.zac.zad(context, i));
        if (onCancelListener != null) {
            alertDialog$Builder2.setOnCancelListener(onCancelListener);
        }
        final String zac = com.google.android.gms.common.internal.zac.zac(context, i);
        if (zac != null) {
            alertDialog$Builder2.setPositiveButton((CharSequence)zac, (DialogInterface$OnClickListener)zag);
        }
        final String zag2 = com.google.android.gms.common.internal.zac.zag(context, i);
        if (zag2 != null) {
            alertDialog$Builder2.setTitle((CharSequence)zag2);
        }
        Log.w("GoogleApiAvailability", String.format("Creating dialog for Google Play services availability issue. ConnectionResult=%s", i), (Throwable)new IllegalArgumentException());
        return (Dialog)alertDialog$Builder2.create();
    }
    
    public final Dialog zab(final Activity activity, final DialogInterface$OnCancelListener dialogInterface$OnCancelListener) {
        final ProgressBar view = new ProgressBar((Context)activity, (AttributeSet)null, 16842874);
        view.setIndeterminate(true);
        ((View)view).setVisibility(0);
        final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder((Context)activity);
        alertDialog$Builder.setView((View)view);
        alertDialog$Builder.setMessage((CharSequence)com.google.android.gms.common.internal.zac.zad((Context)activity, 18));
        alertDialog$Builder.setPositiveButton((CharSequence)"", (DialogInterface$OnClickListener)null);
        final AlertDialog create = alertDialog$Builder.create();
        this.zad(activity, (Dialog)create, "GooglePlayServicesUpdatingDialog", dialogInterface$OnCancelListener);
        return (Dialog)create;
    }
    
    public final zabx zac(final Context context, final zabw zabw) {
        final IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
        intentFilter.addDataScheme("package");
        final zabx zabx = new zabx(zabw);
        context.registerReceiver((BroadcastReceiver)zabx, intentFilter);
        zabx.zaa(context);
        if (!this.isUninstalledAppPossiblyUpdating(context, "com.google.android.gms")) {
            zabw.zaa();
            zabx.zab();
            return null;
        }
        return zabx;
    }
    
    public final void zad(Activity activity, final Dialog dialog, final String s, final DialogInterface$OnCancelListener dialogInterface$OnCancelListener) {
        while (true) {
            try {
                if (activity instanceof e) {
                    activity = (Activity)((e)activity).getSupportFragmentManager();
                    SupportErrorDialogFragment.newInstance(dialog, dialogInterface$OnCancelListener).show((k)activity, s);
                    return;
                }
                activity = (Activity)activity.getFragmentManager();
                ErrorDialogFragment.newInstance(dialog, dialogInterface$OnCancelListener).show((FragmentManager)activity, s);
            }
            catch (final NoClassDefFoundError noClassDefFoundError) {
                continue;
            }
            break;
        }
    }
    
    public final void zae(final Context context, int i, String s, final PendingIntent pendingIntent) {
        Log.w("GoogleApiAvailability", String.format("GMS core API Availability. ConnectionResult=%s, tag=%s", i, null), (Throwable)new IllegalArgumentException());
        if (i == 18) {
            this.zaf(context);
            return;
        }
        if (pendingIntent == null) {
            if (i == 6) {
                Log.w("GoogleApiAvailability", "Missing resolution for ConnectionResult.RESOLUTION_REQUIRED. Call GoogleApiAvailability#showErrorNotification(Context, ConnectionResult) instead.");
            }
            return;
        }
        final String zaf = com.google.android.gms.common.internal.zac.zaf(context, i);
        s = com.google.android.gms.common.internal.zac.zae(context, i);
        final Resources resources = context.getResources();
        final NotificationManager notificationManager = Preconditions.checkNotNull(context.getSystemService("notification"));
        final rz0.e x = new rz0.e(context).q(true).f(true).k(zaf).x(new rz0.c().h(s));
        if (DeviceProperties.isWearable(context)) {
            Preconditions.checkState(PlatformVersion.isAtLeastKitKatWatch());
            x.v(context.getApplicationInfo().icon).s(2);
            if (DeviceProperties.isWearableWithoutPlayStore(context)) {
                x.a(R.drawable.common_full_open_on_phone, resources.getString(R.string.common_open_on_phone), pendingIntent);
            }
            else {
                x.i(pendingIntent);
            }
        }
        else {
            x.v(17301642).y(resources.getString(R.string.common_google_play_services_notification_ticker)).B(System.currentTimeMillis()).i(pendingIntent).j(s);
        }
        Label_0363: {
            if (!PlatformVersion.isAtLeastO()) {
                break Label_0363;
            }
            Preconditions.checkState(PlatformVersion.isAtLeastO());
            s = (String)GoogleApiAvailability.zaa;
            synchronized (s) {
                final String zac = this.zac;
                monitorexit(s);
                s = zac;
                if (zac == null) {
                    final String s2 = "com.google.android.gms.availability";
                    final NotificationChannel a = pj2.a(notificationManager, "com.google.android.gms.availability");
                    final String zab = com.google.android.gms.common.internal.zac.zab(context);
                    if (a == null) {
                        fh.a(notificationManager, eh.a("com.google.android.gms.availability", zab, 4));
                        s = s2;
                    }
                    else {
                        s = s2;
                        if (!zab.contentEquals(jb0.a(a))) {
                            kb0.a(a, (CharSequence)zab);
                            fh.a(notificationManager, a);
                            s = s2;
                        }
                    }
                }
                x.g(s);
                final Notification b = x.b();
                if (i != 1 && i != 2 && i != 3) {
                    i = 39789;
                }
                else {
                    GooglePlayServicesUtilLight.sCanceledAvailabilityNotification.set(false);
                    i = 10436;
                }
                notificationManager.notify(i, b);
            }
        }
    }
    
    public final void zaf(final Context context) {
        ((Handler)new com.google.android.gms.common.zac(this, context)).sendEmptyMessageDelayed(1, 120000L);
    }
    
    public final boolean zag(final Activity activity, final LifecycleFragment lifecycleFragment, final int n, final int n2, final DialogInterface$OnCancelListener dialogInterface$OnCancelListener) {
        final Dialog zaa = this.zaa((Context)activity, n, zag.zad(lifecycleFragment, this.getErrorResolutionIntent((Context)activity, n, "d"), 2), dialogInterface$OnCancelListener);
        if (zaa == null) {
            return false;
        }
        this.zad(activity, zaa, "GooglePlayServicesErrorDialog", dialogInterface$OnCancelListener);
        return true;
    }
    
    public final boolean zah(final Context context, final ConnectionResult connectionResult, final int n) {
        if (InstantApps.isInstantApp(context)) {
            return false;
        }
        final PendingIntent errorResolutionPendingIntent = this.getErrorResolutionPendingIntent(context, connectionResult);
        if (errorResolutionPendingIntent != null) {
            this.zae(context, connectionResult.getErrorCode(), null, zal.zaa(context, 0, GoogleApiActivity.zaa(context, errorResolutionPendingIntent, n, true), zal.zaa | 0x8000000));
            return true;
        }
        return false;
    }
}

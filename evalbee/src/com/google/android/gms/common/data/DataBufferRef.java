// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.data;

import android.net.Uri;
import com.google.android.gms.common.internal.Objects;
import android.database.CharArrayBuffer;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public abstract class DataBufferRef
{
    @KeepForSdk
    protected final DataHolder mDataHolder;
    @KeepForSdk
    protected int mDataRow;
    private int zaa;
    
    @KeepForSdk
    public DataBufferRef(final DataHolder dataHolder, final int n) {
        this.mDataHolder = Preconditions.checkNotNull(dataHolder);
        this.zaa(n);
    }
    
    @KeepForSdk
    public void copyToBuffer(final String s, final CharArrayBuffer charArrayBuffer) {
        this.mDataHolder.zac(s, this.mDataRow, this.zaa, charArrayBuffer);
    }
    
    @KeepForSdk
    @Override
    public boolean equals(final Object o) {
        if (o instanceof DataBufferRef) {
            final DataBufferRef dataBufferRef = (DataBufferRef)o;
            if (Objects.equal(dataBufferRef.mDataRow, this.mDataRow) && Objects.equal(dataBufferRef.zaa, this.zaa) && dataBufferRef.mDataHolder == this.mDataHolder) {
                return true;
            }
        }
        return false;
    }
    
    @KeepForSdk
    public boolean getBoolean(final String s) {
        return this.mDataHolder.getBoolean(s, this.mDataRow, this.zaa);
    }
    
    @KeepForSdk
    public byte[] getByteArray(final String s) {
        return this.mDataHolder.getByteArray(s, this.mDataRow, this.zaa);
    }
    
    @KeepForSdk
    public int getDataRow() {
        return this.mDataRow;
    }
    
    @KeepForSdk
    public double getDouble(final String s) {
        return this.mDataHolder.zaa(s, this.mDataRow, this.zaa);
    }
    
    @KeepForSdk
    public float getFloat(final String s) {
        return this.mDataHolder.zab(s, this.mDataRow, this.zaa);
    }
    
    @KeepForSdk
    public int getInteger(final String s) {
        return this.mDataHolder.getInteger(s, this.mDataRow, this.zaa);
    }
    
    @KeepForSdk
    public long getLong(final String s) {
        return this.mDataHolder.getLong(s, this.mDataRow, this.zaa);
    }
    
    @KeepForSdk
    public String getString(final String s) {
        return this.mDataHolder.getString(s, this.mDataRow, this.zaa);
    }
    
    @KeepForSdk
    public boolean hasColumn(final String s) {
        return this.mDataHolder.hasColumn(s);
    }
    
    @KeepForSdk
    public boolean hasNull(final String s) {
        return this.mDataHolder.hasNull(s, this.mDataRow, this.zaa);
    }
    
    @KeepForSdk
    @Override
    public int hashCode() {
        return Objects.hashCode(this.mDataRow, this.zaa, this.mDataHolder);
    }
    
    @KeepForSdk
    public boolean isDataValid() {
        return !this.mDataHolder.isClosed();
    }
    
    @KeepForSdk
    public Uri parseUri(String string) {
        string = this.mDataHolder.getString(string, this.mDataRow, this.zaa);
        if (string == null) {
            return null;
        }
        return Uri.parse(string);
    }
    
    public final void zaa(final int mDataRow) {
        boolean b = false;
        if (mDataRow >= 0) {
            b = b;
            if (mDataRow < this.mDataHolder.getCount()) {
                b = true;
            }
        }
        Preconditions.checkState(b);
        this.mDataRow = mDataRow;
        this.zaa = this.mDataHolder.getWindowIndex(mDataRow);
    }
}

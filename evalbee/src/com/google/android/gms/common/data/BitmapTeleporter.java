// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.data;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import java.io.FileNotFoundException;
import java.io.DataOutputStream;
import java.io.OutputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import android.os.Parcel;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import android.graphics.Bitmap$Config;
import java.io.InputStream;
import java.io.DataInputStream;
import android.os.ParcelFileDescriptor$AutoCloseInputStream;
import com.google.android.gms.common.internal.Preconditions;
import java.io.IOException;
import android.util.Log;
import java.io.Closeable;
import java.io.File;
import android.graphics.Bitmap;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.ShowFirstParty;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@KeepForSdk
@ShowFirstParty
@Class(creator = "BitmapTeleporterCreator")
public class BitmapTeleporter extends AbstractSafeParcelable implements ReflectedParcelable
{
    @KeepForSdk
    public static final Parcelable$Creator<BitmapTeleporter> CREATOR;
    @VersionField(id = 1)
    final int zaa;
    @Field(id = 2)
    ParcelFileDescriptor zab;
    @Field(id = 3)
    final int zac;
    private Bitmap zad;
    private boolean zae;
    private File zaf;
    
    static {
        CREATOR = (Parcelable$Creator)new zaa();
    }
    
    @Constructor
    public BitmapTeleporter(@Param(id = 1) final int zaa, @Param(id = 2) final ParcelFileDescriptor zab, @Param(id = 3) final int zac) {
        this.zaa = zaa;
        this.zab = zab;
        this.zac = zac;
        this.zad = null;
        this.zae = false;
    }
    
    @KeepForSdk
    public BitmapTeleporter(final Bitmap zad) {
        this.zaa = 1;
        this.zab = null;
        this.zac = 0;
        this.zad = zad;
        this.zae = true;
    }
    
    private static final void zaa(final Closeable closeable) {
        try {
            closeable.close();
        }
        catch (final IOException ex) {
            Log.w("BitmapTeleporter", "Could not close stream", (Throwable)ex);
        }
    }
    
    @KeepForSdk
    public Bitmap get() {
        if (!this.zae) {
            Object wrap = new DataInputStream((InputStream)new ParcelFileDescriptor$AutoCloseInputStream((ParcelFileDescriptor)Preconditions.checkNotNull(this.zab)));
            try {
                try {
                    final byte[] array = new byte[((DataInputStream)wrap).readInt()];
                    final int int1 = ((DataInputStream)wrap).readInt();
                    final int int2 = ((DataInputStream)wrap).readInt();
                    final Bitmap$Config value = Bitmap$Config.valueOf(((DataInputStream)wrap).readUTF());
                    ((DataInputStream)wrap).read(array);
                    zaa((Closeable)wrap);
                    wrap = ByteBuffer.wrap(array);
                    final Bitmap bitmap = Bitmap.createBitmap(int1, int2, value);
                    bitmap.copyPixelsFromBuffer((Buffer)wrap);
                    this.zad = bitmap;
                    this.zae = true;
                }
                finally {}
            }
            catch (final IOException cause) {
                throw new IllegalStateException("Could not read from parcel file descriptor", cause);
            }
            zaa((Closeable)wrap);
        }
        return this.zad;
    }
    
    @KeepForSdk
    public void release() {
        if (!this.zae) {
            try {
                Preconditions.checkNotNull(this.zab).close();
            }
            catch (final IOException ex) {
                Log.w("BitmapTeleporter", "Could not close PFD", (Throwable)ex);
            }
        }
    }
    
    @KeepForSdk
    public void setTempDir(final File zaf) {
        if (zaf != null) {
            this.zaf = zaf;
            return;
        }
        throw new NullPointerException("Cannot set null temp directory");
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        if (this.zab == null) {
            final Bitmap bitmap = Preconditions.checkNotNull(this.zad);
            final ByteBuffer allocate = ByteBuffer.allocate(bitmap.getRowBytes() * bitmap.getHeight());
            bitmap.copyPixelsToBuffer((Buffer)allocate);
            final byte[] array = allocate.array();
            final File zaf = this.zaf;
            if (zaf != null) {
                try {
                    final File tempFile = File.createTempFile("teleporter", ".tmp", zaf);
                    try {
                        final FileOutputStream out = new FileOutputStream(tempFile);
                        this.zab = ParcelFileDescriptor.open(tempFile, 268435456);
                        tempFile.delete();
                        final DataOutputStream dataOutputStream = new DataOutputStream(new BufferedOutputStream(out));
                        try {
                            try {
                                dataOutputStream.writeInt(array.length);
                                dataOutputStream.writeInt(bitmap.getWidth());
                                dataOutputStream.writeInt(bitmap.getHeight());
                                dataOutputStream.writeUTF(bitmap.getConfig().toString());
                                dataOutputStream.write(array);
                                zaa(dataOutputStream);
                            }
                            finally {}
                        }
                        catch (final IOException cause) {
                            throw new IllegalStateException("Could not write into unlinked file", cause);
                        }
                        zaa(dataOutputStream);
                    }
                    catch (final FileNotFoundException ex) {
                        throw new IllegalStateException("Temporary file is somehow already deleted");
                    }
                }
                catch (final IOException cause2) {
                    throw new IllegalStateException("Could not create temporary file", cause2);
                }
            }
            throw new IllegalStateException("setTempDir() must be called before writing this object to a parcel");
        }
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.zaa);
        SafeParcelWriter.writeParcelable(parcel, 2, (Parcelable)this.zab, n | 0x1, false);
        SafeParcelWriter.writeInt(parcel, 3, this.zac);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
        this.zab = null;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.data;

import com.google.android.gms.common.internal.Preconditions;
import java.util.ArrayList;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public abstract class EntityBuffer<T> extends AbstractDataBuffer<T>
{
    private boolean zaa;
    private ArrayList<Integer> zab;
    
    @KeepForSdk
    public EntityBuffer(final DataHolder dataHolder) {
        super(dataHolder);
        this.zaa = false;
    }
    
    private final void zab() {
        synchronized (this) {
            if (!this.zaa) {
                final int count = Preconditions.checkNotNull(super.mDataHolder).getCount();
                final ArrayList<Integer> zab = new ArrayList<Integer>();
                this.zab = zab;
                if (count > 0) {
                    zab.add(0);
                    final String primaryDataMarkerColumn = this.getPrimaryDataMarkerColumn();
                    String string = super.mDataHolder.getString(primaryDataMarkerColumn, 0, super.mDataHolder.getWindowIndex(0));
                    String s;
                    for (int i = 1; i < count; ++i, string = s) {
                        final int windowIndex = super.mDataHolder.getWindowIndex(i);
                        final String string2 = super.mDataHolder.getString(primaryDataMarkerColumn, i, windowIndex);
                        if (string2 == null) {
                            final StringBuilder sb = new StringBuilder(String.valueOf(primaryDataMarkerColumn).length() + 78);
                            sb.append("Missing value for markerColumn: ");
                            sb.append(primaryDataMarkerColumn);
                            sb.append(", at row: ");
                            sb.append(i);
                            sb.append(", for window: ");
                            sb.append(windowIndex);
                            throw new NullPointerException(sb.toString());
                        }
                        s = string;
                        if (!string2.equals(string)) {
                            this.zab.add(i);
                            s = string2;
                        }
                    }
                }
                this.zaa = true;
            }
        }
    }
    
    @KeepForSdk
    @Override
    public final T get(int windowIndex) {
        this.zab();
        final int zaa = this.zaa(windowIndex);
        int n2;
        final int n = n2 = 0;
        if (windowIndex >= 0) {
            if (windowIndex == this.zab.size()) {
                n2 = n;
            }
            else {
                int n3;
                if (windowIndex == this.zab.size() - 1) {
                    n3 = Preconditions.checkNotNull(super.mDataHolder).getCount();
                }
                else {
                    n3 = this.zab.get(windowIndex + 1);
                }
                n2 = n3 - this.zab.get(windowIndex);
                if (n2 == 1) {
                    final int zaa2 = this.zaa(windowIndex);
                    windowIndex = Preconditions.checkNotNull(super.mDataHolder).getWindowIndex(zaa2);
                    final String childDataMarkerColumn = this.getChildDataMarkerColumn();
                    if (childDataMarkerColumn != null && super.mDataHolder.getString(childDataMarkerColumn, zaa2, windowIndex) == null) {
                        n2 = n;
                    }
                    else {
                        n2 = 1;
                    }
                }
            }
        }
        return this.getEntry(zaa, n2);
    }
    
    @KeepForSdk
    public String getChildDataMarkerColumn() {
        return null;
    }
    
    @KeepForSdk
    @Override
    public int getCount() {
        this.zab();
        return this.zab.size();
    }
    
    @KeepForSdk
    public abstract T getEntry(final int p0, final int p1);
    
    @KeepForSdk
    public abstract String getPrimaryDataMarkerColumn();
    
    public final int zaa(final int n) {
        if (n >= 0 && n < this.zab.size()) {
            return this.zab.get(n);
        }
        final StringBuilder sb = new StringBuilder(53);
        sb.append("Position ");
        sb.append(n);
        sb.append(" is out of bounds for this buffer");
        throw new IllegalArgumentException(sb.toString());
    }
}

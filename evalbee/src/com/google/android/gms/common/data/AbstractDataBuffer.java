// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.data;

import java.util.Iterator;
import android.os.Bundle;
import com.google.android.gms.common.annotation.KeepForSdk;

public abstract class AbstractDataBuffer<T> implements DataBuffer<T>
{
    @KeepForSdk
    protected final DataHolder mDataHolder;
    
    @KeepForSdk
    public AbstractDataBuffer(final DataHolder mDataHolder) {
        this.mDataHolder = mDataHolder;
    }
    
    @Override
    public final void close() {
        this.release();
    }
    
    @Override
    public abstract T get(final int p0);
    
    @Override
    public int getCount() {
        final DataHolder mDataHolder = this.mDataHolder;
        if (mDataHolder == null) {
            return 0;
        }
        return mDataHolder.getCount();
    }
    
    @Override
    public final Bundle getMetadata() {
        final DataHolder mDataHolder = this.mDataHolder;
        if (mDataHolder == null) {
            return null;
        }
        return mDataHolder.getMetadata();
    }
    
    @Deprecated
    @Override
    public boolean isClosed() {
        final DataHolder mDataHolder = this.mDataHolder;
        return mDataHolder == null || mDataHolder.isClosed();
    }
    
    @Override
    public Iterator<T> iterator() {
        return new DataBufferIterator<T>(this);
    }
    
    @Override
    public void release() {
        try (final DataHolder mDataHolder = this.mDataHolder) {}
    }
    
    @Override
    public Iterator<T> singleRefIterator() {
        return new SingleRefDataBufferIterator<T>(this);
    }
}

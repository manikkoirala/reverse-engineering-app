// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.data;

import java.util.NoSuchElementException;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public class SingleRefDataBufferIterator<T> extends DataBufferIterator<T>
{
    private T zac;
    
    public SingleRefDataBufferIterator(final DataBuffer<T> dataBuffer) {
        super(dataBuffer);
    }
    
    @Override
    public final T next() {
        if (this.hasNext()) {
            if (++super.zab == 0) {
                final T checkNotNull = Preconditions.checkNotNull(super.zaa.get(0));
                this.zac = checkNotNull;
                if (!(checkNotNull instanceof DataBufferRef)) {
                    final String value = String.valueOf(checkNotNull.getClass());
                    final StringBuilder sb = new StringBuilder(value.length() + 44);
                    sb.append("DataBuffer reference of type ");
                    sb.append(value);
                    sb.append(" is not movable");
                    throw new IllegalStateException(sb.toString());
                }
            }
            else {
                Preconditions.checkNotNull((DataBufferRef)this.zac).zaa(super.zab);
            }
            return this.zac;
        }
        final int zab = super.zab;
        final StringBuilder sb2 = new StringBuilder(46);
        sb2.append("Cannot advance the iterator beyond ");
        sb2.append(zab);
        throw new NoSuchElementException(sb2.toString());
    }
}

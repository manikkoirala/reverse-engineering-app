// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.data;

import java.util.Iterator;
import com.google.android.gms.common.annotation.KeepForSdk;
import android.os.Bundle;
import java.io.Closeable;
import com.google.android.gms.common.api.Releasable;

public interface DataBuffer<T> extends Iterable<T>, Releasable, Closeable
{
    void close();
    
    T get(final int p0);
    
    int getCount();
    
    @KeepForSdk
    Bundle getMetadata();
    
    @Deprecated
    boolean isClosed();
    
    Iterator<T> iterator();
    
    void release();
    
    Iterator<T> singleRefIterator();
}

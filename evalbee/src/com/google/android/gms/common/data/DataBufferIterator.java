// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.data;

import java.util.NoSuchElementException;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.annotation.KeepForSdk;
import java.util.Iterator;

@KeepForSdk
public class DataBufferIterator<T> implements Iterator<T>
{
    protected final DataBuffer<T> zaa;
    protected int zab;
    
    public DataBufferIterator(final DataBuffer<T> dataBuffer) {
        this.zaa = Preconditions.checkNotNull(dataBuffer);
        this.zab = -1;
    }
    
    @Override
    public final boolean hasNext() {
        return this.zab < this.zaa.getCount() - 1;
    }
    
    @Override
    public T next() {
        if (this.hasNext()) {
            final DataBuffer<T> zaa = this.zaa;
            final int zab = this.zab + 1;
            this.zab = zab;
            return zaa.get(zab);
        }
        final int zab2 = this.zab;
        final StringBuilder sb = new StringBuilder(46);
        sb.append("Cannot advance the iterator beyond ");
        sb.append(zab2);
        throw new NoSuchElementException(sb.toString());
    }
    
    @Override
    public final void remove() {
        throw new UnsupportedOperationException("Cannot remove elements from a DataBufferIterator");
    }
}

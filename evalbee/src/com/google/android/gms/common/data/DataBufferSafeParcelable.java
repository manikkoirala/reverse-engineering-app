// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.data;

import android.os.Parcelable;
import com.google.android.gms.common.internal.Preconditions;
import android.content.ContentValues;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

@KeepForSdk
public class DataBufferSafeParcelable<T extends SafeParcelable> extends AbstractDataBuffer<T>
{
    private static final String[] zaa;
    private final Parcelable$Creator<T> zab;
    
    static {
        zaa = new String[] { "data" };
    }
    
    @KeepForSdk
    public DataBufferSafeParcelable(final DataHolder dataHolder, final Parcelable$Creator<T> zab) {
        super(dataHolder);
        this.zab = zab;
    }
    
    @KeepForSdk
    public static <T extends SafeParcelable> void addValue(final DataHolder.Builder builder, final T t) {
        final Parcel obtain = Parcel.obtain();
        ((Parcelable)t).writeToParcel(obtain, 0);
        final ContentValues contentValues = new ContentValues();
        contentValues.put("data", obtain.marshall());
        builder.withRow(contentValues);
        obtain.recycle();
    }
    
    @KeepForSdk
    public static DataHolder.Builder buildDataHolder() {
        return DataHolder.builder(DataBufferSafeParcelable.zaa);
    }
    
    @KeepForSdk
    @Override
    public T get(final int n) {
        final DataHolder dataHolder = Preconditions.checkNotNull(super.mDataHolder);
        final byte[] byteArray = dataHolder.getByteArray("data", n, dataHolder.getWindowIndex(n));
        final Parcel obtain = Parcel.obtain();
        obtain.unmarshall(byteArray, 0, byteArray.length);
        obtain.setDataPosition(0);
        final SafeParcelable safeParcelable = (SafeParcelable)this.zab.createFromParcel(obtain);
        obtain.recycle();
        return (T)safeParcelable;
    }
}

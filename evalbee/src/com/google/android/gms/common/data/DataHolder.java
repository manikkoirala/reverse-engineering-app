// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.data;

import android.os.BaseBundle;
import android.database.sqlite.SQLiteClosable;
import java.util.Iterator;
import com.google.android.gms.common.internal.Asserts;
import android.content.ContentValues;
import java.util.HashMap;
import android.database.CharArrayBuffer;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import java.util.Map;
import android.util.Log;
import android.database.CursorIndexOutOfBoundsException;
import com.google.android.gms.common.internal.Preconditions;
import java.util.ArrayList;
import com.google.android.gms.common.sqlite.CursorWrapper;
import android.database.Cursor;
import android.database.CursorWindow;
import android.os.Bundle;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.annotation.KeepForSdk;
import java.io.Closeable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@KeepForSdk
@KeepName
@Class(creator = "DataHolderCreator", validate = true)
public final class DataHolder extends AbstractSafeParcelable implements Closeable
{
    @KeepForSdk
    public static final Parcelable$Creator<DataHolder> CREATOR;
    private static final Builder zaf;
    @VersionField(id = 1000)
    final int zaa;
    Bundle zab;
    int[] zac;
    int zad;
    boolean zae;
    @Field(getter = "getColumns", id = 1)
    private final String[] zag;
    @Field(getter = "getWindows", id = 2)
    private final CursorWindow[] zah;
    @Field(getter = "getStatusCode", id = 3)
    private final int zai;
    @Field(getter = "getMetadata", id = 4)
    private final Bundle zaj;
    private boolean zak;
    
    static {
        CREATOR = (Parcelable$Creator)new zaf();
        zaf = (Builder)new zab(new String[0], null);
    }
    
    @Constructor
    public DataHolder(@Param(id = 1000) final int zaa, @Param(id = 1) final String[] zag, @Param(id = 2) final CursorWindow[] zah, @Param(id = 3) final int zai, @Param(id = 4) final Bundle zaj) {
        this.zae = false;
        this.zak = true;
        this.zaa = zaa;
        this.zag = zag;
        this.zah = zah;
        this.zai = zai;
        this.zaj = zaj;
    }
    
    @KeepForSdk
    public DataHolder(final Cursor cursor, final int n, final Bundle bundle) {
        final CursorWrapper cursorWrapper = new CursorWrapper(cursor);
        final String[] columnNames = cursorWrapper.getColumnNames();
        final ArrayList list = new ArrayList();
        try {
            final int count = cursorWrapper.getCount();
            final CursorWindow window = cursorWrapper.getWindow();
            int numRows;
            if (window != null && window.getStartPosition() == 0) {
                ((SQLiteClosable)window).acquireReference();
                cursorWrapper.setWindow(null);
                list.add(window);
                numRows = window.getNumRows();
            }
            else {
                numRows = 0;
            }
            while (numRows < count && cursorWrapper.moveToPosition(numRows)) {
                CursorWindow window2 = cursorWrapper.getWindow();
                if (window2 != null) {
                    ((SQLiteClosable)window2).acquireReference();
                    cursorWrapper.setWindow(null);
                }
                else {
                    window2 = new CursorWindow(false);
                    window2.setStartPosition(numRows);
                    cursorWrapper.fillWindow(numRows, window2);
                }
                if (window2.getNumRows() == 0) {
                    break;
                }
                list.add(window2);
                numRows = window2.getStartPosition() + window2.getNumRows();
            }
            cursorWrapper.close();
            this(columnNames, list.toArray(new CursorWindow[list.size()]), n, bundle);
        }
        finally {
            cursorWrapper.close();
        }
    }
    
    private DataHolder(final Builder builder, final int n, final Bundle bundle) {
        this(Builder.zac(builder), zaf(builder, -1), n, null);
    }
    
    @KeepForSdk
    public DataHolder(final String[] array, final CursorWindow[] array2, final int zai, final Bundle zaj) {
        this.zae = false;
        this.zak = true;
        this.zaa = 1;
        this.zag = Preconditions.checkNotNull(array);
        this.zah = Preconditions.checkNotNull(array2);
        this.zai = zai;
        this.zaj = zaj;
        this.zad();
    }
    
    @KeepForSdk
    public static Builder builder(final String[] array) {
        return new Builder(array, null, null);
    }
    
    @KeepForSdk
    public static DataHolder empty(final int n) {
        return new DataHolder(DataHolder.zaf, n, null);
    }
    
    private final void zae(String s, final int n) {
        final Bundle zab = this.zab;
        if (zab == null || !((BaseBundle)zab).containsKey(s)) {
            s = String.valueOf(s);
            if (s.length() != 0) {
                s = "No such column: ".concat(s);
            }
            else {
                s = new String("No such column: ");
            }
            throw new IllegalArgumentException(s);
        }
        if (this.isClosed()) {
            throw new IllegalArgumentException("Buffer is closed.");
        }
        if (n >= 0 && n < this.zad) {
            return;
        }
        throw new CursorIndexOutOfBoundsException(n, this.zad);
    }
    
    private static CursorWindow[] zaf(final Builder builder, int i) {
        i = Builder.zac(builder).length;
        final int n = 0;
        if (i == 0) {
            return new CursorWindow[0];
        }
        final ArrayList zab = Builder.zab(builder);
        final int size = zab.size();
        CursorWindow cursorWindow = new CursorWindow(false);
        final ArrayList list = new ArrayList();
        list.add(cursorWindow);
        cursorWindow.setNumColumns(Builder.zac(builder).length);
        i = 0;
        int n2 = 0;
    Label_0075:
        while (i < size) {
            try {
                if (!cursorWindow.allocRow()) {
                    final StringBuilder sb = new StringBuilder(72);
                    sb.append("Allocating additional cursor window for large data set (row ");
                    sb.append(i);
                    sb.append(")");
                    Log.d("DataHolder", sb.toString());
                    final CursorWindow cursorWindow2 = new CursorWindow(false);
                    cursorWindow2.setStartPosition(i);
                    cursorWindow2.setNumColumns(Builder.zac(builder).length);
                    list.add(cursorWindow2);
                    cursorWindow = cursorWindow2;
                    if (!cursorWindow2.allocRow()) {
                        Log.e("DataHolder", "Unable to allocate row to hold data.");
                        list.remove(cursorWindow2);
                        return list.toArray(new CursorWindow[list.size()]);
                    }
                }
                final Map map = (Map)zab.get(i);
                int j = 0;
                boolean b = true;
            Label_0691_Outer:
                while (true) {
                    while (true) {
                        while (j < Builder.zac(builder).length) {
                            if (b) {
                                final String s = Builder.zac(builder)[j];
                                final Object value = map.get(s);
                                Label_0489: {
                                    if (value == null) {
                                        b = cursorWindow.putNull(i, j);
                                    }
                                    else if (value instanceof String) {
                                        b = cursorWindow.putString((String)value, i, j);
                                    }
                                    else {
                                        long longValue;
                                        if (value instanceof Long) {
                                            longValue = (long)value;
                                        }
                                        else {
                                            if (value instanceof Integer) {
                                                b = cursorWindow.putLong((long)(int)value, i, j);
                                                break Label_0489;
                                            }
                                            if (value instanceof Boolean) {
                                                if (!(boolean)value) {
                                                    longValue = 0L;
                                                }
                                                else {
                                                    longValue = 1L;
                                                }
                                            }
                                            else {
                                                if (value instanceof byte[]) {
                                                    b = cursorWindow.putBlob((byte[])value, i, j);
                                                    break Label_0489;
                                                }
                                                if (value instanceof Double) {
                                                    b = cursorWindow.putDouble((double)value, i, j);
                                                    break Label_0489;
                                                }
                                                if (value instanceof Float) {
                                                    b = cursorWindow.putDouble((double)(float)value, i, j);
                                                    break Label_0489;
                                                }
                                                final String string = value.toString();
                                                i = String.valueOf(s).length();
                                                final StringBuilder sb2 = new StringBuilder(i + 32 + string.length());
                                                sb2.append("Unsupported object for column ");
                                                sb2.append(s);
                                                sb2.append(": ");
                                                sb2.append(string);
                                                throw new IllegalArgumentException(sb2.toString());
                                            }
                                        }
                                        b = cursorWindow.putLong(longValue, i, j);
                                    }
                                }
                                ++j;
                            }
                            else {
                                if (n2 == 0) {
                                    final StringBuilder sb3 = new StringBuilder(74);
                                    sb3.append("Couldn't populate window data for row ");
                                    sb3.append(i);
                                    sb3.append(" - allocating new window.");
                                    Log.d("DataHolder", sb3.toString());
                                    cursorWindow.freeLastRow();
                                    cursorWindow = new CursorWindow(false);
                                    cursorWindow.setStartPosition(i);
                                    cursorWindow.setNumColumns(Builder.zac(builder).length);
                                    list.add(cursorWindow);
                                    --i;
                                    n2 = 1;
                                    ++i;
                                    continue Label_0075;
                                }
                                throw new zad("Could not add the value to a new CursorWindow. The size of value may be larger than what a CursorWindow can handle.");
                            }
                        }
                        if (b) {
                            n2 = 0;
                            continue;
                        }
                        break;
                    }
                    continue Label_0691_Outer;
                }
            }
            catch (final RuntimeException ex) {
                int size2;
                for (size2 = list.size(), i = n; i < size2; ++i) {
                    ((SQLiteClosable)list.get(i)).close();
                }
                throw ex;
            }
            break;
        }
        return list.toArray(new CursorWindow[list.size()]);
    }
    
    @KeepForSdk
    @Override
    public void close() {
        synchronized (this) {
            if (!this.zae) {
                this.zae = true;
                int n = 0;
                while (true) {
                    final CursorWindow[] zah = this.zah;
                    if (n >= zah.length) {
                        break;
                    }
                    ((SQLiteClosable)zah[n]).close();
                    ++n;
                }
            }
        }
    }
    
    public final void finalize() {
        try {
            if (this.zak && this.zah.length > 0 && !this.isClosed()) {
                this.close();
                final String string = this.toString();
                final StringBuilder sb = new StringBuilder(String.valueOf(string).length() + 178);
                sb.append("Internal data leak within a DataBuffer object detected!  Be sure to explicitly call release() on all DataBuffer extending objects when you are done with them. (internal object: ");
                sb.append(string);
                sb.append(")");
                Log.e("DataBuffer", sb.toString());
            }
        }
        finally {
            super.finalize();
        }
    }
    
    @KeepForSdk
    public boolean getBoolean(final String s, final int n, final int n2) {
        this.zae(s, n);
        return this.zah[n2].getLong(n, ((BaseBundle)this.zab).getInt(s)) == 1L;
    }
    
    @KeepForSdk
    public byte[] getByteArray(final String s, final int n, final int n2) {
        this.zae(s, n);
        return this.zah[n2].getBlob(n, ((BaseBundle)this.zab).getInt(s));
    }
    
    @KeepForSdk
    public int getCount() {
        return this.zad;
    }
    
    @KeepForSdk
    public int getInteger(final String s, final int n, final int n2) {
        this.zae(s, n);
        return this.zah[n2].getInt(n, ((BaseBundle)this.zab).getInt(s));
    }
    
    @KeepForSdk
    public long getLong(final String s, final int n, final int n2) {
        this.zae(s, n);
        return this.zah[n2].getLong(n, ((BaseBundle)this.zab).getInt(s));
    }
    
    @KeepForSdk
    public Bundle getMetadata() {
        return this.zaj;
    }
    
    @KeepForSdk
    public int getStatusCode() {
        return this.zai;
    }
    
    @KeepForSdk
    public String getString(final String s, final int n, final int n2) {
        this.zae(s, n);
        return this.zah[n2].getString(n, ((BaseBundle)this.zab).getInt(s));
    }
    
    @KeepForSdk
    public int getWindowIndex(int n) {
        int n2 = 0;
        Preconditions.checkState(n >= 0 && n < this.zad);
        int length;
        int n3;
        while (true) {
            final int[] zac = this.zac;
            length = zac.length;
            n3 = n2;
            if (n2 >= length) {
                break;
            }
            if (n < zac[n2]) {
                n3 = n2 - 1;
                break;
            }
            ++n2;
        }
        if ((n = n3) == length) {
            n = n3 - 1;
        }
        return n;
    }
    
    @KeepForSdk
    public boolean hasColumn(final String s) {
        return ((BaseBundle)this.zab).containsKey(s);
    }
    
    @KeepForSdk
    public boolean hasNull(final String s, final int n, final int n2) {
        this.zae(s, n);
        return this.zah[n2].isNull(n, ((BaseBundle)this.zab).getInt(s));
    }
    
    @KeepForSdk
    public boolean isClosed() {
        synchronized (this) {
            return this.zae;
        }
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeStringArray(parcel, 1, this.zag, false);
        SafeParcelWriter.writeTypedArray(parcel, 2, this.zah, n, false);
        SafeParcelWriter.writeInt(parcel, 3, this.getStatusCode());
        SafeParcelWriter.writeBundle(parcel, 4, this.getMetadata(), false);
        SafeParcelWriter.writeInt(parcel, 1000, this.zaa);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
        if ((n & 0x1) != 0x0) {
            this.close();
        }
    }
    
    public final double zaa(final String s, final int n, final int n2) {
        this.zae(s, n);
        return this.zah[n2].getDouble(n, ((BaseBundle)this.zab).getInt(s));
    }
    
    public final float zab(final String s, final int n, final int n2) {
        this.zae(s, n);
        return this.zah[n2].getFloat(n, ((BaseBundle)this.zab).getInt(s));
    }
    
    public final void zac(final String s, final int n, final int n2, final CharArrayBuffer charArrayBuffer) {
        this.zae(s, n);
        this.zah[n2].copyStringToBuffer(n, ((BaseBundle)this.zab).getInt(s), charArrayBuffer);
    }
    
    public final void zad() {
        this.zab = new Bundle();
        final int n = 0;
        int n2 = 0;
        while (true) {
            final String[] zag = this.zag;
            if (n2 >= zag.length) {
                break;
            }
            ((BaseBundle)this.zab).putInt(zag[n2], n2);
            ++n2;
        }
        this.zac = new int[this.zah.length];
        int zad = 0;
        int n3 = n;
        while (true) {
            final CursorWindow[] zah = this.zah;
            if (n3 >= zah.length) {
                break;
            }
            this.zac[n3] = zad;
            zad += this.zah[n3].getNumRows() - (zad - zah[n3].getStartPosition());
            ++n3;
        }
        this.zad = zad;
    }
    
    @KeepForSdk
    public static class Builder
    {
        private final String[] zaa = Preconditions.checkNotNull(array);
        private final ArrayList<HashMap<String, Object>> zab = new ArrayList<HashMap<String, Object>>();
        private final HashMap<Object, Integer> zac = new HashMap<Object, Integer>();
        
        @KeepForSdk
        public DataHolder build(final int n) {
            return new DataHolder(this, n, null, null);
        }
        
        @KeepForSdk
        public DataHolder build(final int n, final Bundle bundle) {
            return new DataHolder(this, n, bundle, -1, null);
        }
        
        @KeepForSdk
        public Builder withRow(final ContentValues contentValues) {
            Asserts.checkNotNull(contentValues);
            final HashMap hashMap = new HashMap(contentValues.size());
            for (final Map.Entry<String, V> entry : contentValues.valueSet()) {
                hashMap.put(entry.getKey(), entry.getValue());
            }
            return this.zaa(hashMap);
        }
        
        public Builder zaa(final HashMap<String, Object> e) {
            Asserts.checkNotNull(e);
            this.zab.add(e);
            return this;
        }
    }
}

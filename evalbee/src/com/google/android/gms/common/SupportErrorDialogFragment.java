// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common;

import androidx.fragment.app.k;
import android.app.AlertDialog$Builder;
import android.content.Context;
import android.os.Bundle;
import android.content.DialogInterface;
import org.jetbrains.annotations.NotNull;
import android.content.DialogInterface$OnDismissListener;
import com.google.android.gms.common.internal.Preconditions;
import android.content.DialogInterface$OnCancelListener;
import android.app.Dialog;
import androidx.fragment.app.d;

public class SupportErrorDialogFragment extends d
{
    private Dialog zaa;
    private DialogInterface$OnCancelListener zab;
    private Dialog zac;
    
    public static SupportErrorDialogFragment newInstance(final Dialog dialog) {
        return newInstance(dialog, null);
    }
    
    public static SupportErrorDialogFragment newInstance(Dialog zaa, final DialogInterface$OnCancelListener zab) {
        final SupportErrorDialogFragment supportErrorDialogFragment = new SupportErrorDialogFragment();
        zaa = Preconditions.checkNotNull(zaa, "Cannot display null dialog");
        zaa.setOnCancelListener((DialogInterface$OnCancelListener)null);
        zaa.setOnDismissListener((DialogInterface$OnDismissListener)null);
        supportErrorDialogFragment.zaa = zaa;
        if (zab != null) {
            supportErrorDialogFragment.zab = zab;
        }
        return supportErrorDialogFragment;
    }
    
    @Override
    public void onCancel(final DialogInterface dialogInterface) {
        final DialogInterface$OnCancelListener zab = this.zab;
        if (zab != null) {
            zab.onCancel(dialogInterface);
        }
    }
    
    @Override
    public Dialog onCreateDialog(final Bundle bundle) {
        Dialog dialog;
        if ((dialog = this.zaa) == null) {
            this.setShowsDialog(false);
            if (this.zac == null) {
                this.zac = (Dialog)new AlertDialog$Builder((Context)Preconditions.checkNotNull(this.getContext())).create();
            }
            dialog = this.zac;
        }
        return dialog;
    }
    
    @Override
    public void show(final androidx.fragment.app.k k, final String s) {
        super.show(k, s);
    }
}

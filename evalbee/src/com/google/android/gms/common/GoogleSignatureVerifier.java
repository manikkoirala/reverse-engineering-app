// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common;

import android.content.pm.PackageManager$NameNotFoundException;
import android.content.pm.ApplicationInfo;
import android.content.pm.Signature;
import android.util.Log;
import android.content.pm.PackageInfo;
import com.google.android.gms.common.internal.Preconditions;
import android.content.Context;
import java.util.Set;
import com.google.android.gms.common.internal.ShowFirstParty;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.errorprone.annotations.RestrictedInheritance;

@RestrictedInheritance(allowedOnPath = ".*java.*/com/google/android/gms/common/testing/.*", explanation = "Sub classing of GMS Core's APIs are restricted to testing fakes.", link = "go/gmscore-restrictedinheritance")
@KeepForSdk
@ShowFirstParty
public class GoogleSignatureVerifier
{
    private static GoogleSignatureVerifier zza;
    private static volatile Set zzb;
    private final Context zzc;
    private volatile String zzd;
    
    public GoogleSignatureVerifier(final Context context) {
        this.zzc = context.getApplicationContext();
    }
    
    @KeepForSdk
    public static GoogleSignatureVerifier getInstance(final Context context) {
        Preconditions.checkNotNull(context);
        synchronized (GoogleSignatureVerifier.class) {
            if (GoogleSignatureVerifier.zza == null) {
                zzn.zze(context);
                GoogleSignatureVerifier.zza = new GoogleSignatureVerifier(context);
            }
            return GoogleSignatureVerifier.zza;
        }
    }
    
    public static final zzj zza(final PackageInfo packageInfo, final zzj... array) {
        final Signature[] signatures = packageInfo.signatures;
        if (signatures == null) {
            return null;
        }
        if (signatures.length != 1) {
            Log.w("GoogleSignatureVerifier", "Package has more than one signature.");
            return null;
        }
        final Signature[] signatures2 = packageInfo.signatures;
        int i = 0;
        final zzk zzk = new zzk(signatures2[0].toByteArray());
        while (i < array.length) {
            if (array[i].equals(zzk)) {
                return array[i];
            }
            ++i;
        }
        return null;
    }
    
    public static final boolean zzb(final PackageInfo packageInfo, final boolean b) {
        boolean b2 = b;
        Label_0065: {
            if (b) {
                b2 = b;
                if (packageInfo != null) {
                    if (!"com.android.vending".equals(packageInfo.packageName)) {
                        b2 = b;
                        if (!"com.google.android.gms".equals(packageInfo.packageName)) {
                            break Label_0065;
                        }
                    }
                    final ApplicationInfo applicationInfo = packageInfo.applicationInfo;
                    b2 = (applicationInfo != null && (applicationInfo.flags & 0x81) != 0x0);
                }
            }
        }
        if (packageInfo != null && packageInfo.signatures != null) {
            zzj zzj;
            if (b2) {
                zzj = zza(packageInfo, zzm.zza);
            }
            else {
                zzj = zza(packageInfo, zzm.zza[0]);
            }
            if (zzj != null) {
                return true;
            }
        }
        return false;
    }
    
    private final zzx zzc(final String s, final boolean b, final boolean b2) {
        String s2 = "null pkg";
        if (s == null) {
            return zzx.zzc("null pkg");
        }
        if (!s.equals(this.zzd)) {
            Label_0208: {
                if (zzn.zzg()) {
                    final zzx zzx = zzn.zzb(s, GooglePlayServicesUtilLight.honorsDebugCertificates(this.zzc), false, false);
                    break Label_0208;
                }
                try {
                    final PackageInfo packageInfo = this.zzc.getPackageManager().getPackageInfo(s, 64);
                    final boolean honorsDebugCertificates = GooglePlayServicesUtilLight.honorsDebugCertificates(this.zzc);
                    Label_0077: {
                        if (packageInfo != null) {
                            final Signature[] signatures = packageInfo.signatures;
                            if (signatures != null && signatures.length == 1) {
                                final zzk zzk = new zzk(packageInfo.signatures[0].toByteArray());
                                final String packageName = packageInfo.packageName;
                                final zzx zzx = zzn.zza(packageName, zzk, honorsDebugCertificates, false);
                                if (zzx.zza) {
                                    final ApplicationInfo applicationInfo = packageInfo.applicationInfo;
                                    if (applicationInfo != null && (applicationInfo.flags & 0x2) != 0x0 && zzn.zza(packageName, zzk, false, true).zza) {
                                        s2 = "debuggable release cert app rejected";
                                        break Label_0077;
                                    }
                                }
                                break Label_0208;
                            }
                            s2 = "single cert required";
                        }
                    }
                    final zzx zzx = com.google.android.gms.common.zzx.zzc(s2);
                    if (zzx.zza) {
                        this.zzd = s;
                    }
                    return zzx;
                }
                catch (final PackageManager$NameNotFoundException ex) {
                    return zzx.zzd("no pkg ".concat(s), (Throwable)ex);
                }
            }
        }
        return zzx.zzb();
    }
    
    @KeepForSdk
    public boolean isGooglePublicSignedPackage(final PackageInfo packageInfo) {
        if (packageInfo == null) {
            return false;
        }
        if (zzb(packageInfo, false)) {
            return true;
        }
        if (zzb(packageInfo, true)) {
            if (GooglePlayServicesUtilLight.honorsDebugCertificates(this.zzc)) {
                return true;
            }
            Log.w("GoogleSignatureVerifier", "Test-keys aren't accepted on this build.");
        }
        return false;
    }
    
    @KeepForSdk
    @ShowFirstParty
    public boolean isPackageGoogleSigned(final String s) {
        final zzx zzc = this.zzc(s, false, false);
        zzc.zze();
        return zzc.zza;
    }
    
    @KeepForSdk
    @ShowFirstParty
    public boolean isUidGoogleSigned(int i) {
        final String[] packagesForUid = this.zzc.getPackageManager().getPackagesForUid(i);
        zzx zzx = null;
        Label_0079: {
            if (packagesForUid != null) {
                final int length = packagesForUid.length;
                if (length != 0) {
                    zzx = null;
                    for (i = 0; i < length; ++i) {
                        zzx = this.zzc(packagesForUid[i], false, false);
                        if (zzx.zza) {
                            break Label_0079;
                        }
                    }
                    Preconditions.checkNotNull(zzx);
                    break Label_0079;
                }
            }
            zzx = com.google.android.gms.common.zzx.zzc("no pkgs");
        }
        zzx.zze();
        return zzx.zza;
    }
}

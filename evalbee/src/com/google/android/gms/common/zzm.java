// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common;

import com.google.android.gms.common.util.VisibleForTesting;

@VisibleForTesting
final class zzm
{
    static final zzj[] zza;
    
    static {
        zza = new zzj[] { zzn.zzc, zzn.zzd };
    }
}

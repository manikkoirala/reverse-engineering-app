// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.config;

import com.google.android.gms.common.util.VisibleForTesting;
import android.util.Log;
import android.os.StrictMode$ThreadPolicy;
import android.os.Binder;
import android.os.StrictMode;
import com.google.errorprone.annotations.ResultIgnorabilityUnspecified;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public abstract class GservicesValue<T>
{
    private static final Object zzc;
    protected final String zza;
    protected final Object zzb;
    private Object zzd;
    
    static {
        zzc = new Object();
    }
    
    public GservicesValue(final String zza, final Object zzb) {
        this.zzd = null;
        this.zza = zza;
        this.zzb = zzb;
    }
    
    @ResultIgnorabilityUnspecified
    @KeepForSdk
    public static boolean isInitialized() {
        synchronized (GservicesValue.zzc) {
            monitorexit(GservicesValue.zzc);
            return false;
        }
    }
    
    @KeepForSdk
    public static GservicesValue<Float> value(final String s, final Float n) {
        return new zzd(s, n);
    }
    
    @KeepForSdk
    public static GservicesValue<Integer> value(final String s, final Integer n) {
        return new zzc(s, n);
    }
    
    @KeepForSdk
    public static GservicesValue<Long> value(final String s, final Long n) {
        return new zzb(s, n);
    }
    
    @KeepForSdk
    public static GservicesValue<String> value(final String s, final String s2) {
        return new zze(s, s2);
    }
    
    @KeepForSdk
    public static GservicesValue<Boolean> value(final String s, final boolean b) {
        return new zza(s, b);
    }
    
    @ResultIgnorabilityUnspecified
    @KeepForSdk
    public final T get() {
        final Object zzd = this.zzd;
        if (zzd != null) {
            return (T)zzd;
        }
        final StrictMode$ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        synchronized (GservicesValue.zzc) {
            monitorexit(GservicesValue.zzc);
            synchronized (GservicesValue.zzc) {
                monitorexit(GservicesValue.zzc);
                try {
                    try {
                        final Object zza = this.zza(this.zza);
                        StrictMode.setThreadPolicy(allowThreadDiskReads);
                        return (T)zza;
                    }
                    finally {}
                }
                catch (final SecurityException o) {
                    final long clearCallingIdentity = Binder.clearCallingIdentity();
                    try {
                        final Object zza2 = this.zza(this.zza);
                        Binder.restoreCallingIdentity(clearCallingIdentity);
                        StrictMode.setThreadPolicy(allowThreadDiskReads);
                        return (T)zza2;
                    }
                    finally {
                        Binder.restoreCallingIdentity(clearCallingIdentity);
                    }
                }
                StrictMode.setThreadPolicy(allowThreadDiskReads);
            }
        }
    }
    
    @Deprecated
    @KeepForSdk
    public final T getBinderSafe() {
        return this.get();
    }
    
    @KeepForSdk
    @VisibleForTesting
    public void override(final T zzd) {
        Log.w("GservicesValue", "GservicesValue.override(): test should probably call initForTests() first");
        this.zzd = zzd;
        synchronized (GservicesValue.zzc) {
            synchronized (GservicesValue.zzc) {
                monitorexit(GservicesValue.zzc);
            }
        }
    }
    
    @KeepForSdk
    @VisibleForTesting
    public void resetOverride() {
        this.zzd = null;
    }
    
    public abstract Object zza(final String p0);
}

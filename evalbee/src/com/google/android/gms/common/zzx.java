// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common;

import android.util.Log;

class zzx
{
    private static final zzx zze;
    final boolean zza;
    final String zzb;
    final Throwable zzc;
    final int zzd;
    
    static {
        zze = new zzx(true, 3, 1, null, null);
    }
    
    private zzx(final boolean zza, final int zzd, final int n, final String zzb, final Throwable zzc) {
        this.zza = zza;
        this.zzd = zzd;
        this.zzb = zzb;
        this.zzc = zzc;
    }
    
    @Deprecated
    public static zzx zzb() {
        return zzx.zze;
    }
    
    public static zzx zzc(final String s) {
        return new zzx(false, 1, 5, s, null);
    }
    
    public static zzx zzd(final String s, final Throwable t) {
        return new zzx(false, 1, 5, s, t);
    }
    
    public static zzx zzf(final int n) {
        return new zzx(true, n, 1, null, null);
    }
    
    public static zzx zzg(final int n, final int n2, final String s, final Throwable t) {
        return new zzx(false, n, n2, s, t);
    }
    
    public String zza() {
        return this.zzb;
    }
    
    public final void zze() {
        if (!this.zza && Log.isLoggable("GoogleCertificatesRslt", 3)) {
            if (this.zzc != null) {
                Log.d("GoogleCertificatesRslt", this.zza(), this.zzc);
                return;
            }
            Log.d("GoogleCertificatesRslt", this.zza());
        }
    }
}

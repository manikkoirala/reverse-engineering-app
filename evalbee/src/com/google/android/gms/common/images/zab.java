// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.images;

import android.os.ResultReceiver;
import android.net.Uri;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import com.google.android.gms.common.internal.Asserts;

final class zab implements Runnable
{
    final ImageManager zaa;
    private final zag zab;
    
    public zab(final ImageManager zaa, final zag zab) {
        this.zaa = zaa;
        this.zab = zab;
    }
    
    @Override
    public final void run() {
        Asserts.checkMainThread("LoadImageRunnable must be executed on the main thread");
        final ImageManager.ImageReceiver imageReceiver = ImageManager.zag(this.zaa).get(this.zab);
        if (imageReceiver != null) {
            ImageManager.zag(this.zaa).remove(this.zab);
            imageReceiver.zac(this.zab);
        }
        zag zag = this.zab;
        final zad zaa = zag.zaa;
        final Uri zaa2 = zaa.zaa;
        Label_0114: {
            if (zaa2 != null) {
                final Long n = ImageManager.zaf(this.zaa).get(zaa2);
                if (n != null) {
                    if (SystemClock.elapsedRealtime() - n < 3600000L) {
                        zag = this.zab;
                        break Label_0114;
                    }
                    ImageManager.zaf(this.zaa).remove(zaa.zaa);
                }
                this.zab.zaa(null, false, true, false);
                ResultReceiver resultReceiver;
                if ((resultReceiver = ImageManager.zah(this.zaa).get(zaa.zaa)) == null) {
                    resultReceiver = this.zaa.new ImageReceiver(zaa.zaa);
                    ImageManager.zah(this.zaa).put(zaa.zaa, resultReceiver);
                }
                ((ImageManager.ImageReceiver)resultReceiver).zab(this.zab);
                final zag zab = this.zab;
                if (!(zab instanceof zaf)) {
                    ImageManager.zag(this.zaa).put(zab, resultReceiver);
                }
                synchronized (ImageManager.zad()) {
                    if (!ImageManager.zae().contains(zaa.zaa)) {
                        ImageManager.zae().add(zaa.zaa);
                        ((ImageManager.ImageReceiver)resultReceiver).zad();
                    }
                    return;
                }
            }
        }
        final ImageManager zaa3 = this.zaa;
        zag.zab(ImageManager.zaa(zaa3), ImageManager.zac(zaa3), true);
    }
}

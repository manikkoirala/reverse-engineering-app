// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.images;

import android.graphics.Bitmap;
import java.util.concurrent.CountDownLatch;
import java.io.IOException;
import android.util.Log;
import android.graphics.BitmapFactory;
import com.google.android.gms.common.internal.Asserts;
import android.os.ParcelFileDescriptor;
import android.net.Uri;

final class zaa implements Runnable
{
    final ImageManager zaa;
    private final Uri zab;
    private final ParcelFileDescriptor zac;
    
    public zaa(final ImageManager zaa, final Uri zab, final ParcelFileDescriptor zac) {
        this.zaa = zaa;
        this.zab = zab;
        this.zac = zac;
    }
    
    @Override
    public final void run() {
        Asserts.checkNotMainThread("LoadBitmapFromDiskRunnable can't be executed in the main thread");
        final ParcelFileDescriptor zac = this.zac;
        Bitmap decodeFileDescriptor = null;
        final Bitmap bitmap = null;
        boolean b = false;
        final boolean b2 = false;
        if (zac != null) {
            try {
                decodeFileDescriptor = BitmapFactory.decodeFileDescriptor(zac.getFileDescriptor());
                b = b2;
            }
            catch (final OutOfMemoryError outOfMemoryError) {
                Log.e("ImageManager", "OOM while loading bitmap for uri: ".concat(String.valueOf(this.zab)), (Throwable)outOfMemoryError);
                b = true;
                decodeFileDescriptor = bitmap;
            }
            try {
                this.zac.close();
            }
            catch (final IOException ex) {
                Log.e("ImageManager", "closed failed", (Throwable)ex);
            }
        }
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        final ImageManager zaa = this.zaa;
        ImageManager.zab(zaa).post((Runnable)new zac(zaa, this.zab, decodeFileDescriptor, b, countDownLatch));
        try {
            countDownLatch.await();
        }
        catch (final InterruptedException ex2) {
            Log.w("ImageManager", "Latch interrupted while posting ".concat(String.valueOf(this.zab)));
        }
    }
}

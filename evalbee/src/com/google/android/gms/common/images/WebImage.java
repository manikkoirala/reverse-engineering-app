// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.images;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import java.util.Locale;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.annotation.KeepForSdk;
import org.json.JSONException;
import org.json.JSONObject;
import android.net.Uri;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "WebImageCreator")
public final class WebImage extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<WebImage> CREATOR;
    @VersionField(id = 1)
    final int zaa;
    @Field(getter = "getUrl", id = 2)
    private final Uri zab;
    @Field(getter = "getWidth", id = 3)
    private final int zac;
    @Field(getter = "getHeight", id = 4)
    private final int zad;
    
    static {
        CREATOR = (Parcelable$Creator)new zah();
    }
    
    @Constructor
    public WebImage(@Param(id = 1) final int zaa, @Param(id = 2) final Uri zab, @Param(id = 3) final int zac, @Param(id = 4) final int zad) {
        this.zaa = zaa;
        this.zab = zab;
        this.zac = zac;
        this.zad = zad;
    }
    
    public WebImage(final Uri uri) {
        this(uri, 0, 0);
    }
    
    public WebImage(final Uri uri, final int n, final int n2) {
        this(1, uri, n, n2);
        if (uri == null) {
            throw new IllegalArgumentException("url cannot be null");
        }
        if (n >= 0 && n2 >= 0) {
            return;
        }
        throw new IllegalArgumentException("width and height must not be negative");
    }
    
    @KeepForSdk
    public WebImage(final JSONObject jsonObject) {
        Uri uri = Uri.EMPTY;
        while (true) {
            if (!jsonObject.has("url")) {
                break Label_0025;
            }
            try {
                uri = Uri.parse(jsonObject.getString("url"));
                this(uri, jsonObject.optInt("width", 0), jsonObject.optInt("height", 0));
            }
            catch (final JSONException ex) {
                uri = uri;
                continue;
            }
            break;
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o != null) {
            if (o instanceof WebImage) {
                final WebImage webImage = (WebImage)o;
                if (Objects.equal(this.zab, webImage.zab) && this.zac == webImage.zac && this.zad == webImage.zad) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public int getHeight() {
        return this.zad;
    }
    
    public Uri getUrl() {
        return this.zab;
    }
    
    public int getWidth() {
        return this.zac;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zab, this.zac, this.zad);
    }
    
    @KeepForSdk
    public JSONObject toJson() {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("url", (Object)this.zab.toString());
            jsonObject.put("width", this.zac);
            jsonObject.put("height", this.zad);
            return jsonObject;
        }
        catch (final JSONException ex) {
            return jsonObject;
        }
    }
    
    @Override
    public String toString() {
        return String.format(Locale.US, "Image %dx%d %s", this.zac, this.zad, this.zab.toString());
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.zaa);
        SafeParcelWriter.writeParcelable(parcel, 2, (Parcelable)this.getUrl(), n, false);
        SafeParcelWriter.writeInt(parcel, 3, this.getWidth());
        SafeParcelWriter.writeInt(parcel, 4, this.getHeight());
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

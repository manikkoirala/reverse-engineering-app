// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.images;

import android.graphics.drawable.BitmapDrawable;
import com.google.android.gms.common.internal.Asserts;
import android.graphics.Bitmap;
import com.google.android.gms.internal.base.zak;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;

public abstract class zag
{
    final zad zaa;
    protected int zab;
    
    public zag(final Uri uri, final int zab) {
        this.zab = 0;
        this.zaa = new zad(uri);
        this.zab = zab;
    }
    
    public abstract void zaa(final Drawable p0, final boolean p1, final boolean p2, final boolean p3);
    
    public final void zab(final Context context, final zak zak, final boolean b) {
        final int zab = this.zab;
        Drawable drawable;
        if (zab != 0) {
            drawable = context.getResources().getDrawable(zab);
        }
        else {
            drawable = null;
        }
        this.zaa(drawable, b, false, false);
    }
    
    public final void zac(final Context context, final Bitmap bitmap, final boolean b) {
        Asserts.checkNotNull(bitmap);
        this.zaa((Drawable)new BitmapDrawable(context.getResources(), bitmap), false, false, true);
    }
}

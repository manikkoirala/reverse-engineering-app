// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.images;

import com.google.android.gms.internal.base.zai;
import com.google.android.gms.internal.base.zaj;
import android.graphics.drawable.Drawable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Asserts;
import android.net.Uri;
import android.widget.ImageView;
import java.lang.ref.WeakReference;

public final class zae extends zag
{
    private final WeakReference<ImageView> zac;
    
    public zae(final ImageView referent, final int n) {
        super(Uri.EMPTY, n);
        Asserts.checkNotNull(referent);
        this.zac = new WeakReference<ImageView>(referent);
    }
    
    public zae(final ImageView referent, final Uri uri) {
        super(uri, 0);
        Asserts.checkNotNull(referent);
        this.zac = new WeakReference<ImageView>(referent);
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof zae)) {
            return false;
        }
        final zae zae = (zae)o;
        final ImageView imageView = this.zac.get();
        final ImageView imageView2 = zae.zac.get();
        return imageView2 != null && imageView != null && Objects.equal(imageView2, imageView);
    }
    
    @Override
    public final int hashCode() {
        return 0;
    }
    
    @Override
    public final void zaa(final Drawable drawable, final boolean b, final boolean b2, final boolean b3) {
        final ImageView imageView = this.zac.get();
        if (imageView != null) {
            if (!b2 && !b3 && imageView instanceof zaj) {
                final zaj zaj = (zaj)imageView;
                throw null;
            }
            int n2;
            final int n = n2 = 0;
            if (!b2) {
                if (b) {
                    n2 = n;
                }
                else {
                    n2 = 1;
                }
            }
            Object imageDrawable = drawable;
            if (n2 != 0) {
                final Drawable drawable2 = imageView.getDrawable();
                Drawable zaa;
                if (drawable2 != null) {
                    zaa = drawable2;
                    if (drawable2 instanceof zai) {
                        zaa = ((zai)drawable2).zaa();
                    }
                }
                else {
                    zaa = null;
                }
                imageDrawable = new zai(zaa, drawable);
            }
            imageView.setImageDrawable((Drawable)imageDrawable);
            if (imageView instanceof zaj) {
                final zaj zaj2 = (zaj)imageView;
                throw null;
            }
            if (imageDrawable != null && n2 != 0) {
                ((zai)imageDrawable).zab(250);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.images;

import android.graphics.drawable.Drawable;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Asserts;
import android.net.Uri;
import java.lang.ref.WeakReference;

public final class zaf extends zag
{
    private final WeakReference<ImageManager.OnImageLoadedListener> zac;
    
    public zaf(final ImageManager.OnImageLoadedListener referent, final Uri uri) {
        super(uri, 0);
        Asserts.checkNotNull(referent);
        this.zac = new WeakReference<ImageManager.OnImageLoadedListener>(referent);
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof zaf)) {
            return false;
        }
        final zaf zaf = (zaf)o;
        final ImageManager.OnImageLoadedListener onImageLoadedListener = this.zac.get();
        final ImageManager.OnImageLoadedListener onImageLoadedListener2 = zaf.zac.get();
        return onImageLoadedListener2 != null && onImageLoadedListener != null && Objects.equal(onImageLoadedListener2, onImageLoadedListener) && Objects.equal(zaf.zaa, super.zaa);
    }
    
    @Override
    public final int hashCode() {
        return Objects.hashCode(super.zaa);
    }
    
    @Override
    public final void zaa(final Drawable drawable, final boolean b, final boolean b2, final boolean b3) {
        if (!b2) {
            final ImageManager.OnImageLoadedListener onImageLoadedListener = this.zac.get();
            if (onImageLoadedListener != null) {
                onImageLoadedListener.onImageLoaded(super.zaa.zaa, drawable, b3);
            }
        }
    }
}

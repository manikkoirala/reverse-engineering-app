// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.images;

import java.util.ArrayList;
import android.os.SystemClock;
import com.google.android.gms.common.internal.Asserts;
import java.util.concurrent.CountDownLatch;
import android.graphics.Bitmap;
import android.net.Uri;

final class zac implements Runnable
{
    final ImageManager zaa;
    private final Uri zab;
    private final Bitmap zac;
    private final CountDownLatch zad;
    
    public zac(final ImageManager zaa, final Uri zab, final Bitmap zac, final boolean b, final CountDownLatch zad) {
        this.zaa = zaa;
        this.zab = zab;
        this.zac = zac;
        this.zad = zad;
    }
    
    @Override
    public final void run() {
        Asserts.checkMainThread("OnBitmapLoadedRunnable must be executed in the main thread");
        final Bitmap zac = this.zac;
        final ImageManager.ImageReceiver imageReceiver = ImageManager.zah(this.zaa).remove(this.zab);
        if (imageReceiver != null) {
            final ArrayList zaa = ImageManager.ImageReceiver.zaa(imageReceiver);
            for (int size = zaa.size(), i = 0; i < size; ++i) {
                final zag zag = zaa.get(i);
                final Bitmap zac2 = this.zac;
                if (zac2 != null && zac != null) {
                    zag.zac(ImageManager.zaa(this.zaa), zac2, false);
                }
                else {
                    ImageManager.zaf(this.zaa).put(this.zab, SystemClock.elapsedRealtime());
                    final ImageManager zaa2 = this.zaa;
                    zag.zab(ImageManager.zaa(zaa2), ImageManager.zac(zaa2), false);
                }
                if (!(zag instanceof zaf)) {
                    ImageManager.zag(this.zaa).remove(zag);
                }
            }
        }
        this.zad.countDown();
        synchronized (ImageManager.zad()) {
            ImageManager.zae().remove(this.zab);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.images;

public final class Size
{
    private final int zaa;
    private final int zab;
    
    public Size(final int zaa, final int zab) {
        this.zaa = zaa;
        this.zab = zab;
    }
    
    public static Size parseSize(final String s) {
        if (s != null) {
            int endIndex;
            if ((endIndex = s.indexOf(42)) < 0) {
                endIndex = s.indexOf(120);
            }
            if (endIndex >= 0) {
                try {
                    return new Size(Integer.parseInt(s.substring(0, endIndex)), Integer.parseInt(s.substring(endIndex + 1)));
                }
                catch (final NumberFormatException ex) {
                    throw zaa(s);
                }
            }
            throw zaa(s);
        }
        throw new IllegalArgumentException("string must not be null");
    }
    
    private static NumberFormatException zaa(final String str) {
        final StringBuilder sb = new StringBuilder(str.length() + 16);
        sb.append("Invalid Size: \"");
        sb.append(str);
        sb.append("\"");
        throw new NumberFormatException(sb.toString());
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == null) {
            return false;
        }
        if (this == o) {
            return true;
        }
        if (o instanceof Size) {
            final Size size = (Size)o;
            if (this.zaa == size.zaa && this.zab == size.zab) {
                return true;
            }
        }
        return false;
    }
    
    public int getHeight() {
        return this.zab;
    }
    
    public int getWidth() {
        return this.zaa;
    }
    
    @Override
    public int hashCode() {
        final int zab = this.zab;
        final int zaa = this.zaa;
        return zab ^ (zaa >>> 16 | zaa << 16);
    }
    
    @Override
    public String toString() {
        final int zaa = this.zaa;
        final int zab = this.zab;
        final StringBuilder sb = new StringBuilder(23);
        sb.append(zaa);
        sb.append("x");
        sb.append(zab);
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.images;

import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.content.Intent;
import android.os.ParcelFileDescriptor;
import android.os.Bundle;
import java.util.ArrayList;
import com.google.android.gms.common.annotation.KeepName;
import android.os.ResultReceiver;
import com.google.android.gms.common.internal.Asserts;
import android.widget.ImageView;
import java.util.HashMap;
import com.google.android.gms.internal.base.zap;
import com.google.android.gms.internal.base.zaq;
import android.os.Looper;
import java.util.Map;
import com.google.android.gms.internal.base.zak;
import java.util.concurrent.ExecutorService;
import android.os.Handler;
import android.content.Context;
import android.net.Uri;
import java.util.HashSet;

public final class ImageManager
{
    private static final Object zaa;
    private static HashSet<Uri> zab;
    private static ImageManager zac;
    private final Context zad;
    private final Handler zae;
    private final ExecutorService zaf;
    private final zak zag;
    private final Map<zag, ImageReceiver> zah;
    private final Map<Uri, ImageReceiver> zai;
    private final Map<Uri, Long> zaj;
    
    static {
        zaa = new Object();
        ImageManager.zab = new HashSet<Uri>();
    }
    
    private ImageManager(final Context context, final boolean b) {
        this.zad = context.getApplicationContext();
        this.zae = (Handler)new zaq(Looper.getMainLooper());
        this.zaf = zap.zaa().zab(4, 2);
        this.zag = new zak();
        this.zah = new HashMap<zag, ImageReceiver>();
        this.zai = new HashMap<Uri, ImageReceiver>();
        this.zaj = new HashMap<Uri, Long>();
    }
    
    public static ImageManager create(final Context context) {
        if (ImageManager.zac == null) {
            ImageManager.zac = new ImageManager(context, false);
        }
        return ImageManager.zac;
    }
    
    public void loadImage(final ImageView imageView, final int n) {
        this.zaj(new zae(imageView, n));
    }
    
    public void loadImage(final ImageView imageView, final Uri uri) {
        this.zaj(new zae(imageView, uri));
    }
    
    public void loadImage(final ImageView imageView, final Uri uri, final int zab) {
        final zae zae = new zae(imageView, uri);
        zae.zab = zab;
        this.zaj(zae);
    }
    
    public void loadImage(final OnImageLoadedListener onImageLoadedListener, final Uri uri) {
        this.zaj(new zaf(onImageLoadedListener, uri));
    }
    
    public void loadImage(final OnImageLoadedListener onImageLoadedListener, final Uri uri, final int zab) {
        final zaf zaf = new zaf(onImageLoadedListener, uri);
        zaf.zab = zab;
        this.zaj(zaf);
    }
    
    public final void zaj(final zag zag) {
        Asserts.checkMainThread("ImageManager.loadImage() must be called in the main thread");
        new zab(this, zag).run();
    }
    
    @KeepName
    public final class ImageReceiver extends ResultReceiver
    {
        final ImageManager zaa;
        private final Uri zab;
        private final ArrayList<zag> zac;
        
        public ImageReceiver(final ImageManager zaa, final Uri zab) {
            this.zaa = zaa;
            super((Handler)new zaq(Looper.getMainLooper()));
            this.zab = zab;
            this.zac = new ArrayList<zag>();
        }
        
        public final void onReceiveResult(final int n, final Bundle bundle) {
            final ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)bundle.getParcelable("com.google.android.gms.extra.fileDescriptor");
            final ImageManager zaa = this.zaa;
            ImageManager.zai(zaa).execute(new zaa(zaa, this.zab, parcelFileDescriptor));
        }
        
        public final void zab(final zag e) {
            Asserts.checkMainThread("ImageReceiver.addImageRequest() must be called in the main thread");
            this.zac.add(e);
        }
        
        public final void zac(final zag o) {
            Asserts.checkMainThread("ImageReceiver.removeImageRequest() must be called in the main thread");
            this.zac.remove(o);
        }
        
        public final void zad() {
            final Intent intent = new Intent("com.google.android.gms.common.images.LOAD_IMAGE");
            intent.setPackage("com.google.android.gms");
            intent.putExtra("com.google.android.gms.extras.uri", (Parcelable)this.zab);
            intent.putExtra("com.google.android.gms.extras.resultReceiver", (Parcelable)this);
            intent.putExtra("com.google.android.gms.extras.priority", 3);
            ImageManager.zaa(this.zaa).sendBroadcast(intent);
        }
    }
    
    public interface OnImageLoadedListener
    {
        void onImageLoaded(final Uri p0, final Drawable p1, final boolean p2);
    }
}

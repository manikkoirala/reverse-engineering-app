// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.images;

import com.google.android.gms.common.internal.Objects;
import android.net.Uri;

final class zad
{
    public final Uri zaa;
    
    public zad(final Uri zaa) {
        this.zaa = zaa;
    }
    
    @Override
    public final boolean equals(final Object o) {
        return this == o || (o instanceof zad && Objects.equal(((zad)o).zaa, this.zaa));
    }
    
    @Override
    public final int hashCode() {
        return Objects.hashCode(this.zaa);
    }
}

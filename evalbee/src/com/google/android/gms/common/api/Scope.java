// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "ScopeCreator")
public final class Scope extends AbstractSafeParcelable implements ReflectedParcelable
{
    public static final Parcelable$Creator<Scope> CREATOR;
    @VersionField(id = 1)
    final int zza;
    @Field(getter = "getScopeUri", id = 2)
    private final String zzb;
    
    static {
        CREATOR = (Parcelable$Creator)new zza();
    }
    
    @Constructor
    public Scope(@Param(id = 1) final int zza, @Param(id = 2) final String zzb) {
        Preconditions.checkNotEmpty(zzb, "scopeUri must not be null or empty");
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public Scope(final String s) {
        this(1, s);
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof Scope && this.zzb.equals(((Scope)o).zzb));
    }
    
    @KeepForSdk
    public String getScopeUri() {
        return this.zzb;
    }
    
    @Override
    public int hashCode() {
        return this.zzb.hashCode();
    }
    
    @Override
    public String toString() {
        return this.zzb;
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.zza);
        SafeParcelWriter.writeString(parcel, 2, this.getScopeUri(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

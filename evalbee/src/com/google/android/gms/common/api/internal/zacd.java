// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.MethodInvocation;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.common.util.ArrayUtils;
import com.google.android.gms.common.internal.ConnectionTelemetryConfiguration;
import com.google.android.gms.common.internal.RootTelemetryConfiguration;
import android.os.SystemClock;
import com.google.android.gms.common.internal.BaseGmsClient;
import com.google.android.gms.common.internal.RootTelemetryConfigManager;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.tasks.OnCompleteListener;

final class zacd<T> implements OnCompleteListener<T>
{
    private final GoogleApiManager zaa;
    private final int zab;
    private final ApiKey<?> zac;
    private final long zad;
    private final long zae;
    
    @VisibleForTesting
    public zacd(final GoogleApiManager zaa, final int zab, final ApiKey<?> zac, final long zad, final long zae, final String s, final String s2) {
        this.zaa = zaa;
        this.zab = zab;
        this.zac = zac;
        this.zad = zad;
        this.zae = zae;
    }
    
    public static <T> zacd<T> zaa(final GoogleApiManager googleApiManager, final int n, final ApiKey<?> apiKey) {
        if (!googleApiManager.zaF()) {
            return null;
        }
        final RootTelemetryConfiguration config = RootTelemetryConfigManager.getInstance().getConfig();
        boolean methodTimingTelemetryEnabled2;
        if (config != null) {
            if (!config.getMethodInvocationTelemetryEnabled()) {
                return null;
            }
            final boolean methodTimingTelemetryEnabled = config.getMethodTimingTelemetryEnabled();
            final zabq zak = googleApiManager.zak(apiKey);
            methodTimingTelemetryEnabled2 = methodTimingTelemetryEnabled;
            if (zak != null) {
                if (!(zak.zaf() instanceof BaseGmsClient)) {
                    return null;
                }
                final BaseGmsClient baseGmsClient = (BaseGmsClient)zak.zaf();
                methodTimingTelemetryEnabled2 = methodTimingTelemetryEnabled;
                if (baseGmsClient.hasConnectionInfo()) {
                    methodTimingTelemetryEnabled2 = methodTimingTelemetryEnabled;
                    if (!baseGmsClient.isConnecting()) {
                        final ConnectionTelemetryConfiguration zab = zab(zak, baseGmsClient, n);
                        if (zab == null) {
                            return null;
                        }
                        zak.zaq();
                        methodTimingTelemetryEnabled2 = zab.getMethodTimingTelemetryEnabled();
                    }
                }
            }
        }
        else {
            methodTimingTelemetryEnabled2 = true;
        }
        long currentTimeMillis;
        if (methodTimingTelemetryEnabled2) {
            currentTimeMillis = System.currentTimeMillis();
        }
        else {
            currentTimeMillis = 0L;
        }
        long elapsedRealtime;
        if (methodTimingTelemetryEnabled2) {
            elapsedRealtime = SystemClock.elapsedRealtime();
        }
        else {
            elapsedRealtime = 0L;
        }
        return new zacd<T>(googleApiManager, n, apiKey, currentTimeMillis, elapsedRealtime, null, null);
    }
    
    private static ConnectionTelemetryConfiguration zab(final zabq<?> zabq, final BaseGmsClient<?> baseGmsClient, final int n) {
        final ConnectionTelemetryConfiguration telemetryConfiguration = baseGmsClient.getTelemetryConfiguration();
        if (telemetryConfiguration != null && telemetryConfiguration.getMethodInvocationTelemetryEnabled()) {
            final int[] methodInvocationMethodKeyAllowlist = telemetryConfiguration.getMethodInvocationMethodKeyAllowlist();
            if (methodInvocationMethodKeyAllowlist == null) {
                final int[] methodInvocationMethodKeyDisallowlist = telemetryConfiguration.getMethodInvocationMethodKeyDisallowlist();
                if (methodInvocationMethodKeyDisallowlist != null) {
                    if (ArrayUtils.contains(methodInvocationMethodKeyDisallowlist, n)) {
                        return null;
                    }
                }
            }
            else if (!ArrayUtils.contains(methodInvocationMethodKeyAllowlist, n)) {
                return null;
            }
            if (zabq.zac() < telemetryConfiguration.getMaxMethodInvocationsLogged()) {
                return telemetryConfiguration;
            }
        }
        return null;
    }
    
    public final void onComplete(final Task<T> task) {
        if (!this.zaa.zaF()) {
            return;
        }
        final RootTelemetryConfiguration config = RootTelemetryConfigManager.getInstance().getConfig();
        if (config != null && !config.getMethodInvocationTelemetryEnabled()) {
            return;
        }
        final zabq zak = this.zaa.zak(this.zac);
        if (zak != null) {
            if (zak.zaf() instanceof BaseGmsClient) {
                final BaseGmsClient baseGmsClient = (BaseGmsClient)zak.zaf();
                final long zad = this.zad;
                final int n = 1;
                final int n2 = 0;
                int n3;
                if (zad > 0L) {
                    n3 = 1;
                }
                else {
                    n3 = 0;
                }
                final int gCoreServiceId = baseGmsClient.getGCoreServiceId();
                int batchPeriodMillis;
                int version;
                int n5;
                int n6;
                if (config != null) {
                    final boolean b = (n3 & (config.getMethodTimingTelemetryEnabled() ? 1 : 0)) != 0x0;
                    batchPeriodMillis = config.getBatchPeriodMillis();
                    final int maxMethodInvocationsInBatch = config.getMaxMethodInvocationsInBatch();
                    version = config.getVersion();
                    int n4 = b ? 1 : 0;
                    int maxMethodInvocationsLogged = maxMethodInvocationsInBatch;
                    if (baseGmsClient.hasConnectionInfo()) {
                        n4 = (b ? 1 : 0);
                        maxMethodInvocationsLogged = maxMethodInvocationsInBatch;
                        if (!baseGmsClient.isConnecting()) {
                            final ConnectionTelemetryConfiguration zab = zab(zak, baseGmsClient, this.zab);
                            if (zab == null) {
                                return;
                            }
                            if (zab.getMethodTimingTelemetryEnabled() && this.zad > 0L) {
                                n4 = n;
                            }
                            else {
                                n4 = 0;
                            }
                            maxMethodInvocationsLogged = zab.getMaxMethodInvocationsLogged();
                        }
                    }
                    n5 = maxMethodInvocationsLogged;
                    n6 = n4;
                }
                else {
                    version = 0;
                    n5 = 100;
                    batchPeriodMillis = 5000;
                    n6 = n3;
                }
                final GoogleApiManager zaa = this.zaa;
                int n7 = 0;
                int n8 = 0;
                Label_0343: {
                    if (task.isSuccessful()) {
                        n7 = 0;
                        n8 = n2;
                    }
                    else {
                        if (task.isCanceled()) {
                            n8 = 100;
                        }
                        else {
                            final Exception exception = task.getException();
                            if (exception instanceof ApiException) {
                                final Status status = ((ApiException)exception).getStatus();
                                final int statusCode = status.getStatusCode();
                                final ConnectionResult connectionResult = status.getConnectionResult();
                                int errorCode;
                                if (connectionResult == null) {
                                    errorCode = -1;
                                }
                                else {
                                    errorCode = connectionResult.getErrorCode();
                                }
                                n7 = errorCode;
                                n8 = statusCode;
                                break Label_0343;
                            }
                            n8 = 101;
                        }
                        n7 = -1;
                    }
                }
                long zad2;
                long currentTimeMillis;
                int n9;
                if (n6 != 0) {
                    zad2 = this.zad;
                    currentTimeMillis = System.currentTimeMillis();
                    n9 = (int)(SystemClock.elapsedRealtime() - this.zae);
                }
                else {
                    zad2 = 0L;
                    currentTimeMillis = 0L;
                    n9 = -1;
                }
                zaa.zay(new MethodInvocation(this.zab, n8, n7, zad2, currentTimeMillis, null, null, gCoreServiceId, n9), version, batchPeriodMillis, n5);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.Feature;

final class zacv extends TaskApiCall
{
    final Builder zaa;
    
    public zacv(final Builder zaa, final Feature[] array, final boolean b, final int n) {
        this.zaa = zaa;
        super(array, b, n);
    }
    
    @Override
    public final void doExecute(final Api.AnyClient anyClient, final TaskCompletionSource taskCompletionSource) {
        Builder.zaa((Builder<Api.AnyClient, Object>)this.zaa).accept(anyClient, taskCompletionSource);
    }
}

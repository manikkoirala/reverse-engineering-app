// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;

final class zaam extends zabg
{
    final ConnectionResult zaa;
    final zaao zab;
    
    public zaam(final zaao zab, final zabf zabf, final ConnectionResult zaa) {
        this.zab = zab;
        this.zaa = zaa;
        super(zabf);
    }
    
    @Override
    public final void zaa() {
        zaaw.zas(this.zab.zaa, this.zaa);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import android.os.Handler;
import android.content.Intent;
import android.app.Activity;
import org.jetbrains.annotations.NotNull;
import java.util.Iterator;
import java.io.PrintWriter;
import java.io.FileDescriptor;
import com.google.android.gms.internal.common.zzi;
import android.os.Looper;
import java.lang.ref.WeakReference;
import androidx.fragment.app.e;
import java.util.Collections;
import android.os.Bundle;
import java.util.Map;
import java.util.WeakHashMap;
import androidx.fragment.app.Fragment;

public final class zzd extends Fragment implements LifecycleFragment
{
    private static final WeakHashMap zza;
    private final Map zzb;
    private int zzc;
    private Bundle zzd;
    
    static {
        zza = new WeakHashMap();
    }
    
    public zzd() {
        this.zzb = Collections.synchronizedMap((Map<Object, Object>)new r8());
        this.zzc = 0;
    }
    
    public static zzd zzc(final e e) {
        final WeakHashMap zza = zzd.zza;
        final WeakReference weakReference = zza.get(e);
        if (weakReference != null) {
            final zzd zzd = (zzd)weakReference.get();
            if (zzd != null) {
                return zzd;
            }
        }
        try {
            final zzd zzd2 = (zzd)e.getSupportFragmentManager().i0("SupportLifecycleFragmentImpl");
            zzd referent = null;
            Label_0085: {
                if (zzd2 != null) {
                    referent = zzd2;
                    if (!zzd2.isRemoving()) {
                        break Label_0085;
                    }
                }
                referent = new zzd();
                e.getSupportFragmentManager().m().d(referent, "SupportLifecycleFragmentImpl").h();
            }
            zza.put(e, new WeakReference(referent));
            return referent;
        }
        catch (final ClassCastException cause) {
            throw new IllegalStateException("Fragment with tag SupportLifecycleFragmentImpl is not a SupportLifecycleFragmentImpl", cause);
        }
    }
    
    @Override
    public final void addCallback(final String str, final LifecycleCallback lifecycleCallback) {
        if (!this.zzb.containsKey(str)) {
            this.zzb.put(str, lifecycleCallback);
            if (this.zzc > 0) {
                ((Handler)new zzi(Looper.getMainLooper())).post((Runnable)new zzc(this, lifecycleCallback, str));
            }
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("LifecycleCallback with tag ");
        sb.append(str);
        sb.append(" already added to this fragment.");
        throw new IllegalArgumentException(sb.toString());
    }
    
    @Override
    public final void dump(final String s, final FileDescriptor fileDescriptor, final PrintWriter printWriter, final String[] array) {
        super.dump(s, fileDescriptor, printWriter, array);
        final Iterator iterator = this.zzb.values().iterator();
        while (iterator.hasNext()) {
            ((LifecycleCallback)iterator.next()).dump(s, fileDescriptor, printWriter, array);
        }
    }
    
    @Override
    public final <T extends LifecycleCallback> T getCallbackOrNull(final String s, final Class<T> clazz) {
        return clazz.cast(this.zzb.get(s));
    }
    
    @Override
    public final boolean isCreated() {
        return this.zzc > 0;
    }
    
    @Override
    public final boolean isStarted() {
        return this.zzc >= 2;
    }
    
    @Override
    public final void onActivityResult(final int n, final int n2, final Intent intent) {
        super.onActivityResult(n, n2, intent);
        final Iterator iterator = this.zzb.values().iterator();
        while (iterator.hasNext()) {
            ((LifecycleCallback)iterator.next()).onActivityResult(n, n2, intent);
        }
    }
    
    @Override
    public final void onCreate(final Bundle zzd) {
        super.onCreate(zzd);
        this.zzc = 1;
        this.zzd = zzd;
        for (final Map.Entry<K, LifecycleCallback> entry : this.zzb.entrySet()) {
            final LifecycleCallback lifecycleCallback = entry.getValue();
            Bundle bundle;
            if (zzd != null) {
                bundle = zzd.getBundle((String)entry.getKey());
            }
            else {
                bundle = null;
            }
            lifecycleCallback.onCreate(bundle);
        }
    }
    
    @Override
    public final void onDestroy() {
        super.onDestroy();
        this.zzc = 5;
        final Iterator iterator = this.zzb.values().iterator();
        while (iterator.hasNext()) {
            ((LifecycleCallback)iterator.next()).onDestroy();
        }
    }
    
    @Override
    public final void onResume() {
        super.onResume();
        this.zzc = 3;
        final Iterator iterator = this.zzb.values().iterator();
        while (iterator.hasNext()) {
            ((LifecycleCallback)iterator.next()).onResume();
        }
    }
    
    @Override
    public final void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (bundle == null) {
            return;
        }
        for (final Map.Entry<K, LifecycleCallback> entry : this.zzb.entrySet()) {
            final Bundle bundle2 = new Bundle();
            entry.getValue().onSaveInstanceState(bundle2);
            bundle.putBundle((String)entry.getKey(), bundle2);
        }
    }
    
    @Override
    public final void onStart() {
        super.onStart();
        this.zzc = 2;
        final Iterator iterator = this.zzb.values().iterator();
        while (iterator.hasNext()) {
            ((LifecycleCallback)iterator.next()).onStart();
        }
    }
    
    @Override
    public final void onStop() {
        super.onStop();
        this.zzc = 4;
        final Iterator iterator = this.zzb.values().iterator();
        while (iterator.hasNext()) {
            ((LifecycleCallback)iterator.next()).onStop();
        }
    }
}

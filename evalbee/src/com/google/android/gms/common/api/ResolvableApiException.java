// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api;

import android.app.Activity;
import android.app.PendingIntent;

public class ResolvableApiException extends ApiException
{
    public ResolvableApiException(final Status status) {
        super(status);
    }
    
    public PendingIntent getResolution() {
        return this.getStatus().getResolution();
    }
    
    public void startResolutionForResult(final Activity activity, final int n) {
        this.getStatus().startResolutionForResult(activity, n);
    }
}

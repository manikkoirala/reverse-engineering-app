// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import android.os.Parcelable$Creator;
import com.google.android.gms.internal.base.zac;
import android.os.Parcel;
import android.os.IBinder;
import com.google.android.gms.internal.base.zab;
import com.google.android.gms.common.api.Status;
import android.os.IInterface;

public interface IStatusCallback extends IInterface
{
    void onResult(final Status p0);
    
    public abstract static class Stub extends zab implements IStatusCallback
    {
        public Stub() {
            super("com.google.android.gms.common.api.internal.IStatusCallback");
        }
        
        public static IStatusCallback asInterface(final IBinder binder) {
            if (binder == null) {
                return null;
            }
            final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.common.api.internal.IStatusCallback");
            if (queryLocalInterface instanceof IStatusCallback) {
                return (IStatusCallback)queryLocalInterface;
            }
            return new zaby(binder);
        }
        
        public final boolean zaa(final int n, final Parcel parcel, final Parcel parcel2, final int n2) {
            if (n == 1) {
                this.onResult((Status)zac.zaa(parcel, (Parcelable$Creator)Status.CREATOR));
                return true;
            }
            return false;
        }
    }
}

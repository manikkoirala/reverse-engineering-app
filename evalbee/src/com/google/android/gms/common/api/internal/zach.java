// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.GoogleApi;

public final class zach
{
    public final zai zaa;
    public final int zab;
    public final GoogleApi<?> zac;
    
    public zach(final zai zaa, final int zab, final GoogleApi<?> zac) {
        this.zaa = zaa;
        this.zab = zab;
        this.zac = zac;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import java.lang.ref.WeakReference;

public final class zab extends ActivityLifecycleObserver
{
    private final WeakReference<zaa> zaa;
    
    public zab(final zaa referent) {
        this.zaa = new WeakReference<zaa>(referent);
    }
    
    @Override
    public final ActivityLifecycleObserver onStopCallOnce(final Runnable runnable) {
        final zaa zaa = this.zaa.get();
        if (zaa != null) {
            com.google.android.gms.common.api.internal.zaa.zab(zaa, runnable);
            return this;
        }
        throw new IllegalStateException("The target activity has already been GC'd");
    }
}

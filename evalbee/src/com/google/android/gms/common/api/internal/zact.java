// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import android.os.Looper;
import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;
import android.util.Log;
import com.google.android.gms.common.internal.zav;
import com.google.android.gms.signin.internal.zak;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.signin.zad;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.common.api.Scope;
import java.util.Set;
import android.os.Handler;
import android.content.Context;
import com.google.android.gms.signin.SignInOptions;
import com.google.android.gms.signin.zae;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.signin.internal.zac;

public final class zact extends zac implements ConnectionCallbacks, OnConnectionFailedListener
{
    private static final Api.AbstractClientBuilder<? extends zae, SignInOptions> zaa;
    private final Context zab;
    private final Handler zac;
    private final Api.AbstractClientBuilder<? extends zae, SignInOptions> zad;
    private final Set<Scope> zae;
    private final ClientSettings zaf;
    private zae zag;
    private zacs zah;
    
    static {
        zaa = zad.zac;
    }
    
    public zact(final Context zab, final Handler zac, final ClientSettings clientSettings) {
        final Api.AbstractClientBuilder<? extends zae, SignInOptions> zaa = zact.zaa;
        this.zab = zab;
        this.zac = zac;
        this.zaf = Preconditions.checkNotNull(clientSettings, "ClientSettings must not be null");
        this.zae = clientSettings.getRequiredScopes();
        this.zad = zaa;
    }
    
    public final void onConnected(final Bundle bundle) {
        this.zag.zad((com.google.android.gms.signin.internal.zae)this);
    }
    
    public final void onConnectionFailed(final ConnectionResult connectionResult) {
        this.zah.zae(connectionResult);
    }
    
    public final void onConnectionSuspended(final int n) {
        ((Api.Client)this.zag).disconnect();
    }
    
    public final void zab(final zak zak) {
        this.zac.post((Runnable)new zacr(this, zak));
    }
    
    public final void zae(final zacs zah) {
        final zae zag = this.zag;
        if (zag != null) {
            ((Api.Client)zag).disconnect();
        }
        this.zaf.zae(System.identityHashCode(this));
        final Api.AbstractClientBuilder<? extends zae, SignInOptions> zad = this.zad;
        final Context zab = this.zab;
        final Looper looper = this.zac.getLooper();
        final ClientSettings zaf = this.zaf;
        this.zag = (zae)zad.buildClient(zab, looper, zaf, zaf.zaa(), this, this);
        this.zah = zah;
        final Set<Scope> zae = this.zae;
        if (zae != null && !zae.isEmpty()) {
            this.zag.zab();
            return;
        }
        this.zac.post((Runnable)new zacq(this));
    }
    
    public final void zaf() {
        final zae zag = this.zag;
        if (zag != null) {
            ((Api.Client)zag).disconnect();
        }
    }
}

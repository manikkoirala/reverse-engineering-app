// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import android.os.Bundle;
import java.util.Iterator;
import java.util.Collections;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Api;
import org.checkerframework.checker.initialization.qual.NotOnlyInitialized;

public final class zaax implements zabf
{
    @NotOnlyInitialized
    private final zabi zaa;
    
    public zaax(final zabi zaa) {
        this.zaa = zaa;
    }
    
    @Override
    public final <A extends Api.AnyClient, R extends Result, T extends BaseImplementation.ApiMethodImpl<R, A>> T zaa(final T t) {
        this.zaa.zag.zaa.add(t);
        return t;
    }
    
    @Override
    public final <A extends Api.AnyClient, T extends BaseImplementation.ApiMethodImpl<? extends Result, A>> T zab(final T t) {
        throw new IllegalStateException("GoogleApiClient is not connected yet.");
    }
    
    @Override
    public final void zad() {
        final Iterator<Api.Client> iterator = this.zaa.zaa.values().iterator();
        while (iterator.hasNext()) {
            iterator.next().disconnect();
        }
        this.zaa.zag.zad = Collections.emptySet();
    }
    
    @Override
    public final void zae() {
        this.zaa.zaj();
    }
    
    @Override
    public final void zag(final Bundle bundle) {
    }
    
    @Override
    public final void zah(final ConnectionResult connectionResult, final Api<?> api, final boolean b) {
    }
    
    @Override
    public final void zai(final int n) {
    }
    
    @Override
    public final boolean zaj() {
        return true;
    }
}

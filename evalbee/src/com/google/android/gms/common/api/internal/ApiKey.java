// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.api.Api;

public final class ApiKey<O extends Api.ApiOptions>
{
    private final int zaa;
    private final Api<O> zab;
    private final O zac;
    private final String zad;
    
    private ApiKey(final Api<O> zab, final O zac, final String zad) {
        this.zab = zab;
        this.zac = zac;
        this.zad = zad;
        this.zaa = Objects.hashCode(zab, zac, zad);
    }
    
    public static <O extends Api.ApiOptions> ApiKey<O> zaa(final Api<O> api, final O o, final String s) {
        return new ApiKey<O>(api, o, s);
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        if (!(o instanceof ApiKey)) {
            return false;
        }
        final ApiKey apiKey = (ApiKey)o;
        return Objects.equal(this.zab, apiKey.zab) && Objects.equal(this.zac, apiKey.zac) && Objects.equal(this.zad, apiKey.zad);
    }
    
    @Override
    public final int hashCode() {
        return this.zaa;
    }
    
    public final String zab() {
        return this.zab.zad();
    }
}

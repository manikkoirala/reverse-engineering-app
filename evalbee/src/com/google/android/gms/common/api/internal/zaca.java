// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import java.io.PrintWriter;
import java.io.FileDescriptor;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Api;
import java.util.concurrent.TimeUnit;
import com.google.android.gms.common.ConnectionResult;

public interface zaca
{
    ConnectionResult zab();
    
    ConnectionResult zac(final long p0, final TimeUnit p1);
    
    ConnectionResult zad(final Api<?> p0);
    
     <A extends Api.AnyClient, R extends Result, T extends BaseImplementation.ApiMethodImpl<R, A>> T zae(final T p0);
    
     <A extends Api.AnyClient, T extends BaseImplementation.ApiMethodImpl<? extends Result, A>> T zaf(final T p0);
    
    void zaq();
    
    void zar();
    
    void zas(final String p0, final FileDescriptor p1, final PrintWriter p2, final String[] p3);
    
    void zat();
    
    void zau();
    
    boolean zaw();
    
    boolean zax();
    
    boolean zay(final SignInConnectionListener p0);
}

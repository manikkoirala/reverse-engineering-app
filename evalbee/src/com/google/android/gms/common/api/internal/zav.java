// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

final class zav implements Runnable
{
    final zaaa zaa;
    
    public zav(final zaaa zaa) {
        this.zaa = zaa;
    }
    
    @Override
    public final void run() {
        zaaa.zaj(this.zaa).lock();
        try {
            zaaa.zap(this.zaa);
        }
        finally {
            zaaa.zaj(this.zaa).unlock();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.TaskCompletionSource;

abstract class zad<T> extends zac
{
    protected final TaskCompletionSource<T> zaa;
    
    public zad(final int n, final TaskCompletionSource<T> zaa) {
        super(n);
        this.zaa = zaa;
    }
    
    public abstract void zac(final zabq<?> p0);
    
    @Override
    public final void zad(final Status status) {
        this.zaa.trySetException((Exception)new ApiException(status));
    }
    
    @Override
    public final void zae(final Exception ex) {
        this.zaa.trySetException(ex);
    }
    
    @Override
    public final void zaf(final zabq<?> zabq) {
        try {
            this.zac(zabq);
        }
        catch (final RuntimeException ex) {
            this.zaa.trySetException((Exception)ex);
        }
        catch (final RemoteException ex2) {
            this.zad(zai.zah(ex2));
        }
        catch (final DeadObjectException ex3) {
            this.zad(zai.zah((RemoteException)ex3));
            throw ex3;
        }
    }
    
    @Override
    public void zag(final zaad zaad, final boolean b) {
    }
}

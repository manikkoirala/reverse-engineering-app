// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.Preconditions;
import android.app.Activity;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.common.GoogleApiAvailability;

public final class zaae extends zap
{
    private final s8 zad;
    private final GoogleApiManager zae;
    
    @VisibleForTesting
    public zaae(final LifecycleFragment lifecycleFragment, final GoogleApiManager zae, final GoogleApiAvailability googleApiAvailability) {
        super(lifecycleFragment, googleApiAvailability);
        this.zad = new s8();
        this.zae = zae;
        super.mLifecycleFragment.addCallback("ConnectionlessLifecycleHelper", this);
    }
    
    public static void zad(final Activity activity, final GoogleApiManager googleApiManager, final ApiKey<?> apiKey) {
        final LifecycleFragment fragment = LifecycleCallback.getFragment(activity);
        zaae zaae;
        if ((zaae = fragment.getCallbackOrNull("ConnectionlessLifecycleHelper", zaae.class)) == null) {
            zaae = new zaae(fragment, googleApiManager, GoogleApiAvailability.getInstance());
        }
        Preconditions.checkNotNull(apiKey, "ApiKey cannot be null");
        zaae.zad.add(apiKey);
        googleApiManager.zaC(zaae);
    }
    
    private final void zae() {
        if (!this.zad.isEmpty()) {
            this.zae.zaC(this);
        }
    }
    
    @Override
    public final void onResume() {
        super.onResume();
        this.zae();
    }
    
    @Override
    public final void onStart() {
        super.onStart();
        this.zae();
    }
    
    @Override
    public final void onStop() {
        super.onStop();
        this.zae.zaD(this);
    }
    
    public final s8 zaa() {
        return this.zad;
    }
    
    @Override
    public final void zab(final ConnectionResult connectionResult, final int n) {
        this.zae.zaz(connectionResult, n);
    }
    
    @Override
    public final void zac() {
        this.zae.zaA();
    }
}

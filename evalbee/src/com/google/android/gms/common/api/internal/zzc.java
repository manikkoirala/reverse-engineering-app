// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import android.os.Bundle;

final class zzc implements Runnable
{
    final LifecycleCallback zza;
    final String zzb;
    final zzd zzc;
    
    public zzc(final zzd zzc, final LifecycleCallback zza, final String zzb) {
        this.zzc = zzc;
        this.zza = zza;
        this.zzb = zzb;
    }
    
    @Override
    public final void run() {
        final zzd zzc = this.zzc;
        if (zzd.zza(zzc) > 0) {
            final LifecycleCallback zza = this.zza;
            Bundle bundle;
            if (zzd.zzb(zzc) != null) {
                bundle = zzd.zzb(zzc).getBundle(this.zzb);
            }
            else {
                bundle = null;
            }
            zza.onCreate(bundle);
        }
        if (zzd.zza(this.zzc) >= 2) {
            this.zza.onStart();
        }
        if (zzd.zza(this.zzc) >= 3) {
            this.zza.onResume();
        }
        if (zzd.zza(this.zzc) >= 4) {
            this.zza.onStop();
        }
        if (zzd.zza(this.zzc) >= 5) {
            this.zza.onDestroy();
        }
    }
}

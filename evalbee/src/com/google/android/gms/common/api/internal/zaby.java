// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Status;
import android.os.IBinder;
import com.google.android.gms.internal.base.zaa;

public final class zaby extends zaa implements IStatusCallback
{
    public zaby(final IBinder binder) {
        super(binder, "com.google.android.gms.common.api.internal.IStatusCallback");
    }
    
    public final void onResult(final Status status) {
        throw null;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api;

final class zab implements StatusListener
{
    final Batch zaa;
    
    public zab(final Batch zaa) {
        this.zaa = zaa;
    }
    
    @Override
    public final void onComplete(Status result_SUCCESS) {
        synchronized (Batch.zab(this.zaa)) {
            if (this.zaa.isCanceled()) {
                return;
            }
            if (result_SUCCESS.isCanceled()) {
                Batch.zad(this.zaa, true);
            }
            else if (!result_SUCCESS.isSuccess()) {
                Batch.zac(this.zaa, true);
            }
            final Batch zaa = this.zaa;
            Batch.zae(zaa, Batch.zaa(zaa) - 1);
            final Batch zaa2 = this.zaa;
            if (Batch.zaa(zaa2) == 0) {
                if (Batch.zah(zaa2)) {
                    Batch.zaf(zaa2);
                }
                else {
                    if (Batch.zag(zaa2)) {
                        result_SUCCESS = new Status(13);
                    }
                    else {
                        result_SUCCESS = Status.RESULT_SUCCESS;
                    }
                    final Batch zaa3 = this.zaa;
                    zaa3.setResult(new BatchResult(result_SUCCESS, Batch.zai(zaa3)));
                }
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import android.os.Bundle;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Api;

public interface zabf
{
     <A extends Api.AnyClient, R extends Result, T extends BaseImplementation.ApiMethodImpl<R, A>> T zaa(final T p0);
    
     <A extends Api.AnyClient, T extends BaseImplementation.ApiMethodImpl<? extends Result, A>> T zab(final T p0);
    
    void zad();
    
    void zae();
    
    void zag(final Bundle p0);
    
    void zah(final ConnectionResult p0, final Api<?> p1, final boolean p2);
    
    void zai(final int p0);
    
    boolean zaj();
}

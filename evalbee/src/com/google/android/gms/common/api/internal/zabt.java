// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import android.util.Log;
import com.google.android.gms.common.internal.IAccountAccessor;
import com.google.android.gms.common.ConnectionResult;

final class zabt implements Runnable
{
    final ConnectionResult zaa;
    final zabu zab;
    
    public zabt(final zabu zab, final ConnectionResult zaa) {
        this.zab = zab;
        this.zaa = zaa;
    }
    
    @Override
    public final void run() {
        final zabu zab = this.zab;
        final zabq zabq = GoogleApiManager.zat(zab.zaa).get(zabu.zab(zab));
        if (zabq == null) {
            return;
        }
        if (this.zaa.isSuccess()) {
            zabu.zac(this.zab, true);
            if (zabu.zaa(this.zab).requiresSignIn()) {
                zabu.zad(this.zab);
                return;
            }
            try {
                final zabu zab2 = this.zab;
                zabu.zaa(zab2).getRemoteService(null, zabu.zaa(zab2).getScopesForConnectionlessNonSignIn());
                return;
            }
            catch (final SecurityException ex) {
                Log.e("GoogleApiManager", "Failed to get service from broker. ", (Throwable)ex);
                zabu.zaa(this.zab).disconnect("Failed to get service from broker.");
                zabq.zar(new ConnectionResult(10), null);
                return;
            }
        }
        zabq.zar(this.zaa, null);
    }
}

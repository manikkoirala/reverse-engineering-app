// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import android.app.Dialog;

final class zan extends zabw
{
    final Dialog zaa;
    final zao zab;
    
    public zan(final zao zab, final Dialog zaa) {
        this.zab = zab;
        this.zaa = zaa;
    }
    
    @Override
    public final void zaa() {
        zap.zag(this.zab.zaa);
        if (this.zaa.isShowing()) {
            this.zaa.dismiss();
        }
    }
}

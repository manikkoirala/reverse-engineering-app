// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.internal.BaseGmsClient;

final class zabp implements SignOutCallbacks
{
    final zabq zaa;
    
    public zabp(final zabq zaa) {
        this.zaa = zaa;
    }
    
    @Override
    public final void onSignOutComplete() {
        GoogleApiManager.zaf(this.zaa.zaa).post((Runnable)new zabo(this));
    }
}

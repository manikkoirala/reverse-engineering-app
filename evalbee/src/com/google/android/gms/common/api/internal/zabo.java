// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Api;

final class zabo implements Runnable
{
    final zabp zaa;
    
    public zabo(final zabp zaa) {
        this.zaa = zaa;
    }
    
    @Override
    public final void run() {
        final zabq zaa = this.zaa.zaa;
        zabq.zae((zabq<Api.ApiOptions>)zaa).disconnect(zabq.zae((zabq<Api.ApiOptions>)zaa).getClass().getName().concat(" disconnecting because it was signed out."));
    }
}

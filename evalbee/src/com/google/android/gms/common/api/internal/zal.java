// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.AvailabilityException;
import com.google.android.gms.common.ConnectionResult;
import java.util.Set;
import com.google.android.gms.tasks.Task;
import java.util.Iterator;
import com.google.android.gms.common.api.HasApiKey;
import java.util.Map;
import com.google.android.gms.tasks.TaskCompletionSource;

public final class zal
{
    private final r8 zaa;
    private final r8 zab;
    private final TaskCompletionSource<Map<ApiKey<?>, String>> zac;
    private int zad;
    private boolean zae;
    
    public zal(final Iterable<? extends HasApiKey<?>> iterable) {
        this.zab = new r8();
        this.zac = (TaskCompletionSource<Map<ApiKey<?>, String>>)new TaskCompletionSource();
        this.zae = false;
        this.zaa = new r8();
        final Iterator<? extends HasApiKey<?>> iterator = iterable.iterator();
        while (iterator.hasNext()) {
            this.zaa.put(((HasApiKey)iterator.next()).getApiKey(), null);
        }
        this.zad = this.zaa.keySet().size();
    }
    
    public final Task<Map<ApiKey<?>, String>> zaa() {
        return (Task<Map<ApiKey<?>, String>>)this.zac.getTask();
    }
    
    public final Set<ApiKey<?>> zab() {
        return this.zaa.keySet();
    }
    
    public final void zac(final ApiKey<?> apiKey, final ConnectionResult connectionResult, final String s) {
        this.zaa.put(apiKey, connectionResult);
        this.zab.put(apiKey, s);
        --this.zad;
        if (!connectionResult.isSuccess()) {
            this.zae = true;
        }
        if (this.zad == 0) {
            if (this.zae) {
                this.zac.setException((Exception)new AvailabilityException(this.zaa));
                return;
            }
            this.zac.setResult((Object)this.zab);
        }
    }
}

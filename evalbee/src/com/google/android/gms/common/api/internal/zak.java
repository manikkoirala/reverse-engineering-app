// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.ConnectionResult;
import android.util.Log;
import java.io.PrintWriter;
import java.io.FileDescriptor;
import com.google.android.gms.common.GoogleApiAvailability;
import android.util.SparseArray;

public final class zak extends zap
{
    private final SparseArray<zaj> zad;
    
    private zak(final LifecycleFragment lifecycleFragment) {
        super(lifecycleFragment, GoogleApiAvailability.getInstance());
        this.zad = (SparseArray<zaj>)new SparseArray();
        super.mLifecycleFragment.addCallback("AutoManageHelper", this);
    }
    
    public static zak zaa(final LifecycleActivity lifecycleActivity) {
        final LifecycleFragment fragment = LifecycleCallback.getFragment(lifecycleActivity);
        final zak zak = fragment.getCallbackOrNull("AutoManageHelper", zak.class);
        if (zak != null) {
            return zak;
        }
        return new zak(fragment);
    }
    
    private final zaj zai(final int n) {
        if (this.zad.size() <= n) {
            return null;
        }
        final SparseArray<zaj> zad = this.zad;
        return (zaj)zad.get(zad.keyAt(n));
    }
    
    @Override
    public final void dump(final String s, final FileDescriptor fileDescriptor, final PrintWriter printWriter, final String[] array) {
        for (int i = 0; i < this.zad.size(); ++i) {
            final zaj zai = this.zai(i);
            if (zai != null) {
                printWriter.append(s).append("GoogleApiClient #").print(zai.zaa);
                printWriter.println(":");
                zai.zab.dump(String.valueOf(s).concat("  "), fileDescriptor, printWriter, array);
            }
        }
    }
    
    @Override
    public final void onStart() {
        super.onStart();
        final boolean zaa = super.zaa;
        final String value = String.valueOf(this.zad);
        final StringBuilder sb = new StringBuilder(value.length() + 14);
        sb.append("onStart ");
        sb.append(zaa);
        sb.append(" ");
        sb.append(value);
        Log.d("AutoManageHelper", sb.toString());
        if (super.zab.get() == null) {
            for (int i = 0; i < this.zad.size(); ++i) {
                final zaj zai = this.zai(i);
                if (zai != null) {
                    zai.zab.connect();
                }
            }
        }
    }
    
    @Override
    public final void onStop() {
        super.onStop();
        for (int i = 0; i < this.zad.size(); ++i) {
            final zaj zai = this.zai(i);
            if (zai != null) {
                zai.zab.disconnect();
            }
        }
    }
    
    @Override
    public final void zab(final ConnectionResult connectionResult, final int n) {
        Log.w("AutoManageHelper", "Unresolved error while connecting client. Stopping auto-manage.");
        if (n < 0) {
            Log.wtf("AutoManageHelper", "AutoManageLifecycleHelper received onErrorResolutionFailed callback but no failing client ID is set", (Throwable)new Exception());
            return;
        }
        final zaj zaj = (zaj)this.zad.get(n);
        if (zaj != null) {
            this.zae(n);
            final GoogleApiClient.OnConnectionFailedListener zac = zaj.zac;
            if (zac != null) {
                zac.onConnectionFailed(connectionResult);
            }
        }
    }
    
    @Override
    public final void zac() {
        for (int i = 0; i < this.zad.size(); ++i) {
            final zaj zai = this.zai(i);
            if (zai != null) {
                zai.zab.connect();
            }
        }
    }
    
    public final void zad(final int n, final GoogleApiClient googleApiClient, final GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        Preconditions.checkNotNull(googleApiClient, "GoogleApiClient instance cannot be null");
        final boolean b = this.zad.indexOfKey(n) < 0;
        final StringBuilder sb = new StringBuilder(54);
        sb.append("Already managing a GoogleApiClient with id ");
        sb.append(n);
        Preconditions.checkState(b, (Object)sb.toString());
        final zam obj = super.zab.get();
        final boolean zaa = super.zaa;
        final String value = String.valueOf(obj);
        final StringBuilder sb2 = new StringBuilder(value.length() + 49);
        sb2.append("starting AutoManage for client ");
        sb2.append(n);
        sb2.append(" ");
        sb2.append(zaa);
        sb2.append(" ");
        sb2.append(value);
        Log.d("AutoManageHelper", sb2.toString());
        final zaj zaj = new zaj(this, n, googleApiClient, onConnectionFailedListener);
        googleApiClient.registerConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener)zaj);
        this.zad.put(n, (Object)zaj);
        if (super.zaa && obj == null) {
            Log.d("AutoManageHelper", "connecting ".concat(googleApiClient.toString()));
            googleApiClient.connect();
        }
    }
    
    public final void zae(final int n) {
        final zaj zaj = (zaj)this.zad.get(n);
        this.zad.remove(n);
        if (zaj != null) {
            zaj.zab.unregisterConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener)zaj);
            zaj.zab.disconnect();
        }
    }
}

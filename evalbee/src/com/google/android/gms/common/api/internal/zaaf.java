// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.tasks.TaskCompletionSource;

final class zaaf
{
    private final ApiKey<?> zaa;
    private final TaskCompletionSource<Boolean> zab;
    
    public zaaf(final ApiKey<?> zaa) {
        this.zab = (TaskCompletionSource<Boolean>)new TaskCompletionSource();
        this.zaa = zaa;
    }
    
    public final ApiKey<?> zaa() {
        return this.zaa;
    }
    
    public final TaskCompletionSource<Boolean> zab() {
        return this.zab;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import androidx.fragment.app.e;
import com.google.android.gms.common.api.Api;
import java.io.PrintWriter;
import java.io.FileDescriptor;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.PendingResult;
import java.util.concurrent.TimeUnit;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

public class zaag extends GoogleApiClient
{
    private final String zaa;
    
    public zaag(final String s) {
        this.zaa = "Method is not supported by connectionless client. APIs supporting connectionless client must not call this method.";
    }
    
    @Override
    public final ConnectionResult blockingConnect() {
        throw new UnsupportedOperationException(this.zaa);
    }
    
    @Override
    public final ConnectionResult blockingConnect(final long n, final TimeUnit timeUnit) {
        throw new UnsupportedOperationException(this.zaa);
    }
    
    @Override
    public final PendingResult<Status> clearDefaultAccountAndReconnect() {
        throw new UnsupportedOperationException(this.zaa);
    }
    
    @Override
    public final void connect() {
        throw new UnsupportedOperationException(this.zaa);
    }
    
    @Override
    public final void disconnect() {
        throw new UnsupportedOperationException(this.zaa);
    }
    
    @Override
    public final void dump(final String s, final FileDescriptor fileDescriptor, final PrintWriter printWriter, final String[] array) {
        throw new UnsupportedOperationException(this.zaa);
    }
    
    @Override
    public final ConnectionResult getConnectionResult(final Api<?> api) {
        throw new UnsupportedOperationException(this.zaa);
    }
    
    @Override
    public final boolean hasConnectedApi(final Api<?> api) {
        throw new UnsupportedOperationException(this.zaa);
    }
    
    @Override
    public final boolean isConnected() {
        throw new UnsupportedOperationException(this.zaa);
    }
    
    @Override
    public final boolean isConnecting() {
        throw new UnsupportedOperationException(this.zaa);
    }
    
    @Override
    public final boolean isConnectionCallbacksRegistered(final ConnectionCallbacks connectionCallbacks) {
        throw new UnsupportedOperationException(this.zaa);
    }
    
    @Override
    public final boolean isConnectionFailedListenerRegistered(final OnConnectionFailedListener onConnectionFailedListener) {
        throw new UnsupportedOperationException(this.zaa);
    }
    
    @Override
    public final void reconnect() {
        throw new UnsupportedOperationException(this.zaa);
    }
    
    @Override
    public final void registerConnectionCallbacks(final ConnectionCallbacks connectionCallbacks) {
        throw new UnsupportedOperationException(this.zaa);
    }
    
    @Override
    public final void registerConnectionFailedListener(final OnConnectionFailedListener onConnectionFailedListener) {
        throw new UnsupportedOperationException(this.zaa);
    }
    
    @Override
    public final void stopAutoManage(final e e) {
        throw new UnsupportedOperationException(this.zaa);
    }
    
    @Override
    public final void unregisterConnectionCallbacks(final ConnectionCallbacks connectionCallbacks) {
        throw new UnsupportedOperationException(this.zaa);
    }
    
    @Override
    public final void unregisterConnectionFailedListener(final OnConnectionFailedListener onConnectionFailedListener) {
        throw new UnsupportedOperationException(this.zaa);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.internal.zaj;

final class zaay implements zaj
{
    final zabe zaa;
    
    public zaay(final zabe zaa) {
        this.zaa = zaa;
    }
    
    @Override
    public final boolean isConnected() {
        return this.zaa.isConnected();
    }
}

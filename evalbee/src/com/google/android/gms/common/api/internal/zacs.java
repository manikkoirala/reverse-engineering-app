// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Scope;
import java.util.Set;
import com.google.android.gms.common.internal.IAccountAccessor;
import com.google.android.gms.common.ConnectionResult;

public interface zacs
{
    void zae(final ConnectionResult p0);
    
    void zaf(final IAccountAccessor p0, final Set<Scope> p1);
}

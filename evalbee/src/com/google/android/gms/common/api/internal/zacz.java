// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.PendingResult;
import android.util.Log;
import android.os.Message;
import android.os.Looper;
import com.google.android.gms.internal.base.zaq;

final class zacz extends zaq
{
    final zada zaa;
    
    public zacz(final zada zaa, final Looper looper) {
        this.zaa = zaa;
        super(looper);
    }
    
    public final void handleMessage(final Message message) {
        final int what = message.what;
        if (what != 0) {
            if (what != 1) {
                final StringBuilder sb = new StringBuilder(70);
                sb.append("TransformationResultHandler received unknown message type: ");
                sb.append(what);
                Log.e("TransformedResultImpl", sb.toString());
                return;
            }
            final RuntimeException ex = (RuntimeException)message.obj;
            final String value = String.valueOf(ex.getMessage());
            String concat;
            if (value.length() != 0) {
                concat = "Runtime exception on the transformation worker thread: ".concat(value);
            }
            else {
                concat = new String("Runtime exception on the transformation worker thread: ");
            }
            Log.e("TransformedResultImpl", concat);
            throw ex;
        }
        else {
            final PendingResult pendingResult = (PendingResult)message.obj;
            synchronized (zada.zad((zada<Result>)this.zaa)) {
                final zada<Result> zada = Preconditions.checkNotNull(com.google.android.gms.common.api.internal.zada.zac((zada<Result>)this.zaa));
                Status zaa;
                if (pendingResult == null) {
                    zaa = new Status(13, "Transform returned null");
                }
                else {
                    if (!(pendingResult instanceof zacp)) {
                        zada.zai(pendingResult);
                        return;
                    }
                    zaa = ((zacp)pendingResult).zaa();
                }
                com.google.android.gms.common.api.internal.zada.zag(zada, zaa);
            }
        }
    }
}

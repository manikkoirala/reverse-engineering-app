// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;

final class zaz implements zabz
{
    final zaaa zaa;
    
    @Override
    public final void zaa(final ConnectionResult connectionResult) {
        zaaa.zaj(this.zaa).lock();
        try {
            zaaa.zal(this.zaa, connectionResult);
            zaaa.zap(this.zaa);
        }
        finally {
            zaaa.zaj(this.zaa).unlock();
        }
    }
    
    @Override
    public final void zab(final Bundle bundle) {
        zaaa.zaj(this.zaa).lock();
        try {
            zaaa.zal(this.zaa, ConnectionResult.RESULT_SUCCESS);
            zaaa.zap(this.zaa);
        }
        finally {
            zaaa.zaj(this.zaa).unlock();
        }
    }
    
    @Override
    public final void zac(final int n, final boolean b) {
        zaaa.zaj(this.zaa).lock();
        try {
            final zaaa zaa = this.zaa;
            if (zaaa.zav(zaa)) {
                zaaa.zam(zaa, false);
                zaaa.zan(this.zaa, n, b);
            }
            else {
                zaaa.zam(zaa, true);
                zaaa.zah(this.zaa).onConnectionSuspended(n);
            }
        }
        finally {
            zaaa.zaj(this.zaa).unlock();
        }
    }
}

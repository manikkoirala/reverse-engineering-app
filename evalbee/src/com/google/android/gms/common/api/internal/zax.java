// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;

final class zax implements zabz
{
    final zaaa zaa;
    
    @Override
    public final void zaa(final ConnectionResult connectionResult) {
        zaaa.zaj(this.zaa).lock();
        try {
            zaaa.zak(this.zaa, connectionResult);
            zaaa.zap(this.zaa);
        }
        finally {
            zaaa.zaj(this.zaa).unlock();
        }
    }
    
    @Override
    public final void zab(final Bundle bundle) {
        zaaa.zaj(this.zaa).lock();
        try {
            zaaa.zao(this.zaa, bundle);
            zaaa.zak(this.zaa, ConnectionResult.RESULT_SUCCESS);
            zaaa.zap(this.zaa);
        }
        finally {
            zaaa.zaj(this.zaa).unlock();
        }
    }
    
    @Override
    public final void zac(final int n, final boolean b) {
        zaaa.zaj(this.zaa).lock();
        try {
            final zaaa zaa = this.zaa;
            if (!zaaa.zav(zaa) && zaaa.zaa(zaa) != null && zaaa.zaa(zaa).isSuccess()) {
                zaaa.zam(this.zaa, true);
                zaaa.zai(this.zaa).onConnectionSuspended(n);
            }
            else {
                zaaa.zam(this.zaa, false);
                zaaa.zan(this.zaa, n, b);
            }
        }
        finally {
            zaaa.zaj(this.zaa).unlock();
        }
    }
}

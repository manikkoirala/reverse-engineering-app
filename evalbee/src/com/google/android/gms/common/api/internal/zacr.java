// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.signin.internal.zak;

final class zacr implements Runnable
{
    final zak zaa;
    final zact zab;
    
    public zacr(final zact zab, final zak zaa) {
        this.zab = zab;
        this.zaa = zaa;
    }
    
    @Override
    public final void run() {
        zact.zad(this.zab, this.zaa);
    }
}

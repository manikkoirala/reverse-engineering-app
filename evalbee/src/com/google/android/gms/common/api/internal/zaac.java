// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.OnCompleteListener;

final class zaac implements OnCompleteListener
{
    final TaskCompletionSource zaa;
    final zaad zab;
    
    public zaac(final zaad zab, final TaskCompletionSource zaa) {
        this.zab = zab;
        this.zaa = zaa;
    }
    
    public final void onComplete(final Task task) {
        zaad.zab(this.zab).remove(this.zaa);
    }
}

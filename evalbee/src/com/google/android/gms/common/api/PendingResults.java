// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api;

import com.google.android.gms.common.api.internal.OptionalPendingResultImpl;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.api.internal.StatusPendingResult;
import android.os.Looper;
import com.google.android.gms.common.annotation.KeepForSdk;

public final class PendingResults
{
    @KeepForSdk
    private PendingResults() {
    }
    
    public static PendingResult<Status> canceledPendingResult() {
        final StatusPendingResult statusPendingResult = new StatusPendingResult(Looper.getMainLooper());
        statusPendingResult.cancel();
        return statusPendingResult;
    }
    
    public static <R extends Result> PendingResult<R> canceledPendingResult(final R r) {
        Preconditions.checkNotNull(r, "Result must not be null");
        Preconditions.checkArgument(r.getStatus().getStatusCode() == 16, (Object)"Status code must be CommonStatusCodes.CANCELED");
        final zaf zaf = new zaf(r);
        zaf.cancel();
        return zaf;
    }
    
    @KeepForSdk
    public static <R extends Result> PendingResult<R> immediateFailedResult(final R result, final GoogleApiClient googleApiClient) {
        Preconditions.checkNotNull(result, "Result must not be null");
        Preconditions.checkArgument(result.getStatus().isSuccess() ^ true, (Object)"Status code must not be SUCCESS");
        final zag zag = new zag(googleApiClient, (R)result);
        zag.setResult(result);
        return zag;
    }
    
    @KeepForSdk
    public static <R extends Result> OptionalPendingResult<R> immediatePendingResult(final R result) {
        Preconditions.checkNotNull(result, "Result must not be null");
        final zah zah = new zah((GoogleApiClient)null);
        zah.setResult(result);
        return new OptionalPendingResultImpl<R>(zah);
    }
    
    @KeepForSdk
    public static <R extends Result> OptionalPendingResult<R> immediatePendingResult(final R result, final GoogleApiClient googleApiClient) {
        Preconditions.checkNotNull(result, "Result must not be null");
        final zah zah = new zah(googleApiClient);
        zah.setResult(result);
        return new OptionalPendingResultImpl<R>(zah);
    }
    
    @KeepForSdk
    public static PendingResult<Status> immediatePendingResult(final Status result) {
        Preconditions.checkNotNull(result, "Result must not be null");
        final StatusPendingResult statusPendingResult = new StatusPendingResult(Looper.getMainLooper());
        statusPendingResult.setResult(result);
        return statusPendingResult;
    }
    
    @KeepForSdk
    public static PendingResult<Status> immediatePendingResult(final Status result, final GoogleApiClient googleApiClient) {
        Preconditions.checkNotNull(result, "Result must not be null");
        final StatusPendingResult statusPendingResult = new StatusPendingResult(googleApiClient);
        statusPendingResult.setResult(result);
        return statusPendingResult;
    }
}

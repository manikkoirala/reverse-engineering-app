// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.Feature;

final class zabs
{
    private final ApiKey<?> zaa = zaa;
    private final Feature zab = zab;
    
    @Override
    public final boolean equals(final Object o) {
        if (o != null && o instanceof zabs) {
            final zabs zabs = (zabs)o;
            if (Objects.equal(this.zaa, zabs.zaa) && Objects.equal(this.zab, zabs.zab)) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public final int hashCode() {
        return Objects.hashCode(this.zaa, this.zab);
    }
    
    @Override
    public final String toString() {
        return Objects.toStringHelper(this).add("key", this.zaa).add("feature", this.zab).toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api;

import com.google.android.gms.common.api.internal.ApiExceptionMapper;
import com.google.android.gms.common.api.internal.zact;
import android.os.Handler;
import com.google.android.gms.common.api.internal.NonGmsServiceBrokerClient;
import com.google.android.gms.common.internal.BaseGmsClient;
import com.google.android.gms.common.api.internal.zabq;
import com.google.android.gms.common.api.internal.ListenerHolders;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.common.api.internal.RegistrationMethods;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.api.internal.UnregisterListenerMethod;
import com.google.android.gms.common.api.internal.RegisterListenerMethod;
import android.accounts.Account;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import java.util.Collection;
import java.util.Collections;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.common.api.internal.TaskApiCall;
import com.google.android.gms.common.api.internal.BaseImplementation;
import java.lang.reflect.InvocationTargetException;
import com.google.android.gms.common.api.internal.zaae;
import com.google.android.gms.common.api.internal.zabv;
import com.google.android.gms.common.util.PlatformVersion;
import com.google.android.gms.common.internal.Preconditions;
import android.app.Activity;
import com.google.android.gms.common.api.internal.StatusExceptionMapper;
import org.checkerframework.checker.initialization.qual.NotOnlyInitialized;
import android.os.Looper;
import com.google.android.gms.common.api.internal.ApiKey;
import android.content.Context;
import com.google.android.gms.common.api.internal.GoogleApiManager;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public abstract class GoogleApi<O extends Api.ApiOptions> implements HasApiKey<O>
{
    protected final GoogleApiManager zaa;
    private final Context zab;
    private final String zac;
    private final Api<O> zad;
    private final O zae;
    private final ApiKey<O> zaf;
    private final Looper zag;
    private final int zah;
    @NotOnlyInitialized
    private final GoogleApiClient zai;
    private final StatusExceptionMapper zaj;
    
    @KeepForSdk
    public GoogleApi(final Activity activity, final Api<O> api, final O o, final Settings settings) {
        this((Context)activity, activity, (Api<Api.ApiOptions>)api, o, settings);
    }
    
    @Deprecated
    @KeepForSdk
    public GoogleApi(final Activity activity, final Api<O> api, final O o, final StatusExceptionMapper mapper) {
        final Builder builder = new Builder();
        builder.setMapper(mapper);
        builder.setLooper(((Context)activity).getMainLooper());
        this(activity, (Api<Api.ApiOptions>)api, o, builder.build());
    }
    
    private GoogleApi(final Context obj, final Activity activity, Api<O> zaa, final O zae, final Settings settings) {
        Preconditions.checkNotNull(obj, "Null context is not permitted.");
        Preconditions.checkNotNull((Api<O>)zaa, "Api must not be null.");
        Preconditions.checkNotNull(settings, "Settings must not be null; use Settings.DEFAULT_SETTINGS instead.");
        this.zab = obj.getApplicationContext();
        final boolean atLeastR = PlatformVersion.isAtLeastR();
        String zac;
        final String s = zac = null;
        while (true) {
            if (!atLeastR) {
                break Label_0075;
            }
            try {
                zac = (String)Context.class.getMethod("getAttributionTag", (Class<?>[])new Class[0]).invoke(obj, new Object[0]);
                this.zac = zac;
                this.zad = (Api<O>)zaa;
                this.zae = zae;
                this.zag = settings.zab;
                zaa = ApiKey.zaa((Api<O>)zaa, zae, zac);
                this.zaf = zaa;
                this.zai = new zabv<Object>(this);
                final GoogleApiManager zam = GoogleApiManager.zam(this.zab);
                this.zaa = zam;
                this.zah = zam.zaa();
                this.zaj = settings.zaa;
                if (activity != null && !(activity instanceof GoogleApiActivity) && Looper.myLooper() == Looper.getMainLooper()) {
                    zaae.zad(activity, zam, zaa);
                }
                zam.zaB(this);
            }
            catch (final NoSuchMethodException | IllegalAccessException | InvocationTargetException ex) {
                zac = s;
                continue;
            }
            break;
        }
    }
    
    @Deprecated
    @KeepForSdk
    public GoogleApi(final Context context, final Api<O> api, final O o, final Looper looper, final StatusExceptionMapper mapper) {
        final Builder builder = new Builder();
        builder.setLooper(looper);
        builder.setMapper(mapper);
        this(context, (Api<Api.ApiOptions>)api, o, builder.build());
    }
    
    @KeepForSdk
    public GoogleApi(final Context context, final Api<O> api, final O o, final Settings settings) {
        this(context, null, (Api<Api.ApiOptions>)api, o, settings);
    }
    
    @Deprecated
    @KeepForSdk
    public GoogleApi(final Context context, final Api<O> api, final O o, final StatusExceptionMapper mapper) {
        final Builder builder = new Builder();
        builder.setMapper(mapper);
        this(context, (Api<Api.ApiOptions>)api, o, builder.build());
    }
    
    private final <A extends Api.AnyClient, T extends BaseImplementation.ApiMethodImpl<? extends Result, A>> T zad(final int n, final T t) {
        t.zak();
        this.zaa.zaw((GoogleApi<Api.ApiOptions>)this, n, (BaseImplementation.ApiMethodImpl<? extends Result, Api.AnyClient>)t);
        return t;
    }
    
    private final <TResult, A extends Api.AnyClient> Task<TResult> zae(final int n, final TaskApiCall<A, TResult> taskApiCall) {
        final TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        this.zaa.zax((GoogleApi<Api.ApiOptions>)this, n, (TaskApiCall<Api.AnyClient, TResult>)taskApiCall, (com.google.android.gms.tasks.TaskCompletionSource<TResult>)taskCompletionSource, this.zaj);
        return (Task<TResult>)taskCompletionSource.getTask();
    }
    
    @KeepForSdk
    public GoogleApiClient asGoogleApiClient() {
        return this.zai;
    }
    
    @KeepForSdk
    public ClientSettings.Builder createClientSettingsBuilder() {
        final ClientSettings.Builder builder = new ClientSettings.Builder();
        final Api.ApiOptions zae = this.zae;
        Account account = null;
        Label_0069: {
            if (zae instanceof Api.ApiOptions.HasGoogleSignInAccountOptions) {
                final GoogleSignInAccount googleSignInAccount = ((Api.ApiOptions.HasGoogleSignInAccountOptions)zae).getGoogleSignInAccount();
                if (googleSignInAccount != null) {
                    account = googleSignInAccount.getAccount();
                    break Label_0069;
                }
            }
            final Api.ApiOptions zae2 = this.zae;
            if (zae2 instanceof Api.ApiOptions.HasAccountOptions) {
                account = ((Api.ApiOptions.HasAccountOptions)zae2).getAccount();
            }
            else {
                account = null;
            }
        }
        builder.zab(account);
        final Api.ApiOptions zae3 = this.zae;
        Object o = null;
        Label_0116: {
            if (zae3 instanceof Api.ApiOptions.HasGoogleSignInAccountOptions) {
                final GoogleSignInAccount googleSignInAccount2 = ((Api.ApiOptions.HasGoogleSignInAccountOptions)zae3).getGoogleSignInAccount();
                if (googleSignInAccount2 != null) {
                    o = googleSignInAccount2.getRequestedScopes();
                    break Label_0116;
                }
            }
            o = Collections.emptySet();
        }
        builder.zaa((Collection<Scope>)o);
        builder.zac(this.zab.getClass().getName());
        builder.setRealClientPackageName(this.zab.getPackageName());
        return builder;
    }
    
    @KeepForSdk
    public Task<Boolean> disconnectService() {
        return this.zaa.zap(this);
    }
    
    @KeepForSdk
    public <A extends Api.AnyClient, T extends BaseImplementation.ApiMethodImpl<? extends Result, A>> T doBestEffortWrite(final T t) {
        this.zad(2, t);
        return t;
    }
    
    @KeepForSdk
    public <TResult, A extends Api.AnyClient> Task<TResult> doBestEffortWrite(final TaskApiCall<A, TResult> taskApiCall) {
        return this.zae(2, taskApiCall);
    }
    
    @KeepForSdk
    public <A extends Api.AnyClient, T extends BaseImplementation.ApiMethodImpl<? extends Result, A>> T doRead(final T t) {
        this.zad(0, t);
        return t;
    }
    
    @KeepForSdk
    public <TResult, A extends Api.AnyClient> Task<TResult> doRead(final TaskApiCall<A, TResult> taskApiCall) {
        return this.zae(0, taskApiCall);
    }
    
    @Deprecated
    @KeepForSdk
    public <A extends Api.AnyClient, T extends RegisterListenerMethod<A, ?>, U extends UnregisterListenerMethod<A, ?>> Task<Void> doRegisterEventListener(final T t, final U u) {
        Preconditions.checkNotNull(t);
        Preconditions.checkNotNull(u);
        Preconditions.checkNotNull(((RegisterListenerMethod<A, ?>)t).getListenerKey(), "Listener has already been released.");
        Preconditions.checkNotNull(((UnregisterListenerMethod<A, ?>)u).getListenerKey(), "Listener has already been released.");
        Preconditions.checkArgument(Objects.equal(t.getListenerKey(), u.getListenerKey()), (Object)"Listener registration and unregistration methods must be constructed with the same ListenerHolder.");
        return this.zaa.zaq((GoogleApi<Api.ApiOptions>)this, (RegisterListenerMethod<Api.AnyClient, ?>)t, (UnregisterListenerMethod<Api.AnyClient, ?>)u, com.google.android.gms.common.api.zad.zaa);
    }
    
    @KeepForSdk
    public <A extends Api.AnyClient> Task<Void> doRegisterEventListener(final RegistrationMethods<A, ?> registrationMethods) {
        Preconditions.checkNotNull(registrationMethods);
        Preconditions.checkNotNull(registrationMethods.register.getListenerKey(), "Listener has already been released.");
        Preconditions.checkNotNull(registrationMethods.zaa.getListenerKey(), "Listener has already been released.");
        return this.zaa.zaq((GoogleApi<Api.ApiOptions>)this, (RegisterListenerMethod<Api.AnyClient, ?>)registrationMethods.register, (UnregisterListenerMethod<Api.AnyClient, ?>)registrationMethods.zaa, registrationMethods.zab);
    }
    
    @KeepForSdk
    public Task<Boolean> doUnregisterEventListener(final ListenerHolder.ListenerKey<?> listenerKey) {
        return this.doUnregisterEventListener(listenerKey, 0);
    }
    
    @KeepForSdk
    public Task<Boolean> doUnregisterEventListener(final ListenerHolder.ListenerKey<?> listenerKey, final int n) {
        Preconditions.checkNotNull(listenerKey, "Listener key cannot be null.");
        return this.zaa.zar((GoogleApi<Api.ApiOptions>)this, listenerKey, n);
    }
    
    @KeepForSdk
    public <A extends Api.AnyClient, T extends BaseImplementation.ApiMethodImpl<? extends Result, A>> T doWrite(final T t) {
        this.zad(1, t);
        return t;
    }
    
    @KeepForSdk
    public <TResult, A extends Api.AnyClient> Task<TResult> doWrite(final TaskApiCall<A, TResult> taskApiCall) {
        return this.zae(1, taskApiCall);
    }
    
    @Override
    public final ApiKey<O> getApiKey() {
        return this.zaf;
    }
    
    @KeepForSdk
    public O getApiOptions() {
        return this.zae;
    }
    
    @KeepForSdk
    public Context getApplicationContext() {
        return this.zab;
    }
    
    @KeepForSdk
    public String getContextAttributionTag() {
        return this.zac;
    }
    
    @Deprecated
    @KeepForSdk
    public String getContextFeatureId() {
        return this.zac;
    }
    
    @KeepForSdk
    public Looper getLooper() {
        return this.zag;
    }
    
    @KeepForSdk
    public <L> ListenerHolder<L> registerListener(final L l, final String s) {
        return ListenerHolders.createListenerHolder(l, this.zag, s);
    }
    
    public final int zaa() {
        return this.zah;
    }
    
    public final Api.Client zab(final Looper looper, final zabq<O> zabq) {
        final NonGmsServiceBrokerClient buildClient = Preconditions.checkNotNull(this.zad.zaa()).buildClient(this.zab, looper, this.createClientSettingsBuilder().build(), this.zae, zabq, zabq);
        final String contextAttributionTag = this.getContextAttributionTag();
        if (contextAttributionTag != null && buildClient instanceof BaseGmsClient) {
            ((BaseGmsClient)buildClient).setAttributionTag(contextAttributionTag);
        }
        if (contextAttributionTag != null && buildClient instanceof NonGmsServiceBrokerClient) {
            buildClient.zac(contextAttributionTag);
        }
        return buildClient;
    }
    
    public final zact zac(final Context context, final Handler handler) {
        return new zact(context, handler, this.createClientSettingsBuilder().build());
    }
    
    @KeepForSdk
    public static class Settings
    {
        @KeepForSdk
        public static final Settings DEFAULT_SETTINGS;
        public final StatusExceptionMapper zaa;
        public final Looper zab;
        
        static {
            DEFAULT_SETTINGS = new Builder().build();
        }
        
        @KeepForSdk
        private Settings(final StatusExceptionMapper zaa, final Account account, final Looper zab) {
            this.zaa = zaa;
            this.zab = zab;
        }
        
        @KeepForSdk
        public static class Builder
        {
            private StatusExceptionMapper zaa;
            private Looper zab;
            
            @KeepForSdk
            public Builder() {
            }
            
            @KeepForSdk
            public Settings build() {
                if (this.zaa == null) {
                    this.zaa = new ApiExceptionMapper();
                }
                if (this.zab == null) {
                    this.zab = Looper.getMainLooper();
                }
                return new Settings(this.zaa, null, this.zab, null);
            }
            
            @KeepForSdk
            public Builder setLooper(final Looper zab) {
                Preconditions.checkNotNull(zab, "Looper must not be null.");
                this.zab = zab;
                return this;
            }
            
            @KeepForSdk
            public Builder setMapper(final StatusExceptionMapper zaa) {
                Preconditions.checkNotNull(zaa, "StatusExceptionMapper must not be null.");
                this.zaa = zaa;
                return this;
            }
        }
    }
}

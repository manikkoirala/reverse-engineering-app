// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api;

import com.google.android.gms.common.api.internal.BasePendingResult;

final class zag<R extends Result> extends BasePendingResult<R>
{
    private final R zae;
    
    public zag(final GoogleApiClient googleApiClient, final R zae) {
        super(googleApiClient);
        this.zae = zae;
    }
    
    @Override
    public final R createFailedResult(final Status status) {
        return this.zae;
    }
}

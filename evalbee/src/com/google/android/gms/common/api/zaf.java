// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api;

import android.os.Looper;
import com.google.android.gms.common.api.internal.BasePendingResult;

final class zaf<R extends Result> extends BasePendingResult<R>
{
    private final R zae;
    
    public zaf(final R zae) {
        super(Looper.getMainLooper());
        this.zae = zae;
    }
    
    @Override
    public final R createFailedResult(final Status status) {
        if (status.getStatusCode() == this.zae.getStatus().getStatusCode()) {
            return this.zae;
        }
        throw new UnsupportedOperationException("Creating failed results is not supported");
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api;

import com.google.android.gms.common.annotation.KeepForSdk;
import android.content.IntentSender$SendIntentException;
import android.util.Log;
import com.google.android.gms.common.internal.Preconditions;
import android.app.Activity;

public abstract class ResolvingResultCallbacks<R extends Result> extends ResultCallbacks<R>
{
    private final Activity zza;
    private final int zzb;
    
    public ResolvingResultCallbacks(final Activity zza, final int zzb) {
        Preconditions.checkNotNull(zza, "Activity must not be null");
        this.zza = zza;
        this.zzb = zzb;
    }
    
    @KeepForSdk
    @Override
    public final void onFailure(final Status status) {
        if (status.hasResolution()) {
            try {
                status.startResolutionForResult(this.zza, this.zzb);
                return;
            }
            catch (final IntentSender$SendIntentException ex) {
                Log.e("ResolvingResultCallback", "Failed to start resolution", (Throwable)ex);
                this.onUnresolvableFailure(new Status(8));
                return;
            }
        }
        this.onUnresolvableFailure(status);
    }
    
    @Override
    public abstract void onSuccess(final R p0);
    
    public abstract void onUnresolvableFailure(final Status p0);
}

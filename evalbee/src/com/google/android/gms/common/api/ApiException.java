// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api;

public class ApiException extends Exception
{
    @Deprecated
    protected final Status mStatus;
    
    public ApiException(final Status mStatus) {
        final int statusCode = mStatus.getStatusCode();
        String statusMessage;
        if (mStatus.getStatusMessage() != null) {
            statusMessage = mStatus.getStatusMessage();
        }
        else {
            statusMessage = "";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(statusCode);
        sb.append(": ");
        sb.append(statusMessage);
        super(sb.toString());
        this.mStatus = mStatus;
    }
    
    public Status getStatus() {
        return this.mStatus;
    }
    
    public int getStatusCode() {
        return this.mStatus.getStatusCode();
    }
    
    @Deprecated
    public String getStatusMessage() {
        return this.mStatus.getStatusMessage();
    }
}

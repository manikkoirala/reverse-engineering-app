// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api;

import android.content.Intent;
import android.os.IBinder;
import java.util.Set;
import com.google.android.gms.common.internal.IAccountAccessor;
import com.google.android.gms.common.Feature;
import java.io.PrintWriter;
import java.io.FileDescriptor;
import com.google.android.gms.common.internal.BaseGmsClient;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import android.accounts.Account;
import java.util.Collections;
import java.util.List;
import com.google.android.gms.common.api.internal.OnConnectionFailedListener;
import com.google.android.gms.common.api.internal.ConnectionCallbacks;
import com.google.android.gms.common.internal.ClientSettings;
import android.os.Looper;
import android.content.Context;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.Preconditions;

public final class Api<O extends ApiOptions>
{
    private final AbstractClientBuilder<?, O> zaa;
    private final ClientKey<?> zab;
    private final String zac;
    
    @KeepForSdk
    public <C extends Client> Api(final String zac, final AbstractClientBuilder<C, O> zaa, final ClientKey<C> zab) {
        Preconditions.checkNotNull(zaa, "Cannot construct an Api with a null ClientBuilder");
        Preconditions.checkNotNull(zab, "Cannot construct an Api with a null ClientKey");
        this.zac = zac;
        this.zaa = zaa;
        this.zab = zab;
    }
    
    public final AbstractClientBuilder<?, O> zaa() {
        return this.zaa;
    }
    
    public final AnyClientKey<?> zab() {
        return (AnyClientKey<?>)this.zab;
    }
    
    public final BaseClientBuilder<?, O> zac() {
        return (BaseClientBuilder<?, O>)this.zaa;
    }
    
    public final String zad() {
        return this.zac;
    }
    
    @KeepForSdk
    @VisibleForTesting
    public abstract static class AbstractClientBuilder<T extends Client, O> extends BaseClientBuilder<T, O>
    {
        @Deprecated
        @KeepForSdk
        public T buildClient(final Context context, final Looper looper, final ClientSettings clientSettings, final O o, final GoogleApiClient.ConnectionCallbacks connectionCallbacks, final GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
            return this.buildClient(context, looper, clientSettings, o, connectionCallbacks, (OnConnectionFailedListener)onConnectionFailedListener);
        }
        
        @KeepForSdk
        public T buildClient(final Context context, final Looper looper, final ClientSettings clientSettings, final O o, final ConnectionCallbacks connectionCallbacks, final OnConnectionFailedListener onConnectionFailedListener) {
            throw new UnsupportedOperationException("buildClient must be implemented");
        }
    }
    
    @KeepForSdk
    @VisibleForTesting
    public abstract static class BaseClientBuilder<T extends AnyClient, O>
    {
        @KeepForSdk
        public static final int API_PRIORITY_GAMES = 1;
        @KeepForSdk
        public static final int API_PRIORITY_OTHER = Integer.MAX_VALUE;
        @KeepForSdk
        public static final int API_PRIORITY_PLUS = 2;
        
        @KeepForSdk
        public List<Scope> getImpliedScopes(final O o) {
            return Collections.emptyList();
        }
        
        @KeepForSdk
        public int getPriority() {
            return Integer.MAX_VALUE;
        }
    }
    
    @KeepForSdk
    public interface AnyClient
    {
    }
    
    @KeepForSdk
    public static class AnyClientKey<C extends AnyClient>
    {
    }
    
    public interface ApiOptions
    {
        public static final NoOptions NO_OPTIONS = new NoOptions(null);
        
        public interface HasAccountOptions extends HasOptions, NotRequiredOptions
        {
            Account getAccount();
        }
        
        public interface HasOptions extends ApiOptions
        {
        }
        
        public interface NotRequiredOptions extends ApiOptions
        {
        }
        
        public interface HasGoogleSignInAccountOptions extends HasOptions
        {
            GoogleSignInAccount getGoogleSignInAccount();
        }
        
        public static final class NoOptions implements NotRequiredOptions
        {
            private NoOptions() {
            }
        }
        
        public interface Optional extends HasOptions, NotRequiredOptions
        {
        }
    }
    
    @KeepForSdk
    public interface Client extends AnyClient
    {
        @KeepForSdk
        void connect(final BaseGmsClient.ConnectionProgressReportCallbacks p0);
        
        @KeepForSdk
        void disconnect();
        
        @KeepForSdk
        void disconnect(final String p0);
        
        @KeepForSdk
        void dump(final String p0, final FileDescriptor p1, final PrintWriter p2, final String[] p3);
        
        @KeepForSdk
        Feature[] getAvailableFeatures();
        
        @KeepForSdk
        String getEndpointPackageName();
        
        @KeepForSdk
        String getLastDisconnectMessage();
        
        @KeepForSdk
        int getMinApkVersion();
        
        @KeepForSdk
        void getRemoteService(final IAccountAccessor p0, final Set<Scope> p1);
        
        @KeepForSdk
        Feature[] getRequiredFeatures();
        
        @KeepForSdk
        Set<Scope> getScopesForConnectionlessNonSignIn();
        
        @KeepForSdk
        IBinder getServiceBrokerBinder();
        
        @KeepForSdk
        Intent getSignInIntent();
        
        @KeepForSdk
        boolean isConnected();
        
        @KeepForSdk
        boolean isConnecting();
        
        @KeepForSdk
        void onUserSignOut(final BaseGmsClient.SignOutCallbacks p0);
        
        @KeepForSdk
        boolean providesSignIn();
        
        @KeepForSdk
        boolean requiresAccount();
        
        @KeepForSdk
        boolean requiresGooglePlayServices();
        
        @KeepForSdk
        boolean requiresSignIn();
    }
    
    @KeepForSdk
    @VisibleForTesting
    public static final class ClientKey<C extends Client> extends AnyClientKey<C>
    {
    }
}

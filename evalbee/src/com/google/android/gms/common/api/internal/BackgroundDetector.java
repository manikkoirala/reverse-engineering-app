// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import android.app.ActivityManager;
import android.app.ActivityManager$RunningAppProcessInfo;
import com.google.android.gms.common.util.PlatformVersion;
import android.content.res.Configuration;
import android.os.Bundle;
import android.app.Activity;
import java.util.Iterator;
import android.content.ComponentCallbacks;
import android.app.Application;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import com.google.android.gms.common.annotation.KeepForSdk;
import android.content.ComponentCallbacks2;
import android.app.Application$ActivityLifecycleCallbacks;

@KeepForSdk
public final class BackgroundDetector implements Application$ActivityLifecycleCallbacks, ComponentCallbacks2
{
    private static final BackgroundDetector zza;
    private final AtomicBoolean zzb;
    private final AtomicBoolean zzc;
    private final ArrayList zzd;
    private boolean zze;
    
    static {
        zza = new BackgroundDetector();
    }
    
    @KeepForSdk
    private BackgroundDetector() {
        this.zzb = new AtomicBoolean();
        this.zzc = new AtomicBoolean();
        this.zzd = new ArrayList();
        this.zze = false;
    }
    
    @KeepForSdk
    public static BackgroundDetector getInstance() {
        return BackgroundDetector.zza;
    }
    
    @KeepForSdk
    public static void initialize(final Application application) {
        final BackgroundDetector zza = BackgroundDetector.zza;
        synchronized (zza) {
            if (!zza.zze) {
                application.registerActivityLifecycleCallbacks((Application$ActivityLifecycleCallbacks)zza);
                application.registerComponentCallbacks((ComponentCallbacks)zza);
                zza.zze = true;
            }
        }
    }
    
    private final void zza(final boolean b) {
        synchronized (BackgroundDetector.zza) {
            final Iterator iterator = this.zzd.iterator();
            while (iterator.hasNext()) {
                ((BackgroundStateChangeListener)iterator.next()).onBackgroundStateChanged(b);
            }
        }
    }
    
    @KeepForSdk
    public void addListener(final BackgroundStateChangeListener e) {
        synchronized (BackgroundDetector.zza) {
            this.zzd.add(e);
        }
    }
    
    @KeepForSdk
    public boolean isInBackground() {
        return this.zzb.get();
    }
    
    public final void onActivityCreated(final Activity activity, final Bundle bundle) {
        final boolean compareAndSet = this.zzb.compareAndSet(true, false);
        this.zzc.set(true);
        if (compareAndSet) {
            this.zza(false);
        }
    }
    
    public final void onActivityDestroyed(final Activity activity) {
    }
    
    public final void onActivityPaused(final Activity activity) {
    }
    
    public final void onActivityResumed(final Activity activity) {
        final boolean compareAndSet = this.zzb.compareAndSet(true, false);
        this.zzc.set(true);
        if (compareAndSet) {
            this.zza(false);
        }
    }
    
    public final void onActivitySaveInstanceState(final Activity activity, final Bundle bundle) {
    }
    
    public final void onActivityStarted(final Activity activity) {
    }
    
    public final void onActivityStopped(final Activity activity) {
    }
    
    public final void onConfigurationChanged(final Configuration configuration) {
    }
    
    public final void onLowMemory() {
    }
    
    public final void onTrimMemory(final int n) {
        if (n == 20 && this.zzb.compareAndSet(false, true)) {
            this.zzc.set(true);
            this.zza(true);
        }
    }
    
    @KeepForSdk
    public boolean readCurrentStateIfPossible(final boolean b) {
        if (!this.zzc.get()) {
            if (!PlatformVersion.isAtLeastJellyBean()) {
                return b;
            }
            final ActivityManager$RunningAppProcessInfo activityManager$RunningAppProcessInfo = new ActivityManager$RunningAppProcessInfo();
            ActivityManager.getMyMemoryState(activityManager$RunningAppProcessInfo);
            if (!this.zzc.getAndSet(true) && activityManager$RunningAppProcessInfo.importance > 100) {
                this.zzb.set(true);
            }
        }
        return this.isInBackground();
    }
    
    @KeepForSdk
    public interface BackgroundStateChangeListener
    {
        @KeepForSdk
        void onBackgroundStateChanged(final boolean p0);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import java.util.Collections;
import com.google.android.gms.common.api.Scope;
import java.util.Set;
import com.google.android.gms.common.internal.IAccountAccessor;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.Feature;
import java.io.PrintWriter;
import java.io.FileDescriptor;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.GmsClientSupervisor;
import android.content.Intent;
import com.google.android.gms.common.internal.BaseGmsClient;
import com.google.android.gms.internal.base.zaq;
import android.os.Looper;
import android.os.IBinder;
import android.os.Handler;
import android.content.Context;
import android.content.ComponentName;
import com.google.android.gms.common.annotation.KeepForSdk;
import android.content.ServiceConnection;
import com.google.android.gms.common.api.Api;

@KeepForSdk
public final class NonGmsServiceBrokerClient implements Client, ServiceConnection
{
    private static final String zaa = "NonGmsServiceBrokerClient";
    private final String zab;
    private final String zac;
    private final ComponentName zad;
    private final Context zae;
    private final ConnectionCallbacks zaf;
    private final Handler zag;
    private final OnConnectionFailedListener zah;
    private IBinder zai;
    private boolean zaj;
    private String zak;
    private String zal;
    
    @KeepForSdk
    public NonGmsServiceBrokerClient(final Context context, final Looper looper, final ComponentName componentName, final ConnectionCallbacks connectionCallbacks, final OnConnectionFailedListener onConnectionFailedListener) {
        this(context, looper, null, null, componentName, connectionCallbacks, onConnectionFailedListener);
    }
    
    private NonGmsServiceBrokerClient(final Context zae, final Looper looper, final String zab, final String zac, ComponentName zad, final ConnectionCallbacks zaf, final OnConnectionFailedListener zah) {
        this.zaj = false;
        this.zak = null;
        this.zae = zae;
        this.zag = (Handler)new zaq(looper);
        this.zaf = zaf;
        this.zah = zah;
        if (zab != null && zac != null) {
            if (zad != null) {
                throw new AssertionError((Object)"Must specify either package or component, but not both");
            }
            zad = null;
        }
        else if (zad == null) {
            throw new AssertionError((Object)"Must specify either package or component, but not both");
        }
        this.zab = zab;
        this.zac = zac;
        this.zad = zad;
    }
    
    @KeepForSdk
    public NonGmsServiceBrokerClient(final Context context, final Looper looper, final String s, final String s2, final ConnectionCallbacks connectionCallbacks, final OnConnectionFailedListener onConnectionFailedListener) {
        this(context, looper, s, s2, null, connectionCallbacks, onConnectionFailedListener);
    }
    
    private final void zad() {
        if (Thread.currentThread() == this.zag.getLooper().getThread()) {
            return;
        }
        throw new IllegalStateException("This method should only run on the NonGmsServiceBrokerClient's handler thread.");
    }
    
    private final void zae(final String s) {
        String.valueOf(this.zai);
    }
    
    @Override
    public final void connect(final BaseGmsClient.ConnectionProgressReportCallbacks connectionProgressReportCallbacks) {
        this.zad();
        this.zae("Connect started.");
        while (true) {
            if (!this.isConnected()) {
                break Label_0023;
            }
            try {
                this.disconnect("connect() called when already connected");
                try {
                    final Intent intent = new Intent();
                    final ComponentName zad = this.zad;
                    if (zad != null) {
                        intent.setComponent(zad);
                    }
                    else {
                        intent.setPackage(this.zab).setAction(this.zac);
                    }
                    if (!(this.zaj = this.zae.bindService(intent, (ServiceConnection)this, GmsClientSupervisor.getDefaultBindFlags()))) {
                        this.zai = null;
                        this.zah.onConnectionFailed(new ConnectionResult(16));
                    }
                    this.zae("Finished connect.");
                }
                catch (final SecurityException ex) {
                    this.zaj = false;
                    this.zai = null;
                    throw ex;
                }
            }
            catch (final Exception ex2) {
                continue;
            }
            break;
        }
    }
    
    @Override
    public final void disconnect() {
        this.zad();
        this.zae("Disconnect called.");
        while (true) {
            try {
                this.zae.unbindService((ServiceConnection)this);
                this.zaj = false;
                this.zai = null;
            }
            catch (final IllegalArgumentException ex) {
                continue;
            }
            break;
        }
    }
    
    @Override
    public final void disconnect(final String zak) {
        this.zad();
        this.zak = zak;
        this.disconnect();
    }
    
    @Override
    public final void dump(final String s, final FileDescriptor fileDescriptor, final PrintWriter printWriter, final String[] array) {
    }
    
    @Override
    public final Feature[] getAvailableFeatures() {
        return new Feature[0];
    }
    
    @KeepForSdk
    public IBinder getBinder() {
        this.zad();
        return this.zai;
    }
    
    @Override
    public final String getEndpointPackageName() {
        final String zab = this.zab;
        if (zab != null) {
            return zab;
        }
        Preconditions.checkNotNull(this.zad);
        return this.zad.getPackageName();
    }
    
    @Override
    public final String getLastDisconnectMessage() {
        return this.zak;
    }
    
    @Override
    public final int getMinApkVersion() {
        return 0;
    }
    
    @Override
    public final void getRemoteService(final IAccountAccessor accountAccessor, final Set<Scope> set) {
    }
    
    @Override
    public final Feature[] getRequiredFeatures() {
        return new Feature[0];
    }
    
    @Override
    public final Set<Scope> getScopesForConnectionlessNonSignIn() {
        return Collections.emptySet();
    }
    
    @Override
    public final IBinder getServiceBrokerBinder() {
        return null;
    }
    
    @Override
    public final Intent getSignInIntent() {
        return new Intent();
    }
    
    @Override
    public final boolean isConnected() {
        this.zad();
        return this.zai != null;
    }
    
    @Override
    public final boolean isConnecting() {
        this.zad();
        return this.zaj;
    }
    
    public final void onServiceConnected(final ComponentName componentName, final IBinder binder) {
        this.zag.post((Runnable)new zacg(this, binder));
    }
    
    public final void onServiceDisconnected(final ComponentName componentName) {
        this.zag.post((Runnable)new zacf(this));
    }
    
    @Override
    public final void onUserSignOut(final BaseGmsClient.SignOutCallbacks signOutCallbacks) {
    }
    
    @Override
    public final boolean providesSignIn() {
        return false;
    }
    
    @Override
    public final boolean requiresAccount() {
        return false;
    }
    
    @Override
    public final boolean requiresGooglePlayServices() {
        return false;
    }
    
    @Override
    public final boolean requiresSignIn() {
        return false;
    }
    
    public final void zac(final String zal) {
        this.zal = zal;
    }
}

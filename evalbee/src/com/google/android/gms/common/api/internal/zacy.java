// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import android.os.Handler;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.api.ResultTransform;
import com.google.android.gms.common.api.Result;

final class zacy implements Runnable
{
    final Result zaa;
    final zada zab;
    
    public zacy(final zada zab, final Result zaa) {
        this.zab = zab;
        this.zaa = zaa;
    }
    
    @Override
    public final void run() {
        try {
            try {
                final ThreadLocal<Boolean> zaa = BasePendingResult.zaa;
                zaa.set(Boolean.TRUE);
                final PendingResult onSuccess = Preconditions.checkNotNull(zada.zaa((zada<Result>)this.zab)).onSuccess(this.zaa);
                final zada zab = this.zab;
                ((Handler)zada.zab((zada<Result>)zab)).sendMessage(((Handler)zada.zab((zada<Result>)zab)).obtainMessage(0, (Object)onSuccess));
                zaa.set(Boolean.FALSE);
                zada.zaf((zada<Result>)this.zab, this.zaa);
                final GoogleApiClient googleApiClient = (GoogleApiClient)zada.zae((zada<Result>)this.zab).get();
                if (googleApiClient != null) {
                    googleApiClient.zap(this.zab);
                }
            }
            finally {
                BasePendingResult.zaa.set(Boolean.FALSE);
                zada.zaf((zada<Result>)this.zab, this.zaa);
                final GoogleApiClient googleApiClient2 = (GoogleApiClient)zada.zae((zada<Result>)this.zab).get();
                if (googleApiClient2 != null) {
                    googleApiClient2.zap(this.zab);
                }
            }
        }
        catch (final RuntimeException ex) {}
    }
}

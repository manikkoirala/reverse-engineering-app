// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import android.os.Handler;
import android.util.Pair;
import android.os.Message;
import com.google.android.gms.common.api.TransformedResult;
import com.google.android.gms.common.api.ResultTransform;
import java.util.concurrent.TimeUnit;
import android.util.Log;
import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Looper;
import com.google.android.gms.common.internal.ICancelToken;
import com.google.android.gms.common.api.Status;
import java.util.concurrent.atomic.AtomicReference;
import com.google.android.gms.common.api.ResultCallback;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import com.google.android.gms.common.api.GoogleApiClient;
import java.lang.ref.WeakReference;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;

@KeepForSdk
@KeepName
public abstract class BasePendingResult<R extends Result> extends PendingResult<R>
{
    static final ThreadLocal<Boolean> zaa;
    public static final int zad = 0;
    @KeepName
    private zas mResultGuardian;
    protected final CallbackHandler<R> zab;
    protected final WeakReference<GoogleApiClient> zac;
    private final Object zae;
    private final CountDownLatch zaf;
    private final ArrayList<StatusListener> zag;
    private ResultCallback<? super R> zah;
    private final AtomicReference<zadb> zai;
    private R zaj;
    private Status zak;
    private volatile boolean zal;
    private boolean zam;
    private boolean zan;
    private ICancelToken zao;
    private volatile zada<R> zap;
    private boolean zaq;
    
    static {
        zaa = new zaq();
    }
    
    @Deprecated
    public BasePendingResult() {
        this.zae = new Object();
        this.zaf = new CountDownLatch(1);
        this.zag = new ArrayList<StatusListener>();
        this.zai = new AtomicReference<zadb>();
        this.zaq = false;
        this.zab = new CallbackHandler<R>(Looper.getMainLooper());
        this.zac = new WeakReference<GoogleApiClient>(null);
    }
    
    @Deprecated
    @KeepForSdk
    public BasePendingResult(final Looper looper) {
        this.zae = new Object();
        this.zaf = new CountDownLatch(1);
        this.zag = new ArrayList<StatusListener>();
        this.zai = new AtomicReference<zadb>();
        this.zaq = false;
        this.zab = new CallbackHandler<R>(looper);
        this.zac = new WeakReference<GoogleApiClient>(null);
    }
    
    @KeepForSdk
    public BasePendingResult(final GoogleApiClient referent) {
        this.zae = new Object();
        this.zaf = new CountDownLatch(1);
        this.zag = new ArrayList<StatusListener>();
        this.zai = new AtomicReference<zadb>();
        this.zaq = false;
        Looper looper;
        if (referent != null) {
            looper = referent.getLooper();
        }
        else {
            looper = Looper.getMainLooper();
        }
        this.zab = new CallbackHandler<R>(looper);
        this.zac = new WeakReference<GoogleApiClient>(referent);
    }
    
    @KeepForSdk
    @VisibleForTesting
    public BasePendingResult(final CallbackHandler<R> callbackHandler) {
        this.zae = new Object();
        this.zaf = new CountDownLatch(1);
        this.zag = new ArrayList<StatusListener>();
        this.zai = new AtomicReference<zadb>();
        this.zaq = false;
        this.zab = Preconditions.checkNotNull(callbackHandler, "CallbackHandler must not be null");
        this.zac = new WeakReference<GoogleApiClient>(null);
    }
    
    private final R zaa() {
        Object zae = this.zae;
        synchronized (zae) {
            Preconditions.checkState(this.zal ^ true, (Object)"Result has already been consumed.");
            Preconditions.checkState(this.isReady(), (Object)"Result is not ready.");
            final Result zaj = this.zaj;
            this.zaj = null;
            this.zah = null;
            this.zal = true;
            monitorexit(zae);
            zae = this.zai.getAndSet(null);
            if (zae != null) {
                ((zadb)zae).zaa.zab.remove(this);
            }
            return Preconditions.checkNotNull(zaj);
        }
    }
    
    private final void zab(final R zaj) {
        this.zaj = zaj;
        this.zak = zaj.getStatus();
        this.zao = null;
        this.zaf.countDown();
        if (this.zam) {
            this.zah = null;
        }
        else {
            final ResultCallback<? super R> zah = this.zah;
            if (zah == null) {
                if (this.zaj instanceof Releasable) {
                    this.mResultGuardian = new zas(this, null);
                }
            }
            else {
                ((Handler)this.zab).removeMessages(2);
                this.zab.zaa(zah, this.zaa());
            }
        }
        final ArrayList<StatusListener> zag = this.zag;
        for (int size = zag.size(), i = 0; i < size; ++i) {
            ((StatusListener)zag.get(i)).onComplete(this.zak);
        }
        this.zag.clear();
    }
    
    public static void zal(final Result obj) {
        if (obj instanceof Releasable) {
            try {
                ((Releasable)obj).release();
            }
            catch (final RuntimeException ex) {
                Log.w("BasePendingResult", "Unable to release ".concat(String.valueOf(obj)), (Throwable)ex);
            }
        }
    }
    
    @Override
    public final void addStatusListener(final StatusListener e) {
        Preconditions.checkArgument(e != null, (Object)"Callback cannot be null.");
        synchronized (this.zae) {
            if (this.isReady()) {
                e.onComplete(this.zak);
            }
            else {
                this.zag.add(e);
            }
        }
    }
    
    @Override
    public final R await() {
        Preconditions.checkNotMainThread("await must not be called on the UI thread");
        final boolean zal = this.zal;
        boolean b = true;
        Preconditions.checkState(zal ^ true, (Object)"Result has already been consumed");
        if (this.zap != null) {
            b = false;
        }
        Preconditions.checkState(b, (Object)"Cannot await if then() has been called.");
        try {
            this.zaf.await();
        }
        catch (final InterruptedException ex) {
            this.forceFailureUnlessReady(Status.RESULT_INTERRUPTED);
        }
        Preconditions.checkState(this.isReady(), (Object)"Result is not ready.");
        return this.zaa();
    }
    
    @Override
    public final R await(final long timeout, final TimeUnit unit) {
        if (timeout > 0L) {
            Preconditions.checkNotMainThread("await must not be called on the UI thread when time is greater than zero.");
        }
        final boolean zal = this.zal;
        boolean b = true;
        Preconditions.checkState(zal ^ true, (Object)"Result has already been consumed.");
        if (this.zap != null) {
            b = false;
        }
        Preconditions.checkState(b, (Object)"Cannot await if then() has been called.");
        try {
            if (!this.zaf.await(timeout, unit)) {
                this.forceFailureUnlessReady(Status.RESULT_TIMEOUT);
            }
        }
        catch (final InterruptedException ex) {
            this.forceFailureUnlessReady(Status.RESULT_INTERRUPTED);
        }
        Preconditions.checkState(this.isReady(), (Object)"Result is not ready.");
        return this.zaa();
    }
    
    @KeepForSdk
    @Override
    public void cancel() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/google/android/gms/common/api/internal/BasePendingResult.zae:Ljava/lang/Object;
        //     4: astore_1       
        //     5: aload_1        
        //     6: monitorenter   
        //     7: aload_0        
        //     8: getfield        com/google/android/gms/common/api/internal/BasePendingResult.zam:Z
        //    11: ifne            65
        //    14: aload_0        
        //    15: getfield        com/google/android/gms/common/api/internal/BasePendingResult.zal:Z
        //    18: ifeq            24
        //    21: goto            65
        //    24: aload_0        
        //    25: getfield        com/google/android/gms/common/api/internal/BasePendingResult.zao:Lcom/google/android/gms/common/internal/ICancelToken;
        //    28: astore_2       
        //    29: aload_2        
        //    30: ifnull          39
        //    33: aload_2        
        //    34: invokeinterface com/google/android/gms/common/internal/ICancelToken.cancel:()V
        //    39: aload_0        
        //    40: getfield        com/google/android/gms/common/api/internal/BasePendingResult.zaj:Lcom/google/android/gms/common/api/Result;
        //    43: invokestatic    com/google/android/gms/common/api/internal/BasePendingResult.zal:(Lcom/google/android/gms/common/api/Result;)V
        //    46: aload_0        
        //    47: iconst_1       
        //    48: putfield        com/google/android/gms/common/api/internal/BasePendingResult.zam:Z
        //    51: aload_0        
        //    52: aload_0        
        //    53: getstatic       com/google/android/gms/common/api/Status.RESULT_CANCELED:Lcom/google/android/gms/common/api/Status;
        //    56: invokevirtual   com/google/android/gms/common/api/internal/BasePendingResult.createFailedResult:(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
        //    59: invokespecial   com/google/android/gms/common/api/internal/BasePendingResult.zab:(Lcom/google/android/gms/common/api/Result;)V
        //    62: aload_1        
        //    63: monitorexit    
        //    64: return         
        //    65: aload_1        
        //    66: monitorexit    
        //    67: return         
        //    68: astore_2       
        //    69: aload_1        
        //    70: monitorexit    
        //    71: aload_2        
        //    72: athrow         
        //    73: astore_2       
        //    74: goto            39
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                        
        //  -----  -----  -----  -----  ----------------------------
        //  7      21     68     73     Any
        //  24     29     68     73     Any
        //  33     39     73     77     Landroid/os/RemoteException;
        //  33     39     68     73     Any
        //  39     64     68     73     Any
        //  65     67     68     73     Any
        //  69     71     68     73     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0039:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @KeepForSdk
    public abstract R createFailedResult(final Status p0);
    
    @Deprecated
    @KeepForSdk
    public final void forceFailureUnlessReady(final Status status) {
        synchronized (this.zae) {
            if (!this.isReady()) {
                this.setResult(this.createFailedResult(status));
                this.zan = true;
            }
        }
    }
    
    @Override
    public final boolean isCanceled() {
        synchronized (this.zae) {
            return this.zam;
        }
    }
    
    @KeepForSdk
    public final boolean isReady() {
        return this.zaf.getCount() == 0L;
    }
    
    @KeepForSdk
    public final void setCancelToken(final ICancelToken zao) {
        synchronized (this.zae) {
            this.zao = zao;
        }
    }
    
    @KeepForSdk
    public final void setResult(final R r) {
        synchronized (this.zae) {
            if (!this.zan && !this.zam) {
                this.isReady();
                Preconditions.checkState(this.isReady() ^ true, (Object)"Results have already been set");
                Preconditions.checkState(this.zal ^ true, (Object)"Result has already been consumed");
                this.zab(r);
                return;
            }
            zal(r);
        }
    }
    
    @KeepForSdk
    @Override
    public final void setResultCallback(final ResultCallback<? super R> zah) {
        final Object zae = this.zae;
        monitorenter(zae);
        Label_0022: {
            if (zah != null) {
                break Label_0022;
            }
            try {
                this.zah = null;
                return;
                while (true) {
                Label_0049:
                    while (true) {
                        while (true) {
                            break Label_0049;
                            this.zab.zaa(zah, this.zaa());
                            return;
                            final boolean zal = this.zal;
                            final boolean b = true;
                            Preconditions.checkState(zal ^ true, (Object)"Result has already been consumed.");
                            iftrue(Label_0047:)(this.zap != null);
                            continue;
                        }
                        Label_0089: {
                            this.zah = zah;
                        }
                        return;
                        boolean b = false;
                        Preconditions.checkState(b, (Object)"Cannot set callbacks if then() has been called.");
                        iftrue(Label_0067:)(!this.isCanceled());
                        return;
                        Label_0047:
                        b = false;
                        continue Label_0049;
                    }
                    Label_0067: {
                        iftrue(Label_0089:)(!this.isReady());
                    }
                    continue;
                }
            }
            finally {
                monitorexit(zae);
            }
        }
    }
    
    @KeepForSdk
    @Override
    public final void setResultCallback(final ResultCallback<? super R> zah, long millis, final TimeUnit timeUnit) {
        final Object zae = this.zae;
        monitorenter(zae);
        Label_0022: {
            if (zah != null) {
                break Label_0022;
            }
            try {
                this.zah = null;
                return;
                Label_0050: {
                    final boolean b = false;
                }
                while (true) {
                    while (true) {
                        break Label_0053;
                        this.zab.zaa(zah, this.zaa());
                        return;
                        final boolean zal = this.zal;
                        final boolean b = true;
                        Preconditions.checkState(zal ^ true, (Object)"Result has already been consumed.");
                        iftrue(Label_0050:)(this.zap != null);
                        Preconditions.checkState(b, (Object)"Cannot set callbacks if then() has been called.");
                        iftrue(Label_0072:)(!this.isCanceled());
                        return;
                        Label_0094:
                        this.zah = zah;
                        final CallbackHandler<R> zab = this.zab;
                        millis = timeUnit.toMillis(millis);
                        ((Handler)zab).sendMessageDelayed(((Handler)zab).obtainMessage(2, (Object)this), millis);
                        return;
                        continue;
                    }
                    Label_0072:
                    iftrue(Label_0094:)(!this.isReady());
                    continue;
                }
            }
            finally {
                monitorexit(zae);
            }
        }
    }
    
    @Override
    public final <S extends Result> TransformedResult<S> then(final ResultTransform<? super R, ? extends S> resultTransform) {
        Preconditions.checkState(this.zal ^ true, (Object)"Result has already been consumed.");
        synchronized (this.zae) {
            final zada<R> zap = this.zap;
            final boolean b = false;
            Preconditions.checkState(zap == null, (Object)"Cannot call then() twice.");
            boolean b2 = b;
            if (this.zah == null) {
                b2 = true;
            }
            Preconditions.checkState(b2, (Object)"Cannot call then() if callbacks are set.");
            Preconditions.checkState(this.zam ^ true, (Object)"Cannot call then() if result was canceled.");
            this.zaq = true;
            this.zap = new zada<R>(this.zac);
            final TransformedResult<Result> then = this.zap.then((ResultTransform<? super R, ? extends Result>)resultTransform);
            if (this.isReady()) {
                this.zab.zaa(this.zap, this.zaa());
            }
            else {
                this.zah = this.zap;
            }
            return (TransformedResult<S>)then;
        }
    }
    
    public final void zak() {
        final boolean zaq = this.zaq;
        boolean zaq2 = true;
        if (!zaq) {
            zaq2 = (BasePendingResult.zaa.get() && zaq2);
        }
        this.zaq = zaq2;
    }
    
    public final boolean zam() {
        synchronized (this.zae) {
            if (this.zac.get() == null || !this.zaq) {
                this.cancel();
            }
            return this.isCanceled();
        }
    }
    
    public final void zan(final zadb newValue) {
        this.zai.set(newValue);
    }
    
    @VisibleForTesting
    public static class CallbackHandler<R extends Result> extends com.google.android.gms.internal.base.zaq
    {
        public CallbackHandler() {
            super(Looper.getMainLooper());
        }
        
        public CallbackHandler(final Looper looper) {
            super(looper);
        }
        
        public final void handleMessage(Message message) {
            final int what = message.what;
            if (what != 1) {
                if (what != 2) {
                    final StringBuilder sb = new StringBuilder(45);
                    sb.append("Don't know how to handle message: ");
                    sb.append(what);
                    Log.wtf("BasePendingResult", sb.toString(), (Throwable)new Exception());
                    return;
                }
                ((BasePendingResult)message.obj).forceFailureUnlessReady(Status.RESULT_TIMEOUT);
            }
            else {
                final Pair pair = (Pair)message.obj;
                final ResultCallback resultCallback = (ResultCallback)pair.first;
                message = (Message)pair.second;
                try {
                    resultCallback.onResult((Result)message);
                }
                catch (final RuntimeException ex) {
                    BasePendingResult.zal((Result)message);
                    throw ex;
                }
            }
        }
        
        public final void zaa(final ResultCallback<? super R> resultCallback, final R r) {
            final int zad = BasePendingResult.zad;
            ((Handler)this).sendMessage(((Handler)this).obtainMessage(1, (Object)new Pair((Object)Preconditions.checkNotNull(resultCallback), (Object)r)));
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import android.app.Dialog;
import com.google.android.gms.common.ConnectionResult;
import android.content.DialogInterface$OnCancelListener;
import android.content.Context;
import com.google.android.gms.common.api.GoogleApiActivity;
import com.google.android.gms.common.internal.Preconditions;
import android.app.PendingIntent;

final class zao implements Runnable
{
    final zap zaa;
    private final zam zab;
    
    public zao(final zap zaa, final zam zab) {
        this.zaa = zaa;
        this.zab = zab;
    }
    
    @Override
    public final void run() {
        if (!this.zaa.zaa) {
            return;
        }
        final ConnectionResult zab = this.zab.zab();
        if (zab.hasResolution()) {
            final zap zaa = this.zaa;
            zaa.mLifecycleFragment.startActivityForResult(GoogleApiActivity.zaa((Context)zaa.getActivity(), Preconditions.checkNotNull(zab.getResolution()), this.zab.zaa(), false), 1);
            return;
        }
        final zap zaa2 = this.zaa;
        if (zaa2.zac.getErrorResolutionIntent((Context)zaa2.getActivity(), zab.getErrorCode(), null) != null) {
            final zap zaa3 = this.zaa;
            zaa3.zac.zag(zaa3.getActivity(), this.zaa.mLifecycleFragment, zab.getErrorCode(), 2, (DialogInterface$OnCancelListener)this.zaa);
            return;
        }
        if (zab.getErrorCode() == 18) {
            final zap zaa4 = this.zaa;
            final Dialog zab2 = zaa4.zac.zab(zaa4.getActivity(), (DialogInterface$OnCancelListener)this.zaa);
            final zap zaa5 = this.zaa;
            zaa5.zac.zac(((Context)zaa5.getActivity()).getApplicationContext(), new zan(this, zab2));
            return;
        }
        zap.zaf(this.zaa, zab, this.zab.zaa());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import android.os.Handler;
import com.google.android.gms.internal.base.zaq;
import java.io.PrintWriter;
import java.io.FileDescriptor;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.Objects;
import java.util.concurrent.TimeUnit;
import com.google.android.gms.internal.base.zal;
import android.app.PendingIntent;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.api.Result;
import android.util.Log;
import java.util.Iterator;
import java.util.Collections;
import java.util.WeakHashMap;
import java.util.ArrayList;
import com.google.android.gms.signin.SignInOptions;
import com.google.android.gms.signin.zae;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import java.util.concurrent.locks.Lock;
import com.google.android.gms.common.ConnectionResult;
import android.os.Bundle;
import java.util.Set;
import com.google.android.gms.common.api.Api;
import java.util.Map;
import android.os.Looper;
import android.content.Context;

final class zaaa implements zaca
{
    private final Context zaa;
    private final zabe zab;
    private final Looper zac;
    private final zabi zad;
    private final zabi zae;
    private final Map<Api.AnyClientKey<?>, zabi> zaf;
    private final Set<SignInConnectionListener> zag;
    private final Api.Client zah;
    private Bundle zai;
    private ConnectionResult zaj;
    private ConnectionResult zak;
    private boolean zal;
    private final Lock zam;
    private int zan;
    
    private zaaa(final Context zaa, final zabe zab, final Lock zam, final Looper zac, final GoogleApiAvailabilityLight googleApiAvailabilityLight, final Map<Api.AnyClientKey<?>, Api.Client> map, final Map<Api.AnyClientKey<?>, Api.Client> map2, final ClientSettings clientSettings, final Api.AbstractClientBuilder<? extends zae, SignInOptions> abstractClientBuilder, final Api.Client zah, final ArrayList<zat> list, final ArrayList<zat> list2, final Map<Api<?>, Boolean> map3, final Map<Api<?>, Boolean> map4) {
        this.zag = Collections.newSetFromMap(new WeakHashMap<SignInConnectionListener, Boolean>());
        this.zaj = null;
        this.zak = null;
        this.zal = false;
        this.zan = 0;
        this.zaa = zaa;
        this.zab = zab;
        this.zam = zam;
        this.zac = zac;
        this.zah = zah;
        this.zad = new zabi(zaa, zab, zam, zac, googleApiAvailabilityLight, map2, null, map4, null, list2, new zax(this, null));
        this.zae = new zabi(zaa, zab, zam, zac, googleApiAvailabilityLight, map, clientSettings, map3, abstractClientBuilder, list, new zaz(this, null));
        final r8 m = new r8();
        final Iterator<Api.AnyClientKey<?>> iterator = map2.keySet().iterator();
        while (iterator.hasNext()) {
            m.put(iterator.next(), this.zad);
        }
        final Iterator<Api.AnyClientKey<?>> iterator2 = map.keySet().iterator();
        while (iterator2.hasNext()) {
            m.put(iterator2.next(), this.zae);
        }
        this.zaf = (Map<Api.AnyClientKey<?>, zabi>)Collections.unmodifiableMap((Map<?, ?>)m);
    }
    
    private final void zaA(final ConnectionResult connectionResult) {
        final int zan = this.zan;
        Label_0045: {
            if (zan != 1) {
                if (zan != 2) {
                    Log.wtf("CompositeGAC", "Attempted to call failure callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", (Throwable)new Exception());
                    break Label_0045;
                }
                this.zab.zaa(connectionResult);
            }
            this.zaB();
        }
        this.zan = 0;
    }
    
    private final void zaB() {
        final Iterator<SignInConnectionListener> iterator = this.zag.iterator();
        while (iterator.hasNext()) {
            iterator.next().onComplete();
        }
        this.zag.clear();
    }
    
    private final boolean zaC() {
        final ConnectionResult zak = this.zak;
        return zak != null && zak.getErrorCode() == 4;
    }
    
    private final boolean zaD(final BaseImplementation.ApiMethodImpl<? extends Result, ? extends Api.AnyClient> apiMethodImpl) {
        final zabi zabi = this.zaf.get(apiMethodImpl.getClientKey());
        Preconditions.checkNotNull(zabi, "GoogleApiClient is not configured to use the API required for this call.");
        return zabi.equals(this.zae);
    }
    
    private static boolean zaE(final ConnectionResult connectionResult) {
        return connectionResult != null && connectionResult.isSuccess();
    }
    
    public static zaaa zag(final Context context, final zabe zabe, final Lock lock, final Looper looper, final GoogleApiAvailabilityLight googleApiAvailabilityLight, final Map<Api.AnyClientKey<?>, Api.Client> map, final ClientSettings clientSettings, final Map<Api<?>, Boolean> map2, final Api.AbstractClientBuilder<? extends zae, SignInOptions> abstractClientBuilder, final ArrayList<zat> list) {
        final r8 r8 = new r8();
        final r8 r9 = new r8();
        final Iterator<Map.Entry<Api.AnyClientKey<?>, Api.Client>> iterator = map.entrySet().iterator();
        Api.Client client = null;
        while (iterator.hasNext()) {
            final Map.Entry<K, Api.Client> entry = (Map.Entry<K, Api.Client>)iterator.next();
            final Api.Client client2 = entry.getValue();
            if (client2.providesSignIn()) {
                client = client2;
            }
            final boolean requiresSignIn = client2.requiresSignIn();
            final Api.AnyClientKey anyClientKey = (Api.AnyClientKey)entry.getKey();
            if (requiresSignIn) {
                r8.put(anyClientKey, client2);
            }
            else {
                r9.put(anyClientKey, client2);
            }
        }
        Preconditions.checkState(r8.isEmpty() ^ true, (Object)"CompositeGoogleApiClient should not be used without any APIs that require sign-in.");
        final r8 r10 = new r8();
        final r8 r11 = new r8();
        for (final Api api : map2.keySet()) {
            final Api.AnyClientKey zab = api.zab();
            if (r8.containsKey(zab)) {
                r10.put(api, map2.get(api));
            }
            else {
                if (!r9.containsKey(zab)) {
                    throw new IllegalStateException("Each API in the isOptionalMap must have a corresponding client in the clients map.");
                }
                r11.put(api, map2.get(api));
            }
        }
        final ArrayList<zat> list2 = new ArrayList<zat>();
        final ArrayList<zat> list3 = new ArrayList<zat>();
        for (int size = list.size(), i = 0; i < size; ++i) {
            final zat zat = (zat)list.get(i);
            if (r10.containsKey(zat.zaa)) {
                list2.add(zat);
            }
            else {
                if (!r11.containsKey(zat.zaa)) {
                    throw new IllegalStateException("Each ClientCallbacks must have a corresponding API in the isOptionalMap");
                }
                list3.add(zat);
            }
        }
        return new zaaa(context, zabe, lock, looper, googleApiAvailabilityLight, r8, r9, clientSettings, abstractClientBuilder, client, list2, list3, r10, r11);
    }
    
    private final PendingIntent zaz() {
        if (this.zah == null) {
            return null;
        }
        return com.google.android.gms.internal.base.zal.zaa(this.zaa, System.identityHashCode(this.zab), this.zah.getSignInIntent(), com.google.android.gms.internal.base.zal.zaa | 0x8000000);
    }
    
    @Override
    public final ConnectionResult zab() {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public final ConnectionResult zac(final long n, final TimeUnit timeUnit) {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public final ConnectionResult zad(final Api<?> api) {
        if (!Objects.equal(this.zaf.get(api.zab()), this.zae)) {
            return this.zad.zad(api);
        }
        if (this.zaC()) {
            return new ConnectionResult(4, this.zaz());
        }
        return this.zae.zad(api);
    }
    
    @Override
    public final <A extends Api.AnyClient, R extends Result, T extends BaseImplementation.ApiMethodImpl<R, A>> T zae(final T t) {
        if (!this.zaD(t)) {
            this.zad.zae(t);
            return t;
        }
        if (this.zaC()) {
            ((BaseImplementation.ApiMethodImpl)t).setFailedResult(new Status(4, null, this.zaz()));
            return t;
        }
        this.zae.zae(t);
        return t;
    }
    
    @Override
    public final <A extends Api.AnyClient, T extends BaseImplementation.ApiMethodImpl<? extends Result, A>> T zaf(final T t) {
        if (!this.zaD(t)) {
            return this.zad.zaf(t);
        }
        if (this.zaC()) {
            ((BaseImplementation.ApiMethodImpl)t).setFailedResult(new Status(4, null, this.zaz()));
            return t;
        }
        return this.zae.zaf(t);
    }
    
    @Override
    public final void zaq() {
        this.zan = 2;
        this.zal = false;
        this.zak = null;
        this.zaj = null;
        this.zad.zaq();
        this.zae.zaq();
    }
    
    @Override
    public final void zar() {
        this.zak = null;
        this.zaj = null;
        this.zan = 0;
        this.zad.zar();
        this.zae.zar();
        this.zaB();
    }
    
    @Override
    public final void zas(final String s, final FileDescriptor fileDescriptor, final PrintWriter printWriter, final String[] array) {
        printWriter.append(s).append("authClient").println(":");
        this.zae.zas(String.valueOf(s).concat("  "), fileDescriptor, printWriter, array);
        printWriter.append(s).append("anonClient").println(":");
        this.zad.zas(String.valueOf(s).concat("  "), fileDescriptor, printWriter, array);
    }
    
    @Override
    public final void zat() {
        this.zad.zat();
        this.zae.zat();
    }
    
    @Override
    public final void zau() {
        this.zam.lock();
        try {
            final boolean zax = this.zax();
            this.zae.zar();
            this.zak = new ConnectionResult(4);
            if (zax) {
                ((Handler)new zaq(this.zac)).post((Runnable)new zav(this));
            }
            else {
                this.zaB();
            }
        }
        finally {
            this.zam.unlock();
        }
    }
    
    @Override
    public final boolean zaw() {
        this.zam.lock();
        try {
            final boolean zaw = this.zad.zaw();
            boolean b = false;
            if (zaw) {
                if (!this.zae.zaw() && !this.zaC()) {
                    final int zan = this.zan;
                    b = b;
                    if (zan != 1) {
                        return b;
                    }
                }
                b = true;
            }
            return b;
        }
        finally {
            this.zam.unlock();
        }
    }
    
    @Override
    public final boolean zax() {
        this.zam.lock();
        try {
            return this.zan == 2;
        }
        finally {
            this.zam.unlock();
        }
    }
    
    @Override
    public final boolean zay(final SignInConnectionListener signInConnectionListener) {
        this.zam.lock();
        try {
            if ((this.zax() || this.zaw()) && !this.zae.zaw()) {
                this.zag.add(signInConnectionListener);
                if (this.zan == 0) {
                    this.zan = 1;
                }
                this.zak = null;
                this.zae.zaq();
                return true;
            }
            return false;
        }
        finally {
            this.zam.unlock();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.errorprone.annotations.ResultIgnorabilityUnspecified;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.VisibleForTesting;
import android.content.res.Resources;
import android.text.TextUtils;
import com.google.android.gms.common.internal.StringResourceValueReader;
import com.google.android.gms.common.internal.zzah;
import com.google.android.gms.common.R;
import android.content.Context;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.annotation.KeepForSdk;

@Deprecated
@KeepForSdk
public final class GoogleServices
{
    private static final Object zza;
    private static GoogleServices zzb;
    private final String zzc;
    private final Status zzd;
    private final boolean zze;
    private final boolean zzf;
    
    static {
        zza = new Object();
    }
    
    @KeepForSdk
    @VisibleForTesting
    public GoogleServices(final Context context) {
        final Resources resources = context.getResources();
        final int identifier = resources.getIdentifier("google_app_measurement_enable", "integer", resources.getResourcePackageName(R.string.common_google_play_services_unknown_issue));
        final boolean b = true;
        boolean zze = true;
        if (identifier != 0) {
            final int integer = resources.getInteger(identifier);
            final boolean zzf = integer == 0;
            if (integer == 0) {
                zze = false;
            }
            this.zzf = zzf;
        }
        else {
            this.zzf = false;
            zze = b;
        }
        this.zze = zze;
        String zzc;
        if ((zzc = zzah.zzb(context)) == null) {
            zzc = new StringResourceValueReader(context).getString("google_app_id");
        }
        if (TextUtils.isEmpty((CharSequence)zzc)) {
            this.zzd = new Status(10, "Missing google app id value from from string resources with name google_app_id.");
            this.zzc = null;
            return;
        }
        this.zzc = zzc;
        this.zzd = Status.RESULT_SUCCESS;
    }
    
    @KeepForSdk
    @VisibleForTesting
    public GoogleServices(final String zzc, final boolean zze) {
        this.zzc = zzc;
        this.zzd = Status.RESULT_SUCCESS;
        this.zze = zze;
        this.zzf = (zze ^ true);
    }
    
    @KeepForSdk
    private static GoogleServices checkInitialized(final String str) {
        synchronized (GoogleServices.zza) {
            final GoogleServices zzb = GoogleServices.zzb;
            if (zzb != null) {
                return zzb;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Initialize must be called before ");
            sb.append(str);
            sb.append(".");
            throw new IllegalStateException(sb.toString());
        }
    }
    
    @KeepForSdk
    @VisibleForTesting
    public static void clearInstanceForTest() {
        synchronized (GoogleServices.zza) {
            GoogleServices.zzb = null;
        }
    }
    
    @KeepForSdk
    public static String getGoogleAppId() {
        return checkInitialized("getGoogleAppId").zzc;
    }
    
    @KeepForSdk
    public static Status initialize(final Context context) {
        Preconditions.checkNotNull(context, "Context must not be null.");
        synchronized (GoogleServices.zza) {
            if (GoogleServices.zzb == null) {
                GoogleServices.zzb = new GoogleServices(context);
            }
            return GoogleServices.zzb.zzd;
        }
    }
    
    @ResultIgnorabilityUnspecified
    @KeepForSdk
    public static Status initialize(final Context context, final String s, final boolean b) {
        Preconditions.checkNotNull(context, "Context must not be null.");
        Preconditions.checkNotEmpty(s, "App ID must be nonempty.");
        synchronized (GoogleServices.zza) {
            final GoogleServices zzb = GoogleServices.zzb;
            if (zzb != null) {
                return zzb.checkGoogleAppId(s);
            }
            return (GoogleServices.zzb = new GoogleServices(s, b)).zzd;
        }
    }
    
    @KeepForSdk
    public static boolean isMeasurementEnabled() {
        final GoogleServices checkInitialized = checkInitialized("isMeasurementEnabled");
        return checkInitialized.zzd.isSuccess() && checkInitialized.zze;
    }
    
    @KeepForSdk
    public static boolean isMeasurementExplicitlyDisabled() {
        return checkInitialized("isMeasurementExplicitlyDisabled").zzf;
    }
    
    @KeepForSdk
    @VisibleForTesting
    public Status checkGoogleAppId(final String anObject) {
        final String zzc = this.zzc;
        if (zzc != null && !zzc.equals(anObject)) {
            final String zzc2 = this.zzc;
            final StringBuilder sb = new StringBuilder();
            sb.append("Initialize was called with two different Google App IDs.  Only the first app ID will be used: '");
            sb.append(zzc2);
            sb.append("'.");
            return new Status(10, sb.toString());
        }
        return Status.RESULT_SUCCESS;
    }
}

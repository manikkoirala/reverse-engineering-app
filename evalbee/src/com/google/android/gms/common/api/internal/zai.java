// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Status;
import android.os.RemoteException;

public abstract class zai
{
    public final int zac;
    
    public zai(final int zac) {
        this.zac = zac;
    }
    
    public abstract void zad(final Status p0);
    
    public abstract void zae(final Exception p0);
    
    public abstract void zaf(final zabq<?> p0);
    
    public abstract void zag(final zaad p0, final boolean p1);
}

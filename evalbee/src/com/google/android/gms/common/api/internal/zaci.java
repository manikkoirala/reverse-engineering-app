// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Api;

public final class zaci
{
    public final RegisterListenerMethod<Api.AnyClient, ?> zaa;
    public final UnregisterListenerMethod<Api.AnyClient, ?> zab;
    public final Runnable zac;
    
    public zaci(final RegisterListenerMethod<Api.AnyClient, ?> zaa, final UnregisterListenerMethod<Api.AnyClient, ?> zab, final Runnable zac) {
        this.zaa = zaa;
        this.zab = zab;
        this.zac = zac;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.signin.internal.zak;
import java.lang.ref.WeakReference;
import com.google.android.gms.signin.internal.zac;

final class zaar extends zac
{
    private final WeakReference<zaaw> zaa;
    
    public zaar(final zaaw referent) {
        this.zaa = new WeakReference<zaaw>(referent);
    }
    
    public final void zab(final zak zak) {
        final zaaw zaaw = this.zaa.get();
        if (zaaw == null) {
            return;
        }
        com.google.android.gms.common.api.internal.zaaw.zak(zaaw).zal(new zaaq(this, zaaw, zaaw, zak));
    }
}

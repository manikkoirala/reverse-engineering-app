// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import androidx.fragment.app.e;
import android.content.ContextWrapper;
import com.google.android.gms.common.internal.Preconditions;
import android.app.Activity;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public class LifecycleActivity
{
    private final Object zza;
    
    public LifecycleActivity(final Activity zza) {
        Preconditions.checkNotNull(zza, "Activity must not be null");
        this.zza = zza;
    }
    
    @KeepForSdk
    public LifecycleActivity(final ContextWrapper contextWrapper) {
        throw new UnsupportedOperationException();
    }
    
    public final Activity zza() {
        return (Activity)this.zza;
    }
    
    public final e zzb() {
        return (e)this.zza;
    }
    
    public final boolean zzc() {
        return this.zza instanceof Activity;
    }
    
    public final boolean zzd() {
        return this.zza instanceof e;
    }
}

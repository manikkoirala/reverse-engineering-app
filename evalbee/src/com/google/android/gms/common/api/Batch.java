// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api;

import java.util.ArrayList;
import java.util.List;
import com.google.android.gms.common.api.internal.BasePendingResult;

public final class Batch extends BasePendingResult<BatchResult>
{
    private int zae;
    private boolean zaf;
    private boolean zag;
    private final PendingResult<?>[] zah;
    private final Object zai = new Object();
    
    @Override
    public void cancel() {
        super.cancel();
        final PendingResult<?>[] zah = this.zah;
        for (int length = zah.length, i = 0; i < length; ++i) {
            zah[i].cancel();
        }
    }
    
    @Override
    public BatchResult createFailedResult(final Status status) {
        return new BatchResult(status, this.zah);
    }
    
    public static final class Builder
    {
        private List<PendingResult<?>> zaa;
        private GoogleApiClient zab;
        
        public Builder(final GoogleApiClient zab) {
            this.zaa = new ArrayList<PendingResult<?>>();
            this.zab = zab;
        }
        
        public <R extends Result> BatchResultToken<R> add(final PendingResult<R> pendingResult) {
            final BatchResultToken batchResultToken = new BatchResultToken(this.zaa.size());
            this.zaa.add(pendingResult);
            return batchResultToken;
        }
        
        public Batch build() {
            return new Batch(this.zaa, this.zab, null);
        }
    }
}

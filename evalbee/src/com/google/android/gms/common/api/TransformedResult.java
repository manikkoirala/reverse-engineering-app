// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api;

public abstract class TransformedResult<R extends Result>
{
    public abstract void andFinally(final ResultCallbacks<? super R> p0);
    
    public abstract <S extends Result> TransformedResult<S> then(final ResultTransform<? super R, ? extends S> p0);
}

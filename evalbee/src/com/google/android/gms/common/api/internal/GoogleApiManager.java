// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.HasApiKey;
import com.google.android.gms.common.internal.RootTelemetryConfiguration;
import com.google.android.gms.common.internal.RootTelemetryConfigManager;
import java.util.Collection;
import java.util.Iterator;
import android.app.Application;
import com.google.android.gms.common.api.Api;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import com.google.android.gms.common.internal.MethodInvocation;
import android.util.Log;
import android.os.Message;
import com.google.android.gms.common.internal.GmsClientSupervisor;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.OnCompleteListener;
import java.util.concurrent.Executor;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.common.internal.TelemetryLogging;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.util.DeviceProperties;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import com.google.android.gms.internal.base.zaq;
import java.util.concurrent.ConcurrentHashMap;
import android.os.Looper;
import org.checkerframework.checker.initialization.qual.NotOnlyInitialized;
import android.os.Handler;
import java.util.Set;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import com.google.android.gms.common.internal.zal;
import com.google.android.gms.common.GoogleApiAvailability;
import android.content.Context;
import com.google.android.gms.common.internal.TelemetryLoggingClient;
import com.google.android.gms.common.internal.TelemetryData;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.ShowFirstParty;
import com.google.android.gms.common.annotation.KeepForSdk;
import android.os.Handler$Callback;

@KeepForSdk
@ShowFirstParty
public class GoogleApiManager implements Handler$Callback
{
    public static final Status zaa;
    private static final Status zab;
    private static final Object zac;
    private static GoogleApiManager zad;
    private long zae;
    private long zaf;
    private long zag;
    private boolean zah;
    private TelemetryData zai;
    private TelemetryLoggingClient zaj;
    private final Context zak;
    private final GoogleApiAvailability zal;
    private final zal zam;
    private final AtomicInteger zan;
    private final AtomicInteger zao;
    private final Map<ApiKey<?>, zabq<?>> zap;
    private zaae zaq;
    private final Set<ApiKey<?>> zar;
    private final Set<ApiKey<?>> zas;
    @NotOnlyInitialized
    private final Handler zat;
    private volatile boolean zau;
    
    static {
        zaa = new Status(4, "Sign-out occurred while this API call was in progress.");
        zab = new Status(4, "The user must be signed in to make this API call.");
        zac = new Object();
    }
    
    @KeepForSdk
    private GoogleApiManager(final Context zak, final Looper looper, final GoogleApiAvailability zal) {
        this.zae = 5000L;
        this.zaf = 120000L;
        this.zag = 10000L;
        this.zah = false;
        this.zan = new AtomicInteger(1);
        this.zao = new AtomicInteger(0);
        this.zap = new ConcurrentHashMap<ApiKey<?>, zabq<?>>(5, 0.75f, 1);
        this.zaq = null;
        this.zar = new s8();
        this.zas = new s8();
        this.zau = true;
        this.zak = zak;
        final zaq zat = new zaq(looper, (Handler$Callback)this);
        this.zat = (Handler)zat;
        this.zal = zal;
        this.zam = new zal(zal);
        if (DeviceProperties.isAuto(zak)) {
            this.zau = false;
        }
        ((Handler)zat).sendMessage(((Handler)zat).obtainMessage(6));
    }
    
    @KeepForSdk
    public static void reportSignOut() {
        synchronized (GoogleApiManager.zac) {
            final GoogleApiManager zad = GoogleApiManager.zad;
            if (zad != null) {
                zad.zao.incrementAndGet();
                final Handler zat = zad.zat;
                zat.sendMessageAtFrontOfQueue(zat.obtainMessage(10));
            }
        }
    }
    
    private static Status zaH(final ApiKey<?> apiKey, final ConnectionResult obj) {
        final String zab = apiKey.zab();
        final String value = String.valueOf(obj);
        final StringBuilder sb = new StringBuilder(String.valueOf(zab).length() + 63 + value.length());
        sb.append("API: ");
        sb.append(zab);
        sb.append(" is not available on this device. Connection failed with: ");
        sb.append(value);
        return new Status(obj, sb.toString());
    }
    
    private final zabq<?> zaI(final GoogleApi<?> googleApi) {
        final ApiKey<O> apiKey = googleApi.getApiKey();
        zabq zabq;
        if ((zabq = this.zap.get(apiKey)) == null) {
            zabq = new zabq(this, googleApi);
            this.zap.put(apiKey, zabq);
        }
        if (zabq.zaz()) {
            this.zas.add(apiKey);
        }
        zabq.zao();
        return zabq;
    }
    
    private final TelemetryLoggingClient zaJ() {
        if (this.zaj == null) {
            this.zaj = TelemetryLogging.getClient(this.zak);
        }
        return this.zaj;
    }
    
    private final void zaK() {
        final TelemetryData zai = this.zai;
        if (zai != null) {
            if (zai.zaa() > 0 || this.zaF()) {
                this.zaJ().log(zai);
            }
            this.zai = null;
        }
    }
    
    private final <T> void zaL(final TaskCompletionSource<T> taskCompletionSource, final int n, final GoogleApi googleApi) {
        if (n != 0) {
            final zacd<Object> zaa = zacd.zaa(this, n, googleApi.getApiKey());
            if (zaa != null) {
                final Task task = taskCompletionSource.getTask();
                final Handler zat = this.zat;
                zat.getClass();
                task.addOnCompleteListener((Executor)new zabk(zat), (OnCompleteListener)zaa);
            }
        }
    }
    
    public static GoogleApiManager zal() {
        synchronized (GoogleApiManager.zac) {
            Preconditions.checkNotNull(GoogleApiManager.zad, "Must guarantee manager is non-null before using getInstance");
            return GoogleApiManager.zad;
        }
    }
    
    public static GoogleApiManager zam(final Context context) {
        synchronized (GoogleApiManager.zac) {
            if (GoogleApiManager.zad == null) {
                GoogleApiManager.zad = new GoogleApiManager(context.getApplicationContext(), GmsClientSupervisor.getOrStartHandlerThread().getLooper(), GoogleApiAvailability.getInstance());
            }
            return GoogleApiManager.zad;
        }
    }
    
    public final boolean handleMessage(final Message message) {
        final int what = message.what;
        long zag = 300000L;
        final zabq zabq = null;
        switch (what) {
            default: {
                final StringBuilder sb = new StringBuilder(31);
                sb.append("Unknown message id: ");
                sb.append(what);
                Log.w("GoogleApiManager", sb.toString());
                return false;
            }
            case 19: {
                this.zah = false;
                break;
            }
            case 18: {
                final zace zace = (zace)message.obj;
                if (zace.zac == 0L) {
                    this.zaJ().log(new TelemetryData(zace.zab, Arrays.asList(zace.zaa)));
                    break;
                }
                final TelemetryData zai = this.zai;
                if (zai != null) {
                    final List<MethodInvocation> zab = zai.zab();
                    if (zai.zaa() == zace.zab && (zab == null || zab.size() < zace.zad)) {
                        this.zai.zac(zace.zaa);
                    }
                    else {
                        this.zat.removeMessages(17);
                        this.zaK();
                    }
                }
                if (this.zai == null) {
                    final ArrayList<MethodInvocation> list = new ArrayList<MethodInvocation>();
                    list.add(zace.zaa);
                    this.zai = new TelemetryData(zace.zab, list);
                    final Handler zat = this.zat;
                    zat.sendMessageDelayed(zat.obtainMessage(17), zace.zac);
                    break;
                }
                break;
            }
            case 17: {
                this.zaK();
                break;
            }
            case 16: {
                final zabs zabs = (zabs)message.obj;
                if (this.zap.containsKey(com.google.android.gms.common.api.internal.zabs.zab(zabs))) {
                    com.google.android.gms.common.api.internal.zabq.zam((zabq<Api.ApiOptions>)this.zap.get(com.google.android.gms.common.api.internal.zabs.zab(zabs)), zabs);
                    break;
                }
                break;
            }
            case 15: {
                final zabs zabs2 = (zabs)message.obj;
                if (this.zap.containsKey(zabs.zab(zabs2))) {
                    com.google.android.gms.common.api.internal.zabq.zal((zabq<Api.ApiOptions>)this.zap.get(zabs.zab(zabs2)), zabs2);
                    break;
                }
                break;
            }
            case 14: {
                final zaaf zaaf = (zaaf)message.obj;
                final ApiKey<?> zaa = zaaf.zaa();
                TaskCompletionSource<Boolean> taskCompletionSource;
                Boolean result;
                if (!this.zap.containsKey(zaa)) {
                    taskCompletionSource = zaaf.zab();
                    result = Boolean.FALSE;
                }
                else {
                    final boolean zax = com.google.android.gms.common.api.internal.zabq.zax((zabq<Api.ApiOptions>)this.zap.get(zaa), false);
                    taskCompletionSource = zaaf.zab();
                    result = zax;
                }
                taskCompletionSource.setResult((Object)result);
                break;
            }
            case 12: {
                if (this.zap.containsKey(message.obj)) {
                    this.zap.get(message.obj).zaA();
                    break;
                }
                break;
            }
            case 11: {
                if (this.zap.containsKey(message.obj)) {
                    this.zap.get(message.obj).zaw();
                    break;
                }
                break;
            }
            case 10: {
                final Iterator<ApiKey<?>> iterator = this.zas.iterator();
                while (iterator.hasNext()) {
                    final zabq zabq2 = this.zap.remove(iterator.next());
                    if (zabq2 != null) {
                        zabq2.zav();
                    }
                }
                this.zas.clear();
                break;
            }
            case 9: {
                if (this.zap.containsKey(message.obj)) {
                    this.zap.get(message.obj).zau();
                    break;
                }
                break;
            }
            case 7: {
                this.zaI((GoogleApi<?>)message.obj);
                break;
            }
            case 6: {
                if (!(this.zak.getApplicationContext() instanceof Application)) {
                    break;
                }
                BackgroundDetector.initialize((Application)this.zak.getApplicationContext());
                BackgroundDetector.getInstance().addListener((BackgroundDetector.BackgroundStateChangeListener)new zabl(this));
                if (!BackgroundDetector.getInstance().readCurrentStateIfPossible(true)) {
                    this.zag = 300000L;
                    break;
                }
                break;
            }
            case 5: {
                final int arg1 = message.arg1;
                final ConnectionResult connectionResult = (ConnectionResult)message.obj;
                final Iterator<zabq<?>> iterator2 = this.zap.values().iterator();
                zabq zabq3;
                do {
                    zabq3 = zabq;
                    if (!iterator2.hasNext()) {
                        break;
                    }
                    zabq3 = iterator2.next();
                } while (zabq3.zab() != arg1);
                if (zabq3 == null) {
                    final StringBuilder sb2 = new StringBuilder(76);
                    sb2.append("Could not find API instance ");
                    sb2.append(arg1);
                    sb2.append(" while trying to fail enqueued calls.");
                    Log.wtf("GoogleApiManager", sb2.toString(), (Throwable)new Exception());
                    break;
                }
                if (connectionResult.getErrorCode() == 13) {
                    final String errorString = this.zal.getErrorString(connectionResult.getErrorCode());
                    final String errorMessage = connectionResult.getErrorMessage();
                    final StringBuilder sb3 = new StringBuilder(String.valueOf(errorString).length() + 69 + String.valueOf(errorMessage).length());
                    sb3.append("Error resolution was canceled by the user, original error message: ");
                    sb3.append(errorString);
                    sb3.append(": ");
                    sb3.append(errorMessage);
                    com.google.android.gms.common.api.internal.zabq.zai((zabq<Api.ApiOptions>)zabq3, new Status(17, sb3.toString()));
                    break;
                }
                com.google.android.gms.common.api.internal.zabq.zai((zabq<Api.ApiOptions>)zabq3, zaH(com.google.android.gms.common.api.internal.zabq.zag((zabq<Api.ApiOptions>)zabq3), connectionResult));
                break;
            }
            case 4:
            case 8:
            case 13: {
                final zach zach = (zach)message.obj;
                zabq<?> zaI;
                if ((zaI = this.zap.get(zach.zac.getApiKey())) == null) {
                    zaI = this.zaI(zach.zac);
                }
                if (zaI.zaz() && this.zao.get() != zach.zab) {
                    zach.zaa.zad(GoogleApiManager.zaa);
                    zaI.zav();
                    break;
                }
                zaI.zap(zach.zaa);
                break;
            }
            case 3: {
                for (final zabq zabq4 : this.zap.values()) {
                    zabq4.zan();
                    zabq4.zao();
                }
                break;
            }
            case 2: {
                final com.google.android.gms.common.api.internal.zal zal = (com.google.android.gms.common.api.internal.zal)message.obj;
                for (final ApiKey apiKey : zal.zab()) {
                    final zabq zabq5 = this.zap.get(apiKey);
                    if (zabq5 == null) {
                        zal.zac(apiKey, new ConnectionResult(13), null);
                        break;
                    }
                    if (zabq5.zay()) {
                        zal.zac(apiKey, ConnectionResult.RESULT_SUCCESS, zabq5.zaf().getEndpointPackageName());
                    }
                    else {
                        final ConnectionResult zad = zabq5.zad();
                        if (zad != null) {
                            zal.zac(apiKey, zad, null);
                        }
                        else {
                            zabq5.zat(zal);
                            zabq5.zao();
                        }
                    }
                }
                break;
            }
            case 1: {
                if (message.obj) {
                    zag = 10000L;
                }
                this.zag = zag;
                this.zat.removeMessages(12);
                for (final ApiKey apiKey2 : this.zap.keySet()) {
                    final Handler zat2 = this.zat;
                    zat2.sendMessageDelayed(zat2.obtainMessage(12, (Object)apiKey2), this.zag);
                }
                break;
            }
        }
        return true;
    }
    
    public final void zaA() {
        final Handler zat = this.zat;
        zat.sendMessage(zat.obtainMessage(3));
    }
    
    public final void zaB(final GoogleApi<?> googleApi) {
        final Handler zat = this.zat;
        zat.sendMessage(zat.obtainMessage(7, (Object)googleApi));
    }
    
    public final void zaC(final zaae zaq) {
        synchronized (GoogleApiManager.zac) {
            if (this.zaq != zaq) {
                this.zaq = zaq;
                this.zar.clear();
            }
            this.zar.addAll(zaq.zaa());
        }
    }
    
    public final void zaD(final zaae zaae) {
        synchronized (GoogleApiManager.zac) {
            if (this.zaq == zaae) {
                this.zaq = null;
                this.zar.clear();
            }
        }
    }
    
    public final boolean zaF() {
        if (this.zah) {
            return false;
        }
        final RootTelemetryConfiguration config = RootTelemetryConfigManager.getInstance().getConfig();
        if (config != null && !config.getMethodInvocationTelemetryEnabled()) {
            return false;
        }
        final int zaa = this.zam.zaa(this.zak, 203400000);
        return zaa == -1 || zaa == 0;
    }
    
    public final boolean zaG(final ConnectionResult connectionResult, final int n) {
        return this.zal.zah(this.zak, connectionResult, n);
    }
    
    public final int zaa() {
        return this.zan.getAndIncrement();
    }
    
    public final zabq zak(final ApiKey<?> apiKey) {
        return this.zap.get(apiKey);
    }
    
    public final Task<Map<ApiKey<?>, String>> zao(final Iterable<? extends HasApiKey<?>> iterable) {
        final com.google.android.gms.common.api.internal.zal zal = new com.google.android.gms.common.api.internal.zal(iterable);
        final Handler zat = this.zat;
        zat.sendMessage(zat.obtainMessage(2, (Object)zal));
        return zal.zaa();
    }
    
    public final Task<Boolean> zap(final GoogleApi<?> googleApi) {
        final zaaf zaaf = new zaaf(googleApi.getApiKey());
        final Handler zat = this.zat;
        zat.sendMessage(zat.obtainMessage(14, (Object)zaaf));
        return (Task<Boolean>)zaaf.zab().getTask();
    }
    
    public final <O extends Api.ApiOptions> Task<Void> zaq(final GoogleApi<O> googleApi, final RegisterListenerMethod<Api.AnyClient, ?> registerListenerMethod, final UnregisterListenerMethod<Api.AnyClient, ?> unregisterListenerMethod, final Runnable runnable) {
        final TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        this.zaL((com.google.android.gms.tasks.TaskCompletionSource<Object>)taskCompletionSource, registerListenerMethod.zaa(), googleApi);
        final zaf zaf = new zaf(new zaci(registerListenerMethod, unregisterListenerMethod, runnable), (TaskCompletionSource<Void>)taskCompletionSource);
        final Handler zat = this.zat;
        zat.sendMessage(zat.obtainMessage(8, (Object)new zach(zaf, this.zao.get(), googleApi)));
        return (Task<Void>)taskCompletionSource.getTask();
    }
    
    public final <O extends Api.ApiOptions> Task<Boolean> zar(final GoogleApi<O> googleApi, final ListenerHolder.ListenerKey listenerKey, final int n) {
        final TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        this.zaL((com.google.android.gms.tasks.TaskCompletionSource<Object>)taskCompletionSource, n, googleApi);
        final zah zah = new zah(listenerKey, (TaskCompletionSource<Boolean>)taskCompletionSource);
        final Handler zat = this.zat;
        zat.sendMessage(zat.obtainMessage(13, (Object)new zach(zah, this.zao.get(), googleApi)));
        return (Task<Boolean>)taskCompletionSource.getTask();
    }
    
    public final <O extends Api.ApiOptions> void zaw(final GoogleApi<O> googleApi, final int n, final BaseImplementation.ApiMethodImpl<? extends Result, Api.AnyClient> apiMethodImpl) {
        final zae zae = new zae(n, (A)apiMethodImpl);
        final Handler zat = this.zat;
        zat.sendMessage(zat.obtainMessage(4, (Object)new zach(zae, this.zao.get(), googleApi)));
    }
    
    public final <O extends Api.ApiOptions, ResultT> void zax(final GoogleApi<O> googleApi, final int n, final TaskApiCall<Api.AnyClient, ResultT> taskApiCall, final TaskCompletionSource<ResultT> taskCompletionSource, final StatusExceptionMapper statusExceptionMapper) {
        this.zaL((com.google.android.gms.tasks.TaskCompletionSource<Object>)taskCompletionSource, taskApiCall.zaa(), googleApi);
        final zag zag = new zag(n, (TaskApiCall<Api.AnyClient, ResultT>)taskApiCall, (TaskCompletionSource<ResultT>)taskCompletionSource, statusExceptionMapper);
        final Handler zat = this.zat;
        zat.sendMessage(zat.obtainMessage(4, (Object)new zach(zag, this.zao.get(), googleApi)));
    }
    
    public final void zay(final MethodInvocation methodInvocation, final int n, final long n2, final int n3) {
        final Handler zat = this.zat;
        zat.sendMessage(zat.obtainMessage(18, (Object)new zace(methodInvocation, n, n2, n3)));
    }
    
    public final void zaz(final ConnectionResult connectionResult, final int n) {
        if (!this.zaG(connectionResult, n)) {
            final Handler zat = this.zat;
            zat.sendMessage(zat.obtainMessage(5, n, 0, (Object)connectionResult));
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import java.io.Writer;
import java.io.StringWriter;
import android.os.Bundle;
import android.app.Activity;
import androidx.fragment.app.e;
import android.app.PendingIntent;
import android.util.Log;
import com.google.android.gms.common.api.Result;
import java.io.PrintWriter;
import java.io.FileDescriptor;
import android.os.Handler;
import java.util.concurrent.atomic.AtomicReference;
import com.google.android.gms.common.api.PendingResult;
import java.util.concurrent.TimeUnit;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.internal.service.Common;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import java.util.Iterator;
import java.util.HashSet;
import com.google.android.gms.common.util.ClientLibraryUtils;
import java.util.LinkedList;
import java.util.List;
import com.google.android.gms.common.internal.zaj;
import java.util.ArrayList;
import com.google.android.gms.common.GoogleApiAvailability;
import android.os.Looper;
import android.content.Context;
import com.google.android.gms.common.internal.zak;
import java.util.concurrent.locks.Lock;
import com.google.android.gms.signin.SignInOptions;
import com.google.android.gms.signin.zae;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.common.api.Scope;
import java.util.Set;
import com.google.android.gms.common.api.Api;
import java.util.Map;
import com.google.android.gms.common.util.VisibleForTesting;
import java.util.Queue;
import com.google.android.gms.common.api.GoogleApiClient;

public final class zabe extends GoogleApiClient implements zabz
{
    @VisibleForTesting
    final Queue<BaseImplementation.ApiMethodImpl<?, ?>> zaa;
    @VisibleForTesting
    zabx zab;
    final Map<Api.AnyClientKey<?>, Api.Client> zac;
    Set<Scope> zad;
    final ClientSettings zae;
    final Map<Api<?>, Boolean> zaf;
    final Api.AbstractClientBuilder<? extends zae, SignInOptions> zag;
    Set<zada> zah;
    final zadc zai;
    private final Lock zaj;
    private final zak zak;
    private zaca zal;
    private final int zam;
    private final Context zan;
    private final Looper zao;
    private volatile boolean zap;
    private long zaq;
    private long zar;
    private final zabc zas;
    private final GoogleApiAvailability zat;
    private final ListenerHolders zau;
    private final ArrayList<zat> zav;
    private Integer zaw;
    private final zaj zax;
    
    public zabe(final Context zan, final Lock zaj, final Looper zao, final ClientSettings zae, final GoogleApiAvailability zat, final Api.AbstractClientBuilder<? extends zae, SignInOptions> zag, final Map<Api<?>, Boolean> zaf, final List<ConnectionCallbacks> list, final List<OnConnectionFailedListener> list2, final Map<Api.AnyClientKey<?>, Api.Client> zac, final int zam, final int i, final ArrayList<zat> zav) {
        this.zal = null;
        this.zaa = new LinkedList<BaseImplementation.ApiMethodImpl<?, ?>>();
        long zaq;
        if (!ClientLibraryUtils.isPackageSide()) {
            zaq = 120000L;
        }
        else {
            zaq = 10000L;
        }
        this.zaq = zaq;
        this.zar = 5000L;
        this.zad = new HashSet<Scope>();
        this.zau = new ListenerHolders();
        this.zaw = null;
        this.zah = null;
        final zaay zax = new zaay(this);
        this.zax = zax;
        this.zan = zan;
        this.zaj = zaj;
        this.zak = new zak(zao, zax);
        this.zao = zao;
        this.zas = new zabc(this, zao);
        this.zat = zat;
        this.zam = zam;
        if (zam >= 0) {
            this.zaw = i;
        }
        this.zaf = zaf;
        this.zac = zac;
        this.zav = zav;
        this.zai = new zadc();
        final Iterator<ConnectionCallbacks> iterator = list.iterator();
        while (iterator.hasNext()) {
            this.zak.zaf(iterator.next());
        }
        final Iterator<OnConnectionFailedListener> iterator2 = list2.iterator();
        while (iterator2.hasNext()) {
            this.zak.zag(iterator2.next());
        }
        this.zae = zae;
        this.zag = zag;
    }
    
    public static int zad(final Iterable<Api.Client> iterable, final boolean b) {
        final Iterator<Api.Client> iterator = iterable.iterator();
        boolean b2 = false;
        boolean b3 = false;
        while (iterator.hasNext()) {
            final Api.Client client = iterator.next();
            b2 |= client.requiresSignIn();
            b3 |= client.providesSignIn();
        }
        if (!b2) {
            return 3;
        }
        if (b3 && b) {
            return 2;
        }
        return 1;
    }
    
    public static String zag(final int n) {
        if (n == 1) {
            return "SIGN_IN_MODE_REQUIRED";
        }
        if (n == 2) {
            return "SIGN_IN_MODE_OPTIONAL";
        }
        if (n != 3) {
            return "UNKNOWN";
        }
        return "SIGN_IN_MODE_NONE";
    }
    
    private final void zal(int i) {
        final Integer zaw = this.zaw;
        if (zaw == null) {
            this.zaw = i;
        }
        else if (zaw != i) {
            final String zag = zag(i);
            final String zag2 = zag(this.zaw);
            final StringBuilder sb = new StringBuilder(zag.length() + 51 + zag2.length());
            sb.append("Cannot use sign-in mode: ");
            sb.append(zag);
            sb.append(". Mode was already set to ");
            sb.append(zag2);
            throw new IllegalStateException(sb.toString());
        }
        if (this.zal != null) {
            return;
        }
        final Iterator<Api.Client> iterator = this.zac.values().iterator();
        boolean b = false;
        i = 0;
        while (iterator.hasNext()) {
            final Api.Client client = iterator.next();
            b |= client.requiresSignIn();
            i |= (client.providesSignIn() ? 1 : 0);
        }
        final int intValue = this.zaw;
        while (true) {
            Label_0186: {
                zaca zag3;
                if (intValue != 1) {
                    if (intValue != 2) {
                        break Label_0186;
                    }
                    if (!b) {
                        break Label_0186;
                    }
                    zag3 = zaaa.zag(this.zan, this, this.zaj, this.zao, this.zat, this.zac, this.zae, this.zaf, this.zag, this.zav);
                }
                else {
                    if (!b) {
                        throw new IllegalStateException("SIGN_IN_MODE_REQUIRED cannot be used on a GoogleApiClient that does not contain any authenticated APIs. Use connect() instead.");
                    }
                    if (i == 0) {
                        break Label_0186;
                    }
                    throw new IllegalStateException("Cannot use SIGN_IN_MODE_REQUIRED with GOOGLE_SIGN_IN_API. Use connect(SIGN_IN_MODE_OPTIONAL) instead.");
                }
                this.zal = zag3;
                return;
            }
            zaca zag3 = new zabi(this.zan, this, this.zaj, this.zao, this.zat, this.zac, this.zae, this.zaf, this.zag, this.zav, this);
            continue;
        }
    }
    
    private final void zam(final GoogleApiClient googleApiClient, final StatusPendingResult statusPendingResult, final boolean b) {
        Common.zaa.zaa(googleApiClient).setResultCallback(new zabb(this, statusPendingResult, b, googleApiClient));
    }
    
    private final void zan() {
        this.zak.zab();
        Preconditions.checkNotNull(this.zal).zaq();
    }
    
    @Override
    public final ConnectionResult blockingConnect() {
        final Looper myLooper = Looper.myLooper();
        final Looper mainLooper = Looper.getMainLooper();
        final boolean b = true;
        Preconditions.checkState(myLooper != mainLooper, (Object)"blockingConnect must not be called on the UI thread");
        this.zaj.lock();
        try {
            if (this.zam >= 0) {
                Preconditions.checkState(this.zaw != null && b, (Object)"Sign-in mode should have been set explicitly by auto-manage.");
            }
            else {
                final Integer zaw = this.zaw;
                if (zaw == null) {
                    this.zaw = zad(this.zac.values(), false);
                }
                else if (zaw == 2) {
                    throw new IllegalStateException("Cannot call blockingConnect() when sign-in mode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
                }
            }
            this.zal(Preconditions.checkNotNull(this.zaw));
            this.zak.zab();
            return Preconditions.checkNotNull(this.zal).zab();
        }
        finally {
            this.zaj.unlock();
        }
    }
    
    @Override
    public final ConnectionResult blockingConnect(final long n, final TimeUnit timeUnit) {
        Preconditions.checkState(Looper.myLooper() != Looper.getMainLooper(), (Object)"blockingConnect must not be called on the UI thread");
        Preconditions.checkNotNull(timeUnit, "TimeUnit must not be null");
        this.zaj.lock();
        try {
            final Integer zaw = this.zaw;
            if (zaw == null) {
                this.zaw = zad(this.zac.values(), false);
            }
            else if (zaw == 2) {
                throw new IllegalStateException("Cannot call blockingConnect() when sign-in mode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            this.zal(Preconditions.checkNotNull(this.zaw));
            this.zak.zab();
            return Preconditions.checkNotNull(this.zal).zac(n, timeUnit);
        }
        finally {
            this.zaj.unlock();
        }
    }
    
    @Override
    public final PendingResult<Status> clearDefaultAccountAndReconnect() {
        Preconditions.checkState(this.isConnected(), (Object)"GoogleApiClient is not connected yet.");
        final Integer zaw = this.zaw;
        boolean b = true;
        if (zaw != null) {
            b = (zaw != 2 && b);
        }
        Preconditions.checkState(b, (Object)"Cannot use clearDefaultAccountAndReconnect with GOOGLE_SIGN_IN_API");
        final StatusPendingResult statusPendingResult = new StatusPendingResult(this);
        if (this.zac.containsKey(Common.CLIENT_KEY)) {
            this.zam(this, statusPendingResult, false);
        }
        else {
            final AtomicReference atomicReference = new AtomicReference();
            final zaaz zaaz = new zaaz(this, atomicReference, statusPendingResult);
            final zaba zaba = new zaba(this, statusPendingResult);
            final Builder builder = new Builder(this.zan);
            builder.addApi(Common.API);
            builder.addConnectionCallbacks(zaaz);
            builder.addOnConnectionFailedListener(zaba);
            builder.setHandler((Handler)this.zas);
            final GoogleApiClient build = builder.build();
            atomicReference.set(build);
            build.connect();
        }
        return statusPendingResult;
    }
    
    @Override
    public final void connect() {
        this.zaj.lock();
        try {
            final int zam = this.zam;
            final int n = 2;
            final boolean b = false;
            if (zam >= 0) {
                Preconditions.checkState(this.zaw != null, (Object)"Sign-in mode should have been set explicitly by auto-manage.");
            }
            else {
                final Integer zaw = this.zaw;
                if (zaw == null) {
                    this.zaw = zad(this.zac.values(), false);
                }
                else if (zaw == 2) {
                    throw new IllegalStateException("Cannot call connect() when SignInMode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
                }
            }
            int intValue = Preconditions.checkNotNull(this.zaw);
            this.zaj.lock();
            Label_0141: {
                if (intValue != 3 && intValue != 1) {
                    if (intValue != 2) {
                        final boolean b2 = b;
                        break Label_0141;
                    }
                    intValue = n;
                }
                final boolean b2 = true;
                try {
                    final StringBuilder sb = new StringBuilder(33);
                    sb.append("Illegal sign-in mode: ");
                    sb.append(intValue);
                    Preconditions.checkArgument(b2, (Object)sb.toString());
                    this.zal(intValue);
                    this.zan();
                    return;
                }
                finally {
                    this.zaj.unlock();
                }
            }
            throw new IllegalStateException("Cannot call connect() when SignInMode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
        }
        finally {
            this.zaj.unlock();
        }
    }
    
    @Override
    public final void connect(final int n) {
        this.zaj.lock();
        boolean b2;
        final boolean b = b2 = true;
        int i = n;
        if (n != 3) {
            b2 = b;
            if ((i = n) != 1) {
                if (n == 2) {
                    i = 2;
                    b2 = b;
                }
                else {
                    b2 = false;
                    i = n;
                }
            }
        }
        try {
            final StringBuilder sb = new StringBuilder(33);
            sb.append("Illegal sign-in mode: ");
            sb.append(i);
            Preconditions.checkArgument(b2, (Object)sb.toString());
            this.zal(i);
            this.zan();
        }
        finally {
            this.zaj.unlock();
        }
    }
    
    @Override
    public final void disconnect() {
        this.zaj.lock();
        try {
            this.zai.zab();
            final zaca zal = this.zal;
            if (zal != null) {
                zal.zar();
            }
            this.zau.zab();
            for (final BaseImplementation.ApiMethodImpl apiMethodImpl : this.zaa) {
                apiMethodImpl.zan(null);
                apiMethodImpl.cancel();
            }
            this.zaa.clear();
            if (this.zal != null) {
                this.zak();
                this.zak.zaa();
            }
        }
        finally {
            this.zaj.unlock();
        }
    }
    
    @Override
    public final void dump(final String s, final FileDescriptor fileDescriptor, final PrintWriter printWriter, final String[] array) {
        printWriter.append(s).append("mContext=").println(this.zan);
        printWriter.append(s).append("mResuming=").print(this.zap);
        printWriter.append(" mWorkQueue.size()=").print(this.zaa.size());
        printWriter.append(" mUnconsumedApiCalls.size()=").println(this.zai.zab.size());
        final zaca zal = this.zal;
        if (zal != null) {
            zal.zas(s, fileDescriptor, printWriter, array);
        }
    }
    
    @Override
    public final <A extends Api.AnyClient, R extends Result, T extends BaseImplementation.ApiMethodImpl<R, A>> T enqueue(T zae) {
        final Api<?> api = ((BaseImplementation.ApiMethodImpl)zae).getApi();
        final boolean containsKey = this.zac.containsKey(zae.getClientKey());
        String zad;
        if (api != null) {
            zad = api.zad();
        }
        else {
            zad = "the API";
        }
        final StringBuilder sb = new StringBuilder(String.valueOf(zad).length() + 65);
        sb.append("GoogleApiClient is not configured to use ");
        sb.append(zad);
        sb.append(" required for this call.");
        Preconditions.checkArgument(containsKey, (Object)sb.toString());
        this.zaj.lock();
        try {
            final zaca zal = this.zal;
            if (zal == null) {
                this.zaa.add(zae);
            }
            else {
                zae = (T)zal.zae((BaseImplementation.ApiMethodImpl<R, A>)zae);
            }
            return zae;
        }
        finally {
            this.zaj.unlock();
        }
    }
    
    @Override
    public final <A extends Api.AnyClient, T extends BaseImplementation.ApiMethodImpl<? extends Result, A>> T execute(final T t) {
        final Api<?> api = ((BaseImplementation.ApiMethodImpl)t).getApi();
        final boolean containsKey = this.zac.containsKey(t.getClientKey());
        String zad;
        if (api != null) {
            zad = api.zad();
        }
        else {
            zad = "the API";
        }
        final StringBuilder sb = new StringBuilder(String.valueOf(zad).length() + 65);
        sb.append("GoogleApiClient is not configured to use ");
        sb.append(zad);
        sb.append(" required for this call.");
        Preconditions.checkArgument(containsKey, (Object)sb.toString());
        this.zaj.lock();
        try {
            final zaca zal = this.zal;
            if (zal != null) {
                BaseImplementation.ApiMethodImpl<? extends Result, A> zaf;
                if (this.zap) {
                    this.zaa.add(t);
                    while (true) {
                        zaf = t;
                        if (this.zaa.isEmpty()) {
                            break;
                        }
                        final BaseImplementation.ApiMethodImpl apiMethodImpl = this.zaa.remove();
                        this.zai.zaa(apiMethodImpl);
                        apiMethodImpl.setFailedResult(Status.RESULT_INTERNAL_ERROR);
                    }
                }
                else {
                    zaf = zal.zaf(t);
                }
                return (T)zaf;
            }
            throw new IllegalStateException("GoogleApiClient is not connected yet.");
        }
        finally {
            this.zaj.unlock();
        }
    }
    
    @Override
    public final <C extends Api.Client> C getClient(final Api.AnyClientKey<C> anyClientKey) {
        final Api.Client client = this.zac.get(anyClientKey);
        Preconditions.checkNotNull(client, "Appropriate Api was not requested.");
        return (C)client;
    }
    
    @Override
    public final ConnectionResult getConnectionResult(final Api<?> api) {
        this.zaj.lock();
        try {
            if (!this.isConnected() && !this.zap) {
                throw new IllegalStateException("Cannot invoke getConnectionResult unless GoogleApiClient is connected");
            }
            if (!this.zac.containsKey(api.zab())) {
                throw new IllegalArgumentException(String.valueOf(api.zad()).concat(" was never registered with GoogleApiClient"));
            }
            final ConnectionResult zad = Preconditions.checkNotNull(this.zal).zad(api);
            if (zad == null) {
                ConnectionResult result_SUCCESS;
                if (this.zap) {
                    result_SUCCESS = ConnectionResult.RESULT_SUCCESS;
                }
                else {
                    Log.w("GoogleApiClientImpl", this.zaf());
                    Log.wtf("GoogleApiClientImpl", String.valueOf(api.zad()).concat(" requested in getConnectionResult is not connected but is not present in the failed  connections map"), (Throwable)new Exception());
                    result_SUCCESS = new ConnectionResult(8, null);
                }
                return result_SUCCESS;
            }
            return zad;
        }
        finally {
            this.zaj.unlock();
        }
    }
    
    @Override
    public final Context getContext() {
        return this.zan;
    }
    
    @Override
    public final Looper getLooper() {
        return this.zao;
    }
    
    @Override
    public final boolean hasApi(final Api<?> api) {
        return this.zac.containsKey(api.zab());
    }
    
    @Override
    public final boolean hasConnectedApi(final Api<?> api) {
        if (!this.isConnected()) {
            return false;
        }
        final Api.Client client = this.zac.get(api.zab());
        return client != null && client.isConnected();
    }
    
    @Override
    public final boolean isConnected() {
        final zaca zal = this.zal;
        return zal != null && zal.zaw();
    }
    
    @Override
    public final boolean isConnecting() {
        final zaca zal = this.zal;
        return zal != null && zal.zax();
    }
    
    @Override
    public final boolean isConnectionCallbacksRegistered(final ConnectionCallbacks connectionCallbacks) {
        return this.zak.zaj(connectionCallbacks);
    }
    
    @Override
    public final boolean isConnectionFailedListenerRegistered(final OnConnectionFailedListener onConnectionFailedListener) {
        return this.zak.zak(onConnectionFailedListener);
    }
    
    @Override
    public final boolean maybeSignIn(final SignInConnectionListener signInConnectionListener) {
        final zaca zal = this.zal;
        return zal != null && zal.zay(signInConnectionListener);
    }
    
    @Override
    public final void maybeSignOut() {
        final zaca zal = this.zal;
        if (zal != null) {
            zal.zau();
        }
    }
    
    @Override
    public final void reconnect() {
        this.disconnect();
        this.connect();
    }
    
    @Override
    public final void registerConnectionCallbacks(final ConnectionCallbacks connectionCallbacks) {
        this.zak.zaf(connectionCallbacks);
    }
    
    @Override
    public final void registerConnectionFailedListener(final OnConnectionFailedListener onConnectionFailedListener) {
        this.zak.zag(onConnectionFailedListener);
    }
    
    @Override
    public final <L> ListenerHolder<L> registerListener(final L l) {
        this.zaj.lock();
        try {
            return this.zau.zaa(l, this.zao, "NO_TYPE");
        }
        finally {
            this.zaj.unlock();
        }
    }
    
    @Override
    public final void stopAutoManage(final e e) {
        final LifecycleActivity lifecycleActivity = new LifecycleActivity(e);
        if (this.zam >= 0) {
            com.google.android.gms.common.api.internal.zak.zaa(lifecycleActivity).zae(this.zam);
            return;
        }
        throw new IllegalStateException("Called stopAutoManage but automatic lifecycle management is not enabled.");
    }
    
    @Override
    public final void unregisterConnectionCallbacks(final ConnectionCallbacks connectionCallbacks) {
        this.zak.zah(connectionCallbacks);
    }
    
    @Override
    public final void unregisterConnectionFailedListener(final OnConnectionFailedListener onConnectionFailedListener) {
        this.zak.zai(onConnectionFailedListener);
    }
    
    @Override
    public final void zaa(final ConnectionResult connectionResult) {
        if (!this.zat.isPlayServicesPossiblyUpdating(this.zan, connectionResult.getErrorCode())) {
            this.zak();
        }
        if (!this.zap) {
            this.zak.zac(connectionResult);
            this.zak.zaa();
        }
    }
    
    @Override
    public final void zab(final Bundle bundle) {
        while (!this.zaa.isEmpty()) {
            this.execute(this.zaa.remove());
        }
        this.zak.zad(bundle);
    }
    
    @Override
    public final void zac(int i, final boolean b) {
        int n = i;
        Label_0124: {
            if (i != 1) {
                break Label_0124;
            }
            Label_0122: {
                if (b) {
                    break Label_0122;
                }
                if (this.zap) {
                    break Label_0122;
                }
                this.zap = true;
                while (true) {
                    if (this.zab != null || ClientLibraryUtils.isPackageSide()) {
                        break Label_0078;
                    }
                    try {
                        this.zab = this.zat.zac(this.zan.getApplicationContext(), new zabd(this));
                        final zabc zas = this.zas;
                        ((Handler)zas).sendMessageDelayed(((Handler)zas).obtainMessage(1), this.zaq);
                        final zabc zas2 = this.zas;
                        ((Handler)zas2).sendMessageDelayed(((Handler)zas2).obtainMessage(2), this.zar);
                        n = 1;
                        final Set<BasePendingResult<?>> zab = this.zai.zab;
                        i = 0;
                        for (BasePendingResult[] array = zab.toArray(new BasePendingResult[0]); i < array.length; ++i) {
                            array[i].forceFailureUnlessReady(zadc.zaa);
                        }
                        this.zak.zae(n);
                        this.zak.zaa();
                        if (n == 2) {
                            this.zan();
                        }
                    }
                    catch (final SecurityException ex) {
                        continue;
                    }
                    break;
                }
            }
        }
    }
    
    public final String zaf() {
        final StringWriter out = new StringWriter();
        this.dump("", null, new PrintWriter(out), null);
        return out.toString();
    }
    
    public final boolean zak() {
        if (!this.zap) {
            return false;
        }
        this.zap = false;
        ((Handler)this.zas).removeMessages(2);
        ((Handler)this.zas).removeMessages(1);
        final zabx zab = this.zab;
        if (zab != null) {
            zab.zab();
            this.zab = null;
        }
        return true;
    }
    
    @Override
    public final void zao(final zada zada) {
        this.zaj.lock();
        try {
            if (this.zah == null) {
                this.zah = new HashSet<zada>();
            }
            this.zah.add(zada);
        }
        finally {
            this.zaj.unlock();
        }
    }
    
    @Override
    public final void zap(final zada zada) {
        this.zaj.lock();
        try {
            final Set<zada> zah = this.zah;
            Label_0067: {
                Exception ex;
                String s;
                if (zah == null) {
                    ex = new Exception();
                    s = "Attempted to remove pending transform when no transforms are registered.";
                }
                else {
                    if (zah.remove(zada)) {
                        break Label_0067;
                    }
                    ex = new Exception();
                    s = "Failed to remove pending transform - this may lead to memory leaks!";
                }
                Log.wtf("GoogleApiClientImpl", s, (Throwable)ex);
                return;
            }
            this.zaj.lock();
            try {
                final Set<zada> zah2 = this.zah;
                if (zah2 == null) {
                    this.zaj.unlock();
                }
                else {
                    final boolean empty = zah2.isEmpty();
                    this.zaj.unlock();
                    if (empty ^ true) {
                        return;
                    }
                }
                final zaca zal = this.zal;
                if (zal != null) {
                    zal.zat();
                }
            }
            finally {
                this.zaj.unlock();
            }
        }
        finally {
            this.zaj.unlock();
        }
    }
}

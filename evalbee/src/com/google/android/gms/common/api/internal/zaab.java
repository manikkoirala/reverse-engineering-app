// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.PendingResult;

final class zaab implements StatusListener
{
    final BasePendingResult zaa;
    final zaad zab;
    
    public zaab(final zaad zab, final BasePendingResult zaa) {
        this.zab = zab;
        this.zaa = zaa;
    }
    
    @Override
    public final void onComplete(final Status status) {
        zaad.zaa(this.zab).remove(this.zaa);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api;

import java.util.Iterator;
import android.os.Bundle;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.data.DataBuffer;
import com.google.android.gms.common.data.AbstractDataBuffer;

@KeepForSdk
public class DataBufferResponse<T, R extends AbstractDataBuffer<T> & Result> extends Response<R> implements DataBuffer<T>
{
    @KeepForSdk
    public DataBufferResponse() {
    }
    
    @KeepForSdk
    public DataBufferResponse(final R r) {
        super(r);
    }
    
    @Override
    public final void close() {
        this.getResult().close();
    }
    
    @Override
    public final T get(final int n) {
        return this.getResult().get(n);
    }
    
    @Override
    public final int getCount() {
        return this.getResult().getCount();
    }
    
    @Override
    public final Bundle getMetadata() {
        return this.getResult().getMetadata();
    }
    
    @Override
    public final boolean isClosed() {
        return this.getResult().isClosed();
    }
    
    @Override
    public final Iterator<T> iterator() {
        return this.getResult().iterator();
    }
    
    @Override
    public final void release() {
        this.getResult().release();
    }
    
    @Override
    public final Iterator<T> singleRefIterator() {
        return this.getResult().singleRefIterator();
    }
}

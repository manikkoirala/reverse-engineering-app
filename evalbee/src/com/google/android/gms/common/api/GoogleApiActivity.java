// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api;

import android.os.BaseBundle;
import android.content.DialogInterface;
import android.os.Bundle;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.GoogleApiAvailability;
import android.content.ActivityNotFoundException;
import android.os.Build;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.internal.GoogleApiManager;
import android.content.IntentSender$SendIntentException;
import android.util.Log;
import android.os.Parcelable;
import android.content.Intent;
import android.app.PendingIntent;
import android.content.Context;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.common.annotation.KeepName;
import android.content.DialogInterface$OnCancelListener;
import android.app.Activity;

@KeepName
public class GoogleApiActivity extends Activity implements DialogInterface$OnCancelListener
{
    @VisibleForTesting
    protected int zaa;
    
    public GoogleApiActivity() {
        this.zaa = 0;
    }
    
    public static Intent zaa(final Context context, final PendingIntent pendingIntent, final int n, final boolean b) {
        final Intent intent = new Intent(context, (Class)GoogleApiActivity.class);
        intent.putExtra("pending_intent", (Parcelable)pendingIntent);
        intent.putExtra("failing_client_id", n);
        intent.putExtra("notify_manager", b);
        return intent;
    }
    
    private final void zab() {
        final Bundle extras = this.getIntent().getExtras();
        if (extras == null) {
            Log.e("GoogleApiActivity", "Activity started without extras");
            this.finish();
            return;
        }
        final PendingIntent pendingIntent = (PendingIntent)((BaseBundle)extras).get("pending_intent");
        final Integer n = (Integer)((BaseBundle)extras).get("error_code");
        if (pendingIntent == null && n == null) {
            Log.e("GoogleApiActivity", "Activity started without resolution");
            this.finish();
            return;
        }
        if (pendingIntent != null) {
            try {
                this.startIntentSenderForResult(pendingIntent.getIntentSender(), 1, (Intent)null, 0, 0, 0);
                this.zaa = 1;
                return;
            }
            catch (final IntentSender$SendIntentException ex) {
                Log.e("GoogleApiActivity", "Failed to launch pendingIntent", (Throwable)ex);
                this.finish();
                return;
            }
            catch (final ActivityNotFoundException ex2) {
                if (((BaseBundle)extras).getBoolean("notify_manager", true)) {
                    GoogleApiManager.zam((Context)this).zaz(new ConnectionResult(22, null), this.getIntent().getIntExtra("failing_client_id", -1));
                }
                else {
                    final String string = pendingIntent.toString();
                    final StringBuilder sb = new StringBuilder(string.length() + 36);
                    sb.append("Activity not found while launching ");
                    sb.append(string);
                    sb.append(".");
                    String s = sb.toString();
                    if (Build.FINGERPRINT.contains("generic")) {
                        s = s.concat(" This may occur when resolving Google Play services connection issues on emulators with Google APIs but not Google Play Store.");
                    }
                    Log.e("GoogleApiActivity", s, (Throwable)ex2);
                }
                this.zaa = 1;
                this.finish();
                return;
            }
        }
        GoogleApiAvailability.getInstance().showErrorDialogFragment(this, Preconditions.checkNotNull(n), 2, (DialogInterface$OnCancelListener)this);
        this.zaa = 1;
    }
    
    public final void onActivityResult(final int n, final int n2, final Intent intent) {
        super.onActivityResult(n, n2, intent);
        if (n == 1) {
            final boolean booleanExtra = this.getIntent().getBooleanExtra("notify_manager", true);
            this.zaa = 0;
            this.setResult(n2, intent);
            if (booleanExtra) {
                final GoogleApiManager zam = GoogleApiManager.zam((Context)this);
                if (n2 != -1) {
                    if (n2 == 0) {
                        zam.zaz(new ConnectionResult(13, null), this.getIntent().getIntExtra("failing_client_id", -1));
                    }
                }
                else {
                    zam.zaA();
                }
            }
        }
        else if (n == 2) {
            this.zaa = 0;
            this.setResult(n2, intent);
        }
        this.finish();
    }
    
    public final void onCancel(final DialogInterface dialogInterface) {
        this.setResult(this.zaa = 0);
        this.finish();
    }
    
    public final void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        if (bundle != null) {
            this.zaa = ((BaseBundle)bundle).getInt("resolution");
        }
        if (this.zaa != 1) {
            this.zab();
        }
    }
    
    public final void onSaveInstanceState(final Bundle bundle) {
        ((BaseBundle)bundle).putInt("resolution", this.zaa);
        super.onSaveInstanceState(bundle);
    }
}

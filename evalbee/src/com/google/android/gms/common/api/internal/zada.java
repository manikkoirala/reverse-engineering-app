// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import android.util.Log;
import com.google.android.gms.common.api.Releasable;
import android.os.Looper;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.api.GoogleApiClient;
import java.lang.ref.WeakReference;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallbacks;
import com.google.android.gms.common.api.ResultTransform;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.TransformedResult;
import com.google.android.gms.common.api.Result;

public final class zada<R extends Result> extends TransformedResult<R> implements ResultCallback<R>
{
    private ResultTransform<? super R, ? extends Result> zaa;
    private zada<? extends Result> zab;
    private volatile ResultCallbacks<? super R> zac;
    private PendingResult<R> zad;
    private final Object zae;
    private Status zaf;
    private final WeakReference<GoogleApiClient> zag;
    private final zacz zah;
    private boolean zai;
    
    public zada(final WeakReference<GoogleApiClient> zag) {
        this.zaa = null;
        this.zab = null;
        this.zac = null;
        this.zad = null;
        this.zae = new Object();
        this.zaf = null;
        this.zai = false;
        Preconditions.checkNotNull(zag, "GoogleApiClient reference must not be null");
        this.zag = zag;
        final GoogleApiClient googleApiClient = zag.get();
        Looper looper;
        if (googleApiClient != null) {
            looper = googleApiClient.getLooper();
        }
        else {
            looper = Looper.getMainLooper();
        }
        this.zah = new zacz(this, looper);
    }
    
    private final void zaj(final Status zaf) {
        synchronized (this.zae) {
            this.zal(this.zaf = zaf);
        }
    }
    
    private final void zak() {
        if (this.zaa == null && this.zac == null) {
            return;
        }
        final GoogleApiClient googleApiClient = this.zag.get();
        if (!this.zai && this.zaa != null && googleApiClient != null) {
            googleApiClient.zao(this);
            this.zai = true;
        }
        final Status zaf = this.zaf;
        if (zaf != null) {
            this.zal(zaf);
            return;
        }
        final PendingResult<R> zad = this.zad;
        if (zad != null) {
            zad.setResultCallback(this);
        }
    }
    
    private final void zal(Status status) {
        synchronized (this.zae) {
            final ResultTransform<? super R, ? extends Result> zaa = this.zaa;
            if (zaa != null) {
                status = Preconditions.checkNotNull(zaa.onFailure(status), "onFailure must not return null");
                Preconditions.checkNotNull(this.zab).zaj(status);
            }
            else if (this.zam()) {
                Preconditions.checkNotNull(this.zac).onFailure(status);
            }
        }
    }
    
    private final boolean zam() {
        final GoogleApiClient googleApiClient = this.zag.get();
        return this.zac != null && googleApiClient != null;
    }
    
    private static final void zan(final Result obj) {
        if (obj instanceof Releasable) {
            try {
                ((Releasable)obj).release();
            }
            catch (final RuntimeException ex) {
                Log.w("TransformedResultImpl", "Unable to release ".concat(String.valueOf(obj)), (Throwable)ex);
            }
        }
    }
    
    @Override
    public final void andFinally(final ResultCallbacks<? super R> zac) {
        synchronized (this.zae) {
            final ResultCallbacks<? super R> zac2 = this.zac;
            final boolean b = true;
            Preconditions.checkState(zac2 == null, (Object)"Cannot call andFinally() twice.");
            Preconditions.checkState(this.zaa == null && b, (Object)"Cannot call then() and andFinally() on the same TransformedResult.");
            this.zac = zac;
            this.zak();
        }
    }
    
    @Override
    public final void onResult(final R r) {
        synchronized (this.zae) {
            if (r.getStatus().isSuccess()) {
                if (this.zaa != null) {
                    zaco.zaa().submit(new zacy(this, r));
                }
                else if (this.zam()) {
                    Preconditions.checkNotNull(this.zac).onSuccess(r);
                }
            }
            else {
                this.zaj(r.getStatus());
                zan(r);
            }
        }
    }
    
    @Override
    public final <S extends Result> TransformedResult<S> then(final ResultTransform<? super R, ? extends S> zaa) {
        synchronized (this.zae) {
            final ResultTransform<? super R, ? extends Result> zaa2 = this.zaa;
            final boolean b = true;
            Preconditions.checkState(zaa2 == null, (Object)"Cannot call then() twice.");
            Preconditions.checkState(this.zac == null && b, (Object)"Cannot call then() and andFinally() on the same TransformedResult.");
            this.zaa = zaa;
            final zada<Result> zab = new zada<Result>(this.zag);
            this.zab = zab;
            this.zak();
            return (TransformedResult<S>)zab;
        }
    }
    
    public final void zah() {
        this.zac = null;
    }
    
    public final void zai(final PendingResult<?> zad) {
        synchronized (this.zae) {
            this.zad = (PendingResult<R>)zad;
            this.zak();
        }
    }
}

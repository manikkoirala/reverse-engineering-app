// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api;

import com.google.android.gms.common.internal.ShowFirstParty;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public class BooleanResult implements Result
{
    private final Status zaa;
    private final boolean zab;
    
    @KeepForSdk
    @ShowFirstParty
    public BooleanResult(final Status status, final boolean zab) {
        this.zaa = Preconditions.checkNotNull(status, "Status must not be null");
        this.zab = zab;
    }
    
    @KeepForSdk
    @Override
    public final boolean equals(final Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        if (!(o instanceof BooleanResult)) {
            return false;
        }
        final BooleanResult booleanResult = (BooleanResult)o;
        return this.zaa.equals(booleanResult.zaa) && this.zab == booleanResult.zab;
    }
    
    @KeepForSdk
    @Override
    public Status getStatus() {
        return this.zaa;
    }
    
    @KeepForSdk
    public boolean getValue() {
        return this.zab;
    }
    
    @KeepForSdk
    @Override
    public final int hashCode() {
        return (this.zaa.hashCode() + 527) * 31 + (this.zab ? 1 : 0);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import android.os.Bundle;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;

public final class zat implements ConnectionCallbacks, OnConnectionFailedListener
{
    public final Api<?> zaa;
    private final boolean zab;
    private zau zac;
    
    public zat(final Api<?> zaa, final boolean zab) {
        this.zaa = zaa;
        this.zab = zab;
    }
    
    private final zau zab() {
        Preconditions.checkNotNull(this.zac, "Callbacks must be attached to a ClientConnectionHelper instance before connecting the client.");
        return this.zac;
    }
    
    @Override
    public final void onConnected(final Bundle bundle) {
        this.zab().onConnected(bundle);
    }
    
    @Override
    public final void onConnectionFailed(final ConnectionResult connectionResult) {
        this.zab().zaa(connectionResult, this.zaa, this.zab);
    }
    
    @Override
    public final void onConnectionSuspended(final int n) {
        this.zab().onConnectionSuspended(n);
    }
    
    public final void zaa(final zau zac) {
        this.zac = zac;
    }
}

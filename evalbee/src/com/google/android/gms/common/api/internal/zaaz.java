// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.internal.Preconditions;
import android.os.Bundle;
import java.util.concurrent.atomic.AtomicReference;
import com.google.android.gms.common.api.GoogleApiClient;

final class zaaz implements ConnectionCallbacks
{
    final AtomicReference zaa;
    final StatusPendingResult zab;
    final zabe zac;
    
    public zaaz(final zabe zac, final AtomicReference zaa, final StatusPendingResult zab) {
        this.zac = zac;
        this.zaa = zaa;
        this.zab = zab;
    }
    
    @Override
    public final void onConnected(final Bundle bundle) {
        zabe.zah(this.zac, (GoogleApiClient)Preconditions.checkNotNull((GoogleApiClient)this.zaa.get()), this.zab, true);
    }
    
    @Override
    public final void onConnectionSuspended(final int n) {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api;

import java.util.concurrent.TimeUnit;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public abstract class PendingResult<R extends Result>
{
    @KeepForSdk
    public void addStatusListener(final StatusListener statusListener) {
        throw new UnsupportedOperationException();
    }
    
    public abstract R await();
    
    public abstract R await(final long p0, final TimeUnit p1);
    
    public abstract void cancel();
    
    public abstract boolean isCanceled();
    
    public abstract void setResultCallback(final ResultCallback<? super R> p0);
    
    public abstract void setResultCallback(final ResultCallback<? super R> p0, final long p1, final TimeUnit p2);
    
    public <S extends Result> TransformedResult<S> then(final ResultTransform<? super R, ? extends S> resultTransform) {
        throw new UnsupportedOperationException();
    }
    
    @KeepForSdk
    public interface StatusListener
    {
        @KeepForSdk
        void onComplete(final Status p0);
    }
}

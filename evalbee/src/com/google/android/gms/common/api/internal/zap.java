// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import android.os.BaseBundle;
import android.os.Parcelable;
import android.os.Bundle;
import android.content.DialogInterface;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.base.zaq;
import android.os.Looper;
import android.os.Handler;
import com.google.android.gms.common.GoogleApiAvailability;
import java.util.concurrent.atomic.AtomicReference;
import android.content.DialogInterface$OnCancelListener;

public abstract class zap extends LifecycleCallback implements DialogInterface$OnCancelListener
{
    protected volatile boolean zaa;
    protected final AtomicReference<zam> zab;
    protected final GoogleApiAvailability zac;
    private final Handler zad;
    
    @VisibleForTesting
    public zap(final LifecycleFragment lifecycleFragment, final GoogleApiAvailability zac) {
        super(lifecycleFragment);
        this.zab = new AtomicReference<zam>(null);
        this.zad = (Handler)new zaq(Looper.getMainLooper());
        this.zac = zac;
    }
    
    private final void zaa(final ConnectionResult connectionResult, final int n) {
        this.zab.set(null);
        this.zab(connectionResult, n);
    }
    
    private final void zad() {
        this.zab.set(null);
        this.zac();
    }
    
    private static final int zae(final zam zam) {
        if (zam == null) {
            return -1;
        }
        return zam.zaa();
    }
    
    @Override
    public final void onActivityResult(int n, final int n2, final Intent intent) {
        final zam zam = this.zab.get();
        if (n != 1) {
            if (n == 2) {
                n = this.zac.isGooglePlayServicesAvailable((Context)this.getActivity());
                if (n == 0) {
                    this.zad();
                    return;
                }
                if (zam == null) {
                    return;
                }
                if (zam.zab().getErrorCode() == 18 && n == 18) {
                    return;
                }
            }
        }
        else {
            if (n2 == -1) {
                this.zad();
                return;
            }
            if (n2 == 0) {
                if (zam == null) {
                    return;
                }
                n = 13;
                if (intent != null) {
                    n = intent.getIntExtra("<<ResolutionFailureErrorDetail>>", 13);
                }
                this.zaa(new ConnectionResult(n, null, zam.zab().toString()), zae(zam));
                return;
            }
        }
        if (zam != null) {
            this.zaa(zam.zab(), zam.zaa());
        }
    }
    
    public final void onCancel(final DialogInterface dialogInterface) {
        this.zaa(new ConnectionResult(13, null), zae(this.zab.get()));
    }
    
    @Override
    public final void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        if (bundle != null) {
            final AtomicReference<zam> zab = this.zab;
            zam newValue;
            if (((BaseBundle)bundle).getBoolean("resolving_error", false)) {
                newValue = new zam(new ConnectionResult(((BaseBundle)bundle).getInt("failed_status"), (PendingIntent)bundle.getParcelable("failed_resolution")), ((BaseBundle)bundle).getInt("failed_client_id", -1));
            }
            else {
                newValue = null;
            }
            zab.set(newValue);
        }
    }
    
    @Override
    public final void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        final zam zam = this.zab.get();
        if (zam == null) {
            return;
        }
        ((BaseBundle)bundle).putBoolean("resolving_error", true);
        ((BaseBundle)bundle).putInt("failed_client_id", zam.zaa());
        ((BaseBundle)bundle).putInt("failed_status", zam.zab().getErrorCode());
        bundle.putParcelable("failed_resolution", (Parcelable)zam.zab().getResolution());
    }
    
    @Override
    public void onStart() {
        super.onStart();
        this.zaa = true;
    }
    
    @Override
    public void onStop() {
        super.onStop();
        this.zaa = false;
    }
    
    public abstract void zab(final ConnectionResult p0, final int p1);
    
    public abstract void zac();
    
    public final void zah(final ConnectionResult connectionResult, final int n) {
        final zam zam = new zam(connectionResult, n);
        if (ja2.a(this.zab, null, zam)) {
            this.zad.post((Runnable)new zao(this, zam));
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.Feature;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.common.api.Api;

public final class zag<ResultT> extends zac
{
    private final TaskApiCall<Api.AnyClient, ResultT> zaa;
    private final TaskCompletionSource<ResultT> zab;
    private final StatusExceptionMapper zad;
    
    public zag(final int n, final TaskApiCall<Api.AnyClient, ResultT> zaa, final TaskCompletionSource<ResultT> zab, final StatusExceptionMapper zad) {
        super(n);
        this.zab = zab;
        this.zaa = zaa;
        this.zad = zad;
        if (n == 2 && zaa.shouldAutoResolveMissingFeatures()) {
            throw new IllegalArgumentException("Best-effort write calls cannot pass methods that should auto-resolve missing features.");
        }
    }
    
    @Override
    public final boolean zaa(final zabq<?> zabq) {
        return this.zaa.shouldAutoResolveMissingFeatures();
    }
    
    @Override
    public final Feature[] zab(final zabq<?> zabq) {
        return this.zaa.zab();
    }
    
    @Override
    public final void zad(final Status status) {
        this.zab.trySetException(this.zad.getException(status));
    }
    
    @Override
    public final void zae(final Exception ex) {
        this.zab.trySetException(ex);
    }
    
    @Override
    public final void zaf(final zabq<?> zabq) {
        try {
            this.zaa.doExecute(zabq.zaf(), this.zab);
        }
        catch (final RuntimeException ex) {
            this.zab.trySetException((Exception)ex);
        }
        catch (final RemoteException ex2) {
            this.zad(zai.zah(ex2));
        }
        catch (final DeadObjectException ex3) {
            throw ex3;
        }
    }
    
    @Override
    public final void zag(final zaad zaad, final boolean b) {
        zaad.zad(this.zab, b);
    }
}

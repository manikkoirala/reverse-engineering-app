// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.internal.Preconditions;
import android.os.Looper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import java.lang.ref.WeakReference;
import com.google.android.gms.common.internal.BaseGmsClient;

final class zaal implements ConnectionProgressReportCallbacks
{
    private final WeakReference<zaaw> zaa;
    private final Api<?> zab;
    private final boolean zac;
    
    public zaal(final zaaw referent, final Api<?> zab, final boolean zac) {
        this.zaa = new WeakReference<zaaw>(referent);
        this.zab = zab;
        this.zac = zac;
    }
    
    @Override
    public final void onReportServiceBinding(final ConnectionResult connectionResult) {
        final zaaw zaaw = this.zaa.get();
        if (zaaw == null) {
            return;
        }
        Preconditions.checkState(Looper.myLooper() == com.google.android.gms.common.api.internal.zaaw.zak(zaaw).zag.getLooper(), (Object)"onReportServiceBinding must be called on the GoogleApiClient handler thread");
        com.google.android.gms.common.api.internal.zaaw.zap(zaaw).lock();
        try {
            if (com.google.android.gms.common.api.internal.zaaw.zaw(zaaw, 0)) {
                if (!connectionResult.isSuccess()) {
                    com.google.android.gms.common.api.internal.zaaw.zat(zaaw, connectionResult, (Api)this.zab, this.zac);
                }
                if (com.google.android.gms.common.api.internal.zaaw.zax(zaaw)) {
                    com.google.android.gms.common.api.internal.zaaw.zau(zaaw);
                }
            }
        }
        finally {
            com.google.android.gms.common.api.internal.zaaw.zap(zaaw).unlock();
        }
    }
}

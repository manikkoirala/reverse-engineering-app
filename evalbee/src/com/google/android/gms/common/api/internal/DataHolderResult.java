// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Result;

@KeepForSdk
public abstract class DataHolderResult implements Result, Releasable
{
    @KeepForSdk
    protected final DataHolder mDataHolder;
    @KeepForSdk
    protected final Status mStatus;
    
    @KeepForSdk
    public DataHolderResult(final DataHolder dataHolder) {
        this(dataHolder, new Status(dataHolder.getStatusCode()));
    }
    
    @KeepForSdk
    public DataHolderResult(final DataHolder mDataHolder, final Status mStatus) {
        this.mStatus = mStatus;
        this.mDataHolder = mDataHolder;
    }
    
    @KeepForSdk
    @Override
    public Status getStatus() {
        return this.mStatus;
    }
    
    @KeepForSdk
    @Override
    public void release() {
        try (final DataHolder mDataHolder = this.mDataHolder) {}
    }
}

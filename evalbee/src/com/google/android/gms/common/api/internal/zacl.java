// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.common.api.Api;

final class zacl extends UnregisterListenerMethod
{
    final RegistrationMethods.Builder zaa;
    
    public zacl(final RegistrationMethods.Builder zaa, final ListenerHolder.ListenerKey listenerKey) {
        this.zaa = zaa;
        super(listenerKey);
    }
    
    @Override
    public final void unregisterListener(final Api.AnyClient anyClient, final TaskCompletionSource<Boolean> taskCompletionSource) {
        RegistrationMethods.Builder.zab((RegistrationMethods.Builder<Api.AnyClient, Object>)this.zaa).accept(anyClient, taskCompletionSource);
    }
}

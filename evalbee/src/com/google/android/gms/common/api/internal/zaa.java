// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import java.util.Iterator;
import android.app.Activity;
import java.util.ArrayList;
import java.util.List;

final class zaa extends LifecycleCallback
{
    private List<Runnable> zaa;
    
    private zaa(final LifecycleFragment lifecycleFragment) {
        super(lifecycleFragment);
        this.zaa = new ArrayList<Runnable>();
        super.mLifecycleFragment.addCallback("LifecycleObserverOnStop", this);
    }
    
    private final void zac(final Runnable runnable) {
        synchronized (this) {
            this.zaa.add(runnable);
        }
    }
    
    @Override
    public final void onStop() {
        synchronized (this) {
            final List<Runnable> zaa = this.zaa;
            this.zaa = new ArrayList<Runnable>();
            monitorexit(this);
            final Iterator<Runnable> iterator = zaa.iterator();
            while (iterator.hasNext()) {
                iterator.next().run();
            }
        }
    }
}

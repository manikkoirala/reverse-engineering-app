// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import android.util.Log;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;

public final class zae<A extends BaseImplementation.ApiMethodImpl<? extends Result, Api.AnyClient>> extends zai
{
    protected final A zaa;
    
    public zae(final int n, final A a) {
        super(n);
        this.zaa = Preconditions.checkNotNull(a, "Null methods are not runnable.");
    }
    
    @Override
    public final void zad(final Status failedResult) {
        try {
            ((BaseImplementation.ApiMethodImpl)this.zaa).setFailedResult(failedResult);
        }
        catch (final IllegalStateException ex) {
            Log.w("ApiCallRunner", "Exception reporting failure", (Throwable)ex);
        }
    }
    
    @Override
    public final void zae(final Exception ex) {
        final String simpleName = ex.getClass().getSimpleName();
        final String localizedMessage = ex.getLocalizedMessage();
        final StringBuilder sb = new StringBuilder(simpleName.length() + 2 + String.valueOf(localizedMessage).length());
        sb.append(simpleName);
        sb.append(": ");
        sb.append(localizedMessage);
        final Status failedResult = new Status(10, sb.toString());
        try {
            ((BaseImplementation.ApiMethodImpl)this.zaa).setFailedResult(failedResult);
        }
        catch (final IllegalStateException ex2) {
            Log.w("ApiCallRunner", "Exception reporting failure", (Throwable)ex2);
        }
    }
    
    @Override
    public final void zaf(final zabq<?> zabq) {
        try {
            ((BaseImplementation.ApiMethodImpl<R, Api.Client>)this.zaa).run(zabq.zaf());
        }
        catch (final RuntimeException ex) {
            this.zae(ex);
        }
    }
    
    @Override
    public final void zag(final zaad zaad, final boolean b) {
        zaad.zac(this.zaa, b);
    }
}

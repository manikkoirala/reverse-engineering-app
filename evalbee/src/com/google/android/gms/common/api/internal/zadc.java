// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Result;
import java.util.Map;
import java.util.Collections;
import java.util.WeakHashMap;
import com.google.android.gms.common.util.VisibleForTesting;
import java.util.Set;
import com.google.android.gms.common.api.Status;

public final class zadc
{
    public static final Status zaa;
    @VisibleForTesting
    final Set<BasePendingResult<?>> zab;
    private final zadb zac;
    
    static {
        zaa = new Status(8, "The connection to Google Play services was lost");
    }
    
    public zadc() {
        this.zab = Collections.synchronizedSet((Set<BasePendingResult<?>>)Collections.newSetFromMap((Map<T, Boolean>)new WeakHashMap<Object, Boolean>()));
        this.zac = new zadb(this);
    }
    
    public final void zaa(final BasePendingResult<? extends Result> basePendingResult) {
        this.zab.add(basePendingResult);
        basePendingResult.zan(this.zac);
    }
    
    public final void zab() {
        final Set<BasePendingResult<?>> zab = this.zab;
        int i = 0;
        for (BasePendingResult[] array = zab.toArray(new BasePendingResult[0]); i < array.length; ++i) {
            final BasePendingResult basePendingResult = array[i];
            basePendingResult.zan(null);
            if (basePendingResult.zam()) {
                this.zab.remove(basePendingResult);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import android.os.BaseBundle;
import android.os.Looper;
import com.google.android.gms.common.api.GoogleApiClient;
import java.util.HashMap;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.internal.zav;
import com.google.android.gms.signin.internal.zak;
import com.google.android.gms.common.internal.zab;
import java.util.Collection;
import android.util.Log;
import com.google.android.gms.common.internal.Preconditions;
import java.util.Iterator;
import android.app.PendingIntent;
import java.util.Collections;
import java.util.HashSet;
import java.util.concurrent.Future;
import java.util.ArrayList;
import com.google.android.gms.signin.SignInOptions;
import java.util.Map;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.common.internal.IAccountAccessor;
import com.google.android.gms.signin.zae;
import com.google.android.gms.common.api.Api;
import java.util.Set;
import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import android.content.Context;
import java.util.concurrent.locks.Lock;

public final class zaaw implements zabf
{
    private final zabi zaa;
    private final Lock zab;
    private final Context zac;
    private final GoogleApiAvailabilityLight zad;
    private ConnectionResult zae;
    private int zaf;
    private int zag;
    private int zah;
    private final Bundle zai;
    private final Set<Api.AnyClientKey> zaj;
    private zae zak;
    private boolean zal;
    private boolean zam;
    private boolean zan;
    private IAccountAccessor zao;
    private boolean zap;
    private boolean zaq;
    private final ClientSettings zar;
    private final Map<Api<?>, Boolean> zas;
    private final Api.AbstractClientBuilder<? extends zae, SignInOptions> zat;
    private final ArrayList<Future<?>> zau;
    
    public zaaw(final zabi zaa, final ClientSettings zar, final Map<Api<?>, Boolean> zas, final GoogleApiAvailabilityLight zad, final Api.AbstractClientBuilder<? extends zae, SignInOptions> zat, final Lock zab, final Context zac) {
        this.zag = 0;
        this.zai = new Bundle();
        this.zaj = new HashSet<Api.AnyClientKey>();
        this.zau = new ArrayList<Future<?>>();
        this.zaa = zaa;
        this.zar = zar;
        this.zas = zas;
        this.zad = zad;
        this.zat = zat;
        this.zab = zab;
        this.zac = zac;
    }
    
    private final void zaA() {
        this.zam = false;
        this.zaa.zag.zad = Collections.emptySet();
        for (final Api.AnyClientKey anyClientKey : this.zaj) {
            if (!this.zaa.zab.containsKey(anyClientKey)) {
                this.zaa.zab.put(anyClientKey, new ConnectionResult(17, null));
            }
        }
    }
    
    private final void zaB(final boolean b) {
        final zae zak = this.zak;
        if (zak != null) {
            if (((Api.Client)zak).isConnected() && b) {
                zak.zaa();
            }
            ((Api.Client)zak).disconnect();
            final ClientSettings clientSettings = Preconditions.checkNotNull(this.zar);
            this.zao = null;
        }
    }
    
    private final void zaC() {
        this.zaa.zai();
        zabj.zaa().execute(new zaak(this));
        final zae zak = this.zak;
        if (zak != null) {
            if (this.zap) {
                zak.zac((IAccountAccessor)Preconditions.checkNotNull(this.zao), this.zaq);
            }
            this.zaB(false);
        }
        final Iterator<Api.AnyClientKey<?>> iterator = this.zaa.zab.keySet().iterator();
        while (iterator.hasNext()) {
            Preconditions.checkNotNull(this.zaa.zaa.get(iterator.next())).disconnect();
        }
        Bundle zai;
        if (((BaseBundle)this.zai).isEmpty()) {
            zai = null;
        }
        else {
            zai = this.zai;
        }
        this.zaa.zah.zab(zai);
    }
    
    private final void zaD(final ConnectionResult connectionResult) {
        this.zaz();
        this.zaB(connectionResult.hasResolution() ^ true);
        this.zaa.zak(connectionResult);
        this.zaa.zah.zaa(connectionResult);
    }
    
    private final void zaE(final ConnectionResult zae, final Api<?> api, final boolean b) {
        final int priority = api.zac().getPriority();
        Label_0064: {
            if (b) {
                if (!zae.hasResolution()) {
                    if (this.zad.getErrorResolutionIntent(zae.getErrorCode()) == null) {
                        break Label_0064;
                    }
                }
            }
            if (this.zae == null || priority < this.zaf) {
                this.zae = zae;
                this.zaf = priority;
            }
        }
        this.zaa.zab.put(api.zab(), zae);
    }
    
    private final void zaF() {
        if (this.zah != 0) {
            return;
        }
        if (!this.zam || this.zan) {
            final ArrayList list = new ArrayList();
            this.zag = 1;
            this.zah = this.zaa.zaa.size();
            for (final Api.AnyClientKey anyClientKey : this.zaa.zaa.keySet()) {
                if (this.zaa.zab.containsKey(anyClientKey)) {
                    if (!this.zaH()) {
                        continue;
                    }
                    this.zaC();
                }
                else {
                    list.add(this.zaa.zaa.get(anyClientKey));
                }
            }
            if (!list.isEmpty()) {
                this.zau.add(zabj.zaa().submit(new zaap(list)));
            }
        }
    }
    
    private final boolean zaG(final int n) {
        if (this.zag != n) {
            Log.w("GACConnecting", this.zaa.zag.zaf());
            Log.w("GACConnecting", "Unexpected callback in ".concat(this.toString()));
            final int zah = this.zah;
            final StringBuilder sb = new StringBuilder(33);
            sb.append("mRemainingConnections=");
            sb.append(zah);
            Log.w("GACConnecting", sb.toString());
            final String zaJ = zaJ(this.zag);
            final String zaJ2 = zaJ(n);
            final StringBuilder sb2 = new StringBuilder(zaJ.length() + 70 + zaJ2.length());
            sb2.append("GoogleApiClient connecting is in step ");
            sb2.append(zaJ);
            sb2.append(" but received callback for step ");
            sb2.append(zaJ2);
            Log.e("GACConnecting", sb2.toString(), (Throwable)new Exception());
            this.zaD(new ConnectionResult(8, null));
            return false;
        }
        return true;
    }
    
    private final boolean zaH() {
        final int zah = this.zah - 1;
        this.zah = zah;
        if (zah > 0) {
            return false;
        }
        ConnectionResult zae;
        if (zah < 0) {
            Log.w("GACConnecting", this.zaa.zag.zaf());
            Log.wtf("GACConnecting", "GoogleApiClient received too many callbacks for the given step. Clients may be in an unexpected state; GoogleApiClient will now disconnect.", (Throwable)new Exception());
            zae = new ConnectionResult(8, null);
        }
        else {
            zae = this.zae;
            if (zae == null) {
                return true;
            }
            this.zaa.zaf = this.zaf;
        }
        this.zaD(zae);
        return false;
    }
    
    private final boolean zaI(final ConnectionResult connectionResult) {
        return this.zal && !connectionResult.hasResolution();
    }
    
    private static final String zaJ(final int n) {
        if (n != 0) {
            return "STEP_GETTING_REMOTE_SERVICE";
        }
        return "STEP_SERVICE_BINDINGS_AND_SIGN_IN";
    }
    
    private final void zaz() {
        final ArrayList<Future<?>> zau = this.zau;
        for (int size = zau.size(), i = 0; i < size; ++i) {
            ((Future)zau.get(i)).cancel(true);
        }
        this.zau.clear();
    }
    
    @Override
    public final <A extends Api.AnyClient, R extends Result, T extends BaseImplementation.ApiMethodImpl<R, A>> T zaa(final T t) {
        this.zaa.zag.zaa.add(t);
        return t;
    }
    
    @Override
    public final <A extends Api.AnyClient, T extends BaseImplementation.ApiMethodImpl<? extends Result, A>> T zab(final T t) {
        throw new IllegalStateException("GoogleApiClient is not connected yet.");
    }
    
    @Override
    public final void zad() {
        this.zaa.zab.clear();
        this.zam = false;
        this.zae = null;
        this.zag = 0;
        this.zal = true;
        this.zan = false;
        this.zap = false;
        final HashMap hashMap = new HashMap();
        final Iterator<Api<?>> iterator = this.zas.keySet().iterator();
        boolean b = false;
        while (iterator.hasNext()) {
            final Api api = iterator.next();
            final Api.Client client = Preconditions.checkNotNull(this.zaa.zaa.get(api.zab()));
            b |= (api.zac().getPriority() == 1);
            final boolean booleanValue = this.zas.get(api);
            if (client.requiresSignIn()) {
                this.zam = true;
                if (booleanValue) {
                    this.zaj.add((Api.AnyClientKey)api.zab());
                }
                else {
                    this.zal = false;
                }
            }
            hashMap.put(client, new zaal(this, api, booleanValue));
        }
        if (b) {
            this.zam = false;
        }
        if (this.zam) {
            Preconditions.checkNotNull(this.zar);
            Preconditions.checkNotNull(this.zat);
            this.zar.zae(System.identityHashCode(this.zaa.zag));
            final zaat zaat = new zaat(this, null);
            final Api.AbstractClientBuilder<? extends zae, SignInOptions> zat = this.zat;
            final Context zac = this.zac;
            final Looper looper = this.zaa.zag.getLooper();
            final ClientSettings zar = this.zar;
            this.zak = (zae)zat.buildClient(zac, looper, zar, zar.zaa(), zaat, zaat);
        }
        this.zah = this.zaa.zaa.size();
        this.zau.add(zabj.zaa().submit(new zaao(hashMap)));
    }
    
    @Override
    public final void zae() {
    }
    
    @Override
    public final void zag(final Bundle bundle) {
        if (!this.zaG(1)) {
            return;
        }
        if (bundle != null) {
            this.zai.putAll(bundle);
        }
        if (this.zaH()) {
            this.zaC();
        }
    }
    
    @Override
    public final void zah(final ConnectionResult connectionResult, final Api<?> api, final boolean b) {
        if (!this.zaG(1)) {
            return;
        }
        this.zaE(connectionResult, api, b);
        if (this.zaH()) {
            this.zaC();
        }
    }
    
    @Override
    public final void zai(final int n) {
        this.zaD(new ConnectionResult(8, null));
    }
    
    @Override
    public final boolean zaj() {
        this.zaz();
        this.zaB(true);
        this.zaa.zak(null);
        return true;
    }
}

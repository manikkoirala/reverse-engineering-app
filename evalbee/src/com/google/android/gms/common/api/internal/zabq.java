// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.internal.service.zap;
import com.google.android.gms.common.internal.BaseGmsClient;
import android.os.Looper;
import android.os.Bundle;
import com.google.android.gms.common.util.ArrayUtils;
import com.google.android.gms.common.api.UnsupportedApiCallException;
import android.app.PendingIntent;
import android.util.Log;
import android.os.Message;
import android.os.RemoteException;
import android.os.DeadObjectException;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.Collection;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.api.Status;
import java.util.Iterator;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.Feature;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.ConnectionResult;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.checkerframework.checker.initialization.qual.NotOnlyInitialized;
import java.util.Queue;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Api;

public final class zabq<O extends Api.ApiOptions> implements ConnectionCallbacks, OnConnectionFailedListener, zau
{
    final GoogleApiManager zaa;
    private final Queue<zai> zab;
    @NotOnlyInitialized
    private final Api.Client zac;
    private final ApiKey<O> zad;
    private final zaad zae;
    private final Set<zal> zaf;
    private final Map<ListenerHolder.ListenerKey<?>, zaci> zag;
    private final int zah;
    private final zact zai;
    private boolean zaj;
    private final List<zabs> zak;
    private ConnectionResult zal;
    private int zam;
    
    public zabq(final GoogleApiManager zaa, final GoogleApi<O> googleApi) {
        this.zaa = zaa;
        this.zab = new LinkedList<zai>();
        this.zaf = new HashSet<zal>();
        this.zag = new HashMap<ListenerHolder.ListenerKey<?>, zaci>();
        this.zak = new ArrayList<zabs>();
        this.zal = null;
        this.zam = 0;
        final Api.Client zab = googleApi.zab(GoogleApiManager.zaf(zaa).getLooper(), this);
        this.zac = zab;
        this.zad = googleApi.getApiKey();
        this.zae = new zaad();
        this.zah = googleApi.zaa();
        if (zab.requiresSignIn()) {
            this.zai = googleApi.zac(GoogleApiManager.zae(zaa), GoogleApiManager.zaf(zaa));
            return;
        }
        this.zai = null;
    }
    
    private final Feature zaB(final Feature[] array) {
        if (array != null) {
            if (array.length != 0) {
                final Feature[] availableFeatures = this.zac.getAvailableFeatures();
                final int n = 0;
                Feature[] array2;
                if ((array2 = availableFeatures) == null) {
                    array2 = new Feature[0];
                }
                final int length = array2.length;
                final r8 r8 = new r8(length);
                for (final Feature feature : array2) {
                    r8.put(feature.getName(), feature.getVersion());
                }
                for (int length2 = array.length, j = n; j < length2; ++j) {
                    final Feature feature2 = array[j];
                    final Long n2 = r8.get(feature2.getName());
                    if (n2 == null || n2 < feature2.getVersion()) {
                        return feature2;
                    }
                }
            }
        }
        return null;
    }
    
    private final void zaC(final ConnectionResult connectionResult) {
        for (final zal zal : this.zaf) {
            String endpointPackageName;
            if (Objects.equal(connectionResult, ConnectionResult.RESULT_SUCCESS)) {
                endpointPackageName = this.zac.getEndpointPackageName();
            }
            else {
                endpointPackageName = null;
            }
            zal.zac(this.zad, connectionResult, endpointPackageName);
        }
        this.zaf.clear();
    }
    
    private final void zaD(final Status status) {
        Preconditions.checkHandlerThread(GoogleApiManager.zaf(this.zaa));
        this.zaE(status, null, false);
    }
    
    private final void zaE(final Status status, final Exception ex, final boolean b) {
        Preconditions.checkHandlerThread(GoogleApiManager.zaf(this.zaa));
        int n = false ? 1 : 0;
        final boolean b2 = status == null;
        if (ex == null) {
            n = (true ? 1 : 0);
        }
        if ((b2 ? 1 : 0) != n) {
            final Iterator<Object> iterator = this.zab.iterator();
            while (iterator.hasNext()) {
                final zai zai = iterator.next();
                if (!b || zai.zac == 2) {
                    if (status != null) {
                        zai.zad(status);
                    }
                    else {
                        zai.zae(ex);
                    }
                    iterator.remove();
                }
            }
            return;
        }
        throw new IllegalArgumentException("Status XOR exception should be null");
    }
    
    private final void zaF() {
        final ArrayList list = new ArrayList((Collection<? extends E>)this.zab);
        for (int size = list.size(), i = 0; i < size; ++i) {
            final zai zai = (zai)list.get(i);
            if (!this.zac.isConnected()) {
                break;
            }
            if (this.zaL(zai)) {
                this.zab.remove(zai);
            }
        }
    }
    
    private final void zaG() {
        this.zan();
        this.zaC(ConnectionResult.RESULT_SUCCESS);
        this.zaK();
        final Iterator<zaci> iterator = this.zag.values().iterator();
        while (iterator.hasNext()) {
            final zaci zaci = iterator.next();
            if (this.zaB(zaci.zaa.getRequiredFeatures()) == null) {
                try {
                    zaci.zaa.registerListener(this.zac, (TaskCompletionSource<Void>)new TaskCompletionSource());
                    continue;
                }
                catch (final DeadObjectException ex) {
                    this.onConnectionSuspended(3);
                    this.zac.disconnect("DeadObjectException thrown while calling register listener method.");
                }
                catch (final RemoteException ex2) {}
            }
            iterator.remove();
        }
        goto Label_0119;
    }
    
    private final void zaH(final int n) {
        this.zan();
        this.zaj = true;
        this.zae.zae(n, this.zac.getLastDisconnectMessage());
        final GoogleApiManager zaa = this.zaa;
        GoogleApiManager.zaf(zaa).sendMessageDelayed(Message.obtain(GoogleApiManager.zaf(zaa), 9, (Object)this.zad), GoogleApiManager.zab(this.zaa));
        final GoogleApiManager zaa2 = this.zaa;
        GoogleApiManager.zaf(zaa2).sendMessageDelayed(Message.obtain(GoogleApiManager.zaf(zaa2), 11, (Object)this.zad), GoogleApiManager.zac(this.zaa));
        GoogleApiManager.zan(this.zaa).zac();
        final Iterator<zaci> iterator = this.zag.values().iterator();
        while (iterator.hasNext()) {
            iterator.next().zac.run();
        }
    }
    
    private final void zaI() {
        GoogleApiManager.zaf(this.zaa).removeMessages(12, (Object)this.zad);
        final GoogleApiManager zaa = this.zaa;
        GoogleApiManager.zaf(zaa).sendMessageDelayed(GoogleApiManager.zaf(zaa).obtainMessage(12, (Object)this.zad), GoogleApiManager.zad(this.zaa));
    }
    
    private final void zaJ(final zai zai) {
        zai.zag(this.zae, this.zaz());
        try {
            zai.zaf(this);
        }
        catch (final DeadObjectException ex) {
            this.onConnectionSuspended(1);
            this.zac.disconnect("DeadObjectException thrown while running ApiCallRunner.");
        }
    }
    
    private final void zaK() {
        if (this.zaj) {
            GoogleApiManager.zaf(this.zaa).removeMessages(11, (Object)this.zad);
            GoogleApiManager.zaf(this.zaa).removeMessages(9, (Object)this.zad);
            this.zaj = false;
        }
    }
    
    private final boolean zaL(final zai zai) {
        if (!(zai instanceof zac)) {
            this.zaJ(zai);
            return true;
        }
        final zac zac = (zac)zai;
        final Feature zaB = this.zaB(zac.zab(this));
        if (zaB == null) {
            this.zaJ(zai);
            return true;
        }
        final String name = this.zac.getClass().getName();
        final String name2 = zaB.getName();
        final long version = zaB.getVersion();
        final StringBuilder sb = new StringBuilder(name.length() + 77 + String.valueOf(name2).length());
        sb.append(name);
        sb.append(" could not execute call because it requires feature (");
        sb.append(name2);
        sb.append(", ");
        sb.append(version);
        sb.append(").");
        Log.w("GoogleApiManager", sb.toString());
        if (GoogleApiManager.zaE(this.zaa) && zac.zaa(this)) {
            final zabs zabs = new zabs(this.zad, zaB, null);
            final int index = this.zak.indexOf(zabs);
            if (index >= 0) {
                final zabs zabs2 = this.zak.get(index);
                GoogleApiManager.zaf(this.zaa).removeMessages(15, (Object)zabs2);
                final GoogleApiManager zaa = this.zaa;
                GoogleApiManager.zaf(zaa).sendMessageDelayed(Message.obtain(GoogleApiManager.zaf(zaa), 15, (Object)zabs2), GoogleApiManager.zab(this.zaa));
            }
            else {
                this.zak.add(zabs);
                final GoogleApiManager zaa2 = this.zaa;
                GoogleApiManager.zaf(zaa2).sendMessageDelayed(Message.obtain(GoogleApiManager.zaf(zaa2), 15, (Object)zabs), GoogleApiManager.zab(this.zaa));
                final GoogleApiManager zaa3 = this.zaa;
                GoogleApiManager.zaf(zaa3).sendMessageDelayed(Message.obtain(GoogleApiManager.zaf(zaa3), 16, (Object)zabs), GoogleApiManager.zac(this.zaa));
                final ConnectionResult connectionResult = new ConnectionResult(2, null);
                if (!this.zaM(connectionResult)) {
                    this.zaa.zaG(connectionResult, this.zah);
                }
            }
            return false;
        }
        zac.zae(new UnsupportedApiCallException(zaB));
        return true;
    }
    
    private final boolean zaM(final ConnectionResult connectionResult) {
        synchronized (GoogleApiManager.zas()) {
            final GoogleApiManager zaa = this.zaa;
            if (GoogleApiManager.zaj(zaa) != null && GoogleApiManager.zau(zaa).contains(this.zad)) {
                GoogleApiManager.zaj(this.zaa).zah(connectionResult, this.zah);
                return true;
            }
            return false;
        }
    }
    
    private final boolean zaN(final boolean b) {
        Preconditions.checkHandlerThread(GoogleApiManager.zaf(this.zaa));
        if (!this.zac.isConnected() || this.zag.size() != 0) {
            return false;
        }
        if (this.zae.zag()) {
            if (b) {
                this.zaI();
            }
            return false;
        }
        this.zac.disconnect("Timing out service connection.");
        return true;
    }
    
    @Override
    public final void onConnected(final Bundle bundle) {
        if (Looper.myLooper() == GoogleApiManager.zaf(this.zaa).getLooper()) {
            this.zaG();
            return;
        }
        GoogleApiManager.zaf(this.zaa).post((Runnable)new zabm(this));
    }
    
    @Override
    public final void onConnectionFailed(final ConnectionResult connectionResult) {
        this.zar(connectionResult, null);
    }
    
    @Override
    public final void onConnectionSuspended(final int n) {
        if (Looper.myLooper() == GoogleApiManager.zaf(this.zaa).getLooper()) {
            this.zaH(n);
            return;
        }
        GoogleApiManager.zaf(this.zaa).post((Runnable)new zabn(this, n));
    }
    
    public final boolean zaA() {
        return this.zaN(true);
    }
    
    @Override
    public final void zaa(final ConnectionResult connectionResult, final Api<?> api, final boolean b) {
        throw null;
    }
    
    public final int zab() {
        return this.zah;
    }
    
    public final int zac() {
        return this.zam;
    }
    
    public final ConnectionResult zad() {
        Preconditions.checkHandlerThread(GoogleApiManager.zaf(this.zaa));
        return this.zal;
    }
    
    public final Api.Client zaf() {
        return this.zac;
    }
    
    public final Map<ListenerHolder.ListenerKey<?>, zaci> zah() {
        return this.zag;
    }
    
    public final void zan() {
        Preconditions.checkHandlerThread(GoogleApiManager.zaf(this.zaa));
        this.zal = null;
    }
    
    public final void zao() {
        Preconditions.checkHandlerThread(GoogleApiManager.zaf(this.zaa));
        if (!this.zac.isConnected()) {
            if (!this.zac.isConnecting()) {
                while (true) {
                    try {
                        final GoogleApiManager zaa = this.zaa;
                        final int zab = GoogleApiManager.zan(zaa).zab(GoogleApiManager.zae(zaa), this.zac);
                        if (zab != 0) {
                            final ConnectionResult connectionResult = new ConnectionResult(zab, null);
                            final String name = this.zac.getClass().getName();
                            final String string = connectionResult.toString();
                            final StringBuilder sb = new StringBuilder(name.length() + 35 + string.length());
                            sb.append("The service for ");
                            sb.append(name);
                            sb.append(" is not available: ");
                            sb.append(string);
                            Log.w("GoogleApiManager", sb.toString());
                            this.zar(connectionResult, null);
                            return;
                        }
                        final GoogleApiManager zaa2 = this.zaa;
                        final Api.Client zac = this.zac;
                        final zabu zabu = new zabu(zac, this.zad);
                        if (zac.requiresSignIn()) {
                            Preconditions.checkNotNull(this.zai).zae(zabu);
                        }
                        ConnectionResult connectionResult2;
                        try {
                            this.zac.connect(zabu);
                            return;
                        }
                        catch (final SecurityException ex) {
                            connectionResult2 = new ConnectionResult(10);
                        }
                        final SecurityException ex;
                        this.zar(connectionResult2, ex);
                    }
                    catch (final IllegalStateException ex) {
                        final ConnectionResult connectionResult2 = new ConnectionResult(10);
                        continue;
                    }
                    break;
                }
            }
        }
    }
    
    public final void zap(final zai zai) {
        Preconditions.checkHandlerThread(GoogleApiManager.zaf(this.zaa));
        if (this.zac.isConnected()) {
            if (this.zaL(zai)) {
                this.zaI();
                return;
            }
            this.zab.add(zai);
        }
        else {
            this.zab.add(zai);
            final ConnectionResult zal = this.zal;
            if (zal != null && zal.hasResolution()) {
                this.zar(this.zal, null);
                return;
            }
            this.zao();
        }
    }
    
    public final void zaq() {
        ++this.zam;
    }
    
    public final void zar(final ConnectionResult zal, final Exception ex) {
        Preconditions.checkHandlerThread(GoogleApiManager.zaf(this.zaa));
        final zact zai = this.zai;
        if (zai != null) {
            zai.zaf();
        }
        this.zan();
        GoogleApiManager.zan(this.zaa).zac();
        this.zaC(zal);
        if (this.zac instanceof zap && zal.getErrorCode() != 24) {
            GoogleApiManager.zav(this.zaa, true);
            final GoogleApiManager zaa = this.zaa;
            GoogleApiManager.zaf(zaa).sendMessageDelayed(GoogleApiManager.zaf(zaa).obtainMessage(19), 300000L);
        }
        if (zal.getErrorCode() == 4) {
            this.zaD(GoogleApiManager.zah());
            return;
        }
        if (this.zab.isEmpty()) {
            this.zal = zal;
            return;
        }
        if (ex != null) {
            Preconditions.checkHandlerThread(GoogleApiManager.zaf(this.zaa));
            this.zaE(null, ex, false);
            return;
        }
        if (!GoogleApiManager.zaE(this.zaa)) {
            this.zaD(GoogleApiManager.zai((ApiKey)this.zad, zal));
            return;
        }
        this.zaE(GoogleApiManager.zai((ApiKey)this.zad, zal), null, true);
        if (this.zab.isEmpty()) {
            return;
        }
        if (this.zaM(zal)) {
            return;
        }
        if (!this.zaa.zaG(zal, this.zah)) {
            if (zal.getErrorCode() == 18) {
                this.zaj = true;
            }
            if (this.zaj) {
                final GoogleApiManager zaa2 = this.zaa;
                GoogleApiManager.zaf(zaa2).sendMessageDelayed(Message.obtain(GoogleApiManager.zaf(zaa2), 9, (Object)this.zad), GoogleApiManager.zab(this.zaa));
                return;
            }
            this.zaD(GoogleApiManager.zai((ApiKey)this.zad, zal));
        }
    }
    
    public final void zas(final ConnectionResult obj) {
        Preconditions.checkHandlerThread(GoogleApiManager.zaf(this.zaa));
        final Api.Client zac = this.zac;
        final String name = zac.getClass().getName();
        final String value = String.valueOf(obj);
        final StringBuilder sb = new StringBuilder(name.length() + 25 + value.length());
        sb.append("onSignInFailed for ");
        sb.append(name);
        sb.append(" with ");
        sb.append(value);
        zac.disconnect(sb.toString());
        this.zar(obj, null);
    }
    
    public final void zat(final zal zal) {
        Preconditions.checkHandlerThread(GoogleApiManager.zaf(this.zaa));
        this.zaf.add(zal);
    }
    
    public final void zau() {
        Preconditions.checkHandlerThread(GoogleApiManager.zaf(this.zaa));
        if (this.zaj) {
            this.zao();
        }
    }
    
    public final void zav() {
        Preconditions.checkHandlerThread(GoogleApiManager.zaf(this.zaa));
        this.zaD(GoogleApiManager.zaa);
        this.zae.zaf();
        final Set<ListenerHolder.ListenerKey<?>> keySet = this.zag.keySet();
        int i = 0;
        for (ListenerHolder.ListenerKey[] array = (ListenerHolder.ListenerKey[])keySet.toArray((ListenerHolder.ListenerKey[])new ListenerHolder.ListenerKey[0]); i < array.length; ++i) {
            this.zap(new zah(array[i], (TaskCompletionSource<Boolean>)new TaskCompletionSource()));
        }
        this.zaC(new ConnectionResult(4));
        if (this.zac.isConnected()) {
            this.zac.onUserSignOut(new zabp(this));
        }
    }
    
    public final void zaw() {
        Preconditions.checkHandlerThread(GoogleApiManager.zaf(this.zaa));
        if (this.zaj) {
            this.zaK();
            final GoogleApiManager zaa = this.zaa;
            Status status;
            if (GoogleApiManager.zag(zaa).isGooglePlayServicesAvailable(GoogleApiManager.zae(zaa)) == 18) {
                status = new Status(21, "Connection timed out waiting for Google Play services update to complete.");
            }
            else {
                status = new Status(22, "API failed to connect while resuming due to an unknown error.");
            }
            this.zaD(status);
            this.zac.disconnect("Timing out connection while resuming.");
        }
    }
    
    public final boolean zay() {
        return this.zac.isConnected();
    }
    
    public final boolean zaz() {
        return this.zac.requiresSignIn();
    }
}

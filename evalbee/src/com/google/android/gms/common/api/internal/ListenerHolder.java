// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.concurrent.HandlerExecutor;
import android.os.Looper;
import java.util.concurrent.Executor;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public final class ListenerHolder<L>
{
    private final Executor zaa;
    private volatile L zab;
    private volatile ListenerKey<L> zac;
    
    @KeepForSdk
    public ListenerHolder(final Looper looper, final L l, final String s) {
        this.zaa = new HandlerExecutor(looper);
        this.zab = Preconditions.checkNotNull(l, "Listener must not be null");
        this.zac = new ListenerKey<L>(l, Preconditions.checkNotEmpty(s));
    }
    
    @KeepForSdk
    public ListenerHolder(final Executor executor, final L l, final String s) {
        this.zaa = Preconditions.checkNotNull(executor, "Executor must not be null");
        this.zab = Preconditions.checkNotNull(l, "Listener must not be null");
        this.zac = new ListenerKey<L>(l, Preconditions.checkNotEmpty(s));
    }
    
    @KeepForSdk
    public void clear() {
        this.zab = null;
        this.zac = null;
    }
    
    @KeepForSdk
    public ListenerKey<L> getListenerKey() {
        return this.zac;
    }
    
    @KeepForSdk
    public boolean hasListener() {
        return this.zab != null;
    }
    
    @KeepForSdk
    public void notifyListener(final Notifier<? super L> notifier) {
        Preconditions.checkNotNull(notifier, "Notifier must not be null");
        this.zaa.execute(new zacb(this, (Notifier)notifier));
    }
    
    public final void zaa(final Notifier<? super L> notifier) {
        final L zab = this.zab;
        if (zab == null) {
            notifier.onNotifyListenerFailed();
            return;
        }
        try {
            notifier.notifyListener(zab);
        }
        catch (final RuntimeException ex) {
            notifier.onNotifyListenerFailed();
            throw ex;
        }
    }
    
    @KeepForSdk
    public static final class ListenerKey<L>
    {
        private final L zaa;
        private final String zab;
        
        @KeepForSdk
        public ListenerKey(final L zaa, final String zab) {
            this.zaa = zaa;
            this.zab = zab;
        }
        
        @KeepForSdk
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof ListenerKey)) {
                return false;
            }
            final ListenerKey listenerKey = (ListenerKey)o;
            return this.zaa == listenerKey.zaa && this.zab.equals(listenerKey.zab);
        }
        
        @KeepForSdk
        @Override
        public int hashCode() {
            return System.identityHashCode(this.zaa) * 31 + this.zab.hashCode();
        }
        
        @KeepForSdk
        public String toIdString() {
            final String zab = this.zab;
            final int identityHashCode = System.identityHashCode(this.zaa);
            final StringBuilder sb = new StringBuilder(String.valueOf(zab).length() + 12);
            sb.append(zab);
            sb.append("@");
            sb.append(identityHashCode);
            return sb.toString();
        }
    }
    
    @KeepForSdk
    public interface Notifier<L>
    {
        @KeepForSdk
        void notifyListener(final L p0);
        
        @KeepForSdk
        void onNotifyListenerFailed();
    }
}

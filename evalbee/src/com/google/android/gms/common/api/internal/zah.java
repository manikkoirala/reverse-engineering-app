// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.Feature;
import com.google.android.gms.tasks.TaskCompletionSource;

public final class zah extends zad<Boolean>
{
    public final ListenerHolder.ListenerKey<?> zab;
    
    public zah(final ListenerHolder.ListenerKey<?> zab, final TaskCompletionSource<Boolean> taskCompletionSource) {
        super(4, taskCompletionSource);
        this.zab = zab;
    }
    
    @Override
    public final boolean zaa(final zabq<?> zabq) {
        final zaci zaci = (zaci)zabq.zah().get(this.zab);
        return zaci != null && zaci.zaa.zab();
    }
    
    @Override
    public final Feature[] zab(final zabq<?> zabq) {
        final zaci zaci = (zaci)zabq.zah().get(this.zab);
        if (zaci == null) {
            return null;
        }
        return zaci.zaa.getRequiredFeatures();
    }
    
    @Override
    public final void zac(final zabq<?> zabq) {
        final zaci zaci = (zaci)zabq.zah().remove(this.zab);
        if (zaci != null) {
            zaci.zab.unregisterListener(zabq.zaf(), (TaskCompletionSource<Boolean>)super.zaa);
            zaci.zaa.clearListener();
            return;
        }
        super.zaa.trySetResult((Object)Boolean.FALSE);
    }
}

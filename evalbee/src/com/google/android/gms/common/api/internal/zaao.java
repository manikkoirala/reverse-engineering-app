// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import java.util.Iterator;
import com.google.android.gms.common.internal.BaseGmsClient;
import android.app.PendingIntent;
import com.google.android.gms.common.ConnectionResult;
import java.util.ArrayList;
import com.google.android.gms.common.internal.zal;
import com.google.android.gms.common.api.Api;
import java.util.Map;

final class zaao extends zaav
{
    final zaaw zaa;
    private final Map<Api.Client, zaal> zac;
    
    public zaao(final zaaw zaa, final Map<Api.Client, zaal> zac) {
        super(this.zaa = zaa, null);
        this.zac = zac;
    }
    
    @Override
    public final void zaa() {
        final zal zal = new zal(zaaw.zaf(this.zaa));
        final ArrayList list = new ArrayList();
        final ArrayList list2 = new ArrayList();
        for (final Api.Client client : this.zac.keySet()) {
            if (client.requiresGooglePlayServices() && !zaal.zaa((zaal)this.zac.get(client))) {
                list.add(client);
            }
            else {
                list2.add(client);
            }
        }
        final boolean empty = list.isEmpty();
        int n = -1;
        final int n2 = 0;
        int i = 0;
        if (empty) {
            while (i < list2.size()) {
                final int zab = zal.zab(zaaw.zac(this.zaa), (Api.Client)list2.get(i));
                ++i;
                if ((n = zab) == 0) {
                    n = zab;
                    break;
                }
            }
        }
        else {
            final int size = list.size();
            int j = n2;
            while (j < size) {
                final int zab2 = zal.zab(zaaw.zac(this.zaa), (Api.Client)list.get(j));
                ++j;
                if ((n = zab2) != 0) {
                    n = zab2;
                    break;
                }
            }
        }
        if (n != 0) {
            final ConnectionResult connectionResult = new ConnectionResult(n, null);
            final zaaw zaa = this.zaa;
            zaaw.zak(zaa).zal(new zaam(this, zaa, connectionResult));
            return;
        }
        final zaaw zaa2 = this.zaa;
        if (zaaw.zav(zaa2) && zaaw.zan(zaa2) != null) {
            zaaw.zan(zaa2).zab();
        }
        for (final Api.Client client2 : this.zac.keySet()) {
            final BaseGmsClient.ConnectionProgressReportCallbacks connectionProgressReportCallbacks = this.zac.get(client2);
            if (client2.requiresGooglePlayServices() && zal.zab(zaaw.zac(this.zaa), client2) != 0) {
                final zaaw zaa3 = this.zaa;
                zaaw.zak(zaa3).zal(new zaan(this, zaa3, connectionProgressReportCallbacks));
            }
            else {
                client2.connect(connectionProgressReportCallbacks);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.ConnectionResult;

final class zam
{
    private final int zaa;
    private final ConnectionResult zab;
    
    public zam(final ConnectionResult zab, final int zaa) {
        Preconditions.checkNotNull(zab);
        this.zab = zab;
        this.zaa = zaa;
    }
    
    public final int zaa() {
        return this.zaa;
    }
    
    public final ConnectionResult zab() {
        return this.zab;
    }
}

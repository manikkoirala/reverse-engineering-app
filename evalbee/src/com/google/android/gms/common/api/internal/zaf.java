// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.Feature;
import com.google.android.gms.tasks.TaskCompletionSource;

public final class zaf extends zad<Void>
{
    public final zaci zab;
    
    public zaf(final zaci zab, final TaskCompletionSource<Void> taskCompletionSource) {
        super(3, taskCompletionSource);
        this.zab = zab;
    }
    
    @Override
    public final boolean zaa(final zabq<?> zabq) {
        return this.zab.zaa.zab();
    }
    
    @Override
    public final Feature[] zab(final zabq<?> zabq) {
        return this.zab.zaa.getRequiredFeatures();
    }
    
    @Override
    public final void zac(final zabq<?> zabq) {
        this.zab.zaa.registerListener(zabq.zaf(), (TaskCompletionSource<Void>)super.zaa);
        final ListenerHolder.ListenerKey<?> listenerKey = this.zab.zaa.getListenerKey();
        if (listenerKey != null) {
            zabq.zah().put(listenerKey, this.zab);
        }
    }
}

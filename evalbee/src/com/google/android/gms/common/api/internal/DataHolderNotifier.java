// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public abstract class DataHolderNotifier<L> implements Notifier<L>
{
    private final DataHolder zaa;
    
    @KeepForSdk
    public DataHolderNotifier(final DataHolder zaa) {
        this.zaa = zaa;
    }
    
    @KeepForSdk
    @Override
    public final void notifyListener(final L l) {
        this.notifyListener(l, this.zaa);
    }
    
    @KeepForSdk
    public abstract void notifyListener(final L p0, final DataHolder p1);
    
    @KeepForSdk
    @Override
    public void onNotifyListenerFailed() {
        try (final DataHolder zaa = this.zaa) {}
    }
}

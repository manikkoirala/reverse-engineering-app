// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.auth.api.signin.internal.Storage;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.ResultCallback;

final class zabb implements ResultCallback<Status>
{
    final StatusPendingResult zaa;
    final boolean zab;
    final GoogleApiClient zac;
    final zabe zad;
    
    public zabb(final zabe zad, final StatusPendingResult zaa, final boolean zab, final GoogleApiClient zac) {
        this.zad = zad;
        this.zaa = zaa;
        this.zab = zab;
        this.zac = zac;
    }
}

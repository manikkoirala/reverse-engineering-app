// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

final class zaba implements OnConnectionFailedListener
{
    final StatusPendingResult zaa;
    
    public zaba(final zabe zabe, final StatusPendingResult zaa) {
        this.zaa = zaa;
    }
    
    @Override
    public final void onConnectionFailed(final ConnectionResult connectionResult) {
        this.zaa.setResult(new Status(8));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api;

import com.google.android.gms.common.api.internal.OnConnectionFailedListener;
import com.google.android.gms.common.api.internal.ConnectionCallbacks;
import com.google.android.gms.common.util.VisibleForTesting;
import android.os.Handler;
import android.app.Activity;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.common.api.internal.zak;
import java.util.concurrent.locks.Lock;
import com.google.android.gms.common.api.internal.zabe;
import java.util.concurrent.locks.ReentrantLock;
import com.google.android.gms.common.api.internal.zat;
import java.util.List;
import java.util.Collection;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.signin.zad;
import java.util.HashSet;
import java.util.ArrayList;
import com.google.android.gms.signin.SignInOptions;
import com.google.android.gms.signin.zae;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.internal.LifecycleActivity;
import com.google.android.gms.common.internal.zab;
import android.view.View;
import android.accounts.Account;
import com.google.android.gms.common.api.internal.zada;
import androidx.fragment.app.e;
import com.google.android.gms.common.api.internal.ListenerHolder;
import com.google.android.gms.common.api.internal.SignInConnectionListener;
import android.os.Looper;
import android.content.Context;
import com.google.android.gms.common.api.internal.BaseImplementation;
import java.util.concurrent.TimeUnit;
import com.google.android.gms.common.ConnectionResult;
import java.util.Iterator;
import java.io.PrintWriter;
import java.io.FileDescriptor;
import java.util.Map;
import java.util.Collections;
import java.util.WeakHashMap;
import java.util.Set;
import com.google.android.gms.common.annotation.KeepForSdk;

@Deprecated
@KeepForSdk
public abstract class GoogleApiClient
{
    @KeepForSdk
    public static final String DEFAULT_ACCOUNT = "<<default account>>";
    public static final int SIGN_IN_MODE_OPTIONAL = 2;
    public static final int SIGN_IN_MODE_REQUIRED = 1;
    private static final Set<GoogleApiClient> zaa;
    
    static {
        zaa = Collections.newSetFromMap(new WeakHashMap<GoogleApiClient, Boolean>());
    }
    
    public static void dumpAll(final String s, final FileDescriptor fileDescriptor, final PrintWriter printWriter, final String[] array) {
        final Set<GoogleApiClient> zaa = GoogleApiClient.zaa;
        synchronized (zaa) {
            final String concat = String.valueOf(s).concat("  ");
            final Iterator<GoogleApiClient> iterator = zaa.iterator();
            int x = 0;
            while (iterator.hasNext()) {
                final GoogleApiClient googleApiClient = iterator.next();
                printWriter.append(s).append("GoogleApiClient#").println(x);
                googleApiClient.dump(concat, fileDescriptor, printWriter, array);
                ++x;
            }
        }
    }
    
    @KeepForSdk
    public static Set<GoogleApiClient> getAllClients() {
        final Set<GoogleApiClient> zaa = GoogleApiClient.zaa;
        synchronized (zaa) {
            return zaa;
        }
    }
    
    public abstract ConnectionResult blockingConnect();
    
    public abstract ConnectionResult blockingConnect(final long p0, final TimeUnit p1);
    
    public abstract PendingResult<Status> clearDefaultAccountAndReconnect();
    
    public abstract void connect();
    
    public void connect(final int n) {
        throw new UnsupportedOperationException();
    }
    
    public abstract void disconnect();
    
    public abstract void dump(final String p0, final FileDescriptor p1, final PrintWriter p2, final String[] p3);
    
    @KeepForSdk
    public <A extends Api.AnyClient, R extends Result, T extends BaseImplementation.ApiMethodImpl<R, A>> T enqueue(final T t) {
        throw new UnsupportedOperationException();
    }
    
    @KeepForSdk
    public <A extends Api.AnyClient, T extends BaseImplementation.ApiMethodImpl<? extends Result, A>> T execute(final T t) {
        throw new UnsupportedOperationException();
    }
    
    @KeepForSdk
    public <C extends Api.Client> C getClient(final Api.AnyClientKey<C> anyClientKey) {
        throw new UnsupportedOperationException();
    }
    
    public abstract ConnectionResult getConnectionResult(final Api<?> p0);
    
    @KeepForSdk
    public Context getContext() {
        throw new UnsupportedOperationException();
    }
    
    @KeepForSdk
    public Looper getLooper() {
        throw new UnsupportedOperationException();
    }
    
    @KeepForSdk
    public boolean hasApi(final Api<?> api) {
        throw new UnsupportedOperationException();
    }
    
    public abstract boolean hasConnectedApi(final Api<?> p0);
    
    public abstract boolean isConnected();
    
    public abstract boolean isConnecting();
    
    public abstract boolean isConnectionCallbacksRegistered(final ConnectionCallbacks p0);
    
    public abstract boolean isConnectionFailedListenerRegistered(final OnConnectionFailedListener p0);
    
    @KeepForSdk
    public boolean maybeSignIn(final SignInConnectionListener signInConnectionListener) {
        throw new UnsupportedOperationException();
    }
    
    @KeepForSdk
    public void maybeSignOut() {
        throw new UnsupportedOperationException();
    }
    
    public abstract void reconnect();
    
    public abstract void registerConnectionCallbacks(final ConnectionCallbacks p0);
    
    public abstract void registerConnectionFailedListener(final OnConnectionFailedListener p0);
    
    @KeepForSdk
    public <L> ListenerHolder<L> registerListener(final L l) {
        throw new UnsupportedOperationException();
    }
    
    public abstract void stopAutoManage(final e p0);
    
    public abstract void unregisterConnectionCallbacks(final ConnectionCallbacks p0);
    
    public abstract void unregisterConnectionFailedListener(final OnConnectionFailedListener p0);
    
    public void zao(final zada zada) {
        throw new UnsupportedOperationException();
    }
    
    public void zap(final zada zada) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @KeepForSdk
    public static final class Builder
    {
        private Account zaa;
        private final Set<Scope> zab;
        private final Set<Scope> zac;
        private int zad;
        private View zae;
        private String zaf;
        private String zag;
        private final Map<Api<?>, zab> zah;
        private final Context zai;
        private final Map<Api<?>, Api.ApiOptions> zaj;
        private LifecycleActivity zak;
        private int zal;
        private OnConnectionFailedListener zam;
        private Looper zan;
        private GoogleApiAvailability zao;
        private Api.AbstractClientBuilder<? extends zae, SignInOptions> zap;
        private final ArrayList<ConnectionCallbacks> zaq;
        private final ArrayList<OnConnectionFailedListener> zar;
        
        @KeepForSdk
        public Builder(final Context zai) {
            this.zab = new HashSet<Scope>();
            this.zac = new HashSet<Scope>();
            this.zah = new r8();
            this.zaj = new r8();
            this.zal = -1;
            this.zao = GoogleApiAvailability.getInstance();
            this.zap = zad.zac;
            this.zaq = new ArrayList<ConnectionCallbacks>();
            this.zar = new ArrayList<OnConnectionFailedListener>();
            this.zai = zai;
            this.zan = zai.getMainLooper();
            this.zaf = zai.getPackageName();
            this.zag = zai.getClass().getName();
        }
        
        @KeepForSdk
        public Builder(final Context context, final ConnectionCallbacks e, final OnConnectionFailedListener e2) {
            this(context);
            Preconditions.checkNotNull(e, "Must provide a connected listener");
            this.zaq.add(e);
            Preconditions.checkNotNull(e2, "Must provide a connection failed listener");
            this.zar.add(e2);
        }
        
        private final <O extends Api.ApiOptions> void zab(final Api<O> api, final O o, final Scope... array) {
            final HashSet set = new HashSet((Collection<? extends E>)Preconditions.checkNotNull(api.zac(), "Base client builder must not be null").getImpliedScopes(o));
            for (int length = array.length, i = 0; i < length; ++i) {
                set.add(array[i]);
            }
            this.zah.put(api, new zab(set));
        }
        
        public Builder addApi(final Api<? extends Api.ApiOptions.NotRequiredOptions> api) {
            Preconditions.checkNotNull(api, "Api must not be null");
            this.zaj.put(api, null);
            final List<Scope> impliedScopes = ((Api.BaseClientBuilder)Preconditions.checkNotNull(api.zac(), "Base client builder must not be null")).getImpliedScopes(null);
            this.zac.addAll(impliedScopes);
            this.zab.addAll(impliedScopes);
            return this;
        }
        
        public <O extends Api.ApiOptions.HasOptions> Builder addApi(final Api<O> api, final O o) {
            Preconditions.checkNotNull(api, "Api must not be null");
            Preconditions.checkNotNull(o, "Null options are not permitted for this Api");
            this.zaj.put(api, (Api.ApiOptions)o);
            final List<Scope> impliedScopes = Preconditions.checkNotNull(api.zac(), "Base client builder must not be null").getImpliedScopes(o);
            this.zac.addAll(impliedScopes);
            this.zab.addAll(impliedScopes);
            return this;
        }
        
        public <O extends Api.ApiOptions.HasOptions> Builder addApiIfAvailable(final Api<O> api, final O o, final Scope... array) {
            Preconditions.checkNotNull(api, "Api must not be null");
            Preconditions.checkNotNull(o, "Null options are not permitted for this Api");
            this.zaj.put(api, (Api.ApiOptions)o);
            this.zab(api, o, array);
            return this;
        }
        
        public <T extends Api.ApiOptions.NotRequiredOptions> Builder addApiIfAvailable(final Api<? extends Api.ApiOptions.NotRequiredOptions> api, final Scope... array) {
            Preconditions.checkNotNull(api, "Api must not be null");
            this.zaj.put(api, null);
            this.zab((Api<Api.ApiOptions>)api, null, array);
            return this;
        }
        
        public Builder addConnectionCallbacks(final ConnectionCallbacks e) {
            Preconditions.checkNotNull(e, "Listener must not be null");
            this.zaq.add(e);
            return this;
        }
        
        public Builder addOnConnectionFailedListener(final OnConnectionFailedListener e) {
            Preconditions.checkNotNull(e, "Listener must not be null");
            this.zar.add(e);
            return this;
        }
        
        public Builder addScope(final Scope scope) {
            Preconditions.checkNotNull(scope, "Scope must not be null");
            this.zab.add(scope);
            return this;
        }
        
        public GoogleApiClient build() {
            Preconditions.checkArgument(this.zaj.isEmpty() ^ true, (Object)"must call addApi() to add at least one API");
            final ClientSettings zaa = this.zaa();
            final Map<Api<?>, zab> zad = zaa.zad();
            final r8 r8 = new r8();
            final r8 r9 = new r8();
            final ArrayList list = new ArrayList();
            final Iterator<Api<?>> iterator = this.zaj.keySet().iterator();
            final boolean b = false;
            Api api = null;
            int n = 0;
            while (iterator.hasNext()) {
                final Api api2 = iterator.next();
                final Api.ApiOptions value = this.zaj.get(api2);
                final boolean b2 = zad.get(api2) != null;
                r8.put(api2, b2);
                final zat e = new zat(api2, b2);
                list.add(e);
                final Api.AbstractClientBuilder abstractClientBuilder = Preconditions.checkNotNull(api2.zaa());
                final Api.Client buildClient = abstractClientBuilder.buildClient(this.zai, this.zan, zaa, value, e, e);
                r9.put(api2.zab(), buildClient);
                int n2 = n;
                if (((Api.BaseClientBuilder)abstractClientBuilder).getPriority() == 1) {
                    if (value != null) {
                        n2 = 1;
                    }
                    else {
                        n2 = 0;
                    }
                }
                n = n2;
                if (buildClient.providesSignIn()) {
                    if (api != null) {
                        final String zad2 = api2.zad();
                        final String zad3 = api.zad();
                        final StringBuilder sb = new StringBuilder(String.valueOf(zad2).length() + 21 + String.valueOf(zad3).length());
                        sb.append(zad2);
                        sb.append(" cannot be used with ");
                        sb.append(zad3);
                        throw new IllegalStateException(sb.toString());
                    }
                    api = api2;
                    n = n2;
                }
            }
            if (api != null) {
                if (n != 0) {
                    final String zad4 = api.zad();
                    final StringBuilder sb2 = new StringBuilder(String.valueOf(zad4).length() + 82);
                    sb2.append("With using ");
                    sb2.append(zad4);
                    sb2.append(", GamesOptions can only be specified within GoogleSignInOptions.Builder");
                    throw new IllegalStateException(sb2.toString());
                }
                boolean b3 = b;
                if (this.zaa == null) {
                    b3 = true;
                }
                Preconditions.checkState(b3, "Must not set an account in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead", api.zad());
                Preconditions.checkState(this.zab.equals(this.zac), "Must not set scopes in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead.", api.zad());
            }
            final zabe zabe = new zabe(this.zai, new ReentrantLock(), this.zan, zaa, this.zao, this.zap, r8, this.zaq, this.zar, r9, this.zal, com.google.android.gms.common.api.internal.zabe.zad((Iterable<Api.Client>)r9.values(), true), list);
            synchronized (GoogleApiClient.zaq()) {
                GoogleApiClient.zaq().add(zabe);
                monitorexit(GoogleApiClient.zaq());
                if (this.zal >= 0) {
                    com.google.android.gms.common.api.internal.zak.zaa(this.zak).zad(this.zal, zabe, this.zam);
                }
                return zabe;
            }
        }
        
        public Builder enableAutoManage(final e e, final int zal, final OnConnectionFailedListener zam) {
            final LifecycleActivity zak = new LifecycleActivity(e);
            Preconditions.checkArgument(zal >= 0, (Object)"clientId must be non-negative");
            this.zal = zal;
            this.zam = zam;
            this.zak = zak;
            return this;
        }
        
        public Builder enableAutoManage(final e e, final OnConnectionFailedListener onConnectionFailedListener) {
            this.enableAutoManage(e, 0, onConnectionFailedListener);
            return this;
        }
        
        public Builder setAccountName(final String s) {
            Account zaa;
            if (s == null) {
                zaa = null;
            }
            else {
                zaa = new Account(s, "com.google");
            }
            this.zaa = zaa;
            return this;
        }
        
        public Builder setGravityForPopups(final int zad) {
            this.zad = zad;
            return this;
        }
        
        public Builder setHandler(final Handler handler) {
            Preconditions.checkNotNull(handler, "Handler must not be null");
            this.zan = handler.getLooper();
            return this;
        }
        
        public Builder setViewForPopups(final View zae) {
            Preconditions.checkNotNull(zae, "View must not be null");
            this.zae = zae;
            return this;
        }
        
        public Builder useDefaultAccount() {
            this.setAccountName("<<default account>>");
            return this;
        }
        
        @VisibleForTesting
        public final ClientSettings zaa() {
            SignInOptions zaa = SignInOptions.zaa;
            final Map<Api<?>, Api.ApiOptions> zaj = this.zaj;
            final Api zag = com.google.android.gms.signin.zad.zag;
            if (zaj.containsKey(zag)) {
                zaa = (SignInOptions)this.zaj.get(zag);
            }
            return new ClientSettings(this.zaa, this.zab, this.zah, this.zad, this.zae, this.zaf, this.zag, zaa, false);
        }
    }
    
    @Deprecated
    public interface ConnectionCallbacks extends com.google.android.gms.common.api.internal.ConnectionCallbacks
    {
        public static final int CAUSE_NETWORK_LOST = 2;
        public static final int CAUSE_SERVICE_DISCONNECTED = 1;
    }
    
    @Deprecated
    public interface OnConnectionFailedListener extends com.google.android.gms.common.api.internal.OnConnectionFailedListener
    {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import java.util.Iterator;
import java.util.Set;
import com.google.android.gms.common.ConnectionResult;
import android.os.Bundle;
import android.os.DeadObjectException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Api;

public final class zaaj implements zabf
{
    private final zabi zaa;
    private boolean zab;
    
    public zaaj(final zabi zaa) {
        this.zab = false;
        this.zaa = zaa;
    }
    
    @Override
    public final <A extends Api.AnyClient, R extends Result, T extends BaseImplementation.ApiMethodImpl<R, A>> T zaa(final T t) {
        this.zab(t);
        return t;
    }
    
    @Override
    public final <A extends Api.AnyClient, T extends BaseImplementation.ApiMethodImpl<? extends Result, A>> T zab(final T t) {
        try {
            this.zaa.zag.zai.zaa(t);
            final Api.Client client = this.zaa.zag.zac.get(t.getClientKey());
            Preconditions.checkNotNull(client, "Appropriate Api was not requested.");
            if (!client.isConnected() && this.zaa.zab.containsKey(t.getClientKey())) {
                ((BaseImplementation.ApiMethodImpl)t).setFailedResult(new Status(17));
            }
            else {
                ((BaseImplementation.ApiMethodImpl<R, A>)t).run((A)client);
            }
        }
        catch (final DeadObjectException ex) {
            this.zaa.zal(new zaah(this, this));
        }
        return t;
    }
    
    @Override
    public final void zad() {
    }
    
    @Override
    public final void zae() {
        if (this.zab) {
            this.zab = false;
            this.zaa.zal(new zaai(this, this));
        }
    }
    
    public final void zaf() {
        if (this.zab) {
            this.zab = false;
            this.zaa.zag.zai.zab();
            this.zaj();
        }
    }
    
    @Override
    public final void zag(final Bundle bundle) {
    }
    
    @Override
    public final void zah(final ConnectionResult connectionResult, final Api<?> api, final boolean b) {
    }
    
    @Override
    public final void zai(final int n) {
        this.zaa.zak(null);
        this.zaa.zah.zac(n, this.zab);
    }
    
    @Override
    public final boolean zaj() {
        if (this.zab) {
            return false;
        }
        final Set<zada> zah = this.zaa.zag.zah;
        if (zah != null && !zah.isEmpty()) {
            this.zab = true;
            final Iterator iterator = zah.iterator();
            while (iterator.hasNext()) {
                ((zada)iterator.next()).zah();
            }
            return false;
        }
        this.zaa.zak(null);
        return true;
    }
}

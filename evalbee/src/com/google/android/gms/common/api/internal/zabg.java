// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

abstract class zabg
{
    private final zabf zaa;
    
    public zabg(final zabf zaa) {
        this.zaa = zaa;
    }
    
    public abstract void zaa();
    
    public final void zab(final zabi zabi) {
        zabi.zah(zabi).lock();
        try {
            if (zabi.zag(zabi) == this.zaa) {
                this.zaa();
            }
        }
        finally {
            zabi.zah(zabi).unlock();
        }
    }
}

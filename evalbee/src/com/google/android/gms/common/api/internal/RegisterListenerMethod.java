// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.api.Api;

@KeepForSdk
public abstract class RegisterListenerMethod<A extends Api.AnyClient, L>
{
    private final ListenerHolder<L> zaa;
    private final Feature[] zab;
    private final boolean zac;
    private final int zad;
    
    @KeepForSdk
    public RegisterListenerMethod(final ListenerHolder<L> listenerHolder) {
        this(listenerHolder, null, false, 0);
    }
    
    @KeepForSdk
    public RegisterListenerMethod(final ListenerHolder<L> listenerHolder, final Feature[] array, final boolean b) {
        this(listenerHolder, array, b, 0);
    }
    
    @KeepForSdk
    public RegisterListenerMethod(final ListenerHolder<L> zaa, final Feature[] zab, final boolean zac, final int zad) {
        this.zaa = zaa;
        this.zab = zab;
        this.zac = zac;
        this.zad = zad;
    }
    
    @KeepForSdk
    public void clearListener() {
        this.zaa.clear();
    }
    
    @KeepForSdk
    public ListenerHolder.ListenerKey<L> getListenerKey() {
        return this.zaa.getListenerKey();
    }
    
    @KeepForSdk
    public Feature[] getRequiredFeatures() {
        return this.zab;
    }
    
    @KeepForSdk
    public abstract void registerListener(final A p0, final TaskCompletionSource<Void> p1);
    
    public final int zaa() {
        return this.zad;
    }
    
    public final boolean zab() {
        return this.zac;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api;

import com.google.android.gms.common.api.internal.zacp;

public abstract class ResultTransform<R extends Result, S extends Result>
{
    public final PendingResult<S> createFailedResult(final Status status) {
        return new zacp<S>(status);
    }
    
    public Status onFailure(final Status status) {
        return status;
    }
    
    public abstract PendingResult<S> onSuccess(final R p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.tasks.Task;
import android.app.PendingIntent;
import android.content.Context;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.ConnectionResult;
import java.util.concurrent.CancellationException;
import android.app.Activity;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.tasks.TaskCompletionSource;

public final class zacc extends zap
{
    private TaskCompletionSource<Void> zad;
    
    private zacc(final LifecycleFragment lifecycleFragment) {
        super(lifecycleFragment, GoogleApiAvailability.getInstance());
        this.zad = (TaskCompletionSource<Void>)new TaskCompletionSource();
        super.mLifecycleFragment.addCallback("GmsAvailabilityHelper", this);
    }
    
    public static zacc zaa(final Activity activity) {
        final LifecycleFragment fragment = LifecycleCallback.getFragment(activity);
        final zacc zacc = fragment.getCallbackOrNull("GmsAvailabilityHelper", zacc.class);
        if (zacc != null) {
            if (zacc.zad.getTask().isComplete()) {
                zacc.zad = (TaskCompletionSource<Void>)new TaskCompletionSource();
            }
            return zacc;
        }
        return new zacc(fragment);
    }
    
    @Override
    public final void onDestroy() {
        super.onDestroy();
        this.zad.trySetException((Exception)new CancellationException("Host activity was destroyed before Google Play services could be made available."));
    }
    
    @Override
    public final void zab(final ConnectionResult connectionResult, final int n) {
        String errorMessage;
        if ((errorMessage = connectionResult.getErrorMessage()) == null) {
            errorMessage = "Error connecting to Google Play services";
        }
        this.zad.setException((Exception)new ApiException(new Status(connectionResult, errorMessage, connectionResult.getErrorCode())));
    }
    
    @Override
    public final void zac() {
        final Activity lifecycleActivity = super.mLifecycleFragment.getLifecycleActivity();
        if (lifecycleActivity == null) {
            this.zad.trySetException((Exception)new ApiException(new Status(8)));
            return;
        }
        final int googlePlayServicesAvailable = super.zac.isGooglePlayServicesAvailable((Context)lifecycleActivity);
        if (googlePlayServicesAvailable == 0) {
            this.zad.trySetResult((Object)null);
            return;
        }
        if (!this.zad.getTask().isComplete()) {
            this.zah(new ConnectionResult(googlePlayServicesAvailable, null), 0);
        }
    }
    
    public final Task<Void> zad() {
        return (Task<Void>)this.zad.getTask();
    }
}

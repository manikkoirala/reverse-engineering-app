// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Api;

final class zabn implements Runnable
{
    final int zaa;
    final zabq zab;
    
    public zabn(final zabq zab, final int zaa) {
        this.zab = zab;
        this.zaa = zaa;
    }
    
    @Override
    public final void run() {
        zabq.zak((zabq<Api.ApiOptions>)this.zab, this.zaa);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import android.os.Handler;
import java.util.Iterator;
import com.google.android.gms.common.internal.Preconditions;
import java.io.PrintWriter;
import java.io.FileDescriptor;
import com.google.android.gms.common.api.Result;
import java.util.concurrent.TimeUnit;
import android.app.PendingIntent;
import android.os.Bundle;
import java.util.HashMap;
import java.util.ArrayList;
import android.os.Looper;
import org.checkerframework.checker.initialization.qual.NotOnlyInitialized;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import android.content.Context;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import com.google.android.gms.signin.SignInOptions;
import com.google.android.gms.signin.zae;
import com.google.android.gms.common.internal.ClientSettings;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import java.util.Map;

public final class zabi implements zaca, zau
{
    final Map<Api.AnyClientKey<?>, Api.Client> zaa;
    final Map<Api.AnyClientKey<?>, ConnectionResult> zab;
    final ClientSettings zac;
    final Map<Api<?>, Boolean> zad;
    final Api.AbstractClientBuilder<? extends zae, SignInOptions> zae;
    int zaf;
    final zabe zag;
    final zabz zah;
    private final Lock zai;
    private final Condition zaj;
    private final Context zak;
    private final GoogleApiAvailabilityLight zal;
    private final zabh zam;
    @NotOnlyInitialized
    private volatile zabf zan;
    private ConnectionResult zao;
    
    public zabi(final Context zak, final zabe zag, final Lock zai, final Looper looper, final GoogleApiAvailabilityLight zal, final Map<Api.AnyClientKey<?>, Api.Client> zaa, final ClientSettings zac, final Map<Api<?>, Boolean> zad, final Api.AbstractClientBuilder<? extends zae, SignInOptions> zae, final ArrayList<zat> list, final zabz zah) {
        this.zab = new HashMap<Api.AnyClientKey<?>, ConnectionResult>();
        this.zao = null;
        this.zak = zak;
        this.zai = zai;
        this.zal = zal;
        this.zaa = zaa;
        this.zac = zac;
        this.zad = zad;
        this.zae = zae;
        this.zag = zag;
        this.zah = zah;
        for (int size = list.size(), i = 0; i < size; ++i) {
            ((zat)list.get(i)).zaa(this);
        }
        this.zam = new zabh(this, looper);
        this.zaj = zai.newCondition();
        this.zan = new zaax(this);
    }
    
    @Override
    public final void onConnected(final Bundle bundle) {
        this.zai.lock();
        try {
            this.zan.zag(bundle);
        }
        finally {
            this.zai.unlock();
        }
    }
    
    @Override
    public final void onConnectionSuspended(final int n) {
        this.zai.lock();
        try {
            this.zan.zai(n);
        }
        finally {
            this.zai.unlock();
        }
    }
    
    @Override
    public final void zaa(final ConnectionResult connectionResult, final Api<?> api, final boolean b) {
        this.zai.lock();
        try {
            this.zan.zah(connectionResult, api, b);
        }
        finally {
            this.zai.unlock();
        }
    }
    
    @Override
    public final ConnectionResult zab() {
        this.zaq();
        while (this.zan instanceof zaaw) {
            try {
                this.zaj.await();
                continue;
            }
            catch (final InterruptedException ex) {
                Thread.currentThread().interrupt();
                return new ConnectionResult(15, null);
            }
            break;
        }
        if (this.zan instanceof zaaj) {
            return ConnectionResult.RESULT_SUCCESS;
        }
        final ConnectionResult zao = this.zao;
        if (zao != null) {
            return zao;
        }
        return new ConnectionResult(13, null);
    }
    
    @Override
    public final ConnectionResult zac(long duration, final TimeUnit timeUnit) {
        this.zaq();
        duration = timeUnit.toNanos(duration);
    Label_0041_Outer:
        while (this.zan instanceof zaaw) {
            while (true) {
                if (duration <= 0L) {
                    try {
                        this.zar();
                        return new ConnectionResult(14, null);
                        duration = this.zaj.awaitNanos(duration);
                        continue Label_0041_Outer;
                    }
                    catch (final InterruptedException ex) {
                        Thread.currentThread().interrupt();
                        return new ConnectionResult(15, null);
                    }
                    break;
                }
                continue;
            }
        }
        if (this.zan instanceof zaaj) {
            return ConnectionResult.RESULT_SUCCESS;
        }
        final ConnectionResult zao = this.zao;
        if (zao != null) {
            return zao;
        }
        return new ConnectionResult(13, null);
    }
    
    @Override
    public final ConnectionResult zad(final Api<?> api) {
        final Api.AnyClientKey<?> zab = api.zab();
        if (this.zaa.containsKey(zab)) {
            if (this.zaa.get(zab).isConnected()) {
                return ConnectionResult.RESULT_SUCCESS;
            }
            if (this.zab.containsKey(zab)) {
                return this.zab.get(zab);
            }
        }
        return null;
    }
    
    @Override
    public final <A extends Api.AnyClient, R extends Result, T extends BaseImplementation.ApiMethodImpl<R, A>> T zae(final T t) {
        t.zak();
        this.zan.zaa(t);
        return t;
    }
    
    @Override
    public final <A extends Api.AnyClient, T extends BaseImplementation.ApiMethodImpl<? extends Result, A>> T zaf(final T t) {
        t.zak();
        return this.zan.zab(t);
    }
    
    public final void zai() {
        this.zai.lock();
        try {
            this.zag.zak();
            (this.zan = new zaaj(this)).zad();
            this.zaj.signalAll();
        }
        finally {
            this.zai.unlock();
        }
    }
    
    public final void zaj() {
        this.zai.lock();
        try {
            (this.zan = new zaaw(this, this.zac, this.zad, this.zal, this.zae, this.zai, this.zak)).zad();
            this.zaj.signalAll();
        }
        finally {
            this.zai.unlock();
        }
    }
    
    public final void zak(final ConnectionResult zao) {
        this.zai.lock();
        try {
            this.zao = zao;
            (this.zan = new zaax(this)).zad();
            this.zaj.signalAll();
        }
        finally {
            this.zai.unlock();
        }
    }
    
    public final void zal(final zabg zabg) {
        ((Handler)this.zam).sendMessage(((Handler)this.zam).obtainMessage(1, (Object)zabg));
    }
    
    public final void zam(final RuntimeException ex) {
        ((Handler)this.zam).sendMessage(((Handler)this.zam).obtainMessage(2, (Object)ex));
    }
    
    @Override
    public final void zaq() {
        this.zan.zae();
    }
    
    @Override
    public final void zar() {
        if (this.zan.zaj()) {
            this.zab.clear();
        }
    }
    
    @Override
    public final void zas(final String csq, final FileDescriptor fileDescriptor, final PrintWriter printWriter, final String[] array) {
        final String concat = String.valueOf(csq).concat("  ");
        printWriter.append(csq).append("mState=").println(this.zan);
        for (final Api api : this.zad.keySet()) {
            printWriter.append(csq).append(api.zad()).println(":");
            Preconditions.checkNotNull(this.zaa.get(api.zab())).dump(concat, fileDescriptor, printWriter, array);
        }
    }
    
    @Override
    public final void zat() {
        if (this.zan instanceof zaaj) {
            ((zaaj)this.zan).zaf();
        }
    }
    
    @Override
    public final void zau() {
    }
    
    @Override
    public final boolean zaw() {
        return this.zan instanceof zaaj;
    }
    
    @Override
    public final boolean zax() {
        return this.zan instanceof zaaw;
    }
    
    @Override
    public final boolean zay(final SignInConnectionListener signInConnectionListener) {
        return false;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.Api;
import java.util.ArrayList;

final class zaap extends zaav
{
    final zaaw zaa;
    private final ArrayList<Api.Client> zac;
    
    public zaap(final zaaw zaa, final ArrayList<Api.Client> zac) {
        super(this.zaa = zaa, null);
        this.zac = zac;
    }
    
    @Override
    public final void zaa() {
        final zaaw zaa = this.zaa;
        zaaw.zak(zaa).zag.zad = zaaw.zao(zaa);
        final ArrayList<Api.Client> zac = this.zac;
        for (int size = zac.size(), i = 0; i < size; ++i) {
            final Api.Client client = (Api.Client)zac.get(i);
            final zaaw zaa2 = this.zaa;
            client.getRemoteService(zaaw.zam(zaa2), zaaw.zak(zaa2).zag.zad);
        }
    }
}

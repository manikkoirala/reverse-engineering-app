// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.util.BiConsumer;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.api.Api;

@KeepForSdk
public abstract class TaskApiCall<A extends Api.AnyClient, ResultT>
{
    private final Feature[] zaa;
    private final boolean zab;
    private final int zac;
    
    @Deprecated
    @KeepForSdk
    public TaskApiCall() {
        this.zaa = null;
        this.zab = false;
        this.zac = 0;
    }
    
    @KeepForSdk
    public TaskApiCall(final Feature[] zaa, final boolean b, final int zac) {
        this.zaa = zaa;
        boolean zab = false;
        if (zaa != null) {
            zab = zab;
            if (b) {
                zab = true;
            }
        }
        this.zab = zab;
        this.zac = zac;
    }
    
    @KeepForSdk
    public static <A extends Api.AnyClient, ResultT> Builder<A, ResultT> builder() {
        return new Builder<A, ResultT>(null);
    }
    
    @KeepForSdk
    public abstract void doExecute(final A p0, final TaskCompletionSource<ResultT> p1);
    
    @KeepForSdk
    public boolean shouldAutoResolveMissingFeatures() {
        return this.zab;
    }
    
    public final int zaa() {
        return this.zac;
    }
    
    public final Feature[] zab() {
        return this.zaa;
    }
    
    @KeepForSdk
    public static class Builder<A extends Api.AnyClient, ResultT>
    {
        private RemoteCall<A, TaskCompletionSource<ResultT>> zaa;
        private boolean zab = true;
        private Feature[] zac;
        private int zad = 0;
        
        private Builder() {
            this.zab = true;
            this.zad = 0;
        }
        
        @KeepForSdk
        public TaskApiCall<A, ResultT> build() {
            Preconditions.checkArgument(this.zaa != null, (Object)"execute parameter required");
            return new zacv(this, this.zac, this.zab, this.zad);
        }
        
        @Deprecated
        @KeepForSdk
        public Builder<A, ResultT> execute(final BiConsumer<A, TaskCompletionSource<ResultT>> biConsumer) {
            this.zaa = new zacu(biConsumer);
            return this;
        }
        
        @KeepForSdk
        public Builder<A, ResultT> run(final RemoteCall<A, TaskCompletionSource<ResultT>> zaa) {
            this.zaa = zaa;
            return this;
        }
        
        @KeepForSdk
        public Builder<A, ResultT> setAutoResolveMissingFeatures(final boolean zab) {
            this.zab = zab;
            return this;
        }
        
        @KeepForSdk
        public Builder<A, ResultT> setFeatures(final Feature... zac) {
            this.zac = zac;
            return this;
        }
        
        @KeepForSdk
        public Builder<A, ResultT> setMethodKey(final int zad) {
            this.zad = zad;
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.TransformedResult;
import com.google.android.gms.common.api.ResultTransform;
import com.google.android.gms.common.api.ResultCallback;
import java.util.concurrent.TimeUnit;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;

@KeepForSdk
public abstract class PendingResultFacade<A extends Result, B extends Result> extends PendingResult<B>
{
    @Override
    public final void addStatusListener(final StatusListener statusListener) {
        throw null;
    }
    
    @Override
    public final B await() {
        throw null;
    }
    
    @Override
    public final B await(final long n, final TimeUnit timeUnit) {
        throw null;
    }
    
    @Override
    public final void cancel() {
        throw null;
    }
    
    @Override
    public final boolean isCanceled() {
        throw null;
    }
    
    @Override
    public final void setResultCallback(final ResultCallback<? super B> resultCallback) {
        throw null;
    }
    
    @Override
    public final void setResultCallback(final ResultCallback<? super B> resultCallback, final long n, final TimeUnit timeUnit) {
        throw null;
    }
    
    @Override
    public final <S extends Result> TransformedResult<S> then(final ResultTransform<? super B, ? extends S> resultTransform) {
        throw null;
    }
}

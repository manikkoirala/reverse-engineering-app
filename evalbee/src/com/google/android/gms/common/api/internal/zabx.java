// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import android.net.Uri;
import android.content.Intent;
import android.content.Context;
import android.content.BroadcastReceiver;

public final class zabx extends BroadcastReceiver
{
    Context zaa;
    private final zabw zab;
    
    public zabx(final zabw zab) {
        this.zab = zab;
    }
    
    public final void onReceive(final Context context, final Intent intent) {
        final Uri data = intent.getData();
        String schemeSpecificPart;
        if (data != null) {
            schemeSpecificPart = data.getSchemeSpecificPart();
        }
        else {
            schemeSpecificPart = null;
        }
        if ("com.google.android.gms".equals(schemeSpecificPart)) {
            this.zab.zaa();
            this.zab();
        }
    }
    
    public final void zaa(final Context zaa) {
        this.zaa = zaa;
    }
    
    public final void zab() {
        synchronized (this) {
            final Context zaa = this.zaa;
            if (zaa != null) {
                zaa.unregisterReceiver((BroadcastReceiver)this);
            }
            this.zaa = null;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api;

import java.util.Iterator;
import android.text.TextUtils;
import java.util.ArrayList;
import com.google.android.gms.common.api.internal.ApiKey;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.ConnectionResult;

public class AvailabilityException extends Exception
{
    private final r8 zaa;
    
    public AvailabilityException(final r8 zaa) {
        this.zaa = zaa;
    }
    
    public ConnectionResult getConnectionResult(final GoogleApi<? extends Api.ApiOptions> googleApi) {
        final ApiKey<? extends Api.ApiOptions> apiKey = googleApi.getApiKey();
        final boolean b = this.zaa.get(apiKey) != null;
        final String zab = apiKey.zab();
        final StringBuilder sb = new StringBuilder(String.valueOf(zab).length() + 58);
        sb.append("The given API (");
        sb.append(zab);
        sb.append(") was not part of the availability request.");
        Preconditions.checkArgument(b, (Object)sb.toString());
        return Preconditions.checkNotNull(this.zaa.get(apiKey));
    }
    
    public ConnectionResult getConnectionResult(final HasApiKey<? extends Api.ApiOptions> hasApiKey) {
        final ApiKey<? extends Api.ApiOptions> apiKey = hasApiKey.getApiKey();
        final boolean b = this.zaa.get(apiKey) != null;
        final String zab = apiKey.zab();
        final StringBuilder sb = new StringBuilder(String.valueOf(zab).length() + 58);
        sb.append("The given API (");
        sb.append(zab);
        sb.append(") was not part of the availability request.");
        Preconditions.checkArgument(b, (Object)sb.toString());
        return Preconditions.checkNotNull(this.zaa.get(apiKey));
    }
    
    @Override
    public String getMessage() {
        final ArrayList list = new ArrayList();
        final Iterator iterator = this.zaa.keySet().iterator();
        boolean b = true;
        while (iterator.hasNext()) {
            final ApiKey apiKey = (ApiKey)iterator.next();
            final ConnectionResult obj = Preconditions.checkNotNull(this.zaa.get(apiKey));
            b &= (obj.isSuccess() ^ true);
            final String zab = apiKey.zab();
            final String value = String.valueOf(obj);
            final StringBuilder sb = new StringBuilder(String.valueOf(zab).length() + 2 + value.length());
            sb.append(zab);
            sb.append(": ");
            sb.append(value);
            list.add(sb.toString());
        }
        final StringBuilder sb2 = new StringBuilder();
        String str;
        if (b) {
            str = "None of the queried APIs are available. ";
        }
        else {
            str = "Some of the queried APIs are unavailable. ";
        }
        sb2.append(str);
        sb2.append(TextUtils.join((CharSequence)"; ", (Iterable)list));
        return sb2.toString();
    }
}

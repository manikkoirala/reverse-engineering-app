// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import android.os.Bundle;

final class zza implements Runnable
{
    final LifecycleCallback zza;
    final String zzb;
    final zzb zzc;
    
    public zza(final zzb zzc, final LifecycleCallback zza, final String zzb) {
        this.zzc = zzc;
        this.zza = zza;
        this.zzb = zzb;
    }
    
    @Override
    public final void run() {
        final zzb zzc = this.zzc;
        if (com.google.android.gms.common.api.internal.zzb.zza(zzc) > 0) {
            final LifecycleCallback zza = this.zza;
            Bundle bundle;
            if (com.google.android.gms.common.api.internal.zzb.zzb(zzc) != null) {
                bundle = com.google.android.gms.common.api.internal.zzb.zzb(zzc).getBundle(this.zzb);
            }
            else {
                bundle = null;
            }
            zza.onCreate(bundle);
        }
        if (com.google.android.gms.common.api.internal.zzb.zza(this.zzc) >= 2) {
            this.zza.onStart();
        }
        if (com.google.android.gms.common.api.internal.zzb.zza(this.zzc) >= 3) {
            this.zza.onResume();
        }
        if (com.google.android.gms.common.api.internal.zzb.zza(this.zzc) >= 4) {
            this.zza.onStop();
        }
        if (com.google.android.gms.common.api.internal.zzb.zza(this.zzc) >= 5) {
            this.zza.onDestroy();
        }
    }
}

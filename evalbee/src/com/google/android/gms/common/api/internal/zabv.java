// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import android.os.Looper;
import android.content.Context;
import com.google.android.gms.common.api.Result;
import org.checkerframework.checker.initialization.qual.NotOnlyInitialized;
import com.google.android.gms.common.api.GoogleApi;
import com.google.android.gms.common.api.Api;

public final class zabv<O extends Api.ApiOptions> extends zaag
{
    @NotOnlyInitialized
    private final GoogleApi<O> zaa;
    
    public zabv(final GoogleApi<O> zaa) {
        super("Method is not supported by connectionless client. APIs supporting connectionless client must not call this method.");
        this.zaa = zaa;
    }
    
    @Override
    public final <A extends Api.AnyClient, R extends Result, T extends BaseImplementation.ApiMethodImpl<R, A>> T enqueue(final T t) {
        return this.zaa.doRead(t);
    }
    
    @Override
    public final <A extends Api.AnyClient, T extends BaseImplementation.ApiMethodImpl<? extends Result, A>> T execute(final T t) {
        return this.zaa.doWrite(t);
    }
    
    @Override
    public final Context getContext() {
        return this.zaa.getApplicationContext();
    }
    
    @Override
    public final Looper getLooper() {
        return this.zaa.getLooper();
    }
    
    @Override
    public final void zao(final zada zada) {
    }
    
    @Override
    public final void zap(final zada zada) {
    }
}

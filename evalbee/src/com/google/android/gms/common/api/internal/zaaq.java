// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.signin.internal.zak;

final class zaaq extends zabg
{
    final zaaw zaa;
    final zak zab;
    
    public zaaq(final zaar zaar, final zabf zabf, final zaaw zaa, final zak zab) {
        this.zaa = zaa;
        this.zab = zab;
        super(zabf);
    }
    
    @Override
    public final void zaa() {
        zaaw.zar(this.zaa, this.zab);
    }
}

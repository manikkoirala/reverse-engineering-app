// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.signin.zae;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.ClientSettings;
import android.os.Bundle;
import com.google.android.gms.common.api.GoogleApiClient;

final class zaat implements ConnectionCallbacks, OnConnectionFailedListener
{
    final zaaw zaa;
    
    @Override
    public final void onConnected(final Bundle bundle) {
        final ClientSettings clientSettings = Preconditions.checkNotNull(zaaw.zal(this.zaa));
        Preconditions.checkNotNull(zaaw.zan(this.zaa)).zad((com.google.android.gms.signin.internal.zae)new zaar(this.zaa));
    }
    
    @Override
    public final void onConnectionFailed(final ConnectionResult connectionResult) {
        zaaw.zap(this.zaa).lock();
        try {
            if (zaaw.zay(this.zaa, connectionResult)) {
                zaaw.zaq(this.zaa);
                zaaw.zau(this.zaa);
            }
            else {
                zaaw.zas(this.zaa, connectionResult);
            }
        }
        finally {
            zaaw.zap(this.zaa).unlock();
        }
    }
    
    @Override
    public final void onConnectionSuspended(final int n) {
    }
}

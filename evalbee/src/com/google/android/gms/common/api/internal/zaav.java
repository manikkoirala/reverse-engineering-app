// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

abstract class zaav implements Runnable
{
    final zaaw zab;
    
    @Override
    public final void run() {
        zaaw.zap(this.zab).lock();
        while (true) {
            try {
                try {
                    if (!Thread.interrupted()) {
                        this.zaa();
                    }
                    zaaw.zap(this.zab).unlock();
                    return;
                }
                finally {}
            }
            catch (final RuntimeException ex) {
                zaaw.zak(this.zab).zam(ex);
                continue;
            }
            break;
        }
        zaaw.zap(this.zab).unlock();
    }
    
    public abstract void zaa();
}

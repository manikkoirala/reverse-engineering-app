// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import android.app.PendingIntent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.BaseGmsClient;

final class zaan extends zabg
{
    final BaseGmsClient.ConnectionProgressReportCallbacks zaa;
    
    public zaan(final zaao zaao, final zabf zabf, final BaseGmsClient.ConnectionProgressReportCallbacks zaa) {
        this.zaa = zaa;
        super(zabf);
    }
    
    @Override
    public final void zaa() {
        this.zaa.onReportServiceBinding(new ConnectionResult(16, null));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api;

public class Response<T extends Result>
{
    private Result zza;
    
    public Response() {
    }
    
    public Response(final T zza) {
        this.zza = zza;
    }
    
    public T getResult() {
        return (T)this.zza;
    }
    
    public void setResult(final T zza) {
        this.zza = zza;
    }
}

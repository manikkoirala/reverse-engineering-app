// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import android.content.Intent;
import com.google.android.gms.common.internal.Preconditions;
import java.io.PrintWriter;
import java.io.FileDescriptor;
import android.content.ContextWrapper;
import android.app.Activity;
import androidx.annotation.Keep;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public class LifecycleCallback
{
    @KeepForSdk
    protected final LifecycleFragment mLifecycleFragment;
    
    @KeepForSdk
    public LifecycleCallback(final LifecycleFragment mLifecycleFragment) {
        this.mLifecycleFragment = mLifecycleFragment;
    }
    
    @Keep
    private static LifecycleFragment getChimeraLifecycleFragmentImpl(final LifecycleActivity lifecycleActivity) {
        throw new IllegalStateException("Method not available in SDK.");
    }
    
    @KeepForSdk
    public static LifecycleFragment getFragment(final Activity activity) {
        return getFragment(new LifecycleActivity(activity));
    }
    
    @KeepForSdk
    public static LifecycleFragment getFragment(final ContextWrapper contextWrapper) {
        throw new UnsupportedOperationException();
    }
    
    @KeepForSdk
    public static LifecycleFragment getFragment(final LifecycleActivity lifecycleActivity) {
        if (lifecycleActivity.zzd()) {
            return zzd.zzc(lifecycleActivity.zzb());
        }
        if (lifecycleActivity.zzc()) {
            return zzb.zzc(lifecycleActivity.zza());
        }
        throw new IllegalArgumentException("Can't get fragment for unexpected activity.");
    }
    
    @KeepForSdk
    public void dump(final String s, final FileDescriptor fileDescriptor, final PrintWriter printWriter, final String[] array) {
    }
    
    @KeepForSdk
    public Activity getActivity() {
        final Activity lifecycleActivity = this.mLifecycleFragment.getLifecycleActivity();
        Preconditions.checkNotNull(lifecycleActivity);
        return lifecycleActivity;
    }
    
    @KeepForSdk
    public void onActivityResult(final int n, final int n2, final Intent intent) {
    }
    
    @KeepForSdk
    public void onCreate(final Bundle bundle) {
    }
    
    @KeepForSdk
    public void onDestroy() {
    }
    
    @KeepForSdk
    public void onResume() {
    }
    
    @KeepForSdk
    public void onSaveInstanceState(final Bundle bundle) {
    }
    
    @KeepForSdk
    public void onStart() {
    }
    
    @KeepForSdk
    public void onStop() {
    }
}

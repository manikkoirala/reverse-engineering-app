// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

final class zaak implements Runnable
{
    final zaaw zaa;
    
    public zaak(final zaaw zaa) {
        this.zaa = zaa;
    }
    
    @Override
    public final void run() {
        final zaaw zaa = this.zaa;
        zaaw.zaf(zaa).cancelAvailabilityErrorNotifications(zaaw.zac(zaa));
    }
}

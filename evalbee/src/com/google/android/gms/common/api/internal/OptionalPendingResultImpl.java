// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.TransformedResult;
import com.google.android.gms.common.api.ResultTransform;
import com.google.android.gms.common.api.ResultCallback;
import java.util.concurrent.TimeUnit;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.Result;

@KeepForSdk
public final class OptionalPendingResultImpl<R extends Result> extends OptionalPendingResult<R>
{
    private final BasePendingResult<R> zaa;
    
    public OptionalPendingResultImpl(final PendingResult<R> pendingResult) {
        this.zaa = (BasePendingResult<R>)pendingResult;
    }
    
    @Override
    public final void addStatusListener(final StatusListener statusListener) {
        this.zaa.addStatusListener(statusListener);
    }
    
    @Override
    public final R await() {
        return this.zaa.await();
    }
    
    @Override
    public final R await(final long n, final TimeUnit timeUnit) {
        return this.zaa.await(n, timeUnit);
    }
    
    @Override
    public final void cancel() {
        this.zaa.cancel();
    }
    
    @Override
    public final R get() {
        if (this.zaa.isReady()) {
            return this.zaa.await(0L, TimeUnit.MILLISECONDS);
        }
        throw new IllegalStateException("Result is not available. Check that isDone() returns true before calling get().");
    }
    
    @Override
    public final boolean isCanceled() {
        return this.zaa.isCanceled();
    }
    
    @Override
    public final boolean isDone() {
        return this.zaa.isReady();
    }
    
    @Override
    public final void setResultCallback(final ResultCallback<? super R> resultCallback) {
        this.zaa.setResultCallback(resultCallback);
    }
    
    @Override
    public final void setResultCallback(final ResultCallback<? super R> resultCallback, final long n, final TimeUnit timeUnit) {
        this.zaa.setResultCallback(resultCallback, n, timeUnit);
    }
    
    @Override
    public final <S extends Result> TransformedResult<S> then(final ResultTransform<? super R, ? extends S> resultTransform) {
        return this.zaa.then((ResultTransform<? super Result, ? extends S>)resultTransform);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import android.os.DeadObjectException;
import android.app.PendingIntent;
import com.google.android.gms.common.api.Status;
import android.os.RemoteException;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public class BaseImplementation
{
    @KeepForSdk
    public abstract static class ApiMethodImpl<R extends Result, A extends Api.AnyClient> extends BasePendingResult<R> implements ResultHolder<R>
    {
        @KeepForSdk
        private final Api<?> mApi;
        @KeepForSdk
        private final Api.AnyClientKey<A> mClientKey;
        
        @Deprecated
        @KeepForSdk
        public ApiMethodImpl(final Api.AnyClientKey<A> anyClientKey, final GoogleApiClient googleApiClient) {
            super(Preconditions.checkNotNull(googleApiClient, "GoogleApiClient must not be null"));
            this.mClientKey = (Api.AnyClientKey<A>)Preconditions.checkNotNull((Api.AnyClientKey)anyClientKey);
            this.mApi = null;
        }
        
        @KeepForSdk
        public ApiMethodImpl(final Api<?> mApi, final GoogleApiClient googleApiClient) {
            super(Preconditions.checkNotNull(googleApiClient, "GoogleApiClient must not be null"));
            Preconditions.checkNotNull(mApi, "Api must not be null");
            this.mClientKey = (Api.AnyClientKey<A>)mApi.zab();
            this.mApi = mApi;
        }
        
        @KeepForSdk
        public ApiMethodImpl(final CallbackHandler<R> callbackHandler) {
            super((CallbackHandler)callbackHandler);
            this.mClientKey = (Api.AnyClientKey<A>)new Api.AnyClientKey();
            this.mApi = null;
        }
        
        @KeepForSdk
        private void setFailedResult(final RemoteException ex) {
            this.setFailedResult(new Status(8, ((Throwable)ex).getLocalizedMessage(), null));
        }
        
        @KeepForSdk
        public abstract void doExecute(final A p0);
        
        @KeepForSdk
        public final Api<?> getApi() {
            return this.mApi;
        }
        
        @KeepForSdk
        public final Api.AnyClientKey<A> getClientKey() {
            return this.mClientKey;
        }
        
        @KeepForSdk
        public void onSetFailedResult(final R r) {
        }
        
        @KeepForSdk
        public final void run(final A a) {
            try {
                this.doExecute(a);
            }
            catch (final RemoteException failedResult) {
                this.setFailedResult(failedResult);
            }
            catch (final DeadObjectException failedResult2) {
                this.setFailedResult((RemoteException)failedResult2);
                throw failedResult2;
            }
        }
        
        @KeepForSdk
        @Override
        public final void setFailedResult(final Status status) {
            Preconditions.checkArgument(status.isSuccess() ^ true, (Object)"Failed result must not be success");
            final Result failedResult = this.createFailedResult(status);
            this.setResult((R)failedResult);
            this.onSetFailedResult((R)failedResult);
        }
    }
    
    @KeepForSdk
    public interface ResultHolder<R>
    {
        @KeepForSdk
        void setFailedResult(final Status p0);
        
        @KeepForSdk
        void setResult(final R p0);
    }
}

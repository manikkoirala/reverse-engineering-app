// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import java.util.Iterator;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ApiException;
import java.util.HashMap;
import com.google.android.gms.common.api.Status;
import java.util.Collections;
import java.util.WeakHashMap;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.Map;

public final class zaad
{
    private final Map<BasePendingResult<?>, Boolean> zaa;
    private final Map<TaskCompletionSource<?>, Boolean> zab;
    
    public zaad() {
        this.zaa = Collections.synchronizedMap(new WeakHashMap<BasePendingResult<?>, Boolean>());
        this.zab = Collections.synchronizedMap(new WeakHashMap<TaskCompletionSource<?>, Boolean>());
    }
    
    private final void zah(final boolean b, final Status status) {
        Object o = this.zaa;
        synchronized (o) {
            final HashMap hashMap = new HashMap(this.zaa);
            monitorexit(o);
            Object o2 = this.zab;
            synchronized (o2) {
                o = new HashMap<Object, Object>(this.zab);
                monitorexit(o2);
                o2 = hashMap.entrySet().iterator();
                while (((Iterator)o2).hasNext()) {
                    final Map.Entry<K, Boolean> entry = (Map.Entry<K, Boolean>)((Iterator)o2).next();
                    if (b || entry.getValue()) {
                        ((BasePendingResult)entry.getKey()).forceFailureUnlessReady(status);
                    }
                }
                o = ((Map<Object, Object>)o).entrySet().iterator();
                while (((Iterator)o).hasNext()) {
                    final Map.Entry<K, Boolean> entry2 = (Map.Entry<K, Boolean>)((Iterator)o).next();
                    if (b || entry2.getValue()) {
                        ((TaskCompletionSource)entry2.getKey()).trySetException((Exception)new ApiException(status));
                    }
                }
            }
        }
    }
    
    public final void zac(final BasePendingResult<? extends Result> basePendingResult, final boolean b) {
        this.zaa.put(basePendingResult, b);
        basePendingResult.addStatusListener((PendingResult.StatusListener)new zaab(this, basePendingResult));
    }
    
    public final <TResult> void zad(final TaskCompletionSource<TResult> taskCompletionSource, final boolean b) {
        this.zab.put(taskCompletionSource, b);
        taskCompletionSource.getTask().addOnCompleteListener((OnCompleteListener)new zaac(this, taskCompletionSource));
    }
    
    public final void zae(final int n, final String str) {
        final StringBuilder sb = new StringBuilder("The connection to Google Play services was lost");
        Label_0040: {
            String str2;
            if (n == 1) {
                str2 = " due to service disconnection.";
            }
            else {
                if (n != 3) {
                    break Label_0040;
                }
                str2 = " due to dead object exception.";
            }
            sb.append(str2);
        }
        if (str != null) {
            sb.append(" Last reason for disconnect: ");
            sb.append(str);
        }
        this.zah(true, new Status(20, sb.toString()));
    }
    
    public final void zaf() {
        this.zah(false, GoogleApiManager.zaa);
    }
    
    public final boolean zag() {
        return !this.zaa.isEmpty() || !this.zab.isEmpty();
    }
}

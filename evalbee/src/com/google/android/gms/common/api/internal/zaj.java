// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

final class zaj implements OnConnectionFailedListener
{
    public final int zaa;
    public final GoogleApiClient zab;
    public final OnConnectionFailedListener zac;
    final zak zad;
    
    public zaj(final zak zad, final int zaa, final GoogleApiClient zab, final OnConnectionFailedListener zac) {
        this.zad = zad;
        this.zaa = zaa;
        this.zab = zab;
        this.zac = zac;
    }
    
    @Override
    public final void onConnectionFailed(final ConnectionResult obj) {
        Log.d("AutoManageHelper", "beginFailureResolution for ".concat(String.valueOf(obj)));
        this.zad.zah(obj, this.zaa);
    }
}

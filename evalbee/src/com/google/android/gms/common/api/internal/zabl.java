// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

final class zabl implements BackgroundStateChangeListener
{
    final GoogleApiManager zaa;
    
    public zabl(final GoogleApiManager zaa) {
        this.zaa = zaa;
    }
    
    @Override
    public final void onBackgroundStateChanged(final boolean b) {
        final GoogleApiManager zaa = this.zaa;
        GoogleApiManager.zaf(zaa).sendMessage(GoogleApiManager.zaf(zaa).obtainMessage(1, (Object)b));
    }
}

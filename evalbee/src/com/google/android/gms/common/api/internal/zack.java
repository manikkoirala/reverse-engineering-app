// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.Feature;

final class zack extends RegisterListenerMethod
{
    final RegistrationMethods.Builder zaa;
    
    public zack(final RegistrationMethods.Builder zaa, final ListenerHolder listenerHolder, final Feature[] array, final boolean b, final int n) {
        this.zaa = zaa;
        super(listenerHolder, array, b, n);
    }
    
    @Override
    public final void registerListener(final Api.AnyClient anyClient, final TaskCompletionSource<Void> taskCompletionSource) {
        RegistrationMethods.Builder.zaa((RegistrationMethods.Builder<Api.AnyClient, Object>)this.zaa).accept(anyClient, taskCompletionSource);
    }
}

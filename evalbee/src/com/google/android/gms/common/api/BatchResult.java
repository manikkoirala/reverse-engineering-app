// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api;

import java.util.concurrent.TimeUnit;
import com.google.android.gms.common.internal.Preconditions;

public final class BatchResult implements Result
{
    private final Status zaa;
    private final PendingResult<?>[] zab;
    
    public BatchResult(final Status zaa, final PendingResult<?>[] zab) {
        this.zaa = zaa;
        this.zab = zab;
    }
    
    @Override
    public Status getStatus() {
        return this.zaa;
    }
    
    public <R extends Result> R take(final BatchResultToken<R> batchResultToken) {
        Preconditions.checkArgument(batchResultToken.mId < this.zab.length, (Object)"The result token does not belong to this batch");
        return (R)this.zab[batchResultToken.mId].await(0L, TimeUnit.MILLISECONDS);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Scope;
import java.util.Set;
import com.google.android.gms.common.internal.IAccountAccessor;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.internal.BaseGmsClient;

final class zabu implements ConnectionProgressReportCallbacks, zacs
{
    final GoogleApiManager zaa;
    private final Api.Client zab;
    private final ApiKey<?> zac;
    private IAccountAccessor zad;
    private Set<Scope> zae;
    private boolean zaf;
    
    public zabu(final GoogleApiManager zaa, final Api.Client zab, final ApiKey<?> zac) {
        this.zaa = zaa;
        this.zad = null;
        this.zae = null;
        this.zaf = false;
        this.zab = zab;
        this.zac = zac;
    }
    
    private final void zag() {
        if (this.zaf) {
            final IAccountAccessor zad = this.zad;
            if (zad != null) {
                this.zab.getRemoteService(zad, this.zae);
            }
        }
    }
    
    @Override
    public final void onReportServiceBinding(final ConnectionResult connectionResult) {
        GoogleApiManager.zaf(this.zaa).post((Runnable)new zabt(this, connectionResult));
    }
    
    @Override
    public final void zae(final ConnectionResult connectionResult) {
        final zabq zabq = GoogleApiManager.zat(this.zaa).get(this.zac);
        if (zabq != null) {
            zabq.zas(connectionResult);
        }
    }
    
    @Override
    public final void zaf(final IAccountAccessor zad, final Set<Scope> zae) {
        if (zad != null && zae != null) {
            this.zad = zad;
            this.zae = zae;
            this.zag();
            return;
        }
        Log.wtf("GoogleApiManager", "Received null response from onSignInSuccess", (Throwable)new Exception());
        this.zae(new ConnectionResult(4));
    }
}

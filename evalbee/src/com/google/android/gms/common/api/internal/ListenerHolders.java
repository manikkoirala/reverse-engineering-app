// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import java.util.Iterator;
import java.util.concurrent.Executor;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Looper;
import java.util.Map;
import java.util.Collections;
import java.util.WeakHashMap;
import java.util.Set;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public class ListenerHolders
{
    private final Set<ListenerHolder<?>> zaa;
    
    public ListenerHolders() {
        this.zaa = Collections.newSetFromMap(new WeakHashMap<ListenerHolder<?>, Boolean>());
    }
    
    @KeepForSdk
    public static <L> ListenerHolder<L> createListenerHolder(final L l, final Looper looper, final String s) {
        Preconditions.checkNotNull(l, "Listener must not be null");
        Preconditions.checkNotNull(looper, "Looper must not be null");
        Preconditions.checkNotNull(s, "Listener type must not be null");
        return new ListenerHolder<L>(looper, l, s);
    }
    
    @KeepForSdk
    public static <L> ListenerHolder<L> createListenerHolder(final L l, final Executor executor, final String s) {
        Preconditions.checkNotNull(l, "Listener must not be null");
        Preconditions.checkNotNull(executor, "Executor must not be null");
        Preconditions.checkNotNull(s, "Listener type must not be null");
        return new ListenerHolder<L>(executor, l, s);
    }
    
    @KeepForSdk
    public static <L> ListenerHolder.ListenerKey<L> createListenerKey(final L l, final String s) {
        Preconditions.checkNotNull(l, "Listener must not be null");
        Preconditions.checkNotNull(s, "Listener type must not be null");
        Preconditions.checkNotEmpty(s, "Listener type must not be empty");
        return (ListenerHolder.ListenerKey<L>)new ListenerHolder.ListenerKey(l, s);
    }
    
    public final <L> ListenerHolder<L> zaa(final L l, final Looper looper, final String s) {
        final ListenerHolder<Object> listenerHolder = createListenerHolder((Object)l, looper, "NO_TYPE");
        this.zaa.add(listenerHolder);
        return (ListenerHolder<L>)listenerHolder;
    }
    
    public final void zab() {
        final Iterator<ListenerHolder<?>> iterator = this.zaa.iterator();
        while (iterator.hasNext()) {
            iterator.next().clear();
        }
        this.zaa.clear();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api;

import com.google.android.gms.common.annotation.KeepForSdk;
import android.util.Log;

public abstract class ResultCallbacks<R extends Result> implements ResultCallback<R>
{
    public abstract void onFailure(final Status p0);
    
    @KeepForSdk
    @Override
    public final void onResult(final R obj) {
        final Status status = obj.getStatus();
        if (status.isSuccess()) {
            this.onSuccess(obj);
            return;
        }
        this.onFailure(status);
        if (obj instanceof Releasable) {
            try {
                ((Releasable)obj).release();
            }
            catch (final RuntimeException ex) {
                Log.w("ResultCallbacks", "Unable to release ".concat(String.valueOf(obj)), (Throwable)ex);
            }
        }
    }
    
    public abstract void onSuccess(final R p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.Feature;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.api.Api;

@KeepForSdk
public class RegistrationMethods<A extends Api.AnyClient, L>
{
    @KeepForSdk
    public final RegisterListenerMethod<A, L> register = register;
    public final UnregisterListenerMethod<A, L> zaa = zaa;
    public final Runnable zab = zab;
    
    @KeepForSdk
    public static <A extends Api.AnyClient, L> Builder<A, L> builder() {
        return new Builder<A, L>(null);
    }
    
    @KeepForSdk
    public static class Builder<A extends Api.AnyClient, L>
    {
        private RemoteCall<A, TaskCompletionSource<Void>> zaa;
        private RemoteCall<A, TaskCompletionSource<Boolean>> zab;
        private Runnable zac = zacj.zaa;
        private ListenerHolder<L> zad;
        private Feature[] zae;
        private boolean zaf = true;
        private int zag;
        
        private Builder() {
            this.zac = zacj.zaa;
            this.zaf = true;
        }
        
        @KeepForSdk
        public RegistrationMethods<A, L> build() {
            final RemoteCall<A, TaskCompletionSource<Void>> zaa = this.zaa;
            final boolean b = true;
            Preconditions.checkArgument(zaa != null, (Object)"Must set register function");
            Preconditions.checkArgument(this.zab != null, (Object)"Must set unregister function");
            Preconditions.checkArgument(this.zad != null && b, (Object)"Must set holder");
            return new RegistrationMethods<A, L>(new zack(this, this.zad, this.zae, this.zaf, this.zag), new zacl(this, (ListenerHolder.ListenerKey)Preconditions.checkNotNull(this.zad.getListenerKey(), "Key must not be null")), this.zac, null);
        }
        
        @KeepForSdk
        public Builder<A, L> onConnectionSuspended(final Runnable zac) {
            this.zac = zac;
            return this;
        }
        
        @KeepForSdk
        public Builder<A, L> register(final RemoteCall<A, TaskCompletionSource<Void>> zaa) {
            this.zaa = zaa;
            return this;
        }
        
        @KeepForSdk
        public Builder<A, L> setAutoResolveMissingFeatures(final boolean zaf) {
            this.zaf = zaf;
            return this;
        }
        
        @KeepForSdk
        public Builder<A, L> setFeatures(final Feature... zae) {
            this.zae = zae;
            return this;
        }
        
        @KeepForSdk
        public Builder<A, L> setMethodKey(final int zag) {
            this.zag = zag;
            return this;
        }
        
        @KeepForSdk
        public Builder<A, L> unregister(final RemoteCall<A, TaskCompletionSource<Boolean>> zab) {
            this.zab = zab;
            return this;
        }
        
        @KeepForSdk
        public Builder<A, L> withHolder(final ListenerHolder<L> zad) {
            this.zad = zad;
            return this;
        }
    }
}

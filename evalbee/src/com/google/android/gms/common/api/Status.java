// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import android.content.Intent;
import com.google.android.gms.common.internal.Preconditions;
import android.app.Activity;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.errorprone.annotations.ResultIgnorabilityUnspecified;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.ConnectionResult;
import android.app.PendingIntent;
import com.google.android.gms.common.internal.ShowFirstParty;
import com.google.android.gms.common.annotation.KeepForSdk;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "StatusCreator")
public final class Status extends AbstractSafeParcelable implements Result, ReflectedParcelable
{
    public static final Parcelable$Creator<Status> CREATOR;
    @KeepForSdk
    @ShowFirstParty
    public static final Status RESULT_CANCELED;
    @KeepForSdk
    public static final Status RESULT_DEAD_CLIENT;
    @KeepForSdk
    @ShowFirstParty
    public static final Status RESULT_INTERNAL_ERROR;
    @KeepForSdk
    @ShowFirstParty
    public static final Status RESULT_INTERRUPTED;
    @KeepForSdk
    @ShowFirstParty
    public static final Status RESULT_SUCCESS;
    @KeepForSdk
    @ShowFirstParty
    public static final Status RESULT_SUCCESS_CACHE;
    @KeepForSdk
    @ShowFirstParty
    public static final Status RESULT_TIMEOUT;
    @ShowFirstParty
    public static final Status zza;
    @VersionField(id = 1000)
    final int zzb;
    @Field(getter = "getStatusCode", id = 1)
    private final int zzc;
    @Field(getter = "getStatusMessage", id = 2)
    private final String zzd;
    @Field(getter = "getPendingIntent", id = 3)
    private final PendingIntent zze;
    @Field(getter = "getConnectionResult", id = 4)
    private final ConnectionResult zzf;
    
    static {
        RESULT_SUCCESS_CACHE = new Status(-1);
        RESULT_SUCCESS = new Status(0);
        RESULT_INTERRUPTED = new Status(14);
        RESULT_INTERNAL_ERROR = new Status(8);
        RESULT_TIMEOUT = new Status(15);
        RESULT_CANCELED = new Status(16);
        zza = new Status(17);
        RESULT_DEAD_CLIENT = new Status(18);
        CREATOR = (Parcelable$Creator)new zzb();
    }
    
    public Status(final int n) {
        this(n, null);
    }
    
    @Constructor
    public Status(@Param(id = 1000) final int zzb, @Param(id = 1) final int zzc, @Param(id = 2) final String zzd, @Param(id = 3) final PendingIntent zze, @Param(id = 4) final ConnectionResult zzf) {
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
    }
    
    public Status(final int n, final String s) {
        this(1, n, s, null, null);
    }
    
    public Status(final int n, final String s, final PendingIntent pendingIntent) {
        this(1, n, s, pendingIntent, null);
    }
    
    public Status(final ConnectionResult connectionResult, final String s) {
        this(connectionResult, s, 17);
    }
    
    @Deprecated
    @KeepForSdk
    public Status(final ConnectionResult connectionResult, final String s, final int n) {
        this(1, n, s, connectionResult.getResolution(), connectionResult);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof Status)) {
            return false;
        }
        final Status status = (Status)o;
        return this.zzb == status.zzb && this.zzc == status.zzc && Objects.equal(this.zzd, status.zzd) && Objects.equal(this.zze, status.zze) && Objects.equal(this.zzf, status.zzf);
    }
    
    public ConnectionResult getConnectionResult() {
        return this.zzf;
    }
    
    public PendingIntent getResolution() {
        return this.zze;
    }
    
    @Override
    public Status getStatus() {
        return this;
    }
    
    @ResultIgnorabilityUnspecified
    public int getStatusCode() {
        return this.zzc;
    }
    
    public String getStatusMessage() {
        return this.zzd;
    }
    
    @VisibleForTesting
    public boolean hasResolution() {
        return this.zze != null;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zzb, this.zzc, this.zzd, this.zze, this.zzf);
    }
    
    public boolean isCanceled() {
        return this.zzc == 16;
    }
    
    public boolean isInterrupted() {
        return this.zzc == 14;
    }
    
    public boolean isSuccess() {
        return this.zzc <= 0;
    }
    
    public void startResolutionForResult(final Activity activity, final int n) {
        if (!this.hasResolution()) {
            return;
        }
        final PendingIntent zze = this.zze;
        Preconditions.checkNotNull(zze);
        activity.startIntentSenderForResult(zze.getIntentSender(), n, (Intent)null, 0, 0, 0);
    }
    
    @Override
    public String toString() {
        final Objects.ToStringHelper stringHelper = Objects.toStringHelper(this);
        stringHelper.add("statusCode", this.zza());
        stringHelper.add("resolution", this.zze);
        return stringHelper.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.getStatusCode());
        SafeParcelWriter.writeString(parcel, 2, this.getStatusMessage(), false);
        SafeParcelWriter.writeParcelable(parcel, 3, (Parcelable)this.zze, n, false);
        SafeParcelWriter.writeParcelable(parcel, 4, (Parcelable)this.getConnectionResult(), n, false);
        SafeParcelWriter.writeInt(parcel, 1000, this.zzb);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public final String zza() {
        final String zzd = this.zzd;
        if (zzd != null) {
            return zzd;
        }
        return CommonStatusCodes.getStatusCodeString(this.zzc);
    }
}

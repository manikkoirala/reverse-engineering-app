// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import android.util.Log;
import android.os.Message;
import android.os.Looper;
import com.google.android.gms.internal.base.zaq;

final class zabh extends zaq
{
    final zabi zaa;
    
    public zabh(final zabi zaa, final Looper looper) {
        this.zaa = zaa;
        super(looper);
    }
    
    public final void handleMessage(final Message message) {
        final int what = message.what;
        if (what == 1) {
            ((zabg)message.obj).zab(this.zaa);
            return;
        }
        if (what != 2) {
            final StringBuilder sb = new StringBuilder(31);
            sb.append("Unknown message id: ");
            sb.append(what);
            Log.w("GACStateManager", sb.toString());
            return;
        }
        throw (RuntimeException)message.obj;
    }
}

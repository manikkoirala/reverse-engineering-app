// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import java.lang.ref.WeakReference;

final class zabd extends zabw
{
    private final WeakReference<zabe> zaa;
    
    public zabd(final zabe referent) {
        this.zaa = new WeakReference<zabe>(referent);
    }
    
    @Override
    public final void zaa() {
        final zabe zabe = this.zaa.get();
        if (zabe == null) {
            return;
        }
        com.google.android.gms.common.api.internal.zabe.zai(zabe);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api.internal;

import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.api.Api;

@KeepForSdk
public abstract class UnregisterListenerMethod<A extends Api.AnyClient, L>
{
    private final ListenerHolder.ListenerKey<L> zaa;
    
    @KeepForSdk
    public UnregisterListenerMethod(final ListenerHolder.ListenerKey<L> zaa) {
        this.zaa = zaa;
    }
    
    @KeepForSdk
    public ListenerHolder.ListenerKey<L> getListenerKey() {
        return this.zaa;
    }
    
    @KeepForSdk
    public abstract void unregisterListener(final A p0, final TaskCompletionSource<Boolean> p1);
}

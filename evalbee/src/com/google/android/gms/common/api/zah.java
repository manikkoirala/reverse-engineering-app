// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.api;

import com.google.android.gms.common.api.internal.BasePendingResult;

final class zah<R extends Result> extends BasePendingResult<R>
{
    public zah(final GoogleApiClient googleApiClient) {
        super(googleApiClient);
    }
    
    @Override
    public final R createFailedResult(final Status status) {
        throw new UnsupportedOperationException("Creating failed results is not supported");
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common;

import com.google.android.gms.common.internal.zzaf;
import android.content.pm.PackageManager$NameNotFoundException;
import android.os.IBinder;
import java.util.concurrent.Callable;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.dynamite.DynamiteModule;
import android.os.RemoteException;
import android.util.Log;
import java.security.MessageDigest;
import com.google.android.gms.common.util.Hex;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.AndroidUtilsLight;
import android.os.StrictMode$ThreadPolicy;
import android.os.StrictMode;
import android.content.Context;
import com.google.android.gms.common.internal.zzag;

final class zzn
{
    static final zzl zza;
    static final zzl zzb;
    static final zzl zzc;
    static final zzl zzd;
    private static volatile zzag zze;
    private static final Object zzf;
    private static Context zzg;
    
    static {
        zza = new zzf(zzj.zze("0\u0082\u0005\u00c80\u0082\u0003° \u0003\u0002\u0001\u0002\u0002\u0014\u0010\u008ae\bs\u00f9/\u008eQ\u00ed"));
        zzb = new zzg(zzj.zze("0\u0082\u0006\u00040\u0082\u0003\u00ec \u0003\u0002\u0001\u0002\u0002\u0014\u0003£²\u00ad\u00d7\u00e1r\u00cak\u00ec"));
        zzc = new zzh(zzj.zze("0\u0082\u0004C0\u0082\u0003+ \u0003\u0002\u0001\u0002\u0002\t\u0000\u00c2\u00e0\u0087FdJ0\u008d0"));
        zzd = new zzi(zzj.zze("0\u0082\u0004¨0\u0082\u0003\u0090 \u0003\u0002\u0001\u0002\u0002\t\u0000\u00d5\u0085¸l}\u00d3N\u00f50"));
        zzf = new Object();
    }
    
    public static zzx zza(final String s, final zzj zzj, final boolean b, final boolean b2) {
        final StrictMode$ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            return zzh(s, zzj, b, b2);
        }
        finally {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
        }
    }
    
    public static zzx zzb(final String s, final boolean b, final boolean b2, final boolean b3) {
        return zzi(s, b, false, false, true);
    }
    
    public static zzx zzc(final String s, final boolean b, final boolean b2, final boolean b3) {
        return zzi(s, b, false, false, false);
    }
    
    public static void zze(final Context context) {
        synchronized (zzn.class) {
            if (zzn.zzg != null) {
                Log.w("GoogleCertificates", "GoogleCertificates has been initialized already");
                return;
            }
            if (context != null) {
                zzn.zzg = context.getApplicationContext();
            }
        }
    }
    
    public static boolean zzf() {
        final StrictMode$ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            try {
                zzj();
                final boolean zzg = zzn.zze.zzg();
                StrictMode.setThreadPolicy(allowThreadDiskReads);
                return zzg;
            }
            finally {}
        }
        catch (final RemoteException ex) {}
        catch (final DynamiteModule.LoadingException ex2) {}
        final RemoteException ex;
        Log.e("GoogleCertificates", "Failed to get Google certificates from remote", (Throwable)ex);
        StrictMode.setThreadPolicy(allowThreadDiskReads);
        return false;
        StrictMode.setThreadPolicy(allowThreadDiskReads);
    }
    
    public static boolean zzg() {
        final StrictMode$ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            try {
                zzj();
                final boolean zzi = zzn.zze.zzi();
                StrictMode.setThreadPolicy(allowThreadDiskReads);
                return zzi;
            }
            finally {}
        }
        catch (final RemoteException ex) {}
        catch (final DynamiteModule.LoadingException ex2) {}
        final RemoteException ex;
        Log.e("GoogleCertificates", "Failed to get Google certificates from remote", (Throwable)ex);
        StrictMode.setThreadPolicy(allowThreadDiskReads);
        return false;
        StrictMode.setThreadPolicy(allowThreadDiskReads);
    }
    
    private static zzx zzh(final String s, final zzj zzj, final boolean b, final boolean b2) {
        try {
            zzj();
            Preconditions.checkNotNull(zzn.zzg);
            final zzs zzs = new zzs(s, zzj, b, b2);
            try {
                if (zzn.zze.zzh(zzs, ObjectWrapper.wrap(zzn.zzg.getPackageManager()))) {
                    return zzx.zzb();
                }
                return new zzv(new zze(b, s, zzj), null);
            }
            catch (final RemoteException ex) {
                Log.e("GoogleCertificates", "Failed to get Google certificates from remote", (Throwable)ex);
                return zzx.zzd("module call", (Throwable)ex);
            }
        }
        catch (final DynamiteModule.LoadingException ex2) {
            Log.e("GoogleCertificates", "Failed to get Google certificates from remote", (Throwable)ex2);
            return zzx.zzd("module init: ".concat(String.valueOf(ex2.getMessage())), ex2);
        }
    }
    
    private static zzx zzi(final String s, final boolean b, final boolean b2, final boolean b3, final boolean b4) {
        final StrictMode$ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            Preconditions.checkNotNull(zzn.zzg);
            zzx zzx;
            while (true) {
                try {
                    zzj();
                    final zzo zzo = new zzo(s, b, false, (IBinder)ObjectWrapper.wrap(zzn.zzg), false, true);
                    String concat = null;
                    Label_0055: {
                        if (!b4) {
                            break Label_0055;
                        }
                        try {
                            zzq zzq = zzn.zze.zze(zzo);
                            while (true) {
                                if (zzq.zzb()) {
                                    zzx = com.google.android.gms.common.zzx.zzf(zzq.zzc());
                                    return zzx;
                                }
                                final String zza = zzq.zza();
                                Object o;
                                if (zzq.zzd() == 4) {
                                    o = new PackageManager$NameNotFoundException();
                                }
                                else {
                                    o = null;
                                }
                                String s2 = zza;
                                if (zza == null) {
                                    s2 = "error checking package certificate";
                                }
                                zzx = com.google.android.gms.common.zzx.zzg(zzq.zzc(), zzq.zzd(), s2, (Throwable)o);
                                return zzx;
                                zzq = zzn.zze.zzf(zzo);
                                continue;
                            }
                        }
                        catch (final RemoteException ex) {
                            Log.e("GoogleCertificates", "Failed to get Google certificates from remote", (Throwable)ex);
                            concat = "module call";
                        }
                    }
                    final RemoteException ex;
                    zzx = com.google.android.gms.common.zzx.zzd(concat, (Throwable)ex);
                }
                catch (final DynamiteModule.LoadingException ex) {
                    Log.e("GoogleCertificates", "Failed to get Google certificates from remote", (Throwable)ex);
                    final String concat = "module init: ".concat(String.valueOf(((Throwable)ex).getMessage()));
                    continue;
                }
                break;
            }
            return zzx;
        }
        finally {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
        }
    }
    
    private static void zzj() {
        if (zzn.zze != null) {
            return;
        }
        Preconditions.checkNotNull(zzn.zzg);
        synchronized (zzn.zzf) {
            if (zzn.zze == null) {
                zzn.zze = zzaf.zzb(DynamiteModule.load(zzn.zzg, DynamiteModule.PREFER_HIGHEST_OR_LOCAL_VERSION_NO_FORCE_STAGING, "com.google.android.gms.googlecertificates").instantiate("com.google.android.gms.common.GoogleCertificatesImpl"));
            }
        }
    }
}

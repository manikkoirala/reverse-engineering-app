// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common;

import android.util.Log;
import com.google.android.gms.common.internal.Preconditions;
import android.content.Context;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.common.internal.ShowFirstParty;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.errorprone.annotations.RestrictedInheritance;

@RestrictedInheritance(allowedOnPath = ".*javatests.*/com/google/android/gms/common/.*", explanation = "Sub classing of GMS Core's APIs are restricted to testing fakes.", link = "go/gmscore-restrictedinheritance")
@KeepForSdk
@ShowFirstParty
public class PackageSignatureVerifier
{
    @VisibleForTesting
    static volatile zzac zza;
    private static zzad zzb;
    
    private static zzad zza() {
        synchronized (zzad.class) {
            if (PackageSignatureVerifier.zzb == null) {
                PackageSignatureVerifier.zzb = new zzad();
            }
            return PackageSignatureVerifier.zzb;
        }
    }
    
    @KeepForSdk
    @ShowFirstParty
    public PackageVerificationResult queryPackageSignatureVerified(final Context context, final String obj) {
        final boolean honorsDebugCertificates = GooglePlayServicesUtilLight.honorsDebugCertificates(context);
        zza();
        if (zzn.zzf()) {
            String str;
            if (!honorsDebugCertificates) {
                str = "-0";
            }
            else {
                str = "-1";
            }
            final String concat = String.valueOf(obj).concat(str);
            if (PackageSignatureVerifier.zza == null || !zzac.zzb(PackageSignatureVerifier.zza).equals(concat)) {
                zza();
                final zzx zzc = zzn.zzc(obj, honorsDebugCertificates, false, false);
                if (!zzc.zza) {
                    Preconditions.checkNotNull(zzc.zzb);
                    return PackageVerificationResult.zza(obj, zzc.zzb, zzc.zzc);
                }
                PackageSignatureVerifier.zza = new zzac(concat, PackageVerificationResult.zzd(obj, zzc.zzd));
            }
            return zzac.zza(PackageSignatureVerifier.zza);
        }
        throw new zzae();
    }
    
    @KeepForSdk
    @ShowFirstParty
    public PackageVerificationResult queryPackageSignatureVerifiedWithRetry(Context queryPackageSignatureVerified, final String s) {
        try {
            final Object queryPackageSignatureVerified2 = this.queryPackageSignatureVerified(queryPackageSignatureVerified, s);
            ((PackageVerificationResult)queryPackageSignatureVerified2).zzb();
            queryPackageSignatureVerified = (Context)queryPackageSignatureVerified2;
        }
        catch (final SecurityException ex) {
            queryPackageSignatureVerified = (Context)this.queryPackageSignatureVerified(queryPackageSignatureVerified, s);
            if (((PackageVerificationResult)queryPackageSignatureVerified).zzc()) {
                Log.e("PkgSignatureVerifier", "Got flaky result during package signature verification", (Throwable)ex);
                return (PackageVerificationResult)queryPackageSignatureVerified;
            }
        }
        return (PackageVerificationResult)queryPackageSignatureVerified;
    }
}

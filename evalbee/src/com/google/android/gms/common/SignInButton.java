// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common;

import android.view.ViewGroup;
import android.content.res.TypedArray;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.dynamic.RemoteCreator;
import com.google.android.gms.common.internal.zaaa;
import android.util.Log;
import com.google.android.gms.common.internal.zaz;
import com.google.android.gms.base.R;
import android.util.AttributeSet;
import android.content.Context;
import android.view.View;
import android.view.View$OnClickListener;
import android.widget.FrameLayout;

public final class SignInButton extends FrameLayout implements View$OnClickListener
{
    public static final int COLOR_AUTO = 2;
    public static final int COLOR_DARK = 0;
    public static final int COLOR_LIGHT = 1;
    public static final int SIZE_ICON_ONLY = 2;
    public static final int SIZE_STANDARD = 0;
    public static final int SIZE_WIDE = 1;
    private int zaa;
    private int zab;
    private View zac;
    private View$OnClickListener zad;
    
    public SignInButton(final Context context) {
        this(context, null);
    }
    
    public SignInButton(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public SignInButton(Context obtainStyledAttributes, final AttributeSet set, final int n) {
        super(obtainStyledAttributes, set, n);
        this.zad = null;
        obtainStyledAttributes = (Context)obtainStyledAttributes.getTheme().obtainStyledAttributes(set, R.styleable.SignInButton, 0, 0);
        try {
            this.zaa = ((TypedArray)obtainStyledAttributes).getInt(R.styleable.SignInButton_buttonSize, 0);
            this.zab = ((TypedArray)obtainStyledAttributes).getInt(R.styleable.SignInButton_colorScheme, 2);
            ((TypedArray)obtainStyledAttributes).recycle();
            this.setStyle(this.zaa, this.zab);
        }
        finally {
            ((TypedArray)obtainStyledAttributes).recycle();
        }
    }
    
    private final void zaa(final Context context) {
        final View zac = this.zac;
        if (zac != null) {
            ((ViewGroup)this).removeView(zac);
        }
        try {
            this.zac = zaz.zaa(context, this.zaa, this.zab);
        }
        catch (final RemoteCreator.RemoteCreatorException ex) {
            Log.w("SignInButton", "Sign in button not found, using placeholder instead");
            final int zaa = this.zaa;
            final int zab = this.zab;
            final zaaa zac2 = new zaaa(context, null);
            zac2.zaa(context.getResources(), zaa, zab);
            this.zac = (View)zac2;
        }
        ((ViewGroup)this).addView(this.zac);
        this.zac.setEnabled(((View)this).isEnabled());
        this.zac.setOnClickListener((View$OnClickListener)this);
    }
    
    public void onClick(final View view) {
        final View$OnClickListener zad = this.zad;
        if (zad != null && view == this.zac) {
            zad.onClick((View)this);
        }
    }
    
    public void setColorScheme(final int n) {
        this.setStyle(this.zaa, n);
    }
    
    public void setEnabled(final boolean b) {
        super.setEnabled(b);
        this.zac.setEnabled(b);
    }
    
    public void setOnClickListener(final View$OnClickListener zad) {
        this.zad = zad;
        final View zac = this.zac;
        if (zac != null) {
            zac.setOnClickListener((View$OnClickListener)this);
        }
    }
    
    @Deprecated
    public void setScopes(final Scope[] array) {
        this.setStyle(this.zaa, this.zab);
    }
    
    public void setSize(final int n) {
        this.setStyle(n, this.zab);
    }
    
    public void setStyle(final int zaa, final int zab) {
        this.zaa = zaa;
        this.zab = zab;
        this.zaa(((View)this).getContext());
    }
    
    @Deprecated
    public void setStyle(final int n, final int n2, final Scope[] array) {
        this.setStyle(n, n2);
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface ButtonSize {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface ColorScheme {
    }
}

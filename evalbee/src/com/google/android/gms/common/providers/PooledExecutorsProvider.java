// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.providers;

import java.util.concurrent.ScheduledExecutorService;
import com.google.android.gms.common.annotation.KeepForSdk;

@Deprecated
@KeepForSdk
public class PooledExecutorsProvider
{
    private static PooledExecutorFactory zza;
    
    private PooledExecutorsProvider() {
    }
    
    @Deprecated
    @KeepForSdk
    public static PooledExecutorFactory getInstance() {
        synchronized (PooledExecutorsProvider.class) {
            if (PooledExecutorsProvider.zza == null) {
                PooledExecutorsProvider.zza = (PooledExecutorFactory)new zza();
            }
            return PooledExecutorsProvider.zza;
        }
    }
    
    public interface PooledExecutorFactory
    {
        @Deprecated
        @KeepForSdk
        ScheduledExecutorService newSingleThreadScheduledExecutor();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.providers;

import java.util.concurrent.Executors;
import com.google.android.gms.internal.common.zzh;
import java.util.concurrent.ScheduledExecutorService;

final class zza implements PooledExecutorFactory
{
    public zza() {
    }
    
    @Override
    public final ScheduledExecutorService newSingleThreadScheduledExecutor() {
        zzh.zza();
        return Executors.unconfigurableScheduledExecutorService(Executors.newScheduledThreadPool(1));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.wrappers;

import com.google.android.gms.common.util.VisibleForTesting;
import android.content.Context;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public class Wrappers
{
    private static final Wrappers zza;
    private PackageManagerWrapper zzb;
    
    static {
        zza = new Wrappers();
    }
    
    public Wrappers() {
        this.zzb = null;
    }
    
    @KeepForSdk
    public static PackageManagerWrapper packageManager(final Context context) {
        return Wrappers.zza.zza(context);
    }
    
    @VisibleForTesting
    public final PackageManagerWrapper zza(final Context context) {
        synchronized (this) {
            if (this.zzb == null) {
                Context applicationContext = context;
                if (context.getApplicationContext() != null) {
                    applicationContext = context.getApplicationContext();
                }
                this.zzb = new PackageManagerWrapper(applicationContext);
            }
            return this.zzb;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.wrappers;

import com.google.android.gms.common.util.PlatformVersion;
import android.content.Context;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public class InstantApps
{
    private static Context zza;
    private static Boolean zzb;
    
    @KeepForSdk
    public static boolean isInstantApp(final Context context) {
        synchronized (InstantApps.class) {
            final Context applicationContext = context.getApplicationContext();
            final Context zza = InstantApps.zza;
            if (zza != null) {
                final Boolean zzb = InstantApps.zzb;
                if (zzb != null) {
                    if (zza == applicationContext) {
                        return zzb;
                    }
                }
            }
            InstantApps.zzb = null;
            Label_0100: {
                Boolean zzb2 = null;
                Label_0066: {
                    if (!PlatformVersion.isAtLeastO()) {
                        try {
                            context.getClassLoader().loadClass("com.google.android.instantapps.supervisor.InstantAppsRuntime");
                            InstantApps.zzb = Boolean.TRUE;
                        }
                        catch (final ClassNotFoundException ex) {
                            zzb2 = Boolean.FALSE;
                            break Label_0066;
                        }
                        break Label_0100;
                    }
                    zzb2 = rf0.a(applicationContext.getPackageManager());
                }
                InstantApps.zzb = zzb2;
            }
            InstantApps.zza = applicationContext;
            return InstantApps.zzb;
        }
    }
}

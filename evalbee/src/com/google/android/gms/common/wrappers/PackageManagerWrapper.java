// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.wrappers;

import android.app.AppOpsManager;
import com.google.android.gms.common.util.PlatformVersion;
import android.os.Process;
import android.os.Binder;
import android.content.pm.PackageInfo;
import android.content.pm.ApplicationInfo;
import com.google.errorprone.annotations.ResultIgnorabilityUnspecified;
import android.content.Context;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public class PackageManagerWrapper
{
    protected final Context zza;
    
    public PackageManagerWrapper(final Context zza) {
        this.zza = zza;
    }
    
    @KeepForSdk
    public int checkCallingOrSelfPermission(final String s) {
        return this.zza.checkCallingOrSelfPermission(s);
    }
    
    @ResultIgnorabilityUnspecified
    @KeepForSdk
    public int checkPermission(final String s, final String s2) {
        return this.zza.getPackageManager().checkPermission(s, s2);
    }
    
    @ResultIgnorabilityUnspecified
    @KeepForSdk
    public ApplicationInfo getApplicationInfo(final String s, final int n) {
        return this.zza.getPackageManager().getApplicationInfo(s, n);
    }
    
    @KeepForSdk
    public CharSequence getApplicationLabel(final String s) {
        return this.zza.getPackageManager().getApplicationLabel(this.zza.getPackageManager().getApplicationInfo(s, 0));
    }
    
    @ResultIgnorabilityUnspecified
    @KeepForSdk
    public y21 getApplicationLabelAndIcon(final String s) {
        final ApplicationInfo applicationInfo = this.zza.getPackageManager().getApplicationInfo(s, 0);
        return y21.a(this.zza.getPackageManager().getApplicationLabel(applicationInfo), this.zza.getPackageManager().getApplicationIcon(applicationInfo));
    }
    
    @ResultIgnorabilityUnspecified
    @KeepForSdk
    public PackageInfo getPackageInfo(final String s, final int n) {
        return this.zza.getPackageManager().getPackageInfo(s, n);
    }
    
    @KeepForSdk
    public boolean isCallerInstantApp() {
        if (Binder.getCallingUid() == Process.myUid()) {
            return InstantApps.isInstantApp(this.zza);
        }
        if (PlatformVersion.isAtLeastO()) {
            final String nameForUid = this.zza.getPackageManager().getNameForUid(Binder.getCallingUid());
            if (nameForUid != null) {
                return o21.a(this.zza.getPackageManager(), nameForUid);
            }
        }
        return false;
    }
    
    public final boolean zza(int i, final String s) {
        if (PlatformVersion.isAtLeastKitKat()) {
            try {
                final AppOpsManager appOpsManager = (AppOpsManager)this.zza.getSystemService("appops");
                if (appOpsManager != null) {
                    appOpsManager.checkPackage(i, s);
                    return true;
                }
                throw new NullPointerException("context.getSystemService(Context.APP_OPS_SERVICE) is null");
            }
            catch (final SecurityException ex) {
                return false;
            }
        }
        final String[] packagesForUid = this.zza.getPackageManager().getPackagesForUid(i);
        if (s != null && packagesForUid != null) {
            for (i = 0; i < packagesForUid.length; ++i) {
                if (s.equals(packagesForUid[i])) {
                    return true;
                }
            }
        }
        return false;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common;

final class zzac
{
    private final String zza;
    private final PackageVerificationResult zzb;
    
    public zzac(final String zza, final PackageVerificationResult zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
}

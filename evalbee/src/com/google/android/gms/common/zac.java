// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common;

import android.util.Log;
import android.os.Message;
import android.os.Looper;
import android.content.Context;
import com.google.android.gms.internal.base.zaq;

final class zac extends zaq
{
    final GoogleApiAvailability zaa;
    private final Context zab;
    
    public zac(final GoogleApiAvailability zaa, final Context context) {
        this.zaa = zaa;
        Looper looper;
        if (Looper.myLooper() == null) {
            looper = Looper.getMainLooper();
        }
        else {
            looper = Looper.myLooper();
        }
        super(looper);
        this.zab = context.getApplicationContext();
    }
    
    public final void handleMessage(final Message message) {
        final int what = message.what;
        if (what != 1) {
            final StringBuilder sb = new StringBuilder(50);
            sb.append("Don't know how to handle this message: ");
            sb.append(what);
            Log.w("GoogleApiAvailability", sb.toString());
            return;
        }
        final int googlePlayServicesAvailable = this.zaa.isGooglePlayServicesAvailable(this.zab);
        if (this.zaa.isUserResolvableError(googlePlayServicesAvailable)) {
            this.zaa.showErrorNotification(this.zab, googlePlayServicesAvailable);
        }
    }
}

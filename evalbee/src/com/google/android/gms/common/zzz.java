// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common;

import java.util.AbstractCollection;
import java.util.Collection;
import com.google.android.gms.common.internal.Preconditions;
import java.util.List;
import com.google.android.gms.internal.common.zzag;

final class zzz
{
    private String zza;
    private long zzb;
    private zzag zzc;
    private zzag zzd;
    
    public zzz() {
        this.zza = null;
        this.zzb = -1L;
        this.zzc = zzag.zzl();
        this.zzd = zzag.zzl();
    }
    
    public final zzz zza(final long zzb) {
        this.zzb = zzb;
        return this;
    }
    
    public final zzz zzb(final List list) {
        Preconditions.checkNotNull(list);
        this.zzd = zzag.zzk((Collection)list);
        return this;
    }
    
    public final zzz zzc(final List list) {
        Preconditions.checkNotNull(list);
        this.zzc = zzag.zzk((Collection)list);
        return this;
    }
    
    public final zzz zzd(final String zza) {
        this.zza = zza;
        return this;
    }
    
    public final zzab zze() {
        if (this.zza == null) {
            throw new IllegalStateException("packageName must be defined");
        }
        if (this.zzb < 0L) {
            throw new IllegalStateException("minimumStampedVersionNumber must be greater than or equal to 0");
        }
        if (((AbstractCollection)this.zzc).isEmpty() && ((AbstractCollection)this.zzd).isEmpty()) {
            throw new IllegalStateException("Either orderedTestCerts or orderedProdCerts must have at least one cert");
        }
        return new zzab(this.zza, this.zzb, this.zzc, this.zzd, null);
    }
}

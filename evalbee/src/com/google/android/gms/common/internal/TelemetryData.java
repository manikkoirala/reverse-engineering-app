// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import java.util.ArrayList;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import java.util.List;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@KeepForSdk
@Class(creator = "TelemetryDataCreator")
public class TelemetryData extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<TelemetryData> CREATOR;
    @Field(getter = "getTelemetryConfigVersion", id = 1)
    private final int zaa;
    @Field(getter = "getMethodInvocations", id = 2)
    private List<MethodInvocation> zab;
    
    static {
        CREATOR = (Parcelable$Creator)new zaab();
    }
    
    @Constructor
    public TelemetryData(@Param(id = 1) final int zaa, @Param(id = 2) final List<MethodInvocation> zab) {
        this.zaa = zaa;
        this.zab = zab;
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.zaa);
        SafeParcelWriter.writeTypedList(parcel, 2, this.zab, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public final int zaa() {
        return this.zaa;
    }
    
    public final List<MethodInvocation> zab() {
        return this.zab;
    }
    
    public final void zac(final MethodInvocation methodInvocation) {
        if (this.zab == null) {
            this.zab = new ArrayList<MethodInvocation>();
        }
        this.zab.add(methodInvocation);
    }
}

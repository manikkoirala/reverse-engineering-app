// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.os.BaseBundle;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;
import java.util.Collection;
import android.os.Bundle;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public final class Objects
{
    private Objects() {
        throw new AssertionError((Object)"Uninstantiable");
    }
    
    @KeepForSdk
    public static boolean checkBundlesEquality(final Bundle bundle, final Bundle bundle2) {
        if (bundle == null || bundle2 == null) {
            return bundle == bundle2;
        }
        if (((BaseBundle)bundle).size() != ((BaseBundle)bundle2).size()) {
            return false;
        }
        final Set keySet = ((BaseBundle)bundle).keySet();
        if (!keySet.containsAll(((BaseBundle)bundle2).keySet())) {
            return false;
        }
        for (final String s : keySet) {
            if (!equal(((BaseBundle)bundle).get(s), ((BaseBundle)bundle2).get(s))) {
                return false;
            }
        }
        return true;
    }
    
    @KeepForSdk
    public static boolean equal(final Object o, final Object obj) {
        boolean b = true;
        if (o != obj) {
            if (o != null) {
                if (!o.equals(obj)) {
                    return false;
                }
                b = b;
            }
            else {
                b = false;
            }
        }
        return b;
    }
    
    @KeepForSdk
    public static int hashCode(final Object... a) {
        return Arrays.hashCode(a);
    }
    
    @KeepForSdk
    public static ToStringHelper toStringHelper(final Object o) {
        return new ToStringHelper(o, null);
    }
    
    @KeepForSdk
    public static final class ToStringHelper
    {
        private final List zza;
        private final Object zzb;
        
        @KeepForSdk
        public ToStringHelper add(final String str, final Object obj) {
            final List zza = this.zza;
            Preconditions.checkNotNull(str);
            final String value = String.valueOf(obj);
            final StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append("=");
            sb.append(value);
            zza.add(sb.toString());
            return this;
        }
        
        @KeepForSdk
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder(100);
            sb.append(this.zzb.getClass().getSimpleName());
            sb.append('{');
            for (int size = this.zza.size(), i = 0; i < size; ++i) {
                sb.append((String)this.zza.get(i));
                if (i < size - 1) {
                    sb.append(", ");
                }
            }
            sb.append('}');
            return sb.toString();
        }
    }
}

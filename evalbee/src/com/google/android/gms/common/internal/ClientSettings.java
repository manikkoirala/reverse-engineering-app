// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import com.google.android.gms.common.api.GoogleApiClient;
import android.content.Context;
import java.util.Iterator;
import java.util.Collection;
import java.util.HashSet;
import java.util.Collections;
import com.google.android.gms.signin.SignInOptions;
import android.view.View;
import com.google.android.gms.common.api.Api;
import java.util.Map;
import com.google.android.gms.common.api.Scope;
import java.util.Set;
import android.accounts.Account;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
@VisibleForTesting
public final class ClientSettings
{
    private final Account zaa;
    private final Set<Scope> zab;
    private final Set<Scope> zac;
    private final Map<Api<?>, zab> zad;
    private final int zae;
    private final View zaf;
    private final String zag;
    private final String zah;
    private final SignInOptions zai;
    private Integer zaj;
    
    @KeepForSdk
    public ClientSettings(final Account account, final Set<Scope> set, final Map<Api<?>, zab> map, final int n, final View view, final String s, final String s2, final SignInOptions signInOptions) {
        this(account, set, map, n, view, s, s2, signInOptions, false);
    }
    
    public ClientSettings(final Account zaa, final Set<Scope> s, final Map<Api<?>, zab> map, final int zae, final View zaf, final String zag, final String zah, final SignInOptions signInOptions, final boolean b) {
        this.zaa = zaa;
        Set<Object> set;
        if (s == null) {
            set = Collections.emptySet();
        }
        else {
            set = Collections.unmodifiableSet((Set<?>)s);
        }
        this.zab = (Set<Scope>)set;
        Map<Api<?>, zab> emptyMap = map;
        if (map == null) {
            emptyMap = Collections.emptyMap();
        }
        this.zad = emptyMap;
        this.zaf = zaf;
        this.zae = zae;
        this.zag = zag;
        this.zah = zah;
        SignInOptions zaa2;
        if ((zaa2 = signInOptions) == null) {
            zaa2 = SignInOptions.zaa;
        }
        this.zai = zaa2;
        final HashSet s2 = new HashSet(set);
        final Iterator<zab> iterator = emptyMap.values().iterator();
        while (iterator.hasNext()) {
            s2.addAll((Collection)iterator.next().zaa);
        }
        this.zac = Collections.unmodifiableSet((Set<? extends Scope>)s2);
    }
    
    @KeepForSdk
    public static ClientSettings createDefault(final Context context) {
        return new GoogleApiClient.Builder(context).zaa();
    }
    
    @KeepForSdk
    public Account getAccount() {
        return this.zaa;
    }
    
    @Deprecated
    @KeepForSdk
    public String getAccountName() {
        final Account zaa = this.zaa;
        if (zaa != null) {
            return zaa.name;
        }
        return null;
    }
    
    @KeepForSdk
    public Account getAccountOrDefault() {
        final Account zaa = this.zaa;
        if (zaa != null) {
            return zaa;
        }
        return new Account("<<default account>>", "com.google");
    }
    
    @KeepForSdk
    public Set<Scope> getAllRequestedScopes() {
        return this.zac;
    }
    
    @KeepForSdk
    public Set<Scope> getApplicableScopes(final Api<?> api) {
        final zab zab = this.zad.get(api);
        if (zab != null && !zab.zaa.isEmpty()) {
            final HashSet set = new HashSet((Collection<? extends E>)this.zab);
            set.addAll(zab.zaa);
            return set;
        }
        return this.zab;
    }
    
    @KeepForSdk
    public int getGravityForPopups() {
        return this.zae;
    }
    
    @KeepForSdk
    public String getRealClientPackageName() {
        return this.zag;
    }
    
    @KeepForSdk
    public Set<Scope> getRequiredScopes() {
        return this.zab;
    }
    
    @KeepForSdk
    public View getViewForPopups() {
        return this.zaf;
    }
    
    public final SignInOptions zaa() {
        return this.zai;
    }
    
    public final Integer zab() {
        return this.zaj;
    }
    
    public final String zac() {
        return this.zah;
    }
    
    public final Map<Api<?>, zab> zad() {
        return this.zad;
    }
    
    public final void zae(final Integer zaj) {
        this.zaj = zaj;
    }
    
    @KeepForSdk
    public static final class Builder
    {
        private Account zaa;
        private s8 zab;
        private String zac;
        private String zad;
        private SignInOptions zae;
        
        public Builder() {
            this.zae = SignInOptions.zaa;
        }
        
        @KeepForSdk
        public ClientSettings build() {
            return new ClientSettings(this.zaa, this.zab, null, 0, null, this.zac, this.zad, this.zae, false);
        }
        
        @KeepForSdk
        public Builder setRealClientPackageName(final String zac) {
            this.zac = zac;
            return this;
        }
        
        public final Builder zaa(final Collection<Scope> collection) {
            if (this.zab == null) {
                this.zab = new s8();
            }
            this.zab.addAll(collection);
            return this;
        }
        
        public final Builder zab(final Account zaa) {
            this.zaa = zaa;
            return this;
        }
        
        public final Builder zac(final String zad) {
            this.zad = zad;
            return this;
        }
    }
}

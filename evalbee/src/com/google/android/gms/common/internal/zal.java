// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import com.google.android.gms.common.api.Api;
import android.content.Context;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import android.util.SparseIntArray;

public final class zal
{
    private final SparseIntArray zaa;
    private GoogleApiAvailabilityLight zab;
    
    public zal() {
        this(GoogleApiAvailability.getInstance());
    }
    
    public zal(final GoogleApiAvailabilityLight zab) {
        this.zaa = new SparseIntArray();
        Preconditions.checkNotNull(zab);
        this.zab = zab;
    }
    
    public final int zaa(final Context context, final int n) {
        return this.zaa.get(n, -1);
    }
    
    public final int zab(final Context context, final Api.Client client) {
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(client);
        final boolean requiresGooglePlayServices = client.requiresGooglePlayServices();
        final int n = 0;
        if (!requiresGooglePlayServices) {
            return 0;
        }
        final int minApkVersion = client.getMinApkVersion();
        int n2 = this.zaa(context, minApkVersion);
        if (n2 == -1) {
            while (true) {
                for (int i = 0; i < this.zaa.size(); ++i) {
                    final int key = this.zaa.keyAt(i);
                    if (key > minApkVersion && this.zaa.get(key) == 0) {
                        n2 = n;
                        if (n2 == -1) {
                            n2 = this.zab.isGooglePlayServicesAvailable(context, minApkVersion);
                        }
                        this.zaa.put(minApkVersion, n2);
                        return n2;
                    }
                }
                n2 = -1;
                continue;
            }
        }
        return n2;
    }
    
    public final void zac() {
        this.zaa.clear();
    }
}

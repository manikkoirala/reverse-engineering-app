// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@KeepForSdk
@Class(creator = "ClientIdentityCreator")
@Reserved({ 1000 })
public class ClientIdentity extends AbstractSafeParcelable
{
    @KeepForSdk
    public static final Parcelable$Creator<ClientIdentity> CREATOR;
    @KeepForSdk
    @Field(defaultValueUnchecked = "null", id = 2)
    public final String packageName;
    @KeepForSdk
    @Field(defaultValueUnchecked = "0", id = 1)
    public final int uid;
    
    static {
        CREATOR = (Parcelable$Creator)new zaa();
    }
    
    @Constructor
    public ClientIdentity(@Param(id = 1) final int uid, @Param(id = 2) final String packageName) {
        this.uid = uid;
        this.packageName = packageName;
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof ClientIdentity)) {
            return false;
        }
        final ClientIdentity clientIdentity = (ClientIdentity)o;
        return clientIdentity.uid == this.uid && Objects.equal(clientIdentity.packageName, this.packageName);
    }
    
    @Override
    public final int hashCode() {
        return this.uid;
    }
    
    @Override
    public final String toString() {
        final int uid = this.uid;
        final String packageName = this.packageName;
        final StringBuilder sb = new StringBuilder(String.valueOf(packageName).length() + 12);
        sb.append(uid);
        sb.append(":");
        sb.append(packageName);
        return sb.toString();
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.uid);
        SafeParcelWriter.writeString(parcel, 2, this.packageName, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.os.BaseBundle;
import android.os.Bundle;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.api.Api;

@KeepForSdk
public class TelemetryLoggingOptions implements Optional
{
    public static final TelemetryLoggingOptions zaa;
    private final String zab = zab;
    
    static {
        zaa = builder().build();
    }
    
    @KeepForSdk
    public static Builder builder() {
        return new Builder(null);
    }
    
    @Override
    public final boolean equals(final Object o) {
        return o == this || (o instanceof TelemetryLoggingOptions && Objects.equal(this.zab, ((TelemetryLoggingOptions)o).zab));
    }
    
    @Override
    public final int hashCode() {
        return Objects.hashCode(this.zab);
    }
    
    public final Bundle zaa() {
        final Bundle bundle = new Bundle();
        final String zab = this.zab;
        if (zab != null) {
            ((BaseBundle)bundle).putString("api", zab);
        }
        return bundle;
    }
    
    @KeepForSdk
    public static class Builder
    {
        private String zaa;
        
        private Builder() {
        }
        
        @KeepForSdk
        public TelemetryLoggingOptions build() {
            return new TelemetryLoggingOptions(this.zaa, null);
        }
        
        @KeepForSdk
        public Builder setApi(final String zaa) {
            this.zaa = zaa;
            return this;
        }
    }
}

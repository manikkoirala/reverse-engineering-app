// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.os.IInterface;
import android.util.Log;
import android.os.Bundle;
import android.os.IBinder;
import com.google.android.gms.common.util.VisibleForTesting;

@VisibleForTesting
public final class zzd extends zzac
{
    private BaseGmsClient zza;
    private final int zzb;
    
    public zzd(final BaseGmsClient zza, final int zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public final void onPostInitComplete(final int n, final IBinder binder, final Bundle bundle) {
        Preconditions.checkNotNull(this.zza, "onPostInitComplete can be called only once per call to getRemoteService");
        this.zza.onPostInitHandler(n, binder, bundle, this.zzb);
        this.zza = null;
    }
    
    public final void zzb(final int n, final Bundle bundle) {
        Log.wtf("GmsClient", "received deprecated onAccountValidationComplete callback, ignoring", (Throwable)new Exception());
    }
    
    public final void zzc(final int n, final IBinder binder, final zzk zzk) {
        final BaseGmsClient zza = this.zza;
        Preconditions.checkNotNull(zza, "onPostInitCompleteWithConnectionInfo can be called only once per call togetRemoteService");
        Preconditions.checkNotNull(zzk);
        BaseGmsClient.zzj((BaseGmsClient<IInterface>)zza, zzk);
        this.onPostInitComplete(n, binder, zzk.zza);
    }
}

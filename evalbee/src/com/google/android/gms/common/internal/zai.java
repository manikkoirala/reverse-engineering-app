// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.internal.OnConnectionFailedListener;

final class zai implements BaseOnConnectionFailedListener
{
    final OnConnectionFailedListener zaa;
    
    public zai(final OnConnectionFailedListener zaa) {
        this.zaa = zaa;
    }
    
    @Override
    public final void onConnectionFailed(final ConnectionResult connectionResult) {
        this.zaa.onConnectionFailed(connectionResult);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.os.Parcel;
import android.os.IBinder;
import com.google.android.gms.internal.common.zzb;
import android.os.IInterface;

public interface ICancelToken extends IInterface
{
    void cancel();
    
    public abstract static class Stub extends zzb implements ICancelToken
    {
        public Stub() {
            super("com.google.android.gms.common.internal.ICancelToken");
        }
        
        public static ICancelToken asInterface(final IBinder binder) {
            if (binder == null) {
                return null;
            }
            final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.common.internal.ICancelToken");
            if (queryLocalInterface instanceof ICancelToken) {
                return (ICancelToken)queryLocalInterface;
            }
            return new zzx(binder);
        }
        
        public final boolean zza(final int n, final Parcel parcel, final Parcel parcel2, final int n2) {
            if (n == 2) {
                this.cancel();
                return true;
            }
            return false;
        }
    }
}

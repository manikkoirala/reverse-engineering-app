// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal.service;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation;

final class zad extends zaa
{
    private final BaseImplementation.ResultHolder<Status> zaa;
    
    public zad(final BaseImplementation.ResultHolder<Status> zaa) {
        this.zaa = zaa;
    }
    
    @Override
    public final void zab(final int n) {
        this.zaa.setResult(new Status(n));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import java.util.concurrent.Executor;
import java.util.Iterator;
import java.util.HashMap;
import android.content.ComponentName;
import android.os.IBinder;
import java.util.Map;
import android.content.ServiceConnection;

final class zzp implements ServiceConnection, zzt
{
    final zzs zza;
    private final Map zzb;
    private int zzc;
    private boolean zzd;
    private IBinder zze;
    private final zzo zzf;
    private ComponentName zzg;
    
    public zzp(final zzs zza, final zzo zzf) {
        this.zza = zza;
        this.zzf = zzf;
        this.zzb = new HashMap();
        this.zzc = 2;
    }
    
    public final void onBindingDied(final ComponentName componentName) {
        this.onServiceDisconnected(componentName);
    }
    
    public final void onServiceConnected(final ComponentName zzg, final IBinder zze) {
        synchronized (zzs.zzh(this.zza)) {
            zzs.zzf(this.zza).removeMessages(1, (Object)this.zzf);
            this.zze = zze;
            this.zzg = zzg;
            final Iterator iterator = this.zzb.values().iterator();
            while (iterator.hasNext()) {
                ((ServiceConnection)iterator.next()).onServiceConnected(zzg, zze);
            }
            this.zzc = 1;
        }
    }
    
    public final void onServiceDisconnected(final ComponentName zzg) {
        synchronized (zzs.zzh(this.zza)) {
            zzs.zzf(this.zza).removeMessages(1, (Object)this.zzf);
            this.zze = null;
            this.zzg = zzg;
            final Iterator iterator = this.zzb.values().iterator();
            while (iterator.hasNext()) {
                ((ServiceConnection)iterator.next()).onServiceDisconnected(zzg);
            }
            this.zzc = 2;
        }
    }
    
    public final int zza() {
        return this.zzc;
    }
    
    public final ComponentName zzb() {
        return this.zzg;
    }
    
    public final IBinder zzc() {
        return this.zze;
    }
    
    public final void zzd(final ServiceConnection serviceConnection, final ServiceConnection serviceConnection2, final String s) {
        this.zzb.put(serviceConnection, serviceConnection2);
    }
    
    public final void zze(final String p0, final Executor p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: iconst_3       
        //     2: putfield        com/google/android/gms/common/internal/zzp.zzc:I
        //     5: invokestatic    android/os/StrictMode.getVmPolicy:()Landroid/os/StrictMode$VmPolicy;
        //     8: astore          4
        //    10: invokestatic    com/google/android/gms/common/util/PlatformVersion.isAtLeastS:()Z
        //    13: ifeq            34
        //    16: new             Landroid/os/StrictMode$VmPolicy$Builder;
        //    19: dup            
        //    20: aload           4
        //    22: invokespecial   android/os/StrictMode$VmPolicy$Builder.<init>:(Landroid/os/StrictMode$VmPolicy;)V
        //    25: invokestatic    mi2.a:(Landroid/os/StrictMode$VmPolicy$Builder;)Landroid/os/StrictMode$VmPolicy$Builder;
        //    28: invokevirtual   android/os/StrictMode$VmPolicy$Builder.build:()Landroid/os/StrictMode$VmPolicy;
        //    31: invokestatic    android/os/StrictMode.setVmPolicy:(Landroid/os/StrictMode$VmPolicy;)V
        //    34: aload_0        
        //    35: getfield        com/google/android/gms/common/internal/zzp.zza:Lcom/google/android/gms/common/internal/zzs;
        //    38: astore          5
        //    40: aload           5
        //    42: invokestatic    com/google/android/gms/common/internal/zzs.zzg:(Lcom/google/android/gms/common/internal/zzs;)Lcom/google/android/gms/common/stats/ConnectionTracker;
        //    45: aload           5
        //    47: invokestatic    com/google/android/gms/common/internal/zzs.zze:(Lcom/google/android/gms/common/internal/zzs;)Landroid/content/Context;
        //    50: aload_1        
        //    51: aload_0        
        //    52: getfield        com/google/android/gms/common/internal/zzp.zzf:Lcom/google/android/gms/common/internal/zzo;
        //    55: aload           5
        //    57: invokestatic    com/google/android/gms/common/internal/zzs.zze:(Lcom/google/android/gms/common/internal/zzs;)Landroid/content/Context;
        //    60: invokevirtual   com/google/android/gms/common/internal/zzo.zzb:(Landroid/content/Context;)Landroid/content/Intent;
        //    63: aload_0        
        //    64: sipush          4225
        //    67: aload_2        
        //    68: invokevirtual   com/google/android/gms/common/stats/ConnectionTracker.zza:(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;Landroid/content/ServiceConnection;ILjava/util/concurrent/Executor;)Z
        //    71: istore_3       
        //    72: aload_0        
        //    73: iload_3        
        //    74: putfield        com/google/android/gms/common/internal/zzp.zzd:Z
        //    77: iload_3        
        //    78: ifeq            119
        //    81: aload_0        
        //    82: getfield        com/google/android/gms/common/internal/zzp.zza:Lcom/google/android/gms/common/internal/zzs;
        //    85: invokestatic    com/google/android/gms/common/internal/zzs.zzf:(Lcom/google/android/gms/common/internal/zzs;)Landroid/os/Handler;
        //    88: iconst_1       
        //    89: aload_0        
        //    90: getfield        com/google/android/gms/common/internal/zzp.zzf:Lcom/google/android/gms/common/internal/zzo;
        //    93: invokevirtual   android/os/Handler.obtainMessage:(ILjava/lang/Object;)Landroid/os/Message;
        //    96: astore_1       
        //    97: aload_0        
        //    98: getfield        com/google/android/gms/common/internal/zzp.zza:Lcom/google/android/gms/common/internal/zzs;
        //   101: invokestatic    com/google/android/gms/common/internal/zzs.zzf:(Lcom/google/android/gms/common/internal/zzs;)Landroid/os/Handler;
        //   104: aload_1        
        //   105: aload_0        
        //   106: getfield        com/google/android/gms/common/internal/zzp.zza:Lcom/google/android/gms/common/internal/zzs;
        //   109: invokestatic    com/google/android/gms/common/internal/zzs.zzd:(Lcom/google/android/gms/common/internal/zzs;)J
        //   112: invokevirtual   android/os/Handler.sendMessageDelayed:(Landroid/os/Message;J)Z
        //   115: pop            
        //   116: goto            141
        //   119: aload_0        
        //   120: iconst_2       
        //   121: putfield        com/google/android/gms/common/internal/zzp.zzc:I
        //   124: aload_0        
        //   125: getfield        com/google/android/gms/common/internal/zzp.zza:Lcom/google/android/gms/common/internal/zzs;
        //   128: astore_1       
        //   129: aload_1        
        //   130: invokestatic    com/google/android/gms/common/internal/zzs.zzg:(Lcom/google/android/gms/common/internal/zzs;)Lcom/google/android/gms/common/stats/ConnectionTracker;
        //   133: aload_1        
        //   134: invokestatic    com/google/android/gms/common/internal/zzs.zze:(Lcom/google/android/gms/common/internal/zzs;)Landroid/content/Context;
        //   137: aload_0        
        //   138: invokevirtual   com/google/android/gms/common/stats/ConnectionTracker.unbindService:(Landroid/content/Context;Landroid/content/ServiceConnection;)V
        //   141: aload           4
        //   143: invokestatic    android/os/StrictMode.setVmPolicy:(Landroid/os/StrictMode$VmPolicy;)V
        //   146: return         
        //   147: astore_1       
        //   148: aload           4
        //   150: invokestatic    android/os/StrictMode.setVmPolicy:(Landroid/os/StrictMode$VmPolicy;)V
        //   153: aload_1        
        //   154: athrow         
        //   155: astore_1       
        //   156: goto            141
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                
        //  -----  -----  -----  -----  ------------------------------------
        //  34     77     147    155    Any
        //  81     116    147    155    Any
        //  119    124    147    155    Any
        //  124    141    155    159    Ljava/lang/IllegalArgumentException;
        //  124    141    147    155    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0141:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public final void zzf(final ServiceConnection serviceConnection, final String s) {
        this.zzb.remove(serviceConnection);
    }
    
    public final void zzg(final String s) {
        zzs.zzf(this.zza).removeMessages(1, (Object)this.zzf);
        final zzs zza = this.zza;
        zzs.zzg(zza).unbindService(zzs.zze(zza), (ServiceConnection)this);
        this.zzd = false;
        this.zzc = 2;
    }
    
    public final boolean zzh(final ServiceConnection serviceConnection) {
        return this.zzb.containsKey(serviceConnection);
    }
    
    public final boolean zzi() {
        return this.zzb.isEmpty();
    }
    
    public final boolean zzj() {
        return this.zzd;
    }
}

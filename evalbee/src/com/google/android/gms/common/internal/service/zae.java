// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal.service;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.GoogleApiClient;

public final class zae
{
    public final PendingResult<Status> zaa(final GoogleApiClient googleApiClient) {
        return googleApiClient.execute((PendingResult<Status>)new zac(this, googleApiClient));
    }
}

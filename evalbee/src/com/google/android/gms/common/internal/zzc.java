// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.os.IInterface;
import android.util.Log;

public abstract class zzc
{
    private Object zza;
    private boolean zzb;
    final BaseGmsClient zzd;
    
    public zzc(final BaseGmsClient zzd, final Object zza) {
        this.zzd = zzd;
        this.zza = zza;
        this.zzb = false;
    }
    
    public abstract void zza(final Object p0);
    
    public abstract void zzc();
    
    public final void zze() {
        synchronized (this) {
            final Object zza = this.zza;
            if (this.zzb) {
                final String string = this.toString();
                final StringBuilder sb = new StringBuilder();
                sb.append("Callback proxy ");
                sb.append(string);
                sb.append(" being reused. This is not safe.");
                Log.w("GmsClient", sb.toString());
            }
            monitorexit(this);
            if (zza != null) {
                this.zza(zza);
            }
            synchronized (this) {
                this.zzb = true;
                monitorexit(this);
                this.zzg();
            }
        }
    }
    
    public final void zzf() {
        synchronized (this) {
            this.zza = null;
        }
    }
    
    public final void zzg() {
        this.zzf();
        synchronized (BaseGmsClient.zzf((BaseGmsClient<IInterface>)this.zzd)) {
            BaseGmsClient.zzf((BaseGmsClient<IInterface>)this.zzd).remove(this);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.ConnectionResult;
import android.os.IBinder;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "ResolveAccountResponseCreator")
public final class zav extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zav> CREATOR;
    @VersionField(id = 1)
    final int zaa;
    @Field(id = 2)
    final IBinder zab;
    @Field(getter = "getConnectionResult", id = 3)
    private final ConnectionResult zac;
    @Field(getter = "getSaveDefaultAccount", id = 4)
    private final boolean zad;
    @Field(getter = "isFromCrossClientAuth", id = 5)
    private final boolean zae;
    
    static {
        CREATOR = (Parcelable$Creator)new zaw();
    }
    
    @Constructor
    public zav(@Param(id = 1) final int zaa, @Param(id = 2) final IBinder zab, @Param(id = 3) final ConnectionResult zac, @Param(id = 4) final boolean zad, @Param(id = 5) final boolean zae) {
        this.zaa = zaa;
        this.zab = zab;
        this.zac = zac;
        this.zad = zad;
        this.zae = zae;
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (o == null) {
            return false;
        }
        if (this == o) {
            return true;
        }
        if (!(o instanceof zav)) {
            return false;
        }
        final zav zav = (zav)o;
        return this.zac.equals(zav.zac) && Objects.equal(this.zab(), zav.zab());
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.zaa);
        SafeParcelWriter.writeIBinder(parcel, 2, this.zab, false);
        SafeParcelWriter.writeParcelable(parcel, 3, (Parcelable)this.zac, n, false);
        SafeParcelWriter.writeBoolean(parcel, 4, this.zad);
        SafeParcelWriter.writeBoolean(parcel, 5, this.zae);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public final ConnectionResult zaa() {
        return this.zac;
    }
    
    public final IAccountAccessor zab() {
        final IBinder zab = this.zab;
        if (zab == null) {
            return null;
        }
        return IAccountAccessor.Stub.asInterface(zab);
    }
    
    public final boolean zac() {
        return this.zad;
    }
    
    public final boolean zad() {
        return this.zae;
    }
}

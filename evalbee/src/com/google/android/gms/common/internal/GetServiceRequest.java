// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.os.Parcel;
import android.accounts.Account;
import android.os.Bundle;
import android.os.IBinder;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.api.Scope;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@KeepForSdk
@Class(creator = "GetServiceRequestCreator")
@Reserved({ 9 })
public class GetServiceRequest extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<GetServiceRequest> CREATOR;
    static final Scope[] zza;
    static final Feature[] zzb;
    @VersionField(id = 1)
    final int zzc;
    @Field(id = 2)
    final int zzd;
    @Field(id = 3)
    final int zze;
    @Field(id = 4)
    String zzf;
    @Field(id = 5)
    IBinder zzg;
    @Field(defaultValueUnchecked = "GetServiceRequest.EMPTY_SCOPES", id = 6)
    Scope[] zzh;
    @Field(defaultValueUnchecked = "new android.os.Bundle()", id = 7)
    Bundle zzi;
    @Field(id = 8)
    Account zzj;
    @Field(defaultValueUnchecked = "GetServiceRequest.EMPTY_FEATURES", id = 10)
    Feature[] zzk;
    @Field(defaultValueUnchecked = "GetServiceRequest.EMPTY_FEATURES", id = 11)
    Feature[] zzl;
    @Field(id = 12)
    final boolean zzm;
    @Field(defaultValue = "0", id = 13)
    final int zzn;
    @Field(getter = "isRequestingTelemetryConfiguration", id = 14)
    boolean zzo;
    @Field(getter = "getAttributionTag", id = 15)
    private final String zzp;
    
    static {
        CREATOR = (Parcelable$Creator)new zzn();
        zza = new Scope[0];
        zzb = new Feature[0];
    }
    
    @Constructor
    public GetServiceRequest(@Param(id = 1) final int zzc, @Param(id = 2) final int zzd, @Param(id = 3) final int zze, @Param(id = 4) final String s, @Param(id = 5) final IBinder zzg, @Param(id = 6) final Scope[] array, @Param(id = 7) final Bundle bundle, @Param(id = 8) final Account zzj, @Param(id = 10) Feature[] zzb, @Param(id = 11) final Feature[] array2, @Param(id = 12) final boolean zzm, @Param(id = 13) final int zzn, @Param(id = 14) final boolean zzo, @Param(id = 15) final String zzp) {
        Scope[] zza = array;
        if (array == null) {
            zza = GetServiceRequest.zza;
        }
        Bundle zzi;
        if ((zzi = bundle) == null) {
            zzi = new Bundle();
        }
        Feature[] zzb2;
        if ((zzb2 = zzb) == null) {
            zzb2 = GetServiceRequest.zzb;
        }
        if ((zzb = array2) == null) {
            zzb = GetServiceRequest.zzb;
        }
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
        if ("com.google.android.gms".equals(s)) {
            this.zzf = "com.google.android.gms";
        }
        else {
            this.zzf = s;
        }
        if (zzc < 2) {
            Account accountBinderSafe;
            if (zzg != null) {
                accountBinderSafe = AccountAccessor.getAccountBinderSafe(IAccountAccessor.Stub.asInterface(zzg));
            }
            else {
                accountBinderSafe = null;
            }
            this.zzj = accountBinderSafe;
        }
        else {
            this.zzg = zzg;
            this.zzj = zzj;
        }
        this.zzh = zza;
        this.zzi = zzi;
        this.zzk = zzb2;
        this.zzl = zzb;
        this.zzm = zzm;
        this.zzn = zzn;
        this.zzo = zzo;
        this.zzp = zzp;
    }
    
    @KeepForSdk
    public Bundle getExtraArgs() {
        return this.zzi;
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        com.google.android.gms.common.internal.zzn.zza(this, parcel, n);
    }
    
    public final String zza() {
        return this.zzp;
    }
}

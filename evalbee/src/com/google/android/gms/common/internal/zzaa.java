// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IInterface;

public interface zzaa extends IInterface
{
    int zzc();
    
    IObjectWrapper zzd();
}

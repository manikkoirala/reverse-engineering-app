// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public final class ImagesContract
{
    @KeepForSdk
    public static final String LOCAL = "local";
    @KeepForSdk
    public static final String URL = "url";
}

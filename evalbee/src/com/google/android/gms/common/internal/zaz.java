// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.os.IInterface;
import android.os.IBinder;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.dynamic.ObjectWrapper;
import android.view.View;
import android.content.Context;
import com.google.android.gms.dynamic.RemoteCreator;

public final class zaz extends RemoteCreator<zam>
{
    private static final zaz zaa;
    
    static {
        zaa = new zaz();
    }
    
    private zaz() {
        super("com.google.android.gms.common.ui.SignInButtonCreatorImpl");
    }
    
    public static View zaa(final Context context, final int i, final int j) {
        final zaz zaa = zaz.zaa;
        try {
            return (View)ObjectWrapper.unwrap(zaa.getRemoteCreatorInstance(context).zae(ObjectWrapper.wrap(context), new zax(1, i, j, null)));
        }
        catch (final Exception ex) {
            final StringBuilder sb = new StringBuilder(64);
            sb.append("Could not get button with size ");
            sb.append(i);
            sb.append(" and color ");
            sb.append(j);
            throw new RemoteCreatorException(sb.toString(), ex);
        }
    }
}

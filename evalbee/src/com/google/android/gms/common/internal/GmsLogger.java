// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.util.Log;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public final class GmsLogger
{
    private final String zza;
    private final String zzb;
    
    @KeepForSdk
    public GmsLogger(final String s) {
        this(s, null);
    }
    
    @KeepForSdk
    public GmsLogger(final String zza, final String zzb) {
        Preconditions.checkNotNull(zza, "log tag cannot be null");
        Preconditions.checkArgument(zza.length() <= 23, "tag \"%s\" is longer than the %d character maximum", zza, 23);
        this.zza = zza;
        if (zzb != null && zzb.length() > 0) {
            this.zzb = zzb;
            return;
        }
        this.zzb = null;
    }
    
    private final String zza(final String str) {
        final String zzb = this.zzb;
        if (zzb == null) {
            return str;
        }
        return zzb.concat(str);
    }
    
    private final String zzb(String format, final Object... args) {
        format = String.format(format, args);
        final String zzb = this.zzb;
        if (zzb == null) {
            return format;
        }
        return zzb.concat(format);
    }
    
    @KeepForSdk
    public boolean canLog(final int n) {
        return Log.isLoggable(this.zza, n);
    }
    
    @KeepForSdk
    public boolean canLogPii() {
        return false;
    }
    
    @KeepForSdk
    public void d(final String s, final String s2) {
        if (this.canLog(3)) {
            Log.d(s, this.zza(s2));
        }
    }
    
    @KeepForSdk
    public void d(final String s, final String s2, final Throwable t) {
        if (this.canLog(3)) {
            Log.d(s, this.zza(s2), t);
        }
    }
    
    @KeepForSdk
    public void e(final String s, final String s2) {
        if (this.canLog(6)) {
            Log.e(s, this.zza(s2));
        }
    }
    
    @KeepForSdk
    public void e(final String s, final String s2, final Throwable t) {
        if (this.canLog(6)) {
            Log.e(s, this.zza(s2), t);
        }
    }
    
    @KeepForSdk
    public void efmt(final String s, final String s2, final Object... array) {
        if (this.canLog(6)) {
            Log.e(s, this.zzb(s2, array));
        }
    }
    
    @KeepForSdk
    public void i(final String s, final String s2) {
        if (this.canLog(4)) {
            Log.i(s, this.zza(s2));
        }
    }
    
    @KeepForSdk
    public void i(final String s, final String s2, final Throwable t) {
        if (this.canLog(4)) {
            Log.i(s, this.zza(s2), t);
        }
    }
    
    @KeepForSdk
    public void pii(final String s, final String s2) {
    }
    
    @KeepForSdk
    public void pii(final String s, final String s2, final Throwable t) {
    }
    
    @KeepForSdk
    public void v(final String s, final String s2) {
        if (this.canLog(2)) {
            Log.v(s, this.zza(s2));
        }
    }
    
    @KeepForSdk
    public void v(final String s, final String s2, final Throwable t) {
        if (this.canLog(2)) {
            Log.v(s, this.zza(s2), t);
        }
    }
    
    @KeepForSdk
    public void w(final String s, final String s2) {
        if (this.canLog(5)) {
            Log.w(s, this.zza(s2));
        }
    }
    
    @KeepForSdk
    public void w(final String s, final String s2, final Throwable t) {
        if (this.canLog(5)) {
            Log.w(s, this.zza(s2), t);
        }
    }
    
    @KeepForSdk
    public void wfmt(final String s, final String s2, final Object... array) {
        if (this.canLog(5)) {
            Log.w(this.zza, this.zzb(s2, array));
        }
    }
    
    @KeepForSdk
    public void wtf(final String s, final String s2, final Throwable t) {
        if (this.canLog(7)) {
            Log.e(s, this.zza(s2), t);
            Log.wtf(s, this.zza(s2), t);
        }
    }
}

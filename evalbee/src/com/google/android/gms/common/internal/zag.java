// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.content.ActivityNotFoundException;
import android.util.Log;
import android.os.Build;
import android.content.DialogInterface;
import com.google.android.gms.common.api.internal.LifecycleFragment;
import androidx.fragment.app.Fragment;
import android.content.Intent;
import android.app.Activity;
import android.content.DialogInterface$OnClickListener;

public abstract class zag implements DialogInterface$OnClickListener
{
    public static zag zab(final Activity activity, final Intent intent, final int n) {
        return new zad(intent, activity, n);
    }
    
    public static zag zac(final Fragment fragment, final Intent intent, final int n) {
        return new zae(intent, fragment, n);
    }
    
    public static zag zad(final LifecycleFragment lifecycleFragment, final Intent intent, final int n) {
        return new zaf(intent, lifecycleFragment, 2);
    }
    
    public final void onClick(final DialogInterface dialogInterface, final int n) {
        try {
            try {
                this.zaa();
                dialogInterface.dismiss();
                return;
            }
            finally {}
        }
        catch (final ActivityNotFoundException ex) {
            String s = "Failed to start resolution intent.";
            if (Build.FINGERPRINT.contains("generic")) {
                s = "Failed to start resolution intent. This may occur when resolving Google Play services connection issues on emulators with Google APIs but not Google Play Store.";
            }
            Log.e("DialogRedirect", s, (Throwable)ex);
            dialogInterface.dismiss();
            return;
        }
        dialogInterface.dismiss();
    }
    
    public abstract void zaa();
}

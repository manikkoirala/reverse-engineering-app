// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.common.zzc;
import android.os.Bundle;
import android.os.IBinder;
import com.google.android.gms.internal.common.zza;

public final class zzab extends zza implements IGmsCallbacks
{
    public zzab(final IBinder binder) {
        super(binder, "com.google.android.gms.common.internal.IGmsCallbacks");
    }
    
    public final void onPostInitComplete(final int n, final IBinder binder, final Bundle bundle) {
        final Parcel zza = this.zza();
        zza.writeInt(n);
        zza.writeStrongBinder(binder);
        zzc.zzc(zza, (Parcelable)bundle);
        this.zzC(1, zza);
    }
    
    public final void zzb(final int n, final Bundle bundle) {
        throw null;
    }
    
    public final void zzc(final int n, final IBinder binder, final zzk zzk) {
        throw null;
    }
}

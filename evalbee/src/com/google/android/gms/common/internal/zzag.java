// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.common.zzs;
import com.google.android.gms.common.zzq;
import com.google.android.gms.common.zzo;
import android.os.IInterface;

public interface zzag extends IInterface
{
    zzq zze(final zzo p0);
    
    zzq zzf(final zzo p0);
    
    boolean zzg();
    
    boolean zzh(final zzs p0, final IObjectWrapper p1);
    
    boolean zzi();
}

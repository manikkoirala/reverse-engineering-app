// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@KeepForSdk
@Class(creator = "RootTelemetryConfigurationCreator")
public class RootTelemetryConfiguration extends AbstractSafeParcelable
{
    @KeepForSdk
    public static final Parcelable$Creator<RootTelemetryConfiguration> CREATOR;
    @Field(getter = "getVersion", id = 1)
    private final int zza;
    @Field(getter = "getMethodInvocationTelemetryEnabled", id = 2)
    private final boolean zzb;
    @Field(getter = "getMethodTimingTelemetryEnabled", id = 3)
    private final boolean zzc;
    @Field(getter = "getBatchPeriodMillis", id = 4)
    private final int zzd;
    @Field(getter = "getMaxMethodInvocationsInBatch", id = 5)
    private final int zze;
    
    static {
        CREATOR = (Parcelable$Creator)new zzaj();
    }
    
    @Constructor
    public RootTelemetryConfiguration(@Param(id = 1) final int zza, @Param(id = 2) final boolean zzb, @Param(id = 3) final boolean zzc, @Param(id = 4) final int zzd, @Param(id = 5) final int zze) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
    }
    
    @KeepForSdk
    public int getBatchPeriodMillis() {
        return this.zzd;
    }
    
    @KeepForSdk
    public int getMaxMethodInvocationsInBatch() {
        return this.zze;
    }
    
    @KeepForSdk
    public boolean getMethodInvocationTelemetryEnabled() {
        return this.zzb;
    }
    
    @KeepForSdk
    public boolean getMethodTimingTelemetryEnabled() {
        return this.zzc;
    }
    
    @KeepForSdk
    public int getVersion() {
        return this.zza;
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.getVersion());
        SafeParcelWriter.writeBoolean(parcel, 2, this.getMethodInvocationTelemetryEnabled());
        SafeParcelWriter.writeBoolean(parcel, 3, this.getMethodTimingTelemetryEnabled());
        SafeParcelWriter.writeInt(parcel, 4, this.getBatchPeriodMillis());
        SafeParcelWriter.writeInt(parcel, 5, this.getMaxMethodInvocationsInBatch());
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import com.google.android.gms.common.internal.service.zao;
import android.content.Context;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public class TelemetryLogging
{
    private TelemetryLogging() {
    }
    
    @KeepForSdk
    public static TelemetryLoggingClient getClient(final Context context) {
        return getClient(context, TelemetryLoggingOptions.zaa);
    }
    
    @KeepForSdk
    public static TelemetryLoggingClient getClient(final Context context, final TelemetryLoggingOptions telemetryLoggingOptions) {
        return new zao(context, telemetryLoggingOptions);
    }
}

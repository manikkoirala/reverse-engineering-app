// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import com.google.android.gms.common.R;
import android.content.Context;
import android.content.res.Resources;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public class StringResourceValueReader
{
    private final Resources zza;
    private final String zzb;
    
    public StringResourceValueReader(final Context context) {
        Preconditions.checkNotNull(context);
        final Resources resources = context.getResources();
        this.zza = resources;
        this.zzb = resources.getResourcePackageName(R.string.common_google_play_services_unknown_issue);
    }
    
    @KeepForSdk
    public String getString(final String s) {
        final int identifier = this.zza.getIdentifier(s, "string", this.zzb);
        if (identifier == 0) {
            return null;
        }
        return this.zza.getString(identifier);
    }
}

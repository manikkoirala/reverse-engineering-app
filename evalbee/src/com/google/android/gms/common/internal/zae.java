// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import androidx.fragment.app.Fragment;
import android.content.Intent;

final class zae extends zag
{
    final Intent zaa;
    final Fragment zab;
    final int zac;
    
    public zae(final Intent zaa, final Fragment zab, final int zac) {
        this.zaa = zaa;
        this.zab = zab;
        this.zac = zac;
    }
    
    @Override
    public final void zaa() {
        final Intent zaa = this.zaa;
        if (zaa != null) {
            this.zab.startActivityForResult(zaa, this.zac);
        }
    }
}

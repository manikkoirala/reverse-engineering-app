// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal.service;

import android.os.Parcel;
import com.google.android.gms.internal.base.zab;

public abstract class zaj extends zab implements zak
{
    public zaj() {
        super("com.google.android.gms.common.internal.service.ICommonCallbacks");
    }
    
    public final boolean zaa(final int n, final Parcel parcel, final Parcel parcel2, final int n2) {
        if (n == 1) {
            this.zab(parcel.readInt());
            return true;
        }
        return false;
    }
}

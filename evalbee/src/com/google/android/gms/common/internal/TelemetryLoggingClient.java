// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.api.HasApiKey;

@KeepForSdk
public interface TelemetryLoggingClient extends HasApiKey<TelemetryLoggingOptions>
{
    @KeepForSdk
    Task<Void> log(final TelemetryData p0);
}

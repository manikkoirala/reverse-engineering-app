// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.os.IInterface;
import com.google.android.gms.common.ConnectionResult;
import android.os.Bundle;

public final class zzg extends zza
{
    final BaseGmsClient zze;
    
    public zzg(final BaseGmsClient zze, final int n, final Bundle bundle) {
        super(this.zze = zze, n, null);
    }
    
    @Override
    public final void zzb(final ConnectionResult connectionResult) {
        if (this.zze.enableLocalFallback() && BaseGmsClient.zzo((BaseGmsClient<IInterface>)this.zze)) {
            BaseGmsClient.zzk((BaseGmsClient<IInterface>)this.zze, 16);
            return;
        }
        this.zze.zzc.onReportServiceBinding(connectionResult);
        this.zze.onConnectionFailed(connectionResult);
    }
    
    @Override
    public final boolean zzd() {
        this.zze.zzc.onReportServiceBinding(ConnectionResult.RESULT_SUCCESS);
        return true;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.content.res.Resources;
import android.content.res.Resources$NotFoundException;
import android.util.Log;
import android.util.TypedValue;
import android.util.AttributeSet;
import android.content.Context;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public class ViewUtils
{
    private ViewUtils() {
    }
    
    @KeepForSdk
    public static String getXmlAttributeString(String attributeValue, final String str, final Context context, AttributeSet set, final boolean b, final boolean b2, final String s) {
        if (set == null) {
            attributeValue = null;
        }
        else {
            attributeValue = set.getAttributeValue(attributeValue, str);
        }
        String string = attributeValue;
        if (attributeValue != null) {
            string = attributeValue;
            if (attributeValue.startsWith("@string/")) {
                string = attributeValue;
                if (b) {
                    final String substring = attributeValue.substring(8);
                    final String packageName = context.getPackageName();
                    set = (AttributeSet)new TypedValue();
                    try {
                        final Resources resources = context.getResources();
                        final StringBuilder sb = new StringBuilder();
                        sb.append(packageName);
                        sb.append(":string/");
                        sb.append(substring);
                        resources.getValue(sb.toString(), (TypedValue)set, true);
                    }
                    catch (final Resources$NotFoundException ex) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Could not find resource for ");
                        sb2.append(str);
                        sb2.append(": ");
                        sb2.append(attributeValue);
                        Log.w(s, sb2.toString());
                    }
                    final CharSequence string2 = ((TypedValue)set).string;
                    if (string2 != null) {
                        string = string2.toString();
                    }
                    else {
                        final String string3 = set.toString();
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("Resource ");
                        sb3.append(str);
                        sb3.append(" was not a string: ");
                        sb3.append(string3);
                        Log.w(s, sb3.toString());
                        string = attributeValue;
                    }
                }
            }
        }
        if (b2 && string == null) {
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("Required XML attribute \"");
            sb4.append(str);
            sb4.append("\" missing");
            Log.w(s, sb4.toString());
        }
        return string;
    }
}

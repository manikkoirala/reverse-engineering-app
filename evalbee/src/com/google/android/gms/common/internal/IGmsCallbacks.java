// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;

public interface IGmsCallbacks extends IInterface
{
    void onPostInitComplete(final int p0, final IBinder p1, final Bundle p2);
    
    void zzb(final int p0, final Bundle p1);
    
    void zzc(final int p0, final IBinder p1, final zzk p2);
}

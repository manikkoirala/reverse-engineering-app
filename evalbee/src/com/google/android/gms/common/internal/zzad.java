// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.os.IInterface;
import android.os.Parcel;
import android.os.IBinder;

final class zzad implements IGmsServiceBroker
{
    private final IBinder zza;
    
    public zzad(final IBinder zza) {
        this.zza = zza;
    }
    
    public final IBinder asBinder() {
        return this.zza;
    }
    
    @Override
    public final void getService(final IGmsCallbacks gmsCallbacks, final GetServiceRequest getServiceRequest) {
        final Parcel obtain = Parcel.obtain();
        final Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
            IBinder binder;
            if (gmsCallbacks != null) {
                binder = ((IInterface)gmsCallbacks).asBinder();
            }
            else {
                binder = null;
            }
            obtain.writeStrongBinder(binder);
            if (getServiceRequest != null) {
                obtain.writeInt(1);
                zzn.zza(getServiceRequest, obtain, 0);
            }
            else {
                obtain.writeInt(0);
            }
            this.zza.transact(46, obtain, obtain2, 0);
            obtain2.readException();
        }
        finally {
            obtain2.recycle();
            obtain.recycle();
        }
    }
}

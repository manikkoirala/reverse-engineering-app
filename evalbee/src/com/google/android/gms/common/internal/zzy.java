// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.Parcel;
import android.os.IBinder;
import com.google.android.gms.internal.common.zza;

public final class zzy extends zza implements zzaa
{
    public zzy(final IBinder binder) {
        super(binder, "com.google.android.gms.common.internal.ICertData");
    }
    
    public final int zzc() {
        final Parcel zzB = this.zzB(2, this.zza());
        final int int1 = zzB.readInt();
        zzB.recycle();
        return int1;
    }
    
    public final IObjectWrapper zzd() {
        final Parcel zzB = this.zzB(1, this.zza());
        final IObjectWrapper interface1 = IObjectWrapper.Stub.asInterface(zzB.readStrongBinder());
        zzB.recycle();
        return interface1;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.os.RemoteException;
import android.util.Log;
import android.os.IInterface;
import com.google.android.gms.common.ConnectionResult;
import android.os.Bundle;
import android.os.IBinder;

public final class zzf extends zza
{
    public final IBinder zze;
    final BaseGmsClient zzf;
    
    public zzf(final BaseGmsClient zzf, final int n, final IBinder zze, final Bundle bundle) {
        super(this.zzf = zzf, n, bundle);
        this.zze = zze;
    }
    
    @Override
    public final void zzb(final ConnectionResult connectionResult) {
        if (BaseGmsClient.zzc((BaseGmsClient<IInterface>)this.zzf) != null) {
            BaseGmsClient.zzc((BaseGmsClient<IInterface>)this.zzf).onConnectionFailed(connectionResult);
        }
        this.zzf.onConnectionFailed(connectionResult);
    }
    
    @Override
    public final boolean zzd() {
        while (true) {
            try {
                final IBinder zze = this.zze;
                Preconditions.checkNotNull(zze);
                final String interfaceDescriptor = zze.getInterfaceDescriptor();
                if (!this.zzf.getServiceDescriptor().equals(interfaceDescriptor)) {
                    final String serviceDescriptor = this.zzf.getServiceDescriptor();
                    final StringBuilder sb = new StringBuilder();
                    sb.append("service descriptor mismatch: ");
                    sb.append(serviceDescriptor);
                    sb.append(" vs. ");
                    sb.append(interfaceDescriptor);
                    final String string = sb.toString();
                    Log.w("GmsClient", string);
                    return false;
                }
                final IInterface serviceInterface = this.zzf.createServiceInterface(this.zze);
                if (serviceInterface != null && (BaseGmsClient.zzn((BaseGmsClient<IInterface>)this.zzf, 2, 4, serviceInterface) || BaseGmsClient.zzn((BaseGmsClient<IInterface>)this.zzf, 3, 4, serviceInterface))) {
                    BaseGmsClient.zzg((BaseGmsClient<IInterface>)this.zzf, (ConnectionResult)null);
                    final Bundle connectionHint = this.zzf.getConnectionHint();
                    final BaseGmsClient zzf = this.zzf;
                    if (BaseGmsClient.zzb((BaseGmsClient<IInterface>)zzf) != null) {
                        BaseGmsClient.zzb((BaseGmsClient<IInterface>)zzf).onConnected(connectionHint);
                    }
                    return true;
                }
                return false;
            }
            catch (final RemoteException ex) {
                final String string = "service probably died";
                continue;
            }
            break;
        }
    }
}

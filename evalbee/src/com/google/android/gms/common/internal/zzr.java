// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.content.ComponentName;
import android.util.Log;
import android.os.Message;
import android.os.Handler$Callback;

final class zzr implements Handler$Callback
{
    final zzs zza;
    
    public final boolean handleMessage(final Message message) {
        final int what = message.what;
        if (what != 0) {
            if (what != 1) {
                return false;
            }
            synchronized (zzs.zzh(this.zza)) {
                final zzo zzo = (zzo)message.obj;
                final zzp zzp = zzs.zzh(this.zza).get(zzo);
                if (zzp != null && zzp.zza() == 3) {
                    final String value = String.valueOf(zzo);
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Timeout waiting for ServiceConnection callback ");
                    sb.append(value);
                    Log.e("GmsClientSupervisor", sb.toString(), (Throwable)new Exception());
                    ComponentName componentName;
                    if ((componentName = zzp.zzb()) == null) {
                        componentName = zzo.zza();
                    }
                    ComponentName componentName2;
                    if ((componentName2 = componentName) == null) {
                        componentName2 = new(android.content.ComponentName.class)();
                        final String zzc = zzo.zzc();
                        Preconditions.checkNotNull(zzc);
                        new ComponentName(zzc, "unknown");
                    }
                    zzp.onServiceDisconnected(componentName2);
                }
                return true;
            }
        }
        synchronized (zzs.zzh(this.zza)) {
            final zzo zzo2 = (zzo)message.obj;
            final zzp zzp2 = zzs.zzh(this.zza).get(zzo2);
            if (zzp2 != null && zzp2.zzi()) {
                if (zzp2.zzj()) {
                    zzp2.zzg("GmsClientSupervisor");
                }
                zzs.zzh(this.zza).remove(zzo2);
            }
            return true;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import java.util.Iterator;
import java.util.Collection;
import com.google.android.gms.common.ConnectionResult;
import android.util.Log;
import android.os.Bundle;
import android.os.Message;
import com.google.android.gms.internal.base.zaq;
import android.os.Looper;
import android.os.Handler;
import java.util.concurrent.atomic.AtomicInteger;
import org.checkerframework.checker.initialization.qual.NotOnlyInitialized;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.common.api.GoogleApiClient;
import java.util.ArrayList;
import android.os.Handler$Callback;

public final class zak implements Handler$Callback
{
    @VisibleForTesting
    final ArrayList<GoogleApiClient.ConnectionCallbacks> zaa;
    @NotOnlyInitialized
    private final zaj zab;
    private final ArrayList<GoogleApiClient.ConnectionCallbacks> zac;
    private final ArrayList<GoogleApiClient.OnConnectionFailedListener> zad;
    private volatile boolean zae;
    private final AtomicInteger zaf;
    private boolean zag;
    private final Handler zah;
    private final Object zai;
    
    public zak(final Looper looper, final zaj zab) {
        this.zac = new ArrayList<GoogleApiClient.ConnectionCallbacks>();
        this.zaa = new ArrayList<GoogleApiClient.ConnectionCallbacks>();
        this.zad = new ArrayList<GoogleApiClient.OnConnectionFailedListener>();
        this.zae = false;
        this.zaf = new AtomicInteger(0);
        this.zag = false;
        this.zai = new Object();
        this.zab = zab;
        this.zah = (Handler)new zaq(looper, (Handler$Callback)this);
    }
    
    public final boolean handleMessage(final Message message) {
        final int what = message.what;
        if (what == 1) {
            final GoogleApiClient.ConnectionCallbacks o = (GoogleApiClient.ConnectionCallbacks)message.obj;
            synchronized (this.zai) {
                if (this.zae && this.zab.isConnected() && this.zac.contains(o)) {
                    o.onConnected(null);
                }
                return true;
            }
        }
        final StringBuilder sb = new StringBuilder(45);
        sb.append("Don't know how to handle message: ");
        sb.append(what);
        Log.wtf("GmsClientEvents", sb.toString(), (Throwable)new Exception());
        return false;
    }
    
    public final void zaa() {
        this.zae = false;
        this.zaf.incrementAndGet();
    }
    
    public final void zab() {
        this.zae = true;
    }
    
    @VisibleForTesting
    public final void zac(final ConnectionResult connectionResult) {
        Preconditions.checkHandlerThread(this.zah, "onConnectionFailure must only be called on the Handler thread");
        this.zah.removeMessages(1);
        synchronized (this.zai) {
            final ArrayList list = new ArrayList(this.zad);
            final int value = this.zaf.get();
            for (final GoogleApiClient.OnConnectionFailedListener o : list) {
                if (!this.zae || this.zaf.get() != value) {
                    return;
                }
                if (!this.zad.contains(o)) {
                    continue;
                }
                o.onConnectionFailed(connectionResult);
            }
        }
    }
    
    @VisibleForTesting
    public final void zad(final Bundle bundle) {
        Preconditions.checkHandlerThread(this.zah, "onConnectionSuccess must only be called on the Handler thread");
        synchronized (this.zai) {
            Preconditions.checkState(this.zag ^ true);
            this.zah.removeMessages(1);
            this.zag = true;
            Preconditions.checkState(this.zaa.isEmpty());
            final ArrayList list = new ArrayList(this.zac);
            final int value = this.zaf.get();
            for (final GoogleApiClient.ConnectionCallbacks o : list) {
                if (!this.zae || !this.zab.isConnected()) {
                    break;
                }
                if (this.zaf.get() != value) {
                    break;
                }
                if (this.zaa.contains(o)) {
                    continue;
                }
                o.onConnected(bundle);
            }
            this.zaa.clear();
            this.zag = false;
        }
    }
    
    @VisibleForTesting
    public final void zae(final int n) {
        Preconditions.checkHandlerThread(this.zah, "onUnintentionalDisconnection must only be called on the Handler thread");
        this.zah.removeMessages(1);
        synchronized (this.zai) {
            this.zag = true;
            final ArrayList list = new ArrayList(this.zac);
            final int value = this.zaf.get();
            for (final GoogleApiClient.ConnectionCallbacks o : list) {
                if (!this.zae) {
                    break;
                }
                if (this.zaf.get() != value) {
                    break;
                }
                if (!this.zac.contains(o)) {
                    continue;
                }
                o.onConnectionSuspended(n);
            }
            this.zaa.clear();
            this.zag = false;
        }
    }
    
    public final void zaf(final GoogleApiClient.ConnectionCallbacks e) {
        Preconditions.checkNotNull(e);
        Object o = this.zai;
        synchronized (o) {
            if (this.zac.contains(e)) {
                final String value = String.valueOf(e);
                final StringBuilder sb = new StringBuilder(value.length() + 62);
                sb.append("registerConnectionCallbacks(): listener ");
                sb.append(value);
                sb.append(" is already registered");
                Log.w("GmsClientEvents", sb.toString());
            }
            else {
                this.zac.add(e);
            }
            monitorexit(o);
            if (this.zab.isConnected()) {
                o = this.zah;
                ((Handler)o).sendMessage(((Handler)o).obtainMessage(1, (Object)e));
            }
        }
    }
    
    public final void zag(final GoogleApiClient.OnConnectionFailedListener e) {
        Preconditions.checkNotNull(e);
        synchronized (this.zai) {
            if (this.zad.contains(e)) {
                final String value = String.valueOf(e);
                final StringBuilder sb = new StringBuilder(value.length() + 67);
                sb.append("registerConnectionFailedListener(): listener ");
                sb.append(value);
                sb.append(" is already registered");
                Log.w("GmsClientEvents", sb.toString());
            }
            else {
                this.zad.add(e);
            }
        }
    }
    
    public final void zah(final GoogleApiClient.ConnectionCallbacks e) {
        Preconditions.checkNotNull(e);
        synchronized (this.zai) {
            if (!this.zac.remove(e)) {
                final String value = String.valueOf(e);
                final StringBuilder sb = new StringBuilder(value.length() + 52);
                sb.append("unregisterConnectionCallbacks(): listener ");
                sb.append(value);
                sb.append(" not found");
                Log.w("GmsClientEvents", sb.toString());
            }
            else if (this.zag) {
                this.zaa.add(e);
            }
        }
    }
    
    public final void zai(final GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        Preconditions.checkNotNull(onConnectionFailedListener);
        synchronized (this.zai) {
            if (!this.zad.remove(onConnectionFailedListener)) {
                final String value = String.valueOf(onConnectionFailedListener);
                final StringBuilder sb = new StringBuilder(value.length() + 57);
                sb.append("unregisterConnectionFailedListener(): listener ");
                sb.append(value);
                sb.append(" not found");
                Log.w("GmsClientEvents", sb.toString());
            }
        }
    }
    
    public final boolean zaj(final GoogleApiClient.ConnectionCallbacks o) {
        Preconditions.checkNotNull(o);
        synchronized (this.zai) {
            return this.zac.contains(o);
        }
    }
    
    public final boolean zak(final GoogleApiClient.OnConnectionFailedListener o) {
        Preconditions.checkNotNull(o);
        synchronized (this.zai) {
            return this.zad.contains(o);
        }
    }
}

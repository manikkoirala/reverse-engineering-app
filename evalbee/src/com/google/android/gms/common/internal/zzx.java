// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.os.IBinder;
import com.google.android.gms.internal.common.zza;

public final class zzx extends zza implements ICancelToken
{
    public zzx(final IBinder binder) {
        super(binder, "com.google.android.gms.common.internal.ICancelToken");
    }
    
    public final void cancel() {
        this.zzD(2, this.zza());
    }
}

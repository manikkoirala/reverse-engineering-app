// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.Bundle;
import android.os.Parcelable$Creator;
import com.google.android.gms.internal.common.zzc;
import android.os.Parcel;
import com.google.android.gms.internal.common.zzb;

public abstract class zzac extends zzb implements IGmsCallbacks
{
    public zzac() {
        super("com.google.android.gms.common.internal.IGmsCallbacks");
    }
    
    public final boolean zza(int n, final Parcel parcel, final Parcel parcel2, final int n2) {
        if (n != 1) {
            if (n != 2) {
                if (n != 3) {
                    return false;
                }
                n = parcel.readInt();
                final IBinder strongBinder = parcel.readStrongBinder();
                final zzk zzk = (zzk)zzc.zza(parcel, (Parcelable$Creator)com.google.android.gms.common.internal.zzk.CREATOR);
                zzc.zzb(parcel);
                this.zzc(n, strongBinder, zzk);
            }
            else {
                n = parcel.readInt();
                final Bundle bundle = (Bundle)zzc.zza(parcel, Bundle.CREATOR);
                zzc.zzb(parcel);
                this.zzb(n, bundle);
            }
        }
        else {
            n = parcel.readInt();
            final IBinder strongBinder2 = parcel.readStrongBinder();
            final Bundle bundle2 = (Bundle)zzc.zza(parcel, Bundle.CREATOR);
            zzc.zzb(parcel);
            this.onPostInitComplete(n, strongBinder2, bundle2);
        }
        parcel2.writeNoException();
        return true;
    }
}

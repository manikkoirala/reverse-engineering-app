// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.app.PendingIntent;
import com.google.android.gms.common.ConnectionResult;
import android.os.IInterface;
import android.os.Bundle;

abstract class zza extends zzc
{
    public final int zza;
    public final Bundle zzb;
    final BaseGmsClient zzc;
    
    public zza(final BaseGmsClient zzc, final int zza, final Bundle zzb) {
        super(this.zzc = zzc, Boolean.TRUE);
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public abstract void zzb(final ConnectionResult p0);
    
    @Override
    public final void zzc() {
    }
    
    public abstract boolean zzd();
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.os.IInterface;
import android.os.IBinder;
import com.google.android.gms.internal.common.zzb;

public abstract class zzaf extends zzb implements zzag
{
    public static zzag zzb(final IBinder binder) {
        if (binder == null) {
            return null;
        }
        final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.common.internal.IGoogleCertificatesApi");
        if (queryLocalInterface instanceof zzag) {
            return (zzag)queryLocalInterface;
        }
        return new zzae(binder);
    }
}

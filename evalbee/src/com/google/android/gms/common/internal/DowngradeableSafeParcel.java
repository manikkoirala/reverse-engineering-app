// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@KeepForSdk
public abstract class DowngradeableSafeParcel extends AbstractSafeParcelable implements ReflectedParcelable
{
    private static final Object zza;
    private boolean zzb;
    
    static {
        zza = new Object();
    }
    
    public DowngradeableSafeParcel() {
        this.zzb = false;
    }
    
    @KeepForSdk
    public static boolean canUnparcelSafely(final String s) {
        synchronized (DowngradeableSafeParcel.zza) {
            monitorexit(DowngradeableSafeParcel.zza);
            return true;
        }
    }
    
    @KeepForSdk
    public static Integer getUnparcelClientVersion() {
        synchronized (DowngradeableSafeParcel.zza) {
            monitorexit(DowngradeableSafeParcel.zza);
            return null;
        }
    }
    
    @KeepForSdk
    public abstract boolean prepareForClientVersion(final int p0);
    
    @KeepForSdk
    public void setShouldDowngrade(final boolean zzb) {
        this.zzb = zzb;
    }
    
    @KeepForSdk
    public boolean shouldDowngrade() {
        return this.zzb;
    }
}

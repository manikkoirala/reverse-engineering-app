// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@KeepForSdk
@Class(creator = "ConnectionTelemetryConfigurationCreator")
public class ConnectionTelemetryConfiguration extends AbstractSafeParcelable
{
    @KeepForSdk
    public static final Parcelable$Creator<ConnectionTelemetryConfiguration> CREATOR;
    @Field(getter = "getRootTelemetryConfiguration", id = 1)
    private final RootTelemetryConfiguration zza;
    @Field(getter = "getMethodInvocationTelemetryEnabled", id = 2)
    private final boolean zzb;
    @Field(getter = "getMethodTimingTelemetryEnabled", id = 3)
    private final boolean zzc;
    @Field(getter = "getMethodInvocationMethodKeyAllowlist", id = 4)
    private final int[] zzd;
    @Field(getter = "getMaxMethodInvocationsLogged", id = 5)
    private final int zze;
    @Field(getter = "getMethodInvocationMethodKeyDisallowlist", id = 6)
    private final int[] zzf;
    
    static {
        CREATOR = (Parcelable$Creator)new zzm();
    }
    
    @Constructor
    public ConnectionTelemetryConfiguration(@Param(id = 1) final RootTelemetryConfiguration zza, @Param(id = 2) final boolean zzb, @Param(id = 3) final boolean zzc, @Param(id = 4) final int[] zzd, @Param(id = 5) final int zze, @Param(id = 6) final int[] zzf) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
    }
    
    @KeepForSdk
    public int getMaxMethodInvocationsLogged() {
        return this.zze;
    }
    
    @KeepForSdk
    public int[] getMethodInvocationMethodKeyAllowlist() {
        return this.zzd;
    }
    
    @KeepForSdk
    public int[] getMethodInvocationMethodKeyDisallowlist() {
        return this.zzf;
    }
    
    @KeepForSdk
    public boolean getMethodInvocationTelemetryEnabled() {
        return this.zzb;
    }
    
    @KeepForSdk
    public boolean getMethodTimingTelemetryEnabled() {
        return this.zzc;
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeParcelable(parcel, 1, (Parcelable)this.zza, n, false);
        SafeParcelWriter.writeBoolean(parcel, 2, this.getMethodInvocationTelemetryEnabled());
        SafeParcelWriter.writeBoolean(parcel, 3, this.getMethodTimingTelemetryEnabled());
        SafeParcelWriter.writeIntArray(parcel, 4, this.getMethodInvocationMethodKeyAllowlist(), false);
        SafeParcelWriter.writeInt(parcel, 5, this.getMaxMethodInvocationsLogged());
        SafeParcelWriter.writeIntArray(parcel, 6, this.getMethodInvocationMethodKeyDisallowlist(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public final RootTelemetryConfiguration zza() {
        return this.zza;
    }
}

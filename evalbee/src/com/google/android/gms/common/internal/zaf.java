// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import com.google.android.gms.common.api.internal.LifecycleFragment;
import android.content.Intent;

final class zaf extends zag
{
    final Intent zaa;
    final LifecycleFragment zab;
    
    public zaf(final Intent zaa, final LifecycleFragment zab, final int n) {
        this.zaa = zaa;
        this.zab = zab;
    }
    
    @Override
    public final void zaa() {
        final Intent zaa = this.zaa;
        if (zaa != null) {
            this.zab.startActivityForResult(zaa, 2);
        }
    }
}

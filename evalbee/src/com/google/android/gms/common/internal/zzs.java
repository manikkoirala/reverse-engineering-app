// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.content.ServiceConnection;
import android.os.Handler$Callback;
import com.google.android.gms.internal.common.zzi;
import android.os.Looper;
import java.util.concurrent.Executor;
import com.google.android.gms.common.stats.ConnectionTracker;
import android.os.Handler;
import android.content.Context;
import java.util.HashMap;

final class zzs extends GmsClientSupervisor
{
    private final HashMap zzb;
    private final Context zzc;
    private volatile Handler zzd;
    private final zzr zze;
    private final ConnectionTracker zzf;
    private final long zzg;
    private final long zzh;
    private volatile Executor zzi;
    
    public zzs(final Context context, final Looper looper, final Executor zzi) {
        this.zzb = new HashMap();
        final zzr zze = new zzr(this, null);
        this.zze = zze;
        this.zzc = context.getApplicationContext();
        this.zzd = (Handler)new zzi(looper, (Handler$Callback)zze);
        this.zzf = ConnectionTracker.getInstance();
        this.zzg = 5000L;
        this.zzh = 300000L;
        this.zzi = zzi;
    }
    
    @Override
    public final void zza(final zzo key, final ServiceConnection serviceConnection, String s) {
        Preconditions.checkNotNull(serviceConnection, "ServiceConnection must not be null");
        synchronized (this.zzb) {
            final zzp zzp = this.zzb.get(key);
            if (zzp == null) {
                s = key.toString();
                final StringBuilder sb = new StringBuilder();
                sb.append("Nonexistent connection status for service config: ");
                sb.append(s);
                throw new IllegalStateException(sb.toString());
            }
            if (zzp.zzh(serviceConnection)) {
                zzp.zzf(serviceConnection, s);
                if (zzp.zzi()) {
                    this.zzd.sendMessageDelayed(this.zzd.obtainMessage(0, (Object)key), this.zzg);
                }
                return;
            }
            s = key.toString();
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Trying to unbind a GmsServiceConnection  that was not bound before.  config=");
            sb2.append(s);
            throw new IllegalStateException(sb2.toString());
        }
    }
    
    @Override
    public final boolean zzc(final zzo zzo, final ServiceConnection serviceConnection, final String s, final Executor executor) {
        Preconditions.checkNotNull(serviceConnection, "ServiceConnection must not be null");
        synchronized (this.zzb) {
            final zzp zzp = this.zzb.get(zzo);
            Executor zzi = executor;
            if (executor == null) {
                zzi = this.zzi;
            }
            zzp zzp2;
            if (zzp == null) {
                final zzp value = new zzp(this, zzo);
                value.zzd(serviceConnection, serviceConnection, s);
                value.zze(s, zzi);
                this.zzb.put(zzo, value);
                zzp2 = value;
            }
            else {
                this.zzd.removeMessages(0, (Object)zzo);
                if (zzp.zzh(serviceConnection)) {
                    final String string = zzo.toString();
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Trying to bind a GmsServiceConnection that was already connected before.  config=");
                    sb.append(string);
                    throw new IllegalStateException(sb.toString());
                }
                zzp.zzd(serviceConnection, serviceConnection, s);
                final int zza = zzp.zza();
                if (zza != 1) {
                    if (zza != 2) {
                        zzp2 = zzp;
                    }
                    else {
                        zzp.zze(s, zzi);
                        zzp2 = zzp;
                    }
                }
                else {
                    serviceConnection.onServiceConnected(zzp.zzb(), zzp.zzc());
                    zzp2 = zzp;
                }
            }
            return zzp2.zzj();
        }
    }
    
    public final void zzi(final Executor zzi) {
        synchronized (this.zzb) {
            this.zzi = zzi;
        }
    }
    
    public final void zzj(final Looper looper) {
        synchronized (this.zzb) {
            this.zzd = (Handler)new zzi(looper, (Handler$Callback)this.zze);
        }
    }
}

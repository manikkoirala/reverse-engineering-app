// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.util.Log;
import android.app.PendingIntent;
import android.os.IInterface;
import com.google.android.gms.common.ConnectionResult;
import android.os.Message;
import android.os.Looper;
import com.google.android.gms.internal.common.zzi;

final class zzb extends zzi
{
    final BaseGmsClient zza;
    
    public zzb(final BaseGmsClient zza, final Looper looper) {
        this.zza = zza;
        super(looper);
    }
    
    private static final void zza(final Message message) {
        final zzc zzc = (zzc)message.obj;
        zzc.zzc();
        zzc.zzg();
    }
    
    private static final boolean zzb(final Message message) {
        final int what = message.what;
        return what == 2 || what == 1 || what == 7;
    }
    
    public final void handleMessage(final Message message) {
        if (this.zza.zzd.get() != message.arg1) {
            if (zzb(message)) {
                zza(message);
            }
            return;
        }
        final int what = message.what;
        if ((what == 1 || what == 7 || (what == 4 && !this.zza.enableLocalFallback()) || message.what == 5) && !this.zza.isConnecting()) {
            zza(message);
            return;
        }
        final int what2 = message.what;
        PendingIntent pendingIntent = null;
        if (what2 == 4) {
            BaseGmsClient.zzg((BaseGmsClient<IInterface>)this.zza, new ConnectionResult(message.arg2));
            if (BaseGmsClient.zzo((BaseGmsClient<IInterface>)this.zza)) {
                final BaseGmsClient zza = this.zza;
                if (!BaseGmsClient.zzm((BaseGmsClient<IInterface>)zza)) {
                    BaseGmsClient.zzi((BaseGmsClient<IInterface>)zza, 3, (IInterface)null);
                    return;
                }
            }
            final BaseGmsClient zza2 = this.zza;
            ConnectionResult zza3;
            if (BaseGmsClient.zza((BaseGmsClient<IInterface>)zza2) != null) {
                zza3 = BaseGmsClient.zza((BaseGmsClient<IInterface>)zza2);
            }
            else {
                zza3 = new ConnectionResult(8);
            }
            this.zza.zzc.onReportServiceBinding(zza3);
            this.zza.onConnectionFailed(zza3);
            return;
        }
        if (what2 == 5) {
            final BaseGmsClient zza4 = this.zza;
            ConnectionResult zza5;
            if (BaseGmsClient.zza((BaseGmsClient<IInterface>)zza4) != null) {
                zza5 = BaseGmsClient.zza((BaseGmsClient<IInterface>)zza4);
            }
            else {
                zza5 = new ConnectionResult(8);
            }
            this.zza.zzc.onReportServiceBinding(zza5);
            this.zza.onConnectionFailed(zza5);
            return;
        }
        if (what2 == 3) {
            final Object obj = message.obj;
            if (obj instanceof PendingIntent) {
                pendingIntent = (PendingIntent)obj;
            }
            final ConnectionResult connectionResult = new ConnectionResult(message.arg2, pendingIntent);
            this.zza.zzc.onReportServiceBinding(connectionResult);
            this.zza.onConnectionFailed(connectionResult);
            return;
        }
        if (what2 == 6) {
            BaseGmsClient.zzi((BaseGmsClient<IInterface>)this.zza, 5, (IInterface)null);
            final BaseGmsClient zza6 = this.zza;
            if (BaseGmsClient.zzb((BaseGmsClient<IInterface>)zza6) != null) {
                BaseGmsClient.zzb((BaseGmsClient<IInterface>)zza6).onConnectionSuspended(message.arg2);
            }
            this.zza.onConnectionSuspended(message.arg2);
            BaseGmsClient.zzn((BaseGmsClient<IInterface>)this.zza, 5, 1, (IInterface)null);
            return;
        }
        if (what2 == 2 && !this.zza.isConnected()) {
            zza(message);
            return;
        }
        if (zzb(message)) {
            ((zzc)message.obj).zze();
            return;
        }
        final int what3 = message.what;
        final StringBuilder sb = new StringBuilder();
        sb.append("Don't know how to handle message: ");
        sb.append(what3);
        Log.wtf("GmsClient", sb.toString(), (Throwable)new Exception());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Deprecated
@Class(creator = "ValidateAccountRequestCreator")
public final class zzak extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zzak> CREATOR;
    @VersionField(id = 1)
    final int zza;
    
    static {
        CREATOR = (Parcelable$Creator)new zzal();
    }
    
    @Constructor
    public zzak(@Param(id = 1) final int zza) {
        this.zza = zza;
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.zza);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

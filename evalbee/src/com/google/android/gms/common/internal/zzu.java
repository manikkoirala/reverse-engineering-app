// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.net.Uri;

public final class zzu
{
    private static final Uri zza;
    private static final Uri zzb;
    
    static {
        zzb = (zza = Uri.parse("https://plus.google.com/")).buildUpon().appendPath("circles").appendPath("find").build();
    }
}

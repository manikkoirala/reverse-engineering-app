// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import com.google.errorprone.annotations.ResultIgnorabilityUnspecified;
import android.content.ServiceConnection;
import android.content.ComponentName;
import android.os.Looper;
import android.content.Context;
import java.util.concurrent.Executor;
import com.google.android.gms.common.util.VisibleForTesting;
import android.os.HandlerThread;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public abstract class GmsClientSupervisor
{
    @VisibleForTesting
    static HandlerThread zza;
    private static final Object zzb;
    private static zzs zzc;
    private static Executor zzd;
    private static boolean zze = false;
    
    static {
        zzb = new Object();
    }
    
    @KeepForSdk
    public static int getDefaultBindFlags() {
        return 4225;
    }
    
    @KeepForSdk
    public static GmsClientSupervisor getInstance(final Context context) {
        synchronized (GmsClientSupervisor.zzb) {
            if (GmsClientSupervisor.zzc == null) {
                final Context applicationContext = context.getApplicationContext();
                Looper looper;
                if (GmsClientSupervisor.zze) {
                    looper = getOrStartHandlerThread().getLooper();
                }
                else {
                    looper = context.getMainLooper();
                }
                GmsClientSupervisor.zzc = new zzs(applicationContext, looper, GmsClientSupervisor.zzd);
            }
            return GmsClientSupervisor.zzc;
        }
    }
    
    @KeepForSdk
    public static HandlerThread getOrStartHandlerThread() {
        synchronized (GmsClientSupervisor.zzb) {
            final HandlerThread zza = GmsClientSupervisor.zza;
            if (zza != null) {
                return zza;
            }
            ((Thread)(GmsClientSupervisor.zza = new HandlerThread("GoogleApiHandler", 9))).start();
            return GmsClientSupervisor.zza;
        }
    }
    
    @KeepForSdk
    public static void setDefaultBindExecutor(final Executor zzd) {
        synchronized (GmsClientSupervisor.zzb) {
            final zzs zzc = GmsClientSupervisor.zzc;
            if (zzc != null) {
                zzc.zzi(zzd);
            }
            GmsClientSupervisor.zzd = zzd;
        }
    }
    
    @KeepForSdk
    public static void setUseHandlerThreadForCallbacks() {
        synchronized (GmsClientSupervisor.zzb) {
            final zzs zzc = GmsClientSupervisor.zzc;
            if (zzc != null && !GmsClientSupervisor.zze) {
                zzc.zzj(getOrStartHandlerThread().getLooper());
            }
            GmsClientSupervisor.zze = true;
        }
    }
    
    @KeepForSdk
    public boolean bindService(final ComponentName componentName, final ServiceConnection serviceConnection, final String s) {
        return this.zzc(new zzo(componentName, 4225), serviceConnection, s, null);
    }
    
    @KeepForSdk
    public boolean bindService(final ComponentName componentName, final ServiceConnection serviceConnection, final String s, final Executor executor) {
        return this.zzc(new zzo(componentName, 4225), serviceConnection, s, executor);
    }
    
    @ResultIgnorabilityUnspecified
    @KeepForSdk
    public boolean bindService(final String s, final ServiceConnection serviceConnection, final String s2) {
        return this.zzc(new zzo(s, 4225, false), serviceConnection, s2, null);
    }
    
    @KeepForSdk
    public void unbindService(final ComponentName componentName, final ServiceConnection serviceConnection, final String s) {
        this.zza(new zzo(componentName, 4225), serviceConnection, s);
    }
    
    @KeepForSdk
    public void unbindService(final String s, final ServiceConnection serviceConnection, final String s2) {
        this.zza(new zzo(s, 4225, false), serviceConnection, s2);
    }
    
    public abstract void zza(final zzo p0, final ServiceConnection p1, final String p2);
    
    public final void zzb(final String s, final String s2, final int n, final ServiceConnection serviceConnection, final String s3, final boolean b) {
        this.zza(new zzo(s, s2, 4225, b), serviceConnection, s3);
    }
    
    public abstract boolean zzc(final zzo p0, final ServiceConnection p1, final String p2, final Executor p3);
}

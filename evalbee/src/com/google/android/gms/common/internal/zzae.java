// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.os.IInterface;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.common.zzs;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import android.os.Parcelable;
import com.google.android.gms.internal.common.zzc;
import com.google.android.gms.common.zzq;
import com.google.android.gms.common.zzo;
import android.os.IBinder;
import com.google.android.gms.internal.common.zza;

public final class zzae extends zza implements zzag
{
    public zzae(final IBinder binder) {
        super(binder, "com.google.android.gms.common.internal.IGoogleCertificatesApi");
    }
    
    public final zzq zze(final zzo zzo) {
        final Parcel zza = this.zza();
        zzc.zzc(zza, (Parcelable)zzo);
        final Parcel zzB = this.zzB(6, zza);
        final zzq zzq = (zzq)zzc.zza(zzB, (Parcelable$Creator)com.google.android.gms.common.zzq.CREATOR);
        zzB.recycle();
        return zzq;
    }
    
    public final zzq zzf(final zzo zzo) {
        final Parcel zza = this.zza();
        zzc.zzc(zza, (Parcelable)zzo);
        final Parcel zzB = this.zzB(8, zza);
        final zzq zzq = (zzq)zzc.zza(zzB, (Parcelable$Creator)com.google.android.gms.common.zzq.CREATOR);
        zzB.recycle();
        return zzq;
    }
    
    public final boolean zzg() {
        final Parcel zzB = this.zzB(9, this.zza());
        final boolean zzf = zzc.zzf(zzB);
        zzB.recycle();
        return zzf;
    }
    
    public final boolean zzh(final zzs zzs, final IObjectWrapper objectWrapper) {
        final Parcel zza = this.zza();
        zzc.zzc(zza, (Parcelable)zzs);
        zzc.zze(zza, (IInterface)objectWrapper);
        final Parcel zzB = this.zzB(5, zza);
        final boolean zzf = zzc.zzf(zzB);
        zzB.recycle();
        return zzf;
    }
    
    public final boolean zzi() {
        final Parcel zzB = this.zzB(7, this.zza());
        final boolean zzf = zzc.zzf(zzB);
        zzB.recycle();
        return zzf;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal.service;

import android.os.Bundle;
import com.google.android.gms.internal.base.zad;
import com.google.android.gms.common.Feature;
import android.os.IInterface;
import android.os.IBinder;
import com.google.android.gms.common.api.internal.OnConnectionFailedListener;
import com.google.android.gms.common.api.internal.ConnectionCallbacks;
import com.google.android.gms.common.internal.ClientSettings;
import android.os.Looper;
import android.content.Context;
import com.google.android.gms.common.internal.TelemetryLoggingOptions;
import com.google.android.gms.common.internal.GmsClient;

public final class zap extends GmsClient<zai>
{
    private final TelemetryLoggingOptions zaa;
    
    public zap(final Context context, final Looper looper, final ClientSettings clientSettings, final TelemetryLoggingOptions zaa, final ConnectionCallbacks connectionCallbacks, final OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, 270, clientSettings, connectionCallbacks, onConnectionFailedListener);
        this.zaa = zaa;
    }
    
    @Override
    public final Feature[] getApiFeatures() {
        return com.google.android.gms.internal.base.zad.zab;
    }
    
    @Override
    public final Bundle getGetServiceRequestExtraArgs() {
        return this.zaa.zaa();
    }
    
    @Override
    public final int getMinApkVersion() {
        return 203400000;
    }
    
    @Override
    public final String getServiceDescriptor() {
        return "com.google.android.gms.common.internal.service.IClientTelemetryService";
    }
    
    @Override
    public final String getStartServiceAction() {
        return "com.google.android.gms.common.telemetry.service.START";
    }
    
    @Override
    public final boolean getUseDynamicLookup() {
        return true;
    }
}

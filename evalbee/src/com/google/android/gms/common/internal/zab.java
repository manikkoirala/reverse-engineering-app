// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import java.util.Collections;
import com.google.android.gms.common.api.Scope;
import java.util.Set;

public final class zab
{
    public final Set<Scope> zaa;
    
    public zab(final Set<Scope> s) {
        Preconditions.checkNotNull(s);
        this.zaa = Collections.unmodifiableSet((Set<? extends Scope>)s);
    }
}

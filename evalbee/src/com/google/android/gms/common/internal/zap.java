// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import java.util.concurrent.TimeUnit;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.common.api.PendingResult;

final class zap implements StatusListener
{
    final PendingResult zaa;
    final TaskCompletionSource zab;
    final PendingResultUtil.ResultConverter zac;
    final zas zad;
    
    public zap(final PendingResult zaa, final TaskCompletionSource zab, final PendingResultUtil.ResultConverter zac, final zas zad) {
        this.zaa = zaa;
        this.zab = zab;
        this.zac = zac;
        this.zad = zad;
    }
    
    @Override
    public final void onComplete(final Status status) {
        if (status.isSuccess()) {
            this.zab.setResult(this.zac.convert(this.zaa.await(0L, TimeUnit.MILLISECONDS)));
            return;
        }
        this.zab.setException((Exception)ApiExceptionUtil.fromStatus(status));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.app.Activity;
import android.content.Intent;

final class zad extends zag
{
    final Intent zaa;
    final Activity zab;
    final int zac;
    
    public zad(final Intent zaa, final Activity zab, final int zac) {
        this.zaa = zaa;
        this.zab = zab;
        this.zac = zac;
    }
    
    @Override
    public final void zaa() {
        final Intent zaa = this.zaa;
        if (zaa != null) {
            this.zab.startActivityForResult(zaa, this.zac);
        }
    }
}

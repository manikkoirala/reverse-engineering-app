// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal.service;

import android.os.Parcel;
import android.os.IInterface;
import com.google.android.gms.internal.base.zac;
import android.os.IBinder;
import com.google.android.gms.internal.base.zaa;

public final class zal extends zaa
{
    public zal(final IBinder binder) {
        super(binder, "com.google.android.gms.common.internal.service.ICommonService");
    }
    
    public final void zae(final zak zak) {
        final Parcel zaa = this.zaa();
        zac.zad(zaa, (IInterface)zak);
        this.zad(1, zaa);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import org.checkerframework.checker.nullness.qual.EnsuresNonNull;
import android.text.TextUtils;
import com.google.android.gms.common.util.zzb;
import android.os.Looper;
import android.os.Handler;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public final class Preconditions
{
    private Preconditions() {
        throw new AssertionError((Object)"Uninstantiable");
    }
    
    @KeepForSdk
    public static void checkArgument(final boolean b) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException();
    }
    
    @KeepForSdk
    public static void checkArgument(final boolean b, final Object obj) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(String.valueOf(obj));
    }
    
    @KeepForSdk
    public static void checkArgument(final boolean b, final String format, final Object... args) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(String.format(format, args));
    }
    
    @KeepForSdk
    public static double checkArgumentInRange(final double n, final double n2, final double n3, final String s) {
        if (n < n2) {
            throw new IllegalArgumentException(zza("%s is out of range of [%f, %f] (too low)", s, n2, n3));
        }
        if (n <= n3) {
            return n;
        }
        throw new IllegalArgumentException(zza("%s is out of range of [%f, %f] (too high)", s, n2, n3));
    }
    
    @KeepForSdk
    public static float checkArgumentInRange(final float n, final float n2, final float n3, final String s) {
        if (n < n2) {
            throw new IllegalArgumentException(zza("%s is out of range of [%f, %f] (too low)", s, n2, n3));
        }
        if (n <= n3) {
            return n;
        }
        throw new IllegalArgumentException(zza("%s is out of range of [%f, %f] (too high)", s, n2, n3));
    }
    
    @KeepForSdk
    public static int checkArgumentInRange(final int n, final int n2, final int n3, final String s) {
        if (n < n2) {
            throw new IllegalArgumentException(zza("%s is out of range of [%d, %d] (too low)", s, n2, n3));
        }
        if (n <= n3) {
            return n;
        }
        throw new IllegalArgumentException(zza("%s is out of range of [%d, %d] (too high)", s, n2, n3));
    }
    
    @KeepForSdk
    public static long checkArgumentInRange(final long n, final long n2, final long n3, final String s) {
        if (n < n2) {
            throw new IllegalArgumentException(zza("%s is out of range of [%d, %d] (too low)", s, n2, n3));
        }
        if (n <= n3) {
            return n;
        }
        throw new IllegalArgumentException(zza("%s is out of range of [%d, %d] (too high)", s, n2, n3));
    }
    
    @KeepForSdk
    public static void checkHandlerThread(final Handler handler) {
        final Looper myLooper = Looper.myLooper();
        if (myLooper != handler.getLooper()) {
            String name;
            if (myLooper != null) {
                name = myLooper.getThread().getName();
            }
            else {
                name = "null current looper";
            }
            final String name2 = handler.getLooper().getThread().getName();
            final StringBuilder sb = new StringBuilder();
            sb.append("Must be called on ");
            sb.append(name2);
            sb.append(" thread, but got ");
            sb.append(name);
            sb.append(".");
            throw new IllegalStateException(sb.toString());
        }
    }
    
    @KeepForSdk
    public static void checkHandlerThread(final Handler handler, final String s) {
        if (Looper.myLooper() == handler.getLooper()) {
            return;
        }
        throw new IllegalStateException(s);
    }
    
    @KeepForSdk
    public static void checkMainThread() {
        checkMainThread("Must be called on the main application thread");
    }
    
    @KeepForSdk
    public static void checkMainThread(final String s) {
        if (zzb.zza()) {
            return;
        }
        throw new IllegalStateException(s);
    }
    
    @EnsuresNonNull({ "#1" })
    @KeepForSdk
    public static String checkNotEmpty(final String s) {
        if (!TextUtils.isEmpty((CharSequence)s)) {
            return s;
        }
        throw new IllegalArgumentException("Given String is empty or null");
    }
    
    @EnsuresNonNull({ "#1" })
    @KeepForSdk
    public static String checkNotEmpty(final String s, final Object obj) {
        if (!TextUtils.isEmpty((CharSequence)s)) {
            return s;
        }
        throw new IllegalArgumentException(String.valueOf(obj));
    }
    
    @KeepForSdk
    public static void checkNotMainThread() {
        checkNotMainThread("Must not be called on the main application thread");
    }
    
    @KeepForSdk
    public static void checkNotMainThread(final String s) {
        if (!zzb.zza()) {
            return;
        }
        throw new IllegalStateException(s);
    }
    
    @EnsuresNonNull({ "#1" })
    @KeepForSdk
    public static <T> T checkNotNull(final T t) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException("null reference");
    }
    
    @EnsuresNonNull({ "#1" })
    @KeepForSdk
    public static <T> T checkNotNull(final T t, final Object obj) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(String.valueOf(obj));
    }
    
    @KeepForSdk
    public static int checkNotZero(final int n) {
        if (n != 0) {
            return n;
        }
        throw new IllegalArgumentException("Given Integer is zero");
    }
    
    @KeepForSdk
    public static int checkNotZero(final int n, final Object obj) {
        if (n != 0) {
            return n;
        }
        throw new IllegalArgumentException(String.valueOf(obj));
    }
    
    @KeepForSdk
    public static long checkNotZero(final long n) {
        if (n != 0L) {
            return n;
        }
        throw new IllegalArgumentException("Given Long is zero");
    }
    
    @KeepForSdk
    public static long checkNotZero(final long n, final Object obj) {
        if (n != 0L) {
            return n;
        }
        throw new IllegalArgumentException(String.valueOf(obj));
    }
    
    @KeepForSdk
    public static void checkState(final boolean b) {
        if (b) {
            return;
        }
        throw new IllegalStateException();
    }
    
    @KeepForSdk
    public static void checkState(final boolean b, final Object obj) {
        if (b) {
            return;
        }
        throw new IllegalStateException(String.valueOf(obj));
    }
    
    @KeepForSdk
    public static void checkState(final boolean b, final String format, final Object... args) {
        if (b) {
            return;
        }
        throw new IllegalStateException(String.format(format, args));
    }
    
    public static String zza(final String s, final Object... array) {
        final StringBuilder sb = new StringBuilder(s.length() + 48);
        int i = 0;
        int beginIndex = 0;
        while (i < 3) {
            final int index = s.indexOf("%s", beginIndex);
            if (index == -1) {
                break;
            }
            sb.append(s.substring(beginIndex, index));
            sb.append(array[i]);
            beginIndex = index + 2;
            ++i;
        }
        sb.append(s.substring(beginIndex));
        if (i < 3) {
            sb.append(" [");
            final int n = i + 1;
            sb.append(array[i]);
            for (int j = n; j < 3; ++j) {
                sb.append(", ");
                sb.append(array[j]);
            }
            sb.append("]");
        }
        return sb.toString();
    }
}

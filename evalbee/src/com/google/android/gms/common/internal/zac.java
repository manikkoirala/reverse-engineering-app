// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import com.google.android.gms.common.GooglePlayServicesUtil;
import android.util.Log;
import com.google.android.gms.common.util.DeviceProperties;
import android.content.res.Resources;
import com.google.android.gms.base.R;
import android.content.pm.PackageManager$NameNotFoundException;
import android.text.TextUtils;
import com.google.android.gms.common.wrappers.Wrappers;
import android.content.Context;
import java.util.Locale;

public final class zac
{
    private static final co1 zaa;
    private static Locale zab;
    
    static {
        zaa = new co1();
    }
    
    public static String zaa(final Context context) {
        final String packageName = context.getPackageName();
        try {
            return Wrappers.packageManager(context).getApplicationLabel(packageName).toString();
        }
        catch (final PackageManager$NameNotFoundException | NullPointerException ex) {
            final String name = context.getApplicationInfo().name;
            if (TextUtils.isEmpty((CharSequence)name)) {
                return packageName;
            }
            return name;
        }
    }
    
    public static String zab(final Context context) {
        return context.getResources().getString(R.string.common_google_play_services_notification_channel_name);
    }
    
    public static String zac(final Context context, int n) {
        final Resources resources = context.getResources();
        if (n != 1) {
            if (n != 2) {
                if (n != 3) {
                    n = 17039370;
                }
                else {
                    n = R.string.common_google_play_services_enable_button;
                }
            }
            else {
                n = R.string.common_google_play_services_update_button;
            }
        }
        else {
            n = R.string.common_google_play_services_install_button;
        }
        return resources.getString(n);
    }
    
    public static String zad(final Context context, final int n) {
        final Resources resources = context.getResources();
        final String zaa = zaa(context);
        if (n == 1) {
            return resources.getString(R.string.common_google_play_services_install_text, new Object[] { zaa });
        }
        if (n != 2) {
            if (n == 3) {
                return resources.getString(R.string.common_google_play_services_enable_text, new Object[] { zaa });
            }
            if (n == 5) {
                return zah(context, "common_google_play_services_invalid_account_text", zaa);
            }
            if (n == 7) {
                return zah(context, "common_google_play_services_network_error_text", zaa);
            }
            if (n == 9) {
                return resources.getString(R.string.common_google_play_services_unsupported_text, new Object[] { zaa });
            }
            if (n == 20) {
                return zah(context, "common_google_play_services_restricted_profile_text", zaa);
            }
            switch (n) {
                default: {
                    return resources.getString(com.google.android.gms.common.R.string.common_google_play_services_unknown_issue, new Object[] { zaa });
                }
                case 18: {
                    return resources.getString(R.string.common_google_play_services_updating_text, new Object[] { zaa });
                }
                case 17: {
                    return zah(context, "common_google_play_services_sign_in_failed_text", zaa);
                }
                case 16: {
                    return zah(context, "common_google_play_services_api_unavailable_text", zaa);
                }
            }
        }
        else {
            if (DeviceProperties.isWearableWithoutPlayStore(context)) {
                return resources.getString(R.string.common_google_play_services_wear_update_text);
            }
            return resources.getString(R.string.common_google_play_services_update_text, new Object[] { zaa });
        }
    }
    
    public static String zae(final Context context, final int n) {
        if (n != 6 && n != 19) {
            return zad(context, n);
        }
        return zah(context, "common_google_play_services_resolution_required_text", zaa(context));
    }
    
    public static String zaf(final Context context, final int n) {
        String s;
        if (n == 6) {
            s = zai(context, "common_google_play_services_resolution_required_title");
        }
        else {
            s = zag(context, n);
        }
        if (s == null) {
            return context.getResources().getString(R.string.common_google_play_services_notification_ticker);
        }
        return s;
    }
    
    public static String zag(final Context context, final int i) {
        final Resources resources = context.getResources();
        String string = null;
        switch (i) {
            default: {
                final StringBuilder sb = new StringBuilder(33);
                sb.append("Unexpected error code ");
                sb.append(i);
                string = sb.toString();
                break;
            }
            case 20: {
                Log.e("GoogleApiAvailability", "The current user profile is restricted and could not use authenticated features.");
                return zai(context, "common_google_play_services_restricted_profile_title");
            }
            case 17: {
                Log.e("GoogleApiAvailability", "The specified account could not be signed in.");
                return zai(context, "common_google_play_services_sign_in_failed_title");
            }
            case 16: {
                string = "One of the API components you attempted to connect to is not available.";
                break;
            }
            case 11: {
                string = "The application is not licensed to the user.";
                break;
            }
            case 10: {
                string = "Developer error occurred. Please see logs for detailed information";
                break;
            }
            case 9: {
                string = "Google Play services is invalid. Cannot recover.";
                break;
            }
            case 8: {
                string = "Internal error occurred. Please see logs for detailed information";
                break;
            }
            case 7: {
                Log.e("GoogleApiAvailability", "Network error occurred. Please retry request later.");
                return zai(context, "common_google_play_services_network_error_title");
            }
            case 5: {
                Log.e("GoogleApiAvailability", "An invalid account was specified when connecting. Please provide a valid account.");
                return zai(context, "common_google_play_services_invalid_account_title");
            }
            case 4:
            case 6:
            case 18: {
                return null;
            }
            case 3: {
                return resources.getString(R.string.common_google_play_services_enable_title);
            }
            case 2: {
                return resources.getString(R.string.common_google_play_services_update_title);
            }
            case 1: {
                return resources.getString(R.string.common_google_play_services_install_title);
            }
        }
        Log.e("GoogleApiAvailability", string);
        return null;
    }
    
    private static String zah(final Context context, String s, final String s2) {
        final Resources resources = context.getResources();
        String format;
        s = (format = zai(context, s));
        if (s == null) {
            format = resources.getString(com.google.android.gms.common.R.string.common_google_play_services_unknown_issue);
        }
        return String.format(resources.getConfiguration().locale, format, s2);
    }
    
    private static String zai(final Context context, final String s) {
        final co1 zaa = zac.zaa;
        synchronized (zaa) {
            final Locale c = hk.a(context.getResources().getConfiguration()).c(0);
            if (!c.equals(zac.zab)) {
                zaa.clear();
                zac.zab = c;
            }
            final String s2 = (String)zaa.get(s);
            if (s2 != null) {
                return s2;
            }
            final Resources remoteResource = GooglePlayServicesUtil.getRemoteResource(context);
            if (remoteResource == null) {
                return null;
            }
            final int identifier = remoteResource.getIdentifier(s, "string", "com.google.android.gms");
            if (identifier == 0) {
                String concat;
                if (s.length() != 0) {
                    concat = "Missing resource: ".concat(s);
                }
                else {
                    concat = new String("Missing resource: ");
                }
                Log.w("GoogleApiAvailability", concat);
                return null;
            }
            final String string = remoteResource.getString(identifier);
            if (TextUtils.isEmpty((CharSequence)string)) {
                String concat2;
                if (s.length() != 0) {
                    concat2 = "Got empty resource: ".concat(s);
                }
                else {
                    concat2 = new String("Got empty resource: ");
                }
                Log.w("GoogleApiAvailability", concat2);
                return null;
            }
            zaa.put(s, string);
            return string;
        }
    }
}

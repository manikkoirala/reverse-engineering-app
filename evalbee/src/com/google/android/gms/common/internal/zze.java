// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.os.Handler;
import android.os.Bundle;
import android.os.IInterface;
import android.os.IBinder;
import android.content.ComponentName;
import com.google.android.gms.common.util.VisibleForTesting;
import android.content.ServiceConnection;

@VisibleForTesting
public final class zze implements ServiceConnection
{
    final BaseGmsClient zza;
    private final int zzb;
    
    public zze(final BaseGmsClient zza, final int zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
    
    public final void onServiceConnected(final ComponentName componentName, final IBinder binder) {
        final BaseGmsClient zza = this.zza;
        if (binder == null) {
            BaseGmsClient.zzk((BaseGmsClient<IInterface>)zza, 16);
            return;
        }
        synchronized (BaseGmsClient.zzd((BaseGmsClient<IInterface>)zza)) {
            final BaseGmsClient zza2 = this.zza;
            final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
            IGmsServiceBroker gmsServiceBroker;
            if (queryLocalInterface != null && queryLocalInterface instanceof IGmsServiceBroker) {
                gmsServiceBroker = (IGmsServiceBroker)queryLocalInterface;
            }
            else {
                gmsServiceBroker = new zzad(binder);
            }
            BaseGmsClient.zzh((BaseGmsClient<IInterface>)zza2, gmsServiceBroker);
            monitorexit(BaseGmsClient.zzd((BaseGmsClient<IInterface>)zza));
            this.zza.zzl(0, null, this.zzb);
        }
    }
    
    public final void onServiceDisconnected(ComponentName componentName) {
        componentName = (ComponentName)BaseGmsClient.zzd((BaseGmsClient<IInterface>)this.zza);
        synchronized (componentName) {
            BaseGmsClient.zzh((BaseGmsClient<IInterface>)this.zza, (IGmsServiceBroker)null);
            monitorexit(componentName);
            componentName = (ComponentName)this.zza.zzb;
            ((Handler)componentName).sendMessage(((Handler)componentName).obtainMessage(6, this.zzb, 1));
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.Feature;
import android.os.Bundle;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "ConnectionInfoCreator")
public final class zzk extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zzk> CREATOR;
    @Field(id = 1)
    Bundle zza;
    @Field(id = 2)
    Feature[] zzb;
    @Field(defaultValue = "0", id = 3)
    int zzc;
    @Field(id = 4)
    ConnectionTelemetryConfiguration zzd;
    
    static {
        CREATOR = (Parcelable$Creator)new zzl();
    }
    
    public zzk() {
    }
    
    @Constructor
    public zzk(@Param(id = 1) final Bundle zza, @Param(id = 2) final Feature[] zzb, @Param(id = 3) final int zzc, @Param(id = 4) final ConnectionTelemetryConfiguration zzd) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeBundle(parcel, 1, this.zza, false);
        SafeParcelWriter.writeTypedArray(parcel, 2, this.zzb, n, false);
        SafeParcelWriter.writeInt(parcel, 3, this.zzc);
        SafeParcelWriter.writeParcelable(parcel, 4, (Parcelable)this.zzd, n, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

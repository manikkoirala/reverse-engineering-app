// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import java.io.Serializable;
import java.io.IOException;
import java.io.Closeable;
import com.google.android.gms.common.util.IOUtils;
import java.io.InputStream;
import java.util.Properties;
import com.google.android.gms.common.util.VisibleForTesting;
import java.util.concurrent.ConcurrentHashMap;
import com.google.android.gms.common.annotation.KeepForSdk;

@Deprecated
@KeepForSdk
public class LibraryVersion
{
    private static final GmsLogger zza;
    private static final LibraryVersion zzb;
    private final ConcurrentHashMap zzc;
    
    static {
        zza = new GmsLogger("LibraryVersion", "");
        zzb = new LibraryVersion();
    }
    
    @VisibleForTesting
    public LibraryVersion() {
        this.zzc = new ConcurrentHashMap();
    }
    
    @KeepForSdk
    public static LibraryVersion getInstance() {
        return LibraryVersion.zzb;
    }
    
    @Deprecated
    @KeepForSdk
    public String getVersion(final String s) {
        Preconditions.checkNotEmpty(s, "Please provide a valid libraryName");
        if (this.zzc.containsKey(s)) {
            return this.zzc.get(s);
        }
        final Properties properties = new Properties();
        Serializable s2 = null;
        Serializable property = null;
        Object zza = null;
        Object str = null;
        Label_0339: {
            Object resourceAsStream;
            try {
                try {
                    resourceAsStream = LibraryVersion.class.getResourceAsStream(String.format("/%s.properties", s));
                    Label_0162: {
                        if (resourceAsStream == null) {
                            break Label_0162;
                        }
                        property = s2;
                        try {
                            properties.load((InputStream)resourceAsStream);
                            property = s2;
                            str = (property = properties.getProperty("version", null));
                            zza = LibraryVersion.zza;
                            property = (Serializable)str;
                            property = (Serializable)str;
                            s2 = new StringBuilder();
                            property = (Serializable)str;
                            ((StringBuilder)s2).append(s);
                            property = (Serializable)str;
                            ((StringBuilder)s2).append(" version is ");
                            property = (Serializable)str;
                            ((StringBuilder)s2).append((String)str);
                            property = (Serializable)str;
                            ((GmsLogger)zza).v("LibraryVersion", ((StringBuilder)s2).toString());
                            property = (Serializable)str;
                            if (resourceAsStream == null) {
                                break Label_0339;
                            }
                            IOUtils.closeQuietly((Closeable)resourceAsStream);
                            property = s2;
                            final GmsLogger zza2 = LibraryVersion.zza;
                            property = s2;
                            zza = new(java.lang.StringBuilder.class)();
                            property = s2;
                            new StringBuilder();
                            property = s2;
                            ((StringBuilder)zza).append("Failed to get app version for libraryName: ");
                            property = s2;
                            ((StringBuilder)zza).append(s);
                            property = s2;
                            zza2.w("LibraryVersion", ((StringBuilder)zza).toString());
                        }
                        catch (final IOException s2) {}
                        finally {
                            property = (Serializable)resourceAsStream;
                        }
                    }
                }
                finally {}
            }
            catch (final IOException s2) {
                str = null;
                resourceAsStream = zza;
            }
            final GmsLogger zza3 = LibraryVersion.zza;
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed to get app version for libraryName: ");
            sb.append(s);
            zza3.e("LibraryVersion", sb.toString(), (Throwable)s2);
            if (resourceAsStream != null) {
                IOUtils.closeQuietly((Closeable)resourceAsStream);
            }
            property = (Serializable)str;
        }
        String value;
        if ((value = (String)property) == null) {
            LibraryVersion.zza.d("LibraryVersion", ".properties file is dropped during release process. Failure to read app version is expected during Google internal testing where locally-built libraries are used");
            value = "UNKNOWN";
        }
        this.zzc.put(s, value);
        return value;
        if (property != null) {
            IOUtils.closeQuietly((Closeable)property);
        }
    }
}

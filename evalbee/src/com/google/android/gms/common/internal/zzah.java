// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.os.BaseBundle;
import android.os.Bundle;
import com.google.android.gms.common.wrappers.PackageManagerWrapper;
import android.content.pm.PackageManager$NameNotFoundException;
import android.util.Log;
import com.google.android.gms.common.wrappers.Wrappers;
import android.content.Context;

public final class zzah
{
    private static final Object zza;
    private static boolean zzb;
    private static String zzc;
    private static int zzd;
    
    static {
        zza = new Object();
    }
    
    public static int zza(final Context context) {
        zzc(context);
        return zzah.zzd;
    }
    
    public static String zzb(final Context context) {
        zzc(context);
        return zzah.zzc;
    }
    
    private static void zzc(final Context context) {
        synchronized (zzah.zza) {
            if (zzah.zzb) {
                return;
            }
            zzah.zzb = true;
            final String packageName = context.getPackageName();
            final PackageManagerWrapper packageManager = Wrappers.packageManager(context);
            try {
                final Bundle metaData = packageManager.getApplicationInfo(packageName, 128).metaData;
                if (metaData == null) {
                    return;
                }
                zzah.zzc = ((BaseBundle)metaData).getString("com.google.app.id");
                zzah.zzd = ((BaseBundle)metaData).getInt("com.google.android.gms.version");
            }
            catch (final PackageManager$NameNotFoundException ex) {
                Log.wtf("MetadataValueReader", "This should never happen.", (Throwable)ex);
            }
        }
    }
}

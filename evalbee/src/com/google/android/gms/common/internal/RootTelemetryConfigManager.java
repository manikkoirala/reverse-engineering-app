// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public final class RootTelemetryConfigManager
{
    private static RootTelemetryConfigManager zza;
    private static final RootTelemetryConfiguration zzb;
    private RootTelemetryConfiguration zzc;
    
    static {
        zzb = new RootTelemetryConfiguration(0, false, false, 0, 0);
    }
    
    private RootTelemetryConfigManager() {
    }
    
    @KeepForSdk
    public static RootTelemetryConfigManager getInstance() {
        synchronized (RootTelemetryConfigManager.class) {
            if (RootTelemetryConfigManager.zza == null) {
                RootTelemetryConfigManager.zza = new RootTelemetryConfigManager();
            }
            return RootTelemetryConfigManager.zza;
        }
    }
    
    @KeepForSdk
    public RootTelemetryConfiguration getConfig() {
        return this.zzc;
    }
    
    @VisibleForTesting
    public final void zza(final RootTelemetryConfiguration zzc) {
        monitorenter(this);
        Label_0016: {
            if (zzc != null) {
                break Label_0016;
            }
            try {
                this.zzc = RootTelemetryConfigManager.zzb;
                return;
                final RootTelemetryConfiguration zzc2 = this.zzc;
                iftrue(Label_0049:)(zzc2 == null || zzc2.getVersion() < zzc.getVersion());
                return;
                Label_0049: {
                    this.zzc = zzc;
                }
            }
            finally {
                monitorexit(this);
            }
        }
    }
}

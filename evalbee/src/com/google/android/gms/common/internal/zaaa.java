// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.view.View;
import android.widget.TextView;
import android.graphics.drawable.Drawable;
import com.google.android.gms.common.util.DeviceProperties;
import android.text.method.TransformationMethod;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff$Mode;
import com.google.android.gms.base.R;
import android.graphics.Typeface;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.Button;

public final class zaaa extends Button
{
    public zaaa(final Context context, final AttributeSet set) {
        super(context, (AttributeSet)null, 16842824);
    }
    
    private static final int zab(final int i, final int n, final int n2, final int n3) {
        if (i == 0) {
            return n;
        }
        if (i == 1) {
            return n2;
        }
        if (i == 2) {
            return n3;
        }
        final StringBuilder sb = new StringBuilder(33);
        sb.append("Unknown color scheme: ");
        sb.append(i);
        throw new IllegalStateException(sb.toString());
    }
    
    public final void zaa(final Resources resources, int n, final int n2) {
        ((TextView)this).setTypeface(Typeface.DEFAULT_BOLD);
        ((TextView)this).setTextSize(14.0f);
        final int n3 = (int)(resources.getDisplayMetrics().density * 48.0f + 0.5f);
        ((TextView)this).setMinHeight(n3);
        ((TextView)this).setMinWidth(n3);
        final int common_google_signin_btn_icon_dark = R.drawable.common_google_signin_btn_icon_dark;
        final int common_google_signin_btn_icon_light = R.drawable.common_google_signin_btn_icon_light;
        int zab = zab(n2, common_google_signin_btn_icon_dark, common_google_signin_btn_icon_light, common_google_signin_btn_icon_light);
        final int common_google_signin_btn_text_dark = R.drawable.common_google_signin_btn_text_dark;
        final int common_google_signin_btn_text_light = R.drawable.common_google_signin_btn_text_light;
        final int zab2 = zab(n2, common_google_signin_btn_text_dark, common_google_signin_btn_text_light, common_google_signin_btn_text_light);
        if (n != 0 && n != 1) {
            if (n != 2) {
                final StringBuilder sb = new StringBuilder(32);
                sb.append("Unknown button size: ");
                sb.append(n);
                throw new IllegalStateException(sb.toString());
            }
        }
        else {
            zab = zab2;
        }
        final Drawable r = wu.r(resources.getDrawable(zab));
        wu.o(r, resources.getColorStateList(R.color.common_google_signin_btn_tint));
        wu.p(r, PorterDuff$Mode.SRC_ATOP);
        ((View)this).setBackgroundDrawable(r);
        final int common_google_signin_btn_text_dark2 = R.color.common_google_signin_btn_text_dark;
        final int common_google_signin_btn_text_light2 = R.color.common_google_signin_btn_text_light;
        ((TextView)this).setTextColor((ColorStateList)Preconditions.checkNotNull(resources.getColorStateList(zab(n2, common_google_signin_btn_text_dark2, common_google_signin_btn_text_light2, common_google_signin_btn_text_light2))));
        Label_0289: {
            if (n != 0) {
                if (n != 1) {
                    if (n == 2) {
                        ((TextView)this).setText((CharSequence)null);
                        break Label_0289;
                    }
                    final StringBuilder sb2 = new StringBuilder(32);
                    sb2.append("Unknown button size: ");
                    sb2.append(n);
                    throw new IllegalStateException(sb2.toString());
                }
                else {
                    n = R.string.common_signin_button_text_long;
                }
            }
            else {
                n = R.string.common_signin_button_text;
            }
            ((TextView)this).setText((CharSequence)resources.getString(n));
        }
        ((TextView)this).setTransformationMethod((TransformationMethod)null);
        if (DeviceProperties.isWearable(((View)this).getContext())) {
            ((TextView)this).setGravity(19);
        }
    }
}

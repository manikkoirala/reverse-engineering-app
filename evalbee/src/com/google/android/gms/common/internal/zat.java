// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import android.accounts.Account;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "ResolveAccountRequestCreator")
public final class zat extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zat> CREATOR;
    @VersionField(id = 1)
    final int zaa;
    @Field(getter = "getAccount", id = 2)
    private final Account zab;
    @Field(getter = "getSessionId", id = 3)
    private final int zac;
    @Field(getter = "getSignInAccountHint", id = 4)
    private final GoogleSignInAccount zad;
    
    static {
        CREATOR = (Parcelable$Creator)new zau();
    }
    
    @Constructor
    public zat(@Param(id = 1) final int zaa, @Param(id = 2) final Account zab, @Param(id = 3) final int zac, @Param(id = 4) final GoogleSignInAccount zad) {
        this.zaa = zaa;
        this.zab = zab;
        this.zac = zac;
        this.zad = zad;
    }
    
    public zat(final Account account, final int n, final GoogleSignInAccount googleSignInAccount) {
        this(2, account, n, googleSignInAccount);
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.zaa);
        SafeParcelWriter.writeParcelable(parcel, 2, (Parcelable)this.zab, n, false);
        SafeParcelWriter.writeInt(parcel, 3, this.zac);
        SafeParcelWriter.writeParcelable(parcel, 4, (Parcelable)this.zad, n, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

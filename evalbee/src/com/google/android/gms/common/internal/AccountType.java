// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public final class AccountType
{
    @KeepForSdk
    public static final String GOOGLE = "com.google";
    public static final String[] zza;
    
    static {
        zza = new String[] { "com.google", "com.google.work", "cn.google" };
    }
    
    private AccountType() {
    }
}

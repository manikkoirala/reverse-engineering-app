// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.common.api.Response;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public class PendingResultUtil
{
    private static final zas zaa;
    
    static {
        zaa = new zao();
    }
    
    @KeepForSdk
    public static <R extends Result, T extends Response<R>> Task<T> toResponseTask(final PendingResult<R> pendingResult, final T t) {
        return toTask(pendingResult, (ResultConverter<R, T>)new zaq(t));
    }
    
    @KeepForSdk
    public static <R extends Result, T> Task<T> toTask(final PendingResult<R> pendingResult, final ResultConverter<R, T> resultConverter) {
        final zas zaa = PendingResultUtil.zaa;
        final TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        pendingResult.addStatusListener((PendingResult.StatusListener)new zap(pendingResult, taskCompletionSource, (ResultConverter)resultConverter, zaa));
        return (Task<T>)taskCompletionSource.getTask();
    }
    
    @KeepForSdk
    public static <R extends Result> Task<Void> toVoidTask(final PendingResult<R> pendingResult) {
        return toTask(pendingResult, (ResultConverter<R, Void>)new zar());
    }
    
    @KeepForSdk
    public interface ResultConverter<R extends Result, T>
    {
        @KeepForSdk
        T convert(final R p0);
    }
}

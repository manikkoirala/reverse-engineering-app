// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import java.util.concurrent.ExecutorService;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public final class CallbackExecutor
{
    private CallbackExecutor() {
    }
    
    @KeepForSdk
    public static ExecutorService executorService() {
        return zzj.zza;
    }
}

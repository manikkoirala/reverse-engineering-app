// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import com.google.android.gms.common.util.concurrent.NamedThreadFactory;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import com.google.android.gms.internal.common.zzh;
import java.util.concurrent.ExecutorService;

final class zzj
{
    static final ExecutorService zza;
    
    static {
        zzh.zza();
        final ThreadPoolExecutor executor = new ThreadPoolExecutor(1, 1, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(), new NamedThreadFactory("CallbackExecutor"));
        executor.allowCoreThreadTimeOut(true);
        zza = Executors.unconfigurableExecutorService(executor);
    }
}

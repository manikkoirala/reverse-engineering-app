// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@KeepForSdk
@Class(creator = "MethodInvocationCreator")
public class MethodInvocation extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<MethodInvocation> CREATOR;
    @Field(getter = "getMethodKey", id = 1)
    private final int zaa;
    @Field(getter = "getResultStatusCode", id = 2)
    private final int zab;
    @Field(getter = "getConnectionResultStatusCode", id = 3)
    private final int zac;
    @Field(getter = "getStartTimeMillis", id = 4)
    private final long zad;
    @Field(getter = "getEndTimeMillis", id = 5)
    private final long zae;
    @Field(getter = "getCallingModuleId", id = 6)
    private final String zaf;
    @Field(getter = "getCallingEntryPoint", id = 7)
    private final String zag;
    @Field(defaultValue = "0", getter = "getServiceId", id = 8)
    private final int zah;
    @Field(defaultValue = "-1", getter = "getLatencyMillis", id = 9)
    private final int zai;
    
    static {
        CREATOR = (Parcelable$Creator)new zan();
    }
    
    @Deprecated
    @KeepForSdk
    public MethodInvocation(final int n, final int n2, final int n3, final long n4, final long n5, final String s, final String s2, final int n6) {
        this(n, n2, n3, n4, n5, s, s2, n6, -1);
    }
    
    @Constructor
    public MethodInvocation(@Param(id = 1) final int zaa, @Param(id = 2) final int zab, @Param(id = 3) final int zac, @Param(id = 4) final long zad, @Param(id = 5) final long zae, @Param(id = 6) final String zaf, @Param(id = 7) final String zag, @Param(id = 8) final int zah, @Param(id = 9) final int zai) {
        this.zaa = zaa;
        this.zab = zab;
        this.zac = zac;
        this.zad = zad;
        this.zae = zae;
        this.zaf = zaf;
        this.zag = zag;
        this.zah = zah;
        this.zai = zai;
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.zaa);
        SafeParcelWriter.writeInt(parcel, 2, this.zab);
        SafeParcelWriter.writeInt(parcel, 3, this.zac);
        SafeParcelWriter.writeLong(parcel, 4, this.zad);
        SafeParcelWriter.writeLong(parcel, 5, this.zae);
        SafeParcelWriter.writeString(parcel, 6, this.zaf, false);
        SafeParcelWriter.writeString(parcel, 7, this.zag, false);
        SafeParcelWriter.writeInt(parcel, 8, this.zah);
        SafeParcelWriter.writeInt(parcel, 9, this.zai);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

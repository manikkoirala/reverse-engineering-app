// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.os.BaseBundle;
import android.util.Log;
import android.os.Bundle;
import android.content.Intent;
import android.content.Context;
import android.net.Uri$Builder;
import android.content.ComponentName;
import android.net.Uri;

public final class zzo
{
    private static final Uri zza;
    private final String zzb;
    private final String zzc;
    private final ComponentName zzd;
    private final int zze;
    private final boolean zzf;
    
    static {
        zza = new Uri$Builder().scheme("content").authority("com.google.android.gms.chimera").build();
    }
    
    public zzo(final ComponentName zzd, final int n) {
        this.zzb = null;
        this.zzc = null;
        Preconditions.checkNotNull(zzd);
        this.zzd = zzd;
        this.zze = 4225;
        this.zzf = false;
    }
    
    public zzo(final String s, final int n, final boolean b) {
        this(s, "com.google.android.gms", 4225, false);
    }
    
    public zzo(final String zzb, final String zzc, final int n, final boolean zzf) {
        Preconditions.checkNotEmpty(zzb);
        this.zzb = zzb;
        Preconditions.checkNotEmpty(zzc);
        this.zzc = zzc;
        this.zzd = null;
        this.zze = 4225;
        this.zzf = zzf;
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof zzo)) {
            return false;
        }
        final zzo zzo = (zzo)o;
        return Objects.equal(this.zzb, zzo.zzb) && Objects.equal(this.zzc, zzo.zzc) && Objects.equal(this.zzd, zzo.zzd) && this.zzf == zzo.zzf;
    }
    
    @Override
    public final int hashCode() {
        return Objects.hashCode(this.zzb, this.zzc, this.zzd, 4225, this.zzf);
    }
    
    @Override
    public final String toString() {
        String s;
        if ((s = this.zzb) == null) {
            Preconditions.checkNotNull(this.zzd);
            s = this.zzd.flattenToString();
        }
        return s;
    }
    
    public final ComponentName zza() {
        return this.zzd;
    }
    
    public final Intent zzb(final Context context) {
        Intent setComponent;
        if (this.zzb != null) {
            final boolean zzf = this.zzf;
            Intent intent = null;
            final Intent intent2 = null;
            if (zzf) {
                final Bundle bundle = new Bundle();
                ((BaseBundle)bundle).putString("serviceActionBundleKey", this.zzb);
                Bundle call;
                try {
                    call = context.getContentResolver().call(zzo.zza, "serviceIntentCall", (String)null, bundle);
                }
                catch (final IllegalArgumentException ex) {
                    Log.w("ConnectionStatusConfig", "Dynamic intent resolution failed: ".concat(ex.toString()));
                    call = null;
                }
                Intent intent3;
                if (call == null) {
                    intent3 = intent2;
                }
                else {
                    intent3 = (Intent)call.getParcelable("serviceResponseIntentKey");
                }
                intent = intent3;
                if (intent3 == null) {
                    Log.w("ConnectionStatusConfig", "Dynamic lookup for intent failed for action: ".concat(String.valueOf(this.zzb)));
                    intent = intent3;
                }
            }
            if ((setComponent = intent) == null) {
                return new Intent(this.zzb).setPackage(this.zzc);
            }
        }
        else {
            setComponent = new Intent().setComponent(this.zzd);
        }
        return setComponent;
    }
    
    public final String zzc() {
        return this.zzc;
    }
}

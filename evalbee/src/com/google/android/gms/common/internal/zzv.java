// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

public final class zzv
{
    private final String zza;
    private final String zzb;
    private final boolean zzc;
    
    public zzv(final String zzb, final String zza, final boolean b, final int n, final boolean zzc) {
        this.zzb = zzb;
        this.zza = zza;
        this.zzc = zzc;
    }
    
    public final String zza() {
        return this.zzb;
    }
    
    public final String zzb() {
        return this.zza;
    }
    
    public final boolean zzc() {
        return this.zzc;
    }
}

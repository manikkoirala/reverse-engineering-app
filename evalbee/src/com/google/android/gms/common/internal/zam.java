// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.IInterface;
import com.google.android.gms.internal.base.zac;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.IBinder;
import com.google.android.gms.internal.base.zaa;

public final class zam extends zaa
{
    public zam(final IBinder binder) {
        super(binder, "com.google.android.gms.common.internal.ISignInButtonCreator");
    }
    
    public final IObjectWrapper zae(final IObjectWrapper objectWrapper, final zax zax) {
        final Parcel zaa = this.zaa();
        zac.zad(zaa, (IInterface)objectWrapper);
        zac.zac(zaa, (Parcelable)zax);
        final Parcel zab = this.zab(2, zaa);
        final IObjectWrapper interface1 = IObjectWrapper.Stub.asInterface(zab.readStrongBinder());
        zab.recycle();
        return interface1;
    }
}

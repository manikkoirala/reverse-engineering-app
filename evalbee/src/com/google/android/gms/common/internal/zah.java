// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.os.Bundle;
import com.google.android.gms.common.api.internal.ConnectionCallbacks;

final class zah implements BaseConnectionCallbacks
{
    final ConnectionCallbacks zaa;
    
    public zah(final ConnectionCallbacks zaa) {
        this.zaa = zaa;
    }
    
    @Override
    public final void onConnected(final Bundle bundle) {
        this.zaa.onConnected(bundle);
    }
    
    @Override
    public final void onConnectionSuspended(final int n) {
        this.zaa.onConnectionSuspended(n);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal.service;

import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.common.api.internal.RemoteCall;
import com.google.android.gms.internal.base.zad;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.api.internal.TaskApiCall;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.common.internal.TelemetryData;
import android.content.Context;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.internal.TelemetryLoggingClient;
import com.google.android.gms.common.internal.TelemetryLoggingOptions;
import com.google.android.gms.common.api.GoogleApi;

public final class zao extends GoogleApi<TelemetryLoggingOptions> implements TelemetryLoggingClient
{
    public static final int zab = 0;
    private static final Api.ClientKey<zap> zac;
    private static final Api.AbstractClientBuilder<zap, TelemetryLoggingOptions> zad;
    private static final Api<TelemetryLoggingOptions> zae;
    
    static {
        zae = new Api<TelemetryLoggingOptions>("ClientTelemetry.API", zad = new zan(), zac = new Api.ClientKey());
    }
    
    public zao(final Context context, final TelemetryLoggingOptions telemetryLoggingOptions) {
        super(context, (Api<Api.ApiOptions>)zao.zae, (Api.ApiOptions)telemetryLoggingOptions, Settings.DEFAULT_SETTINGS);
    }
    
    @Override
    public final Task<Void> log(final TelemetryData telemetryData) {
        final TaskApiCall.Builder<Api.AnyClient, Object> builder = TaskApiCall.builder();
        builder.setFeatures(com.google.android.gms.internal.base.zad.zaa);
        builder.setAutoResolveMissingFeatures(false);
        builder.run(new zam(telemetryData));
        return this.doBestEffortWrite((TaskApiCall<Api.AnyClient, Void>)builder.build());
    }
}

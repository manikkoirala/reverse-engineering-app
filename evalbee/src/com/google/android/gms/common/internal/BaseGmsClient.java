// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import android.content.Intent;
import java.util.Collections;
import android.os.DeadObjectException;
import android.os.RemoteException;
import com.google.android.gms.common.api.Scope;
import java.util.Set;
import java.util.concurrent.Executor;
import android.accounts.Account;
import com.google.android.gms.common.api.CommonStatusCodes;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.io.PrintWriter;
import java.io.FileDescriptor;
import android.os.IBinder;
import android.app.PendingIntent;
import android.os.Bundle;
import android.content.ServiceConnection;
import android.util.Log;
import android.text.TextUtils;
import java.util.ArrayList;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import android.os.Looper;
import android.content.Context;
import java.util.concurrent.atomic.AtomicInteger;
import android.os.Handler;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.annotation.KeepForSdk;
import android.os.IInterface;

@KeepForSdk
public abstract class BaseGmsClient<T extends IInterface>
{
    @KeepForSdk
    public static final int CONNECT_STATE_CONNECTED = 4;
    @KeepForSdk
    public static final int CONNECT_STATE_DISCONNECTED = 1;
    @KeepForSdk
    public static final int CONNECT_STATE_DISCONNECTING = 5;
    @KeepForSdk
    public static final String DEFAULT_ACCOUNT = "<<default account>>";
    @KeepForSdk
    public static final String[] GOOGLE_PLUS_REQUIRED_FEATURES;
    @KeepForSdk
    public static final String KEY_PENDING_INTENT = "pendingIntent";
    private static final Feature[] zze;
    private volatile String zzA;
    private ConnectionResult zzB;
    private boolean zzC;
    private volatile zzk zzD;
    @VisibleForTesting
    zzv zza;
    final Handler zzb;
    @VisibleForTesting
    protected ConnectionProgressReportCallbacks zzc;
    @VisibleForTesting
    protected AtomicInteger zzd;
    private int zzf;
    private long zzg;
    private long zzh;
    private int zzi;
    private long zzj;
    private volatile String zzk;
    private final Context zzl;
    private final Looper zzm;
    private final GmsClientSupervisor zzn;
    private final GoogleApiAvailabilityLight zzo;
    private final Object zzp;
    private final Object zzq;
    private IGmsServiceBroker zzr;
    private IInterface zzs;
    private final ArrayList zzt;
    private zze zzu;
    private int zzv;
    private final BaseConnectionCallbacks zzw;
    private final BaseOnConnectionFailedListener zzx;
    private final int zzy;
    private final String zzz;
    
    static {
        zze = new Feature[0];
        GOOGLE_PLUS_REQUIRED_FEATURES = new String[] { "service_esmobile", "service_googleme" };
    }
    
    @KeepForSdk
    @VisibleForTesting
    public BaseGmsClient(final Context zzl, final Handler zzb, final GmsClientSupervisor zzn, final GoogleApiAvailabilityLight zzo, final int zzy, final BaseConnectionCallbacks zzw, final BaseOnConnectionFailedListener zzx) {
        this.zzk = null;
        this.zzp = new Object();
        this.zzq = new Object();
        this.zzt = new ArrayList();
        this.zzv = 1;
        this.zzB = null;
        this.zzC = false;
        this.zzD = null;
        this.zzd = new AtomicInteger(0);
        Preconditions.checkNotNull(zzl, "Context must not be null");
        this.zzl = zzl;
        Preconditions.checkNotNull(zzb, "Handler must not be null");
        this.zzb = zzb;
        this.zzm = zzb.getLooper();
        Preconditions.checkNotNull(zzn, "Supervisor must not be null");
        this.zzn = zzn;
        Preconditions.checkNotNull(zzo, "API availability must not be null");
        this.zzo = zzo;
        this.zzy = zzy;
        this.zzw = zzw;
        this.zzx = zzx;
        this.zzz = null;
    }
    
    @KeepForSdk
    public BaseGmsClient(final Context context, final Looper looper, final int n, final BaseConnectionCallbacks baseConnectionCallbacks, final BaseOnConnectionFailedListener baseOnConnectionFailedListener, final String s) {
        final GmsClientSupervisor instance = GmsClientSupervisor.getInstance(context);
        final GoogleApiAvailabilityLight instance2 = GoogleApiAvailabilityLight.getInstance();
        Preconditions.checkNotNull(baseConnectionCallbacks);
        Preconditions.checkNotNull(baseOnConnectionFailedListener);
        this(context, looper, instance, instance2, n, baseConnectionCallbacks, baseOnConnectionFailedListener, s);
    }
    
    @KeepForSdk
    @VisibleForTesting
    public BaseGmsClient(final Context zzl, final Looper zzm, final GmsClientSupervisor zzn, final GoogleApiAvailabilityLight zzo, final int zzy, final BaseConnectionCallbacks zzw, final BaseOnConnectionFailedListener zzx, final String zzz) {
        this.zzk = null;
        this.zzp = new Object();
        this.zzq = new Object();
        this.zzt = new ArrayList();
        this.zzv = 1;
        this.zzB = null;
        this.zzC = false;
        this.zzD = null;
        this.zzd = new AtomicInteger(0);
        Preconditions.checkNotNull(zzl, "Context must not be null");
        this.zzl = zzl;
        Preconditions.checkNotNull(zzm, "Looper must not be null");
        this.zzm = zzm;
        Preconditions.checkNotNull(zzn, "Supervisor must not be null");
        this.zzn = zzn;
        Preconditions.checkNotNull(zzo, "API availability must not be null");
        this.zzo = zzo;
        this.zzb = (Handler)new zzb(this, zzm);
        this.zzy = zzy;
        this.zzw = zzw;
        this.zzx = zzx;
        this.zzz = zzz;
    }
    
    private final void zzp(final int zzv, final IInterface zzs) {
        boolean b = false;
        if (zzv == 4 == (zzs != null)) {
            b = true;
        }
        Preconditions.checkArgument(b);
        synchronized (this.zzp) {
            this.zzv = zzv;
            this.zzs = zzs;
            if (zzv != 1) {
                if (zzv != 2 && zzv != 3) {
                    if (zzv == 4) {
                        Preconditions.checkNotNull(zzs);
                        this.onConnectedLocked(zzs);
                    }
                }
                else {
                    final zze zzu = this.zzu;
                    if (zzu != null) {
                        final zzv zza = this.zza;
                        if (zza != null) {
                            final String zzb = zza.zzb();
                            final String zza2 = zza.zza();
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Calling connect() while still connected, missing disconnect() for ");
                            sb.append(zzb);
                            sb.append(" on ");
                            sb.append(zza2);
                            Log.e("GmsClient", sb.toString());
                            final GmsClientSupervisor zzn = this.zzn;
                            final String zzb2 = this.zza.zzb();
                            Preconditions.checkNotNull(zzb2);
                            zzn.zzb(zzb2, this.zza.zza(), 4225, (ServiceConnection)zzu, this.zze(), this.zza.zzc());
                            this.zzd.incrementAndGet();
                        }
                    }
                    final zze zzu2 = new zze(this, this.zzd.get());
                    this.zzu = zzu2;
                    zzv zza3;
                    if (this.zzv == 3 && this.getLocalStartServiceAction() != null) {
                        zza3 = new zzv(this.getContext().getPackageName(), this.getLocalStartServiceAction(), true, 4225, false);
                    }
                    else {
                        zza3 = new zzv(this.getStartServicePackage(), this.getStartServiceAction(), false, 4225, this.getUseDynamicLookup());
                    }
                    this.zza = zza3;
                    if (zza3.zzc() && this.getMinApkVersion() < 17895000) {
                        throw new IllegalStateException("Internal Error, the minimum apk version of this BaseGmsClient is too low to support dynamic lookup. Start service action: ".concat(String.valueOf(this.zza.zzb())));
                    }
                    final GmsClientSupervisor zzn2 = this.zzn;
                    final String zzb3 = this.zza.zzb();
                    Preconditions.checkNotNull(zzb3);
                    if (!zzn2.zzc(new zzo(zzb3, this.zza.zza(), 4225, this.zza.zzc()), (ServiceConnection)zzu2, this.zze(), this.getBindServiceExecutor())) {
                        final String zzb4 = this.zza.zzb();
                        final String zza4 = this.zza.zza();
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("unable to connect to service: ");
                        sb2.append(zzb4);
                        sb2.append(" on ");
                        sb2.append(zza4);
                        Log.w("GmsClient", sb2.toString());
                        this.zzl(16, null, this.zzd.get());
                    }
                }
            }
            else {
                final zze zzu3 = this.zzu;
                if (zzu3 != null) {
                    final GmsClientSupervisor zzn3 = this.zzn;
                    final String zzb5 = this.zza.zzb();
                    Preconditions.checkNotNull(zzb5);
                    zzn3.zzb(zzb5, this.zza.zza(), 4225, (ServiceConnection)zzu3, this.zze(), this.zza.zzc());
                    this.zzu = null;
                }
            }
        }
    }
    
    @KeepForSdk
    public void checkAvailabilityAndConnect() {
        final int googlePlayServicesAvailable = this.zzo.isGooglePlayServicesAvailable(this.zzl, this.getMinApkVersion());
        if (googlePlayServicesAvailable != 0) {
            this.zzp(1, null);
            this.triggerNotAvailable((ConnectionProgressReportCallbacks)new LegacyClientCallbackAdapter(), googlePlayServicesAvailable, null);
            return;
        }
        this.connect((ConnectionProgressReportCallbacks)new LegacyClientCallbackAdapter());
    }
    
    @KeepForSdk
    public final void checkConnected() {
        if (this.isConnected()) {
            return;
        }
        throw new IllegalStateException("Not connected. Call connect() and wait for onConnected() to be called.");
    }
    
    @KeepForSdk
    public void connect(final ConnectionProgressReportCallbacks zzc) {
        Preconditions.checkNotNull(zzc, "Connection progress callbacks cannot be null.");
        this.zzc = zzc;
        this.zzp(2, null);
    }
    
    @KeepForSdk
    public abstract T createServiceInterface(final IBinder p0);
    
    @KeepForSdk
    public void disconnect() {
        this.zzd.incrementAndGet();
        synchronized (this.zzt) {
            for (int size = this.zzt.size(), i = 0; i < size; ++i) {
                ((zzc)this.zzt.get(i)).zzf();
            }
            this.zzt.clear();
            monitorexit(this.zzt);
            final Object zzq = this.zzq;
            synchronized (this.zzt) {
                this.zzr = null;
                monitorexit(this.zzt);
                this.zzp(1, null);
            }
        }
    }
    
    @KeepForSdk
    public void disconnect(final String zzk) {
        this.zzk = zzk;
        this.disconnect();
    }
    
    @KeepForSdk
    public void dump(final String s, FileDescriptor csq, final PrintWriter printWriter, final String[] array) {
        csq = (FileDescriptor)this.zzp;
        synchronized (csq) {
            final int zzv = this.zzv;
            final IInterface zzs = this.zzs;
            monitorexit(csq);
            csq = (FileDescriptor)this.zzq;
            synchronized (csq) {
                final IGmsServiceBroker zzr = this.zzr;
                monitorexit(csq);
                printWriter.append(s).append("mConnectState=");
                if (zzv != 1) {
                    if (zzv != 2) {
                        if (zzv != 3) {
                            if (zzv != 4) {
                                if (zzv != 5) {
                                    csq = (FileDescriptor)"UNKNOWN";
                                }
                                else {
                                    csq = (FileDescriptor)"DISCONNECTING";
                                }
                            }
                            else {
                                csq = (FileDescriptor)"CONNECTED";
                            }
                        }
                        else {
                            csq = (FileDescriptor)"LOCAL_CONNECTING";
                        }
                    }
                    else {
                        csq = (FileDescriptor)"REMOTE_CONNECTING";
                    }
                }
                else {
                    csq = (FileDescriptor)"DISCONNECTED";
                }
                printWriter.print((String)csq);
                printWriter.append(" mService=");
                if (zzs == null) {
                    printWriter.append("null");
                }
                else {
                    printWriter.append(this.getServiceDescriptor()).append("@").append(Integer.toHexString(System.identityHashCode(zzs.asBinder())));
                }
                printWriter.append(" mServiceBroker=");
                if (zzr == null) {
                    printWriter.println("null");
                }
                else {
                    printWriter.append("IGmsServiceBroker@").println(Integer.toHexString(System.identityHashCode(((IInterface)zzr).asBinder())));
                }
                final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
                if (this.zzh > 0L) {
                    final PrintWriter append = printWriter.append(s).append("lastConnectedTime=");
                    final long zzh = this.zzh;
                    csq = (FileDescriptor)simpleDateFormat.format(new Date(zzh));
                    final StringBuilder sb = new StringBuilder();
                    sb.append(zzh);
                    sb.append(" ");
                    sb.append((String)csq);
                    append.println(sb.toString());
                }
                if (this.zzg > 0L) {
                    printWriter.append(s).append("lastSuspendedCause=");
                    final int zzf = this.zzf;
                    if (zzf != 1) {
                        if (zzf != 2) {
                            if (zzf != 3) {
                                csq = (FileDescriptor)String.valueOf(zzf);
                            }
                            else {
                                csq = (FileDescriptor)"CAUSE_DEAD_OBJECT_EXCEPTION";
                            }
                        }
                        else {
                            csq = (FileDescriptor)"CAUSE_NETWORK_LOST";
                        }
                    }
                    else {
                        csq = (FileDescriptor)"CAUSE_SERVICE_DISCONNECTED";
                    }
                    printWriter.append((CharSequence)csq);
                    csq = (FileDescriptor)printWriter.append(" lastSuspendedTime=");
                    final long zzg = this.zzg;
                    final String format = simpleDateFormat.format(new Date(zzg));
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append(zzg);
                    sb2.append(" ");
                    sb2.append(format);
                    ((PrintWriter)csq).println(sb2.toString());
                }
                if (this.zzj > 0L) {
                    printWriter.append(s).append("lastFailedStatus=").append(CommonStatusCodes.getStatusCodeString(this.zzi));
                    final PrintWriter append2 = printWriter.append(" lastFailedTime=");
                    final long zzj = this.zzj;
                    final String format2 = simpleDateFormat.format(new Date(zzj));
                    csq = (FileDescriptor)new StringBuilder();
                    ((StringBuilder)csq).append(zzj);
                    ((StringBuilder)csq).append(" ");
                    ((StringBuilder)csq).append(format2);
                    append2.println(((StringBuilder)csq).toString());
                }
            }
        }
    }
    
    @KeepForSdk
    public boolean enableLocalFallback() {
        return false;
    }
    
    @KeepForSdk
    public Account getAccount() {
        return null;
    }
    
    @KeepForSdk
    public Feature[] getApiFeatures() {
        return BaseGmsClient.zze;
    }
    
    @KeepForSdk
    public final Feature[] getAvailableFeatures() {
        final zzk zzD = this.zzD;
        if (zzD == null) {
            return null;
        }
        return zzD.zzb;
    }
    
    @KeepForSdk
    public Executor getBindServiceExecutor() {
        return null;
    }
    
    @KeepForSdk
    public Bundle getConnectionHint() {
        return null;
    }
    
    @KeepForSdk
    public final Context getContext() {
        return this.zzl;
    }
    
    @KeepForSdk
    public String getEndpointPackageName() {
        if (this.isConnected()) {
            final zzv zza = this.zza;
            if (zza != null) {
                return zza.zza();
            }
        }
        throw new RuntimeException("Failed to connect when checking package");
    }
    
    @KeepForSdk
    public int getGCoreServiceId() {
        return this.zzy;
    }
    
    @KeepForSdk
    public Bundle getGetServiceRequestExtraArgs() {
        return new Bundle();
    }
    
    @KeepForSdk
    public String getLastDisconnectMessage() {
        return this.zzk;
    }
    
    @KeepForSdk
    public String getLocalStartServiceAction() {
        return null;
    }
    
    @KeepForSdk
    public final Looper getLooper() {
        return this.zzm;
    }
    
    @KeepForSdk
    public int getMinApkVersion() {
        return GoogleApiAvailabilityLight.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }
    
    @KeepForSdk
    public void getRemoteService(final IAccountAccessor ex, final Set<Scope> set) {
        final Bundle getServiceRequestExtraArgs = this.getGetServiceRequestExtraArgs();
        final int zzy = this.zzy;
        final String zzA = this.zzA;
        final int google_PLAY_SERVICES_VERSION_CODE = GoogleApiAvailabilityLight.GOOGLE_PLAY_SERVICES_VERSION_CODE;
        final Scope[] zza = GetServiceRequest.zza;
        final Bundle bundle = new Bundle();
        final Feature[] zzb = GetServiceRequest.zzb;
        final GetServiceRequest getServiceRequest = new GetServiceRequest(6, zzy, google_PLAY_SERVICES_VERSION_CODE, null, null, zza, bundle, null, zzb, zzb, true, 0, false, zzA);
        getServiceRequest.zzf = this.zzl.getPackageName();
        getServiceRequest.zzi = getServiceRequestExtraArgs;
        if (set != null) {
            getServiceRequest.zzh = (Scope[])set.toArray((Object[])new Scope[0]);
        }
        if (this.requiresSignIn()) {
            Account account;
            if ((account = this.getAccount()) == null) {
                account = new Account("<<default account>>", "com.google");
            }
            getServiceRequest.zzj = account;
            if (ex != null) {
                getServiceRequest.zzg = ((IInterface)ex).asBinder();
            }
        }
        else if (this.requiresAccount()) {
            getServiceRequest.zzj = this.getAccount();
        }
        getServiceRequest.zzk = BaseGmsClient.zze;
        getServiceRequest.zzl = this.getApiFeatures();
        if (this.usesClientTelemetry()) {
            getServiceRequest.zzo = true;
        }
        try {
            synchronized (this.zzq) {
                final IGmsServiceBroker zzr = this.zzr;
                if (zzr != null) {
                    zzr.getService(new zzd(this, this.zzd.get()), getServiceRequest);
                }
                else {
                    Log.w("GmsClient", "mServiceBroker is null, client disconnected");
                }
            }
        }
        catch (final RuntimeException ex) {
            goto Label_0286;
        }
        catch (final RemoteException ex2) {}
        catch (final SecurityException ex3) {
            throw ex3;
        }
        catch (final DeadObjectException ex4) {
            Log.w("GmsClient", "IGmsServiceBroker.getService failed", (Throwable)ex4);
            this.triggerConnectionSuspended(3);
        }
    }
    
    @KeepForSdk
    public Set<Scope> getScopes() {
        return Collections.emptySet();
    }
    
    @KeepForSdk
    public final T getService() {
        synchronized (this.zzp) {
            if (this.zzv != 5) {
                this.checkConnected();
                final IInterface zzs = this.zzs;
                Preconditions.checkNotNull(zzs, "Client is connected but service is null");
                return (T)zzs;
            }
            throw new DeadObjectException();
        }
    }
    
    @KeepForSdk
    public IBinder getServiceBrokerBinder() {
        synchronized (this.zzq) {
            final IGmsServiceBroker zzr = this.zzr;
            if (zzr == null) {
                return null;
            }
            return ((IInterface)zzr).asBinder();
        }
    }
    
    @KeepForSdk
    public abstract String getServiceDescriptor();
    
    @KeepForSdk
    public Intent getSignInIntent() {
        throw new UnsupportedOperationException("Not a sign in API");
    }
    
    @KeepForSdk
    public abstract String getStartServiceAction();
    
    @KeepForSdk
    public String getStartServicePackage() {
        return "com.google.android.gms";
    }
    
    @KeepForSdk
    public ConnectionTelemetryConfiguration getTelemetryConfiguration() {
        final zzk zzD = this.zzD;
        if (zzD == null) {
            return null;
        }
        return zzD.zzd;
    }
    
    @KeepForSdk
    public boolean getUseDynamicLookup() {
        return this.getMinApkVersion() >= 211700000;
    }
    
    @KeepForSdk
    public boolean hasConnectionInfo() {
        return this.zzD != null;
    }
    
    @KeepForSdk
    public boolean isConnected() {
        synchronized (this.zzp) {
            return this.zzv == 4;
        }
    }
    
    @KeepForSdk
    public boolean isConnecting() {
        synchronized (this.zzp) {
            final int zzv = this.zzv;
            boolean b = true;
            if (zzv != 2) {
                b = (zzv == 3 && b);
            }
            return b;
        }
    }
    
    @KeepForSdk
    public void onConnectedLocked(final T t) {
        this.zzh = System.currentTimeMillis();
    }
    
    @KeepForSdk
    public void onConnectionFailed(final ConnectionResult connectionResult) {
        this.zzi = connectionResult.getErrorCode();
        this.zzj = System.currentTimeMillis();
    }
    
    @KeepForSdk
    public void onConnectionSuspended(final int zzf) {
        this.zzf = zzf;
        this.zzg = System.currentTimeMillis();
    }
    
    @KeepForSdk
    public void onPostInitHandler(final int n, final IBinder binder, final Bundle bundle, final int n2) {
        final Handler zzb = this.zzb;
        zzb.sendMessage(zzb.obtainMessage(1, n2, -1, (Object)new zzf(this, n, binder, bundle)));
    }
    
    @KeepForSdk
    public void onUserSignOut(final SignOutCallbacks signOutCallbacks) {
        signOutCallbacks.onSignOutComplete();
    }
    
    @KeepForSdk
    public boolean providesSignIn() {
        return false;
    }
    
    @KeepForSdk
    public boolean requiresAccount() {
        return false;
    }
    
    @KeepForSdk
    public boolean requiresGooglePlayServices() {
        return true;
    }
    
    @KeepForSdk
    public boolean requiresSignIn() {
        return false;
    }
    
    @KeepForSdk
    public void setAttributionTag(final String zzA) {
        this.zzA = zzA;
    }
    
    @KeepForSdk
    public void triggerConnectionSuspended(final int n) {
        final Handler zzb = this.zzb;
        zzb.sendMessage(zzb.obtainMessage(6, this.zzd.get(), n));
    }
    
    @KeepForSdk
    @VisibleForTesting
    public void triggerNotAvailable(final ConnectionProgressReportCallbacks zzc, final int n, final PendingIntent pendingIntent) {
        Preconditions.checkNotNull(zzc, "Connection progress callbacks cannot be null.");
        this.zzc = zzc;
        final Handler zzb = this.zzb;
        zzb.sendMessage(zzb.obtainMessage(3, this.zzd.get(), n, (Object)pendingIntent));
    }
    
    @KeepForSdk
    public boolean usesClientTelemetry() {
        return false;
    }
    
    public final String zze() {
        String s;
        if ((s = this.zzz) == null) {
            s = this.zzl.getClass().getName();
        }
        return s;
    }
    
    public final void zzl(final int n, final Bundle bundle, final int n2) {
        final Handler zzb = this.zzb;
        zzb.sendMessage(zzb.obtainMessage(7, n2, -1, (Object)new zzg(this, n, null)));
    }
    
    @KeepForSdk
    public interface BaseConnectionCallbacks
    {
        @KeepForSdk
        public static final int CAUSE_DEAD_OBJECT_EXCEPTION = 3;
        @KeepForSdk
        public static final int CAUSE_SERVICE_DISCONNECTED = 1;
        
        @KeepForSdk
        void onConnected(final Bundle p0);
        
        @KeepForSdk
        void onConnectionSuspended(final int p0);
    }
    
    @KeepForSdk
    public interface BaseOnConnectionFailedListener
    {
        @KeepForSdk
        void onConnectionFailed(final ConnectionResult p0);
    }
    
    @KeepForSdk
    public interface ConnectionProgressReportCallbacks
    {
        @KeepForSdk
        void onReportServiceBinding(final ConnectionResult p0);
    }
    
    public class LegacyClientCallbackAdapter implements ConnectionProgressReportCallbacks
    {
        final BaseGmsClient zza;
        
        @KeepForSdk
        public LegacyClientCallbackAdapter(final BaseGmsClient zza) {
            this.zza = zza;
        }
        
        @Override
        public final void onReportServiceBinding(final ConnectionResult connectionResult) {
            if (connectionResult.isSuccess()) {
                final BaseGmsClient zza = this.zza;
                zza.getRemoteService(null, zza.getScopes());
                return;
            }
            if (BaseGmsClient.zzc(this.zza) != null) {
                BaseGmsClient.zzc(this.zza).onConnectionFailed(connectionResult);
            }
        }
    }
    
    @KeepForSdk
    public interface SignOutCallbacks
    {
        @KeepForSdk
        void onSignOutComplete();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common.internal;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.api.Scope;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "SignInButtonConfigCreator")
public final class zax extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zax> CREATOR;
    @VersionField(id = 1)
    final int zaa;
    @Field(getter = "getButtonSize", id = 2)
    private final int zab;
    @Field(getter = "getColorScheme", id = 3)
    private final int zac;
    @Deprecated
    @Field(getter = "getScopes", id = 4)
    private final Scope[] zad;
    
    static {
        CREATOR = (Parcelable$Creator)new zay();
    }
    
    @Constructor
    public zax(@Param(id = 1) final int zaa, @Param(id = 2) final int zab, @Param(id = 3) final int zac, @Param(id = 4) final Scope[] zad) {
        this.zaa = zaa;
        this.zab = zab;
        this.zac = zac;
        this.zad = zad;
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.zaa);
        SafeParcelWriter.writeInt(parcel, 2, this.zab);
        SafeParcelWriter.writeInt(parcel, 3, this.zac);
        SafeParcelWriter.writeTypedArray(parcel, 4, this.zad, n, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.common;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.dynamic.IObjectWrapper;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.common.internal.zzz;
import android.os.IBinder;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "GoogleCertificatesQueryCreator")
public final class zzs extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<zzs> CREATOR;
    @Field(getter = "getCallingPackage", id = 1)
    private final String zza;
    @Field(getter = "getCallingCertificateBinder", id = 2, type = "android.os.IBinder")
    private final zzj zzb;
    @Field(getter = "getAllowTestKeys", id = 3)
    private final boolean zzc;
    @Field(defaultValue = "false", getter = "getIgnoreTestKeysOverride", id = 4)
    private final boolean zzd;
    
    static {
        CREATOR = (Parcelable$Creator)new zzt();
    }
    
    @Constructor
    public zzs(@Param(id = 1) final String zza, @Param(id = 2) final IBinder binder, @Param(id = 3) final boolean zzc, @Param(id = 4) final boolean zzd) {
        this.zza = zza;
        final zzj zzj = null;
        zzj zzb;
        if (binder == null) {
            zzb = zzj;
        }
        else {
            try {
                final IObjectWrapper zzd2 = zzz.zzg(binder).zzd();
                byte[] array;
                if (zzd2 == null) {
                    array = null;
                }
                else {
                    array = ObjectWrapper.unwrap(zzd2);
                }
                if (array != null) {
                    zzb = new zzk(array);
                }
                else {
                    Log.e("GoogleCertificatesQuery", "Could not unwrap certificate");
                    zzb = zzj;
                }
            }
            catch (final RemoteException ex) {
                Log.e("GoogleCertificatesQuery", "Could not unwrap certificate", (Throwable)ex);
                zzb = zzj;
            }
        }
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
    }
    
    public zzs(final String zza, final zzj zzb, final boolean zzc, final boolean zzd) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.zza, false);
        Object zzb;
        if ((zzb = this.zzb) == null) {
            Log.w("GoogleCertificatesQuery", "certificate binder is null");
            zzb = null;
        }
        SafeParcelWriter.writeIBinder(parcel, 2, (IBinder)zzb, false);
        SafeParcelWriter.writeBoolean(parcel, 3, this.zzc);
        SafeParcelWriter.writeBoolean(parcel, 4, this.zzd);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

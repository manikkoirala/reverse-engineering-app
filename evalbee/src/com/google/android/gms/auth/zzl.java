// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth;

import android.os.BaseBundle;
import java.util.Iterator;
import com.google.android.gms.common.GoogleApiAvailability;
import android.os.SystemClock;
import android.text.TextUtils;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesIncorrectManifestValueException;
import com.google.android.gms.common.GooglePlayServicesUtilLight;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.CancellationException;
import com.google.android.gms.tasks.Tasks;
import android.util.Log;
import java.util.concurrent.TimeoutException;
import android.os.RemoteException;
import android.content.ServiceConnection;
import com.google.android.gms.common.internal.GmsClientSupervisor;
import com.google.android.gms.common.BlockingServiceConnection;
import android.os.Parcelable$Creator;
import java.net.URISyntaxException;
import com.google.android.gms.internal.auth.zzbw;
import java.io.IOException;
import com.google.android.gms.internal.auth.zze;
import android.os.IBinder;
import com.google.android.gms.internal.auth.zzby;
import android.content.Intent;
import android.accounts.AccountManager;
import android.accounts.Account;
import android.os.Bundle;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.internal.auth.zzh;
import com.google.android.gms.internal.auth.zzhs;
import com.google.android.gms.internal.auth.zzdc;
import com.google.android.gms.common.internal.Preconditions;
import java.util.List;
import android.content.Context;
import com.google.android.gms.common.logging.Logger;
import android.content.ComponentName;
import com.google.android.gms.common.internal.ShowFirstParty;

@ShowFirstParty
public class zzl
{
    public static final int CHANGE_TYPE_ACCOUNT_ADDED = 1;
    public static final int CHANGE_TYPE_ACCOUNT_REMOVED = 2;
    public static final int CHANGE_TYPE_ACCOUNT_RENAMED_FROM = 3;
    public static final int CHANGE_TYPE_ACCOUNT_RENAMED_TO = 4;
    public static final String GOOGLE_ACCOUNT_TYPE = "com.google";
    public static final String KEY_SUPPRESS_PROGRESS_SCREEN = "suppressProgressScreen";
    @ShowFirstParty
    public static final String WORK_ACCOUNT_TYPE = "com.google.work";
    @ShowFirstParty
    public static final String[] zza;
    @ShowFirstParty
    public static final String zzb;
    private static final ComponentName zzc;
    private static final Logger zzd;
    
    static {
        zza = new String[] { "com.google", "com.google.work", "cn.google" };
        zzb = "androidPackageName";
        zzc = new ComponentName("com.google.android.gms", "com.google.android.gms.auth.GetToken");
        zzd = com.google.android.gms.auth.zzd.zza("GoogleAuthUtil");
    }
    
    public static void clearToken(final Context context, final String s) {
        zze(context, s, 0L);
    }
    
    public static List<AccountChangeEvent> getAccountChangeEvents(final Context context, final int eventIndex, final String accountName) {
        Preconditions.checkNotEmpty(accountName, "accountName must be provided");
        Preconditions.checkNotMainThread("Calling this from your main thread can lead to deadlock");
        zzk(context, 8400000);
        final AccountChangeEventsRequest accountChangeEventsRequest = new AccountChangeEventsRequest();
        accountChangeEventsRequest.setAccountName(accountName);
        accountChangeEventsRequest.setEventIndex(eventIndex);
        zzdc.zze(context);
        if (zzhs.zzd() && zzo(context)) {
            final Task zzb = zzh.zza(context).zzb(accountChangeEventsRequest);
            try {
                final AccountChangeEventsResponse accountChangeEventsResponse = (AccountChangeEventsResponse)zzi(zzb, "account change events retrieval");
                zzj(accountChangeEventsResponse);
                return accountChangeEventsResponse.getEvents();
            }
            catch (final ApiException ex) {
                zzl(ex, "account change events retrieval");
            }
        }
        return (List)zzh(context, zzl.zzc, new zzi(accountChangeEventsRequest), 0L);
    }
    
    public static String getAccountId(final Context context, final String s) {
        Preconditions.checkNotEmpty(s, "accountName must be provided");
        Preconditions.checkNotMainThread("Calling this from your main thread can lead to deadlock");
        zzk(context, 8400000);
        return getToken(context, s, "^^_account_id_^^", new Bundle());
    }
    
    public static String getToken(final Context context, final Account account, final String s) {
        return getToken(context, account, s, new Bundle());
    }
    
    public static String getToken(final Context context, final Account account, final String s, final Bundle bundle) {
        zzn(account);
        return zza(context, account, s, bundle).zza();
    }
    
    @Deprecated
    public static String getToken(final Context context, final String s, final String s2) {
        return getToken(context, new Account(s, "com.google"), s2);
    }
    
    @Deprecated
    public static String getToken(final Context context, final String s, final String s2, final Bundle bundle) {
        return getToken(context, new Account(s, "com.google"), s2, bundle);
    }
    
    @Deprecated
    public static void invalidateToken(final Context context, final String s) {
        AccountManager.get(context).invalidateAuthToken("com.google", s);
    }
    
    @ShowFirstParty
    public static Bundle removeAccount(final Context context, final Account account) {
        Preconditions.checkNotNull(context);
        zzn(account);
        zzk(context, 8400000);
        zzdc.zze(context);
        if (zzhs.zze() && zzo(context)) {
            final Task zzd = zzh.zza(context).zzd(account);
            try {
                final Bundle bundle = (Bundle)zzi(zzd, "account removal");
                zzj(bundle);
                return bundle;
            }
            catch (final ApiException ex) {
                zzl(ex, "account removal");
            }
        }
        return (Bundle)zzh(context, zzl.zzc, new zzg(account), 0L);
    }
    
    public static Boolean requestGoogleAccountsAccess(final Context context) {
        Preconditions.checkNotNull(context);
        zzk(context, 11400000);
        final String packageName = context.getApplicationInfo().packageName;
        zzdc.zze(context);
        if (zzhs.zze() && zzo(context)) {
            final Task zze = zzh.zza(context).zze(packageName);
            try {
                final Bundle bundle = (Bundle)zzi(zze, "google accounts access request");
                final String string = ((BaseBundle)bundle).getString("Error");
                final Intent intent = (Intent)bundle.getParcelable("userRecoveryIntent");
                final zzby zza = zzby.zza(string);
                if (zzby.zzc.equals(zza)) {
                    return Boolean.TRUE;
                }
                if (zzby.zzb(zza)) {
                    final Logger zzd = zzl.zzd;
                    final String value = String.valueOf(zza);
                    final StringBuilder sb = new StringBuilder();
                    sb.append("isUserRecoverableError status: ");
                    sb.append(value);
                    zzd.w(sb.toString(), new Object[0]);
                    throw new UserRecoverableAuthException(string, intent);
                }
                throw new GoogleAuthException(string);
            }
            catch (final ApiException ex) {
                zzl(ex, "google accounts access request");
            }
        }
        return (Boolean)zzh(context, zzl.zzc, new zzj(packageName), 0L);
    }
    
    public static TokenData zza(final Context context, final Account account, final String s, Bundle bundle) {
        Preconditions.checkNotMainThread("Calling this from your main thread can lead to deadlock");
        Preconditions.checkNotEmpty(s, "Scope cannot be empty or null.");
        zzn(account);
        zzk(context, 8400000);
        if (bundle == null) {
            bundle = new Bundle();
        }
        else {
            bundle = new Bundle(bundle);
        }
        zzm(context, bundle);
        zzdc.zze(context);
        if (zzhs.zze() && zzo(context)) {
            final Task zzc = zzh.zza(context).zzc(account, s, bundle);
            try {
                final Bundle bundle2 = (Bundle)zzi(zzc, "token retrieval");
                zzj(bundle2);
                return zzg(bundle2);
            }
            catch (final ApiException ex) {
                zzl(ex, "token retrieval");
            }
        }
        return (TokenData)zzh(context, zzl.zzc, new zzf(account, s, bundle), 0L);
    }
    
    @ShowFirstParty
    public static void zze(final Context context, final String s, final long n) {
        Preconditions.checkNotMainThread("Calling this from your main thread can lead to deadlock");
        zzk(context, 8400000);
        final Bundle bundle = new Bundle();
        zzm(context, bundle);
        zzdc.zze(context);
        if (zzhs.zze() && zzo(context)) {
            final com.google.android.gms.internal.auth.zzg zza = zzh.zza(context);
            final zzbw zzbw = new zzbw();
            zzbw.zza(s);
            final Task zza2 = zza.zza(zzbw);
            try {
                zzi(zza2, "clear token");
                return;
            }
            catch (final ApiException ex) {
                zzl(ex, "clear token");
            }
        }
        zzh(context, zzl.zzc, new com.google.android.gms.auth.zzh(s, bundle), 0L);
    }
    
    public static void zzf(final Intent intent) {
        if (intent != null) {
            final String uri = intent.toUri(1);
            try {
                Intent.parseUri(uri, 1);
                return;
            }
            catch (final URISyntaxException ex) {
                throw new IllegalArgumentException("Parameter callback contains invalid data. It must be serializable using toUri() and parseUri().");
            }
        }
        throw new IllegalArgumentException("Callback cannot be null.");
    }
    
    private static TokenData zzg(final Bundle bundle) {
        final Parcelable$Creator<TokenData> creator = TokenData.CREATOR;
        final ClassLoader classLoader = TokenData.class.getClassLoader();
        if (classLoader != null) {
            bundle.setClassLoader(classLoader);
        }
        final Bundle bundle2 = bundle.getBundle("tokenDetails");
        TokenData tokenData;
        if (bundle2 == null) {
            tokenData = null;
        }
        else {
            if (classLoader != null) {
                bundle2.setClassLoader(classLoader);
            }
            tokenData = (TokenData)bundle2.getParcelable("TokenData");
        }
        if (tokenData != null) {
            return tokenData;
        }
        final String string = ((BaseBundle)bundle).getString("Error");
        Preconditions.checkNotNull(string);
        final Intent intent = (Intent)bundle.getParcelable("userRecoveryIntent");
        final zzby zza = zzby.zza(string);
        if (zzby.zzb(zza)) {
            zzl.zzd.w("isUserRecoverableError status: ".concat(String.valueOf(zza)), new Object[0]);
            throw new UserRecoverableAuthException(string, intent);
        }
        if (!zzby.zze.equals(zza) && !zzby.zzf.equals(zza) && !zzby.zzg.equals(zza) && !zzby.zzaf.equals(zza) && !zzby.zzah.equals(zza)) {
            throw new GoogleAuthException(string);
        }
        throw new IOException(string);
    }
    
    private static Object zzh(final Context cause, final ComponentName componentName, final zzk zzk, final long n) {
        final BlockingServiceConnection blockingServiceConnection = new BlockingServiceConnection();
        final GmsClientSupervisor instance = GmsClientSupervisor.getInstance((Context)cause);
        try {
            if (!instance.bindService(componentName, (ServiceConnection)blockingServiceConnection, "GoogleAuthUtil")) {
                throw new IOException("Could not bind to service.");
            }
            try {
                try {
                    final Object zza = zzk.zza(blockingServiceConnection.getService());
                    instance.unbindService(componentName, (ServiceConnection)blockingServiceConnection, "GoogleAuthUtil");
                    return zza;
                }
                finally {}
            }
            catch (final InterruptedException cause) {}
            catch (final RemoteException cause) {}
            catch (final TimeoutException ex) {}
            Log.i("GoogleAuthUtil", "Error on service connection.", (Throwable)cause);
            throw new IOException("Error on service connection.", cause);
            instance.unbindService(componentName, (ServiceConnection)blockingServiceConnection, "GoogleAuthUtil");
        }
        catch (final SecurityException cause2) {
            Log.w("GoogleAuthUtil", String.format("SecurityException while bind to auth service: %s", cause2.getMessage()));
            throw new IOException("SecurityException while binding to Auth service.", cause2);
        }
    }
    
    private static Object zzi(final Task task, String message) {
        try {
            return Tasks.await(task);
        }
        catch (final CancellationException cause) {
            message = String.format("Canceled while waiting for the task of %s to finish.", message);
            zzl.zzd.w(message, new Object[0]);
            throw new IOException(message, cause);
        }
        catch (final InterruptedException cause2) {
            message = String.format("Interrupted while waiting for the task of %s to finish.", message);
            zzl.zzd.w(message, new Object[0]);
            throw new IOException(message, cause2);
        }
        catch (final ExecutionException cause3) {
            final Throwable cause4 = cause3.getCause();
            if (cause4 instanceof ApiException) {
                throw (ApiException)cause4;
            }
            message = String.format("Unable to get a result for %s due to ExecutionException.", message);
            zzl.zzd.w(message, new Object[0]);
            throw new IOException(message, cause3);
        }
    }
    
    private static Object zzj(final Object o) {
        if (o != null) {
            return o;
        }
        zzl.zzd.w("Service call returned null.", new Object[0]);
        throw new IOException("Service unavailable.");
    }
    
    private static void zzk(final Context ex, final int n) {
        try {
            GooglePlayServicesUtilLight.ensurePlayServicesAvailable(((Context)ex).getApplicationContext(), n);
        }
        catch (final GooglePlayServicesIncorrectManifestValueException ex) {
            goto Label_0014;
        }
        catch (final GooglePlayServicesNotAvailableException ex2) {}
        catch (final GooglePlayServicesRepairableException ex3) {
            throw new GooglePlayServicesAvailabilityException(ex3.getConnectionStatusCode(), ex3.getMessage(), ex3.getIntent());
        }
    }
    
    private static void zzl(final ApiException ex, final String s) {
        zzl.zzd.w("%s failed via GoogleAuthServiceClient, falling back to previous approach:\n%s", s, Log.getStackTraceString((Throwable)ex));
    }
    
    private static void zzm(final Context context, final Bundle bundle) {
        final String packageName = context.getApplicationInfo().packageName;
        ((BaseBundle)bundle).putString("clientPackageName", packageName);
        final String zzb = zzl.zzb;
        if (TextUtils.isEmpty((CharSequence)((BaseBundle)bundle).getString(zzb))) {
            ((BaseBundle)bundle).putString(zzb, packageName);
        }
        ((BaseBundle)bundle).putLong("service_connection_start_time_millis", SystemClock.elapsedRealtime());
    }
    
    private static void zzn(final Account account) {
        if (account == null) {
            throw new IllegalArgumentException("Account cannot be null");
        }
        if (!TextUtils.isEmpty((CharSequence)account.name)) {
            final String[] zza = zzl.zza;
            for (int i = 0; i < 3; ++i) {
                if (zza[i].equals(account.type)) {
                    return;
                }
            }
            throw new IllegalArgumentException("Account type not supported");
        }
        throw new IllegalArgumentException("Account name cannot be empty!");
    }
    
    private static boolean zzo(final Context context) {
        final int googlePlayServicesAvailable = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context, 17895000);
        boolean b = false;
        if (googlePlayServicesAvailable != 0) {
            return false;
        }
        final List zzl = zzhs.zzb().zzl();
        final String packageName = context.getApplicationInfo().packageName;
        final Iterator iterator = zzl.iterator();
        while (iterator.hasNext()) {
            if (((String)iterator.next()).equals(packageName)) {
                return b;
            }
        }
        b = true;
        return b;
    }
}

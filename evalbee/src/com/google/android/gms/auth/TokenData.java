// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import android.text.TextUtils;
import com.google.android.gms.common.internal.Preconditions;
import java.util.List;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.ShowFirstParty;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@ShowFirstParty
@Class(creator = "TokenDataCreator")
public class TokenData extends AbstractSafeParcelable implements ReflectedParcelable
{
    public static final Parcelable$Creator<TokenData> CREATOR;
    @VersionField(id = 1)
    final int zza;
    @Field(getter = "getToken", id = 2)
    private final String zzb;
    @Field(getter = "getExpirationTimeSecs", id = 3)
    private final Long zzc;
    @Field(getter = "isCached", id = 4)
    private final boolean zzd;
    @Field(getter = "isSnowballed", id = 5)
    private final boolean zze;
    @Field(getter = "getGrantedScopes", id = 6)
    private final List zzf;
    @Field(getter = "getScopeData", id = 7)
    private final String zzg;
    
    static {
        CREATOR = (Parcelable$Creator)new zzm();
    }
    
    @Constructor
    public TokenData(@Param(id = 1) final int zza, @Param(id = 2) final String s, @Param(id = 3) final Long zzc, @Param(id = 4) final boolean zzd, @Param(id = 5) final boolean zze, @Param(id = 6) final List zzf, @Param(id = 7) final String zzg) {
        this.zza = zza;
        this.zzb = Preconditions.checkNotEmpty(s);
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
        this.zzg = zzg;
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (!(o instanceof TokenData)) {
            return false;
        }
        final TokenData tokenData = (TokenData)o;
        return TextUtils.equals((CharSequence)this.zzb, (CharSequence)tokenData.zzb) && Objects.equal(this.zzc, tokenData.zzc) && this.zzd == tokenData.zzd && this.zze == tokenData.zze && Objects.equal(this.zzf, tokenData.zzf) && Objects.equal(this.zzg, tokenData.zzg);
    }
    
    @Override
    public final int hashCode() {
        return Objects.hashCode(this.zzb, this.zzc, this.zzd, this.zze, this.zzf, this.zzg);
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.zza);
        SafeParcelWriter.writeString(parcel, 2, this.zzb, false);
        SafeParcelWriter.writeLongObject(parcel, 3, this.zzc, false);
        SafeParcelWriter.writeBoolean(parcel, 4, this.zzd);
        SafeParcelWriter.writeBoolean(parcel, 5, this.zze);
        SafeParcelWriter.writeStringList(parcel, 6, this.zzf, false);
        SafeParcelWriter.writeString(parcel, 7, this.zzg, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public final String zza() {
        return this.zzb;
    }
}

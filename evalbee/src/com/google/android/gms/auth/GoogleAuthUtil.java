// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth;

import android.os.BaseBundle;
import com.google.android.gms.common.GooglePlayServicesUtil;
import android.util.Log;
import com.google.android.gms.common.GooglePlayServicesUtilLight;
import android.content.ContentResolver;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable;
import android.content.Intent;
import android.os.Bundle;
import android.accounts.Account;
import java.util.List;
import android.content.Context;

public final class GoogleAuthUtil extends zzl
{
    public static final int CHANGE_TYPE_ACCOUNT_ADDED = 1;
    public static final int CHANGE_TYPE_ACCOUNT_REMOVED = 2;
    public static final int CHANGE_TYPE_ACCOUNT_RENAMED_FROM = 3;
    public static final int CHANGE_TYPE_ACCOUNT_RENAMED_TO = 4;
    public static final String GOOGLE_ACCOUNT_TYPE = "com.google";
    public static final String KEY_SUPPRESS_PROGRESS_SCREEN = "suppressProgressScreen";
    public static final String WORK_ACCOUNT_TYPE = "com.google.work";
    
    private GoogleAuthUtil() {
    }
    
    public static void clearToken(final Context context, final String s) {
        zzl.clearToken(context, s);
    }
    
    public static List<AccountChangeEvent> getAccountChangeEvents(final Context context, final int n, final String s) {
        return zzl.getAccountChangeEvents(context, n, s);
    }
    
    public static String getAccountId(final Context context, final String s) {
        return zzl.getAccountId(context, s);
    }
    
    public static String getToken(final Context context, final Account account, final String s) {
        return zzl.getToken(context, account, s);
    }
    
    public static String getToken(final Context context, final Account account, final String s, final Bundle bundle) {
        return zzl.getToken(context, account, s, bundle);
    }
    
    @Deprecated
    public static String getToken(final Context context, final String s, final String s2) {
        return zzl.getToken(context, s, s2);
    }
    
    @Deprecated
    public static String getToken(final Context context, final String s, final String s2, final Bundle bundle) {
        return zzl.getToken(context, s, s2, bundle);
    }
    
    public static String getTokenWithNotification(final Context context, final Account account, final String s, final Bundle bundle) {
        Bundle bundle2 = bundle;
        if (bundle == null) {
            bundle2 = new Bundle();
        }
        ((BaseBundle)bundle2).putBoolean("handle_notification", true);
        return zzg(context, account, s, bundle2).zza();
    }
    
    public static String getTokenWithNotification(final Context context, final Account account, final String s, final Bundle bundle, final Intent intent) {
        zzl.zzf(intent);
        Bundle bundle2 = bundle;
        if (bundle == null) {
            bundle2 = new Bundle();
        }
        bundle2.putParcelable("callback_intent", (Parcelable)intent);
        ((BaseBundle)bundle2).putBoolean("handle_notification", true);
        return zzg(context, account, s, bundle2).zza();
    }
    
    public static String getTokenWithNotification(final Context context, final Account account, final String s, Bundle o, final String s2, final Bundle bundle) {
        Preconditions.checkNotEmpty(s2, "Authority cannot be empty or null.");
        Object o2 = o;
        if (o == null) {
            o2 = new Bundle();
        }
        if ((o = bundle) == null) {
            o = new Bundle();
        }
        ContentResolver.validateSyncExtrasBundle((Bundle)o);
        ((BaseBundle)o2).putString("authority", s2);
        ((Bundle)o2).putBundle("sync_extras", (Bundle)o);
        ((BaseBundle)o2).putBoolean("handle_notification", true);
        return zzg(context, account, s, (Bundle)o2).zza();
    }
    
    @Deprecated
    public static String getTokenWithNotification(final Context context, final String s, final String s2, final Bundle bundle) {
        return getTokenWithNotification(context, new Account(s, "com.google"), s2, bundle);
    }
    
    @Deprecated
    public static String getTokenWithNotification(final Context context, final String s, final String s2, final Bundle bundle, final Intent intent) {
        return getTokenWithNotification(context, new Account(s, "com.google"), s2, bundle, intent);
    }
    
    @Deprecated
    public static String getTokenWithNotification(final Context context, final String s, final String s2, final Bundle bundle, final String s3, final Bundle bundle2) {
        return getTokenWithNotification(context, new Account(s, "com.google"), s2, bundle, s3, bundle2);
    }
    
    @Deprecated
    public static void invalidateToken(final Context context, final String s) {
        zzl.invalidateToken(context, s);
    }
    
    public static Bundle removeAccount(final Context context, final Account account) {
        return zzl.removeAccount(context, account);
    }
    
    public static Boolean requestGoogleAccountsAccess(final Context context) {
        return zzl.requestGoogleAccountsAccess(context);
    }
    
    private static TokenData zzg(final Context context, final Account account, final String s, final Bundle bundle) {
        try {
            final TokenData zza = zzl.zza(context, account, s, bundle);
            GooglePlayServicesUtilLight.cancelAvailabilityErrorNotifications(context);
            return zza;
        }
        catch (final UserRecoverableAuthException ex) {
            GooglePlayServicesUtilLight.cancelAvailabilityErrorNotifications(context);
            Log.w("GoogleAuthUtil", "Error when getting token", (Throwable)ex);
            throw new UserRecoverableNotifiedException("User intervention required. Notification has been pushed.", ex);
        }
        catch (final GooglePlayServicesAvailabilityException ex2) {
            GooglePlayServicesUtil.showErrorNotification(ex2.getConnectionStatusCode(), context);
            Log.w("GoogleAuthUtil", "Error when getting token", (Throwable)ex2);
            throw new UserRecoverableNotifiedException("User intervention required. Notification has been pushed.", ex2);
        }
    }
}

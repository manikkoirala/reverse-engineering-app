// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth;

import android.text.TextUtils;
import com.google.android.gms.common.internal.Preconditions;

public final class CookieUtil
{
    private CookieUtil() {
    }
    
    public static String getCookieUrl(final String str, final Boolean b) {
        Preconditions.checkNotEmpty(str);
        String str2;
        if (!zza(b)) {
            str2 = "http";
        }
        else {
            str2 = "https";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(str2);
        sb.append("://");
        sb.append(str);
        return sb.toString();
    }
    
    public static String getCookieValue(final String s, final String str, final String str2, final String str3, final Boolean b, final Boolean b2, final Long obj) {
        String str4 = s;
        if (s == null) {
            str4 = "";
        }
        final StringBuilder sb = new StringBuilder(str4);
        sb.append('=');
        if (!TextUtils.isEmpty((CharSequence)str)) {
            sb.append(str);
        }
        if (zza(b)) {
            sb.append(";HttpOnly");
        }
        if (zza(b2)) {
            sb.append(";Secure");
        }
        if (!TextUtils.isEmpty((CharSequence)str2)) {
            sb.append(";Domain=");
            sb.append(str2);
        }
        if (!TextUtils.isEmpty((CharSequence)str3)) {
            sb.append(";Path=");
            sb.append(str3);
        }
        if (obj != null && obj > 0L) {
            sb.append(";Max-Age=");
            sb.append(obj);
        }
        if (!TextUtils.isEmpty((CharSequence)null)) {
            sb.append(";Priority=null");
        }
        if (!TextUtils.isEmpty((CharSequence)null)) {
            sb.append(";SameSite=null");
        }
        if (zza(null)) {
            sb.append(";SameParty");
        }
        return sb.toString();
    }
    
    private static boolean zza(final Boolean b) {
        return b != null && b;
    }
}

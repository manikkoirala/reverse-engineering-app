// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Preconditions;
import java.util.List;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "AccountChangeEventsResponseCreator")
public class AccountChangeEventsResponse extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<AccountChangeEventsResponse> CREATOR;
    @VersionField(id = 1)
    final int zza;
    @Field(id = 2)
    final List zzb;
    
    static {
        CREATOR = (Parcelable$Creator)new zzc();
    }
    
    @Constructor
    public AccountChangeEventsResponse(@Param(id = 1) final int zza, @Param(id = 2) final List list) {
        this.zza = zza;
        this.zzb = Preconditions.checkNotNull(list);
    }
    
    public AccountChangeEventsResponse(final List<AccountChangeEvent> list) {
        this.zza = 1;
        this.zzb = Preconditions.checkNotNull(list);
    }
    
    public List<AccountChangeEvent> getEvents() {
        return this.zzb;
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.zza);
        SafeParcelWriter.writeTypedList(parcel, 2, (List<Parcelable>)this.zzb, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

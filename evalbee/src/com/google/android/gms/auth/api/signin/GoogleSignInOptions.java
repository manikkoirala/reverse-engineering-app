// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.signin;

import java.util.Arrays;
import com.google.android.gms.common.internal.Preconditions;
import java.util.Set;
import org.json.JSONException;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.auth.api.signin.internal.HashAccumulator;
import java.util.Collections;
import com.google.android.gms.common.annotation.KeepForSdk;
import java.util.Iterator;
import org.json.JSONArray;
import java.util.HashMap;
import java.util.HashSet;
import org.json.JSONObject;
import android.text.TextUtils;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import com.google.android.gms.auth.api.signin.internal.GoogleSignInOptionsExtensionParcelable;
import android.accounts.Account;
import java.util.ArrayList;
import java.util.Comparator;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.common.api.Scope;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "GoogleSignInOptionsCreator")
public class GoogleSignInOptions extends AbstractSafeParcelable implements Optional, ReflectedParcelable
{
    public static final Parcelable$Creator<GoogleSignInOptions> CREATOR;
    public static final GoogleSignInOptions DEFAULT_GAMES_SIGN_IN;
    public static final GoogleSignInOptions DEFAULT_SIGN_IN;
    @VisibleForTesting
    public static final Scope zaa;
    @VisibleForTesting
    public static final Scope zab;
    @VisibleForTesting
    public static final Scope zac;
    @VisibleForTesting
    public static final Scope zad;
    @VisibleForTesting
    public static final Scope zae;
    private static Comparator<Scope> zag;
    @VersionField(id = 1)
    final int zaf;
    @Field(getter = "getScopes", id = 2)
    private final ArrayList<Scope> zah;
    @Field(getter = "getAccount", id = 3)
    private Account zai;
    @Field(getter = "isIdTokenRequested", id = 4)
    private boolean zaj;
    @Field(getter = "isServerAuthCodeRequested", id = 5)
    private final boolean zak;
    @Field(getter = "isForceCodeForRefreshToken", id = 6)
    private final boolean zal;
    @Field(getter = "getServerClientId", id = 7)
    private String zam;
    @Field(getter = "getHostedDomain", id = 8)
    private String zan;
    @Field(getter = "getExtensions", id = 9)
    private ArrayList<GoogleSignInOptionsExtensionParcelable> zao;
    @Field(getter = "getLogSessionId", id = 10)
    private String zap;
    private Map<Integer, GoogleSignInOptionsExtensionParcelable> zaq;
    
    static {
        zaa = new Scope("profile");
        zab = new Scope("email");
        zac = new Scope("openid");
        final Scope scope = zad = new Scope("https://www.googleapis.com/auth/games_lite");
        zae = new Scope("https://www.googleapis.com/auth/games");
        final Builder builder = new Builder();
        builder.requestId();
        builder.requestProfile();
        DEFAULT_SIGN_IN = builder.build();
        final Builder builder2 = new Builder();
        builder2.requestScopes(scope, new Scope[0]);
        DEFAULT_GAMES_SIGN_IN = builder2.build();
        CREATOR = (Parcelable$Creator)new zae();
        GoogleSignInOptions.zag = new zac();
    }
    
    @Constructor
    public GoogleSignInOptions(@Param(id = 1) final int n, @Param(id = 2) final ArrayList<Scope> list, @Param(id = 3) final Account account, @Param(id = 4) final boolean b, @Param(id = 5) final boolean b2, @Param(id = 6) final boolean b3, @Param(id = 7) final String s, @Param(id = 8) final String s2, @Param(id = 9) final ArrayList<GoogleSignInOptionsExtensionParcelable> list2, @Param(id = 10) final String s3) {
        this(n, list, account, b, b2, b3, s, s2, zam(list2), s3);
    }
    
    private GoogleSignInOptions(final int zaf, final ArrayList<Scope> zah, final Account zai, final boolean zaj, final boolean zak, final boolean zal, final String zam, final String zan, final Map<Integer, GoogleSignInOptionsExtensionParcelable> zaq, final String zap) {
        this.zaf = zaf;
        this.zah = zah;
        this.zai = zai;
        this.zaj = zaj;
        this.zak = zak;
        this.zal = zal;
        this.zam = zam;
        this.zan = zan;
        this.zao = new ArrayList<GoogleSignInOptionsExtensionParcelable>(zaq.values());
        this.zaq = zaq;
        this.zap = zap;
    }
    
    public static GoogleSignInOptions zab(String optString) {
        final boolean empty = TextUtils.isEmpty((CharSequence)optString);
        String optString2 = null;
        if (empty) {
            return null;
        }
        final JSONObject jsonObject = new JSONObject(optString);
        final HashSet c = new HashSet();
        final JSONArray jsonArray = jsonObject.getJSONArray("scopes");
        for (int length = jsonArray.length(), i = 0; i < length; ++i) {
            c.add(new Scope(jsonArray.getString(i)));
        }
        if (jsonObject.has("accountName")) {
            optString = jsonObject.optString("accountName");
        }
        else {
            optString = null;
        }
        Account account;
        if (!TextUtils.isEmpty((CharSequence)optString)) {
            account = new Account(optString, "com.google");
        }
        else {
            account = null;
        }
        final ArrayList list = new ArrayList<Scope>(c);
        final boolean boolean1 = jsonObject.getBoolean("idTokenRequested");
        final boolean boolean2 = jsonObject.getBoolean("serverAuthRequested");
        final boolean boolean3 = jsonObject.getBoolean("forceCodeForRefreshToken");
        String optString3;
        if (jsonObject.has("serverClientId")) {
            optString3 = jsonObject.optString("serverClientId");
        }
        else {
            optString3 = null;
        }
        if (jsonObject.has("hostedDomain")) {
            optString2 = jsonObject.optString("hostedDomain");
        }
        return new GoogleSignInOptions(3, (ArrayList<Scope>)list, account, boolean1, boolean2, boolean3, optString3, optString2, new HashMap<Integer, GoogleSignInOptionsExtensionParcelable>(), null);
    }
    
    private static Map<Integer, GoogleSignInOptionsExtensionParcelable> zam(final List<GoogleSignInOptionsExtensionParcelable> list) {
        final HashMap hashMap = new HashMap();
        if (list == null) {
            return hashMap;
        }
        for (final GoogleSignInOptionsExtensionParcelable googleSignInOptionsExtensionParcelable : list) {
            hashMap.put(googleSignInOptionsExtensionParcelable.getType(), googleSignInOptionsExtensionParcelable);
        }
        return hashMap;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == null) {
            return false;
        }
        try {
            final GoogleSignInOptions googleSignInOptions = (GoogleSignInOptions)o;
            if (this.zao.size() <= 0) {
                if (googleSignInOptions.zao.size() <= 0) {
                    if (this.zah.size() == googleSignInOptions.getScopes().size()) {
                        if (this.zah.containsAll(googleSignInOptions.getScopes())) {
                            final Account zai = this.zai;
                            if (zai == null) {
                                if (googleSignInOptions.getAccount() != null) {
                                    return false;
                                }
                            }
                            else if (!zai.equals((Object)googleSignInOptions.getAccount())) {
                                return false;
                            }
                            if (TextUtils.isEmpty((CharSequence)this.zam)) {
                                if (!TextUtils.isEmpty((CharSequence)googleSignInOptions.getServerClientId())) {
                                    return false;
                                }
                            }
                            else if (!this.zam.equals(googleSignInOptions.getServerClientId())) {
                                return false;
                            }
                            if (this.zal == googleSignInOptions.isForceCodeForRefreshToken() && this.zaj == googleSignInOptions.isIdTokenRequested() && this.zak == googleSignInOptions.isServerAuthCodeRequested() && TextUtils.equals((CharSequence)this.zap, (CharSequence)googleSignInOptions.getLogSessionId())) {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }
        catch (final ClassCastException ex) {
            return false;
        }
    }
    
    @KeepForSdk
    public Account getAccount() {
        return this.zai;
    }
    
    @KeepForSdk
    public ArrayList<GoogleSignInOptionsExtensionParcelable> getExtensions() {
        return this.zao;
    }
    
    @KeepForSdk
    public String getLogSessionId() {
        return this.zap;
    }
    
    public Scope[] getScopeArray() {
        final ArrayList<Scope> zah = this.zah;
        return zah.toArray(new Scope[zah.size()]);
    }
    
    @KeepForSdk
    public ArrayList<Scope> getScopes() {
        return new ArrayList<Scope>(this.zah);
    }
    
    @KeepForSdk
    public String getServerClientId() {
        return this.zam;
    }
    
    @Override
    public int hashCode() {
        final ArrayList list = new ArrayList();
        final ArrayList<Scope> zah = this.zah;
        for (int size = zah.size(), i = 0; i < size; ++i) {
            list.add(((Scope)zah.get(i)).getScopeUri());
        }
        Collections.sort((List<Comparable>)list);
        final HashAccumulator hashAccumulator = new HashAccumulator();
        hashAccumulator.addObject(list);
        hashAccumulator.addObject(this.zai);
        hashAccumulator.addObject(this.zam);
        hashAccumulator.zaa(this.zal);
        hashAccumulator.zaa(this.zaj);
        hashAccumulator.zaa(this.zak);
        hashAccumulator.addObject(this.zap);
        return hashAccumulator.hash();
    }
    
    @KeepForSdk
    public boolean isForceCodeForRefreshToken() {
        return this.zal;
    }
    
    @KeepForSdk
    public boolean isIdTokenRequested() {
        return this.zaj;
    }
    
    @KeepForSdk
    public boolean isServerAuthCodeRequested() {
        return this.zak;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.zaf);
        SafeParcelWriter.writeTypedList(parcel, 2, this.getScopes(), false);
        SafeParcelWriter.writeParcelable(parcel, 3, (Parcelable)this.getAccount(), n, false);
        SafeParcelWriter.writeBoolean(parcel, 4, this.isIdTokenRequested());
        SafeParcelWriter.writeBoolean(parcel, 5, this.isServerAuthCodeRequested());
        SafeParcelWriter.writeBoolean(parcel, 6, this.isForceCodeForRefreshToken());
        SafeParcelWriter.writeString(parcel, 7, this.getServerClientId(), false);
        SafeParcelWriter.writeString(parcel, 8, this.zan, false);
        SafeParcelWriter.writeTypedList(parcel, 9, this.getExtensions(), false);
        SafeParcelWriter.writeString(parcel, 10, this.getLogSessionId(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public final String zaf() {
        final JSONObject jsonObject = new JSONObject();
        try {
            final JSONArray jsonArray = new JSONArray();
            Collections.sort(this.zah, GoogleSignInOptions.zag);
            final Iterator<Scope> iterator = this.zah.iterator();
            while (iterator.hasNext()) {
                jsonArray.put((Object)iterator.next().getScopeUri());
            }
            jsonObject.put("scopes", (Object)jsonArray);
            final Account zai = this.zai;
            if (zai != null) {
                jsonObject.put("accountName", (Object)zai.name);
            }
            jsonObject.put("idTokenRequested", this.zaj);
            jsonObject.put("forceCodeForRefreshToken", this.zal);
            jsonObject.put("serverAuthRequested", this.zak);
            if (!TextUtils.isEmpty((CharSequence)this.zam)) {
                jsonObject.put("serverClientId", (Object)this.zam);
            }
            if (!TextUtils.isEmpty((CharSequence)this.zan)) {
                jsonObject.put("hostedDomain", (Object)this.zan);
            }
            return jsonObject.toString();
        }
        catch (final JSONException cause) {
            throw new RuntimeException((Throwable)cause);
        }
    }
    
    public static final class Builder
    {
        private Set<Scope> zaa;
        private boolean zab;
        private boolean zac;
        private boolean zad;
        private String zae;
        private Account zaf;
        private String zag;
        private Map<Integer, GoogleSignInOptionsExtensionParcelable> zah;
        private String zai;
        
        public Builder() {
            this.zaa = new HashSet<Scope>();
            this.zah = new HashMap<Integer, GoogleSignInOptionsExtensionParcelable>();
        }
        
        public Builder(final GoogleSignInOptions googleSignInOptions) {
            this.zaa = new HashSet<Scope>();
            this.zah = new HashMap<Integer, GoogleSignInOptionsExtensionParcelable>();
            Preconditions.checkNotNull(googleSignInOptions);
            this.zaa = new HashSet<Scope>(GoogleSignInOptions.zah(googleSignInOptions));
            this.zab = GoogleSignInOptions.zal(googleSignInOptions);
            this.zac = GoogleSignInOptions.zaj(googleSignInOptions);
            this.zad = GoogleSignInOptions.zak(googleSignInOptions);
            this.zae = GoogleSignInOptions.zae(googleSignInOptions);
            this.zaf = GoogleSignInOptions.zaa(googleSignInOptions);
            this.zag = GoogleSignInOptions.zac(googleSignInOptions);
            this.zah = zam(GoogleSignInOptions.zag(googleSignInOptions));
            this.zai = GoogleSignInOptions.zad(googleSignInOptions);
        }
        
        private final String zaa(final String anObject) {
            Preconditions.checkNotEmpty(anObject);
            final String zae = this.zae;
            boolean b = true;
            if (zae != null) {
                b = (zae.equals(anObject) && b);
            }
            Preconditions.checkArgument(b, (Object)"two different server client ids provided");
            return anObject;
        }
        
        public Builder addExtension(final GoogleSignInOptionsExtension googleSignInOptionsExtension) {
            if (!this.zah.containsKey(googleSignInOptionsExtension.getExtensionType())) {
                final List<Scope> impliedScopes = googleSignInOptionsExtension.getImpliedScopes();
                if (impliedScopes != null) {
                    this.zaa.addAll(impliedScopes);
                }
                this.zah.put(googleSignInOptionsExtension.getExtensionType(), new GoogleSignInOptionsExtensionParcelable(googleSignInOptionsExtension));
                return this;
            }
            throw new IllegalStateException("Only one extension per type may be added");
        }
        
        public GoogleSignInOptions build() {
            if (this.zaa.contains(GoogleSignInOptions.zae)) {
                final Set<Scope> zaa = this.zaa;
                final Scope zad = GoogleSignInOptions.zad;
                if (zaa.contains(zad)) {
                    this.zaa.remove(zad);
                }
            }
            if (this.zad && (this.zaf == null || !this.zaa.isEmpty())) {
                this.requestId();
            }
            return new GoogleSignInOptions(3, new ArrayList((Collection<? extends E>)this.zaa), this.zaf, this.zad, this.zab, this.zac, this.zae, this.zag, this.zah, this.zai, null);
        }
        
        public Builder requestEmail() {
            this.zaa.add(GoogleSignInOptions.zab);
            return this;
        }
        
        public Builder requestId() {
            this.zaa.add(GoogleSignInOptions.zac);
            return this;
        }
        
        public Builder requestIdToken(final String zae) {
            this.zad = true;
            this.zaa(zae);
            this.zae = zae;
            return this;
        }
        
        public Builder requestProfile() {
            this.zaa.add(GoogleSignInOptions.zaa);
            return this;
        }
        
        public Builder requestScopes(final Scope scope, final Scope... a) {
            this.zaa.add(scope);
            this.zaa.addAll(Arrays.asList(a));
            return this;
        }
        
        public Builder requestServerAuthCode(final String s) {
            this.requestServerAuthCode(s, false);
            return this;
        }
        
        public Builder requestServerAuthCode(final String zae, final boolean zac) {
            this.zab = true;
            this.zaa(zae);
            this.zae = zae;
            this.zac = zac;
            return this;
        }
        
        public Builder setAccountName(final String s) {
            this.zaf = new Account(Preconditions.checkNotEmpty(s), "com.google");
            return this;
        }
        
        public Builder setHostedDomain(final String s) {
            this.zag = Preconditions.checkNotEmpty(s);
            return this;
        }
        
        @KeepForSdk
        public Builder setLogSessionId(final String zai) {
            this.zai = zai;
            return this;
        }
    }
}

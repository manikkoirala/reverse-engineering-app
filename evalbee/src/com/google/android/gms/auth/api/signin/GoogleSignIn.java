// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.signin;

import android.text.TextUtils;
import androidx.fragment.app.Fragment;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import com.google.android.gms.common.internal.ApiExceptionUtil;
import com.google.android.gms.tasks.Tasks;
import com.google.android.gms.auth.api.signin.internal.zbm;
import com.google.android.gms.tasks.Task;
import android.content.Intent;
import com.google.android.gms.auth.api.signin.internal.zbn;
import android.app.Activity;
import com.google.android.gms.common.api.Scope;
import java.util.List;
import com.google.android.gms.common.internal.Preconditions;
import android.content.Context;

public final class GoogleSignIn
{
    private GoogleSignIn() {
    }
    
    public static GoogleSignInAccount getAccountForExtension(final Context context, final GoogleSignInOptionsExtension googleSignInOptionsExtension) {
        Preconditions.checkNotNull(context, "please provide a valid Context object");
        Preconditions.checkNotNull(googleSignInOptionsExtension, "please provide valid GoogleSignInOptionsExtension");
        GoogleSignInAccount googleSignInAccount;
        if ((googleSignInAccount = getLastSignedInAccount(context)) == null) {
            googleSignInAccount = GoogleSignInAccount.createDefault();
        }
        return googleSignInAccount.requestExtraScopes(zbb(googleSignInOptionsExtension.getImpliedScopes()));
    }
    
    public static GoogleSignInAccount getAccountForScopes(final Context context, final Scope scope, final Scope... array) {
        Preconditions.checkNotNull(context, "please provide a valid Context object");
        Preconditions.checkNotNull(scope, "please provide at least one valid scope");
        GoogleSignInAccount googleSignInAccount;
        if ((googleSignInAccount = getLastSignedInAccount(context)) == null) {
            googleSignInAccount = GoogleSignInAccount.createDefault();
        }
        googleSignInAccount.requestExtraScopes(scope);
        googleSignInAccount.requestExtraScopes(array);
        return googleSignInAccount;
    }
    
    public static GoogleSignInClient getClient(final Activity activity, final GoogleSignInOptions googleSignInOptions) {
        return new GoogleSignInClient(activity, Preconditions.checkNotNull(googleSignInOptions));
    }
    
    public static GoogleSignInClient getClient(final Context context, final GoogleSignInOptions googleSignInOptions) {
        return new GoogleSignInClient(context, Preconditions.checkNotNull(googleSignInOptions));
    }
    
    public static GoogleSignInAccount getLastSignedInAccount(final Context context) {
        return zbn.zbc(context).zba();
    }
    
    public static Task<GoogleSignInAccount> getSignedInAccountFromIntent(final Intent intent) {
        final GoogleSignInResult zbd = zbm.zbd(intent);
        final GoogleSignInAccount signInAccount = zbd.getSignInAccount();
        if (zbd.getStatus().isSuccess() && signInAccount != null) {
            return (Task<GoogleSignInAccount>)Tasks.forResult((Object)signInAccount);
        }
        return (Task<GoogleSignInAccount>)Tasks.forException((Exception)ApiExceptionUtil.fromStatus(zbd.getStatus()));
    }
    
    public static boolean hasPermissions(final GoogleSignInAccount googleSignInAccount, final GoogleSignInOptionsExtension googleSignInOptionsExtension) {
        Preconditions.checkNotNull(googleSignInOptionsExtension, "Please provide a non-null GoogleSignInOptionsExtension");
        return hasPermissions(googleSignInAccount, zbb(googleSignInOptionsExtension.getImpliedScopes()));
    }
    
    public static boolean hasPermissions(final GoogleSignInAccount googleSignInAccount, final Scope... elements) {
        if (googleSignInAccount == null) {
            return false;
        }
        final HashSet c = new HashSet();
        Collections.addAll(c, elements);
        return googleSignInAccount.getGrantedScopes().containsAll(c);
    }
    
    public static void requestPermissions(final Activity activity, final int n, final GoogleSignInAccount googleSignInAccount, final GoogleSignInOptionsExtension googleSignInOptionsExtension) {
        Preconditions.checkNotNull(activity, "Please provide a non-null Activity");
        Preconditions.checkNotNull(googleSignInOptionsExtension, "Please provide a non-null GoogleSignInOptionsExtension");
        requestPermissions(activity, n, googleSignInAccount, zbb(googleSignInOptionsExtension.getImpliedScopes()));
    }
    
    public static void requestPermissions(final Activity activity, final int n, final GoogleSignInAccount googleSignInAccount, final Scope... array) {
        Preconditions.checkNotNull(activity, "Please provide a non-null Activity");
        Preconditions.checkNotNull(array, "Please provide at least one scope");
        activity.startActivityForResult(zba(activity, googleSignInAccount, array), n);
    }
    
    public static void requestPermissions(final Fragment fragment, final int n, final GoogleSignInAccount googleSignInAccount, final GoogleSignInOptionsExtension googleSignInOptionsExtension) {
        Preconditions.checkNotNull(fragment, "Please provide a non-null Fragment");
        Preconditions.checkNotNull(googleSignInOptionsExtension, "Please provide a non-null GoogleSignInOptionsExtension");
        requestPermissions(fragment, n, googleSignInAccount, zbb(googleSignInOptionsExtension.getImpliedScopes()));
    }
    
    public static void requestPermissions(final Fragment fragment, final int n, final GoogleSignInAccount googleSignInAccount, final Scope... array) {
        Preconditions.checkNotNull(fragment, "Please provide a non-null Fragment");
        Preconditions.checkNotNull(array, "Please provide at least one scope");
        fragment.startActivityForResult(zba(fragment.getActivity(), googleSignInAccount, array), n);
    }
    
    private static Intent zba(final Activity activity, final GoogleSignInAccount googleSignInAccount, final Scope... array) {
        final GoogleSignInOptions.Builder builder = new GoogleSignInOptions.Builder();
        if (array.length > 0) {
            builder.requestScopes(array[0], array);
        }
        if (googleSignInAccount != null && !TextUtils.isEmpty((CharSequence)googleSignInAccount.getEmail())) {
            builder.setAccountName(Preconditions.checkNotNull(googleSignInAccount.getEmail()));
        }
        return new GoogleSignInClient(activity, builder.build()).getSignInIntent();
    }
    
    private static Scope[] zbb(final List list) {
        if (list == null) {
            return new Scope[0];
        }
        return list.toArray(new Scope[list.size()]);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.accounttransfer;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import java.util.Map;
import com.google.android.gms.common.server.response.FastJsonResponse;
import android.app.PendingIntent;
import java.util.Set;
import java.util.HashMap;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.auth.zzbz;

@SafeParcelable.Class(creator = "AuthenticatorTransferInfoCreator")
public final class zzw extends zzbz
{
    public static final Parcelable$Creator<zzw> CREATOR;
    private static final HashMap zzc;
    @SafeParcelable.Indicator
    final Set zza;
    @SafeParcelable.VersionField(id = 1)
    final int zzb;
    @SafeParcelable.Field(getter = "getAccountType", id = 2)
    private String zzd;
    @SafeParcelable.Field(getter = "getStatus", id = 3)
    private int zze;
    @SafeParcelable.Field(getter = "getTransferBytes", id = 4)
    private byte[] zzf;
    @SafeParcelable.Field(getter = "getPendingIntent", id = 5)
    private PendingIntent zzg;
    @SafeParcelable.Field(getter = "getDeviceMetaData", id = 6)
    private DeviceMetaData zzh;
    
    static {
        CREATOR = (Parcelable$Creator)new zzx();
        final HashMap zzc2 = new HashMap();
        (zzc = zzc2).put("accountType", FastJsonResponse.Field.forString("accountType", 2));
        zzc2.put("status", FastJsonResponse.Field.forInteger("status", 3));
        zzc2.put("transferBytes", FastJsonResponse.Field.forBase64("transferBytes", 4));
    }
    
    public zzw() {
        this.zza = new s8(3);
        this.zzb = 1;
    }
    
    @SafeParcelable.Constructor
    public zzw(@SafeParcelable.Indicator final Set zza, @SafeParcelable.Param(id = 1) final int zzb, @SafeParcelable.Param(id = 2) final String zzd, @SafeParcelable.Param(id = 3) final int zze, @SafeParcelable.Param(id = 4) final byte[] zzf, @SafeParcelable.Param(id = 5) final PendingIntent zzg, @SafeParcelable.Param(id = 6) final DeviceMetaData zzh) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
        this.zzg = zzg;
        this.zzh = zzh;
    }
    
    public final Object getFieldValue(final FastJsonResponse.Field field) {
        final int safeParcelableFieldId = field.getSafeParcelableFieldId();
        int i;
        if (safeParcelableFieldId != 1) {
            if (safeParcelableFieldId == 2) {
                return this.zzd;
            }
            if (safeParcelableFieldId != 3) {
                if (safeParcelableFieldId == 4) {
                    return this.zzf;
                }
                final int safeParcelableFieldId2 = field.getSafeParcelableFieldId();
                final StringBuilder sb = new StringBuilder();
                sb.append("Unknown SafeParcelable id=");
                sb.append(safeParcelableFieldId2);
                throw new IllegalStateException(sb.toString());
            }
            else {
                i = this.zze;
            }
        }
        else {
            i = this.zzb;
        }
        return i;
    }
    
    public final boolean isFieldSet(final FastJsonResponse.Field field) {
        return this.zza.contains(field.getSafeParcelableFieldId());
    }
    
    public final void setDecodedBytesInternal(final FastJsonResponse.Field field, final String s, final byte[] zzf) {
        final int safeParcelableFieldId = field.getSafeParcelableFieldId();
        if (safeParcelableFieldId == 4) {
            this.zzf = zzf;
            this.zza.add(safeParcelableFieldId);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Field with id=");
        sb.append(safeParcelableFieldId);
        sb.append(" is not known to be an byte array.");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public final void setIntegerInternal(final FastJsonResponse.Field field, final String s, final int zze) {
        final int safeParcelableFieldId = field.getSafeParcelableFieldId();
        if (safeParcelableFieldId == 3) {
            this.zze = zze;
            this.zza.add(safeParcelableFieldId);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Field with id=");
        sb.append(safeParcelableFieldId);
        sb.append(" is not known to be an int.");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public final void setStringInternal(final FastJsonResponse.Field field, final String s, final String zzd) {
        final int safeParcelableFieldId = field.getSafeParcelableFieldId();
        if (safeParcelableFieldId == 2) {
            this.zzd = zzd;
            this.zza.add(safeParcelableFieldId);
            return;
        }
        throw new IllegalArgumentException(String.format("Field with id=%d is not known to be a string.", safeParcelableFieldId));
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        final Set zza = this.zza;
        if (zza.contains(1)) {
            SafeParcelWriter.writeInt(parcel, 1, this.zzb);
        }
        if (zza.contains(2)) {
            SafeParcelWriter.writeString(parcel, 2, this.zzd, true);
        }
        if (zza.contains(3)) {
            SafeParcelWriter.writeInt(parcel, 3, this.zze);
        }
        if (zza.contains(4)) {
            SafeParcelWriter.writeByteArray(parcel, 4, this.zzf, true);
        }
        if (zza.contains(5)) {
            SafeParcelWriter.writeParcelable(parcel, 5, (Parcelable)this.zzg, n, true);
        }
        if (zza.contains(6)) {
            SafeParcelWriter.writeParcelable(parcel, 6, (Parcelable)this.zzh, n, true);
        }
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

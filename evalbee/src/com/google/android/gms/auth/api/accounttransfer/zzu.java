// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.accounttransfer;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import java.util.Map;
import java.util.HashSet;
import com.google.android.gms.common.server.response.FastJsonResponse;
import java.util.Set;
import java.util.HashMap;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.auth.zzbz;

@SafeParcelable.Class(creator = "AuthenticatorAnnotatedDataCreator")
public final class zzu extends zzbz
{
    public static final Parcelable$Creator<zzu> CREATOR;
    private static final HashMap zzc;
    @SafeParcelable.Indicator
    final Set zza;
    @SafeParcelable.VersionField(id = 1)
    final int zzb;
    @SafeParcelable.Field(getter = "getInfo", id = 2)
    private zzw zzd;
    @SafeParcelable.Field(getter = "getSignature", id = 3)
    private String zze;
    @SafeParcelable.Field(getter = "getPackageName", id = 4)
    private String zzf;
    @SafeParcelable.Field(getter = "getId", id = 5)
    private String zzg;
    
    static {
        CREATOR = (Parcelable$Creator)new zzv();
        final HashMap zzc2 = new HashMap();
        (zzc = zzc2).put("authenticatorInfo", FastJsonResponse.Field.forConcreteType("authenticatorInfo", 2, zzw.class));
        zzc2.put("signature", FastJsonResponse.Field.forString("signature", 3));
        zzc2.put("package", FastJsonResponse.Field.forString("package", 4));
    }
    
    public zzu() {
        this.zza = new HashSet(3);
        this.zzb = 1;
    }
    
    @SafeParcelable.Constructor
    public zzu(@SafeParcelable.Indicator final Set zza, @SafeParcelable.Param(id = 1) final int zzb, @SafeParcelable.Param(id = 2) final zzw zzd, @SafeParcelable.Param(id = 3) final String zze, @SafeParcelable.Param(id = 4) final String zzf, @SafeParcelable.Param(id = 5) final String zzg) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
        this.zzg = zzg;
    }
    
    public final void addConcreteTypeInternal(final FastJsonResponse.Field field, final String s, final FastJsonResponse fastJsonResponse) {
        final int safeParcelableFieldId = field.getSafeParcelableFieldId();
        if (safeParcelableFieldId == 2) {
            this.zzd = (zzw)fastJsonResponse;
            this.zza.add(safeParcelableFieldId);
            return;
        }
        throw new IllegalArgumentException(String.format("Field with id=%d is not a known custom type. Found %s", safeParcelableFieldId, fastJsonResponse.getClass().getCanonicalName()));
    }
    
    public final Object getFieldValue(final FastJsonResponse.Field field) {
        final int safeParcelableFieldId = field.getSafeParcelableFieldId();
        if (safeParcelableFieldId == 1) {
            return this.zzb;
        }
        if (safeParcelableFieldId == 2) {
            return this.zzd;
        }
        if (safeParcelableFieldId == 3) {
            return this.zze;
        }
        if (safeParcelableFieldId == 4) {
            return this.zzf;
        }
        final int safeParcelableFieldId2 = field.getSafeParcelableFieldId();
        final StringBuilder sb = new StringBuilder();
        sb.append("Unknown SafeParcelable id=");
        sb.append(safeParcelableFieldId2);
        throw new IllegalStateException(sb.toString());
    }
    
    public final boolean isFieldSet(final FastJsonResponse.Field field) {
        return this.zza.contains(field.getSafeParcelableFieldId());
    }
    
    public final void setStringInternal(final FastJsonResponse.Field field, final String s, final String s2) {
        final int safeParcelableFieldId = field.getSafeParcelableFieldId();
        if (safeParcelableFieldId != 3) {
            if (safeParcelableFieldId != 4) {
                throw new IllegalArgumentException(String.format("Field with id=%d is not known to be a string.", safeParcelableFieldId));
            }
            this.zzf = s2;
        }
        else {
            this.zze = s2;
        }
        this.zza.add(safeParcelableFieldId);
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        final Set zza = this.zza;
        if (zza.contains(1)) {
            SafeParcelWriter.writeInt(parcel, 1, this.zzb);
        }
        if (zza.contains(2)) {
            SafeParcelWriter.writeParcelable(parcel, 2, (Parcelable)this.zzd, n, true);
        }
        if (zza.contains(3)) {
            SafeParcelWriter.writeString(parcel, 3, this.zze, true);
        }
        if (zza.contains(4)) {
            SafeParcelWriter.writeString(parcel, 4, this.zzf, true);
        }
        if (zza.contains(5)) {
            SafeParcelWriter.writeString(parcel, 5, this.zzg, true);
        }
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.identity;

import com.google.android.gms.internal.auth_api.zbbg;
import com.google.android.gms.internal.auth_api.zbaw;
import android.content.Context;
import com.google.android.gms.internal.auth_api.zbaq;
import com.google.android.gms.common.internal.Preconditions;
import android.app.Activity;

public final class Identity
{
    private Identity() {
    }
    
    public static AuthorizationClient getAuthorizationClient(final Activity activity) {
        return (AuthorizationClient)new zbaq((Activity)Preconditions.checkNotNull(activity), new zbb(null).zbb());
    }
    
    public static AuthorizationClient getAuthorizationClient(final Context context) {
        return (AuthorizationClient)new zbaq((Context)Preconditions.checkNotNull(context), new zbb(null).zbb());
    }
    
    public static CredentialSavingClient getCredentialSavingClient(final Activity activity) {
        return (CredentialSavingClient)new zbaw((Activity)Preconditions.checkNotNull(activity), new zbh());
    }
    
    public static CredentialSavingClient getCredentialSavingClient(final Context context) {
        return (CredentialSavingClient)new zbaw((Context)Preconditions.checkNotNull(context), new zbh());
    }
    
    public static SignInClient getSignInClient(final Activity activity) {
        return (SignInClient)new zbbg((Activity)Preconditions.checkNotNull(activity), new zbu());
    }
    
    public static SignInClient getSignInClient(final Context context) {
        return (SignInClient)new zbbg((Context)Preconditions.checkNotNull(context), new zbu());
    }
}

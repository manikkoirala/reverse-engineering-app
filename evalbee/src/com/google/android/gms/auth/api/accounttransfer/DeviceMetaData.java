// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.accounttransfer;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "DeviceMetaDataCreator")
public class DeviceMetaData extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<DeviceMetaData> CREATOR;
    @VersionField(id = 1)
    final int zza;
    @Field(getter = "isLockScreenSolved", id = 2)
    private boolean zzb;
    @Field(getter = "getMinAgeOfLockScreen", id = 3)
    private long zzc;
    @Field(getter = "isChallengeAllowed", id = 4)
    private final boolean zzd;
    
    static {
        CREATOR = (Parcelable$Creator)new zzy();
    }
    
    @Constructor
    public DeviceMetaData(@Param(id = 1) final int zza, @Param(id = 2) final boolean zzb, @Param(id = 3) final long zzc, @Param(id = 4) final boolean zzd) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        this.zzd = zzd;
    }
    
    public long getMinAgeOfLockScreen() {
        return this.zzc;
    }
    
    public boolean isChallengeAllowed() {
        return this.zzd;
    }
    
    public boolean isLockScreenSolved() {
        return this.zzb;
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.zza);
        SafeParcelWriter.writeBoolean(parcel, 2, this.isLockScreenSolved());
        SafeParcelWriter.writeLong(parcel, 3, this.getMinAgeOfLockScreen());
        SafeParcelWriter.writeBoolean(parcel, 4, this.isChallengeAllowed());
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.signin.internal;

import com.google.android.gms.common.api.Status;

final class zbh extends zba
{
    final zbi zba;
    
    public zbh(final zbi zba) {
        this.zba = zba;
    }
    
    @Override
    public final void zbc(final Status result) {
        this.zba.setResult(result);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.proxy;

import android.os.BaseBundle;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import java.util.HashMap;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import android.os.Bundle;
import android.app.PendingIntent;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.ShowFirstParty;
import com.google.android.gms.common.annotation.KeepForSdkWithMembers;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@KeepForSdkWithMembers
@ShowFirstParty
@Class(creator = "ProxyResponseCreator")
public class ProxyResponse extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<ProxyResponse> CREATOR;
    public static final int STATUS_CODE_NO_CONNECTION = -1;
    @Field(id = 5)
    public final byte[] body;
    @Field(id = 1)
    public final int googlePlayServicesStatusCode;
    @Field(id = 2)
    public final PendingIntent recoveryAction;
    @Field(id = 3)
    public final int statusCode;
    @VersionField(id = 1000)
    final int zza;
    @Field(id = 4)
    final Bundle zzb;
    
    static {
        CREATOR = (Parcelable$Creator)new zzb();
    }
    
    @Constructor
    public ProxyResponse(@Param(id = 1000) final int zza, @Param(id = 1) final int googlePlayServicesStatusCode, @Param(id = 2) final PendingIntent recoveryAction, @Param(id = 3) final int statusCode, @Param(id = 4) final Bundle zzb, @Param(id = 5) final byte[] body) {
        this.zza = zza;
        this.googlePlayServicesStatusCode = googlePlayServicesStatusCode;
        this.statusCode = statusCode;
        this.zzb = zzb;
        this.body = body;
        this.recoveryAction = recoveryAction;
    }
    
    public ProxyResponse(final int n, final PendingIntent pendingIntent, final int n2, final Bundle bundle, final byte[] array) {
        this(1, n, pendingIntent, n2, bundle, array);
    }
    
    public ProxyResponse(final int n, final Map<String, String> map, final byte[] array) {
        this(1, 0, null, n, zza(map), array);
    }
    
    public static ProxyResponse createErrorProxyResponse(final int n, final PendingIntent pendingIntent, final int n2, final Map<String, String> map, final byte[] array) {
        return new ProxyResponse(1, n, pendingIntent, n2, zza(map), array);
    }
    
    private static Bundle zza(final Map map) {
        final Bundle bundle = new Bundle();
        if (map == null) {
            return bundle;
        }
        for (final Map.Entry<String, V> entry : map.entrySet()) {
            ((BaseBundle)bundle).putString((String)entry.getKey(), (String)entry.getValue());
        }
        return bundle;
    }
    
    public Map<String, String> getHeaders() {
        if (this.zzb == null) {
            return Collections.emptyMap();
        }
        final HashMap hashMap = new HashMap();
        for (final String s : ((BaseBundle)this.zzb).keySet()) {
            hashMap.put(s, ((BaseBundle)this.zzb).getString(s));
        }
        return hashMap;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.googlePlayServicesStatusCode);
        SafeParcelWriter.writeParcelable(parcel, 2, (Parcelable)this.recoveryAction, n, false);
        SafeParcelWriter.writeInt(parcel, 3, this.statusCode);
        SafeParcelWriter.writeBundle(parcel, 4, this.zzb, false);
        SafeParcelWriter.writeByteArray(parcel, 5, this.body, false);
        SafeParcelWriter.writeInt(parcel, 1000, this.zza);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

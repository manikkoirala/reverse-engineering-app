// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.identity;

import android.app.PendingIntent;
import android.content.Intent;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.common.api.HasApiKey;

public interface SignInClient extends HasApiKey<zbu>
{
    Task<BeginSignInResult> beginSignIn(final BeginSignInRequest p0);
    
    String getPhoneNumberFromIntent(final Intent p0);
    
    Task<PendingIntent> getPhoneNumberHintIntent(final GetPhoneNumberHintIntentRequest p0);
    
    SignInCredential getSignInCredentialFromIntent(final Intent p0);
    
    Task<PendingIntent> getSignInIntent(final GetSignInIntentRequest p0);
    
    Task<Void> signOut();
}

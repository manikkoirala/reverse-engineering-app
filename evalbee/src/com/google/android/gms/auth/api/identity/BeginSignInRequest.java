// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.identity;

import java.util.Arrays;
import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "BeginSignInRequestCreator")
public final class BeginSignInRequest extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<BeginSignInRequest> CREATOR;
    @Field(getter = "getPasswordRequestOptions", id = 1)
    private final PasswordRequestOptions zba;
    @Field(getter = "getGoogleIdTokenRequestOptions", id = 2)
    private final GoogleIdTokenRequestOptions zbb;
    @Field(getter = "getSessionId", id = 3)
    private final String zbc;
    @Field(getter = "isAutoSelectEnabled", id = 4)
    private final boolean zbd;
    @Field(getter = "getTheme", id = 5)
    private final int zbe;
    @Field(getter = "getPasskeysRequestOptions", id = 6)
    private final PasskeysRequestOptions zbf;
    @Field(getter = "getPasskeyJsonRequestOptions", id = 7)
    private final PasskeyJsonRequestOptions zbg;
    
    static {
        CREATOR = (Parcelable$Creator)new zbf();
    }
    
    @Constructor
    public BeginSignInRequest(@Param(id = 1) final PasswordRequestOptions passwordRequestOptions, @Param(id = 2) final GoogleIdTokenRequestOptions googleIdTokenRequestOptions, @Param(id = 3) final String zbc, @Param(id = 4) final boolean zbd, @Param(id = 5) final int zbe, @Param(id = 6) final PasskeysRequestOptions passkeysRequestOptions, @Param(id = 7) final PasskeyJsonRequestOptions passkeyJsonRequestOptions) {
        this.zba = Preconditions.checkNotNull(passwordRequestOptions);
        this.zbb = Preconditions.checkNotNull(googleIdTokenRequestOptions);
        this.zbc = zbc;
        this.zbd = zbd;
        this.zbe = zbe;
        PasskeysRequestOptions build = passkeysRequestOptions;
        if (passkeysRequestOptions == null) {
            final PasskeysRequestOptions.Builder builder = PasskeysRequestOptions.builder();
            builder.setSupported(false);
            build = builder.build();
        }
        this.zbf = build;
        PasskeyJsonRequestOptions build2;
        if ((build2 = passkeyJsonRequestOptions) == null) {
            final PasskeyJsonRequestOptions.Builder builder2 = PasskeyJsonRequestOptions.builder();
            builder2.setSupported(false);
            build2 = builder2.build();
        }
        this.zbg = build2;
    }
    
    public static Builder builder() {
        return new Builder();
    }
    
    public static Builder zba(final BeginSignInRequest beginSignInRequest) {
        Preconditions.checkNotNull(beginSignInRequest);
        final Builder builder = builder();
        builder.setGoogleIdTokenRequestOptions(beginSignInRequest.getGoogleIdTokenRequestOptions());
        builder.setPasswordRequestOptions(beginSignInRequest.getPasswordRequestOptions());
        builder.setPasskeysSignInRequestOptions(beginSignInRequest.getPasskeysRequestOptions());
        builder.setPasskeyJsonSignInRequestOptions(beginSignInRequest.getPasskeyJsonRequestOptions());
        builder.setAutoSelectEnabled(beginSignInRequest.zbd);
        builder.zbb(beginSignInRequest.zbe);
        final String zbc = beginSignInRequest.zbc;
        if (zbc != null) {
            builder.zba(zbc);
        }
        return builder;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof BeginSignInRequest)) {
            return false;
        }
        final BeginSignInRequest beginSignInRequest = (BeginSignInRequest)o;
        return Objects.equal(this.zba, beginSignInRequest.zba) && Objects.equal(this.zbb, beginSignInRequest.zbb) && Objects.equal(this.zbf, beginSignInRequest.zbf) && Objects.equal(this.zbg, beginSignInRequest.zbg) && Objects.equal(this.zbc, beginSignInRequest.zbc) && this.zbd == beginSignInRequest.zbd && this.zbe == beginSignInRequest.zbe;
    }
    
    public GoogleIdTokenRequestOptions getGoogleIdTokenRequestOptions() {
        return this.zbb;
    }
    
    public PasskeyJsonRequestOptions getPasskeyJsonRequestOptions() {
        return this.zbg;
    }
    
    public PasskeysRequestOptions getPasskeysRequestOptions() {
        return this.zbf;
    }
    
    public PasswordRequestOptions getPasswordRequestOptions() {
        return this.zba;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zba, this.zbb, this.zbf, this.zbg, this.zbc, this.zbd);
    }
    
    public boolean isAutoSelectEnabled() {
        return this.zbd;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeParcelable(parcel, 1, (Parcelable)this.getPasswordRequestOptions(), n, false);
        SafeParcelWriter.writeParcelable(parcel, 2, (Parcelable)this.getGoogleIdTokenRequestOptions(), n, false);
        SafeParcelWriter.writeString(parcel, 3, this.zbc, false);
        SafeParcelWriter.writeBoolean(parcel, 4, this.isAutoSelectEnabled());
        SafeParcelWriter.writeInt(parcel, 5, this.zbe);
        SafeParcelWriter.writeParcelable(parcel, 6, (Parcelable)this.getPasskeysRequestOptions(), n, false);
        SafeParcelWriter.writeParcelable(parcel, 7, (Parcelable)this.getPasskeyJsonRequestOptions(), n, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public static final class Builder
    {
        private PasswordRequestOptions zba;
        private GoogleIdTokenRequestOptions zbb;
        private PasskeysRequestOptions zbc;
        private PasskeyJsonRequestOptions zbd;
        private String zbe;
        private boolean zbf;
        private int zbg;
        
        public Builder() {
            final PasswordRequestOptions.Builder builder = PasswordRequestOptions.builder();
            builder.setSupported(false);
            this.zba = builder.build();
            final GoogleIdTokenRequestOptions.Builder builder2 = GoogleIdTokenRequestOptions.builder();
            builder2.setSupported(false);
            this.zbb = builder2.build();
            final PasskeysRequestOptions.Builder builder3 = PasskeysRequestOptions.builder();
            builder3.setSupported(false);
            this.zbc = builder3.build();
            final PasskeyJsonRequestOptions.Builder builder4 = PasskeyJsonRequestOptions.builder();
            builder4.setSupported(false);
            this.zbd = builder4.build();
        }
        
        public BeginSignInRequest build() {
            return new BeginSignInRequest(this.zba, this.zbb, this.zbe, this.zbf, this.zbg, this.zbc, this.zbd);
        }
        
        public Builder setAutoSelectEnabled(final boolean zbf) {
            this.zbf = zbf;
            return this;
        }
        
        public Builder setGoogleIdTokenRequestOptions(final GoogleIdTokenRequestOptions googleIdTokenRequestOptions) {
            this.zbb = Preconditions.checkNotNull(googleIdTokenRequestOptions);
            return this;
        }
        
        public Builder setPasskeyJsonSignInRequestOptions(final PasskeyJsonRequestOptions passkeyJsonRequestOptions) {
            this.zbd = Preconditions.checkNotNull(passkeyJsonRequestOptions);
            return this;
        }
        
        @Deprecated
        public Builder setPasskeysSignInRequestOptions(final PasskeysRequestOptions passkeysRequestOptions) {
            this.zbc = Preconditions.checkNotNull(passkeysRequestOptions);
            return this;
        }
        
        public Builder setPasswordRequestOptions(final PasswordRequestOptions passwordRequestOptions) {
            this.zba = Preconditions.checkNotNull(passwordRequestOptions);
            return this;
        }
        
        public final Builder zba(final String zbe) {
            this.zbe = zbe;
            return this;
        }
        
        public final Builder zbb(final int zbg) {
            this.zbg = zbg;
            return this;
        }
    }
    
    @Class(creator = "GoogleIdTokenRequestOptionsCreator")
    public static final class GoogleIdTokenRequestOptions extends AbstractSafeParcelable
    {
        public static final Parcelable$Creator<GoogleIdTokenRequestOptions> CREATOR;
        @Field(getter = "isSupported", id = 1)
        private final boolean zba;
        @Field(getter = "getServerClientId", id = 2)
        private final String zbb;
        @Field(getter = "getNonce", id = 3)
        private final String zbc;
        @Field(getter = "filterByAuthorizedAccounts", id = 4)
        private final boolean zbd;
        @Field(getter = "getLinkedServiceId", id = 5)
        private final String zbe;
        @Field(getter = "getIdTokenDepositionScopes", id = 6)
        private final List zbf;
        @Field(getter = "requestVerifiedPhoneNumber", id = 7)
        private final boolean zbg;
        
        static {
            CREATOR = (Parcelable$Creator)new zbl();
        }
        
        @Constructor
        public GoogleIdTokenRequestOptions(@Param(id = 1) final boolean zba, @Param(id = 2) final String zbb, @Param(id = 3) final String zbc, @Param(id = 4) final boolean zbd, @Param(id = 5) final String zbe, @Param(id = 6) final List c, @Param(id = 7) final boolean zbg) {
            boolean b = true;
            if (zbd) {
                b = (!zbg && b);
            }
            Preconditions.checkArgument(b, (Object)"filterByAuthorizedAccounts and requestVerifiedPhoneNumber must not both be true; the Verified Phone Number feature only works in sign-ups.");
            this.zba = zba;
            if (zba) {
                Preconditions.checkNotNull(zbb, "serverClientId must be provided if Google ID tokens are requested");
            }
            this.zbb = zbb;
            this.zbc = zbc;
            this.zbd = zbd;
            final Parcelable$Creator<BeginSignInRequest> creator = BeginSignInRequest.CREATOR;
            List<Comparable> list2;
            final List<Comparable> list = list2 = null;
            if (c != null) {
                if (c.isEmpty()) {
                    list2 = list;
                }
                else {
                    list2 = new ArrayList<Comparable>(c);
                    Collections.sort(list2);
                }
            }
            this.zbf = list2;
            this.zbe = zbe;
            this.zbg = zbg;
        }
        
        public static Builder builder() {
            return new Builder();
        }
        
        @Override
        public boolean equals(final Object o) {
            if (!(o instanceof GoogleIdTokenRequestOptions)) {
                return false;
            }
            final GoogleIdTokenRequestOptions googleIdTokenRequestOptions = (GoogleIdTokenRequestOptions)o;
            return this.zba == googleIdTokenRequestOptions.zba && Objects.equal(this.zbb, googleIdTokenRequestOptions.zbb) && Objects.equal(this.zbc, googleIdTokenRequestOptions.zbc) && this.zbd == googleIdTokenRequestOptions.zbd && Objects.equal(this.zbe, googleIdTokenRequestOptions.zbe) && Objects.equal(this.zbf, googleIdTokenRequestOptions.zbf) && this.zbg == googleIdTokenRequestOptions.zbg;
        }
        
        public boolean filterByAuthorizedAccounts() {
            return this.zbd;
        }
        
        public List<String> getIdTokenDepositionScopes() {
            return this.zbf;
        }
        
        public String getLinkedServiceId() {
            return this.zbe;
        }
        
        public String getNonce() {
            return this.zbc;
        }
        
        public String getServerClientId() {
            return this.zbb;
        }
        
        @Override
        public int hashCode() {
            return Objects.hashCode(this.zba, this.zbb, this.zbc, this.zbd, this.zbe, this.zbf, this.zbg);
        }
        
        public boolean isSupported() {
            return this.zba;
        }
        
        @Deprecated
        public boolean requestVerifiedPhoneNumber() {
            return this.zbg;
        }
        
        public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
            beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
            SafeParcelWriter.writeBoolean(parcel, 1, this.isSupported());
            SafeParcelWriter.writeString(parcel, 2, this.getServerClientId(), false);
            SafeParcelWriter.writeString(parcel, 3, this.getNonce(), false);
            SafeParcelWriter.writeBoolean(parcel, 4, this.filterByAuthorizedAccounts());
            SafeParcelWriter.writeString(parcel, 5, this.getLinkedServiceId(), false);
            SafeParcelWriter.writeStringList(parcel, 6, this.getIdTokenDepositionScopes(), false);
            SafeParcelWriter.writeBoolean(parcel, 7, this.requestVerifiedPhoneNumber());
            SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
        }
        
        public static final class Builder
        {
            private boolean zba;
            private String zbb;
            private String zbc;
            private boolean zbd;
            private String zbe;
            private List zbf;
            private boolean zbg;
            
            public Builder() {
                this.zba = false;
                this.zbb = null;
                this.zbc = null;
                this.zbd = true;
                this.zbe = null;
                this.zbf = null;
                this.zbg = false;
            }
            
            public Builder associateLinkedAccounts(final String s, final List<String> zbf) {
                this.zbe = Preconditions.checkNotNull(s, "linkedServiceId must be provided if you want to associate linked accounts.");
                this.zbf = zbf;
                return this;
            }
            
            public GoogleIdTokenRequestOptions build() {
                return new GoogleIdTokenRequestOptions(this.zba, this.zbb, this.zbc, this.zbd, this.zbe, this.zbf, this.zbg);
            }
            
            public Builder setFilterByAuthorizedAccounts(final boolean zbd) {
                this.zbd = zbd;
                return this;
            }
            
            public Builder setNonce(final String zbc) {
                this.zbc = zbc;
                return this;
            }
            
            @Deprecated
            public Builder setRequestVerifiedPhoneNumber(final boolean zbg) {
                this.zbg = zbg;
                return this;
            }
            
            public Builder setServerClientId(final String s) {
                this.zbb = Preconditions.checkNotEmpty(s);
                return this;
            }
            
            public Builder setSupported(final boolean zba) {
                this.zba = zba;
                return this;
            }
        }
    }
    
    @Class(creator = "PasskeyJsonRequestOptionsCreator")
    public static final class PasskeyJsonRequestOptions extends AbstractSafeParcelable
    {
        public static final Parcelable$Creator<PasskeyJsonRequestOptions> CREATOR;
        @Field(getter = "isSupported", id = 1)
        private final boolean zba;
        @Field(getter = "getRequestJson", id = 2)
        private final String zbb;
        
        static {
            CREATOR = (Parcelable$Creator)new zbm();
        }
        
        @Constructor
        public PasskeyJsonRequestOptions(@Param(id = 1) final boolean zba, @Param(id = 2) final String zbb) {
            if (zba) {
                Preconditions.checkNotNull(zbb);
            }
            this.zba = zba;
            this.zbb = zbb;
        }
        
        public static Builder builder() {
            return new Builder();
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof PasskeyJsonRequestOptions)) {
                return false;
            }
            final PasskeyJsonRequestOptions passkeyJsonRequestOptions = (PasskeyJsonRequestOptions)o;
            return this.zba == passkeyJsonRequestOptions.zba && Objects.equal(this.zbb, passkeyJsonRequestOptions.zbb);
        }
        
        public String getRequestJson() {
            return this.zbb;
        }
        
        @Override
        public int hashCode() {
            return Objects.hashCode(this.zba, this.zbb);
        }
        
        public boolean isSupported() {
            return this.zba;
        }
        
        public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
            beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
            SafeParcelWriter.writeBoolean(parcel, 1, this.isSupported());
            SafeParcelWriter.writeString(parcel, 2, this.getRequestJson(), false);
            SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
        }
        
        public static final class Builder
        {
            private boolean zba;
            private String zbb;
            
            public Builder() {
                this.zba = false;
            }
            
            public PasskeyJsonRequestOptions build() {
                return new PasskeyJsonRequestOptions(this.zba, this.zbb);
            }
            
            public Builder setRequestJson(final String zbb) {
                this.zbb = zbb;
                return this;
            }
            
            public Builder setSupported(final boolean zba) {
                this.zba = zba;
                return this;
            }
        }
    }
    
    @Deprecated
    @Class(creator = "PasskeysRequestOptionsCreator")
    public static final class PasskeysRequestOptions extends AbstractSafeParcelable
    {
        public static final Parcelable$Creator<PasskeysRequestOptions> CREATOR;
        @Field(getter = "isSupported", id = 1)
        private final boolean zba;
        @Field(getter = "getChallenge", id = 2)
        private final byte[] zbb;
        @Field(getter = "getRpId", id = 3)
        private final String zbc;
        
        static {
            CREATOR = (Parcelable$Creator)new zbn();
        }
        
        @Constructor
        public PasskeysRequestOptions(@Param(id = 1) final boolean zba, @Param(id = 2) final byte[] zbb, @Param(id = 3) final String zbc) {
            if (zba) {
                Preconditions.checkNotNull(zbb);
                Preconditions.checkNotNull(zbc);
            }
            this.zba = zba;
            this.zbb = zbb;
            this.zbc = zbc;
        }
        
        public static Builder builder() {
            return new Builder();
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof PasskeysRequestOptions)) {
                return false;
            }
            final PasskeysRequestOptions passkeysRequestOptions = (PasskeysRequestOptions)o;
            if (this.zba == passkeysRequestOptions.zba && Arrays.equals(this.zbb, passkeysRequestOptions.zbb)) {
                final String zbc = this.zbc;
                final String zbc2 = passkeysRequestOptions.zbc;
                if (zbc == zbc2 || (zbc != null && zbc.equals(zbc2))) {
                    return true;
                }
            }
            return false;
        }
        
        public byte[] getChallenge() {
            return this.zbb;
        }
        
        public String getRpId() {
            return this.zbc;
        }
        
        @Override
        public int hashCode() {
            return Arrays.hashCode(new Object[] { this.zba, this.zbc }) * 31 + Arrays.hashCode(this.zbb);
        }
        
        public boolean isSupported() {
            return this.zba;
        }
        
        public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
            beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
            SafeParcelWriter.writeBoolean(parcel, 1, this.isSupported());
            SafeParcelWriter.writeByteArray(parcel, 2, this.getChallenge(), false);
            SafeParcelWriter.writeString(parcel, 3, this.getRpId(), false);
            SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
        }
        
        public static final class Builder
        {
            private boolean zba;
            private byte[] zbb;
            private String zbc;
            
            public Builder() {
                this.zba = false;
            }
            
            public PasskeysRequestOptions build() {
                return new PasskeysRequestOptions(this.zba, this.zbb, this.zbc);
            }
            
            public Builder setChallenge(final byte[] zbb) {
                this.zbb = zbb;
                return this;
            }
            
            public Builder setRpId(final String zbc) {
                this.zbc = zbc;
                return this;
            }
            
            public Builder setSupported(final boolean zba) {
                this.zba = zba;
                return this;
            }
        }
    }
    
    @Class(creator = "PasswordRequestOptionsCreator")
    public static final class PasswordRequestOptions extends AbstractSafeParcelable
    {
        public static final Parcelable$Creator<PasswordRequestOptions> CREATOR;
        @Field(getter = "isSupported", id = 1)
        private final boolean zba;
        
        static {
            CREATOR = (Parcelable$Creator)new zbo();
        }
        
        @Constructor
        public PasswordRequestOptions(@Param(id = 1) final boolean zba) {
            this.zba = zba;
        }
        
        public static Builder builder() {
            return new Builder();
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof PasswordRequestOptions && this.zba == ((PasswordRequestOptions)o).zba;
        }
        
        @Override
        public int hashCode() {
            return Objects.hashCode(this.zba);
        }
        
        public boolean isSupported() {
            return this.zba;
        }
        
        public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
            beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
            SafeParcelWriter.writeBoolean(parcel, 1, this.isSupported());
            SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
        }
        
        public static final class Builder
        {
            private boolean zba;
            
            public Builder() {
                this.zba = false;
            }
            
            public PasswordRequestOptions build() {
                return new PasswordRequestOptions(this.zba);
            }
            
            public Builder setSupported(final boolean zba) {
                this.zba = zba;
                return this;
            }
        }
    }
}

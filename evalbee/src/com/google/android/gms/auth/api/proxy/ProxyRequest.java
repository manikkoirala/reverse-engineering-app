// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.proxy;

import android.os.BaseBundle;
import android.util.Patterns;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import java.util.Iterator;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import android.os.Bundle;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.ShowFirstParty;
import com.google.android.gms.common.annotation.KeepForSdkWithMembers;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@KeepForSdkWithMembers
@ShowFirstParty
@Class(creator = "ProxyRequestCreator")
public class ProxyRequest extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<ProxyRequest> CREATOR;
    public static final int HTTP_METHOD_DELETE;
    public static final int HTTP_METHOD_GET;
    public static final int HTTP_METHOD_HEAD;
    public static final int HTTP_METHOD_OPTIONS;
    public static final int HTTP_METHOD_PATCH;
    public static final int HTTP_METHOD_POST;
    public static final int HTTP_METHOD_PUT;
    public static final int HTTP_METHOD_TRACE;
    public static final int LAST_CODE;
    public static final int VERSION_CODE = 2;
    @Field(id = 4)
    public final byte[] body;
    @Field(id = 2)
    public final int httpMethod;
    @Field(id = 3)
    public final long timeoutMillis;
    @Field(id = 1)
    public final String url;
    @VersionField(id = 1000)
    final int zza;
    @Field(id = 5)
    Bundle zzb;
    
    static {
        CREATOR = (Parcelable$Creator)new zza();
        HTTP_METHOD_GET = 0;
        HTTP_METHOD_POST = 1;
        HTTP_METHOD_PUT = 2;
        HTTP_METHOD_DELETE = 3;
        HTTP_METHOD_HEAD = 4;
        HTTP_METHOD_OPTIONS = 5;
        HTTP_METHOD_TRACE = 6;
        HTTP_METHOD_PATCH = 7;
        LAST_CODE = 7;
    }
    
    @Constructor
    public ProxyRequest(@Param(id = 1000) final int zza, @Param(id = 1) final String url, @Param(id = 2) final int httpMethod, @Param(id = 3) final long timeoutMillis, @Param(id = 4) final byte[] body, @Param(id = 5) final Bundle zzb) {
        this.zza = zza;
        this.url = url;
        this.httpMethod = httpMethod;
        this.timeoutMillis = timeoutMillis;
        this.body = body;
        this.zzb = zzb;
    }
    
    public Map<String, String> getHeaderMap() {
        final LinkedHashMap m = new LinkedHashMap(((BaseBundle)this.zzb).size());
        for (final String s : ((BaseBundle)this.zzb).keySet()) {
            String string;
            if ((string = ((BaseBundle)this.zzb).getString(s)) == null) {
                string = "";
            }
            m.put(s, string);
        }
        return (Map<String, String>)Collections.unmodifiableMap((Map<?, ?>)m);
    }
    
    @Override
    public String toString() {
        final String url = this.url;
        final int httpMethod = this.httpMethod;
        final StringBuilder sb = new StringBuilder();
        sb.append("ProxyRequest[ url: ");
        sb.append(url);
        sb.append(", method: ");
        sb.append(httpMethod);
        sb.append(" ]");
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.url, false);
        SafeParcelWriter.writeInt(parcel, 2, this.httpMethod);
        SafeParcelWriter.writeLong(parcel, 3, this.timeoutMillis);
        SafeParcelWriter.writeByteArray(parcel, 4, this.body, false);
        SafeParcelWriter.writeBundle(parcel, 5, this.zzb, false);
        SafeParcelWriter.writeInt(parcel, 1000, this.zza);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    @KeepForSdkWithMembers
    @ShowFirstParty
    public static class Builder
    {
        private final String zza;
        private int zzb;
        private long zzc;
        private byte[] zzd;
        private final Bundle zze;
        
        public Builder(final String str) {
            this.zzb = ProxyRequest.HTTP_METHOD_GET;
            this.zzc = 3000L;
            this.zzd = new byte[0];
            this.zze = new Bundle();
            Preconditions.checkNotEmpty(str);
            if (Patterns.WEB_URL.matcher(str).matches()) {
                this.zza = str;
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("The supplied url [ ");
            sb.append(str);
            sb.append("] is not match Patterns.WEB_URL!");
            throw new IllegalArgumentException(sb.toString());
        }
        
        public ProxyRequest build() {
            if (this.zzd == null) {
                this.zzd = new byte[0];
            }
            return new ProxyRequest(2, this.zza, this.zzb, this.zzc, this.zzd, this.zze);
        }
        
        public Builder putHeader(final String s, final String s2) {
            Preconditions.checkNotEmpty(s, "Header name cannot be null or empty!");
            final Bundle zze = this.zze;
            String s3 = s2;
            if (s2 == null) {
                s3 = "";
            }
            ((BaseBundle)zze).putString(s, s3);
            return this;
        }
        
        public Builder setBody(final byte[] zzd) {
            this.zzd = zzd;
            return this;
        }
        
        public Builder setHttpMethod(final int zzb) {
            boolean b = false;
            if (zzb >= 0) {
                b = b;
                if (zzb <= ProxyRequest.LAST_CODE) {
                    b = true;
                }
            }
            Preconditions.checkArgument(b, (Object)"Unrecognized http method code.");
            this.zzb = zzb;
            return this;
        }
        
        public Builder setTimeoutMillis(final long zzc) {
            Preconditions.checkArgument(zzc >= 0L, (Object)"The specified timeout must be non-negative.");
            this.zzc = zzc;
            return this;
        }
    }
}

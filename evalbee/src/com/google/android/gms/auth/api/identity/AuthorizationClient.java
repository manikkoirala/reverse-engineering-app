// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.identity;

import android.content.Intent;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.common.api.HasApiKey;

public interface AuthorizationClient extends HasApiKey<zbc>
{
    Task<AuthorizationResult> authorize(final AuthorizationRequest p0);
    
    AuthorizationResult getAuthorizationResultFromIntent(final Intent p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.signin.internal;

import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import android.content.Intent;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.auth.api.signin.GoogleSignInApi;

public final class zbd implements GoogleSignInApi
{
    private static final GoogleSignInOptions zba(final GoogleApiClient googleApiClient) {
        return googleApiClient.getClient((Api.AnyClientKey<zbe>)Auth.zbb).zba();
    }
    
    @Override
    public final Intent getSignInIntent(final GoogleApiClient googleApiClient) {
        return zbm.zbc(googleApiClient.getContext(), zba(googleApiClient));
    }
    
    @Override
    public final GoogleSignInResult getSignInResultFromIntent(final Intent intent) {
        return zbm.zbd(intent);
    }
    
    @Override
    public final PendingResult<Status> revokeAccess(final GoogleApiClient googleApiClient) {
        return zbm.zbf(googleApiClient, googleApiClient.getContext(), false);
    }
    
    @Override
    public final PendingResult<Status> signOut(final GoogleApiClient googleApiClient) {
        return zbm.zbg(googleApiClient, googleApiClient.getContext(), false);
    }
    
    @Override
    public final OptionalPendingResult<GoogleSignInResult> silentSignIn(final GoogleApiClient googleApiClient) {
        return zbm.zbe(googleApiClient, googleApiClient.getContext(), zba(googleApiClient), false);
    }
}

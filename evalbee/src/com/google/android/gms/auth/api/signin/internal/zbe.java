// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.signin.internal;

import android.content.Intent;
import android.os.IInterface;
import android.os.IBinder;
import java.util.Iterator;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.internal.auth_api.zbbj;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.ClientSettings;
import android.os.Looper;
import android.content.Context;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.internal.GmsClient;

public final class zbe extends GmsClient
{
    private final GoogleSignInOptions zba;
    
    public zbe(final Context context, final Looper looper, final ClientSettings clientSettings, final GoogleSignInOptions googleSignInOptions, final GoogleApiClient.ConnectionCallbacks connectionCallbacks, final GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, 91, clientSettings, connectionCallbacks, onConnectionFailedListener);
        GoogleSignInOptions.Builder builder;
        if (googleSignInOptions != null) {
            builder = new GoogleSignInOptions.Builder(googleSignInOptions);
        }
        else {
            builder = new GoogleSignInOptions.Builder();
        }
        builder.setLogSessionId(zbbj.zba());
        if (!clientSettings.getAllRequestedScopes().isEmpty()) {
            final Iterator<Scope> iterator = clientSettings.getAllRequestedScopes().iterator();
            while (iterator.hasNext()) {
                builder.requestScopes(iterator.next(), new Scope[0]);
            }
        }
        this.zba = builder.build();
    }
    
    @Override
    public final int getMinApkVersion() {
        return 12451000;
    }
    
    @Override
    public final String getServiceDescriptor() {
        return "com.google.android.gms.auth.api.signin.internal.ISignInService";
    }
    
    @Override
    public final Intent getSignInIntent() {
        return zbm.zbc(this.getContext(), this.zba);
    }
    
    @Override
    public final String getStartServiceAction() {
        return "com.google.android.gms.auth.api.signin.service.START";
    }
    
    @Override
    public final boolean providesSignIn() {
        return true;
    }
    
    public final GoogleSignInOptions zba() {
        return this.zba;
    }
}

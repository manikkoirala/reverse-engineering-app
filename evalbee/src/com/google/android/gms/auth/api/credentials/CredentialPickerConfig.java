// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.credentials;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Deprecated
@Class(creator = "CredentialPickerConfigCreator")
public final class CredentialPickerConfig extends AbstractSafeParcelable implements ReflectedParcelable
{
    public static final Parcelable$Creator<CredentialPickerConfig> CREATOR;
    @Field(id = 1000)
    final int zba;
    @Field(getter = "shouldShowAddAccountButton", id = 1)
    private final boolean zbb;
    @Field(getter = "shouldShowCancelButton", id = 2)
    private final boolean zbc;
    @Field(getter = "getPromptInternalId", id = 4)
    private final int zbd;
    
    static {
        CREATOR = (Parcelable$Creator)new zbb();
    }
    
    @Constructor
    public CredentialPickerConfig(@Param(id = 1000) int n, @Param(id = 1) final boolean zbb, @Param(id = 2) final boolean zbc, @Param(id = 3) final boolean b, @Param(id = 4) final int zbd) {
        this.zba = n;
        this.zbb = zbb;
        this.zbc = zbc;
        if (n < 2) {
            n = 1;
            if (b) {
                n = 3;
            }
            this.zbd = n;
            return;
        }
        this.zbd = zbd;
    }
    
    @Deprecated
    public boolean isForNewAccount() {
        return this.zbd == 3;
    }
    
    public boolean shouldShowAddAccountButton() {
        return this.zbb;
    }
    
    public boolean shouldShowCancelButton() {
        return this.zbc;
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeBoolean(parcel, 1, this.shouldShowAddAccountButton());
        SafeParcelWriter.writeBoolean(parcel, 2, this.shouldShowCancelButton());
        SafeParcelWriter.writeBoolean(parcel, 3, this.isForNewAccount());
        SafeParcelWriter.writeInt(parcel, 4, this.zbd);
        SafeParcelWriter.writeInt(parcel, 1000, this.zba);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public static class Builder
    {
        private boolean zba;
        private boolean zbb;
        private int zbc;
        
        public Builder() {
            this.zba = false;
            this.zbb = true;
            this.zbc = 1;
        }
        
        public CredentialPickerConfig build() {
            return new CredentialPickerConfig(2, this.zba, this.zbb, false, this.zbc);
        }
        
        @Deprecated
        public Builder setForNewAccount(final boolean b) {
            int zbc = 1;
            if (b) {
                zbc = 3;
            }
            this.zbc = zbc;
            return this;
        }
        
        public Builder setPrompt(final int zbc) {
            this.zbc = zbc;
            return this;
        }
        
        public Builder setShowAddAccountButton(final boolean zba) {
            this.zba = zba;
            return this;
        }
        
        public Builder setShowCancelButton(final boolean zbb) {
            this.zbb = zbb;
            return this;
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface Prompt {
        public static final int CONTINUE = 1;
        public static final int SIGN_IN = 2;
        public static final int SIGN_UP = 3;
    }
}

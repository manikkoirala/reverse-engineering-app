// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.accounttransfer;

import com.google.android.gms.internal.auth.zzbb;
import android.app.PendingIntent;
import com.google.android.gms.internal.auth.zzaz;
import com.google.android.gms.internal.auth.zzax;
import com.google.android.gms.internal.auth.zzav;
import com.google.android.gms.common.api.internal.TaskApiCall;
import com.google.android.gms.internal.auth.zzaq;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.tasks.Task;
import android.content.Context;
import com.google.android.gms.common.api.internal.StatusExceptionMapper;
import com.google.android.gms.common.api.internal.ApiExceptionMapper;
import android.app.Activity;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApi;

public class AccountTransferClient extends GoogleApi<zzr>
{
    public static final int zza = 0;
    private static final Api.ClientKey zzb;
    private static final Api.AbstractClientBuilder zzc;
    private static final Api zzd;
    
    static {
        zzd = new Api("AccountTransfer.ACCOUNT_TRANSFER_API", zzc = new zzb(), zzb = new Api.ClientKey());
    }
    
    public AccountTransferClient(final Activity activity, final zzr zzr) {
        super(activity, AccountTransferClient.zzd, (Api.ApiOptions)zzr.zza, new Settings.Builder().setMapper(new ApiExceptionMapper()).build());
    }
    
    public AccountTransferClient(final Context context, final zzr zzr) {
        super(context, AccountTransferClient.zzd, (Api.ApiOptions)zzr.zza, new Settings.Builder().setMapper(new ApiExceptionMapper()).build());
    }
    
    public Task<DeviceMetaData> getDeviceMetaData(final String s) {
        Preconditions.checkNotNull(s);
        return this.doRead((TaskApiCall<Api.AnyClient, DeviceMetaData>)new zzg(this, 1608, new zzaq(s)));
    }
    
    public Task<Void> notifyCompletion(final String s, final int n) {
        Preconditions.checkNotNull(s);
        return this.doWrite((TaskApiCall<Api.AnyClient, Void>)new zzi(this, 1610, new zzav(s, n)));
    }
    
    public Task<byte[]> retrieveData(final String s) {
        Preconditions.checkNotNull(s);
        return this.doRead((TaskApiCall<Api.AnyClient, byte[]>)new zze(this, 1607, new zzax(s)));
    }
    
    public Task<Void> sendData(final String s, final byte[] array) {
        Preconditions.checkNotNull(s);
        Preconditions.checkNotNull(array);
        return this.doWrite((TaskApiCall<Api.AnyClient, Void>)new zzc(this, 1606, new zzaz(s, array)));
    }
    
    public Task<Void> showUserChallenge(final String s, final PendingIntent pendingIntent) {
        Preconditions.checkNotNull(s);
        Preconditions.checkNotNull(pendingIntent);
        return this.doWrite((TaskApiCall<Api.AnyClient, Void>)new zzh(this, 1609, new zzbb(s, pendingIntent)));
    }
}

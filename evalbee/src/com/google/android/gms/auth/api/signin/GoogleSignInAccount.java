// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.signin;

import org.json.JSONException;
import java.util.Comparator;
import java.util.Arrays;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import java.util.Collections;
import org.json.JSONArray;
import org.json.JSONObject;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Collection;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.annotation.KeepForSdk;
import android.accounts.Account;
import java.util.HashSet;
import com.google.android.gms.common.util.DefaultClock;
import java.util.Set;
import android.net.Uri;
import com.google.android.gms.common.api.Scope;
import java.util.List;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.common.util.Clock;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "GoogleSignInAccountCreator")
public class GoogleSignInAccount extends AbstractSafeParcelable implements ReflectedParcelable
{
    public static final Parcelable$Creator<GoogleSignInAccount> CREATOR;
    @VisibleForTesting
    public static Clock zaa;
    @VersionField(id = 1)
    final int zab;
    @Field(id = 10)
    List<Scope> zac;
    @Field(getter = "getId", id = 2)
    private String zad;
    @Field(getter = "getIdToken", id = 3)
    private String zae;
    @Field(getter = "getEmail", id = 4)
    private String zaf;
    @Field(getter = "getDisplayName", id = 5)
    private String zag;
    @Field(getter = "getPhotoUrl", id = 6)
    private Uri zah;
    @Field(getter = "getServerAuthCode", id = 7)
    private String zai;
    @Field(getter = "getExpirationTimeSecs", id = 8)
    private long zaj;
    @Field(getter = "getObfuscatedIdentifier", id = 9)
    private String zak;
    @Field(getter = "getGivenName", id = 11)
    private String zal;
    @Field(getter = "getFamilyName", id = 12)
    private String zam;
    private Set<Scope> zan;
    
    static {
        CREATOR = (Parcelable$Creator)new zab();
        GoogleSignInAccount.zaa = DefaultClock.getInstance();
    }
    
    @Constructor
    public GoogleSignInAccount(@Param(id = 1) final int zab, @Param(id = 2) final String zad, @Param(id = 3) final String zae, @Param(id = 4) final String zaf, @Param(id = 5) final String zag, @Param(id = 6) final Uri zah, @Param(id = 7) final String zai, @Param(id = 8) final long zaj, @Param(id = 9) final String zak, @Param(id = 10) final List<Scope> zac, @Param(id = 11) final String zal, @Param(id = 12) final String zam) {
        this.zan = new HashSet<Scope>();
        this.zab = zab;
        this.zad = zad;
        this.zae = zae;
        this.zaf = zaf;
        this.zag = zag;
        this.zah = zah;
        this.zai = zai;
        this.zaj = zaj;
        this.zak = zak;
        this.zac = zac;
        this.zal = zal;
        this.zam = zam;
    }
    
    @KeepForSdk
    public static GoogleSignInAccount createDefault() {
        return zae(new Account("<<default account>>", "com.google"), new HashSet<Scope>());
    }
    
    @KeepForSdk
    public static GoogleSignInAccount fromAccount(final Account account) {
        return zae(account, new s8());
    }
    
    public static GoogleSignInAccount zaa(final String s, final String s2, final String s3, final String s4, final String s5, final String s6, final Uri uri, final Long n, final String s7, final Set<Scope> set) {
        return new GoogleSignInAccount(3, s, s2, s3, s4, uri, null, n, Preconditions.checkNotEmpty(s7), new ArrayList<Scope>(Preconditions.checkNotNull(set)), s5, s6);
    }
    
    public static GoogleSignInAccount zab(String zai) {
        final boolean empty = TextUtils.isEmpty((CharSequence)zai);
        final String s = null;
        if (empty) {
            return null;
        }
        final JSONObject jsonObject = new JSONObject(zai);
        zai = jsonObject.optString("photoUrl");
        Uri parse;
        if (!TextUtils.isEmpty((CharSequence)zai)) {
            parse = Uri.parse(zai);
        }
        else {
            parse = null;
        }
        final long long1 = Long.parseLong(jsonObject.getString("expirationTime"));
        final HashSet set = new HashSet();
        final JSONArray jsonArray = jsonObject.getJSONArray("grantedScopes");
        for (int length = jsonArray.length(), i = 0; i < length; ++i) {
            set.add(new Scope(jsonArray.getString(i)));
        }
        final String optString = jsonObject.optString("id");
        String optString2;
        if (jsonObject.has("tokenId")) {
            optString2 = jsonObject.optString("tokenId");
        }
        else {
            optString2 = null;
        }
        String optString3;
        if (jsonObject.has("email")) {
            optString3 = jsonObject.optString("email");
        }
        else {
            optString3 = null;
        }
        String optString4;
        if (jsonObject.has("displayName")) {
            optString4 = jsonObject.optString("displayName");
        }
        else {
            optString4 = null;
        }
        String optString5;
        if (jsonObject.has("givenName")) {
            optString5 = jsonObject.optString("givenName");
        }
        else {
            optString5 = null;
        }
        String optString6;
        if (jsonObject.has("familyName")) {
            optString6 = jsonObject.optString("familyName");
        }
        else {
            optString6 = null;
        }
        final GoogleSignInAccount zaa = zaa(optString, optString2, optString3, optString4, optString5, optString6, parse, long1, jsonObject.getString("obfuscatedIdentifier"), set);
        zai = s;
        if (jsonObject.has("serverAuthCode")) {
            zai = jsonObject.optString("serverAuthCode");
        }
        zaa.zai = zai;
        return zaa;
    }
    
    private static GoogleSignInAccount zae(final Account account, final Set<Scope> set) {
        return zaa(null, null, account.name, null, null, null, null, 0L, account.name, set);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        if (!(o instanceof GoogleSignInAccount)) {
            return false;
        }
        final GoogleSignInAccount googleSignInAccount = (GoogleSignInAccount)o;
        return googleSignInAccount.zak.equals(this.zak) && googleSignInAccount.getRequestedScopes().equals(this.getRequestedScopes());
    }
    
    public Account getAccount() {
        final String zaf = this.zaf;
        if (zaf == null) {
            return null;
        }
        return new Account(zaf, "com.google");
    }
    
    public String getDisplayName() {
        return this.zag;
    }
    
    public String getEmail() {
        return this.zaf;
    }
    
    public String getFamilyName() {
        return this.zam;
    }
    
    public String getGivenName() {
        return this.zal;
    }
    
    public Set<Scope> getGrantedScopes() {
        return new HashSet<Scope>(this.zac);
    }
    
    public String getId() {
        return this.zad;
    }
    
    public String getIdToken() {
        return this.zae;
    }
    
    public Uri getPhotoUrl() {
        return this.zah;
    }
    
    @KeepForSdk
    public Set<Scope> getRequestedScopes() {
        final HashSet set = new HashSet((Collection<? extends E>)this.zac);
        set.addAll(this.zan);
        return set;
    }
    
    public String getServerAuthCode() {
        return this.zai;
    }
    
    @Override
    public int hashCode() {
        return (this.zak.hashCode() + 527) * 31 + this.getRequestedScopes().hashCode();
    }
    
    @KeepForSdk
    public boolean isExpired() {
        return GoogleSignInAccount.zaa.currentTimeMillis() / 1000L >= this.zaj - 300L;
    }
    
    @KeepForSdk
    public GoogleSignInAccount requestExtraScopes(final Scope... elements) {
        if (elements != null) {
            Collections.addAll(this.zan, elements);
        }
        return this;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.zab);
        SafeParcelWriter.writeString(parcel, 2, this.getId(), false);
        SafeParcelWriter.writeString(parcel, 3, this.getIdToken(), false);
        SafeParcelWriter.writeString(parcel, 4, this.getEmail(), false);
        SafeParcelWriter.writeString(parcel, 5, this.getDisplayName(), false);
        SafeParcelWriter.writeParcelable(parcel, 6, (Parcelable)this.getPhotoUrl(), n, false);
        SafeParcelWriter.writeString(parcel, 7, this.getServerAuthCode(), false);
        SafeParcelWriter.writeLong(parcel, 8, this.zaj);
        SafeParcelWriter.writeString(parcel, 9, this.zak, false);
        SafeParcelWriter.writeTypedList(parcel, 10, this.zac, false);
        SafeParcelWriter.writeString(parcel, 11, this.getGivenName(), false);
        SafeParcelWriter.writeString(parcel, 12, this.getFamilyName(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public final String zac() {
        return this.zak;
    }
    
    public final String zad() {
        final JSONObject jsonObject = new JSONObject();
        try {
            if (this.getId() != null) {
                jsonObject.put("id", (Object)this.getId());
            }
            if (this.getIdToken() != null) {
                jsonObject.put("tokenId", (Object)this.getIdToken());
            }
            if (this.getEmail() != null) {
                jsonObject.put("email", (Object)this.getEmail());
            }
            if (this.getDisplayName() != null) {
                jsonObject.put("displayName", (Object)this.getDisplayName());
            }
            if (this.getGivenName() != null) {
                jsonObject.put("givenName", (Object)this.getGivenName());
            }
            if (this.getFamilyName() != null) {
                jsonObject.put("familyName", (Object)this.getFamilyName());
            }
            final Uri photoUrl = this.getPhotoUrl();
            if (photoUrl != null) {
                jsonObject.put("photoUrl", (Object)photoUrl.toString());
            }
            if (this.getServerAuthCode() != null) {
                jsonObject.put("serverAuthCode", (Object)this.getServerAuthCode());
            }
            jsonObject.put("expirationTime", this.zaj);
            jsonObject.put("obfuscatedIdentifier", (Object)this.zak);
            final JSONArray jsonArray = new JSONArray();
            final List<Scope> zac = this.zac;
            final Scope[] a = zac.toArray(new Scope[zac.size()]);
            Arrays.sort(a, com.google.android.gms.auth.api.signin.zaa.zaa);
            for (int length = a.length, i = 0; i < length; ++i) {
                jsonArray.put((Object)a[i].getScopeUri());
            }
            jsonObject.put("grantedScopes", (Object)jsonArray);
            jsonObject.remove("serverAuthCode");
            return jsonObject.toString();
        }
        catch (final JSONException cause) {
            throw new RuntimeException((Throwable)cause);
        }
    }
}

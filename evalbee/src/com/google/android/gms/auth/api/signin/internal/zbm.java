// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.signin.internal;

import java.util.Iterator;
import com.google.android.gms.common.api.internal.GoogleApiManager;
import android.accounts.Account;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.internal.OptionalPendingResultImpl;
import com.google.android.gms.common.api.PendingResults;
import java.util.Collection;
import java.util.HashSet;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import android.os.Parcelable;
import android.os.Bundle;
import android.content.Intent;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import android.content.Context;
import com.google.android.gms.common.logging.Logger;

public final class zbm
{
    private static final Logger zba;
    
    static {
        zba = new Logger("GoogleSignInCommon", new String[0]);
    }
    
    public static Intent zba(final Context context, final GoogleSignInOptions googleSignInOptions) {
        zbm.zba.d("getFallbackSignInIntent()", new Object[0]);
        final Intent zbc = zbc(context, googleSignInOptions);
        zbc.setAction("com.google.android.gms.auth.APPAUTH_SIGN_IN");
        return zbc;
    }
    
    public static Intent zbb(final Context context, final GoogleSignInOptions googleSignInOptions) {
        zbm.zba.d("getNoImplementationSignInIntent()", new Object[0]);
        final Intent zbc = zbc(context, googleSignInOptions);
        zbc.setAction("com.google.android.gms.auth.NO_IMPL");
        return zbc;
    }
    
    public static Intent zbc(final Context context, final GoogleSignInOptions googleSignInOptions) {
        zbm.zba.d("getSignInIntent()", new Object[0]);
        final SignInConfiguration signInConfiguration = new SignInConfiguration(context.getPackageName(), googleSignInOptions);
        final Intent intent = new Intent("com.google.android.gms.auth.GOOGLE_SIGN_IN");
        intent.setPackage(context.getPackageName());
        intent.setClass(context, (Class)SignInHubActivity.class);
        final Bundle bundle = new Bundle();
        bundle.putParcelable("config", (Parcelable)signInConfiguration);
        intent.putExtra("config", bundle);
        return intent;
    }
    
    public static GoogleSignInResult zbd(final Intent intent) {
        if (intent == null) {
            return new GoogleSignInResult(null, Status.RESULT_INTERNAL_ERROR);
        }
        final Status status = (Status)intent.getParcelableExtra("googleSignInStatus");
        final GoogleSignInAccount googleSignInAccount = (GoogleSignInAccount)intent.getParcelableExtra("googleSignInAccount");
        if (googleSignInAccount == null) {
            Status result_INTERNAL_ERROR;
            if ((result_INTERNAL_ERROR = status) == null) {
                result_INTERNAL_ERROR = Status.RESULT_INTERNAL_ERROR;
            }
            return new GoogleSignInResult(null, result_INTERNAL_ERROR);
        }
        return new GoogleSignInResult(googleSignInAccount, Status.RESULT_SUCCESS);
    }
    
    public static OptionalPendingResult zbe(final GoogleApiClient googleApiClient, final Context context, final GoogleSignInOptions googleSignInOptions, final boolean b) {
        final Logger zba = zbm.zba;
        zba.d("silentSignIn()", new Object[0]);
        zba.d("getEligibleSavedSignInResult()", new Object[0]);
        Preconditions.checkNotNull(googleSignInOptions);
        final GoogleSignInOptions zbb = zbn.zbc(context).zbb();
        GoogleSignInResult googleSignInResult = null;
        Label_0207: {
            Label_0046: {
                if (zbb != null) {
                    final Account account = zbb.getAccount();
                    final Account account2 = googleSignInOptions.getAccount();
                    if (account == null) {
                        if (account2 != null) {
                            break Label_0046;
                        }
                    }
                    else if (!account.equals((Object)account2)) {
                        break Label_0046;
                    }
                    if (!googleSignInOptions.isServerAuthCodeRequested()) {
                        if (googleSignInOptions.isIdTokenRequested()) {
                            if (!zbb.isIdTokenRequested()) {
                                break Label_0046;
                            }
                            if (!Objects.equal(googleSignInOptions.getServerClientId(), zbb.getServerClientId())) {
                                break Label_0046;
                            }
                        }
                        if (new HashSet(zbb.getScopes()).containsAll(new HashSet(googleSignInOptions.getScopes()))) {
                            final GoogleSignInAccount zba2 = zbn.zbc(context).zba();
                            if (zba2 != null && !zba2.isExpired()) {
                                googleSignInResult = new GoogleSignInResult(zba2, Status.RESULT_SUCCESS);
                                break Label_0207;
                            }
                        }
                    }
                }
            }
            googleSignInResult = null;
        }
        if (googleSignInResult != null) {
            zba.d("Eligible saved sign in result found", new Object[0]);
            return PendingResults.immediatePendingResult(googleSignInResult, googleApiClient);
        }
        if (b) {
            return PendingResults.immediatePendingResult(new GoogleSignInResult(null, new Status(4)), googleApiClient);
        }
        zba.d("trySilentSignIn()", new Object[0]);
        return new OptionalPendingResultImpl(googleApiClient.enqueue(new zbg(googleApiClient, context, googleSignInOptions)));
    }
    
    public static PendingResult zbf(final GoogleApiClient googleApiClient, final Context context, final boolean b) {
        zbm.zba.d("Revoking access", new Object[0]);
        final String savedRefreshToken = Storage.getInstance(context).getSavedRefreshToken();
        zbh(context);
        if (b) {
            return zbb.zba(savedRefreshToken);
        }
        return googleApiClient.execute(new zbk(googleApiClient));
    }
    
    public static PendingResult zbg(final GoogleApiClient googleApiClient, final Context context, final boolean b) {
        zbm.zba.d("Signing out", new Object[0]);
        zbh(context);
        if (b) {
            return PendingResults.immediatePendingResult(Status.RESULT_SUCCESS, googleApiClient);
        }
        return googleApiClient.execute(new zbi(googleApiClient));
    }
    
    private static void zbh(final Context context) {
        zbn.zbc(context).zbd();
        final Iterator<GoogleApiClient> iterator = GoogleApiClient.getAllClients().iterator();
        while (iterator.hasNext()) {
            iterator.next().maybeSignOut();
        }
        GoogleApiManager.reportSignOut();
    }
}

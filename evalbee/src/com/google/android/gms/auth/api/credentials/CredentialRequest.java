// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.credentials;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import java.util.Set;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Deprecated
@Class(creator = "CredentialRequestCreator")
public final class CredentialRequest extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<CredentialRequest> CREATOR;
    @Field(id = 1000)
    final int zba;
    @Field(getter = "isPasswordLoginSupported", id = 1)
    private final boolean zbb;
    @Field(getter = "getAccountTypes", id = 2)
    private final String[] zbc;
    @Field(getter = "getCredentialPickerConfig", id = 3)
    private final CredentialPickerConfig zbd;
    @Field(getter = "getCredentialHintPickerConfig", id = 4)
    private final CredentialPickerConfig zbe;
    @Field(getter = "isIdTokenRequested", id = 5)
    private final boolean zbf;
    @Field(getter = "getServerClientId", id = 6)
    private final String zbg;
    @Field(getter = "getIdTokenNonce", id = 7)
    private final String zbh;
    @Field(getter = "getRequireUserMediation", id = 8)
    private final boolean zbi;
    
    static {
        CREATOR = (Parcelable$Creator)new zbc();
    }
    
    @Constructor
    public CredentialRequest(@Param(id = 1000) final int zba, @Param(id = 1) final boolean zbb, @Param(id = 2) final String[] array, @Param(id = 3) final CredentialPickerConfig credentialPickerConfig, @Param(id = 4) final CredentialPickerConfig credentialPickerConfig2, @Param(id = 5) final boolean zbf, @Param(id = 6) final String zbg, @Param(id = 7) final String zbh, @Param(id = 8) final boolean zbi) {
        this.zba = zba;
        this.zbb = zbb;
        this.zbc = Preconditions.checkNotNull(array);
        CredentialPickerConfig build = credentialPickerConfig;
        if (credentialPickerConfig == null) {
            build = new CredentialPickerConfig.Builder().build();
        }
        this.zbd = build;
        CredentialPickerConfig build2;
        if ((build2 = credentialPickerConfig2) == null) {
            build2 = new CredentialPickerConfig.Builder().build();
        }
        this.zbe = build2;
        if (zba < 3) {
            this.zbf = true;
            this.zbg = null;
            this.zbh = null;
        }
        else {
            this.zbf = zbf;
            this.zbg = zbg;
            this.zbh = zbh;
        }
        this.zbi = zbi;
    }
    
    public String[] getAccountTypes() {
        return this.zbc;
    }
    
    public Set<String> getAccountTypesSet() {
        return new HashSet<String>(Arrays.asList(this.zbc));
    }
    
    public CredentialPickerConfig getCredentialHintPickerConfig() {
        return this.zbe;
    }
    
    public CredentialPickerConfig getCredentialPickerConfig() {
        return this.zbd;
    }
    
    public String getIdTokenNonce() {
        return this.zbh;
    }
    
    public String getServerClientId() {
        return this.zbg;
    }
    
    @Deprecated
    public boolean getSupportsPasswordLogin() {
        return this.isPasswordLoginSupported();
    }
    
    public boolean isIdTokenRequested() {
        return this.zbf;
    }
    
    public boolean isPasswordLoginSupported() {
        return this.zbb;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeBoolean(parcel, 1, this.isPasswordLoginSupported());
        SafeParcelWriter.writeStringArray(parcel, 2, this.getAccountTypes(), false);
        SafeParcelWriter.writeParcelable(parcel, 3, (Parcelable)this.getCredentialPickerConfig(), n, false);
        SafeParcelWriter.writeParcelable(parcel, 4, (Parcelable)this.getCredentialHintPickerConfig(), n, false);
        SafeParcelWriter.writeBoolean(parcel, 5, this.isIdTokenRequested());
        SafeParcelWriter.writeString(parcel, 6, this.getServerClientId(), false);
        SafeParcelWriter.writeString(parcel, 7, this.getIdTokenNonce(), false);
        SafeParcelWriter.writeBoolean(parcel, 8, this.zbi);
        SafeParcelWriter.writeInt(parcel, 1000, this.zba);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public static final class Builder
    {
        private boolean zba;
        private String[] zbb;
        private CredentialPickerConfig zbc;
        private CredentialPickerConfig zbd;
        private boolean zbe;
        private String zbf;
        private String zbg;
        
        public Builder() {
            this.zbe = false;
            this.zbf = null;
        }
        
        public CredentialRequest build() {
            if (this.zbb == null) {
                this.zbb = new String[0];
            }
            if (!this.zba && this.zbb.length == 0) {
                throw new IllegalStateException("At least one authentication method must be specified");
            }
            return new CredentialRequest(4, this.zba, this.zbb, this.zbc, this.zbd, this.zbe, this.zbf, this.zbg, false);
        }
        
        public Builder setAccountTypes(final String... array) {
            String[] zbb = array;
            if (array == null) {
                zbb = new String[0];
            }
            this.zbb = zbb;
            return this;
        }
        
        public Builder setCredentialHintPickerConfig(final CredentialPickerConfig zbd) {
            this.zbd = zbd;
            return this;
        }
        
        public Builder setCredentialPickerConfig(final CredentialPickerConfig zbc) {
            this.zbc = zbc;
            return this;
        }
        
        public Builder setIdTokenNonce(final String zbg) {
            this.zbg = zbg;
            return this;
        }
        
        public Builder setIdTokenRequested(final boolean zbe) {
            this.zbe = zbe;
            return this;
        }
        
        public Builder setPasswordLoginSupported(final boolean zba) {
            this.zba = zba;
            return this;
        }
        
        public Builder setServerClientId(final String zbf) {
            this.zbf = zbf;
            return this;
        }
        
        @Deprecated
        public Builder setSupportsPasswordLogin(final boolean passwordLoginSupported) {
            this.setPasswordLoginSupported(passwordLoginSupported);
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.accounttransfer;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.auth.zzan;

final class zzm extends zzan
{
    final zzn zza;
    
    public zzm(final zzn zza) {
        this.zza = zza;
    }
    
    public final void zzd(final Status status) {
        this.zza.zzb.setException((Exception)new AccountTransferException(status));
    }
    
    public final void zze() {
        this.zza.zzb.setResult((Object)null);
    }
}

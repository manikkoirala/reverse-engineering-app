// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.signin.internal;

import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public class HashAccumulator
{
    @VisibleForTesting
    static int zaa = 31;
    private int zab;
    
    public HashAccumulator() {
        this.zab = 1;
    }
    
    @KeepForSdk
    public HashAccumulator addObject(final Object o) {
        final int zaa = HashAccumulator.zaa;
        final int zab = this.zab;
        int hashCode;
        if (o == null) {
            hashCode = 0;
        }
        else {
            hashCode = o.hashCode();
        }
        this.zab = zaa * zab + hashCode;
        return this;
    }
    
    @KeepForSdk
    public int hash() {
        return this.zab;
    }
    
    public final HashAccumulator zaa(final boolean b) {
        this.zab = HashAccumulator.zaa * this.zab + (b ? 1 : 0);
        return this;
    }
}

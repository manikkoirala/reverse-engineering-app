// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.accounttransfer;

import com.google.android.gms.internal.auth.zzat;
import com.google.android.gms.internal.auth.zzau;
import com.google.android.gms.internal.auth.zzbb;

final class zzh extends zzn
{
    final zzbb zza;
    
    public zzh(final AccountTransferClient accountTransferClient, final int n, final zzbb zza) {
        this.zza = zza;
        super(1609);
    }
    
    @Override
    public final void zza(final zzau zzau) {
        zzau.zze((zzat)super.zzc, this.zza);
    }
}

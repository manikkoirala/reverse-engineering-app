// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.phone;

import com.google.android.gms.internal.auth_api_phone.zzv;
import android.content.Context;
import com.google.android.gms.internal.auth_api_phone.zzr;
import android.app.Activity;

public final class SmsCodeRetriever
{
    public static final String EXTRA_SMS_CODE = "com.google.android.gms.auth.api.phone.EXTRA_SMS_CODE";
    public static final String EXTRA_SMS_CODE_LINE = "com.google.android.gms.auth.api.phone.EXTRA_SMS_CODE_LINE";
    public static final String EXTRA_STATUS = "com.google.android.gms.auth.api.phone.EXTRA_STATUS";
    public static final String SMS_CODE_RETRIEVED_ACTION = "com.google.android.gms.auth.api.phone.SMS_CODE_RETRIEVED";
    
    private SmsCodeRetriever() {
    }
    
    public static SmsCodeAutofillClient getAutofillClient(final Activity activity) {
        return (SmsCodeAutofillClient)new zzr(activity);
    }
    
    public static SmsCodeAutofillClient getAutofillClient(final Context context) {
        return (SmsCodeAutofillClient)new zzr(context);
    }
    
    public static SmsCodeBrowserClient getBrowserClient(final Activity activity) {
        return (SmsCodeBrowserClient)new zzv(activity);
    }
    
    public static SmsCodeBrowserClient getBrowserClient(final Context context) {
        return (SmsCodeBrowserClient)new zzv(context);
    }
}

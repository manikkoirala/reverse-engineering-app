// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.identity;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "GetPhoneNumberHintIntentRequestCreator")
public class GetPhoneNumberHintIntentRequest extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<GetPhoneNumberHintIntentRequest> CREATOR;
    @Field(getter = "getTheme", id = 1)
    private final int zba;
    
    static {
        CREATOR = (Parcelable$Creator)new zbj();
    }
    
    @Constructor
    public GetPhoneNumberHintIntentRequest(@Param(id = 1) final int zba) {
        this.zba = zba;
    }
    
    public static Builder builder() {
        return new Builder(null);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof GetPhoneNumberHintIntentRequest && Objects.equal(this.zba, ((GetPhoneNumberHintIntentRequest)o).zba);
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zba);
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.zba);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public static final class Builder
    {
        private Builder() {
        }
        
        public GetPhoneNumberHintIntentRequest build() {
            return new GetPhoneNumberHintIntentRequest(0);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.signin;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.Result;

public class GoogleSignInResult implements Result
{
    private final Status zba;
    private final GoogleSignInAccount zbb;
    
    public GoogleSignInResult(final GoogleSignInAccount zbb, final Status zba) {
        this.zbb = zbb;
        this.zba = zba;
    }
    
    public GoogleSignInAccount getSignInAccount() {
        return this.zbb;
    }
    
    @Override
    public Status getStatus() {
        return this.zba;
    }
    
    public boolean isSuccess() {
        return this.zba.isSuccess();
    }
}

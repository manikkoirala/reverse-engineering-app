// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.signin.internal;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import com.google.android.gms.common.api.PendingResults;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.api.internal.StatusPendingResult;
import com.google.android.gms.common.logging.Logger;

public final class zbb implements Runnable
{
    private static final Logger zba;
    private final String zbb;
    private final StatusPendingResult zbc;
    
    static {
        zba = new Logger("RevokeAccessOperation", new String[0]);
    }
    
    public zbb(final String s) {
        this.zbb = Preconditions.checkNotEmpty(s);
        this.zbc = new StatusPendingResult((GoogleApiClient)null);
    }
    
    public static PendingResult zba(final String s) {
        if (s == null) {
            return PendingResults.immediateFailedResult(new Status(4), null);
        }
        final zbb target = new zbb(s);
        new Thread(target).start();
        return target.zbc;
    }
    
    @Override
    public final void run() {
        Status result_INTERNAL_ERROR;
        Status status;
        Status result_SUCCESS = status = (result_INTERNAL_ERROR = Status.RESULT_INTERNAL_ERROR);
        Label_0299: {
            Label_0274: {
                try {
                    result_INTERNAL_ERROR = result_SUCCESS;
                    status = result_SUCCESS;
                    final String zbb = this.zbb;
                    result_INTERNAL_ERROR = result_SUCCESS;
                    status = result_SUCCESS;
                    result_INTERNAL_ERROR = result_SUCCESS;
                    status = result_SUCCESS;
                    final StringBuilder sb = new StringBuilder();
                    result_INTERNAL_ERROR = result_SUCCESS;
                    status = result_SUCCESS;
                    sb.append("https://accounts.google.com/o/oauth2/revoke?token=");
                    result_INTERNAL_ERROR = result_SUCCESS;
                    status = result_SUCCESS;
                    sb.append(zbb);
                    result_INTERNAL_ERROR = result_SUCCESS;
                    status = result_SUCCESS;
                    final URL url = new URL(sb.toString());
                    result_INTERNAL_ERROR = result_SUCCESS;
                    status = result_SUCCESS;
                    final HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                    result_INTERNAL_ERROR = result_SUCCESS;
                    status = result_SUCCESS;
                    httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    result_INTERNAL_ERROR = result_SUCCESS;
                    status = result_SUCCESS;
                    final int responseCode = httpURLConnection.getResponseCode();
                    if (responseCode == 200) {
                        result_INTERNAL_ERROR = result_SUCCESS;
                        status = result_SUCCESS;
                        result_SUCCESS = Status.RESULT_SUCCESS;
                    }
                    else {
                        result_INTERNAL_ERROR = result_SUCCESS;
                        status = result_SUCCESS;
                        com.google.android.gms.auth.api.signin.internal.zbb.zba.e("Unable to revoke access!", new Object[0]);
                    }
                    result_INTERNAL_ERROR = result_SUCCESS;
                    status = result_SUCCESS;
                    final Logger zba = com.google.android.gms.auth.api.signin.internal.zbb.zba;
                    result_INTERNAL_ERROR = result_SUCCESS;
                    status = result_SUCCESS;
                    result_INTERNAL_ERROR = result_SUCCESS;
                    status = result_SUCCESS;
                    final StringBuilder sb2 = new StringBuilder();
                    result_INTERNAL_ERROR = result_SUCCESS;
                    status = result_SUCCESS;
                    sb2.append("Response Code: ");
                    result_INTERNAL_ERROR = result_SUCCESS;
                    status = result_SUCCESS;
                    sb2.append(responseCode);
                    result_INTERNAL_ERROR = result_SUCCESS;
                    status = result_SUCCESS;
                    zba.d(sb2.toString(), new Object[0]);
                    break Label_0299;
                }
                catch (final Exception ex) {}
                catch (final IOException ex2) {
                    break Label_0274;
                }
                final Exception ex;
                com.google.android.gms.auth.api.signin.internal.zbb.zba.e("Exception when revoking access: ".concat(String.valueOf(ex.toString())), new Object[0]);
                result_SUCCESS = result_INTERNAL_ERROR;
                break Label_0299;
            }
            final IOException ex2;
            com.google.android.gms.auth.api.signin.internal.zbb.zba.e("IOException when revoking access: ".concat(String.valueOf(ex2.toString())), new Object[0]);
            result_SUCCESS = status;
        }
        this.zbc.setResult(result_SUCCESS);
    }
}

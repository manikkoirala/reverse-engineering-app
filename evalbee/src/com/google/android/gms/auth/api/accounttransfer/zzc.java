// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.accounttransfer;

import com.google.android.gms.internal.auth.zzat;
import com.google.android.gms.internal.auth.zzau;
import com.google.android.gms.internal.auth.zzaz;

final class zzc extends zzn
{
    final zzaz zza;
    
    public zzc(final AccountTransferClient accountTransferClient, final int n, final zzaz zza) {
        this.zza = zza;
        super(1606);
    }
    
    @Override
    public final void zza(final zzau zzau) {
        zzau.zzh((zzat)super.zzc, this.zza);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.identity;

import android.os.BaseBundle;
import android.os.Bundle;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.api.Api;

public final class zbc implements Optional
{
    private final String zba;
    
    public zbc(final String zba) {
        this.zba = zba;
    }
    
    @Override
    public final boolean equals(final Object o) {
        return o instanceof zbc;
    }
    
    @Override
    public final int hashCode() {
        return Objects.hashCode(zbc.class);
    }
    
    public final Bundle zba() {
        final Bundle bundle = new Bundle();
        ((BaseBundle)bundle).putString("session_id", this.zba);
        return bundle;
    }
    
    public final String zbb() {
        return this.zba;
    }
}

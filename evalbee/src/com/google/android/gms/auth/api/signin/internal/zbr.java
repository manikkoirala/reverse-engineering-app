// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.signin.internal;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;
import android.os.IInterface;

public interface zbr extends IInterface
{
    void zbb(final Status p0);
    
    void zbc(final Status p0);
    
    void zbd(final GoogleSignInAccount p0, final Status p1);
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.accounttransfer;

final class zzf extends zzj
{
    final zzg zza;
    
    public zzf(final zzg zza, final zzl zzl) {
        this.zza = zza;
        super(zzl);
    }
    
    public final void zzc(final DeviceMetaData result) {
        this.zza.zzb.setResult((Object)result);
    }
}

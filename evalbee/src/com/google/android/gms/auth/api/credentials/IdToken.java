// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.credentials;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import android.text.TextUtils;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Deprecated
@Class(creator = "IdTokenCreator")
@Reserved({ 1000 })
public final class IdToken extends AbstractSafeParcelable implements ReflectedParcelable
{
    public static final Parcelable$Creator<IdToken> CREATOR;
    @Field(getter = "getAccountType", id = 1)
    private final String zba;
    @Field(getter = "getIdToken", id = 2)
    private final String zbb;
    
    static {
        CREATOR = (Parcelable$Creator)new zbf();
    }
    
    @Constructor
    public IdToken(@Param(id = 1) final String zba, @Param(id = 2) final String zbb) {
        Preconditions.checkArgument(TextUtils.isEmpty((CharSequence)zba) ^ true, (Object)"account type string cannot be null or empty");
        Preconditions.checkArgument(TextUtils.isEmpty((CharSequence)zbb) ^ true, (Object)"id token string cannot be null or empty");
        this.zba = zba;
        this.zbb = zbb;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof IdToken)) {
            return false;
        }
        final IdToken idToken = (IdToken)o;
        return Objects.equal(this.zba, idToken.zba) && Objects.equal(this.zbb, idToken.zbb);
    }
    
    public String getAccountType() {
        return this.zba;
    }
    
    public String getIdToken() {
        return this.zbb;
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.getAccountType(), false);
        SafeParcelWriter.writeString(parcel, 2, this.getIdToken(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api;

import android.os.BaseBundle;
import android.os.Bundle;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.auth.api.signin.internal.zbd;
import com.google.android.gms.internal.auth_api.zbl;
import com.google.android.gms.auth.api.proxy.ProxyApi;
import com.google.android.gms.common.internal.ShowFirstParty;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.auth.api.signin.GoogleSignInApi;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.credentials.CredentialsApi;
import com.google.android.gms.common.api.Api;

public final class Auth
{
    public static final Api<AuthCredentialsOptions> CREDENTIALS_API;
    public static final CredentialsApi CredentialsApi;
    public static final Api<GoogleSignInOptions> GOOGLE_SIGN_IN_API;
    public static final GoogleSignInApi GoogleSignInApi;
    @Deprecated
    @KeepForSdk
    @ShowFirstParty
    public static final Api<AuthProxyOptions> PROXY_API;
    @Deprecated
    @KeepForSdk
    @ShowFirstParty
    public static final ProxyApi ProxyApi;
    public static final Api.ClientKey zba;
    public static final Api.ClientKey zbb;
    private static final Api.AbstractClientBuilder zbc;
    private static final Api.AbstractClientBuilder zbd;
    
    static {
        final Api.ClientKey clientKey = zba = new Api.ClientKey();
        final Api.ClientKey clientKey2 = zbb = new Api.ClientKey();
        final Api.AbstractClientBuilder abstractClientBuilder = zbc = new zba();
        final Api.AbstractClientBuilder abstractClientBuilder2 = zbd = new zbb();
        PROXY_API = AuthProxy.API;
        CREDENTIALS_API = new Api<AuthCredentialsOptions>("Auth.CREDENTIALS_API", abstractClientBuilder, clientKey);
        GOOGLE_SIGN_IN_API = new Api<GoogleSignInOptions>("Auth.GOOGLE_SIGN_IN_API", abstractClientBuilder2, clientKey2);
        ProxyApi = AuthProxy.ProxyApi;
        CredentialsApi = (CredentialsApi)new zbl();
        GoogleSignInApi = new zbd();
    }
    
    private Auth() {
    }
    
    @Deprecated
    public static class AuthCredentialsOptions implements Optional
    {
        public static final AuthCredentialsOptions zba;
        private final String zbb;
        private final boolean zbc;
        private final String zbd;
        
        static {
            zba = new AuthCredentialsOptions(new Builder());
        }
        
        public AuthCredentialsOptions(final Builder builder) {
            this.zbb = null;
            this.zbc = builder.zba;
            this.zbd = builder.zbb;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof AuthCredentialsOptions)) {
                return false;
            }
            final AuthCredentialsOptions authCredentialsOptions = (AuthCredentialsOptions)o;
            final String zbb = authCredentialsOptions.zbb;
            return Objects.equal(null, null) && this.zbc == authCredentialsOptions.zbc && Objects.equal(this.zbd, authCredentialsOptions.zbd);
        }
        
        @Override
        public int hashCode() {
            return Objects.hashCode(null, this.zbc, this.zbd);
        }
        
        public final Bundle zba() {
            final Bundle bundle = new Bundle();
            ((BaseBundle)bundle).putString("consumer_package", (String)null);
            ((BaseBundle)bundle).putBoolean("force_save_dialog", this.zbc);
            ((BaseBundle)bundle).putString("log_session_id", this.zbd);
            return bundle;
        }
        
        public final String zbd() {
            return this.zbd;
        }
        
        @Deprecated
        public static class Builder
        {
            protected Boolean zba;
            protected String zbb;
            
            public Builder() {
                this.zba = Boolean.FALSE;
            }
            
            @ShowFirstParty
            public Builder(final AuthCredentialsOptions authCredentialsOptions) {
                this.zba = Boolean.FALSE;
                AuthCredentialsOptions.zbb(authCredentialsOptions);
                this.zba = AuthCredentialsOptions.zbe(authCredentialsOptions);
                this.zbb = AuthCredentialsOptions.zbc(authCredentialsOptions);
            }
            
            public Builder forceEnableSaveDialog() {
                this.zba = Boolean.TRUE;
                return this;
            }
            
            @ShowFirstParty
            public final Builder zba(final String zbb) {
                this.zbb = zbb;
                return this;
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.signin;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.internal.PendingResultUtil;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.auth.api.signin.internal.zbm;
import android.content.Intent;
import com.google.android.gms.dynamite.DynamiteModule;
import com.google.android.gms.common.GoogleApiAvailability;
import android.content.Context;
import com.google.android.gms.common.api.internal.StatusExceptionMapper;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.ApiExceptionMapper;
import com.google.android.gms.auth.api.Auth;
import android.app.Activity;
import com.google.android.gms.common.api.GoogleApi;

public class GoogleSignInClient extends GoogleApi<GoogleSignInOptions>
{
    static int zba;
    private static final zbb zbb;
    
    static {
        zbb = new zbb(null);
        GoogleSignInClient.zba = 1;
    }
    
    public GoogleSignInClient(final Activity activity, final GoogleSignInOptions googleSignInOptions) {
        super(activity, (Api<Api.ApiOptions>)Auth.GOOGLE_SIGN_IN_API, (Api.ApiOptions)googleSignInOptions, new ApiExceptionMapper());
    }
    
    public GoogleSignInClient(final Context context, final GoogleSignInOptions googleSignInOptions) {
        super(context, (Api<Api.ApiOptions>)Auth.GOOGLE_SIGN_IN_API, (Api.ApiOptions)googleSignInOptions, new Settings.Builder().setMapper(new ApiExceptionMapper()).build());
    }
    
    private final int zba() {
        synchronized (this) {
            int zba;
            if ((zba = GoogleSignInClient.zba) == 1) {
                final Context applicationContext = this.getApplicationContext();
                final GoogleApiAvailability instance = GoogleApiAvailability.getInstance();
                final int googlePlayServicesAvailable = instance.isGooglePlayServicesAvailable(applicationContext, 12451000);
                if (googlePlayServicesAvailable == 0) {
                    zba = 4;
                    GoogleSignInClient.zba = 4;
                }
                else if (instance.getErrorResolutionIntent(applicationContext, googlePlayServicesAvailable, null) == null && DynamiteModule.getLocalVersion(applicationContext, "com.google.android.gms.auth.api.fallback") != 0) {
                    zba = 3;
                    GoogleSignInClient.zba = 3;
                }
                else {
                    zba = 2;
                    GoogleSignInClient.zba = 2;
                }
            }
            return zba;
        }
    }
    
    public Intent getSignInIntent() {
        final Context applicationContext = this.getApplicationContext();
        final int zba = this.zba();
        final int n = zba - 1;
        if (zba == 0) {
            throw null;
        }
        if (n == 2) {
            return zbm.zba(applicationContext, this.getApiOptions());
        }
        if (n != 3) {
            return zbm.zbb(applicationContext, this.getApiOptions());
        }
        return zbm.zbc(applicationContext, this.getApiOptions());
    }
    
    public Task<Void> revokeAccess() {
        return PendingResultUtil.toVoidTask((PendingResult<Result>)zbm.zbf(this.asGoogleApiClient(), this.getApplicationContext(), this.zba() == 3));
    }
    
    public Task<Void> signOut() {
        return PendingResultUtil.toVoidTask((PendingResult<Result>)zbm.zbg(this.asGoogleApiClient(), this.getApplicationContext(), this.zba() == 3));
    }
    
    public Task<GoogleSignInAccount> silentSignIn() {
        return PendingResultUtil.toTask(zbm.zbe(this.asGoogleApiClient(), this.getApplicationContext(), this.getApiOptions(), this.zba() == 3), (PendingResultUtil.ResultConverter<Result, GoogleSignInAccount>)GoogleSignInClient.zbb);
    }
}

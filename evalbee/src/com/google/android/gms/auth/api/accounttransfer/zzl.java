// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.accounttransfer;

import com.google.android.gms.common.internal.BaseGmsClient;
import com.google.android.gms.internal.auth.zzau;
import com.google.android.gms.internal.auth.zzap;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.Feature;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.common.api.internal.TaskApiCall;

abstract class zzl extends TaskApiCall
{
    protected TaskCompletionSource zzb;
    
    public abstract void zza(final zzau p0);
}

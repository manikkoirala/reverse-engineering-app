// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.accounttransfer;

import com.google.android.gms.internal.auth.zzat;
import com.google.android.gms.internal.auth.zzau;
import com.google.android.gms.internal.auth.zzax;

final class zze extends zzl
{
    final zzax zza;
    
    public zze(final AccountTransferClient accountTransferClient, final int n, final zzax zza) {
        this.zza = zza;
        super(1607, null);
    }
    
    @Override
    public final void zza(final zzau zzau) {
        zzau.zzg((zzat)new zzd(this, this), this.zza);
    }
}

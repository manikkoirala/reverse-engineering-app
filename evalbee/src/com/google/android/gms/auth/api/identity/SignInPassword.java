// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.identity;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "SignInPasswordCreator")
public class SignInPassword extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<SignInPassword> CREATOR;
    @Field(getter = "getId", id = 1)
    private final String zba;
    @Field(getter = "getPassword", id = 2)
    private final String zbb;
    
    static {
        CREATOR = (Parcelable$Creator)new zbv();
    }
    
    @Constructor
    public SignInPassword(@Param(id = 1) final String s, @Param(id = 2) final String s2) {
        this.zba = Preconditions.checkNotEmpty(Preconditions.checkNotNull(s, "Account identifier cannot be null").trim(), "Account identifier cannot be empty");
        this.zbb = Preconditions.checkNotEmpty(s2);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof SignInPassword)) {
            return false;
        }
        final SignInPassword signInPassword = (SignInPassword)o;
        return Objects.equal(this.zba, signInPassword.zba) && Objects.equal(this.zbb, signInPassword.zbb);
    }
    
    public String getId() {
        return this.zba;
    }
    
    public String getPassword() {
        return this.zbb;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zba, this.zbb);
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.getId(), false);
        SafeParcelWriter.writeString(parcel, 2, this.getPassword(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.phone;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.HasApiKey;

public interface SmsCodeAutofillClient extends HasApiKey<Api.ApiOptions.NoOptions>
{
    Task<Integer> checkPermissionState();
    
    Task<Boolean> hasOngoingSmsRequest(final String p0);
    
    Task<Void> startSmsCodeRetriever();
    
    @Retention(RetentionPolicy.SOURCE)
    @Target({ ElementType.TYPE_PARAMETER, ElementType.TYPE_USE })
    public @interface PermissionState {
        public static final int DENIED = 2;
        public static final int GRANTED = 1;
        public static final int NONE = 0;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.identity;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "SavePasswordRequestCreator")
public class SavePasswordRequest extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<SavePasswordRequest> CREATOR;
    @Field(getter = "getSignInPassword", id = 1)
    private final SignInPassword zba;
    @Field(getter = "getSessionId", id = 2)
    private final String zbb;
    @Field(getter = "getTheme", id = 3)
    private final int zbc;
    
    static {
        CREATOR = (Parcelable$Creator)new zbr();
    }
    
    @Constructor
    public SavePasswordRequest(@Param(id = 1) final SignInPassword signInPassword, @Param(id = 2) final String zbb, @Param(id = 3) final int zbc) {
        this.zba = Preconditions.checkNotNull(signInPassword);
        this.zbb = zbb;
        this.zbc = zbc;
    }
    
    public static Builder builder() {
        return new Builder();
    }
    
    public static Builder zba(final SavePasswordRequest savePasswordRequest) {
        Preconditions.checkNotNull(savePasswordRequest);
        final Builder builder = builder();
        builder.setSignInPassword(savePasswordRequest.getSignInPassword());
        builder.zbb(savePasswordRequest.zbc);
        final String zbb = savePasswordRequest.zbb;
        if (zbb != null) {
            builder.zba(zbb);
        }
        return builder;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof SavePasswordRequest)) {
            return false;
        }
        final SavePasswordRequest savePasswordRequest = (SavePasswordRequest)o;
        return Objects.equal(this.zba, savePasswordRequest.zba) && Objects.equal(this.zbb, savePasswordRequest.zbb) && this.zbc == savePasswordRequest.zbc;
    }
    
    public SignInPassword getSignInPassword() {
        return this.zba;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zba, this.zbb);
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeParcelable(parcel, 1, (Parcelable)this.getSignInPassword(), n, false);
        SafeParcelWriter.writeString(parcel, 2, this.zbb, false);
        SafeParcelWriter.writeInt(parcel, 3, this.zbc);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public static final class Builder
    {
        private SignInPassword zba;
        private String zbb;
        private int zbc;
        
        public SavePasswordRequest build() {
            return new SavePasswordRequest(this.zba, this.zbb, this.zbc);
        }
        
        public Builder setSignInPassword(final SignInPassword zba) {
            this.zba = zba;
            return this;
        }
        
        public final Builder zba(final String zbb) {
            this.zbb = zbb;
            return this;
        }
        
        public final Builder zbb(final int zbc) {
            this.zbc = zbc;
            return this;
        }
    }
}

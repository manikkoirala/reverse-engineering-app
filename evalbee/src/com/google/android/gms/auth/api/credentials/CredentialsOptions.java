// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.credentials;

import com.google.android.gms.auth.api.Auth.AuthCredentialsOptions;
import com.google.android.gms.auth.api.Auth;

@Deprecated
public final class CredentialsOptions extends AuthCredentialsOptions
{
    public static final CredentialsOptions DEFAULT;
    
    static {
        DEFAULT = new Builder().build();
    }
    
    public static final class Builder extends AuthCredentialsOptions.Builder
    {
        public CredentialsOptions build() {
            return new CredentialsOptions(this, null);
        }
        
        public Builder forceEnableSaveDialog() {
            super.zba = Boolean.TRUE;
            return this;
        }
    }
}

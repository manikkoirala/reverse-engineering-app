// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.identity;

import com.google.android.gms.common.internal.ShowFirstParty;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.Objects;
import java.util.Collection;
import com.google.android.gms.common.internal.Preconditions;
import android.accounts.Account;
import java.util.List;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "AuthorizationRequestCreator")
public class AuthorizationRequest extends AbstractSafeParcelable implements ReflectedParcelable
{
    public static final Parcelable$Creator<AuthorizationRequest> CREATOR;
    @Field(getter = "getRequestedScopes", id = 1)
    private final List zba;
    @Field(getter = "getServerClientId", id = 2)
    private final String zbb;
    @Field(getter = "isOfflineAccessRequested", id = 3)
    private final boolean zbc;
    @Field(getter = "isIdTokenRequested", id = 4)
    private final boolean zbd;
    @Field(getter = "getAccount", id = 5)
    private final Account zbe;
    @Field(getter = "getHostedDomain", id = 6)
    private final String zbf;
    @Field(getter = "getSessionId", id = 7)
    private final String zbg;
    @Field(getter = "isForceCodeForRefreshToken", id = 8)
    private final boolean zbh;
    
    static {
        CREATOR = (Parcelable$Creator)new zbd();
    }
    
    @Constructor
    public AuthorizationRequest(@Param(id = 1) final List zba, @Param(id = 2) final String zbb, @Param(id = 3) final boolean zbc, @Param(id = 4) final boolean zbd, @Param(id = 5) final Account zbe, @Param(id = 6) final String zbf, @Param(id = 7) final String zbg, @Param(id = 8) final boolean zbh) {
        boolean b = false;
        if (zba != null) {
            b = b;
            if (!zba.isEmpty()) {
                b = true;
            }
        }
        Preconditions.checkArgument(b, (Object)"requestedScopes cannot be null or empty");
        this.zba = zba;
        this.zbb = zbb;
        this.zbc = zbc;
        this.zbd = zbd;
        this.zbe = zbe;
        this.zbf = zbf;
        this.zbg = zbg;
        this.zbh = zbh;
    }
    
    public static Builder builder() {
        return new Builder();
    }
    
    public static Builder zba(final AuthorizationRequest authorizationRequest) {
        Preconditions.checkNotNull(authorizationRequest);
        final Builder builder = builder();
        builder.setRequestedScopes(authorizationRequest.getRequestedScopes());
        final boolean forceCodeForRefreshToken = authorizationRequest.isForceCodeForRefreshToken();
        final String zbg = authorizationRequest.zbg;
        final String hostedDomain = authorizationRequest.getHostedDomain();
        final Account account = authorizationRequest.getAccount();
        final String serverClientId = authorizationRequest.getServerClientId();
        if (zbg != null) {
            builder.zbb(zbg);
        }
        if (hostedDomain != null) {
            builder.filterByHostedDomain(hostedDomain);
        }
        if (account != null) {
            builder.setAccount(account);
        }
        if (authorizationRequest.zbd && serverClientId != null) {
            builder.zba(serverClientId);
        }
        if (authorizationRequest.isOfflineAccessRequested() && serverClientId != null) {
            builder.requestOfflineAccess(serverClientId, forceCodeForRefreshToken);
        }
        return builder;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof AuthorizationRequest)) {
            return false;
        }
        final AuthorizationRequest authorizationRequest = (AuthorizationRequest)o;
        if (this.zba.size() == authorizationRequest.zba.size()) {
            if (this.zba.containsAll(authorizationRequest.zba)) {
                if (this.zbc == authorizationRequest.zbc && this.zbh == authorizationRequest.zbh && this.zbd == authorizationRequest.zbd && Objects.equal(this.zbb, authorizationRequest.zbb) && Objects.equal(this.zbe, authorizationRequest.zbe) && Objects.equal(this.zbf, authorizationRequest.zbf) && Objects.equal(this.zbg, authorizationRequest.zbg)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public Account getAccount() {
        return this.zbe;
    }
    
    public String getHostedDomain() {
        return this.zbf;
    }
    
    public List<Scope> getRequestedScopes() {
        return this.zba;
    }
    
    public String getServerClientId() {
        return this.zbb;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zba, this.zbb, this.zbc, this.zbh, this.zbd, this.zbe, this.zbf, this.zbg);
    }
    
    public boolean isForceCodeForRefreshToken() {
        return this.zbh;
    }
    
    public boolean isOfflineAccessRequested() {
        return this.zbc;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeTypedList(parcel, 1, this.getRequestedScopes(), false);
        SafeParcelWriter.writeString(parcel, 2, this.getServerClientId(), false);
        SafeParcelWriter.writeBoolean(parcel, 3, this.isOfflineAccessRequested());
        SafeParcelWriter.writeBoolean(parcel, 4, this.zbd);
        SafeParcelWriter.writeParcelable(parcel, 5, (Parcelable)this.getAccount(), n, false);
        SafeParcelWriter.writeString(parcel, 6, this.getHostedDomain(), false);
        SafeParcelWriter.writeString(parcel, 7, this.zbg, false);
        SafeParcelWriter.writeBoolean(parcel, 8, this.isForceCodeForRefreshToken());
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public static final class Builder
    {
        private List zba;
        private String zbb;
        private boolean zbc;
        private boolean zbd;
        private Account zbe;
        private String zbf;
        private String zbg;
        private boolean zbh;
        
        private final String zbc(final String anObject) {
            Preconditions.checkNotNull(anObject);
            final String zbb = this.zbb;
            boolean b = true;
            if (zbb != null) {
                b = (zbb.equals(anObject) && b);
            }
            Preconditions.checkArgument(b, (Object)"two different server client ids provided");
            return anObject;
        }
        
        public AuthorizationRequest build() {
            return new AuthorizationRequest(this.zba, this.zbb, this.zbc, this.zbd, this.zbe, this.zbf, this.zbg, this.zbh);
        }
        
        public Builder filterByHostedDomain(final String s) {
            this.zbf = Preconditions.checkNotEmpty(s);
            return this;
        }
        
        public Builder requestOfflineAccess(final String s) {
            this.requestOfflineAccess(s, false);
            return this;
        }
        
        public Builder requestOfflineAccess(final String zbb, final boolean zbh) {
            this.zbc(zbb);
            this.zbb = zbb;
            this.zbc = true;
            this.zbh = zbh;
            return this;
        }
        
        public Builder setAccount(final Account account) {
            this.zbe = Preconditions.checkNotNull(account);
            return this;
        }
        
        public Builder setRequestedScopes(final List<Scope> zba) {
            boolean b = false;
            if (zba != null) {
                b = b;
                if (!zba.isEmpty()) {
                    b = true;
                }
            }
            Preconditions.checkArgument(b, (Object)"requestedScopes cannot be null or empty");
            this.zba = zba;
            return this;
        }
        
        @ShowFirstParty
        public final Builder zba(final String zbb) {
            this.zbc(zbb);
            this.zbb = zbb;
            this.zbd = true;
            return this;
        }
        
        public final Builder zbb(final String zbg) {
            this.zbg = zbg;
            return this;
        }
    }
}

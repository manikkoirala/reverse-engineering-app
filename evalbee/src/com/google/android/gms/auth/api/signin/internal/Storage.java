// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.signin.internal;

import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import org.json.JSONException;
import android.text.TextUtils;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.VisibleForTesting;
import android.content.Context;
import java.util.concurrent.locks.ReentrantLock;
import android.content.SharedPreferences;
import java.util.concurrent.locks.Lock;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
public class Storage
{
    private static final Lock zaa;
    private static Storage zab;
    private final Lock zac;
    private final SharedPreferences zad;
    
    static {
        zaa = new ReentrantLock();
    }
    
    @VisibleForTesting
    public Storage(final Context context) {
        this.zac = new ReentrantLock();
        this.zad = context.getSharedPreferences("com.google.android.gms.signin", 0);
    }
    
    @KeepForSdk
    public static Storage getInstance(final Context context) {
        Preconditions.checkNotNull(context);
        final Lock zaa = Storage.zaa;
        zaa.lock();
        try {
            if (Storage.zab == null) {
                Storage.zab = new Storage(context.getApplicationContext());
            }
            final Storage zab = Storage.zab;
            zaa.unlock();
            return zab;
        }
        finally {
            Storage.zaa.unlock();
        }
    }
    
    private static final String zae(final String str, final String s) {
        final StringBuilder sb = new StringBuilder(str.length() + 1 + String.valueOf(s).length());
        sb.append(str);
        sb.append(":");
        sb.append(s);
        return sb.toString();
    }
    
    @KeepForSdk
    public void clear() {
        this.zac.lock();
        try {
            this.zad.edit().clear().apply();
        }
        finally {
            this.zac.unlock();
        }
    }
    
    @KeepForSdk
    public GoogleSignInAccount getSavedDefaultGoogleSignInAccount() {
        final String zaa = this.zaa("defaultGoogleSignInAccount");
        final boolean empty = TextUtils.isEmpty((CharSequence)zaa);
        final GoogleSignInAccount googleSignInAccount = null;
        if (empty) {
            return googleSignInAccount;
        }
        final String zaa2 = this.zaa(zae("googleSignInAccount", zaa));
        GoogleSignInAccount zab = googleSignInAccount;
        if (zaa2 == null) {
            return zab;
        }
        try {
            zab = GoogleSignInAccount.zab(zaa2);
            return zab;
        }
        catch (final JSONException ex) {
            zab = googleSignInAccount;
            return zab;
        }
    }
    
    @KeepForSdk
    public GoogleSignInOptions getSavedDefaultGoogleSignInOptions() {
        final String zaa = this.zaa("defaultGoogleSignInAccount");
        final boolean empty = TextUtils.isEmpty((CharSequence)zaa);
        final GoogleSignInOptions googleSignInOptions = null;
        if (empty) {
            return googleSignInOptions;
        }
        final String zaa2 = this.zaa(zae("googleSignInOptions", zaa));
        GoogleSignInOptions zab = googleSignInOptions;
        if (zaa2 == null) {
            return zab;
        }
        try {
            zab = GoogleSignInOptions.zab(zaa2);
            return zab;
        }
        catch (final JSONException ex) {
            zab = googleSignInOptions;
            return zab;
        }
    }
    
    @KeepForSdk
    public String getSavedRefreshToken() {
        return this.zaa("refreshToken");
    }
    
    @KeepForSdk
    public void saveDefaultGoogleSignInAccount(final GoogleSignInAccount googleSignInAccount, final GoogleSignInOptions googleSignInOptions) {
        Preconditions.checkNotNull(googleSignInAccount);
        Preconditions.checkNotNull(googleSignInOptions);
        this.zad("defaultGoogleSignInAccount", googleSignInAccount.zac());
        Preconditions.checkNotNull(googleSignInAccount);
        Preconditions.checkNotNull(googleSignInOptions);
        final String zac = googleSignInAccount.zac();
        this.zad(zae("googleSignInAccount", zac), googleSignInAccount.zad());
        this.zad(zae("googleSignInOptions", zac), googleSignInOptions.zaf());
    }
    
    public final String zaa(String string) {
        this.zac.lock();
        try {
            string = this.zad.getString(string, (String)null);
            return string;
        }
        finally {
            this.zac.unlock();
        }
    }
    
    public final void zab(final String s) {
        this.zac.lock();
        try {
            this.zad.edit().remove(s).apply();
        }
        finally {
            this.zac.unlock();
        }
    }
    
    public final void zac() {
        final String zaa = this.zaa("defaultGoogleSignInAccount");
        this.zab("defaultGoogleSignInAccount");
        if (TextUtils.isEmpty((CharSequence)zaa)) {
            return;
        }
        this.zab(zae("googleSignInAccount", zaa));
        this.zab(zae("googleSignInOptions", zaa));
    }
    
    public final void zad(final String s, final String s2) {
        this.zac.lock();
        try {
            this.zad.edit().putString(s, s2).apply();
        }
        finally {
            this.zac.unlock();
        }
    }
}

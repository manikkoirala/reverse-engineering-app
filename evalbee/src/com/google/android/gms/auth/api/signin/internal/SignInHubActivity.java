// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.signin.internal;

import android.os.BaseBundle;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import android.content.Context;
import com.google.android.gms.auth.api.signin.SignInAccount;
import android.view.accessibility.AccessibilityEvent;
import android.content.ActivityNotFoundException;
import android.util.Log;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;
import android.os.Bundle;
import android.content.Intent;
import com.google.android.gms.common.annotation.KeepName;
import androidx.fragment.app.e;

@KeepName
public class SignInHubActivity extends e
{
    private static boolean zba = false;
    private boolean zbb;
    private SignInConfiguration zbc;
    private boolean zbd;
    private int zbe;
    private Intent zbf;
    
    public SignInHubActivity() {
        this.zbb = false;
    }
    
    private final void zbc() {
        this.getSupportLoaderManager().c(0, null, (rk0.a)new zbw(this, null));
        SignInHubActivity.zba = false;
    }
    
    private final void zbd(final int n) {
        final Status status = new Status(n);
        final Intent intent = new Intent();
        intent.putExtra("googleSignInStatus", (Parcelable)status);
        this.setResult(0, intent);
        this.finish();
        SignInHubActivity.zba = false;
    }
    
    private final void zbe(String packageName) {
        final Intent intent = new Intent(packageName);
        if (packageName.equals("com.google.android.gms.auth.GOOGLE_SIGN_IN")) {
            packageName = "com.google.android.gms";
        }
        else {
            packageName = ((Context)this).getPackageName();
        }
        intent.setPackage(packageName);
        intent.putExtra("config", (Parcelable)this.zbc);
        try {
            this.startActivityForResult(intent, 40962);
        }
        catch (final ActivityNotFoundException ex) {
            this.zbb = true;
            Log.w("AuthSignInClient", "Could not launch sign in Intent. Google Play Service is probably being updated...");
            this.zbd(17);
        }
    }
    
    public final boolean dispatchPopulateAccessibilityEvent(final AccessibilityEvent accessibilityEvent) {
        return true;
    }
    
    @Override
    public final void onActivityResult(int n, int intExtra, final Intent zbf) {
        if (this.zbb) {
            return;
        }
        this.setResult(0);
        if (n != 40962) {
            return;
        }
        if (zbf != null) {
            final SignInAccount signInAccount = (SignInAccount)zbf.getParcelableExtra("signInAccount");
            if (signInAccount != null && signInAccount.zba() != null) {
                final GoogleSignInAccount zba = signInAccount.zba();
                final zbn zbc = zbn.zbc((Context)this);
                final GoogleSignInOptions zba2 = this.zbc.zba();
                zba.getClass();
                zbc.zbe(zba2, zba);
                zbf.removeExtra("signInAccount");
                zbf.putExtra("googleSignInAccount", (Parcelable)zba);
                this.zbd = true;
                this.zbe = intExtra;
                this.zbf = zbf;
                this.zbc();
                return;
            }
            if (zbf.hasExtra("errorCode")) {
                intExtra = zbf.getIntExtra("errorCode", 8);
                if ((n = intExtra) == 13) {
                    n = 12501;
                }
                this.zbd(n);
                return;
            }
        }
        this.zbd(8);
    }
    
    @Override
    public final void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        final Intent intent = this.getIntent();
        final String action = intent.getAction();
        action.getClass();
        if ("com.google.android.gms.auth.NO_IMPL".equals(action)) {
            this.zbd(12500);
            return;
        }
        if (!action.equals("com.google.android.gms.auth.GOOGLE_SIGN_IN") && !action.equals("com.google.android.gms.auth.APPAUTH_SIGN_IN")) {
            Log.e("AuthSignInClient", "Unknown action: ".concat(String.valueOf(intent.getAction())));
            this.finish();
            return;
        }
        final Bundle bundleExtra = intent.getBundleExtra("config");
        bundleExtra.getClass();
        final SignInConfiguration zbc = (SignInConfiguration)bundleExtra.getParcelable("config");
        if (zbc == null) {
            Log.e("AuthSignInClient", "Activity started with invalid configuration.");
            this.setResult(0);
            this.finish();
            return;
        }
        this.zbc = zbc;
        if (bundle != null) {
            final boolean boolean1 = ((BaseBundle)bundle).getBoolean("signingInGoogleApiClients");
            this.zbd = boolean1;
            if (boolean1) {
                this.zbe = ((BaseBundle)bundle).getInt("signInResultCode");
                final Intent zbf = (Intent)bundle.getParcelable("signInResultData");
                zbf.getClass();
                this.zbf = zbf;
                this.zbc();
            }
            return;
        }
        if (SignInHubActivity.zba) {
            this.setResult(0);
            this.zbd(12502);
            return;
        }
        SignInHubActivity.zba = true;
        this.zbe(action);
    }
    
    @Override
    public final void onDestroy() {
        super.onDestroy();
        SignInHubActivity.zba = false;
    }
    
    @Override
    public final void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        ((BaseBundle)bundle).putBoolean("signingInGoogleApiClients", this.zbd);
        if (this.zbd) {
            ((BaseBundle)bundle).putInt("signInResultCode", this.zbe);
            bundle.putParcelable("signInResultData", (Parcelable)this.zbf);
        }
    }
}

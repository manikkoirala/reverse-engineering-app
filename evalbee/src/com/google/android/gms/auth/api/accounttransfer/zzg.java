// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.accounttransfer;

import com.google.android.gms.internal.auth.zzat;
import com.google.android.gms.internal.auth.zzau;
import com.google.android.gms.internal.auth.zzaq;

final class zzg extends zzl
{
    final zzaq zza;
    
    public zzg(final AccountTransferClient accountTransferClient, final int n, final zzaq zza) {
        this.zza = zza;
        super(1608, null);
    }
    
    @Override
    public final void zza(final zzau zzau) {
        zzau.zzd((zzat)new zzf(this, this), this.zza);
    }
}

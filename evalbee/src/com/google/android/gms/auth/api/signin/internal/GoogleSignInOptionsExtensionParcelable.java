// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.signin.internal;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.auth.api.signin.GoogleSignInOptionsExtension;
import android.os.Bundle;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "GoogleSignInOptionsExtensionCreator")
public class GoogleSignInOptionsExtensionParcelable extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<GoogleSignInOptionsExtensionParcelable> CREATOR;
    @VersionField(id = 1)
    final int zaa;
    @Field(getter = "getType", id = 2)
    private int zab;
    @Field(getter = "getBundle", id = 3)
    private Bundle zac;
    
    static {
        CREATOR = (Parcelable$Creator)new zaa();
    }
    
    @Constructor
    public GoogleSignInOptionsExtensionParcelable(@Param(id = 1) final int zaa, @Param(id = 2) final int zab, @Param(id = 3) final Bundle zac) {
        this.zaa = zaa;
        this.zab = zab;
        this.zac = zac;
    }
    
    public GoogleSignInOptionsExtensionParcelable(final GoogleSignInOptionsExtension googleSignInOptionsExtension) {
        this(1, googleSignInOptionsExtension.getExtensionType(), googleSignInOptionsExtension.toBundle());
    }
    
    @KeepForSdk
    public int getType() {
        return this.zab;
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.zaa);
        SafeParcelWriter.writeInt(parcel, 2, this.getType());
        SafeParcelWriter.writeBundle(parcel, 3, this.zac, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.credentials;

import com.google.android.gms.internal.auth_api.zbn;
import android.app.PendingIntent;
import com.google.android.gms.common.internal.PendingResultUtil;
import com.google.android.gms.tasks.Task;
import android.content.Context;
import com.google.android.gms.common.api.internal.StatusExceptionMapper;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.internal.ApiExceptionMapper;
import android.app.Activity;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.GoogleApi;

@Deprecated
public class CredentialsClient extends GoogleApi<Auth.AuthCredentialsOptions>
{
    public CredentialsClient(final Activity activity, final Auth.AuthCredentialsOptions authCredentialsOptions) {
        super(activity, (Api<Api.ApiOptions>)Auth.CREDENTIALS_API, (Api.ApiOptions)authCredentialsOptions, new ApiExceptionMapper());
    }
    
    public CredentialsClient(final Context context, final Auth.AuthCredentialsOptions authCredentialsOptions) {
        super(context, (Api<Api.ApiOptions>)Auth.CREDENTIALS_API, (Api.ApiOptions)authCredentialsOptions, new Settings.Builder().setMapper(new ApiExceptionMapper()).build());
    }
    
    @Deprecated
    public Task<Void> delete(final Credential credential) {
        return PendingResultUtil.toVoidTask(Auth.CredentialsApi.delete(this.asGoogleApiClient(), credential));
    }
    
    @Deprecated
    public Task<Void> disableAutoSignIn() {
        return PendingResultUtil.toVoidTask(Auth.CredentialsApi.disableAutoSignIn(this.asGoogleApiClient()));
    }
    
    @Deprecated
    public PendingIntent getHintPickerIntent(final HintRequest hintRequest) {
        return zbn.zba(this.getApplicationContext(), (Auth.AuthCredentialsOptions)this.getApiOptions(), hintRequest, ((Auth.AuthCredentialsOptions)this.getApiOptions()).zbd());
    }
    
    @Deprecated
    public Task<CredentialRequestResponse> request(final CredentialRequest credentialRequest) {
        return PendingResultUtil.toResponseTask(Auth.CredentialsApi.request(this.asGoogleApiClient(), credentialRequest), new CredentialRequestResponse());
    }
    
    @Deprecated
    public Task<Void> save(final Credential credential) {
        return PendingResultUtil.toVoidTask(Auth.CredentialsApi.save(this.asGoogleApiClient(), credential));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.accounttransfer;

import android.content.Context;
import android.app.Activity;
import com.google.android.gms.internal.auth.zzao;
import com.google.android.gms.common.api.Api;

public final class AccountTransfer
{
    public static final String ACTION_ACCOUNT_EXPORT_DATA_AVAILABLE = "com.google.android.gms.auth.ACCOUNT_EXPORT_DATA_AVAILABLE";
    public static final String ACTION_ACCOUNT_IMPORT_DATA_AVAILABLE = "com.google.android.gms.auth.ACCOUNT_IMPORT_DATA_AVAILABLE";
    public static final String ACTION_START_ACCOUNT_EXPORT = "com.google.android.gms.auth.START_ACCOUNT_EXPORT";
    public static final String KEY_EXTRA_ACCOUNT_TYPE = "key_extra_account_type";
    public static final Api zza;
    @Deprecated
    public static final zzao zzb;
    @Deprecated
    public static final zzao zzc;
    private static final Api.ClientKey zzd;
    private static final Api.AbstractClientBuilder zze;
    
    static {
        zza = new Api("AccountTransfer.ACCOUNT_TRANSFER_API", zze = new zza(), zzd = new Api.ClientKey());
        zzb = new zzao();
        zzc = new zzao();
    }
    
    private AccountTransfer() {
    }
    
    public static AccountTransferClient getAccountTransferClient(final Activity activity) {
        return new AccountTransferClient(activity, null);
    }
    
    public static AccountTransferClient getAccountTransferClient(final Context context) {
        return new AccountTransferClient(context, null);
    }
}

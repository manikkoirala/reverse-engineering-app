// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.signin.internal;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;

public class zba extends zbq
{
    public void zbb(final Status status) {
        throw new UnsupportedOperationException();
    }
    
    public void zbc(final Status status) {
        throw new UnsupportedOperationException();
    }
    
    public void zbd(final GoogleSignInAccount googleSignInAccount, final Status status) {
        throw new UnsupportedOperationException();
    }
}

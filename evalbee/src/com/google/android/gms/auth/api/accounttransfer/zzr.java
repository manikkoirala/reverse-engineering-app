// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.accounttransfer;

import android.os.BaseBundle;
import java.util.List;
import java.util.Collections;
import java.util.Collection;
import java.util.Iterator;
import com.google.android.gms.common.internal.Objects;
import java.util.ArrayList;
import android.os.Bundle;
import com.google.android.gms.common.api.Api;

public final class zzr implements Optional
{
    public static final zzr zza;
    private final Bundle zzb = zzb;
    
    static {
        final Bundle bundle = new Bundle();
        if (!((BaseBundle)bundle).containsKey("accountTypes")) {
            bundle.putStringArrayList("accountTypes", new ArrayList(0));
        }
        zza = new zzr(bundle, null);
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof zzr)) {
            return false;
        }
        final zzr zzr = (zzr)o;
        final Bundle zza = this.zza();
        final Bundle zza2 = zzr.zza();
        if (((BaseBundle)zza).size() != ((BaseBundle)zza2).size()) {
            return false;
        }
        for (final String s : ((BaseBundle)zza).keySet()) {
            if (!((BaseBundle)zza2).containsKey(s)) {
                return false;
            }
            if (!Objects.equal(((BaseBundle)zza).get(s), ((BaseBundle)zza2).get(s))) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public final int hashCode() {
        final Bundle zza = this.zza();
        final int size = ((BaseBundle)zza).size();
        final ArrayList list = new ArrayList<String>(size + size);
        final ArrayList list2 = new ArrayList<Object>(((BaseBundle)zza).keySet());
        Collections.sort((List<Comparable>)list2);
        for (int size2 = list2.size(), i = 0; i < size2; ++i) {
            final String e = (String)list2.get(i);
            list.add(e);
            list.add((String)((BaseBundle)zza).get(e));
        }
        return Objects.hashCode(list);
    }
    
    public final Bundle zza() {
        return new Bundle(this.zzb);
    }
}

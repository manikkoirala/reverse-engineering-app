// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.signin.internal;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "SignInConfigurationCreator")
@Reserved({ 1 })
public final class SignInConfiguration extends AbstractSafeParcelable implements ReflectedParcelable
{
    public static final Parcelable$Creator<SignInConfiguration> CREATOR;
    @Field(getter = "getConsumerPkgName", id = 2)
    private final String zba;
    @Field(getter = "getGoogleConfig", id = 5)
    private final GoogleSignInOptions zbb;
    
    static {
        CREATOR = (Parcelable$Creator)new zbu();
    }
    
    @Constructor
    public SignInConfiguration(@Param(id = 2) final String s, @Param(id = 5) final GoogleSignInOptions zbb) {
        this.zba = Preconditions.checkNotEmpty(s);
        this.zbb = zbb;
    }
    
    @Override
    public final boolean equals(final Object o) {
        if (!(o instanceof SignInConfiguration)) {
            return false;
        }
        final SignInConfiguration signInConfiguration = (SignInConfiguration)o;
        if (this.zba.equals(signInConfiguration.zba)) {
            final GoogleSignInOptions zbb = this.zbb;
            final GoogleSignInOptions zbb2 = signInConfiguration.zbb;
            if (zbb == null) {
                if (zbb2 != null) {
                    return false;
                }
            }
            else if (!zbb.equals(zbb2)) {
                return false;
            }
            return true;
        }
        return false;
    }
    
    @Override
    public final int hashCode() {
        return new HashAccumulator().addObject(this.zba).addObject(this.zbb).hash();
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 2, this.zba, false);
        SafeParcelWriter.writeParcelable(parcel, 5, (Parcelable)this.zbb, n, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public final GoogleSignInOptions zba() {
        return this.zbb;
    }
}

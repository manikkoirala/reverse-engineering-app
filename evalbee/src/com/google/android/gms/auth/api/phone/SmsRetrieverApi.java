// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.phone;

import com.google.android.gms.tasks.Task;

public interface SmsRetrieverApi
{
    Task<Void> startSmsRetriever();
    
    Task<Void> startSmsUserConsent(final String p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api;

import android.content.Context;
import com.google.android.gms.internal.auth.zzbo;
import com.google.android.gms.auth.api.proxy.ProxyClient;
import android.app.Activity;
import com.google.android.gms.internal.auth.zzbt;
import com.google.android.gms.auth.api.proxy.ProxyApi;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.internal.ShowFirstParty;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
@ShowFirstParty
public final class AuthProxy
{
    @KeepForSdk
    @ShowFirstParty
    public static final Api<AuthProxyOptions> API;
    @KeepForSdk
    @ShowFirstParty
    public static final ProxyApi ProxyApi;
    public static final Api.ClientKey zza;
    private static final Api.AbstractClientBuilder zzb;
    
    static {
        API = new Api<AuthProxyOptions>("Auth.PROXY_API", zzb = new zza(), zza = new Api.ClientKey());
        ProxyApi = (ProxyApi)new zzbt();
    }
    
    @KeepForSdk
    public static ProxyClient getClient(final Activity activity, final AuthProxyOptions authProxyOptions) {
        return (ProxyClient)new zzbo(activity, authProxyOptions);
    }
    
    @KeepForSdk
    public static ProxyClient getClient(final Context context, final AuthProxyOptions authProxyOptions) {
        return (ProxyClient)new zzbo(context, authProxyOptions);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.signin.internal;

import com.google.android.gms.common.api.Api;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import android.content.Context;

final class zbg extends zbl
{
    final Context zba;
    final GoogleSignInOptions zbb;
    
    public zbg(final GoogleApiClient googleApiClient, final Context zba, final GoogleSignInOptions zbb) {
        this.zba = zba;
        this.zbb = zbb;
        super(googleApiClient);
    }
}

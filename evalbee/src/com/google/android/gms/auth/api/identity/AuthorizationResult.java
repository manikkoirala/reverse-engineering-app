// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.identity;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import android.app.PendingIntent;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import java.util.List;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "AuthorizationResultCreator")
public final class AuthorizationResult extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<AuthorizationResult> CREATOR;
    @Field(getter = "getServerAuthCode", id = 1)
    private final String zba;
    @Field(getter = "getAccessToken", id = 2)
    private final String zbb;
    @Field(getter = "getIdToken", id = 3)
    private final String zbc;
    @Field(getter = "getGrantedScopes", id = 4)
    private final List zbd;
    @Field(getter = "toGoogleSignInAccount", id = 5)
    private final GoogleSignInAccount zbe;
    @Field(getter = "getPendingIntent", id = 6)
    private final PendingIntent zbf;
    
    static {
        CREATOR = (Parcelable$Creator)new zbe();
    }
    
    @Constructor
    public AuthorizationResult(@Param(id = 1) final String zba, @Param(id = 2) final String zbb, @Param(id = 3) final String zbc, @Param(id = 4) final List<String> list, @Param(id = 5) final GoogleSignInAccount zbe, @Param(id = 6) final PendingIntent zbf) {
        this.zba = zba;
        this.zbb = zbb;
        this.zbc = zbc;
        this.zbd = Preconditions.checkNotNull(list);
        this.zbf = zbf;
        this.zbe = zbe;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof AuthorizationResult)) {
            return false;
        }
        final AuthorizationResult authorizationResult = (AuthorizationResult)o;
        return Objects.equal(this.zba, authorizationResult.zba) && Objects.equal(this.zbb, authorizationResult.zbb) && Objects.equal(this.zbc, authorizationResult.zbc) && Objects.equal(this.zbd, authorizationResult.zbd) && Objects.equal(this.zbf, authorizationResult.zbf) && Objects.equal(this.zbe, authorizationResult.zbe);
    }
    
    public String getAccessToken() {
        return this.zbb;
    }
    
    public List<String> getGrantedScopes() {
        return this.zbd;
    }
    
    public PendingIntent getPendingIntent() {
        return this.zbf;
    }
    
    public String getServerAuthCode() {
        return this.zba;
    }
    
    public boolean hasResolution() {
        return this.zbf != null;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zba, this.zbb, this.zbc, this.zbd, this.zbf, this.zbe);
    }
    
    public GoogleSignInAccount toGoogleSignInAccount() {
        return this.zbe;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.getServerAuthCode(), false);
        SafeParcelWriter.writeString(parcel, 2, this.getAccessToken(), false);
        SafeParcelWriter.writeString(parcel, 3, this.zbc, false);
        SafeParcelWriter.writeStringList(parcel, 4, this.getGrantedScopes(), false);
        SafeParcelWriter.writeParcelable(parcel, 5, (Parcelable)this.toGoogleSignInAccount(), n, false);
        SafeParcelWriter.writeParcelable(parcel, 6, (Parcelable)this.getPendingIntent(), n, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.signin.internal;

import com.google.android.gms.common.api.Status;

final class zbj extends zba
{
    final zbk zba;
    
    public zbj(final zbk zba) {
        this.zba = zba;
    }
    
    @Override
    public final void zbb(final Status result) {
        this.zba.setResult(result);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.credentials;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Deprecated
@Class(creator = "HintRequestCreator")
public final class HintRequest extends AbstractSafeParcelable implements ReflectedParcelable
{
    public static final Parcelable$Creator<HintRequest> CREATOR;
    @Field(id = 1000)
    final int zba;
    @Field(getter = "getHintPickerConfig", id = 1)
    private final CredentialPickerConfig zbb;
    @Field(getter = "isEmailAddressIdentifierSupported", id = 2)
    private final boolean zbc;
    @Field(getter = "isPhoneNumberIdentifierSupported", id = 3)
    private final boolean zbd;
    @Field(getter = "getAccountTypes", id = 4)
    private final String[] zbe;
    @Field(getter = "isIdTokenRequested", id = 5)
    private final boolean zbf;
    @Field(getter = "getServerClientId", id = 6)
    private final String zbg;
    @Field(getter = "getIdTokenNonce", id = 7)
    private final String zbh;
    
    static {
        CREATOR = (Parcelable$Creator)new zbe();
    }
    
    @Constructor
    public HintRequest(@Param(id = 1000) final int zba, @Param(id = 1) final CredentialPickerConfig credentialPickerConfig, @Param(id = 2) final boolean zbc, @Param(id = 3) final boolean zbd, @Param(id = 4) final String[] array, @Param(id = 5) final boolean zbf, @Param(id = 6) final String zbg, @Param(id = 7) final String zbh) {
        this.zba = zba;
        this.zbb = Preconditions.checkNotNull(credentialPickerConfig);
        this.zbc = zbc;
        this.zbd = zbd;
        this.zbe = Preconditions.checkNotNull(array);
        if (zba < 2) {
            this.zbf = true;
            this.zbg = null;
            this.zbh = null;
            return;
        }
        this.zbf = zbf;
        this.zbg = zbg;
        this.zbh = zbh;
    }
    
    public String[] getAccountTypes() {
        return this.zbe;
    }
    
    public CredentialPickerConfig getHintPickerConfig() {
        return this.zbb;
    }
    
    public String getIdTokenNonce() {
        return this.zbh;
    }
    
    public String getServerClientId() {
        return this.zbg;
    }
    
    public boolean isEmailAddressIdentifierSupported() {
        return this.zbc;
    }
    
    public boolean isIdTokenRequested() {
        return this.zbf;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeParcelable(parcel, 1, (Parcelable)this.getHintPickerConfig(), n, false);
        SafeParcelWriter.writeBoolean(parcel, 2, this.isEmailAddressIdentifierSupported());
        SafeParcelWriter.writeBoolean(parcel, 3, this.zbd);
        SafeParcelWriter.writeStringArray(parcel, 4, this.getAccountTypes(), false);
        SafeParcelWriter.writeBoolean(parcel, 5, this.isIdTokenRequested());
        SafeParcelWriter.writeString(parcel, 6, this.getServerClientId(), false);
        SafeParcelWriter.writeString(parcel, 7, this.getIdTokenNonce(), false);
        SafeParcelWriter.writeInt(parcel, 1000, this.zba);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public static final class Builder
    {
        private boolean zba;
        private boolean zbb;
        private String[] zbc;
        private CredentialPickerConfig zbd;
        private boolean zbe;
        private String zbf;
        private String zbg;
        
        public Builder() {
            this.zbd = new CredentialPickerConfig.Builder().build();
            this.zbe = false;
        }
        
        public HintRequest build() {
            if (this.zbc == null) {
                this.zbc = new String[0];
            }
            if (!this.zba && !this.zbb && this.zbc.length == 0) {
                throw new IllegalStateException("At least one authentication method must be specified");
            }
            return new HintRequest(2, this.zbd, this.zba, this.zbb, this.zbc, this.zbe, this.zbf, this.zbg);
        }
        
        public Builder setAccountTypes(final String... array) {
            String[] zbc = array;
            if (array == null) {
                zbc = new String[0];
            }
            this.zbc = zbc;
            return this;
        }
        
        public Builder setEmailAddressIdentifierSupported(final boolean zba) {
            this.zba = zba;
            return this;
        }
        
        public Builder setHintPickerConfig(final CredentialPickerConfig credentialPickerConfig) {
            this.zbd = Preconditions.checkNotNull(credentialPickerConfig);
            return this;
        }
        
        public Builder setIdTokenNonce(final String zbg) {
            this.zbg = zbg;
            return this;
        }
        
        public Builder setIdTokenRequested(final boolean zbe) {
            this.zbe = zbe;
            return this;
        }
        
        public Builder setPhoneNumberIdentifierSupported(final boolean zbb) {
            this.zbb = zbb;
            return this;
        }
        
        public Builder setServerClientId(final String zbf) {
            this.zbf = zbf;
            return this;
        }
    }
}

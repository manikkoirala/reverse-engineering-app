// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api;

import com.google.android.gms.common.internal.Objects;
import android.os.Bundle;
import com.google.android.gms.common.internal.ShowFirstParty;
import com.google.android.gms.common.annotation.KeepForSdk;
import com.google.android.gms.common.api.Api;

@KeepForSdk
@ShowFirstParty
public final class AuthProxyOptions implements Optional
{
    public static final AuthProxyOptions zza;
    private final Bundle zzb = zzb;
    
    static {
        zza = new AuthProxyOptions(new Bundle(), null);
    }
    
    @Override
    public final boolean equals(final Object o) {
        return o == this || (o instanceof AuthProxyOptions && Objects.checkBundlesEquality(this.zzb, ((AuthProxyOptions)o).zzb));
    }
    
    @Override
    public final int hashCode() {
        return Objects.hashCode(this.zzb);
    }
    
    public final Bundle zza() {
        return new Bundle(this.zzb);
    }
}

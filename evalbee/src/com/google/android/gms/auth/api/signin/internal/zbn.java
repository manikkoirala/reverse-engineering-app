// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.signin.internal;

import android.content.Context;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

public final class zbn
{
    private static zbn zbd;
    final Storage zba;
    GoogleSignInAccount zbb;
    GoogleSignInOptions zbc;
    
    private zbn(final Context context) {
        final Storage instance = Storage.getInstance(context);
        this.zba = instance;
        this.zbb = instance.getSavedDefaultGoogleSignInAccount();
        this.zbc = instance.getSavedDefaultGoogleSignInOptions();
    }
    
    public static zbn zbc(final Context context) {
        synchronized (zbn.class) {
            return zbf(context.getApplicationContext());
        }
    }
    
    private static zbn zbf(final Context context) {
        synchronized (zbn.class) {
            final zbn zbd = zbn.zbd;
            if (zbd != null) {
                return zbd;
            }
            return zbn.zbd = new zbn(context);
        }
    }
    
    public final GoogleSignInAccount zba() {
        synchronized (this) {
            return this.zbb;
        }
    }
    
    public final GoogleSignInOptions zbb() {
        synchronized (this) {
            return this.zbc;
        }
    }
    
    public final void zbd() {
        synchronized (this) {
            this.zba.clear();
            this.zbb = null;
            this.zbc = null;
        }
    }
    
    public final void zbe(final GoogleSignInOptions zbc, final GoogleSignInAccount zbb) {
        synchronized (this) {
            this.zba.saveDefaultGoogleSignInAccount(zbb, zbc);
            this.zbb = zbb;
            this.zbc = zbc;
        }
    }
}

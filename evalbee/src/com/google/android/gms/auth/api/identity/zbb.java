// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.identity;

import com.google.android.gms.common.internal.Preconditions;

public final class zbb
{
    private String zba;
    
    private zbb() {
    }
    
    public static final zbb zbc(final zbc zbc) {
        final String zbb = zbc.zbb();
        final zbb zbb2 = new zbb();
        if (zbb != null) {
            zbb2.zba = Preconditions.checkNotEmpty(zbb);
        }
        return zbb2;
    }
    
    public final zbb zba(final String s) {
        this.zba = Preconditions.checkNotEmpty(s);
        return this;
    }
    
    public final zbc zbb() {
        return new zbc(this.zba);
    }
}

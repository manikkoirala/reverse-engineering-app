// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.identity;

import java.util.ArrayList;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import java.util.Collection;
import android.text.TextUtils;
import com.google.android.gms.common.internal.Preconditions;
import java.util.List;
import android.app.PendingIntent;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "SaveAccountLinkingTokenRequestCreator")
public class SaveAccountLinkingTokenRequest extends AbstractSafeParcelable implements ReflectedParcelable
{
    public static final Parcelable$Creator<SaveAccountLinkingTokenRequest> CREATOR;
    public static final String EXTRA_TOKEN = "extra_token";
    public static final String TOKEN_TYPE_AUTH_CODE = "auth_code";
    @Field(getter = "getConsentPendingIntent", id = 1)
    private final PendingIntent zba;
    @Field(getter = "getTokenType", id = 2)
    private final String zbb;
    @Field(getter = "getServiceId", id = 3)
    private final String zbc;
    @Field(getter = "getScopes", id = 4)
    private final List zbd;
    @Field(getter = "getSessionId", id = 5)
    private final String zbe;
    @Field(getter = "getTheme", id = 6)
    private final int zbf;
    
    static {
        CREATOR = (Parcelable$Creator)new zbp();
    }
    
    @Constructor
    public SaveAccountLinkingTokenRequest(@Param(id = 1) final PendingIntent zba, @Param(id = 2) final String zbb, @Param(id = 3) final String zbc, @Param(id = 4) final List zbd, @Param(id = 5) final String zbe, @Param(id = 6) final int zbf) {
        this.zba = zba;
        this.zbb = zbb;
        this.zbc = zbc;
        this.zbd = zbd;
        this.zbe = zbe;
        this.zbf = zbf;
    }
    
    public static Builder builder() {
        return new Builder();
    }
    
    public static Builder zba(final SaveAccountLinkingTokenRequest saveAccountLinkingTokenRequest) {
        Preconditions.checkNotNull(saveAccountLinkingTokenRequest);
        final Builder builder = builder();
        builder.setScopes(saveAccountLinkingTokenRequest.getScopes());
        builder.setServiceId(saveAccountLinkingTokenRequest.getServiceId());
        builder.setConsentPendingIntent(saveAccountLinkingTokenRequest.getConsentPendingIntent());
        builder.setTokenType(saveAccountLinkingTokenRequest.getTokenType());
        builder.zbb(saveAccountLinkingTokenRequest.zbf);
        final String zbe = saveAccountLinkingTokenRequest.zbe;
        if (!TextUtils.isEmpty((CharSequence)zbe)) {
            builder.zba(zbe);
        }
        return builder;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof SaveAccountLinkingTokenRequest)) {
            return false;
        }
        final SaveAccountLinkingTokenRequest saveAccountLinkingTokenRequest = (SaveAccountLinkingTokenRequest)o;
        if (this.zbd.size() == saveAccountLinkingTokenRequest.zbd.size()) {
            if (this.zbd.containsAll(saveAccountLinkingTokenRequest.zbd)) {
                if (Objects.equal(this.zba, saveAccountLinkingTokenRequest.zba) && Objects.equal(this.zbb, saveAccountLinkingTokenRequest.zbb) && Objects.equal(this.zbc, saveAccountLinkingTokenRequest.zbc) && Objects.equal(this.zbe, saveAccountLinkingTokenRequest.zbe) && this.zbf == saveAccountLinkingTokenRequest.zbf) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public PendingIntent getConsentPendingIntent() {
        return this.zba;
    }
    
    public List<String> getScopes() {
        return this.zbd;
    }
    
    public String getServiceId() {
        return this.zbc;
    }
    
    public String getTokenType() {
        return this.zbb;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zba, this.zbb, this.zbc, this.zbd, this.zbe);
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeParcelable(parcel, 1, (Parcelable)this.getConsentPendingIntent(), n, false);
        SafeParcelWriter.writeString(parcel, 2, this.getTokenType(), false);
        SafeParcelWriter.writeString(parcel, 3, this.getServiceId(), false);
        SafeParcelWriter.writeStringList(parcel, 4, this.getScopes(), false);
        SafeParcelWriter.writeString(parcel, 5, this.zbe, false);
        SafeParcelWriter.writeInt(parcel, 6, this.zbf);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public static final class Builder
    {
        private PendingIntent zba;
        private String zbb;
        private String zbc;
        private List zbd;
        private String zbe;
        private int zbf;
        
        public Builder() {
            this.zbd = new ArrayList();
        }
        
        public SaveAccountLinkingTokenRequest build() {
            final PendingIntent zba = this.zba;
            final boolean b = false;
            Preconditions.checkArgument(zba != null, (Object)"Consent PendingIntent cannot be null");
            Preconditions.checkArgument("auth_code".equals(this.zbb), (Object)"Invalid tokenType");
            Preconditions.checkArgument(TextUtils.isEmpty((CharSequence)this.zbc) ^ true, (Object)"serviceId cannot be null or empty");
            boolean b2 = b;
            if (this.zbd != null) {
                b2 = true;
            }
            Preconditions.checkArgument(b2, (Object)"scopes cannot be null");
            return new SaveAccountLinkingTokenRequest(this.zba, this.zbb, this.zbc, this.zbd, this.zbe, this.zbf);
        }
        
        public Builder setConsentPendingIntent(final PendingIntent zba) {
            this.zba = zba;
            return this;
        }
        
        public Builder setScopes(final List<String> zbd) {
            this.zbd = zbd;
            return this;
        }
        
        public Builder setServiceId(final String zbc) {
            this.zbc = zbc;
            return this;
        }
        
        public Builder setTokenType(final String zbb) {
            this.zbb = zbb;
            return this;
        }
        
        public final Builder zba(final String zbe) {
            this.zbe = zbe;
            return this;
        }
        
        public final Builder zbb(final int zbf) {
            this.zbf = zbf;
            return this;
        }
    }
}

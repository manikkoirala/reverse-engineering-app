// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.identity;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.common.api.Status;
import android.content.Intent;
import com.google.android.gms.common.api.HasApiKey;

public interface CredentialSavingClient extends HasApiKey<zbh>
{
    Status getStatusFromIntent(final Intent p0);
    
    Task<SaveAccountLinkingTokenResult> saveAccountLinkingToken(final SaveAccountLinkingTokenRequest p0);
    
    Task<SavePasswordResult> savePassword(final SavePasswordRequest p0);
}

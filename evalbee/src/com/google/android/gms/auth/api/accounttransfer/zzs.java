// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.accounttransfer;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import java.util.ArrayList;
import java.util.Map;
import com.google.android.gms.common.server.response.FastJsonResponse;
import java.util.List;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.auth.zzbz;

@SafeParcelable.Class(creator = "AccountTransferProgressCreator")
public final class zzs extends zzbz
{
    public static final Parcelable$Creator<zzs> CREATOR;
    private static final r8 zzb;
    @SafeParcelable.VersionField(id = 1)
    final int zza;
    @SafeParcelable.Field(getter = "getRegisteredAccountTypes", id = 2)
    private List zzc;
    @SafeParcelable.Field(getter = "getInProgressAccountTypes", id = 3)
    private List zzd;
    @SafeParcelable.Field(getter = "getSuccessAccountTypes", id = 4)
    private List zze;
    @SafeParcelable.Field(getter = "getFailedAccountTypes", id = 5)
    private List zzf;
    @SafeParcelable.Field(getter = "getEscrowedAccountTypes", id = 6)
    private List zzg;
    
    static {
        CREATOR = (Parcelable$Creator)new zzt();
        final r8 zzb2 = new r8();
        (zzb = zzb2).put("registered", FastJsonResponse.Field.forStrings("registered", 2));
        zzb2.put("in_progress", FastJsonResponse.Field.forStrings("in_progress", 3));
        zzb2.put("success", FastJsonResponse.Field.forStrings("success", 4));
        zzb2.put("failed", FastJsonResponse.Field.forStrings("failed", 5));
        zzb2.put("escrowed", FastJsonResponse.Field.forStrings("escrowed", 6));
    }
    
    public zzs() {
        this.zza = 1;
    }
    
    @SafeParcelable.Constructor
    public zzs(@SafeParcelable.Param(id = 1) final int zza, @SafeParcelable.Param(id = 2) final List zzc, @SafeParcelable.Param(id = 3) final List zzd, @SafeParcelable.Param(id = 4) final List zze, @SafeParcelable.Param(id = 5) final List zzf, @SafeParcelable.Param(id = 6) final List zzg) {
        this.zza = zza;
        this.zzc = zzc;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
        this.zzg = zzg;
    }
    
    public final Map getFieldMappings() {
        return zzs.zzb;
    }
    
    public final Object getFieldValue(final FastJsonResponse.Field field) {
        switch (field.getSafeParcelableFieldId()) {
            default: {
                final int safeParcelableFieldId = field.getSafeParcelableFieldId();
                final StringBuilder sb = new StringBuilder();
                sb.append("Unknown SafeParcelable id=");
                sb.append(safeParcelableFieldId);
                throw new IllegalStateException(sb.toString());
            }
            case 6: {
                return this.zzg;
            }
            case 5: {
                return this.zzf;
            }
            case 4: {
                return this.zze;
            }
            case 3: {
                return this.zzd;
            }
            case 2: {
                return this.zzc;
            }
            case 1: {
                return this.zza;
            }
        }
    }
    
    public final boolean isFieldSet(final FastJsonResponse.Field field) {
        return true;
    }
    
    public final void setStringsInternal(final FastJsonResponse.Field field, final String s, final ArrayList zzg) {
        final int safeParcelableFieldId = field.getSafeParcelableFieldId();
        if (safeParcelableFieldId == 2) {
            this.zzc = zzg;
            return;
        }
        if (safeParcelableFieldId == 3) {
            this.zzd = zzg;
            return;
        }
        if (safeParcelableFieldId == 4) {
            this.zze = zzg;
            return;
        }
        if (safeParcelableFieldId == 5) {
            this.zzf = zzg;
            return;
        }
        if (safeParcelableFieldId == 6) {
            this.zzg = zzg;
            return;
        }
        throw new IllegalArgumentException(String.format("Field with id=%d is not known to be a string list.", safeParcelableFieldId));
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.zza);
        SafeParcelWriter.writeStringList(parcel, 2, this.zzc, false);
        SafeParcelWriter.writeStringList(parcel, 3, this.zzd, false);
        SafeParcelWriter.writeStringList(parcel, 4, this.zze, false);
        SafeParcelWriter.writeStringList(parcel, 5, this.zzf, false);
        SafeParcelWriter.writeStringList(parcel, 6, this.zzg, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

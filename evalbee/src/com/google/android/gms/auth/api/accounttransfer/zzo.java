// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.accounttransfer;

import android.os.Parcelable;
import java.util.List;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import java.util.Map;
import java.util.HashSet;
import com.google.android.gms.common.server.response.FastJsonResponse;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashMap;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.auth.zzbz;

@SafeParcelable.Class(creator = "AccountTransferMsgCreator")
public final class zzo extends zzbz
{
    public static final Parcelable$Creator<zzo> CREATOR;
    private static final HashMap zzc;
    @SafeParcelable.Indicator
    final Set zza;
    @SafeParcelable.VersionField(id = 1)
    final int zzb;
    @SafeParcelable.Field(getter = "getAuthenticatorDatas", id = 2)
    private ArrayList zzd;
    @SafeParcelable.Field(getter = "getRequestType", id = 3)
    private int zze;
    @SafeParcelable.Field(getter = "getProgress", id = 4)
    private zzs zzf;
    
    static {
        CREATOR = (Parcelable$Creator)new zzp();
        final HashMap zzc2 = new HashMap();
        (zzc = zzc2).put("authenticatorData", FastJsonResponse.Field.forConcreteTypeArray("authenticatorData", 2, zzu.class));
        zzc2.put("progress", FastJsonResponse.Field.forConcreteType("progress", 4, zzs.class));
    }
    
    public zzo() {
        this.zza = new HashSet(1);
        this.zzb = 1;
    }
    
    @SafeParcelable.Constructor
    public zzo(@SafeParcelable.Indicator final Set zza, @SafeParcelable.Param(id = 1) final int zzb, @SafeParcelable.Param(id = 2) final ArrayList zzd, @SafeParcelable.Param(id = 3) final int zze, @SafeParcelable.Param(id = 4) final zzs zzf) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
    }
    
    public final void addConcreteTypeArrayInternal(final FastJsonResponse.Field field, final String s, final ArrayList zzd) {
        final int safeParcelableFieldId = field.getSafeParcelableFieldId();
        if (safeParcelableFieldId == 2) {
            this.zzd = zzd;
            this.zza.add(safeParcelableFieldId);
            return;
        }
        throw new IllegalArgumentException(String.format("Field with id=%d is not a known ConcreteTypeArray type. Found %s", safeParcelableFieldId, zzd.getClass().getCanonicalName()));
    }
    
    public final void addConcreteTypeInternal(final FastJsonResponse.Field field, final String s, final FastJsonResponse fastJsonResponse) {
        final int safeParcelableFieldId = field.getSafeParcelableFieldId();
        if (safeParcelableFieldId == 4) {
            this.zzf = (zzs)fastJsonResponse;
            this.zza.add(safeParcelableFieldId);
            return;
        }
        throw new IllegalArgumentException(String.format("Field with id=%d is not a known custom type. Found %s", safeParcelableFieldId, fastJsonResponse.getClass().getCanonicalName()));
    }
    
    public final Object getFieldValue(final FastJsonResponse.Field field) {
        final int safeParcelableFieldId = field.getSafeParcelableFieldId();
        if (safeParcelableFieldId == 1) {
            return this.zzb;
        }
        if (safeParcelableFieldId == 2) {
            return this.zzd;
        }
        if (safeParcelableFieldId == 4) {
            return this.zzf;
        }
        final int safeParcelableFieldId2 = field.getSafeParcelableFieldId();
        final StringBuilder sb = new StringBuilder();
        sb.append("Unknown SafeParcelable id=");
        sb.append(safeParcelableFieldId2);
        throw new IllegalStateException(sb.toString());
    }
    
    public final boolean isFieldSet(final FastJsonResponse.Field field) {
        return this.zza.contains(field.getSafeParcelableFieldId());
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        final Set zza = this.zza;
        if (zza.contains(1)) {
            SafeParcelWriter.writeInt(parcel, 1, this.zzb);
        }
        if (zza.contains(2)) {
            SafeParcelWriter.writeTypedList(parcel, 2, (List<Parcelable>)this.zzd, true);
        }
        if (zza.contains(3)) {
            SafeParcelWriter.writeInt(parcel, 3, this.zze);
        }
        if (zza.contains(4)) {
            SafeParcelWriter.writeParcelable(parcel, 4, (Parcelable)this.zzf, n, true);
        }
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

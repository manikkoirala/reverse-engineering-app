// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.identity;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.fido.fido2.api.common.PublicKeyCredential;
import android.net.Uri;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "SignInCredentialCreator")
public final class SignInCredential extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<SignInCredential> CREATOR;
    @Field(getter = "getId", id = 1)
    private final String zba;
    @Field(getter = "getDisplayName", id = 2)
    private final String zbb;
    @Field(getter = "getGivenName", id = 3)
    private final String zbc;
    @Field(getter = "getFamilyName", id = 4)
    private final String zbd;
    @Field(getter = "getProfilePictureUri", id = 5)
    private final Uri zbe;
    @Field(getter = "getPassword", id = 6)
    private final String zbf;
    @Field(getter = "getGoogleIdToken", id = 7)
    private final String zbg;
    @Field(getter = "getPhoneNumber", id = 8)
    private final String zbh;
    @Field(getter = "getPublicKeyCredential", id = 9)
    private final PublicKeyCredential zbi;
    
    static {
        CREATOR = (Parcelable$Creator)new zbt();
    }
    
    @Constructor
    public SignInCredential(@Param(id = 1) final String s, @Param(id = 2) final String zbb, @Param(id = 3) final String zbc, @Param(id = 4) final String zbd, @Param(id = 5) final Uri zbe, @Param(id = 6) final String zbf, @Param(id = 7) final String zbg, @Param(id = 8) final String zbh, @Param(id = 9) final PublicKeyCredential zbi) {
        this.zba = Preconditions.checkNotEmpty(s);
        this.zbb = zbb;
        this.zbc = zbc;
        this.zbd = zbd;
        this.zbe = zbe;
        this.zbf = zbf;
        this.zbg = zbg;
        this.zbh = zbh;
        this.zbi = zbi;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof SignInCredential)) {
            return false;
        }
        final SignInCredential signInCredential = (SignInCredential)o;
        return Objects.equal(this.zba, signInCredential.zba) && Objects.equal(this.zbb, signInCredential.zbb) && Objects.equal(this.zbc, signInCredential.zbc) && Objects.equal(this.zbd, signInCredential.zbd) && Objects.equal(this.zbe, signInCredential.zbe) && Objects.equal(this.zbf, signInCredential.zbf) && Objects.equal(this.zbg, signInCredential.zbg) && Objects.equal(this.zbh, signInCredential.zbh) && Objects.equal(this.zbi, signInCredential.zbi);
    }
    
    public String getDisplayName() {
        return this.zbb;
    }
    
    public String getFamilyName() {
        return this.zbd;
    }
    
    public String getGivenName() {
        return this.zbc;
    }
    
    public String getGoogleIdToken() {
        return this.zbg;
    }
    
    public String getId() {
        return this.zba;
    }
    
    public String getPassword() {
        return this.zbf;
    }
    
    @Deprecated
    public String getPhoneNumber() {
        return this.zbh;
    }
    
    public Uri getProfilePictureUri() {
        return this.zbe;
    }
    
    public PublicKeyCredential getPublicKeyCredential() {
        return this.zbi;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zba, this.zbb, this.zbc, this.zbd, this.zbe, this.zbf, this.zbg, this.zbh, this.zbi);
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.getId(), false);
        SafeParcelWriter.writeString(parcel, 2, this.getDisplayName(), false);
        SafeParcelWriter.writeString(parcel, 3, this.getGivenName(), false);
        SafeParcelWriter.writeString(parcel, 4, this.getFamilyName(), false);
        SafeParcelWriter.writeParcelable(parcel, 5, (Parcelable)this.getProfilePictureUri(), n, false);
        SafeParcelWriter.writeString(parcel, 6, this.getPassword(), false);
        SafeParcelWriter.writeString(parcel, 7, this.getGoogleIdToken(), false);
        SafeParcelWriter.writeString(parcel, 8, this.getPhoneNumber(), false);
        SafeParcelWriter.writeParcelable(parcel, 9, (Parcelable)this.getPublicKeyCredential(), n, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

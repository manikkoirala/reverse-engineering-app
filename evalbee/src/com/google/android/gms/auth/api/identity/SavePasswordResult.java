// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.identity;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import android.app.PendingIntent;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "SavePasswordResultCreator")
public class SavePasswordResult extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<SavePasswordResult> CREATOR;
    @Field(getter = "getPendingIntent", id = 1)
    private final PendingIntent zba;
    
    static {
        CREATOR = (Parcelable$Creator)new zbs();
    }
    
    @Constructor
    public SavePasswordResult(@Param(id = 1) final PendingIntent pendingIntent) {
        this.zba = Preconditions.checkNotNull(pendingIntent);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof SavePasswordResult && Objects.equal(this.zba, ((SavePasswordResult)o).zba);
    }
    
    public PendingIntent getPendingIntent() {
        return this.zba;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zba);
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeParcelable(parcel, 1, (Parcelable)this.getPendingIntent(), n, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

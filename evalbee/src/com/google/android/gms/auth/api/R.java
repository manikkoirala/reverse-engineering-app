// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api;

public final class R
{
    private R() {
    }
    
    public static final class anim
    {
        public static final int fragment_fast_out_extra_slow_in = 2130771996;
        
        private anim() {
        }
    }
    
    public static final class animator
    {
        public static final int fragment_close_enter = 2130837507;
        public static final int fragment_close_exit = 2130837508;
        public static final int fragment_fade_enter = 2130837509;
        public static final int fragment_fade_exit = 2130837510;
        public static final int fragment_open_enter = 2130837511;
        public static final int fragment_open_exit = 2130837512;
        
        private animator() {
        }
    }
    
    public static final class attr
    {
        public static final int alpha = 2130968627;
        public static final int buttonSize = 2130968731;
        public static final int circleCrop = 2130968792;
        public static final int colorScheme = 2130968866;
        public static final int coordinatorLayoutStyle = 2130968914;
        public static final int drawerLayoutStyle = 2130968990;
        public static final int elevation = 2130968999;
        public static final int font = 2130969096;
        public static final int fontProviderAuthority = 2130969098;
        public static final int fontProviderCerts = 2130969099;
        public static final int fontProviderFetchStrategy = 2130969100;
        public static final int fontProviderFetchTimeout = 2130969101;
        public static final int fontProviderPackage = 2130969102;
        public static final int fontProviderQuery = 2130969103;
        public static final int fontProviderSystemFontFamily = 2130969104;
        public static final int fontStyle = 2130969105;
        public static final int fontVariationSettings = 2130969106;
        public static final int fontWeight = 2130969107;
        public static final int imageAspectRatio = 2130969150;
        public static final int imageAspectRatioAdjust = 2130969151;
        public static final int keylines = 2130969203;
        public static final int lStar = 2130969204;
        public static final int layout_anchor = 2130969216;
        public static final int layout_anchorGravity = 2130969217;
        public static final int layout_behavior = 2130969218;
        public static final int layout_dodgeInsetEdges = 2130969267;
        public static final int layout_insetEdge = 2130969280;
        public static final int layout_keyline = 2130969281;
        public static final int nestedScrollViewStyle = 2130969452;
        public static final int queryPatterns = 2130969512;
        public static final int scopeUris = 2130969535;
        public static final int shortcutMatchRequired = 2130969560;
        public static final int statusBarBackground = 2130969617;
        public static final int ttcIndex = 2130969814;
        
        private attr() {
        }
    }
    
    public static final class color
    {
        public static final int androidx_core_ripple_material_light = 2131099675;
        public static final int androidx_core_secondary_text_default_material_light = 2131099676;
        public static final int common_google_signin_btn_text_dark = 2131099741;
        public static final int common_google_signin_btn_text_dark_default = 2131099742;
        public static final int common_google_signin_btn_text_dark_disabled = 2131099743;
        public static final int common_google_signin_btn_text_dark_focused = 2131099744;
        public static final int common_google_signin_btn_text_dark_pressed = 2131099745;
        public static final int common_google_signin_btn_text_light = 2131099746;
        public static final int common_google_signin_btn_text_light_default = 2131099747;
        public static final int common_google_signin_btn_text_light_disabled = 2131099748;
        public static final int common_google_signin_btn_text_light_focused = 2131099749;
        public static final int common_google_signin_btn_text_light_pressed = 2131099750;
        public static final int common_google_signin_btn_tint = 2131099751;
        public static final int notification_action_color_filter = 2131100452;
        public static final int notification_icon_bg_color = 2131100453;
        public static final int notification_material_background_media_default_color = 2131100454;
        public static final int primary_text_default_material_dark = 2131100459;
        public static final int secondary_text_default_material_dark = 2131100465;
        
        private color() {
        }
    }
    
    public static final class dimen
    {
        public static final int compat_button_inset_horizontal_material = 2131165272;
        public static final int compat_button_inset_vertical_material = 2131165273;
        public static final int compat_button_padding_horizontal_material = 2131165274;
        public static final int compat_button_padding_vertical_material = 2131165275;
        public static final int compat_control_corner_material = 2131165276;
        public static final int compat_notification_large_icon_max_height = 2131165277;
        public static final int compat_notification_large_icon_max_width = 2131165278;
        public static final int def_drawer_elevation = 2131165279;
        public static final int notification_action_icon_size = 2131165959;
        public static final int notification_action_text_size = 2131165960;
        public static final int notification_big_circle_margin = 2131165961;
        public static final int notification_content_margin_start = 2131165962;
        public static final int notification_large_icon_height = 2131165963;
        public static final int notification_large_icon_width = 2131165964;
        public static final int notification_main_column_padding_top = 2131165965;
        public static final int notification_media_narrow_margin = 2131165966;
        public static final int notification_right_icon_size = 2131165967;
        public static final int notification_right_side_padding_top = 2131165968;
        public static final int notification_small_icon_background_padding = 2131165969;
        public static final int notification_small_icon_size_as_large = 2131165970;
        public static final int notification_subtext_size = 2131165971;
        public static final int notification_top_pad = 2131165972;
        public static final int notification_top_pad_large_text = 2131165973;
        
        private dimen() {
        }
    }
    
    public static final class drawable
    {
        public static final int common_full_open_on_phone = 2131230875;
        public static final int common_google_signin_btn_icon_dark = 2131230876;
        public static final int common_google_signin_btn_icon_dark_focused = 2131230877;
        public static final int common_google_signin_btn_icon_dark_normal = 2131230878;
        public static final int common_google_signin_btn_icon_dark_normal_background = 2131230879;
        public static final int common_google_signin_btn_icon_disabled = 2131230880;
        public static final int common_google_signin_btn_icon_light = 2131230881;
        public static final int common_google_signin_btn_icon_light_focused = 2131230882;
        public static final int common_google_signin_btn_icon_light_normal = 2131230883;
        public static final int common_google_signin_btn_icon_light_normal_background = 2131230884;
        public static final int common_google_signin_btn_text_dark = 2131230885;
        public static final int common_google_signin_btn_text_dark_focused = 2131230886;
        public static final int common_google_signin_btn_text_dark_normal = 2131230887;
        public static final int common_google_signin_btn_text_dark_normal_background = 2131230888;
        public static final int common_google_signin_btn_text_disabled = 2131230889;
        public static final int common_google_signin_btn_text_light = 2131230890;
        public static final int common_google_signin_btn_text_light_focused = 2131230891;
        public static final int common_google_signin_btn_text_light_normal = 2131230892;
        public static final int common_google_signin_btn_text_light_normal_background = 2131230893;
        public static final int googleg_disabled_color_18 = 2131230901;
        public static final int googleg_standard_color_18 = 2131230902;
        public static final int notification_action_background = 2131231070;
        public static final int notification_bg = 2131231071;
        public static final int notification_bg_low = 2131231072;
        public static final int notification_bg_low_normal = 2131231073;
        public static final int notification_bg_low_pressed = 2131231074;
        public static final int notification_bg_normal = 2131231075;
        public static final int notification_bg_normal_pressed = 2131231076;
        public static final int notification_icon_background = 2131231077;
        public static final int notification_template_icon_bg = 2131231078;
        public static final int notification_template_icon_low_bg = 2131231079;
        public static final int notification_tile_bg = 2131231080;
        public static final int notify_panel_notification_icon_bg = 2131231081;
        
        private drawable() {
        }
    }
    
    public static final class id
    {
        public static final int accessibility_action_clickable_span = 2131296271;
        public static final int accessibility_custom_action_0 = 2131296272;
        public static final int accessibility_custom_action_1 = 2131296273;
        public static final int accessibility_custom_action_10 = 2131296274;
        public static final int accessibility_custom_action_11 = 2131296275;
        public static final int accessibility_custom_action_12 = 2131296276;
        public static final int accessibility_custom_action_13 = 2131296277;
        public static final int accessibility_custom_action_14 = 2131296278;
        public static final int accessibility_custom_action_15 = 2131296279;
        public static final int accessibility_custom_action_16 = 2131296280;
        public static final int accessibility_custom_action_17 = 2131296281;
        public static final int accessibility_custom_action_18 = 2131296282;
        public static final int accessibility_custom_action_19 = 2131296283;
        public static final int accessibility_custom_action_2 = 2131296284;
        public static final int accessibility_custom_action_20 = 2131296285;
        public static final int accessibility_custom_action_21 = 2131296286;
        public static final int accessibility_custom_action_22 = 2131296287;
        public static final int accessibility_custom_action_23 = 2131296288;
        public static final int accessibility_custom_action_24 = 2131296289;
        public static final int accessibility_custom_action_25 = 2131296290;
        public static final int accessibility_custom_action_26 = 2131296291;
        public static final int accessibility_custom_action_27 = 2131296292;
        public static final int accessibility_custom_action_28 = 2131296293;
        public static final int accessibility_custom_action_29 = 2131296294;
        public static final int accessibility_custom_action_3 = 2131296295;
        public static final int accessibility_custom_action_30 = 2131296296;
        public static final int accessibility_custom_action_31 = 2131296297;
        public static final int accessibility_custom_action_4 = 2131296298;
        public static final int accessibility_custom_action_5 = 2131296299;
        public static final int accessibility_custom_action_6 = 2131296300;
        public static final int accessibility_custom_action_7 = 2131296301;
        public static final int accessibility_custom_action_8 = 2131296302;
        public static final int accessibility_custom_action_9 = 2131296303;
        public static final int action0 = 2131296304;
        public static final int action_container = 2131296315;
        public static final int action_divider = 2131296320;
        public static final int action_image = 2131296327;
        public static final int action_text = 2131296341;
        public static final int actions = 2131296346;
        public static final int adjust_height = 2131296349;
        public static final int adjust_width = 2131296350;
        public static final int all = 2131296353;
        public static final int async = 2131296366;
        public static final int auto = 2131296367;
        public static final int blocking = 2131296376;
        public static final int bottom = 2131296377;
        public static final int cancel_action = 2131296488;
        public static final int center = 2131296491;
        public static final int center_horizontal = 2131296494;
        public static final int center_vertical = 2131296495;
        public static final int chronometer = 2131296516;
        public static final int clip_horizontal = 2131296519;
        public static final int clip_vertical = 2131296520;
        public static final int dark = 2131296542;
        public static final int dialog_button = 2131296555;
        public static final int end = 2131296598;
        public static final int end_padder = 2131296600;
        public static final int fill = 2131296611;
        public static final int fill_horizontal = 2131296612;
        public static final int fill_vertical = 2131296613;
        public static final int forever = 2131296625;
        public static final int fragment_container_view_tag = 2131296626;
        public static final int icon = 2131296643;
        public static final int icon_group = 2131296644;
        public static final int icon_only = 2131296645;
        public static final int info = 2131296702;
        public static final int italic = 2131296705;
        public static final int left = 2131296820;
        public static final int light = 2131296823;
        public static final int line1 = 2131296824;
        public static final int line3 = 2131296825;
        public static final int media_actions = 2131296878;
        public static final int none = 2131296928;
        public static final int normal = 2131296929;
        public static final int notification_background = 2131296931;
        public static final int notification_main_column = 2131296932;
        public static final int notification_main_column_container = 2131296933;
        public static final int report_drawn = 2131297015;
        public static final int right = 2131297017;
        public static final int right_icon = 2131297019;
        public static final int right_side = 2131297020;
        public static final int special_effects_controller_view_tag = 2131297072;
        public static final int standard = 2131297120;
        public static final int start = 2131297121;
        public static final int status_bar_latest_event_content = 2131297127;
        public static final int tag_accessibility_actions = 2131297150;
        public static final int tag_accessibility_clickable_spans = 2131297151;
        public static final int tag_accessibility_heading = 2131297152;
        public static final int tag_accessibility_pane_title = 2131297153;
        public static final int tag_on_apply_window_listener = 2131297154;
        public static final int tag_on_receive_content_listener = 2131297155;
        public static final int tag_on_receive_content_mime_types = 2131297156;
        public static final int tag_screen_reader_focusable = 2131297157;
        public static final int tag_state_description = 2131297158;
        public static final int tag_transition_group = 2131297159;
        public static final int tag_unhandled_key_event_manager = 2131297160;
        public static final int tag_unhandled_key_listeners = 2131297161;
        public static final int tag_window_insets_animation_callback = 2131297162;
        public static final int text = 2131297169;
        public static final int text2 = 2131297170;
        public static final int time = 2131297329;
        public static final int title = 2131297330;
        public static final int top = 2131297362;
        public static final int view_tree_lifecycle_owner = 2131297400;
        public static final int view_tree_on_back_pressed_dispatcher_owner = 2131297401;
        public static final int view_tree_saved_state_registry_owner = 2131297402;
        public static final int view_tree_view_model_store_owner = 2131297403;
        public static final int visible_removing_fragment_view_tag = 2131297405;
        public static final int wide = 2131297411;
        
        private id() {
        }
    }
    
    public static final class integer
    {
        public static final int cancel_button_image_alpha = 2131361796;
        public static final int google_play_services_version = 2131361800;
        public static final int status_bar_notification_info_maxnum = 2131361860;
        
        private integer() {
        }
    }
    
    public static final class layout
    {
        public static final int custom_dialog = 2131492940;
        public static final int notification_action = 2131493084;
        public static final int notification_action_tombstone = 2131493085;
        public static final int notification_media_action = 2131493086;
        public static final int notification_media_cancel_action = 2131493087;
        public static final int notification_template_big_media = 2131493088;
        public static final int notification_template_big_media_custom = 2131493089;
        public static final int notification_template_big_media_narrow = 2131493090;
        public static final int notification_template_big_media_narrow_custom = 2131493091;
        public static final int notification_template_custom_big = 2131493092;
        public static final int notification_template_icon_group = 2131493093;
        public static final int notification_template_lines_media = 2131493094;
        public static final int notification_template_media = 2131493095;
        public static final int notification_template_media_custom = 2131493096;
        public static final int notification_template_part_chronometer = 2131493097;
        public static final int notification_template_part_time = 2131493098;
        
        private layout() {
        }
    }
    
    public static final class string
    {
        public static final int common_google_play_services_enable_button = 2131886181;
        public static final int common_google_play_services_enable_text = 2131886182;
        public static final int common_google_play_services_enable_title = 2131886183;
        public static final int common_google_play_services_install_button = 2131886184;
        public static final int common_google_play_services_install_text = 2131886185;
        public static final int common_google_play_services_install_title = 2131886186;
        public static final int common_google_play_services_notification_channel_name = 2131886187;
        public static final int common_google_play_services_notification_ticker = 2131886188;
        public static final int common_google_play_services_unknown_issue = 2131886189;
        public static final int common_google_play_services_unsupported_text = 2131886190;
        public static final int common_google_play_services_update_button = 2131886191;
        public static final int common_google_play_services_update_text = 2131886192;
        public static final int common_google_play_services_update_title = 2131886193;
        public static final int common_google_play_services_updating_text = 2131886194;
        public static final int common_google_play_services_wear_update_text = 2131886195;
        public static final int common_open_on_phone = 2131886196;
        public static final int common_signin_button_text = 2131886197;
        public static final int common_signin_button_text_long = 2131886198;
        public static final int status_bar_notification_info_overflow = 2131886849;
        
        private string() {
        }
    }
    
    public static final class style
    {
        public static final int TextAppearance_Compat_Notification = 2131952102;
        public static final int TextAppearance_Compat_Notification_Info = 2131952103;
        public static final int TextAppearance_Compat_Notification_Info_Media = 2131952104;
        public static final int TextAppearance_Compat_Notification_Line2 = 2131952105;
        public static final int TextAppearance_Compat_Notification_Line2_Media = 2131952106;
        public static final int TextAppearance_Compat_Notification_Media = 2131952107;
        public static final int TextAppearance_Compat_Notification_Time = 2131952108;
        public static final int TextAppearance_Compat_Notification_Time_Media = 2131952109;
        public static final int TextAppearance_Compat_Notification_Title = 2131952110;
        public static final int TextAppearance_Compat_Notification_Title_Media = 2131952111;
        public static final int Widget_Compat_NotificationActionContainer = 2131952479;
        public static final int Widget_Compat_NotificationActionText = 2131952480;
        public static final int Widget_Support_CoordinatorLayout = 2131952783;
        
        private style() {
        }
    }
    
    public static final class styleable
    {
        public static final int[] Capability;
        public static final int Capability_queryPatterns = 0;
        public static final int Capability_shortcutMatchRequired = 1;
        public static final int[] ColorStateListItem;
        public static final int ColorStateListItem_alpha = 3;
        public static final int ColorStateListItem_android_alpha = 1;
        public static final int ColorStateListItem_android_color = 0;
        public static final int ColorStateListItem_android_lStar = 2;
        public static final int ColorStateListItem_lStar = 4;
        public static final int[] CoordinatorLayout;
        public static final int[] CoordinatorLayout_Layout;
        public static final int CoordinatorLayout_Layout_android_layout_gravity = 0;
        public static final int CoordinatorLayout_Layout_layout_anchor = 1;
        public static final int CoordinatorLayout_Layout_layout_anchorGravity = 2;
        public static final int CoordinatorLayout_Layout_layout_behavior = 3;
        public static final int CoordinatorLayout_Layout_layout_dodgeInsetEdges = 4;
        public static final int CoordinatorLayout_Layout_layout_insetEdge = 5;
        public static final int CoordinatorLayout_Layout_layout_keyline = 6;
        public static final int CoordinatorLayout_keylines = 0;
        public static final int CoordinatorLayout_statusBarBackground = 1;
        public static final int[] DrawerLayout;
        public static final int DrawerLayout_elevation = 0;
        public static final int[] FontFamily;
        public static final int[] FontFamilyFont;
        public static final int FontFamilyFont_android_font = 0;
        public static final int FontFamilyFont_android_fontStyle = 2;
        public static final int FontFamilyFont_android_fontVariationSettings = 4;
        public static final int FontFamilyFont_android_fontWeight = 1;
        public static final int FontFamilyFont_android_ttcIndex = 3;
        public static final int FontFamilyFont_font = 5;
        public static final int FontFamilyFont_fontStyle = 6;
        public static final int FontFamilyFont_fontVariationSettings = 7;
        public static final int FontFamilyFont_fontWeight = 8;
        public static final int FontFamilyFont_ttcIndex = 9;
        public static final int FontFamily_fontProviderAuthority = 0;
        public static final int FontFamily_fontProviderCerts = 1;
        public static final int FontFamily_fontProviderFetchStrategy = 2;
        public static final int FontFamily_fontProviderFetchTimeout = 3;
        public static final int FontFamily_fontProviderPackage = 4;
        public static final int FontFamily_fontProviderQuery = 5;
        public static final int FontFamily_fontProviderSystemFontFamily = 6;
        public static final int[] Fragment;
        public static final int[] FragmentContainerView;
        public static final int FragmentContainerView_android_name = 0;
        public static final int FragmentContainerView_android_tag = 1;
        public static final int Fragment_android_id = 1;
        public static final int Fragment_android_name = 0;
        public static final int Fragment_android_tag = 2;
        public static final int[] GradientColor;
        public static final int[] GradientColorItem;
        public static final int GradientColorItem_android_color = 0;
        public static final int GradientColorItem_android_offset = 1;
        public static final int GradientColor_android_centerColor = 7;
        public static final int GradientColor_android_centerX = 3;
        public static final int GradientColor_android_centerY = 4;
        public static final int GradientColor_android_endColor = 1;
        public static final int GradientColor_android_endX = 10;
        public static final int GradientColor_android_endY = 11;
        public static final int GradientColor_android_gradientRadius = 5;
        public static final int GradientColor_android_startColor = 0;
        public static final int GradientColor_android_startX = 8;
        public static final int GradientColor_android_startY = 9;
        public static final int GradientColor_android_tileMode = 6;
        public static final int GradientColor_android_type = 2;
        public static final int[] LoadingImageView;
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
        public static final int[] SignInButton;
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;
        
        static {
            Capability = new int[] { 2130969512, 2130969560 };
            ColorStateListItem = new int[] { 16843173, 16843551, 16844359, 2130968627, 2130969204 };
            CoordinatorLayout = new int[] { 2130969203, 2130969617 };
            CoordinatorLayout_Layout = new int[] { 16842931, 2130969216, 2130969217, 2130969218, 2130969267, 2130969280, 2130969281 };
            DrawerLayout = new int[] { 2130968999 };
            FontFamily = new int[] { 2130969098, 2130969099, 2130969100, 2130969101, 2130969102, 2130969103, 2130969104 };
            FontFamilyFont = new int[] { 16844082, 16844083, 16844095, 16844143, 16844144, 2130969096, 2130969105, 2130969106, 2130969107, 2130969814 };
            Fragment = new int[] { 16842755, 16842960, 16842961 };
            FragmentContainerView = new int[] { 16842755, 16842961 };
            GradientColor = new int[] { 16843165, 16843166, 16843169, 16843170, 16843171, 16843172, 16843265, 16843275, 16844048, 16844049, 16844050, 16844051 };
            GradientColorItem = new int[] { 16843173, 16844052 };
            LoadingImageView = new int[] { 2130968792, 2130969150, 2130969151 };
            SignInButton = new int[] { 2130968731, 2130968866, 2130969535 };
        }
        
        private styleable() {
        }
    }
}

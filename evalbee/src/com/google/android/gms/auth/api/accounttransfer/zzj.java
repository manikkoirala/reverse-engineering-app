// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.accounttransfer;

import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.auth.zzan;

class zzj extends zzan
{
    final zzl zzb;
    
    public zzj(final zzl zzb) {
        this.zzb = zzb;
    }
    
    public final void zzd(final Status status) {
        final TaskCompletionSource zzb = this.zzb.zzb;
        final int zza = AccountTransferClient.zza;
        zzb.setException((Exception)new AccountTransferException(status));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.proxy;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.ShowFirstParty;
import com.google.android.gms.common.annotation.KeepForSdk;

@KeepForSdk
@ShowFirstParty
public interface ProxyApi
{
    @Deprecated
    @KeepForSdk
    @ShowFirstParty
    PendingResult<SpatulaHeaderResult> getSpatulaHeader(final GoogleApiClient p0);
    
    @Deprecated
    @KeepForSdk
    PendingResult<ProxyResult> performProxyRequest(final GoogleApiClient p0, final ProxyRequest p1);
    
    @KeepForSdk
    @ShowFirstParty
    public interface ProxyResult extends Result
    {
        @KeepForSdk
        ProxyResponse getResponse();
    }
    
    @KeepForSdk
    @ShowFirstParty
    public interface SpatulaHeaderResult extends Result
    {
        @KeepForSdk
        @ShowFirstParty
        String getSpatulaHeader();
    }
}

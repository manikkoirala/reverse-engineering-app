// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.signin;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "SignInAccountCreator")
@Reserved({ 1 })
public class SignInAccount extends AbstractSafeParcelable implements ReflectedParcelable
{
    public static final Parcelable$Creator<SignInAccount> CREATOR;
    @Deprecated
    @Field(defaultValue = "", id = 4)
    final String zba;
    @Deprecated
    @Field(defaultValue = "", id = 8)
    final String zbb;
    @Field(getter = "getGoogleSignInAccount", id = 7)
    private final GoogleSignInAccount zbc;
    
    static {
        CREATOR = (Parcelable$Creator)new zbc();
    }
    
    @Constructor
    public SignInAccount(@Param(id = 4) final String s, @Param(id = 7) final GoogleSignInAccount zbc, @Param(id = 8) final String s2) {
        this.zbc = zbc;
        this.zba = Preconditions.checkNotEmpty(s, "8.3 and 8.4 SDKs require non-null email");
        this.zbb = Preconditions.checkNotEmpty(s2, "8.3 and 8.4 SDKs require non-null userId");
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 4, this.zba, false);
        SafeParcelWriter.writeParcelable(parcel, 7, (Parcelable)this.zbc, n, false);
        SafeParcelWriter.writeString(parcel, 8, this.zbb, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public final GoogleSignInAccount zba() {
        return this.zbc;
    }
}

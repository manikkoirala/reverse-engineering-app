// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.accounttransfer;

final class zzd extends zzj
{
    final zze zza;
    
    public zzd(final zze zza, final zzl zzl) {
        this.zza = zza;
        super(zzl);
    }
    
    public final void zzb(final byte[] result) {
        this.zza.zzb.setResult((Object)result);
    }
}

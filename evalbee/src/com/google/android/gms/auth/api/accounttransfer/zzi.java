// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.accounttransfer;

import com.google.android.gms.internal.auth.zzat;
import com.google.android.gms.internal.auth.zzau;
import com.google.android.gms.internal.auth.zzav;

final class zzi extends zzn
{
    final zzav zza;
    
    public zzi(final AccountTransferClient accountTransferClient, final int n, final zzav zza) {
        this.zza = zza;
        super(1610);
    }
    
    @Override
    public final void zza(final zzau zzau) {
        zzau.zzf((zzat)super.zzc, this.zza);
    }
}

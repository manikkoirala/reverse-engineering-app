// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.credentials;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import java.util.Collections;
import android.text.TextUtils;
import com.google.android.gms.common.internal.Preconditions;
import java.util.List;
import android.net.Uri;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Deprecated
@Class(creator = "CredentialCreator")
@Reserved({ 1000, 1001, 1002 })
public class Credential extends AbstractSafeParcelable implements ReflectedParcelable
{
    public static final Parcelable$Creator<Credential> CREATOR;
    public static final String EXTRA_KEY = "com.google.android.gms.credentials.Credential";
    @Field(getter = "getId", id = 1)
    private final String zba;
    @Field(getter = "getName", id = 2)
    private final String zbb;
    @Field(getter = "getProfilePictureUri", id = 3)
    private final Uri zbc;
    @Field(getter = "getIdTokens", id = 4)
    private final List zbd;
    @Field(getter = "getPassword", id = 5)
    private final String zbe;
    @Field(getter = "getAccountType", id = 6)
    private final String zbf;
    @Field(getter = "getGivenName", id = 9)
    private final String zbg;
    @Field(getter = "getFamilyName", id = 10)
    private final String zbh;
    
    static {
        CREATOR = (Parcelable$Creator)new zba();
    }
    
    @Constructor
    public Credential(@Param(id = 1) String zbb, @Param(id = 2) final String s, @Param(id = 3) final Uri zbc, @Param(id = 4) final List list, @Param(id = 5) final String zbe, @Param(id = 6) final String zbf, @Param(id = 9) final String zbg, @Param(id = 10) final String zbh) {
        final String trim = Preconditions.checkNotNull(zbb, "credential identifier cannot be null").trim();
        Preconditions.checkNotEmpty(trim, "credential identifier cannot be empty");
        if (zbe != null && TextUtils.isEmpty((CharSequence)zbe)) {
            throw new IllegalArgumentException("Password must not be empty if set");
        }
        if (zbf != null) {
            Boolean b2 = null;
            Label_0166: {
                if (!TextUtils.isEmpty((CharSequence)zbf)) {
                    final Uri parse = Uri.parse(zbf);
                    if (parse.isAbsolute() && parse.isHierarchical() && !TextUtils.isEmpty((CharSequence)parse.getScheme())) {
                        if (!TextUtils.isEmpty((CharSequence)parse.getAuthority())) {
                            final boolean equalsIgnoreCase = "http".equalsIgnoreCase(parse.getScheme());
                            boolean b = true;
                            if (!equalsIgnoreCase) {
                                b = ("https".equalsIgnoreCase(parse.getScheme()) && b);
                            }
                            b2 = b;
                            break Label_0166;
                        }
                    }
                }
                b2 = Boolean.FALSE;
            }
            if (!b2) {
                throw new IllegalArgumentException("Account type must be a valid Http/Https URI");
            }
        }
        if (!TextUtils.isEmpty((CharSequence)zbf) && !TextUtils.isEmpty((CharSequence)zbe)) {
            throw new IllegalArgumentException("Password and AccountType are mutually exclusive");
        }
        if ((zbb = s) != null) {
            zbb = s;
            if (TextUtils.isEmpty((CharSequence)s.trim())) {
                zbb = null;
            }
        }
        this.zbb = zbb;
        this.zbc = zbc;
        List<Object> zbd;
        if (list == null) {
            zbd = Collections.emptyList();
        }
        else {
            zbd = Collections.unmodifiableList((List<?>)list);
        }
        this.zbd = zbd;
        this.zba = trim;
        this.zbe = zbe;
        this.zbf = zbf;
        this.zbg = zbg;
        this.zbh = zbh;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Credential)) {
            return false;
        }
        final Credential credential = (Credential)o;
        return TextUtils.equals((CharSequence)this.zba, (CharSequence)credential.zba) && TextUtils.equals((CharSequence)this.zbb, (CharSequence)credential.zbb) && Objects.equal(this.zbc, credential.zbc) && TextUtils.equals((CharSequence)this.zbe, (CharSequence)credential.zbe) && TextUtils.equals((CharSequence)this.zbf, (CharSequence)credential.zbf);
    }
    
    public String getAccountType() {
        return this.zbf;
    }
    
    public String getFamilyName() {
        return this.zbh;
    }
    
    public String getGivenName() {
        return this.zbg;
    }
    
    public String getId() {
        return this.zba;
    }
    
    public List<IdToken> getIdTokens() {
        return this.zbd;
    }
    
    public String getName() {
        return this.zbb;
    }
    
    public String getPassword() {
        return this.zbe;
    }
    
    public Uri getProfilePictureUri() {
        return this.zbc;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zba, this.zbb, this.zbc, this.zbe, this.zbf);
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.getId(), false);
        SafeParcelWriter.writeString(parcel, 2, this.getName(), false);
        SafeParcelWriter.writeParcelable(parcel, 3, (Parcelable)this.getProfilePictureUri(), n, false);
        SafeParcelWriter.writeTypedList(parcel, 4, this.getIdTokens(), false);
        SafeParcelWriter.writeString(parcel, 5, this.getPassword(), false);
        SafeParcelWriter.writeString(parcel, 6, this.getAccountType(), false);
        SafeParcelWriter.writeString(parcel, 9, this.getGivenName(), false);
        SafeParcelWriter.writeString(parcel, 10, this.getFamilyName(), false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    @Deprecated
    public static class Builder
    {
        private final String zba;
        private String zbb;
        private Uri zbc;
        private List zbd;
        private String zbe;
        private String zbf;
        private String zbg;
        private String zbh;
        
        public Builder(final Credential credential) {
            this.zba = Credential.zbe(credential);
            this.zbb = Credential.zbf(credential);
            this.zbc = Credential.zba(credential);
            this.zbd = Credential.zbh(credential);
            this.zbe = Credential.zbg(credential);
            this.zbf = Credential.zbb(credential);
            this.zbg = Credential.zbd(credential);
            this.zbh = Credential.zbc(credential);
        }
        
        public Builder(final String zba) {
            this.zba = zba;
        }
        
        public Credential build() {
            return new Credential(this.zba, this.zbb, this.zbc, this.zbd, this.zbe, this.zbf, this.zbg, this.zbh);
        }
        
        public Builder setAccountType(final String zbf) {
            this.zbf = zbf;
            return this;
        }
        
        public Builder setName(final String zbb) {
            this.zbb = zbb;
            return this;
        }
        
        public Builder setPassword(final String zbe) {
            this.zbe = zbe;
            return this;
        }
        
        public Builder setProfilePictureUri(final Uri zbc) {
            this.zbc = zbc;
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.signin.internal;

import java.util.Iterator;
import android.util.Log;
import java.util.concurrent.TimeUnit;
import com.google.android.gms.common.api.GoogleApiClient;
import android.content.Context;
import java.util.Set;
import java.util.concurrent.Semaphore;
import com.google.android.gms.common.api.internal.SignInConnectionListener;

public final class zbc extends p9 implements SignInConnectionListener
{
    private final Semaphore zba;
    private final Set zbb;
    
    public zbc(final Context context, final Set zbb) {
        super(context);
        this.zba = new Semaphore(0);
        this.zbb = zbb;
    }
    
    @Override
    public final void onComplete() {
        this.zba.release();
    }
    
    @Override
    public final void onStartLoading() {
        this.zba.drainPermits();
        this.forceLoad();
    }
}

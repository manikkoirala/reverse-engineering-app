// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.phone;

import android.content.Context;
import com.google.android.gms.internal.auth_api_phone.zzab;
import android.app.Activity;

public final class SmsRetriever
{
    public static final String EXTRA_CONSENT_INTENT = "com.google.android.gms.auth.api.phone.EXTRA_CONSENT_INTENT";
    public static final String EXTRA_SIM_SUBSCRIPTION_ID = "com.google.android.gms.auth.api.phone.EXTRA_SIM_SUBSCRIPTION_ID";
    public static final String EXTRA_SMS_MESSAGE = "com.google.android.gms.auth.api.phone.EXTRA_SMS_MESSAGE";
    public static final String EXTRA_STATUS = "com.google.android.gms.auth.api.phone.EXTRA_STATUS";
    public static final String SEND_PERMISSION = "com.google.android.gms.auth.api.phone.permission.SEND";
    public static final String SMS_RETRIEVED_ACTION = "com.google.android.gms.auth.api.phone.SMS_RETRIEVED";
    
    private SmsRetriever() {
    }
    
    public static SmsRetrieverClient getClient(final Activity activity) {
        return (SmsRetrieverClient)new zzab(activity);
    }
    
    public static SmsRetrieverClient getClient(final Context context) {
        return (SmsRetrieverClient)new zzab(context);
    }
}

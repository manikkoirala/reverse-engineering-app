// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.api.identity;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "GetSignInIntentRequestCreator")
public class GetSignInIntentRequest extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<GetSignInIntentRequest> CREATOR;
    @Field(getter = "getServerClientId", id = 1)
    private final String zba;
    @Field(getter = "getHostedDomainFilter", id = 2)
    private final String zbb;
    @Field(getter = "getSessionId", id = 3)
    private final String zbc;
    @Field(getter = "getNonce", id = 4)
    private final String zbd;
    @Field(getter = "requestVerifiedPhoneNumber", id = 5)
    private final boolean zbe;
    @Field(getter = "getTheme", id = 6)
    private final int zbf;
    
    static {
        CREATOR = (Parcelable$Creator)new zbk();
    }
    
    @Constructor
    public GetSignInIntentRequest(@Param(id = 1) final String zba, @Param(id = 2) final String zbb, @Param(id = 3) final String zbc, @Param(id = 4) final String zbd, @Param(id = 5) final boolean zbe, @Param(id = 6) final int zbf) {
        Preconditions.checkNotNull(zba);
        this.zba = zba;
        this.zbb = zbb;
        this.zbc = zbc;
        this.zbd = zbd;
        this.zbe = zbe;
        this.zbf = zbf;
    }
    
    public static Builder builder() {
        return new Builder();
    }
    
    public static Builder zba(final GetSignInIntentRequest getSignInIntentRequest) {
        Preconditions.checkNotNull(getSignInIntentRequest);
        final Builder builder = builder();
        builder.setServerClientId(getSignInIntentRequest.getServerClientId());
        builder.setNonce(getSignInIntentRequest.getNonce());
        builder.filterByHostedDomain(getSignInIntentRequest.getHostedDomainFilter());
        builder.setRequestVerifiedPhoneNumber(getSignInIntentRequest.zbe);
        builder.zbb(getSignInIntentRequest.zbf);
        final String zbc = getSignInIntentRequest.zbc;
        if (zbc != null) {
            builder.zba(zbc);
        }
        return builder;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof GetSignInIntentRequest)) {
            return false;
        }
        final GetSignInIntentRequest getSignInIntentRequest = (GetSignInIntentRequest)o;
        return Objects.equal(this.zba, getSignInIntentRequest.zba) && Objects.equal(this.zbd, getSignInIntentRequest.zbd) && Objects.equal(this.zbb, getSignInIntentRequest.zbb) && Objects.equal(this.zbe, getSignInIntentRequest.zbe) && this.zbf == getSignInIntentRequest.zbf;
    }
    
    public String getHostedDomainFilter() {
        return this.zbb;
    }
    
    public String getNonce() {
        return this.zbd;
    }
    
    public String getServerClientId() {
        return this.zba;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zba, this.zbb, this.zbd, this.zbe, this.zbf);
    }
    
    @Deprecated
    public boolean requestVerifiedPhoneNumber() {
        return this.zbe;
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.getServerClientId(), false);
        SafeParcelWriter.writeString(parcel, 2, this.getHostedDomainFilter(), false);
        SafeParcelWriter.writeString(parcel, 3, this.zbc, false);
        SafeParcelWriter.writeString(parcel, 4, this.getNonce(), false);
        SafeParcelWriter.writeBoolean(parcel, 5, this.requestVerifiedPhoneNumber());
        SafeParcelWriter.writeInt(parcel, 6, this.zbf);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public static final class Builder
    {
        private String zba;
        private String zbb;
        private String zbc;
        private String zbd;
        private boolean zbe;
        private int zbf;
        
        public GetSignInIntentRequest build() {
            return new GetSignInIntentRequest(this.zba, this.zbb, this.zbc, this.zbd, this.zbe, this.zbf);
        }
        
        public Builder filterByHostedDomain(final String zbb) {
            this.zbb = zbb;
            return this;
        }
        
        public Builder setNonce(final String zbd) {
            this.zbd = zbd;
            return this;
        }
        
        @Deprecated
        public Builder setRequestVerifiedPhoneNumber(final boolean zbe) {
            this.zbe = zbe;
            return this;
        }
        
        public Builder setServerClientId(final String zba) {
            Preconditions.checkNotNull(zba);
            this.zba = zba;
            return this;
        }
        
        public final Builder zba(final String zbc) {
            this.zbc = zbc;
            return this;
        }
        
        public final Builder zbb(final int zbf) {
            this.zbf = zbf;
            return this;
        }
    }
}

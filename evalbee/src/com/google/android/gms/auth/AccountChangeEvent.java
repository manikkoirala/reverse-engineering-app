// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth;

import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Objects;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "AccountChangeEventCreator")
public class AccountChangeEvent extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<AccountChangeEvent> CREATOR;
    @VersionField(id = 1)
    final int zza;
    @Field(id = 2)
    final long zzb;
    @Field(id = 3)
    final String zzc;
    @Field(id = 4)
    final int zzd;
    @Field(id = 5)
    final int zze;
    @Field(id = 6)
    final String zzf;
    
    static {
        CREATOR = (Parcelable$Creator)new zza();
    }
    
    @Constructor
    public AccountChangeEvent(@Param(id = 1) final int zza, @Param(id = 2) final long zzb, @Param(id = 3) final String s, @Param(id = 4) final int zzd, @Param(id = 5) final int zze, @Param(id = 6) final String zzf) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = Preconditions.checkNotNull(s);
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
    }
    
    public AccountChangeEvent(final long zzb, final String s, final int zzd, final int zze, final String zzf) {
        this.zza = 1;
        this.zzb = zzb;
        this.zzc = Preconditions.checkNotNull(s);
        this.zzd = zzd;
        this.zze = zze;
        this.zzf = zzf;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof AccountChangeEvent)) {
            return false;
        }
        if (o == this) {
            return true;
        }
        final AccountChangeEvent accountChangeEvent = (AccountChangeEvent)o;
        return this.zza == accountChangeEvent.zza && this.zzb == accountChangeEvent.zzb && Objects.equal(this.zzc, accountChangeEvent.zzc) && this.zzd == accountChangeEvent.zzd && this.zze == accountChangeEvent.zze && Objects.equal(this.zzf, accountChangeEvent.zzf);
    }
    
    public String getAccountName() {
        return this.zzc;
    }
    
    public String getChangeData() {
        return this.zzf;
    }
    
    public int getChangeType() {
        return this.zzd;
    }
    
    public int getEventIndex() {
        return this.zze;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.zza, this.zzb, this.zzc, this.zzd, this.zze, this.zzf);
    }
    
    @Override
    public String toString() {
        final int zzd = this.zzd;
        String str;
        if (zzd != 1) {
            if (zzd != 2) {
                if (zzd != 3) {
                    if (zzd != 4) {
                        str = "UNKNOWN";
                    }
                    else {
                        str = "RENAMED_TO";
                    }
                }
                else {
                    str = "RENAMED_FROM";
                }
            }
            else {
                str = "REMOVED";
            }
        }
        else {
            str = "ADDED";
        }
        final String zzc = this.zzc;
        final String zzf = this.zzf;
        final int zze = this.zze;
        final StringBuilder sb = new StringBuilder();
        sb.append("AccountChangeEvent {accountName = ");
        sb.append(zzc);
        sb.append(", changeType = ");
        sb.append(str);
        sb.append(", changeData = ");
        sb.append(zzf);
        sb.append(", eventIndex = ");
        sb.append(zze);
        sb.append("}");
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.zza);
        SafeParcelWriter.writeLong(parcel, 2, this.zzb);
        SafeParcelWriter.writeString(parcel, 3, this.zzc, false);
        SafeParcelWriter.writeInt(parcel, 4, this.zzd);
        SafeParcelWriter.writeInt(parcel, 5, this.zze);
        SafeParcelWriter.writeString(parcel, 6, this.zzf, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

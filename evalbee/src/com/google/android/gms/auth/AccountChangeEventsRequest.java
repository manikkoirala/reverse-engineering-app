// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth;

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import android.text.TextUtils;
import android.accounts.Account;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

@Class(creator = "AccountChangeEventsRequestCreator")
public class AccountChangeEventsRequest extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<AccountChangeEventsRequest> CREATOR;
    @VersionField(id = 1)
    final int zza;
    @Field(id = 2)
    int zzb;
    @Deprecated
    @Field(id = 3)
    String zzc;
    @Field(id = 4)
    Account zzd;
    
    static {
        CREATOR = (Parcelable$Creator)new zzb();
    }
    
    public AccountChangeEventsRequest() {
        this.zza = 1;
    }
    
    @Constructor
    public AccountChangeEventsRequest(@Param(id = 1) final int zza, @Param(id = 2) final int zzb, @Param(id = 3) final String zzc, @Param(id = 4) final Account zzd) {
        this.zza = zza;
        this.zzb = zzb;
        this.zzc = zzc;
        if (zzd == null && !TextUtils.isEmpty((CharSequence)zzc)) {
            this.zzd = new Account(zzc, "com.google");
            return;
        }
        this.zzd = zzd;
    }
    
    public Account getAccount() {
        return this.zzd;
    }
    
    @Deprecated
    public String getAccountName() {
        return this.zzc;
    }
    
    public int getEventIndex() {
        return this.zzb;
    }
    
    public AccountChangeEventsRequest setAccount(final Account zzd) {
        this.zzd = zzd;
        return this;
    }
    
    @Deprecated
    public AccountChangeEventsRequest setAccountName(final String zzc) {
        this.zzc = zzc;
        return this;
    }
    
    public AccountChangeEventsRequest setEventIndex(final int zzb) {
        this.zzb = zzb;
        return this;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeInt(parcel, 1, this.zza);
        SafeParcelWriter.writeInt(parcel, 2, this.zzb);
        SafeParcelWriter.writeString(parcel, 3, this.zzc, false);
        SafeParcelWriter.writeParcelable(parcel, 4, (Parcelable)this.zzd, n, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

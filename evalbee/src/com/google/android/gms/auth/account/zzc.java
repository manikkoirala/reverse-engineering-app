// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.account;

import android.os.Parcelable;
import android.accounts.Account;
import android.os.Parcel;
import android.os.IInterface;
import android.os.IBinder;
import com.google.android.gms.internal.auth.zza;

public final class zzc extends zza implements zze
{
    public zzc(final IBinder binder) {
        super(binder, "com.google.android.gms.auth.account.IWorkAccountService");
    }
    
    public final void zzd(final zzb zzb, final String s) {
        final Parcel zza = this.zza();
        com.google.android.gms.internal.auth.zzc.zze(zza, (IInterface)zzb);
        zza.writeString(s);
        this.zzc(2, zza);
    }
    
    public final void zze(final zzb zzb, final Account account) {
        final Parcel zza = this.zza();
        com.google.android.gms.internal.auth.zzc.zze(zza, (IInterface)zzb);
        com.google.android.gms.internal.auth.zzc.zzd(zza, (Parcelable)account);
        this.zzc(3, zza);
    }
    
    public final void zzf(final boolean b) {
        final Parcel zza = this.zza();
        com.google.android.gms.internal.auth.zzc.zzc(zza, b);
        this.zzc(1, zza);
    }
}

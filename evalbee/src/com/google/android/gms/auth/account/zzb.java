// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.account;

import android.accounts.Account;
import android.os.IInterface;

public interface zzb extends IInterface
{
    void zzb(final Account p0);
    
    void zzc(final boolean p0);
}

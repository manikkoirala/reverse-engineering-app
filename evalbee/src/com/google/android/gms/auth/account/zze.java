// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.account;

import android.accounts.Account;
import android.os.IInterface;

public interface zze extends IInterface
{
    void zzd(final zzb p0, final String p1);
    
    void zze(final zzb p0, final Account p1);
    
    void zzf(final boolean p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.account;

import android.content.Context;
import android.app.Activity;
import com.google.android.gms.internal.auth.zzal;
import com.google.android.gms.common.api.Api;

public class WorkAccount
{
    public static final Api<Api.ApiOptions.NoOptions> API;
    @Deprecated
    public static final WorkAccountApi WorkAccountApi;
    private static final Api.ClientKey zza;
    private static final Api.AbstractClientBuilder zzb;
    
    static {
        API = new Api<Api.ApiOptions.NoOptions>("WorkAccount.API", zzb = new zzf(), zza = new Api.ClientKey());
        WorkAccountApi = (WorkAccountApi)new zzal();
    }
    
    private WorkAccount() {
    }
    
    public static WorkAccountClient getClient(final Activity activity) {
        return new WorkAccountClient(activity);
    }
    
    public static WorkAccountClient getClient(final Context context) {
        return new WorkAccountClient(context);
    }
}

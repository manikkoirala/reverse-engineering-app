// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.account;

import android.accounts.Account;
import com.google.android.gms.internal.auth.zzc;
import android.os.Parcel;
import com.google.android.gms.internal.auth.zzb;

public abstract class zza extends com.google.android.gms.internal.auth.zzb implements zzb
{
    public zza() {
        super("com.google.android.gms.auth.account.IWorkAccountCallback");
    }
    
    public final boolean zza(final int n, final Parcel parcel, final Parcel parcel2, final int n2) {
        if (n != 1) {
            if (n != 2) {
                return false;
            }
            final boolean zzf = zzc.zzf(parcel);
            zzc.zzb(parcel);
            this.zzc(zzf);
        }
        else {
            final Account account = (Account)zzc.zza(parcel, Account.CREATOR);
            zzc.zzb(parcel);
            this.zzb(account);
        }
        return true;
    }
}

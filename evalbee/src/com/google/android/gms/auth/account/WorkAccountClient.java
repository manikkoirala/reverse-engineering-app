// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth.account;

import com.google.android.gms.common.internal.PendingResultUtil;
import android.accounts.Account;
import com.google.android.gms.tasks.Task;
import android.content.Context;
import com.google.android.gms.internal.auth.zzal;
import android.app.Activity;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApi;

public class WorkAccountClient extends GoogleApi<Api.ApiOptions.NoOptions>
{
    private final WorkAccountApi zza;
    
    public WorkAccountClient(final Activity activity) {
        super(activity, (Api<Api.ApiOptions>)WorkAccount.API, (Api.ApiOptions)Api.ApiOptions.NO_OPTIONS, Settings.DEFAULT_SETTINGS);
        this.zza = (WorkAccountApi)new zzal();
    }
    
    public WorkAccountClient(final Context context) {
        super(context, (Api<Api.ApiOptions>)WorkAccount.API, (Api.ApiOptions)Api.ApiOptions.NO_OPTIONS, Settings.DEFAULT_SETTINGS);
        this.zza = (WorkAccountApi)new zzal();
    }
    
    public Task<Account> addWorkAccount(final String s) {
        return PendingResultUtil.toTask(this.zza.addWorkAccount(this.asGoogleApiClient(), s), (PendingResultUtil.ResultConverter<WorkAccountApi.AddAccountResult, Account>)new zzg(this));
    }
    
    public Task<Void> removeWorkAccount(final Account account) {
        return PendingResultUtil.toVoidTask(this.zza.removeWorkAccount(this.asGoogleApiClient(), account));
    }
    
    public Task<Void> setWorkAuthenticatorEnabled(final boolean b) {
        return PendingResultUtil.toVoidTask(this.zza.setWorkAuthenticatorEnabledWithResult(this.asGoogleApiClient(), b));
    }
}

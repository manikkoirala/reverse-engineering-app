// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth;

import android.os.BaseBundle;
import com.google.android.gms.internal.auth.zze;
import android.os.IBinder;
import android.os.Bundle;

final class zzh implements zzk
{
    final String zza;
    final Bundle zzb;
    
    public zzh(final String zza, final Bundle zzb) {
        this.zza = zza;
        this.zzb = zzb;
    }
}

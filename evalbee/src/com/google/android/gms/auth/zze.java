// 
// Decompiled by Procyon v0.6.0
// 

package com.google.android.gms.auth;

import com.google.android.gms.common.Feature;

public final class zze
{
    public static final Feature zza;
    public static final Feature zzb;
    public static final Feature zzc;
    public static final Feature zzd;
    public static final Feature zze;
    public static final Feature zzf;
    public static final Feature zzg;
    public static final Feature zzh;
    public static final Feature zzi;
    public static final Feature zzj;
    public static final Feature zzk;
    public static final Feature zzl;
    public static final Feature[] zzm;
    
    static {
        zzm = new Feature[] { zza = new Feature("account_capability_api", 1L), zzb = new Feature("account_data_service", 6L), zzc = new Feature("account_data_service_legacy", 1L), zzd = new Feature("account_data_service_token", 8L), zze = new Feature("account_data_service_visibility", 1L), zzf = new Feature("config_sync", 1L), zzg = new Feature("device_account_api", 1L), zzh = new Feature("gaiaid_primary_email_api", 1L), zzi = new Feature("google_auth_service_accounts", 2L), zzj = new Feature("google_auth_service_token", 3L), zzk = new Feature("hub_mode_api", 1L), zzl = new Feature("work_account_client_is_whitelisted", 1L) };
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.j2objc.annotations;

public enum ReflectionSupport$Level
{
    private static final ReflectionSupport$Level[] $VALUES;
    
    FULL, 
    NATIVE_ONLY;
}

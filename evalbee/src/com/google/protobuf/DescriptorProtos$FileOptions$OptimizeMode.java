// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

public enum DescriptorProtos$FileOptions$OptimizeMode implements a
{
    private static final DescriptorProtos$FileOptions$OptimizeMode[] $VALUES;
    
    CODE_SIZE(2);
    
    public static final int CODE_SIZE_VALUE = 2;
    
    LITE_RUNTIME(3);
    
    public static final int LITE_RUNTIME_VALUE = 3;
    
    SPEED(1);
    
    public static final int SPEED_VALUE = 1;
    private static final t.b internalValueMap;
    private final int value;
    
    static {
        internalValueMap = new t.b() {};
    }
    
    private DescriptorProtos$FileOptions$OptimizeMode(final int value) {
        this.value = value;
    }
    
    public static DescriptorProtos$FileOptions$OptimizeMode forNumber(final int n) {
        if (n == 1) {
            return DescriptorProtos$FileOptions$OptimizeMode.SPEED;
        }
        if (n == 2) {
            return DescriptorProtos$FileOptions$OptimizeMode.CODE_SIZE;
        }
        if (n != 3) {
            return null;
        }
        return DescriptorProtos$FileOptions$OptimizeMode.LITE_RUNTIME;
    }
    
    public static t.b internalGetValueMap() {
        return DescriptorProtos$FileOptions$OptimizeMode.internalValueMap;
    }
    
    public static c internalGetVerifier() {
        return b.a;
    }
    
    @Deprecated
    public static DescriptorProtos$FileOptions$OptimizeMode valueOf(final int n) {
        return forNumber(n);
    }
    
    @Override
    public final int getNumber() {
        return this.value;
    }
    
    public static final class b implements c
    {
        public static final c a;
        
        static {
            a = new b();
        }
        
        @Override
        public boolean a(final int n) {
            return DescriptorProtos$FileOptions$OptimizeMode.forNumber(n) != null;
        }
    }
}

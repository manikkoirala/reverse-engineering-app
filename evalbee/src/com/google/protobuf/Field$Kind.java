// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

public enum Field$Kind implements a
{
    private static final Field$Kind[] $VALUES;
    
    TYPE_BOOL(8);
    
    public static final int TYPE_BOOL_VALUE = 8;
    
    TYPE_BYTES(12);
    
    public static final int TYPE_BYTES_VALUE = 12;
    
    TYPE_DOUBLE(1);
    
    public static final int TYPE_DOUBLE_VALUE = 1;
    
    TYPE_ENUM(14);
    
    public static final int TYPE_ENUM_VALUE = 14;
    
    TYPE_FIXED32(7);
    
    public static final int TYPE_FIXED32_VALUE = 7;
    
    TYPE_FIXED64(6);
    
    public static final int TYPE_FIXED64_VALUE = 6;
    
    TYPE_FLOAT(2);
    
    public static final int TYPE_FLOAT_VALUE = 2;
    
    TYPE_GROUP(10);
    
    public static final int TYPE_GROUP_VALUE = 10;
    
    TYPE_INT32(5);
    
    public static final int TYPE_INT32_VALUE = 5;
    
    TYPE_INT64(3);
    
    public static final int TYPE_INT64_VALUE = 3;
    
    TYPE_MESSAGE(11);
    
    public static final int TYPE_MESSAGE_VALUE = 11;
    
    TYPE_SFIXED32(15);
    
    public static final int TYPE_SFIXED32_VALUE = 15;
    
    TYPE_SFIXED64(16);
    
    public static final int TYPE_SFIXED64_VALUE = 16;
    
    TYPE_SINT32(17);
    
    public static final int TYPE_SINT32_VALUE = 17;
    
    TYPE_SINT64(18);
    
    public static final int TYPE_SINT64_VALUE = 18;
    
    TYPE_STRING(9);
    
    public static final int TYPE_STRING_VALUE = 9;
    
    TYPE_UINT32(13);
    
    public static final int TYPE_UINT32_VALUE = 13;
    
    TYPE_UINT64(4);
    
    public static final int TYPE_UINT64_VALUE = 4;
    
    TYPE_UNKNOWN(0);
    
    public static final int TYPE_UNKNOWN_VALUE = 0;
    
    UNRECOGNIZED(-1);
    
    private static final t.b internalValueMap;
    private final int value;
    
    static {
        internalValueMap = new t.b() {};
    }
    
    private Field$Kind(final int value) {
        this.value = value;
    }
    
    public static Field$Kind forNumber(final int n) {
        switch (n) {
            default: {
                return null;
            }
            case 18: {
                return Field$Kind.TYPE_SINT64;
            }
            case 17: {
                return Field$Kind.TYPE_SINT32;
            }
            case 16: {
                return Field$Kind.TYPE_SFIXED64;
            }
            case 15: {
                return Field$Kind.TYPE_SFIXED32;
            }
            case 14: {
                return Field$Kind.TYPE_ENUM;
            }
            case 13: {
                return Field$Kind.TYPE_UINT32;
            }
            case 12: {
                return Field$Kind.TYPE_BYTES;
            }
            case 11: {
                return Field$Kind.TYPE_MESSAGE;
            }
            case 10: {
                return Field$Kind.TYPE_GROUP;
            }
            case 9: {
                return Field$Kind.TYPE_STRING;
            }
            case 8: {
                return Field$Kind.TYPE_BOOL;
            }
            case 7: {
                return Field$Kind.TYPE_FIXED32;
            }
            case 6: {
                return Field$Kind.TYPE_FIXED64;
            }
            case 5: {
                return Field$Kind.TYPE_INT32;
            }
            case 4: {
                return Field$Kind.TYPE_UINT64;
            }
            case 3: {
                return Field$Kind.TYPE_INT64;
            }
            case 2: {
                return Field$Kind.TYPE_FLOAT;
            }
            case 1: {
                return Field$Kind.TYPE_DOUBLE;
            }
            case 0: {
                return Field$Kind.TYPE_UNKNOWN;
            }
        }
    }
    
    public static t.b internalGetValueMap() {
        return Field$Kind.internalValueMap;
    }
    
    public static c internalGetVerifier() {
        return b.a;
    }
    
    @Deprecated
    public static Field$Kind valueOf(final int n) {
        return forNumber(n);
    }
    
    @Override
    public final int getNumber() {
        if (this != Field$Kind.UNRECOGNIZED) {
            return this.value;
        }
        throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
    }
    
    public static final class b implements c
    {
        public static final c a;
        
        static {
            a = new b();
        }
        
        @Override
        public boolean a(final int n) {
            return Field$Kind.forNumber(n) != null;
        }
    }
}

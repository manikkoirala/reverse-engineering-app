// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.TreeMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.List;
import java.io.Serializable;
import java.util.Arrays;

public abstract class b0
{
    public static final char[] a;
    
    static {
        Arrays.fill(a = new char[80], ' ');
    }
    
    public static void a(int i, final StringBuilder sb) {
        while (i > 0) {
            final char[] a = b0.a;
            int length;
            if (i > a.length) {
                length = a.length;
            }
            else {
                length = i;
            }
            sb.append(a, 0, length);
            i -= length;
        }
    }
    
    public static boolean b(final Object o) {
        final boolean b = o instanceof Boolean;
        final boolean b2 = true;
        final boolean b3 = true;
        final boolean b4 = true;
        boolean b5 = true;
        final boolean b6 = true;
        if (b) {
            return (boolean)o ^ true;
        }
        if (o instanceof Integer) {
            return (int)o == 0 && b6;
        }
        if (o instanceof Float) {
            return Float.floatToRawIntBits((float)o) == 0 && b2;
        }
        if (o instanceof Double) {
            return Double.doubleToRawLongBits((double)o) == 0L && b3;
        }
        Serializable empty;
        if (o instanceof String) {
            empty = "";
        }
        else if (o instanceof ByteString) {
            empty = ByteString.EMPTY;
        }
        else {
            if (o instanceof a0) {
                return o == ((a0)o).a() && b4;
            }
            if (o instanceof Enum) {
                if (((Enum)o).ordinal() != 0) {
                    b5 = false;
                }
                return b5;
            }
            return false;
        }
        return o.equals(empty);
    }
    
    public static String c(final String s) {
        if (s.isEmpty()) {
            return s;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(Character.toLowerCase(s.charAt(0)));
        for (int i = 1; i < s.length(); ++i) {
            final char char1 = s.charAt(i);
            if (Character.isUpperCase(char1)) {
                sb.append("_");
            }
            sb.append(Character.toLowerCase(char1));
        }
        return sb.toString();
    }
    
    public static void d(final StringBuilder sb, final int n, String str, final Object obj) {
        if (obj instanceof List) {
            final Iterator iterator = ((List)obj).iterator();
            while (iterator.hasNext()) {
                d(sb, n, str, iterator.next());
            }
            return;
        }
        if (obj instanceof Map) {
            final Iterator iterator2 = ((Map)obj).entrySet().iterator();
            while (iterator2.hasNext()) {
                d(sb, n, str, iterator2.next());
            }
            return;
        }
        sb.append('\n');
        a(n, sb);
        sb.append(c(str));
        if (obj instanceof String) {
            sb.append(": \"");
            str = bv1.c((String)obj);
        }
        else {
            if (!(obj instanceof ByteString)) {
                if (obj instanceof GeneratedMessageLite) {
                    sb.append(" {");
                    e((a0)obj, sb, n + 2);
                }
                else {
                    if (!(obj instanceof Map.Entry)) {
                        sb.append(": ");
                        sb.append(obj);
                        return;
                    }
                    sb.append(" {");
                    final Map.Entry entry = (Map.Entry)obj;
                    final int n2 = n + 2;
                    d(sb, n2, "key", entry.getKey());
                    d(sb, n2, "value", entry.getValue());
                }
                sb.append("\n");
                a(n, sb);
                sb.append("}");
                return;
            }
            sb.append(": \"");
            str = bv1.b((ByteString)obj);
        }
        sb.append(str);
        sb.append('\"');
    }
    
    public static void e(final a0 a0, final StringBuilder sb, final int n) {
        final HashSet set = new HashSet();
        final HashMap hashMap = new HashMap();
        final TreeMap treeMap = new TreeMap();
        for (final Method method : a0.getClass().getDeclaredMethods()) {
            if (!Modifier.isStatic(method.getModifiers())) {
                if (method.getName().length() >= 3) {
                    if (method.getName().startsWith("set")) {
                        set.add(method.getName());
                    }
                    else if (Modifier.isPublic(method.getModifiers())) {
                        if (method.getParameterTypes().length == 0) {
                            if (method.getName().startsWith("has")) {
                                hashMap.put(method.getName(), method);
                            }
                            else if (method.getName().startsWith("get")) {
                                treeMap.put(method.getName(), method);
                            }
                        }
                    }
                }
            }
        }
        for (final Map.Entry<String, V> entry : treeMap.entrySet()) {
            final String substring = entry.getKey().substring(3);
            if (substring.endsWith("List") && !substring.endsWith("OrBuilderList") && !substring.equals("List")) {
                final Method method2 = (Method)entry.getValue();
                if (method2 != null && method2.getReturnType().equals(List.class)) {
                    d(sb, n, substring.substring(0, substring.length() - 4), GeneratedMessageLite.G(method2, a0, new Object[0]));
                    continue;
                }
            }
            if (substring.endsWith("Map") && !substring.equals("Map")) {
                final Method method3 = (Method)entry.getValue();
                if (method3 != null && method3.getReturnType().equals(Map.class) && !method3.isAnnotationPresent(Deprecated.class) && Modifier.isPublic(method3.getModifiers())) {
                    d(sb, n, substring.substring(0, substring.length() - 3), GeneratedMessageLite.G(method3, a0, new Object[0]));
                    continue;
                }
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("set");
            sb2.append(substring);
            if (!set.contains(sb2.toString())) {
                continue;
            }
            if (substring.endsWith("Bytes")) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("get");
                sb3.append(substring.substring(0, substring.length() - 5));
                if (treeMap.containsKey(sb3.toString())) {
                    continue;
                }
            }
            final Method method4 = (Method)entry.getValue();
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("has");
            sb4.append(substring);
            final Method method5 = (Method)hashMap.get(sb4.toString());
            if (method4 == null) {
                continue;
            }
            final Object g = GeneratedMessageLite.G(method4, a0, new Object[0]);
            boolean booleanValue;
            if (method5 == null) {
                booleanValue = !b(g);
            }
            else {
                booleanValue = (boolean)GeneratedMessageLite.G(method5, a0, new Object[0]);
            }
            if (!booleanValue) {
                continue;
            }
            d(sb, n, substring, g);
        }
        final l0 unknownFields = ((GeneratedMessageLite)a0).unknownFields;
        if (unknownFields != null) {
            unknownFields.m(sb, n);
        }
    }
    
    public static String f(final a0 a0, final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append("# ");
        sb.append(str);
        e(a0, sb, 0);
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

public abstract class b implements b31
{
    public static final l a;
    
    static {
        a = l.b();
    }
    
    public final a0 d(final a0 unfinishedMessage) {
        if (unfinishedMessage != null && !unfinishedMessage.isInitialized()) {
            throw this.e(unfinishedMessage).asInvalidProtocolBufferException().setUnfinishedMessage(unfinishedMessage);
        }
        return unfinishedMessage;
    }
    
    public final UninitializedMessageException e(final a0 a0) {
        if (a0 instanceof a) {
            return ((a)a0).m();
        }
        return new UninitializedMessageException(a0);
    }
    
    public a0 f(final g g, final l l) {
        return this.d((a0)this.b(g, l));
    }
    
    public a0 g(final byte[] array, final int n, final int n2, final l l) {
        return this.d(this.i(array, n, n2, l));
    }
    
    public a0 h(final byte[] array, final l l) {
        return this.g(array, 0, array.length, l);
    }
    
    public abstract a0 i(final byte[] p0, final int p1, final int p2, final l p3);
}

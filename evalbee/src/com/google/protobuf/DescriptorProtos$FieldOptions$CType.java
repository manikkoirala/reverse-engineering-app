// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

public enum DescriptorProtos$FieldOptions$CType implements a
{
    private static final DescriptorProtos$FieldOptions$CType[] $VALUES;
    
    CORD(1);
    
    public static final int CORD_VALUE = 1;
    
    STRING(0), 
    STRING_PIECE(2);
    
    public static final int STRING_PIECE_VALUE = 2;
    public static final int STRING_VALUE = 0;
    private static final t.b internalValueMap;
    private final int value;
    
    static {
        internalValueMap = new t.b() {};
    }
    
    private DescriptorProtos$FieldOptions$CType(final int value) {
        this.value = value;
    }
    
    public static DescriptorProtos$FieldOptions$CType forNumber(final int n) {
        if (n == 0) {
            return DescriptorProtos$FieldOptions$CType.STRING;
        }
        if (n == 1) {
            return DescriptorProtos$FieldOptions$CType.CORD;
        }
        if (n != 2) {
            return null;
        }
        return DescriptorProtos$FieldOptions$CType.STRING_PIECE;
    }
    
    public static t.b internalGetValueMap() {
        return DescriptorProtos$FieldOptions$CType.internalValueMap;
    }
    
    public static c internalGetVerifier() {
        return b.a;
    }
    
    @Deprecated
    public static DescriptorProtos$FieldOptions$CType valueOf(final int n) {
        return forNumber(n);
    }
    
    @Override
    public final int getNumber() {
        return this.value;
    }
    
    public static final class b implements c
    {
        public static final c a;
        
        static {
            a = new b();
        }
        
        @Override
        public boolean a(final int n) {
            return DescriptorProtos$FieldOptions$CType.forNumber(n) != null;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

import java.nio.ByteBuffer;
import java.io.IOException;
import java.util.logging.Level;
import java.io.OutputStream;
import java.util.logging.Logger;

public abstract class CodedOutputStream extends pd
{
    public static final Logger c;
    public static final boolean d;
    public i a;
    public boolean b;
    
    static {
        c = Logger.getLogger(CodedOutputStream.class.getName());
        d = d12.I();
    }
    
    public static int A(final int n) {
        return S(n) + n;
    }
    
    public static int B(final int n, final a0 a0, final g0 g0) {
        return Q(n) + D(a0, g0);
    }
    
    public static int C(final a0 a0) {
        return A(a0.b());
    }
    
    public static int D(final a0 a0, final g0 g0) {
        return A(((a)a0).k(g0));
    }
    
    public static int E(final int n) {
        if (n > 4096) {
            return 4096;
        }
        return n;
    }
    
    public static int F(final int n, final ByteString byteString) {
        return Q(1) * 2 + R(2, n) + h(3, byteString);
    }
    
    public static int G(final int n, final int n2) {
        return Q(n) + H(n2);
    }
    
    public static int H(final int n) {
        return 4;
    }
    
    public static int I(final int n, final long n2) {
        return Q(n) + J(n2);
    }
    
    public static int J(final long n) {
        return 8;
    }
    
    public static int K(final int n, final int n2) {
        return Q(n) + L(n2);
    }
    
    public static int L(final int n) {
        return S(V(n));
    }
    
    public static int M(final int n, final long n2) {
        return Q(n) + N(n2);
    }
    
    public static int N(final long n) {
        return U(W(n));
    }
    
    public static int O(final int n, final String s) {
        return Q(n) + P(s);
    }
    
    public static int P(final String s) {
        int n;
        try {
            n = Utf8.j(s);
        }
        catch (final Utf8.UnpairedSurrogateException ex) {
            n = s.getBytes(t.b).length;
        }
        return A(n);
    }
    
    public static int Q(final int n) {
        return S(WireFormat.c(n, 0));
    }
    
    public static int R(final int n, final int n2) {
        return Q(n) + S(n2);
    }
    
    public static int S(final int n) {
        if ((n & 0xFFFFFF80) == 0x0) {
            return 1;
        }
        if ((n & 0xFFFFC000) == 0x0) {
            return 2;
        }
        if ((0xFFE00000 & n) == 0x0) {
            return 3;
        }
        if ((n & 0xF0000000) == 0x0) {
            return 4;
        }
        return 5;
    }
    
    public static int T(final int n, final long n2) {
        return Q(n) + U(n2);
    }
    
    public static int U(long n) {
        if ((0xFFFFFFFFFFFFFF80L & n) == 0x0L) {
            return 1;
        }
        if (n < 0L) {
            return 10;
        }
        int n2;
        if ((0xFFFFFFF800000000L & n) != 0x0L) {
            n >>>= 28;
            n2 = 6;
        }
        else {
            n2 = 2;
        }
        int n3 = n2;
        long n4 = n;
        if ((0xFFFFFFFFFFE00000L & n) != 0x0L) {
            n3 = n2 + 2;
            n4 = n >>> 14;
        }
        int n5 = n3;
        if ((n4 & 0xFFFFFFFFFFFFC000L) != 0x0L) {
            n5 = n3 + 1;
        }
        return n5;
    }
    
    public static int V(final int n) {
        return n >> 31 ^ n << 1;
    }
    
    public static long W(final long n) {
        return n >> 63 ^ n << 1;
    }
    
    public static CodedOutputStream a0(final OutputStream outputStream, final int n) {
        return new d(outputStream, n);
    }
    
    public static CodedOutputStream b0(final byte[] array) {
        return c0(array, 0, array.length);
    }
    
    public static /* synthetic */ boolean c() {
        return CodedOutputStream.d;
    }
    
    public static CodedOutputStream c0(final byte[] array, final int n, final int n2) {
        return new c(array, n, n2);
    }
    
    public static int e(final int n, final boolean b) {
        return Q(n) + f(b);
    }
    
    public static int f(final boolean b) {
        return 1;
    }
    
    public static int g(final byte[] array) {
        return A(array.length);
    }
    
    public static int h(final int n, final ByteString byteString) {
        return Q(n) + i(byteString);
    }
    
    public static int i(final ByteString byteString) {
        return A(byteString.size());
    }
    
    public static int j(final int n, final double n2) {
        return Q(n) + k(n2);
    }
    
    public static int k(final double n) {
        return 8;
    }
    
    public static int l(final int n, final int n2) {
        return Q(n) + m(n2);
    }
    
    public static int m(final int n) {
        return x(n);
    }
    
    public static int n(final int n, final int n2) {
        return Q(n) + o(n2);
    }
    
    public static int o(final int n) {
        return 4;
    }
    
    public static int p(final int n, final long n2) {
        return Q(n) + q(n2);
    }
    
    public static int q(final long n) {
        return 8;
    }
    
    public static int r(final int n, final float n2) {
        return Q(n) + s(n2);
    }
    
    public static int s(final float n) {
        return 4;
    }
    
    public static int t(final int n, final a0 a0, final g0 g0) {
        return Q(n) * 2 + v(a0, g0);
    }
    
    public static int u(final a0 a0) {
        return a0.b();
    }
    
    public static int v(final a0 a0, final g0 g0) {
        return ((a)a0).k(g0);
    }
    
    public static int w(final int n, final int n2) {
        return Q(n) + x(n2);
    }
    
    public static int x(final int n) {
        if (n >= 0) {
            return S(n);
        }
        return 10;
    }
    
    public static int y(final int n, final long n2) {
        return Q(n) + z(n2);
    }
    
    public static int z(final long n) {
        return U(n);
    }
    
    public abstract void A0(final int p0);
    
    public final void B0(final int n, final long n2) {
        this.U0(n, n2);
    }
    
    public final void C0(final long n) {
        this.V0(n);
    }
    
    public abstract void D0(final int p0, final a0 p1, final g0 p2);
    
    public abstract void E0(final a0 p0);
    
    public abstract void F0(final int p0, final a0 p1);
    
    public abstract void G0(final int p0, final ByteString p1);
    
    public final void H0(final int n, final int n2) {
        this.p0(n, n2);
    }
    
    public final void I0(final int n) {
        this.q0(n);
    }
    
    public final void J0(final int n, final long n2) {
        this.r0(n, n2);
    }
    
    public final void K0(final long n) {
        this.s0(n);
    }
    
    public final void L0(final int n, final int n2) {
        this.S0(n, V(n2));
    }
    
    public final void M0(final int n) {
        this.T0(V(n));
    }
    
    public final void N0(final int n, final long n2) {
        this.U0(n, W(n2));
    }
    
    public final void O0(final long n) {
        this.V0(W(n));
    }
    
    public abstract void P0(final int p0, final String p1);
    
    public abstract void Q0(final String p0);
    
    public abstract void R0(final int p0, final int p1);
    
    public abstract void S0(final int p0, final int p1);
    
    public abstract void T0(final int p0);
    
    public abstract void U0(final int p0, final long p1);
    
    public abstract void V0(final long p0);
    
    public abstract void X();
    
    public final void Y(final String s, final Utf8.UnpairedSurrogateException thrown) {
        CodedOutputStream.c.log(Level.WARNING, "Converting ill-formed UTF-16. Your Protocol Buffer will not round trip correctly!", thrown);
        final byte[] bytes = s.getBytes(t.b);
        try {
            this.T0(bytes.length);
            this.b(bytes, 0, bytes.length);
        }
        catch (final IndexOutOfBoundsException ex) {
            throw new OutOfSpaceException(ex);
        }
    }
    
    public boolean Z() {
        return this.b;
    }
    
    @Override
    public abstract void b(final byte[] p0, final int p1, final int p2);
    
    public final void d() {
        if (this.d0() == 0) {
            return;
        }
        throw new IllegalStateException("Did not write as much data as expected.");
    }
    
    public abstract int d0();
    
    public abstract void e0(final byte p0);
    
    public abstract void f0(final int p0, final boolean p1);
    
    public final void g0(final boolean b) {
        this.e0((byte)(b ? 1 : 0));
    }
    
    public final void h0(final byte[] array) {
        this.i0(array, 0, array.length);
    }
    
    public abstract void i0(final byte[] p0, final int p1, final int p2);
    
    public abstract void j0(final int p0, final ByteString p1);
    
    public abstract void k0(final ByteString p0);
    
    public final void l0(final int n, final double n2) {
        this.r0(n, Double.doubleToRawLongBits(n2));
    }
    
    public final void m0(final double n) {
        this.s0(Double.doubleToRawLongBits(n));
    }
    
    public final void n0(final int n, final int n2) {
        this.z0(n, n2);
    }
    
    public final void o0(final int n) {
        this.A0(n);
    }
    
    public abstract void p0(final int p0, final int p1);
    
    public abstract void q0(final int p0);
    
    public abstract void r0(final int p0, final long p1);
    
    public abstract void s0(final long p0);
    
    public final void t0(final int n, final float n2) {
        this.p0(n, Float.floatToRawIntBits(n2));
    }
    
    public final void u0(final float n) {
        this.q0(Float.floatToRawIntBits(n));
    }
    
    public final void v0(final int n, final a0 a0) {
        this.R0(n, 3);
        this.x0(a0);
        this.R0(n, 4);
    }
    
    public final void w0(final int n, final a0 a0, final g0 g0) {
        this.R0(n, 3);
        this.y0(a0, g0);
        this.R0(n, 4);
    }
    
    public final void x0(final a0 a0) {
        a0.f(this);
    }
    
    public final void y0(final a0 a0, final g0 g0) {
        g0.g(a0, this.a);
    }
    
    public abstract void z0(final int p0, final int p1);
    
    public static class OutOfSpaceException extends IOException
    {
        private static final String MESSAGE = "CodedOutputStream was writing to a flat byte array and ran out of space.";
        private static final long serialVersionUID = -6947486886997889499L;
        
        public OutOfSpaceException() {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.");
        }
        
        public OutOfSpaceException(final String str) {
            final StringBuilder sb = new StringBuilder();
            sb.append("CodedOutputStream was writing to a flat byte array and ran out of space.: ");
            sb.append(str);
            super(sb.toString());
        }
        
        public OutOfSpaceException(final String str, final Throwable cause) {
            final StringBuilder sb = new StringBuilder();
            sb.append("CodedOutputStream was writing to a flat byte array and ran out of space.: ");
            sb.append(str);
            super(sb.toString(), cause);
        }
        
        public OutOfSpaceException(final Throwable cause) {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.", cause);
        }
    }
    
    public abstract static class b extends CodedOutputStream
    {
        public final byte[] e;
        public final int f;
        public int g;
        public int h;
        
        public b(final int a) {
            super(null);
            if (a >= 0) {
                final byte[] e = new byte[Math.max(a, 20)];
                this.e = e;
                this.f = e.length;
                return;
            }
            throw new IllegalArgumentException("bufferSize must be >= 0");
        }
        
        public final void W0(final byte b) {
            this.e[this.g++] = b;
            ++this.h;
        }
        
        public final void X0(final int n) {
            final byte[] e = this.e;
            final int g = this.g;
            final int n2 = g + 1;
            e[g] = (byte)(n & 0xFF);
            final int n3 = n2 + 1;
            e[n2] = (byte)(n >> 8 & 0xFF);
            final int n4 = n3 + 1;
            e[n3] = (byte)(n >> 16 & 0xFF);
            this.g = n4 + 1;
            e[n4] = (byte)(n >> 24 & 0xFF);
            this.h += 4;
        }
        
        public final void Y0(final long n) {
            final byte[] e = this.e;
            final int g = this.g;
            final int n2 = g + 1;
            e[g] = (byte)(n & 0xFFL);
            final int n3 = n2 + 1;
            e[n2] = (byte)(n >> 8 & 0xFFL);
            final int n4 = n3 + 1;
            e[n3] = (byte)(n >> 16 & 0xFFL);
            final int n5 = n4 + 1;
            e[n4] = (byte)(0xFFL & n >> 24);
            final int n6 = n5 + 1;
            e[n5] = (byte)((int)(n >> 32) & 0xFF);
            final int n7 = n6 + 1;
            e[n6] = (byte)((int)(n >> 40) & 0xFF);
            final int n8 = n7 + 1;
            e[n7] = (byte)((int)(n >> 48) & 0xFF);
            this.g = n8 + 1;
            e[n8] = (byte)((int)(n >> 56) & 0xFF);
            this.h += 8;
        }
        
        public final void Z0(final int n) {
            if (n >= 0) {
                this.b1(n);
            }
            else {
                this.c1(n);
            }
        }
        
        public final void a1(final int n, final int n2) {
            this.b1(WireFormat.c(n, n2));
        }
        
        public final void b1(int n) {
            int n2 = n;
            if (CodedOutputStream.c()) {
                final long n3 = this.g;
                while ((n & 0xFFFFFF80) != 0x0) {
                    d12.P(this.e, this.g++, (byte)((n & 0x7F) | 0x80));
                    n >>>= 7;
                }
                d12.P(this.e, this.g++, (byte)n);
                n = (int)(this.g - n3);
                this.h += n;
                return;
            }
            while ((n2 & 0xFFFFFF80) != 0x0) {
                final byte[] e = this.e;
                n = this.g++;
                e[n] = (byte)((n2 & 0x7F) | 0x80);
                ++this.h;
                n2 >>>= 7;
            }
            final byte[] e2 = this.e;
            n = this.g++;
            e2[n] = (byte)n2;
            ++this.h;
        }
        
        public final void c1(long n) {
            long n2 = n;
            if (CodedOutputStream.c()) {
                final long n3 = this.g;
                while ((n & 0xFFFFFFFFFFFFFF80L) != 0x0L) {
                    d12.P(this.e, this.g++, (byte)(((int)n & 0x7F) | 0x80));
                    n >>>= 7;
                }
                d12.P(this.e, this.g++, (byte)n);
                this.h += (int)(this.g - n3);
                return;
            }
            while ((n2 & 0xFFFFFFFFFFFFFF80L) != 0x0L) {
                this.e[this.g++] = (byte)(((int)n2 & 0x7F) | 0x80);
                ++this.h;
                n2 >>>= 7;
            }
            this.e[this.g++] = (byte)n2;
            ++this.h;
        }
        
        @Override
        public final int d0() {
            throw new UnsupportedOperationException("spaceLeft() can only be called on CodedOutputStreams that are writing to a flat array or ByteBuffer.");
        }
    }
    
    public static class c extends CodedOutputStream
    {
        public final byte[] e;
        public final int f;
        public final int g;
        public int h;
        
        public c(final byte[] e, final int i, final int j) {
            super(null);
            if (e == null) {
                throw new NullPointerException("buffer");
            }
            final int length = e.length;
            final int g = i + j;
            if ((i | j | length - g) >= 0) {
                this.e = e;
                this.f = i;
                this.h = i;
                this.g = g;
                return;
            }
            throw new IllegalArgumentException(String.format("Array range is invalid. Buffer.length=%d, offset=%d, length=%d", e.length, i, j));
        }
        
        @Override
        public final void A0(final int n) {
            if (n >= 0) {
                this.T0(n);
            }
            else {
                this.V0(n);
            }
        }
        
        @Override
        public final void D0(final int n, final a0 a0, final g0 g0) {
            this.R0(n, 2);
            this.T0(((a)a0).k(g0));
            g0.g(a0, super.a);
        }
        
        @Override
        public final void E0(final a0 a0) {
            this.T0(a0.b());
            a0.f(this);
        }
        
        @Override
        public final void F0(final int n, final a0 a0) {
            this.R0(1, 3);
            this.S0(2, n);
            this.Y0(3, a0);
            this.R0(1, 4);
        }
        
        @Override
        public final void G0(final int n, final ByteString byteString) {
            this.R0(1, 3);
            this.S0(2, n);
            this.j0(3, byteString);
            this.R0(1, 4);
        }
        
        @Override
        public final void P0(final int n, final String s) {
            this.R0(n, 2);
            this.Q0(s);
        }
        
        @Override
        public final void Q0(final String s) {
            final int h = this.h;
            try {
                final int s2 = CodedOutputStream.S(s.length() * 3);
                final int s3 = CodedOutputStream.S(s.length());
                int h3;
                if (s3 == s2) {
                    final int h2 = h + s3;
                    this.h = h2;
                    h3 = Utf8.i(s, this.e, h2, this.d0());
                    this.T0(h3 - (this.h = h) - s3);
                }
                else {
                    this.T0(Utf8.j(s));
                    h3 = Utf8.i(s, this.e, this.h, this.d0());
                }
                this.h = h3;
            }
            catch (final IndexOutOfBoundsException ex) {
                throw new OutOfSpaceException(ex);
            }
            catch (final Utf8.UnpairedSurrogateException ex2) {
                this.h = h;
                this.Y(s, ex2);
            }
        }
        
        @Override
        public final void R0(final int n, final int n2) {
            this.T0(WireFormat.c(n, n2));
        }
        
        @Override
        public final void S0(final int n, final int n2) {
            this.R0(n, 0);
            this.T0(n2);
        }
        
        @Override
        public final void T0(int n) {
            while (true) {
                Label_0030: {
                    if ((n & 0xFFFFFF80) != 0x0) {
                        break Label_0030;
                    }
                    try {
                        this.e[this.h++] = (byte)n;
                        return;
                        this.e[this.h++] = (byte)((n & 0x7F) | 0x80);
                        n >>>= 7;
                    }
                    catch (final IndexOutOfBoundsException ex) {
                        throw new OutOfSpaceException(String.format("Pos: %d, limit: %d, len: %d", this.h, this.g, 1), ex);
                    }
                }
            }
        }
        
        @Override
        public final void U0(final int n, final long n2) {
            this.R0(n, 0);
            this.V0(n2);
        }
        
        @Override
        public final void V0(long n) {
            long n2 = n;
            if (CodedOutputStream.c()) {
                n2 = n;
                if (this.d0() >= 10) {
                    while ((n & 0xFFFFFFFFFFFFFF80L) != 0x0L) {
                        d12.P(this.e, this.h++, (byte)(((int)n & 0x7F) | 0x80));
                        n >>>= 7;
                    }
                    d12.P(this.e, this.h++, (byte)n);
                    return;
                }
            }
            while (true) {
                Label_0141: {
                    if ((n2 & 0xFFFFFFFFFFFFFF80L) != 0x0L) {
                        break Label_0141;
                    }
                    try {
                        this.e[this.h++] = (byte)n2;
                        return;
                        this.e[this.h++] = (byte)(((int)n2 & 0x7F) | 0x80);
                        n2 >>>= 7;
                    }
                    catch (final IndexOutOfBoundsException ex) {
                        throw new OutOfSpaceException(String.format("Pos: %d, limit: %d, len: %d", this.h, this.g, 1), ex);
                    }
                }
            }
        }
        
        public final void W0(final ByteBuffer byteBuffer) {
            final int remaining = byteBuffer.remaining();
            try {
                byteBuffer.get(this.e, this.h, remaining);
                this.h += remaining;
            }
            catch (final IndexOutOfBoundsException ex) {
                throw new OutOfSpaceException(String.format("Pos: %d, limit: %d, len: %d", this.h, this.g, remaining), ex);
            }
        }
        
        @Override
        public void X() {
        }
        
        public final void X0(final byte[] array, final int n, final int i) {
            try {
                System.arraycopy(array, n, this.e, this.h, i);
                this.h += i;
            }
            catch (final IndexOutOfBoundsException ex) {
                throw new OutOfSpaceException(String.format("Pos: %d, limit: %d, len: %d", this.h, this.g, i), ex);
            }
        }
        
        public final void Y0(final int n, final a0 a0) {
            this.R0(n, 2);
            this.E0(a0);
        }
        
        @Override
        public final void a(final ByteBuffer byteBuffer) {
            this.W0(byteBuffer);
        }
        
        @Override
        public final void b(final byte[] array, final int n, final int n2) {
            this.X0(array, n, n2);
        }
        
        @Override
        public final int d0() {
            return this.g - this.h;
        }
        
        @Override
        public final void e0(final byte b) {
            try {
                this.e[this.h++] = b;
            }
            catch (final IndexOutOfBoundsException ex) {
                throw new OutOfSpaceException(String.format("Pos: %d, limit: %d, len: %d", this.h, this.g, 1), ex);
            }
        }
        
        @Override
        public final void f0(final int n, final boolean b) {
            this.R0(n, 0);
            this.e0((byte)(b ? 1 : 0));
        }
        
        @Override
        public final void i0(final byte[] array, final int n, final int n2) {
            this.T0(n2);
            this.X0(array, n, n2);
        }
        
        @Override
        public final void j0(final int n, final ByteString byteString) {
            this.R0(n, 2);
            this.k0(byteString);
        }
        
        @Override
        public final void k0(final ByteString byteString) {
            this.T0(byteString.size());
            byteString.writeTo(this);
        }
        
        @Override
        public final void p0(final int n, final int n2) {
            this.R0(n, 5);
            this.q0(n2);
        }
        
        @Override
        public final void q0(final int n) {
            try {
                final byte[] e = this.e;
                final int h = this.h;
                final int n2 = h + 1;
                e[h] = (byte)(n & 0xFF);
                final int n3 = n2 + 1;
                e[n2] = (byte)(n >> 8 & 0xFF);
                final int n4 = n3 + 1;
                e[n3] = (byte)(n >> 16 & 0xFF);
                this.h = n4 + 1;
                e[n4] = (byte)(n >> 24 & 0xFF);
            }
            catch (final IndexOutOfBoundsException ex) {
                throw new OutOfSpaceException(String.format("Pos: %d, limit: %d, len: %d", this.h, this.g, 1), ex);
            }
        }
        
        @Override
        public final void r0(final int n, final long n2) {
            this.R0(n, 1);
            this.s0(n2);
        }
        
        @Override
        public final void s0(final long n) {
            try {
                final byte[] e = this.e;
                final int h = this.h;
                final int n2 = h + 1;
                e[h] = (byte)((int)n & 0xFF);
                final int n3 = n2 + 1;
                e[n2] = (byte)((int)(n >> 8) & 0xFF);
                final int n4 = n3 + 1;
                e[n3] = (byte)((int)(n >> 16) & 0xFF);
                final int n5 = n4 + 1;
                e[n4] = (byte)((int)(n >> 24) & 0xFF);
                final int n6 = n5 + 1;
                e[n5] = (byte)((int)(n >> 32) & 0xFF);
                final int n7 = n6 + 1;
                e[n6] = (byte)((int)(n >> 40) & 0xFF);
                final int n8 = n7 + 1;
                e[n7] = (byte)((int)(n >> 48) & 0xFF);
                this.h = n8 + 1;
                e[n8] = (byte)((int)(n >> 56) & 0xFF);
            }
            catch (final IndexOutOfBoundsException ex) {
                throw new OutOfSpaceException(String.format("Pos: %d, limit: %d, len: %d", this.h, this.g, 1), ex);
            }
        }
        
        @Override
        public final void z0(final int n, final int n2) {
            this.R0(n, 0);
            this.A0(n2);
        }
    }
    
    public static final class d extends b
    {
        public final OutputStream i;
        
        public d(final OutputStream i, final int n) {
            super(n);
            if (i != null) {
                this.i = i;
                return;
            }
            throw new NullPointerException("out");
        }
        
        @Override
        public void A0(final int n) {
            if (n >= 0) {
                this.T0(n);
            }
            else {
                this.V0(n);
            }
        }
        
        @Override
        public void D0(final int n, final a0 a0, final g0 g0) {
            this.R0(n, 2);
            this.i1(a0, g0);
        }
        
        @Override
        public void E0(final a0 a0) {
            this.T0(a0.b());
            a0.f(this);
        }
        
        @Override
        public void F0(final int n, final a0 a0) {
            this.R0(1, 3);
            this.S0(2, n);
            this.h1(3, a0);
            this.R0(1, 4);
        }
        
        @Override
        public void G0(final int n, final ByteString byteString) {
            this.R0(1, 3);
            this.S0(2, n);
            this.j0(3, byteString);
            this.R0(1, 4);
        }
        
        @Override
        public void P0(final int n, final String s) {
            this.R0(n, 2);
            this.Q0(s);
        }
        
        @Override
        public void Q0(final String s) {
            try {
                final int n = s.length() * 3;
                final int s2 = CodedOutputStream.S(n);
                final int n2 = s2 + n;
                final int f = super.f;
                if (n2 > f) {
                    final byte[] array = new byte[n];
                    final int i = Utf8.i(s, array, 0, n);
                    this.T0(i);
                    this.b(array, 0, i);
                    return;
                }
                if (n2 > f - super.g) {
                    this.d1();
                }
                final int s3 = CodedOutputStream.S(s.length());
                final int g = super.g;
                Label_0148: {
                    if (s3 != s2) {
                        break Label_0148;
                    }
                    final int g2 = g + s3;
                    try {
                        super.g = g2;
                        final int j = Utf8.i(s, super.e, g2, super.f - g2);
                        super.g = g;
                        int k = j - g - s3;
                        ((b)this).b1(k);
                        super.g = j;
                        super.h += k;
                        return;
                        k = Utf8.j(s);
                        ((b)this).b1(k);
                        super.g = Utf8.i(s, super.e, super.g, k);
                    }
                    catch (final ArrayIndexOutOfBoundsException ex) {
                        throw new OutOfSpaceException(ex);
                    }
                    catch (final Utf8.UnpairedSurrogateException ex2) {
                        try {
                            super.h -= super.g - g;
                            super.g = g;
                            throw ex2;
                        }
                        catch (final Utf8.UnpairedSurrogateException ex3) {
                            this.Y(s, ex3);
                        }
                    }
                }
            }
            catch (final Utf8.UnpairedSurrogateException ex4) {}
        }
        
        @Override
        public void R0(final int n, final int n2) {
            this.T0(WireFormat.c(n, n2));
        }
        
        @Override
        public void S0(final int n, final int n2) {
            this.e1(20);
            ((b)this).a1(n, 0);
            ((b)this).b1(n2);
        }
        
        @Override
        public void T0(final int n) {
            this.e1(5);
            ((b)this).b1(n);
        }
        
        @Override
        public void U0(final int n, final long n2) {
            this.e1(20);
            ((b)this).a1(n, 0);
            ((b)this).c1(n2);
        }
        
        @Override
        public void V0(final long n) {
            this.e1(10);
            ((b)this).c1(n);
        }
        
        @Override
        public void X() {
            if (super.g > 0) {
                this.d1();
            }
        }
        
        @Override
        public void a(final ByteBuffer byteBuffer) {
            this.f1(byteBuffer);
        }
        
        @Override
        public void b(final byte[] array, final int n, final int n2) {
            this.g1(array, n, n2);
        }
        
        public final void d1() {
            this.i.write(super.e, 0, super.g);
            super.g = 0;
        }
        
        @Override
        public void e0(final byte b) {
            if (super.g == super.f) {
                this.d1();
            }
            ((b)this).W0(b);
        }
        
        public final void e1(final int n) {
            if (super.f - super.g < n) {
                this.d1();
            }
        }
        
        @Override
        public void f0(final int n, final boolean b) {
            this.e1(11);
            ((b)this).a1(n, 0);
            ((b)this).W0((byte)(b ? 1 : 0));
        }
        
        public void f1(final ByteBuffer byteBuffer) {
            int remaining = byteBuffer.remaining();
            final int f = super.f;
            final int g = super.g;
            if (f - g >= remaining) {
                byteBuffer.get(super.e, g, remaining);
                super.g += remaining;
            }
            else {
                final int length = f - g;
                byteBuffer.get(super.e, g, length);
                remaining -= length;
                super.g = super.f;
                super.h += length;
                this.d1();
                while (true) {
                    final int f2 = super.f;
                    if (remaining <= f2) {
                        break;
                    }
                    byteBuffer.get(super.e, 0, f2);
                    this.i.write(super.e, 0, super.f);
                    final int f3 = super.f;
                    remaining -= f3;
                    super.h += f3;
                }
                byteBuffer.get(super.e, 0, remaining);
                super.g = remaining;
            }
            super.h += remaining;
        }
        
        public void g1(final byte[] b, int off, int n) {
            final int f = super.f;
            final int g = super.g;
            if (f - g >= n) {
                System.arraycopy(b, off, super.e, g, n);
                super.g += n;
            }
            else {
                final int n2 = f - g;
                System.arraycopy(b, off, super.e, g, n2);
                off += n2;
                n -= n2;
                super.g = super.f;
                super.h += n2;
                this.d1();
                if (n <= super.f) {
                    System.arraycopy(b, off, super.e, 0, n);
                    super.g = n;
                }
                else {
                    this.i.write(b, off, n);
                }
            }
            super.h += n;
        }
        
        public void h1(final int n, final a0 a0) {
            this.R0(n, 2);
            this.E0(a0);
        }
        
        @Override
        public void i0(final byte[] array, final int n, final int n2) {
            this.T0(n2);
            this.g1(array, n, n2);
        }
        
        public void i1(final a0 a0, final g0 g0) {
            this.T0(((a)a0).k(g0));
            g0.g(a0, super.a);
        }
        
        @Override
        public void j0(final int n, final ByteString byteString) {
            this.R0(n, 2);
            this.k0(byteString);
        }
        
        @Override
        public void k0(final ByteString byteString) {
            this.T0(byteString.size());
            byteString.writeTo(this);
        }
        
        @Override
        public void p0(final int n, final int n2) {
            this.e1(14);
            ((b)this).a1(n, 5);
            ((b)this).X0(n2);
        }
        
        @Override
        public void q0(final int n) {
            this.e1(4);
            ((b)this).X0(n);
        }
        
        @Override
        public void r0(final int n, final long n2) {
            this.e1(18);
            ((b)this).a1(n, 1);
            ((b)this).Y0(n2);
        }
        
        @Override
        public void s0(final long n) {
            this.e1(8);
            ((b)this).Y0(n);
        }
        
        @Override
        public void z0(final int n, final int n2) {
            this.e1(20);
            ((b)this).a1(n, 0);
            ((b)this).Z0(n2);
        }
    }
}

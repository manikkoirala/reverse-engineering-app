// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

import java.util.RandomAccess;
import java.util.List;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public abstract class t
{
    public static final Charset a;
    public static final Charset b;
    public static final Charset c;
    public static final byte[] d;
    public static final ByteBuffer e;
    public static final g f;
    
    static {
        a = Charset.forName("US-ASCII");
        b = Charset.forName("UTF-8");
        c = Charset.forName("ISO-8859-1");
        final byte[] array = d = new byte[0];
        e = ByteBuffer.wrap(array);
        f = g.j(array);
    }
    
    public static Object a(final Object o) {
        o.getClass();
        return o;
    }
    
    public static Object b(final Object o, final String s) {
        if (o != null) {
            return o;
        }
        throw new NullPointerException(s);
    }
    
    public static int c(final boolean b) {
        int n;
        if (b) {
            n = 1231;
        }
        else {
            n = 1237;
        }
        return n;
    }
    
    public static int d(final byte[] array) {
        return e(array, 0, array.length);
    }
    
    public static int e(final byte[] array, int h, int n) {
        n = (h = h(n, array, h, n));
        if (n == 0) {
            h = 1;
        }
        return h;
    }
    
    public static int f(final long n) {
        return (int)(n ^ n >>> 32);
    }
    
    public static boolean g(final byte[] array) {
        return Utf8.s(array);
    }
    
    public static int h(int n, final byte[] array, final int n2, final int n3) {
        for (int i = n2; i < n2 + n3; ++i) {
            n = n * 31 + array[i];
        }
        return n;
    }
    
    public static String i(final byte[] bytes) {
        return new String(bytes, t.b);
    }
    
    public interface a
    {
        int getNumber();
    }
    
    public interface b
    {
    }
    
    public interface c
    {
        boolean a(final int p0);
    }
    
    public interface d extends e
    {
    }
    
    public interface e extends List, RandomAccess
    {
        void e();
        
        boolean h();
        
        e j(final int p0);
    }
}

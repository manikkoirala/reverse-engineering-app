// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

import java.lang.reflect.Field;
import java.io.Serializable;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Map;

public abstract class GeneratedMessageLite extends com.google.protobuf.a
{
    private static final int MEMOIZED_SERIALIZED_SIZE_MASK = Integer.MAX_VALUE;
    private static final int MUTABLE_FLAG_MASK = Integer.MIN_VALUE;
    static final int UNINITIALIZED_HASH_CODE = 0;
    static final int UNINITIALIZED_SERIALIZED_SIZE = Integer.MAX_VALUE;
    private static Map<Object, GeneratedMessageLite> defaultInstanceMap;
    private int memoizedSerializedSize;
    protected l0 unknownFields;
    
    static {
        GeneratedMessageLite.defaultInstanceMap = new ConcurrentHashMap<Object, GeneratedMessageLite>();
    }
    
    public GeneratedMessageLite() {
        this.memoizedSerializedSize = -1;
        this.unknownFields = l0.c();
    }
    
    public static t.e A() {
        return e0.c();
    }
    
    public static GeneratedMessageLite B(final Class clazz) {
        GeneratedMessageLite generatedMessageLite;
        if ((generatedMessageLite = GeneratedMessageLite.defaultInstanceMap.get(clazz)) == null) {
            try {
                Class.forName(clazz.getName(), true, clazz.getClassLoader());
                generatedMessageLite = GeneratedMessageLite.defaultInstanceMap.get(clazz);
            }
            catch (final ClassNotFoundException cause) {
                throw new IllegalStateException("Class initialization cannot fail.", cause);
            }
        }
        GeneratedMessageLite c;
        if ((c = generatedMessageLite) == null) {
            c = ((GeneratedMessageLite)d12.l(clazz)).C();
            if (c == null) {
                throw new IllegalStateException();
            }
            GeneratedMessageLite.defaultInstanceMap.put(clazz, c);
        }
        return c;
    }
    
    static Object G(final Method method, final Object obj, final Object... args) {
        try {
            return method.invoke(obj, args);
        }
        catch (final InvocationTargetException ex) {
            final Throwable cause = ex.getCause();
            if (cause instanceof RuntimeException) {
                throw (RuntimeException)cause;
            }
            if (cause instanceof Error) {
                throw (Error)cause;
            }
            throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
        }
        catch (final IllegalAccessException cause2) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", cause2);
        }
    }
    
    public static final boolean H(final GeneratedMessageLite generatedMessageLite, final boolean b) {
        final byte byteValue = (byte)generatedMessageLite.w(MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED);
        if (byteValue == 1) {
            return true;
        }
        if (byteValue == 0) {
            return false;
        }
        final boolean b2 = k91.a().d(generatedMessageLite).b(generatedMessageLite);
        if (b) {
            final MethodToInvoke set_MEMOIZED_IS_INITIALIZED = MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED;
            GeneratedMessageLite generatedMessageLite2;
            if (b2) {
                generatedMessageLite2 = generatedMessageLite;
            }
            else {
                generatedMessageLite2 = null;
            }
            generatedMessageLite.x(set_MEMOIZED_IS_INITIALIZED, generatedMessageLite2);
        }
        return b2;
    }
    
    public static t.e L(final t.e e) {
        final int size = e.size();
        int n;
        if (size == 0) {
            n = 10;
        }
        else {
            n = size * 2;
        }
        return e.j(n);
    }
    
    public static Object N(final a0 a0, final String s, final Object[] array) {
        return new kc1(a0, s, array);
    }
    
    public static GeneratedMessageLite P(final GeneratedMessageLite generatedMessageLite, final ByteString byteString) {
        return p(Q(generatedMessageLite, byteString, l.b()));
    }
    
    public static GeneratedMessageLite Q(final GeneratedMessageLite generatedMessageLite, final ByteString byteString, final l l) {
        return p(S(generatedMessageLite, byteString, l));
    }
    
    public static GeneratedMessageLite R(final GeneratedMessageLite generatedMessageLite, final byte[] array) {
        return p(U(generatedMessageLite, array, 0, array.length, l.b()));
    }
    
    public static GeneratedMessageLite S(GeneratedMessageLite t, final ByteString byteString, final l l) {
        final g codedInput = byteString.newCodedInput();
        t = T(t, codedInput, l);
        try {
            codedInput.a(0);
            return t;
        }
        catch (final InvalidProtocolBufferException ex) {
            throw ex.setUnfinishedMessage(t);
        }
    }
    
    public static GeneratedMessageLite T(final GeneratedMessageLite generatedMessageLite, final g g, final l l) {
        final GeneratedMessageLite o = generatedMessageLite.O();
        try {
            final g0 d = k91.a().d(o);
            d.i(o, h.N(g), l);
            d.d(o);
            return o;
        }
        catch (final RuntimeException ex) {
            if (ex.getCause() instanceof InvalidProtocolBufferException) {
                throw (InvalidProtocolBufferException)ex.getCause();
            }
            throw ex;
        }
        catch (final IOException ex2) {
            if (ex2.getCause() instanceof InvalidProtocolBufferException) {
                throw (InvalidProtocolBufferException)ex2.getCause();
            }
            throw new InvalidProtocolBufferException(ex2).setUnfinishedMessage(o);
        }
        catch (final UninitializedMessageException ex3) {
            throw ex3.asInvalidProtocolBufferException().setUnfinishedMessage(o);
        }
        catch (final InvalidProtocolBufferException ex4) {
            InvalidProtocolBufferException ex5 = ex4;
            if (ex4.getThrownFromInputStream()) {
                ex5 = new InvalidProtocolBufferException(ex4);
            }
            throw ex5.setUnfinishedMessage(o);
        }
    }
    
    public static GeneratedMessageLite U(final GeneratedMessageLite generatedMessageLite, final byte[] array, final int n, final int n2, final l l) {
        final GeneratedMessageLite o = generatedMessageLite.O();
        try {
            final g0 d = k91.a().d(o);
            d.h(o, array, n, n + n2, new e.a(l));
            d.d(o);
            return o;
        }
        catch (final IndexOutOfBoundsException ex) {
            throw InvalidProtocolBufferException.truncatedMessage().setUnfinishedMessage(o);
        }
        catch (final IOException ex2) {
            if (ex2.getCause() instanceof InvalidProtocolBufferException) {
                throw (InvalidProtocolBufferException)ex2.getCause();
            }
            throw new InvalidProtocolBufferException(ex2).setUnfinishedMessage(o);
        }
        catch (final UninitializedMessageException ex3) {
            throw ex3.asInvalidProtocolBufferException().setUnfinishedMessage(o);
        }
        catch (final InvalidProtocolBufferException ex4) {
            InvalidProtocolBufferException ex5 = ex4;
            if (ex4.getThrownFromInputStream()) {
                ex5 = new InvalidProtocolBufferException(ex4);
            }
            throw ex5.setUnfinishedMessage(o);
        }
    }
    
    public static void V(final Class clazz, final GeneratedMessageLite generatedMessageLite) {
        GeneratedMessageLite.defaultInstanceMap.put(clazz, generatedMessageLite);
        generatedMessageLite.J();
    }
    
    public static GeneratedMessageLite p(final GeneratedMessageLite unfinishedMessage) {
        if (unfinishedMessage != null && !unfinishedMessage.isInitialized()) {
            throw unfinishedMessage.m().asInvalidProtocolBufferException().setUnfinishedMessage(unfinishedMessage);
        }
        return unfinishedMessage;
    }
    
    public static t.d z() {
        return s.l();
    }
    
    public final GeneratedMessageLite C() {
        return (GeneratedMessageLite)this.w(MethodToInvoke.GET_DEFAULT_INSTANCE);
    }
    
    public int D() {
        return super.memoizedHashCode;
    }
    
    int E() {
        return this.memoizedSerializedSize & Integer.MAX_VALUE;
    }
    
    public boolean F() {
        return this.D() == 0;
    }
    
    boolean I() {
        return (this.memoizedSerializedSize & Integer.MIN_VALUE) != 0x0;
    }
    
    public void J() {
        k91.a().d(this).d(this);
        this.K();
    }
    
    public void K() {
        this.memoizedSerializedSize &= Integer.MAX_VALUE;
    }
    
    public final a M() {
        return (a)this.w(MethodToInvoke.NEW_BUILDER);
    }
    
    public GeneratedMessageLite O() {
        return (GeneratedMessageLite)this.w(MethodToInvoke.NEW_MUTABLE_INSTANCE);
    }
    
    public void W(final int memoizedHashCode) {
        super.memoizedHashCode = memoizedHashCode;
    }
    
    void X(final int i) {
        if (i >= 0) {
            this.memoizedSerializedSize = ((i & Integer.MAX_VALUE) | (this.memoizedSerializedSize & Integer.MIN_VALUE));
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("serialized size must be non-negative, was ");
        sb.append(i);
        throw new IllegalStateException(sb.toString());
    }
    
    public final a Y() {
        return ((a)this.w(MethodToInvoke.NEW_BUILDER)).v(this);
    }
    
    @Override
    public int b() {
        return this.k(null);
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o != null && this.getClass() == o.getClass() && k91.a().d(this).c(this, o));
    }
    
    @Override
    public void f(final CodedOutputStream codedOutputStream) {
        k91.a().d(this).g(this, i.P(codedOutputStream));
    }
    
    @Override
    public int hashCode() {
        if (this.I()) {
            return this.s();
        }
        if (this.F()) {
            this.W(this.s());
        }
        return this.D();
    }
    
    @Override
    public final b31 i() {
        return (b31)this.w(MethodToInvoke.GET_PARSER);
    }
    
    @Override
    public final boolean isInitialized() {
        return H(this, true);
    }
    
    @Override
    public int k(final g0 g0) {
        if (this.I()) {
            final int t = this.t(g0);
            if (t >= 0) {
                return t;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("serialized size must be non-negative, was ");
            sb.append(t);
            throw new IllegalStateException(sb.toString());
        }
        else {
            if (this.E() != Integer.MAX_VALUE) {
                return this.E();
            }
            final int t2 = this.t(g0);
            this.X(t2);
            return t2;
        }
    }
    
    Object o() {
        return this.w(MethodToInvoke.BUILD_MESSAGE_INFO);
    }
    
    public void q() {
        super.memoizedHashCode = 0;
    }
    
    public void r() {
        this.X(Integer.MAX_VALUE);
    }
    
    public int s() {
        return k91.a().d(this).f(this);
    }
    
    public final int t(final g0 g0) {
        if (g0 == null) {
            return k91.a().d(this).e(this);
        }
        return g0.e(this);
    }
    
    @Override
    public String toString() {
        return b0.f(this, super.toString());
    }
    
    public final a u() {
        return (a)this.w(MethodToInvoke.NEW_BUILDER);
    }
    
    public final a v(final GeneratedMessageLite generatedMessageLite) {
        return this.u().v(generatedMessageLite);
    }
    
    public Object w(final MethodToInvoke methodToInvoke) {
        return this.y(methodToInvoke, null, null);
    }
    
    public Object x(final MethodToInvoke methodToInvoke, final Object o) {
        return this.y(methodToInvoke, o, null);
    }
    
    public abstract Object y(final MethodToInvoke p0, final Object p1, final Object p2);
    
    public enum MethodToInvoke
    {
        private static final MethodToInvoke[] $VALUES;
        
        BUILD_MESSAGE_INFO, 
        GET_DEFAULT_INSTANCE, 
        GET_MEMOIZED_IS_INITIALIZED, 
        GET_PARSER, 
        NEW_BUILDER, 
        NEW_MUTABLE_INSTANCE, 
        SET_MEMOIZED_IS_INITIALIZED;
    }
    
    public static final class SerializedForm implements Serializable
    {
        private static final long serialVersionUID = 0L;
        private final byte[] asBytes;
        private final Class<?> messageClass;
        private final String messageClassName;
        
        public SerializedForm(final a0 a0) {
            final Class<? extends a0> class1 = a0.getClass();
            this.messageClass = class1;
            this.messageClassName = class1.getName();
            this.asBytes = a0.toByteArray();
        }
        
        public static SerializedForm of(final a0 a0) {
            return new SerializedForm(a0);
        }
        
        @Deprecated
        private Object readResolveFallback() {
            try {
                final Field declaredField = this.resolveMessageClass().getDeclaredField("defaultInstance");
                declaredField.setAccessible(true);
                return ((a0)declaredField.get(null)).d().c(this.asBytes).g();
            }
            catch (final InvalidProtocolBufferException cause) {
                throw new RuntimeException("Unable to understand proto buffer", cause);
            }
            catch (final IllegalAccessException cause2) {
                throw new RuntimeException("Unable to call parsePartialFrom", cause2);
            }
            catch (final SecurityException cause3) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unable to call defaultInstance in ");
                sb.append(this.messageClassName);
                throw new RuntimeException(sb.toString(), cause3);
            }
            catch (final NoSuchFieldException cause4) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Unable to find defaultInstance in ");
                sb2.append(this.messageClassName);
                throw new RuntimeException(sb2.toString(), cause4);
            }
            catch (final ClassNotFoundException cause5) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Unable to find proto buffer class: ");
                sb3.append(this.messageClassName);
                throw new RuntimeException(sb3.toString(), cause5);
            }
        }
        
        private Class<?> resolveMessageClass() {
            Class<?> clazz = this.messageClass;
            if (clazz == null) {
                clazz = Class.forName(this.messageClassName);
            }
            return clazz;
        }
        
        public Object readResolve() {
            try {
                final Field declaredField = this.resolveMessageClass().getDeclaredField("DEFAULT_INSTANCE");
                declaredField.setAccessible(true);
                return ((a0)declaredField.get(null)).d().c(this.asBytes).g();
            }
            catch (final InvalidProtocolBufferException cause) {
                throw new RuntimeException("Unable to understand proto buffer", cause);
            }
            catch (final IllegalAccessException cause2) {
                throw new RuntimeException("Unable to call parsePartialFrom", cause2);
            }
            catch (final SecurityException cause3) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unable to call DEFAULT_INSTANCE in ");
                sb.append(this.messageClassName);
                throw new RuntimeException(sb.toString(), cause3);
            }
            catch (final NoSuchFieldException ex) {
                return this.readResolveFallback();
            }
            catch (final ClassNotFoundException cause4) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Unable to find proto buffer class: ");
                sb2.append(this.messageClassName);
                throw new RuntimeException(sb2.toString(), cause4);
            }
        }
    }
    
    public abstract static class a extends com.google.protobuf.a.a
    {
        public final GeneratedMessageLite a;
        public GeneratedMessageLite b;
        
        public a(final GeneratedMessageLite a) {
            this.a = a;
            if (!a.I()) {
                this.b = this.z();
                return;
            }
            throw new IllegalArgumentException("Default instance must be immutable.");
        }
        
        public static void y(final Object o, final Object o2) {
            k91.a().d(o).a(o, o2);
        }
        
        private GeneratedMessageLite z() {
            return this.a.O();
        }
        
        @Override
        public final boolean isInitialized() {
            return GeneratedMessageLite.H(this.b, false);
        }
        
        public final GeneratedMessageLite p() {
            final GeneratedMessageLite q = this.q();
            if (q.isInitialized()) {
                return q;
            }
            throw com.google.protobuf.a.a.n(q);
        }
        
        public GeneratedMessageLite q() {
            if (!this.b.I()) {
                return this.b;
            }
            this.b.J();
            return this.b;
        }
        
        public a r() {
            final a m = this.u().M();
            m.b = this.q();
            return m;
        }
        
        public final void s() {
            if (!this.b.I()) {
                this.t();
            }
        }
        
        public void t() {
            final GeneratedMessageLite z = this.z();
            y(z, this.b);
            this.b = z;
        }
        
        public GeneratedMessageLite u() {
            return this.a;
        }
        
        public a v(final GeneratedMessageLite generatedMessageLite) {
            if (this.u().equals(generatedMessageLite)) {
                return this;
            }
            this.s();
            y(this.b, generatedMessageLite);
            return this;
        }
        
        public a w(final byte[] array, final int n, final int n2) {
            return this.x(array, n, n2, l.b());
        }
        
        public a x(final byte[] array, final int n, final int n2, final l l) {
            this.s();
            try {
                k91.a().d(this.b).h(this.b, array, n, n + n2, new e.a(l));
                return this;
            }
            catch (final IOException cause) {
                throw new RuntimeException("Reading from byte array should not throw IOException.", cause);
            }
            catch (final IndexOutOfBoundsException ex) {
                throw InvalidProtocolBufferException.truncatedMessage();
            }
            catch (final InvalidProtocolBufferException ex2) {
                throw ex2;
            }
        }
    }
    
    public static class b extends com.google.protobuf.b
    {
        public final GeneratedMessageLite b;
        
        public b(final GeneratedMessageLite b) {
            this.b = b;
        }
        
        public GeneratedMessageLite j(final g g, final l l) {
            return GeneratedMessageLite.T(this.b, g, l);
        }
        
        public GeneratedMessageLite k(final byte[] array, final int n, final int n2, final l l) {
            return U(this.b, array, n, n2, l);
        }
    }
    
    public abstract static class c extends k
    {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

import java.util.Map;
import java.util.List;

public final class h implements f0
{
    public final g a;
    public int b;
    public int c;
    public int d;
    
    public h(g a) {
        this.d = 0;
        a = (g)t.b(a, "input");
        this.a = a;
        a.d = this;
    }
    
    public static h N(final g g) {
        final h d = g.d;
        if (d != null) {
            return d;
        }
        return new h(g);
    }
    
    @Override
    public int A() {
        this.W(0);
        return this.a.v();
    }
    
    @Override
    public void B(final List list) {
        if (list instanceof w) {
            final w w = (w)list;
            final int b = WireFormat.b(this.b);
            if (b == 1) {
                int i;
                do {
                    w.b(this.a.t());
                    if (this.a.e()) {
                        return;
                    }
                    i = this.a.D();
                } while (i == this.b);
                this.d = i;
                return;
            }
            if (b != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            final int e = this.a.E();
            this.Y(e);
            do {
                w.b(this.a.t());
            } while (this.a.d() < this.a.d() + e);
        }
        else {
            final int b2 = WireFormat.b(this.b);
            if (b2 == 1) {
                int j;
                do {
                    list.add(this.a.t());
                    if (this.a.e()) {
                        return;
                    }
                    j = this.a.D();
                } while (j == this.b);
                this.d = j;
                return;
            }
            if (b2 != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            final int e2 = this.a.E();
            this.Y(e2);
            do {
                list.add(this.a.t());
            } while (this.a.d() < this.a.d() + e2);
        }
    }
    
    @Override
    public void C(final List list) {
        int n;
        if (list instanceof s) {
            final s s = (s)list;
            final int b = WireFormat.b(this.b);
            if (b == 0) {
                int i;
                do {
                    s.g(this.a.E());
                    if (this.a.e()) {
                        return;
                    }
                    i = this.a.D();
                } while (i == this.b);
                this.d = i;
                return;
            }
            if (b != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            n = this.a.d() + this.a.E();
            do {
                s.g(this.a.E());
            } while (this.a.d() < n);
        }
        else {
            final int b2 = WireFormat.b(this.b);
            if (b2 == 0) {
                int j;
                do {
                    list.add(this.a.E());
                    if (this.a.e()) {
                        return;
                    }
                    j = this.a.D();
                } while (j == this.b);
                this.d = j;
                return;
            }
            if (b2 != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            n = this.a.d() + this.a.E();
            do {
                list.add(this.a.E());
            } while (this.a.d() < n);
        }
        this.V(n);
    }
    
    @Override
    public int D() {
        this.W(5);
        return this.a.s();
    }
    
    @Override
    public long E() {
        this.W(0);
        return this.a.A();
    }
    
    @Override
    public String F() {
        this.W(2);
        return this.a.B();
    }
    
    @Override
    public int G() {
        this.W(5);
        return this.a.x();
    }
    
    @Override
    public String H() {
        this.W(2);
        return this.a.C();
    }
    
    @Override
    public void I(final Map map, final x.a a, final l l) {
        this.W(2);
        final int n = this.a.n(this.a.E());
        Object o = a.b;
        Object o2 = a.d;
        try {
            while (true) {
                final int m = this.m();
                if (m == Integer.MAX_VALUE) {
                    break;
                }
                if (this.a.e()) {
                    break;
                }
                Label_0129: {
                    if (m == 1) {
                        break Label_0129;
                    }
                    Label_0104: {
                        if (m == 2) {
                            break Label_0104;
                        }
                        try {
                            if (this.p()) {
                                continue;
                            }
                            throw new InvalidProtocolBufferException("Unable to parse map entry.");
                            o = this.Q(a.a, null, null);
                            continue;
                            o2 = this.Q(a.c, a.d.getClass(), l);
                            continue;
                        }
                        catch (final InvalidProtocolBufferException.InvalidWireTypeException ex) {
                            if (this.p()) {
                                continue;
                            }
                            throw new InvalidProtocolBufferException("Unable to parse map entry.");
                        }
                    }
                }
                break;
            }
            map.put(o, o2);
        }
        finally {
            this.a.m(n);
        }
    }
    
    @Override
    public void J(final Object o, final g0 g0, final l l) {
        this.W(3);
        this.O(o, g0, l);
    }
    
    @Override
    public void K(final Object o, final g0 g0, final l l) {
        this.W(2);
        this.P(o, g0, l);
    }
    
    @Override
    public void L(final List list, final g0 g0, final l l) {
        if (WireFormat.b(this.b) == 3) {
            int i;
            do {
                list.add(this.R(g0, l));
                if (!this.a.e()) {
                    if (this.d == 0) {
                        i = this.a.D();
                        continue;
                    }
                }
                return;
            } while (i == this.b);
            this.d = i;
            return;
        }
        throw InvalidProtocolBufferException.invalidWireType();
    }
    
    @Override
    public void M(final List list, final g0 g0, final l l) {
        if (WireFormat.b(this.b) == 2) {
            int i;
            do {
                list.add(this.S(g0, l));
                if (!this.a.e()) {
                    if (this.d == 0) {
                        i = this.a.D();
                        continue;
                    }
                }
                return;
            } while (i == this.b);
            this.d = i;
            return;
        }
        throw InvalidProtocolBufferException.invalidWireType();
    }
    
    public final void O(final Object o, final g0 g0, final l l) {
        final int c = this.c;
        this.c = WireFormat.c(WireFormat.a(this.b), 4);
        try {
            g0.i(o, this, l);
            if (this.b == this.c) {
                return;
            }
            throw InvalidProtocolBufferException.parseFailure();
        }
        finally {
            this.c = c;
        }
    }
    
    public final void P(final Object o, final g0 g0, final l l) {
        final int e = this.a.E();
        final g a = this.a;
        if (a.a < a.b) {
            final int n = a.n(e);
            final g a2 = this.a;
            ++a2.a;
            g0.i(o, this, l);
            this.a.a(0);
            final g a3 = this.a;
            --a3.a;
            a3.m(n);
            return;
        }
        throw InvalidProtocolBufferException.recursionLimitExceeded();
    }
    
    public final Object Q(final WireFormat.FieldType fieldType, final Class clazz, final l l) {
        switch (h$a.a[fieldType.ordinal()]) {
            default: {
                throw new IllegalArgumentException("unsupported field type.");
            }
            case 17: {
                return this.i();
            }
            case 16: {
                return this.c();
            }
            case 15: {
                return this.H();
            }
            case 14: {
                return this.E();
            }
            case 13: {
                return this.e();
            }
            case 12: {
                return this.b();
            }
            case 11: {
                return this.G();
            }
            case 10: {
                return this.T(clazz, l);
            }
            case 9: {
                return this.s();
            }
            case 8: {
                return this.A();
            }
            case 7: {
                return this.readFloat();
            }
            case 6: {
                return this.t();
            }
            case 5: {
                return this.D();
            }
            case 4: {
                return this.d();
            }
            case 3: {
                return this.readDouble();
            }
            case 2: {
                return this.g();
            }
            case 1: {
                return this.v();
            }
        }
    }
    
    public final Object R(final g0 g0, final l l) {
        final Object instance = g0.newInstance();
        this.O(instance, g0, l);
        g0.d(instance);
        return instance;
    }
    
    public final Object S(final g0 g0, final l l) {
        final Object instance = g0.newInstance();
        this.P(instance, g0, l);
        g0.d(instance);
        return instance;
    }
    
    public Object T(final Class clazz, final l l) {
        this.W(2);
        return this.S(k91.a().c(clazz), l);
    }
    
    public void U(final List list, final boolean b) {
        if (WireFormat.b(this.b) != 2) {
            throw InvalidProtocolBufferException.invalidWireType();
        }
        if (list instanceof fj0 && !b) {
            final fj0 fj0 = (fj0)list;
            int i;
            do {
                fj0.q(this.g());
                if (this.a.e()) {
                    return;
                }
                i = this.a.D();
            } while (i == this.b);
            this.d = i;
            return;
        }
        int j;
        do {
            String s;
            if (b) {
                s = this.H();
            }
            else {
                s = this.F();
            }
            list.add(s);
            if (this.a.e()) {
                return;
            }
            j = this.a.D();
        } while (j == this.b);
        this.d = j;
    }
    
    public final void V(final int n) {
        if (this.a.d() == n) {
            return;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public final void W(final int n) {
        if (WireFormat.b(this.b) == n) {
            return;
        }
        throw InvalidProtocolBufferException.invalidWireType();
    }
    
    public final void X(final int n) {
        if ((n & 0x3) == 0x0) {
            return;
        }
        throw InvalidProtocolBufferException.parseFailure();
    }
    
    public final void Y(final int n) {
        if ((n & 0x7) == 0x0) {
            return;
        }
        throw InvalidProtocolBufferException.parseFailure();
    }
    
    @Override
    public void a(final List list) {
        int n;
        if (list instanceof w) {
            final w w = (w)list;
            final int b = WireFormat.b(this.b);
            if (b == 0) {
                int i;
                do {
                    w.b(this.a.A());
                    if (this.a.e()) {
                        return;
                    }
                    i = this.a.D();
                } while (i == this.b);
                this.d = i;
                return;
            }
            if (b != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            n = this.a.d() + this.a.E();
            do {
                w.b(this.a.A());
            } while (this.a.d() < n);
        }
        else {
            final int b2 = WireFormat.b(this.b);
            if (b2 == 0) {
                int j;
                do {
                    list.add(this.a.A());
                    if (this.a.e()) {
                        return;
                    }
                    j = this.a.D();
                } while (j == this.b);
                this.d = j;
                return;
            }
            if (b2 != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            n = this.a.d() + this.a.E();
            do {
                list.add(this.a.A());
            } while (this.a.d() < n);
        }
        this.V(n);
    }
    
    @Override
    public long b() {
        this.W(1);
        return this.a.y();
    }
    
    @Override
    public int c() {
        this.W(0);
        return this.a.E();
    }
    
    @Override
    public int d() {
        this.W(0);
        return this.a.r();
    }
    
    @Override
    public int e() {
        this.W(0);
        return this.a.z();
    }
    
    @Override
    public void f(final List list) {
        int n;
        if (list instanceof f) {
            final f f = (f)list;
            final int b = WireFormat.b(this.b);
            if (b == 0) {
                int i;
                do {
                    f.b(this.a.o());
                    if (this.a.e()) {
                        return;
                    }
                    i = this.a.D();
                } while (i == this.b);
                this.d = i;
                return;
            }
            if (b != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            n = this.a.d() + this.a.E();
            do {
                f.b(this.a.o());
            } while (this.a.d() < n);
        }
        else {
            final int b2 = WireFormat.b(this.b);
            if (b2 == 0) {
                int j;
                do {
                    list.add(this.a.o());
                    if (this.a.e()) {
                        return;
                    }
                    j = this.a.D();
                } while (j == this.b);
                this.d = j;
                return;
            }
            if (b2 != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            n = this.a.d() + this.a.E();
            do {
                list.add(this.a.o());
            } while (this.a.d() < n);
        }
        this.V(n);
    }
    
    @Override
    public ByteString g() {
        this.W(2);
        return this.a.p();
    }
    
    @Override
    public int getTag() {
        return this.b;
    }
    
    @Override
    public void h(final List list) {
        int n;
        if (list instanceof s) {
            final s s = (s)list;
            final int b = WireFormat.b(this.b);
            if (b == 0) {
                int i;
                do {
                    s.g(this.a.z());
                    if (this.a.e()) {
                        return;
                    }
                    i = this.a.D();
                } while (i == this.b);
                this.d = i;
                return;
            }
            if (b != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            n = this.a.d() + this.a.E();
            do {
                s.g(this.a.z());
            } while (this.a.d() < n);
        }
        else {
            final int b2 = WireFormat.b(this.b);
            if (b2 == 0) {
                int j;
                do {
                    list.add(this.a.z());
                    if (this.a.e()) {
                        return;
                    }
                    j = this.a.D();
                } while (j == this.b);
                this.d = j;
                return;
            }
            if (b2 != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            n = this.a.d() + this.a.E();
            do {
                list.add(this.a.z());
            } while (this.a.d() < n);
        }
        this.V(n);
    }
    
    @Override
    public long i() {
        this.W(0);
        return this.a.F();
    }
    
    @Override
    public void j(final List list) {
        if (list instanceof w) {
            final w w = (w)list;
            final int b = WireFormat.b(this.b);
            if (b == 1) {
                int i;
                do {
                    w.b(this.a.y());
                    if (this.a.e()) {
                        return;
                    }
                    i = this.a.D();
                } while (i == this.b);
                this.d = i;
                return;
            }
            if (b != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            final int e = this.a.E();
            this.Y(e);
            do {
                w.b(this.a.y());
            } while (this.a.d() < this.a.d() + e);
        }
        else {
            final int b2 = WireFormat.b(this.b);
            if (b2 == 1) {
                int j;
                do {
                    list.add(this.a.y());
                    if (this.a.e()) {
                        return;
                    }
                    j = this.a.D();
                } while (j == this.b);
                this.d = j;
                return;
            }
            if (b2 != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            final int e2 = this.a.E();
            this.Y(e2);
            do {
                list.add(this.a.y());
            } while (this.a.d() < this.a.d() + e2);
        }
    }
    
    @Override
    public void k(final List list) {
        int n;
        if (list instanceof s) {
            final s s = (s)list;
            final int b = WireFormat.b(this.b);
            if (b == 0) {
                int i;
                do {
                    s.g(this.a.v());
                    if (this.a.e()) {
                        return;
                    }
                    i = this.a.D();
                } while (i == this.b);
                this.d = i;
                return;
            }
            if (b != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            n = this.a.d() + this.a.E();
            do {
                s.g(this.a.v());
            } while (this.a.d() < n);
        }
        else {
            final int b2 = WireFormat.b(this.b);
            if (b2 == 0) {
                int j;
                do {
                    list.add(this.a.v());
                    if (this.a.e()) {
                        return;
                    }
                    j = this.a.D();
                } while (j == this.b);
                this.d = j;
                return;
            }
            if (b2 != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            n = this.a.d() + this.a.E();
            do {
                list.add(this.a.v());
            } while (this.a.d() < n);
        }
        this.V(n);
    }
    
    @Override
    public void l(final List list) {
        if (list instanceof s) {
            final s s = (s)list;
            final int b = WireFormat.b(this.b);
            if (b != 2) {
                if (b == 5) {
                    int i;
                    do {
                        s.g(this.a.s());
                        if (this.a.e()) {
                            return;
                        }
                        i = this.a.D();
                    } while (i == this.b);
                    this.d = i;
                    return;
                }
                throw InvalidProtocolBufferException.invalidWireType();
            }
            else {
                final int e = this.a.E();
                this.X(e);
                do {
                    s.g(this.a.s());
                } while (this.a.d() < this.a.d() + e);
            }
        }
        else {
            final int b2 = WireFormat.b(this.b);
            if (b2 != 2) {
                if (b2 == 5) {
                    int j;
                    do {
                        list.add(this.a.s());
                        if (this.a.e()) {
                            return;
                        }
                        j = this.a.D();
                    } while (j == this.b);
                    this.d = j;
                    return;
                }
                throw InvalidProtocolBufferException.invalidWireType();
            }
            else {
                final int e2 = this.a.E();
                this.X(e2);
                do {
                    list.add(this.a.s());
                } while (this.a.d() < this.a.d() + e2);
            }
        }
    }
    
    @Override
    public int m() {
        final int d = this.d;
        if (d != 0) {
            this.b = d;
            this.d = 0;
        }
        else {
            this.b = this.a.D();
        }
        final int b = this.b;
        if (b != 0 && b != this.c) {
            return WireFormat.a(b);
        }
        return Integer.MAX_VALUE;
    }
    
    @Override
    public void n(final List list) {
        this.U(list, false);
    }
    
    @Override
    public void o(final List list) {
        if (list instanceof p) {
            final p p = (p)list;
            final int b = WireFormat.b(this.b);
            if (b != 2) {
                if (b == 5) {
                    int i;
                    do {
                        p.b(this.a.u());
                        if (this.a.e()) {
                            return;
                        }
                        i = this.a.D();
                    } while (i == this.b);
                    this.d = i;
                    return;
                }
                throw InvalidProtocolBufferException.invalidWireType();
            }
            else {
                final int e = this.a.E();
                this.X(e);
                do {
                    p.b(this.a.u());
                } while (this.a.d() < this.a.d() + e);
            }
        }
        else {
            final int b2 = WireFormat.b(this.b);
            if (b2 != 2) {
                if (b2 == 5) {
                    int j;
                    do {
                        list.add(this.a.u());
                        if (this.a.e()) {
                            return;
                        }
                        j = this.a.D();
                    } while (j == this.b);
                    this.d = j;
                    return;
                }
                throw InvalidProtocolBufferException.invalidWireType();
            }
            else {
                final int e2 = this.a.E();
                this.X(e2);
                do {
                    list.add(this.a.u());
                } while (this.a.d() < this.a.d() + e2);
            }
        }
    }
    
    @Override
    public boolean p() {
        if (!this.a.e()) {
            final int b = this.b;
            if (b != this.c) {
                return this.a.H(b);
            }
        }
        return false;
    }
    
    @Override
    public void q(final List list) {
        if (WireFormat.b(this.b) == 2) {
            int i;
            do {
                list.add(this.g());
                if (this.a.e()) {
                    return;
                }
                i = this.a.D();
            } while (i == this.b);
            this.d = i;
            return;
        }
        throw InvalidProtocolBufferException.invalidWireType();
    }
    
    @Override
    public void r(final List list) {
        if (list instanceof j) {
            final j j = (j)list;
            final int b = WireFormat.b(this.b);
            if (b == 1) {
                int i;
                do {
                    j.b(this.a.q());
                    if (this.a.e()) {
                        return;
                    }
                    i = this.a.D();
                } while (i == this.b);
                this.d = i;
                return;
            }
            if (b != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            final int e = this.a.E();
            this.Y(e);
            do {
                j.b(this.a.q());
            } while (this.a.d() < this.a.d() + e);
        }
        else {
            final int b2 = WireFormat.b(this.b);
            if (b2 == 1) {
                int k;
                do {
                    list.add(this.a.q());
                    if (this.a.e()) {
                        return;
                    }
                    k = this.a.D();
                } while (k == this.b);
                this.d = k;
                return;
            }
            if (b2 != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            final int e2 = this.a.E();
            this.Y(e2);
            do {
                list.add(this.a.q());
            } while (this.a.d() < this.a.d() + e2);
        }
    }
    
    @Override
    public double readDouble() {
        this.W(1);
        return this.a.q();
    }
    
    @Override
    public float readFloat() {
        this.W(5);
        return this.a.u();
    }
    
    @Override
    public long s() {
        this.W(0);
        return this.a.w();
    }
    
    @Override
    public long t() {
        this.W(1);
        return this.a.t();
    }
    
    @Override
    public void u(final List list) {
        if (list instanceof s) {
            final s s = (s)list;
            final int b = WireFormat.b(this.b);
            if (b != 2) {
                if (b == 5) {
                    int i;
                    do {
                        s.g(this.a.x());
                        if (this.a.e()) {
                            return;
                        }
                        i = this.a.D();
                    } while (i == this.b);
                    this.d = i;
                    return;
                }
                throw InvalidProtocolBufferException.invalidWireType();
            }
            else {
                final int e = this.a.E();
                this.X(e);
                do {
                    s.g(this.a.x());
                } while (this.a.d() < this.a.d() + e);
            }
        }
        else {
            final int b2 = WireFormat.b(this.b);
            if (b2 != 2) {
                if (b2 == 5) {
                    int j;
                    do {
                        list.add(this.a.x());
                        if (this.a.e()) {
                            return;
                        }
                        j = this.a.D();
                    } while (j == this.b);
                    this.d = j;
                    return;
                }
                throw InvalidProtocolBufferException.invalidWireType();
            }
            else {
                final int e2 = this.a.E();
                this.X(e2);
                do {
                    list.add(this.a.x());
                } while (this.a.d() < this.a.d() + e2);
            }
        }
    }
    
    @Override
    public boolean v() {
        this.W(0);
        return this.a.o();
    }
    
    @Override
    public void w(final List list) {
        int n;
        if (list instanceof w) {
            final w w = (w)list;
            final int b = WireFormat.b(this.b);
            if (b == 0) {
                int i;
                do {
                    w.b(this.a.F());
                    if (this.a.e()) {
                        return;
                    }
                    i = this.a.D();
                } while (i == this.b);
                this.d = i;
                return;
            }
            if (b != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            n = this.a.d() + this.a.E();
            do {
                w.b(this.a.F());
            } while (this.a.d() < n);
        }
        else {
            final int b2 = WireFormat.b(this.b);
            if (b2 == 0) {
                int j;
                do {
                    list.add(this.a.F());
                    if (this.a.e()) {
                        return;
                    }
                    j = this.a.D();
                } while (j == this.b);
                this.d = j;
                return;
            }
            if (b2 != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            n = this.a.d() + this.a.E();
            do {
                list.add(this.a.F());
            } while (this.a.d() < n);
        }
        this.V(n);
    }
    
    @Override
    public void x(final List list) {
        int n;
        if (list instanceof w) {
            final w w = (w)list;
            final int b = WireFormat.b(this.b);
            if (b == 0) {
                int i;
                do {
                    w.b(this.a.w());
                    if (this.a.e()) {
                        return;
                    }
                    i = this.a.D();
                } while (i == this.b);
                this.d = i;
                return;
            }
            if (b != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            n = this.a.d() + this.a.E();
            do {
                w.b(this.a.w());
            } while (this.a.d() < n);
        }
        else {
            final int b2 = WireFormat.b(this.b);
            if (b2 == 0) {
                int j;
                do {
                    list.add(this.a.w());
                    if (this.a.e()) {
                        return;
                    }
                    j = this.a.D();
                } while (j == this.b);
                this.d = j;
                return;
            }
            if (b2 != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            n = this.a.d() + this.a.E();
            do {
                list.add(this.a.w());
            } while (this.a.d() < n);
        }
        this.V(n);
    }
    
    @Override
    public void y(final List list) {
        int n;
        if (list instanceof s) {
            final s s = (s)list;
            final int b = WireFormat.b(this.b);
            if (b == 0) {
                int i;
                do {
                    s.g(this.a.r());
                    if (this.a.e()) {
                        return;
                    }
                    i = this.a.D();
                } while (i == this.b);
                this.d = i;
                return;
            }
            if (b != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            n = this.a.d() + this.a.E();
            do {
                s.g(this.a.r());
            } while (this.a.d() < n);
        }
        else {
            final int b2 = WireFormat.b(this.b);
            if (b2 == 0) {
                int j;
                do {
                    list.add(this.a.r());
                    if (this.a.e()) {
                        return;
                    }
                    j = this.a.D();
                } while (j == this.b);
                this.d = j;
                return;
            }
            if (b2 != 2) {
                throw InvalidProtocolBufferException.invalidWireType();
            }
            n = this.a.d() + this.a.E();
            do {
                list.add(this.a.r());
            } while (this.a.d() < n);
        }
        this.V(n);
    }
    
    @Override
    public void z(final List list) {
        this.U(list, true);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

public enum DescriptorProtos$FieldDescriptorProto$Label implements a
{
    private static final DescriptorProtos$FieldDescriptorProto$Label[] $VALUES;
    
    LABEL_OPTIONAL(1);
    
    public static final int LABEL_OPTIONAL_VALUE = 1;
    
    LABEL_REPEATED(3);
    
    public static final int LABEL_REPEATED_VALUE = 3;
    
    LABEL_REQUIRED(2);
    
    public static final int LABEL_REQUIRED_VALUE = 2;
    private static final t.b internalValueMap;
    private final int value;
    
    static {
        internalValueMap = new t.b() {};
    }
    
    private DescriptorProtos$FieldDescriptorProto$Label(final int value) {
        this.value = value;
    }
    
    public static DescriptorProtos$FieldDescriptorProto$Label forNumber(final int n) {
        if (n == 1) {
            return DescriptorProtos$FieldDescriptorProto$Label.LABEL_OPTIONAL;
        }
        if (n == 2) {
            return DescriptorProtos$FieldDescriptorProto$Label.LABEL_REQUIRED;
        }
        if (n != 3) {
            return null;
        }
        return DescriptorProtos$FieldDescriptorProto$Label.LABEL_REPEATED;
    }
    
    public static t.b internalGetValueMap() {
        return DescriptorProtos$FieldDescriptorProto$Label.internalValueMap;
    }
    
    public static c internalGetVerifier() {
        return b.a;
    }
    
    @Deprecated
    public static DescriptorProtos$FieldDescriptorProto$Label valueOf(final int n) {
        return forNumber(n);
    }
    
    @Override
    public final int getNumber() {
        return this.value;
    }
    
    public static final class b implements c
    {
        public static final c a;
        
        static {
            a = new b();
        }
        
        @Override
        public boolean a(final int n) {
            return DescriptorProtos$FieldDescriptorProto$Label.forNumber(n) != null;
        }
    }
}

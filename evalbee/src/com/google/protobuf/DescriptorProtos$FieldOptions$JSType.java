// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

public enum DescriptorProtos$FieldOptions$JSType implements a
{
    private static final DescriptorProtos$FieldOptions$JSType[] $VALUES;
    
    JS_NORMAL(0);
    
    public static final int JS_NORMAL_VALUE = 0;
    
    JS_NUMBER(2);
    
    public static final int JS_NUMBER_VALUE = 2;
    
    JS_STRING(1);
    
    public static final int JS_STRING_VALUE = 1;
    private static final t.b internalValueMap;
    private final int value;
    
    static {
        internalValueMap = new t.b() {};
    }
    
    private DescriptorProtos$FieldOptions$JSType(final int value) {
        this.value = value;
    }
    
    public static DescriptorProtos$FieldOptions$JSType forNumber(final int n) {
        if (n == 0) {
            return DescriptorProtos$FieldOptions$JSType.JS_NORMAL;
        }
        if (n == 1) {
            return DescriptorProtos$FieldOptions$JSType.JS_STRING;
        }
        if (n != 2) {
            return null;
        }
        return DescriptorProtos$FieldOptions$JSType.JS_NUMBER;
    }
    
    public static t.b internalGetValueMap() {
        return DescriptorProtos$FieldOptions$JSType.internalValueMap;
    }
    
    public static c internalGetVerifier() {
        return b.a;
    }
    
    @Deprecated
    public static DescriptorProtos$FieldOptions$JSType valueOf(final int n) {
        return forNumber(n);
    }
    
    @Override
    public final int getNumber() {
        return this.value;
    }
    
    public static final class b implements c
    {
        public static final c a;
        
        static {
            a = new b();
        }
        
        @Override
        public boolean a(final int n) {
            return DescriptorProtos$FieldOptions$JSType.forNumber(n) != null;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

public enum DescriptorProtos$MethodOptions$IdempotencyLevel implements a
{
    private static final DescriptorProtos$MethodOptions$IdempotencyLevel[] $VALUES;
    
    IDEMPOTENCY_UNKNOWN(0);
    
    public static final int IDEMPOTENCY_UNKNOWN_VALUE = 0;
    
    IDEMPOTENT(2);
    
    public static final int IDEMPOTENT_VALUE = 2;
    
    NO_SIDE_EFFECTS(1);
    
    public static final int NO_SIDE_EFFECTS_VALUE = 1;
    private static final t.b internalValueMap;
    private final int value;
    
    static {
        internalValueMap = new t.b() {};
    }
    
    private DescriptorProtos$MethodOptions$IdempotencyLevel(final int value) {
        this.value = value;
    }
    
    public static DescriptorProtos$MethodOptions$IdempotencyLevel forNumber(final int n) {
        if (n == 0) {
            return DescriptorProtos$MethodOptions$IdempotencyLevel.IDEMPOTENCY_UNKNOWN;
        }
        if (n == 1) {
            return DescriptorProtos$MethodOptions$IdempotencyLevel.NO_SIDE_EFFECTS;
        }
        if (n != 2) {
            return null;
        }
        return DescriptorProtos$MethodOptions$IdempotencyLevel.IDEMPOTENT;
    }
    
    public static t.b internalGetValueMap() {
        return DescriptorProtos$MethodOptions$IdempotencyLevel.internalValueMap;
    }
    
    public static c internalGetVerifier() {
        return b.a;
    }
    
    @Deprecated
    public static DescriptorProtos$MethodOptions$IdempotencyLevel valueOf(final int n) {
        return forNumber(n);
    }
    
    @Override
    public final int getNumber() {
        return this.value;
    }
    
    public static final class b implements c
    {
        public static final c a;
        
        static {
            a = new b();
        }
        
        @Override
        public boolean a(final int n) {
            return DescriptorProtos$MethodOptions$IdempotencyLevel.forNumber(n) != null;
        }
    }
}

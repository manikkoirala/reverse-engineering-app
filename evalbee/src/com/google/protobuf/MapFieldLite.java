// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

import java.util.Collections;
import java.util.Set;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.LinkedHashMap;

public final class MapFieldLite<K, V> extends LinkedHashMap<K, V>
{
    private static final MapFieldLite<?, ?> EMPTY_MAP_FIELD;
    private boolean isMutable;
    
    static {
        (EMPTY_MAP_FIELD = new MapFieldLite<Object, Object>()).makeImmutable();
    }
    
    private MapFieldLite() {
        this.isMutable = true;
    }
    
    private MapFieldLite(final Map<K, V> m) {
        super(m);
        this.isMutable = true;
    }
    
    public static <K, V> int calculateHashCodeForMap(final Map<K, V> map) {
        final Iterator<Map.Entry<K, V>> iterator = map.entrySet().iterator();
        int n = 0;
        while (iterator.hasNext()) {
            final Map.Entry entry = iterator.next();
            n += (calculateHashCodeForObject(entry.getValue()) ^ calculateHashCodeForObject(entry.getKey()));
        }
        return n;
    }
    
    private static int calculateHashCodeForObject(final Object o) {
        if (o instanceof byte[]) {
            return t.d((byte[])o);
        }
        if (!(o instanceof t.a)) {
            return o.hashCode();
        }
        throw new UnsupportedOperationException();
    }
    
    private static void checkForNullKeysAndValues(final Map<?, ?> map) {
        for (final Object next : map.keySet()) {
            t.a(next);
            t.a(map.get(next));
        }
    }
    
    private static Object copy(final Object o) {
        Object copy = o;
        if (o instanceof byte[]) {
            final byte[] original = (byte[])o;
            copy = Arrays.copyOf(original, original.length);
        }
        return copy;
    }
    
    public static <K, V> Map<K, V> copy(final Map<K, V> map) {
        final LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (final Map.Entry<Object, V> entry : map.entrySet()) {
            linkedHashMap.put(entry.getKey(), copy(entry.getValue()));
        }
        return linkedHashMap;
    }
    
    public static <K, V> MapFieldLite<K, V> emptyMapField() {
        return (MapFieldLite<K, V>)MapFieldLite.EMPTY_MAP_FIELD;
    }
    
    private void ensureMutable() {
        if (this.isMutable()) {
            return;
        }
        throw new UnsupportedOperationException();
    }
    
    private static boolean equals(final Object o, final Object obj) {
        if (o instanceof byte[] && obj instanceof byte[]) {
            return Arrays.equals((byte[])o, (byte[])obj);
        }
        return o.equals(obj);
    }
    
    public static <K, V> boolean equals(final Map<K, V> map, final Map<K, V> map2) {
        if (map == map2) {
            return true;
        }
        if (map.size() != map2.size()) {
            return false;
        }
        for (final Map.Entry<Object, V> entry : map.entrySet()) {
            if (!map2.containsKey(entry.getKey())) {
                return false;
            }
            if (!equals(entry.getValue(), map2.get(entry.getKey()))) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public void clear() {
        this.ensureMutable();
        super.clear();
    }
    
    @Override
    public Set<Map.Entry<K, V>> entrySet() {
        Object o;
        if (this.isEmpty()) {
            o = Collections.emptySet();
        }
        else {
            o = super.entrySet();
        }
        return (Set<Map.Entry<K, V>>)o;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof Map && equals((Map<Object, Object>)this, (Map<Object, Object>)o);
    }
    
    @Override
    public int hashCode() {
        return calculateHashCodeForMap((Map<Object, Object>)this);
    }
    
    public boolean isMutable() {
        return this.isMutable;
    }
    
    public void makeImmutable() {
        this.isMutable = false;
    }
    
    public void mergeFrom(final MapFieldLite<K, V> mapFieldLite) {
        this.ensureMutable();
        if (!mapFieldLite.isEmpty()) {
            this.putAll((Map<? extends K, ? extends V>)mapFieldLite);
        }
    }
    
    public MapFieldLite<K, V> mutableCopy() {
        MapFieldLite mapFieldLite;
        if (this.isEmpty()) {
            mapFieldLite = new MapFieldLite();
        }
        else {
            mapFieldLite = new MapFieldLite(this);
        }
        return mapFieldLite;
    }
    
    @Override
    public V put(final K key, final V value) {
        this.ensureMutable();
        t.a(key);
        t.a(value);
        return super.put(key, value);
    }
    
    public V put(final Map.Entry<K, V> entry) {
        return this.put(entry.getKey(), entry.getValue());
    }
    
    @Override
    public void putAll(final Map<? extends K, ? extends V> m) {
        this.ensureMutable();
        checkForNullKeysAndValues(m);
        super.putAll(m);
    }
    
    @Override
    public V remove(final Object key) {
        this.ensureMutable();
        return super.remove(key);
    }
}

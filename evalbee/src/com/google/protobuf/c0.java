// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

import java.io.IOException;
import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;
import java.util.Arrays;
import java.lang.reflect.Field;
import java.util.List;
import sun.misc.Unsafe;

public final class c0 implements g0
{
    public static final int[] r;
    public static final Unsafe s;
    public final int[] a;
    public final Object[] b;
    public final int c;
    public final int d;
    public final a0 e;
    public final boolean f;
    public final boolean g;
    public final boolean h;
    public final boolean i;
    public final int[] j;
    public final int k;
    public final int l;
    public final ez0 m;
    public final v n;
    public final k0 o;
    public final m p;
    public final y q;
    
    static {
        r = new int[0];
        s = d12.H();
    }
    
    public c0(final int[] a, final Object[] b, final int c, final int d, final a0 e, final boolean h, final boolean i, final int[] j, final int k, final int l, final ez0 m, final v n, final k0 o, final m p15, final y q) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.g = (e instanceof GeneratedMessageLite);
        this.h = h;
        this.f = (p15 != null && p15.e(e));
        this.i = i;
        this.j = j;
        this.k = k;
        this.l = l;
        this.m = m;
        this.n = n;
        this.o = o;
        this.p = p15;
        this.e = e;
        this.q = q;
    }
    
    public static int A(final Object o, final long n) {
        return d12.C(o, n);
    }
    
    public static boolean B(final int n) {
        return (n & 0x20000000) != 0x0;
    }
    
    public static boolean E(final Object o, final int n, final g0 g0) {
        return g0.b(d12.G(o, Y(n)));
    }
    
    public static boolean H(final Object o) {
        return o != null && (!(o instanceof GeneratedMessageLite) || ((GeneratedMessageLite)o).I());
    }
    
    public static boolean K(final int n) {
        return (n & 0x10000000) != 0x0;
    }
    
    public static List L(final Object o, final long n) {
        return (List)d12.G(o, n);
    }
    
    public static long M(final Object o, final long n) {
        return d12.E(o, n);
    }
    
    public static c0 U(final Class clazz, final tv0 tv0, final ez0 ez0, final v v, final k0 k0, final m m, final y y) {
        if (tv0 instanceof kc1) {
            return W((kc1)tv0, ez0, v, k0, m, y);
        }
        zu0.a(tv0);
        return V(null, ez0, v, k0, m, y);
    }
    
    public static c0 V(final tr1 tr1, final ez0 ez0, final v v, final k0 k0, final m m, final y y) {
        throw null;
    }
    
    public static c0 W(final kc1 kc1, final ez0 ez0, final v v, final k0 k0, final m m, final y y) {
        final boolean b = kc1.c() == ProtoSyntax.PROTO3;
        final String e = kc1.e();
        final int length = e.length();
        int index2;
        if (e.charAt(0) >= '\ud800') {
            int index = 1;
            while (true) {
                final int n = index2 = index + 1;
                if (e.charAt(index) < '\ud800') {
                    break;
                }
                index = n;
            }
        }
        else {
            index2 = 1;
        }
        final int n2 = index2 + 1;
        final char char1 = e.charAt(index2);
        int i = n2;
        int n3;
        if ((n3 = char1) >= 55296) {
            int n4 = char1 & '\u1fff';
            int n5 = 13;
            int index3 = n2;
            int n6;
            char char2;
            while (true) {
                n6 = index3 + 1;
                char2 = e.charAt(index3);
                if (char2 < '\ud800') {
                    break;
                }
                n4 |= (char2 & '\u1fff') << n5;
                n5 += 13;
                index3 = n6;
            }
            n3 = (n4 | char2 << n5);
            i = n6;
        }
        int[] r;
        int n7;
        int n8;
        int char3;
        int n12;
        int n11;
        int n13;
        int n14;
        if (n3 == 0) {
            r = c0.r;
            n7 = 0;
            final int n9;
            n8 = (n9 = 0);
            final int n10 = char3 = n9;
            n11 = (n12 = char3);
            n13 = n9;
            n14 = n10;
        }
        else {
            final int n15 = i + 1;
            int char4;
            final char c = (char)(char4 = e.charAt(i));
            int index4 = n15;
            if (c >= '\ud800') {
                int n16 = c & '\u1fff';
                int n17 = 13;
                int index5 = n15;
                int n18;
                char char5;
                while (true) {
                    n18 = index5 + 1;
                    char5 = e.charAt(index5);
                    if (char5 < '\ud800') {
                        break;
                    }
                    n16 |= (char5 & '\u1fff') << n17;
                    n17 += 13;
                    index5 = n18;
                }
                char4 = (n16 | char5 << n17);
                index4 = n18;
            }
            final int n19 = index4 + 1;
            int char6;
            final char c2 = (char)(char6 = e.charAt(index4));
            int index6 = n19;
            if (c2 >= '\ud800') {
                int n20 = c2 & '\u1fff';
                int n21 = 13;
                int index7 = n19;
                int n22;
                char char7;
                while (true) {
                    n22 = index7 + 1;
                    char7 = e.charAt(index7);
                    if (char7 < '\ud800') {
                        break;
                    }
                    n20 |= (char7 & '\u1fff') << n21;
                    n21 += 13;
                    index7 = n22;
                }
                char6 = (n20 | char7 << n21);
                index6 = n22;
            }
            final int n23 = index6 + 1;
            int char8;
            final char c3 = (char)(char8 = e.charAt(index6));
            int index8 = n23;
            if (c3 >= '\ud800') {
                final int n24 = c3 & '\u1fff';
                int n25 = 13;
                int index9 = n23;
                int n26 = n24;
                int n27;
                char char9;
                while (true) {
                    n27 = index9 + 1;
                    char9 = e.charAt(index9);
                    if (char9 < '\ud800') {
                        break;
                    }
                    n26 |= (char9 & '\u1fff') << n25;
                    n25 += 13;
                    index9 = n27;
                }
                final int n28 = n26 | char9 << n25;
                index8 = n27;
                char8 = n28;
            }
            final int n29 = index8 + 1;
            int char10;
            final char c4 = (char)(char10 = e.charAt(index8));
            int index10 = n29;
            if (c4 >= '\ud800') {
                int n30 = c4 & '\u1fff';
                int n31 = 13;
                int index11 = n29;
                int n32;
                char char11;
                while (true) {
                    n32 = index11 + 1;
                    char11 = e.charAt(index11);
                    if (char11 < '\ud800') {
                        break;
                    }
                    n30 |= (char11 & '\u1fff') << n31;
                    n31 += 13;
                    index11 = n32;
                }
                char10 = (n30 | char11 << n31);
                index10 = n32;
            }
            final int n33 = index10 + 1;
            int char12;
            final char c5 = (char)(char12 = e.charAt(index10));
            int index12 = n33;
            if (c5 >= '\ud800') {
                int n34 = c5 & '\u1fff';
                int n35 = 13;
                int index13 = n33;
                int n36;
                char char13;
                while (true) {
                    n36 = index13 + 1;
                    char13 = e.charAt(index13);
                    if (char13 < '\ud800') {
                        break;
                    }
                    n34 |= (char13 & '\u1fff') << n35;
                    n35 += 13;
                    index13 = n36;
                }
                char12 = (n34 | char13 << n35);
                index12 = n36;
            }
            final int n37 = index12 + 1;
            int char14;
            final char c6 = (char)(char14 = e.charAt(index12));
            int index14 = n37;
            if (c6 >= '\ud800') {
                final int n38 = c6 & '\u1fff';
                int n39 = 13;
                int index15 = n37;
                int n40 = n38;
                int n41;
                char char15;
                while (true) {
                    n41 = index15 + 1;
                    char15 = e.charAt(index15);
                    if (char15 < '\ud800') {
                        break;
                    }
                    n40 |= (char15 & '\u1fff') << n39;
                    n39 += 13;
                    index15 = n41;
                }
                final int n42 = n40 | char15 << n39;
                index14 = n41;
                char14 = n42;
            }
            final int n43 = index14 + 1;
            int char16;
            final char c7 = (char)(char16 = e.charAt(index14));
            int index16 = n43;
            if (c7 >= '\ud800') {
                int n44 = c7 & '\u1fff';
                int n45 = 13;
                int index17 = n43;
                int n46;
                char char17;
                while (true) {
                    n46 = index17 + 1;
                    char17 = e.charAt(index17);
                    if (char17 < '\ud800') {
                        break;
                    }
                    n44 |= (char17 & '\u1fff') << n45;
                    n45 += 13;
                    index17 = n46;
                }
                char16 = (n44 | char17 << n45);
                index16 = n46;
            }
            final int n47 = index16 + 1;
            final char c8 = (char)(char3 = e.charAt(index16));
            int n48 = n47;
            if (c8 >= '\ud800') {
                final int n49 = c8 & '\u1fff';
                int n50 = 13;
                int index18 = n47;
                int n51 = n49;
                char char18;
                while (true) {
                    n48 = index18 + 1;
                    char18 = e.charAt(index18);
                    if (char18 < '\ud800') {
                        break;
                    }
                    n51 |= (char18 & '\u1fff') << n50;
                    n50 += 13;
                    index18 = n48;
                }
                char3 = (n51 | char18 << n50);
            }
            r = new int[char3 + char14 + char16];
            final int n52 = char4 * 2 + char6;
            n12 = char4;
            final int n53 = n48;
            n11 = n52;
            n14 = char14;
            n13 = char12;
            n8 = char10;
            n7 = char8;
            i = n53;
        }
        final Unsafe s = c0.s;
        final Object[] d = kc1.d();
        final Class<? extends a0> class1 = kc1.b().getClass();
        final int[] array = new int[n13 * 3];
        final Object[] array2 = new Object[n13 * 2];
        final int n54 = char3 + n14;
        int n55 = char3;
        int n56 = n54;
        final int n57 = 0;
        int n58 = 0;
        int n59 = n11;
        final int n60 = char3;
        int n61 = n57;
        final int n62 = n8;
        final int n63 = n7;
        int n71;
        int n74;
        int n113;
        for (int n64 = length; i < n64; n64 = n71, i = n113, n61 = n74) {
            int index19 = i + 1;
            int char19 = e.charAt(i);
            int index20;
            if (char19 >= 55296) {
                int n65 = char19 & 0x1FFF;
                int n66 = 13;
                int n67;
                char char20;
                while (true) {
                    n67 = index19 + 1;
                    char20 = e.charAt(index19);
                    if (char20 < '\ud800') {
                        break;
                    }
                    n65 |= (char20 & '\u1fff') << n66;
                    n66 += 13;
                    index19 = n67;
                }
                char19 = (n65 | char20 << n66);
                index20 = n67;
            }
            else {
                index20 = index19;
            }
            int index21 = index20 + 1;
            int char21 = e.charAt(index20);
            int n72;
            if (char21 >= 55296) {
                int n68 = char21 & 0x1FFF;
                int n69 = 13;
                int n70;
                char char22;
                while (true) {
                    n70 = index21 + 1;
                    char22 = e.charAt(index21);
                    n71 = n64;
                    if (char22 < '\ud800') {
                        break;
                    }
                    n68 |= (char22 & '\u1fff') << n69;
                    n69 += 13;
                    n64 = n71;
                    index21 = n70;
                }
                char21 = (n68 | char22 << n69);
                n72 = n70;
            }
            else {
                n72 = index21;
                n71 = n64;
            }
            final int n73 = char21 & 0xFF;
            n74 = n61;
            if ((char21 & 0x400) != 0x0) {
                r[n61] = n58;
                n74 = n61 + 1;
            }
            int n76;
            int n88;
            int n89;
            int n90;
            if (n73 >= 51) {
                final int n75 = n72 + 1;
                int char23;
                final char c9 = (char)(char23 = e.charAt(n72));
                n76 = n75;
                if (c9 >= '\ud800') {
                    int n77 = c9 & '\u1fff';
                    int n78 = 13;
                    int index22 = n75;
                    int n79;
                    char char24;
                    while (true) {
                        n79 = index22 + 1;
                        char24 = e.charAt(index22);
                        if (char24 < '\ud800') {
                            break;
                        }
                        n77 |= (char24 & '\u1fff') << n78;
                        n78 += 13;
                        index22 = n79;
                    }
                    char23 = (n77 | char24 << n78);
                    n76 = n79;
                }
                final int n80 = n73 - 51;
                int n81 = 0;
                Label_1709: {
                    int n84;
                    if (n80 != 9 && n80 != 17) {
                        n81 = n59;
                        if (n80 != 12) {
                            break Label_1709;
                        }
                        n81 = n59;
                        if (b) {
                            break Label_1709;
                        }
                        final int n82 = n58 / 3;
                        final int n83 = n59 + 1;
                        array2[n82 * 2 + 1] = d[n59];
                        n84 = n83;
                    }
                    else {
                        final int n85 = n58 / 3;
                        final int n86 = n59 + 1;
                        array2[n85 * 2 + 1] = d[n59];
                        n84 = n86;
                    }
                    n81 = n84;
                }
                int n87 = char23 * 2;
                final Object o = d[n87];
                Field q0;
                if (o instanceof Field) {
                    q0 = (Field)o;
                }
                else {
                    q0 = q0(class1, (String)o);
                    d[n87] = q0;
                }
                n88 = (int)s.objectFieldOffset(q0);
                ++n87;
                final Object o2 = d[n87];
                Field q2;
                if (o2 instanceof Field) {
                    q2 = (Field)o2;
                }
                else {
                    q2 = q0(class1, (String)o2);
                    d[n87] = q2;
                }
                n89 = (int)s.objectFieldOffset(q2);
                n90 = 0;
                n59 = n81;
            }
            else {
                final int n91 = n59 + 1;
                final Field q3 = q0(class1, (String)d[n59]);
                int n96 = 0;
                Label_2107: {
                    int n92 = 0;
                    Label_2103: {
                        if (n73 != 9 && n73 != 17) {
                            Label_2080: {
                                if (n73 != 27 && n73 != 49) {
                                    int n93;
                                    if (n73 != 12 && n73 != 30 && n73 != 44) {
                                        n92 = n91;
                                        n93 = n55;
                                        if (n73 == 50) {
                                            n93 = n55 + 1;
                                            r[n55] = n58;
                                            final int n94 = n58 / 3 * 2;
                                            final int n95 = n91 + 1;
                                            array2[n94] = d[n91];
                                            if ((char21 & 0x800) == 0x0) {
                                                n55 = n93;
                                                n96 = n95;
                                                break Label_2107;
                                            }
                                            n92 = n95 + 1;
                                            array2[n94 + 1] = d[n95];
                                        }
                                    }
                                    else {
                                        n92 = n91;
                                        n93 = n55;
                                        if (!b) {
                                            final int n97 = n58 / 3;
                                            n96 = n91 + 1;
                                            array2[n97 * 2 + 1] = d[n91];
                                            break Label_2080;
                                        }
                                    }
                                    n55 = n93;
                                    break Label_2103;
                                }
                                final int n98 = n58 / 3;
                                n96 = n91 + 1;
                                array2[n98 * 2 + 1] = d[n91];
                            }
                            break Label_2107;
                        }
                        array2[n58 / 3 * 2 + 1] = q3.getType();
                        n92 = n91;
                    }
                    n96 = n92;
                }
                final int n99 = (int)s.objectFieldOffset(q3);
                int n100;
                int n104;
                int n105;
                if ((char21 & 0x1000) == 0x1000 && n73 <= 17) {
                    n100 = n72 + 1;
                    int char25 = e.charAt(n72);
                    if (char25 >= 55296) {
                        int n101 = char25 & 0x1FFF;
                        int n102 = 13;
                        int index23 = n100;
                        char char26;
                        while (true) {
                            n100 = index23 + 1;
                            char26 = e.charAt(index23);
                            if (char26 < '\ud800') {
                                break;
                            }
                            n101 |= (char26 & '\u1fff') << n102;
                            n102 += 13;
                            index23 = n100;
                        }
                        char25 = (n101 | char26 << n102);
                    }
                    final int n103 = n12 * 2 + char25 / 32;
                    final Object o3 = d[n103];
                    Field q4;
                    if (o3 instanceof Field) {
                        q4 = (Field)o3;
                    }
                    else {
                        q4 = q0(class1, (String)o3);
                        d[n103] = q4;
                    }
                    n104 = (int)s.objectFieldOffset(q4);
                    n105 = char25 % 32;
                }
                else {
                    final int n106 = 1048575;
                    n100 = n72;
                    n105 = 0;
                    n104 = n106;
                }
                int n107 = n56;
                if (n73 >= 18) {
                    n107 = n56;
                    if (n73 <= 49) {
                        r[n56] = n99;
                        n107 = n56 + 1;
                    }
                }
                final int n108 = n96;
                n76 = n100;
                n56 = n107;
                n59 = n108;
                n89 = n104;
                n88 = n99;
                n90 = n105;
            }
            final int n109 = n58 + 1;
            array[n58] = char19;
            final int n110 = n109 + 1;
            int n111;
            if ((char21 & 0x200) != 0x0) {
                n111 = 536870912;
            }
            else {
                n111 = 0;
            }
            int n112;
            if ((char21 & 0x100) != 0x0) {
                n112 = 268435456;
            }
            else {
                n112 = 0;
            }
            array[n109] = (n112 | n111 | n73 << 20 | n88);
            n58 = n110 + 1;
            array[n110] = (n90 << 20 | n89);
            n113 = n76;
        }
        return new c0(array, array2, n63, n62, kc1.b(), b, false, r, n60, n54, ez0, v, k0, m, y);
    }
    
    public static long Y(final int n) {
        return n & 0xFFFFF;
    }
    
    public static boolean Z(final Object o, final long n) {
        return (boolean)d12.G(o, n);
    }
    
    public static double a0(final Object o, final long n) {
        return (double)d12.G(o, n);
    }
    
    public static float b0(final Object o, final long n) {
        return (float)d12.G(o, n);
    }
    
    public static int c0(final Object o, final long n) {
        return (int)d12.G(o, n);
    }
    
    public static long d0(final Object o, final long n) {
        return (long)d12.G(o, n);
    }
    
    public static boolean k(final Object o, final long n) {
        return d12.t(o, n);
    }
    
    public static void l(final Object obj) {
        if (H(obj)) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Mutating immutable message: ");
        sb.append(obj);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static double o(final Object o, final long n) {
        return d12.A(o, n);
    }
    
    public static Field q0(final Class clazz, final String s) {
        try {
            return clazz.getDeclaredField(s);
        }
        catch (final NoSuchFieldException ex) {
            final Field[] declaredFields = clazz.getDeclaredFields();
            for (final Field field : declaredFields) {
                if (s.equals(field.getName())) {
                    return field;
                }
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Field ");
            sb.append(s);
            sb.append(" for ");
            sb.append(clazz.getName());
            sb.append(" not found. Known fields are ");
            sb.append(Arrays.toString(declaredFields));
            throw new RuntimeException(sb.toString());
        }
    }
    
    public static float s(final Object o, final long n) {
        return d12.B(o, n);
    }
    
    public static l0 w(final Object o) {
        final GeneratedMessageLite generatedMessageLite = (GeneratedMessageLite)o;
        l0 unknownFields;
        if ((unknownFields = generatedMessageLite.unknownFields) == l0.c()) {
            unknownFields = l0.k();
            generatedMessageLite.unknownFields = unknownFields;
        }
        return unknownFields;
    }
    
    public static int w0(final int n) {
        return (n & 0xFF00000) >>> 20;
    }
    
    public final void A0(final Object o, final Writer writer) {
        this.D0(this.o, o, writer);
        Iterator e = null;
        Map.Entry entry = null;
        Label_0063: {
            if (this.f) {
                final o c = this.p.c(o);
                if (!c.j()) {
                    e = c.e();
                    entry = (Map.Entry)e.next();
                    break Label_0063;
                }
            }
            e = null;
            entry = null;
        }
        int n = this.a.length - 3;
        Map.Entry entry2;
        while (true) {
            entry2 = entry;
            if (n < 0) {
                break;
            }
            final int x0 = this.x0(n);
            final int x2 = this.X(n);
            while (entry != null && this.p.a(entry) > x2) {
                this.p.j(writer, entry);
                if (e.hasNext()) {
                    entry = (Map.Entry)e.next();
                }
                else {
                    entry = null;
                }
            }
            Label_2344: {
                double n14 = 0.0;
                Label_2335: {
                    float n13 = 0.0f;
                    Label_2302: {
                        long n12 = 0L;
                        Label_2268: {
                            long n11 = 0L;
                            Label_2234: {
                                int n10 = 0;
                                Label_2200: {
                                    long n9 = 0L;
                                    Label_2166: {
                                        int n8 = 0;
                                        Label_2132: {
                                            boolean b = false;
                                            Label_2098: {
                                                Label_2058: {
                                                    Label_2022: {
                                                        Label_1989: {
                                                            int n7 = 0;
                                                            Label_1966: {
                                                                int n6 = 0;
                                                                Label_1932: {
                                                                    int n5 = 0;
                                                                    Label_1898: {
                                                                        long n4 = 0L;
                                                                        Label_1864: {
                                                                            int n3 = 0;
                                                                            Label_1830: {
                                                                                long n2 = 0L;
                                                                                Label_1796: {
                                                                                    switch (w0(x0)) {
                                                                                        default: {
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 68: {
                                                                                            if (this.J(o, x2, n)) {
                                                                                                break;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 67: {
                                                                                            if (this.J(o, x2, n)) {
                                                                                                n2 = d0(o, Y(x0));
                                                                                                break Label_1796;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 66: {
                                                                                            if (this.J(o, x2, n)) {
                                                                                                n3 = c0(o, Y(x0));
                                                                                                break Label_1830;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 65: {
                                                                                            if (this.J(o, x2, n)) {
                                                                                                n4 = d0(o, Y(x0));
                                                                                                break Label_1864;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 64: {
                                                                                            if (this.J(o, x2, n)) {
                                                                                                n5 = c0(o, Y(x0));
                                                                                                break Label_1898;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 63: {
                                                                                            if (this.J(o, x2, n)) {
                                                                                                n6 = c0(o, Y(x0));
                                                                                                break Label_1932;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 62: {
                                                                                            if (this.J(o, x2, n)) {
                                                                                                n7 = c0(o, Y(x0));
                                                                                                break Label_1966;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 61: {
                                                                                            if (this.J(o, x2, n)) {
                                                                                                break Label_1989;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 60: {
                                                                                            if (this.J(o, x2, n)) {
                                                                                                break Label_2022;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 59: {
                                                                                            if (this.J(o, x2, n)) {
                                                                                                break Label_2058;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 58: {
                                                                                            if (this.J(o, x2, n)) {
                                                                                                b = Z(o, Y(x0));
                                                                                                break Label_2098;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 57: {
                                                                                            if (this.J(o, x2, n)) {
                                                                                                n8 = c0(o, Y(x0));
                                                                                                break Label_2132;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 56: {
                                                                                            if (this.J(o, x2, n)) {
                                                                                                n9 = d0(o, Y(x0));
                                                                                                break Label_2166;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 55: {
                                                                                            if (this.J(o, x2, n)) {
                                                                                                n10 = c0(o, Y(x0));
                                                                                                break Label_2200;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 54: {
                                                                                            if (this.J(o, x2, n)) {
                                                                                                n11 = d0(o, Y(x0));
                                                                                                break Label_2234;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 53: {
                                                                                            if (this.J(o, x2, n)) {
                                                                                                n12 = d0(o, Y(x0));
                                                                                                break Label_2268;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 52: {
                                                                                            if (this.J(o, x2, n)) {
                                                                                                n13 = b0(o, Y(x0));
                                                                                                break Label_2302;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 51: {
                                                                                            if (this.J(o, x2, n)) {
                                                                                                n14 = a0(o, Y(x0));
                                                                                                break Label_2335;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 50: {
                                                                                            this.B0(writer, x2, d12.G(o, Y(x0)), n);
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 49: {
                                                                                            h0.T(this.X(n), (List)d12.G(o, Y(x0)), writer, this.v(n));
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 48: {
                                                                                            h0.a0(this.X(n), (List)d12.G(o, Y(x0)), writer, true);
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 47: {
                                                                                            h0.Z(this.X(n), (List)d12.G(o, Y(x0)), writer, true);
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 46: {
                                                                                            h0.Y(this.X(n), (List)d12.G(o, Y(x0)), writer, true);
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 45: {
                                                                                            h0.X(this.X(n), (List)d12.G(o, Y(x0)), writer, true);
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 44: {
                                                                                            h0.P(this.X(n), (List)d12.G(o, Y(x0)), writer, true);
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 43: {
                                                                                            h0.c0(this.X(n), (List)d12.G(o, Y(x0)), writer, true);
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 42: {
                                                                                            h0.M(this.X(n), (List)d12.G(o, Y(x0)), writer, true);
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 41: {
                                                                                            h0.Q(this.X(n), (List)d12.G(o, Y(x0)), writer, true);
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 40: {
                                                                                            h0.R(this.X(n), (List)d12.G(o, Y(x0)), writer, true);
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 39: {
                                                                                            h0.U(this.X(n), (List)d12.G(o, Y(x0)), writer, true);
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 38: {
                                                                                            h0.d0(this.X(n), (List)d12.G(o, Y(x0)), writer, true);
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 37: {
                                                                                            h0.V(this.X(n), (List)d12.G(o, Y(x0)), writer, true);
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 36: {
                                                                                            h0.S(this.X(n), (List)d12.G(o, Y(x0)), writer, true);
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 35: {
                                                                                            h0.O(this.X(n), (List)d12.G(o, Y(x0)), writer, true);
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 34: {
                                                                                            h0.a0(this.X(n), (List)d12.G(o, Y(x0)), writer, false);
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 33: {
                                                                                            h0.Z(this.X(n), (List)d12.G(o, Y(x0)), writer, false);
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 32: {
                                                                                            h0.Y(this.X(n), (List)d12.G(o, Y(x0)), writer, false);
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 31: {
                                                                                            h0.X(this.X(n), (List)d12.G(o, Y(x0)), writer, false);
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 30: {
                                                                                            h0.P(this.X(n), (List)d12.G(o, Y(x0)), writer, false);
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 29: {
                                                                                            h0.c0(this.X(n), (List)d12.G(o, Y(x0)), writer, false);
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 28: {
                                                                                            h0.N(this.X(n), (List)d12.G(o, Y(x0)), writer);
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 27: {
                                                                                            h0.W(this.X(n), (List)d12.G(o, Y(x0)), writer, this.v(n));
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 26: {
                                                                                            h0.b0(this.X(n), (List)d12.G(o, Y(x0)), writer);
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 25: {
                                                                                            h0.M(this.X(n), (List)d12.G(o, Y(x0)), writer, false);
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 24: {
                                                                                            h0.Q(this.X(n), (List)d12.G(o, Y(x0)), writer, false);
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 23: {
                                                                                            h0.R(this.X(n), (List)d12.G(o, Y(x0)), writer, false);
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 22: {
                                                                                            h0.U(this.X(n), (List)d12.G(o, Y(x0)), writer, false);
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 21: {
                                                                                            h0.d0(this.X(n), (List)d12.G(o, Y(x0)), writer, false);
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 20: {
                                                                                            h0.V(this.X(n), (List)d12.G(o, Y(x0)), writer, false);
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 19: {
                                                                                            h0.S(this.X(n), (List)d12.G(o, Y(x0)), writer, false);
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 18: {
                                                                                            h0.O(this.X(n), (List)d12.G(o, Y(x0)), writer, false);
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 17: {
                                                                                            if (this.C(o, n)) {
                                                                                                break;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 16: {
                                                                                            if (this.C(o, n)) {
                                                                                                n2 = M(o, Y(x0));
                                                                                                break Label_1796;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 15: {
                                                                                            if (this.C(o, n)) {
                                                                                                n3 = A(o, Y(x0));
                                                                                                break Label_1830;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 14: {
                                                                                            if (this.C(o, n)) {
                                                                                                n4 = M(o, Y(x0));
                                                                                                break Label_1864;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 13: {
                                                                                            if (this.C(o, n)) {
                                                                                                n5 = A(o, Y(x0));
                                                                                                break Label_1898;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 12: {
                                                                                            if (this.C(o, n)) {
                                                                                                n6 = A(o, Y(x0));
                                                                                                break Label_1932;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 11: {
                                                                                            if (this.C(o, n)) {
                                                                                                n7 = A(o, Y(x0));
                                                                                                break Label_1966;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 10: {
                                                                                            if (this.C(o, n)) {
                                                                                                break Label_1989;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 9: {
                                                                                            if (this.C(o, n)) {
                                                                                                break Label_2022;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 8: {
                                                                                            if (this.C(o, n)) {
                                                                                                break Label_2058;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 7: {
                                                                                            if (this.C(o, n)) {
                                                                                                b = k(o, Y(x0));
                                                                                                break Label_2098;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 6: {
                                                                                            if (this.C(o, n)) {
                                                                                                n8 = A(o, Y(x0));
                                                                                                break Label_2132;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 5: {
                                                                                            if (this.C(o, n)) {
                                                                                                n9 = M(o, Y(x0));
                                                                                                break Label_2166;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 4: {
                                                                                            if (this.C(o, n)) {
                                                                                                n10 = A(o, Y(x0));
                                                                                                break Label_2200;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 3: {
                                                                                            if (this.C(o, n)) {
                                                                                                n11 = M(o, Y(x0));
                                                                                                break Label_2234;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 2: {
                                                                                            if (this.C(o, n)) {
                                                                                                n12 = M(o, Y(x0));
                                                                                                break Label_2268;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 1: {
                                                                                            if (this.C(o, n)) {
                                                                                                n13 = s(o, Y(x0));
                                                                                                break Label_2302;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                        case 0: {
                                                                                            if (this.C(o, n)) {
                                                                                                n14 = o(o, Y(x0));
                                                                                                break Label_2335;
                                                                                            }
                                                                                            break Label_2344;
                                                                                        }
                                                                                    }
                                                                                    writer.M(x2, d12.G(o, Y(x0)), this.v(n));
                                                                                    break Label_2344;
                                                                                }
                                                                                writer.j(x2, n2);
                                                                                break Label_2344;
                                                                            }
                                                                            writer.I(x2, n3);
                                                                            break Label_2344;
                                                                        }
                                                                        writer.w(x2, n4);
                                                                        break Label_2344;
                                                                    }
                                                                    writer.o(x2, n5);
                                                                    break Label_2344;
                                                                }
                                                                writer.G(x2, n6);
                                                                break Label_2344;
                                                            }
                                                            writer.k(x2, n7);
                                                            break Label_2344;
                                                        }
                                                        writer.J(x2, (ByteString)d12.G(o, Y(x0)));
                                                        break Label_2344;
                                                    }
                                                    writer.O(x2, d12.G(o, Y(x0)), this.v(n));
                                                    break Label_2344;
                                                }
                                                this.C0(x2, d12.G(o, Y(x0)), writer);
                                                break Label_2344;
                                            }
                                            writer.n(x2, b);
                                            break Label_2344;
                                        }
                                        writer.c(x2, n8);
                                        break Label_2344;
                                    }
                                    writer.m(x2, n9);
                                    break Label_2344;
                                }
                                writer.g(x2, n10);
                                break Label_2344;
                            }
                            writer.e(x2, n11);
                            break Label_2344;
                        }
                        writer.C(x2, n12);
                        break Label_2344;
                    }
                    writer.F(x2, n13);
                    break Label_2344;
                }
                writer.z(x2, n14);
            }
            n -= 3;
        }
        while (entry2 != null) {
            this.p.j(writer, entry2);
            if (e.hasNext()) {
                entry2 = (Map.Entry)e.next();
            }
            else {
                entry2 = null;
            }
        }
    }
    
    public final void B0(final Writer writer, final int n, final Object o, final int n2) {
        if (o != null) {
            writer.K(n, this.q.b(this.u(n2)), this.q.g(o));
        }
    }
    
    public final boolean C(Object g, int x0) {
        final int l0 = this.l0(x0);
        final long n = 0xFFFFF & l0;
        final boolean b = false;
        final boolean b2 = false;
        final boolean b3 = false;
        final boolean b4 = false;
        final boolean b5 = false;
        final boolean b6 = false;
        boolean b7 = false;
        final boolean b8 = false;
        final boolean b9 = false;
        final boolean b10 = false;
        final boolean b11 = false;
        final boolean b12 = false;
        final boolean b13 = false;
        final boolean b14 = false;
        final boolean b15 = false;
        final boolean b16 = false;
        if (n != 1048575L) {
            boolean b17 = b15;
            if ((d12.C(g, n) & 1 << (l0 >>> 20)) != 0x0) {
                b17 = true;
            }
            return b17;
        }
        x0 = this.x0(x0);
        final long y = Y(x0);
        switch (w0(x0)) {
            default: {
                throw new IllegalArgumentException();
            }
            case 17: {
                boolean b18 = b16;
                if (d12.G(g, y) != null) {
                    b18 = true;
                }
                return b18;
            }
            case 16: {
                boolean b19 = b;
                if (d12.E(g, y) != 0L) {
                    b19 = true;
                }
                return b19;
            }
            case 15: {
                boolean b20 = b2;
                if (d12.C(g, y) != 0) {
                    b20 = true;
                }
                return b20;
            }
            case 14: {
                boolean b21 = b3;
                if (d12.E(g, y) != 0L) {
                    b21 = true;
                }
                return b21;
            }
            case 13: {
                boolean b22 = b4;
                if (d12.C(g, y) != 0) {
                    b22 = true;
                }
                return b22;
            }
            case 12: {
                boolean b23 = b5;
                if (d12.C(g, y) != 0) {
                    b23 = true;
                }
                return b23;
            }
            case 11: {
                boolean b24 = b6;
                if (d12.C(g, y) != 0) {
                    b24 = true;
                }
                return b24;
            }
            case 10: {
                return ByteString.EMPTY.equals(d12.G(g, y)) ^ true;
            }
            case 9: {
                if (d12.G(g, y) != null) {
                    b7 = true;
                }
                return b7;
            }
            case 8: {
                g = d12.G(g, y);
                if (g instanceof String) {
                    return ((String)g).isEmpty() ^ true;
                }
                if (g instanceof ByteString) {
                    return ByteString.EMPTY.equals(g) ^ true;
                }
                throw new IllegalArgumentException();
            }
            case 7: {
                return d12.t(g, y);
            }
            case 6: {
                boolean b25 = b8;
                if (d12.C(g, y) != 0) {
                    b25 = true;
                }
                return b25;
            }
            case 5: {
                boolean b26 = b9;
                if (d12.E(g, y) != 0L) {
                    b26 = true;
                }
                return b26;
            }
            case 4: {
                boolean b27 = b10;
                if (d12.C(g, y) != 0) {
                    b27 = true;
                }
                return b27;
            }
            case 3: {
                boolean b28 = b11;
                if (d12.E(g, y) != 0L) {
                    b28 = true;
                }
                return b28;
            }
            case 2: {
                boolean b29 = b12;
                if (d12.E(g, y) != 0L) {
                    b29 = true;
                }
                return b29;
            }
            case 1: {
                boolean b30 = b13;
                if (Float.floatToRawIntBits(d12.B(g, y)) != 0) {
                    b30 = true;
                }
                return b30;
            }
            case 0: {
                boolean b31 = b14;
                if (Double.doubleToRawLongBits(d12.A(g, y)) != 0L) {
                    b31 = true;
                }
                return b31;
            }
        }
    }
    
    public final void C0(final int n, final Object o, final Writer writer) {
        if (o instanceof String) {
            writer.d(n, (String)o);
        }
        else {
            writer.J(n, (ByteString)o);
        }
    }
    
    public final boolean D(final Object o, final int n, final int n2, final int n3, final int n4) {
        if (n2 == 1048575) {
            return this.C(o, n);
        }
        return (n3 & n4) != 0x0;
    }
    
    public final void D0(final k0 k0, final Object o, final Writer writer) {
        k0.t(k0.g(o), writer);
    }
    
    public final boolean F(final Object o, int i, final int n) {
        final List list = (List)d12.G(o, Y(i));
        if (list.isEmpty()) {
            return true;
        }
        final g0 v = this.v(n);
        for (i = 0; i < list.size(); ++i) {
            if (!v.b(list.get(i))) {
                return false;
            }
        }
        return true;
    }
    
    public final boolean G(Object u, final int n, final int n2) {
        final Map g = this.q.g(d12.G(u, Y(n)));
        if (g.isEmpty()) {
            return true;
        }
        u = this.u(n2);
        if (this.q.b(u).c.getJavaType() != WireFormat.JavaType.MESSAGE) {
            return true;
        }
        final Iterator iterator = g.values().iterator();
        g0 g2 = null;
        while (iterator.hasNext()) {
            final Object next = iterator.next();
            g0 c;
            if ((c = g2) == null) {
                c = k91.a().c(next.getClass());
            }
            g2 = c;
            if (!c.b(next)) {
                return false;
            }
        }
        return true;
    }
    
    public final boolean I(final Object o, final Object o2, final int n) {
        final long n2 = this.l0(n) & 0xFFFFF;
        return d12.C(o, n2) == d12.C(o2, n2);
    }
    
    public final boolean J(final Object o, final int n, final int n2) {
        return d12.C(o, this.l0(n2) & 0xFFFFF) == n;
    }
    
    public final void N(final k0 p0, final m p1, final Object p2, final f0 p3, final l p4) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore          19
        //     3: aload_3        
        //     4: astore          14
        //     6: aload           5
        //     8: astore          20
        //    10: aconst_null    
        //    11: astore          13
        //    13: aconst_null    
        //    14: astore          17
        //    16: aload           4
        //    18: invokeinterface com/google/protobuf/f0.m:()I
        //    23: istore          6
        //    25: aload_0        
        //    26: iload           6
        //    28: invokevirtual   com/google/protobuf/c0.j0:(I)I
        //    31: istore          8
        //    33: iload           8
        //    35: ifge            349
        //    38: iload           6
        //    40: ldc_w           2147483647
        //    43: if_icmpne       100
        //    46: aload_0        
        //    47: getfield        com/google/protobuf/c0.k:I
        //    50: istore          6
        //    52: iload           6
        //    54: aload_0        
        //    55: getfield        com/google/protobuf/c0.l:I
        //    58: if_icmpge       85
        //    61: aload_0        
        //    62: aload_3        
        //    63: aload_0        
        //    64: getfield        com/google/protobuf/c0.j:[I
        //    67: iload           6
        //    69: iaload         
        //    70: aload           13
        //    72: aload_1        
        //    73: aload_3        
        //    74: invokevirtual   com/google/protobuf/c0.q:(Ljava/lang/Object;ILjava/lang/Object;Lcom/google/protobuf/k0;Ljava/lang/Object;)Ljava/lang/Object;
        //    77: astore          13
        //    79: iinc            6, 1
        //    82: goto            52
        //    85: aload           13
        //    87: ifnull          99
        //    90: aload           19
        //    92: aload           14
        //    94: aload           13
        //    96: invokevirtual   com/google/protobuf/k0.o:(Ljava/lang/Object;Ljava/lang/Object;)V
        //    99: return         
        //   100: aload_0        
        //   101: getfield        com/google/protobuf/c0.f:Z
        //   104: ifne            113
        //   107: aconst_null    
        //   108: astore          18
        //   110: goto            127
        //   113: aload_2        
        //   114: aload           20
        //   116: aload_0        
        //   117: getfield        com/google/protobuf/c0.e:Lcom/google/protobuf/a0;
        //   120: iload           6
        //   122: invokevirtual   com/google/protobuf/m.b:(Lcom/google/protobuf/l;Lcom/google/protobuf/a0;I)Ljava/lang/Object;
        //   125: astore          18
        //   127: aload           18
        //   129: ifnull          184
        //   132: aload           17
        //   134: ifnonnull       151
        //   137: aload_2        
        //   138: aload_3        
        //   139: invokevirtual   com/google/protobuf/m.d:(Ljava/lang/Object;)Lcom/google/protobuf/o;
        //   142: astore          15
        //   144: goto            155
        //   147: astore_2       
        //   148: goto            2772
        //   151: aload           17
        //   153: astore          15
        //   155: aload           13
        //   157: astore          16
        //   159: aload_2        
        //   160: aload_3        
        //   161: aload           4
        //   163: aload           18
        //   165: aload           5
        //   167: aload           15
        //   169: aload           13
        //   171: aload_1        
        //   172: invokevirtual   com/google/protobuf/m.g:(Ljava/lang/Object;Lcom/google/protobuf/f0;Ljava/lang/Object;Lcom/google/protobuf/l;Lcom/google/protobuf/o;Ljava/lang/Object;Lcom/google/protobuf/k0;)Ljava/lang/Object;
        //   175: astore          13
        //   177: aload           15
        //   179: astore          17
        //   181: goto            16
        //   184: aload           14
        //   186: astore          21
        //   188: aload           13
        //   190: astore          16
        //   192: aload           19
        //   194: aload           4
        //   196: invokevirtual   com/google/protobuf/k0.q:(Lcom/google/protobuf/f0;)Z
        //   199: ifeq            223
        //   202: aload           13
        //   204: astore          18
        //   206: aload           13
        //   208: astore          16
        //   210: aload           4
        //   212: invokeinterface com/google/protobuf/f0.p:()Z
        //   217: ifeq            276
        //   220: goto            181
        //   223: aload           13
        //   225: astore          15
        //   227: aload           13
        //   229: ifnonnull       245
        //   232: aload           13
        //   234: astore          16
        //   236: aload           19
        //   238: aload           21
        //   240: invokevirtual   com/google/protobuf/k0.f:(Ljava/lang/Object;)Ljava/lang/Object;
        //   243: astore          15
        //   245: aload           15
        //   247: astore          16
        //   249: aload           19
        //   251: aload           15
        //   253: aload           4
        //   255: invokevirtual   com/google/protobuf/k0.m:(Ljava/lang/Object;Lcom/google/protobuf/f0;)Z
        //   258: istore          12
        //   260: aload           15
        //   262: astore          18
        //   264: iload           12
        //   266: ifeq            276
        //   269: aload           15
        //   271: astore          13
        //   273: goto            220
        //   276: aload_0        
        //   277: getfield        com/google/protobuf/c0.k:I
        //   280: istore          6
        //   282: aload           18
        //   284: astore_2       
        //   285: iload           6
        //   287: aload_0        
        //   288: getfield        com/google/protobuf/c0.l:I
        //   291: if_icmpge       316
        //   294: aload_0        
        //   295: aload_3        
        //   296: aload_0        
        //   297: getfield        com/google/protobuf/c0.j:[I
        //   300: iload           6
        //   302: iaload         
        //   303: aload_2        
        //   304: aload_1        
        //   305: aload_3        
        //   306: invokevirtual   com/google/protobuf/c0.q:(Ljava/lang/Object;ILjava/lang/Object;Lcom/google/protobuf/k0;Ljava/lang/Object;)Ljava/lang/Object;
        //   309: astore_2       
        //   310: iinc            6, 1
        //   313: goto            285
        //   316: aload_2        
        //   317: ifnull          328
        //   320: aload           19
        //   322: aload           21
        //   324: aload_2        
        //   325: invokevirtual   com/google/protobuf/k0.o:(Ljava/lang/Object;Ljava/lang/Object;)V
        //   328: return         
        //   329: astore_2       
        //   330: aload           14
        //   332: astore          4
        //   334: aload           16
        //   336: astore          13
        //   338: goto            2764
        //   341: astore_2       
        //   342: aload           14
        //   344: astore          4
        //   346: goto            2764
        //   349: aload           14
        //   351: astore          18
        //   353: aload_0        
        //   354: iload           8
        //   356: invokevirtual   com/google/protobuf/c0.x0:(I)I
        //   359: istore          7
        //   361: iload           7
        //   363: invokestatic    com/google/protobuf/c0.w0:(I)I
        //   366: istore          9
        //   368: iload           9
        //   370: tableswitch {
        //                0: 2439
        //                1: 2419
        //                2: 2399
        //                3: 2379
        //                4: 2359
        //                5: 2339
        //                6: 2319
        //                7: 2299
        //                8: 2286
        //                9: 2253
        //               10: 2233
        //               11: 2213
        //               12: 2139
        //               13: 2119
        //               14: 2099
        //               15: 2079
        //               16: 2051
        //               17: 2008
        //               18: 1989
        //               19: 1970
        //               20: 1951
        //               21: 1932
        //               22: 1913
        //               23: 1894
        //               24: 1875
        //               25: 1856
        //               26: 1843
        //               27: 1823
        //               28: 1799
        //               29: 1780
        //               30: 1744
        //               31: 1725
        //               32: 1706
        //               33: 1687
        //               34: 1668
        //               35: 1640
        //               36: 1612
        //               37: 1584
        //               38: 1556
        //               39: 1528
        //               40: 1500
        //               41: 1472
        //               42: 1444
        //               43: 1416
        //               44: 1361
        //               45: 1333
        //               46: 1305
        //               47: 1277
        //               48: 1249
        //               49: 1209
        //               50: 1185
        //               51: 1158
        //               52: 1135
        //               53: 1112
        //               54: 1089
        //               55: 1066
        //               56: 1043
        //               57: 1020
        //               58: 997
        //               59: 981
        //               60: 946
        //               61: 926
        //               62: 903
        //               63: 826
        //               64: 803
        //               65: 780
        //               66: 747
        //               67: 724
        //               68: 677
        //          default: 660
        //        }
        //   660: aload           13
        //   662: ifnonnull       2476
        //   665: aload           19
        //   667: aload           18
        //   669: invokevirtual   com/google/protobuf/k0.f:(Ljava/lang/Object;)Ljava/lang/Object;
        //   672: astore          15
        //   674: goto            2473
        //   677: aload_0        
        //   678: aload           18
        //   680: iload           6
        //   682: iload           8
        //   684: invokevirtual   com/google/protobuf/c0.T:(Ljava/lang/Object;II)Ljava/lang/Object;
        //   687: checkcast       Lcom/google/protobuf/a0;
        //   690: astore          15
        //   692: aload           4
        //   694: aload           15
        //   696: aload_0        
        //   697: iload           8
        //   699: invokevirtual   com/google/protobuf/c0.v:(I)Lcom/google/protobuf/g0;
        //   702: aload           20
        //   704: invokeinterface com/google/protobuf/f0.J:(Ljava/lang/Object;Lcom/google/protobuf/g0;Lcom/google/protobuf/l;)V
        //   709: aload_0        
        //   710: aload           18
        //   712: iload           6
        //   714: iload           8
        //   716: aload           15
        //   718: invokevirtual   com/google/protobuf/c0.v0:(Ljava/lang/Object;IILjava/lang/Object;)V
        //   721: goto            994
        //   724: aload           18
        //   726: iload           7
        //   728: invokestatic    com/google/protobuf/c0.Y:(I)J
        //   731: aload           4
        //   733: invokeinterface com/google/protobuf/f0.E:()J
        //   738: invokestatic    java/lang/Long.valueOf:(J)Ljava/lang/Long;
        //   741: invokestatic    d12.W:(Ljava/lang/Object;JLjava/lang/Object;)V
        //   744: goto            767
        //   747: aload           18
        //   749: iload           7
        //   751: invokestatic    com/google/protobuf/c0.Y:(I)J
        //   754: aload           4
        //   756: invokeinterface com/google/protobuf/f0.e:()I
        //   761: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //   764: invokestatic    d12.W:(Ljava/lang/Object;JLjava/lang/Object;)V
        //   767: aload_0        
        //   768: aload           18
        //   770: iload           6
        //   772: iload           8
        //   774: invokevirtual   com/google/protobuf/c0.s0:(Ljava/lang/Object;II)V
        //   777: goto            994
        //   780: aload           18
        //   782: iload           7
        //   784: invokestatic    com/google/protobuf/c0.Y:(I)J
        //   787: aload           4
        //   789: invokeinterface com/google/protobuf/f0.b:()J
        //   794: invokestatic    java/lang/Long.valueOf:(J)Ljava/lang/Long;
        //   797: invokestatic    d12.W:(Ljava/lang/Object;JLjava/lang/Object;)V
        //   800: goto            767
        //   803: aload           18
        //   805: iload           7
        //   807: invokestatic    com/google/protobuf/c0.Y:(I)J
        //   810: aload           4
        //   812: invokeinterface com/google/protobuf/f0.G:()I
        //   817: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //   820: invokestatic    d12.W:(Ljava/lang/Object;JLjava/lang/Object;)V
        //   823: goto            767
        //   826: aload           4
        //   828: invokeinterface com/google/protobuf/f0.d:()I
        //   833: istore          9
        //   835: aload_0        
        //   836: iload           8
        //   838: invokevirtual   com/google/protobuf/c0.t:(I)Lcom/google/protobuf/t$c;
        //   841: astore          15
        //   843: aload           15
        //   845: ifnull          885
        //   848: aload           15
        //   850: iload           9
        //   852: invokeinterface com/google/protobuf/t$c.a:(I)Z
        //   857: ifeq            863
        //   860: goto            885
        //   863: aload           18
        //   865: iload           6
        //   867: iload           9
        //   869: aload           13
        //   871: aload           19
        //   873: invokestatic    com/google/protobuf/h0.K:(Ljava/lang/Object;IILjava/lang/Object;Lcom/google/protobuf/k0;)Ljava/lang/Object;
        //   876: astore          15
        //   878: aload           15
        //   880: astore          13
        //   882: goto            2752
        //   885: aload           18
        //   887: iload           7
        //   889: invokestatic    com/google/protobuf/c0.Y:(I)J
        //   892: iload           9
        //   894: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //   897: invokestatic    d12.W:(Ljava/lang/Object;JLjava/lang/Object;)V
        //   900: goto            767
        //   903: aload           18
        //   905: iload           7
        //   907: invokestatic    com/google/protobuf/c0.Y:(I)J
        //   910: aload           4
        //   912: invokeinterface com/google/protobuf/f0.c:()I
        //   917: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //   920: invokestatic    d12.W:(Ljava/lang/Object;JLjava/lang/Object;)V
        //   923: goto            767
        //   926: aload           18
        //   928: iload           7
        //   930: invokestatic    com/google/protobuf/c0.Y:(I)J
        //   933: aload           4
        //   935: invokeinterface com/google/protobuf/f0.g:()Lcom/google/protobuf/ByteString;
        //   940: invokestatic    d12.W:(Ljava/lang/Object;JLjava/lang/Object;)V
        //   943: goto            767
        //   946: aload_0        
        //   947: aload           18
        //   949: iload           6
        //   951: iload           8
        //   953: invokevirtual   com/google/protobuf/c0.T:(Ljava/lang/Object;II)Ljava/lang/Object;
        //   956: checkcast       Lcom/google/protobuf/a0;
        //   959: astore          15
        //   961: aload           4
        //   963: aload           15
        //   965: aload_0        
        //   966: iload           8
        //   968: invokevirtual   com/google/protobuf/c0.v:(I)Lcom/google/protobuf/g0;
        //   971: aload           20
        //   973: invokeinterface com/google/protobuf/f0.K:(Ljava/lang/Object;Lcom/google/protobuf/g0;Lcom/google/protobuf/l;)V
        //   978: goto            709
        //   981: aload_0        
        //   982: aload           18
        //   984: iload           7
        //   986: aload           4
        //   988: invokevirtual   com/google/protobuf/c0.o0:(Ljava/lang/Object;ILcom/google/protobuf/f0;)V
        //   991: goto            767
        //   994: goto            1206
        //   997: aload           18
        //   999: iload           7
        //  1001: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1004: aload           4
        //  1006: invokeinterface com/google/protobuf/f0.v:()Z
        //  1011: invokestatic    java/lang/Boolean.valueOf:(Z)Ljava/lang/Boolean;
        //  1014: invokestatic    d12.W:(Ljava/lang/Object;JLjava/lang/Object;)V
        //  1017: goto            767
        //  1020: aload           18
        //  1022: iload           7
        //  1024: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1027: aload           4
        //  1029: invokeinterface com/google/protobuf/f0.D:()I
        //  1034: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //  1037: invokestatic    d12.W:(Ljava/lang/Object;JLjava/lang/Object;)V
        //  1040: goto            767
        //  1043: aload           18
        //  1045: iload           7
        //  1047: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1050: aload           4
        //  1052: invokeinterface com/google/protobuf/f0.t:()J
        //  1057: invokestatic    java/lang/Long.valueOf:(J)Ljava/lang/Long;
        //  1060: invokestatic    d12.W:(Ljava/lang/Object;JLjava/lang/Object;)V
        //  1063: goto            767
        //  1066: aload           18
        //  1068: iload           7
        //  1070: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1073: aload           4
        //  1075: invokeinterface com/google/protobuf/f0.A:()I
        //  1080: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //  1083: invokestatic    d12.W:(Ljava/lang/Object;JLjava/lang/Object;)V
        //  1086: goto            767
        //  1089: aload           18
        //  1091: iload           7
        //  1093: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1096: aload           4
        //  1098: invokeinterface com/google/protobuf/f0.i:()J
        //  1103: invokestatic    java/lang/Long.valueOf:(J)Ljava/lang/Long;
        //  1106: invokestatic    d12.W:(Ljava/lang/Object;JLjava/lang/Object;)V
        //  1109: goto            767
        //  1112: aload           18
        //  1114: iload           7
        //  1116: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1119: aload           4
        //  1121: invokeinterface com/google/protobuf/f0.s:()J
        //  1126: invokestatic    java/lang/Long.valueOf:(J)Ljava/lang/Long;
        //  1129: invokestatic    d12.W:(Ljava/lang/Object;JLjava/lang/Object;)V
        //  1132: goto            767
        //  1135: aload           18
        //  1137: iload           7
        //  1139: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1142: aload           4
        //  1144: invokeinterface com/google/protobuf/f0.readFloat:()F
        //  1149: invokestatic    java/lang/Float.valueOf:(F)Ljava/lang/Float;
        //  1152: invokestatic    d12.W:(Ljava/lang/Object;JLjava/lang/Object;)V
        //  1155: goto            767
        //  1158: aload           18
        //  1160: iload           7
        //  1162: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1165: aload           4
        //  1167: invokeinterface com/google/protobuf/f0.readDouble:()D
        //  1172: invokestatic    java/lang/Double.valueOf:(D)Ljava/lang/Double;
        //  1175: invokestatic    d12.W:(Ljava/lang/Object;JLjava/lang/Object;)V
        //  1178: goto            767
        //  1181: astore_2       
        //  1182: goto            2764
        //  1185: aload_0        
        //  1186: iload           8
        //  1188: invokevirtual   com/google/protobuf/c0.u:(I)Ljava/lang/Object;
        //  1191: astore          15
        //  1193: aload_0        
        //  1194: aload_3        
        //  1195: iload           8
        //  1197: aload           15
        //  1199: aload           5
        //  1201: aload           4
        //  1203: invokevirtual   com/google/protobuf/c0.O:(Ljava/lang/Object;ILjava/lang/Object;Lcom/google/protobuf/l;Lcom/google/protobuf/f0;)V
        //  1206: goto            2459
        //  1209: iload           7
        //  1211: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1214: lstore          10
        //  1216: aload_0        
        //  1217: iload           8
        //  1219: invokevirtual   com/google/protobuf/c0.v:(I)Lcom/google/protobuf/g0;
        //  1222: astore          15
        //  1224: aload_0        
        //  1225: aload_3        
        //  1226: lload           10
        //  1228: aload           4
        //  1230: aload           15
        //  1232: aload           5
        //  1234: invokevirtual   com/google/protobuf/c0.m0:(Ljava/lang/Object;JLcom/google/protobuf/f0;Lcom/google/protobuf/g0;Lcom/google/protobuf/l;)V
        //  1237: goto            2459
        //  1240: astore_2       
        //  1241: goto            2463
        //  1244: astore          15
        //  1246: goto            2466
        //  1249: aload_0        
        //  1250: getfield        com/google/protobuf/c0.n:Lcom/google/protobuf/v;
        //  1253: aload           18
        //  1255: iload           7
        //  1257: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1260: invokevirtual   com/google/protobuf/v.e:(Ljava/lang/Object;J)Ljava/util/List;
        //  1263: astore          15
        //  1265: aload           4
        //  1267: aload           15
        //  1269: invokeinterface com/google/protobuf/f0.a:(Ljava/util/List;)V
        //  1274: goto            2459
        //  1277: aload_0        
        //  1278: getfield        com/google/protobuf/c0.n:Lcom/google/protobuf/v;
        //  1281: aload           18
        //  1283: iload           7
        //  1285: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1288: invokevirtual   com/google/protobuf/v.e:(Ljava/lang/Object;J)Ljava/util/List;
        //  1291: astore          15
        //  1293: aload           4
        //  1295: aload           15
        //  1297: invokeinterface com/google/protobuf/f0.h:(Ljava/util/List;)V
        //  1302: goto            2459
        //  1305: aload_0        
        //  1306: getfield        com/google/protobuf/c0.n:Lcom/google/protobuf/v;
        //  1309: aload           18
        //  1311: iload           7
        //  1313: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1316: invokevirtual   com/google/protobuf/v.e:(Ljava/lang/Object;J)Ljava/util/List;
        //  1319: astore          15
        //  1321: aload           4
        //  1323: aload           15
        //  1325: invokeinterface com/google/protobuf/f0.j:(Ljava/util/List;)V
        //  1330: goto            2459
        //  1333: aload_0        
        //  1334: getfield        com/google/protobuf/c0.n:Lcom/google/protobuf/v;
        //  1337: aload           18
        //  1339: iload           7
        //  1341: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1344: invokevirtual   com/google/protobuf/v.e:(Ljava/lang/Object;J)Ljava/util/List;
        //  1347: astore          15
        //  1349: aload           4
        //  1351: aload           15
        //  1353: invokeinterface com/google/protobuf/f0.u:(Ljava/util/List;)V
        //  1358: goto            2459
        //  1361: aload_0        
        //  1362: getfield        com/google/protobuf/c0.n:Lcom/google/protobuf/v;
        //  1365: aload           18
        //  1367: iload           7
        //  1369: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1372: invokevirtual   com/google/protobuf/v.e:(Ljava/lang/Object;J)Ljava/util/List;
        //  1375: astore          15
        //  1377: aload           4
        //  1379: aload           15
        //  1381: invokeinterface com/google/protobuf/f0.y:(Ljava/util/List;)V
        //  1386: aload_0        
        //  1387: iload           8
        //  1389: invokevirtual   com/google/protobuf/c0.t:(I)Lcom/google/protobuf/t$c;
        //  1392: astore          16
        //  1394: aload_3        
        //  1395: iload           6
        //  1397: aload           15
        //  1399: aload           16
        //  1401: aload           13
        //  1403: aload_1        
        //  1404: invokestatic    com/google/protobuf/h0.z:(Ljava/lang/Object;ILjava/util/List;Lcom/google/protobuf/t$c;Ljava/lang/Object;Lcom/google/protobuf/k0;)Ljava/lang/Object;
        //  1407: astore          15
        //  1409: aload           15
        //  1411: astore          13
        //  1413: goto            2752
        //  1416: aload_0        
        //  1417: getfield        com/google/protobuf/c0.n:Lcom/google/protobuf/v;
        //  1420: aload           18
        //  1422: iload           7
        //  1424: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1427: invokevirtual   com/google/protobuf/v.e:(Ljava/lang/Object;J)Ljava/util/List;
        //  1430: astore          15
        //  1432: aload           4
        //  1434: aload           15
        //  1436: invokeinterface com/google/protobuf/f0.C:(Ljava/util/List;)V
        //  1441: goto            2459
        //  1444: aload_0        
        //  1445: getfield        com/google/protobuf/c0.n:Lcom/google/protobuf/v;
        //  1448: aload           18
        //  1450: iload           7
        //  1452: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1455: invokevirtual   com/google/protobuf/v.e:(Ljava/lang/Object;J)Ljava/util/List;
        //  1458: astore          15
        //  1460: aload           4
        //  1462: aload           15
        //  1464: invokeinterface com/google/protobuf/f0.f:(Ljava/util/List;)V
        //  1469: goto            2459
        //  1472: aload_0        
        //  1473: getfield        com/google/protobuf/c0.n:Lcom/google/protobuf/v;
        //  1476: aload           18
        //  1478: iload           7
        //  1480: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1483: invokevirtual   com/google/protobuf/v.e:(Ljava/lang/Object;J)Ljava/util/List;
        //  1486: astore          15
        //  1488: aload           4
        //  1490: aload           15
        //  1492: invokeinterface com/google/protobuf/f0.l:(Ljava/util/List;)V
        //  1497: goto            2459
        //  1500: aload_0        
        //  1501: getfield        com/google/protobuf/c0.n:Lcom/google/protobuf/v;
        //  1504: aload           18
        //  1506: iload           7
        //  1508: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1511: invokevirtual   com/google/protobuf/v.e:(Ljava/lang/Object;J)Ljava/util/List;
        //  1514: astore          15
        //  1516: aload           4
        //  1518: aload           15
        //  1520: invokeinterface com/google/protobuf/f0.B:(Ljava/util/List;)V
        //  1525: goto            2459
        //  1528: aload_0        
        //  1529: getfield        com/google/protobuf/c0.n:Lcom/google/protobuf/v;
        //  1532: aload           18
        //  1534: iload           7
        //  1536: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1539: invokevirtual   com/google/protobuf/v.e:(Ljava/lang/Object;J)Ljava/util/List;
        //  1542: astore          15
        //  1544: aload           4
        //  1546: aload           15
        //  1548: invokeinterface com/google/protobuf/f0.k:(Ljava/util/List;)V
        //  1553: goto            2459
        //  1556: aload_0        
        //  1557: getfield        com/google/protobuf/c0.n:Lcom/google/protobuf/v;
        //  1560: aload           18
        //  1562: iload           7
        //  1564: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1567: invokevirtual   com/google/protobuf/v.e:(Ljava/lang/Object;J)Ljava/util/List;
        //  1570: astore          15
        //  1572: aload           4
        //  1574: aload           15
        //  1576: invokeinterface com/google/protobuf/f0.w:(Ljava/util/List;)V
        //  1581: goto            2459
        //  1584: aload_0        
        //  1585: getfield        com/google/protobuf/c0.n:Lcom/google/protobuf/v;
        //  1588: aload           18
        //  1590: iload           7
        //  1592: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1595: invokevirtual   com/google/protobuf/v.e:(Ljava/lang/Object;J)Ljava/util/List;
        //  1598: astore          15
        //  1600: aload           4
        //  1602: aload           15
        //  1604: invokeinterface com/google/protobuf/f0.x:(Ljava/util/List;)V
        //  1609: goto            2459
        //  1612: aload_0        
        //  1613: getfield        com/google/protobuf/c0.n:Lcom/google/protobuf/v;
        //  1616: aload           18
        //  1618: iload           7
        //  1620: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1623: invokevirtual   com/google/protobuf/v.e:(Ljava/lang/Object;J)Ljava/util/List;
        //  1626: astore          15
        //  1628: aload           4
        //  1630: aload           15
        //  1632: invokeinterface com/google/protobuf/f0.o:(Ljava/util/List;)V
        //  1637: goto            2459
        //  1640: aload_0        
        //  1641: getfield        com/google/protobuf/c0.n:Lcom/google/protobuf/v;
        //  1644: aload           18
        //  1646: iload           7
        //  1648: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1651: invokevirtual   com/google/protobuf/v.e:(Ljava/lang/Object;J)Ljava/util/List;
        //  1654: astore          15
        //  1656: aload           4
        //  1658: aload           15
        //  1660: invokeinterface com/google/protobuf/f0.r:(Ljava/util/List;)V
        //  1665: goto            2459
        //  1668: aload_0        
        //  1669: getfield        com/google/protobuf/c0.n:Lcom/google/protobuf/v;
        //  1672: aload           18
        //  1674: iload           7
        //  1676: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1679: invokevirtual   com/google/protobuf/v.e:(Ljava/lang/Object;J)Ljava/util/List;
        //  1682: astore          15
        //  1684: goto            1265
        //  1687: aload_0        
        //  1688: getfield        com/google/protobuf/c0.n:Lcom/google/protobuf/v;
        //  1691: aload           18
        //  1693: iload           7
        //  1695: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1698: invokevirtual   com/google/protobuf/v.e:(Ljava/lang/Object;J)Ljava/util/List;
        //  1701: astore          15
        //  1703: goto            1293
        //  1706: aload_0        
        //  1707: getfield        com/google/protobuf/c0.n:Lcom/google/protobuf/v;
        //  1710: aload           18
        //  1712: iload           7
        //  1714: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1717: invokevirtual   com/google/protobuf/v.e:(Ljava/lang/Object;J)Ljava/util/List;
        //  1720: astore          15
        //  1722: goto            1321
        //  1725: aload_0        
        //  1726: getfield        com/google/protobuf/c0.n:Lcom/google/protobuf/v;
        //  1729: aload           18
        //  1731: iload           7
        //  1733: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1736: invokevirtual   com/google/protobuf/v.e:(Ljava/lang/Object;J)Ljava/util/List;
        //  1739: astore          15
        //  1741: goto            1349
        //  1744: aload_0        
        //  1745: getfield        com/google/protobuf/c0.n:Lcom/google/protobuf/v;
        //  1748: aload           18
        //  1750: iload           7
        //  1752: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1755: invokevirtual   com/google/protobuf/v.e:(Ljava/lang/Object;J)Ljava/util/List;
        //  1758: astore          15
        //  1760: aload           4
        //  1762: aload           15
        //  1764: invokeinterface com/google/protobuf/f0.y:(Ljava/util/List;)V
        //  1769: aload_0        
        //  1770: iload           8
        //  1772: invokevirtual   com/google/protobuf/c0.t:(I)Lcom/google/protobuf/t$c;
        //  1775: astore          16
        //  1777: goto            1394
        //  1780: aload_0        
        //  1781: getfield        com/google/protobuf/c0.n:Lcom/google/protobuf/v;
        //  1784: aload           18
        //  1786: iload           7
        //  1788: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1791: invokevirtual   com/google/protobuf/v.e:(Ljava/lang/Object;J)Ljava/util/List;
        //  1794: astore          15
        //  1796: goto            1432
        //  1799: aload           4
        //  1801: aload_0        
        //  1802: getfield        com/google/protobuf/c0.n:Lcom/google/protobuf/v;
        //  1805: aload           18
        //  1807: iload           7
        //  1809: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1812: invokevirtual   com/google/protobuf/v.e:(Ljava/lang/Object;J)Ljava/util/List;
        //  1815: invokeinterface com/google/protobuf/f0.q:(Ljava/util/List;)V
        //  1820: goto            2459
        //  1823: aload_0        
        //  1824: aload_3        
        //  1825: iload           7
        //  1827: aload           4
        //  1829: aload_0        
        //  1830: iload           8
        //  1832: invokevirtual   com/google/protobuf/c0.v:(I)Lcom/google/protobuf/g0;
        //  1835: aload           5
        //  1837: invokevirtual   com/google/protobuf/c0.n0:(Ljava/lang/Object;ILcom/google/protobuf/f0;Lcom/google/protobuf/g0;Lcom/google/protobuf/l;)V
        //  1840: goto            2459
        //  1843: aload_0        
        //  1844: aload           18
        //  1846: iload           7
        //  1848: aload           4
        //  1850: invokevirtual   com/google/protobuf/c0.p0:(Ljava/lang/Object;ILcom/google/protobuf/f0;)V
        //  1853: goto            2459
        //  1856: aload_0        
        //  1857: getfield        com/google/protobuf/c0.n:Lcom/google/protobuf/v;
        //  1860: aload           18
        //  1862: iload           7
        //  1864: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1867: invokevirtual   com/google/protobuf/v.e:(Ljava/lang/Object;J)Ljava/util/List;
        //  1870: astore          15
        //  1872: goto            1460
        //  1875: aload_0        
        //  1876: getfield        com/google/protobuf/c0.n:Lcom/google/protobuf/v;
        //  1879: aload           18
        //  1881: iload           7
        //  1883: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1886: invokevirtual   com/google/protobuf/v.e:(Ljava/lang/Object;J)Ljava/util/List;
        //  1889: astore          15
        //  1891: goto            1488
        //  1894: aload_0        
        //  1895: getfield        com/google/protobuf/c0.n:Lcom/google/protobuf/v;
        //  1898: aload           18
        //  1900: iload           7
        //  1902: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1905: invokevirtual   com/google/protobuf/v.e:(Ljava/lang/Object;J)Ljava/util/List;
        //  1908: astore          15
        //  1910: goto            1516
        //  1913: aload_0        
        //  1914: getfield        com/google/protobuf/c0.n:Lcom/google/protobuf/v;
        //  1917: aload           18
        //  1919: iload           7
        //  1921: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1924: invokevirtual   com/google/protobuf/v.e:(Ljava/lang/Object;J)Ljava/util/List;
        //  1927: astore          15
        //  1929: goto            1544
        //  1932: aload_0        
        //  1933: getfield        com/google/protobuf/c0.n:Lcom/google/protobuf/v;
        //  1936: aload           18
        //  1938: iload           7
        //  1940: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1943: invokevirtual   com/google/protobuf/v.e:(Ljava/lang/Object;J)Ljava/util/List;
        //  1946: astore          15
        //  1948: goto            1572
        //  1951: aload_0        
        //  1952: getfield        com/google/protobuf/c0.n:Lcom/google/protobuf/v;
        //  1955: aload           18
        //  1957: iload           7
        //  1959: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1962: invokevirtual   com/google/protobuf/v.e:(Ljava/lang/Object;J)Ljava/util/List;
        //  1965: astore          15
        //  1967: goto            1600
        //  1970: aload_0        
        //  1971: getfield        com/google/protobuf/c0.n:Lcom/google/protobuf/v;
        //  1974: aload           18
        //  1976: iload           7
        //  1978: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  1981: invokevirtual   com/google/protobuf/v.e:(Ljava/lang/Object;J)Ljava/util/List;
        //  1984: astore          15
        //  1986: goto            1628
        //  1989: aload_0        
        //  1990: getfield        com/google/protobuf/c0.n:Lcom/google/protobuf/v;
        //  1993: aload           18
        //  1995: iload           7
        //  1997: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  2000: invokevirtual   com/google/protobuf/v.e:(Ljava/lang/Object;J)Ljava/util/List;
        //  2003: astore          15
        //  2005: goto            1656
        //  2008: aload_0        
        //  2009: aload           18
        //  2011: iload           8
        //  2013: invokevirtual   com/google/protobuf/c0.S:(Ljava/lang/Object;I)Ljava/lang/Object;
        //  2016: checkcast       Lcom/google/protobuf/a0;
        //  2019: astore          15
        //  2021: aload           4
        //  2023: aload           15
        //  2025: aload_0        
        //  2026: iload           8
        //  2028: invokevirtual   com/google/protobuf/c0.v:(I)Lcom/google/protobuf/g0;
        //  2031: aload           20
        //  2033: invokeinterface com/google/protobuf/f0.J:(Ljava/lang/Object;Lcom/google/protobuf/g0;Lcom/google/protobuf/l;)V
        //  2038: aload_0        
        //  2039: aload           18
        //  2041: iload           8
        //  2043: aload           15
        //  2045: invokevirtual   com/google/protobuf/c0.u0:(Ljava/lang/Object;ILjava/lang/Object;)V
        //  2048: goto            2459
        //  2051: aload           18
        //  2053: iload           7
        //  2055: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  2058: aload           4
        //  2060: invokeinterface com/google/protobuf/f0.E:()J
        //  2065: invokestatic    d12.V:(Ljava/lang/Object;JJ)V
        //  2068: aload_0        
        //  2069: aload           18
        //  2071: iload           8
        //  2073: invokevirtual   com/google/protobuf/c0.r0:(Ljava/lang/Object;I)V
        //  2076: goto            2459
        //  2079: aload           18
        //  2081: iload           7
        //  2083: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  2086: aload           4
        //  2088: invokeinterface com/google/protobuf/f0.e:()I
        //  2093: invokestatic    d12.U:(Ljava/lang/Object;JI)V
        //  2096: goto            2068
        //  2099: aload           18
        //  2101: iload           7
        //  2103: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  2106: aload           4
        //  2108: invokeinterface com/google/protobuf/f0.b:()J
        //  2113: invokestatic    d12.V:(Ljava/lang/Object;JJ)V
        //  2116: goto            2068
        //  2119: aload           18
        //  2121: iload           7
        //  2123: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  2126: aload           4
        //  2128: invokeinterface com/google/protobuf/f0.G:()I
        //  2133: invokestatic    d12.U:(Ljava/lang/Object;JI)V
        //  2136: goto            2068
        //  2139: aload           4
        //  2141: invokeinterface com/google/protobuf/f0.d:()I
        //  2146: istore          9
        //  2148: aload_0        
        //  2149: iload           8
        //  2151: invokevirtual   com/google/protobuf/c0.t:(I)Lcom/google/protobuf/t$c;
        //  2154: astore          15
        //  2156: aload           15
        //  2158: ifnull          2198
        //  2161: aload           15
        //  2163: iload           9
        //  2165: invokeinterface com/google/protobuf/t$c.a:(I)Z
        //  2170: ifeq            2176
        //  2173: goto            2198
        //  2176: aload           18
        //  2178: iload           6
        //  2180: iload           9
        //  2182: aload           13
        //  2184: aload           19
        //  2186: invokestatic    com/google/protobuf/h0.K:(Ljava/lang/Object;IILjava/lang/Object;Lcom/google/protobuf/k0;)Ljava/lang/Object;
        //  2189: astore          15
        //  2191: aload           15
        //  2193: astore          13
        //  2195: goto            2752
        //  2198: aload           18
        //  2200: iload           7
        //  2202: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  2205: iload           9
        //  2207: invokestatic    d12.U:(Ljava/lang/Object;JI)V
        //  2210: goto            2068
        //  2213: aload           18
        //  2215: iload           7
        //  2217: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  2220: aload           4
        //  2222: invokeinterface com/google/protobuf/f0.c:()I
        //  2227: invokestatic    d12.U:(Ljava/lang/Object;JI)V
        //  2230: goto            2068
        //  2233: aload           18
        //  2235: iload           7
        //  2237: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  2240: aload           4
        //  2242: invokeinterface com/google/protobuf/f0.g:()Lcom/google/protobuf/ByteString;
        //  2247: invokestatic    d12.W:(Ljava/lang/Object;JLjava/lang/Object;)V
        //  2250: goto            2068
        //  2253: aload_0        
        //  2254: aload           18
        //  2256: iload           8
        //  2258: invokevirtual   com/google/protobuf/c0.S:(Ljava/lang/Object;I)Ljava/lang/Object;
        //  2261: checkcast       Lcom/google/protobuf/a0;
        //  2264: astore          15
        //  2266: aload           4
        //  2268: aload           15
        //  2270: aload_0        
        //  2271: iload           8
        //  2273: invokevirtual   com/google/protobuf/c0.v:(I)Lcom/google/protobuf/g0;
        //  2276: aload           20
        //  2278: invokeinterface com/google/protobuf/f0.K:(Ljava/lang/Object;Lcom/google/protobuf/g0;Lcom/google/protobuf/l;)V
        //  2283: goto            2038
        //  2286: aload_0        
        //  2287: aload           18
        //  2289: iload           7
        //  2291: aload           4
        //  2293: invokevirtual   com/google/protobuf/c0.o0:(Ljava/lang/Object;ILcom/google/protobuf/f0;)V
        //  2296: goto            2068
        //  2299: aload           18
        //  2301: iload           7
        //  2303: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  2306: aload           4
        //  2308: invokeinterface com/google/protobuf/f0.v:()Z
        //  2313: invokestatic    d12.M:(Ljava/lang/Object;JZ)V
        //  2316: goto            2068
        //  2319: aload           18
        //  2321: iload           7
        //  2323: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  2326: aload           4
        //  2328: invokeinterface com/google/protobuf/f0.D:()I
        //  2333: invokestatic    d12.U:(Ljava/lang/Object;JI)V
        //  2336: goto            2068
        //  2339: aload           18
        //  2341: iload           7
        //  2343: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  2346: aload           4
        //  2348: invokeinterface com/google/protobuf/f0.t:()J
        //  2353: invokestatic    d12.V:(Ljava/lang/Object;JJ)V
        //  2356: goto            2068
        //  2359: aload           18
        //  2361: iload           7
        //  2363: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  2366: aload           4
        //  2368: invokeinterface com/google/protobuf/f0.A:()I
        //  2373: invokestatic    d12.U:(Ljava/lang/Object;JI)V
        //  2376: goto            2068
        //  2379: aload           18
        //  2381: iload           7
        //  2383: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  2386: aload           4
        //  2388: invokeinterface com/google/protobuf/f0.i:()J
        //  2393: invokestatic    d12.V:(Ljava/lang/Object;JJ)V
        //  2396: goto            2068
        //  2399: aload           18
        //  2401: iload           7
        //  2403: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  2406: aload           4
        //  2408: invokeinterface com/google/protobuf/f0.s:()J
        //  2413: invokestatic    d12.V:(Ljava/lang/Object;JJ)V
        //  2416: goto            2068
        //  2419: aload           18
        //  2421: iload           7
        //  2423: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  2426: aload           4
        //  2428: invokeinterface com/google/protobuf/f0.readFloat:()F
        //  2433: invokestatic    d12.T:(Ljava/lang/Object;JF)V
        //  2436: goto            2068
        //  2439: aload           18
        //  2441: iload           7
        //  2443: invokestatic    com/google/protobuf/c0.Y:(I)J
        //  2446: aload           4
        //  2448: invokeinterface com/google/protobuf/f0.readDouble:()D
        //  2453: invokestatic    d12.S:(Ljava/lang/Object;JD)V
        //  2456: goto            2068
        //  2459: goto            2752
        //  2462: astore_2       
        //  2463: goto            2776
        //  2466: aload           13
        //  2468: astore          15
        //  2470: goto            2563
        //  2473: goto            2480
        //  2476: aload           13
        //  2478: astore          15
        //  2480: aload           15
        //  2482: astore          13
        //  2484: aload           19
        //  2486: aload           15
        //  2488: aload           4
        //  2490: invokevirtual   com/google/protobuf/k0.m:(Ljava/lang/Object;Lcom/google/protobuf/f0;)Z
        //  2493: istore          12
        //  2495: aload           15
        //  2497: astore          13
        //  2499: iload           12
        //  2501: ifne            2752
        //  2504: aload_0        
        //  2505: getfield        com/google/protobuf/c0.k:I
        //  2508: istore          6
        //  2510: aload           15
        //  2512: astore_2       
        //  2513: iload           6
        //  2515: aload_0        
        //  2516: getfield        com/google/protobuf/c0.l:I
        //  2519: if_icmpge       2544
        //  2522: aload_0        
        //  2523: aload_3        
        //  2524: aload_0        
        //  2525: getfield        com/google/protobuf/c0.j:[I
        //  2528: iload           6
        //  2530: iaload         
        //  2531: aload_2        
        //  2532: aload_1        
        //  2533: aload_3        
        //  2534: invokevirtual   com/google/protobuf/c0.q:(Ljava/lang/Object;ILjava/lang/Object;Lcom/google/protobuf/k0;Ljava/lang/Object;)Ljava/lang/Object;
        //  2537: astore_2       
        //  2538: iinc            6, 1
        //  2541: goto            2513
        //  2544: aload_2        
        //  2545: ifnull          2556
        //  2548: aload           19
        //  2550: aload           18
        //  2552: aload_2        
        //  2553: invokevirtual   com/google/protobuf/k0.o:(Ljava/lang/Object;Ljava/lang/Object;)V
        //  2556: return         
        //  2557: astore          15
        //  2559: aload           13
        //  2561: astore          15
        //  2563: aload           15
        //  2565: astore          13
        //  2567: aload           19
        //  2569: aload           4
        //  2571: invokevirtual   com/google/protobuf/k0.q:(Lcom/google/protobuf/f0;)Z
        //  2574: ifeq            2653
        //  2577: aload           15
        //  2579: astore          13
        //  2581: aload           4
        //  2583: invokeinterface com/google/protobuf/f0.p:()Z
        //  2588: istore          12
        //  2590: aload           15
        //  2592: astore          13
        //  2594: iload           12
        //  2596: ifne            2752
        //  2599: aload_0        
        //  2600: getfield        com/google/protobuf/c0.k:I
        //  2603: istore          6
        //  2605: iload           6
        //  2607: aload_0        
        //  2608: getfield        com/google/protobuf/c0.l:I
        //  2611: if_icmpge       2638
        //  2614: aload_0        
        //  2615: aload_3        
        //  2616: aload_0        
        //  2617: getfield        com/google/protobuf/c0.j:[I
        //  2620: iload           6
        //  2622: iaload         
        //  2623: aload           15
        //  2625: aload_1        
        //  2626: aload_3        
        //  2627: invokevirtual   com/google/protobuf/c0.q:(Ljava/lang/Object;ILjava/lang/Object;Lcom/google/protobuf/k0;Ljava/lang/Object;)Ljava/lang/Object;
        //  2630: astore          15
        //  2632: iinc            6, 1
        //  2635: goto            2605
        //  2638: aload           15
        //  2640: ifnull          2652
        //  2643: aload           19
        //  2645: aload           18
        //  2647: aload           15
        //  2649: invokevirtual   com/google/protobuf/k0.o:(Ljava/lang/Object;Ljava/lang/Object;)V
        //  2652: return         
        //  2653: aload           15
        //  2655: astore          16
        //  2657: aload           15
        //  2659: ifnonnull       2675
        //  2662: aload           15
        //  2664: astore          13
        //  2666: aload           19
        //  2668: aload           18
        //  2670: invokevirtual   com/google/protobuf/k0.f:(Ljava/lang/Object;)Ljava/lang/Object;
        //  2673: astore          16
        //  2675: aload           16
        //  2677: astore          13
        //  2679: aload           19
        //  2681: aload           16
        //  2683: aload           4
        //  2685: invokevirtual   com/google/protobuf/k0.m:(Ljava/lang/Object;Lcom/google/protobuf/f0;)Z
        //  2688: istore          12
        //  2690: aload           16
        //  2692: astore          13
        //  2694: iload           12
        //  2696: ifne            2752
        //  2699: aload_0        
        //  2700: getfield        com/google/protobuf/c0.k:I
        //  2703: istore          6
        //  2705: aload           16
        //  2707: astore_2       
        //  2708: iload           6
        //  2710: aload_0        
        //  2711: getfield        com/google/protobuf/c0.l:I
        //  2714: if_icmpge       2739
        //  2717: aload_0        
        //  2718: aload_3        
        //  2719: aload_0        
        //  2720: getfield        com/google/protobuf/c0.j:[I
        //  2723: iload           6
        //  2725: iaload         
        //  2726: aload_2        
        //  2727: aload_1        
        //  2728: aload_3        
        //  2729: invokevirtual   com/google/protobuf/c0.q:(Ljava/lang/Object;ILjava/lang/Object;Lcom/google/protobuf/k0;Ljava/lang/Object;)Ljava/lang/Object;
        //  2732: astore_2       
        //  2733: iinc            6, 1
        //  2736: goto            2708
        //  2739: aload_2        
        //  2740: ifnull          2751
        //  2743: aload           19
        //  2745: aload           18
        //  2747: aload_2        
        //  2748: invokevirtual   com/google/protobuf/k0.o:(Ljava/lang/Object;Ljava/lang/Object;)V
        //  2751: return         
        //  2752: aload           18
        //  2754: astore          14
        //  2756: goto            16
        //  2759: astore_2       
        //  2760: goto            2776
        //  2763: astore_2       
        //  2764: aload           14
        //  2766: astore          4
        //  2768: goto            2776
        //  2771: astore_2       
        //  2772: aload           14
        //  2774: astore          4
        //  2776: aload_0        
        //  2777: getfield        com/google/protobuf/c0.k:I
        //  2780: istore          6
        //  2782: iload           6
        //  2784: aload_0        
        //  2785: getfield        com/google/protobuf/c0.l:I
        //  2788: if_icmpge       2815
        //  2791: aload_0        
        //  2792: aload_3        
        //  2793: aload_0        
        //  2794: getfield        com/google/protobuf/c0.j:[I
        //  2797: iload           6
        //  2799: iaload         
        //  2800: aload           13
        //  2802: aload_1        
        //  2803: aload_3        
        //  2804: invokevirtual   com/google/protobuf/c0.q:(Ljava/lang/Object;ILjava/lang/Object;Lcom/google/protobuf/k0;Ljava/lang/Object;)Ljava/lang/Object;
        //  2807: astore          13
        //  2809: iinc            6, 1
        //  2812: goto            2782
        //  2815: aload           13
        //  2817: ifnull          2829
        //  2820: aload           19
        //  2822: aload           14
        //  2824: aload           13
        //  2826: invokevirtual   com/google/protobuf/k0.o:(Ljava/lang/Object;Ljava/lang/Object;)V
        //  2829: aload_2        
        //  2830: athrow         
        //  2831: astore          15
        //  2833: goto            2466
        //  2836: astore          15
        //  2838: goto            2559
        //  2841: astore          13
        //  2843: goto            2563
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                                                         
        //  -----  -----  -----  -----  -----------------------------------------------------------------------------
        //  16     33     2771   2772   Any
        //  100    107    341    349    Any
        //  113    127    341    349    Any
        //  137    144    147    151    Any
        //  159    177    329    341    Any
        //  192    202    329    341    Any
        //  210    220    329    341    Any
        //  236    245    329    341    Any
        //  249    260    329    341    Any
        //  353    361    2763   2764   Any
        //  361    368    2557   2559   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  361    368    2763   2764   Any
        //  665    674    2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  665    674    2462   2463   Any
        //  677    709    2836   2841   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  677    709    1181   1185   Any
        //  709    721    2836   2841   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  709    721    1181   1185   Any
        //  724    744    2836   2841   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  724    744    1181   1185   Any
        //  747    767    2836   2841   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  747    767    1181   1185   Any
        //  767    777    2836   2841   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  767    777    1181   1185   Any
        //  780    800    2836   2841   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  780    800    1181   1185   Any
        //  803    823    2836   2841   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  803    823    1181   1185   Any
        //  826    843    2836   2841   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  826    843    1181   1185   Any
        //  848    860    2836   2841   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  848    860    1181   1185   Any
        //  863    878    2836   2841   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  863    878    1181   1185   Any
        //  885    900    2836   2841   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  885    900    1181   1185   Any
        //  903    923    2836   2841   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  903    923    1181   1185   Any
        //  926    943    2836   2841   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  926    943    1181   1185   Any
        //  946    978    2836   2841   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  946    978    1181   1185   Any
        //  981    991    2836   2841   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  981    991    1181   1185   Any
        //  997    1017   2836   2841   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  997    1017   1181   1185   Any
        //  1020   1040   2836   2841   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1020   1040   1181   1185   Any
        //  1043   1063   2836   2841   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1043   1063   1181   1185   Any
        //  1066   1086   2836   2841   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1066   1086   1181   1185   Any
        //  1089   1109   2836   2841   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1089   1109   1181   1185   Any
        //  1112   1132   2836   2841   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1112   1132   1181   1185   Any
        //  1135   1155   2836   2841   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1135   1155   1181   1185   Any
        //  1158   1178   2836   2841   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1158   1178   1181   1185   Any
        //  1185   1193   2557   2559   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1185   1193   2763   2764   Any
        //  1193   1206   1244   1249   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1193   1206   1240   1244   Any
        //  1209   1224   1244   1249   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1209   1224   1240   1244   Any
        //  1224   1237   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1224   1237   2462   2463   Any
        //  1249   1265   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1249   1265   2462   2463   Any
        //  1265   1274   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1265   1274   2462   2463   Any
        //  1277   1293   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1277   1293   2462   2463   Any
        //  1293   1302   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1293   1302   2462   2463   Any
        //  1305   1321   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1305   1321   2462   2463   Any
        //  1321   1330   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1321   1330   2462   2463   Any
        //  1333   1349   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1333   1349   2462   2463   Any
        //  1349   1358   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1349   1358   2462   2463   Any
        //  1361   1394   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1361   1394   2462   2463   Any
        //  1394   1409   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1394   1409   2462   2463   Any
        //  1416   1432   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1416   1432   2462   2463   Any
        //  1432   1441   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1432   1441   2462   2463   Any
        //  1444   1460   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1444   1460   2462   2463   Any
        //  1460   1469   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1460   1469   2462   2463   Any
        //  1472   1488   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1472   1488   2462   2463   Any
        //  1488   1497   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1488   1497   2462   2463   Any
        //  1500   1516   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1500   1516   2462   2463   Any
        //  1516   1525   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1516   1525   2462   2463   Any
        //  1528   1544   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1528   1544   2462   2463   Any
        //  1544   1553   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1544   1553   2462   2463   Any
        //  1556   1572   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1556   1572   2462   2463   Any
        //  1572   1581   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1572   1581   2462   2463   Any
        //  1584   1600   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1584   1600   2462   2463   Any
        //  1600   1609   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1600   1609   2462   2463   Any
        //  1612   1628   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1612   1628   2462   2463   Any
        //  1628   1637   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1628   1637   2462   2463   Any
        //  1640   1656   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1640   1656   2462   2463   Any
        //  1656   1665   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1656   1665   2462   2463   Any
        //  1668   1684   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1668   1684   2462   2463   Any
        //  1687   1703   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1687   1703   2462   2463   Any
        //  1706   1722   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1706   1722   2462   2463   Any
        //  1725   1741   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1725   1741   2462   2463   Any
        //  1744   1777   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1744   1777   2462   2463   Any
        //  1780   1796   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1780   1796   2462   2463   Any
        //  1799   1820   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1799   1820   2462   2463   Any
        //  1823   1840   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1823   1840   2462   2463   Any
        //  1843   1853   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1843   1853   2462   2463   Any
        //  1856   1872   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1856   1872   2462   2463   Any
        //  1875   1891   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1875   1891   2462   2463   Any
        //  1894   1910   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1894   1910   2462   2463   Any
        //  1913   1929   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1913   1929   2462   2463   Any
        //  1932   1948   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1932   1948   2462   2463   Any
        //  1951   1967   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1951   1967   2462   2463   Any
        //  1970   1986   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1970   1986   2462   2463   Any
        //  1989   2005   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  1989   2005   2462   2463   Any
        //  2008   2038   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  2008   2038   2462   2463   Any
        //  2038   2048   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  2038   2048   2462   2463   Any
        //  2051   2068   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  2051   2068   2462   2463   Any
        //  2068   2076   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  2068   2076   2462   2463   Any
        //  2079   2096   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  2079   2096   2462   2463   Any
        //  2099   2116   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  2099   2116   2462   2463   Any
        //  2119   2136   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  2119   2136   2462   2463   Any
        //  2139   2156   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  2139   2156   2462   2463   Any
        //  2161   2173   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  2161   2173   2462   2463   Any
        //  2176   2191   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  2176   2191   2462   2463   Any
        //  2198   2210   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  2198   2210   2462   2463   Any
        //  2213   2230   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  2213   2230   2462   2463   Any
        //  2233   2250   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  2233   2250   2462   2463   Any
        //  2253   2283   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  2253   2283   2462   2463   Any
        //  2286   2296   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  2286   2296   2462   2463   Any
        //  2299   2316   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  2299   2316   2462   2463   Any
        //  2319   2336   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  2319   2336   2462   2463   Any
        //  2339   2356   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  2339   2356   2462   2463   Any
        //  2359   2376   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  2359   2376   2462   2463   Any
        //  2379   2396   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  2379   2396   2462   2463   Any
        //  2399   2416   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  2399   2416   2462   2463   Any
        //  2419   2436   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  2419   2436   2462   2463   Any
        //  2439   2456   2831   2836   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  2439   2456   2462   2463   Any
        //  2484   2495   2841   2846   Lcom/google/protobuf/InvalidProtocolBufferException$InvalidWireTypeException;
        //  2484   2495   2759   2763   Any
        //  2567   2577   2759   2763   Any
        //  2581   2590   2759   2763   Any
        //  2666   2675   2759   2763   Any
        //  2679   2690   2759   2763   Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_2513:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public final void O(final Object o, final int n, final Object o2, final l l, final f0 f0) {
        final long y = Y(this.x0(n));
        final Object g = d12.G(o, y);
        Object o3;
        if (g == null) {
            o3 = this.q.f(o2);
            d12.W(o, y, o3);
        }
        else {
            o3 = g;
            if (this.q.h(g)) {
                o3 = this.q.f(o2);
                this.q.a(o3, g);
                d12.W(o, y, o3);
            }
        }
        f0.I(this.q.e(o3), this.q.b(o2), l);
    }
    
    public final void P(final Object o, Object o2, final int n) {
        if (!this.C(o2, n)) {
            return;
        }
        final long y = Y(this.x0(n));
        final Unsafe s = c0.s;
        final Object object = s.getObject(o2, y);
        if (object == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Source subfield ");
            sb.append(this.X(n));
            sb.append(" is present but null: ");
            sb.append(o2);
            throw new IllegalStateException(sb.toString());
        }
        final g0 v = this.v(n);
        if (!this.C(o, n)) {
            if (!H(object)) {
                s.putObject(o, y, object);
            }
            else {
                o2 = v.newInstance();
                v.a(o2, object);
                s.putObject(o, y, o2);
            }
            this.r0(o, n);
            return;
        }
        final Object o3 = o2 = s.getObject(o, y);
        if (!H(o3)) {
            o2 = v.newInstance();
            v.a(o2, o3);
            s.putObject(o, y, o2);
        }
        v.a(o2, object);
    }
    
    public final void Q(final Object o, Object o2, final int n) {
        final int x = this.X(n);
        if (!this.J(o2, x, n)) {
            return;
        }
        final long y = Y(this.x0(n));
        final Unsafe s = c0.s;
        final Object object = s.getObject(o2, y);
        if (object == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Source subfield ");
            sb.append(this.X(n));
            sb.append(" is present but null: ");
            sb.append(o2);
            throw new IllegalStateException(sb.toString());
        }
        final g0 v = this.v(n);
        if (!this.J(o, x, n)) {
            if (!H(object)) {
                s.putObject(o, y, object);
            }
            else {
                o2 = v.newInstance();
                v.a(o2, object);
                s.putObject(o, y, o2);
            }
            this.s0(o, x, n);
            return;
        }
        final Object o3 = o2 = s.getObject(o, y);
        if (!H(o3)) {
            o2 = v.newInstance();
            v.a(o2, o3);
            s.putObject(o, y, o2);
        }
        v.a(o2, object);
    }
    
    public final void R(final Object o, final Object o2, final int n) {
        final int x0 = this.x0(n);
        final long y = Y(x0);
        final int x2 = this.X(n);
        Label_0676: {
            Label_0616: {
                Label_0580: {
                    Label_0508: {
                        Label_0462: {
                            switch (w0(x0)) {
                                default: {
                                    return;
                                }
                                case 61:
                                case 62:
                                case 63:
                                case 64:
                                case 65:
                                case 66:
                                case 67: {
                                    if (this.J(o2, x2, n)) {
                                        break;
                                    }
                                    return;
                                }
                                case 60:
                                case 68: {
                                    this.Q(o, o2, n);
                                    return;
                                }
                                case 51:
                                case 52:
                                case 53:
                                case 54:
                                case 55:
                                case 56:
                                case 57:
                                case 58:
                                case 59: {
                                    if (this.J(o2, x2, n)) {
                                        break;
                                    }
                                    return;
                                }
                                case 50: {
                                    h0.E(this.q, o, o2, y);
                                    return;
                                }
                                case 18:
                                case 19:
                                case 20:
                                case 21:
                                case 22:
                                case 23:
                                case 24:
                                case 25:
                                case 26:
                                case 27:
                                case 28:
                                case 29:
                                case 30:
                                case 31:
                                case 32:
                                case 33:
                                case 34:
                                case 35:
                                case 36:
                                case 37:
                                case 38:
                                case 39:
                                case 40:
                                case 41:
                                case 42:
                                case 43:
                                case 44:
                                case 45:
                                case 46:
                                case 47:
                                case 48:
                                case 49: {
                                    this.n.d(o, o2, y);
                                    return;
                                }
                                case 16: {
                                    if (this.C(o2, n)) {
                                        break Label_0616;
                                    }
                                    return;
                                }
                                case 15: {
                                    if (this.C(o2, n)) {
                                        break Label_0462;
                                    }
                                    return;
                                }
                                case 14: {
                                    if (this.C(o2, n)) {
                                        break Label_0616;
                                    }
                                    return;
                                }
                                case 13: {
                                    if (this.C(o2, n)) {
                                        break Label_0462;
                                    }
                                    return;
                                }
                                case 12: {
                                    if (this.C(o2, n)) {
                                        break Label_0462;
                                    }
                                    return;
                                }
                                case 11: {
                                    if (this.C(o2, n)) {
                                        break Label_0580;
                                    }
                                    return;
                                }
                                case 10: {
                                    if (this.C(o2, n)) {
                                        break Label_0508;
                                    }
                                    return;
                                }
                                case 9:
                                case 17: {
                                    this.P(o, o2, n);
                                    return;
                                }
                                case 8: {
                                    if (this.C(o2, n)) {
                                        break Label_0508;
                                    }
                                    return;
                                }
                                case 7: {
                                    if (this.C(o2, n)) {
                                        d12.M(o, y, d12.t(o2, y));
                                        break Label_0676;
                                    }
                                    return;
                                }
                                case 6: {
                                    if (this.C(o2, n)) {
                                        break Label_0580;
                                    }
                                    return;
                                }
                                case 5: {
                                    if (this.C(o2, n)) {
                                        break Label_0616;
                                    }
                                    return;
                                }
                                case 4: {
                                    if (this.C(o2, n)) {
                                        break Label_0580;
                                    }
                                    return;
                                }
                                case 3: {
                                    if (this.C(o2, n)) {
                                        break Label_0616;
                                    }
                                    return;
                                }
                                case 2: {
                                    if (this.C(o2, n)) {
                                        break Label_0616;
                                    }
                                    return;
                                }
                                case 1: {
                                    if (this.C(o2, n)) {
                                        d12.T(o, y, d12.B(o2, y));
                                        break Label_0676;
                                    }
                                    return;
                                }
                                case 0: {
                                    if (this.C(o2, n)) {
                                        d12.S(o, y, d12.A(o2, y));
                                        break Label_0676;
                                    }
                                    return;
                                }
                            }
                            d12.W(o, y, d12.G(o2, y));
                            this.s0(o, x2, n);
                            return;
                        }
                        break Label_0580;
                    }
                    d12.W(o, y, d12.G(o2, y));
                    break Label_0676;
                }
                d12.U(o, y, d12.C(o2, y));
                break Label_0676;
            }
            d12.V(o, y, d12.E(o2, y));
        }
        this.r0(o, n);
    }
    
    public final Object S(Object object, final int n) {
        final g0 v = this.v(n);
        final long y = Y(this.x0(n));
        if (!this.C(object, n)) {
            return v.newInstance();
        }
        object = c0.s.getObject(object, y);
        if (H(object)) {
            return object;
        }
        final Object instance = v.newInstance();
        if (object != null) {
            v.a(instance, object);
        }
        return instance;
    }
    
    public final Object T(Object instance, final int n, final int n2) {
        final g0 v = this.v(n2);
        if (!this.J(instance, n, n2)) {
            return v.newInstance();
        }
        final Object object = c0.s.getObject(instance, Y(this.x0(n2)));
        if (H(object)) {
            return object;
        }
        instance = v.newInstance();
        if (object != null) {
            v.a(instance, object);
        }
        return instance;
    }
    
    public final int X(final int n) {
        return this.a[n];
    }
    
    @Override
    public void a(final Object o, final Object o2) {
        l(o);
        o2.getClass();
        for (int i = 0; i < this.a.length; i += 3) {
            this.R(o, o2, i);
        }
        h0.F(this.o, o, o2);
        if (this.f) {
            h0.D(this.p, o, o2);
        }
    }
    
    @Override
    public final boolean b(final Object o) {
        int n = 1048575;
        int int1 = 0;
        for (int i = 0; i < this.k; ++i) {
            final int n2 = this.j[i];
            final int x = this.X(n2);
            final int x2 = this.x0(n2);
            final int n3 = this.a[n2 + 2];
            final int n4 = n3 & 0xFFFFF;
            final int n5 = 1 << (n3 >>> 20);
            if (n4 != n) {
                if (n4 != 1048575) {
                    int1 = c0.s.getInt(o, n4);
                }
                n = n4;
            }
            if (K(x2) && !this.D(o, n2, n, int1, n5)) {
                return false;
            }
            final int w0 = w0(x2);
            if (w0 != 9 && w0 != 17) {
                if (w0 != 27) {
                    if (w0 != 60 && w0 != 68) {
                        if (w0 != 49) {
                            if (w0 != 50) {
                                continue;
                            }
                            if (!this.G(o, x2, n2)) {
                                return false;
                            }
                            continue;
                        }
                    }
                    else {
                        if (this.J(o, x, n2) && !E(o, x2, this.v(n2))) {
                            return false;
                        }
                        continue;
                    }
                }
                if (!this.F(o, x2, n2)) {
                    return false;
                }
            }
            else if (this.D(o, n2, n, int1, n5) && !E(o, x2, this.v(n2))) {
                return false;
            }
        }
        return !this.f || this.p.c(o).k();
    }
    
    @Override
    public boolean c(final Object o, final Object o2) {
        for (int length = this.a.length, i = 0; i < length; i += 3) {
            if (!this.p(o, o2, i)) {
                return false;
            }
        }
        return this.o.g(o).equals(this.o.g(o2)) && (!this.f || this.p.c(o).equals(this.p.c(o2)));
    }
    
    @Override
    public void d(final Object o) {
        if (!H(o)) {
            return;
        }
        if (o instanceof GeneratedMessageLite) {
            final GeneratedMessageLite generatedMessageLite = (GeneratedMessageLite)o;
            generatedMessageLite.r();
            generatedMessageLite.q();
            generatedMessageLite.K();
        }
        for (int length = this.a.length, i = 0; i < length; i += 3) {
            final int x0 = this.x0(i);
            final long y = Y(x0);
            final int w0 = w0(x0);
            if (w0 != 9) {
                switch (w0) {
                    default: {
                        continue;
                    }
                    case 50: {
                        final Unsafe s = c0.s;
                        final Object object = s.getObject(o, y);
                        if (object != null) {
                            s.putObject(o, y, this.q.c(object));
                        }
                        continue;
                    }
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                    case 28:
                    case 29:
                    case 30:
                    case 31:
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 40:
                    case 41:
                    case 42:
                    case 43:
                    case 44:
                    case 45:
                    case 46:
                    case 47:
                    case 48:
                    case 49: {
                        this.n.c(o, y);
                        continue;
                    }
                    case 17: {
                        break;
                    }
                }
            }
            if (this.C(o, i)) {
                this.v(i).d(c0.s.getObject(o, y));
            }
        }
        this.o.j(o);
        if (this.f) {
            this.p.f(o);
        }
    }
    
    @Override
    public int e(final Object o) {
        int n;
        if (this.h) {
            n = this.y(o);
        }
        else {
            n = this.x(o);
        }
        return n;
    }
    
    public final int e0(final Object o, final byte[] array, final int n, final int n2, final int n3, final long n4, final e.a a) {
        final Unsafe s = c0.s;
        final Object u = this.u(n3);
        Object x;
        final Object o2 = x = s.getObject(o, n4);
        if (this.q.h(o2)) {
            x = this.q.f(u);
            this.q.a(x, o2);
            s.putObject(o, n4, x);
        }
        return this.m(array, n, n2, this.q.b(u), this.q.e(x), a);
    }
    
    @Override
    public int f(final Object o) {
        final int length = this.a.length;
        int i = 0;
        int n = 0;
        while (i < length) {
            final int x0 = this.x0(i);
            final int x2 = this.X(i);
            final long y = Y(x0);
            final int w0 = w0(x0);
            int hashCode = 37;
            int n2 = 0;
            Label_1004: {
                int n3 = 0;
                int n4 = 0;
                Label_0997: {
                    long n5 = 0L;
                    Label_0990: {
                        double value2 = 0.0;
                        Label_0984: {
                            float value = 0.0f;
                            Label_0960: {
                                boolean b = false;
                                Label_0899: {
                                    Label_0847: {
                                        Object o2 = null;
                                        Label_0840: {
                                            Object o3 = null;
                                            Label_0817: {
                                                Label_0701: {
                                                    Label_0648: {
                                                        Label_0523: {
                                                            switch (w0) {
                                                                default: {
                                                                    n2 = n;
                                                                    break Label_1004;
                                                                }
                                                                case 68: {
                                                                    n2 = n;
                                                                    if (this.J(o, x2, i)) {
                                                                        break Label_0523;
                                                                    }
                                                                    break Label_1004;
                                                                }
                                                                case 67: {
                                                                    n2 = n;
                                                                    if (this.J(o, x2, i)) {
                                                                        break Label_0701;
                                                                    }
                                                                    break Label_1004;
                                                                }
                                                                case 66: {
                                                                    n2 = n;
                                                                    if (this.J(o, x2, i)) {
                                                                        break;
                                                                    }
                                                                    break Label_1004;
                                                                }
                                                                case 65: {
                                                                    n2 = n;
                                                                    if (this.J(o, x2, i)) {
                                                                        break Label_0701;
                                                                    }
                                                                    break Label_1004;
                                                                }
                                                                case 64: {
                                                                    n2 = n;
                                                                    if (this.J(o, x2, i)) {
                                                                        break;
                                                                    }
                                                                    break Label_1004;
                                                                }
                                                                case 63: {
                                                                    n2 = n;
                                                                    if (this.J(o, x2, i)) {
                                                                        break;
                                                                    }
                                                                    break Label_1004;
                                                                }
                                                                case 62: {
                                                                    n2 = n;
                                                                    if (this.J(o, x2, i)) {
                                                                        break;
                                                                    }
                                                                    break Label_1004;
                                                                }
                                                                case 60: {
                                                                    n2 = n;
                                                                    if (this.J(o, x2, i)) {
                                                                        break Label_0523;
                                                                    }
                                                                    break Label_1004;
                                                                }
                                                                case 58: {
                                                                    n2 = n;
                                                                    if (this.J(o, x2, i)) {
                                                                        n3 = n * 53;
                                                                        b = Z(o, y);
                                                                        break Label_0899;
                                                                    }
                                                                    break Label_1004;
                                                                }
                                                                case 57: {
                                                                    n2 = n;
                                                                    if (this.J(o, x2, i)) {
                                                                        break;
                                                                    }
                                                                    break Label_1004;
                                                                }
                                                                case 56: {
                                                                    n2 = n;
                                                                    if (this.J(o, x2, i)) {
                                                                        break Label_0701;
                                                                    }
                                                                    break Label_1004;
                                                                }
                                                                case 55: {
                                                                    n2 = n;
                                                                    if (this.J(o, x2, i)) {
                                                                        break;
                                                                    }
                                                                    break Label_1004;
                                                                }
                                                                case 54: {
                                                                    n2 = n;
                                                                    if (this.J(o, x2, i)) {
                                                                        break Label_0701;
                                                                    }
                                                                    break Label_1004;
                                                                }
                                                                case 53: {
                                                                    n2 = n;
                                                                    if (this.J(o, x2, i)) {
                                                                        break Label_0701;
                                                                    }
                                                                    break Label_1004;
                                                                }
                                                                case 52: {
                                                                    n2 = n;
                                                                    if (this.J(o, x2, i)) {
                                                                        n3 = n * 53;
                                                                        value = b0(o, y);
                                                                        break Label_0960;
                                                                    }
                                                                    break Label_1004;
                                                                }
                                                                case 51: {
                                                                    n2 = n;
                                                                    if (this.J(o, x2, i)) {
                                                                        n3 = n * 53;
                                                                        value2 = a0(o, y);
                                                                        break Label_0984;
                                                                    }
                                                                    break Label_1004;
                                                                }
                                                                case 17: {
                                                                    o2 = d12.G(o, y);
                                                                    if (o2 != null) {
                                                                        break Label_0840;
                                                                    }
                                                                    break Label_0847;
                                                                }
                                                                case 61: {
                                                                    n2 = n;
                                                                    if (this.J(o, x2, i))
                                                                    break Label_1004;
                                                                }
                                                                case 10:
                                                                case 18:
                                                                case 19:
                                                                case 20:
                                                                case 21:
                                                                case 22:
                                                                case 23:
                                                                case 24:
                                                                case 25:
                                                                case 26:
                                                                case 27:
                                                                case 28:
                                                                case 29:
                                                                case 30:
                                                                case 31:
                                                                case 32:
                                                                case 33:
                                                                case 34:
                                                                case 35:
                                                                case 36:
                                                                case 37:
                                                                case 38:
                                                                case 39:
                                                                case 40:
                                                                case 41:
                                                                case 42:
                                                                case 43:
                                                                case 44:
                                                                case 45:
                                                                case 46:
                                                                case 47:
                                                                case 48:
                                                                case 49:
                                                                case 50: {
                                                                    n3 = n * 53;
                                                                    o3 = d12.G(o, y);
                                                                    break Label_0817;
                                                                }
                                                                case 9: {
                                                                    o2 = d12.G(o, y);
                                                                    if (o2 != null) {
                                                                        break Label_0840;
                                                                    }
                                                                    break Label_0847;
                                                                }
                                                                case 59: {
                                                                    n2 = n;
                                                                    if (this.J(o, x2, i))
                                                                    break Label_1004;
                                                                }
                                                                case 8: {
                                                                    n3 = n * 53;
                                                                    n4 = ((String)d12.G(o, y)).hashCode();
                                                                    break Label_0997;
                                                                }
                                                                case 7: {
                                                                    n3 = n * 53;
                                                                    b = d12.t(o, y);
                                                                    break Label_0899;
                                                                }
                                                                case 4:
                                                                case 6:
                                                                case 11:
                                                                case 12:
                                                                case 13:
                                                                case 15: {
                                                                    n3 = n * 53;
                                                                    n4 = d12.C(o, y);
                                                                    break Label_0997;
                                                                }
                                                                case 2:
                                                                case 3:
                                                                case 5:
                                                                case 14:
                                                                case 16: {
                                                                    n3 = n * 53;
                                                                    n5 = d12.E(o, y);
                                                                    break Label_0990;
                                                                }
                                                                case 1: {
                                                                    n3 = n * 53;
                                                                    value = d12.B(o, y);
                                                                    break Label_0960;
                                                                }
                                                                case 0: {
                                                                    n3 = n * 53;
                                                                    value2 = d12.A(o, y);
                                                                    break Label_0984;
                                                                }
                                                            }
                                                            break Label_0648;
                                                        }
                                                        o3 = d12.G(o, y);
                                                        n3 = n * 53;
                                                        break Label_0817;
                                                    }
                                                    n3 = n * 53;
                                                    n4 = c0(o, y);
                                                    break Label_0997;
                                                }
                                                n3 = n * 53;
                                                n5 = d0(o, y);
                                                break Label_0990;
                                            }
                                            n4 = o3.hashCode();
                                            break Label_0997;
                                        }
                                        hashCode = o2.hashCode();
                                    }
                                    n2 = n * 53 + hashCode;
                                    break Label_1004;
                                }
                                n4 = t.c(b);
                                break Label_0997;
                            }
                            n4 = Float.floatToIntBits(value);
                            break Label_0997;
                        }
                        n5 = Double.doubleToLongBits(value2);
                    }
                    n4 = t.f(n5);
                }
                n2 = n3 + n4;
            }
            i += 3;
            n = n2;
        }
        int n6 = n * 53 + this.o.g(o).hashCode();
        if (this.f) {
            n6 = n6 * 53 + this.p.c(o).hashCode();
        }
        return n6;
    }
    
    public final int f0(final Object o, final byte[] bytes, int offset, int i, final int n, final int n2, final int n3, final int n4, final int n5, final long offset2, final int n6, final e.a a) {
        final Unsafe s = c0.s;
        final long n7 = this.a[n6 + 2] & 0xFFFFF;
        Label_0665: {
            Number x3 = null;
            Label_0653: {
                Number x2 = null;
                Label_0623: {
                    Object x = null;
                    Label_0596: {
                        long l = 0L;
                        Label_0590: {
                            Label_0561: {
                                Object o2 = null;
                                switch (n5) {
                                    default: {
                                        return offset;
                                    }
                                    case 68: {
                                        if (n3 == 3) {
                                            final Object t = this.T(o, n2, n6);
                                            offset = com.google.protobuf.e.M(t, this.v(n6), bytes, offset, i, (n & 0xFFFFFFF8) | 0x4, a);
                                            o2 = t;
                                            break;
                                        }
                                        return offset;
                                    }
                                    case 67: {
                                        if (n3 == 0) {
                                            offset = com.google.protobuf.e.K(bytes, offset, a);
                                            l = com.google.protobuf.g.c(a.b);
                                            break Label_0590;
                                        }
                                        return offset;
                                    }
                                    case 66: {
                                        if (n3 == 0) {
                                            offset = com.google.protobuf.e.H(bytes, offset, a);
                                            i = com.google.protobuf.g.b(a.a);
                                            break Label_0561;
                                        }
                                        return offset;
                                    }
                                    case 63: {
                                        if (n3 == 0) {
                                            offset = com.google.protobuf.e.H(bytes, offset, a);
                                            i = a.a;
                                            final t.c t2 = this.t(n6);
                                            if (t2 != null && !t2.a(i)) {
                                                w(o).n(n, (long)i);
                                            }
                                            else {
                                                s.putObject(o, offset2, i);
                                                s.putInt(o, n7, n2);
                                            }
                                            return offset;
                                        }
                                        return offset;
                                    }
                                    case 61: {
                                        if (n3 == 2) {
                                            offset = com.google.protobuf.e.b(bytes, offset, a);
                                            x = a.c;
                                            break Label_0596;
                                        }
                                        return offset;
                                    }
                                    case 60: {
                                        if (n3 == 2) {
                                            final Object t3 = this.T(o, n2, n6);
                                            offset = com.google.protobuf.e.N(t3, this.v(n6), bytes, offset, i, a);
                                            o2 = t3;
                                            break;
                                        }
                                        return offset;
                                    }
                                    case 59: {
                                        if (n3 != 2) {
                                            return offset;
                                        }
                                        offset = com.google.protobuf.e.H(bytes, offset, a);
                                        i = a.a;
                                        if (i == 0) {
                                            x = "";
                                            break Label_0596;
                                        }
                                        if ((n4 & 0x20000000) != 0x0 && !Utf8.t(bytes, offset, offset + i)) {
                                            throw InvalidProtocolBufferException.invalidUtf8();
                                        }
                                        s.putObject(o, offset2, new String(bytes, offset, i, t.b));
                                        offset += i;
                                        break Label_0665;
                                    }
                                    case 58: {
                                        if (n3 == 0) {
                                            offset = com.google.protobuf.e.K(bytes, offset, a);
                                            x = (a.b != 0L);
                                            break Label_0596;
                                        }
                                        return offset;
                                    }
                                    case 57:
                                    case 64: {
                                        if (n3 == 5) {
                                            x2 = com.google.protobuf.e.g(bytes, offset);
                                            break Label_0623;
                                        }
                                        return offset;
                                    }
                                    case 56:
                                    case 65: {
                                        if (n3 == 1) {
                                            x3 = com.google.protobuf.e.i(bytes, offset);
                                            break Label_0653;
                                        }
                                        return offset;
                                    }
                                    case 55:
                                    case 62: {
                                        if (n3 == 0) {
                                            offset = com.google.protobuf.e.H(bytes, offset, a);
                                            i = a.a;
                                            break Label_0561;
                                        }
                                        return offset;
                                    }
                                    case 53:
                                    case 54: {
                                        if (n3 == 0) {
                                            offset = com.google.protobuf.e.K(bytes, offset, a);
                                            l = a.b;
                                            break Label_0590;
                                        }
                                        return offset;
                                    }
                                    case 52: {
                                        if (n3 == 5) {
                                            x2 = com.google.protobuf.e.k(bytes, offset);
                                            break Label_0623;
                                        }
                                        return offset;
                                    }
                                    case 51: {
                                        if (n3 == 1) {
                                            x3 = com.google.protobuf.e.d(bytes, offset);
                                            break Label_0653;
                                        }
                                        return offset;
                                    }
                                }
                                this.v0(o, n2, n6, o2);
                                return offset;
                            }
                            x = i;
                            break Label_0596;
                        }
                        x = l;
                    }
                    s.putObject(o, offset2, x);
                    break Label_0665;
                }
                s.putObject(o, offset2, x2);
                offset += 4;
                break Label_0665;
            }
            s.putObject(o, offset2, x3);
            offset += 8;
        }
        s.putInt(o, n7, n2);
        return offset;
    }
    
    @Override
    public void g(final Object o, final Writer writer) {
        if (writer.B() == Writer.FieldOrder.DESCENDING) {
            this.A0(o, writer);
        }
        else if (this.h) {
            this.z0(o, writer);
        }
        else {
            this.y0(o, writer);
        }
    }
    
    public int g0(final Object o, final byte[] array, int i, final int n, int n2, final e.a a) {
        int n3 = n2;
        l(o);
        final Unsafe s = c0.s;
        int n4 = 0;
        int x;
        int n5 = x = 0;
        int n6 = -1;
        int n7 = 1048575;
        while (true) {
            while (i < n) {
                int g = i + 1;
                i = array[i];
                if (i < 0) {
                    g = com.google.protobuf.e.G(i, array, g, a);
                    i = a.a;
                }
                final int n8 = i >>> 3;
                final int n9 = i & 0x7;
                int n10;
                if (n8 > n6) {
                    n10 = this.k0(n8, n4 / 3);
                }
                else {
                    n10 = this.j0(n8);
                }
                int n11;
                int n13;
                int n14;
                int n15;
                int n16;
                if (n10 == -1) {
                    n11 = g;
                    final int n12 = x;
                    n13 = n7;
                    n14 = n3;
                    n15 = 0;
                    n16 = n12;
                }
                else {
                    final int n17 = this.a[n10 + 1];
                    final int w0 = w0(n17);
                    final long y = Y(n17);
                    if (w0 <= 17) {
                        final int n18 = this.a[n10 + 2];
                        final int n19 = 1 << (n18 >>> 20);
                        final int n20 = n18 & 0xFFFFF;
                        int n21;
                        if (n20 != n7) {
                            if (n7 != 1048575) {
                                s.putInt(o, n7, x);
                            }
                            final int int1 = s.getInt(o, n20);
                            n7 = n20;
                            n21 = int1;
                        }
                        else {
                            n21 = x;
                        }
                        Label_1021: {
                            int n24 = 0;
                            Label_0989: {
                                int n25 = 0;
                                Label_0970: {
                                    int n23 = 0;
                                    Label_0955: {
                                        Label_0921: {
                                            int n22 = 0;
                                            long x2 = 0L;
                                            Label_0875: {
                                                int x3 = 0;
                                                Label_0840: {
                                                    switch (w0) {
                                                        default: {
                                                            break Label_1021;
                                                        }
                                                        case 17: {
                                                            if (n9 == 3) {
                                                                final Object s2 = this.S(o, n10);
                                                                final int m = com.google.protobuf.e.M(s2, this.v(n10), array, g, n, n8 << 3 | 0x4, a);
                                                                this.u0(o, n10, s2);
                                                                x = (n21 | n19);
                                                                n3 = n2;
                                                                n4 = n10;
                                                                n5 = i;
                                                                i = m;
                                                                n6 = n8;
                                                                continue;
                                                            }
                                                            break Label_1021;
                                                        }
                                                        case 16: {
                                                            if (n9 == 0) {
                                                                n22 = com.google.protobuf.e.K(array, g, a);
                                                                x2 = com.google.protobuf.g.c(a.b);
                                                                break Label_0875;
                                                            }
                                                            break Label_1021;
                                                        }
                                                        case 15: {
                                                            if (n9 == 0) {
                                                                n23 = com.google.protobuf.e.H(array, g, a);
                                                                x3 = com.google.protobuf.g.b(a.a);
                                                                break Label_0840;
                                                            }
                                                            break Label_1021;
                                                        }
                                                        case 12: {
                                                            if (n9 != 0) {
                                                                break Label_1021;
                                                            }
                                                            final int h = com.google.protobuf.e.H(array, g, a);
                                                            final int a2 = a.a;
                                                            final t.c t = this.t(n10);
                                                            n23 = h;
                                                            x3 = a2;
                                                            if (t == null) {
                                                                break Label_0840;
                                                            }
                                                            if (t.a(a2)) {
                                                                n23 = h;
                                                                x3 = a2;
                                                                break Label_0840;
                                                            }
                                                            w(o).n(i, (long)a2);
                                                            x = n21;
                                                            n24 = h;
                                                            break Label_0989;
                                                        }
                                                        case 10: {
                                                            if (n9 == 2) {
                                                                n23 = com.google.protobuf.e.b(array, g, a);
                                                                break;
                                                            }
                                                            break Label_1021;
                                                        }
                                                        case 9: {
                                                            if (n9 == 2) {
                                                                final Object s3 = this.S(o, n10);
                                                                n23 = com.google.protobuf.e.N(s3, this.v(n10), array, g, n, a);
                                                                this.u0(o, n10, s3);
                                                                break Label_0955;
                                                            }
                                                            break Label_1021;
                                                        }
                                                        case 8: {
                                                            if (n9 != 2) {
                                                                break Label_1021;
                                                            }
                                                            if ((0x20000000 & n17) == 0x0) {
                                                                n23 = com.google.protobuf.e.B(array, g, a);
                                                                break;
                                                            }
                                                            n23 = com.google.protobuf.e.E(array, g, a);
                                                            break;
                                                        }
                                                        case 7: {
                                                            if (n9 == 0) {
                                                                n23 = com.google.protobuf.e.K(array, g, a);
                                                                d12.M(o, y, a.b != 0L);
                                                                break Label_0955;
                                                            }
                                                            break Label_1021;
                                                        }
                                                        case 6:
                                                        case 13: {
                                                            if (n9 == 5) {
                                                                s.putInt(o, y, com.google.protobuf.e.g(array, g));
                                                                break Label_0921;
                                                            }
                                                            break Label_1021;
                                                        }
                                                        case 5:
                                                        case 14: {
                                                            if (n9 == 1) {
                                                                s.putLong(o, y, com.google.protobuf.e.i(array, g));
                                                                n23 = g + 8;
                                                                break Label_0955;
                                                            }
                                                            break Label_1021;
                                                        }
                                                        case 4:
                                                        case 11: {
                                                            if (n9 == 0) {
                                                                n23 = com.google.protobuf.e.H(array, g, a);
                                                                x3 = a.a;
                                                                break Label_0840;
                                                            }
                                                            break Label_1021;
                                                        }
                                                        case 2:
                                                        case 3: {
                                                            if (n9 == 0) {
                                                                n22 = com.google.protobuf.e.K(array, g, a);
                                                                x2 = a.b;
                                                                break Label_0875;
                                                            }
                                                            break Label_1021;
                                                        }
                                                        case 1: {
                                                            if (n9 == 5) {
                                                                d12.T(o, y, com.google.protobuf.e.k(array, g));
                                                                break Label_0921;
                                                            }
                                                            break Label_1021;
                                                        }
                                                        case 0: {
                                                            if (n9 == 1) {
                                                                d12.S(o, y, com.google.protobuf.e.d(array, g));
                                                                n23 = g + 8;
                                                                break Label_0955;
                                                            }
                                                            break Label_1021;
                                                        }
                                                    }
                                                    s.putObject(o, y, a.c);
                                                    break Label_0955;
                                                }
                                                s.putInt(o, y, x3);
                                                break Label_0955;
                                            }
                                            s.putLong(o, y, x2);
                                            n25 = (n21 | n19);
                                            n24 = n22;
                                            break Label_0970;
                                        }
                                        n23 = g + 4;
                                    }
                                    n25 = (n21 | n19);
                                    n24 = n23;
                                }
                                x = n25;
                            }
                            final int n26 = i;
                            n6 = n8;
                            final int n27 = n2;
                            i = n24;
                            n4 = n10;
                            n5 = n26;
                            n3 = n27;
                            continue;
                        }
                        n13 = n7;
                        final int n28 = n2;
                        n11 = g;
                        final int n29 = n10;
                        n14 = n28;
                        n16 = n21;
                        n15 = n29;
                    }
                    else {
                        final int n30 = n7;
                        n16 = x;
                        Label_1350: {
                            Label_1346: {
                                if (w0 != 27) {
                                    int n31 = 0;
                                    Label_1417: {
                                        int n32;
                                        if (w0 <= 49) {
                                            final int i2 = this.i0(o, array, g, n, i, n8, n9, n10, n17, w0, y, a);
                                            if ((n31 = i2) == g) {
                                                break Label_1417;
                                            }
                                            n32 = i2;
                                        }
                                        else if (w0 == 50) {
                                            if (n9 != 2) {
                                                break Label_1346;
                                            }
                                            final int e0 = this.e0(o, array, g, n, n10, y, a);
                                            if ((n31 = e0) == g) {
                                                break Label_1417;
                                            }
                                            n32 = e0;
                                        }
                                        else {
                                            final int f0 = this.f0(o, array, g, n, i, n8, n9, n17, w0, y, n10, a);
                                            if ((n31 = f0) == g) {
                                                break Label_1417;
                                            }
                                            n32 = f0;
                                        }
                                        final int n33 = n2;
                                        n4 = n10;
                                        n5 = i;
                                        i = n32;
                                        n6 = n8;
                                        x = n16;
                                        n7 = n30;
                                        n3 = n33;
                                        continue;
                                    }
                                    n11 = n31;
                                    break Label_1350;
                                }
                                if (n9 == 2) {
                                    Object j;
                                    final Object o2 = j = s.getObject(o, y);
                                    if (!((t.e)o2).h()) {
                                        final int size = ((List)o2).size();
                                        int n34;
                                        if (size == 0) {
                                            n34 = 10;
                                        }
                                        else {
                                            n34 = size * 2;
                                        }
                                        j = ((t.e)o2).j(n34);
                                        s.putObject(o, y, j);
                                    }
                                    final int p6 = com.google.protobuf.e.p(this.v(n10), i, array, g, n, (t.e)j, a);
                                    n4 = n10;
                                    n5 = i;
                                    x = n16;
                                    n6 = n8;
                                    final int n35 = n2;
                                    i = p6;
                                    n7 = n30;
                                    n3 = n35;
                                    continue;
                                }
                            }
                            n11 = g;
                        }
                        final int n36 = n10;
                        n14 = n2;
                        n13 = n30;
                        n15 = n36;
                    }
                }
                if (i == n14 && n14 != 0) {
                    n2 = n11;
                    final int x4 = n16;
                    if (n13 != 1048575) {
                        s.putInt(o, n13, x4);
                    }
                    int k = this.k;
                    Object o3 = null;
                    while (k < this.l) {
                        o3 = this.q(o, this.j[k], o3, this.o, o);
                        ++k;
                    }
                    if (o3 != null) {
                        this.o.o(o, o3);
                    }
                    if (n14 == 0) {
                        if (n2 != n) {
                            throw InvalidProtocolBufferException.parseFailure();
                        }
                    }
                    else if (n2 > n || i != n14) {
                        throw InvalidProtocolBufferException.parseFailure();
                    }
                    return n2;
                }
                int n37;
                if (this.f && a.d != com.google.protobuf.l.b()) {
                    n37 = com.google.protobuf.e.f(i, array, n11, n, o, this.e, this.o, a);
                }
                else {
                    n37 = com.google.protobuf.e.F(i, array, n11, n, w(o), a);
                }
                final int n38 = i;
                final int n39 = n13;
                x = n16;
                n4 = n15;
                n3 = n14;
                i = n37;
                n6 = n8;
                n5 = n38;
                n7 = n39;
            }
            n2 = i;
            i = n5;
            int n14 = n3;
            final int x4 = x;
            int n13 = n7;
            continue;
        }
    }
    
    @Override
    public void h(final Object o, final byte[] array, final int n, final int n2, final e.a a) {
        if (this.h) {
            this.h0(o, array, n, n2, a);
        }
        else {
            this.g0(o, array, n, n2, 0, a);
        }
    }
    
    public final int h0(final Object o, final byte[] array, int n, final int n2, final e.a a) {
        l(o);
        final Unsafe s = c0.s;
        int n3 = -1;
        int i = n;
        n = -1;
        int n4 = 0;
        int int1 = 0;
        int n5 = 1048575;
        while (i < n2) {
            final int n6 = i + 1;
            int a2 = array[i];
            int g;
            if (a2 < 0) {
                g = com.google.protobuf.e.G(a2, array, n6, a);
                a2 = a.a;
            }
            else {
                g = n6;
            }
            final int n7 = a2 >>> 3;
            final int n8 = a2 & 0x7;
            if (n7 > n) {
                n = this.k0(n7, n4 / 3);
            }
            else {
                n = this.j0(n7);
            }
            Label_1220: {
                int n19 = 0;
                int n20 = 0;
                Label_1188: {
                    if (n != n3) {
                        final int n9 = this.a[n + 1];
                        final int w0 = w0(n9);
                        final long y = Y(n9);
                        int n14 = 0;
                        int n15 = 0;
                        Label_0985: {
                            if (w0 <= 17) {
                                final int n10 = this.a[n + 2];
                                final int n11 = 1 << (n10 >>> 20);
                                final int n12 = n10 & 0xFFFFF;
                                int n13 = int1;
                                if (n12 != (n14 = n5)) {
                                    if (n5 != 1048575) {
                                        s.putInt(o, n5, int1);
                                    }
                                    if (n12 != 1048575) {
                                        int1 = s.getInt(o, n12);
                                    }
                                    n14 = n12;
                                    n13 = int1;
                                }
                                Label_0843: {
                                    int n16 = 0;
                                    int n17 = 0;
                                    Label_0829: {
                                        Label_0822: {
                                            Label_0815: {
                                                Label_0528: {
                                                    Label_0477: {
                                                        switch (w0) {
                                                            case 16: {
                                                                if (n8 == 0) {
                                                                    n15 = com.google.protobuf.e.K(array, g, a);
                                                                    s.putLong(o, y, com.google.protobuf.g.c(a.b));
                                                                    int1 = (n13 | n11);
                                                                    break Label_0985;
                                                                }
                                                                break;
                                                            }
                                                            case 15: {
                                                                if (n8 == 0) {
                                                                    n16 = com.google.protobuf.e.H(array, g, a);
                                                                    s.putInt(o, y, com.google.protobuf.g.b(a.a));
                                                                    break Label_0528;
                                                                }
                                                                break;
                                                            }
                                                            case 12: {
                                                                if (n8 == 0) {
                                                                    n16 = com.google.protobuf.e.H(array, g, a);
                                                                    s.putInt(o, y, a.a);
                                                                    break Label_0477;
                                                                }
                                                                break;
                                                            }
                                                            case 10: {
                                                                if (n8 == 2) {
                                                                    n16 = com.google.protobuf.e.b(array, g, a);
                                                                    s.putObject(o, y, a.c);
                                                                    break Label_0477;
                                                                }
                                                                break;
                                                            }
                                                            case 9: {
                                                                if (n8 == 2) {
                                                                    final Object s2 = this.S(o, n);
                                                                    n16 = com.google.protobuf.e.N(s2, this.v(n), array, g, n2, a);
                                                                    this.u0(o, n, s2);
                                                                    break Label_0528;
                                                                }
                                                                break;
                                                            }
                                                            case 8: {
                                                                if (n8 == 2) {
                                                                    if ((0x20000000 & n9) == 0x0) {
                                                                        n16 = com.google.protobuf.e.B(array, g, a);
                                                                    }
                                                                    else {
                                                                        n16 = com.google.protobuf.e.E(array, g, a);
                                                                    }
                                                                    s.putObject(o, y, a.c);
                                                                    break Label_0822;
                                                                }
                                                                break;
                                                            }
                                                            case 7: {
                                                                boolean b = true;
                                                                if (n8 == 0) {
                                                                    n16 = com.google.protobuf.e.K(array, g, a);
                                                                    if (a.b == 0L) {
                                                                        b = false;
                                                                    }
                                                                    d12.M(o, y, b);
                                                                    n17 = (n13 | n11);
                                                                    break Label_0829;
                                                                }
                                                                break;
                                                            }
                                                            case 6:
                                                            case 13: {
                                                                if (n8 == 5) {
                                                                    s.putInt(o, y, com.google.protobuf.e.g(array, g));
                                                                    n16 = g + 4;
                                                                    break Label_0822;
                                                                }
                                                                break;
                                                            }
                                                            case 5:
                                                            case 14: {
                                                                if (n8 == 1) {
                                                                    s.putLong(o, y, com.google.protobuf.e.i(array, g));
                                                                    break Label_0815;
                                                                }
                                                                break;
                                                            }
                                                            case 4:
                                                            case 11: {
                                                                if (n8 == 0) {
                                                                    n16 = com.google.protobuf.e.H(array, g, a);
                                                                    s.putInt(o, y, a.a);
                                                                    break Label_0822;
                                                                }
                                                                break;
                                                            }
                                                            case 2:
                                                            case 3: {
                                                                if (n8 == 0) {
                                                                    n15 = com.google.protobuf.e.K(array, g, a);
                                                                    s.putLong(o, y, a.b);
                                                                    int1 = (n13 | n11);
                                                                    break Label_0985;
                                                                }
                                                                break;
                                                            }
                                                            case 1: {
                                                                if (n8 == 5) {
                                                                    d12.T(o, y, com.google.protobuf.e.k(array, g));
                                                                    n16 = g + 4;
                                                                    break Label_0822;
                                                                }
                                                                break;
                                                            }
                                                            case 0: {
                                                                if (n8 == 1) {
                                                                    d12.S(o, y, com.google.protobuf.e.d(array, g));
                                                                    break Label_0815;
                                                                }
                                                                break;
                                                            }
                                                        }
                                                        break Label_0843;
                                                    }
                                                    n17 = (n13 | n11);
                                                    break Label_0829;
                                                }
                                                n17 = (n13 | n11);
                                                break Label_0829;
                                                break Label_0843;
                                            }
                                            n16 = g + 8;
                                        }
                                        n17 = (n13 | n11);
                                    }
                                    n15 = n16;
                                    int1 = n17;
                                    break Label_0985;
                                }
                                int1 = n13;
                                final int n18 = -1;
                                n19 = g;
                                n5 = n14;
                                n20 = n18;
                                break Label_1188;
                            }
                            Label_1130: {
                                Label_1126: {
                                    if (w0 != 27) {
                                        int n21 = 0;
                                        Label_1181: {
                                            if (w0 <= 49) {
                                                final int i2 = this.i0(o, array, g, n2, a2, n7, n8, n, n9, w0, y, a);
                                                if ((n21 = i2) == g) {
                                                    break Label_1181;
                                                }
                                                i = i2;
                                            }
                                            else if (w0 == 50) {
                                                if (n8 != 2) {
                                                    break Label_1126;
                                                }
                                                final int e0 = this.e0(o, array, g, n2, n, y, a);
                                                if ((n21 = e0) == g) {
                                                    break Label_1181;
                                                }
                                                i = e0;
                                            }
                                            else {
                                                final int f0 = this.f0(o, array, g, n2, a2, n7, n8, n9, w0, y, n, a);
                                                if ((n21 = f0) == g) {
                                                    break Label_1181;
                                                }
                                                i = f0;
                                            }
                                            final int n22 = -1;
                                            n4 = n;
                                            n = n22;
                                            break Label_1220;
                                        }
                                        n19 = n21;
                                        break Label_1130;
                                    }
                                    if (n8 == 2) {
                                        Object j;
                                        final Object o2 = j = s.getObject(o, y);
                                        if (!((t.e)o2).h()) {
                                            final int size = ((List)o2).size();
                                            int n23;
                                            if (size == 0) {
                                                n23 = 10;
                                            }
                                            else {
                                                n23 = size * 2;
                                            }
                                            j = ((t.e)o2).j(n23);
                                            s.putObject(o, y, j);
                                        }
                                        final int p5 = com.google.protobuf.e.p(this.v(n), a2, array, g, n2, (t.e)j, a);
                                        n14 = n5;
                                        n15 = p5;
                                        break Label_0985;
                                    }
                                }
                                n19 = g;
                            }
                            n20 = -1;
                            break Label_1188;
                        }
                        n4 = n;
                        n = n7;
                        final int n24 = -1;
                        i = n15;
                        n5 = n14;
                        n3 = n24;
                        continue;
                    }
                    n19 = g;
                    n20 = n3;
                    n = 0;
                }
                final int f2 = com.google.protobuf.e.F(a2, array, n19, n2, w(o), a);
                n4 = n;
                n = n20;
                i = f2;
            }
            n3 = n;
            n = n7;
        }
        if (n5 != 1048575) {
            s.putInt(o, n5, int1);
        }
        if (i == n2) {
            return i;
        }
        throw InvalidProtocolBufferException.parseFailure();
    }
    
    @Override
    public void i(final Object o, final f0 f0, final l l) {
        l.getClass();
        l(o);
        this.N(this.o, this.p, o, f0, l);
    }
    
    public final int i0(final Object o, final byte[] array, int n, final int n2, final int n3, final int n4, final int n5, final int n6, final long n7, final int n8, final long n9, final e.a a) {
        final Unsafe s = c0.s;
        Object j;
        final Object o2 = j = s.getObject(o, n9);
        if (!((t.e)o2).h()) {
            final int size = ((List)o2).size();
            int n10;
            if (size == 0) {
                n10 = 10;
            }
            else {
                n10 = size * 2;
            }
            j = ((t.e)o2).j(n10);
            s.putObject(o, n9, j);
        }
        switch (n8) {
            case 49: {
                if (n5 == 3) {
                    n = com.google.protobuf.e.n(this.v(n6), n3, array, n, n2, (t.e)j, a);
                    break;
                }
                break;
            }
            case 34:
            case 48: {
                if (n5 == 2) {
                    n = com.google.protobuf.e.w(array, n, (t.e)j, a);
                    break;
                }
                if (n5 == 0) {
                    n = com.google.protobuf.e.A(n3, array, n, n2, (t.e)j, a);
                    break;
                }
                break;
            }
            case 33:
            case 47: {
                if (n5 == 2) {
                    n = com.google.protobuf.e.v(array, n, (t.e)j, a);
                    break;
                }
                if (n5 == 0) {
                    n = com.google.protobuf.e.z(n3, array, n, n2, (t.e)j, a);
                    break;
                }
                break;
            }
            case 30:
            case 44: {
                if (n5 == 2) {
                    n = com.google.protobuf.e.x(array, n, (t.e)j, a);
                }
                else {
                    if (n5 != 0) {
                        break;
                    }
                    n = com.google.protobuf.e.I(n3, array, n, n2, (t.e)j, a);
                }
                h0.z(o, n4, (List)j, this.t(n6), null, this.o);
                break;
            }
            case 28: {
                if (n5 == 2) {
                    n = com.google.protobuf.e.c(n3, array, n, n2, (t.e)j, a);
                    break;
                }
                break;
            }
            case 27: {
                if (n5 == 2) {
                    n = com.google.protobuf.e.p(this.v(n6), n3, array, n, n2, (t.e)j, a);
                    break;
                }
                break;
            }
            case 26: {
                if (n5 != 2) {
                    break;
                }
                if ((n7 & 0x20000000L) == 0x0L) {
                    n = com.google.protobuf.e.C(n3, array, n, n2, (t.e)j, a);
                    break;
                }
                n = com.google.protobuf.e.D(n3, array, n, n2, (t.e)j, a);
                break;
            }
            case 25:
            case 42: {
                if (n5 == 2) {
                    n = com.google.protobuf.e.q(array, n, (t.e)j, a);
                    break;
                }
                if (n5 == 0) {
                    n = com.google.protobuf.e.a(n3, array, n, n2, (t.e)j, a);
                    break;
                }
                break;
            }
            case 24:
            case 31:
            case 41:
            case 45: {
                if (n5 == 2) {
                    n = com.google.protobuf.e.s(array, n, (t.e)j, a);
                    break;
                }
                if (n5 == 5) {
                    n = com.google.protobuf.e.h(n3, array, n, n2, (t.e)j, a);
                    break;
                }
                break;
            }
            case 23:
            case 32:
            case 40:
            case 46: {
                if (n5 == 2) {
                    n = com.google.protobuf.e.t(array, n, (t.e)j, a);
                    break;
                }
                if (n5 == 1) {
                    n = com.google.protobuf.e.j(n3, array, n, n2, (t.e)j, a);
                    break;
                }
                break;
            }
            case 22:
            case 29:
            case 39:
            case 43: {
                if (n5 == 2) {
                    n = com.google.protobuf.e.x(array, n, (t.e)j, a);
                    break;
                }
                if (n5 == 0) {
                    n = com.google.protobuf.e.I(n3, array, n, n2, (t.e)j, a);
                    break;
                }
                break;
            }
            case 20:
            case 21:
            case 37:
            case 38: {
                if (n5 == 2) {
                    n = com.google.protobuf.e.y(array, n, (t.e)j, a);
                    break;
                }
                if (n5 == 0) {
                    n = com.google.protobuf.e.L(n3, array, n, n2, (t.e)j, a);
                    break;
                }
                break;
            }
            case 19:
            case 36: {
                if (n5 == 2) {
                    n = com.google.protobuf.e.u(array, n, (t.e)j, a);
                    break;
                }
                if (n5 == 5) {
                    n = com.google.protobuf.e.l(n3, array, n, n2, (t.e)j, a);
                    break;
                }
                break;
            }
            case 18:
            case 35: {
                if (n5 == 2) {
                    n = com.google.protobuf.e.r(array, n, (t.e)j, a);
                    break;
                }
                if (n5 == 1) {
                    n = com.google.protobuf.e.e(n3, array, n, n2, (t.e)j, a);
                    break;
                }
                break;
            }
        }
        return n;
    }
    
    public final boolean j(final Object o, final Object o2, final int n) {
        return this.C(o, n) == this.C(o2, n);
    }
    
    public final int j0(final int n) {
        if (n >= this.c && n <= this.d) {
            return this.t0(n, 0);
        }
        return -1;
    }
    
    public final int k0(final int n, final int n2) {
        if (n >= this.c && n <= this.d) {
            return this.t0(n, n2);
        }
        return -1;
    }
    
    public final int l0(final int n) {
        return this.a[n + 2];
    }
    
    public final int m(final byte[] array, int i, final int n, final x.a a, final Map map, final e.a a2) {
        i = com.google.protobuf.e.H(array, i, a2);
        final int a3 = a2.a;
        if (a3 < 0 || a3 > n - i) {
            throw InvalidProtocolBufferException.truncatedMessage();
        }
        final int n2 = i + a3;
        Object o = a.b;
        Object o2 = a.d;
        while (i < n2) {
            final int n3 = i + 1;
            int a4 = array[i];
            if (a4 < 0) {
                i = com.google.protobuf.e.G(a4, array, n3, a2);
                a4 = a2.a;
            }
            else {
                i = n3;
            }
            final int n4 = a4 >>> 3;
            final int n5 = a4 & 0x7;
            if (n4 != 1) {
                if (n4 == 2) {
                    if (n5 == a.c.getWireType()) {
                        i = this.n(array, i, n, a.c, a.d.getClass(), a2);
                        o2 = a2.c;
                        continue;
                    }
                }
            }
            else if (n5 == a.a.getWireType()) {
                i = this.n(array, i, n, a.a, null, a2);
                o = a2.c;
                continue;
            }
            i = com.google.protobuf.e.O(a4, array, i, n, a2);
        }
        if (i == n2) {
            map.put(o, o2);
            return n2;
        }
        throw InvalidProtocolBufferException.parseFailure();
    }
    
    public final void m0(final Object o, final long n, final f0 f0, final g0 g0, final l l) {
        f0.L(this.n.e(o, n), g0, l);
    }
    
    public final int n(final byte[] array, int n, int i, final WireFormat.FieldType fieldType, final Class clazz, final e.a a) {
        Number c2 = null;
        Label_0281: {
            Number c = null;
            Label_0260: {
                Serializable c3 = null;
                Label_0218: {
                    Label_0213: {
                        long l = 0L;
                        switch (c0$a.a[fieldType.ordinal()]) {
                            default: {
                                throw new RuntimeException("unsupported field type.");
                            }
                            case 17: {
                                n = com.google.protobuf.e.E(array, n, a);
                                return n;
                            }
                            case 16: {
                                n = com.google.protobuf.e.K(array, n, a);
                                l = com.google.protobuf.g.c(a.b);
                                break;
                            }
                            case 15: {
                                n = com.google.protobuf.e.H(array, n, a);
                                i = com.google.protobuf.g.b(a.a);
                                break Label_0213;
                            }
                            case 14: {
                                n = com.google.protobuf.e.o(k91.a().c(clazz), array, n, i, a);
                                return n;
                            }
                            case 12:
                            case 13: {
                                n = com.google.protobuf.e.K(array, n, a);
                                l = a.b;
                                break;
                            }
                            case 9:
                            case 10:
                            case 11: {
                                n = com.google.protobuf.e.H(array, n, a);
                                i = a.a;
                                break Label_0213;
                            }
                            case 8: {
                                c = com.google.protobuf.e.k(array, n);
                                break Label_0260;
                            }
                            case 6:
                            case 7: {
                                c2 = com.google.protobuf.e.i(array, n);
                                break Label_0281;
                            }
                            case 4:
                            case 5: {
                                c = com.google.protobuf.e.g(array, n);
                                break Label_0260;
                            }
                            case 3: {
                                c2 = com.google.protobuf.e.d(array, n);
                                break Label_0281;
                            }
                            case 2: {
                                n = com.google.protobuf.e.b(array, n, a);
                                return n;
                            }
                            case 1: {
                                n = com.google.protobuf.e.K(array, n, a);
                                c3 = (a.b != 0L);
                                break Label_0218;
                            }
                        }
                        c3 = l;
                        break Label_0218;
                    }
                    c3 = i;
                }
                a.c = c3;
                return n;
            }
            a.c = c;
            n += 4;
            return n;
        }
        a.c = c2;
        n += 8;
        return n;
    }
    
    public final void n0(final Object o, final int n, final f0 f0, final g0 g0, final l l) {
        f0.M(this.n.e(o, Y(n)), g0, l);
    }
    
    @Override
    public Object newInstance() {
        return this.m.a(this.e);
    }
    
    public final void o0(final Object o, final int n, final f0 f0) {
        long n2;
        Serializable s;
        if (B(n)) {
            n2 = Y(n);
            s = f0.H();
        }
        else if (this.g) {
            n2 = Y(n);
            s = f0.F();
        }
        else {
            n2 = Y(n);
            s = f0.g();
        }
        d12.W(o, n2, s);
    }
    
    public final boolean p(final Object o, final Object o2, final int n) {
        final int x0 = this.x0(n);
        final long y = Y(x0);
        final int w0 = w0(x0);
        final boolean b = false;
        final boolean b2 = false;
        final boolean b3 = false;
        final boolean b4 = false;
        final boolean b5 = false;
        final boolean b6 = false;
        final boolean b7 = false;
        final boolean b8 = false;
        final boolean b9 = false;
        final boolean b10 = false;
        final boolean b11 = false;
        final boolean b12 = false;
        final boolean b13 = false;
        final boolean b14 = false;
        final boolean b15 = false;
        final boolean b16 = false;
        final boolean b17 = false;
        final boolean b18 = false;
        final boolean b19 = false;
        switch (w0) {
            default: {
                return true;
            }
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
            case 58:
            case 59:
            case 60:
            case 61:
            case 62:
            case 63:
            case 64:
            case 65:
            case 66:
            case 67:
            case 68: {
                boolean b20 = b19;
                if (this.I(o, o2, n)) {
                    b20 = b19;
                    if (h0.J(d12.G(o, y), d12.G(o2, y))) {
                        b20 = true;
                    }
                }
                return b20;
            }
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            case 42:
            case 43:
            case 44:
            case 45:
            case 46:
            case 47:
            case 48:
            case 49:
            case 50: {
                return h0.J(d12.G(o, y), d12.G(o2, y));
            }
            case 17: {
                boolean b21 = b;
                if (this.j(o, o2, n)) {
                    b21 = b;
                    if (h0.J(d12.G(o, y), d12.G(o2, y))) {
                        b21 = true;
                    }
                }
                return b21;
            }
            case 16: {
                boolean b22 = b2;
                if (this.j(o, o2, n)) {
                    b22 = b2;
                    if (d12.E(o, y) == d12.E(o2, y)) {
                        b22 = true;
                    }
                }
                return b22;
            }
            case 15: {
                boolean b23 = b3;
                if (this.j(o, o2, n)) {
                    b23 = b3;
                    if (d12.C(o, y) == d12.C(o2, y)) {
                        b23 = true;
                    }
                }
                return b23;
            }
            case 14: {
                boolean b24 = b4;
                if (this.j(o, o2, n)) {
                    b24 = b4;
                    if (d12.E(o, y) == d12.E(o2, y)) {
                        b24 = true;
                    }
                }
                return b24;
            }
            case 13: {
                boolean b25 = b5;
                if (this.j(o, o2, n)) {
                    b25 = b5;
                    if (d12.C(o, y) == d12.C(o2, y)) {
                        b25 = true;
                    }
                }
                return b25;
            }
            case 12: {
                boolean b26 = b6;
                if (this.j(o, o2, n)) {
                    b26 = b6;
                    if (d12.C(o, y) == d12.C(o2, y)) {
                        b26 = true;
                    }
                }
                return b26;
            }
            case 11: {
                boolean b27 = b7;
                if (this.j(o, o2, n)) {
                    b27 = b7;
                    if (d12.C(o, y) == d12.C(o2, y)) {
                        b27 = true;
                    }
                }
                return b27;
            }
            case 10: {
                boolean b28 = b8;
                if (this.j(o, o2, n)) {
                    b28 = b8;
                    if (h0.J(d12.G(o, y), d12.G(o2, y))) {
                        b28 = true;
                    }
                }
                return b28;
            }
            case 9: {
                boolean b29 = b9;
                if (this.j(o, o2, n)) {
                    b29 = b9;
                    if (h0.J(d12.G(o, y), d12.G(o2, y))) {
                        b29 = true;
                    }
                }
                return b29;
            }
            case 8: {
                boolean b30 = b10;
                if (this.j(o, o2, n)) {
                    b30 = b10;
                    if (h0.J(d12.G(o, y), d12.G(o2, y))) {
                        b30 = true;
                    }
                }
                return b30;
            }
            case 7: {
                boolean b31 = b11;
                if (this.j(o, o2, n)) {
                    b31 = b11;
                    if (d12.t(o, y) == d12.t(o2, y)) {
                        b31 = true;
                    }
                }
                return b31;
            }
            case 6: {
                boolean b32 = b12;
                if (this.j(o, o2, n)) {
                    b32 = b12;
                    if (d12.C(o, y) == d12.C(o2, y)) {
                        b32 = true;
                    }
                }
                return b32;
            }
            case 5: {
                boolean b33 = b13;
                if (this.j(o, o2, n)) {
                    b33 = b13;
                    if (d12.E(o, y) == d12.E(o2, y)) {
                        b33 = true;
                    }
                }
                return b33;
            }
            case 4: {
                boolean b34 = b14;
                if (this.j(o, o2, n)) {
                    b34 = b14;
                    if (d12.C(o, y) == d12.C(o2, y)) {
                        b34 = true;
                    }
                }
                return b34;
            }
            case 3: {
                boolean b35 = b15;
                if (this.j(o, o2, n)) {
                    b35 = b15;
                    if (d12.E(o, y) == d12.E(o2, y)) {
                        b35 = true;
                    }
                }
                return b35;
            }
            case 2: {
                boolean b36 = b16;
                if (this.j(o, o2, n)) {
                    b36 = b16;
                    if (d12.E(o, y) == d12.E(o2, y)) {
                        b36 = true;
                    }
                }
                return b36;
            }
            case 1: {
                boolean b37 = b17;
                if (this.j(o, o2, n)) {
                    b37 = b17;
                    if (Float.floatToIntBits(d12.B(o, y)) == Float.floatToIntBits(d12.B(o2, y))) {
                        b37 = true;
                    }
                }
                return b37;
            }
            case 0: {
                boolean b38 = b18;
                if (this.j(o, o2, n)) {
                    b38 = b18;
                    if (Double.doubleToLongBits(d12.A(o, y)) == Double.doubleToLongBits(d12.A(o2, y))) {
                        b38 = true;
                    }
                }
                return b38;
            }
        }
    }
    
    public final void p0(final Object o, final int n, final f0 f0) {
        if (B(n)) {
            f0.z(this.n.e(o, Y(n)));
        }
        else {
            f0.n(this.n.e(o, Y(n)));
        }
    }
    
    public final Object q(Object g, final int n, final Object o, final k0 k0, final Object o2) {
        final int x = this.X(n);
        g = d12.G(g, Y(this.x0(n)));
        if (g == null) {
            return o;
        }
        final t.c t = this.t(n);
        if (t == null) {
            return o;
        }
        return this.r(n, x, this.q.e(g), t, o, k0, o2);
    }
    
    public final Object r(final int n, final int n2, final Map map, final t.c c, Object o, final k0 k0, final Object o2) {
        final x.a b = this.q.b(this.u(n));
        final Iterator iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            final Map.Entry<K, Integer> entry = (Map.Entry<K, Integer>)iterator.next();
            if (!c.a(entry.getValue())) {
                Object f;
                if ((f = o) == null) {
                    f = k0.f(o2);
                }
                final ByteString.g codedBuilder = ByteString.newCodedBuilder(x.b(b, entry.getKey(), entry.getValue()));
                final CodedOutputStream b2 = codedBuilder.b();
                try {
                    x.e(b2, b, entry.getKey(), entry.getValue());
                    k0.d(f, n2, codedBuilder.a());
                    iterator.remove();
                    o = f;
                    continue;
                }
                catch (final IOException cause) {
                    throw new RuntimeException(cause);
                }
                break;
            }
        }
        return o;
    }
    
    public final void r0(final Object o, int l0) {
        l0 = this.l0(l0);
        final long n = 0xFFFFF & l0;
        if (n == 1048575L) {
            return;
        }
        d12.U(o, n, 1 << (l0 >>> 20) | d12.C(o, n));
    }
    
    public final void s0(final Object o, final int n, final int n2) {
        d12.U(o, this.l0(n2) & 0xFFFFF, n);
    }
    
    public final t.c t(final int n) {
        return (t.c)this.b[n / 3 * 2 + 1];
    }
    
    public final int t0(final int n, int i) {
        int n2 = this.a.length / 3 - 1;
        while (i <= n2) {
            final int n3 = n2 + i >>> 1;
            final int n4 = n3 * 3;
            final int x = this.X(n4);
            if (n == x) {
                return n4;
            }
            if (n < x) {
                n2 = n3 - 1;
            }
            else {
                i = n3 + 1;
            }
        }
        return -1;
    }
    
    public final Object u(final int n) {
        return this.b[n / 3 * 2];
    }
    
    public final void u0(final Object o, final int n, final Object x) {
        c0.s.putObject(o, Y(this.x0(n)), x);
        this.r0(o, n);
    }
    
    public final g0 v(int n) {
        n = n / 3 * 2;
        final g0 g0 = (g0)this.b[n];
        if (g0 != null) {
            return g0;
        }
        return (g0)(this.b[n] = k91.a().c((Class)this.b[n + 1]));
    }
    
    public final void v0(final Object o, final int n, final int n2, final Object x) {
        c0.s.putObject(o, Y(this.x0(n2)), x);
        this.s0(o, n, n2);
    }
    
    public final int x(final Object o) {
        final Unsafe s = c0.s;
        int n = 1048575;
        int i = 0;
        int n2 = 0;
        int int1 = 0;
    Label_1853_Outer:
        while (i < this.a.length) {
            final int x0 = this.x0(i);
            final int x2 = this.X(i);
            final int w0 = w0(x0);
            int n6;
            int n7;
            int n8;
            if (w0 <= 17) {
                final int n3 = this.a[i + 2];
                final int n4 = n3 & 0xFFFFF;
                final int n5 = 1 << (n3 >>> 20);
                n6 = n;
                n7 = n3;
                n8 = n5;
                if (n4 != n) {
                    int1 = s.getInt(o, n4);
                    n6 = n4;
                    n7 = n3;
                    n8 = n5;
                }
            }
            else {
                int n9;
                if (this.i && w0 >= FieldType.DOUBLE_LIST_PACKED.id() && w0 <= FieldType.SINT64_LIST_PACKED.id()) {
                    n9 = (this.a[i + 2] & 0xFFFFF);
                }
                else {
                    n9 = 0;
                }
                n8 = 0;
                n7 = n9;
                n6 = n;
            }
            final long y = Y(x0);
            int n10 = 0;
            Label_2359: {
                int n19 = 0;
            Label_1853:
                while (true) {
                    Label_2187: {
                        Label_2098: {
                            Label_2068: {
                                int n14 = 0;
                                Label_2048: {
                                    int n13 = 0;
                                Label_2019:
                                    while (true) {
                                        int n16 = 0;
                                        Label_1993: {
                                            Label_1986: {
                                                Label_1966: {
                                                    int n12 = 0;
                                                    Label_1946: {
                                                        long n11 = 0L;
                                                        Label_1916: {
                                                            Label_1870: {
                                                                int n18 = 0;
                                                                Label_1828: {
                                                                    int x3 = 0;
                                                                    Label_1539: {
                                                                        switch (w0) {
                                                                            default: {
                                                                                n10 = n2;
                                                                                break Label_1857;
                                                                            }
                                                                            case 68: {
                                                                                n10 = n2;
                                                                                if (this.J(o, x2, i)) {
                                                                                    break Label_1870;
                                                                                }
                                                                                break Label_1857;
                                                                            }
                                                                            case 67: {
                                                                                n10 = n2;
                                                                                if (this.J(o, x2, i)) {
                                                                                    n11 = d0(o, y);
                                                                                    break Label_1916;
                                                                                }
                                                                                break Label_1857;
                                                                            }
                                                                            case 66: {
                                                                                n10 = n2;
                                                                                if (this.J(o, x2, i)) {
                                                                                    n12 = c0(o, y);
                                                                                    break Label_1946;
                                                                                }
                                                                                break Label_1857;
                                                                            }
                                                                            case 65: {
                                                                                n10 = n2;
                                                                                if (this.J(o, x2, i)) {
                                                                                    break Label_1966;
                                                                                }
                                                                                break Label_1857;
                                                                            }
                                                                            case 64: {
                                                                                n10 = n2;
                                                                                if (this.J(o, x2, i)) {
                                                                                    break Label_1986;
                                                                                }
                                                                                break Label_1857;
                                                                            }
                                                                            case 63: {
                                                                                n10 = n2;
                                                                                if (this.J(o, x2, i)) {
                                                                                    n13 = c0(o, y);
                                                                                    break Label_2019;
                                                                                }
                                                                                break Label_1857;
                                                                            }
                                                                            case 62: {
                                                                                n10 = n2;
                                                                                if (this.J(o, x2, i)) {
                                                                                    n14 = c0(o, y);
                                                                                    break Label_2048;
                                                                                }
                                                                                break Label_1857;
                                                                            }
                                                                            case 61: {
                                                                                n10 = n2;
                                                                                if (this.J(o, x2, i)) {
                                                                                    break Label_2068;
                                                                                }
                                                                                break Label_1857;
                                                                            }
                                                                            case 60: {
                                                                                n10 = n2;
                                                                                if (this.J(o, x2, i)) {
                                                                                    break Label_2098;
                                                                                }
                                                                                break Label_1857;
                                                                            }
                                                                            case 59: {
                                                                                n10 = n2;
                                                                                if (!this.J(o, x2, i)) {
                                                                                    break Label_1857;
                                                                                }
                                                                                final Object object = s.getObject(o, y);
                                                                                if (object instanceof ByteString) {
                                                                                    final int n15 = CodedOutputStream.h(x2, (ByteString)object);
                                                                                    break Label_1853;
                                                                                }
                                                                                final int n15 = CodedOutputStream.O(x2, (String)object);
                                                                                break Label_1853;
                                                                            }
                                                                            case 58: {
                                                                                n10 = n2;
                                                                                if (this.J(o, x2, i)) {
                                                                                    break Label_2187;
                                                                                }
                                                                                break Label_1857;
                                                                            }
                                                                            case 57: {
                                                                                n10 = n2;
                                                                                if (this.J(o, x2, i)) {
                                                                                    n16 = CodedOutputStream.n(x2, 0);
                                                                                    break Label_1993;
                                                                                }
                                                                                break Label_1857;
                                                                            }
                                                                            case 56: {
                                                                                n10 = n2;
                                                                                if (this.J(o, x2, i)) {
                                                                                    final int n15 = CodedOutputStream.p(x2, 0L);
                                                                                    break Label_1853;
                                                                                }
                                                                                break Label_1857;
                                                                            }
                                                                            case 55: {
                                                                                n10 = n2;
                                                                                if (this.J(o, x2, i)) {
                                                                                    final int n15 = CodedOutputStream.w(x2, c0(o, y));
                                                                                    break Label_1853;
                                                                                }
                                                                                break Label_1857;
                                                                            }
                                                                            case 54: {
                                                                                n10 = n2;
                                                                                if (this.J(o, x2, i)) {
                                                                                    final int n15 = CodedOutputStream.T(x2, d0(o, y));
                                                                                    break Label_1853;
                                                                                }
                                                                                break Label_1857;
                                                                            }
                                                                            case 53: {
                                                                                n10 = n2;
                                                                                if (this.J(o, x2, i)) {
                                                                                    final int n15 = CodedOutputStream.y(x2, d0(o, y));
                                                                                    break Label_1853;
                                                                                }
                                                                                break Label_1857;
                                                                            }
                                                                            case 52: {
                                                                                n10 = n2;
                                                                                if (this.J(o, x2, i)) {
                                                                                    final int n15 = CodedOutputStream.r(x2, 0.0f);
                                                                                    break Label_1853;
                                                                                }
                                                                                break Label_1857;
                                                                            }
                                                                            case 51: {
                                                                                n10 = n2;
                                                                                if (this.J(o, x2, i)) {
                                                                                    final int n15 = CodedOutputStream.j(x2, 0.0);
                                                                                    break Label_1853;
                                                                                }
                                                                                break Label_1857;
                                                                            }
                                                                            case 50: {
                                                                                final int n15 = this.q.d(x2, s.getObject(o, y), this.u(i));
                                                                                break Label_1853;
                                                                            }
                                                                            case 49: {
                                                                                final int n15 = h0.j(x2, (List)s.getObject(o, y), this.v(i));
                                                                                break Label_1853;
                                                                            }
                                                                            case 48: {
                                                                                final int t = h0.t((List)s.getObject(o, y));
                                                                                n10 = n2;
                                                                                if (t <= 0) {
                                                                                    break Label_1857;
                                                                                }
                                                                                x3 = t;
                                                                                if (this.i) {
                                                                                    x3 = t;
                                                                                    break;
                                                                                }
                                                                                break Label_1539;
                                                                            }
                                                                            case 47: {
                                                                                final int r = h0.r((List)s.getObject(o, y));
                                                                                n10 = n2;
                                                                                if (r <= 0) {
                                                                                    break Label_1857;
                                                                                }
                                                                                x3 = r;
                                                                                if (this.i) {
                                                                                    x3 = r;
                                                                                    break;
                                                                                }
                                                                                break Label_1539;
                                                                            }
                                                                            case 46: {
                                                                                final int j = h0.i((List)s.getObject(o, y));
                                                                                n10 = n2;
                                                                                if (j <= 0) {
                                                                                    break Label_1857;
                                                                                }
                                                                                x3 = j;
                                                                                if (this.i) {
                                                                                    x3 = j;
                                                                                    break;
                                                                                }
                                                                                break Label_1539;
                                                                            }
                                                                            case 45: {
                                                                                final int g = h0.g((List)s.getObject(o, y));
                                                                                n10 = n2;
                                                                                if (g <= 0) {
                                                                                    break Label_1857;
                                                                                }
                                                                                x3 = g;
                                                                                if (this.i) {
                                                                                    x3 = g;
                                                                                    break;
                                                                                }
                                                                                break Label_1539;
                                                                            }
                                                                            case 44: {
                                                                                final int e = h0.e((List)s.getObject(o, y));
                                                                                n10 = n2;
                                                                                if (e <= 0) {
                                                                                    break Label_1857;
                                                                                }
                                                                                x3 = e;
                                                                                if (this.i) {
                                                                                    x3 = e;
                                                                                    break;
                                                                                }
                                                                                break Label_1539;
                                                                            }
                                                                            case 43: {
                                                                                final int w2 = h0.w((List)s.getObject(o, y));
                                                                                n10 = n2;
                                                                                if (w2 <= 0) {
                                                                                    break Label_1857;
                                                                                }
                                                                                x3 = w2;
                                                                                if (this.i) {
                                                                                    x3 = w2;
                                                                                    break;
                                                                                }
                                                                                break Label_1539;
                                                                            }
                                                                            case 42: {
                                                                                final int b = h0.b((List)s.getObject(o, y));
                                                                                n10 = n2;
                                                                                if (b <= 0) {
                                                                                    break Label_1857;
                                                                                }
                                                                                x3 = b;
                                                                                if (this.i) {
                                                                                    x3 = b;
                                                                                    break;
                                                                                }
                                                                                break Label_1539;
                                                                            }
                                                                            case 41: {
                                                                                final int g2 = h0.g((List)s.getObject(o, y));
                                                                                n10 = n2;
                                                                                if (g2 <= 0) {
                                                                                    break Label_1857;
                                                                                }
                                                                                x3 = g2;
                                                                                if (this.i) {
                                                                                    x3 = g2;
                                                                                    break;
                                                                                }
                                                                                break Label_1539;
                                                                            }
                                                                            case 40: {
                                                                                final int k = h0.i((List)s.getObject(o, y));
                                                                                n10 = n2;
                                                                                if (k <= 0) {
                                                                                    break Label_1857;
                                                                                }
                                                                                x3 = k;
                                                                                if (this.i) {
                                                                                    x3 = k;
                                                                                    break;
                                                                                }
                                                                                break Label_1539;
                                                                            }
                                                                            case 39: {
                                                                                final int l = h0.l((List)s.getObject(o, y));
                                                                                n10 = n2;
                                                                                if (l <= 0) {
                                                                                    break Label_1857;
                                                                                }
                                                                                x3 = l;
                                                                                if (this.i) {
                                                                                    x3 = l;
                                                                                    break;
                                                                                }
                                                                                break Label_1539;
                                                                            }
                                                                            case 38: {
                                                                                final int y2 = h0.y((List)s.getObject(o, y));
                                                                                n10 = n2;
                                                                                if (y2 <= 0) {
                                                                                    break Label_1857;
                                                                                }
                                                                                x3 = y2;
                                                                                if (this.i) {
                                                                                    x3 = y2;
                                                                                    break;
                                                                                }
                                                                                break Label_1539;
                                                                            }
                                                                            case 37: {
                                                                                final int n17 = h0.n((List)s.getObject(o, y));
                                                                                n10 = n2;
                                                                                if (n17 <= 0) {
                                                                                    break Label_1857;
                                                                                }
                                                                                x3 = n17;
                                                                                if (this.i) {
                                                                                    x3 = n17;
                                                                                    break;
                                                                                }
                                                                                break Label_1539;
                                                                            }
                                                                            case 36: {
                                                                                final int g3 = h0.g((List)s.getObject(o, y));
                                                                                n10 = n2;
                                                                                if (g3 <= 0) {
                                                                                    break Label_1857;
                                                                                }
                                                                                x3 = g3;
                                                                                if (this.i) {
                                                                                    x3 = g3;
                                                                                    break;
                                                                                }
                                                                                break Label_1539;
                                                                            }
                                                                            case 35: {
                                                                                final int m = h0.i((List)s.getObject(o, y));
                                                                                n10 = n2;
                                                                                if (m <= 0) {
                                                                                    break Label_1857;
                                                                                }
                                                                                x3 = m;
                                                                                if (this.i) {
                                                                                    x3 = m;
                                                                                    break;
                                                                                }
                                                                                break Label_1539;
                                                                            }
                                                                            case 34: {
                                                                                n18 = h0.s(x2, (List)s.getObject(o, y), false);
                                                                                break Label_1828;
                                                                            }
                                                                            case 33: {
                                                                                n18 = h0.q(x2, (List)s.getObject(o, y), false);
                                                                                break Label_1828;
                                                                            }
                                                                            case 30: {
                                                                                n18 = h0.d(x2, (List)s.getObject(o, y), false);
                                                                                break Label_1828;
                                                                            }
                                                                            case 29: {
                                                                                final int n15 = h0.v(x2, (List)s.getObject(o, y), false);
                                                                                break Label_1853;
                                                                            }
                                                                            case 28: {
                                                                                final int n15 = h0.c(x2, (List)s.getObject(o, y));
                                                                                break Label_1853;
                                                                            }
                                                                            case 27: {
                                                                                final int n15 = h0.p(x2, (List)s.getObject(o, y), this.v(i));
                                                                                break Label_1853;
                                                                            }
                                                                            case 26: {
                                                                                final int n15 = h0.u(x2, (List)s.getObject(o, y));
                                                                                break Label_1853;
                                                                            }
                                                                            case 25: {
                                                                                n18 = h0.a(x2, (List)s.getObject(o, y), false);
                                                                                break Label_1828;
                                                                            }
                                                                            case 23:
                                                                            case 32: {
                                                                                n18 = h0.h(x2, (List)s.getObject(o, y), false);
                                                                                break Label_1828;
                                                                            }
                                                                            case 22: {
                                                                                n18 = h0.k(x2, (List)s.getObject(o, y), false);
                                                                                break Label_1828;
                                                                            }
                                                                            case 21: {
                                                                                n18 = h0.x(x2, (List)s.getObject(o, y), false);
                                                                                break Label_1828;
                                                                            }
                                                                            case 20: {
                                                                                n18 = h0.m(x2, (List)s.getObject(o, y), false);
                                                                                break Label_1828;
                                                                            }
                                                                            case 19:
                                                                            case 24:
                                                                            case 31: {
                                                                                n18 = h0.f(x2, (List)s.getObject(o, y), false);
                                                                                break Label_1828;
                                                                            }
                                                                            case 18: {
                                                                                final int n15 = h0.h(x2, (List)s.getObject(o, y), false);
                                                                                break Label_1853;
                                                                            }
                                                                            case 17: {
                                                                                n10 = n2;
                                                                                if ((int1 & n8) != 0x0) {
                                                                                    break Label_1870;
                                                                                }
                                                                                break Label_1857;
                                                                            }
                                                                            case 16: {
                                                                                n10 = n2;
                                                                                if ((int1 & n8) != 0x0) {
                                                                                    n11 = s.getLong(o, y);
                                                                                    break Label_1916;
                                                                                }
                                                                                break Label_1857;
                                                                            }
                                                                            case 15: {
                                                                                n10 = n2;
                                                                                if ((int1 & n8) != 0x0) {
                                                                                    n12 = s.getInt(o, y);
                                                                                    break Label_1946;
                                                                                }
                                                                                break Label_1857;
                                                                            }
                                                                            case 14: {
                                                                                n10 = n2;
                                                                                if ((int1 & n8) != 0x0) {
                                                                                    break Label_1966;
                                                                                }
                                                                                break Label_1857;
                                                                            }
                                                                            case 13: {
                                                                                n10 = n2;
                                                                                if ((int1 & n8) != 0x0) {
                                                                                    break Label_1986;
                                                                                }
                                                                                break Label_1857;
                                                                            }
                                                                            case 12: {
                                                                                n10 = n2;
                                                                                if ((int1 & n8) != 0x0) {
                                                                                    n13 = s.getInt(o, y);
                                                                                    break Label_2019;
                                                                                }
                                                                                break Label_1857;
                                                                            }
                                                                            case 11: {
                                                                                n10 = n2;
                                                                                if ((int1 & n8) != 0x0) {
                                                                                    n14 = s.getInt(o, y);
                                                                                    break Label_2048;
                                                                                }
                                                                                break Label_1857;
                                                                            }
                                                                            case 10: {
                                                                                n10 = n2;
                                                                                if ((int1 & n8) != 0x0) {
                                                                                    break Label_2068;
                                                                                }
                                                                                break Label_1857;
                                                                            }
                                                                            case 9: {
                                                                                n10 = n2;
                                                                                if ((int1 & n8) != 0x0) {
                                                                                    break Label_2098;
                                                                                }
                                                                                break Label_1857;
                                                                            }
                                                                            case 8: {
                                                                                n10 = n2;
                                                                                if ((int1 & n8) == 0x0) {
                                                                                    break Label_1857;
                                                                                }
                                                                                final Object object2 = s.getObject(o, y);
                                                                                if (object2 instanceof ByteString) {
                                                                                    final int n15 = CodedOutputStream.h(x2, (ByteString)object2);
                                                                                    break Label_1853;
                                                                                }
                                                                                final int n15 = CodedOutputStream.O(x2, (String)object2);
                                                                                break Label_1853;
                                                                            }
                                                                            case 7: {
                                                                                n10 = n2;
                                                                                if ((int1 & n8) != 0x0) {
                                                                                    break Label_2187;
                                                                                }
                                                                                break Label_1857;
                                                                            }
                                                                            case 6: {
                                                                                n10 = n2;
                                                                                if ((int1 & n8) != 0x0) {
                                                                                    n19 = CodedOutputStream.n(x2, 0);
                                                                                    break Label_1853;
                                                                                }
                                                                                break Label_1857;
                                                                            }
                                                                            case 5: {
                                                                                n10 = n2;
                                                                                if ((int1 & n8) != 0x0) {
                                                                                    n19 = CodedOutputStream.p(x2, 0L);
                                                                                    break Label_1853;
                                                                                }
                                                                                break Label_2359;
                                                                            }
                                                                            case 4: {
                                                                                n10 = n2;
                                                                                if ((int1 & n8) != 0x0) {
                                                                                    n19 = CodedOutputStream.w(x2, s.getInt(o, y));
                                                                                    break Label_1853;
                                                                                }
                                                                                break Label_2359;
                                                                            }
                                                                            case 3: {
                                                                                n10 = n2;
                                                                                if ((int1 & n8) != 0x0) {
                                                                                    n19 = CodedOutputStream.T(x2, s.getLong(o, y));
                                                                                    break Label_1853;
                                                                                }
                                                                                break Label_2359;
                                                                            }
                                                                            case 2: {
                                                                                n10 = n2;
                                                                                if ((int1 & n8) != 0x0) {
                                                                                    n19 = CodedOutputStream.y(x2, s.getLong(o, y));
                                                                                    break Label_1853;
                                                                                }
                                                                                break Label_2359;
                                                                            }
                                                                            case 1: {
                                                                                n10 = n2;
                                                                                if ((int1 & n8) != 0x0) {
                                                                                    n19 = CodedOutputStream.r(x2, 0.0f);
                                                                                    break Label_1853;
                                                                                }
                                                                                break Label_2359;
                                                                            }
                                                                            case 0: {
                                                                                n10 = n2;
                                                                                if ((int1 & n8) != 0x0) {
                                                                                    n19 = CodedOutputStream.j(x2, 0.0);
                                                                                    break Label_1853;
                                                                                }
                                                                                break Label_2359;
                                                                            }
                                                                        }
                                                                        s.putInt(o, n7, x3);
                                                                    }
                                                                    n16 = CodedOutputStream.Q(x2) + CodedOutputStream.S(x3) + x3;
                                                                    break Label_1993;
                                                                }
                                                                n10 = n2 + n18;
                                                                break Label_2359;
                                                                int n15 = 0;
                                                                n10 = n2 + n15;
                                                                break Label_2359;
                                                            }
                                                            final int n15 = CodedOutputStream.t(x2, (a0)s.getObject(o, y), this.v(i));
                                                            continue Label_1853;
                                                        }
                                                        final int n15 = CodedOutputStream.M(x2, n11);
                                                        continue Label_1853;
                                                    }
                                                    final int n15 = CodedOutputStream.K(x2, n12);
                                                    continue Label_1853;
                                                }
                                                final int n15 = CodedOutputStream.I(x2, 0L);
                                                continue Label_1853;
                                            }
                                            n16 = CodedOutputStream.G(x2, 0);
                                        }
                                        n10 = n2 + n16;
                                        continue Label_1853_Outer;
                                    }
                                    final int n15 = CodedOutputStream.l(x2, n13);
                                    continue Label_1853;
                                }
                                final int n15 = CodedOutputStream.R(x2, n14);
                                continue Label_1853;
                            }
                            final int n15 = CodedOutputStream.h(x2, (ByteString)s.getObject(o, y));
                            continue Label_1853;
                        }
                        final int n15 = h0.o(x2, s.getObject(o, y), this.v(i));
                        continue Label_1853;
                    }
                    final int n15 = CodedOutputStream.e(x2, true);
                    continue Label_1853;
                }
                n10 = n2 + n19;
            }
            i += 3;
            n2 = n10;
            n = n6;
        }
        int n20 = n2 + this.z(this.o, o);
        if (this.f) {
            n20 += this.p.c(o).h();
        }
        return n20;
    }
    
    public final int x0(final int n) {
        return this.a[n + 1];
    }
    
    public final int y(final Object o) {
        final Unsafe s = c0.s;
        int i = 0;
        int n = 0;
        while (i < this.a.length) {
            final int x0 = this.x0(i);
            final int w0 = w0(x0);
            final int x2 = this.X(i);
            final long y = Y(x0);
            int n2;
            if (w0 >= FieldType.DOUBLE_LIST_PACKED.id() && w0 <= FieldType.SINT64_LIST_PACKED.id()) {
                n2 = (this.a[i + 2] & 0xFFFFF);
            }
            else {
                n2 = 0;
            }
            int n3 = 0;
            Label_2118: {
                while (true) {
                    Label_2108: {
                        Label_2086: {
                            long n10 = 0L;
                            Label_2063: {
                                long n9 = 0L;
                                Label_2032: {
                                    int n8 = 0;
                                    Label_2002: {
                                        Label_1973: {
                                            Label_1951: {
                                                Label_1929: {
                                                    Label_1857: {
                                                        Object g = null;
                                                        Label_1831: {
                                                            Label_1823: {
                                                                int n7 = 0;
                                                                Label_1801: {
                                                                    int n6 = 0;
                                                                    Label_1772: {
                                                                        Label_1743: {
                                                                            Label_1721: {
                                                                                int n5 = 0;
                                                                                Label_1699: {
                                                                                    long n4 = 0L;
                                                                                    Label_1669: {
                                                                                        Label_1625: {
                                                                                            int x3 = 0;
                                                                                            Label_1382: {
                                                                                                Label_1372: {
                                                                                                    Object o3 = null;
                                                                                                    Label_0608: {
                                                                                                        switch (w0) {
                                                                                                            default: {
                                                                                                                n3 = n;
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 68: {
                                                                                                                n3 = n;
                                                                                                                if (this.J(o, x2, i)) {
                                                                                                                    break Label_1625;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 67: {
                                                                                                                n3 = n;
                                                                                                                if (this.J(o, x2, i)) {
                                                                                                                    n4 = d0(o, y);
                                                                                                                    break Label_1669;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 66: {
                                                                                                                n3 = n;
                                                                                                                if (this.J(o, x2, i)) {
                                                                                                                    n5 = c0(o, y);
                                                                                                                    break Label_1699;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 65: {
                                                                                                                n3 = n;
                                                                                                                if (this.J(o, x2, i)) {
                                                                                                                    break Label_1721;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 64: {
                                                                                                                n3 = n;
                                                                                                                if (this.J(o, x2, i)) {
                                                                                                                    break Label_1743;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 63: {
                                                                                                                n3 = n;
                                                                                                                if (this.J(o, x2, i)) {
                                                                                                                    n6 = c0(o, y);
                                                                                                                    break Label_1772;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 62: {
                                                                                                                n3 = n;
                                                                                                                if (this.J(o, x2, i)) {
                                                                                                                    n7 = c0(o, y);
                                                                                                                    break Label_1801;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 61: {
                                                                                                                n3 = n;
                                                                                                                if (this.J(o, x2, i)) {
                                                                                                                    break Label_1823;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 60: {
                                                                                                                n3 = n;
                                                                                                                if (this.J(o, x2, i)) {
                                                                                                                    break Label_1857;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 59: {
                                                                                                                n3 = n;
                                                                                                                if (!this.J(o, x2, i)) {
                                                                                                                    break Label_2118;
                                                                                                                }
                                                                                                                final Object o2 = o3 = d12.G(o, y);
                                                                                                                if (o2 instanceof ByteString) {
                                                                                                                    g = o2;
                                                                                                                    break;
                                                                                                                }
                                                                                                                break Label_0608;
                                                                                                            }
                                                                                                            case 58: {
                                                                                                                n3 = n;
                                                                                                                if (this.J(o, x2, i)) {
                                                                                                                    break Label_1929;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 57: {
                                                                                                                n3 = n;
                                                                                                                if (this.J(o, x2, i)) {
                                                                                                                    break Label_1951;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 56: {
                                                                                                                n3 = n;
                                                                                                                if (this.J(o, x2, i)) {
                                                                                                                    break Label_1973;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 55: {
                                                                                                                n3 = n;
                                                                                                                if (this.J(o, x2, i)) {
                                                                                                                    n8 = c0(o, y);
                                                                                                                    break Label_2002;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 54: {
                                                                                                                n3 = n;
                                                                                                                if (this.J(o, x2, i)) {
                                                                                                                    n9 = d0(o, y);
                                                                                                                    break Label_2032;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 53: {
                                                                                                                n3 = n;
                                                                                                                if (this.J(o, x2, i)) {
                                                                                                                    n10 = d0(o, y);
                                                                                                                    break Label_2063;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 52: {
                                                                                                                n3 = n;
                                                                                                                if (this.J(o, x2, i)) {
                                                                                                                    break Label_2086;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 51: {
                                                                                                                n3 = n;
                                                                                                                if (this.J(o, x2, i)) {
                                                                                                                    break Label_2108;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 50: {
                                                                                                                final int n11 = this.q.d(x2, d12.G(o, y), this.u(i));
                                                                                                                break Label_1606;
                                                                                                            }
                                                                                                            case 49: {
                                                                                                                final int n11 = h0.j(x2, L(o, y), this.v(i));
                                                                                                                break Label_1606;
                                                                                                            }
                                                                                                            case 48: {
                                                                                                                final int t = h0.t((List)s.getObject(o, y));
                                                                                                                n3 = n;
                                                                                                                if (t <= 0) {
                                                                                                                    break Label_2118;
                                                                                                                }
                                                                                                                x3 = t;
                                                                                                                if (this.i) {
                                                                                                                    x3 = t;
                                                                                                                    break Label_1372;
                                                                                                                }
                                                                                                                break Label_1382;
                                                                                                            }
                                                                                                            case 47: {
                                                                                                                final int r = h0.r((List)s.getObject(o, y));
                                                                                                                n3 = n;
                                                                                                                if (r <= 0) {
                                                                                                                    break Label_2118;
                                                                                                                }
                                                                                                                x3 = r;
                                                                                                                if (this.i) {
                                                                                                                    x3 = r;
                                                                                                                    break Label_1372;
                                                                                                                }
                                                                                                                break Label_1382;
                                                                                                            }
                                                                                                            case 46: {
                                                                                                                final int j = h0.i((List)s.getObject(o, y));
                                                                                                                n3 = n;
                                                                                                                if (j <= 0) {
                                                                                                                    break Label_2118;
                                                                                                                }
                                                                                                                x3 = j;
                                                                                                                if (this.i) {
                                                                                                                    x3 = j;
                                                                                                                    break Label_1372;
                                                                                                                }
                                                                                                                break Label_1382;
                                                                                                            }
                                                                                                            case 45: {
                                                                                                                final int g2 = h0.g((List)s.getObject(o, y));
                                                                                                                n3 = n;
                                                                                                                if (g2 <= 0) {
                                                                                                                    break Label_2118;
                                                                                                                }
                                                                                                                x3 = g2;
                                                                                                                if (this.i) {
                                                                                                                    x3 = g2;
                                                                                                                    break Label_1372;
                                                                                                                }
                                                                                                                break Label_1382;
                                                                                                            }
                                                                                                            case 44: {
                                                                                                                final int e = h0.e((List)s.getObject(o, y));
                                                                                                                n3 = n;
                                                                                                                if (e <= 0) {
                                                                                                                    break Label_2118;
                                                                                                                }
                                                                                                                x3 = e;
                                                                                                                if (this.i) {
                                                                                                                    x3 = e;
                                                                                                                    break Label_1372;
                                                                                                                }
                                                                                                                break Label_1382;
                                                                                                            }
                                                                                                            case 43: {
                                                                                                                final int w2 = h0.w((List)s.getObject(o, y));
                                                                                                                n3 = n;
                                                                                                                if (w2 <= 0) {
                                                                                                                    break Label_2118;
                                                                                                                }
                                                                                                                x3 = w2;
                                                                                                                if (this.i) {
                                                                                                                    x3 = w2;
                                                                                                                    break Label_1372;
                                                                                                                }
                                                                                                                break Label_1382;
                                                                                                            }
                                                                                                            case 42: {
                                                                                                                final int b = h0.b((List)s.getObject(o, y));
                                                                                                                n3 = n;
                                                                                                                if (b <= 0) {
                                                                                                                    break Label_2118;
                                                                                                                }
                                                                                                                x3 = b;
                                                                                                                if (this.i) {
                                                                                                                    x3 = b;
                                                                                                                    break Label_1372;
                                                                                                                }
                                                                                                                break Label_1382;
                                                                                                            }
                                                                                                            case 41: {
                                                                                                                final int g3 = h0.g((List)s.getObject(o, y));
                                                                                                                n3 = n;
                                                                                                                if (g3 <= 0) {
                                                                                                                    break Label_2118;
                                                                                                                }
                                                                                                                x3 = g3;
                                                                                                                if (this.i) {
                                                                                                                    x3 = g3;
                                                                                                                    break Label_1372;
                                                                                                                }
                                                                                                                break Label_1382;
                                                                                                            }
                                                                                                            case 40: {
                                                                                                                final int k = h0.i((List)s.getObject(o, y));
                                                                                                                n3 = n;
                                                                                                                if (k <= 0) {
                                                                                                                    break Label_2118;
                                                                                                                }
                                                                                                                x3 = k;
                                                                                                                if (this.i) {
                                                                                                                    x3 = k;
                                                                                                                    break Label_1372;
                                                                                                                }
                                                                                                                break Label_1382;
                                                                                                            }
                                                                                                            case 39: {
                                                                                                                final int l = h0.l((List)s.getObject(o, y));
                                                                                                                n3 = n;
                                                                                                                if (l <= 0) {
                                                                                                                    break Label_2118;
                                                                                                                }
                                                                                                                x3 = l;
                                                                                                                if (this.i) {
                                                                                                                    x3 = l;
                                                                                                                    break Label_1372;
                                                                                                                }
                                                                                                                break Label_1382;
                                                                                                            }
                                                                                                            case 38: {
                                                                                                                final int y2 = h0.y((List)s.getObject(o, y));
                                                                                                                n3 = n;
                                                                                                                if (y2 <= 0) {
                                                                                                                    break Label_2118;
                                                                                                                }
                                                                                                                x3 = y2;
                                                                                                                if (this.i) {
                                                                                                                    x3 = y2;
                                                                                                                    break Label_1372;
                                                                                                                }
                                                                                                                break Label_1382;
                                                                                                            }
                                                                                                            case 37: {
                                                                                                                final int n12 = h0.n((List)s.getObject(o, y));
                                                                                                                n3 = n;
                                                                                                                if (n12 <= 0) {
                                                                                                                    break Label_2118;
                                                                                                                }
                                                                                                                x3 = n12;
                                                                                                                if (this.i) {
                                                                                                                    x3 = n12;
                                                                                                                    break Label_1372;
                                                                                                                }
                                                                                                                break Label_1382;
                                                                                                            }
                                                                                                            case 36: {
                                                                                                                final int g4 = h0.g((List)s.getObject(o, y));
                                                                                                                n3 = n;
                                                                                                                if (g4 <= 0) {
                                                                                                                    break Label_2118;
                                                                                                                }
                                                                                                                x3 = g4;
                                                                                                                if (this.i) {
                                                                                                                    x3 = g4;
                                                                                                                    break Label_1372;
                                                                                                                }
                                                                                                                break Label_1382;
                                                                                                            }
                                                                                                            case 35: {
                                                                                                                final int m = h0.i((List)s.getObject(o, y));
                                                                                                                n3 = n;
                                                                                                                if (m <= 0) {
                                                                                                                    break Label_2118;
                                                                                                                }
                                                                                                                x3 = m;
                                                                                                                if (this.i) {
                                                                                                                    x3 = m;
                                                                                                                    break Label_1372;
                                                                                                                }
                                                                                                                break Label_1382;
                                                                                                            }
                                                                                                            case 34: {
                                                                                                                final int n11 = h0.s(x2, L(o, y), false);
                                                                                                                break Label_1606;
                                                                                                            }
                                                                                                            case 33: {
                                                                                                                final int n11 = h0.q(x2, L(o, y), false);
                                                                                                                break Label_1606;
                                                                                                            }
                                                                                                            case 30: {
                                                                                                                final int n11 = h0.d(x2, L(o, y), false);
                                                                                                                break Label_1606;
                                                                                                            }
                                                                                                            case 29: {
                                                                                                                final int n11 = h0.v(x2, L(o, y), false);
                                                                                                                break Label_1606;
                                                                                                            }
                                                                                                            case 28: {
                                                                                                                final int n11 = h0.c(x2, L(o, y));
                                                                                                                break Label_1606;
                                                                                                            }
                                                                                                            case 27: {
                                                                                                                final int n11 = h0.p(x2, L(o, y), this.v(i));
                                                                                                                break Label_1606;
                                                                                                            }
                                                                                                            case 26: {
                                                                                                                final int n11 = h0.u(x2, L(o, y));
                                                                                                                break Label_1606;
                                                                                                            }
                                                                                                            case 25: {
                                                                                                                final int n11 = h0.a(x2, L(o, y), false);
                                                                                                                break Label_1606;
                                                                                                            }
                                                                                                            case 22: {
                                                                                                                final int n11 = h0.k(x2, L(o, y), false);
                                                                                                                break Label_1606;
                                                                                                            }
                                                                                                            case 21: {
                                                                                                                final int n11 = h0.x(x2, L(o, y), false);
                                                                                                                break Label_1606;
                                                                                                            }
                                                                                                            case 20: {
                                                                                                                final int n11 = h0.m(x2, L(o, y), false);
                                                                                                                break Label_1606;
                                                                                                            }
                                                                                                            case 19:
                                                                                                            case 24:
                                                                                                            case 31: {
                                                                                                                final int n11 = h0.f(x2, L(o, y), false);
                                                                                                                break Label_1606;
                                                                                                            }
                                                                                                            case 18:
                                                                                                            case 23:
                                                                                                            case 32: {
                                                                                                                final int n11 = h0.h(x2, L(o, y), false);
                                                                                                                break Label_1606;
                                                                                                            }
                                                                                                            case 17: {
                                                                                                                n3 = n;
                                                                                                                if (this.C(o, i)) {
                                                                                                                    break Label_1625;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 16: {
                                                                                                                n3 = n;
                                                                                                                if (this.C(o, i)) {
                                                                                                                    n4 = d12.E(o, y);
                                                                                                                    break Label_1669;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 15: {
                                                                                                                n3 = n;
                                                                                                                if (this.C(o, i)) {
                                                                                                                    n5 = d12.C(o, y);
                                                                                                                    break Label_1699;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 14: {
                                                                                                                n3 = n;
                                                                                                                if (this.C(o, i)) {
                                                                                                                    break Label_1721;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 13: {
                                                                                                                n3 = n;
                                                                                                                if (this.C(o, i)) {
                                                                                                                    break Label_1743;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 12: {
                                                                                                                n3 = n;
                                                                                                                if (this.C(o, i)) {
                                                                                                                    n6 = d12.C(o, y);
                                                                                                                    break Label_1772;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 11: {
                                                                                                                n3 = n;
                                                                                                                if (this.C(o, i)) {
                                                                                                                    n7 = d12.C(o, y);
                                                                                                                    break Label_1801;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 10: {
                                                                                                                n3 = n;
                                                                                                                if (this.C(o, i)) {
                                                                                                                    break Label_1823;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 9: {
                                                                                                                n3 = n;
                                                                                                                if (this.C(o, i)) {
                                                                                                                    break Label_1857;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 8: {
                                                                                                                n3 = n;
                                                                                                                if (!this.C(o, i)) {
                                                                                                                    break Label_2118;
                                                                                                                }
                                                                                                                final Object o4 = o3 = d12.G(o, y);
                                                                                                                if (o4 instanceof ByteString) {
                                                                                                                    g = o4;
                                                                                                                    break;
                                                                                                                }
                                                                                                                break Label_0608;
                                                                                                            }
                                                                                                            case 7: {
                                                                                                                n3 = n;
                                                                                                                if (this.C(o, i)) {
                                                                                                                    break Label_1929;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 6: {
                                                                                                                n3 = n;
                                                                                                                if (this.C(o, i)) {
                                                                                                                    break Label_1951;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 5: {
                                                                                                                n3 = n;
                                                                                                                if (this.C(o, i)) {
                                                                                                                    break Label_1973;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 4: {
                                                                                                                n3 = n;
                                                                                                                if (this.C(o, i)) {
                                                                                                                    n8 = d12.C(o, y);
                                                                                                                    break Label_2002;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 3: {
                                                                                                                n3 = n;
                                                                                                                if (this.C(o, i)) {
                                                                                                                    n9 = d12.E(o, y);
                                                                                                                    break Label_2032;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 2: {
                                                                                                                n3 = n;
                                                                                                                if (this.C(o, i)) {
                                                                                                                    n10 = d12.E(o, y);
                                                                                                                    break Label_2063;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 1: {
                                                                                                                n3 = n;
                                                                                                                if (this.C(o, i)) {
                                                                                                                    break Label_2086;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                            case 0: {
                                                                                                                n3 = n;
                                                                                                                if (this.C(o, i)) {
                                                                                                                    break Label_2108;
                                                                                                                }
                                                                                                                break Label_2118;
                                                                                                            }
                                                                                                        }
                                                                                                        break Label_1831;
                                                                                                    }
                                                                                                    final int n11 = CodedOutputStream.O(x2, (String)o3);
                                                                                                    break Label_1606;
                                                                                                }
                                                                                                s.putInt(o, n2, x3);
                                                                                            }
                                                                                            final int n11 = CodedOutputStream.Q(x2) + CodedOutputStream.S(x3) + x3;
                                                                                            n3 = n + n11;
                                                                                            break Label_2118;
                                                                                        }
                                                                                        final int n11 = CodedOutputStream.t(x2, (a0)d12.G(o, y), this.v(i));
                                                                                        continue;
                                                                                    }
                                                                                    final int n11 = CodedOutputStream.M(x2, n4);
                                                                                    continue;
                                                                                }
                                                                                final int n11 = CodedOutputStream.K(x2, n5);
                                                                                continue;
                                                                            }
                                                                            final int n11 = CodedOutputStream.I(x2, 0L);
                                                                            continue;
                                                                        }
                                                                        final int n11 = CodedOutputStream.G(x2, 0);
                                                                        continue;
                                                                    }
                                                                    final int n11 = CodedOutputStream.l(x2, n6);
                                                                    continue;
                                                                }
                                                                final int n11 = CodedOutputStream.R(x2, n7);
                                                                continue;
                                                            }
                                                            g = d12.G(o, y);
                                                        }
                                                        final int n11 = CodedOutputStream.h(x2, (ByteString)g);
                                                        continue;
                                                    }
                                                    final int n11 = h0.o(x2, d12.G(o, y), this.v(i));
                                                    continue;
                                                }
                                                final int n11 = CodedOutputStream.e(x2, true);
                                                continue;
                                            }
                                            final int n11 = CodedOutputStream.n(x2, 0);
                                            continue;
                                        }
                                        final int n11 = CodedOutputStream.p(x2, 0L);
                                        continue;
                                    }
                                    final int n11 = CodedOutputStream.w(x2, n8);
                                    continue;
                                }
                                final int n11 = CodedOutputStream.T(x2, n9);
                                continue;
                            }
                            final int n11 = CodedOutputStream.y(x2, n10);
                            continue;
                        }
                        final int n11 = CodedOutputStream.r(x2, 0.0f);
                        continue;
                    }
                    final int n11 = CodedOutputStream.j(x2, 0.0);
                    continue;
                }
            }
            i += 3;
            n = n3;
        }
        return n + this.z(this.o, o);
    }
    
    public final void y0(final Object o, final Writer writer) {
        Iterator n = null;
        Map.Entry entry = null;
        Label_0053: {
            if (this.f) {
                final o c = this.p.c(o);
                if (!c.j()) {
                    n = c.n();
                    entry = (Map.Entry)n.next();
                    break Label_0053;
                }
            }
            n = null;
            entry = null;
        }
        final int length = this.a.length;
        final Unsafe s = c0.s;
        int n2 = 1048575;
        int n3 = 0;
        int int1 = 0;
        Map.Entry entry2 = entry;
        Map.Entry entry3;
        while (true) {
            entry3 = entry2;
            if (n3 >= length) {
                break;
            }
            final int x0 = this.x0(n3);
            final int x2 = this.X(n3);
            final int w0 = w0(x0);
            int n7;
            if (w0 <= 17) {
                final int n4 = this.a[n3 + 2];
                final int n5 = n4 & 0xFFFFF;
                int n6;
                if (n5 != (n6 = n2)) {
                    int1 = s.getInt(o, n5);
                    n6 = n5;
                }
                n7 = 1 << (n4 >>> 20);
                n2 = n6;
            }
            else {
                n7 = 0;
            }
            while (entry2 != null && this.p.a(entry2) <= x2) {
                this.p.j(writer, entry2);
                if (n.hasNext()) {
                    entry2 = (Map.Entry)n.next();
                }
                else {
                    entry2 = null;
                }
            }
            final long y = Y(x0);
            switch (w0) {
                case 68: {
                    if (this.J(o, x2, n3)) {
                        writer.M(x2, s.getObject(o, y), this.v(n3));
                        break;
                    }
                    break;
                }
                case 67: {
                    if (this.J(o, x2, n3)) {
                        writer.j(x2, d0(o, y));
                        break;
                    }
                    break;
                }
                case 66: {
                    if (this.J(o, x2, n3)) {
                        writer.I(x2, c0(o, y));
                        break;
                    }
                    break;
                }
                case 65: {
                    if (this.J(o, x2, n3)) {
                        writer.w(x2, d0(o, y));
                        break;
                    }
                    break;
                }
                case 64: {
                    if (this.J(o, x2, n3)) {
                        writer.o(x2, c0(o, y));
                        break;
                    }
                    break;
                }
                case 63: {
                    if (this.J(o, x2, n3)) {
                        writer.G(x2, c0(o, y));
                        break;
                    }
                    break;
                }
                case 62: {
                    if (this.J(o, x2, n3)) {
                        writer.k(x2, c0(o, y));
                        break;
                    }
                    break;
                }
                case 61: {
                    if (this.J(o, x2, n3)) {
                        writer.J(x2, (ByteString)s.getObject(o, y));
                        break;
                    }
                    break;
                }
                case 60: {
                    if (this.J(o, x2, n3)) {
                        writer.O(x2, s.getObject(o, y), this.v(n3));
                        break;
                    }
                    break;
                }
                case 59: {
                    if (this.J(o, x2, n3)) {
                        this.C0(x2, s.getObject(o, y), writer);
                        break;
                    }
                    break;
                }
                case 58: {
                    if (this.J(o, x2, n3)) {
                        writer.n(x2, Z(o, y));
                        break;
                    }
                    break;
                }
                case 57: {
                    if (this.J(o, x2, n3)) {
                        writer.c(x2, c0(o, y));
                        break;
                    }
                    break;
                }
                case 56: {
                    if (this.J(o, x2, n3)) {
                        writer.m(x2, d0(o, y));
                        break;
                    }
                    break;
                }
                case 55: {
                    if (this.J(o, x2, n3)) {
                        writer.g(x2, c0(o, y));
                        break;
                    }
                    break;
                }
                case 54: {
                    if (this.J(o, x2, n3)) {
                        writer.e(x2, d0(o, y));
                        break;
                    }
                    break;
                }
                case 53: {
                    if (this.J(o, x2, n3)) {
                        writer.C(x2, d0(o, y));
                        break;
                    }
                    break;
                }
                case 52: {
                    if (this.J(o, x2, n3)) {
                        writer.F(x2, b0(o, y));
                        break;
                    }
                    break;
                }
                case 51: {
                    if (this.J(o, x2, n3)) {
                        writer.z(x2, a0(o, y));
                        break;
                    }
                    break;
                }
                case 50: {
                    this.B0(writer, x2, s.getObject(o, y), n3);
                    break;
                }
                case 49: {
                    h0.T(this.X(n3), (List)s.getObject(o, y), writer, this.v(n3));
                    break;
                }
                case 48: {
                    h0.a0(this.X(n3), (List)s.getObject(o, y), writer, true);
                    break;
                }
                case 47: {
                    h0.Z(this.X(n3), (List)s.getObject(o, y), writer, true);
                    break;
                }
                case 46: {
                    h0.Y(this.X(n3), (List)s.getObject(o, y), writer, true);
                    break;
                }
                case 45: {
                    h0.X(this.X(n3), (List)s.getObject(o, y), writer, true);
                    break;
                }
                case 44: {
                    h0.P(this.X(n3), (List)s.getObject(o, y), writer, true);
                    break;
                }
                case 43: {
                    h0.c0(this.X(n3), (List)s.getObject(o, y), writer, true);
                    break;
                }
                case 42: {
                    h0.M(this.X(n3), (List)s.getObject(o, y), writer, true);
                    break;
                }
                case 41: {
                    h0.Q(this.X(n3), (List)s.getObject(o, y), writer, true);
                    break;
                }
                case 40: {
                    h0.R(this.X(n3), (List)s.getObject(o, y), writer, true);
                    break;
                }
                case 39: {
                    h0.U(this.X(n3), (List)s.getObject(o, y), writer, true);
                    break;
                }
                case 38: {
                    h0.d0(this.X(n3), (List)s.getObject(o, y), writer, true);
                    break;
                }
                case 37: {
                    h0.V(this.X(n3), (List)s.getObject(o, y), writer, true);
                    break;
                }
                case 36: {
                    h0.S(this.X(n3), (List)s.getObject(o, y), writer, true);
                    break;
                }
                case 35: {
                    h0.O(this.X(n3), (List)s.getObject(o, y), writer, true);
                    break;
                }
                case 34: {
                    h0.a0(this.X(n3), (List)s.getObject(o, y), writer, false);
                    break;
                }
                case 33: {
                    h0.Z(this.X(n3), (List)s.getObject(o, y), writer, false);
                    break;
                }
                case 32: {
                    h0.Y(this.X(n3), (List)s.getObject(o, y), writer, false);
                    break;
                }
                case 31: {
                    h0.X(this.X(n3), (List)s.getObject(o, y), writer, false);
                    break;
                }
                case 30: {
                    h0.P(this.X(n3), (List)s.getObject(o, y), writer, false);
                    break;
                }
                case 29: {
                    h0.c0(this.X(n3), (List)s.getObject(o, y), writer, false);
                    break;
                }
                case 28: {
                    h0.N(this.X(n3), (List)s.getObject(o, y), writer);
                    break;
                }
                case 27: {
                    h0.W(this.X(n3), (List)s.getObject(o, y), writer, this.v(n3));
                    break;
                }
                case 26: {
                    h0.b0(this.X(n3), (List)s.getObject(o, y), writer);
                    break;
                }
                case 25: {
                    h0.M(this.X(n3), (List)s.getObject(o, y), writer, false);
                    break;
                }
                case 24: {
                    h0.Q(this.X(n3), (List)s.getObject(o, y), writer, false);
                    break;
                }
                case 23: {
                    h0.R(this.X(n3), (List)s.getObject(o, y), writer, false);
                    break;
                }
                case 22: {
                    h0.U(this.X(n3), (List)s.getObject(o, y), writer, false);
                    break;
                }
                case 21: {
                    h0.d0(this.X(n3), (List)s.getObject(o, y), writer, false);
                    break;
                }
                case 20: {
                    h0.V(this.X(n3), (List)s.getObject(o, y), writer, false);
                    break;
                }
                case 19: {
                    h0.S(this.X(n3), (List)s.getObject(o, y), writer, false);
                    break;
                }
                case 18: {
                    h0.O(this.X(n3), (List)s.getObject(o, y), writer, false);
                    break;
                }
                case 17: {
                    if ((n7 & int1) != 0x0) {
                        writer.M(x2, s.getObject(o, y), this.v(n3));
                        break;
                    }
                    break;
                }
                case 16: {
                    if ((n7 & int1) != 0x0) {
                        writer.j(x2, s.getLong(o, y));
                        break;
                    }
                    break;
                }
                case 15: {
                    if ((n7 & int1) != 0x0) {
                        writer.I(x2, s.getInt(o, y));
                        break;
                    }
                    break;
                }
                case 14: {
                    if ((n7 & int1) != 0x0) {
                        writer.w(x2, s.getLong(o, y));
                        break;
                    }
                    break;
                }
                case 13: {
                    if ((n7 & int1) != 0x0) {
                        writer.o(x2, s.getInt(o, y));
                        break;
                    }
                    break;
                }
                case 12: {
                    if ((n7 & int1) != 0x0) {
                        writer.G(x2, s.getInt(o, y));
                        break;
                    }
                    break;
                }
                case 11: {
                    if ((n7 & int1) != 0x0) {
                        writer.k(x2, s.getInt(o, y));
                        break;
                    }
                    break;
                }
                case 10: {
                    if ((n7 & int1) != 0x0) {
                        writer.J(x2, (ByteString)s.getObject(o, y));
                        break;
                    }
                    break;
                }
                case 9: {
                    if ((n7 & int1) != 0x0) {
                        writer.O(x2, s.getObject(o, y), this.v(n3));
                        break;
                    }
                    break;
                }
                case 8: {
                    if ((n7 & int1) != 0x0) {
                        this.C0(x2, s.getObject(o, y), writer);
                        break;
                    }
                    break;
                }
                case 7: {
                    if ((n7 & int1) != 0x0) {
                        writer.n(x2, k(o, y));
                        break;
                    }
                    break;
                }
                case 6: {
                    if ((n7 & int1) != 0x0) {
                        writer.c(x2, s.getInt(o, y));
                        break;
                    }
                    break;
                }
                case 5: {
                    if ((n7 & int1) != 0x0) {
                        writer.m(x2, s.getLong(o, y));
                        break;
                    }
                    break;
                }
                case 4: {
                    if ((n7 & int1) != 0x0) {
                        writer.g(x2, s.getInt(o, y));
                        break;
                    }
                    break;
                }
                case 3: {
                    if ((n7 & int1) != 0x0) {
                        writer.e(x2, s.getLong(o, y));
                        break;
                    }
                    break;
                }
                case 2: {
                    if ((n7 & int1) != 0x0) {
                        writer.C(x2, s.getLong(o, y));
                        break;
                    }
                    break;
                }
                case 1: {
                    if ((n7 & int1) != 0x0) {
                        writer.F(x2, s(o, y));
                        break;
                    }
                    break;
                }
                case 0: {
                    if ((n7 & int1) != 0x0) {
                        writer.z(x2, o(o, y));
                        break;
                    }
                    break;
                }
            }
            n3 += 3;
        }
        while (entry3 != null) {
            this.p.j(writer, entry3);
            if (n.hasNext()) {
                entry3 = (Map.Entry)n.next();
            }
            else {
                entry3 = null;
            }
        }
        this.D0(this.o, o, writer);
    }
    
    public final int z(final k0 k0, final Object o) {
        return k0.h(k0.g(o));
    }
    
    public final void z0(final Object o, final Writer writer) {
        Iterator n = null;
        Map.Entry entry = null;
        Label_0053: {
            if (this.f) {
                final o c = this.p.c(o);
                if (!c.j()) {
                    n = c.n();
                    entry = (Map.Entry)n.next();
                    break Label_0053;
                }
            }
            n = null;
            entry = null;
        }
        final int length = this.a.length;
        int n2 = 0;
        Map.Entry entry2 = entry;
        Map.Entry entry3;
        while (true) {
            entry3 = entry2;
            if (n2 >= length) {
                break;
            }
            final int x0 = this.x0(n2);
            final int x2 = this.X(n2);
            while (entry2 != null && this.p.a(entry2) <= x2) {
                this.p.j(writer, entry2);
                if (n.hasNext()) {
                    entry2 = (Map.Entry)n.next();
                }
                else {
                    entry2 = null;
                }
            }
            Label_2340: {
                double n15 = 0.0;
                Label_2331: {
                    float n14 = 0.0f;
                    Label_2298: {
                        long n13 = 0L;
                        Label_2264: {
                            long n12 = 0L;
                            Label_2230: {
                                int n11 = 0;
                                Label_2196: {
                                    long n10 = 0L;
                                    Label_2162: {
                                        int n9 = 0;
                                        Label_2128: {
                                            boolean b = false;
                                            Label_2094: {
                                                Label_2054: {
                                                    Label_2018: {
                                                        Label_1985: {
                                                            int n8 = 0;
                                                            Label_1962: {
                                                                int n7 = 0;
                                                                Label_1928: {
                                                                    int n6 = 0;
                                                                    Label_1894: {
                                                                        long n5 = 0L;
                                                                        Label_1860: {
                                                                            int n4 = 0;
                                                                            Label_1826: {
                                                                                long n3 = 0L;
                                                                                Label_1792: {
                                                                                    switch (w0(x0)) {
                                                                                        default: {
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 68: {
                                                                                            if (this.J(o, x2, n2)) {
                                                                                                break;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 67: {
                                                                                            if (this.J(o, x2, n2)) {
                                                                                                n3 = d0(o, Y(x0));
                                                                                                break Label_1792;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 66: {
                                                                                            if (this.J(o, x2, n2)) {
                                                                                                n4 = c0(o, Y(x0));
                                                                                                break Label_1826;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 65: {
                                                                                            if (this.J(o, x2, n2)) {
                                                                                                n5 = d0(o, Y(x0));
                                                                                                break Label_1860;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 64: {
                                                                                            if (this.J(o, x2, n2)) {
                                                                                                n6 = c0(o, Y(x0));
                                                                                                break Label_1894;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 63: {
                                                                                            if (this.J(o, x2, n2)) {
                                                                                                n7 = c0(o, Y(x0));
                                                                                                break Label_1928;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 62: {
                                                                                            if (this.J(o, x2, n2)) {
                                                                                                n8 = c0(o, Y(x0));
                                                                                                break Label_1962;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 61: {
                                                                                            if (this.J(o, x2, n2)) {
                                                                                                break Label_1985;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 60: {
                                                                                            if (this.J(o, x2, n2)) {
                                                                                                break Label_2018;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 59: {
                                                                                            if (this.J(o, x2, n2)) {
                                                                                                break Label_2054;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 58: {
                                                                                            if (this.J(o, x2, n2)) {
                                                                                                b = Z(o, Y(x0));
                                                                                                break Label_2094;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 57: {
                                                                                            if (this.J(o, x2, n2)) {
                                                                                                n9 = c0(o, Y(x0));
                                                                                                break Label_2128;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 56: {
                                                                                            if (this.J(o, x2, n2)) {
                                                                                                n10 = d0(o, Y(x0));
                                                                                                break Label_2162;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 55: {
                                                                                            if (this.J(o, x2, n2)) {
                                                                                                n11 = c0(o, Y(x0));
                                                                                                break Label_2196;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 54: {
                                                                                            if (this.J(o, x2, n2)) {
                                                                                                n12 = d0(o, Y(x0));
                                                                                                break Label_2230;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 53: {
                                                                                            if (this.J(o, x2, n2)) {
                                                                                                n13 = d0(o, Y(x0));
                                                                                                break Label_2264;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 52: {
                                                                                            if (this.J(o, x2, n2)) {
                                                                                                n14 = b0(o, Y(x0));
                                                                                                break Label_2298;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 51: {
                                                                                            if (this.J(o, x2, n2)) {
                                                                                                n15 = a0(o, Y(x0));
                                                                                                break Label_2331;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 50: {
                                                                                            this.B0(writer, x2, d12.G(o, Y(x0)), n2);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 49: {
                                                                                            h0.T(this.X(n2), (List)d12.G(o, Y(x0)), writer, this.v(n2));
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 48: {
                                                                                            h0.a0(this.X(n2), (List)d12.G(o, Y(x0)), writer, true);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 47: {
                                                                                            h0.Z(this.X(n2), (List)d12.G(o, Y(x0)), writer, true);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 46: {
                                                                                            h0.Y(this.X(n2), (List)d12.G(o, Y(x0)), writer, true);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 45: {
                                                                                            h0.X(this.X(n2), (List)d12.G(o, Y(x0)), writer, true);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 44: {
                                                                                            h0.P(this.X(n2), (List)d12.G(o, Y(x0)), writer, true);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 43: {
                                                                                            h0.c0(this.X(n2), (List)d12.G(o, Y(x0)), writer, true);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 42: {
                                                                                            h0.M(this.X(n2), (List)d12.G(o, Y(x0)), writer, true);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 41: {
                                                                                            h0.Q(this.X(n2), (List)d12.G(o, Y(x0)), writer, true);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 40: {
                                                                                            h0.R(this.X(n2), (List)d12.G(o, Y(x0)), writer, true);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 39: {
                                                                                            h0.U(this.X(n2), (List)d12.G(o, Y(x0)), writer, true);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 38: {
                                                                                            h0.d0(this.X(n2), (List)d12.G(o, Y(x0)), writer, true);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 37: {
                                                                                            h0.V(this.X(n2), (List)d12.G(o, Y(x0)), writer, true);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 36: {
                                                                                            h0.S(this.X(n2), (List)d12.G(o, Y(x0)), writer, true);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 35: {
                                                                                            h0.O(this.X(n2), (List)d12.G(o, Y(x0)), writer, true);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 34: {
                                                                                            h0.a0(this.X(n2), (List)d12.G(o, Y(x0)), writer, false);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 33: {
                                                                                            h0.Z(this.X(n2), (List)d12.G(o, Y(x0)), writer, false);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 32: {
                                                                                            h0.Y(this.X(n2), (List)d12.G(o, Y(x0)), writer, false);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 31: {
                                                                                            h0.X(this.X(n2), (List)d12.G(o, Y(x0)), writer, false);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 30: {
                                                                                            h0.P(this.X(n2), (List)d12.G(o, Y(x0)), writer, false);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 29: {
                                                                                            h0.c0(this.X(n2), (List)d12.G(o, Y(x0)), writer, false);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 28: {
                                                                                            h0.N(this.X(n2), (List)d12.G(o, Y(x0)), writer);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 27: {
                                                                                            h0.W(this.X(n2), (List)d12.G(o, Y(x0)), writer, this.v(n2));
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 26: {
                                                                                            h0.b0(this.X(n2), (List)d12.G(o, Y(x0)), writer);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 25: {
                                                                                            h0.M(this.X(n2), (List)d12.G(o, Y(x0)), writer, false);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 24: {
                                                                                            h0.Q(this.X(n2), (List)d12.G(o, Y(x0)), writer, false);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 23: {
                                                                                            h0.R(this.X(n2), (List)d12.G(o, Y(x0)), writer, false);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 22: {
                                                                                            h0.U(this.X(n2), (List)d12.G(o, Y(x0)), writer, false);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 21: {
                                                                                            h0.d0(this.X(n2), (List)d12.G(o, Y(x0)), writer, false);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 20: {
                                                                                            h0.V(this.X(n2), (List)d12.G(o, Y(x0)), writer, false);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 19: {
                                                                                            h0.S(this.X(n2), (List)d12.G(o, Y(x0)), writer, false);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 18: {
                                                                                            h0.O(this.X(n2), (List)d12.G(o, Y(x0)), writer, false);
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 17: {
                                                                                            if (this.C(o, n2)) {
                                                                                                break;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 16: {
                                                                                            if (this.C(o, n2)) {
                                                                                                n3 = M(o, Y(x0));
                                                                                                break Label_1792;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 15: {
                                                                                            if (this.C(o, n2)) {
                                                                                                n4 = A(o, Y(x0));
                                                                                                break Label_1826;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 14: {
                                                                                            if (this.C(o, n2)) {
                                                                                                n5 = M(o, Y(x0));
                                                                                                break Label_1860;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 13: {
                                                                                            if (this.C(o, n2)) {
                                                                                                n6 = A(o, Y(x0));
                                                                                                break Label_1894;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 12: {
                                                                                            if (this.C(o, n2)) {
                                                                                                n7 = A(o, Y(x0));
                                                                                                break Label_1928;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 11: {
                                                                                            if (this.C(o, n2)) {
                                                                                                n8 = A(o, Y(x0));
                                                                                                break Label_1962;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 10: {
                                                                                            if (this.C(o, n2)) {
                                                                                                break Label_1985;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 9: {
                                                                                            if (this.C(o, n2)) {
                                                                                                break Label_2018;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 8: {
                                                                                            if (this.C(o, n2)) {
                                                                                                break Label_2054;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 7: {
                                                                                            if (this.C(o, n2)) {
                                                                                                b = k(o, Y(x0));
                                                                                                break Label_2094;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 6: {
                                                                                            if (this.C(o, n2)) {
                                                                                                n9 = A(o, Y(x0));
                                                                                                break Label_2128;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 5: {
                                                                                            if (this.C(o, n2)) {
                                                                                                n10 = M(o, Y(x0));
                                                                                                break Label_2162;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 4: {
                                                                                            if (this.C(o, n2)) {
                                                                                                n11 = A(o, Y(x0));
                                                                                                break Label_2196;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 3: {
                                                                                            if (this.C(o, n2)) {
                                                                                                n12 = M(o, Y(x0));
                                                                                                break Label_2230;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 2: {
                                                                                            if (this.C(o, n2)) {
                                                                                                n13 = M(o, Y(x0));
                                                                                                break Label_2264;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 1: {
                                                                                            if (this.C(o, n2)) {
                                                                                                n14 = s(o, Y(x0));
                                                                                                break Label_2298;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                        case 0: {
                                                                                            if (this.C(o, n2)) {
                                                                                                n15 = o(o, Y(x0));
                                                                                                break Label_2331;
                                                                                            }
                                                                                            break Label_2340;
                                                                                        }
                                                                                    }
                                                                                    writer.M(x2, d12.G(o, Y(x0)), this.v(n2));
                                                                                    break Label_2340;
                                                                                }
                                                                                writer.j(x2, n3);
                                                                                break Label_2340;
                                                                            }
                                                                            writer.I(x2, n4);
                                                                            break Label_2340;
                                                                        }
                                                                        writer.w(x2, n5);
                                                                        break Label_2340;
                                                                    }
                                                                    writer.o(x2, n6);
                                                                    break Label_2340;
                                                                }
                                                                writer.G(x2, n7);
                                                                break Label_2340;
                                                            }
                                                            writer.k(x2, n8);
                                                            break Label_2340;
                                                        }
                                                        writer.J(x2, (ByteString)d12.G(o, Y(x0)));
                                                        break Label_2340;
                                                    }
                                                    writer.O(x2, d12.G(o, Y(x0)), this.v(n2));
                                                    break Label_2340;
                                                }
                                                this.C0(x2, d12.G(o, Y(x0)), writer);
                                                break Label_2340;
                                            }
                                            writer.n(x2, b);
                                            break Label_2340;
                                        }
                                        writer.c(x2, n9);
                                        break Label_2340;
                                    }
                                    writer.m(x2, n10);
                                    break Label_2340;
                                }
                                writer.g(x2, n11);
                                break Label_2340;
                            }
                            writer.e(x2, n12);
                            break Label_2340;
                        }
                        writer.C(x2, n13);
                        break Label_2340;
                    }
                    writer.F(x2, n14);
                    break Label_2340;
                }
                writer.z(x2, n15);
            }
            n2 += 3;
        }
        while (entry3 != null) {
            this.p.j(writer, entry3);
            if (n.hasNext()) {
                entry3 = (Map.Entry)n.next();
            }
            else {
                entry3 = null;
            }
        }
        this.D0(this.o, o, writer);
    }
}

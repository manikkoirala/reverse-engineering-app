// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

public final class s extends c implements d, RandomAccess, a81
{
    public static final s d;
    public int[] b;
    public int c;
    
    static {
        (d = new s(new int[0], 0)).e();
    }
    
    public s(final int[] b, final int c) {
        this.b = b;
        this.c = c;
    }
    
    public static s l() {
        return s.d;
    }
    
    @Override
    public boolean addAll(final Collection collection) {
        this.a();
        t.a(collection);
        if (!(collection instanceof s)) {
            return super.addAll(collection);
        }
        final s s = (s)collection;
        final int c = s.c;
        if (c == 0) {
            return false;
        }
        final int c2 = this.c;
        if (Integer.MAX_VALUE - c2 >= c) {
            final int n = c2 + c;
            final int[] b = this.b;
            if (n > b.length) {
                this.b = Arrays.copyOf(b, n);
            }
            System.arraycopy(s.b, 0, this.b, this.c, s.c);
            this.c = n;
            ++super.modCount;
            return true;
        }
        throw new OutOfMemoryError();
    }
    
    public void b(final int n, final Integer n2) {
        this.i(n, n2);
    }
    
    public boolean c(final Integer n) {
        this.g(n);
        return true;
    }
    
    @Override
    public boolean contains(final Object o) {
        return this.indexOf(o) != -1;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof s)) {
            return super.equals(o);
        }
        final s s = (s)o;
        if (this.c != s.c) {
            return false;
        }
        final int[] b = s.b;
        for (int i = 0; i < this.c; ++i) {
            if (this.b[i] != b[i]) {
                return false;
            }
        }
        return true;
    }
    
    public void g(final int n) {
        this.a();
        final int c = this.c;
        final int[] b = this.b;
        if (c == b.length) {
            final int[] b2 = new int[c * 3 / 2 + 1];
            System.arraycopy(b, 0, b2, 0, c);
            this.b = b2;
        }
        this.b[this.c++] = n;
    }
    
    @Override
    public int hashCode() {
        int n = 1;
        for (int i = 0; i < this.c; ++i) {
            n = n * 31 + this.b[i];
        }
        return n;
    }
    
    public final void i(final int n, final int n2) {
        this.a();
        if (n >= 0) {
            final int c = this.c;
            if (n <= c) {
                final int[] b = this.b;
                if (c < b.length) {
                    System.arraycopy(b, n, b, n + 1, c - n);
                }
                else {
                    final int[] b2 = new int[c * 3 / 2 + 1];
                    System.arraycopy(b, 0, b2, 0, n);
                    System.arraycopy(this.b, n, b2, n + 1, this.c - n);
                    this.b = b2;
                }
                this.b[n] = n2;
                ++this.c;
                ++super.modCount;
                return;
            }
        }
        throw new IndexOutOfBoundsException(this.p(n));
    }
    
    @Override
    public int indexOf(final Object o) {
        if (!(o instanceof Integer)) {
            return -1;
        }
        final int intValue = (int)o;
        for (int size = this.size(), i = 0; i < size; ++i) {
            if (this.b[i] == intValue) {
                return i;
            }
        }
        return -1;
    }
    
    public final void m(final int n) {
        if (n >= 0 && n < this.c) {
            return;
        }
        throw new IndexOutOfBoundsException(this.p(n));
    }
    
    public Integer n(final int n) {
        return this.o(n);
    }
    
    public int o(final int n) {
        this.m(n);
        return this.b[n];
    }
    
    public final String p(final int i) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(this.c);
        return sb.toString();
    }
    
    public d r(final int newLength) {
        if (newLength >= this.c) {
            return new s(Arrays.copyOf(this.b, newLength), this.c);
        }
        throw new IllegalArgumentException();
    }
    
    public void removeRange(final int n, final int n2) {
        this.a();
        if (n2 >= n) {
            final int[] b = this.b;
            System.arraycopy(b, n2, b, n, this.c - n2);
            this.c -= n2 - n;
            ++super.modCount;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }
    
    public Integer s(final int n) {
        this.a();
        this.m(n);
        final int[] b = this.b;
        final int i = b[n];
        final int c = this.c;
        if (n < c - 1) {
            System.arraycopy(b, n + 1, b, n, c - n - 1);
        }
        --this.c;
        ++super.modCount;
        return i;
    }
    
    @Override
    public int size() {
        return this.c;
    }
    
    public Integer t(final int n, final Integer n2) {
        return this.u(n, n2);
    }
    
    public int u(final int n, final int n2) {
        this.a();
        this.m(n);
        final int[] b = this.b;
        final int n3 = b[n];
        b[n] = n2;
        return n3;
    }
}

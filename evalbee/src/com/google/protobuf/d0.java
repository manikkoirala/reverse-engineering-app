// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

import java.util.Iterator;
import java.util.Map;

public final class d0 implements g0
{
    public final a0 a;
    public final k0 b;
    public final boolean c;
    public final m d;
    
    public d0(final k0 b, final m d, final a0 a) {
        this.b = b;
        this.c = d.e(a);
        this.d = d;
        this.a = a;
    }
    
    public static d0 l(final k0 k0, final m m, final a0 a0) {
        return new d0(k0, m, a0);
    }
    
    @Override
    public void a(final Object o, final Object o2) {
        h0.F(this.b, o, o2);
        if (this.c) {
            h0.D(this.d, o, o2);
        }
    }
    
    @Override
    public final boolean b(final Object o) {
        return this.d.c(o).k();
    }
    
    @Override
    public boolean c(final Object o, final Object o2) {
        return this.b.g(o).equals(this.b.g(o2)) && (!this.c || this.d.c(o).equals(this.d.c(o2)));
    }
    
    @Override
    public void d(final Object o) {
        this.b.j(o);
        this.d.f(o);
    }
    
    @Override
    public int e(final Object o) {
        int n = this.j(this.b, o) + 0;
        if (this.c) {
            n += this.d.c(o).f();
        }
        return n;
    }
    
    @Override
    public int f(final Object o) {
        int hashCode = this.b.g(o).hashCode();
        if (this.c) {
            hashCode = hashCode * 53 + this.d.c(o).hashCode();
        }
        return hashCode;
    }
    
    @Override
    public void g(final Object o, final Writer writer) {
        final Iterator n = this.d.c(o).n();
        if (!n.hasNext()) {
            this.n(this.b, o, writer);
            return;
        }
        zu0.a(((Map.Entry<Object, V>)n.next()).getKey());
        throw null;
    }
    
    @Override
    public void h(final Object o, final byte[] array, final int n, final int n2, final e.a a) {
        final GeneratedMessageLite generatedMessageLite = (GeneratedMessageLite)o;
        if (generatedMessageLite.unknownFields == l0.c()) {
            generatedMessageLite.unknownFields = l0.k();
        }
        zu0.a(o);
        throw null;
    }
    
    @Override
    public void i(final Object o, final f0 f0, final l l) {
        this.k(this.b, this.d, o, f0, l);
    }
    
    public final int j(final k0 k0, final Object o) {
        return k0.i(k0.g(o));
    }
    
    public final void k(final k0 k0, final m m, final Object o, final f0 f0, final l l) {
        final Object f2 = k0.f(o);
        final o d = m.d(o);
        try {
            while (f0.m() != Integer.MAX_VALUE) {
                if (this.m(f0, l, m, d, k0, f2)) {
                    continue;
                }
            }
        }
        finally {
            k0.o(o, f2);
        }
    }
    
    public final boolean m(final f0 f0, final l l, final m m, final o o, final k0 k0, final Object o2) {
        final int tag = f0.getTag();
        if (tag == WireFormat.a) {
            Object b = null;
            int c = 0;
            ByteString g = null;
            while (true) {
                while (f0.m() != Integer.MAX_VALUE) {
                    final int tag2 = f0.getTag();
                    if (tag2 == WireFormat.c) {
                        c = f0.c();
                        b = m.b(l, this.a, c);
                    }
                    else if (tag2 == WireFormat.d) {
                        if (b != null) {
                            m.h(f0, b, l, o);
                        }
                        else {
                            g = f0.g();
                        }
                    }
                    else {
                        if (f0.p()) {
                            continue;
                        }
                        if (f0.getTag() == WireFormat.b) {
                            if (g != null) {
                                if (b != null) {
                                    m.i(g, b, l, o);
                                }
                                else {
                                    k0.d(o2, c, g);
                                }
                            }
                            return true;
                        }
                        throw InvalidProtocolBufferException.invalidEndTag();
                    }
                }
                continue;
            }
        }
        if (WireFormat.b(tag) != 2) {
            return f0.p();
        }
        final Object b2 = m.b(l, this.a, WireFormat.a(tag));
        if (b2 != null) {
            m.h(f0, b2, l, o);
            return true;
        }
        return k0.m(o2, f0);
    }
    
    public final void n(final k0 k0, final Object o, final Writer writer) {
        k0.s(k0.g(o), writer);
    }
    
    @Override
    public Object newInstance() {
        final a0 a = this.a;
        if (a instanceof GeneratedMessageLite) {
            return ((GeneratedMessageLite)a).O();
        }
        return a.d().g();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

import java.util.Collections;
import java.util.Map;

public class l
{
    public static boolean b = true;
    public static volatile l c;
    public static final l d;
    public final Map a;
    
    static {
        d = new l(true);
    }
    
    public l(final boolean b) {
        this.a = Collections.emptyMap();
    }
    
    public static l b() {
        final l c;
        if ((c = l.c) == null) {
            synchronized (l.class) {
                if (l.c == null) {
                    l c2;
                    if (l.b) {
                        c2 = qz.a();
                    }
                    else {
                        c2 = l.d;
                    }
                    l.c = c2;
                }
            }
        }
        return c;
    }
    
    public GeneratedMessageLite.c a(final a0 a0, final int n) {
        zu0.a(this.a.get(new a(a0, n)));
        return null;
    }
    
    public static final class a
    {
        public final Object a;
        public final int b;
        
        public a(final Object a, final int b) {
            this.a = a;
            this.b = b;
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof a;
            final boolean b2 = false;
            if (!b) {
                return false;
            }
            final a a = (a)o;
            boolean b3 = b2;
            if (this.a == a.a) {
                b3 = b2;
                if (this.b == a.b) {
                    b3 = true;
                }
            }
            return b3;
        }
        
        @Override
        public int hashCode() {
            return System.identityHashCode(this.a) * 65535 + this.b;
        }
    }
}

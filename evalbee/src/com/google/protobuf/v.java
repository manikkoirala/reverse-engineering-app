// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

import java.util.Collection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class v
{
    public static final v a;
    public static final v b;
    
    static {
        a = new b(null);
        b = new c(null);
    }
    
    public static v a() {
        return v.a;
    }
    
    public static v b() {
        return v.b;
    }
    
    public abstract void c(final Object p0, final long p1);
    
    public abstract void d(final Object p0, final Object p1, final long p2);
    
    public abstract List e(final Object p0, final long p1);
    
    public static final class b extends v
    {
        public static final Class c;
        
        static {
            c = Collections.unmodifiableList(Collections.emptyList()).getClass();
        }
        
        public b() {
            super(null);
        }
        
        public static List f(final Object o, final long n) {
            return (List)d12.G(o, n);
        }
        
        public static List g(final Object o, final long n, final int initialCapacity) {
            final List f = f(o, n);
            List<E> list = null;
            Label_0080: {
                if (!f.isEmpty()) {
                    if (b.c.isAssignableFrom(f.getClass())) {
                        list = (List<E>)new ArrayList<Object>(f.size() + initialCapacity);
                        ((ArrayList<Object>)list).addAll(f);
                    }
                    else if (f instanceof x02) {
                        list = new u(f.size() + initialCapacity);
                        ((u)list).addAll(f);
                    }
                    else {
                        list = f;
                        if (!(f instanceof a81)) {
                            return list;
                        }
                        list = f;
                        if (!(f instanceof t.e)) {
                            return list;
                        }
                        final t.e e = (t.e)f;
                        list = f;
                        if (!e.h()) {
                            list = e.j(f.size() + initialCapacity);
                            break Label_0080;
                        }
                        return list;
                    }
                    d12.W(o, n, list);
                    return list;
                }
                if (f instanceof fj0) {
                    list = new u(initialCapacity);
                }
                else if (f instanceof a81 && f instanceof t.e) {
                    list = ((t.e)f).j(initialCapacity);
                }
                else {
                    list = (List<E>)new ArrayList<Object>(initialCapacity);
                }
            }
            d12.W(o, n, list);
            return list;
        }
        
        @Override
        public void c(final Object o, final long n) {
            final List list = (List)d12.G(o, n);
            List<Object> list2;
            if (list instanceof fj0) {
                list2 = ((fj0)list).d();
            }
            else {
                if (b.c.isAssignableFrom(((fj0)list).getClass())) {
                    return;
                }
                if (list instanceof a81 && list instanceof t.e) {
                    final t.e e = (t.e)list;
                    if (e.h()) {
                        e.e();
                    }
                    return;
                }
                list2 = Collections.unmodifiableList((List<?>)list);
            }
            d12.W(o, n, list2);
        }
        
        @Override
        public void d(final Object o, final Object o2, final long n) {
            List f = f(o2, n);
            final List g = g(o, n, f.size());
            final int size = g.size();
            final int size2 = f.size();
            if (size > 0 && size2 > 0) {
                g.addAll(f);
            }
            if (size > 0) {
                f = g;
            }
            d12.W(o, n, f);
        }
        
        @Override
        public List e(final Object o, final long n) {
            return g(o, n, 10);
        }
    }
    
    public static final class c extends v
    {
        public c() {
            super(null);
        }
        
        public static t.e f(final Object o, final long n) {
            return (t.e)d12.G(o, n);
        }
        
        @Override
        public void c(final Object o, final long n) {
            f(o, n).e();
        }
        
        @Override
        public void d(final Object o, final Object o2, final long n) {
            final t.e f = f(o, n);
            final t.e f2 = f(o2, n);
            final int size = f.size();
            final int size2 = f2.size();
            Object j = f;
            if (size > 0) {
                j = f;
                if (size2 > 0) {
                    j = f;
                    if (!f.h()) {
                        j = f.j(size2 + size);
                    }
                    ((List<Object>)j).addAll(f2);
                }
            }
            List<Object> list = f2;
            if (size > 0) {
                list = (List<Object>)j;
            }
            d12.W(o, n, list);
        }
        
        @Override
        public List e(final Object o, final long n) {
            Object o2;
            final t.e e = (t.e)(o2 = f(o, n));
            if (!e.h()) {
                final int size = e.size();
                int n2;
                if (size == 0) {
                    n2 = 10;
                }
                else {
                    n2 = size * 2;
                }
                o2 = e.j(n2);
                d12.W(o, n, o2);
            }
            return (List)o2;
        }
    }
}

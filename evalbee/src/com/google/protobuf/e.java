// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

public abstract class e
{
    public static int A(final int n, final byte[] array, int n2, final int n3, final t.e e, final a a) {
        final w w = (w)e;
        n2 = K(array, n2, a);
        while (true) {
            w.b(g.c(a.b));
            if (n2 >= n3) {
                break;
            }
            final int h = H(array, n2, a);
            if (n != a.a) {
                break;
            }
            n2 = K(array, h, a);
        }
        return n2;
    }
    
    public static int B(final byte[] bytes, int a, final a a2) {
        final int h = H(bytes, a, a2);
        a = a2.a;
        if (a < 0) {
            throw InvalidProtocolBufferException.negativeSize();
        }
        if (a == 0) {
            a2.c = "";
            return h;
        }
        a2.c = new String(bytes, h, a, t.b);
        return h + a;
    }
    
    public static int C(final int n, final byte[] array, int n2, final int n3, final t.e e, final a a) {
        n2 = H(array, n2, a);
        final int a2 = a.a;
        if (a2 >= 0) {
        Label_0025_Outer:
            while (true) {
                if (a2 != 0) {
                    final String s = new String(array, n2, a2, t.b);
                    final int h = n2;
                    n2 = a2;
                    break Label_0060;
                }
                while (true) {
                    e.add("");
                    Label_0075: {
                        break Label_0075;
                        final String s;
                        e.add(s);
                        final int h;
                        n2 += h;
                    }
                    if (n2 < n3) {
                        final int h2 = H(array, n2, a);
                        if (n == a.a) {
                            final int h = H(array, h2, a);
                            n2 = a.a;
                            if (n2 < 0) {
                                throw InvalidProtocolBufferException.negativeSize();
                            }
                            if (n2 == 0) {
                                n2 = h;
                                continue;
                            }
                            final String s = new String(array, h, n2, t.b);
                            continue Label_0025_Outer;
                        }
                    }
                    break;
                }
                break;
            }
            return n2;
        }
        throw InvalidProtocolBufferException.negativeSize();
    }
    
    public static int D(final int n, final byte[] array, int n2, final int n3, final t.e e, final a a) {
        n2 = H(array, n2, a);
        final int a2 = a.a;
        if (a2 >= 0) {
        Label_0025_Outer:
            while (true) {
                if (a2 != 0) {
                    final int n4 = n2 + a2;
                    if (Utf8.t(array, n2, n4)) {
                        final String s = new String(array, n2, a2, t.b);
                        n2 = n4;
                        break Label_0073;
                    }
                    throw InvalidProtocolBufferException.invalidUtf8();
                }
                while (true) {
                    e.add("");
                    Label_0083: {
                        break Label_0083;
                        final String s;
                        e.add(s);
                    }
                    if (n2 < n3) {
                        final int h = H(array, n2, a);
                        if (n == a.a) {
                            n2 = H(array, h, a);
                            final int a3 = a.a;
                            if (a3 < 0) {
                                throw InvalidProtocolBufferException.negativeSize();
                            }
                            if (a3 == 0) {
                                continue;
                            }
                            final int n5 = n2 + a3;
                            if (Utf8.t(array, n2, n5)) {
                                final String s = new String(array, n2, a3, t.b);
                                n2 = n5;
                                continue Label_0025_Outer;
                            }
                            throw InvalidProtocolBufferException.invalidUtf8();
                        }
                    }
                    break;
                }
                break;
            }
            return n2;
        }
        throw InvalidProtocolBufferException.negativeSize();
    }
    
    public static int E(final byte[] array, int a, final a a2) {
        final int h = H(array, a, a2);
        a = a2.a;
        if (a < 0) {
            throw InvalidProtocolBufferException.negativeSize();
        }
        if (a == 0) {
            a2.c = "";
            return h;
        }
        a2.c = Utf8.h(array, h, a);
        return h + a;
    }
    
    public static int F(final int n, final byte[] array, int n2, int a, final l0 l0, final a a2) {
        if (WireFormat.a(n) == 0) {
            throw InvalidProtocolBufferException.invalidTag();
        }
        final int b = WireFormat.b(n);
        if (b == 0) {
            n2 = K(array, n2, a2);
            l0.n(n, a2.b);
            return n2;
        }
        if (b == 1) {
            l0.n(n, i(array, n2));
            return n2 + 8;
        }
        if (b != 2) {
            if (b != 3) {
                if (b == 5) {
                    l0.n(n, g(array, n2));
                    return n2 + 4;
                }
                throw InvalidProtocolBufferException.invalidTag();
            }
            else {
                final l0 k = l0.k();
                final int n3 = (n & 0xFFFFFFF8) | 0x4;
                int a3 = 0;
                int h;
                while (true) {
                    h = n2;
                    if (n2 >= a) {
                        break;
                    }
                    h = H(array, n2, a2);
                    a3 = a2.a;
                    if ((n2 = a3) == n3) {
                        a3 = n2;
                        break;
                    }
                    n2 = F(n2, array, h, a, k, a2);
                }
                if (h <= a && a3 == n3) {
                    l0.n(n, k);
                    return h;
                }
                throw InvalidProtocolBufferException.parseFailure();
            }
        }
        else {
            n2 = H(array, n2, a2);
            a = a2.a;
            if (a < 0) {
                throw InvalidProtocolBufferException.negativeSize();
            }
            if (a <= array.length - n2) {
                ByteString byteString;
                if (a == 0) {
                    byteString = ByteString.EMPTY;
                }
                else {
                    byteString = ByteString.copyFrom(array, n2, a);
                }
                l0.n(n, byteString);
                return n2 + a;
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }
    }
    
    public static int G(int n, final byte[] array, int n2, final a a) {
        final int n3 = n & 0x7F;
        int n4 = n2 + 1;
        n = array[n2];
        Label_0027: {
            if (n < 0) {
                final int n5 = n3 | (n & 0x7F) << 7;
                n = n4 + 1;
                n2 = array[n4];
                int n6;
                if (n2 >= 0) {
                    n2 <<= 14;
                    n6 = n5;
                }
                else {
                    final int n7 = n5 | (n2 & 0x7F) << 14;
                    n2 = n + 1;
                    n = array[n];
                    if (n >= 0) {
                        n <<= 21;
                        n4 = n2;
                        n2 = n7;
                        break Label_0027;
                    }
                    n6 = (n7 | (n & 0x7F) << 21);
                    n = n2 + 1;
                    final byte b = array[n2];
                    if (b < 0) {
                        while (true) {
                            n2 = n + 1;
                            if (array[n] >= 0) {
                                break;
                            }
                            n = n2;
                        }
                        a.a = (n6 | (b & 0x7F) << 28);
                        return n2;
                    }
                    n2 = b << 28;
                }
                a.a = (n6 | n2);
                return n;
            }
            n <<= 7;
            n2 = n3;
        }
        a.a = (n2 | n);
        return n4;
    }
    
    public static int H(final byte[] array, int a, final a a2) {
        final int n = a + 1;
        a = array[a];
        if (a >= 0) {
            a2.a = a;
            return n;
        }
        return G(a, array, n, a2);
    }
    
    public static int I(final int n, final byte[] array, int n2, final int n3, final t.e e, final a a) {
        final s s = (s)e;
        n2 = H(array, n2, a);
        while (true) {
            s.g(a.a);
            if (n2 >= n3) {
                break;
            }
            final int h = H(array, n2, a);
            if (n != a.a) {
                break;
            }
            n2 = H(array, h, a);
        }
        return n2;
    }
    
    public static int J(long b, final byte[] array, int n, final a a) {
        final int n2 = n + 1;
        byte b2;
        int n3;
        for (b2 = array[n], b = ((b & 0x7FL) | (long)(b2 & 0x7F) << 7), n3 = 7, n = n2; b2 < 0; b2 = array[n], n3 += 7, b |= (long)(b2 & 0x7F) << n3, ++n) {}
        a.b = b;
        return n;
    }
    
    public static int K(final byte[] array, final int n, final a a) {
        final int n2 = n + 1;
        final long b = array[n];
        if (b >= 0L) {
            a.b = b;
            return n2;
        }
        return J(b, array, n2, a);
    }
    
    public static int L(final int n, final byte[] array, int n2, final int n3, final t.e e, final a a) {
        final w w = (w)e;
        n2 = K(array, n2, a);
        while (true) {
            w.b(a.b);
            if (n2 >= n3) {
                break;
            }
            final int h = H(array, n2, a);
            if (n != a.a) {
                break;
            }
            n2 = K(array, h, a);
        }
        return n2;
    }
    
    public static int M(final Object c, final g0 g0, final byte[] array, int g2, final int n, final int n2, final a a) {
        g2 = ((c0)g0).g0(c, array, g2, n, n2, a);
        a.c = c;
        return g2;
    }
    
    public static int N(final Object c, final g0 g0, final byte[] array, int a, final int n, final a a2) {
        final int n2 = a + 1;
        final int n3 = array[a];
        int g2 = n2;
        a = n3;
        if (n3 < 0) {
            g2 = G(n3, array, n2, a2);
            a = a2.a;
        }
        if (a >= 0 && a <= n - g2) {
            a += g2;
            g0.h(c, array, g2, a, a2);
            a2.c = c;
            return a;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public static int O(int a, final byte[] array, int o, final int n, final a a2) {
        if (WireFormat.a(a) == 0) {
            throw InvalidProtocolBufferException.invalidTag();
        }
        final int b = WireFormat.b(a);
        if (b == 0) {
            return K(array, o, a2);
        }
        if (b == 1) {
            return o + 8;
        }
        if (b == 2) {
            return H(array, o, a2) + a2.a;
        }
        if (b != 3) {
            if (b == 5) {
                return o + 4;
            }
            throw InvalidProtocolBufferException.invalidTag();
        }
        else {
            final int n2 = (a & 0xFFFFFFF8) | 0x4;
            a = 0;
            int h;
            while (true) {
                h = o;
                if (o >= n) {
                    break;
                }
                h = H(array, o, a2);
                a = a2.a;
                if (a == n2) {
                    break;
                }
                o = O(a, array, h, n, a2);
            }
            if (h <= n && a == n2) {
                return h;
            }
            throw InvalidProtocolBufferException.parseFailure();
        }
    }
    
    public static int a(final int n, final byte[] array, int n2, final int n3, final t.e e, final a a) {
        final f f = (f)e;
        final int n4 = n2 = K(array, n2, a);
    Label_0032_Outer:
        while (true) {
            if (a.b == 0L) {
                break Label_0038;
            }
            n2 = n4;
            while (true) {
                boolean b = true;
                Label_0041: {
                    break Label_0041;
                    b = false;
                }
                f.b(b);
                if (n2 < n3) {
                    final int h = H(array, n2, a);
                    if (n == a.a) {
                        final int n5 = n2 = K(array, h, a);
                        if (a.b != 0L) {
                            n2 = n5;
                            continue;
                        }
                        continue Label_0032_Outer;
                    }
                }
                break;
            }
            break;
        }
        return n2;
    }
    
    public static int b(final byte[] array, int a, final a a2) {
        final int h = H(array, a, a2);
        a = a2.a;
        if (a < 0) {
            throw InvalidProtocolBufferException.negativeSize();
        }
        if (a > array.length - h) {
            throw InvalidProtocolBufferException.truncatedMessage();
        }
        if (a == 0) {
            a2.c = ByteString.EMPTY;
            return h;
        }
        a2.c = ByteString.copyFrom(array, h, a);
        return h + a;
    }
    
    public static int c(final int n, final byte[] array, int n2, final int n3, final t.e e, final a a) {
        final int h = H(array, n2, a);
        final int a2 = a.a;
        if (a2 < 0) {
            throw InvalidProtocolBufferException.negativeSize();
        }
        if (a2 <= array.length - h) {
            int n4 = a2;
            n2 = h;
        Label_0046_Outer:
            while (true) {
                if (a2 != 0) {
                    break Label_0060;
                }
                n2 = h;
                while (true) {
                    e.add(ByteString.EMPTY);
                    Label_0080: {
                        break Label_0080;
                        e.add(ByteString.copyFrom(array, n2, n4));
                        n2 += n4;
                    }
                    if (n2 < n3) {
                        final int h2 = H(array, n2, a);
                        if (n == a.a) {
                            final int h3 = H(array, h2, a);
                            final int a3 = a.a;
                            if (a3 < 0) {
                                throw InvalidProtocolBufferException.negativeSize();
                            }
                            if (a3 > array.length - h3) {
                                throw InvalidProtocolBufferException.truncatedMessage();
                            }
                            n4 = a3;
                            n2 = h3;
                            if (a3 == 0) {
                                n2 = h3;
                                continue;
                            }
                            continue Label_0046_Outer;
                        }
                    }
                    break;
                }
                break;
            }
            return n2;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public static double d(final byte[] array, final int n) {
        return Double.longBitsToDouble(i(array, n));
    }
    
    public static int e(final int n, final byte[] array, int i, final int n2, final t.e e, final a a) {
        final j j = (j)e;
        j.b(d(array, i));
        int h;
        for (i += 8; i < n2; i = h + 8) {
            h = H(array, i, a);
            if (n != a.a) {
                break;
            }
            j.b(d(array, h));
        }
        return i;
    }
    
    public static int f(final int n, final byte[] array, final int n2, final int n3, final Object o, final a0 a0, final k0 k0, final a a2) {
        a2.d.a(a0, n >>> 3);
        return F(n, array, n2, n3, c0.w(o), a2);
    }
    
    public static int g(final byte[] array, final int n) {
        return (array[n + 3] & 0xFF) << 24 | ((array[n] & 0xFF) | (array[n + 1] & 0xFF) << 8 | (array[n + 2] & 0xFF) << 16);
    }
    
    public static int h(final int n, final byte[] array, int i, final int n2, final t.e e, final a a) {
        final s s = (s)e;
        s.g(g(array, i));
        int h;
        for (i += 4; i < n2; i = h + 4) {
            h = H(array, i, a);
            if (n != a.a) {
                break;
            }
            s.g(g(array, h));
        }
        return i;
    }
    
    public static long i(final byte[] array, final int n) {
        return ((long)array[n + 7] & 0xFFL) << 56 | (((long)array[n] & 0xFFL) | ((long)array[n + 1] & 0xFFL) << 8 | ((long)array[n + 2] & 0xFFL) << 16 | ((long)array[n + 3] & 0xFFL) << 24 | ((long)array[n + 4] & 0xFFL) << 32 | ((long)array[n + 5] & 0xFFL) << 40 | ((long)array[n + 6] & 0xFFL) << 48);
    }
    
    public static int j(final int n, final byte[] array, int i, final int n2, final t.e e, final a a) {
        final w w = (w)e;
        w.b(i(array, i));
        int h;
        for (i += 8; i < n2; i = h + 8) {
            h = H(array, i, a);
            if (n != a.a) {
                break;
            }
            w.b(i(array, h));
        }
        return i;
    }
    
    public static float k(final byte[] array, final int n) {
        return Float.intBitsToFloat(g(array, n));
    }
    
    public static int l(final int n, final byte[] array, int i, final int n2, final t.e e, final a a) {
        final p p6 = (p)e;
        p6.b(k(array, i));
        int h;
        for (i += 4; i < n2; i = h + 4) {
            h = H(array, i, a);
            if (n != a.a) {
                break;
            }
            p6.b(k(array, h));
        }
        return i;
    }
    
    public static int m(final g0 g0, final byte[] array, int m, final int n, final int n2, final a a) {
        final Object instance = g0.newInstance();
        m = M(instance, g0, array, m, n, n2, a);
        g0.d(instance);
        a.c = instance;
        return m;
    }
    
    public static int n(final g0 g0, final int n, final byte[] array, int n2, final int n3, final t.e e, final a a) {
        final int n4 = (n & 0xFFFFFFF8) | 0x4;
        n2 = m(g0, array, n2, n3, n4, a);
        while (true) {
            e.add(a.c);
            if (n2 >= n3) {
                break;
            }
            final int h = H(array, n2, a);
            if (n != a.a) {
                break;
            }
            n2 = m(g0, array, h, n3, n4, a);
        }
        return n2;
    }
    
    public static int o(final g0 g0, final byte[] array, int n, final int n2, final a a) {
        final Object instance = g0.newInstance();
        n = N(instance, g0, array, n, n2, a);
        g0.d(instance);
        a.c = instance;
        return n;
    }
    
    public static int p(final g0 g0, final int n, final byte[] array, int n2, final int n3, final t.e e, final a a) {
        n2 = o(g0, array, n2, n3, a);
        while (true) {
            e.add(a.c);
            if (n2 >= n3) {
                break;
            }
            final int h = H(array, n2, a);
            if (n != a.a) {
                break;
            }
            n2 = o(g0, array, h, n3, a);
        }
        return n2;
    }
    
    public static int q(final byte[] array, int i, final t.e e, final a a) {
        final f f = (f)e;
        i = H(array, i, a);
        final int n = a.a + i;
        while (i < n) {
            i = K(array, i, a);
            f.b(a.b != 0L);
        }
        if (i == n) {
            return i;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public static int r(final byte[] array, int i, final t.e e, final a a) {
        final j j = (j)e;
        int n;
        for (i = H(array, i, a), n = a.a + i; i < n; i += 8) {
            j.b(d(array, i));
        }
        if (i == n) {
            return i;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public static int s(final byte[] array, int i, final t.e e, final a a) {
        final s s = (s)e;
        int n;
        for (i = H(array, i, a), n = a.a + i; i < n; i += 4) {
            s.g(g(array, i));
        }
        if (i == n) {
            return i;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public static int t(final byte[] array, int i, final t.e e, final a a) {
        final w w = (w)e;
        int n;
        for (i = H(array, i, a), n = a.a + i; i < n; i += 8) {
            w.b(i(array, i));
        }
        if (i == n) {
            return i;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public static int u(final byte[] array, int i, final t.e e, final a a) {
        final p p4 = (p)e;
        int n;
        for (i = H(array, i, a), n = a.a + i; i < n; i += 4) {
            p4.b(k(array, i));
        }
        if (i == n) {
            return i;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public static int v(final byte[] array, int i, final t.e e, final a a) {
        final s s = (s)e;
        i = H(array, i, a);
        final int n = a.a + i;
        while (i < n) {
            i = H(array, i, a);
            s.g(g.b(a.a));
        }
        if (i == n) {
            return i;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public static int w(final byte[] array, int i, final t.e e, final a a) {
        final w w = (w)e;
        i = H(array, i, a);
        final int n = a.a + i;
        while (i < n) {
            i = K(array, i, a);
            w.b(g.c(a.b));
        }
        if (i == n) {
            return i;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public static int x(final byte[] array, int i, final t.e e, final a a) {
        final s s = (s)e;
        i = H(array, i, a);
        final int n = a.a + i;
        while (i < n) {
            i = H(array, i, a);
            s.g(a.a);
        }
        if (i == n) {
            return i;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public static int y(final byte[] array, int i, final t.e e, final a a) {
        final w w = (w)e;
        i = H(array, i, a);
        final int n = a.a + i;
        while (i < n) {
            i = K(array, i, a);
            w.b(a.b);
        }
        if (i == n) {
            return i;
        }
        throw InvalidProtocolBufferException.truncatedMessage();
    }
    
    public static int z(final int n, final byte[] array, int n2, final int n3, final t.e e, final a a) {
        final s s = (s)e;
        n2 = H(array, n2, a);
        while (true) {
            s.g(g.b(a.a));
            if (n2 >= n3) {
                break;
            }
            final int h = H(array, n2, a);
            if (n != a.a) {
                break;
            }
            n2 = H(array, h, a);
        }
        return n2;
    }
    
    public static final class a
    {
        public int a;
        public long b;
        public Object c;
        public final l d;
        
        public a(final l d) {
            d.getClass();
            this.d = d;
        }
    }
}

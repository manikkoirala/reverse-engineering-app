// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

public enum Syntax implements a
{
    private static final Syntax[] $VALUES;
    
    SYNTAX_PROTO2(0);
    
    public static final int SYNTAX_PROTO2_VALUE = 0;
    
    SYNTAX_PROTO3(1);
    
    public static final int SYNTAX_PROTO3_VALUE = 1;
    
    UNRECOGNIZED(-1);
    
    private static final t.b internalValueMap;
    private final int value;
    
    static {
        internalValueMap = new t.b() {};
    }
    
    private Syntax(final int value) {
        this.value = value;
    }
    
    public static Syntax forNumber(final int n) {
        if (n == 0) {
            return Syntax.SYNTAX_PROTO2;
        }
        if (n != 1) {
            return null;
        }
        return Syntax.SYNTAX_PROTO3;
    }
    
    public static t.b internalGetValueMap() {
        return Syntax.internalValueMap;
    }
    
    public static c internalGetVerifier() {
        return b.a;
    }
    
    @Deprecated
    public static Syntax valueOf(final int n) {
        return forNumber(n);
    }
    
    @Override
    public final int getNumber() {
        if (this != Syntax.UNRECOGNIZED) {
            return this.value;
        }
        throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
    }
    
    public static final class b implements c
    {
        public static final c a;
        
        static {
            a = new b();
        }
        
        @Override
        public boolean a(final int n) {
            return Syntax.forNumber(n) != null;
        }
    }
}

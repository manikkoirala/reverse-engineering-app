// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

import java.util.NoSuchElementException;
import java.util.AbstractSet;
import java.util.TreeMap;
import java.util.SortedMap;
import java.util.ArrayList;
import java.util.Set;
import java.util.Iterator;
import java.util.Collections;
import java.util.Map;
import java.util.List;
import java.util.AbstractMap;

public abstract class i0 extends AbstractMap
{
    public final int a;
    public List b;
    public Map c;
    public boolean d;
    public volatile g e;
    public Map f;
    public volatile c g;
    
    public i0(final int a) {
        this.a = a;
        this.b = Collections.emptyList();
        this.c = Collections.emptyMap();
        this.f = Collections.emptyMap();
    }
    
    public static /* synthetic */ List b(final i0 i0) {
        return i0.b;
    }
    
    public static /* synthetic */ Map c(final i0 i0) {
        return i0.c;
    }
    
    public static /* synthetic */ Map e(final i0 i0) {
        return i0.f;
    }
    
    public static i0 q(final int n) {
        return new i0(n) {
            @Override
            public void p() {
                if (!this.o()) {
                    if (this.k() > 0) {
                        zu0.a(this.j(0).getKey());
                        throw null;
                    }
                    final Iterator iterator = this.m().iterator();
                    if (iterator.hasNext()) {
                        zu0.a(((Map.Entry<Object, V>)iterator.next()).getKey());
                        throw null;
                    }
                }
                super.p();
            }
        };
    }
    
    @Override
    public void clear() {
        this.g();
        if (!this.b.isEmpty()) {
            this.b.clear();
        }
        if (!this.c.isEmpty()) {
            this.c.clear();
        }
    }
    
    @Override
    public boolean containsKey(final Object o) {
        final Comparable comparable = (Comparable)o;
        return this.f(comparable) >= 0 || this.c.containsKey(comparable);
    }
    
    @Override
    public Set entrySet() {
        if (this.e == null) {
            this.e = new g(null);
        }
        return this.e;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof i0)) {
            return super.equals(o);
        }
        final i0 i0 = (i0)o;
        final int size = this.size();
        if (size != i0.size()) {
            return false;
        }
        final int k = this.k();
        if (k != i0.k()) {
            return this.entrySet().equals(i0.entrySet());
        }
        for (int j = 0; j < k; ++j) {
            if (!this.j(j).equals(i0.j(j))) {
                return false;
            }
        }
        return k == size || this.c.equals(i0.c);
    }
    
    public final int f(final Comparable comparable) {
        int n = this.b.size() - 1;
        if (n >= 0) {
            final int compareTo = comparable.compareTo(this.b.get(n).d());
            if (compareTo > 0) {
                return -(n + 2);
            }
            if (compareTo == 0) {
                return n;
            }
        }
        int i = 0;
        while (i <= n) {
            final int n2 = (i + n) / 2;
            final int compareTo2 = comparable.compareTo(this.b.get(n2).d());
            if (compareTo2 < 0) {
                n = n2 - 1;
            }
            else {
                if (compareTo2 <= 0) {
                    return n2;
                }
                i = n2 + 1;
            }
        }
        return -(i + 1);
    }
    
    public final void g() {
        if (!this.d) {
            return;
        }
        throw new UnsupportedOperationException();
    }
    
    @Override
    public Object get(final Object o) {
        final Comparable comparable = (Comparable)o;
        final int f = this.f(comparable);
        if (f >= 0) {
            return ((e)this.b.get(f)).getValue();
        }
        return this.c.get(comparable);
    }
    
    public Set h() {
        if (this.g == null) {
            this.g = new c(null);
        }
        return this.g;
    }
    
    @Override
    public int hashCode() {
        final int k = this.k();
        int i = 0;
        int n = 0;
        while (i < k) {
            n += this.b.get(i).hashCode();
            ++i;
        }
        int n2 = n;
        if (this.l() > 0) {
            n2 = n + this.c.hashCode();
        }
        return n2;
    }
    
    public final void i() {
        this.g();
        if (this.b.isEmpty() && !(this.b instanceof ArrayList)) {
            this.b = new ArrayList(this.a);
        }
    }
    
    public Entry j(final int n) {
        return (Entry)this.b.get(n);
    }
    
    public int k() {
        return this.b.size();
    }
    
    public int l() {
        return this.c.size();
    }
    
    public Iterable m() {
        Iterable iterable;
        if (this.c.isEmpty()) {
            iterable = i0.d.b();
        }
        else {
            iterable = this.c.entrySet();
        }
        return iterable;
    }
    
    public final SortedMap n() {
        this.g();
        if (this.c.isEmpty() && !(this.c instanceof TreeMap)) {
            final TreeMap c = new TreeMap();
            this.c = c;
            this.f = c.descendingMap();
        }
        return (SortedMap)this.c;
    }
    
    public boolean o() {
        return this.d;
    }
    
    public void p() {
        if (!this.d) {
            Map<Object, Object> c;
            if (this.c.isEmpty()) {
                c = Collections.emptyMap();
            }
            else {
                c = Collections.unmodifiableMap((Map<?, ?>)this.c);
            }
            this.c = c;
            Map<Object, Object> f;
            if (this.f.isEmpty()) {
                f = Collections.emptyMap();
            }
            else {
                f = Collections.unmodifiableMap((Map<?, ?>)this.f);
            }
            this.f = f;
            this.d = true;
        }
    }
    
    public Object r(final Comparable comparable, final Object value) {
        this.g();
        final int f = this.f(comparable);
        if (f >= 0) {
            return ((e)this.b.get(f)).setValue(value);
        }
        this.i();
        final int n = -(f + 1);
        if (n >= this.a) {
            return this.n().put(comparable, value);
        }
        final int size = this.b.size();
        final int a = this.a;
        if (size == a) {
            final e e = this.b.remove(a - 1);
            this.n().put(e.d(), e.getValue());
        }
        this.b.add(n, new e(comparable, value));
        return null;
    }
    
    @Override
    public Object remove(final Object o) {
        this.g();
        final Comparable comparable = (Comparable)o;
        final int f = this.f(comparable);
        if (f >= 0) {
            return this.s(f);
        }
        if (this.c.isEmpty()) {
            return null;
        }
        return this.c.remove(comparable);
    }
    
    public final Object s(final int n) {
        this.g();
        final Object value = this.b.remove(n).getValue();
        if (!this.c.isEmpty()) {
            final Iterator iterator = this.n().entrySet().iterator();
            this.b.add(new e((Entry)iterator.next()));
            iterator.remove();
        }
        return value;
    }
    
    @Override
    public int size() {
        return this.b.size() + this.c.size();
    }
    
    public class b implements Iterator
    {
        public int a;
        public Iterator b;
        public final i0 c;
        
        public b(final i0 c) {
            this.c = c;
            this.a = i0.b(c).size();
        }
        
        public final Iterator b() {
            if (this.b == null) {
                this.b = i0.e(this.c).entrySet().iterator();
            }
            return this.b;
        }
        
        public Entry c() {
            Map.Entry entry;
            if (this.b().hasNext()) {
                entry = this.b().next();
            }
            else {
                final List b = i0.b(this.c);
                final int a = this.a - 1;
                this.a = a;
                entry = (Map.Entry)b.get(a);
            }
            return (Entry)entry;
        }
        
        @Override
        public boolean hasNext() {
            final int a = this.a;
            return (a > 0 && a <= i0.b(this.c).size()) || this.b().hasNext();
        }
        
        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
    
    public class c extends g
    {
        public final i0 b;
        
        public c(final i0 b) {
            this.b = b.super(null);
        }
        
        @Override
        public Iterator iterator() {
            return this.b.new b(null);
        }
    }
    
    public class g extends AbstractSet
    {
        public final i0 a;
        
        public g(final i0 a) {
            this.a = a;
        }
        
        public boolean a(final Entry entry) {
            if (!this.contains(entry)) {
                this.a.r(entry.getKey(), entry.getValue());
                return true;
            }
            return false;
        }
        
        @Override
        public void clear() {
            this.a.clear();
        }
        
        @Override
        public boolean contains(Object value) {
            final Entry entry = (Entry)value;
            value = this.a.get(entry.getKey());
            final Object value2 = entry.getValue();
            return value == value2 || (value != null && value.equals(value2));
        }
        
        @Override
        public Iterator iterator() {
            return this.a.new f(null);
        }
        
        @Override
        public boolean remove(final Object o) {
            final Entry entry = (Entry)o;
            if (this.contains(entry)) {
                this.a.remove(entry.getKey());
                return true;
            }
            return false;
        }
        
        @Override
        public int size() {
            return this.a.size();
        }
    }
    
    public abstract static class d
    {
        public static final Iterator a;
        public static final Iterable b;
        
        static {
            a = new Iterator() {
                @Override
                public boolean hasNext() {
                    return false;
                }
                
                @Override
                public Object next() {
                    throw new NoSuchElementException();
                }
                
                @Override
                public void remove() {
                    throw new UnsupportedOperationException();
                }
            };
            b = new Iterable() {
                @Override
                public Iterator iterator() {
                    return d.a();
                }
            };
        }
        
        public static /* synthetic */ Iterator a() {
            return d.a;
        }
        
        public static Iterable b() {
            return d.b;
        }
    }
    
    public class e implements Entry, Comparable
    {
        public final Comparable a;
        public Object b;
        public final i0 c;
        
        public e(final i0 c, final Comparable a, final Object b) {
            this.c = c;
            this.a = a;
            this.b = b;
        }
        
        public e(final i0 i0, final Entry entry) {
            this(i0, entry.getKey(), entry.getValue());
        }
        
        public int a(final e e) {
            return this.d().compareTo(e.d());
        }
        
        public final boolean c(final Object o, final Object obj) {
            boolean equals;
            if (o == null) {
                equals = (obj == null);
            }
            else {
                equals = o.equals(obj);
            }
            return equals;
        }
        
        public Comparable d() {
            return this.a;
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (o == this) {
                return true;
            }
            if (!(o instanceof Entry)) {
                return false;
            }
            final Entry entry = (Entry)o;
            if (!this.c(this.a, entry.getKey()) || !this.c(this.b, entry.getValue())) {
                b = false;
            }
            return b;
        }
        
        @Override
        public Object getValue() {
            return this.b;
        }
        
        @Override
        public int hashCode() {
            final Comparable a = this.a;
            int hashCode = 0;
            int hashCode2;
            if (a == null) {
                hashCode2 = 0;
            }
            else {
                hashCode2 = a.hashCode();
            }
            final Object b = this.b;
            if (b != null) {
                hashCode = b.hashCode();
            }
            return hashCode2 ^ hashCode;
        }
        
        @Override
        public Object setValue(final Object b) {
            this.c.g();
            final Object b2 = this.b;
            this.b = b;
            return b2;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.a);
            sb.append("=");
            sb.append(this.b);
            return sb.toString();
        }
    }
    
    public class f implements Iterator
    {
        public int a;
        public boolean b;
        public Iterator c;
        public final i0 d;
        
        public f(final i0 d) {
            this.d = d;
            this.a = -1;
        }
        
        public final Iterator b() {
            if (this.c == null) {
                this.c = i0.c(this.d).entrySet().iterator();
            }
            return this.c;
        }
        
        public Entry c() {
            this.b = true;
            final int a = this.a + 1;
            this.a = a;
            Map.Entry entry;
            if (a < i0.b(this.d).size()) {
                entry = i0.b(this.d).get(this.a);
            }
            else {
                entry = this.b().next();
            }
            return (Entry)entry;
        }
        
        @Override
        public boolean hasNext() {
            final int a = this.a;
            boolean b = true;
            if (a + 1 >= i0.b(this.d).size()) {
                b = (!i0.c(this.d).isEmpty() && this.b().hasNext() && b);
            }
            return b;
        }
        
        @Override
        public void remove() {
            if (this.b) {
                this.b = false;
                this.d.g();
                if (this.a < i0.b(this.d).size()) {
                    this.d.s(this.a--);
                }
                else {
                    this.b().remove();
                }
                return;
            }
            throw new IllegalStateException("remove() was called before next()");
        }
    }
}

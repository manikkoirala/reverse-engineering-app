// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

public final class r extends GeneratedMessageLite implements xv0
{
    private static final r DEFAULT_INSTANCE;
    private static volatile b31 PARSER;
    public static final int VALUE_FIELD_NUMBER = 1;
    private int value_;
    
    static {
        GeneratedMessageLite.V(r.class, DEFAULT_INSTANCE = new r());
    }
    
    public static /* synthetic */ r Z() {
        return r.DEFAULT_INSTANCE;
    }
    
    public static r b0() {
        return r.DEFAULT_INSTANCE;
    }
    
    public static b d0() {
        return (b)r.DEFAULT_INSTANCE.u();
    }
    
    public int c0() {
        return this.value_;
    }
    
    public final void e0(final int value_) {
        this.value_ = value_;
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (r$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = r.PARSER) == null) {
                    synchronized (r.class) {
                        if (r.PARSER == null) {
                            r.PARSER = new GeneratedMessageLite.b(r.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return r.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(r.DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\u0004", new Object[] { "value_" });
            }
            case 2: {
                return new b((r$a)null);
            }
            case 1: {
                return new r();
            }
        }
    }
    
    public static final class b extends a implements xv0
    {
        public b() {
            super(r.Z());
        }
        
        public b A(final int n) {
            ((a)this).s();
            ((r)super.b).e0(n);
            return this;
        }
    }
}

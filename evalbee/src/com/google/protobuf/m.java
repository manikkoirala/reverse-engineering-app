// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

import java.util.Map;

public abstract class m
{
    public abstract int a(final Map.Entry p0);
    
    public abstract Object b(final l p0, final a0 p1, final int p2);
    
    public abstract o c(final Object p0);
    
    public abstract o d(final Object p0);
    
    public abstract boolean e(final a0 p0);
    
    public abstract void f(final Object p0);
    
    public abstract Object g(final Object p0, final f0 p1, final Object p2, final l p3, final o p4, final Object p5, final k0 p6);
    
    public abstract void h(final f0 p0, final Object p1, final l p2, final o p3);
    
    public abstract void i(final ByteString p0, final Object p1, final l p2, final o p3);
    
    public abstract void j(final Writer p0, final Map.Entry p1);
}

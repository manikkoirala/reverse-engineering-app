// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

import java.util.Map;
import java.util.List;

public interface f0
{
    int A();
    
    void B(final List p0);
    
    void C(final List p0);
    
    int D();
    
    long E();
    
    String F();
    
    int G();
    
    String H();
    
    void I(final Map p0, final x.a p1, final l p2);
    
    void J(final Object p0, final g0 p1, final l p2);
    
    void K(final Object p0, final g0 p1, final l p2);
    
    void L(final List p0, final g0 p1, final l p2);
    
    void M(final List p0, final g0 p1, final l p2);
    
    void a(final List p0);
    
    long b();
    
    int c();
    
    int d();
    
    int e();
    
    void f(final List p0);
    
    ByteString g();
    
    int getTag();
    
    void h(final List p0);
    
    long i();
    
    void j(final List p0);
    
    void k(final List p0);
    
    void l(final List p0);
    
    int m();
    
    void n(final List p0);
    
    void o(final List p0);
    
    boolean p();
    
    void q(final List p0);
    
    void r(final List p0);
    
    double readDouble();
    
    float readFloat();
    
    long s();
    
    long t();
    
    void u(final List p0);
    
    boolean v();
    
    void w(final List p0);
    
    void x(final List p0);
    
    void y(final List p0);
    
    void z(final List p0);
}

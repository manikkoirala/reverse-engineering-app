// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

public final class d extends GeneratedMessageLite implements xv0
{
    private static final d DEFAULT_INSTANCE;
    private static volatile b31 PARSER;
    public static final int TYPE_URL_FIELD_NUMBER = 1;
    public static final int VALUE_FIELD_NUMBER = 2;
    private String typeUrl_;
    private ByteString value_;
    
    static {
        GeneratedMessageLite.V(d.class, DEFAULT_INSTANCE = new d());
    }
    
    public d() {
        this.typeUrl_ = "";
        this.value_ = ByteString.EMPTY;
    }
    
    public static /* synthetic */ d Z() {
        return d.DEFAULT_INSTANCE;
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (d$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = d.PARSER) == null) {
                    synchronized (d.class) {
                        if (d.PARSER == null) {
                            d.PARSER = new GeneratedMessageLite.b(d.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return d.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(d.DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0208\u0002\n", new Object[] { "typeUrl_", "value_" });
            }
            case 2: {
                return new b((d$a)null);
            }
            case 1: {
                return new d();
            }
        }
    }
    
    public static final class b extends a implements xv0
    {
        public b() {
            super(d.Z());
        }
    }
}

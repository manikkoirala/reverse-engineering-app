// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.nio.ByteBuffer;
import java.io.InputStream;

public abstract class g
{
    public static volatile int f = 100;
    public int a;
    public int b;
    public int c;
    public h d;
    public boolean e;
    
    public g() {
        this.b = g.f;
        this.c = Integer.MAX_VALUE;
        this.e = false;
    }
    
    public static int b(final int n) {
        return -(n & 0x1) ^ n >>> 1;
    }
    
    public static long c(final long n) {
        return -(n & 0x1L) ^ n >>> 1;
    }
    
    public static g f(final InputStream inputStream) {
        return g(inputStream, 4096);
    }
    
    public static g g(final InputStream inputStream, final int n) {
        if (n <= 0) {
            throw new IllegalArgumentException("bufferSize must be > 0");
        }
        if (inputStream == null) {
            return j(t.d);
        }
        return new d(inputStream, n, null);
    }
    
    public static g h(final Iterable iterable, final boolean b) {
        final Iterator iterator = iterable.iterator();
        int n = 0;
        int n2 = 0;
        while (iterator.hasNext()) {
            final ByteBuffer byteBuffer = (ByteBuffer)iterator.next();
            n2 += byteBuffer.remaining();
            if (byteBuffer.hasArray()) {
                n |= 0x1;
            }
            else if (byteBuffer.isDirect()) {
                n |= 0x2;
            }
            else {
                n |= 0x4;
            }
        }
        if (n == 2) {
            return new c(iterable, n2, b, null);
        }
        return f(new qg0(iterable));
    }
    
    public static g i(final ByteBuffer byteBuffer, final boolean b) {
        if (byteBuffer.hasArray()) {
            return l(byteBuffer.array(), byteBuffer.arrayOffset() + byteBuffer.position(), byteBuffer.remaining(), b);
        }
        if (byteBuffer.isDirect() && e.J()) {
            return new e(byteBuffer, b, null);
        }
        final int remaining = byteBuffer.remaining();
        final byte[] dst = new byte[remaining];
        byteBuffer.duplicate().get(dst);
        return l(dst, 0, remaining, true);
    }
    
    public static g j(final byte[] array) {
        return k(array, 0, array.length);
    }
    
    public static g k(final byte[] array, final int n, final int n2) {
        return l(array, n, n2, false);
    }
    
    public static g l(final byte[] array, final int n, final int n2, final boolean b) {
        final b b2 = new b(array, n, n2, b, null);
        try {
            b2.n(n2);
            return b2;
        }
        catch (final InvalidProtocolBufferException cause) {
            throw new IllegalArgumentException(cause);
        }
    }
    
    public abstract long A();
    
    public abstract String B();
    
    public abstract String C();
    
    public abstract int D();
    
    public abstract int E();
    
    public abstract long F();
    
    public final int G(final int n) {
        if (n >= 0) {
            final int c = this.c;
            this.c = n;
            return c;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Size limit cannot be negative: ");
        sb.append(n);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public abstract boolean H(final int p0);
    
    public abstract void a(final int p0);
    
    public abstract int d();
    
    public abstract boolean e();
    
    public abstract void m(final int p0);
    
    public abstract int n(final int p0);
    
    public abstract boolean o();
    
    public abstract ByteString p();
    
    public abstract double q();
    
    public abstract int r();
    
    public abstract int s();
    
    public abstract long t();
    
    public abstract float u();
    
    public abstract int v();
    
    public abstract long w();
    
    public abstract int x();
    
    public abstract long y();
    
    public abstract int z();
    
    public static final class b extends g
    {
        public final byte[] g;
        public final boolean h;
        public int i;
        public int j;
        public int k;
        public int l;
        public int m;
        public boolean n;
        public int o;
        
        public b(final byte[] g, final int n, final int n2, final boolean h) {
            super(null);
            this.o = Integer.MAX_VALUE;
            this.g = g;
            this.i = n2 + n;
            this.k = n;
            this.l = n;
            this.h = h;
        }
        
        @Override
        public long A() {
            return com.google.protobuf.g.c(this.N());
        }
        
        @Override
        public String B() {
            final int m = this.M();
            if (m > 0) {
                final int i = this.i;
                final int k = this.k;
                if (m <= i - k) {
                    final String s = new String(this.g, k, m, t.b);
                    this.k += m;
                    return s;
                }
            }
            if (m == 0) {
                return "";
            }
            if (m < 0) {
                throw InvalidProtocolBufferException.negativeSize();
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }
        
        @Override
        public String C() {
            final int m = this.M();
            if (m > 0) {
                final int i = this.i;
                final int k = this.k;
                if (m <= i - k) {
                    final String h = Utf8.h(this.g, k, m);
                    this.k += m;
                    return h;
                }
            }
            if (m == 0) {
                return "";
            }
            if (m <= 0) {
                throw InvalidProtocolBufferException.negativeSize();
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }
        
        @Override
        public int D() {
            if (this.e()) {
                return this.m = 0;
            }
            final int m = this.M();
            this.m = m;
            if (WireFormat.a(m) != 0) {
                return this.m;
            }
            throw InvalidProtocolBufferException.invalidTag();
        }
        
        @Override
        public int E() {
            return this.M();
        }
        
        @Override
        public long F() {
            return this.N();
        }
        
        @Override
        public boolean H(final int n) {
            final int b = WireFormat.b(n);
            if (b == 0) {
                this.S();
                return true;
            }
            if (b == 1) {
                this.R(8);
                return true;
            }
            if (b == 2) {
                this.R(this.M());
                return true;
            }
            if (b == 3) {
                this.Q();
                this.a(WireFormat.c(WireFormat.a(n), 4));
                return true;
            }
            if (b == 4) {
                return false;
            }
            if (b == 5) {
                this.R(4);
                return true;
            }
            throw InvalidProtocolBufferException.invalidWireType();
        }
        
        public byte I() {
            final int k = this.k;
            if (k != this.i) {
                final byte[] g = this.g;
                this.k = k + 1;
                return g[k];
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }
        
        public byte[] J(int n) {
            if (n > 0) {
                final int i = this.i;
                final int k = this.k;
                if (n <= i - k) {
                    n += k;
                    this.k = n;
                    return Arrays.copyOfRange(this.g, k, n);
                }
            }
            if (n > 0) {
                throw InvalidProtocolBufferException.truncatedMessage();
            }
            if (n == 0) {
                return t.d;
            }
            throw InvalidProtocolBufferException.negativeSize();
        }
        
        public int K() {
            final int k = this.k;
            if (this.i - k >= 4) {
                final byte[] g = this.g;
                this.k = k + 4;
                return (g[k + 3] & 0xFF) << 24 | ((g[k] & 0xFF) | (g[k + 1] & 0xFF) << 8 | (g[k + 2] & 0xFF) << 16);
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }
        
        public long L() {
            final int k = this.k;
            if (this.i - k >= 8) {
                final byte[] g = this.g;
                this.k = k + 8;
                return ((long)g[k + 7] & 0xFFL) << 56 | (((long)g[k] & 0xFFL) | ((long)g[k + 1] & 0xFFL) << 8 | ((long)g[k + 2] & 0xFFL) << 16 | ((long)g[k + 3] & 0xFFL) << 24 | ((long)g[k + 4] & 0xFFL) << 32 | ((long)g[k + 5] & 0xFFL) << 40 | ((long)g[k + 6] & 0xFFL) << 48);
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }
        
        public int M() {
            final int k = this.k;
            final int i = this.i;
            if (i != k) {
                final byte[] g = this.g;
                final int j = k + 1;
                final byte b = g[k];
                if (b >= 0) {
                    this.k = j;
                    return b;
                }
                if (i - j >= 9) {
                    int l = j + 1;
                    final int n = b ^ g[j] << 7;
                    int n2;
                    if (n < 0) {
                        n2 = (n ^ 0xFFFFFF80);
                    }
                    else {
                        final int n3 = l + 1;
                        final int n4 = n ^ g[l] << 14;
                        if (n4 >= 0) {
                            final int n5 = n4 ^ 0x3F80;
                            l = n3;
                            n2 = n5;
                        }
                        else {
                            l = n3 + 1;
                            final int n6 = n4 ^ g[n3] << 21;
                            if (n6 < 0) {
                                n2 = (n6 ^ 0xFFE03F80);
                            }
                            else {
                                final int n7 = l + 1;
                                final byte b2 = g[l];
                                final int n8 = n2 = (n6 ^ b2 << 28 ^ 0xFE03F80);
                                l = n7;
                                if (b2 < 0) {
                                    final int n9 = n7 + 1;
                                    n2 = n8;
                                    l = n9;
                                    if (g[n7] < 0) {
                                        final int n10 = n9 + 1;
                                        n2 = n8;
                                        l = n10;
                                        if (g[n9] < 0) {
                                            final int n11 = n10 + 1;
                                            n2 = n8;
                                            l = n11;
                                            if (g[n10] < 0) {
                                                final int n12 = n11 + 1;
                                                n2 = n8;
                                                l = n12;
                                                if (g[n11] < 0) {
                                                    l = n12 + 1;
                                                    n2 = n8;
                                                    if (g[n12] < 0) {
                                                        return (int)this.O();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    this.k = l;
                    return n2;
                }
            }
            return (int)this.O();
        }
        
        public long N() {
            final int k = this.k;
            final int i = this.i;
            if (i != k) {
                final byte[] g = this.g;
                final int j = k + 1;
                final byte b = g[k];
                if (b >= 0) {
                    this.k = j;
                    return b;
                }
                if (i - j >= 9) {
                    int l = j + 1;
                    final int n = b ^ g[j] << 7;
                    long n5 = 0L;
                    Label_0342: {
                        int n2;
                        if (n < 0) {
                            n2 = (n ^ 0xFFFFFF80);
                        }
                        else {
                            final int n3 = l + 1;
                            final int n4 = n ^ g[l] << 14;
                            if (n4 >= 0) {
                                n5 = (n4 ^ 0x3F80);
                                l = n3;
                                break Label_0342;
                            }
                            l = n3 + 1;
                            final int n6 = n4 ^ g[n3] << 21;
                            if (n6 >= 0) {
                                final long n7 = n6;
                                final int n8 = l + 1;
                                long n9 = n7 ^ (long)g[l] << 28;
                                long n13 = 0L;
                                Label_0178: {
                                    if (n9 < 0L) {
                                        l = n8 + 1;
                                        long n10 = n9 ^ (long)g[n8] << 35;
                                        long n11;
                                        if (n10 < 0L) {
                                            n11 = -34093383808L;
                                        }
                                        else {
                                            final int n12 = l + 1;
                                            n9 = (n10 ^ (long)g[l] << 42);
                                            if (n9 >= 0L) {
                                                n13 = 4363953127296L;
                                                l = n12;
                                                break Label_0178;
                                            }
                                            l = n12 + 1;
                                            n10 = (n9 ^ (long)g[n12] << 49);
                                            if (n10 < 0L) {
                                                n11 = -558586000294016L;
                                            }
                                            else {
                                                final int n14 = l + 1;
                                                n5 = (n10 ^ (long)g[l] << 56 ^ 0xFE03F80FE03F80L);
                                                if (n5 >= 0L) {
                                                    l = n14;
                                                    break Label_0342;
                                                }
                                                l = n14 + 1;
                                                if (g[n14] < 0L) {
                                                    return this.O();
                                                }
                                                break Label_0342;
                                            }
                                        }
                                        n5 = (n10 ^ n11);
                                        break Label_0342;
                                    }
                                    n13 = 266354560L;
                                    l = n8;
                                }
                                n5 = (n9 ^ n13);
                                break Label_0342;
                            }
                            n2 = (n6 ^ 0xFFE03F80);
                        }
                        n5 = n2;
                    }
                    this.k = l;
                    return n5;
                }
            }
            return this.O();
        }
        
        public long O() {
            long n = 0L;
            for (int i = 0; i < 64; i += 7) {
                final byte j = this.I();
                n |= (long)(j & 0x7F) << i;
                if ((j & 0x80) == 0x0) {
                    return n;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }
        
        public final void P() {
            final int i = this.i + this.j;
            this.i = i;
            final int n = i - this.l;
            final int o = this.o;
            if (n > o) {
                final int j = n - o;
                this.j = j;
                this.i = i - j;
            }
            else {
                this.j = 0;
            }
        }
        
        public void Q() {
            int d;
            do {
                d = this.D();
            } while (d != 0 && this.H(d));
        }
        
        public void R(final int n) {
            if (n >= 0) {
                final int i = this.i;
                final int k = this.k;
                if (n <= i - k) {
                    this.k = k + n;
                    return;
                }
            }
            if (n < 0) {
                throw InvalidProtocolBufferException.negativeSize();
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }
        
        public final void S() {
            if (this.i - this.k >= 10) {
                this.T();
            }
            else {
                this.U();
            }
        }
        
        public final void T() {
            for (int i = 0; i < 10; ++i) {
                if (this.g[this.k++] >= 0) {
                    return;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }
        
        public final void U() {
            for (int i = 0; i < 10; ++i) {
                if (this.I() >= 0) {
                    return;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }
        
        @Override
        public void a(final int n) {
            if (this.m == n) {
                return;
            }
            throw InvalidProtocolBufferException.invalidEndTag();
        }
        
        @Override
        public int d() {
            return this.k - this.l;
        }
        
        @Override
        public boolean e() {
            return this.k == this.i;
        }
        
        @Override
        public void m(final int o) {
            this.o = o;
            this.P();
        }
        
        @Override
        public int n(int o) {
            if (o < 0) {
                throw InvalidProtocolBufferException.negativeSize();
            }
            o += this.d();
            if (o < 0) {
                throw InvalidProtocolBufferException.parseFailure();
            }
            final int o2 = this.o;
            if (o <= o2) {
                this.o = o;
                this.P();
                return o2;
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }
        
        @Override
        public boolean o() {
            return this.N() != 0L;
        }
        
        @Override
        public ByteString p() {
            final int m = this.M();
            if (m > 0) {
                final int i = this.i;
                final int k = this.k;
                if (m <= i - k) {
                    ByteString byteString;
                    if (this.h && this.n) {
                        byteString = ByteString.wrap(this.g, k, m);
                    }
                    else {
                        byteString = ByteString.copyFrom(this.g, k, m);
                    }
                    this.k += m;
                    return byteString;
                }
            }
            if (m == 0) {
                return ByteString.EMPTY;
            }
            return ByteString.wrap(this.J(m));
        }
        
        @Override
        public double q() {
            return Double.longBitsToDouble(this.L());
        }
        
        @Override
        public int r() {
            return this.M();
        }
        
        @Override
        public int s() {
            return this.K();
        }
        
        @Override
        public long t() {
            return this.L();
        }
        
        @Override
        public float u() {
            return Float.intBitsToFloat(this.K());
        }
        
        @Override
        public int v() {
            return this.M();
        }
        
        @Override
        public long w() {
            return this.N();
        }
        
        @Override
        public int x() {
            return this.K();
        }
        
        @Override
        public long y() {
            return this.L();
        }
        
        @Override
        public int z() {
            return com.google.protobuf.g.b(this.M());
        }
    }
    
    public static final class c extends g
    {
        public final Iterable g;
        public final Iterator h;
        public ByteBuffer i;
        public final boolean j;
        public boolean k;
        public int l;
        public int m;
        public int n;
        public int o;
        public int p;
        public int q;
        public long r;
        public long s;
        public long t;
        public long u;
        
        public c(final Iterable g, final int l, final boolean j) {
            super(null);
            this.n = Integer.MAX_VALUE;
            this.l = l;
            this.g = g;
            this.h = g.iterator();
            this.j = j;
            this.p = 0;
            this.q = 0;
            if (l == 0) {
                this.i = t.e;
                this.r = 0L;
                this.s = 0L;
                this.u = 0L;
                this.t = 0L;
            }
            else {
                this.X();
            }
        }
        
        @Override
        public long A() {
            return com.google.protobuf.g.c(this.P());
        }
        
        @Override
        public String B() {
            final int o = this.O();
            if (o > 0) {
                final long n = o;
                final long u = this.u;
                final long r = this.r;
                if (n <= u - r) {
                    final byte[] bytes = new byte[o];
                    d12.p(r, bytes, 0L, n);
                    final String s = new String(bytes, com.google.protobuf.t.b);
                    this.r += n;
                    return s;
                }
            }
            if (o > 0 && o <= this.S()) {
                final byte[] bytes2 = new byte[o];
                this.L(bytes2, 0, o);
                return new String(bytes2, com.google.protobuf.t.b);
            }
            if (o == 0) {
                return "";
            }
            if (o < 0) {
                throw InvalidProtocolBufferException.negativeSize();
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }
        
        @Override
        public String C() {
            final int o = this.O();
            if (o > 0) {
                final long n = o;
                final long u = this.u;
                final long r = this.r;
                if (n <= u - r) {
                    final String g = Utf8.g(this.i, (int)(r - this.s), o);
                    this.r += n;
                    return g;
                }
            }
            if (o >= 0 && o <= this.S()) {
                final byte[] array = new byte[o];
                this.L(array, 0, o);
                return Utf8.h(array, 0, o);
            }
            if (o == 0) {
                return "";
            }
            if (o <= 0) {
                throw InvalidProtocolBufferException.negativeSize();
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }
        
        @Override
        public int D() {
            if (this.e()) {
                return this.o = 0;
            }
            final int o = this.O();
            this.o = o;
            if (WireFormat.a(o) != 0) {
                return this.o;
            }
            throw InvalidProtocolBufferException.invalidTag();
        }
        
        @Override
        public int E() {
            return this.O();
        }
        
        @Override
        public long F() {
            return this.P();
        }
        
        @Override
        public boolean H(final int n) {
            final int b = WireFormat.b(n);
            if (b == 0) {
                this.V();
                return true;
            }
            if (b == 1) {
                this.U(8);
                return true;
            }
            if (b == 2) {
                this.U(this.O());
                return true;
            }
            if (b == 3) {
                this.T();
                this.a(WireFormat.c(WireFormat.a(n), 4));
                return true;
            }
            if (b == 4) {
                return false;
            }
            if (b == 5) {
                this.U(4);
                return true;
            }
            throw InvalidProtocolBufferException.invalidWireType();
        }
        
        public final long I() {
            return this.u - this.r;
        }
        
        public final void J() {
            if (this.h.hasNext()) {
                this.X();
                return;
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }
        
        public byte K() {
            if (this.I() == 0L) {
                this.J();
            }
            final long r = this.r;
            this.r = 1L + r;
            return d12.w(r);
        }
        
        public final void L(final byte[] array, final int n, final int n2) {
            if (n2 >= 0 && n2 <= this.S()) {
                int min;
                long n4;
                for (int i = n2; i > 0; i -= min, this.r += n4) {
                    if (this.I() == 0L) {
                        this.J();
                    }
                    min = Math.min(i, (int)this.I());
                    final long r = this.r;
                    final long n3 = n2 - i + n;
                    n4 = min;
                    d12.p(r, array, n3, n4);
                }
                return;
            }
            if (n2 > 0) {
                throw InvalidProtocolBufferException.truncatedMessage();
            }
            if (n2 == 0) {
                return;
            }
            throw InvalidProtocolBufferException.negativeSize();
        }
        
        public int M() {
            if (this.I() >= 4L) {
                final long r = this.r;
                this.r = 4L + r;
                return (d12.w(r + 3L) & 0xFF) << 24 | ((d12.w(r) & 0xFF) | (d12.w(1L + r) & 0xFF) << 8 | (d12.w(2L + r) & 0xFF) << 16);
            }
            return (this.K() & 0xFF) | (this.K() & 0xFF) << 8 | (this.K() & 0xFF) << 16 | (this.K() & 0xFF) << 24;
        }
        
        public long N() {
            long n;
            byte b;
            if (this.I() >= 8L) {
                final long r = this.r;
                this.r = 8L + r;
                n = (((long)d12.w(r) & 0xFFL) | ((long)d12.w(1L + r) & 0xFFL) << 8 | ((long)d12.w(2L + r) & 0xFFL) << 16 | ((long)d12.w(3L + r) & 0xFFL) << 24 | ((long)d12.w(4L + r) & 0xFFL) << 32 | ((long)d12.w(5L + r) & 0xFFL) << 40 | ((long)d12.w(6L + r) & 0xFFL) << 48);
                b = d12.w(r + 7L);
            }
            else {
                n = (((long)this.K() & 0xFFL) | ((long)this.K() & 0xFFL) << 8 | ((long)this.K() & 0xFFL) << 16 | ((long)this.K() & 0xFFL) << 24 | ((long)this.K() & 0xFFL) << 32 | ((long)this.K() & 0xFFL) << 40 | ((long)this.K() & 0xFFL) << 48);
                b = this.K();
            }
            return ((long)b & 0xFFL) << 56 | n;
        }
        
        public int O() {
            final long r = this.r;
            if (this.u != r) {
                final long n = r + 1L;
                final byte w = d12.w(r);
                if (w >= 0) {
                    ++this.r;
                    return w;
                }
                if (this.u - this.r >= 10L) {
                    long r2 = n + 1L;
                    final int n2 = w ^ d12.w(n) << 7;
                    int n3;
                    if (n2 < 0) {
                        n3 = (n2 ^ 0xFFFFFF80);
                    }
                    else {
                        final long n4 = r2 + 1L;
                        final int n5 = n2 ^ d12.w(r2) << 14;
                        if (n5 >= 0) {
                            n3 = (n5 ^ 0x3F80);
                            r2 = n4;
                        }
                        else {
                            r2 = n4 + 1L;
                            final int n6 = n5 ^ d12.w(n4) << 21;
                            if (n6 < 0) {
                                n3 = (n6 ^ 0xFFE03F80);
                            }
                            else {
                                final long n7 = r2 + 1L;
                                final byte w2 = d12.w(r2);
                                final int n8 = n3 = (n6 ^ w2 << 28 ^ 0xFE03F80);
                                r2 = n7;
                                if (w2 < 0) {
                                    final long n9 = n7 + 1L;
                                    n3 = n8;
                                    r2 = n9;
                                    if (d12.w(n7) < 0) {
                                        final long n10 = n9 + 1L;
                                        n3 = n8;
                                        r2 = n10;
                                        if (d12.w(n9) < 0) {
                                            final long n11 = n10 + 1L;
                                            n3 = n8;
                                            r2 = n11;
                                            if (d12.w(n10) < 0) {
                                                final long n12 = n11 + 1L;
                                                n3 = n8;
                                                r2 = n12;
                                                if (d12.w(n11) < 0) {
                                                    r2 = n12 + 1L;
                                                    n3 = n8;
                                                    if (d12.w(n12) < 0) {
                                                        return (int)this.Q();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    this.r = r2;
                    return n3;
                }
            }
            return (int)this.Q();
        }
        
        public long P() {
            final long r = this.r;
            if (this.u != r) {
                final long n = r + 1L;
                final byte w = d12.w(r);
                if (w >= 0) {
                    ++this.r;
                    return w;
                }
                if (this.u - this.r >= 10L) {
                    final long n2 = n + 1L;
                    final int n3 = w ^ d12.w(n) << 7;
                    long r2 = 0L;
                    long n5 = 0L;
                    Label_0388: {
                        int n8 = 0;
                        Label_0091: {
                            if (n3 >= 0) {
                                r2 = n2 + 1L;
                                final int n4 = n3 ^ d12.w(n2) << 14;
                                Label_0123: {
                                    if (n4 >= 0) {
                                        n5 = (n4 ^ 0x3F80);
                                    }
                                    else {
                                        final long n6 = r2 + 1L;
                                        final int n7 = n4 ^ d12.w(r2) << 21;
                                        if (n7 < 0) {
                                            n8 = (n7 ^ 0xFFE03F80);
                                            r2 = n6;
                                            break Label_0091;
                                        }
                                        final long n9 = n7;
                                        r2 = n6 + 1L;
                                        long n10 = n9 ^ (long)d12.w(n6) << 28;
                                        long n18 = 0L;
                                        Label_0189: {
                                            if (n10 < 0L) {
                                                final long n11 = r2 + 1L;
                                                final long n12 = n10 ^ (long)d12.w(r2) << 35;
                                                long n14;
                                                long n15;
                                                long n16;
                                                if (n12 < 0L) {
                                                    final long n13 = -34093383808L;
                                                    n14 = n11;
                                                    n15 = n13;
                                                    n16 = n12;
                                                }
                                                else {
                                                    r2 = n11 + 1L;
                                                    final long n17 = n12 ^ (long)d12.w(n11) << 42;
                                                    if (n17 >= 0L) {
                                                        n18 = 4363953127296L;
                                                        n10 = n17;
                                                        break Label_0189;
                                                    }
                                                    n14 = r2 + 1L;
                                                    n16 = (n17 ^ (long)d12.w(r2) << 49);
                                                    if (n16 < 0L) {
                                                        n15 = -558586000294016L;
                                                    }
                                                    else {
                                                        final long n19 = n14 + 1L;
                                                        final long n20 = n5 = (n16 ^ (long)d12.w(n14) << 56 ^ 0xFE03F80FE03F80L);
                                                        r2 = n19;
                                                        if (n20 >= 0L) {
                                                            break Label_0123;
                                                        }
                                                        if (d12.w(n19) < 0L) {
                                                            return this.Q();
                                                        }
                                                        r2 = 1L + n19;
                                                        n5 = n20;
                                                        break Label_0388;
                                                    }
                                                }
                                                final long n21 = n16 ^ n15;
                                                r2 = n14;
                                                n5 = n21;
                                                break Label_0388;
                                            }
                                            n18 = 266354560L;
                                        }
                                        n5 = (n10 ^ n18);
                                    }
                                }
                                break Label_0388;
                            }
                            n8 = (n3 ^ 0xFFFFFF80);
                            r2 = n2;
                        }
                        n5 = n8;
                    }
                    this.r = r2;
                    return n5;
                }
            }
            return this.Q();
        }
        
        public long Q() {
            long n = 0L;
            for (int i = 0; i < 64; i += 7) {
                final byte k = this.K();
                n |= (long)(k & 0x7F) << i;
                if ((k & 0x80) == 0x0) {
                    return n;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }
        
        public final void R() {
            final int l = this.l + this.m;
            this.l = l;
            final int n = l - this.q;
            final int n2 = this.n;
            if (n > n2) {
                final int m = n - n2;
                this.m = m;
                this.l = l - m;
            }
            else {
                this.m = 0;
            }
        }
        
        public final int S() {
            return (int)(this.l - this.p - this.r + this.s);
        }
        
        public void T() {
            int d;
            do {
                d = this.D();
            } while (d != 0 && this.H(d));
        }
        
        public void U(int i) {
            if (i >= 0 && i <= this.l - this.p - this.r + this.s) {
                while (i > 0) {
                    if (this.I() == 0L) {
                        this.J();
                    }
                    final int min = Math.min(i, (int)this.I());
                    i -= min;
                    this.r += min;
                }
                return;
            }
            if (i < 0) {
                throw InvalidProtocolBufferException.negativeSize();
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }
        
        public final void V() {
            for (int i = 0; i < 10; ++i) {
                if (this.K() >= 0) {
                    return;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }
        
        public final ByteBuffer W(final int newPosition, final int newLimit) {
            final int position = this.i.position();
            final int limit = this.i.limit();
            final ByteBuffer i = this.i;
            try {
                try {
                    i.position(newPosition);
                    i.limit(newLimit);
                    final ByteBuffer slice = this.i.slice();
                    i.position(position);
                    i.limit(limit);
                    return slice;
                }
                finally {}
            }
            catch (final IllegalArgumentException ex) {
                throw InvalidProtocolBufferException.truncatedMessage();
            }
            i.position(position);
            i.limit(limit);
        }
        
        public final void X() {
            final ByteBuffer i = this.h.next();
            this.i = i;
            this.p += (int)(this.r - this.s);
            final long n = i.position();
            this.r = n;
            this.s = n;
            this.u = this.i.limit();
            final long k = d12.k(this.i);
            this.t = k;
            this.r += k;
            this.s += k;
            this.u += k;
        }
        
        @Override
        public void a(final int n) {
            if (this.o == n) {
                return;
            }
            throw InvalidProtocolBufferException.invalidEndTag();
        }
        
        @Override
        public int d() {
            return (int)(this.p - this.q + this.r - this.s);
        }
        
        @Override
        public boolean e() {
            return this.p + this.r - this.s == this.l;
        }
        
        @Override
        public void m(final int n) {
            this.n = n;
            this.R();
        }
        
        @Override
        public int n(int n) {
            if (n < 0) {
                throw InvalidProtocolBufferException.negativeSize();
            }
            n += this.d();
            final int n2 = this.n;
            if (n <= n2) {
                this.n = n;
                this.R();
                return n2;
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }
        
        @Override
        public boolean o() {
            return this.P() != 0L;
        }
        
        @Override
        public ByteString p() {
            int i = this.O();
            if (i > 0) {
                final long n = i;
                final long u = this.u;
                final long r = this.r;
                if (n <= u - r) {
                    if (this.j && this.k) {
                        final int n2 = (int)(r - this.t);
                        final ByteString wrap = ByteString.wrap(this.W(n2, i + n2));
                        this.r += n;
                        return wrap;
                    }
                    final byte[] array = new byte[i];
                    d12.p(r, array, 0L, n);
                    this.r += n;
                    return ByteString.wrap(array);
                }
            }
            if (i > 0 && i <= this.S()) {
                if (this.j && this.k) {
                    final ArrayList<ByteString> list = new ArrayList<ByteString>();
                    while (i > 0) {
                        if (this.I() == 0L) {
                            this.J();
                        }
                        final int min = Math.min(i, (int)this.I());
                        final int n3 = (int)(this.r - this.t);
                        list.add(ByteString.wrap(this.W(n3, n3 + min)));
                        i -= min;
                        this.r += min;
                    }
                    return ByteString.copyFrom(list);
                }
                final byte[] array2 = new byte[i];
                this.L(array2, 0, i);
                return ByteString.wrap(array2);
            }
            else {
                if (i == 0) {
                    return ByteString.EMPTY;
                }
                if (i < 0) {
                    throw InvalidProtocolBufferException.negativeSize();
                }
                throw InvalidProtocolBufferException.truncatedMessage();
            }
        }
        
        @Override
        public double q() {
            return Double.longBitsToDouble(this.N());
        }
        
        @Override
        public int r() {
            return this.O();
        }
        
        @Override
        public int s() {
            return this.M();
        }
        
        @Override
        public long t() {
            return this.N();
        }
        
        @Override
        public float u() {
            return Float.intBitsToFloat(this.M());
        }
        
        @Override
        public int v() {
            return this.O();
        }
        
        @Override
        public long w() {
            return this.P();
        }
        
        @Override
        public int x() {
            return this.M();
        }
        
        @Override
        public long y() {
            return this.N();
        }
        
        @Override
        public int z() {
            return com.google.protobuf.g.b(this.O());
        }
    }
    
    public static final class d extends g
    {
        public final InputStream g;
        public final byte[] h;
        public int i;
        public int j;
        public int k;
        public int l;
        public int m;
        public int n;
        
        public d(final InputStream g, final int n) {
            super(null);
            this.n = Integer.MAX_VALUE;
            t.b(g, "input");
            this.g = g;
            this.h = new byte[n];
            this.i = 0;
            this.k = 0;
            this.m = 0;
        }
        
        public static int I(final InputStream inputStream) {
            try {
                return inputStream.available();
            }
            catch (final InvalidProtocolBufferException ex) {
                ex.setThrownFromInputStream();
                throw ex;
            }
        }
        
        public static int J(final InputStream inputStream, final byte[] b, int read, final int len) {
            try {
                read = inputStream.read(b, read, len);
                return read;
            }
            catch (final InvalidProtocolBufferException ex) {
                ex.setThrownFromInputStream();
                throw ex;
            }
        }
        
        public static long W(final InputStream inputStream, long skip) {
            try {
                skip = inputStream.skip(skip);
                return skip;
            }
            catch (final InvalidProtocolBufferException ex) {
                ex.setThrownFromInputStream();
                throw ex;
            }
        }
        
        @Override
        public long A() {
            return com.google.protobuf.g.c(this.S());
        }
        
        @Override
        public String B() {
            final int r = this.R();
            if (r > 0) {
                final int i = this.i;
                final int k = this.k;
                if (r <= i - k) {
                    final String s = new String(this.h, k, r, t.b);
                    this.k += r;
                    return s;
                }
            }
            if (r == 0) {
                return "";
            }
            if (r <= this.i) {
                this.V(r);
                final String s2 = new String(this.h, this.k, r, t.b);
                this.k += r;
                return s2;
            }
            return new String(this.M(r, false), t.b);
        }
        
        @Override
        public String C() {
            final int r = this.R();
            int k = this.k;
            final int i = this.i;
            byte[] array;
            int j;
            if (r <= i - k && r > 0) {
                array = this.h;
                j = k + r;
            }
            else {
                if (r == 0) {
                    return "";
                }
                final int n = 0;
                k = 0;
                if (r > i) {
                    array = this.M(r, false);
                    k = n;
                    return Utf8.h(array, k, r);
                }
                this.V(r);
                array = this.h;
                j = r + 0;
            }
            this.k = j;
            return Utf8.h(array, k, r);
        }
        
        @Override
        public int D() {
            if (this.e()) {
                return this.l = 0;
            }
            final int r = this.R();
            this.l = r;
            if (WireFormat.a(r) != 0) {
                return this.l;
            }
            throw InvalidProtocolBufferException.invalidTag();
        }
        
        @Override
        public int E() {
            return this.R();
        }
        
        @Override
        public long F() {
            return this.S();
        }
        
        @Override
        public boolean H(final int n) {
            final int b = WireFormat.b(n);
            if (b == 0) {
                this.a0();
                return true;
            }
            if (b == 1) {
                this.Y(8);
                return true;
            }
            if (b == 2) {
                this.Y(this.R());
                return true;
            }
            if (b == 3) {
                this.X();
                this.a(WireFormat.c(WireFormat.a(n), 4));
                return true;
            }
            if (b == 4) {
                return false;
            }
            if (b == 5) {
                this.Y(4);
                return true;
            }
            throw InvalidProtocolBufferException.invalidWireType();
        }
        
        public final ByteString K(int n) {
            final byte[] n2 = this.N(n);
            if (n2 != null) {
                return ByteString.copyFrom(n2);
            }
            final int k = this.k;
            final int i = this.i;
            final int n3 = i - k;
            this.m += i;
            this.k = 0;
            this.i = 0;
            final List o = this.O(n - n3);
            final byte[] array = new byte[n];
            System.arraycopy(this.h, k, array, 0, n3);
            final Iterator iterator = o.iterator();
            n = n3;
            while (iterator.hasNext()) {
                final byte[] array2 = (byte[])iterator.next();
                System.arraycopy(array2, 0, array, n, array2.length);
                n += array2.length;
            }
            return ByteString.wrap(array);
        }
        
        public byte L() {
            if (this.k == this.i) {
                this.V(1);
            }
            return this.h[this.k++];
        }
        
        public final byte[] M(int n, final boolean b) {
            final byte[] n2 = this.N(n);
            if (n2 != null) {
                byte[] array = n2;
                if (b) {
                    array = n2.clone();
                }
                return array;
            }
            final int k = this.k;
            final int i = this.i;
            final int n3 = i - k;
            this.m += i;
            this.k = 0;
            this.i = 0;
            final List o = this.O(n - n3);
            final byte[] array2 = new byte[n];
            System.arraycopy(this.h, k, array2, 0, n3);
            final Iterator iterator = o.iterator();
            n = n3;
            while (iterator.hasNext()) {
                final byte[] array3 = (byte[])iterator.next();
                System.arraycopy(array3, 0, array2, n, array3.length);
                n += array3.length;
            }
            return array2;
        }
        
        public final byte[] N(final int n) {
            if (n == 0) {
                return t.d;
            }
            if (n < 0) {
                throw InvalidProtocolBufferException.negativeSize();
            }
            final int m = this.m;
            final int k = this.k;
            final int n2 = m + k + n;
            if (n2 - super.c > 0) {
                throw InvalidProtocolBufferException.sizeLimitExceeded();
            }
            final int n3 = this.n;
            if (n2 > n3) {
                this.Y(n3 - m - k);
                throw InvalidProtocolBufferException.truncatedMessage();
            }
            int i = this.i - k;
            final int n4 = n - i;
            if (n4 >= 4096 && n4 > I(this.g)) {
                return null;
            }
            final byte[] array = new byte[n];
            System.arraycopy(this.h, this.k, array, 0, i);
            this.m += this.i;
            this.k = 0;
            this.i = 0;
            while (i < n) {
                final int j = J(this.g, array, i, n - i);
                if (j == -1) {
                    throw InvalidProtocolBufferException.truncatedMessage();
                }
                this.m += j;
                i += j;
            }
            return array;
        }
        
        public final List O(int i) {
            final ArrayList list = new ArrayList();
            while (i > 0) {
                final int min = Math.min(i, 4096);
                final byte[] b = new byte[min];
                int read;
                for (int j = 0; j < min; j += read) {
                    read = this.g.read(b, j, min - j);
                    if (read == -1) {
                        throw InvalidProtocolBufferException.truncatedMessage();
                    }
                    this.m += read;
                }
                i -= min;
                list.add(b);
            }
            return list;
        }
        
        public int P() {
            int n;
            if (this.i - (n = this.k) < 4) {
                this.V(4);
                n = this.k;
            }
            final byte[] h = this.h;
            this.k = n + 4;
            return (h[n + 3] & 0xFF) << 24 | ((h[n] & 0xFF) | (h[n + 1] & 0xFF) << 8 | (h[n + 2] & 0xFF) << 16);
        }
        
        public long Q() {
            int n;
            if (this.i - (n = this.k) < 8) {
                this.V(8);
                n = this.k;
            }
            final byte[] h = this.h;
            this.k = n + 8;
            return ((long)h[n + 7] & 0xFFL) << 56 | (((long)h[n] & 0xFFL) | ((long)h[n + 1] & 0xFFL) << 8 | ((long)h[n + 2] & 0xFFL) << 16 | ((long)h[n + 3] & 0xFFL) << 24 | ((long)h[n + 4] & 0xFFL) << 32 | ((long)h[n + 5] & 0xFFL) << 40 | ((long)h[n + 6] & 0xFFL) << 48);
        }
        
        public int R() {
            final int k = this.k;
            final int i = this.i;
            if (i != k) {
                final byte[] h = this.h;
                final int j = k + 1;
                final byte b = h[k];
                if (b >= 0) {
                    this.k = j;
                    return b;
                }
                if (i - j >= 9) {
                    int l = j + 1;
                    final int n = b ^ h[j] << 7;
                    int n2;
                    if (n < 0) {
                        n2 = (n ^ 0xFFFFFF80);
                    }
                    else {
                        final int n3 = l + 1;
                        final int n4 = n ^ h[l] << 14;
                        if (n4 >= 0) {
                            final int n5 = n4 ^ 0x3F80;
                            l = n3;
                            n2 = n5;
                        }
                        else {
                            l = n3 + 1;
                            final int n6 = n4 ^ h[n3] << 21;
                            if (n6 < 0) {
                                n2 = (n6 ^ 0xFFE03F80);
                            }
                            else {
                                final int n7 = l + 1;
                                final byte b2 = h[l];
                                final int n8 = n2 = (n6 ^ b2 << 28 ^ 0xFE03F80);
                                l = n7;
                                if (b2 < 0) {
                                    final int n9 = n7 + 1;
                                    n2 = n8;
                                    l = n9;
                                    if (h[n7] < 0) {
                                        final int n10 = n9 + 1;
                                        n2 = n8;
                                        l = n10;
                                        if (h[n9] < 0) {
                                            final int n11 = n10 + 1;
                                            n2 = n8;
                                            l = n11;
                                            if (h[n10] < 0) {
                                                final int n12 = n11 + 1;
                                                n2 = n8;
                                                l = n12;
                                                if (h[n11] < 0) {
                                                    l = n12 + 1;
                                                    n2 = n8;
                                                    if (h[n12] < 0) {
                                                        return (int)this.T();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    this.k = l;
                    return n2;
                }
            }
            return (int)this.T();
        }
        
        public long S() {
            final int k = this.k;
            final int i = this.i;
            if (i != k) {
                final byte[] h = this.h;
                final int j = k + 1;
                final byte b = h[k];
                if (b >= 0) {
                    this.k = j;
                    return b;
                }
                if (i - j >= 9) {
                    int l = j + 1;
                    final int n = b ^ h[j] << 7;
                    long n5 = 0L;
                    Label_0342: {
                        int n2;
                        if (n < 0) {
                            n2 = (n ^ 0xFFFFFF80);
                        }
                        else {
                            final int n3 = l + 1;
                            final int n4 = n ^ h[l] << 14;
                            if (n4 >= 0) {
                                n5 = (n4 ^ 0x3F80);
                                l = n3;
                                break Label_0342;
                            }
                            l = n3 + 1;
                            final int n6 = n4 ^ h[n3] << 21;
                            if (n6 >= 0) {
                                final long n7 = n6;
                                final int n8 = l + 1;
                                long n9 = n7 ^ (long)h[l] << 28;
                                long n13 = 0L;
                                Label_0178: {
                                    if (n9 < 0L) {
                                        l = n8 + 1;
                                        long n10 = n9 ^ (long)h[n8] << 35;
                                        long n11;
                                        if (n10 < 0L) {
                                            n11 = -34093383808L;
                                        }
                                        else {
                                            final int n12 = l + 1;
                                            n9 = (n10 ^ (long)h[l] << 42);
                                            if (n9 >= 0L) {
                                                n13 = 4363953127296L;
                                                l = n12;
                                                break Label_0178;
                                            }
                                            l = n12 + 1;
                                            n10 = (n9 ^ (long)h[n12] << 49);
                                            if (n10 < 0L) {
                                                n11 = -558586000294016L;
                                            }
                                            else {
                                                final int n14 = l + 1;
                                                n5 = (n10 ^ (long)h[l] << 56 ^ 0xFE03F80FE03F80L);
                                                if (n5 >= 0L) {
                                                    l = n14;
                                                    break Label_0342;
                                                }
                                                l = n14 + 1;
                                                if (h[n14] < 0L) {
                                                    return this.T();
                                                }
                                                break Label_0342;
                                            }
                                        }
                                        n5 = (n10 ^ n11);
                                        break Label_0342;
                                    }
                                    n13 = 266354560L;
                                    l = n8;
                                }
                                n5 = (n9 ^ n13);
                                break Label_0342;
                            }
                            n2 = (n6 ^ 0xFFE03F80);
                        }
                        n5 = n2;
                    }
                    this.k = l;
                    return n5;
                }
            }
            return this.T();
        }
        
        public long T() {
            long n = 0L;
            for (int i = 0; i < 64; i += 7) {
                final byte l = this.L();
                n |= (long)(l & 0x7F) << i;
                if ((l & 0x80) == 0x0) {
                    return n;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }
        
        public final void U() {
            final int i = this.i + this.j;
            this.i = i;
            final int n = this.m + i;
            final int n2 = this.n;
            if (n > n2) {
                final int j = n - n2;
                this.j = j;
                this.i = i - j;
            }
            else {
                this.j = 0;
            }
        }
        
        public final void V(final int n) {
            if (this.d0(n)) {
                return;
            }
            if (n > super.c - this.m - this.k) {
                throw InvalidProtocolBufferException.sizeLimitExceeded();
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }
        
        public void X() {
            int d;
            do {
                d = this.D();
            } while (d != 0 && this.H(d));
        }
        
        public void Y(final int n) {
            final int i = this.i;
            final int k = this.k;
            if (n <= i - k && n >= 0) {
                this.k = k + n;
            }
            else {
                this.Z(n);
            }
        }
        
        public final void Z(final int n) {
            if (n < 0) {
                throw InvalidProtocolBufferException.negativeSize();
            }
            final int m = this.m;
            final int k = this.k;
            final int n2 = this.n;
            if (m + k + n <= n2) {
                this.m = m + k;
                int i = this.i - k;
                this.i = 0;
                this.k = 0;
                while (i < n) {
                    try {
                        final InputStream g = this.g;
                        final long n3 = n - i;
                        final long w = W(g, n3);
                        final long n4 = lcmp(w, 0L);
                        if (n4 < 0 || w > n3) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append(this.g.getClass());
                            sb.append("#skip returned invalid result: ");
                            sb.append(w);
                            sb.append("\nThe InputStream implementation is buggy.");
                            throw new IllegalStateException(sb.toString());
                        }
                        if (n4 != 0) {
                            i += (int)w;
                            continue;
                        }
                    }
                    finally {
                        this.m += i;
                        this.U();
                    }
                    break;
                }
                this.m += i;
                this.U();
                if (i < n) {
                    final int j = this.i;
                    int n5 = j - this.k;
                    this.k = j;
                    int l;
                    while (true) {
                        this.V(1);
                        l = n - n5;
                        final int i2 = this.i;
                        if (l <= i2) {
                            break;
                        }
                        n5 += i2;
                        this.k = i2;
                    }
                    this.k = l;
                }
                return;
            }
            this.Y(n2 - m - k);
            throw InvalidProtocolBufferException.truncatedMessage();
        }
        
        @Override
        public void a(final int n) {
            if (this.l == n) {
                return;
            }
            throw InvalidProtocolBufferException.invalidEndTag();
        }
        
        public final void a0() {
            if (this.i - this.k >= 10) {
                this.b0();
            }
            else {
                this.c0();
            }
        }
        
        public final void b0() {
            for (int i = 0; i < 10; ++i) {
                if (this.h[this.k++] >= 0) {
                    return;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }
        
        public final void c0() {
            for (int i = 0; i < 10; ++i) {
                if (this.L() >= 0) {
                    return;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }
        
        @Override
        public int d() {
            return this.m + this.k;
        }
        
        public final boolean d0(final int i) {
            final int k = this.k;
            final int j = this.i;
            if (k + i <= j) {
                final StringBuilder sb = new StringBuilder();
                sb.append("refillBuffer() called when ");
                sb.append(i);
                sb.append(" bytes were already available in buffer");
                throw new IllegalStateException(sb.toString());
            }
            final int c = super.c;
            final int m = this.m;
            if (i > c - m - k) {
                return false;
            }
            if (m + k + i > this.n) {
                return false;
            }
            if (k > 0) {
                if (j > k) {
                    final byte[] h = this.h;
                    System.arraycopy(h, k, h, 0, j - k);
                }
                this.m += k;
                this.i -= k;
                this.k = 0;
            }
            final InputStream g = this.g;
            final byte[] h2 = this.h;
            final int l = this.i;
            final int j2 = J(g, h2, l, Math.min(h2.length - l, super.c - this.m - l));
            if (j2 == 0 || j2 < -1 || j2 > this.h.length) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(this.g.getClass());
                sb2.append("#read(byte[]) returned invalid result: ");
                sb2.append(j2);
                sb2.append("\nThe InputStream implementation is buggy.");
                throw new IllegalStateException(sb2.toString());
            }
            if (j2 > 0) {
                this.i += j2;
                this.U();
                return this.i >= i || this.d0(i);
            }
            return false;
        }
        
        @Override
        public boolean e() {
            if (this.k == this.i) {
                final boolean b = true;
                if (!this.d0(1)) {
                    return b;
                }
            }
            return false;
        }
        
        @Override
        public void m(final int n) {
            this.n = n;
            this.U();
        }
        
        @Override
        public int n(int n) {
            if (n < 0) {
                throw InvalidProtocolBufferException.negativeSize();
            }
            n += this.m + this.k;
            final int n2 = this.n;
            if (n <= n2) {
                this.n = n;
                this.U();
                return n2;
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }
        
        @Override
        public boolean o() {
            return this.S() != 0L;
        }
        
        @Override
        public ByteString p() {
            final int r = this.R();
            final int i = this.i;
            final int k = this.k;
            if (r <= i - k && r > 0) {
                final ByteString copy = ByteString.copyFrom(this.h, k, r);
                this.k += r;
                return copy;
            }
            if (r == 0) {
                return ByteString.EMPTY;
            }
            return this.K(r);
        }
        
        @Override
        public double q() {
            return Double.longBitsToDouble(this.Q());
        }
        
        @Override
        public int r() {
            return this.R();
        }
        
        @Override
        public int s() {
            return this.P();
        }
        
        @Override
        public long t() {
            return this.Q();
        }
        
        @Override
        public float u() {
            return Float.intBitsToFloat(this.P());
        }
        
        @Override
        public int v() {
            return this.R();
        }
        
        @Override
        public long w() {
            return this.S();
        }
        
        @Override
        public int x() {
            return this.P();
        }
        
        @Override
        public long y() {
            return this.Q();
        }
        
        @Override
        public int z() {
            return com.google.protobuf.g.b(this.R());
        }
    }
    
    public static final class e extends g
    {
        public final ByteBuffer g;
        public final boolean h;
        public final long i;
        public long j;
        public long k;
        public long l;
        public int m;
        public int n;
        public boolean o;
        public int p;
        
        public e(final ByteBuffer g, final boolean h) {
            super(null);
            this.p = Integer.MAX_VALUE;
            this.g = g;
            final long k = d12.k(g);
            this.i = k;
            this.j = g.limit() + k;
            final long n = k + g.position();
            this.k = n;
            this.l = n;
            this.h = h;
        }
        
        public static boolean J() {
            return d12.J();
        }
        
        @Override
        public long A() {
            return com.google.protobuf.g.c(this.O());
        }
        
        @Override
        public String B() {
            final int n = this.N();
            if (n > 0 && n <= this.R()) {
                final byte[] bytes = new byte[n];
                final long k = this.k;
                final long n2 = n;
                d12.p(k, bytes, 0L, n2);
                final String s = new String(bytes, t.b);
                this.k += n2;
                return s;
            }
            if (n == 0) {
                return "";
            }
            if (n < 0) {
                throw InvalidProtocolBufferException.negativeSize();
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }
        
        @Override
        public String C() {
            final int n = this.N();
            if (n > 0 && n <= this.R()) {
                final String g = Utf8.g(this.g, this.I(this.k), n);
                this.k += n;
                return g;
            }
            if (n == 0) {
                return "";
            }
            if (n <= 0) {
                throw InvalidProtocolBufferException.negativeSize();
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }
        
        @Override
        public int D() {
            if (this.e()) {
                return this.n = 0;
            }
            final int n = this.N();
            this.n = n;
            if (WireFormat.a(n) != 0) {
                return this.n;
            }
            throw InvalidProtocolBufferException.invalidTag();
        }
        
        @Override
        public int E() {
            return this.N();
        }
        
        @Override
        public long F() {
            return this.O();
        }
        
        @Override
        public boolean H(final int n) {
            final int b = WireFormat.b(n);
            if (b == 0) {
                this.U();
                return true;
            }
            if (b == 1) {
                this.T(8);
                return true;
            }
            if (b == 2) {
                this.T(this.N());
                return true;
            }
            if (b == 3) {
                this.S();
                this.a(WireFormat.c(WireFormat.a(n), 4));
                return true;
            }
            if (b == 4) {
                return false;
            }
            if (b == 5) {
                this.T(4);
                return true;
            }
            throw InvalidProtocolBufferException.invalidWireType();
        }
        
        public final int I(final long n) {
            return (int)(n - this.i);
        }
        
        public byte K() {
            final long k = this.k;
            if (k != this.j) {
                this.k = 1L + k;
                return d12.w(k);
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }
        
        public int L() {
            final long k = this.k;
            if (this.j - k >= 4L) {
                this.k = 4L + k;
                return (d12.w(k + 3L) & 0xFF) << 24 | ((d12.w(k) & 0xFF) | (d12.w(1L + k) & 0xFF) << 8 | (d12.w(2L + k) & 0xFF) << 16);
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }
        
        public long M() {
            final long k = this.k;
            if (this.j - k >= 8L) {
                this.k = 8L + k;
                return ((long)d12.w(k + 7L) & 0xFFL) << 56 | (((long)d12.w(k) & 0xFFL) | ((long)d12.w(1L + k) & 0xFFL) << 8 | ((long)d12.w(2L + k) & 0xFFL) << 16 | ((long)d12.w(3L + k) & 0xFFL) << 24 | ((long)d12.w(4L + k) & 0xFFL) << 32 | ((long)d12.w(5L + k) & 0xFFL) << 40 | ((long)d12.w(6L + k) & 0xFFL) << 48);
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }
        
        public int N() {
            final long k = this.k;
            if (this.j != k) {
                final long i = k + 1L;
                final byte w = d12.w(k);
                if (w >= 0) {
                    this.k = i;
                    return w;
                }
                if (this.j - i >= 9L) {
                    long j = i + 1L;
                    final int n = w ^ d12.w(i) << 7;
                    int n2;
                    if (n < 0) {
                        n2 = (n ^ 0xFFFFFF80);
                    }
                    else {
                        final long n3 = j + 1L;
                        final int n4 = n ^ d12.w(j) << 14;
                        if (n4 >= 0) {
                            n2 = (n4 ^ 0x3F80);
                            j = n3;
                        }
                        else {
                            j = n3 + 1L;
                            final int n5 = n4 ^ d12.w(n3) << 21;
                            if (n5 < 0) {
                                n2 = (n5 ^ 0xFFE03F80);
                            }
                            else {
                                final long n6 = j + 1L;
                                final byte w2 = d12.w(j);
                                final int n7 = n2 = (n5 ^ w2 << 28 ^ 0xFE03F80);
                                j = n6;
                                if (w2 < 0) {
                                    final long n8 = n6 + 1L;
                                    n2 = n7;
                                    j = n8;
                                    if (d12.w(n6) < 0) {
                                        final long n9 = n8 + 1L;
                                        n2 = n7;
                                        j = n9;
                                        if (d12.w(n8) < 0) {
                                            final long n10 = n9 + 1L;
                                            n2 = n7;
                                            j = n10;
                                            if (d12.w(n9) < 0) {
                                                final long n11 = n10 + 1L;
                                                n2 = n7;
                                                j = n11;
                                                if (d12.w(n10) < 0) {
                                                    j = n11 + 1L;
                                                    n2 = n7;
                                                    if (d12.w(n11) < 0) {
                                                        return (int)this.P();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    this.k = j;
                    return n2;
                }
            }
            return (int)this.P();
        }
        
        public long O() {
            final long k = this.k;
            if (this.j != k) {
                final long i = k + 1L;
                final byte w = d12.w(k);
                if (w >= 0) {
                    this.k = i;
                    return w;
                }
                if (this.j - i >= 9L) {
                    final long n = i + 1L;
                    final int n2 = w ^ d12.w(i) << 7;
                    long j = 0L;
                    long n4 = 0L;
                    Label_0370: {
                        int n7 = 0;
                        Label_0085: {
                            if (n2 >= 0) {
                                j = n + 1L;
                                final int n3 = n2 ^ d12.w(n) << 14;
                                Label_0117: {
                                    if (n3 >= 0) {
                                        n4 = (n3 ^ 0x3F80);
                                    }
                                    else {
                                        final long n5 = j + 1L;
                                        final int n6 = n3 ^ d12.w(j) << 21;
                                        if (n6 < 0) {
                                            n7 = (n6 ^ 0xFFE03F80);
                                            j = n5;
                                            break Label_0085;
                                        }
                                        final long n8 = n6;
                                        j = n5 + 1L;
                                        long n9 = n8 ^ (long)d12.w(n5) << 28;
                                        long n14 = 0L;
                                        Label_0184: {
                                            if (n9 < 0L) {
                                                long n10 = j + 1L;
                                                final long n11 = n9 ^ (long)d12.w(j) << 35;
                                                long n12;
                                                long n13;
                                                if (n11 < 0L) {
                                                    n12 = -34093383808L;
                                                    n13 = n11;
                                                }
                                                else {
                                                    j = n10 + 1L;
                                                    n9 = (n11 ^ (long)d12.w(n10) << 42);
                                                    if (n9 >= 0L) {
                                                        n14 = 4363953127296L;
                                                        break Label_0184;
                                                    }
                                                    n10 = j + 1L;
                                                    n13 = (n9 ^ (long)d12.w(j) << 49);
                                                    if (n13 < 0L) {
                                                        n12 = -558586000294016L;
                                                    }
                                                    else {
                                                        final long n15 = n10 + 1L;
                                                        final long n16 = n4 = (n13 ^ (long)d12.w(n10) << 56 ^ 0xFE03F80FE03F80L);
                                                        j = n15;
                                                        if (n16 >= 0L) {
                                                            break Label_0117;
                                                        }
                                                        if (d12.w(n15) < 0L) {
                                                            return this.P();
                                                        }
                                                        j = 1L + n15;
                                                        n4 = n16;
                                                        break Label_0370;
                                                    }
                                                }
                                                final long n17 = n13 ^ n12;
                                                j = n10;
                                                n4 = n17;
                                                break Label_0370;
                                            }
                                            n14 = 266354560L;
                                        }
                                        n4 = (n9 ^ n14);
                                    }
                                }
                                break Label_0370;
                            }
                            n7 = (n2 ^ 0xFFFFFF80);
                            j = n;
                        }
                        n4 = n7;
                    }
                    this.k = j;
                    return n4;
                }
            }
            return this.P();
        }
        
        public long P() {
            long n = 0L;
            for (int i = 0; i < 64; i += 7) {
                final byte k = this.K();
                n |= (long)(k & 0x7F) << i;
                if ((k & 0x80) == 0x0) {
                    return n;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }
        
        public final void Q() {
            final long j = this.j + this.m;
            this.j = j;
            final int n = (int)(j - this.l);
            final int p = this.p;
            if (n > p) {
                final int m = n - p;
                this.m = m;
                this.j = j - m;
            }
            else {
                this.m = 0;
            }
        }
        
        public final int R() {
            return (int)(this.j - this.k);
        }
        
        public void S() {
            int d;
            do {
                d = this.D();
            } while (d != 0 && this.H(d));
        }
        
        public void T(final int n) {
            if (n >= 0 && n <= this.R()) {
                this.k += n;
                return;
            }
            if (n < 0) {
                throw InvalidProtocolBufferException.negativeSize();
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }
        
        public final void U() {
            if (this.R() >= 10) {
                this.V();
            }
            else {
                this.W();
            }
        }
        
        public final void V() {
            for (int i = 0; i < 10; ++i) {
                final long k = this.k;
                this.k = 1L + k;
                if (d12.w(k) >= 0) {
                    return;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }
        
        public final void W() {
            for (int i = 0; i < 10; ++i) {
                if (this.K() >= 0) {
                    return;
                }
            }
            throw InvalidProtocolBufferException.malformedVarint();
        }
        
        public final ByteBuffer X(final long n, final long n2) {
            final int position = this.g.position();
            final int limit = this.g.limit();
            final ByteBuffer g = this.g;
            try {
                try {
                    g.position(this.I(n));
                    g.limit(this.I(n2));
                    final ByteBuffer slice = this.g.slice();
                    g.position(position);
                    g.limit(limit);
                    return slice;
                }
                finally {}
            }
            catch (final IllegalArgumentException cause) {
                final InvalidProtocolBufferException truncatedMessage = InvalidProtocolBufferException.truncatedMessage();
                truncatedMessage.initCause(cause);
                throw truncatedMessage;
            }
            g.position(position);
            g.limit(limit);
        }
        
        @Override
        public void a(final int n) {
            if (this.n == n) {
                return;
            }
            throw InvalidProtocolBufferException.invalidEndTag();
        }
        
        @Override
        public int d() {
            return (int)(this.k - this.l);
        }
        
        @Override
        public boolean e() {
            return this.k == this.j;
        }
        
        @Override
        public void m(final int p) {
            this.p = p;
            this.Q();
        }
        
        @Override
        public int n(int p) {
            if (p < 0) {
                throw InvalidProtocolBufferException.negativeSize();
            }
            final int p2 = p + this.d();
            p = this.p;
            if (p2 <= p) {
                this.p = p2;
                this.Q();
                return p;
            }
            throw InvalidProtocolBufferException.truncatedMessage();
        }
        
        @Override
        public boolean o() {
            return this.O() != 0L;
        }
        
        @Override
        public ByteString p() {
            final int n = this.N();
            if (n > 0 && n <= this.R()) {
                if (this.h && this.o) {
                    final long k = this.k;
                    final long n2 = n;
                    final ByteBuffer x = this.X(k, k + n2);
                    this.k += n2;
                    return ByteString.wrap(x);
                }
                final byte[] array = new byte[n];
                final long i = this.k;
                final long n3 = n;
                d12.p(i, array, 0L, n3);
                this.k += n3;
                return ByteString.wrap(array);
            }
            else {
                if (n == 0) {
                    return ByteString.EMPTY;
                }
                if (n < 0) {
                    throw InvalidProtocolBufferException.negativeSize();
                }
                throw InvalidProtocolBufferException.truncatedMessage();
            }
        }
        
        @Override
        public double q() {
            return Double.longBitsToDouble(this.M());
        }
        
        @Override
        public int r() {
            return this.N();
        }
        
        @Override
        public int s() {
            return this.L();
        }
        
        @Override
        public long t() {
            return this.M();
        }
        
        @Override
        public float u() {
            return Float.intBitsToFloat(this.L());
        }
        
        @Override
        public int v() {
            return this.N();
        }
        
        @Override
        public long w() {
            return this.O();
        }
        
        @Override
        public int x() {
            return this.L();
        }
        
        @Override
        public long y() {
            return this.M();
        }
        
        @Override
        public int z() {
            return com.google.protobuf.g.b(this.N());
        }
    }
}

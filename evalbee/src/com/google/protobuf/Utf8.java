// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.nio.ByteBuffer;

public abstract class Utf8
{
    public static final b a;
    
    static {
        b a2;
        if (d.m() && !p4.c()) {
            a2 = new d();
        }
        else {
            a2 = new c();
        }
        a = a2;
    }
    
    public static String g(final ByteBuffer byteBuffer, final int n, final int n2) {
        return Utf8.a.a(byteBuffer, n, n2);
    }
    
    public static String h(final byte[] array, final int n, final int n2) {
        return Utf8.a.b(array, n, n2);
    }
    
    public static int i(final CharSequence charSequence, final byte[] array, final int n, final int n2) {
        return Utf8.a.e(charSequence, array, n, n2);
    }
    
    public static int j(final CharSequence charSequence) {
        int length;
        int n;
        for (length = charSequence.length(), n = 0; n < length && charSequence.charAt(n) < '\u0080'; ++n) {}
        int n2 = length;
        int n3;
        while (true) {
            n3 = n2;
            if (n >= length) {
                break;
            }
            final char char1 = charSequence.charAt(n);
            if (char1 >= '\u0800') {
                n3 = n2 + k(charSequence, n);
                break;
            }
            n2 += '\u007f' - char1 >>> 31;
            ++n;
        }
        if (n3 >= length) {
            return n3;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("UTF-8 length does not fit in int: ");
        sb.append(n3 + 4294967296L);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static int k(final CharSequence seq, int i) {
        final int length = seq.length();
        int n = 0;
        while (i < length) {
            final char char1 = seq.charAt(i);
            int n2;
            if (char1 < '\u0800') {
                n += '\u007f' - char1 >>> 31;
                n2 = i;
            }
            else {
                final int n3 = n += 2;
                n2 = i;
                if ('\ud800' <= char1) {
                    n = n3;
                    n2 = i;
                    if (char1 <= '\udfff') {
                        if (Character.codePointAt(seq, i) < 65536) {
                            throw new UnpairedSurrogateException(i, length);
                        }
                        n2 = i + 1;
                        n = n3;
                    }
                }
            }
            i = n2 + 1;
        }
        return n;
    }
    
    public static int l(final ByteBuffer byteBuffer, final int n, final int n2) {
        int n3;
        for (n3 = n; n3 < n2 - 7 && (byteBuffer.getLong(n3) & 0x8080808080808080L) == 0x0L; n3 += 8) {}
        return n3 - n;
    }
    
    public static int m(final int n) {
        int n2 = n;
        if (n > -12) {
            n2 = -1;
        }
        return n2;
    }
    
    public static int n(int n, final int n2) {
        if (n <= -12 && n2 <= -65) {
            n ^= n2 << 8;
        }
        else {
            n = -1;
        }
        return n;
    }
    
    public static int o(int n, final int n2, final int n3) {
        if (n <= -12 && n2 <= -65 && n3 <= -65) {
            n = (n ^ n2 << 8 ^ n3 << 16);
        }
        else {
            n = -1;
        }
        return n;
    }
    
    public static int p(final ByteBuffer byteBuffer, final int n, final int n2, final int n3) {
        if (n3 == 0) {
            return m(n);
        }
        if (n3 == 1) {
            return n(n, byteBuffer.get(n2));
        }
        if (n3 == 2) {
            return o(n, byteBuffer.get(n2), byteBuffer.get(n2 + 1));
        }
        throw new AssertionError();
    }
    
    public static int q(final byte[] array, final int n, int n2) {
        final byte b = array[n - 1];
        n2 -= n;
        if (n2 == 0) {
            return m(b);
        }
        if (n2 == 1) {
            return n(b, array[n]);
        }
        if (n2 == 2) {
            return o(b, array[n], array[n + 1]);
        }
        throw new AssertionError();
    }
    
    public static boolean r(final ByteBuffer byteBuffer) {
        return Utf8.a.f(byteBuffer, byteBuffer.position(), byteBuffer.remaining());
    }
    
    public static boolean s(final byte[] array) {
        return Utf8.a.g(array, 0, array.length);
    }
    
    public static boolean t(final byte[] array, final int n, final int n2) {
        return Utf8.a.g(array, n, n2);
    }
    
    public static int u(final int n, final ByteBuffer byteBuffer, final int n2, final int n3) {
        return Utf8.a.h(n, byteBuffer, n2, n3);
    }
    
    public static int v(final int n, final byte[] array, final int n2, final int n3) {
        return Utf8.a.i(n, array, n2, n3);
    }
    
    public static class UnpairedSurrogateException extends IllegalArgumentException
    {
        public UnpairedSurrogateException(final int i, final int j) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unpaired surrogate at index ");
            sb.append(i);
            sb.append(" of ");
            sb.append(j);
            super(sb.toString());
        }
    }
    
    public abstract static class a
    {
        public static void h(final byte b, final byte b2, final byte b3, final byte b4, final char[] array, final int n) {
            if (!m(b2) && (b << 28) + (b2 + 112) >> 30 == 0 && !m(b3) && !m(b4)) {
                final int n2 = (b & 0x7) << 18 | r(b2) << 12 | r(b3) << 6 | r(b4);
                array[n] = l(n2);
                array[n + 1] = q(n2);
                return;
            }
            throw InvalidProtocolBufferException.invalidUtf8();
        }
        
        public static void i(final byte b, final char[] array, final int n) {
            array[n] = (char)b;
        }
        
        public static void j(final byte b, final byte b2, final byte b3, final char[] array, final int n) {
            if (!m(b2) && (b != -32 || b2 >= -96) && (b != -19 || b2 < -96) && !m(b3)) {
                array[n] = (char)((b & 0xF) << 12 | r(b2) << 6 | r(b3));
                return;
            }
            throw InvalidProtocolBufferException.invalidUtf8();
        }
        
        public static void k(final byte b, final byte b2, final char[] array, final int n) {
            if (b >= -62 && !m(b2)) {
                array[n] = (char)((b & 0x1F) << 6 | r(b2));
                return;
            }
            throw InvalidProtocolBufferException.invalidUtf8();
        }
        
        public static char l(final int n) {
            return (char)((n >>> 10) + 55232);
        }
        
        public static boolean m(final byte b) {
            return b > -65;
        }
        
        public static boolean n(final byte b) {
            return b >= 0;
        }
        
        public static boolean o(final byte b) {
            return b < -16;
        }
        
        public static boolean p(final byte b) {
            return b < -32;
        }
        
        public static char q(final int n) {
            return (char)((n & 0x3FF) + 56320);
        }
        
        public static int r(final byte b) {
            return b & 0x3F;
        }
    }
    
    public abstract static class b
    {
        public static int j(final ByteBuffer byteBuffer, int i, final int n) {
            i += l(byteBuffer, i, n);
            while (i < n) {
                final int n2 = i + 1;
                final byte value = byteBuffer.get(i);
                i = n2;
                if (value < 0) {
                    if (value >= -32) {
                        if (value < -16) {
                            if (n2 < n - 1) {
                                i = n2 + 1;
                                final byte value2 = byteBuffer.get(n2);
                                if (value2 <= -65 && (value != -32 || value2 >= -96) && (value != -19 || value2 < -96) && byteBuffer.get(i) <= -65) {
                                    ++i;
                                    continue;
                                }
                                return -1;
                            }
                        }
                        else if (n2 < n - 2) {
                            i = n2 + 1;
                            final byte value3 = byteBuffer.get(n2);
                            if (value3 <= -65 && (value << 28) + (value3 + 112) >> 30 == 0) {
                                final int n3 = i + 1;
                                if (byteBuffer.get(i) <= -65) {
                                    i = n3 + 1;
                                    if (byteBuffer.get(n3) <= -65) {
                                        continue;
                                    }
                                }
                            }
                            return -1;
                        }
                        return p(byteBuffer, value, n2, n - n2);
                    }
                    if (n2 >= n) {
                        return value;
                    }
                    if (value < -62 || byteBuffer.get(n2) > -65) {
                        return -1;
                    }
                    i = n2 + 1;
                }
            }
            return 0;
        }
        
        public final String a(final ByteBuffer byteBuffer, final int n, final int n2) {
            if (byteBuffer.hasArray()) {
                return this.b(byteBuffer.array(), byteBuffer.arrayOffset() + n, n2);
            }
            if (byteBuffer.isDirect()) {
                return this.d(byteBuffer, n, n2);
            }
            return this.c(byteBuffer, n, n2);
        }
        
        public abstract String b(final byte[] p0, final int p1, final int p2);
        
        public final String c(final ByteBuffer byteBuffer, int i, int j) {
            if ((i | j | byteBuffer.limit() - i - j) >= 0) {
                final int n = i + j;
                final char[] value = new char[j];
                j = 0;
                while (i < n) {
                    final byte value2 = byteBuffer.get(i);
                    if (!n(value2)) {
                        break;
                    }
                    ++i;
                    i(value2, value, j);
                    ++j;
                }
                final int n2 = j;
                j = i;
                i = n2;
                while (j < n) {
                    final int n3 = j + 1;
                    final byte value3 = byteBuffer.get(j);
                    if (n(value3)) {
                        j = i + 1;
                        i(value3, value, i);
                        i = j;
                        j = n3;
                        while (j < n) {
                            final byte value4 = byteBuffer.get(j);
                            if (!n(value4)) {
                                break;
                            }
                            ++j;
                            i(value4, value, i);
                            ++i;
                        }
                    }
                    else if (p(value3)) {
                        if (n3 >= n) {
                            throw InvalidProtocolBufferException.invalidUtf8();
                        }
                        k(value3, byteBuffer.get(n3), value, i);
                        j = n3 + 1;
                        ++i;
                    }
                    else if (o(value3)) {
                        if (n3 >= n - 1) {
                            throw InvalidProtocolBufferException.invalidUtf8();
                        }
                        j = n3 + 1;
                        j(value3, byteBuffer.get(n3), byteBuffer.get(j), value, i);
                        ++j;
                        ++i;
                    }
                    else {
                        if (n3 >= n - 2) {
                            throw InvalidProtocolBufferException.invalidUtf8();
                        }
                        j = n3 + 1;
                        final byte value5 = byteBuffer.get(n3);
                        final int n4 = j + 1;
                        h(value3, value5, byteBuffer.get(j), byteBuffer.get(n4), value, i);
                        j = n4 + 1;
                        i = i + 1 + 1;
                    }
                }
                return new String(value, 0, i);
            }
            throw new ArrayIndexOutOfBoundsException(String.format("buffer limit=%d, index=%d, limit=%d", byteBuffer.limit(), i, j));
        }
        
        public abstract String d(final ByteBuffer p0, final int p1, final int p2);
        
        public abstract int e(final CharSequence p0, final byte[] p1, final int p2, final int p3);
        
        public final boolean f(final ByteBuffer byteBuffer, final int n, final int n2) {
            boolean b = false;
            if (this.h(0, byteBuffer, n, n2) == 0) {
                b = true;
            }
            return b;
        }
        
        public final boolean g(final byte[] array, final int n, final int n2) {
            boolean b = false;
            if (this.i(0, array, n, n2) == 0) {
                b = true;
            }
            return b;
        }
        
        public final int h(final int n, final ByteBuffer byteBuffer, final int n2, final int n3) {
            if (byteBuffer.hasArray()) {
                final int arrayOffset = byteBuffer.arrayOffset();
                return this.i(n, byteBuffer.array(), n2 + arrayOffset, arrayOffset + n3);
            }
            if (byteBuffer.isDirect()) {
                return this.l(n, byteBuffer, n2, n3);
            }
            return this.k(n, byteBuffer, n2, n3);
        }
        
        public abstract int i(final int p0, final byte[] p1, final int p2, final int p3);
        
        public final int k(int value, final ByteBuffer byteBuffer, int n, final int n2) {
            int n3 = n;
            if (value != 0) {
                if (n >= n2) {
                    return value;
                }
                final byte b = (byte)value;
                Label_0050: {
                    if (b < -32) {
                        if (b >= -62) {
                            value = n + 1;
                            if (byteBuffer.get(n) <= -65) {
                                break Label_0050;
                            }
                        }
                        return -1;
                    }
                    if (b < -16) {
                        final byte b2 = (byte)(value = (byte)~(value >> 8));
                        int n4 = n;
                        if (b2 == 0) {
                            n4 = n + 1;
                            value = byteBuffer.get(n);
                            if (n4 >= n2) {
                                return n(b, value);
                            }
                        }
                        if (value <= -65 && (b != -32 || value >= -96) && (b != -19 || value < -96)) {
                            value = n4 + 1;
                            if (byteBuffer.get(n4) <= -65) {
                                break Label_0050;
                            }
                        }
                        return -1;
                    }
                    byte value2 = (byte)~(value >> 8);
                    if (value2 == 0) {
                        value = n + 1;
                        value2 = byteBuffer.get(n);
                        if (value >= n2) {
                            return n(b, value2);
                        }
                        n = 0;
                    }
                    else {
                        final int n5 = (byte)(value >> 16);
                        value = n;
                        n = n5;
                    }
                    int n6 = value;
                    int value3 = n;
                    if (n == 0) {
                        n6 = value + 1;
                        value3 = byteBuffer.get(value);
                        if (n6 >= n2) {
                            return o(b, value2, value3);
                        }
                    }
                    if (value2 <= -65 && (b << 28) + (value2 + 112) >> 30 == 0 && value3 <= -65) {
                        n3 = n6 + 1;
                        if (byteBuffer.get(n6) <= -65) {
                            return j(byteBuffer, n3, n2);
                        }
                    }
                    return -1;
                }
                n3 = value;
            }
            return j(byteBuffer, n3, n2);
        }
        
        public abstract int l(final int p0, final ByteBuffer p1, final int p2, final int p3);
    }
    
    public static final class c extends b
    {
        public static int m(final byte[] array, int n, final int n2) {
            while (n < n2 && array[n] >= 0) {
                ++n;
            }
            if (n >= n2) {
                n = 0;
            }
            else {
                n = n(array, n, n2);
            }
            return n;
        }
        
        public static int n(final byte[] array, int i, final int n) {
            while (i < n) {
                final int n2 = i + 1;
                final byte b = array[i];
                i = n2;
                if (b < 0) {
                    if (b < -32) {
                        if (n2 >= n) {
                            return b;
                        }
                        if (b >= -62) {
                            i = n2 + 1;
                            if (array[n2] <= -65) {
                                continue;
                            }
                        }
                        return -1;
                    }
                    else if (b < -16) {
                        if (n2 >= n - 1) {
                            return q(array, n2, n);
                        }
                        final int n3 = n2 + 1;
                        i = array[n2];
                        if (i <= -65 && (b != -32 || i >= -96) && (b != -19 || i < -96)) {
                            i = n3 + 1;
                            if (array[n3] <= -65) {
                                continue;
                            }
                        }
                        return -1;
                    }
                    else {
                        if (n2 >= n - 2) {
                            return q(array, n2, n);
                        }
                        i = n2 + 1;
                        final byte b2 = array[n2];
                        if (b2 <= -65 && (b << 28) + (b2 + 112) >> 30 == 0) {
                            final int n4 = i + 1;
                            if (array[i] <= -65) {
                                i = n4 + 1;
                                if (array[n4] <= -65) {
                                    continue;
                                }
                            }
                        }
                        return -1;
                    }
                }
            }
            return 0;
        }
        
        @Override
        public String b(final byte[] array, int i, int j) {
            if ((i | j | array.length - i - j) >= 0) {
                final int n = i + j;
                final char[] value = new char[j];
                j = 0;
                while (i < n) {
                    final byte b = array[i];
                    if (!n(b)) {
                        break;
                    }
                    ++i;
                    i(b, value, j);
                    ++j;
                }
                final int n2 = j;
                j = i;
                i = n2;
                while (j < n) {
                    final int n3 = j + 1;
                    final byte b2 = array[j];
                    if (n(b2)) {
                        j = i + 1;
                        i(b2, value, i);
                        i = j;
                        j = n3;
                        while (j < n) {
                            final byte b3 = array[j];
                            if (!n(b3)) {
                                break;
                            }
                            ++j;
                            i(b3, value, i);
                            ++i;
                        }
                    }
                    else if (p(b2)) {
                        if (n3 >= n) {
                            throw InvalidProtocolBufferException.invalidUtf8();
                        }
                        k(b2, array[n3], value, i);
                        j = n3 + 1;
                        ++i;
                    }
                    else if (o(b2)) {
                        if (n3 >= n - 1) {
                            throw InvalidProtocolBufferException.invalidUtf8();
                        }
                        j = n3 + 1;
                        j(b2, array[n3], array[j], value, i);
                        ++j;
                        ++i;
                    }
                    else {
                        if (n3 >= n - 2) {
                            throw InvalidProtocolBufferException.invalidUtf8();
                        }
                        j = n3 + 1;
                        final byte b4 = array[n3];
                        final int n4 = j + 1;
                        h(b2, b4, array[j], array[n4], value, i);
                        j = n4 + 1;
                        i = i + 1 + 1;
                    }
                }
                return new String(value, 0, i);
            }
            throw new ArrayIndexOutOfBoundsException(String.format("buffer length=%d, index=%d, size=%d", array.length, i, j));
        }
        
        @Override
        public String d(final ByteBuffer byteBuffer, final int n, final int n2) {
            return ((b)this).c(byteBuffer, n, n2);
        }
        
        @Override
        public int e(final CharSequence charSequence, final byte[] array, int i, int j) {
            final int length = charSequence.length();
            final int n = j + i;
            int n2;
            char char1;
            for (j = 0; j < length; ++j) {
                n2 = j + i;
                if (n2 >= n) {
                    break;
                }
                char1 = charSequence.charAt(j);
                if (char1 >= '\u0080') {
                    break;
                }
                array[n2] = (byte)char1;
            }
            if (j == length) {
                return i + length;
            }
            int k = i + j;
            char char2;
            int n3;
            int n4;
            char char3;
            int n5;
            int n6;
            int n7;
            int n8;
            StringBuilder sb;
            for (i = j; i < length; ++i, k = j) {
                char2 = charSequence.charAt(i);
                if (char2 < '\u0080' && k < n) {
                    j = k + 1;
                    array[k] = (byte)char2;
                }
                else if (char2 < '\u0800' && k <= n - 2) {
                    n3 = k + 1;
                    array[k] = (byte)(char2 >>> 6 | 0x3C0);
                    j = n3 + 1;
                    array[n3] = (byte)((char2 & '?') | 0x80);
                }
                else if ((char2 < '\ud800' || '\udfff' < char2) && k <= n - 3) {
                    j = k + 1;
                    array[k] = (byte)(char2 >>> 12 | 0x1E0);
                    n4 = j + 1;
                    array[j] = (byte)((char2 >>> 6 & 0x3F) | 0x80);
                    j = n4 + 1;
                    array[n4] = (byte)((char2 & '?') | 0x80);
                }
                else {
                    if (k <= n - 4) {
                        j = i + 1;
                        if (j != charSequence.length()) {
                            char3 = charSequence.charAt(j);
                            if (Character.isSurrogatePair(char2, char3)) {
                                i = Character.toCodePoint(char2, char3);
                                n5 = k + 1;
                                array[k] = (byte)(i >>> 18 | 0xF0);
                                n6 = n5 + 1;
                                array[n5] = (byte)((i >>> 12 & 0x3F) | 0x80);
                                n7 = n6 + 1;
                                array[n6] = (byte)((i >>> 6 & 0x3F) | 0x80);
                                n8 = n7 + 1;
                                array[n7] = (byte)((i & 0x3F) | 0x80);
                                i = j;
                                j = n8;
                                continue;
                            }
                            i = j;
                        }
                        throw new UnpairedSurrogateException(i - 1, length);
                    }
                    if ('\ud800' <= char2 && char2 <= '\udfff') {
                        j = i + 1;
                        if (j == charSequence.length() || !Character.isSurrogatePair(char2, charSequence.charAt(j))) {
                            throw new UnpairedSurrogateException(i, length);
                        }
                    }
                    sb = new StringBuilder();
                    sb.append("Failed writing ");
                    sb.append(char2);
                    sb.append(" at index ");
                    sb.append(k);
                    throw new ArrayIndexOutOfBoundsException(sb.toString());
                }
            }
            return k;
        }
        
        @Override
        public int i(int n, final byte[] array, int n2, final int n3) {
            int n4 = n2;
            if (n != 0) {
                if (n2 >= n3) {
                    return n;
                }
                final byte b = (byte)n;
                Label_0048: {
                    if (b < -32) {
                        if (b >= -62) {
                            n = n2 + 1;
                            if (array[n2] <= -65) {
                                break Label_0048;
                            }
                        }
                        return -1;
                    }
                    if (b < -16) {
                        final byte b2 = (byte)(n = (byte)~(n >> 8));
                        int n5 = n2;
                        if (b2 == 0) {
                            n5 = n2 + 1;
                            n = array[n2];
                            if (n5 >= n3) {
                                return n(b, n);
                            }
                        }
                        if (n <= -65 && (b != -32 || n >= -96) && (b != -19 || n < -96)) {
                            n = n5 + 1;
                            if (array[n5] <= -65) {
                                break Label_0048;
                            }
                        }
                        return -1;
                    }
                    byte b3 = (byte)~(n >> 8);
                    if (b3 == 0) {
                        n = n2 + 1;
                        b3 = array[n2];
                        if (n >= n3) {
                            return n(b, b3);
                        }
                        n2 = 0;
                    }
                    else {
                        final int n6 = (byte)(n >> 16);
                        n = n2;
                        n2 = n6;
                    }
                    int n7 = n;
                    int n8 = n2;
                    if (n2 == 0) {
                        n7 = n + 1;
                        n8 = array[n];
                        if (n7 >= n3) {
                            return o(b, b3, n8);
                        }
                    }
                    if (b3 <= -65 && (b << 28) + (b3 + 112) >> 30 == 0 && n8 <= -65) {
                        n4 = n7 + 1;
                        if (array[n7] <= -65) {
                            return m(array, n4, n3);
                        }
                    }
                    return -1;
                }
                n4 = n;
            }
            return m(array, n4, n3);
        }
        
        @Override
        public int l(final int n, final ByteBuffer byteBuffer, final int n2, final int n3) {
            return ((b)this).k(n, byteBuffer, n2, n3);
        }
    }
    
    public static final class d extends b
    {
        public static boolean m() {
            return d12.I() && d12.J();
        }
        
        public static int n(long n, int w) {
            final int p2 = p(n, w);
            n += p2;
            w -= p2;
            while (true) {
                final int n2 = 0;
                int n3 = w;
                w = n2;
                long n4;
                while (true) {
                    n4 = n;
                    if (n3 <= 0) {
                        break;
                    }
                    n4 = n + 1L;
                    w = d12.w(n);
                    if (w < 0) {
                        break;
                    }
                    --n3;
                    n = n4;
                }
                if (n3 == 0) {
                    return 0;
                }
                --n3;
                if (w < -32) {
                    if (n3 == 0) {
                        return w;
                    }
                    --n3;
                    if (w < -62) {
                        break;
                    }
                    n = 1L + n4;
                    w = n3;
                    if (d12.w(n4) > -65) {
                        break;
                    }
                    continue;
                }
                else if (w < -16) {
                    if (n3 < 2) {
                        return r(n4, w, n3);
                    }
                    n3 -= 2;
                    final long n5 = n4 + 1L;
                    final byte w2 = d12.w(n4);
                    if (w2 > -65 || (w == -32 && w2 < -96) || (w == -19 && w2 >= -96)) {
                        return -1;
                    }
                    n = 1L + n5;
                    w = n3;
                    if (d12.w(n5) > -65) {
                        return -1;
                    }
                    continue;
                }
                else {
                    if (n3 < 3) {
                        return r(n4, w, n3);
                    }
                    n3 -= 3;
                    n = n4 + 1L;
                    final byte w3 = d12.w(n4);
                    if (w3 > -65 || (w << 28) + (w3 + 112) >> 30 != 0) {
                        return -1;
                    }
                    final long n6 = n + 1L;
                    if (d12.w(n) > -65) {
                        return -1;
                    }
                    n = 1L + n6;
                    w = n3;
                    if (d12.w(n6) > -65) {
                        return -1;
                    }
                    continue;
                }
            }
            return -1;
        }
        
        public static int o(final byte[] array, long n, int x) {
            final int q = q(array, n, x);
            x -= q;
            n += q;
            while (true) {
                final int n2 = 0;
                int n3 = x;
                x = n2;
                long n4;
                while (true) {
                    n4 = n;
                    if (n3 <= 0) {
                        break;
                    }
                    n4 = n + 1L;
                    x = d12.x(array, n);
                    if (x < 0) {
                        break;
                    }
                    --n3;
                    n = n4;
                }
                if (n3 == 0) {
                    return 0;
                }
                --n3;
                if (x < -32) {
                    if (n3 == 0) {
                        return x;
                    }
                    --n3;
                    if (x < -62) {
                        break;
                    }
                    n = 1L + n4;
                    x = n3;
                    if (d12.x(array, n4) > -65) {
                        break;
                    }
                    continue;
                }
                else if (x < -16) {
                    if (n3 < 2) {
                        return s(array, x, n4, n3);
                    }
                    n3 -= 2;
                    final long n5 = n4 + 1L;
                    final byte x2 = d12.x(array, n4);
                    if (x2 > -65 || (x == -32 && x2 < -96) || (x == -19 && x2 >= -96)) {
                        return -1;
                    }
                    n = 1L + n5;
                    x = n3;
                    if (d12.x(array, n5) > -65) {
                        return -1;
                    }
                    continue;
                }
                else {
                    if (n3 < 3) {
                        return s(array, x, n4, n3);
                    }
                    n3 -= 3;
                    n = n4 + 1L;
                    final byte x3 = d12.x(array, n4);
                    if (x3 > -65 || (x << 28) + (x3 + 112) >> 30 != 0) {
                        return -1;
                    }
                    final long n6 = n + 1L;
                    if (d12.x(array, n) > -65) {
                        return -1;
                    }
                    n = 1L + n6;
                    x = n3;
                    if (d12.x(array, n6) > -65) {
                        return -1;
                    }
                    continue;
                }
            }
            return -1;
        }
        
        public static int p(long n, final int n2) {
            if (n2 < 16) {
                return 0;
            }
            int i;
            int n3;
            for (n3 = (i = (int)(-n & 0x7L)); i > 0; --i, ++n) {
                if (d12.w(n) < 0) {
                    return n3 - i;
                }
            }
            int n4;
            for (n4 = n2 - n3; n4 >= 8 && (d12.D(n) & 0x8080808080808080L) == 0x0L; n += 8L, n4 -= 8) {}
            return n2 - n4;
        }
        
        public static int q(final byte[] array, long n, final int n2) {
            int n3 = 0;
            if (n2 < 16) {
                return 0;
            }
            final int n4 = (int)n;
            long n5 = n;
            int n6;
            while (true) {
                n6 = n3;
                n = n5;
                if (n3 >= 8 - (n4 & 0x7)) {
                    break;
                }
                if (d12.x(array, n5) < 0) {
                    return n3;
                }
                ++n3;
                ++n5;
            }
            int i;
            long n8;
            while (true) {
                final int n7 = n6 + 8;
                i = n6;
                n8 = n;
                if (n7 > n2) {
                    break;
                }
                if ((d12.E(array, d12.h + n) & 0x8080808080808080L) != 0x0L) {
                    i = n6;
                    n8 = n;
                    break;
                }
                n += 8L;
                n6 = n7;
            }
            while (i < n2) {
                if (d12.x(array, n8) < 0) {
                    return i;
                }
                ++i;
                ++n8;
            }
            return n2;
        }
        
        public static int r(final long n, final int n2, final int n3) {
            if (n3 == 0) {
                return m(n2);
            }
            if (n3 == 1) {
                return n(n2, d12.w(n));
            }
            if (n3 == 2) {
                return o(n2, d12.w(n), d12.w(n + 1L));
            }
            throw new AssertionError();
        }
        
        public static int s(final byte[] array, final int n, final long n2, final int n3) {
            if (n3 == 0) {
                return m(n);
            }
            if (n3 == 1) {
                return n(n, d12.x(array, n2));
            }
            if (n3 == 2) {
                return o(n, d12.x(array, n2), d12.x(array, n2 + 1L));
            }
            throw new AssertionError();
        }
        
        @Override
        public String b(final byte[] array, final int n, final int length) {
            final Charset b = t.b;
            final String s = new String(array, n, length, b);
            if (!s.contains("\ufffd")) {
                return s;
            }
            if (Arrays.equals(s.getBytes(b), Arrays.copyOfRange(array, n, length + n))) {
                return s;
            }
            throw InvalidProtocolBufferException.invalidUtf8();
        }
        
        @Override
        public String d(final ByteBuffer byteBuffer, int n, int i) {
            if ((n | i | byteBuffer.limit() - n - i) >= 0) {
                long n2 = d12.k(byteBuffer) + n;
                final long n3 = i + n2;
                final char[] value = new char[i];
                n = 0;
                while (n2 < n3) {
                    final byte w = d12.w(n2);
                    if (!n(w)) {
                        break;
                    }
                    ++n2;
                    i(w, value, n);
                    ++n;
                }
                while (n2 < n3) {
                    final long n4 = n2 + 1L;
                    final byte w2 = d12.w(n2);
                    if (n(w2)) {
                        i = n + 1;
                        i(w2, value, n);
                        n2 = n4;
                        n = i;
                        while (n2 < n3) {
                            final byte w3 = d12.w(n2);
                            if (!n(w3)) {
                                break;
                            }
                            ++n2;
                            i(w3, value, n);
                            ++n;
                        }
                    }
                    else if (p(w2)) {
                        if (n4 >= n3) {
                            throw InvalidProtocolBufferException.invalidUtf8();
                        }
                        k(w2, d12.w(n4), value, n);
                        ++n;
                        n2 = n4 + 1L;
                    }
                    else if (o(w2)) {
                        if (n4 >= n3 - 1L) {
                            throw InvalidProtocolBufferException.invalidUtf8();
                        }
                        final long n5 = n4 + 1L;
                        j(w2, d12.w(n4), d12.w(n5), value, n);
                        n2 = n5 + 1L;
                        ++n;
                    }
                    else {
                        if (n4 >= n3 - 2L) {
                            throw InvalidProtocolBufferException.invalidUtf8();
                        }
                        final long n6 = n4 + 1L;
                        final byte w4 = d12.w(n4);
                        final long n7 = n6 + 1L;
                        h(w2, w4, d12.w(n6), d12.w(n7), value, n);
                        n = n + 1 + 1;
                        n2 = n7 + 1L;
                    }
                }
                return new String(value, 0, n);
            }
            throw new ArrayIndexOutOfBoundsException(String.format("buffer limit=%d, index=%d, limit=%d", byteBuffer.limit(), n, i));
        }
        
        @Override
        public int e(final CharSequence charSequence, final byte[] array, int i, int n) {
            long n2 = i;
            final long n3 = n + n2;
            final int length = charSequence.length();
            if (length > n || array.length - n < i) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Failed writing ");
                sb.append(charSequence.charAt(length - 1));
                sb.append(" at index ");
                sb.append(i + n);
                throw new ArrayIndexOutOfBoundsException(sb.toString());
            }
            n = 0;
            char c;
            long n4;
            while (true) {
                c = '\u0080';
                n4 = 1L;
                if (n >= length) {
                    break;
                }
                i = charSequence.charAt(n);
                if (i >= 128) {
                    break;
                }
                d12.P(array, n2, (byte)i);
                ++n;
                ++n2;
            }
            i = n;
            long lng = n2;
            if (n == length) {
                return (int)n2;
            }
            while (i < length) {
                final char char1 = charSequence.charAt(i);
                long n7 = 0L;
                Label_0523: {
                    if (char1 < c && lng < n3) {
                        d12.P(array, lng, (byte)char1);
                        final long n5 = n4;
                        final long n6 = lng + n4;
                        n = i;
                        n4 = n5;
                        i = c;
                        n7 = n6;
                    }
                    else if (char1 < '\u0800' && lng <= n3 - 2L) {
                        final long n8 = lng + n4;
                        d12.P(array, lng, (byte)(char1 >>> 6 | 0x3C0));
                        d12.P(array, n8, (byte)((char1 & '?') | 0x80));
                        final int n9 = 128;
                        n7 = n8 + n4;
                        n = i;
                        i = n9;
                    }
                    else if ((char1 < '\ud800' || '\udfff' < char1) && lng <= n3 - 3L) {
                        final long n10 = lng + n4;
                        d12.P(array, lng, (byte)(char1 >>> 12 | 0x1E0));
                        final long n11 = n10 + n4;
                        d12.P(array, n10, (byte)((char1 >>> 6 & 0x3F) | 0x80));
                        d12.P(array, n11, (byte)((char1 & '?') | 0x80));
                        n7 = n11 + 1L;
                        n4 = 1L;
                        final int n12 = 128;
                        n = i;
                        i = n12;
                    }
                    else {
                        if (lng <= n3 - 4L) {
                            n = i + 1;
                            if (n != length) {
                                final char char2 = charSequence.charAt(n);
                                if (Character.isSurrogatePair(char1, char2)) {
                                    final int codePoint = Character.toCodePoint(char1, char2);
                                    final long n13 = lng + 1L;
                                    d12.P(array, lng, (byte)(codePoint >>> 18 | 0xF0));
                                    final long n14 = n13 + 1L;
                                    i = 128;
                                    d12.P(array, n13, (byte)((codePoint >>> 12 & 0x3F) | 0x80));
                                    final long n15 = n14 + 1L;
                                    d12.P(array, n14, (byte)((codePoint >>> 6 & 0x3F) | 0x80));
                                    n4 = 1L;
                                    n7 = n15 + 1L;
                                    d12.P(array, n15, (byte)((codePoint & 0x3F) | 0x80));
                                    break Label_0523;
                                }
                                i = n;
                            }
                            throw new UnpairedSurrogateException(i - 1, length);
                        }
                        if ('\ud800' <= char1 && char1 <= '\udfff') {
                            n = i + 1;
                            if (n == length || !Character.isSurrogatePair(char1, charSequence.charAt(n))) {
                                throw new UnpairedSurrogateException(i, length);
                            }
                        }
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Failed writing ");
                        sb2.append(char1);
                        sb2.append(" at index ");
                        sb2.append(lng);
                        throw new ArrayIndexOutOfBoundsException(sb2.toString());
                    }
                }
                ++n;
                c = (char)i;
                i = n;
                lng = n7;
            }
            return (int)lng;
        }
        
        @Override
        public int i(int x, final byte[] array, int x2, int x3) {
            if ((x2 | x3 | array.length - x3) >= 0) {
                long n = x2;
                final long n2 = x3;
                long n3 = n;
                if (x != 0) {
                    if (n >= n2) {
                        return x;
                    }
                    final byte b = (byte)x;
                    if (b < -32) {
                        if (b < -62 || d12.x(array, n) > -65) {
                            return -1;
                        }
                        n3 = 1L + n;
                    }
                    else {
                        long n5 = 0L;
                        Label_0188: {
                            if (b < -16) {
                                x2 = (byte)~(x >> 8);
                                long n4 = n;
                                if ((x = x2) == 0) {
                                    n4 = n + 1L;
                                    x = d12.x(array, n);
                                    if (n4 >= n2) {
                                        return n(b, x);
                                    }
                                }
                                if (x <= -65 && (b != -32 || x >= -96) && (b != -19 || x < -96)) {
                                    n5 = n4 + 1L;
                                    if (d12.x(array, n4) <= -65) {
                                        break Label_0188;
                                    }
                                }
                                return -1;
                            }
                            x2 = (byte)~(x >> 8);
                            if (x2 == 0) {
                                final long n6 = n + 1L;
                                x2 = d12.x(array, n);
                                if (n6 >= n2) {
                                    return n(b, x2);
                                }
                                x = 0;
                                n = n6;
                            }
                            else {
                                x = (byte)(x >> 16);
                            }
                            long n7 = n;
                            x3 = x;
                            if (x == 0) {
                                n7 = n + 1L;
                                x3 = d12.x(array, n);
                                if (n7 >= n2) {
                                    return o(b, x2, x3);
                                }
                            }
                            if (x2 <= -65 && (b << 28) + (x2 + 112) >> 30 == 0 && x3 <= -65) {
                                n5 = n7 + 1L;
                                if (d12.x(array, n7) <= -65) {
                                    break Label_0188;
                                }
                            }
                            return -1;
                        }
                        n3 = n5;
                    }
                }
                return o(array, n3, (int)(n2 - n3));
            }
            throw new ArrayIndexOutOfBoundsException(String.format("Array length=%d, index=%d, limit=%d", array.length, x2, x3));
        }
        
        @Override
        public int l(int w, final ByteBuffer byteBuffer, int w2, int w3) {
            if ((w2 | w3 | byteBuffer.limit() - w3) >= 0) {
                long n = d12.k(byteBuffer) + w2;
                final long n2 = w3 - w2 + n;
                long n3 = n;
                if (w != 0) {
                    if (n >= n2) {
                        return w;
                    }
                    final byte b = (byte)w;
                    if (b < -32) {
                        if (b >= -62) {
                            n3 = 1L + n;
                            if (d12.w(n) <= -65) {
                                return n(n3, (int)(n2 - n3));
                            }
                        }
                        return -1;
                    }
                    if (b < -16) {
                        w2 = (byte)~(w >> 8);
                        long n4 = n;
                        if ((w = w2) == 0) {
                            n4 = n + 1L;
                            w = d12.w(n);
                            if (n4 >= n2) {
                                return n(b, w);
                            }
                        }
                        if (w <= -65 && (b != -32 || w >= -96) && (b != -19 || w < -96)) {
                            n3 = 1L + n4;
                            if (d12.w(n4) <= -65) {
                                return n(n3, (int)(n2 - n3));
                            }
                        }
                        return -1;
                    }
                    w2 = (byte)~(w >> 8);
                    if (w2 == 0) {
                        final long n5 = n + 1L;
                        w2 = d12.w(n);
                        if (n5 >= n2) {
                            return n(b, w2);
                        }
                        w = 0;
                        n = n5;
                    }
                    else {
                        w = (byte)(w >> 16);
                    }
                    long n6 = n;
                    w3 = w;
                    if (w == 0) {
                        n6 = n + 1L;
                        w3 = d12.w(n);
                        if (n6 >= n2) {
                            return o(b, w2, w3);
                        }
                    }
                    if (w2 <= -65 && (b << 28) + (w2 + 112) >> 30 == 0 && w3 <= -65) {
                        n3 = 1L + n6;
                        if (d12.w(n6) <= -65) {
                            return n(n3, (int)(n2 - n3));
                        }
                    }
                    return -1;
                }
                return n(n3, (int)(n2 - n3));
            }
            throw new ArrayIndexOutOfBoundsException(String.format("buffer limit=%d, index=%d, limit=%d", byteBuffer.limit(), w2, w3));
        }
    }
}

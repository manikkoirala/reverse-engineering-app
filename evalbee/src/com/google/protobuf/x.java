// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

public class x
{
    public final a a;
    public final Object b;
    public final Object c;
    
    public x(final WireFormat.FieldType fieldType, final Object b, final WireFormat.FieldType fieldType2, final Object c) {
        this.a = new a(fieldType, b, fieldType2, c);
        this.b = b;
        this.c = c;
    }
    
    public static int b(final a a, final Object o, final Object o2) {
        return o.b(a.a, 1, o) + o.b(a.c, 2, o2);
    }
    
    public static x d(final WireFormat.FieldType fieldType, final Object o, final WireFormat.FieldType fieldType2, final Object o2) {
        return new x(fieldType, o, fieldType2, o2);
    }
    
    public static void e(final CodedOutputStream codedOutputStream, final a a, final Object o, final Object o2) {
        o.u(codedOutputStream, a.a, 1, o);
        o.u(codedOutputStream, a.c, 2, o2);
    }
    
    public int a(final int n, final Object o, final Object o2) {
        return CodedOutputStream.Q(n) + CodedOutputStream.A(b(this.a, o, o2));
    }
    
    public a c() {
        return this.a;
    }
    
    public static class a
    {
        public final WireFormat.FieldType a;
        public final Object b;
        public final WireFormat.FieldType c;
        public final Object d;
        
        public a(final WireFormat.FieldType a, final Object b, final WireFormat.FieldType c, final Object d) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
        }
    }
}

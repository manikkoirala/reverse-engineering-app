// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

import java.util.Collection;
import java.util.ArrayList;
import java.util.Map;
import java.util.Iterator;
import java.util.List;

public final class o
{
    public static final o d;
    public final i0 a;
    public boolean b;
    public boolean c;
    
    static {
        d = new o(true);
    }
    
    public o() {
        this.a = i0.q(16);
    }
    
    public o(final i0 a) {
        this.a = a;
        this.o();
    }
    
    public o(final boolean b) {
        this(i0.q(0));
        this.o();
    }
    
    public static int b(final WireFormat.FieldType fieldType, int q, final Object o) {
        final int n = q = CodedOutputStream.Q(q);
        if (fieldType == WireFormat.FieldType.GROUP) {
            q = n * 2;
        }
        return q + c(fieldType, o);
    }
    
    public static int c(final WireFormat.FieldType fieldType, final Object o) {
        switch (o$a.b[fieldType.ordinal()]) {
            default: {
                throw new RuntimeException("There is no way to get here, but the compiler thinks otherwise.");
            }
            case 18: {
                if (o instanceof t.a) {
                    return CodedOutputStream.m(((t.a)o).getNumber());
                }
                return CodedOutputStream.m((int)o);
            }
            case 17: {
                return CodedOutputStream.N((long)o);
            }
            case 16: {
                return CodedOutputStream.L((int)o);
            }
            case 15: {
                return CodedOutputStream.J((long)o);
            }
            case 14: {
                return CodedOutputStream.H((int)o);
            }
            case 13: {
                return CodedOutputStream.S((int)o);
            }
            case 12: {
                if (o instanceof ByteString) {
                    return CodedOutputStream.i((ByteString)o);
                }
                return CodedOutputStream.g((byte[])o);
            }
            case 11: {
                if (o instanceof ByteString) {
                    return CodedOutputStream.i((ByteString)o);
                }
                return CodedOutputStream.P((String)o);
            }
            case 10: {
                return CodedOutputStream.C((a0)o);
            }
            case 9: {
                return CodedOutputStream.u((a0)o);
            }
            case 8: {
                return CodedOutputStream.f((boolean)o);
            }
            case 7: {
                return CodedOutputStream.o((int)o);
            }
            case 6: {
                return CodedOutputStream.q((long)o);
            }
            case 5: {
                return CodedOutputStream.x((int)o);
            }
            case 4: {
                return CodedOutputStream.U((long)o);
            }
            case 3: {
                return CodedOutputStream.z((long)o);
            }
            case 2: {
                return CodedOutputStream.s((float)o);
            }
            case 1: {
                return CodedOutputStream.k((double)o);
            }
        }
    }
    
    public static int d(final b b, final Object o) {
        final WireFormat.FieldType b2 = b.b();
        final int number = b.getNumber();
        if (!b.i()) {
            return b(b2, number, o);
        }
        final boolean packed = b.isPacked();
        int n = 0;
        final int n2 = 0;
        final List list = (List)o;
        if (packed) {
            final Iterator iterator = list.iterator();
            int n3 = n2;
            while (iterator.hasNext()) {
                n3 += c(b2, iterator.next());
            }
            return CodedOutputStream.Q(number) + n3 + CodedOutputStream.S(n3);
        }
        final Iterator iterator2 = list.iterator();
        while (iterator2.hasNext()) {
            n += b(b2, number, iterator2.next());
        }
        return n;
    }
    
    public static int i(final WireFormat.FieldType fieldType, final boolean b) {
        if (b) {
            return 2;
        }
        return fieldType.getWireType();
    }
    
    public static boolean l(final Map.Entry entry) {
        zu0.a(entry.getKey());
        throw null;
    }
    
    public static boolean m(final WireFormat.FieldType fieldType, final Object o) {
        t.a(o);
        final int n = o$a.a[fieldType.getJavaType().ordinal()];
        final boolean b = true;
        final boolean b2 = true;
        boolean b3 = true;
        switch (n) {
            default: {
                return false;
            }
            case 9: {
                if (!(o instanceof a0)) {
                    b3 = false;
                }
                return b3;
            }
            case 8: {
                boolean b4 = b;
                if (!(o instanceof Integer)) {
                    b4 = (o instanceof t.a && b);
                }
                return b4;
            }
            case 7: {
                boolean b5 = b2;
                if (!(o instanceof ByteString)) {
                    b5 = (o instanceof byte[] && b2);
                }
                return b5;
            }
            case 6: {
                return o instanceof String;
            }
            case 5: {
                return o instanceof Boolean;
            }
            case 4: {
                return o instanceof Double;
            }
            case 3: {
                return o instanceof Float;
            }
            case 2: {
                return o instanceof Long;
            }
            case 1: {
                return o instanceof Integer;
            }
        }
    }
    
    public static o r() {
        return new o();
    }
    
    public static void u(final CodedOutputStream codedOutputStream, final WireFormat.FieldType fieldType, final int n, final Object o) {
        if (fieldType == WireFormat.FieldType.GROUP) {
            codedOutputStream.v0(n, (a0)o);
        }
        else {
            codedOutputStream.R0(n, i(fieldType, false));
            v(codedOutputStream, fieldType, o);
        }
    }
    
    public static void v(final CodedOutputStream codedOutputStream, final WireFormat.FieldType fieldType, final Object o) {
        switch (o$a.b[fieldType.ordinal()]) {
            default: {
                return;
            }
            case 18: {
                int n;
                if (o instanceof t.a) {
                    n = ((t.a)o).getNumber();
                }
                else {
                    n = (int)o;
                }
                codedOutputStream.o0(n);
                return;
            }
            case 17: {
                codedOutputStream.O0((long)o);
                return;
            }
            case 16: {
                codedOutputStream.M0((int)o);
                return;
            }
            case 15: {
                codedOutputStream.K0((long)o);
                return;
            }
            case 14: {
                codedOutputStream.I0((int)o);
                return;
            }
            case 13: {
                codedOutputStream.T0((int)o);
                return;
            }
            case 12: {
                if (o instanceof ByteString) {
                    break;
                }
                codedOutputStream.h0((byte[])o);
                return;
            }
            case 11: {
                if (o instanceof ByteString) {
                    break;
                }
                codedOutputStream.Q0((String)o);
                return;
            }
            case 10: {
                codedOutputStream.E0((a0)o);
                return;
            }
            case 9: {
                codedOutputStream.x0((a0)o);
                return;
            }
            case 8: {
                codedOutputStream.g0((boolean)o);
                return;
            }
            case 7: {
                codedOutputStream.q0((int)o);
                return;
            }
            case 6: {
                codedOutputStream.s0((long)o);
                return;
            }
            case 5: {
                codedOutputStream.A0((int)o);
                return;
            }
            case 4: {
                codedOutputStream.V0((long)o);
                return;
            }
            case 3: {
                codedOutputStream.C0((long)o);
                return;
            }
            case 2: {
                codedOutputStream.u0((float)o);
                return;
            }
            case 1: {
                codedOutputStream.m0((double)o);
                return;
            }
        }
        codedOutputStream.k0((ByteString)o);
    }
    
    public o a() {
        final o r = r();
        for (int i = 0; i < this.a.k(); ++i) {
            final Map.Entry j = this.a.j(i);
            zu0.a(j.getKey());
            r.s(null, j.getValue());
        }
        for (final Map.Entry<Object, V> entry : this.a.m()) {
            zu0.a(entry.getKey());
            r.s(null, entry.getValue());
        }
        r.c = this.c;
        return r;
    }
    
    public Iterator e() {
        if (this.c) {
            return new bj0(this.a.h().iterator());
        }
        return this.a.h().iterator();
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof o && this.a.equals(((o)o).a));
    }
    
    public int f() {
        int i = 0;
        int n = 0;
        while (i < this.a.k()) {
            n += this.g(this.a.j(i));
            ++i;
        }
        final Iterator iterator = this.a.m().iterator();
        while (iterator.hasNext()) {
            n += this.g((Map.Entry)iterator.next());
        }
        return n;
    }
    
    public final int g(final Map.Entry entry) {
        zu0.a(entry.getKey());
        entry.getValue();
        throw null;
    }
    
    public int h() {
        int i = 0;
        int n = 0;
        while (i < this.a.k()) {
            final Map.Entry j = this.a.j(i);
            zu0.a(j.getKey());
            n += d(null, j.getValue());
            ++i;
        }
        for (final Map.Entry<Object, V> entry : this.a.m()) {
            zu0.a(entry.getKey());
            n += d(null, entry.getValue());
        }
        return n;
    }
    
    @Override
    public int hashCode() {
        return this.a.hashCode();
    }
    
    public boolean j() {
        return this.a.isEmpty();
    }
    
    public boolean k() {
        for (int i = 0; i < this.a.k(); ++i) {
            if (!l(this.a.j(i))) {
                return false;
            }
        }
        final Iterator iterator = this.a.m().iterator();
        while (iterator.hasNext()) {
            if (!l((Map.Entry)iterator.next())) {
                return false;
            }
        }
        return true;
    }
    
    public Iterator n() {
        if (this.c) {
            return new bj0(this.a.entrySet().iterator());
        }
        return this.a.entrySet().iterator();
    }
    
    public void o() {
        if (this.b) {
            return;
        }
        for (int i = 0; i < this.a.k(); ++i) {
            final Map.Entry j = this.a.j(i);
            if (j.getValue() instanceof GeneratedMessageLite) {
                j.getValue().J();
            }
        }
        this.a.p();
        this.b = true;
    }
    
    public void p(final o o) {
        for (int i = 0; i < o.a.k(); ++i) {
            this.q(o.a.j(i));
        }
        final Iterator iterator = o.a.m().iterator();
        while (iterator.hasNext()) {
            this.q((Map.Entry)iterator.next());
        }
    }
    
    public final void q(final Map.Entry entry) {
        zu0.a(entry.getKey());
        entry.getValue();
        throw null;
    }
    
    public void s(final b b, Object o) {
        if (b.i()) {
            if (!(o instanceof List)) {
                throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
            }
            final ArrayList list = new ArrayList();
            list.addAll((Collection)o);
            final Iterator iterator = list.iterator();
            while (iterator.hasNext()) {
                this.t(b, iterator.next());
            }
            o = list;
        }
        else {
            this.t(b, o);
        }
        this.a.r(b, o);
    }
    
    public final void t(final b b, final Object o) {
        if (m(b.b(), o)) {
            return;
        }
        throw new IllegalArgumentException(String.format("Wrong object type used with protocol message reflection.\nField number: %d, field java type: %s, value type: %s\n", b.getNumber(), b.b().getJavaType(), o.getClass().getName()));
    }
    
    public interface b extends Comparable
    {
        WireFormat.FieldType b();
        
        int getNumber();
        
        boolean i();
        
        boolean isPacked();
    }
}

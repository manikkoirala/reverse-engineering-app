// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.InvalidMarkException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.nio.ByteOrder;
import java.nio.ByteBuffer;

final class NioByteString extends LeafByteString
{
    private final ByteBuffer buffer;
    
    public NioByteString(final ByteBuffer byteBuffer) {
        t.b(byteBuffer, "buffer");
        this.buffer = byteBuffer.slice().order(ByteOrder.nativeOrder());
    }
    
    public static /* synthetic */ ByteBuffer access$000(final NioByteString nioByteString) {
        return nioByteString.buffer;
    }
    
    private void readObject(final ObjectInputStream objectInputStream) {
        throw new InvalidObjectException("NioByteString instances are not to be serialized directly");
    }
    
    private ByteBuffer slice(final int i, final int j) {
        if (i >= this.buffer.position() && j <= this.buffer.limit() && i <= j) {
            final ByteBuffer slice = this.buffer.slice();
            slice.position(i - this.buffer.position());
            slice.limit(j - this.buffer.position());
            return slice;
        }
        throw new IllegalArgumentException(String.format("Invalid indices [%d, %d]", i, j));
    }
    
    private Object writeReplace() {
        return ByteString.copyFrom(this.buffer.slice());
    }
    
    @Override
    public ByteBuffer asReadOnlyByteBuffer() {
        return this.buffer.asReadOnlyBuffer();
    }
    
    @Override
    public List<ByteBuffer> asReadOnlyByteBufferList() {
        return Collections.singletonList(this.asReadOnlyByteBuffer());
    }
    
    @Override
    public byte byteAt(final int n) {
        try {
            return this.buffer.get(n);
        }
        catch (final IndexOutOfBoundsException ex) {
            throw new ArrayIndexOutOfBoundsException(ex.getMessage());
        }
        catch (final ArrayIndexOutOfBoundsException ex2) {
            throw ex2;
        }
    }
    
    @Override
    public void copyTo(final ByteBuffer byteBuffer) {
        byteBuffer.put(this.buffer.slice());
    }
    
    @Override
    public void copyToInternal(final byte[] dst, final int newPosition, final int offset, final int length) {
        final ByteBuffer slice = this.buffer.slice();
        slice.position(newPosition);
        slice.get(dst, offset, length);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof ByteString)) {
            return false;
        }
        final ByteString byteString = (ByteString)o;
        if (this.size() != byteString.size()) {
            return false;
        }
        if (this.size() == 0) {
            return true;
        }
        if (o instanceof NioByteString) {
            return this.buffer.equals(((NioByteString)o).buffer);
        }
        if (o instanceof RopeByteString) {
            return o.equals(this);
        }
        return this.buffer.equals(byteString.asReadOnlyByteBuffer());
    }
    
    @Override
    public boolean equalsRange(final ByteString byteString, final int n, final int n2) {
        return this.substring(0, n2).equals(byteString.substring(n, n2 + n));
    }
    
    @Override
    public byte internalByteAt(final int n) {
        return this.byteAt(n);
    }
    
    @Override
    public boolean isValidUtf8() {
        return Utf8.r(this.buffer);
    }
    
    @Override
    public com.google.protobuf.g newCodedInput() {
        return com.google.protobuf.g.i(this.buffer, true);
    }
    
    @Override
    public InputStream newInput() {
        return new InputStream(this) {
            public final ByteBuffer a = NioByteString.access$000(b).slice();
            public final NioByteString b;
            
            @Override
            public int available() {
                return this.a.remaining();
            }
            
            @Override
            public void mark(final int n) {
                this.a.mark();
            }
            
            @Override
            public boolean markSupported() {
                return true;
            }
            
            @Override
            public int read() {
                if (!this.a.hasRemaining()) {
                    return -1;
                }
                return this.a.get() & 0xFF;
            }
            
            @Override
            public int read(final byte[] dst, final int offset, int min) {
                if (!this.a.hasRemaining()) {
                    return -1;
                }
                min = Math.min(min, this.a.remaining());
                this.a.get(dst, offset, min);
                return min;
            }
            
            @Override
            public void reset() {
                try {
                    this.a.reset();
                }
                catch (final InvalidMarkException cause) {
                    throw new IOException(cause);
                }
            }
        };
    }
    
    @Override
    public int partialHash(int n, final int n2, final int n3) {
        for (int i = n2; i < n2 + n3; ++i) {
            n = n * 31 + this.buffer.get(i);
        }
        return n;
    }
    
    @Override
    public int partialIsValidUtf8(final int n, final int n2, final int n3) {
        return Utf8.u(n, this.buffer, n2, n3 + n2);
    }
    
    @Override
    public int size() {
        return this.buffer.remaining();
    }
    
    @Override
    public ByteString substring(final int n, final int n2) {
        try {
            return new NioByteString(this.slice(n, n2));
        }
        catch (final IndexOutOfBoundsException ex) {
            throw new ArrayIndexOutOfBoundsException(ex.getMessage());
        }
        catch (final ArrayIndexOutOfBoundsException ex2) {
            throw ex2;
        }
    }
    
    @Override
    public String toStringInternal(final Charset charset) {
        byte[] bytes;
        int offset;
        int length;
        if (this.buffer.hasArray()) {
            bytes = this.buffer.array();
            offset = this.buffer.arrayOffset() + this.buffer.position();
            length = this.buffer.remaining();
        }
        else {
            bytes = this.toByteArray();
            length = bytes.length;
            offset = 0;
        }
        return new String(bytes, offset, length, charset);
    }
    
    @Override
    public void writeTo(final OutputStream outputStream) {
        outputStream.write(this.toByteArray());
    }
    
    @Override
    public void writeTo(final pd pd) {
        pd.a(this.buffer.slice());
    }
    
    @Override
    public void writeToInternal(final OutputStream outputStream, final int n, final int len) {
        if (this.buffer.hasArray()) {
            outputStream.write(this.buffer.array(), this.buffer.arrayOffset() + this.buffer.position() + n, len);
            return;
        }
        md.g(this.slice(n, len + n), outputStream);
    }
}

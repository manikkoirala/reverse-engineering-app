// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

public enum DescriptorProtos$FieldDescriptorProto$Type implements a
{
    private static final DescriptorProtos$FieldDescriptorProto$Type[] $VALUES;
    
    TYPE_BOOL(8);
    
    public static final int TYPE_BOOL_VALUE = 8;
    
    TYPE_BYTES(12);
    
    public static final int TYPE_BYTES_VALUE = 12;
    
    TYPE_DOUBLE(1);
    
    public static final int TYPE_DOUBLE_VALUE = 1;
    
    TYPE_ENUM(14);
    
    public static final int TYPE_ENUM_VALUE = 14;
    
    TYPE_FIXED32(7);
    
    public static final int TYPE_FIXED32_VALUE = 7;
    
    TYPE_FIXED64(6);
    
    public static final int TYPE_FIXED64_VALUE = 6;
    
    TYPE_FLOAT(2);
    
    public static final int TYPE_FLOAT_VALUE = 2;
    
    TYPE_GROUP(10);
    
    public static final int TYPE_GROUP_VALUE = 10;
    
    TYPE_INT32(5);
    
    public static final int TYPE_INT32_VALUE = 5;
    
    TYPE_INT64(3);
    
    public static final int TYPE_INT64_VALUE = 3;
    
    TYPE_MESSAGE(11);
    
    public static final int TYPE_MESSAGE_VALUE = 11;
    
    TYPE_SFIXED32(15);
    
    public static final int TYPE_SFIXED32_VALUE = 15;
    
    TYPE_SFIXED64(16);
    
    public static final int TYPE_SFIXED64_VALUE = 16;
    
    TYPE_SINT32(17);
    
    public static final int TYPE_SINT32_VALUE = 17;
    
    TYPE_SINT64(18);
    
    public static final int TYPE_SINT64_VALUE = 18;
    
    TYPE_STRING(9);
    
    public static final int TYPE_STRING_VALUE = 9;
    
    TYPE_UINT32(13);
    
    public static final int TYPE_UINT32_VALUE = 13;
    
    TYPE_UINT64(4);
    
    public static final int TYPE_UINT64_VALUE = 4;
    private static final t.b internalValueMap;
    private final int value;
    
    static {
        internalValueMap = new t.b() {};
    }
    
    private DescriptorProtos$FieldDescriptorProto$Type(final int value) {
        this.value = value;
    }
    
    public static DescriptorProtos$FieldDescriptorProto$Type forNumber(final int n) {
        switch (n) {
            default: {
                return null;
            }
            case 18: {
                return DescriptorProtos$FieldDescriptorProto$Type.TYPE_SINT64;
            }
            case 17: {
                return DescriptorProtos$FieldDescriptorProto$Type.TYPE_SINT32;
            }
            case 16: {
                return DescriptorProtos$FieldDescriptorProto$Type.TYPE_SFIXED64;
            }
            case 15: {
                return DescriptorProtos$FieldDescriptorProto$Type.TYPE_SFIXED32;
            }
            case 14: {
                return DescriptorProtos$FieldDescriptorProto$Type.TYPE_ENUM;
            }
            case 13: {
                return DescriptorProtos$FieldDescriptorProto$Type.TYPE_UINT32;
            }
            case 12: {
                return DescriptorProtos$FieldDescriptorProto$Type.TYPE_BYTES;
            }
            case 11: {
                return DescriptorProtos$FieldDescriptorProto$Type.TYPE_MESSAGE;
            }
            case 10: {
                return DescriptorProtos$FieldDescriptorProto$Type.TYPE_GROUP;
            }
            case 9: {
                return DescriptorProtos$FieldDescriptorProto$Type.TYPE_STRING;
            }
            case 8: {
                return DescriptorProtos$FieldDescriptorProto$Type.TYPE_BOOL;
            }
            case 7: {
                return DescriptorProtos$FieldDescriptorProto$Type.TYPE_FIXED32;
            }
            case 6: {
                return DescriptorProtos$FieldDescriptorProto$Type.TYPE_FIXED64;
            }
            case 5: {
                return DescriptorProtos$FieldDescriptorProto$Type.TYPE_INT32;
            }
            case 4: {
                return DescriptorProtos$FieldDescriptorProto$Type.TYPE_UINT64;
            }
            case 3: {
                return DescriptorProtos$FieldDescriptorProto$Type.TYPE_INT64;
            }
            case 2: {
                return DescriptorProtos$FieldDescriptorProto$Type.TYPE_FLOAT;
            }
            case 1: {
                return DescriptorProtos$FieldDescriptorProto$Type.TYPE_DOUBLE;
            }
        }
    }
    
    public static t.b internalGetValueMap() {
        return DescriptorProtos$FieldDescriptorProto$Type.internalValueMap;
    }
    
    public static c internalGetVerifier() {
        return b.a;
    }
    
    @Deprecated
    public static DescriptorProtos$FieldDescriptorProto$Type valueOf(final int n) {
        return forNumber(n);
    }
    
    @Override
    public final int getNumber() {
        return this.value;
    }
    
    public static final class b implements c
    {
        public static final c a;
        
        static {
            a = new b();
        }
        
        @Override
        public boolean a(final int n) {
            return DescriptorProtos$FieldDescriptorProto$Type.forNumber(n) != null;
        }
    }
}

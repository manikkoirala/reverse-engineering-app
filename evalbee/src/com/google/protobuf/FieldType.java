// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

import java.lang.reflect.Field;
import java.lang.reflect.TypeVariable;
import java.util.List;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public enum FieldType
{
    private static final FieldType[] $VALUES;
    
    BOOL(7, scalar, boolean1), 
    BOOL_LIST(25, vector, boolean1), 
    BOOL_LIST_PACKED(42, packed_VECTOR, boolean1), 
    BYTES(10, scalar, byte_STRING), 
    BYTES_LIST(28, vector, byte_STRING), 
    DOUBLE(0, scalar, double1), 
    DOUBLE_LIST(18, vector, double1), 
    DOUBLE_LIST_PACKED(35, packed_VECTOR, double1);
    
    private static final Type[] EMPTY_TYPES;
    
    ENUM(12, scalar, enum1), 
    ENUM_LIST(30, vector, enum1), 
    ENUM_LIST_PACKED(44, packed_VECTOR, enum1), 
    FIXED32(6, scalar, int1), 
    FIXED32_LIST(24, vector, int1), 
    FIXED32_LIST_PACKED(41, packed_VECTOR, int1), 
    FIXED64(5, scalar, long1), 
    FIXED64_LIST(23, vector, long1), 
    FIXED64_LIST_PACKED(40, packed_VECTOR, long1), 
    FLOAT(1, scalar, float1), 
    FLOAT_LIST(19, vector, float1), 
    FLOAT_LIST_PACKED(36, packed_VECTOR, float1), 
    GROUP(17, scalar, message), 
    GROUP_LIST(49, vector, message), 
    INT32(4, scalar, int1), 
    INT32_LIST(22, vector, int1), 
    INT32_LIST_PACKED(39, packed_VECTOR, int1), 
    INT64(2, scalar, long1), 
    INT64_LIST(20, vector, long1), 
    INT64_LIST_PACKED(37, packed_VECTOR, long1), 
    MAP(50, Collection.MAP, JavaType.VOID), 
    MESSAGE(9, scalar, message), 
    MESSAGE_LIST(27, vector, message), 
    SFIXED32(13, scalar, int1), 
    SFIXED32_LIST(31, vector, int1), 
    SFIXED32_LIST_PACKED(45, packed_VECTOR, int1), 
    SFIXED64(14, scalar, long1), 
    SFIXED64_LIST(32, vector, long1), 
    SFIXED64_LIST_PACKED(46, packed_VECTOR, long1), 
    SINT32(15, scalar, int1), 
    SINT32_LIST(33, vector, int1), 
    SINT32_LIST_PACKED(47, packed_VECTOR, int1), 
    SINT64(16, scalar, long1), 
    SINT64_LIST(34, vector, long1), 
    SINT64_LIST_PACKED(48, packed_VECTOR, long1), 
    STRING(8, scalar, string), 
    STRING_LIST(26, vector, string), 
    UINT32(11, scalar, int1), 
    UINT32_LIST(29, vector, int1), 
    UINT32_LIST_PACKED(43, packed_VECTOR, int1), 
    UINT64(3, scalar, long1), 
    UINT64_LIST(21, vector, long1), 
    UINT64_LIST_PACKED(38, packed_VECTOR, long1);
    
    private static final FieldType[] VALUES;
    private final Collection collection;
    private final Class<?> elementType;
    private final int id;
    private final JavaType javaType;
    private final boolean primitiveScalar;
    
    static {
        final Collection scalar = Collection.SCALAR;
        final JavaType double1 = JavaType.DOUBLE;
        final JavaType float1 = JavaType.FLOAT;
        final JavaType long1 = JavaType.LONG;
        final JavaType int1 = JavaType.INT;
        final JavaType boolean1 = JavaType.BOOLEAN;
        final JavaType string = JavaType.STRING;
        final JavaType message = JavaType.MESSAGE;
        final JavaType byte_STRING = JavaType.BYTE_STRING;
        final JavaType enum1 = JavaType.ENUM;
        final Collection vector = Collection.VECTOR;
        final Collection packed_VECTOR = Collection.PACKED_VECTOR;
        int i = 0;
        EMPTY_TYPES = new Type[0];
        final FieldType[] values = values();
        VALUES = new FieldType[values.length];
        while (i < values.length) {
            final FieldType fieldType36 = values[i];
            FieldType.VALUES[fieldType36.id] = fieldType36;
            ++i;
        }
    }
    
    private FieldType(final int id, final Collection collection, final JavaType javaType) {
        this.id = id;
        this.collection = collection;
        this.javaType = javaType;
        ordinal = FieldType$a.a[collection.ordinal()];
        boolean primitiveScalar = true;
        Class<?> boxedType;
        if (ordinal != 1 && ordinal != 2) {
            boxedType = null;
        }
        else {
            boxedType = javaType.getBoxedType();
        }
        this.elementType = boxedType;
        Label_0101: {
            if (collection == Collection.SCALAR) {
                ordinal = FieldType$a.b[javaType.ordinal()];
                if (ordinal != 1 && ordinal != 2 && ordinal != 3) {
                    break Label_0101;
                }
            }
            primitiveScalar = false;
        }
        this.primitiveScalar = primitiveScalar;
    }
    
    public static FieldType forId(final int n) {
        if (n >= 0) {
            final FieldType[] values = FieldType.VALUES;
            if (n < values.length) {
                return values[n];
            }
        }
        return null;
    }
    
    private static Type getGenericSuperList(final Class<?> clazz) {
        for (final Type type : clazz.getGenericInterfaces()) {
            if (type instanceof ParameterizedType && List.class.isAssignableFrom((Class<?>)((ParameterizedType)type).getRawType())) {
                return type;
            }
        }
        final Type genericSuperclass = clazz.getGenericSuperclass();
        if (genericSuperclass instanceof ParameterizedType && List.class.isAssignableFrom((Class<?>)((ParameterizedType)genericSuperclass).getRawType())) {
            return genericSuperclass;
        }
        return null;
    }
    
    private static Type getListParameter(Class<?> superclass, Type[] empty_TYPES) {
    Label_0000:
        while (true) {
            int i = 0;
            if (superclass != List.class) {
                final Type genericSuperList = getGenericSuperList(superclass);
                if (genericSuperList instanceof ParameterizedType) {
                    final ParameterizedType parameterizedType = (ParameterizedType)genericSuperList;
                    final Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
                Label_0167:
                    for (int j = 0; j < actualTypeArguments.length; ++j) {
                        final Type obj = actualTypeArguments[j];
                        if (obj instanceof TypeVariable) {
                            final TypeVariable[] typeParameters = superclass.getTypeParameters();
                            if (empty_TYPES.length == typeParameters.length) {
                                int k = 0;
                                while (true) {
                                    while (k < typeParameters.length) {
                                        if (obj == typeParameters[k]) {
                                            actualTypeArguments[j] = empty_TYPES[k];
                                            final boolean b = true;
                                            if (b) {
                                                continue Label_0167;
                                            }
                                            final StringBuilder sb = new StringBuilder();
                                            sb.append("Unable to find replacement for ");
                                            sb.append(obj);
                                            throw new RuntimeException(sb.toString());
                                        }
                                        else {
                                            ++k;
                                        }
                                    }
                                    final boolean b = false;
                                    continue;
                                }
                            }
                            throw new RuntimeException("Type array mismatch");
                        }
                    }
                    superclass = (Class)parameterizedType.getRawType();
                    empty_TYPES = actualTypeArguments;
                }
                else {
                    empty_TYPES = FieldType.EMPTY_TYPES;
                    for (Class[] interfaces = superclass.getInterfaces(); i < interfaces.length; ++i) {
                        final Class clazz = interfaces[i];
                        if (List.class.isAssignableFrom(clazz)) {
                            superclass = clazz;
                            continue Label_0000;
                        }
                    }
                    superclass = superclass.getSuperclass();
                }
            }
            else {
                if (empty_TYPES.length == 1) {
                    return empty_TYPES[0];
                }
                throw new RuntimeException("Unable to identify parameter type for List<T>");
            }
        }
    }
    
    private boolean isValidForList(final Field field) {
        final Class<?> type = field.getType();
        if (!this.javaType.getType().isAssignableFrom(type)) {
            return false;
        }
        Type[] array = FieldType.EMPTY_TYPES;
        if (field.getGenericType() instanceof ParameterizedType) {
            array = ((ParameterizedType)field.getGenericType()).getActualTypeArguments();
        }
        final Type listParameter = getListParameter(type, array);
        return !(listParameter instanceof Class) || this.elementType.isAssignableFrom((Class<?>)listParameter);
    }
    
    public JavaType getJavaType() {
        return this.javaType;
    }
    
    public int id() {
        return this.id;
    }
    
    public boolean isList() {
        return this.collection.isList();
    }
    
    public boolean isMap() {
        return this.collection == Collection.MAP;
    }
    
    public boolean isPacked() {
        return Collection.PACKED_VECTOR.equals(this.collection);
    }
    
    public boolean isPrimitiveScalar() {
        return this.primitiveScalar;
    }
    
    public boolean isScalar() {
        return this.collection == Collection.SCALAR;
    }
    
    public boolean isValidForField(final Field field) {
        if (Collection.VECTOR.equals(this.collection)) {
            return this.isValidForList(field);
        }
        return this.javaType.getType().isAssignableFrom(field.getType());
    }
    
    public enum Collection
    {
        private static final Collection[] $VALUES;
        
        MAP(false), 
        PACKED_VECTOR(true), 
        SCALAR(false), 
        VECTOR(true);
        
        private final boolean isList;
        
        private Collection(final boolean isList) {
            this.isList = isList;
        }
        
        public boolean isList() {
            return this.isList;
        }
    }
}

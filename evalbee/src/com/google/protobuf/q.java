// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

public class q implements vv0
{
    public static final q a;
    
    static {
        a = new q();
    }
    
    public static q c() {
        return q.a;
    }
    
    @Override
    public tv0 a(final Class clazz) {
        if (GeneratedMessageLite.class.isAssignableFrom(clazz)) {
            try {
                return (tv0)GeneratedMessageLite.B(clazz.asSubclass(GeneratedMessageLite.class)).o();
            }
            catch (final Exception cause) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unable to get message info for ");
                sb.append(clazz.getName());
                throw new RuntimeException(sb.toString(), cause);
            }
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Unsupported message type: ");
        sb2.append(clazz.getName());
        throw new IllegalArgumentException(sb2.toString());
    }
    
    @Override
    public boolean b(final Class clazz) {
        return GeneratedMessageLite.class.isAssignableFrom(clazz);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

public final class j0 extends GeneratedMessageLite implements xv0
{
    private static final j0 DEFAULT_INSTANCE;
    public static final int NANOS_FIELD_NUMBER = 2;
    private static volatile b31 PARSER;
    public static final int SECONDS_FIELD_NUMBER = 1;
    private int nanos_;
    private long seconds_;
    
    static {
        GeneratedMessageLite.V(j0.class, DEFAULT_INSTANCE = new j0());
    }
    
    public static /* synthetic */ j0 Z() {
        return j0.DEFAULT_INSTANCE;
    }
    
    public static j0 c0() {
        return j0.DEFAULT_INSTANCE;
    }
    
    public static b f0() {
        return (b)j0.DEFAULT_INSTANCE.u();
    }
    
    public int d0() {
        return this.nanos_;
    }
    
    public long e0() {
        return this.seconds_;
    }
    
    public final void g0(final int nanos_) {
        this.nanos_ = nanos_;
    }
    
    public final void h0(final long seconds_) {
        this.seconds_ = seconds_;
    }
    
    @Override
    public final Object y(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (j0$a.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final b31 parser;
                if ((parser = j0.PARSER) == null) {
                    synchronized (j0.class) {
                        if (j0.PARSER == null) {
                            j0.PARSER = new GeneratedMessageLite.b(j0.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return j0.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.N(j0.DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0002\u0002\u0004", new Object[] { "seconds_", "nanos_" });
            }
            case 2: {
                return new b((j0$a)null);
            }
            case 1: {
                return new j0();
            }
        }
    }
    
    public static final class b extends a implements xv0
    {
        public b() {
            super(j0.Z());
        }
        
        public b A(final int n) {
            ((a)this).s();
            ((j0)super.b).g0(n);
            return this;
        }
        
        public b B(final long n) {
            ((a)this).s();
            ((j0)super.b).h0(n);
            return this;
        }
    }
}

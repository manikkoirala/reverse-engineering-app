// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

import java.util.Iterator;
import java.util.RandomAccess;
import java.util.List;

public abstract class h0
{
    public static final Class a;
    public static final k0 b;
    public static final k0 c;
    public static final k0 d;
    
    static {
        a = A();
        b = B(false);
        c = B(true);
        d = new m0();
    }
    
    public static Class A() {
        try {
            return Class.forName("com.google.protobuf.GeneratedMessageV3");
        }
        finally {
            return null;
        }
    }
    
    public static k0 B(final boolean b) {
        try {
            final Class c = C();
            if (c == null) {}
            return c.getConstructor(Boolean.TYPE).newInstance(b);
        }
        finally {
            return null;
        }
    }
    
    public static Class C() {
        try {
            return Class.forName("com.google.protobuf.UnknownFieldSetSchema");
        }
        finally {
            return null;
        }
    }
    
    public static void D(final m m, final Object o, final Object o2) {
        final o c = m.c(o2);
        if (!c.j()) {
            m.d(o).p(c);
        }
    }
    
    public static void E(final y y, final Object o, final Object o2, final long n) {
        d12.W(o, n, y.a(d12.G(o, n), d12.G(o2, n)));
    }
    
    public static void F(final k0 k0, final Object o, final Object o2) {
        k0.p(o, k0.k(k0.g(o), k0.g(o2)));
    }
    
    public static k0 G() {
        return h0.b;
    }
    
    public static k0 H() {
        return h0.c;
    }
    
    public static void I(final Class clazz) {
        if (!GeneratedMessageLite.class.isAssignableFrom(clazz)) {
            final Class a = h0.a;
            if (a != null) {
                if (!a.isAssignableFrom(clazz)) {
                    throw new IllegalArgumentException("Message classes must extend GeneratedMessageV3 or GeneratedMessageLite");
                }
            }
        }
    }
    
    public static boolean J(final Object o, final Object obj) {
        return o == obj || (o != null && o.equals(obj));
    }
    
    public static Object K(final Object o, final int n, final int n2, final Object o2, final k0 k0) {
        Object f = o2;
        if (o2 == null) {
            f = k0.f(o);
        }
        k0.e(f, n, n2);
        return f;
    }
    
    public static k0 L() {
        return h0.d;
    }
    
    public static void M(final int n, final List list, final Writer writer, final boolean b) {
        if (list != null && !list.isEmpty()) {
            writer.E(n, list, b);
        }
    }
    
    public static void N(final int n, final List list, final Writer writer) {
        if (list != null && !list.isEmpty()) {
            writer.u(n, list);
        }
    }
    
    public static void O(final int n, final List list, final Writer writer, final boolean b) {
        if (list != null && !list.isEmpty()) {
            writer.t(n, list, b);
        }
    }
    
    public static void P(final int n, final List list, final Writer writer, final boolean b) {
        if (list != null && !list.isEmpty()) {
            writer.y(n, list, b);
        }
    }
    
    public static void Q(final int n, final List list, final Writer writer, final boolean b) {
        if (list != null && !list.isEmpty()) {
            writer.h(n, list, b);
        }
    }
    
    public static void R(final int n, final List list, final Writer writer, final boolean b) {
        if (list != null && !list.isEmpty()) {
            writer.q(n, list, b);
        }
    }
    
    public static void S(final int n, final List list, final Writer writer, final boolean b) {
        if (list != null && !list.isEmpty()) {
            writer.a(n, list, b);
        }
    }
    
    public static void T(final int n, final List list, final Writer writer, final g0 g0) {
        if (list != null && !list.isEmpty()) {
            writer.N(n, list, g0);
        }
    }
    
    public static void U(final int n, final List list, final Writer writer, final boolean b) {
        if (list != null && !list.isEmpty()) {
            writer.f(n, list, b);
        }
    }
    
    public static void V(final int n, final List list, final Writer writer, final boolean b) {
        if (list != null && !list.isEmpty()) {
            writer.H(n, list, b);
        }
    }
    
    public static void W(final int n, final List list, final Writer writer, final g0 g0) {
        if (list != null && !list.isEmpty()) {
            writer.L(n, list, g0);
        }
    }
    
    public static void X(final int n, final List list, final Writer writer, final boolean b) {
        if (list != null && !list.isEmpty()) {
            writer.D(n, list, b);
        }
    }
    
    public static void Y(final int n, final List list, final Writer writer, final boolean b) {
        if (list != null && !list.isEmpty()) {
            writer.A(n, list, b);
        }
    }
    
    public static void Z(final int n, final List list, final Writer writer, final boolean b) {
        if (list != null && !list.isEmpty()) {
            writer.s(n, list, b);
        }
    }
    
    public static int a(final int n, final List list, final boolean b) {
        final int size = list.size();
        if (size == 0) {
            return 0;
        }
        if (b) {
            return CodedOutputStream.Q(n) + CodedOutputStream.A(size);
        }
        return size * CodedOutputStream.e(n, true);
    }
    
    public static void a0(final int n, final List list, final Writer writer, final boolean b) {
        if (list != null && !list.isEmpty()) {
            writer.x(n, list, b);
        }
    }
    
    public static int b(final List list) {
        return list.size();
    }
    
    public static void b0(final int n, final List list, final Writer writer) {
        if (list != null && !list.isEmpty()) {
            writer.v(n, list);
        }
    }
    
    public static int c(int i, final List list) {
        final int size = list.size();
        final int n = 0;
        if (size == 0) {
            return 0;
        }
        final int n2 = size * CodedOutputStream.Q(i);
        i = n;
        int n3 = n2;
        while (i < list.size()) {
            n3 += CodedOutputStream.i(list.get(i));
            ++i;
        }
        return n3;
    }
    
    public static void c0(final int n, final List list, final Writer writer, final boolean b) {
        if (list != null && !list.isEmpty()) {
            writer.i(n, list, b);
        }
    }
    
    public static int d(int q, final List list, final boolean b) {
        final int size = list.size();
        if (size == 0) {
            return 0;
        }
        final int e = e(list);
        q = CodedOutputStream.Q(q);
        if (b) {
            return q + CodedOutputStream.A(e);
        }
        return e + size * q;
    }
    
    public static void d0(final int n, final List list, final Writer writer, final boolean b) {
        if (list != null && !list.isEmpty()) {
            writer.l(n, list, b);
        }
    }
    
    public static int e(final List list) {
        final int size = list.size();
        int n = 0;
        final int n2 = 0;
        if (size == 0) {
            return 0;
        }
        int n5;
        if (list instanceof s) {
            final s s = (s)list;
            int n3 = 0;
            int n4 = n2;
            while (true) {
                n5 = n3;
                if (n4 >= size) {
                    break;
                }
                n3 += CodedOutputStream.m(s.o(n4));
                ++n4;
            }
        }
        else {
            int n6 = 0;
            while (true) {
                n5 = n6;
                if (n >= size) {
                    break;
                }
                n6 += CodedOutputStream.m(list.get(n));
                ++n;
            }
        }
        return n5;
    }
    
    public static int f(final int n, final List list, final boolean b) {
        final int size = list.size();
        if (size == 0) {
            return 0;
        }
        if (b) {
            return CodedOutputStream.Q(n) + CodedOutputStream.A(size * 4);
        }
        return size * CodedOutputStream.n(n, 0);
    }
    
    public static int g(final List list) {
        return list.size() * 4;
    }
    
    public static int h(final int n, final List list, final boolean b) {
        final int size = list.size();
        if (size == 0) {
            return 0;
        }
        if (b) {
            return CodedOutputStream.Q(n) + CodedOutputStream.A(size * 8);
        }
        return size * CodedOutputStream.p(n, 0L);
    }
    
    public static int i(final List list) {
        return list.size() * 8;
    }
    
    public static int j(final int n, final List list, final g0 g0) {
        final int size = list.size();
        int i = 0;
        if (size == 0) {
            return 0;
        }
        int n2 = 0;
        while (i < size) {
            n2 += CodedOutputStream.t(n, list.get(i), g0);
            ++i;
        }
        return n2;
    }
    
    public static int k(int q, final List list, final boolean b) {
        final int size = list.size();
        if (size == 0) {
            return 0;
        }
        final int l = l(list);
        q = CodedOutputStream.Q(q);
        if (b) {
            return q + CodedOutputStream.A(l);
        }
        return l + size * q;
    }
    
    public static int l(final List list) {
        final int size = list.size();
        final int n = 0;
        int n2 = 0;
        if (size == 0) {
            return 0;
        }
        int n4;
        if (list instanceof s) {
            final s s = (s)list;
            int n3 = 0;
            while (true) {
                n4 = n3;
                if (n2 >= size) {
                    break;
                }
                n3 += CodedOutputStream.x(s.o(n2));
                ++n2;
            }
        }
        else {
            int n5 = 0;
            int n6 = n;
            while (true) {
                n4 = n5;
                if (n6 >= size) {
                    break;
                }
                n5 += CodedOutputStream.x((int)list.get(n6));
                ++n6;
            }
        }
        return n4;
    }
    
    public static int m(final int n, final List list, final boolean b) {
        if (list.size() == 0) {
            return 0;
        }
        final int n2 = n(list);
        if (b) {
            return CodedOutputStream.Q(n) + CodedOutputStream.A(n2);
        }
        return n2 + list.size() * CodedOutputStream.Q(n);
    }
    
    public static int n(final List list) {
        final int size = list.size();
        final int n = 0;
        int n2 = 0;
        if (size == 0) {
            return 0;
        }
        int n4;
        if (list instanceof w) {
            final w w = (w)list;
            int n3 = 0;
            while (true) {
                n4 = n3;
                if (n2 >= size) {
                    break;
                }
                n3 += CodedOutputStream.z(w.c(n2));
                ++n2;
            }
        }
        else {
            int n5 = 0;
            int n6 = n;
            while (true) {
                n4 = n5;
                if (n6 >= size) {
                    break;
                }
                n5 += CodedOutputStream.z((long)list.get(n6));
                ++n6;
            }
        }
        return n4;
    }
    
    public static int o(final int n, final Object o, final g0 g0) {
        return CodedOutputStream.B(n, (a0)o, g0);
    }
    
    public static int p(int i, final List list, final g0 g0) {
        final int size = list.size();
        final int n = 0;
        if (size == 0) {
            return 0;
        }
        int n2 = CodedOutputStream.Q(i) * size;
        for (i = n; i < size; ++i) {
            n2 += CodedOutputStream.D(list.get(i), g0);
        }
        return n2;
    }
    
    public static int q(int q, final List list, final boolean b) {
        final int size = list.size();
        if (size == 0) {
            return 0;
        }
        final int r = r(list);
        q = CodedOutputStream.Q(q);
        if (b) {
            return q + CodedOutputStream.A(r);
        }
        return r + size * q;
    }
    
    public static int r(final List list) {
        final int size = list.size();
        final int n = 0;
        int n2 = 0;
        if (size == 0) {
            return 0;
        }
        int n4;
        if (list instanceof s) {
            final s s = (s)list;
            int n3 = 0;
            while (true) {
                n4 = n3;
                if (n2 >= size) {
                    break;
                }
                n3 += CodedOutputStream.L(s.o(n2));
                ++n2;
            }
        }
        else {
            int n5 = 0;
            int n6 = n;
            while (true) {
                n4 = n5;
                if (n6 >= size) {
                    break;
                }
                n5 += CodedOutputStream.L((int)list.get(n6));
                ++n6;
            }
        }
        return n4;
    }
    
    public static int s(int q, final List list, final boolean b) {
        final int size = list.size();
        if (size == 0) {
            return 0;
        }
        final int t = t(list);
        q = CodedOutputStream.Q(q);
        if (b) {
            return q + CodedOutputStream.A(t);
        }
        return t + size * q;
    }
    
    public static int t(final List list) {
        final int size = list.size();
        final int n = 0;
        int n2 = 0;
        if (size == 0) {
            return 0;
        }
        int n4;
        if (list instanceof w) {
            final w w = (w)list;
            int n3 = 0;
            while (true) {
                n4 = n3;
                if (n2 >= size) {
                    break;
                }
                n3 += CodedOutputStream.N(w.c(n2));
                ++n2;
            }
        }
        else {
            int n5 = 0;
            int n6 = n;
            while (true) {
                n4 = n5;
                if (n6 >= size) {
                    break;
                }
                n5 += CodedOutputStream.N((long)list.get(n6));
                ++n6;
            }
        }
        return n4;
    }
    
    public static int u(int n, final List list) {
        final int size = list.size();
        int n2 = 0;
        final int n3 = 0;
        if (size == 0) {
            return 0;
        }
        final int n4 = n = CodedOutputStream.Q(n) * size;
        int n6;
        if (list instanceof fj0) {
            final fj0 fj0 = (fj0)list;
            n = n4;
            int n5 = n3;
            while (true) {
                n6 = n;
                if (n5 >= size) {
                    break;
                }
                final Object k = fj0.k(n5);
                int n7;
                if (k instanceof ByteString) {
                    n7 = CodedOutputStream.i((ByteString)k);
                }
                else {
                    n7 = CodedOutputStream.P((String)k);
                }
                n += n7;
                ++n5;
            }
        }
        else {
            while (true) {
                n6 = n;
                if (n2 >= size) {
                    break;
                }
                final Object value = list.get(n2);
                int n8;
                if (value instanceof ByteString) {
                    n8 = CodedOutputStream.i((ByteString)value);
                }
                else {
                    n8 = CodedOutputStream.P((String)value);
                }
                n += n8;
                ++n2;
            }
        }
        return n6;
    }
    
    public static int v(int q, final List list, final boolean b) {
        final int size = list.size();
        if (size == 0) {
            return 0;
        }
        final int w = w(list);
        q = CodedOutputStream.Q(q);
        if (b) {
            return q + CodedOutputStream.A(w);
        }
        return w + size * q;
    }
    
    public static int w(final List list) {
        final int size = list.size();
        int n = 0;
        final int n2 = 0;
        if (size == 0) {
            return 0;
        }
        int n5;
        if (list instanceof s) {
            final s s = (s)list;
            int n3 = 0;
            int n4 = n2;
            while (true) {
                n5 = n3;
                if (n4 >= size) {
                    break;
                }
                n3 += CodedOutputStream.S(s.o(n4));
                ++n4;
            }
        }
        else {
            int n6 = 0;
            while (true) {
                n5 = n6;
                if (n >= size) {
                    break;
                }
                n6 += CodedOutputStream.S(list.get(n));
                ++n;
            }
        }
        return n5;
    }
    
    public static int x(int q, final List list, final boolean b) {
        final int size = list.size();
        if (size == 0) {
            return 0;
        }
        final int y = y(list);
        q = CodedOutputStream.Q(q);
        if (b) {
            return q + CodedOutputStream.A(y);
        }
        return y + size * q;
    }
    
    public static int y(final List list) {
        final int size = list.size();
        int n = 0;
        final int n2 = 0;
        if (size == 0) {
            return 0;
        }
        int n5;
        if (list instanceof w) {
            final w w = (w)list;
            int n3 = 0;
            int n4 = n2;
            while (true) {
                n5 = n3;
                if (n4 >= size) {
                    break;
                }
                n3 += CodedOutputStream.U(w.c(n4));
                ++n4;
            }
        }
        else {
            int n6 = 0;
            while (true) {
                n5 = n6;
                if (n >= size) {
                    break;
                }
                n6 += CodedOutputStream.U(list.get(n));
                ++n;
            }
        }
        return n5;
    }
    
    public static Object z(final Object o, final int n, final List list, final t.c c, Object o2, final k0 k0) {
        if (c == null) {
            return o2;
        }
        Object o3;
        if (list instanceof RandomAccess) {
            final int size = list.size();
            int i = 0;
            int n2 = 0;
            while (i < size) {
                final int intValue = list.get(i);
                if (c.a(intValue)) {
                    if (i != n2) {
                        list.set(n2, intValue);
                    }
                    ++n2;
                }
                else {
                    o2 = K(o, n, intValue, o2, k0);
                }
                ++i;
            }
            o3 = o2;
            if (n2 != size) {
                list.subList(n2, size).clear();
                o3 = o2;
            }
        }
        else {
            final Iterator iterator = list.iterator();
            while (true) {
                o3 = o2;
                if (!iterator.hasNext()) {
                    break;
                }
                final int intValue2 = (int)iterator.next();
                if (c.a(intValue2)) {
                    continue;
                }
                o2 = K(o, n, intValue2, o2, k0);
                iterator.remove();
            }
        }
        return o3;
    }
}

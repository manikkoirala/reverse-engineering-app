// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

public class m0 extends k0
{
    public l0 A(final Object o) {
        return ((GeneratedMessageLite)o).unknownFields;
    }
    
    public int B(final l0 l0) {
        return l0.d();
    }
    
    public int C(final l0 l0) {
        return l0.e();
    }
    
    public l0 D(final l0 l0, final l0 l2) {
        if (l0.c().equals(l2)) {
            return l0;
        }
        if (l0.c().equals(l0)) {
            return l0.j(l0, l2);
        }
        return l0.i(l2);
    }
    
    public l0 E() {
        return l0.k();
    }
    
    public void F(final Object o, final l0 l0) {
        this.G(o, l0);
    }
    
    public void G(final Object o, final l0 unknownFields) {
        ((GeneratedMessageLite)o).unknownFields = unknownFields;
    }
    
    public l0 H(final l0 l0) {
        l0.h();
        return l0;
    }
    
    public void I(final l0 l0, final Writer writer) {
        l0.p(writer);
    }
    
    public void J(final l0 l0, final Writer writer) {
        l0.r(writer);
    }
    
    @Override
    public void j(final Object o) {
        this.A(o).h();
    }
    
    @Override
    public boolean q(final f0 f0) {
        return false;
    }
    
    public void u(final l0 l0, final int n, final int i) {
        l0.n(WireFormat.c(n, 5), i);
    }
    
    public void v(final l0 l0, final int n, final long i) {
        l0.n(WireFormat.c(n, 1), i);
    }
    
    public void w(final l0 l0, final int n, final l0 l2) {
        l0.n(WireFormat.c(n, 3), l2);
    }
    
    public void x(final l0 l0, final int n, final ByteString byteString) {
        l0.n(WireFormat.c(n, 2), byteString);
    }
    
    public void y(final l0 l0, final int n, final long i) {
        l0.n(WireFormat.c(n, 0), i);
    }
    
    public l0 z(final Object o) {
        l0 l0;
        if ((l0 = this.A(o)) == com.google.protobuf.l0.c()) {
            l0 = com.google.protobuf.l0.k();
            this.G(o, l0);
        }
        return l0;
    }
}

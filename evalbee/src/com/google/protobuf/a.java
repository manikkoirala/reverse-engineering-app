// 
// Decompiled by Procyon v0.6.0
// 

package com.google.protobuf;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Collection;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

public abstract class a implements a0
{
    protected int memoizedHashCode;
    
    public a() {
        this.memoizedHashCode = 0;
    }
    
    public static void j(final Iterable iterable, final List list) {
        a.j(iterable, list);
    }
    
    private String l(final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Serializing ");
        sb.append(this.getClass().getName());
        sb.append(" to a ");
        sb.append(str);
        sb.append(" threw an IOException (should never happen).");
        return sb.toString();
    }
    
    @Override
    public void e(final OutputStream outputStream) {
        final CodedOutputStream a0 = CodedOutputStream.a0(outputStream, CodedOutputStream.E(this.b()));
        this.f(a0);
        a0.X();
    }
    
    public abstract int k(final g0 p0);
    
    public UninitializedMessageException m() {
        return new UninitializedMessageException(this);
    }
    
    @Override
    public byte[] toByteArray() {
        try {
            final byte[] array = new byte[this.b()];
            final CodedOutputStream b0 = CodedOutputStream.b0(array);
            this.f(b0);
            b0.d();
            return array;
        }
        catch (final IOException cause) {
            throw new RuntimeException(this.l("byte array"), cause);
        }
    }
    
    public abstract static class a implements a0.a
    {
        public static void j(final Iterable iterable, final List list) {
            t.a(iterable);
            if (iterable instanceof fj0) {
                final List f = ((fj0)iterable).f();
                final fj0 fj0 = (fj0)list;
                final int size = list.size();
                for (final Object next : f) {
                    if (next == null) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Element at index ");
                        sb.append(fj0.size() - size);
                        sb.append(" is null.");
                        final String string = sb.toString();
                        for (int i = fj0.size() - 1; i >= size; --i) {
                            fj0.remove(i);
                        }
                        throw new NullPointerException(string);
                    }
                    if (next instanceof ByteString) {
                        fj0.q((ByteString)next);
                    }
                    else {
                        fj0.add((String)next);
                    }
                }
            }
            else if (iterable instanceof a81) {
                list.addAll((Collection<?>)iterable);
            }
            else {
                k(iterable, list);
            }
        }
        
        public static void k(final Iterable iterable, final List list) {
            if (list instanceof ArrayList && iterable instanceof Collection) {
                ((ArrayList)list).ensureCapacity(list.size() + ((Collection)iterable).size());
            }
            final int size = list.size();
            for (final Object next : iterable) {
                if (next == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Element at index ");
                    sb.append(list.size() - size);
                    sb.append(" is null.");
                    final String string = sb.toString();
                    for (int i = list.size() - 1; i >= size; --i) {
                        list.remove(i);
                    }
                    throw new NullPointerException(string);
                }
                list.add(next);
            }
        }
        
        public static UninitializedMessageException n(final a0 a0) {
            return new UninitializedMessageException(a0);
        }
        
        public a l(final byte[] array) {
            return this.m(array, 0, array.length);
        }
        
        public abstract a m(final byte[] p0, final int p1, final int p2);
    }
}

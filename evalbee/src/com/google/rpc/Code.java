// 
// Decompiled by Procyon v0.6.0
// 

package com.google.rpc;

import com.google.protobuf.t;

public enum Code implements a
{
    private static final Code[] $VALUES;
    
    ABORTED(10);
    
    public static final int ABORTED_VALUE = 10;
    
    ALREADY_EXISTS(6);
    
    public static final int ALREADY_EXISTS_VALUE = 6;
    
    CANCELLED(1);
    
    public static final int CANCELLED_VALUE = 1;
    
    DATA_LOSS(15);
    
    public static final int DATA_LOSS_VALUE = 15;
    
    DEADLINE_EXCEEDED(4);
    
    public static final int DEADLINE_EXCEEDED_VALUE = 4;
    
    FAILED_PRECONDITION(9);
    
    public static final int FAILED_PRECONDITION_VALUE = 9;
    
    INTERNAL(13);
    
    public static final int INTERNAL_VALUE = 13;
    
    INVALID_ARGUMENT(3);
    
    public static final int INVALID_ARGUMENT_VALUE = 3;
    
    NOT_FOUND(5);
    
    public static final int NOT_FOUND_VALUE = 5;
    
    OK(0);
    
    public static final int OK_VALUE = 0;
    
    OUT_OF_RANGE(11);
    
    public static final int OUT_OF_RANGE_VALUE = 11;
    
    PERMISSION_DENIED(7);
    
    public static final int PERMISSION_DENIED_VALUE = 7;
    
    RESOURCE_EXHAUSTED(8);
    
    public static final int RESOURCE_EXHAUSTED_VALUE = 8;
    
    UNAUTHENTICATED(16);
    
    public static final int UNAUTHENTICATED_VALUE = 16;
    
    UNAVAILABLE(14);
    
    public static final int UNAVAILABLE_VALUE = 14;
    
    UNIMPLEMENTED(12);
    
    public static final int UNIMPLEMENTED_VALUE = 12;
    
    UNKNOWN(2);
    
    public static final int UNKNOWN_VALUE = 2;
    
    UNRECOGNIZED(-1);
    
    private static final t.b internalValueMap;
    private final int value;
    
    static {
        internalValueMap = new t.b() {};
    }
    
    private Code(final int value) {
        this.value = value;
    }
    
    public static Code forNumber(final int n) {
        switch (n) {
            default: {
                return null;
            }
            case 16: {
                return Code.UNAUTHENTICATED;
            }
            case 15: {
                return Code.DATA_LOSS;
            }
            case 14: {
                return Code.UNAVAILABLE;
            }
            case 13: {
                return Code.INTERNAL;
            }
            case 12: {
                return Code.UNIMPLEMENTED;
            }
            case 11: {
                return Code.OUT_OF_RANGE;
            }
            case 10: {
                return Code.ABORTED;
            }
            case 9: {
                return Code.FAILED_PRECONDITION;
            }
            case 8: {
                return Code.RESOURCE_EXHAUSTED;
            }
            case 7: {
                return Code.PERMISSION_DENIED;
            }
            case 6: {
                return Code.ALREADY_EXISTS;
            }
            case 5: {
                return Code.NOT_FOUND;
            }
            case 4: {
                return Code.DEADLINE_EXCEEDED;
            }
            case 3: {
                return Code.INVALID_ARGUMENT;
            }
            case 2: {
                return Code.UNKNOWN;
            }
            case 1: {
                return Code.CANCELLED;
            }
            case 0: {
                return Code.OK;
            }
        }
    }
    
    public static t.b internalGetValueMap() {
        return Code.internalValueMap;
    }
    
    public static c internalGetVerifier() {
        return b.a;
    }
    
    @Deprecated
    public static Code valueOf(final int n) {
        return forNumber(n);
    }
    
    @Override
    public final int getNumber() {
        if (this != Code.UNRECOGNIZED) {
            return this.value;
        }
        throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
    }
    
    public static final class b implements c
    {
        public static final c a;
        
        static {
            a = new b();
        }
        
        @Override
        public boolean a(final int n) {
            return Code.forNumber(n) != null;
        }
    }
}

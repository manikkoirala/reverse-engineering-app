import com.google.common.collect.j;
import java.util.Set;
import java.util.Collection;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public interface rx0
{
    Map asMap();
    
    void clear();
    
    boolean containsEntry(final Object p0, final Object p1);
    
    boolean containsKey(final Object p0);
    
    boolean containsValue(final Object p0);
    
    Collection entries();
    
    boolean equals(final Object p0);
    
    Collection get(final Object p0);
    
    int hashCode();
    
    boolean isEmpty();
    
    Set keySet();
    
    j keys();
    
    boolean put(final Object p0, final Object p1);
    
    boolean putAll(final Object p0, final Iterable p1);
    
    boolean putAll(final rx0 p0);
    
    boolean remove(final Object p0, final Object p1);
    
    Collection removeAll(final Object p0);
    
    Collection replaceValues(final Object p0, final Iterable p1);
    
    int size();
    
    Collection values();
}

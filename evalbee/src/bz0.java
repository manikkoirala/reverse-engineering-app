import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.ConnectivityManager;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class bz0
{
    public static final String a;
    
    static {
        final String i = xl0.i("NetworkStateTracker");
        fg0.d((Object)i, "tagWithPrefix(\"NetworkStateTracker\")");
        a = i;
    }
    
    public static final tk a(final Context context, final hu1 hu1) {
        fg0.e((Object)context, "context");
        fg0.e((Object)hu1, "taskExecutor");
        return new az0(context, hu1);
    }
    
    public static final zy0 c(final ConnectivityManager connectivityManager) {
        fg0.e((Object)connectivityManager, "<this>");
        final NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        boolean b = true;
        final boolean b2 = activeNetworkInfo != null && activeNetworkInfo.isConnected();
        final boolean d = d(connectivityManager);
        final boolean a = mk.a(connectivityManager);
        if (activeNetworkInfo == null || activeNetworkInfo.isRoaming()) {
            b = false;
        }
        return new zy0(b2, d, a, b);
    }
    
    public static final boolean d(final ConnectivityManager connectivityManager) {
        fg0.e((Object)connectivityManager, "<this>");
        final boolean b = false;
        boolean b2;
        try {
            final NetworkCapabilities a = ry0.a(connectivityManager, sy0.a(connectivityManager));
            b2 = b;
            if (a != null) {
                b2 = ry0.b(a, 16);
            }
        }
        catch (final SecurityException ex) {
            xl0.e().d(bz0.a, "Unable to validate active network", ex);
            b2 = b;
        }
        return b2;
    }
}

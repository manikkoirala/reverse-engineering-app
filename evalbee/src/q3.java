import android.widget.AdapterView;
import android.graphics.Color;
import android.widget.ListView;
import android.view.View$OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.LayoutInflater;
import com.ekodroid.omrevaluator.database.StudentDataModel;
import android.view.ViewGroup;
import android.view.View;
import android.content.Context;
import java.util.ArrayList;
import android.util.SparseBooleanArray;
import android.widget.BaseAdapter;

// 
// Decompiled by Procyon v0.6.0
// 

public class q3 extends BaseAdapter
{
    public SparseBooleanArray a;
    public ArrayList b;
    public Context c;
    
    public q3(final ArrayList b, final Context c) {
        this.a = new SparseBooleanArray();
        this.b = b;
        this.c = c;
    }
    
    public SparseBooleanArray a() {
        return this.a;
    }
    
    public void b() {
        this.a = new SparseBooleanArray();
        this.notifyDataSetChanged();
    }
    
    public final void c(final int n, final boolean b) {
        if (b) {
            this.a.put(n, b);
        }
        else {
            this.a.delete(n);
        }
        this.notifyDataSetChanged();
    }
    
    public void d(final int n) {
        this.c(n, this.a.get(n) ^ true);
    }
    
    public int getCount() {
        return this.b.size();
    }
    
    public Object getItem(final int index) {
        return this.b.get(index);
    }
    
    public long getItemId(final int n) {
        return n;
    }
    
    public View getView(int n, View inflate, final ViewGroup viewGroup) {
        final StudentDataModel studentDataModel = this.b.get(n);
        inflate = ((LayoutInflater)this.c.getSystemService("layout_inflater")).inflate(2131493031, (ViewGroup)null);
        final TextView textView = (TextView)inflate.findViewById(2131297297);
        final TextView textView2 = (TextView)inflate.findViewById(2131297328);
        final TextView textView3 = (TextView)inflate.findViewById(2131297274);
        final TextView textView4 = (TextView)inflate.findViewById(2131297258);
        final TextView textView5 = (TextView)inflate.findViewById(2131297296);
        final ImageView imageView = (ImageView)inflate.findViewById(2131296691);
        final ImageView imageView2 = (ImageView)inflate.findViewById(2131296686);
        final ImageButton imageButton = (ImageButton)inflate.findViewById(2131296668);
        textView2.setText((CharSequence)studentDataModel.getStudentName());
        final StringBuilder sb = new StringBuilder();
        sb.append(studentDataModel.getRollNO());
        sb.append("");
        textView3.setText((CharSequence)sb.toString());
        if (studentDataModel.getEmailId().trim().equals("")) {
            ((View)textView5).setVisibility(4);
        }
        textView5.setText((CharSequence)studentDataModel.getEmailId());
        if (studentDataModel.getPhoneNo().trim().equals("")) {
            ((View)textView4).setVisibility(4);
            inflate.findViewById(2131296687).setVisibility(4);
        }
        textView4.setText((CharSequence)studentDataModel.getPhoneNo());
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(studentDataModel.getStudentName().charAt(0));
        sb2.append("");
        textView.setText((CharSequence)sb2.toString());
        ((View)imageButton).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, viewGroup, n) {
            public final ViewGroup a;
            public final int b;
            public final q3 c;
            
            public void onClick(final View view) {
                ((AdapterView)this.a).performItemClick(view, this.b, (long)view.getId());
            }
        });
        if (this.a.get(n, false)) {
            n = this.c.getResources().getColor(2131099717);
        }
        else {
            n = Color.rgb(255, 255, 255);
        }
        inflate.setBackgroundColor(n);
        if (studentDataModel.isSynced()) {
            imageView.setVisibility(0);
            imageView2.setVisibility(8);
        }
        else {
            imageView.setVisibility(8);
            imageView2.setVisibility(0);
        }
        return inflate;
    }
}

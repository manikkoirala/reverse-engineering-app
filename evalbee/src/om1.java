import android.os.Bundle;
import android.app.Activity;
import com.google.firebase.sessions.SessionLifecycleClient;
import android.app.Application$ActivityLifecycleCallbacks;

// 
// Decompiled by Procyon v0.6.0
// 

public final class om1 implements Application$ActivityLifecycleCallbacks
{
    public static final om1 a;
    public static boolean b;
    public static SessionLifecycleClient c;
    
    static {
        a = new om1();
    }
    
    public final void a(final SessionLifecycleClient c) {
        om1.c = c;
        if (c != null && om1.b) {
            om1.b = false;
            c.k();
        }
    }
    
    public void onActivityCreated(final Activity activity, final Bundle bundle) {
        fg0.e((Object)activity, "activity");
    }
    
    public void onActivityDestroyed(final Activity activity) {
        fg0.e((Object)activity, "activity");
    }
    
    public void onActivityPaused(final Activity activity) {
        fg0.e((Object)activity, "activity");
        final SessionLifecycleClient c = om1.c;
        if (c != null) {
            c.h();
        }
    }
    
    public void onActivityResumed(final Activity activity) {
        fg0.e((Object)activity, "activity");
        final SessionLifecycleClient c = om1.c;
        u02 a;
        if (c != null) {
            c.k();
            a = u02.a;
        }
        else {
            a = null;
        }
        if (a == null) {
            om1.b = true;
        }
    }
    
    public void onActivitySaveInstanceState(final Activity activity, final Bundle bundle) {
        fg0.e((Object)activity, "activity");
        fg0.e((Object)bundle, "outState");
    }
    
    public void onActivityStarted(final Activity activity) {
        fg0.e((Object)activity, "activity");
    }
    
    public void onActivityStopped(final Activity activity) {
        fg0.e((Object)activity, "activity");
    }
}

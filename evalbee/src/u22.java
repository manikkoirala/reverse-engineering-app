import android.text.TextUtils;
import com.google.firebase.installations.local.b;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

// 
// Decompiled by Procyon v0.6.0
// 

public final class u22
{
    public static final long b;
    public static final Pattern c;
    public static u22 d;
    public final bh a;
    
    static {
        b = TimeUnit.HOURS.toSeconds(1L);
        c = Pattern.compile("\\AA[\\w-]{38}\\z");
    }
    
    public u22(final bh a) {
        this.a = a;
    }
    
    public static u22 c() {
        return d(ct1.a());
    }
    
    public static u22 d(final bh bh) {
        if (u22.d == null) {
            u22.d = new u22(bh);
        }
        return u22.d;
    }
    
    public static boolean g(final String input) {
        return u22.c.matcher(input).matches();
    }
    
    public static boolean h(final String s) {
        return s.contains(":");
    }
    
    public long a() {
        return this.a.currentTimeMillis();
    }
    
    public long b() {
        return TimeUnit.MILLISECONDS.toSeconds(this.a());
    }
    
    public long e() {
        return (long)(Math.random() * 1000.0);
    }
    
    public boolean f(final b b) {
        return TextUtils.isEmpty((CharSequence)b.b()) || b.h() + b.c() < this.b() + u22.b;
    }
}

import android.view.accessibility.AccessibilityManager$TouchExplorationStateChangeListener;
import android.view.accessibility.AccessibilityManager;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class r0
{
    public static boolean a(final AccessibilityManager accessibilityManager, final b b) {
        return a.a(accessibilityManager, b);
    }
    
    public static boolean b(final AccessibilityManager accessibilityManager, final b b) {
        return a.b(accessibilityManager, b);
    }
    
    public abstract static class a
    {
        public static boolean a(final AccessibilityManager accessibilityManager, final b b) {
            return accessibilityManager.addTouchExplorationStateChangeListener((AccessibilityManager$TouchExplorationStateChangeListener)new c(b));
        }
        
        public static boolean b(final AccessibilityManager accessibilityManager, final b b) {
            return accessibilityManager.removeTouchExplorationStateChangeListener((AccessibilityManager$TouchExplorationStateChangeListener)new c(b));
        }
    }
    
    public interface b
    {
        void onTouchExplorationStateChanged(final boolean p0);
    }
    
    public static final class c implements AccessibilityManager$TouchExplorationStateChangeListener
    {
        public final b a;
        
        public c(final b a) {
            this.a = a;
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || (o instanceof c && this.a.equals(((c)o).a));
        }
        
        @Override
        public int hashCode() {
            return this.a.hashCode();
        }
        
        public void onTouchExplorationStateChanged(final boolean b) {
            this.a.onTouchExplorationStateChanged(b);
        }
    }
}

import com.google.firebase.components.MissingDependencyException;
import java.util.Collections;
import com.google.firebase.components.InvalidRegistrarException;
import android.util.Log;
import com.google.firebase.components.ComponentRegistrar;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicReference;
import java.util.Set;
import java.util.List;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public class wj implements gj, nj
{
    public static final r91 i;
    public final Map a;
    public final Map b;
    public final Map c;
    public final List d;
    public Set e;
    public final mx f;
    public final AtomicReference g;
    public final rj h;
    
    static {
        i = new tj();
    }
    
    public wj(final Executor executor, final Iterable iterable, final Collection collection, final rj h) {
        this.a = new HashMap();
        this.b = new HashMap();
        this.c = new HashMap();
        this.e = new HashSet();
        this.g = new AtomicReference();
        final mx f = new mx(executor);
        this.f = f;
        this.h = h;
        final ArrayList list = new ArrayList();
        list.add(zi.s(f, mx.class, bs1.class, y91.class));
        list.add(zi.s(this, nj.class, new Class[0]));
        for (final zi zi : collection) {
            if (zi != null) {
                list.add(zi);
            }
        }
        this.d = q(iterable);
        this.n(list);
    }
    
    public static b m(final Executor executor) {
        return new b(executor);
    }
    
    public static List q(final Iterable iterable) {
        final ArrayList list = new ArrayList();
        final Iterator iterator = iterable.iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next());
        }
        return list;
    }
    
    @Override
    public r91 b(final da1 da1) {
        synchronized (this) {
            final dj0 dj0 = this.c.get(da1);
            if (dj0 != null) {
                return dj0;
            }
            return wj.i;
        }
    }
    
    @Override
    public jr f(final da1 da1) {
        final r91 g = this.g(da1);
        if (g == null) {
            return g21.e();
        }
        if (g instanceof g21) {
            return (g21)g;
        }
        return g21.i(g);
    }
    
    @Override
    public r91 g(final da1 da1) {
        synchronized (this) {
            j71.c(da1, "Null interface requested.");
            return this.b.get(da1);
        }
    }
    
    public final void n(final List c) {
        final ArrayList list = new ArrayList();
        synchronized (this) {
            final Iterator iterator = this.d.iterator();
            while (iterator.hasNext()) {
                final r91 r91 = (r91)iterator.next();
                try {
                    final ComponentRegistrar componentRegistrar = (ComponentRegistrar)r91.get();
                    if (componentRegistrar == null) {
                        continue;
                    }
                    c.addAll(this.h.a(componentRegistrar));
                    iterator.remove();
                }
                catch (final InvalidRegistrarException ex) {
                    iterator.remove();
                    Log.w("ComponentDiscovery", "Invalid component registrar.", (Throwable)ex);
                }
            }
            final Iterator iterator2 = c.iterator();
            while (iterator2.hasNext()) {
                for (final Object o : ((zi)iterator2.next()).j().toArray()) {
                    if (o.toString().contains("kotlinx.coroutines.CoroutineDispatcher")) {
                        if (this.e.contains(o.toString())) {
                            iterator2.remove();
                            break;
                        }
                        this.e.add(o.toString());
                    }
                }
            }
            if (this.a.isEmpty()) {
                ap.a(c);
            }
            else {
                final ArrayList list2 = new ArrayList(this.a.keySet());
                list2.addAll(c);
                ap.a(list2);
            }
            for (final zi zi : c) {
                this.a.put(zi, new yi0(new sj(this, zi)));
            }
            list.addAll(this.w(c));
            list.addAll(this.x());
            this.v();
            monitorexit(this);
            final Iterator iterator4 = list.iterator();
            while (iterator4.hasNext()) {
                ((Runnable)iterator4.next()).run();
            }
            this.u();
        }
    }
    
    public final void o(final Map map, final boolean b) {
        for (final Map.Entry<zi, V> entry : map.entrySet()) {
            final zi zi = entry.getKey();
            final r91 r91 = (r91)entry.getValue();
            if (zi.n() || (zi.o() && b)) {
                r91.get();
            }
        }
        this.f.d();
    }
    
    public void p(final boolean b) {
        if (!ja2.a(this.g, null, b)) {
            return;
        }
        synchronized (this) {
            final HashMap hashMap = new HashMap(this.a);
            monitorexit(this);
            this.o(hashMap, b);
        }
    }
    
    public final void u() {
        final Boolean b = this.g.get();
        if (b != null) {
            this.o(this.a, b);
        }
    }
    
    public final void v() {
        for (final zi zi : this.a.keySet()) {
            for (final os os : zi.g()) {
                Map map;
                da1 da1;
                r91 r91;
                if (os.g() && !this.c.containsKey(os.c())) {
                    map = this.c;
                    da1 = os.c();
                    r91 = dj0.b(Collections.emptySet());
                }
                else {
                    if (this.b.containsKey(os.c())) {
                        continue;
                    }
                    if (os.f()) {
                        throw new MissingDependencyException(String.format("Unsatisfied dependency for component %s: %s", zi, os.c()));
                    }
                    if (os.g()) {
                        continue;
                    }
                    map = this.b;
                    da1 = os.c();
                    r91 = g21.e();
                }
                map.put(da1, r91);
            }
        }
    }
    
    public final List w(final List list) {
        final ArrayList list2 = new ArrayList();
        for (final zi zi : list) {
            if (!zi.p()) {
                continue;
            }
            final r91 r91 = this.a.get(zi);
            for (final da1 da1 : zi.j()) {
                if (!this.b.containsKey(da1)) {
                    this.b.put(da1, r91);
                }
                else {
                    list2.add(new uj((g21)this.b.get(da1), r91));
                }
            }
        }
        return list2;
    }
    
    public final List x() {
        final ArrayList list = new ArrayList();
        final HashMap hashMap = new HashMap();
        for (final Map.Entry<zi, V> entry : this.a.entrySet()) {
            final zi zi = entry.getKey();
            if (zi.p()) {
                continue;
            }
            final r91 r91 = (r91)entry.getValue();
            for (final da1 da1 : zi.j()) {
                if (!hashMap.containsKey(da1)) {
                    hashMap.put(da1, new HashSet());
                }
                ((Set)hashMap.get(da1)).add(r91);
            }
        }
        for (final Map.Entry<Object, V> entry2 : hashMap.entrySet()) {
            if (!this.c.containsKey(entry2.getKey())) {
                this.c.put(entry2.getKey(), dj0.b((Collection)entry2.getValue()));
            }
            else {
                final dj0 dj0 = this.c.get(entry2.getKey());
                final Iterator iterator4 = ((Set)entry2.getValue()).iterator();
                while (iterator4.hasNext()) {
                    list.add(new vj(dj0, (r91)iterator4.next()));
                }
            }
        }
        return list;
    }
    
    public static final class b
    {
        public final Executor a;
        public final List b;
        public final List c;
        public rj d;
        
        public b(final Executor a) {
            this.b = new ArrayList();
            this.c = new ArrayList();
            this.d = rj.a;
            this.a = a;
        }
        
        public b b(final zi zi) {
            this.c.add(zi);
            return this;
        }
        
        public b c(final ComponentRegistrar componentRegistrar) {
            this.b.add(new xj(componentRegistrar));
            return this;
        }
        
        public b d(final Collection collection) {
            this.b.addAll(collection);
            return this;
        }
        
        public wj e() {
            return new wj(this.a, this.b, this.c, this.d, null);
        }
        
        public b g(final rj d) {
            this.d = d;
            return this;
        }
    }
}

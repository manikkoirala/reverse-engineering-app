import android.view.View;
import android.text.method.TransformationMethod;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.text.InputFilter;
import android.widget.TextView;

// 
// Decompiled by Procyon v0.6.0
// 

public class v6
{
    public final TextView a;
    public final pw b;
    
    public v6(final TextView a) {
        this.a = a;
        this.b = new pw(a, false);
    }
    
    public InputFilter[] a(final InputFilter[] array) {
        return this.b.a(array);
    }
    
    public boolean b() {
        return this.b.b();
    }
    
    public void c(final AttributeSet set, int u0) {
        final TypedArray obtainStyledAttributes = ((View)this.a).getContext().obtainStyledAttributes(set, bc1.g0, u0, 0);
        try {
            u0 = bc1.u0;
            final boolean hasValue = obtainStyledAttributes.hasValue(u0);
            boolean boolean1 = true;
            if (hasValue) {
                boolean1 = obtainStyledAttributes.getBoolean(u0, true);
            }
            obtainStyledAttributes.recycle();
            this.e(boolean1);
        }
        finally {
            obtainStyledAttributes.recycle();
        }
    }
    
    public void d(final boolean b) {
        this.b.c(b);
    }
    
    public void e(final boolean b) {
        this.b.d(b);
    }
    
    public TransformationMethod f(final TransformationMethod transformationMethod) {
        return this.b.e(transformationMethod);
    }
}

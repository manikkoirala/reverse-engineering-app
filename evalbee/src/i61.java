import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.OnCompleteListener;
import com.android.volley.Request;
import java.util.HashMap;
import com.google.firebase.auth.FirebaseAuth;
import java.util.Map;
import java.io.UnsupportedEncodingException;
import com.android.volley.VolleyError;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.OrgProfile;
import com.ekodroid.omrevaluator.more.models.Teacher;
import com.android.volley.d;
import android.content.Context;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.PendingJoinRequestDto;

// 
// Decompiled by Procyon v0.6.0
// 

public class i61
{
    public ee1 a;
    public zg b;
    public PendingJoinRequestDto c;
    public String d;
    public Context e;
    
    public i61(final PendingJoinRequestDto c, final Context e, final zg b) {
        this.b = b;
        this.c = c;
        this.e = e;
        this.a = new n52(e, a91.v()).b();
        final StringBuilder sb = new StringBuilder();
        sb.append("https://");
        sb.append(a91.v());
        sb.append(":");
        sb.append(a91.p());
        sb.append("/api/pending-invitation");
        this.d = sb.toString();
    }
    
    public static /* synthetic */ PendingJoinRequestDto c(final i61 i61) {
        return i61.c;
    }
    
    public static /* synthetic */ Context d(final i61 i61) {
        return i61.e;
    }
    
    public final void e(final String s) {
        final hr1 hr1 = new hr1(this, 1, this.d, new d.b(this) {
            public final i61 a;
            
            public void b(final String s) {
                if (i61.c(this.a).getMemberStatus() == Teacher.MemberStatus.ACCEPTED) {
                    OrgProfile.reset(i61.d(this.a));
                }
                this.a.f(true, 200, null);
            }
        }, new d.a(this) {
            public final i61 a;
            
            @Override
            public void a(final VolleyError volleyError) {
                final yy0 networkResponse = volleyError.networkResponse;
                int a;
                if (networkResponse != null) {
                    a = networkResponse.a;
                }
                else {
                    a = 400;
                }
                this.a.f(false, a, volleyError.getMessage());
            }
        }, s) {
            public final String w;
            public final i61 x;
            
            @Override
            public byte[] k() {
                try {
                    return new gc0().s(i61.c(this.x)).getBytes("UTF-8");
                }
                catch (final UnsupportedEncodingException ex) {
                    ex.printStackTrace();
                    return null;
                }
            }
            
            @Override
            public String l() {
                return "application/json";
            }
            
            @Override
            public Map o() {
                final String o = FirebaseAuth.getInstance().e().O();
                final String email = FirebaseAuth.getInstance().e().getEmail();
                final HashMap hashMap = new HashMap();
                hashMap.put("x-user-id", o);
                hashMap.put("x-user-email", email);
                hashMap.put("x-user-token", this.w);
                return hashMap;
            }
        };
        hr1.L(new wq(20000, 0, 1.0f));
        this.a.a(hr1);
    }
    
    public final void f(final boolean b, final int n, final Object o) {
        final zg b2 = this.b;
        if (b2 != null) {
            b2.a(b, n, o);
        }
    }
    
    public void g() {
        FirebaseAuth.getInstance().e().i(false).addOnCompleteListener((OnCompleteListener)new OnCompleteListener(this) {
            public final i61 a;
            
            public void onComplete(final Task task) {
                if (task.isSuccessful()) {
                    this.a.e(((ya0)task.getResult()).c());
                }
                else {
                    this.a.f(false, 400, null);
                }
            }
        });
    }
}

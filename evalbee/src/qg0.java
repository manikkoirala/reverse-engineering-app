import com.google.protobuf.t;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.io.InputStream;

// 
// Decompiled by Procyon v0.6.0
// 

public class qg0 extends InputStream
{
    public Iterator a;
    public ByteBuffer b;
    public int c;
    public int d;
    public int e;
    public boolean f;
    public byte[] g;
    public int h;
    public long i;
    
    public qg0(final Iterable iterable) {
        this.a = iterable.iterator();
        this.c = 0;
        for (final ByteBuffer byteBuffer : iterable) {
            ++this.c;
        }
        this.d = -1;
        if (!this.a()) {
            this.b = t.e;
            this.d = 0;
            this.e = 0;
            this.i = 0L;
        }
    }
    
    public final boolean a() {
        ++this.d;
        if (!this.a.hasNext()) {
            return false;
        }
        final ByteBuffer b = this.a.next();
        this.b = b;
        this.e = b.position();
        if (this.b.hasArray()) {
            this.f = true;
            this.g = this.b.array();
            this.h = this.b.arrayOffset();
        }
        else {
            this.f = false;
            this.i = d12.k(this.b);
            this.g = null;
        }
        return true;
    }
    
    public final void b(int e) {
        e += this.e;
        this.e = e;
        if (e == this.b.limit()) {
            this.a();
        }
    }
    
    @Override
    public int read() {
        if (this.d == this.c) {
            return -1;
        }
        byte w;
        if (this.f) {
            w = this.g[this.e + this.h];
        }
        else {
            w = d12.w(this.e + this.i);
        }
        this.b(1);
        return w & 0xFF;
    }
    
    @Override
    public int read(final byte[] dst, final int offset, int position) {
        if (this.d == this.c) {
            return -1;
        }
        final int limit = this.b.limit();
        final int e = this.e;
        final int n = limit - e;
        int length;
        if ((length = position) > n) {
            length = n;
        }
        if (this.f) {
            System.arraycopy(this.g, e + this.h, dst, offset, length);
        }
        else {
            position = this.b.position();
            this.b.position(this.e);
            this.b.get(dst, offset, length);
            this.b.position(position);
        }
        this.b(length);
        return length;
    }
}

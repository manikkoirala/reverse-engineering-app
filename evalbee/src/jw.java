import android.text.Editable;
import android.text.Editable$Factory;

// 
// Decompiled by Procyon v0.6.0
// 

public final class jw extends Editable$Factory
{
    public static final Object a;
    public static volatile Editable$Factory b;
    public static Class c;
    
    static {
        a = new Object();
    }
    
    public jw() {
        try {
            jw.c = Class.forName("android.text.DynamicLayout$ChangeWatcher", false, jw.class.getClassLoader());
        }
        finally {}
    }
    
    public static Editable$Factory getInstance() {
        if (jw.b == null) {
            synchronized (jw.a) {
                if (jw.b == null) {
                    jw.b = new jw();
                }
            }
        }
        return jw.b;
    }
    
    public Editable newEditable(final CharSequence charSequence) {
        final Class c = jw.c;
        if (c != null) {
            return (Editable)vo1.c(c, charSequence);
        }
        return super.newEditable(charSequence);
    }
}

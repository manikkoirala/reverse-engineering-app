import java.io.IOException;
import java.util.Arrays;
import com.google.gson.stream.JsonToken;
import java.io.EOFException;
import com.google.gson.stream.MalformedJsonException;
import java.util.Objects;
import java.io.Reader;
import java.io.Closeable;

// 
// Decompiled by Procyon v0.6.0
// 

public class rh0 implements Closeable
{
    public final Reader a;
    public boolean b;
    public final char[] c;
    public int d;
    public int e;
    public int f;
    public int g;
    public int h;
    public long i;
    public int j;
    public String k;
    public int[] l;
    public int m;
    public String[] n;
    public int[] p;
    
    static {
        sh0.a = new sh0() {
            @Override
            public void a(final rh0 rh0) {
                int n;
                if ((n = rh0.h) == 0) {
                    n = rh0.e();
                }
                int h;
                if (n == 13) {
                    h = 9;
                }
                else if (n == 12) {
                    h = 8;
                }
                else {
                    if (n != 14) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Expected a name but was ");
                        sb.append(rh0.o0());
                        sb.append(rh0.u());
                        throw new IllegalStateException(sb.toString());
                    }
                    h = 10;
                }
                rh0.h = h;
            }
        };
    }
    
    public rh0(final Reader reader) {
        this.b = false;
        this.c = new char[1024];
        this.d = 0;
        this.e = 0;
        this.f = 0;
        this.g = 0;
        this.h = 0;
        final int[] l = new int[32];
        this.l = l;
        this.m = 0 + 1;
        l[0] = 6;
        this.n = new String[32];
        this.p = new int[32];
        Objects.requireNonNull(reader, "in == null");
        this.a = reader;
    }
    
    public double E() {
        int n;
        if ((n = this.h) == 0) {
            n = this.e();
        }
        if (n == 15) {
            this.h = 0;
            final int[] p = this.p;
            final int n2 = this.m - 1;
            ++p[n2];
            return (double)this.i;
        }
        Label_0233: {
            if (n == 16) {
                this.k = new String(this.c, this.d, this.j);
                this.d += this.j;
            }
            else {
                String k;
                if (n != 8 && n != 9) {
                    if (n == 10) {
                        k = this.m0();
                    }
                    else {
                        if (n == 11) {
                            break Label_0233;
                        }
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Expected a double but was ");
                        sb.append(this.o0());
                        sb.append(this.u());
                        throw new IllegalStateException(sb.toString());
                    }
                }
                else {
                    char c;
                    if (n == 8) {
                        c = '\'';
                    }
                    else {
                        c = '\"';
                    }
                    k = this.Z(c);
                }
                this.k = k;
            }
        }
        this.h = 11;
        final double double1 = Double.parseDouble(this.k);
        if (!this.b && (Double.isNaN(double1) || Double.isInfinite(double1))) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("JSON forbids NaN and infinities: ");
            sb2.append(double1);
            sb2.append(this.u());
            throw new MalformedJsonException(sb2.toString());
        }
        this.k = null;
        this.h = 0;
        final int[] p2 = this.p;
        final int n3 = this.m - 1;
        ++p2[n3];
        return double1;
    }
    
    public int H() {
        int n;
        if ((n = this.h) == 0) {
            n = this.e();
        }
        if (n == 15) {
            final long i = this.i;
            final int n2 = (int)i;
            if (i == n2) {
                this.h = 0;
                final int[] p = this.p;
                final int n3 = this.m - 1;
                ++p[n3];
                return n2;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Expected an int but was ");
            sb.append(this.i);
            sb.append(this.u());
            throw new NumberFormatException(sb.toString());
        }
        else {
            while (true) {
                if (n == 16) {
                    this.k = new String(this.c, this.d, this.j);
                    this.d += this.j;
                    break Label_0343;
                }
                if (n != 8 && n != 9 && n != 10) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Expected an int but was ");
                    sb2.append(this.o0());
                    sb2.append(this.u());
                    throw new IllegalStateException(sb2.toString());
                }
                String k;
                if (n == 10) {
                    k = this.m0();
                }
                else {
                    char c;
                    if (n == 8) {
                        c = '\'';
                    }
                    else {
                        c = '\"';
                    }
                    k = this.Z(c);
                }
                this.k = k;
                try {
                    final int int1 = Integer.parseInt(this.k);
                    this.h = 0;
                    final int[] p2 = this.p;
                    final int n4 = this.m - 1;
                    ++p2[n4];
                    return int1;
                    final StringBuilder sb3;
                    Label_0408: {
                        sb3 = new StringBuilder();
                    }
                    sb3.append("Expected an int but was ");
                    sb3.append(this.k);
                    sb3.append(this.u());
                    throw new NumberFormatException(sb3.toString());
                    this.h = 11;
                    final double double1 = Double.parseDouble(this.k);
                    final int n5 = (int)double1;
                    iftrue(Label_0408:)(n5 != double1);
                    this.k = null;
                    this.h = 0;
                    final int[] p3 = this.p;
                    final int n6 = this.m - 1;
                    ++p3[n6];
                    return n5;
                }
                catch (final NumberFormatException ex) {
                    continue;
                }
                break;
            }
        }
    }
    
    public long J() {
        int n;
        if ((n = this.h) == 0) {
            n = this.e();
        }
        if (n == 15) {
            this.h = 0;
            final int[] p = this.p;
            final int n2 = this.m - 1;
            ++p[n2];
            return this.i;
        }
        while (true) {
            if (n == 16) {
                this.k = new String(this.c, this.d, this.j);
                this.d += this.j;
                break Label_0275;
            }
            if (n != 8 && n != 9 && n != 10) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Expected a long but was ");
                sb.append(this.o0());
                sb.append(this.u());
                throw new IllegalStateException(sb.toString());
            }
            String k;
            if (n == 10) {
                k = this.m0();
            }
            else {
                char c;
                if (n == 8) {
                    c = '\'';
                }
                else {
                    c = '\"';
                }
                k = this.Z(c);
            }
            this.k = k;
            try {
                final long long1 = Long.parseLong(this.k);
                this.h = 0;
                final int[] p2 = this.p;
                final int n3 = this.m - 1;
                ++p2[n3];
                return long1;
                final StringBuilder sb2;
                Label_0340: {
                    sb2 = new StringBuilder();
                }
                sb2.append("Expected a long but was ");
                sb2.append(this.k);
                sb2.append(this.u());
                throw new NumberFormatException(sb2.toString());
                this.h = 11;
                final double double1 = Double.parseDouble(this.k);
                final long n4 = (long)double1;
                iftrue(Label_0340:)(n4 != double1);
                this.k = null;
                this.h = 0;
                final int[] p3 = this.p;
                final int n5 = this.m - 1;
                ++p3[n5];
                return n4;
            }
            catch (final NumberFormatException ex) {
                continue;
            }
            break;
        }
    }
    
    public String K() {
        int n;
        if ((n = this.h) == 0) {
            n = this.e();
        }
        String s;
        if (n == 14) {
            s = this.m0();
        }
        else {
            char c;
            if (n == 12) {
                c = '\'';
            }
            else {
                if (n != 13) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Expected a name but was ");
                    sb.append(this.o0());
                    sb.append(this.u());
                    throw new IllegalStateException(sb.toString());
                }
                c = '\"';
            }
            s = this.Z(c);
        }
        this.h = 0;
        return this.n[this.m - 1] = s;
    }
    
    public final int O(final boolean b) {
        final char[] c = this.c;
        while (true) {
            int d = this.d;
        Label_0245:
            while (true) {
                int e = this.e;
                while (true) {
                    int d2 = d;
                    int e2 = e;
                    if (d == e) {
                        this.d = d;
                        if (!this.h(1)) {
                            if (!b) {
                                return -1;
                            }
                            final StringBuilder sb = new StringBuilder();
                            sb.append("End of input");
                            sb.append(this.u());
                            throw new EOFException(sb.toString());
                        }
                        else {
                            d2 = this.d;
                            e2 = this.e;
                        }
                    }
                    d = d2 + 1;
                    final char c2 = c[d2];
                    if (c2 == '\n') {
                        ++this.f;
                        this.g = d;
                    }
                    else if (c2 != ' ' && c2 != '\r') {
                        if (c2 != '\t') {
                            if (c2 == '/') {
                                if ((this.d = d) == e2) {
                                    this.d = d - 1;
                                    final boolean h = this.h(2);
                                    ++this.d;
                                    if (!h) {
                                        return c2;
                                    }
                                }
                                this.c();
                                final int d3 = this.d;
                                final char c3 = c[d3];
                                if (c3 != '*') {
                                    if (c3 != '/') {
                                        return c2;
                                    }
                                    this.d = d3 + 1;
                                    break Label_0245;
                                }
                                else {
                                    this.d = d3 + 1;
                                    if (this.v0("*/")) {
                                        d = this.d + 2;
                                        break;
                                    }
                                    throw this.z0("Unterminated comment");
                                }
                            }
                            else {
                                this.d = d;
                                if (c2 == '#') {
                                    this.c();
                                    break Label_0245;
                                }
                                return c2;
                            }
                        }
                    }
                    e = e2;
                }
            }
            this.w0();
        }
    }
    
    public void R() {
        int n;
        if ((n = this.h) == 0) {
            n = this.e();
        }
        if (n == 7) {
            this.h = 0;
            final int[] p = this.p;
            final int n2 = this.m - 1;
            ++p[n2];
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Expected null but was ");
        sb.append(this.o0());
        sb.append(this.u());
        throw new IllegalStateException(sb.toString());
    }
    
    public final String Z(final char c) {
        final char[] c2 = this.c;
        StringBuilder sb = null;
        while (true) {
            final int d = this.d;
            final int e = this.e;
            int g = d;
            while (true) {
                final int d2 = g;
                if (d2 < e) {
                    g = d2 + 1;
                    final char c3 = c2[d2];
                    if (c3 == c) {
                        this.d = g;
                        final int n = g - d - 1;
                        if (sb == null) {
                            return new String(c2, d, n);
                        }
                        sb.append(c2, d, n);
                        return sb.toString();
                    }
                    else {
                        if (c3 == '\\') {
                            this.d = g;
                            final int len = g - d - 1;
                            StringBuilder sb2;
                            if ((sb2 = sb) == null) {
                                sb2 = new StringBuilder(Math.max((len + 1) * 2, 16));
                            }
                            sb2.append(c2, d, len);
                            sb2.append(this.s0());
                            sb = sb2;
                            break;
                        }
                        if (c3 != '\n') {
                            continue;
                        }
                        ++this.f;
                        this.g = g;
                    }
                }
                else {
                    StringBuilder sb3;
                    if ((sb3 = sb) == null) {
                        sb3 = new StringBuilder(Math.max((d2 - d) * 2, 16));
                    }
                    sb3.append(c2, d, d2 - d);
                    this.d = d2;
                    if (this.h(1)) {
                        sb = sb3;
                        break;
                    }
                    throw this.z0("Unterminated string");
                }
            }
        }
    }
    
    public void a() {
        int n;
        if ((n = this.h) == 0) {
            n = this.e();
        }
        if (n == 3) {
            this.r0(1);
            this.p[this.m - 1] = 0;
            this.h = 0;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Expected BEGIN_ARRAY but was ");
        sb.append(this.o0());
        sb.append(this.u());
        throw new IllegalStateException(sb.toString());
    }
    
    public void b() {
        int n;
        if ((n = this.h) == 0) {
            n = this.e();
        }
        if (n == 1) {
            this.r0(3);
            this.h = 0;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Expected BEGIN_OBJECT but was ");
        sb.append(this.o0());
        sb.append(this.u());
        throw new IllegalStateException(sb.toString());
    }
    
    public final void c() {
        if (this.b) {
            return;
        }
        throw this.z0("Use JsonReader.setLenient(true) to accept malformed JSON");
    }
    
    @Override
    public void close() {
        this.h = 0;
        this.l[0] = 8;
        this.m = 1;
        this.a.close();
    }
    
    public final void d() {
        this.O(true);
        final int d = this.d - 1;
        this.d = d;
        if (d + 5 > this.e && !this.h(5)) {
            return;
        }
        final int d2 = this.d;
        final char[] c = this.c;
        if (c[d2] == ')' && c[d2 + 1] == ']' && c[d2 + 2] == '}' && c[d2 + 3] == '\'') {
            if (c[d2 + 4] == '\n') {
                this.d = d2 + 5;
            }
        }
    }
    
    public int e() {
        final int[] l = this.l;
        final int m = this.m;
        final int n = l[m - 1];
        Label_0261: {
            if (n == 1) {
                l[m - 1] = 2;
                break Label_0261;
            }
            int h;
            if (n == 2) {
                final int o = this.O(true);
                if (o == 44) {
                    break Label_0261;
                }
                if (o == 59) {
                    this.c();
                    break Label_0261;
                }
                if (o == 93) {
                    return this.h = 4;
                }
                throw this.z0("Unterminated array");
            }
            else if (n != 3 && n != 5) {
                if (n == 4) {
                    l[m - 1] = 5;
                    final int o2 = this.O(true);
                    if (o2 == 58) {
                        break Label_0261;
                    }
                    if (o2 != 61) {
                        throw this.z0("Expected ':'");
                    }
                    this.c();
                    if (this.d >= this.e && !this.h(1)) {
                        break Label_0261;
                    }
                    final char[] c = this.c;
                    final int d = this.d;
                    if (c[d] == '>') {
                        this.d = d + 1;
                    }
                    break Label_0261;
                }
                else {
                    if (n == 6) {
                        if (this.b) {
                            this.d();
                        }
                        this.l[this.m - 1] = 7;
                        break Label_0261;
                    }
                    if (n == 7) {
                        if (this.O(false) != -1) {
                            this.c();
                            --this.d;
                            break Label_0261;
                        }
                        h = 17;
                    }
                    else {
                        if (n != 8) {
                            break Label_0261;
                        }
                        throw new IllegalStateException("JsonReader is closed");
                    }
                }
            }
            else {
                l[m - 1] = 4;
                if (n == 5) {
                    final int o3 = this.O(true);
                    if (o3 != 44) {
                        if (o3 != 59) {
                            if (o3 == 125) {
                                return this.h = 2;
                            }
                            throw this.z0("Unterminated object");
                        }
                        else {
                            this.c();
                        }
                    }
                }
                final int o4 = this.O(true);
                if (o4 != 34) {
                    if (o4 != 39) {
                        if (o4 != 125) {
                            this.c();
                            --this.d;
                            if (!this.q((char)o4)) {
                                throw this.z0("Expected name");
                            }
                            h = 14;
                        }
                        else {
                            if (n != 5) {
                                return this.h = 2;
                            }
                            throw this.z0("Expected name");
                        }
                    }
                    else {
                        this.c();
                        h = 12;
                    }
                }
                else {
                    h = 13;
                }
            }
            return this.h = h;
        }
        final int o5 = this.O(true);
        if (o5 == 34) {
            final int h = 9;
            return this.h = h;
        }
        if (o5 == 39) {
            this.c();
            return this.h = 8;
        }
        if (o5 != 44 && o5 != 59) {
            if (o5 == 91) {
                return this.h = 3;
            }
            if (o5 != 93) {
                if (o5 == 123) {
                    return this.h = 1;
                }
                --this.d;
                final int p0 = this.p0();
                if (p0 != 0) {
                    return p0;
                }
                final int q0 = this.q0();
                if (q0 != 0) {
                    return q0;
                }
                if (this.q(this.c[this.d])) {
                    this.c();
                    final int h = 10;
                    return this.h = h;
                }
                throw this.z0("Expected value");
            }
            else if (n == 1) {
                return this.h = 4;
            }
        }
        if (n != 1 && n != 2) {
            throw this.z0("Unexpected value");
        }
        this.c();
        --this.d;
        return this.h = 7;
    }
    
    public void f() {
        int n;
        if ((n = this.h) == 0) {
            n = this.e();
        }
        if (n == 4) {
            int m = this.m - 1;
            this.m = m;
            final int[] p = this.p;
            --m;
            ++p[m];
            this.h = 0;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Expected END_ARRAY but was ");
        sb.append(this.o0());
        sb.append(this.u());
        throw new IllegalStateException(sb.toString());
    }
    
    public void g() {
        int n;
        if ((n = this.h) == 0) {
            n = this.e();
        }
        if (n == 2) {
            int m = this.m - 1;
            this.m = m;
            this.n[m] = null;
            final int[] p = this.p;
            --m;
            ++p[m];
            this.h = 0;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Expected END_OBJECT but was ");
        sb.append(this.o0());
        sb.append(this.u());
        throw new IllegalStateException(sb.toString());
    }
    
    public String getPath() {
        return this.i(false);
    }
    
    public final boolean h(int n) {
        final char[] c = this.c;
        final int g = this.g;
        final int d = this.d;
        this.g = g - d;
        final int e = this.e;
        if (e != d) {
            System.arraycopy(c, d, c, 0, this.e = e - d);
        }
        else {
            this.e = 0;
        }
        this.d = 0;
        int i;
        int n2;
        do {
            final Reader a = this.a;
            final int e2 = this.e;
            final int read = a.read(c, e2, c.length - e2);
            if (read == -1) {
                return false;
            }
            i = this.e + read;
            this.e = i;
            n2 = n;
            if (this.f != 0) {
                continue;
            }
            final int g2 = this.g;
            n2 = n;
            if (g2 != 0) {
                continue;
            }
            n2 = n;
            if (i <= 0) {
                continue;
            }
            n2 = n;
            if (c[0] != '\ufeff') {
                continue;
            }
            ++this.d;
            this.g = g2 + 1;
            n2 = n + 1;
        } while (i < (n = n2));
        return true;
    }
    
    public final String i(final boolean b) {
        final StringBuilder sb = new StringBuilder();
        sb.append('$');
        int n = 0;
        while (true) {
            final int m = this.m;
            if (n >= m) {
                break;
            }
            final int n2 = this.l[n];
            if (n2 != 1 && n2 != 2) {
                if (n2 == 3 || n2 == 4 || n2 == 5) {
                    sb.append('.');
                    final String str = this.n[n];
                    if (str != null) {
                        sb.append(str);
                    }
                }
            }
            else {
                int i;
                final int n3 = i = this.p[n];
                if (b && (i = n3) > 0) {
                    i = n3;
                    if (n == m - 1) {
                        i = n3 - 1;
                    }
                }
                sb.append('[');
                sb.append(i);
                sb.append(']');
            }
            ++n;
        }
        return sb.toString();
    }
    
    public String i0() {
        int n;
        if ((n = this.h) == 0) {
            n = this.e();
        }
        String s = null;
        Label_0140: {
            if (n == 10) {
                s = this.m0();
            }
            else {
                char c;
                if (n == 8) {
                    c = '\'';
                }
                else if (n == 9) {
                    c = '\"';
                }
                else {
                    if (n == 11) {
                        s = this.k;
                        this.k = null;
                        break Label_0140;
                    }
                    if (n == 15) {
                        s = Long.toString(this.i);
                        break Label_0140;
                    }
                    if (n == 16) {
                        s = new String(this.c, this.d, this.j);
                        this.d += this.j;
                        break Label_0140;
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Expected a string but was ");
                    sb.append(this.o0());
                    sb.append(this.u());
                    throw new IllegalStateException(sb.toString());
                }
                s = this.Z(c);
            }
        }
        this.h = 0;
        final int[] p = this.p;
        final int n2 = this.m - 1;
        ++p[n2];
        return s;
    }
    
    public String j() {
        return this.i(true);
    }
    
    public boolean k() {
        int n;
        if ((n = this.h) == 0) {
            n = this.e();
        }
        return n != 2 && n != 4 && n != 17;
    }
    
    public final String m0() {
        StringBuilder sb = null;
        final int n = 0;
        while (true) {
            do {
                int n2 = 0;
                Label_0171: {
                    Label_0142: {
                        while (true) {
                            final int d = this.d;
                            if (d + n2 < this.e) {
                                final char c = this.c[d + n2];
                                if (c == '\t' || c == '\n' || c == '\f' || c == '\r' || c == ' ') {
                                    break Label_0171;
                                }
                                if (c == '#') {
                                    break Label_0142;
                                }
                                if (c == ',') {
                                    break Label_0171;
                                }
                                if (c == '/' || c == '=') {
                                    break Label_0142;
                                }
                                if (c == '{' || c == '}' || c == ':') {
                                    break Label_0171;
                                }
                                if (c == ';') {
                                    break Label_0142;
                                }
                                switch (c) {
                                    default: {
                                        ++n2;
                                        continue;
                                    }
                                    case 92: {
                                        break Label_0142;
                                    }
                                    case 91:
                                    case 93: {
                                        break Label_0171;
                                    }
                                }
                            }
                            else {
                                if (n2 >= this.c.length) {
                                    break;
                                }
                                if (this.h(n2 + 1)) {
                                    continue;
                                }
                                break Label_0171;
                            }
                        }
                        StringBuilder sb2;
                        if ((sb2 = sb) == null) {
                            sb2 = new StringBuilder(Math.max(n2, 16));
                        }
                        sb2.append(this.c, this.d, n2);
                        this.d += n2;
                        sb = sb2;
                        continue;
                    }
                    this.c();
                }
                StringBuilder sb2 = sb;
                String string;
                if (sb2 == null) {
                    string = new String(this.c, this.d, n2);
                }
                else {
                    sb2.append(this.c, this.d, n2);
                    string = sb2.toString();
                }
                this.d += n2;
                return string;
            } while (this.h(1));
            int n2 = n;
            continue;
        }
    }
    
    public final boolean o() {
        return this.b;
    }
    
    public JsonToken o0() {
        int n;
        if ((n = this.h) == 0) {
            n = this.e();
        }
        switch (n) {
            default: {
                throw new AssertionError();
            }
            case 17: {
                return JsonToken.END_DOCUMENT;
            }
            case 15:
            case 16: {
                return JsonToken.NUMBER;
            }
            case 12:
            case 13:
            case 14: {
                return JsonToken.NAME;
            }
            case 8:
            case 9:
            case 10:
            case 11: {
                return JsonToken.STRING;
            }
            case 7: {
                return JsonToken.NULL;
            }
            case 5:
            case 6: {
                return JsonToken.BOOLEAN;
            }
            case 4: {
                return JsonToken.END_ARRAY;
            }
            case 3: {
                return JsonToken.BEGIN_ARRAY;
            }
            case 2: {
                return JsonToken.END_OBJECT;
            }
            case 1: {
                return JsonToken.BEGIN_OBJECT;
            }
        }
    }
    
    public final int p0() {
        final char c = this.c[this.d];
        String s;
        String s2;
        int h;
        if (c != 't' && c != 'T') {
            if (c != 'f' && c != 'F') {
                if (c != 'n' && c != 'N') {
                    return 0;
                }
                s = "null";
                s2 = "NULL";
                h = 7;
            }
            else {
                s = "false";
                s2 = "FALSE";
                h = 6;
            }
        }
        else {
            s = "true";
            s2 = "TRUE";
            h = 5;
        }
        final int length = s.length();
        for (int i = 1; i < length; ++i) {
            if (this.d + i >= this.e && !this.h(i + 1)) {
                return 0;
            }
            final char c2 = this.c[this.d + i];
            if (c2 != s.charAt(i) && c2 != s2.charAt(i)) {
                return 0;
            }
        }
        if ((this.d + length < this.e || this.h(length + 1)) && this.q(this.c[this.d + length])) {
            return 0;
        }
        this.d += length;
        return this.h = h;
    }
    
    public final boolean q(final char c) {
        if (c != '\t' && c != '\n' && c != '\f' && c != '\r' && c != ' ') {
            if (c != '#') {
                if (c == ',') {
                    return false;
                }
                if (c != '/' && c != '=') {
                    if (c == '{' || c == '}' || c == ':') {
                        return false;
                    }
                    if (c != ';') {
                        switch (c) {
                            default: {
                                return true;
                            }
                            case '\\': {
                                break;
                            }
                            case '[':
                            case ']': {
                                return false;
                            }
                        }
                    }
                }
            }
            this.c();
        }
        return false;
    }
    
    public final int q0() {
        final char[] c = this.c;
        int d = this.d;
        int e = this.e;
        int j = 0;
        int n2;
        int n = n2 = 0;
        int n3 = 1;
        long i = 0L;
        while (true) {
            int d2 = d;
            int e2 = e;
            if (d + j == e) {
                if (j == c.length) {
                    return 0;
                }
                if (!this.h(j + 1)) {
                    break;
                }
                d2 = this.d;
                e2 = this.e;
            }
            final char c2 = c[d2 + j];
            if (c2 != '+') {
                if (c2 != 'E' && c2 != 'e') {
                    if (c2 != '-') {
                        final int n4 = 3;
                        if (c2 != '.') {
                            if (c2 >= '0' && c2 <= '9') {
                                Label_0293: {
                                    long n5 = 0L;
                                    int n7 = 0;
                                    int n8 = 0;
                                    Label_0283: {
                                        if (n != 1 && n != 0) {
                                            if (n == 2) {
                                                if (i == 0L) {
                                                    return 0;
                                                }
                                                n5 = 10L * i - (c2 - '0');
                                                final long n6 = lcmp(i, -922337203685477580L);
                                                n7 = (n3 & ((n6 > 0 || (n6 == 0 && n5 < i)) ? 1 : 0));
                                                n8 = n;
                                            }
                                            else {
                                                if (n == 3) {
                                                    n = 4;
                                                    break Label_0293;
                                                }
                                                if (n != 5) {
                                                    n8 = n;
                                                    n7 = n3;
                                                    n5 = i;
                                                    if (n != 6) {
                                                        break Label_0283;
                                                    }
                                                }
                                                n = 7;
                                                break Label_0293;
                                            }
                                        }
                                        else {
                                            n5 = -(c2 - '0');
                                            n8 = 2;
                                            n7 = n3;
                                        }
                                    }
                                    i = n5;
                                    n3 = n7;
                                    n = n8;
                                }
                            }
                            else {
                                if (!this.q(c2)) {
                                    break;
                                }
                                return 0;
                            }
                        }
                        else {
                            if (n != 2) {
                                return 0;
                            }
                            n = n4;
                        }
                    }
                    else {
                        final int n9 = 6;
                        if (n == 0) {
                            n = 1;
                            n2 = 1;
                        }
                        else {
                            if (n != 5) {
                                return 0;
                            }
                            n = n9;
                        }
                    }
                }
                else {
                    if (n != 2 && n != 4) {
                        return 0;
                    }
                    n = 5;
                }
            }
            else {
                final int n10 = 6;
                if (n != 5) {
                    return 0;
                }
                n = n10;
            }
            ++j;
            d = d2;
            e = e2;
        }
        int h;
        if (n == 2 && n3 && (i != Long.MIN_VALUE || n2 != 0) && (i != 0L || n2 == 0)) {
            if (n2 == 0) {
                i = -i;
            }
            this.i = i;
            this.d += j;
            h = 15;
        }
        else {
            if (n != 2 && n != 4 && n != 7) {
                return 0;
            }
            this.j = j;
            h = 16;
        }
        return this.h = h;
    }
    
    public final void r0(final int n) {
        final int m = this.m;
        final int[] l = this.l;
        if (m == l.length) {
            final int newLength = m * 2;
            this.l = Arrays.copyOf(l, newLength);
            this.p = Arrays.copyOf(this.p, newLength);
            this.n = Arrays.copyOf(this.n, newLength);
        }
        this.l[this.m++] = n;
    }
    
    public final char s0() {
        if (this.d == this.e && !this.h(1)) {
            throw this.z0("Unterminated escape sequence");
        }
        final char[] c = this.c;
        final int d = this.d;
        final int n = d + 1;
        this.d = n;
        final char c2 = c[d];
        if (c2 != '\n') {
            if (c2 != '\"' && c2 != '\'' && c2 != '/' && c2 != '\\') {
                if (c2 == 'b') {
                    return '\b';
                }
                if (c2 == 'f') {
                    return '\f';
                }
                if (c2 == 'n') {
                    return '\n';
                }
                if (c2 == 'r') {
                    return '\r';
                }
                if (c2 == 't') {
                    return '\t';
                }
                if (c2 != 'u') {
                    throw this.z0("Invalid escape sequence");
                }
                if (n + 4 > this.e && !this.h(4)) {
                    throw this.z0("Unterminated escape sequence");
                }
                final int d2 = this.d;
                char c3 = '\0';
                int n2 = d2;
                while (true) {
                    final int n3 = n2;
                    if (n3 >= d2 + 4) {
                        this.d += 4;
                        return c3;
                    }
                    int n4 = this.c[n3];
                    final char c4 = (char)(c3 << 4);
                    if (n4 >= 48 && n4 <= 57) {
                        n4 -= 48;
                    }
                    else {
                        if (n4 >= 97 && n4 <= 102) {
                            n4 -= 97;
                        }
                        else {
                            if (n4 < 65 || n4 > 70) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("\\u");
                                sb.append(new String(this.c, this.d, 4));
                                throw new NumberFormatException(sb.toString());
                            }
                            n4 -= 65;
                        }
                        n4 += 10;
                    }
                    c3 = (char)(c4 + n4);
                    n2 = n3 + 1;
                }
            }
        }
        else {
            ++this.f;
            this.g = n;
        }
        return c2;
    }
    
    public final void t0(final boolean b) {
        this.b = b;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getSimpleName());
        sb.append(this.u());
        return sb.toString();
    }
    
    public String u() {
        final int f = this.f;
        final int d = this.d;
        final int g = this.g;
        final StringBuilder sb = new StringBuilder();
        sb.append(" at line ");
        sb.append(f + 1);
        sb.append(" column ");
        sb.append(d - g + 1);
        sb.append(" path ");
        sb.append(this.getPath());
        return sb.toString();
    }
    
    public final void u0(final char c) {
        final char[] c2 = this.c;
    Label_0006:
        while (true) {
            int i;
            int g;
            for (i = this.d; i < this.e; i = g) {
                g = i + 1;
                final char c3 = c2[i];
                if (c3 == c) {
                    this.d = g;
                    return;
                }
                if (c3 == '\\') {
                    this.d = g;
                    this.s0();
                    continue Label_0006;
                }
                if (c3 == '\n') {
                    ++this.f;
                    this.g = g;
                }
            }
            this.d = i;
            if (this.h(1)) {
                continue;
            }
            throw this.z0("Unterminated string");
        }
    }
    
    public final boolean v0(final String s) {
        final int length = s.length();
        while (true) {
            final int d = this.d;
            final int e = this.e;
            int i = 0;
            if (d + length > e && !this.h(length)) {
                return false;
            }
            final char[] c = this.c;
            final int d2 = this.d;
            Label_0108: {
                if (c[d2] != '\n') {
                    while (i < length) {
                        if (this.c[this.d + i] != s.charAt(i)) {
                            break Label_0108;
                        }
                        ++i;
                    }
                    return true;
                }
                ++this.f;
                this.g = d2 + 1;
            }
            ++this.d;
        }
    }
    
    public final void w0() {
        while (this.d < this.e || this.h(1)) {
            final char[] c = this.c;
            final int d = this.d;
            final int n = d + 1;
            this.d = n;
            final char c2 = c[d];
            if (c2 == '\n') {
                ++this.f;
                this.g = n;
                break;
            }
            if (c2 == '\r') {
                break;
            }
        }
    }
    
    public boolean x() {
        int n;
        if ((n = this.h) == 0) {
            n = this.e();
        }
        if (n == 5) {
            this.h = 0;
            final int[] p = this.p;
            final int n2 = this.m - 1;
            ++p[n2];
            return true;
        }
        if (n == 6) {
            this.h = 0;
            final int[] p2 = this.p;
            final int n3 = this.m - 1;
            ++p2[n3];
            return false;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Expected a boolean but was ");
        sb.append(this.o0());
        sb.append(this.u());
        throw new IllegalStateException(sb.toString());
    }
    
    public final void x0() {
        do {
            int n = 0;
            Label_0142: {
                Label_0138: {
                    int d;
                    while (true) {
                        d = this.d;
                        if (d + n >= this.e) {
                            break;
                        }
                        final char c = this.c[d + n];
                        if (c == '\t' || c == '\n' || c == '\f' || c == '\r' || c == ' ') {
                            break Label_0142;
                        }
                        if (c == '#') {
                            break Label_0138;
                        }
                        if (c == ',') {
                            break Label_0142;
                        }
                        if (c == '/' || c == '=') {
                            break Label_0138;
                        }
                        if (c == '{' || c == '}' || c == ':') {
                            break Label_0142;
                        }
                        if (c == ';') {
                            break Label_0138;
                        }
                        switch (c) {
                            default: {
                                ++n;
                                continue;
                            }
                            case 92: {
                                break Label_0138;
                            }
                            case 91:
                            case 93: {
                                break Label_0142;
                            }
                        }
                    }
                    this.d = d + n;
                    continue;
                }
                this.c();
            }
            this.d += n;
        } while (this.h(1));
    }
    
    public void y0() {
        int n = 0;
        int n2 = 0;
        do {
            int n3;
            if ((n3 = this.h) == 0) {
                n3 = this.e();
            }
            Label_0299: {
                switch (n3) {
                    default: {
                        n2 = n;
                        break Label_0299;
                    }
                    case 17: {
                        return;
                    }
                    case 16: {
                        this.d += this.j;
                        n2 = n;
                        break Label_0299;
                    }
                    case 14: {
                        this.x0();
                        n2 = n;
                        if (n == 0) {
                            this.n[this.m - 1] = "<skipped>";
                            n2 = n;
                        }
                        break Label_0299;
                    }
                    case 13: {
                        this.u0('\"');
                        n2 = n;
                        if (n == 0) {
                            this.n[this.m - 1] = "<skipped>";
                            n2 = n;
                        }
                        break Label_0299;
                    }
                    case 12: {
                        this.u0('\'');
                        n2 = n;
                        if (n == 0) {
                            this.n[this.m - 1] = "<skipped>";
                            n2 = n;
                        }
                        break Label_0299;
                    }
                    case 10: {
                        this.x0();
                        n2 = n;
                        break Label_0299;
                    }
                    case 9: {
                        this.u0('\"');
                        n2 = n;
                        break Label_0299;
                    }
                    case 8: {
                        this.u0('\'');
                        n2 = n;
                        break Label_0299;
                    }
                    case 2: {
                        if (n == 0) {
                            this.n[this.m - 1] = null;
                        }
                    }
                    case 4: {
                        --this.m;
                        n2 = n - 1;
                        break Label_0299;
                    }
                    case 3: {
                        this.r0(1);
                        break;
                    }
                    case 1: {
                        this.r0(3);
                        break;
                    }
                }
                n2 = n + 1;
            }
            this.h = 0;
        } while ((n = n2) > 0);
        final int[] p = this.p;
        final int n4 = this.m - 1;
        ++p[n4];
    }
    
    public final IOException z0(final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(this.u());
        throw new MalformedJsonException(sb.toString());
    }
}

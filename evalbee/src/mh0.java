import java.util.TimeZone;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.text.DateFormat;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import com.google.firebase.encoders.EncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public final class mh0 implements zw
{
    public static final w01 e;
    public static final x22 f;
    public static final x22 g;
    public static final b h;
    public final Map a;
    public final Map b;
    public w01 c;
    public boolean d;
    
    static {
        e = new jh0();
        f = new kh0();
        g = new lh0();
        h = new b(null);
    }
    
    public mh0() {
        this.a = new HashMap();
        this.b = new HashMap();
        this.c = mh0.e;
        this.d = false;
        this.p(String.class, mh0.f);
        this.p(Boolean.class, mh0.g);
        this.p(Date.class, mh0.h);
    }
    
    public static /* synthetic */ Map e(final mh0 mh0) {
        return mh0.a;
    }
    
    public static /* synthetic */ Map f(final mh0 mh0) {
        return mh0.b;
    }
    
    public static /* synthetic */ w01 g(final mh0 mh0) {
        return mh0.c;
    }
    
    public static /* synthetic */ boolean h(final mh0 mh0) {
        return mh0.d;
    }
    
    public hp i() {
        return new hp(this) {
            public final mh0 a;
            
            @Override
            public void a(final Object o, final Writer writer) {
                final uh0 uh0 = new uh0(writer, mh0.e(this.a), mh0.f(this.a), mh0.g(this.a), mh0.h(this.a));
                uh0.j(o, false);
                uh0.t();
            }
            
            @Override
            public String encode(final Object o) {
                final StringWriter stringWriter = new StringWriter();
                try {
                    this.a(o, stringWriter);
                    return stringWriter.toString();
                }
                catch (final IOException ex) {
                    return stringWriter.toString();
                }
            }
        };
    }
    
    public mh0 j(final jk jk) {
        jk.configure(this);
        return this;
    }
    
    public mh0 k(final boolean d) {
        this.d = d;
        return this;
    }
    
    public mh0 o(final Class clazz, final w01 w01) {
        this.a.put(clazz, w01);
        this.b.remove(clazz);
        return this;
    }
    
    public mh0 p(final Class clazz, final x22 x22) {
        this.b.put(clazz, x22);
        this.a.remove(clazz);
        return this;
    }
    
    public static final class b implements x22
    {
        public static final DateFormat a;
        
        static {
            (a = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US)).setTimeZone(TimeZone.getTimeZone("UTC"));
        }
        
        public void a(final Date date, final y22 y22) {
            y22.add(b.a.format(date));
        }
    }
}

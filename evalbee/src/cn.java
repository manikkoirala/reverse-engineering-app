import java.io.File;
import java.io.IOException;

// 
// Decompiled by Procyon v0.6.0
// 

public class cn
{
    public final String a;
    public final z00 b;
    
    public cn(final String a, final z00 b) {
        this.a = a;
        this.b = b;
    }
    
    public boolean a() {
        boolean newFile;
        try {
            newFile = this.b().createNewFile();
        }
        catch (final IOException ex) {
            final zl0 f = zl0.f();
            final StringBuilder sb = new StringBuilder();
            sb.append("Error creating marker: ");
            sb.append(this.a);
            f.e(sb.toString(), ex);
            newFile = false;
        }
        return newFile;
    }
    
    public final File b() {
        return this.b.e(this.a);
    }
    
    public boolean c() {
        return this.b().exists();
    }
    
    public boolean d() {
        return this.b().delete();
    }
}

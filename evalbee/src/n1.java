import android.text.Spannable;
import android.os.BaseBundle;
import android.util.Log;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.accessibility.AccessibilityNodeInfo$RangeInfo;
import java.util.Collections;
import java.util.List;
import android.view.accessibility.AccessibilityNodeInfo$CollectionItemInfo;
import android.view.accessibility.AccessibilityNodeInfo$CollectionInfo;
import android.graphics.Rect;
import android.util.SparseArray;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import android.view.accessibility.AccessibilityNodeInfo$AccessibilityAction;
import android.os.Bundle;
import android.os.Build$VERSION;
import android.text.Spanned;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.accessibility.AccessibilityNodeInfo;

// 
// Decompiled by Procyon v0.6.0
// 

public class n1
{
    public static int d;
    public final AccessibilityNodeInfo a;
    public int b;
    public int c;
    
    public n1(final AccessibilityNodeInfo a) {
        this.b = -1;
        this.c = -1;
        this.a = a;
    }
    
    public static n1 B0(final AccessibilityNodeInfo accessibilityNodeInfo) {
        return new n1(accessibilityNodeInfo);
    }
    
    public static n1 L() {
        return B0(AccessibilityNodeInfo.obtain());
    }
    
    public static n1 M(final n1 n1) {
        return B0(AccessibilityNodeInfo.obtain(n1.a));
    }
    
    public static n1 N(final View view) {
        return B0(AccessibilityNodeInfo.obtain(view));
    }
    
    public static String i(final int n) {
        if (n == 1) {
            return "ACTION_FOCUS";
        }
        if (n == 2) {
            return "ACTION_CLEAR_FOCUS";
        }
        switch (n) {
            default: {
                switch (n) {
                    default: {
                        switch (n) {
                            default: {
                                switch (n) {
                                    default: {
                                        return "ACTION_UNKNOWN";
                                    }
                                    case 16908375: {
                                        return "ACTION_DRAG_CANCEL";
                                    }
                                    case 16908374: {
                                        return "ACTION_DRAG_DROP";
                                    }
                                    case 16908373: {
                                        return "ACTION_DRAG_START";
                                    }
                                    case 16908372: {
                                        return "ACTION_IME_ENTER";
                                    }
                                }
                                break;
                            }
                            case 16908362: {
                                return "ACTION_PRESS_AND_HOLD";
                            }
                            case 16908361: {
                                return "ACTION_PAGE_RIGHT";
                            }
                            case 16908360: {
                                return "ACTION_PAGE_LEFT";
                            }
                            case 16908359: {
                                return "ACTION_PAGE_DOWN";
                            }
                            case 16908358: {
                                return "ACTION_PAGE_UP";
                            }
                            case 16908357: {
                                return "ACTION_HIDE_TOOLTIP";
                            }
                            case 16908356: {
                                return "ACTION_SHOW_TOOLTIP";
                            }
                        }
                        break;
                    }
                    case 16908349: {
                        return "ACTION_SET_PROGRESS";
                    }
                    case 16908348: {
                        return "ACTION_CONTEXT_CLICK";
                    }
                    case 16908347: {
                        return "ACTION_SCROLL_RIGHT";
                    }
                    case 16908346: {
                        return "ACTION_SCROLL_DOWN";
                    }
                    case 16908345: {
                        return "ACTION_SCROLL_LEFT";
                    }
                    case 16908344: {
                        return "ACTION_SCROLL_UP";
                    }
                    case 16908343: {
                        return "ACTION_SCROLL_TO_POSITION";
                    }
                    case 16908342: {
                        return "ACTION_SHOW_ON_SCREEN";
                    }
                }
                break;
            }
            case 16908354: {
                return "ACTION_MOVE_WINDOW";
            }
            case 2097152: {
                return "ACTION_SET_TEXT";
            }
            case 524288: {
                return "ACTION_COLLAPSE";
            }
            case 262144: {
                return "ACTION_EXPAND";
            }
            case 131072: {
                return "ACTION_SET_SELECTION";
            }
            case 65536: {
                return "ACTION_CUT";
            }
            case 32768: {
                return "ACTION_PASTE";
            }
            case 16384: {
                return "ACTION_COPY";
            }
            case 8192: {
                return "ACTION_SCROLL_BACKWARD";
            }
            case 4096: {
                return "ACTION_SCROLL_FORWARD";
            }
            case 2048: {
                return "ACTION_PREVIOUS_HTML_ELEMENT";
            }
            case 1024: {
                return "ACTION_NEXT_HTML_ELEMENT";
            }
            case 512: {
                return "ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY";
            }
            case 256: {
                return "ACTION_NEXT_AT_MOVEMENT_GRANULARITY";
            }
            case 128: {
                return "ACTION_CLEAR_ACCESSIBILITY_FOCUS";
            }
            case 64: {
                return "ACTION_ACCESSIBILITY_FOCUS";
            }
            case 32: {
                return "ACTION_LONG_CLICK";
            }
            case 16: {
                return "ACTION_CLICK";
            }
            case 8: {
                return "ACTION_CLEAR_SELECTION";
            }
            case 4: {
                return "ACTION_SELECT";
            }
        }
    }
    
    public static ClickableSpan[] p(final CharSequence charSequence) {
        if (charSequence instanceof Spanned) {
            return (ClickableSpan[])((Spanned)charSequence).getSpans(0, charSequence.length(), (Class)ClickableSpan.class);
        }
        return null;
    }
    
    public boolean A() {
        return this.a.isCheckable();
    }
    
    public AccessibilityNodeInfo A0() {
        return this.a;
    }
    
    public boolean B() {
        return this.a.isChecked();
    }
    
    public boolean C() {
        return this.a.isClickable();
    }
    
    public boolean D() {
        return this.a.isEnabled();
    }
    
    public boolean E() {
        return this.a.isFocusable();
    }
    
    public boolean F() {
        return this.a.isFocused();
    }
    
    public boolean G() {
        return this.a.isLongClickable();
    }
    
    public boolean H() {
        return this.a.isPassword();
    }
    
    public boolean I() {
        return this.a.isScrollable();
    }
    
    public boolean J() {
        return this.a.isSelected();
    }
    
    public boolean K() {
        if (Build$VERSION.SDK_INT >= 26) {
            return t0.a(this.a);
        }
        return this.k(4);
    }
    
    public boolean O(final int n, final Bundle bundle) {
        return this.a.performAction(n, bundle);
    }
    
    public void P() {
        this.a.recycle();
    }
    
    public boolean Q(final a a) {
        return this.a.removeAction((AccessibilityNodeInfo$AccessibilityAction)a.a);
    }
    
    public final void R(final View view) {
        final SparseArray u = this.u(view);
        if (u != null) {
            final ArrayList list = new ArrayList();
            final int n = 0;
            int i = 0;
            int j;
            while (true) {
                j = n;
                if (i >= u.size()) {
                    break;
                }
                if (((WeakReference)u.valueAt(i)).get() == null) {
                    list.add(i);
                }
                ++i;
            }
            while (j < list.size()) {
                u.remove((int)list.get(j));
                ++j;
            }
        }
    }
    
    public void S(final boolean accessibilityFocused) {
        this.a.setAccessibilityFocused(accessibilityFocused);
    }
    
    public final void T(final int n, final boolean b) {
        final Bundle r = this.r();
        if (r != null) {
            final int int1 = ((BaseBundle)r).getInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.BOOLEAN_PROPERTY_KEY", 0);
            int n2;
            if (b) {
                n2 = n;
            }
            else {
                n2 = 0;
            }
            ((BaseBundle)r).putInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.BOOLEAN_PROPERTY_KEY", n2 | (int1 & ~n));
        }
    }
    
    public void U(final Rect boundsInParent) {
        this.a.setBoundsInParent(boundsInParent);
    }
    
    public void V(final Rect boundsInScreen) {
        this.a.setBoundsInScreen(boundsInScreen);
    }
    
    public void W(final boolean checkable) {
        this.a.setCheckable(checkable);
    }
    
    public void X(final boolean checked) {
        this.a.setChecked(checked);
    }
    
    public void Y(final CharSequence className) {
        this.a.setClassName(className);
    }
    
    public void Z(final boolean clickable) {
        this.a.setClickable(clickable);
    }
    
    public void a(final int n) {
        this.a.addAction(n);
    }
    
    public void a0(final Object o) {
        final AccessibilityNodeInfo a = this.a;
        AccessibilityNodeInfo$CollectionInfo collectionInfo;
        if (o == null) {
            collectionInfo = null;
        }
        else {
            collectionInfo = (AccessibilityNodeInfo$CollectionInfo)((b)o).a;
        }
        a.setCollectionInfo(collectionInfo);
    }
    
    public void b(final a a) {
        this.a.addAction((AccessibilityNodeInfo$AccessibilityAction)a.a);
    }
    
    public void b0(final Object o) {
        final AccessibilityNodeInfo a = this.a;
        AccessibilityNodeInfo$CollectionItemInfo collectionItemInfo;
        if (o == null) {
            collectionItemInfo = null;
        }
        else {
            collectionItemInfo = (AccessibilityNodeInfo$CollectionItemInfo)((c)o).a;
        }
        a.setCollectionItemInfo(collectionItemInfo);
    }
    
    public void c(final View view, final int n) {
        this.a.addChild(view, n);
    }
    
    public void c0(final CharSequence contentDescription) {
        this.a.setContentDescription(contentDescription);
    }
    
    public final void d(final ClickableSpan clickableSpan, final Spanned spanned, final int i) {
        this.g("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY").add(spanned.getSpanStart((Object)clickableSpan));
        this.g("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_END_KEY").add(spanned.getSpanEnd((Object)clickableSpan));
        this.g("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_FLAGS_KEY").add(spanned.getSpanFlags((Object)clickableSpan));
        this.g("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ID_KEY").add(i);
    }
    
    public void d0(final boolean dismissable) {
        this.a.setDismissable(dismissable);
    }
    
    public void e(final CharSequence charSequence, final View view) {
        if (Build$VERSION.SDK_INT < 26) {
            this.f();
            this.R(view);
            final ClickableSpan[] p2 = p(charSequence);
            if (p2 != null && p2.length > 0) {
                ((BaseBundle)this.r()).putInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ACTION_ID_KEY", fb1.a);
                final SparseArray s = this.s(view);
                for (int i = 0; i < p2.length; ++i) {
                    final int z = this.z(p2[i], s);
                    s.put(z, (Object)new WeakReference(p2[i]));
                    this.d(p2[i], (Spanned)charSequence, z);
                }
            }
        }
    }
    
    public void e0(final boolean enabled) {
        this.a.setEnabled(enabled);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (!(o instanceof n1)) {
            return false;
        }
        final n1 n1 = (n1)o;
        final AccessibilityNodeInfo a = this.a;
        if (a == null) {
            if (n1.a != null) {
                return false;
            }
        }
        else if (!a.equals((Object)n1.a)) {
            return false;
        }
        return this.c == n1.c && this.b == n1.b;
    }
    
    public final void f() {
        this.a.getExtras().remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY");
        this.a.getExtras().remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_END_KEY");
        this.a.getExtras().remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_FLAGS_KEY");
        this.a.getExtras().remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ID_KEY");
    }
    
    public void f0(final CharSequence error) {
        this.a.setError(error);
    }
    
    public final List g(final String s) {
        ArrayList integerArrayList;
        if ((integerArrayList = this.a.getExtras().getIntegerArrayList(s)) == null) {
            integerArrayList = new ArrayList();
            this.a.getExtras().putIntegerArrayList(s, integerArrayList);
        }
        return integerArrayList;
    }
    
    public void g0(final boolean focusable) {
        this.a.setFocusable(focusable);
    }
    
    public List h() {
        final List actionList = this.a.getActionList();
        if (actionList != null) {
            final ArrayList list = new ArrayList();
            for (int size = actionList.size(), i = 0; i < size; ++i) {
                list.add(new a(actionList.get(i)));
            }
            return list;
        }
        return Collections.emptyList();
    }
    
    public void h0(final boolean focused) {
        this.a.setFocused(focused);
    }
    
    @Override
    public int hashCode() {
        final AccessibilityNodeInfo a = this.a;
        int hashCode;
        if (a == null) {
            hashCode = 0;
        }
        else {
            hashCode = a.hashCode();
        }
        return hashCode;
    }
    
    public void i0(final boolean b) {
        if (Build$VERSION.SDK_INT >= 28) {
            z0.a(this.a, b);
        }
        else {
            this.T(2, b);
        }
    }
    
    public int j() {
        return this.a.getActions();
    }
    
    public void j0(final CharSequence charSequence) {
        if (Build$VERSION.SDK_INT >= 26) {
            s0.a(this.a, charSequence);
        }
        else {
            this.a.getExtras().putCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.HINT_TEXT_KEY", charSequence);
        }
    }
    
    public final boolean k(final int n) {
        final Bundle r = this.r();
        boolean b = false;
        if (r == null) {
            return false;
        }
        if ((((BaseBundle)r).getInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.BOOLEAN_PROPERTY_KEY", 0) & n) == n) {
            b = true;
        }
        return b;
    }
    
    public void k0(final View labelFor) {
        this.a.setLabelFor(labelFor);
    }
    
    public void l(final Rect rect) {
        this.a.getBoundsInParent(rect);
    }
    
    public void l0(final int maxTextLength) {
        this.a.setMaxTextLength(maxTextLength);
    }
    
    public void m(final Rect rect) {
        this.a.getBoundsInScreen(rect);
    }
    
    public void m0(final CharSequence packageName) {
        this.a.setPackageName(packageName);
    }
    
    public int n() {
        return this.a.getChildCount();
    }
    
    public void n0(final CharSequence charSequence) {
        if (Build$VERSION.SDK_INT >= 28) {
            w0.a(this.a, charSequence);
        }
        else {
            this.a.getExtras().putCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.PANE_TITLE_KEY", charSequence);
        }
    }
    
    public CharSequence o() {
        return this.a.getClassName();
    }
    
    public void o0(final View parent) {
        this.b = -1;
        this.a.setParent(parent);
    }
    
    public void p0(final View view, final int b) {
        this.b = b;
        this.a.setParent(view, b);
    }
    
    public CharSequence q() {
        return this.a.getContentDescription();
    }
    
    public void q0(final d d) {
        this.a.setRangeInfo((AccessibilityNodeInfo$RangeInfo)d.a);
    }
    
    public Bundle r() {
        return this.a.getExtras();
    }
    
    public void r0(final CharSequence charSequence) {
        this.a.getExtras().putCharSequence("AccessibilityNodeInfo.roleDescription", charSequence);
    }
    
    public final SparseArray s(final View view) {
        SparseArray u;
        if ((u = this.u(view)) == null) {
            u = new SparseArray();
            view.setTag(fb1.I, (Object)u);
        }
        return u;
    }
    
    public void s0(final boolean b) {
        if (Build$VERSION.SDK_INT >= 28) {
            u0.a(this.a, b);
        }
        else {
            this.T(1, b);
        }
    }
    
    public CharSequence t() {
        return this.a.getPackageName();
    }
    
    public void t0(final boolean scrollable) {
        this.a.setScrollable(scrollable);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        final Rect rect = new Rect();
        this.l(rect);
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("; boundsInParent: ");
        sb2.append(rect);
        sb.append(sb2.toString());
        this.m(rect);
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("; boundsInScreen: ");
        sb3.append(rect);
        sb.append(sb3.toString());
        sb.append("; packageName: ");
        sb.append(this.t());
        sb.append("; className: ");
        sb.append(this.o());
        sb.append("; text: ");
        sb.append(this.v());
        sb.append("; contentDescription: ");
        sb.append(this.q());
        sb.append("; viewId: ");
        sb.append(this.x());
        sb.append("; uniqueId: ");
        sb.append(this.w());
        sb.append("; checkable: ");
        sb.append(this.A());
        sb.append("; checked: ");
        sb.append(this.B());
        sb.append("; focusable: ");
        sb.append(this.E());
        sb.append("; focused: ");
        sb.append(this.F());
        sb.append("; selected: ");
        sb.append(this.J());
        sb.append("; clickable: ");
        sb.append(this.C());
        sb.append("; longClickable: ");
        sb.append(this.G());
        sb.append("; enabled: ");
        sb.append(this.D());
        sb.append("; password: ");
        sb.append(this.H());
        final StringBuilder sb4 = new StringBuilder();
        sb4.append("; scrollable: ");
        sb4.append(this.I());
        sb.append(sb4.toString());
        sb.append("; [");
        final List h = this.h();
        for (int i = 0; i < h.size(); ++i) {
            final a a = h.get(i);
            String str;
            final String s = str = i(a.b());
            if (s.equals("ACTION_UNKNOWN")) {
                str = s;
                if (a.c() != null) {
                    str = a.c().toString();
                }
            }
            sb.append(str);
            if (i != h.size() - 1) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }
    
    public final SparseArray u(final View view) {
        return (SparseArray)view.getTag(fb1.I);
    }
    
    public void u0(final boolean b) {
        if (Build$VERSION.SDK_INT >= 26) {
            y0.a(this.a, b);
        }
        else {
            this.T(4, b);
        }
    }
    
    public CharSequence v() {
        if (this.y()) {
            final List g = this.g("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY");
            final List g2 = this.g("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_END_KEY");
            final List g3 = this.g("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_FLAGS_KEY");
            final List g4 = this.g("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ID_KEY");
            final CharSequence text = this.a.getText();
            final int length = this.a.getText().length();
            int i = 0;
            final SpannableString spannableString = new SpannableString((CharSequence)TextUtils.substring(text, 0, length));
            while (i < g.size()) {
                ((Spannable)spannableString).setSpan((Object)new o0(g4.get(i), this, ((BaseBundle)this.r()).getInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ACTION_ID_KEY")), (int)g.get(i), (int)g2.get(i), (int)g3.get(i));
                ++i;
            }
            return (CharSequence)spannableString;
        }
        return this.a.getText();
    }
    
    public void v0(final View view, final int c) {
        this.c = c;
        this.a.setSource(view, c);
    }
    
    public String w() {
        if (yc.c()) {
            return v0.a(this.a);
        }
        return ((BaseBundle)this.a.getExtras()).getString("androidx.view.accessibility.AccessibilityNodeInfoCompat.UNIQUE_ID_KEY");
    }
    
    public void w0(final CharSequence charSequence) {
        if (yc.b()) {
            x0.a(this.a, charSequence);
        }
        else {
            this.a.getExtras().putCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.STATE_DESCRIPTION_KEY", charSequence);
        }
    }
    
    public String x() {
        return this.a.getViewIdResourceName();
    }
    
    public void x0(final CharSequence text) {
        this.a.setText(text);
    }
    
    public final boolean y() {
        return this.g("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY").isEmpty() ^ true;
    }
    
    public void y0(final View traversalAfter) {
        this.a.setTraversalAfter(traversalAfter);
    }
    
    public final int z(final ClickableSpan clickableSpan, final SparseArray sparseArray) {
        if (sparseArray != null) {
            for (int i = 0; i < sparseArray.size(); ++i) {
                if (clickableSpan.equals(((WeakReference)sparseArray.valueAt(i)).get())) {
                    return sparseArray.keyAt(i);
                }
            }
        }
        final int d = n1.d;
        n1.d = d + 1;
        return d;
    }
    
    public void z0(final boolean visibleToUser) {
        this.a.setVisibleToUser(visibleToUser);
    }
    
    public static class a
    {
        public static final a A;
        public static final a B;
        public static final a C;
        public static final a D;
        public static final a E;
        public static final a F;
        public static final a G;
        public static final a H;
        public static final a I;
        public static final a J;
        public static final a K;
        public static final a L;
        public static final a M;
        public static final a N;
        public static final a O;
        public static final a P;
        public static final a Q;
        public static final a R;
        public static final a S;
        public static final a T;
        public static final a U;
        public static final a e;
        public static final a f;
        public static final a g;
        public static final a h;
        public static final a i;
        public static final a j;
        public static final a k;
        public static final a l;
        public static final a m;
        public static final a n;
        public static final a o;
        public static final a p;
        public static final a q;
        public static final a r;
        public static final a s;
        public static final a t;
        public static final a u;
        public static final a v;
        public static final a w;
        public static final a x;
        public static final a y;
        public static final a z;
        public final Object a;
        public final int b;
        public final Class c;
        public final q1 d;
        
        static {
            final Object o2 = null;
            e = new a(1, null);
            f = new a(2, null);
            g = new a(4, null);
            h = new a(8, null);
            i = new a(16, null);
            j = new a(32, null);
            k = new a(64, null);
            l = new a(128, null);
            m = new a(256, null, q1.b.class);
            n = new a(512, null, q1.b.class);
            o = new a(1024, null, q1.c.class);
            p = new a(2048, null, q1.c.class);
            q = new a(4096, null);
            r = new a(8192, null);
            s = new a(16384, null);
            t = new a(32768, null);
            u = new a(65536, null);
            v = new a(131072, null, q1.g.class);
            w = new a(262144, null);
            x = new a(524288, null);
            y = new a(1048576, null);
            z = new a(2097152, null, q1.h.class);
            final int sdk_INT = Build$VERSION.SDK_INT;
            A = new a(AccessibilityNodeInfo$AccessibilityAction.ACTION_SHOW_ON_SCREEN, 16908342, null, null, null);
            B = new a(AccessibilityNodeInfo$AccessibilityAction.ACTION_SCROLL_TO_POSITION, 16908343, null, null, q1.e.class);
            C = new a(AccessibilityNodeInfo$AccessibilityAction.ACTION_SCROLL_UP, 16908344, null, null, null);
            D = new a(AccessibilityNodeInfo$AccessibilityAction.ACTION_SCROLL_LEFT, 16908345, null, null, null);
            E = new a(AccessibilityNodeInfo$AccessibilityAction.ACTION_SCROLL_DOWN, 16908346, null, null, null);
            F = new a(AccessibilityNodeInfo$AccessibilityAction.ACTION_SCROLL_RIGHT, 16908347, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction a;
            if (sdk_INT >= 29) {
                a = a1.a();
            }
            else {
                a = null;
            }
            G = new a(a, 16908358, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction a2;
            if (sdk_INT >= 29) {
                a2 = h1.a();
            }
            else {
                a2 = null;
            }
            H = new a(a2, 16908359, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction a3;
            if (sdk_INT >= 29) {
                a3 = i1.a();
            }
            else {
                a3 = null;
            }
            I = new a(a3, 16908360, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction a4;
            if (sdk_INT >= 29) {
                a4 = j1.a();
            }
            else {
                a4 = null;
            }
            J = new a(a4, 16908361, null, null, null);
            K = new a(AccessibilityNodeInfo$AccessibilityAction.ACTION_CONTEXT_CLICK, 16908348, null, null, null);
            L = new a(AccessibilityNodeInfo$AccessibilityAction.ACTION_SET_PROGRESS, 16908349, null, null, q1.f.class);
            AccessibilityNodeInfo$AccessibilityAction a5;
            if (sdk_INT >= 26) {
                a5 = k1.a();
            }
            else {
                a5 = null;
            }
            M = new a(a5, 16908354, null, null, q1.d.class);
            AccessibilityNodeInfo$AccessibilityAction a6;
            if (sdk_INT >= 28) {
                a6 = l1.a();
            }
            else {
                a6 = null;
            }
            N = new a(a6, 16908356, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction a7;
            if (sdk_INT >= 28) {
                a7 = m1.a();
            }
            else {
                a7 = null;
            }
            O = new a(a7, 16908357, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction a8;
            if (sdk_INT >= 30) {
                a8 = b1.a();
            }
            else {
                a8 = null;
            }
            P = new a(a8, 16908362, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction a9;
            if (sdk_INT >= 30) {
                a9 = c1.a();
            }
            else {
                a9 = null;
            }
            Q = new a(a9, 16908372, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction a10;
            if (sdk_INT >= 32) {
                a10 = d1.a();
            }
            else {
                a10 = null;
            }
            R = new a(a10, 16908373, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction a11;
            if (sdk_INT >= 32) {
                a11 = e1.a();
            }
            else {
                a11 = null;
            }
            S = new a(a11, 16908374, null, null, null);
            AccessibilityNodeInfo$AccessibilityAction a12;
            if (sdk_INT >= 32) {
                a12 = f1.a();
            }
            else {
                a12 = null;
            }
            T = new a(a12, 16908375, null, null, null);
            Object a13 = o2;
            if (sdk_INT >= 33) {
                a13 = g1.a();
            }
            U = new a(a13, 16908376, null, null, null);
        }
        
        public a(final int n, final CharSequence charSequence) {
            this(null, n, charSequence, null, null);
        }
        
        public a(final int n, final CharSequence charSequence, final Class clazz) {
            this(null, n, charSequence, null, clazz);
        }
        
        public a(final int n, final CharSequence charSequence, final q1 q1) {
            this(null, n, charSequence, q1, null);
        }
        
        public a(final Object o) {
            this(o, 0, null, null, null);
        }
        
        public a(final Object o, final int b, final CharSequence charSequence, final q1 d, final Class c) {
            this.b = b;
            this.d = d;
            Object a = o;
            if (o == null) {
                a = new AccessibilityNodeInfo$AccessibilityAction(b, charSequence);
            }
            this.a = a;
            this.c = c;
        }
        
        public a a(final CharSequence charSequence, final q1 q1) {
            return new a(null, this.b, charSequence, q1, this.c);
        }
        
        public int b() {
            return ((AccessibilityNodeInfo$AccessibilityAction)this.a).getId();
        }
        
        public CharSequence c() {
            return ((AccessibilityNodeInfo$AccessibilityAction)this.a).getLabel();
        }
        
        public boolean d(final View view, final Bundle bundle) {
            if (this.d != null) {
                final Class c = this.c;
                if (c != null) {
                    try {
                        zu0.a(c.getDeclaredConstructor((Class[])new Class[0]).newInstance(new Object[0]));
                        throw null;
                    }
                    catch (final Exception ex) {
                        final Class c2 = this.c;
                        String name;
                        if (c2 == null) {
                            name = "null";
                        }
                        else {
                            name = c2.getName();
                        }
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Failed to execute command with argument class ViewCommandArgument: ");
                        sb.append(name);
                        Log.e("A11yActionCompat", sb.toString(), (Throwable)ex);
                    }
                }
                return this.d.perform(view, null);
            }
            return false;
        }
        
        @Override
        public boolean equals(Object a) {
            if (a == null) {
                return false;
            }
            if (!(a instanceof a)) {
                return false;
            }
            final a a2 = (a)a;
            a = this.a;
            final Object a3 = a2.a;
            if (a == null) {
                if (a3 != null) {
                    return false;
                }
            }
            else if (!a.equals(a3)) {
                return false;
            }
            return true;
        }
        
        @Override
        public int hashCode() {
            final Object a = this.a;
            int hashCode;
            if (a != null) {
                hashCode = a.hashCode();
            }
            else {
                hashCode = 0;
            }
            return hashCode;
        }
    }
    
    public static class b
    {
        public final Object a;
        
        public b(final Object a) {
            this.a = a;
        }
        
        public static b a(final int n, final int n2, final boolean b) {
            return new b(AccessibilityNodeInfo$CollectionInfo.obtain(n, n2, b));
        }
        
        public static b b(final int n, final int n2, final boolean b, final int n3) {
            return new b(AccessibilityNodeInfo$CollectionInfo.obtain(n, n2, b, n3));
        }
    }
    
    public static class c
    {
        public final Object a;
        
        public c(final Object a) {
            this.a = a;
        }
        
        public static c a(final int n, final int n2, final int n3, final int n4, final boolean b, final boolean b2) {
            return new c(AccessibilityNodeInfo$CollectionItemInfo.obtain(n, n2, n3, n4, b, b2));
        }
    }
    
    public static class d
    {
        public final Object a;
        
        public d(final Object a) {
            this.a = a;
        }
        
        public static d a(final int n, final float n2, final float n3, final float n4) {
            return new d(AccessibilityNodeInfo$RangeInfo.obtain(n, n2, n3, n4));
        }
    }
}

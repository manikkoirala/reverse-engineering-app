import com.ekodroid.omrevaluator.clients.UserProfileClients.models.UserRole;
import android.widget.CompoundButton;
import android.widget.CompoundButton$OnCheckedChangeListener;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import android.content.Context;
import java.util.ArrayList;
import android.widget.BaseAdapter;

// 
// Decompiled by Procyon v0.6.0
// 

public class c3 extends BaseAdapter
{
    public ArrayList a;
    public Context b;
    
    public c3(final ArrayList a, final Context b) {
        this.a = a;
        this.b = b;
    }
    
    public int getCount() {
        return this.a.size();
    }
    
    public Object getItem(final int index) {
        return this.a.get(index);
    }
    
    public long getItemId(final int n) {
        return n;
    }
    
    public View getView(int text, final View view, final ViewGroup viewGroup) {
        final su1 su1 = this.a.get(text);
        final View inflate = ((LayoutInflater)this.b.getSystemService("layout_inflater")).inflate(2131493015, (ViewGroup)null);
        final TextView textView = (TextView)inflate.findViewById(2131297387);
        final TextView textView2 = (TextView)inflate.findViewById(2131297388);
        final TextView textView3 = (TextView)inflate.findViewById(2131297386);
        final LinearLayout linearLayout = (LinearLayout)inflate.findViewById(2131296810);
        final CheckBox checkBox = (CheckBox)inflate.findViewById(2131296513);
        ((CompoundButton)checkBox).setChecked(su1.b);
        ((CompoundButton)checkBox).setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)new CompoundButton$OnCheckedChangeListener(this, su1) {
            public final su1 a;
            public final c3 b;
            
            public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
                this.a.b = b;
            }
        });
        textView.setText((CharSequence)su1.a.getDisplayName());
        textView2.setText((CharSequence)su1.a.getEmailId());
        if (su1.a.getUserRole() == UserRole.OWNER) {
            ((View)linearLayout).setVisibility(0);
        }
        else {
            ((View)linearLayout).setVisibility(8);
        }
        ((View)textView3).setVisibility(0);
        ((View)checkBox).setEnabled(false);
        Label_0288: {
            switch (c3$b.a[su1.a.getMemberStatus().ordinal()]) {
                default: {
                    return inflate;
                }
                case 6: {
                    ((View)textView3).setVisibility(8);
                    ((View)checkBox).setEnabled(true);
                    return inflate;
                }
                case 5: {
                    text = 2131886739;
                    break;
                }
                case 4: {
                    text = 2131886889;
                    break;
                }
                case 3: {
                    text = 2131886738;
                    break;
                }
                case 2: {
                    text = 2131886702;
                    break Label_0288;
                }
                case 1: {
                    text = 2131886366;
                    break Label_0288;
                }
            }
            textView3.setText(text);
            textView3.setTextColor(-65536);
            return inflate;
        }
        textView3.setText(text);
        textView3.setTextColor(-7829368);
        return inflate;
    }
}

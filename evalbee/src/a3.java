import android.os.Build$VERSION;

// 
// Decompiled by Procyon v0.6.0
// 

public final class a3
{
    public static final a3 a;
    
    static {
        a = new a3();
    }
    
    public final int a() {
        int a;
        if (Build$VERSION.SDK_INT >= 30) {
            a = a3.a.a.a();
        }
        else {
            a = 0;
        }
        return a;
    }
    
    public static final class a
    {
        public static final a a;
        
        static {
            a = new a();
        }
        
        public final int a() {
            return z2.a(1000000);
        }
    }
}

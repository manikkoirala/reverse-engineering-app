// 
// Decompiled by Procyon v0.6.0
// 

public final class t11 extends n92
{
    public static final b e;
    
    static {
        e = new b(null);
    }
    
    public t11(final a a) {
        fg0.e((Object)a, "builder");
        super(((n92.a)a).e(), ((n92.a)a).h(), ((n92.a)a).f());
    }
    
    public static final t11 e(final Class clazz) {
        return t11.e.a(clazz);
    }
    
    public static final class a extends n92.a
    {
        public a(final Class clazz) {
            fg0.e((Object)clazz, "workerClass");
            super(clazz);
        }
        
        public t11 l() {
            if (!((n92.a)this).d() || !((n92.a)this).h().j.h()) {
                return new t11(this);
            }
            throw new IllegalArgumentException("Cannot set backoff criteria on an idle mode job".toString());
        }
        
        public a m() {
            return this;
        }
    }
    
    public static final class b
    {
        public final t11 a(final Class clazz) {
            fg0.e((Object)clazz, "workerClass");
            return (t11)((n92.a)new a(clazz)).b();
        }
    }
}

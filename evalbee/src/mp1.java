import android.view.MenuItem;
import android.view.MenuInflater;
import android.view.Menu;
import android.view.View;
import java.lang.ref.WeakReference;
import androidx.appcompat.widget.ActionBarContextView;
import android.content.Context;
import androidx.appcompat.view.menu.e;

// 
// Decompiled by Procyon v0.6.0
// 

public class mp1 extends c2 implements e.a
{
    public Context c;
    public ActionBarContextView d;
    public c2.a e;
    public WeakReference f;
    public boolean g;
    public boolean h;
    public e i;
    
    public mp1(final Context c, final ActionBarContextView d, final c2.a e, final boolean h) {
        this.c = c;
        this.d = d;
        this.e = e;
        (this.i = new e(((View)d).getContext()).setDefaultShowAsAction(1)).setCallback((e.a)this);
        this.h = h;
    }
    
    @Override
    public void a() {
        if (this.g) {
            return;
        }
        this.g = true;
        this.e.b(this);
    }
    
    @Override
    public View b() {
        final WeakReference f = this.f;
        View view;
        if (f != null) {
            view = (View)f.get();
        }
        else {
            view = null;
        }
        return view;
    }
    
    @Override
    public Menu c() {
        return (Menu)this.i;
    }
    
    @Override
    public MenuInflater d() {
        return new ls1(((View)this.d).getContext());
    }
    
    @Override
    public CharSequence e() {
        return this.d.getSubtitle();
    }
    
    @Override
    public CharSequence g() {
        return this.d.getTitle();
    }
    
    @Override
    public void i() {
        this.e.d(this, (Menu)this.i);
    }
    
    @Override
    public boolean j() {
        return this.d.j();
    }
    
    @Override
    public void k(final View view) {
        this.d.setCustomView(view);
        WeakReference f;
        if (view != null) {
            f = new WeakReference((T)view);
        }
        else {
            f = null;
        }
        this.f = f;
    }
    
    @Override
    public void l(final int n) {
        this.m(this.c.getString(n));
    }
    
    @Override
    public void m(final CharSequence subtitle) {
        this.d.setSubtitle(subtitle);
    }
    
    @Override
    public void o(final int n) {
        this.p(this.c.getString(n));
    }
    
    @Override
    public boolean onMenuItemSelected(final e e, final MenuItem menuItem) {
        return this.e.a(this, menuItem);
    }
    
    @Override
    public void onMenuModeChange(final e e) {
        this.i();
        this.d.l();
    }
    
    @Override
    public void p(final CharSequence title) {
        this.d.setTitle(title);
    }
    
    @Override
    public void q(final boolean titleOptional) {
        super.q(titleOptional);
        this.d.setTitleOptional(titleOptional);
    }
}

import androidx.privacysandbox.ads.adservices.topics.TopicsManagerImplCommon;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class kx1
{
    public static final a a;
    
    static {
        a = new a(null);
    }
    
    public abstract Object a(final za0 p0, final vl p1);
    
    public static final class a
    {
        public final kx1 a(final Context context) {
            fg0.e((Object)context, "context");
            final a3 a = a3.a;
            TopicsManagerImplCommon topicsManagerImplCommon;
            if (a.a() >= 5) {
                topicsManagerImplCommon = new sx1(context);
            }
            else if (a.a() == 4) {
                topicsManagerImplCommon = new nx1(context);
            }
            else {
                topicsManagerImplCommon = null;
            }
            return topicsManagerImplCommon;
        }
    }
}

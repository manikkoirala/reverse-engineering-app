import com.google.common.collect.Iterators;
import java.util.Iterator;
import java.util.Collection;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class t70 extends a80 implements Collection
{
    @Override
    public boolean add(final Object o) {
        return this.delegate().add(o);
    }
    
    @Override
    public boolean addAll(final Collection<Object> collection) {
        return this.delegate().addAll(collection);
    }
    
    @Override
    public void clear() {
        this.delegate().clear();
    }
    
    @Override
    public boolean contains(final Object o) {
        return this.delegate().contains(o);
    }
    
    @Override
    public boolean containsAll(final Collection<?> collection) {
        return this.delegate().containsAll(collection);
    }
    
    @Override
    public abstract Collection delegate();
    
    @Override
    public boolean isEmpty() {
        return this.delegate().isEmpty();
    }
    
    @Override
    public Iterator<Object> iterator() {
        return this.delegate().iterator();
    }
    
    @Override
    public boolean remove(final Object o) {
        return this.delegate().remove(o);
    }
    
    @Override
    public boolean removeAll(final Collection<?> collection) {
        return this.delegate().removeAll(collection);
    }
    
    @Override
    public boolean retainAll(final Collection<?> collection) {
        return this.delegate().retainAll(collection);
    }
    
    @Override
    public int size() {
        return this.delegate().size();
    }
    
    public boolean standardAddAll(final Collection<Object> collection) {
        return Iterators.a(this, collection.iterator());
    }
    
    public void standardClear() {
        Iterators.d(this.iterator());
    }
    
    public boolean standardContains(final Object o) {
        return Iterators.f(this.iterator(), o);
    }
    
    public boolean standardContainsAll(final Collection<?> collection) {
        return lh.a(this, collection);
    }
    
    public boolean standardIsEmpty() {
        return this.iterator().hasNext() ^ true;
    }
    
    public boolean standardRemove(final Object o) {
        final Iterator<Object> iterator = this.iterator();
        while (iterator.hasNext()) {
            if (b11.a(iterator.next(), o)) {
                iterator.remove();
                return true;
            }
        }
        return false;
    }
    
    public boolean standardRemoveAll(final Collection<?> collection) {
        return Iterators.r(this.iterator(), collection);
    }
    
    public boolean standardRetainAll(final Collection<?> collection) {
        return Iterators.s(this.iterator(), collection);
    }
    
    public Object[] standardToArray() {
        return this.toArray(new Object[this.size()]);
    }
    
    public <T> T[] standardToArray(final T[] array) {
        return (T[])t01.g(this, array);
    }
    
    public String standardToString() {
        return lh.e(this);
    }
    
    @Override
    public Object[] toArray() {
        return this.delegate().toArray();
    }
    
    @Override
    public <T> T[] toArray(final T[] array) {
        return this.delegate().toArray(array);
    }
}

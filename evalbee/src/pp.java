import java.util.concurrent.Callable;
import java.io.File;
import java.util.Set;
import android.content.Intent;
import java.util.concurrent.Executor;
import java.util.List;
import androidx.room.RoomDatabase;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class pp
{
    public final Context a;
    public final String b;
    public final ts1.c c;
    public final RoomDatabase.d d;
    public final List e;
    public final boolean f;
    public final RoomDatabase.JournalMode g;
    public final Executor h;
    public final Executor i;
    public final Intent j;
    public final boolean k;
    public final boolean l;
    public final Set m;
    public final String n;
    public final File o;
    public final Callable p;
    public final List q;
    public final List r;
    public final boolean s;
    
    public pp(final Context a, final String b, final ts1.c c, final RoomDatabase.d d, final List e, final boolean f, final RoomDatabase.JournalMode g, final Executor h, final Executor i, final Intent j, final boolean k, final boolean l, final Set m, final String n, final File o, final Callable p19, final RoomDatabase.e e2, final List q, final List r) {
        fg0.e((Object)a, "context");
        fg0.e((Object)c, "sqliteOpenHelperFactory");
        fg0.e((Object)d, "migrationContainer");
        fg0.e((Object)g, "journalMode");
        fg0.e((Object)h, "queryExecutor");
        fg0.e((Object)i, "transactionExecutor");
        fg0.e((Object)q, "typeConverters");
        fg0.e((Object)r, "autoMigrationSpecs");
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = h;
        this.i = i;
        this.j = j;
        this.k = k;
        this.l = l;
        this.m = m;
        this.n = n;
        this.o = o;
        this.p = p19;
        this.q = q;
        this.r = r;
        this.s = (j != null);
    }
    
    public boolean a(final int i, int n) {
        final boolean b = true;
        if (i > n) {
            n = 1;
        }
        else {
            n = 0;
        }
        if (n != 0 && this.l) {
            return false;
        }
        if (this.k) {
            final Set m = this.m;
            boolean b2 = b;
            if (m == null) {
                return b2;
            }
            if (!m.contains(i)) {
                b2 = b;
                return b2;
            }
        }
        return false;
    }
}

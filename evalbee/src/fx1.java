import android.view.accessibility.AccessibilityManager;
import android.view.MotionEvent;
import android.util.Log;
import android.text.TextUtils;
import android.view.ViewConfiguration;
import android.view.View;
import android.view.View$OnAttachStateChangeListener;
import android.view.View$OnHoverListener;
import android.view.View$OnLongClickListener;

// 
// Decompiled by Procyon v0.6.0
// 

public class fx1 implements View$OnLongClickListener, View$OnHoverListener, View$OnAttachStateChangeListener
{
    public static fx1 k;
    public static fx1 l;
    public final View a;
    public final CharSequence b;
    public final int c;
    public final Runnable d;
    public final Runnable e;
    public int f;
    public int g;
    public gx1 h;
    public boolean i;
    public boolean j;
    
    public fx1(final View a, final CharSequence b) {
        this.d = new dx1(this);
        this.e = new ex1(this);
        this.a = a;
        this.b = b;
        this.c = q32.c(ViewConfiguration.get(a.getContext()));
        this.c();
        a.setOnLongClickListener((View$OnLongClickListener)this);
        a.setOnHoverListener((View$OnHoverListener)this);
    }
    
    public static void g(final fx1 k) {
        final fx1 i = fx1.k;
        if (i != null) {
            i.b();
        }
        if ((fx1.k = k) != null) {
            k.f();
        }
    }
    
    public static void h(final View view, final CharSequence charSequence) {
        final fx1 k = fx1.k;
        if (k != null && k.a == view) {
            g(null);
        }
        if (TextUtils.isEmpty(charSequence)) {
            final fx1 l = fx1.l;
            if (l != null && l.a == view) {
                l.d();
            }
            view.setOnLongClickListener((View$OnLongClickListener)null);
            view.setLongClickable(false);
            view.setOnHoverListener((View$OnHoverListener)null);
        }
        else {
            new fx1(view, charSequence);
        }
    }
    
    public final void b() {
        this.a.removeCallbacks(this.d);
    }
    
    public final void c() {
        this.j = true;
    }
    
    public void d() {
        if (fx1.l == this) {
            fx1.l = null;
            final gx1 h = this.h;
            if (h != null) {
                h.c();
                this.h = null;
                this.c();
                this.a.removeOnAttachStateChangeListener((View$OnAttachStateChangeListener)this);
            }
            else {
                Log.e("TooltipCompatHandler", "sActiveHandler.mPopup == null");
            }
        }
        if (fx1.k == this) {
            g(null);
        }
        this.a.removeCallbacks(this.e);
    }
    
    public final void f() {
        this.a.postDelayed(this.d, (long)ViewConfiguration.getLongPressTimeout());
    }
    
    public void i(final boolean i) {
        if (!o32.T(this.a)) {
            return;
        }
        g(null);
        final fx1 l = fx1.l;
        if (l != null) {
            l.d();
        }
        fx1.l = this;
        this.i = i;
        (this.h = new gx1(this.a.getContext())).e(this.a, this.f, this.g, this.i, this.b);
        this.a.addOnAttachStateChangeListener((View$OnAttachStateChangeListener)this);
        long n;
        if (this.i) {
            n = 2500L;
        }
        else {
            long n2;
            long n3;
            if ((o32.M(this.a) & 0x1) == 0x1) {
                n2 = ViewConfiguration.getLongPressTimeout();
                n3 = 3000L;
            }
            else {
                n2 = ViewConfiguration.getLongPressTimeout();
                n3 = 15000L;
            }
            n = n3 - n2;
        }
        this.a.removeCallbacks(this.e);
        this.a.postDelayed(this.e, n);
    }
    
    public final boolean j(final MotionEvent motionEvent) {
        final int f = (int)motionEvent.getX();
        final int g = (int)motionEvent.getY();
        if (!this.j && Math.abs(f - this.f) <= this.c && Math.abs(g - this.g) <= this.c) {
            return false;
        }
        this.f = f;
        this.g = g;
        this.j = false;
        return true;
    }
    
    public boolean onHover(final View view, final MotionEvent motionEvent) {
        if (this.h != null && this.i) {
            return false;
        }
        final AccessibilityManager accessibilityManager = (AccessibilityManager)this.a.getContext().getSystemService("accessibility");
        if (accessibilityManager.isEnabled() && accessibilityManager.isTouchExplorationEnabled()) {
            return false;
        }
        final int action = motionEvent.getAction();
        if (action != 7) {
            if (action == 10) {
                this.c();
                this.d();
            }
        }
        else if (this.a.isEnabled() && this.h == null && this.j(motionEvent)) {
            g(this);
        }
        return false;
    }
    
    public boolean onLongClick(final View view) {
        this.f = view.getWidth() / 2;
        this.g = view.getHeight() / 2;
        this.i(true);
        return true;
    }
    
    public void onViewAttachedToWindow(final View view) {
    }
    
    public void onViewDetachedFromWindow(final View view) {
        this.d();
    }
}

import java.util.Iterator;
import android.util.Log;
import java.util.Collection;
import com.google.android.gms.common.api.internal.LifecycleActivity;
import java.util.ArrayList;
import com.google.android.gms.common.api.internal.LifecycleFragment;
import java.util.List;
import com.google.android.gms.common.api.internal.LifecycleCallback;
import android.app.Activity;
import java.util.HashMap;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public class i2
{
    public static final i2 c;
    public final Map a;
    public final Object b;
    
    static {
        c = new i2();
    }
    
    public i2() {
        this.a = new HashMap();
        this.b = new Object();
    }
    
    public static i2 a() {
        return i2.c;
    }
    
    public void b(final Object o) {
        synchronized (this.b) {
            final a a = this.a.get(o);
            if (a != null) {
                i2.b.b(a.a()).c(a);
            }
        }
    }
    
    public void c(final Activity activity, final Object o, final Runnable runnable) {
        synchronized (this.b) {
            final a a = new a(activity, runnable, o);
            i2.b.b(activity).a(a);
            this.a.put(o, a);
        }
    }
    
    public static class a
    {
        public final Activity a;
        public final Runnable b;
        public final Object c;
        
        public a(final Activity a, final Runnable b, final Object c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
        
        public Activity a() {
            return this.a;
        }
        
        public Object b() {
            return this.c;
        }
        
        public Runnable c() {
            return this.b;
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof a;
            final boolean b2 = false;
            if (!b) {
                return false;
            }
            final a a = (a)o;
            boolean b3 = b2;
            if (a.c.equals(this.c)) {
                b3 = b2;
                if (a.b == this.b) {
                    b3 = b2;
                    if (a.a == this.a) {
                        b3 = true;
                    }
                }
            }
            return b3;
        }
        
        @Override
        public int hashCode() {
            return this.c.hashCode();
        }
    }
    
    public static class b extends LifecycleCallback
    {
        public final List a;
        
        public b(final LifecycleFragment lifecycleFragment) {
            super(lifecycleFragment);
            this.a = new ArrayList();
            super.mLifecycleFragment.addCallback("StorageOnStopCallback", this);
        }
        
        public static b b(final Activity activity) {
            final LifecycleFragment fragment = LifecycleCallback.getFragment(new LifecycleActivity(activity));
            b b;
            if ((b = fragment.getCallbackOrNull("StorageOnStopCallback", b.class)) == null) {
                b = new b(fragment);
            }
            return b;
        }
        
        public void a(final a a) {
            synchronized (this.a) {
                this.a.add(a);
            }
        }
        
        public void c(final a a) {
            synchronized (this.a) {
                this.a.remove(a);
            }
        }
        
        @Override
        public void onStop() {
            Object o = this.a;
            synchronized (o) {
                final ArrayList list = new ArrayList(this.a);
                this.a.clear();
                monitorexit(o);
                o = list.iterator();
                while (((Iterator)o).hasNext()) {
                    final a a = (a)((Iterator)o).next();
                    if (a != null) {
                        Log.d("StorageOnStopCallback", "removing subscription from activity.");
                        a.c().run();
                        i2.a().b(a.b());
                    }
                }
            }
        }
    }
}

import com.google.android.gms.internal.firebase-auth-api.zzafw;

// 
// Decompiled by Procyon v0.6.0
// 

public final class hj2
{
    public final int a;
    public final String b;
    public final String c;
    public final x1 d;
    
    public hj2(final zzafw zzafw) {
        String b;
        if (((com.google.android.gms.internal.firebase_auth_api.zzafw)zzafw).zzg()) {
            b = ((com.google.android.gms.internal.firebase_auth_api.zzafw)zzafw).zzc();
        }
        else {
            b = ((com.google.android.gms.internal.firebase_auth_api.zzafw)zzafw).zzb();
        }
        this.b = b;
        this.c = ((com.google.android.gms.internal.firebase_auth_api.zzafw)zzafw).zzb();
        final boolean zzh = ((com.google.android.gms.internal.firebase_auth_api.zzafw)zzafw).zzh();
        x1 d = null;
        if (!zzh) {
            this.a = 3;
            this.d = null;
            return;
        }
        final String zzd = ((com.google.android.gms.internal.firebase_auth_api.zzafw)zzafw).zzd();
        zzd.hashCode();
        final int hashCode = zzd.hashCode();
        int a = 5;
        int n = -1;
        switch (hashCode) {
            case 970484929: {
                if (!zzd.equals("RECOVER_EMAIL")) {
                    break;
                }
                n = 5;
                break;
            }
            case 870738373: {
                if (!zzd.equals("EMAIL_SIGNIN")) {
                    break;
                }
                n = 4;
                break;
            }
            case -1099157829: {
                if (!zzd.equals("VERIFY_AND_CHANGE_EMAIL")) {
                    break;
                }
                n = 3;
                break;
            }
            case -1341836234: {
                if (!zzd.equals("VERIFY_EMAIL")) {
                    break;
                }
                n = 2;
                break;
            }
            case -1452371317: {
                if (!zzd.equals("PASSWORD_RESET")) {
                    break;
                }
                n = 1;
                break;
            }
            case -1874510116: {
                if (!zzd.equals("REVERT_SECOND_FACTOR_ADDITION")) {
                    break;
                }
                n = 0;
                break;
            }
        }
        while (true) {
            switch (n) {
                default: {
                    a = 3;
                }
                case 3: {
                    this.a = a;
                    if (a != 4 && a != 3) {
                        if (((com.google.android.gms.internal.firebase_auth_api.zzafw)zzafw).zzf()) {
                            d = new pi2(((com.google.android.gms.internal.firebase_auth_api.zzafw)zzafw).zzb(), wc2.a(((com.google.android.gms.internal.firebase_auth_api.zzafw)zzafw).zza()));
                        }
                        else if (((com.google.android.gms.internal.firebase_auth_api.zzafw)zzafw).zzg()) {
                            d = new jg2(((com.google.android.gms.internal.firebase_auth_api.zzafw)zzafw).zzc(), ((com.google.android.gms.internal.firebase_auth_api.zzafw)zzafw).zzb());
                        }
                        else if (((com.google.android.gms.internal.firebase_auth_api.zzafw)zzafw).zze()) {
                            d = new zi2(((com.google.android.gms.internal.firebase_auth_api.zzafw)zzafw).zzb());
                        }
                        this.d = d;
                        return;
                    }
                    this.d = null;
                    return;
                }
                case 5: {
                    a = 2;
                    continue;
                }
                case 4: {
                    a = 4;
                    continue;
                }
                case 2: {
                    a = 1;
                    continue;
                }
                case 1: {
                    a = 0;
                    continue;
                }
                case 0: {
                    a = 6;
                    continue;
                }
            }
            break;
        }
    }
    
    public final int a() {
        return this.a;
    }
}

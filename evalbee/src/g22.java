import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import android.text.TextUtils;
import android.net.Uri;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

// 
// Decompiled by Procyon v0.6.0
// 

public class g22 extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<g22> CREATOR;
    public String a;
    public String b;
    public boolean c;
    public boolean d;
    public Uri e;
    
    static {
        CREATOR = (Parcelable$Creator)new hc2();
    }
    
    public g22(final String a, final String b, final boolean c, final boolean d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        Uri parse;
        if (TextUtils.isEmpty((CharSequence)b)) {
            parse = null;
        }
        else {
            parse = Uri.parse(b);
        }
        this.e = parse;
    }
    
    public String getDisplayName() {
        return this.a;
    }
    
    public Uri i() {
        return this.e;
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 2, this.getDisplayName(), false);
        SafeParcelWriter.writeString(parcel, 3, this.b, false);
        SafeParcelWriter.writeBoolean(parcel, 4, this.c);
        SafeParcelWriter.writeBoolean(parcel, 5, this.d);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public final String zza() {
        return this.b;
    }
    
    public final boolean zzb() {
        return this.c;
    }
    
    public final boolean zzc() {
        return this.d;
    }
    
    public static class a
    {
        public String a;
        public Uri b;
        public boolean c;
        public boolean d;
        
        public g22 a() {
            final String a = this.a;
            final Uri b = this.b;
            String string;
            if (b == null) {
                string = null;
            }
            else {
                string = b.toString();
            }
            return new g22(a, string, this.c, this.d);
        }
        
        public a b(final String a) {
            if (a == null) {
                this.c = true;
            }
            else {
                this.a = a;
            }
            return this;
        }
    }
}

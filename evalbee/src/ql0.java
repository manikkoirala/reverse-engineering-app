import java.util.Locale;
import java.util.LinkedHashSet;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class ql0
{
    public static nl0 a(final nl0 nl0, final nl0 nl2) {
        final LinkedHashSet set = new LinkedHashSet();
        for (int i = 0; i < nl0.f() + nl2.f(); ++i) {
            Locale locale;
            if (i < nl0.f()) {
                locale = nl0.c(i);
            }
            else {
                locale = nl2.c(i - nl0.f());
            }
            if (locale != null) {
                set.add(locale);
            }
        }
        return nl0.a((Locale[])set.toArray(new Locale[set.size()]));
    }
    
    public static nl0 b(final nl0 nl0, final nl0 nl2) {
        if (nl0 != null && !nl0.e()) {
            return a(nl0, nl2);
        }
        return nl0.d();
    }
}

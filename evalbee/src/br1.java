import com.google.firebase.concurrent.FirebaseExecutors;
import java.util.concurrent.Executor;

// 
// Decompiled by Procyon v0.6.0
// 

public class br1
{
    public static br1 a;
    public static Executor b;
    public static Executor c;
    public static Executor d;
    public static Executor e;
    public static Executor f;
    
    static {
        br1.a = new br1();
    }
    
    public static br1 a() {
        return br1.a;
    }
    
    public static void c(final Executor executor, final Executor f) {
        br1.b = FirebaseExecutors.a(executor, 5);
        br1.d = FirebaseExecutors.a(executor, 3);
        br1.c = FirebaseExecutors.a(executor, 2);
        br1.e = FirebaseExecutors.b(executor);
        br1.f = f;
    }
    
    public Executor b() {
        return br1.f;
    }
    
    public void d(final Runnable runnable) {
        br1.e.execute(runnable);
    }
    
    public void e(final Runnable runnable) {
        br1.b.execute(runnable);
    }
    
    public void f(final Runnable runnable) {
        br1.d.execute(runnable);
    }
    
    public void g(final Runnable runnable) {
        br1.c.execute(runnable);
    }
}

import org.json.JSONObject;

// 
// Decompiled by Procyon v0.6.0
// 

public class wm1
{
    public final io a;
    
    public wm1(final io a) {
        this.a = a;
    }
    
    public static xm1 a(final int i) {
        if (i != 3) {
            final zl0 f = zl0.f();
            final StringBuilder sb = new StringBuilder();
            sb.append("Could not determine SettingsJsonTransform for settings version ");
            sb.append(i);
            sb.append(". Using default settings values.");
            f.d(sb.toString());
            return new br();
        }
        return new cn1();
    }
    
    public vm1 b(final JSONObject jsonObject) {
        return a(jsonObject.getInt("settings_version")).a(this.a, jsonObject);
    }
}

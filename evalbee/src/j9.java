import com.google.firebase.firestore.FirebaseFirestoreException;
import java.util.concurrent.Executor;

// 
// Decompiled by Procyon v0.6.0
// 

public class j9 implements rx
{
    public final Executor a;
    public final rx b;
    public volatile boolean c;
    
    public j9(final Executor a, final rx b) {
        this.c = false;
        this.a = a;
        this.b = b;
    }
    
    @Override
    public void a(final Object o, final FirebaseFirestoreException ex) {
        this.a.execute(new i9(this, o, ex));
    }
    
    public void d() {
        this.c = true;
    }
}

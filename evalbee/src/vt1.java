import java.util.Collection;
import java.util.TreeMap;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Set;
import java.util.List;
import android.database.Cursor;
import java.io.Closeable;
import kotlin.collections.b;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class vt1
{
    public static final Map a(ss1 h0, String string) {
        final StringBuilder sb = new StringBuilder();
        sb.append("PRAGMA table_info(`");
        sb.append((String)string);
        sb.append("`)");
        h0 = (ss1)h0.h0(sb.toString());
        try {
            if (((Cursor)h0).getColumnCount() <= 0) {
                final Map h2 = b.h();
                dh.a((Closeable)h0, (Throwable)null);
                return h2;
            }
            final int columnIndex = ((Cursor)h0).getColumnIndex("name");
            final int columnIndex2 = ((Cursor)h0).getColumnIndex("type");
            final int columnIndex3 = ((Cursor)h0).getColumnIndex("notnull");
            final int columnIndex4 = ((Cursor)h0).getColumnIndex("pk");
            final int columnIndex5 = ((Cursor)h0).getColumnIndex("dflt_value");
            final Map c = en0.c();
            while (((Cursor)h0).moveToNext()) {
                final String string2 = ((Cursor)h0).getString(columnIndex);
                final String string3 = ((Cursor)h0).getString(columnIndex2);
                final boolean b = ((Cursor)h0).getInt(columnIndex3) != 0;
                final int int1 = ((Cursor)h0).getInt(columnIndex4);
                string = ((Cursor)h0).getString(columnIndex5);
                fg0.d((Object)string2, "name");
                fg0.d((Object)string3, "type");
                c.put(string2, new ut1.a(string2, string3, b, int1, (String)string, 2));
            }
            final Map b2 = en0.b(c);
            dh.a((Closeable)h0, (Throwable)null);
            return b2;
        }
        finally {
            try {}
            finally {
                dh.a((Closeable)h0, (Throwable)string);
            }
        }
    }
    
    public static final List b(final Cursor cursor) {
        final int columnIndex = cursor.getColumnIndex("id");
        final int columnIndex2 = cursor.getColumnIndex("seq");
        final int columnIndex3 = cursor.getColumnIndex("from");
        final int columnIndex4 = cursor.getColumnIndex("to");
        final List c = mh.c();
        while (cursor.moveToNext()) {
            final int int1 = cursor.getInt(columnIndex);
            final int int2 = cursor.getInt(columnIndex2);
            final String string = cursor.getString(columnIndex3);
            fg0.d((Object)string, "cursor.getString(fromColumnIndex)");
            final String string2 = cursor.getString(columnIndex4);
            fg0.d((Object)string2, "cursor.getString(toColumnIndex)");
            c.add(new ut1.d(int1, int2, string, string2));
        }
        return vh.J((Iterable)mh.a(c));
    }
    
    public static final Set c(ss1 h0, final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append("PRAGMA foreign_key_list(`");
        sb.append(str);
        sb.append("`)");
        h0 = (ss1)h0.h0(sb.toString());
        try {
            final int columnIndex = ((Cursor)h0).getColumnIndex("id");
            final int columnIndex2 = ((Cursor)h0).getColumnIndex("seq");
            final int columnIndex3 = ((Cursor)h0).getColumnIndex("table");
            final int columnIndex4 = ((Cursor)h0).getColumnIndex("on_delete");
            final int columnIndex5 = ((Cursor)h0).getColumnIndex("on_update");
            final List b = b((Cursor)h0);
            ((Cursor)h0).moveToPosition(-1);
            final Set b2 = sm1.b();
            while (((Cursor)h0).moveToNext()) {
                if (((Cursor)h0).getInt(columnIndex2) != 0) {
                    continue;
                }
                final int int1 = ((Cursor)h0).getInt(columnIndex);
                final ArrayList list = new ArrayList();
                final ArrayList list2 = new ArrayList();
                final Iterable iterable = b;
                final ArrayList list3 = new ArrayList();
                for (final Object next : iterable) {
                    if (((ut1.d)next).d() == int1) {
                        list3.add(next);
                    }
                }
                for (final ut1.d d : list3) {
                    list.add(d.c());
                    list2.add(d.e());
                }
                final String string = ((Cursor)h0).getString(columnIndex3);
                fg0.d((Object)string, "cursor.getString(tableColumnIndex)");
                final String string2 = ((Cursor)h0).getString(columnIndex4);
                fg0.d((Object)string2, "cursor.getString(onDeleteColumnIndex)");
                final String string3 = ((Cursor)h0).getString(columnIndex5);
                fg0.d((Object)string3, "cursor.getString(onUpdateColumnIndex)");
                b2.add(new ut1.c(string, string2, string3, list, list2));
            }
            final Set a = sm1.a(b2);
            dh.a((Closeable)h0, (Throwable)null);
            return a;
        }
        finally {
            try {}
            finally {
                final Throwable t;
                dh.a((Closeable)h0, t);
            }
        }
    }
    
    public static final ut1.e d(final ss1 ss1, final String str, final boolean b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("PRAGMA index_xinfo(`");
        sb.append(str);
        sb.append("`)");
        final Cursor h0 = ss1.h0(sb.toString());
        try {
            final int columnIndex = h0.getColumnIndex("seqno");
            final int columnIndex2 = h0.getColumnIndex("cid");
            final int columnIndex3 = h0.getColumnIndex("name");
            final int columnIndex4 = h0.getColumnIndex("desc");
            if (columnIndex != -1 && columnIndex2 != -1 && columnIndex3 != -1 && columnIndex4 != -1) {
                final TreeMap treeMap = new TreeMap();
                final TreeMap treeMap2 = new TreeMap();
                while (h0.moveToNext()) {
                    if (h0.getInt(columnIndex2) < 0) {
                        continue;
                    }
                    final int int1 = h0.getInt(columnIndex);
                    final String string = h0.getString(columnIndex3);
                    String s;
                    if (h0.getInt(columnIndex4) > 0) {
                        s = "DESC";
                    }
                    else {
                        s = "ASC";
                    }
                    fg0.d((Object)string, "columnName");
                    treeMap.put(int1, string);
                    treeMap2.put(int1, s);
                }
                final Collection values = treeMap.values();
                fg0.d((Object)values, "columnsMap.values");
                final List p3 = vh.P((Iterable)values);
                final Collection values2 = treeMap2.values();
                fg0.d((Object)values2, "ordersMap.values");
                final ut1.e e = new ut1.e(str, b, p3, vh.P((Iterable)values2));
                dh.a((Closeable)h0, (Throwable)null);
                return e;
            }
            dh.a((Closeable)h0, (Throwable)null);
            return null;
        }
        finally {
            try {}
            finally {
                dh.a((Closeable)h0, (Throwable)ss1);
            }
        }
    }
    
    public static final Set e(final ss1 ss1, String h0) {
        final StringBuilder sb = new StringBuilder();
        sb.append("PRAGMA index_list(`");
        sb.append(h0);
        sb.append("`)");
        h0 = (String)ss1.h0(sb.toString());
        try {
            final int columnIndex = ((Cursor)h0).getColumnIndex("name");
            final int columnIndex2 = ((Cursor)h0).getColumnIndex("origin");
            final int columnIndex3 = ((Cursor)h0).getColumnIndex("unique");
            if (columnIndex != -1 && columnIndex2 != -1 && columnIndex3 != -1) {
                final Set b = sm1.b();
                while (((Cursor)h0).moveToNext()) {
                    if (!fg0.a((Object)"c", (Object)((Cursor)h0).getString(columnIndex2))) {
                        continue;
                    }
                    final String string = ((Cursor)h0).getString(columnIndex);
                    final int int1 = ((Cursor)h0).getInt(columnIndex3);
                    boolean b2 = true;
                    if (int1 != 1) {
                        b2 = false;
                    }
                    fg0.d((Object)string, "name");
                    final ut1.e d = d(ss1, string, b2);
                    if (d == null) {
                        dh.a((Closeable)h0, (Throwable)null);
                        return null;
                    }
                    b.add(d);
                }
                final Set a = sm1.a(b);
                dh.a((Closeable)h0, (Throwable)null);
                return a;
            }
            dh.a((Closeable)h0, (Throwable)null);
            return null;
        }
        finally {
            try {}
            finally {
                final Throwable t;
                dh.a((Closeable)h0, t);
            }
        }
    }
    
    public static final ut1 f(final ss1 ss1, final String s) {
        fg0.e((Object)ss1, "database");
        fg0.e((Object)s, "tableName");
        return new ut1(s, a(ss1, s), c(ss1, s), e(ss1, s));
    }
}

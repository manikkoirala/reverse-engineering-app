import androidx.appcompat.view.menu.e;
import androidx.appcompat.widget.c;
import android.view.ViewGroup;
import android.view.Window$Callback;
import android.graphics.drawable.Drawable;
import android.content.Context;
import androidx.appcompat.view.menu.i;
import android.view.Menu;

// 
// Decompiled by Procyon v0.6.0
// 

public interface dq
{
    boolean a();
    
    boolean b();
    
    boolean c();
    
    void collapseActionView();
    
    boolean d();
    
    void e(final Menu p0, final i.a p1);
    
    void f();
    
    boolean g();
    
    Context getContext();
    
    CharSequence getTitle();
    
    boolean h();
    
    void i(final int p0);
    
    int j();
    
    void k();
    
    void l(final boolean p0);
    
    void m();
    
    void n(final int p0);
    
    int o();
    
    void p();
    
    void q(final CharSequence p0);
    
    Menu r();
    
    i42 s(final int p0, final long p1);
    
    void setIcon(final int p0);
    
    void setIcon(final Drawable p0);
    
    void setTitle(final CharSequence p0);
    
    void setWindowCallback(final Window$Callback p0);
    
    void setWindowTitle(final CharSequence p0);
    
    ViewGroup t();
    
    void u(final boolean p0);
    
    void v(final c p0);
    
    void w(final int p0);
    
    void x(final i.a p0, final e.a p1);
}

import java.lang.reflect.Field;
import java.util.Iterator;
import com.google.gson.reflect.TypeToken;
import java.util.Collections;
import java.util.List;

// 
// Decompiled by Procyon v0.6.0
// 

public final class oy implements iz1, Cloneable
{
    public static final oy g;
    public double a;
    public int b;
    public boolean c;
    public boolean d;
    public List e;
    public List f;
    
    static {
        g = new oy();
    }
    
    public oy() {
        this.a = -1.0;
        this.b = 136;
        this.c = true;
        this.e = Collections.emptyList();
        this.f = Collections.emptyList();
    }
    
    @Override
    public hz1 a(final gc0 gc0, final TypeToken typeToken) {
        final Class rawType = typeToken.getRawType();
        final boolean e = this.e(rawType);
        final boolean b = e || this.f(rawType, true);
        final boolean b2 = e || this.f(rawType, false);
        if (!b && !b2) {
            return null;
        }
        return new hz1(this, b2, b, gc0, typeToken) {
            public hz1 a;
            public final boolean b;
            public final boolean c;
            public final gc0 d;
            public final TypeToken e;
            public final oy f;
            
            @Override
            public Object b(final rh0 rh0) {
                if (this.b) {
                    rh0.y0();
                    return null;
                }
                return this.e().b(rh0);
            }
            
            @Override
            public void d(final vh0 vh0, final Object o) {
                if (this.c) {
                    vh0.u();
                    return;
                }
                this.e().d(vh0, o);
            }
            
            public final hz1 e() {
                hz1 a = this.a;
                if (a == null) {
                    a = this.d.n(this.f, this.e);
                    this.a = a;
                }
                return a;
            }
        };
    }
    
    public oy b() {
        try {
            return (oy)super.clone();
        }
        catch (final CloneNotSupportedException detailMessage) {
            throw new AssertionError((Object)detailMessage);
        }
    }
    
    public boolean d(final Class clazz, final boolean b) {
        return this.e(clazz) || this.f(clazz, b);
    }
    
    public final boolean e(final Class clazz) {
        return (this.a != -1.0 && !this.n(clazz.getAnnotation(fo1.class), clazz.getAnnotation(h12.class))) || (!this.c && this.j(clazz)) || this.i(clazz);
    }
    
    public final boolean f(final Class clazz, final boolean b) {
        List list;
        if (b) {
            list = this.e;
        }
        else {
            list = this.f;
        }
        final Iterator iterator = list.iterator();
        if (!iterator.hasNext()) {
            return false;
        }
        zu0.a(iterator.next());
        throw null;
    }
    
    public boolean h(final Field field, final boolean b) {
        if ((this.b & field.getModifiers()) != 0x0) {
            return true;
        }
        if (this.a != -1.0 && !this.n(field.getAnnotation(fo1.class), field.getAnnotation(h12.class))) {
            return true;
        }
        if (field.isSynthetic()) {
            return true;
        }
        Label_0109: {
            if (this.d) {
                final nz nz = field.getAnnotation(nz.class);
                if (nz != null) {
                    if (b) {
                        if (nz.serialize()) {
                            break Label_0109;
                        }
                    }
                    else if (nz.deserialize()) {
                        break Label_0109;
                    }
                }
                return true;
            }
        }
        if (!this.c && this.j(field.getType())) {
            return true;
        }
        if (this.i(field.getType())) {
            return true;
        }
        List list;
        if (b) {
            list = this.e;
        }
        else {
            list = this.f;
        }
        if (!list.isEmpty()) {
            new m00(field);
            final Iterator iterator = list.iterator();
            if (iterator.hasNext()) {
                zu0.a(iterator.next());
                throw null;
            }
        }
        return false;
    }
    
    public final boolean i(final Class clazz) {
        return !Enum.class.isAssignableFrom(clazz) && !this.k(clazz) && (clazz.isAnonymousClass() || clazz.isLocalClass());
    }
    
    public final boolean j(final Class clazz) {
        return clazz.isMemberClass() && !this.k(clazz);
    }
    
    public final boolean k(final Class clazz) {
        return (clazz.getModifiers() & 0x8) != 0x0;
    }
    
    public final boolean l(final fo1 fo1) {
        boolean b = true;
        if (fo1 != null) {
            b = (this.a >= fo1.value() && b);
        }
        return b;
    }
    
    public final boolean m(final h12 h12) {
        boolean b = true;
        if (h12 != null) {
            b = (this.a < h12.value() && b);
        }
        return b;
    }
    
    public final boolean n(final fo1 fo1, final h12 h12) {
        return this.l(fo1) && this.m(h12);
    }
}

import android.text.Spanned;
import android.content.ClipData;
import android.widget.TextView;
import android.util.Log;
import android.view.View;
import android.text.Spannable;
import android.text.Selection;
import android.text.Editable;
import android.content.ClipData$Item;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ov1 implements p11
{
    public static CharSequence b(final Context context, final ClipData$Item clipData$Item, final int n) {
        return a.a(context, clipData$Item, n);
    }
    
    public static void c(final Editable editable, final CharSequence charSequence) {
        final int selectionStart = Selection.getSelectionStart((CharSequence)editable);
        final int selectionEnd = Selection.getSelectionEnd((CharSequence)editable);
        final int max = Math.max(0, Math.min(selectionStart, selectionEnd));
        final int max2 = Math.max(0, Math.max(selectionStart, selectionEnd));
        Selection.setSelection((Spannable)editable, max2);
        editable.replace(max, max2, charSequence);
    }
    
    @Override
    public gl a(final View view, final gl obj) {
        if (Log.isLoggable("ReceiveContent", 3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("onReceive: ");
            sb.append(obj);
            Log.d("ReceiveContent", sb.toString());
        }
        if (obj.d() == 2) {
            return obj;
        }
        final ClipData b = obj.b();
        final int c = obj.c();
        final TextView textView = (TextView)view;
        final Editable editable = (Editable)textView.getText();
        final Context context = ((View)textView).getContext();
        int i = 0;
        int n = 0;
        while (i < b.getItemCount()) {
            final CharSequence b2 = b(context, b.getItemAt(i), c);
            int n2 = n;
            if (b2 != null) {
                if (n == 0) {
                    c(editable, b2);
                    n2 = 1;
                }
                else {
                    editable.insert(Selection.getSelectionEnd((CharSequence)editable), (CharSequence)"\n");
                    editable.insert(Selection.getSelectionEnd((CharSequence)editable), b2);
                    n2 = n;
                }
            }
            ++i;
            n = n2;
        }
        return null;
    }
    
    public abstract static final class a
    {
        public static CharSequence a(final Context context, final ClipData$Item clipData$Item, final int n) {
            if ((n & 0x1) != 0x0) {
                CharSequence charSequence2;
                final CharSequence charSequence = charSequence2 = clipData$Item.coerceToText(context);
                if (charSequence instanceof Spanned) {
                    charSequence2 = charSequence.toString();
                }
                return charSequence2;
            }
            return clipData$Item.coerceToStyledText(context);
        }
    }
}

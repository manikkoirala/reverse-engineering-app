import com.google.android.gms.common.util.ProcessUtils;
import android.os.Build$VERSION;
import android.os.Process;
import java.util.Iterator;
import android.app.ActivityManager$RunningAppProcessInfo;
import java.util.ArrayList;
import android.app.ActivityManager;
import java.util.List;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public final class h81
{
    public static final h81 a;
    
    static {
        a = new h81();
    }
    
    public static /* synthetic */ f81 b(final h81 h81, final String s, int n, int n2, boolean b, final int n3, final Object o) {
        if ((n3 & 0x2) != 0x0) {
            n = 0;
        }
        if ((n3 & 0x4) != 0x0) {
            n2 = 0;
        }
        if ((n3 & 0x8) != 0x0) {
            b = false;
        }
        return h81.a(s, n, n2, b);
    }
    
    public final f81 a(final String s, final int n, final int n2, final boolean b) {
        return new f81(s, n, n2, b);
    }
    
    public final List c(final Context context) {
        fg0.e((Object)context, "context");
        final int uid = context.getApplicationInfo().uid;
        final String processName = context.getApplicationInfo().processName;
        final Object systemService = context.getSystemService("activity");
        final boolean b = systemService instanceof ActivityManager;
        List runningAppProcesses = null;
        ActivityManager activityManager;
        if (b) {
            activityManager = (ActivityManager)systemService;
        }
        else {
            activityManager = null;
        }
        if (activityManager != null) {
            runningAppProcesses = activityManager.getRunningAppProcesses();
        }
        List g;
        if ((g = runningAppProcesses) == null) {
            g = nh.g();
        }
        final Iterable iterable = vh.w((Iterable)g);
        final ArrayList list = new ArrayList();
        for (final Object next : iterable) {
            if (((ActivityManager$RunningAppProcessInfo)next).uid == uid) {
                list.add(next);
            }
        }
        final ArrayList list2 = new ArrayList(oh.o((Iterable)list, 10));
        for (final ActivityManager$RunningAppProcessInfo activityManager$RunningAppProcessInfo : list) {
            final String processName2 = activityManager$RunningAppProcessInfo.processName;
            fg0.d((Object)processName2, "runningAppProcessInfo.processName");
            list2.add((Object)new f81(processName2, activityManager$RunningAppProcessInfo.pid, activityManager$RunningAppProcessInfo.importance, fg0.a((Object)activityManager$RunningAppProcessInfo.processName, (Object)processName)));
        }
        return list2;
    }
    
    public final f81 d(final Context context) {
        fg0.e((Object)context, "context");
        final int myPid = Process.myPid();
        while (true) {
            for (final Object next : this.c(context)) {
                if (((f81)next).b() == myPid) {
                    f81 b;
                    if ((b = (f81)next) == null) {
                        b = b(this, this.e(), myPid, 0, false, 12, null);
                    }
                    return b;
                }
            }
            Object next = null;
            continue;
        }
    }
    
    public final String e() {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 33) {
            final String a = g81.a();
            fg0.d((Object)a, "myProcessName()");
            return a;
        }
        if (sdk_INT >= 28) {
            final String a2 = l81.a();
            if (a2 != null) {
                return a2;
            }
        }
        final String myProcessName = ProcessUtils.getMyProcessName();
        if (myProcessName != null) {
            return myProcessName;
        }
        return "";
    }
}

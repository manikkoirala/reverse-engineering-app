import java.util.concurrent.CancellationException;
import kotlinx.coroutines.n;
import java.util.Iterator;
import androidx.work.impl.constraints.WorkConstraintsTrackerKt;
import android.text.TextUtils;
import androidx.work.WorkInfo$State;
import java.util.HashSet;
import java.util.HashMap;
import androidx.work.impl.constraints.WorkConstraintsTracker;
import androidx.work.a;
import java.util.Map;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class qb0 implements ij1, k11, qy
{
    public static final String p;
    public final Context a;
    public final Map b;
    public or c;
    public boolean d;
    public final Object e;
    public final pp1 f;
    public final q81 g;
    public final a92 h;
    public final a i;
    public final Map j;
    public Boolean k;
    public final WorkConstraintsTracker l;
    public final hu1 m;
    public final jw1 n;
    
    static {
        p = xl0.i("GreedyScheduler");
    }
    
    public qb0(final Context a, final a i, final ky1 ky1, final q81 g, final a92 h, final hu1 m) {
        this.b = new HashMap();
        this.e = new Object();
        this.f = new pp1();
        this.j = new HashMap();
        this.a = a;
        final ag1 k = i.k();
        this.c = new or(this, k, i.a());
        this.n = new jw1(k, h);
        this.m = m;
        this.l = new WorkConstraintsTracker(ky1);
        this.i = i;
        this.g = g;
        this.h = h;
    }
    
    @Override
    public void a(final p92... array) {
        if (this.k == null) {
            this.f();
        }
        if (!this.k) {
            xl0.e().f(qb0.p, "Ignoring schedule request in a secondary process");
            return;
        }
        this.g();
        final HashSet set = new HashSet();
        final HashSet set2 = new HashSet();
        for (final p92 p92 : array) {
            Label_0424: {
                if (!this.f.a(u92.a(p92))) {
                    final long max = Math.max(p92.c(), this.i(p92));
                    final long currentTimeMillis = this.i.a().currentTimeMillis();
                    if (p92.b == WorkInfo$State.ENQUEUED) {
                        if (currentTimeMillis < max) {
                            final or c = this.c;
                            if (c != null) {
                                c.a(p92, max);
                            }
                        }
                        else if (p92.k()) {
                            xl0 xl0;
                            String s;
                            StringBuilder sb;
                            String str;
                            if (p92.j.h()) {
                                xl0 = xl0.e();
                                s = qb0.p;
                                sb = new StringBuilder();
                                sb.append("Ignoring ");
                                sb.append(p92);
                                str = ". Requires device idle.";
                            }
                            else {
                                if (!p92.j.e()) {
                                    set.add(p92);
                                    set2.add(p92.a);
                                    break Label_0424;
                                }
                                xl0 = xl0.e();
                                s = qb0.p;
                                sb = new StringBuilder();
                                sb.append("Ignoring ");
                                sb.append(p92);
                                str = ". Requires ContentUri triggers.";
                            }
                            sb.append(str);
                            xl0.a(s, sb.toString());
                        }
                        else if (!this.f.a(u92.a(p92))) {
                            final xl0 e = xl0.e();
                            final String p93 = qb0.p;
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Starting work for ");
                            sb2.append(p92.a);
                            e.a(p93, sb2.toString());
                            final op1 e2 = this.f.e(p92);
                            this.n.c(e2);
                            this.h.e(e2);
                        }
                    }
                }
            }
        }
        synchronized (this.e) {
            if (!set.isEmpty()) {
                final String join = TextUtils.join((CharSequence)",", (Iterable)set2);
                final xl0 e3 = xl0.e();
                final String p94 = qb0.p;
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Starting tracking for ");
                sb3.append(join);
                e3.a(p94, sb3.toString());
                for (final p92 p95 : set) {
                    final x82 a = u92.a(p95);
                    if (!this.b.containsKey(a)) {
                        this.b.put(a, WorkConstraintsTrackerKt.b(this.l, p95, this.m.a(), this));
                    }
                }
            }
        }
    }
    
    @Override
    public boolean b() {
        return false;
    }
    
    @Override
    public void c(final p92 p2, final androidx.work.impl.constraints.a a) {
        final x82 a2 = u92.a(p2);
        if (a instanceof androidx.work.impl.constraints.a.a) {
            if (!this.f.a(a2)) {
                final xl0 e = xl0.e();
                final String p3 = qb0.p;
                final StringBuilder sb = new StringBuilder();
                sb.append("Constraints met: Scheduling work ID ");
                sb.append(a2);
                e.a(p3, sb.toString());
                final op1 d = this.f.d(a2);
                this.n.c(d);
                this.h.e(d);
            }
        }
        else {
            final xl0 e2 = xl0.e();
            final String p4 = qb0.p;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Constraints not met: Cancelling work ID ");
            sb2.append(a2);
            e2.a(p4, sb2.toString());
            final op1 b = this.f.b(a2);
            if (b != null) {
                this.n.b(b);
                this.h.c(b, ((androidx.work.impl.constraints.a.b)a).a());
            }
        }
    }
    
    @Override
    public void d(final x82 x82, final boolean b) {
        final op1 b2 = this.f.b(x82);
        if (b2 != null) {
            this.n.b(b2);
        }
        this.h(x82);
        if (!b) {
            synchronized (this.e) {
                this.j.remove(x82);
            }
        }
    }
    
    @Override
    public void e(final String str) {
        if (this.k == null) {
            this.f();
        }
        if (!this.k) {
            xl0.e().f(qb0.p, "Ignoring schedule request in non-main process");
            return;
        }
        this.g();
        final xl0 e = xl0.e();
        final String p = qb0.p;
        final StringBuilder sb = new StringBuilder();
        sb.append("Cancelling work ID ");
        sb.append(str);
        e.a(p, sb.toString());
        final or c = this.c;
        if (c != null) {
            c.b(str);
        }
        for (final op1 op1 : this.f.c(str)) {
            this.n.b(op1);
            this.h.b(op1);
        }
    }
    
    public final void f() {
        this.k = m81.b(this.a, this.i);
    }
    
    public final void g() {
        if (!this.d) {
            this.g.e(this);
            this.d = true;
        }
    }
    
    public final void h(final x82 obj) {
        Object e = this.e;
        synchronized (e) {
            final n n = this.b.remove(obj);
            monitorexit(e);
            if (n != null) {
                final xl0 e2 = xl0.e();
                final String p = qb0.p;
                e = new StringBuilder();
                ((StringBuilder)e).append("Stopping tracking for ");
                ((StringBuilder)e).append(obj);
                e2.a(p, ((StringBuilder)e).toString());
                n.b((CancellationException)null);
            }
        }
    }
    
    public final long i(final p92 p) {
        synchronized (this.e) {
            final x82 a = u92.a(p);
            b b;
            if ((b = this.j.get(a)) == null) {
                b = new b(p.k, this.i.a().currentTimeMillis(), null);
                this.j.put(a, b);
            }
            final long b2 = b.b;
            final long n = Math.max(p.k - b.a - 5, 0);
            monitorexit(this.e);
            return b2 + n * 30000L;
        }
    }
    
    public static class b
    {
        public final int a;
        public final long b;
        
        public b(final int a, final long b) {
            this.a = a;
            this.b = b;
        }
    }
}

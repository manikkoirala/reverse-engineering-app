import android.widget.AdapterView;
import android.widget.AbsListView;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.view.View;
import androidx.appcompat.view.menu.ListMenuItemView;
import android.view.KeyEvent;
import android.widget.ListAdapter;
import androidx.appcompat.view.menu.d;
import android.widget.HeaderViewListAdapter;
import android.view.MotionEvent;
import android.view.MenuItem;
import androidx.appcompat.view.menu.e;
import android.transition.Transition;
import android.util.AttributeSet;
import android.content.Context;
import android.util.Log;
import android.widget.PopupWindow;
import android.os.Build$VERSION;
import java.lang.reflect.Method;

// 
// Decompiled by Procyon v0.6.0
// 

public class qv0 extends ek0 implements nv0
{
    public static Method U;
    public nv0 Q;
    
    static {
        try {
            if (Build$VERSION.SDK_INT <= 28) {
                qv0.U = PopupWindow.class.getDeclaredMethod("setTouchModal", Boolean.TYPE);
            }
        }
        catch (final NoSuchMethodException ex) {
            Log.i("MenuPopupWindow", "Could not find method setTouchModal() on PopupWindow. Oh well.");
        }
    }
    
    public qv0(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
    }
    
    public void P(final Object o) {
        a.a(super.M, (Transition)o);
    }
    
    public void Q(final Object o) {
        a.b(super.M, (Transition)o);
    }
    
    public void R(final nv0 q) {
        this.Q = q;
    }
    
    public void S(final boolean b) {
        if (Build$VERSION.SDK_INT <= 28) {
            final Method u = qv0.U;
            if (u != null) {
                try {
                    u.invoke(super.M, b);
                }
                catch (final Exception ex) {
                    Log.i("MenuPopupWindow", "Could not invoke setTouchModal() on PopupWindow. Oh well.");
                }
            }
        }
        else {
            qv0.b.a(super.M, b);
        }
    }
    
    @Override
    public void b(final androidx.appcompat.view.menu.e e, final MenuItem menuItem) {
        final nv0 q = this.Q;
        if (q != null) {
            q.b(e, menuItem);
        }
    }
    
    @Override
    public void g(final androidx.appcompat.view.menu.e e, final MenuItem menuItem) {
        final nv0 q = this.Q;
        if (q != null) {
            q.g(e, menuItem);
        }
    }
    
    @Override
    public kv p(final Context context, final boolean b) {
        final c c = new c(context, b);
        c.setHoverListener(this);
        return c;
    }
    
    public abstract static class a
    {
        public static void a(final PopupWindow popupWindow, final Transition enterTransition) {
            popupWindow.setEnterTransition(enterTransition);
        }
        
        public static void b(final PopupWindow popupWindow, final Transition exitTransition) {
            popupWindow.setExitTransition(exitTransition);
        }
    }
    
    public abstract static class b
    {
        public static void a(final PopupWindow popupWindow, final boolean touchModal) {
            popupWindow.setTouchModal(touchModal);
        }
    }
    
    public static class c extends kv
    {
        public final int n;
        public final int p;
        public nv0 q;
        public MenuItem t;
        
        public c(final Context context, final boolean b) {
            super(context, b);
            if (1 == a.a(context.getResources().getConfiguration())) {
                this.n = 21;
                this.p = 22;
            }
            else {
                this.n = 22;
                this.p = 21;
            }
        }
        
        @Override
        public boolean onHoverEvent(final MotionEvent motionEvent) {
            if (this.q != null) {
                final ListAdapter adapter = this.getAdapter();
                int headersCount;
                d d;
                if (adapter instanceof HeaderViewListAdapter) {
                    final HeaderViewListAdapter headerViewListAdapter = (HeaderViewListAdapter)adapter;
                    headersCount = headerViewListAdapter.getHeadersCount();
                    d = (d)headerViewListAdapter.getWrappedAdapter();
                }
                else {
                    d = (d)adapter;
                    headersCount = 0;
                }
                Object c = null;
                Label_0116: {
                    if (motionEvent.getAction() != 10) {
                        final int pointToPosition = ((AbsListView)this).pointToPosition((int)motionEvent.getX(), (int)motionEvent.getY());
                        if (pointToPosition != -1) {
                            final int n = pointToPosition - headersCount;
                            if (n >= 0 && n < d.getCount()) {
                                c = d.c(n);
                                break Label_0116;
                            }
                        }
                    }
                    c = null;
                }
                final MenuItem t = this.t;
                if (t != c) {
                    final e b = d.b();
                    if (t != null) {
                        this.q.g(b, t);
                    }
                    if ((this.t = (MenuItem)c) != null) {
                        this.q.b(b, (MenuItem)c);
                    }
                }
            }
            return super.onHoverEvent(motionEvent);
        }
        
        public boolean onKeyDown(final int n, final KeyEvent keyEvent) {
            final ListMenuItemView listMenuItemView = (ListMenuItemView)((AdapterView)this).getSelectedView();
            if (listMenuItemView != null && n == this.n) {
                if (((View)listMenuItemView).isEnabled() && listMenuItemView.getItemData().hasSubMenu()) {
                    ((AdapterView)this).performItemClick((View)listMenuItemView, ((AdapterView)this).getSelectedItemPosition(), ((AdapterView)this).getSelectedItemId());
                }
                return true;
            }
            if (listMenuItemView != null && n == this.p) {
                ((AdapterView)this).setSelection(-1);
                ListAdapter listAdapter2;
                final ListAdapter listAdapter = listAdapter2 = this.getAdapter();
                if (listAdapter instanceof HeaderViewListAdapter) {
                    listAdapter2 = ((HeaderViewListAdapter)listAdapter).getWrappedAdapter();
                }
                ((d)listAdapter2).b().close(false);
                return true;
            }
            return super.onKeyDown(n, keyEvent);
        }
        
        public void setHoverListener(final nv0 q) {
            this.q = q;
        }
        
        public abstract static class a
        {
            public static int a(final Configuration configuration) {
                return configuration.getLayoutDirection();
            }
        }
    }
}

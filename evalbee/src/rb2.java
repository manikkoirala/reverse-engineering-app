import android.os.BaseBundle;
import com.google.android.gms.internal.play_billing.zziz;
import java.util.concurrent.Callable;
import com.google.android.gms.internal.play_billing.zzl;
import android.os.IBinder;
import android.content.ComponentName;
import com.android.billingclient.api.a;
import com.android.billingclient.api.b;
import com.google.android.gms.internal.play_billing.zzm;
import com.google.android.gms.internal.play_billing.zzb;
import android.os.Bundle;
import android.text.TextUtils;
import android.content.ServiceConnection;

// 
// Decompiled by Procyon v0.6.0
// 

public final class rb2 implements ServiceConnection
{
    public final Object a = new Object();
    public boolean b = false;
    public yb c = c;
    public final xb d;
    
    public final void c() {
        synchronized (this.a) {
            this.c = null;
            this.b = true;
        }
    }
    
    public final void d(final a a) {
        synchronized (this.a) {
            final yb c = this.c;
            if (c != null) {
                c.a(a);
            }
        }
    }
    
    public final void onServiceConnected(final ComponentName componentName, final IBinder binder) {
        zzb.zzj("BillingClient", "Billing service connected.");
        xb.r(this.d, zzl.zzr(binder));
        final lb2 lb2 = new lb2(this);
        final nb2 nb2 = new nb2(this);
        final xb d = this.d;
        if (xb.O(d, (Callable)lb2, 30000L, (Runnable)nb2, xb.I(d)) == null) {
            final xb d2 = this.d;
            final a l = xb.L(d2);
            xb.K(d2).a(bd2.a(25, 6, l));
            this.d(l);
        }
    }
    
    public final void onServiceDisconnected(final ComponentName componentName) {
        zzb.zzk("BillingClient", "Billing service disconnected.");
        xb.K(this.d).b(zziz.zzw());
        xb.r(this.d, (zzm)null);
        xb.P(this.d, 0);
        synchronized (this.a) {
            final yb c = this.c;
            if (c != null) {
                c.b();
            }
        }
    }
}

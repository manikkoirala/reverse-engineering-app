import org.json.JSONObject;

// 
// Decompiled by Procyon v0.6.0
// 

public class cn1 implements xm1
{
    public static vm1.a b(final JSONObject jsonObject) {
        return new vm1.a(jsonObject.optBoolean("collect_reports", true), jsonObject.optBoolean("collect_anrs", false), jsonObject.optBoolean("collect_build_ids", false));
    }
    
    public static vm1.b c(final JSONObject jsonObject) {
        return new vm1.b(jsonObject.optInt("max_custom_exception_events", 8), 4);
    }
    
    public static long d(final io io, long optLong, final JSONObject jsonObject) {
        if (jsonObject.has("expires_at")) {
            optLong = jsonObject.optLong("expires_at");
        }
        else {
            optLong = io.a() + optLong * 1000L;
        }
        return optLong;
    }
    
    @Override
    public vm1 a(final io io, final JSONObject jsonObject) {
        final int optInt = jsonObject.optInt("settings_version", 0);
        final int optInt2 = jsonObject.optInt("cache_duration", 3600);
        final double optDouble = jsonObject.optDouble("on_demand_upload_rate_per_minute", 10.0);
        final double optDouble2 = jsonObject.optDouble("on_demand_backoff_base", 1.2);
        final int optInt3 = jsonObject.optInt("on_demand_backoff_step_duration_seconds", 60);
        JSONObject jsonObject2;
        if (jsonObject.has("session")) {
            jsonObject2 = jsonObject.getJSONObject("session");
        }
        else {
            jsonObject2 = new JSONObject();
        }
        return new vm1(d(io, optInt2, jsonObject), c(jsonObject2), b(jsonObject.getJSONObject("features")), optInt, optInt2, optDouble, optDouble2, optInt3);
    }
}

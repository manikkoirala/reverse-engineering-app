import android.util.Log;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class xl0
{
    public static final Object a;
    public static volatile xl0 b;
    public static final int c = 20;
    
    static {
        a = new Object();
    }
    
    public xl0(final int n) {
    }
    
    public static xl0 e() {
        synchronized (xl0.a) {
            if (xl0.b == null) {
                xl0.b = new a(3);
            }
            return xl0.b;
        }
    }
    
    public static void h(final xl0 b) {
        synchronized (xl0.a) {
            xl0.b = b;
        }
    }
    
    public static String i(final String s) {
        final int length = s.length();
        final StringBuilder sb = new StringBuilder(23);
        sb.append("WM-");
        final int c = xl0.c;
        String substring = s;
        if (length >= c) {
            substring = s.substring(0, c);
        }
        sb.append(substring);
        return sb.toString();
    }
    
    public abstract void a(final String p0, final String p1);
    
    public abstract void b(final String p0, final String p1, final Throwable p2);
    
    public abstract void c(final String p0, final String p1);
    
    public abstract void d(final String p0, final String p1, final Throwable p2);
    
    public abstract void f(final String p0, final String p1);
    
    public abstract void g(final String p0, final String p1, final Throwable p2);
    
    public abstract void j(final String p0, final String p1);
    
    public abstract void k(final String p0, final String p1);
    
    public abstract void l(final String p0, final String p1, final Throwable p2);
    
    public static class a extends xl0
    {
        public final int d;
        
        public a(final int d) {
            super(d);
            this.d = d;
        }
        
        @Override
        public void a(final String s, final String s2) {
            if (this.d <= 3) {
                Log.d(s, s2);
            }
        }
        
        @Override
        public void b(final String s, final String s2, final Throwable t) {
            if (this.d <= 3) {
                Log.d(s, s2, t);
            }
        }
        
        @Override
        public void c(final String s, final String s2) {
            if (this.d <= 6) {
                Log.e(s, s2);
            }
        }
        
        @Override
        public void d(final String s, final String s2, final Throwable t) {
            if (this.d <= 6) {
                Log.e(s, s2, t);
            }
        }
        
        @Override
        public void f(final String s, final String s2) {
            if (this.d <= 4) {
                Log.i(s, s2);
            }
        }
        
        @Override
        public void g(final String s, final String s2, final Throwable t) {
            if (this.d <= 4) {
                Log.i(s, s2, t);
            }
        }
        
        @Override
        public void j(final String s, final String s2) {
            if (this.d <= 2) {
                Log.v(s, s2);
            }
        }
        
        @Override
        public void k(final String s, final String s2) {
            if (this.d <= 5) {
                Log.w(s, s2);
            }
        }
        
        @Override
        public void l(final String s, final String s2, final Throwable t) {
            if (this.d <= 5) {
                Log.w(s, s2, t);
            }
        }
    }
}

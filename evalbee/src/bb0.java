import com.android.volley.Request;
import java.util.HashMap;
import java.util.Map;
import com.android.volley.VolleyError;
import com.android.volley.d;
import android.os.Bundle;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.OrgProfile;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class bb0
{
    public ee1 a;
    public zg b;
    public String c;
    public Context d;
    public OrgProfile e;
    
    public bb0(final OrgProfile e, final Context d, final zg b) {
        this.b = b;
        this.e = e;
        this.a = new n52(d, a91.v()).b();
        final StringBuilder sb = new StringBuilder();
        sb.append("https://");
        sb.append(a91.v());
        sb.append(":");
        sb.append(a91.p());
        sb.append("/api/user-claims");
        this.c = sb.toString();
        this.d = d;
        this.c();
    }
    
    public final void c() {
        FirebaseAuth.getInstance().e().i(false).addOnSuccessListener((OnSuccessListener)new OnSuccessListener(this) {
            public final bb0 a;
            
            public void a(final ya0 ya0) {
                if (!this.a.e.getOrgId().equals(ya0.a().get("orgId"))) {
                    this.a.d();
                }
            }
        });
    }
    
    public final void d() {
        FirebaseAuth.getInstance().e().i(true).addOnSuccessListener((OnSuccessListener)new OnSuccessListener(this) {
            public final bb0 a;
            
            public void a(final ya0 ya0) {
                if (!this.a.e.getOrgId().equals(ya0.a().get("orgId"))) {
                    this.a.e(ya0.c());
                    FirebaseAnalytics.getInstance(this.a.d).a("FORCE_ORG_UPDATE", null);
                }
            }
        });
    }
    
    public final void e(final String s) {
        final hr1 hr1 = new hr1(this, 0, this.c, new d.b(this) {
            public final bb0 a;
            
            public void b(final String s) {
                FirebaseAuth.getInstance().e().i(true).addOnSuccessListener((OnSuccessListener)new OnSuccessListener(this) {
                    public final bb0$c a;
                    
                    public void a(final ya0 ya0) {
                    }
                });
            }
        }, new d.a(this) {
            public final bb0 a;
            
            @Override
            public void a(final VolleyError volleyError) {
            }
        }, s) {
            public final String w;
            public final bb0 x;
            
            @Override
            public byte[] k() {
                return null;
            }
            
            @Override
            public String l() {
                return "application/json";
            }
            
            @Override
            public Map o() {
                final String o = FirebaseAuth.getInstance().e().O();
                final String email = FirebaseAuth.getInstance().e().getEmail();
                final HashMap hashMap = new HashMap();
                hashMap.put("x-user-id", o);
                hashMap.put("x-user-email", email);
                hashMap.put("x-user-token", this.w);
                return hashMap;
            }
        };
        hr1.L(new wq(20000, 0, 1.0f));
        this.a.a(hr1);
    }
}

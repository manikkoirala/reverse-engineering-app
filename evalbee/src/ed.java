import org.jetbrains.annotations.Nullable;
import android.os.IBinder;
import org.jetbrains.annotations.NotNull;
import android.os.Bundle;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ed
{
    public static final ed a;
    
    static {
        a = new ed();
    }
    
    public static final void a(@NotNull final Bundle bundle, @NotNull final String s, @Nullable final IBinder binder) {
        fg0.e((Object)bundle, "bundle");
        fg0.e((Object)s, "key");
        bundle.putBinder(s, binder);
    }
}

import android.os.Build$VERSION;
import android.view.ViewGroup;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class w32
{
    public static boolean a = true;
    
    public static u32 a(final ViewGroup viewGroup) {
        return new t32(viewGroup);
    }
    
    public static void b(final ViewGroup viewGroup, final boolean b) {
        if (w32.a) {
            try {
                v32.a(viewGroup, b);
            }
            catch (final NoSuchMethodError noSuchMethodError) {
                w32.a = false;
            }
        }
    }
    
    public static void c(final ViewGroup viewGroup, final boolean b) {
        if (Build$VERSION.SDK_INT >= 29) {
            v32.a(viewGroup, b);
        }
        else {
            b(viewGroup, b);
        }
    }
}

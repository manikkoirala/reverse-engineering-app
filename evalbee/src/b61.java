import android.view.Menu;
import android.view.MenuInflater;
import android.widget.PopupWindow$OnDismissListener;
import android.view.MenuItem;
import androidx.appcompat.view.menu.h;
import android.view.View;
import androidx.appcompat.view.menu.e;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class b61
{
    public final Context a;
    public final e b;
    public final View c;
    public final h d;
    public c e;
    
    public b61(final Context context, final View view) {
        this(context, view, 0);
    }
    
    public b61(final Context context, final View view, final int n) {
        this(context, view, n, sa1.F, 0);
    }
    
    public b61(final Context a, final View c, final int n, final int n2, final int n3) {
        this.a = a;
        this.c = c;
        final e b = new e(a);
        (this.b = b).setCallback((e.a)new e.a(this) {
            public final b61 a;
            
            @Override
            public boolean onMenuItemSelected(final e e, final MenuItem menuItem) {
                final c e2 = this.a.e;
                return e2 != null && e2.onMenuItemClick(menuItem);
            }
            
            @Override
            public void onMenuModeChange(final e e) {
            }
        });
        final h d = new h(a, b, c, false, n2, n3);
        (this.d = d).h(n);
        d.i((PopupWindow$OnDismissListener)new PopupWindow$OnDismissListener(this) {
            public final b61 a;
            
            public void onDismiss() {
                this.a.getClass();
            }
        });
    }
    
    public MenuInflater a() {
        return new ls1(this.a);
    }
    
    public void b(final int n) {
        this.a().inflate(n, (Menu)this.b);
    }
    
    public void c(final boolean b) {
        this.d.g(b);
    }
    
    public void d(final c e) {
        this.e = e;
    }
    
    public void e() {
        this.d.k();
    }
    
    public interface c
    {
        boolean onMenuItemClick(final MenuItem p0);
    }
}

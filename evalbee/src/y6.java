import android.content.Context;
import android.view.View;
import android.util.AttributeSet;
import android.graphics.drawable.RippleDrawable;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

// 
// Decompiled by Procyon v0.6.0
// 

public class y6
{
    public final ImageView a;
    public rw1 b;
    public rw1 c;
    public rw1 d;
    public int e;
    
    public y6(final ImageView a) {
        this.e = 0;
        this.a = a;
    }
    
    public final boolean a(final Drawable drawable) {
        if (this.d == null) {
            this.d = new rw1();
        }
        final rw1 d = this.d;
        d.a();
        final ColorStateList a = je0.a(this.a);
        if (a != null) {
            d.d = true;
            d.a = a;
        }
        final PorterDuff$Mode b = je0.b(this.a);
        if (b != null) {
            d.c = true;
            d.b = b;
        }
        if (!d.d && !d.c) {
            return false;
        }
        s6.i(drawable, d, ((View)this.a).getDrawableState());
        return true;
    }
    
    public void b() {
        if (this.a.getDrawable() != null) {
            this.a.getDrawable().setLevel(this.e);
        }
    }
    
    public void c() {
        final Drawable drawable = this.a.getDrawable();
        if (drawable != null) {
            fv.b(drawable);
        }
        if (drawable != null) {
            if (this.l() && this.a(drawable)) {
                return;
            }
            final rw1 c = this.c;
            if (c != null) {
                s6.i(drawable, c, ((View)this.a).getDrawableState());
            }
            else {
                final rw1 b = this.b;
                if (b != null) {
                    s6.i(drawable, b, ((View)this.a).getDrawableState());
                }
            }
        }
    }
    
    public ColorStateList d() {
        final rw1 c = this.c;
        ColorStateList a;
        if (c != null) {
            a = c.a;
        }
        else {
            a = null;
        }
        return a;
    }
    
    public PorterDuff$Mode e() {
        final rw1 c = this.c;
        PorterDuff$Mode b;
        if (c != null) {
            b = c.b;
        }
        else {
            b = null;
        }
        return b;
    }
    
    public boolean f() {
        return !(((View)this.a).getBackground() instanceof RippleDrawable);
    }
    
    public void g(final AttributeSet set, int n) {
        final Context context = ((View)this.a).getContext();
        final int[] p2 = bc1.P;
        final tw1 v = tw1.v(context, set, p2, n, 0);
        final ImageView a = this.a;
        o32.o0((View)a, ((View)a).getContext(), p2, set, v.r(), n, 0);
        try {
            Drawable drawable2;
            final Drawable drawable = drawable2 = this.a.getDrawable();
            if (drawable == null) {
                n = v.n(bc1.Q, -1);
                drawable2 = drawable;
                if (n != -1) {
                    final Drawable b = g7.b(((View)this.a).getContext(), n);
                    if ((drawable2 = b) != null) {
                        this.a.setImageDrawable(b);
                        drawable2 = b;
                    }
                }
            }
            if (drawable2 != null) {
                fv.b(drawable2);
            }
            n = bc1.R;
            if (v.s(n)) {
                je0.c(this.a, v.c(n));
            }
            n = bc1.S;
            if (v.s(n)) {
                je0.d(this.a, fv.e(v.k(n, -1), null));
            }
        }
        finally {
            v.w();
        }
    }
    
    public void h(final Drawable drawable) {
        this.e = drawable.getLevel();
    }
    
    public void i(final int n) {
        if (n != 0) {
            final Drawable b = g7.b(((View)this.a).getContext(), n);
            if (b != null) {
                fv.b(b);
            }
            this.a.setImageDrawable(b);
        }
        else {
            this.a.setImageDrawable((Drawable)null);
        }
        this.c();
    }
    
    public void j(final ColorStateList a) {
        if (this.c == null) {
            this.c = new rw1();
        }
        final rw1 c = this.c;
        c.a = a;
        c.d = true;
        this.c();
    }
    
    public void k(final PorterDuff$Mode b) {
        if (this.c == null) {
            this.c = new rw1();
        }
        final rw1 c = this.c;
        c.b = b;
        c.c = true;
        this.c();
    }
    
    public final boolean l() {
        return this.b != null;
    }
}

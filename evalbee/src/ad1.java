import java.lang.reflect.Method;
import java.util.Iterator;
import com.google.gson.ReflectionAccessFilter$FilterResult;
import java.util.List;
import java.lang.reflect.AccessibleObject;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class ad1
{
    public static boolean a(final AccessibleObject accessibleObject, final Object o) {
        return b.a.a(accessibleObject, o);
    }
    
    public static ReflectionAccessFilter$FilterResult b(final List list, final Class clazz) {
        final Iterator iterator = list.iterator();
        if (!iterator.hasNext()) {
            return ReflectionAccessFilter$FilterResult.ALLOW;
        }
        zu0.a(iterator.next());
        throw null;
    }
    
    public abstract static class b
    {
        public static final b a;
        
        static {
            while (true) {
                if (!wg0.d()) {
                    break Label_0035;
                }
                try {
                    b b = new b(AccessibleObject.class.getDeclaredMethod("canAccess", Object.class)) {
                        public final Method b;
                        
                        @Override
                        public boolean a(final AccessibleObject obj, final Object o) {
                            try {
                                return (boolean)this.b.invoke(obj, o);
                            }
                            catch (final Exception cause) {
                                throw new RuntimeException("Failed invoking canAccess", cause);
                            }
                        }
                    };
                    while (true) {
                        b a2 = b;
                        if (b == null) {
                            a2 = new b() {
                                @Override
                                public boolean a(final AccessibleObject accessibleObject, final Object o) {
                                    return true;
                                }
                            };
                        }
                        a = a2;
                        return;
                        b = null;
                        continue;
                    }
                }
                catch (final NoSuchMethodException ex) {
                    continue;
                }
                break;
            }
        }
        
        public abstract boolean a(final AccessibleObject p0, final Object p1);
    }
}

import android.os.Build$VERSION;
import android.os.StatFs;
import android.os.Environment;
import com.google.firebase.crashlytics.internal.common.CommonUtils;
import java.util.Iterator;
import java.util.List;
import java.util.Collections;
import java.util.ArrayList;
import com.google.firebase.crashlytics.internal.model.CrashlyticsReport;
import android.text.TextUtils;
import android.os.Build;
import java.util.Locale;
import java.util.HashMap;
import android.content.Context;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public class kn
{
    public static final Map g;
    public static final String h;
    public final Context a;
    public final de0 b;
    public final s7 c;
    public final lp1 d;
    public final zm1 e;
    public final i81 f;
    
    static {
        final HashMap g2 = new HashMap();
        (g = g2).put("armeabi", 5);
        g2.put("armeabi-v7a", 6);
        g2.put("arm64-v8a", 9);
        g2.put("x86", 0);
        g2.put("x86_64", 1);
        h = String.format(Locale.US, "Crashlytics Android SDK/%s", "18.6.0");
    }
    
    public kn(final Context a, final de0 b, final s7 c, final lp1 d, final zm1 e) {
        this.f = i81.a;
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
    }
    
    public static long f(long n) {
        if (n <= 0L) {
            n = 0L;
        }
        return n;
    }
    
    public static int g() {
        final String cpu_ABI = Build.CPU_ABI;
        if (TextUtils.isEmpty((CharSequence)cpu_ABI)) {
            return 7;
        }
        final Integer n = kn.g.get(cpu_ABI.toLowerCase(Locale.US));
        if (n == null) {
            return 7;
        }
        return n;
    }
    
    public final CrashlyticsReport.e.d.a.c A(final CrashlyticsReport.a a) {
        return this.f.a(a.e(), a.d(), a.c());
    }
    
    public final CrashlyticsReport.a a(final CrashlyticsReport.a a) {
        List<Object> unmodifiableList;
        if (this.e.a().b.c && this.c.c.size() > 0) {
            final ArrayList list = new ArrayList();
            for (final zc zc : this.c.c) {
                list.add(CrashlyticsReport.a.a.a().d(zc.c()).b(zc.a()).c(zc.b()).a());
            }
            unmodifiableList = Collections.unmodifiableList((List<?>)list);
        }
        else {
            unmodifiableList = null;
        }
        return CrashlyticsReport.a.a().c(a.c()).e(a.e()).g(a.g()).i(a.i()).d(a.d()).f(a.f()).h(a.h()).j(a.j()).b(unmodifiableList).a();
    }
    
    public final CrashlyticsReport.b b() {
        return CrashlyticsReport.b().k("18.6.0").g(this.c.a).h(this.b.a().c()).f(this.b.a().d()).d(this.c.f).e(this.c.g).j(4);
    }
    
    public CrashlyticsReport.e.d c(final CrashlyticsReport.a a) {
        final int orientation = this.a.getResources().getConfiguration().orientation;
        return CrashlyticsReport.e.d.a().g("anr").f(a.i()).b(this.k(orientation, this.a(a))).c(this.l(orientation)).a();
    }
    
    public CrashlyticsReport.e.d d(final Throwable t, final Thread thread, final String s, final long n, final int n2, final int n3, final boolean b) {
        final int orientation = this.a.getResources().getConfiguration().orientation;
        return CrashlyticsReport.e.d.a().g(s).f(n).b(this.j(orientation, cz1.a(t, this.d), thread, n2, n3, b)).c(this.l(orientation)).a();
    }
    
    public CrashlyticsReport e(final String s, final long n) {
        return this.b().l(this.t(s, n)).a();
    }
    
    public final CrashlyticsReport.e.d.a.b.a h() {
        return CrashlyticsReport.e.d.a.b.a.a().b(0L).d(0L).c(this.c.e).e(this.c.b).a();
    }
    
    public final List i() {
        return Collections.singletonList(this.h());
    }
    
    public final CrashlyticsReport.e.d.a j(final int n, final cz1 cz1, final Thread thread, final int n2, final int n3, final boolean b) {
        final CrashlyticsReport.e.d.a.c e = this.f.e(this.a);
        Boolean value;
        if (e.b() > 0) {
            value = (e.b() != 100);
        }
        else {
            value = null;
        }
        return CrashlyticsReport.e.d.a.a().c(value).d(e).b(this.f.d(this.a)).h(n).f(this.o(cz1, thread, n2, n3, b)).a();
    }
    
    public final CrashlyticsReport.e.d.a k(final int n, final CrashlyticsReport.a a) {
        return CrashlyticsReport.e.d.a.a().c(a.c() != 100).d(this.A(a)).h(n).f(this.p(a)).a();
    }
    
    public final CrashlyticsReport.e.d.c l(final int n) {
        final tb a = tb.a(this.a);
        final Float b = a.b();
        Double value;
        if (b != null) {
            value = (double)b;
        }
        else {
            value = null;
        }
        return CrashlyticsReport.e.d.c.a().b(value).c(a.c()).f(CommonUtils.n(this.a)).e(n).g(f(CommonUtils.b(this.a) - CommonUtils.a(this.a))).d(CommonUtils.c(Environment.getDataDirectory().getPath())).a();
    }
    
    public final CrashlyticsReport.e.d.a.b.c m(final cz1 cz1, final int n, final int n2) {
        return this.n(cz1, n, n2, 0);
    }
    
    public final CrashlyticsReport.e.d.a.b.c n(cz1 d, final int n, final int n2, final int n3) {
        final String b = d.b;
        final String a = d.a;
        StackTraceElement[] c = d.c;
        int n4 = 0;
        int n5 = 0;
        if (c == null) {
            c = new StackTraceElement[0];
        }
        final cz1 d2 = d.d;
        if (n3 >= n2) {
            d = d2;
            while (true) {
                n4 = n5;
                if (d == null) {
                    break;
                }
                d = d.d;
                ++n5;
            }
        }
        final CrashlyticsReport.e.d.a.b.c.a d3 = CrashlyticsReport.e.d.a.b.c.a().f(b).e(a).c(this.r(c, n)).d(n4);
        if (d2 != null && n4 == 0) {
            d3.b(this.n(d2, n, n2, n3 + 1));
        }
        return d3.a();
    }
    
    public final CrashlyticsReport.e.d.a.b o(final cz1 cz1, final Thread thread, final int n, final int n2, final boolean b) {
        return CrashlyticsReport.e.d.a.b.a().f(this.z(cz1, thread, n, b)).d(this.m(cz1, n, n2)).e(this.w()).c(this.i()).a();
    }
    
    public final CrashlyticsReport.e.d.a.b p(final CrashlyticsReport.a a) {
        return CrashlyticsReport.e.d.a.b.a().b(a).e(this.w()).c(this.i()).a();
    }
    
    public final CrashlyticsReport.e.d.a.b.e.b q(final StackTraceElement stackTraceElement, final CrashlyticsReport.e.d.a.b.e.b.a a) {
        final boolean nativeMethod = stackTraceElement.isNativeMethod();
        final long n = 0L;
        long max;
        if (nativeMethod) {
            max = Math.max(stackTraceElement.getLineNumber(), 0L);
        }
        else {
            max = 0L;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(stackTraceElement.getClassName());
        sb.append(".");
        sb.append(stackTraceElement.getMethodName());
        final String string = sb.toString();
        final String fileName = stackTraceElement.getFileName();
        long n2 = n;
        if (!stackTraceElement.isNativeMethod()) {
            n2 = n;
            if (stackTraceElement.getLineNumber() > 0) {
                n2 = stackTraceElement.getLineNumber();
            }
        }
        return a.e(max).f(string).b(fileName).d(n2).a();
    }
    
    public final List r(final StackTraceElement[] array, final int n) {
        final ArrayList list = new ArrayList();
        for (int length = array.length, i = 0; i < length; ++i) {
            list.add(this.q(array[i], CrashlyticsReport.e.d.a.b.e.b.a().c(n)));
        }
        return Collections.unmodifiableList((List<?>)list);
    }
    
    public final CrashlyticsReport.e.a s() {
        return CrashlyticsReport.e.a.a().e(this.b.f()).g(this.c.f).d(this.c.g).f(this.b.a().c()).b(this.c.h.d()).c(this.c.h.e()).a();
    }
    
    public final CrashlyticsReport.e t(final String s, final long n) {
        return CrashlyticsReport.e.a().m(n).j(s).h(kn.h).b(this.s()).l(this.v()).e(this.u()).i(3).a();
    }
    
    public final CrashlyticsReport.e.c u() {
        final StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        return CrashlyticsReport.e.c.a().b(g()).f(Build.MODEL).c(Runtime.getRuntime().availableProcessors()).h(CommonUtils.b(this.a)).d(statFs.getBlockCount() * (long)statFs.getBlockSize()).i(CommonUtils.w()).j(CommonUtils.l()).e(Build.MANUFACTURER).g(Build.PRODUCT).a();
    }
    
    public final CrashlyticsReport.e.e v() {
        return CrashlyticsReport.e.e.a().d(3).e(Build$VERSION.RELEASE).b(Build$VERSION.CODENAME).c(CommonUtils.x()).a();
    }
    
    public final CrashlyticsReport.e.d.a.b.d w() {
        return CrashlyticsReport.e.d.a.b.d.a().d("0").c("0").b(0L).a();
    }
    
    public final CrashlyticsReport.e.d.a.b.e x(final Thread thread, final StackTraceElement[] array) {
        return this.y(thread, array, 0);
    }
    
    public final CrashlyticsReport.e.d.a.b.e y(final Thread thread, final StackTraceElement[] array, final int n) {
        return CrashlyticsReport.e.d.a.b.e.a().d(thread.getName()).c(n).b(this.r(array, n)).a();
    }
    
    public final List z(final cz1 cz1, final Thread obj, final int n, final boolean b) {
        final ArrayList list = new ArrayList();
        list.add(this.y(obj, cz1.c, n));
        if (b) {
            for (final Map.Entry<Thread, V> entry : Thread.getAllStackTraces().entrySet()) {
                final Thread thread = entry.getKey();
                if (!thread.equals(obj)) {
                    list.add(this.x(thread, this.d.a((StackTraceElement[])(Object)entry.getValue())));
                }
            }
        }
        return Collections.unmodifiableList((List<?>)list);
    }
}

import com.google.gson.internal.LinkedTreeMap;
import java.util.ArrayList;
import com.google.gson.stream.JsonToken;
import java.util.List;
import java.util.Map;
import java.util.ArrayDeque;
import com.google.gson.reflect.TypeToken;
import com.google.gson.ToNumberPolicy;

// 
// Decompiled by Procyon v0.6.0
// 

public final class z01 extends hz1
{
    public static final iz1 c;
    public final gc0 a;
    public final vw1 b;
    
    static {
        c = f(ToNumberPolicy.DOUBLE);
    }
    
    public z01(final gc0 a, final vw1 b) {
        this.a = a;
        this.b = b;
    }
    
    public static iz1 e(final vw1 vw1) {
        if (vw1 == ToNumberPolicy.DOUBLE) {
            return z01.c;
        }
        return f(vw1);
    }
    
    public static iz1 f(final vw1 vw1) {
        return new iz1(vw1) {
            public final vw1 a;
            
            @Override
            public hz1 a(final gc0 gc0, final TypeToken typeToken) {
                if (typeToken.getRawType() == Object.class) {
                    return new z01(gc0, this.a, null);
                }
                return null;
            }
        };
    }
    
    @Override
    public Object b(final rh0 rh0) {
        final JsonToken o0 = rh0.o0();
        Object o2 = this.h(rh0, o0);
        if (o2 == null) {
            return this.g(rh0, o0);
        }
        final ArrayDeque arrayDeque = new ArrayDeque();
        while (true) {
            if (rh0.k()) {
                String k;
                if (o2 instanceof Map) {
                    k = rh0.K();
                }
                else {
                    k = null;
                }
                final JsonToken o3 = rh0.o0();
                final Object h = this.h(rh0, o3);
                final boolean b = h != null;
                Object g = h;
                if (h == null) {
                    g = this.g(rh0, o3);
                }
                if (o2 instanceof List) {
                    ((List<Map>)o2).add((Map)g);
                }
                else {
                    ((Map<String, Map>)o2).put(k, (Map)g);
                }
                if (!b) {
                    continue;
                }
                arrayDeque.addLast(o2);
                o2 = g;
            }
            else {
                if (o2 instanceof List) {
                    rh0.f();
                }
                else {
                    rh0.g();
                }
                if (arrayDeque.isEmpty()) {
                    break;
                }
                o2 = arrayDeque.removeLast();
            }
        }
        return o2;
    }
    
    @Override
    public void d(final vh0 vh0, final Object o) {
        if (o == null) {
            vh0.u();
            return;
        }
        final hz1 m = this.a.m(o.getClass());
        if (m instanceof z01) {
            vh0.d();
            vh0.g();
            return;
        }
        m.d(vh0, o);
    }
    
    public final Object g(final rh0 rh0, final JsonToken obj) {
        final int n = z01$b.a[obj.ordinal()];
        if (n == 3) {
            return rh0.i0();
        }
        if (n == 4) {
            return this.b.readNumber(rh0);
        }
        if (n == 5) {
            return rh0.x();
        }
        if (n == 6) {
            rh0.R();
            return null;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unexpected token: ");
        sb.append(obj);
        throw new IllegalStateException(sb.toString());
    }
    
    public final Object h(final rh0 rh0, final JsonToken jsonToken) {
        final int n = z01$b.a[jsonToken.ordinal()];
        if (n == 1) {
            rh0.a();
            return new ArrayList();
        }
        if (n != 2) {
            return null;
        }
        rh0.b();
        return new LinkedTreeMap();
    }
}

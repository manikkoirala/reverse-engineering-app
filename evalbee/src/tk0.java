import com.google.common.collect.ImmutableMap;

// 
// Decompiled by Procyon v0.6.0
// 

public interface tk0 extends ne, m90
{
    Object apply(final Object p0);
    
    Object get(final Object p0);
    
    ImmutableMap getAll(final Iterable p0);
    
    Object getUnchecked(final Object p0);
    
    void refresh(final Object p0);
}

import android.database.Cursor;
import android.os.CancellationSignal;
import java.util.Collections;
import java.util.List;
import androidx.room.RoomDatabase;

// 
// Decompiled by Procyon v0.6.0
// 

public final class p71 implements o71
{
    public final RoomDatabase a;
    public final ix b;
    
    public p71(final RoomDatabase a) {
        this.a = a;
        this.b = new ix(this, a) {
            public final p71 d;
            
            @Override
            public String e() {
                return "INSERT OR REPLACE INTO `Preference` (`key`,`long_value`) VALUES (?,?)";
            }
            
            public void k(final ws1 ws1, final n71 n71) {
                if (n71.a() == null) {
                    ws1.I(1);
                }
                else {
                    ws1.y(1, n71.a());
                }
                if (n71.b() == null) {
                    ws1.I(2);
                }
                else {
                    ws1.A(2, n71.b());
                }
            }
        };
    }
    
    public static List c() {
        return Collections.emptyList();
    }
    
    @Override
    public void a(final n71 n71) {
        this.a.d();
        this.a.e();
        try {
            this.b.j(n71);
            this.a.D();
        }
        finally {
            this.a.i();
        }
    }
    
    @Override
    public Long b(final String s) {
        final sf1 c = sf1.c("SELECT long_value FROM Preference where `key`=?", 1);
        if (s == null) {
            c.I(1);
        }
        else {
            c.y(1, s);
        }
        this.a.d();
        final RoomDatabase a = this.a;
        final Long n = null;
        final Cursor b = bp.b(a, c, false, null);
        Long value = n;
        try {
            if (b.moveToFirst()) {
                if (b.isNull(0)) {
                    value = n;
                }
                else {
                    value = b.getLong(0);
                }
            }
            return value;
        }
        finally {
            b.close();
            c.release();
        }
    }
}

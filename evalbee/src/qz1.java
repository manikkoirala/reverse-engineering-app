import java.lang.reflect.WildcardType;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Type;
import com.google.common.collect.Sets;
import java.util.Set;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class qz1
{
    public final Set a;
    
    public qz1() {
        this.a = Sets.c();
    }
    
    public final void a(final Type... p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: arraylength    
        //     2: istore_3       
        //     3: iconst_0       
        //     4: istore_2       
        //     5: iload_2        
        //     6: iload_3        
        //     7: if_icmpge       216
        //    10: aload_1        
        //    11: iload_2        
        //    12: aaload         
        //    13: astore          4
        //    15: aload           4
        //    17: ifnull          210
        //    20: aload_0        
        //    21: getfield        qz1.a:Ljava/util/Set;
        //    24: aload           4
        //    26: invokeinterface java/util/Set.add:(Ljava/lang/Object;)Z
        //    31: ifne            37
        //    34: goto            210
        //    37: aload           4
        //    39: instanceof      Ljava/lang/reflect/TypeVariable;
        //    42: ifeq            57
        //    45: aload_0        
        //    46: aload           4
        //    48: checkcast       Ljava/lang/reflect/TypeVariable;
        //    51: invokevirtual   qz1.e:(Ljava/lang/reflect/TypeVariable;)V
        //    54: goto            210
        //    57: aload           4
        //    59: instanceof      Ljava/lang/reflect/WildcardType;
        //    62: ifeq            77
        //    65: aload_0        
        //    66: aload           4
        //    68: checkcast       Ljava/lang/reflect/WildcardType;
        //    71: invokevirtual   qz1.f:(Ljava/lang/reflect/WildcardType;)V
        //    74: goto            210
        //    77: aload           4
        //    79: instanceof      Ljava/lang/reflect/ParameterizedType;
        //    82: ifeq            97
        //    85: aload_0        
        //    86: aload           4
        //    88: checkcast       Ljava/lang/reflect/ParameterizedType;
        //    91: invokevirtual   qz1.d:(Ljava/lang/reflect/ParameterizedType;)V
        //    94: goto            210
        //    97: aload           4
        //    99: instanceof      Ljava/lang/Class;
        //   102: ifeq            117
        //   105: aload_0        
        //   106: aload           4
        //   108: checkcast       Ljava/lang/Class;
        //   111: invokevirtual   qz1.b:(Ljava/lang/Class;)V
        //   114: goto            210
        //   117: aload           4
        //   119: instanceof      Ljava/lang/reflect/GenericArrayType;
        //   122: ifeq            137
        //   125: aload_0        
        //   126: aload           4
        //   128: checkcast       Ljava/lang/reflect/GenericArrayType;
        //   131: invokevirtual   qz1.c:(Ljava/lang/reflect/GenericArrayType;)V
        //   134: goto            210
        //   137: new             Ljava/lang/AssertionError;
        //   140: astore          5
        //   142: aload           4
        //   144: invokestatic    java/lang/String.valueOf:(Ljava/lang/Object;)Ljava/lang/String;
        //   147: astore_1       
        //   148: aload_1        
        //   149: invokevirtual   java/lang/String.length:()I
        //   152: istore_2       
        //   153: new             Ljava/lang/StringBuilder;
        //   156: astore          6
        //   158: aload           6
        //   160: iload_2        
        //   161: bipush          14
        //   163: iadd           
        //   164: invokespecial   java/lang/StringBuilder.<init>:(I)V
        //   167: aload           6
        //   169: ldc             "Unknown type: "
        //   171: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   174: pop            
        //   175: aload           6
        //   177: aload_1        
        //   178: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   181: pop            
        //   182: aload           5
        //   184: aload           6
        //   186: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   189: invokespecial   java/lang/AssertionError.<init>:(Ljava/lang/Object;)V
        //   192: aload           5
        //   194: athrow         
        //   195: astore_1       
        //   196: aload_0        
        //   197: getfield        qz1.a:Ljava/util/Set;
        //   200: aload           4
        //   202: invokeinterface java/util/Set.remove:(Ljava/lang/Object;)Z
        //   207: pop            
        //   208: aload_1        
        //   209: athrow         
        //   210: iinc            2, 1
        //   213: goto            5
        //   216: return         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  37     54     195    210    Any
        //  57     74     195    210    Any
        //  77     94     195    210    Any
        //  97     114    195    210    Any
        //  117    134    195    210    Any
        //  137    195    195    210    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException
        //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:837)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2086)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void b(final Class clazz) {
    }
    
    public void c(final GenericArrayType genericArrayType) {
    }
    
    public void d(final ParameterizedType parameterizedType) {
    }
    
    public abstract void e(final TypeVariable p0);
    
    public abstract void f(final WildcardType p0);
}

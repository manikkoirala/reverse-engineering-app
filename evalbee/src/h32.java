import com.google.firebase.firestore.model.MutableDocument;

// 
// Decompiled by Procyon v0.6.0
// 

public final class h32 extends wx0
{
    public h32(final du du, final h71 h71) {
        super(du, h71);
    }
    
    @Override
    public q00 a(final MutableDocument mutableDocument, final q00 q00, final pw1 pw1) {
        throw g9.a("VerifyMutation should only be used in Transactions.", new Object[0]);
    }
    
    @Override
    public void b(final MutableDocument mutableDocument, final ay0 ay0) {
        throw g9.a("VerifyMutation should only be used in Transactions.", new Object[0]);
    }
    
    @Override
    public q00 e() {
        return null;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o != null && h32.class == o.getClass() && this.i((wx0)o));
    }
    
    @Override
    public int hashCode() {
        return this.j();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("VerifyMutation{");
        sb.append(this.k());
        sb.append("}");
        return sb.toString();
    }
}

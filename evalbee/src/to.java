import android.os.BaseBundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.os.Build$VERSION;
import android.app.PendingIntent;
import android.os.IBinder;
import android.util.SparseArray;
import java.util.ArrayList;
import android.os.LocaleList;
import android.app.ActivityOptions;
import android.net.Uri;
import android.content.Context;
import android.os.Bundle;
import android.content.Intent;

// 
// Decompiled by Procyon v0.6.0
// 

public final class to
{
    public final Intent a;
    public final Bundle b;
    
    public to(final Intent a, final Bundle b) {
        this.a = a;
        this.b = b;
    }
    
    public void a(final Context context, final Uri data) {
        this.a.setData(data);
        sl.startActivity(context, this.a, this.b);
    }
    
    public abstract static class a
    {
        public static ActivityOptions a() {
            return ActivityOptions.makeBasic();
        }
    }
    
    public abstract static class b
    {
        public static String a() {
            final LocaleList adjustedDefault = LocaleList.getAdjustedDefault();
            String languageTag;
            if (adjustedDefault.size() > 0) {
                languageTag = adjustedDefault.get(0).toLanguageTag();
            }
            else {
                languageTag = null;
            }
            return languageTag;
        }
    }
    
    public abstract static class c
    {
        public static void a(final ActivityOptions activityOptions, final boolean shareIdentityEnabled) {
            activityOptions.setShareIdentityEnabled(shareIdentityEnabled);
        }
    }
    
    public static final class d
    {
        public final Intent a;
        public final qo.a b;
        public ArrayList c;
        public ActivityOptions d;
        public ArrayList e;
        public SparseArray f;
        public Bundle g;
        public int h;
        public boolean i;
        public boolean j;
        
        public d() {
            this.a = new Intent("android.intent.action.VIEW");
            this.b = new qo.a();
            this.h = 0;
            this.i = true;
        }
        
        public d(final vo vo) {
            this.a = new Intent("android.intent.action.VIEW");
            this.b = new qo.a();
            this.h = 0;
            this.i = true;
            if (vo != null) {
                this.c(vo);
            }
        }
        
        public to a() {
            final boolean hasExtra = this.a.hasExtra("android.support.customtabs.extra.SESSION");
            Bundle bundle = null;
            if (!hasExtra) {
                this.d(null, null);
            }
            final ArrayList c = this.c;
            if (c != null) {
                this.a.putParcelableArrayListExtra("android.support.customtabs.extra.MENU_ITEMS", c);
            }
            final ArrayList e = this.e;
            if (e != null) {
                this.a.putParcelableArrayListExtra("android.support.customtabs.extra.TOOLBAR_ITEMS", e);
            }
            this.a.putExtra("android.support.customtabs.extra.EXTRA_ENABLE_INSTANT_APPS", this.i);
            this.a.putExtras(this.b.a().a());
            final Bundle g = this.g;
            if (g != null) {
                this.a.putExtras(g);
            }
            if (this.f != null) {
                final Bundle bundle2 = new Bundle();
                bundle2.putSparseParcelableArray("androidx.browser.customtabs.extra.COLOR_SCHEME_PARAMS", this.f);
                this.a.putExtras(bundle2);
            }
            this.a.putExtra("androidx.browser.customtabs.extra.SHARE_STATE", this.h);
            final int sdk_INT = Build$VERSION.SDK_INT;
            this.b();
            if (sdk_INT >= 34) {
                this.e();
            }
            final ActivityOptions d = this.d;
            if (d != null) {
                bundle = d.toBundle();
            }
            return new to(this.a, bundle);
        }
        
        public final void b() {
            final String a = to.b.a();
            if (!TextUtils.isEmpty((CharSequence)a)) {
                Bundle bundleExtra;
                if (this.a.hasExtra("com.android.browser.headers")) {
                    bundleExtra = this.a.getBundleExtra("com.android.browser.headers");
                }
                else {
                    bundleExtra = new Bundle();
                }
                if (!((BaseBundle)bundleExtra).containsKey("Accept-Language")) {
                    ((BaseBundle)bundleExtra).putString("Accept-Language", a);
                    this.a.putExtra("com.android.browser.headers", bundleExtra);
                }
            }
        }
        
        public d c(final vo vo) {
            this.a.setPackage(vo.b().getPackageName());
            this.d(vo.a(), vo.c());
            return this;
        }
        
        public final void d(final IBinder binder, final PendingIntent pendingIntent) {
            final Bundle bundle = new Bundle();
            hd.a(bundle, "android.support.customtabs.extra.SESSION", binder);
            if (pendingIntent != null) {
                bundle.putParcelable("android.support.customtabs.extra.SESSION_ID", (Parcelable)pendingIntent);
            }
            this.a.putExtras(bundle);
        }
        
        public final void e() {
            if (this.d == null) {
                this.d = to.a.a();
            }
            to.c.a(this.d, this.j);
        }
    }
}

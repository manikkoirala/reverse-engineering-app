import android.util.Log;
import java.io.InputStream;

// 
// Decompiled by Procyon v0.6.0
// 

public class s3
{
    public static final Runtime f;
    public final InputStream a;
    public byte[] b;
    public int c;
    public boolean d;
    public boolean e;
    
    static {
        f = Runtime.getRuntime();
    }
    
    public s3(final InputStream a, final int n) {
        this.a = a;
        this.b = new byte[n];
        this.c = 0;
        this.e = true;
        this.d = false;
    }
    
    public int a(int n) {
        final int c = this.c;
        int i = 0;
        if (n <= c) {
            final int c2 = c - n;
            this.c = c2;
            final byte[] b = this.b;
            System.arraycopy(b, n, b, 0, c2);
        }
        else {
            this.c = 0;
            while (i < n) {
                final int n2 = (int)this.a.skip(n - i);
                if (n2 > 0) {
                    i += n2;
                }
                else {
                    if (n2 != 0) {
                        continue;
                    }
                    if (this.a.read() == -1) {
                        break;
                    }
                    ++i;
                }
            }
            n = i;
        }
        return n;
    }
    
    public int b() {
        return this.c;
    }
    
    public void c() {
        this.a.close();
    }
    
    public int d(int n) {
        int min = n;
        if (n > this.b.length) {
            min = Math.min(n, this.g(n));
        }
        while (true) {
            n = this.c;
            if (n >= min) {
                break;
            }
            n = this.a.read(this.b, n, min - n);
            if (n == -1) {
                this.d = true;
                break;
            }
            this.c += n;
        }
        return this.c;
    }
    
    public byte[] e() {
        return this.b;
    }
    
    public boolean f() {
        return this.d;
    }
    
    public final int g(int max) {
        max = Math.max(this.b.length * 2, max);
        final Runtime f = s3.f;
        final long totalMemory = f.totalMemory();
        final long freeMemory = f.freeMemory();
        final long maxMemory = f.maxMemory();
        if (this.e && max < maxMemory - (totalMemory - freeMemory)) {
            try {
                final byte[] b = new byte[max];
                System.arraycopy(this.b, 0, b, 0, this.c);
                this.b = b;
            }
            catch (final OutOfMemoryError outOfMemoryError) {
                Log.w("AdaptiveStreamBuffer", "Turning off adaptive buffer resizing due to low memory.");
                this.e = false;
            }
        }
        else {
            Log.w("AdaptiveStreamBuffer", "Turning off adaptive buffer resizing to conserve memory.");
        }
        return this.b.length;
    }
}

import android.widget.CompoundButton;
import com.ekodroid.omrevaluator.util.DataModels.SortStudentList;
import android.view.View;
import android.view.View$OnClickListener;
import androidx.appcompat.widget.Toolbar;
import android.os.Bundle;
import android.widget.RadioButton;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class m10 extends u80
{
    public Context a;
    public j10 b;
    public vr1 c;
    public RadioButton d;
    public RadioButton e;
    public RadioButton f;
    public RadioButton g;
    
    public m10(final Context a, final vr1 c, final j10 b) {
        super(a);
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public static /* synthetic */ j10 a(final m10 m10) {
        return m10.b;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492999);
        ((Toolbar)this.findViewById(2131297345)).setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final m10 a;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
        this.d = (RadioButton)this.findViewById(2131297012);
        this.e = (RadioButton)this.findViewById(2131297010);
        this.f = (RadioButton)this.findViewById(2131297005);
        this.g = (RadioButton)this.findViewById(2131297008);
        this.findViewById(2131296388).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final m10 a;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
        this.findViewById(2131296416).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final m10 a;
            
            public void onClick(final View view) {
                if (m10.a(this.a) != null) {
                    final j10 a = m10.a(this.a);
                    SortStudentList.SortBy sortBy;
                    if (((CompoundButton)this.a.e).isChecked()) {
                        sortBy = SortStudentList.SortBy.NAME;
                    }
                    else {
                        sortBy = SortStudentList.SortBy.ROLLNO;
                    }
                    a.a(new vr1(sortBy, ((CompoundButton)this.a.f).isChecked()));
                }
                this.a.dismiss();
            }
        });
        final vr1 c = this.c;
        if (c != null) {
            final RadioButton g = this.g;
            final boolean b = c.b;
            final boolean b2 = true;
            ((CompoundButton)g).setChecked(b ^ true);
            ((CompoundButton)this.f).setChecked(this.c.b);
            ((CompoundButton)this.e).setChecked(this.c.a == SortStudentList.SortBy.NAME);
            ((CompoundButton)this.d).setChecked(this.c.a == SortStudentList.SortBy.ROLLNO && b2);
        }
    }
}

import android.os.Build$VERSION;
import java.util.Locale;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class yc
{
    public static boolean a(final String s, final String anObject) {
        final boolean equals = "REL".equals(anObject);
        boolean b = false;
        if (equals) {
            return false;
        }
        final Locale root = Locale.ROOT;
        if (anObject.toUpperCase(root).compareTo(s.toUpperCase(root)) >= 0) {
            b = true;
        }
        return b;
    }
    
    public static boolean b() {
        return Build$VERSION.SDK_INT >= 30;
    }
    
    public static boolean c() {
        final int sdk_INT = Build$VERSION.SDK_INT;
        return sdk_INT >= 33 || (sdk_INT >= 32 && a("Tiramisu", Build$VERSION.CODENAME));
    }
    
    public static boolean d() {
        return Build$VERSION.SDK_INT >= 33 && a("UpsideDownCake", Build$VERSION.CODENAME);
    }
}

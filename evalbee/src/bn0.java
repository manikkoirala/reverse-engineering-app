import java.util.Iterator;
import java.util.ArrayList;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonToken;
import java.lang.reflect.Type;
import com.google.gson.internal.$Gson$Types;
import java.util.Map;
import com.google.gson.reflect.TypeToken;

// 
// Decompiled by Procyon v0.6.0
// 

public final class bn0 implements iz1
{
    public final bl a;
    public final boolean b;
    
    public bn0(final bl a, final boolean b) {
        this.a = a;
        this.b = b;
    }
    
    @Override
    public hz1 a(final gc0 gc0, final TypeToken typeToken) {
        final Type type = typeToken.getType();
        final Class rawType = typeToken.getRawType();
        if (!Map.class.isAssignableFrom(rawType)) {
            return null;
        }
        final Type[] j = $Gson$Types.j(type, rawType);
        return new a(gc0, j[0], this.b(gc0, j[0]), j[1], gc0.l(TypeToken.get(j[1])), this.a.b(typeToken));
    }
    
    public final hz1 b(final gc0 gc0, final Type type) {
        hz1 hz1;
        if (type != Boolean.TYPE && type != Boolean.class) {
            hz1 = gc0.l(TypeToken.get(type));
        }
        else {
            hz1 = kz1.f;
        }
        return hz1;
    }
    
    public final class a extends hz1
    {
        public final hz1 a;
        public final hz1 b;
        public final u01 c;
        public final bn0 d;
        
        public a(final bn0 d, final gc0 gc0, final Type type, final hz1 hz1, final Type type2, final hz1 hz2, final u01 c) {
            this.d = d;
            this.a = new jz1(gc0, hz1, type);
            this.b = new jz1(gc0, hz2, type2);
            this.c = c;
        }
        
        public final String e(final nh0 nh0) {
            if (nh0.m()) {
                final qh0 c = nh0.c();
                if (c.t()) {
                    return String.valueOf(c.o());
                }
                if (c.r()) {
                    return Boolean.toString(c.n());
                }
                if (c.u()) {
                    return c.p();
                }
                throw new AssertionError();
            }
            else {
                if (nh0.i()) {
                    return "null";
                }
                throw new AssertionError();
            }
        }
        
        public Map f(final rh0 rh0) {
            final JsonToken o0 = rh0.o0();
            if (o0 == JsonToken.NULL) {
                rh0.R();
                return null;
            }
            final Map map = (Map)this.c.a();
            if (o0 == JsonToken.BEGIN_ARRAY) {
                rh0.a();
                while (rh0.k()) {
                    rh0.a();
                    final Object b = this.a.b(rh0);
                    if (map.put(b, this.b.b(rh0)) != null) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("duplicate key: ");
                        sb.append(b);
                        throw new JsonSyntaxException(sb.toString());
                    }
                    rh0.f();
                }
                rh0.f();
            }
            else {
                rh0.b();
                while (rh0.k()) {
                    sh0.a.a(rh0);
                    final Object b2 = this.a.b(rh0);
                    if (map.put(b2, this.b.b(rh0)) == null) {
                        continue;
                    }
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("duplicate key: ");
                    sb2.append(b2);
                    throw new JsonSyntaxException(sb2.toString());
                }
                rh0.g();
            }
            return map;
        }
        
        public void g(final vh0 vh0, final Map map) {
            if (map == null) {
                vh0.u();
                return;
            }
            if (!this.d.b) {
                vh0.d();
                for (final Map.Entry<Object, V> entry : map.entrySet()) {
                    vh0.o(String.valueOf(entry.getKey()));
                    this.b.d(vh0, entry.getValue());
                }
                vh0.g();
                return;
            }
            final ArrayList list = new ArrayList(map.size());
            final ArrayList list2 = new ArrayList(map.size());
            final Iterator iterator2 = map.entrySet().iterator();
            final int n = 0;
            final int n2 = 0;
            boolean b = false;
            while (iterator2.hasNext()) {
                final Map.Entry<Object, V> entry2 = (Map.Entry<Object, V>)iterator2.next();
                final nh0 c = this.a.c(entry2.getKey());
                list.add(c);
                list2.add(entry2.getValue());
                b |= (c.g() || c.l());
            }
            if (b) {
                vh0.c();
                for (int size = list.size(), i = n2; i < size; ++i) {
                    vh0.c();
                    er1.a((nh0)list.get(i), vh0);
                    this.b.d(vh0, list2.get(i));
                    vh0.f();
                }
                vh0.f();
            }
            else {
                vh0.d();
                for (int size2 = list.size(), j = n; j < size2; ++j) {
                    vh0.o(this.e((nh0)list.get(j)));
                    this.b.d(vh0, list2.get(j));
                }
                vh0.g();
            }
        }
    }
}

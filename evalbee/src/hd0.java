import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.io.InputStream;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import java.util.Iterator;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public class hd0
{
    public final String a;
    public final Map b;
    public final Map c;
    
    public hd0(final String a, final Map b) {
        this.a = a;
        this.b = b;
        this.c = new HashMap();
    }
    
    public final String a(final Map map) {
        final StringBuilder sb = new StringBuilder();
        final Iterator iterator = map.entrySet().iterator();
        Map.Entry<String, V> entry = (Map.Entry<String, V>)iterator.next();
        sb.append(entry.getKey());
        sb.append("=");
    Label_0060_Outer:
        while (true) {
            if (entry.getValue() == null) {
                break Label_0078;
            }
            while (true) {
                String encode = URLEncoder.encode((String)entry.getValue(), "UTF-8");
                Label_0081: {
                    break Label_0081;
                    encode = "";
                }
                sb.append(encode);
                if (!iterator.hasNext()) {
                    return sb.toString();
                }
                entry = (Map.Entry)iterator.next();
                sb.append("&");
                sb.append(entry.getKey());
                sb.append("=");
                if (entry.getValue() != null) {
                    continue;
                }
                break;
            }
            continue Label_0060_Outer;
        }
    }
    
    public final String b(final String s, final Map map) {
        final String a = this.a(map);
        if (a.isEmpty()) {
            return s;
        }
        if (s.contains("?")) {
            String string = a;
            if (!s.endsWith("&")) {
                final StringBuilder sb = new StringBuilder();
                sb.append("&");
                sb.append(a);
                string = sb.toString();
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(s);
            sb2.append(string);
            return sb2.toString();
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(s);
        sb3.append("?");
        sb3.append(a);
        return sb3.toString();
    }
    
    public kd0 c() {
        InputStream inputStream = null;
        final String s = null;
        HttpURLConnection httpURLConnection;
        try {
            final String b = this.b(this.a, this.b);
            final zl0 f = zl0.f();
            final StringBuilder sb = new StringBuilder();
            sb.append("GET Request URL: ");
            sb.append(b);
            f.i(sb.toString());
            final HttpsURLConnection httpsURLConnection = (HttpsURLConnection)new URL(b).openConnection();
            try {
                httpsURLConnection.setReadTimeout(10000);
                httpsURLConnection.setConnectTimeout(10000);
                httpsURLConnection.setRequestMethod("GET");
                for (final Map.Entry<String, V> entry : this.c.entrySet()) {
                    httpsURLConnection.addRequestProperty(entry.getKey(), (String)entry.getValue());
                }
                httpsURLConnection.connect();
                final int responseCode = httpsURLConnection.getResponseCode();
                final InputStream inputStream2 = httpsURLConnection.getInputStream();
                if (inputStream2 != null) {
                    try {
                        this.e(inputStream2);
                    }
                    finally {
                        inputStream = inputStream2;
                    }
                }
                if (inputStream2 != null) {
                    inputStream2.close();
                }
                httpsURLConnection.disconnect();
                return new kd0(responseCode, s);
            }
            finally {}
        }
        finally {
            httpURLConnection = null;
        }
        if (inputStream != null) {
            inputStream.close();
        }
        if (httpURLConnection != null) {
            httpURLConnection.disconnect();
        }
    }
    
    public hd0 d(final String s, final String s2) {
        this.c.put(s, s2);
        return this;
    }
    
    public final String e(final InputStream in) {
        final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
        final char[] array = new char[8192];
        final StringBuilder sb = new StringBuilder();
        while (true) {
            final int read = bufferedReader.read(array);
            if (read == -1) {
                break;
            }
            sb.append(array, 0, read);
        }
        return sb.toString();
    }
}

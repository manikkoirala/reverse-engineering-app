import androidx.datastore.preferences.protobuf.WireFormat;
import androidx.datastore.preferences.protobuf.v;
import androidx.datastore.preferences.protobuf.y;
import java.util.Collections;
import java.io.InputStream;
import java.util.Map;
import androidx.datastore.preferences.PreferencesProto$Value;
import androidx.datastore.preferences.protobuf.MapFieldLite;
import androidx.datastore.preferences.protobuf.GeneratedMessageLite;

// 
// Decompiled by Procyon v0.6.0
// 

public final class x71 extends GeneratedMessageLite implements yv0
{
    private static final x71 DEFAULT_INSTANCE;
    private static volatile c31 PARSER;
    public static final int PREFERENCES_FIELD_NUMBER = 1;
    private MapFieldLite<String, PreferencesProto$Value> preferences_;
    
    static {
        GeneratedMessageLite.H(x71.class, DEFAULT_INSTANCE = new x71());
    }
    
    public x71() {
        this.preferences_ = MapFieldLite.emptyMapField();
    }
    
    public static /* synthetic */ x71 J() {
        return x71.DEFAULT_INSTANCE;
    }
    
    public static a P() {
        return (a)x71.DEFAULT_INSTANCE.r();
    }
    
    public static x71 Q(final InputStream inputStream) {
        return (x71)GeneratedMessageLite.F(x71.DEFAULT_INSTANCE, inputStream);
    }
    
    public final Map L() {
        return this.N();
    }
    
    public Map M() {
        return Collections.unmodifiableMap((Map<?, ?>)this.O());
    }
    
    public final MapFieldLite N() {
        if (!this.preferences_.isMutable()) {
            this.preferences_ = this.preferences_.mutableCopy();
        }
        return this.preferences_;
    }
    
    public final MapFieldLite O() {
        return this.preferences_;
    }
    
    @Override
    public final Object u(final MethodToInvoke methodToInvoke, final Object o, final Object o2) {
        switch (w71.a[methodToInvoke.ordinal()]) {
            default: {
                throw new UnsupportedOperationException();
            }
            case 7: {
                return null;
            }
            case 6: {
                return 1;
            }
            case 5: {
                final c31 parser;
                if ((parser = x71.PARSER) == null) {
                    synchronized (x71.class) {
                        if (x71.PARSER == null) {
                            x71.PARSER = new GeneratedMessageLite.b(x71.DEFAULT_INSTANCE);
                        }
                    }
                }
                return parser;
            }
            case 4: {
                return x71.DEFAULT_INSTANCE;
            }
            case 3: {
                return GeneratedMessageLite.E(x71.DEFAULT_INSTANCE, "\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0001\u0000\u0000\u00012", new Object[] { "preferences_", b.a });
            }
            case 2: {
                return new a((w71)null);
            }
            case 1: {
                return new x71();
            }
        }
    }
    
    public static final class a extends GeneratedMessageLite.a implements yv0
    {
        public a() {
            super(x71.J());
        }
        
        public a z(final String s, final PreferencesProto$Value preferencesProto$Value) {
            s.getClass();
            preferencesProto$Value.getClass();
            ((GeneratedMessageLite.a)this).s();
            ((x71)super.b).L().put(s, preferencesProto$Value);
            return this;
        }
    }
    
    public abstract static final class b
    {
        public static final v a;
        
        static {
            a = v.d(WireFormat.FieldType.STRING, "", WireFormat.FieldType.MESSAGE, PreferencesProto$Value.S());
        }
    }
}

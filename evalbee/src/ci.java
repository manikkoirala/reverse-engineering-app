import android.graphics.Color;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class ci
{
    public static final ThreadLocal a;
    
    static {
        a = new ThreadLocal();
    }
    
    public static void a(final int n, final int n2, final int n3, final double[] array) {
        if (array.length == 3) {
            final double n4 = n / 255.0;
            double pow;
            if (n4 < 0.04045) {
                pow = n4 / 12.92;
            }
            else {
                pow = Math.pow((n4 + 0.055) / 1.055, 2.4);
            }
            final double n5 = n2 / 255.0;
            double pow2;
            if (n5 < 0.04045) {
                pow2 = n5 / 12.92;
            }
            else {
                pow2 = Math.pow((n5 + 0.055) / 1.055, 2.4);
            }
            final double n6 = n3 / 255.0;
            double pow3;
            if (n6 < 0.04045) {
                pow3 = n6 / 12.92;
            }
            else {
                pow3 = Math.pow((n6 + 0.055) / 1.055, 2.4);
            }
            array[0] = (0.4124 * pow + 0.3576 * pow2 + 0.1805 * pow3) * 100.0;
            array[1] = (0.2126 * pow + 0.7152 * pow2 + 0.0722 * pow3) * 100.0;
            array[2] = (pow * 0.0193 + pow2 * 0.1192 + pow3 * 0.9505) * 100.0;
            return;
        }
        throw new IllegalArgumentException("outXyz must have a length of 3.");
    }
    
    public static int b(double n, double n2, double a) {
        final double a2 = (3.2406 * n + -1.5372 * n2 + -0.4986 * a) / 100.0;
        final double a3 = (-0.9689 * n + 1.8758 * n2 + 0.0415 * a) / 100.0;
        a = (0.0557 * n + -0.204 * n2 + 1.057 * a) / 100.0;
        if (a2 > 0.0031308) {
            n = Math.pow(a2, 0.4166666666666667) * 1.055 - 0.055;
        }
        else {
            n = a2 * 12.92;
        }
        if (a3 > 0.0031308) {
            n2 = Math.pow(a3, 0.4166666666666667) * 1.055 - 0.055;
        }
        else {
            n2 = a3 * 12.92;
        }
        if (a > 0.0031308) {
            a = Math.pow(a, 0.4166666666666667) * 1.055 - 0.055;
        }
        else {
            a *= 12.92;
        }
        return Color.rgb(i((int)Math.round(n * 255.0), 0, 255), i((int)Math.round(n2 * 255.0), 0, 255), i((int)Math.round(a * 255.0), 0, 255));
    }
    
    public static int c(final int n, final int n2, final float n3) {
        final float n4 = 1.0f - n3;
        return Color.argb((int)(Color.alpha(n) * n4 + Color.alpha(n2) * n3), (int)(Color.red(n) * n4 + Color.red(n2) * n3), (int)(Color.green(n) * n4 + Color.green(n2) * n3), (int)(Color.blue(n) * n4 + Color.blue(n2) * n3));
    }
    
    public static double d(final int n) {
        final double[] j = j();
        e(n, j);
        return j[1] / 100.0;
    }
    
    public static void e(final int n, final double[] array) {
        a(Color.red(n), Color.green(n), Color.blue(n), array);
    }
    
    public static int f(final int n, final int n2) {
        return 255 - (255 - n2) * (255 - n) / 255;
    }
    
    public static int g(final int n, final int n2) {
        final int alpha = Color.alpha(n2);
        final int alpha2 = Color.alpha(n);
        final int f = f(alpha2, alpha);
        return Color.argb(f, h(Color.red(n), alpha2, Color.red(n2), alpha, f), h(Color.green(n), alpha2, Color.green(n2), alpha, f), h(Color.blue(n), alpha2, Color.blue(n2), alpha, f));
    }
    
    public static int h(final int n, final int n2, final int n3, final int n4, final int n5) {
        if (n5 == 0) {
            return 0;
        }
        return (n * 255 * n2 + n3 * n4 * (255 - n2)) / (n5 * 255);
    }
    
    public static int i(final int a, int min, final int b) {
        if (a >= min) {
            min = Math.min(a, b);
        }
        return min;
    }
    
    public static double[] j() {
        final ThreadLocal a = ci.a;
        double[] value;
        if ((value = a.get()) == null) {
            value = new double[3];
            a.set(value);
        }
        return value;
    }
    
    public static int k(final int n, final int n2) {
        if (n2 >= 0 && n2 <= 255) {
            return (n & 0xFFFFFF) | n2 << 24;
        }
        throw new IllegalArgumentException("alpha must be between 0 and 255.");
    }
}

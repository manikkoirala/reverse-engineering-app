// 
// Decompiled by Procyon v0.6.0
// 

public abstract class ye0 implements Comparable
{
    public static ye0 c(final int n, final du du, final byte[] array, final byte[] array2) {
        return new ka(n, du, array, array2);
    }
    
    public int a(final ye0 ye0) {
        final int compare = Integer.compare(this.g(), ye0.g());
        if (compare != 0) {
            return compare;
        }
        final int c = this.f().c(ye0.f());
        if (c != 0) {
            return c;
        }
        final int h = o22.h(this.d(), ye0.d());
        if (h != 0) {
            return h;
        }
        return o22.h(this.e(), ye0.e());
    }
    
    public abstract byte[] d();
    
    public abstract byte[] e();
    
    public abstract du f();
    
    public abstract int g();
}

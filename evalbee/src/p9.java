import androidx.core.os.OperationCanceledException;
import java.util.concurrent.CountDownLatch;
import java.io.PrintWriter;
import java.io.FileDescriptor;
import android.os.SystemClock;
import androidx.loader.content.ModernAsyncTask;
import android.content.Context;
import android.os.Handler;
import java.util.concurrent.Executor;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class p9 extends qk0
{
    static final boolean DEBUG = false;
    static final String TAG = "AsyncTaskLoader";
    volatile a mCancellingTask;
    private final Executor mExecutor;
    Handler mHandler;
    long mLastLoadCompleteTime;
    volatile a mTask;
    long mUpdateThrottle;
    
    public p9(final Context context) {
        this(context, ModernAsyncTask.h);
    }
    
    public p9(final Context context, final Executor mExecutor) {
        super(context);
        this.mLastLoadCompleteTime = -10000L;
        this.mExecutor = mExecutor;
    }
    
    public void cancelLoadInBackground() {
    }
    
    public void dispatchOnCancelled(final a a, final Object o) {
        this.onCanceled(o);
        if (this.mCancellingTask == a) {
            this.rollbackContentChanged();
            this.mLastLoadCompleteTime = SystemClock.uptimeMillis();
            this.mCancellingTask = null;
            this.deliverCancellation();
            this.executePendingTask();
        }
    }
    
    public void dispatchOnLoadComplete(final a a, final Object o) {
        if (this.mTask != a) {
            this.dispatchOnCancelled(a, o);
        }
        else if (this.isAbandoned()) {
            this.onCanceled(o);
        }
        else {
            this.commitContentChanged();
            this.mLastLoadCompleteTime = SystemClock.uptimeMillis();
            this.mTask = null;
            this.deliverResult(o);
        }
    }
    
    @Deprecated
    @Override
    public void dump(final String s, final FileDescriptor fileDescriptor, final PrintWriter printWriter, final String[] array) {
        super.dump(s, fileDescriptor, printWriter, array);
        if (this.mTask != null) {
            printWriter.print(s);
            printWriter.print("mTask=");
            printWriter.print(this.mTask);
            printWriter.print(" waiting=");
            printWriter.println(this.mTask.l);
        }
        if (this.mCancellingTask != null) {
            printWriter.print(s);
            printWriter.print("mCancellingTask=");
            printWriter.print(this.mCancellingTask);
            printWriter.print(" waiting=");
            printWriter.println(this.mCancellingTask.l);
        }
        if (this.mUpdateThrottle != 0L) {
            printWriter.print(s);
            printWriter.print("mUpdateThrottle=");
            lw1.c(this.mUpdateThrottle, printWriter);
            printWriter.print(" mLastLoadCompleteTime=");
            lw1.b(this.mLastLoadCompleteTime, SystemClock.uptimeMillis(), printWriter);
            printWriter.println();
        }
    }
    
    public void executePendingTask() {
        if (this.mCancellingTask == null && this.mTask != null) {
            if (this.mTask.l) {
                this.mTask.l = false;
                this.mHandler.removeCallbacks((Runnable)this.mTask);
            }
            if (this.mUpdateThrottle > 0L && SystemClock.uptimeMillis() < this.mLastLoadCompleteTime + this.mUpdateThrottle) {
                this.mTask.l = true;
                this.mHandler.postAtTime((Runnable)this.mTask, this.mLastLoadCompleteTime + this.mUpdateThrottle);
                return;
            }
            this.mTask.c(this.mExecutor, (Object[])null);
        }
    }
    
    public boolean isLoadInBackgroundCanceled() {
        return this.mCancellingTask != null;
    }
    
    public abstract Object loadInBackground();
    
    @Override
    public boolean onCancelLoad() {
        if (this.mTask == null) {
            return false;
        }
        if (!super.mStarted) {
            super.mContentChanged = true;
        }
        if (this.mCancellingTask != null) {
            if (this.mTask.l) {
                this.mTask.l = false;
                this.mHandler.removeCallbacks((Runnable)this.mTask);
            }
            this.mTask = null;
            return false;
        }
        if (this.mTask.l) {
            this.mTask.l = false;
            this.mHandler.removeCallbacks((Runnable)this.mTask);
            this.mTask = null;
            return false;
        }
        final boolean a = this.mTask.a(false);
        if (a) {
            this.mCancellingTask = this.mTask;
            this.cancelLoadInBackground();
        }
        this.mTask = null;
        return a;
    }
    
    public void onCanceled(final Object o) {
    }
    
    @Override
    public void onForceLoad() {
        super.onForceLoad();
        this.cancelLoad();
        this.mTask = new a();
        this.executePendingTask();
    }
    
    public Object onLoadInBackground() {
        return this.loadInBackground();
    }
    
    public void setUpdateThrottle(final long mUpdateThrottle) {
        this.mUpdateThrottle = mUpdateThrottle;
        if (mUpdateThrottle != 0L) {
            this.mHandler = new Handler();
        }
    }
    
    public void waitForLoader() {
        final a mTask = this.mTask;
        if (mTask != null) {
            mTask.n();
        }
    }
    
    public final class a extends ModernAsyncTask implements Runnable
    {
        public final CountDownLatch k;
        public boolean l;
        public final p9 m;
        
        public a(final p9 m) {
            this.m = m;
            this.k = new CountDownLatch(1);
        }
        
        @Override
        public void g(final Object o) {
            try {
                this.m.dispatchOnCancelled(this, o);
            }
            finally {
                this.k.countDown();
            }
        }
        
        @Override
        public void h(final Object o) {
            try {
                this.m.dispatchOnLoadComplete(this, o);
            }
            finally {
                this.k.countDown();
            }
        }
        
        public Object m(final Void... array) {
            try {
                return this.m.onLoadInBackground();
            }
            catch (final OperationCanceledException ex) {
                if (this.f()) {
                    return null;
                }
                throw ex;
            }
        }
        
        public void n() {
            try {
                this.k.await();
            }
            catch (final InterruptedException ex) {}
        }
        
        @Override
        public void run() {
            this.l = false;
            this.m.executePendingTask();
        }
    }
}

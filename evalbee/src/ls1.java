import android.view.View;
import androidx.appcompat.view.menu.g;
import android.content.res.TypedArray;
import java.lang.reflect.Constructor;
import android.util.Log;
import android.view.SubMenu;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import android.view.InflateException;
import android.view.MenuItem;
import java.lang.reflect.Method;
import android.view.MenuItem$OnMenuItemClickListener;
import android.content.res.XmlResourceParser;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import android.util.Xml;
import android.view.Menu;
import android.util.AttributeSet;
import org.xmlpull.v1.XmlPullParser;
import android.content.ContextWrapper;
import android.app.Activity;
import android.content.Context;
import android.view.MenuInflater;

// 
// Decompiled by Procyon v0.6.0
// 

public class ls1 extends MenuInflater
{
    public static final Class[] e;
    public static final Class[] f;
    public final Object[] a;
    public final Object[] b;
    public Context c;
    public Object d;
    
    static {
        f = (e = new Class[] { Context.class });
    }
    
    public ls1(final Context c) {
        super(c);
        this.c = c;
        final Object[] array = { c };
        this.a = array;
        this.b = array;
    }
    
    public final Object a(final Object o) {
        if (o instanceof Activity) {
            return o;
        }
        Object a = o;
        if (o instanceof ContextWrapper) {
            a = this.a(((ContextWrapper)o).getBaseContext());
        }
        return a;
    }
    
    public Object b() {
        if (this.d == null) {
            this.d = this.a(this.c);
        }
        return this.d;
    }
    
    public final void c(final XmlPullParser xmlPullParser, final AttributeSet set, final Menu menu) {
        final b b = new b(menu);
        int i = xmlPullParser.getEventType();
        String name3;
        while (true) {
            while (i != 2) {
                int n = xmlPullParser.next();
                if ((i = n) == 1) {
                    int j = 0;
                    int n2 = 0;
                    String anObject = null;
                    while (j == 0) {
                        if (n == 1) {
                            throw new RuntimeException("Unexpected end of document");
                        }
                        int n3;
                        int n4;
                        String name;
                        if (n != 2) {
                            if (n != 3) {
                                n3 = j;
                                n4 = n2;
                                name = anObject;
                            }
                            else {
                                final String name2 = xmlPullParser.getName();
                                if (n2 != 0 && name2.equals(anObject)) {
                                    n4 = 0;
                                    name = null;
                                    n3 = j;
                                }
                                else if (name2.equals("group")) {
                                    b.h();
                                    n3 = j;
                                    n4 = n2;
                                    name = anObject;
                                }
                                else if (name2.equals("item")) {
                                    n3 = j;
                                    n4 = n2;
                                    name = anObject;
                                    if (!b.d()) {
                                        final d2 a = b.A;
                                        if (a != null && a.a()) {
                                            b.b();
                                            n3 = j;
                                            n4 = n2;
                                            name = anObject;
                                        }
                                        else {
                                            b.a();
                                            n3 = j;
                                            n4 = n2;
                                            name = anObject;
                                        }
                                    }
                                }
                                else {
                                    n3 = j;
                                    n4 = n2;
                                    name = anObject;
                                    if (name2.equals("menu")) {
                                        n3 = 1;
                                        n4 = n2;
                                        name = anObject;
                                    }
                                }
                            }
                        }
                        else if (n2 != 0) {
                            n3 = j;
                            n4 = n2;
                            name = anObject;
                        }
                        else {
                            name = xmlPullParser.getName();
                            if (name.equals("group")) {
                                b.f(set);
                                n3 = j;
                                n4 = n2;
                                name = anObject;
                            }
                            else if (name.equals("item")) {
                                b.g(set);
                                n3 = j;
                                n4 = n2;
                                name = anObject;
                            }
                            else if (name.equals("menu")) {
                                this.c(xmlPullParser, set, (Menu)b.b());
                                n3 = j;
                                n4 = n2;
                                name = anObject;
                            }
                            else {
                                n4 = 1;
                                n3 = j;
                            }
                        }
                        final int next = xmlPullParser.next();
                        j = n3;
                        n2 = n4;
                        anObject = name;
                        n = next;
                    }
                    return;
                }
            }
            name3 = xmlPullParser.getName();
            if (name3.equals("menu")) {
                final int n = xmlPullParser.next();
                continue;
            }
            break;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Expecting menu, got ");
        sb.append(name3);
        throw new RuntimeException(sb.toString());
    }
    
    public void inflate(final int n, final Menu menu) {
        if (!(menu instanceof ks1)) {
            super.inflate(n, menu);
            return;
        }
        XmlResourceParser layout = null;
        try {
            try {
                final XmlResourceParser xmlResourceParser = layout = this.c.getResources().getLayout(n);
                this.c((XmlPullParser)xmlResourceParser, Xml.asAttributeSet((XmlPullParser)xmlResourceParser), menu);
                if (xmlResourceParser != null) {
                    xmlResourceParser.close();
                }
            }
            finally {
                if (layout != null) {
                    layout.close();
                }
            }
        }
        catch (final IOException ex) {}
        catch (final XmlPullParserException ex2) {}
    }
    
    public static class a implements MenuItem$OnMenuItemClickListener
    {
        public static final Class[] c;
        public Object a;
        public Method b;
        
        static {
            c = new Class[] { MenuItem.class };
        }
        
        public a(final Object a, final String s) {
            this.a = a;
            final Class<?> class1 = a.getClass();
            try {
                this.b = class1.getMethod(s, (Class<?>[])a.c);
            }
            catch (final Exception cause) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Couldn't resolve menu item onClick handler ");
                sb.append(s);
                sb.append(" in class ");
                sb.append(class1.getName());
                final InflateException ex = new InflateException(sb.toString());
                ((Throwable)ex).initCause(cause);
                throw ex;
            }
        }
        
        public boolean onMenuItemClick(final MenuItem menuItem) {
            try {
                if (this.b.getReturnType() == Boolean.TYPE) {
                    return (boolean)this.b.invoke(this.a, menuItem);
                }
                this.b.invoke(this.a, menuItem);
                return true;
            }
            catch (final Exception cause) {
                throw new RuntimeException(cause);
            }
        }
    }
    
    public class b
    {
        public d2 A;
        public CharSequence B;
        public CharSequence C;
        public ColorStateList D;
        public PorterDuff$Mode E;
        public final ls1 F;
        public Menu a;
        public int b;
        public int c;
        public int d;
        public int e;
        public boolean f;
        public boolean g;
        public boolean h;
        public int i;
        public int j;
        public CharSequence k;
        public CharSequence l;
        public int m;
        public char n;
        public int o;
        public char p;
        public int q;
        public int r;
        public boolean s;
        public boolean t;
        public boolean u;
        public int v;
        public int w;
        public String x;
        public String y;
        public String z;
        
        public b(final ls1 f, final Menu a) {
            this.F = f;
            this.D = null;
            this.E = null;
            this.a = a;
            this.h();
        }
        
        public void a() {
            this.h = true;
            this.i(this.a.add(this.b, this.i, this.j, this.k));
        }
        
        public SubMenu b() {
            this.h = true;
            final SubMenu addSubMenu = this.a.addSubMenu(this.b, this.i, this.j, this.k);
            this.i(addSubMenu.getItem());
            return addSubMenu;
        }
        
        public final char c(final String s) {
            if (s == null) {
                return '\0';
            }
            return s.charAt(0);
        }
        
        public boolean d() {
            return this.h;
        }
        
        public final Object e(final String s, final Class[] parameterTypes, final Object[] initargs) {
            try {
                final Constructor<?> constructor = Class.forName(s, false, this.F.c.getClassLoader()).getConstructor((Class<?>[])parameterTypes);
                constructor.setAccessible(true);
                return constructor.newInstance(initargs);
            }
            catch (final Exception ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Cannot instantiate class: ");
                sb.append(s);
                Log.w("SupportMenuInflater", sb.toString(), (Throwable)ex);
                return null;
            }
        }
        
        public void f(final AttributeSet set) {
            final TypedArray obtainStyledAttributes = this.F.c.obtainStyledAttributes(set, bc1.w1);
            this.b = obtainStyledAttributes.getResourceId(bc1.y1, 0);
            this.c = obtainStyledAttributes.getInt(bc1.A1, 0);
            this.d = obtainStyledAttributes.getInt(bc1.B1, 0);
            this.e = obtainStyledAttributes.getInt(bc1.C1, 0);
            this.f = obtainStyledAttributes.getBoolean(bc1.z1, true);
            this.g = obtainStyledAttributes.getBoolean(bc1.x1, true);
            obtainStyledAttributes.recycle();
        }
        
        public void g(final AttributeSet set) {
            final tw1 u = tw1.u(this.F.c, set, bc1.D1);
            this.i = u.n(bc1.G1, 0);
            this.j = ((u.k(bc1.J1, this.c) & 0xFFFF0000) | (u.k(bc1.K1, this.d) & 0xFFFF));
            this.k = u.p(bc1.L1);
            this.l = u.p(bc1.M1);
            this.m = u.n(bc1.E1, 0);
            this.n = this.c(u.o(bc1.N1));
            this.o = u.k(bc1.U1, 4096);
            this.p = this.c(u.o(bc1.O1));
            this.q = u.k(bc1.Y1, 4096);
            final int p = bc1.P1;
            int r;
            if (u.s(p)) {
                r = (u.a(p, false) ? 1 : 0);
            }
            else {
                r = this.e;
            }
            this.r = r;
            this.s = u.a(bc1.H1, false);
            this.t = u.a(bc1.I1, this.f);
            this.u = u.a(bc1.F1, this.g);
            this.v = u.k(bc1.Z1, -1);
            this.z = u.o(bc1.Q1);
            this.w = u.n(bc1.R1, 0);
            this.x = u.o(bc1.T1);
            final String o = u.o(bc1.S1);
            this.y = o;
            final boolean b = o != null;
            if (b && this.w == 0 && this.x == null) {
                this.A = (d2)this.e(o, ls1.f, this.F.b);
            }
            else {
                if (b) {
                    Log.w("SupportMenuInflater", "Ignoring attribute 'actionProviderClass'. Action view already specified.");
                }
                this.A = null;
            }
            this.B = u.p(bc1.V1);
            this.C = u.p(bc1.a2);
            final int x1 = bc1.X1;
            if (u.s(x1)) {
                this.E = fv.e(u.k(x1, -1), this.E);
            }
            else {
                this.E = null;
            }
            final int w1 = bc1.W1;
            if (u.s(w1)) {
                this.D = u.c(w1);
            }
            else {
                this.D = null;
            }
            u.w();
            this.h = false;
        }
        
        public void h() {
            this.b = 0;
            this.c = 0;
            this.d = 0;
            this.e = 0;
            this.f = true;
            this.g = true;
        }
        
        public final void i(final MenuItem menuItem) {
            final MenuItem setEnabled = menuItem.setChecked(this.s).setVisible(this.t).setEnabled(this.u);
            final int r = this.r;
            boolean b = false;
            setEnabled.setCheckable(r >= 1).setTitleCondensed(this.l).setIcon(this.m);
            final int v = this.v;
            if (v >= 0) {
                menuItem.setShowAsAction(v);
            }
            if (this.z != null) {
                if (this.F.c.isRestricted()) {
                    throw new IllegalStateException("The android:onClick attribute cannot be used within a restricted context");
                }
                menuItem.setOnMenuItemClickListener((MenuItem$OnMenuItemClickListener)new a(this.F.b(), this.z));
            }
            if (this.r >= 2) {
                if (menuItem instanceof g) {
                    ((g)menuItem).t(true);
                }
                else if (menuItem instanceof ov0) {
                    ((ov0)menuItem).h(true);
                }
            }
            final String x = this.x;
            if (x != null) {
                menuItem.setActionView((View)this.e(x, ls1.e, this.F.a));
                b = true;
            }
            final int w = this.w;
            if (w > 0) {
                if (!b) {
                    menuItem.setActionView(w);
                }
                else {
                    Log.w("SupportMenuInflater", "Ignoring attribute 'itemActionViewLayout'. Action view already specified.");
                }
            }
            final d2 a = this.A;
            if (a != null) {
                mv0.a(menuItem, a);
            }
            mv0.c(menuItem, this.B);
            mv0.g(menuItem, this.C);
            mv0.b(menuItem, this.n, this.o);
            mv0.f(menuItem, this.p, this.q);
            final PorterDuff$Mode e = this.E;
            if (e != null) {
                mv0.e(menuItem, e);
            }
            final ColorStateList d = this.D;
            if (d != null) {
                mv0.d(menuItem, d);
            }
        }
    }
}

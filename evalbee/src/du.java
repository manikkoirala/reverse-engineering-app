import java.util.List;
import java.util.Collections;
import com.google.firebase.database.collection.c;
import java.util.Comparator;

// 
// Decompiled by Procyon v0.6.0
// 

public final class du implements Comparable
{
    public static final Comparator b;
    public static final c c;
    public final ke1 a;
    
    static {
        c = new c(Collections.emptyList(), b = new cu());
    }
    
    public du(final ke1 a) {
        g9.d(o(a), "Not a document key path: %s", a);
        this.a = a;
    }
    
    public static Comparator a() {
        return du.b;
    }
    
    public static du d() {
        return h(Collections.emptyList());
    }
    
    public static c e() {
        return du.c;
    }
    
    public static du f(final String s) {
        final ke1 q = ke1.q(s);
        final int l = q.l();
        boolean b2;
        final boolean b = b2 = false;
        if (l > 4) {
            b2 = b;
            if (q.h(0).equals("projects")) {
                b2 = b;
                if (q.h(2).equals("databases")) {
                    b2 = b;
                    if (q.h(4).equals("documents")) {
                        b2 = true;
                    }
                }
            }
        }
        g9.d(b2, "Tried to parse an invalid key: %s", q);
        return g((ke1)q.m(5));
    }
    
    public static du g(final ke1 ke1) {
        return new du(ke1);
    }
    
    public static du h(final List list) {
        return new du(ke1.p(list));
    }
    
    public static boolean o(final ke1 ke1) {
        return ke1.l() % 2 == 0;
    }
    
    public int c(final du du) {
        return this.a.e(du.a);
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o != null && du.class == o.getClass() && this.a.equals(((du)o).a));
    }
    
    @Override
    public int hashCode() {
        return this.a.hashCode();
    }
    
    public String j() {
        final ke1 a = this.a;
        return a.h(a.l() - 2);
    }
    
    public ke1 k() {
        return (ke1)this.a.n();
    }
    
    public String l() {
        return this.a.g();
    }
    
    public ke1 m() {
        return this.a;
    }
    
    public boolean n(final String anObject) {
        if (this.a.l() >= 2) {
            final ke1 a = this.a;
            if (((String)a.a.get(a.l() - 2)).equals(anObject)) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public String toString() {
        return this.a.toString();
    }
}

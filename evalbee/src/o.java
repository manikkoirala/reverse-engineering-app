import java.util.NoSuchElementException;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class o extends z02
{
    public final int a;
    public int b;
    
    public o(final int n) {
        this(n, 0);
    }
    
    public o(final int a, final int b) {
        i71.u(b, a);
        this.a = a;
        this.b = b;
    }
    
    public abstract Object b(final int p0);
    
    @Override
    public final boolean hasNext() {
        return this.b < this.a;
    }
    
    @Override
    public final boolean hasPrevious() {
        return this.b > 0;
    }
    
    @Override
    public final Object next() {
        if (this.hasNext()) {
            return this.b(this.b++);
        }
        throw new NoSuchElementException();
    }
    
    @Override
    public final int nextIndex() {
        return this.b;
    }
    
    @Override
    public final Object previous() {
        if (this.hasPrevious()) {
            final int b = this.b - 1;
            this.b = b;
            return this.b(b);
        }
        throw new NoSuchElementException();
    }
    
    @Override
    public final int previousIndex() {
        return this.b - 1;
    }
}

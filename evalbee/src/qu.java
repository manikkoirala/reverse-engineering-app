// 
// Decompiled by Procyon v0.6.0
// 

public abstract class qu
{
    public static double a(final double n) {
        i71.d(Double.isNaN(n) ^ true);
        return Math.max(n, 0.0);
    }
    
    public static long b(final double d) {
        i71.e(c(d), "not a normal value");
        final int exponent = Math.getExponent(d);
        final long n = Double.doubleToRawLongBits(d) & 0xFFFFFFFFFFFFFL;
        long n2;
        if (exponent == -1023) {
            n2 = n << 1;
        }
        else {
            n2 = (n | 0x10000000000000L);
        }
        return n2;
    }
    
    public static boolean c(final double d) {
        return Math.getExponent(d) <= 1023;
    }
}

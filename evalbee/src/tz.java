import androidx.datastore.preferences.protobuf.m;
import androidx.datastore.preferences.protobuf.l;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class tz
{
    public static final l a;
    public static final l b;
    
    static {
        a = new m();
        b = c();
    }
    
    public static l a() {
        final l b = tz.b;
        if (b != null) {
            return b;
        }
        throw new IllegalStateException("Protobuf runtime is not correctly loaded.");
    }
    
    public static l b() {
        return tz.a;
    }
    
    public static l c() {
        try {
            return (l)Class.forName("androidx.datastore.preferences.protobuf.ExtensionSchemaFull").getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
        }
        catch (final Exception ex) {
            return null;
        }
    }
}

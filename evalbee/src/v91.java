import android.os.BaseBundle;
import android.os.Bundle;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager$NameNotFoundException;
import com.google.android.gms.tasks.Tasks;
import com.google.android.gms.common.util.PlatformVersion;
import com.google.android.gms.tasks.Task;
import android.app.NotificationManager;
import android.util.Log;
import java.util.concurrent.Executor;
import android.os.Binder;
import com.google.android.gms.tasks.TaskCompletionSource;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class v91
{
    public static boolean b(final Context context) {
        return Binder.getCallingUid() == context.getApplicationInfo().uid;
    }
    
    public static void c(final Context context) {
        if (w91.b(context)) {
            return;
        }
        e(new xu0(), context, f(context));
    }
    
    public static Task e(final Executor executor, final Context context, final boolean b) {
        if (!PlatformVersion.isAtLeastQ()) {
            return Tasks.forResult((Object)null);
        }
        final TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        executor.execute(new u91(context, b, taskCompletionSource));
        return taskCompletionSource.getTask();
    }
    
    public static boolean f(Context applicationContext) {
        try {
            applicationContext = applicationContext.getApplicationContext();
            final PackageManager packageManager = applicationContext.getPackageManager();
            if (packageManager != null) {
                final ApplicationInfo applicationInfo = packageManager.getApplicationInfo(applicationContext.getPackageName(), 128);
                if (applicationInfo != null) {
                    final Bundle metaData = applicationInfo.metaData;
                    if (metaData != null && ((BaseBundle)metaData).containsKey("firebase_messaging_notification_delegation_enabled")) {
                        return ((BaseBundle)applicationInfo.metaData).getBoolean("firebase_messaging_notification_delegation_enabled");
                    }
                }
            }
            return true;
        }
        catch (final PackageManager$NameNotFoundException ex) {
            return true;
        }
    }
}

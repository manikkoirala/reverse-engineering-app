import java.util.Collection;
import com.google.common.collect.l;
import java.util.Iterator;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class t01
{
    public static Object a(final Object o, final int i) {
        if (o != null) {
            return o;
        }
        final StringBuilder sb = new StringBuilder(20);
        sb.append("at index ");
        sb.append(i);
        throw new NullPointerException(sb.toString());
    }
    
    public static Object[] b(final Object... array) {
        c(array, array.length);
        return array;
    }
    
    public static Object[] c(final Object[] array, final int n) {
        for (int i = 0; i < n; ++i) {
            a(array[i], i);
        }
        return array;
    }
    
    public static Object[] d(final Iterable iterable, final Object[] array) {
        final Iterator iterator = iterable.iterator();
        int n = 0;
        while (iterator.hasNext()) {
            array[n] = iterator.next();
            ++n;
        }
        return array;
    }
    
    public static Object[] e(final Object[] array, final int n) {
        return l.b(array, n);
    }
    
    public static Object[] f(final Collection collection) {
        return d(collection, new Object[collection.size()]);
    }
    
    public static Object[] g(final Collection collection, final Object[] array) {
        final int size = collection.size();
        Object[] e = array;
        if (array.length < size) {
            e = e(array, size);
        }
        d(collection, e);
        if (e.length > size) {
            e[size] = null;
        }
        return e;
    }
    
    public static Object[] h(final Object[] array, final int n, final int n2, final Object[] array2) {
        i71.w(n, n + n2, array.length);
        Object[] e;
        if (array2.length < n2) {
            e = e(array2, n2);
        }
        else {
            e = array2;
            if (array2.length > n2) {
                array2[n2] = null;
                e = array2;
            }
        }
        System.arraycopy(array, n, e, 0, n2);
        return e;
    }
}

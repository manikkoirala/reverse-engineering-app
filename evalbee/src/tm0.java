import androidx.datastore.preferences.protobuf.i0;
import androidx.datastore.preferences.protobuf.b0;
import androidx.datastore.preferences.protobuf.l;
import androidx.datastore.preferences.protobuf.a0;
import androidx.datastore.preferences.protobuf.g0;
import androidx.datastore.preferences.protobuf.t;
import androidx.datastore.preferences.protobuf.GeneratedMessageLite;
import androidx.datastore.preferences.protobuf.f0;
import androidx.datastore.preferences.protobuf.ProtoSyntax;
import androidx.datastore.preferences.protobuf.p;
import androidx.datastore.preferences.protobuf.r;

// 
// Decompiled by Procyon v0.6.0
// 

public final class tm0 implements pj1
{
    public static final wv0 b;
    public final wv0 a;
    
    static {
        b = new wv0() {
            @Override
            public uv0 a(final Class clazz) {
                throw new IllegalStateException("This should never be called.");
            }
            
            @Override
            public boolean b(final Class clazz) {
                return false;
            }
        };
    }
    
    public tm0() {
        this(b());
    }
    
    public tm0(final wv0 wv0) {
        this.a = (wv0)r.b(wv0, "messageInfoFactory");
    }
    
    public static wv0 b() {
        return new b(new wv0[] { p.c(), c() });
    }
    
    public static wv0 c() {
        try {
            return (wv0)Class.forName("androidx.datastore.preferences.protobuf.DescriptorMessageInfoFactory").getDeclaredMethod("getInstance", (Class<?>[])new Class[0]).invoke(null, new Object[0]);
        }
        catch (final Exception ex) {
            return tm0.b;
        }
    }
    
    public static boolean d(final uv0 uv0) {
        return uv0.c() == ProtoSyntax.PROTO2;
    }
    
    public static f0 e(final Class clazz, final uv0 uv0) {
        if (GeneratedMessageLite.class.isAssignableFrom(clazz)) {
            a0 a0;
            if (d(uv0)) {
                a0 = androidx.datastore.preferences.protobuf.a0.Q(clazz, uv0, iz0.b(), t.b(), g0.L(), tz.b(), an0.b());
            }
            else {
                a0 = androidx.datastore.preferences.protobuf.a0.Q(clazz, uv0, iz0.b(), t.b(), g0.L(), null, an0.b());
            }
            return a0;
        }
        a0 a2;
        if (d(uv0)) {
            a2 = a0.Q(clazz, uv0, iz0.a(), t.a(), g0.G(), tz.a(), an0.a());
        }
        else {
            a2 = a0.Q(clazz, uv0, iz0.a(), t.a(), g0.H(), null, an0.a());
        }
        return a2;
    }
    
    @Override
    public f0 a(final Class clazz) {
        g0.I(clazz);
        final uv0 a = this.a.a(clazz);
        if (a.a()) {
            i0 i0;
            l l;
            if (GeneratedMessageLite.class.isAssignableFrom(clazz)) {
                i0 = g0.L();
                l = tz.b();
            }
            else {
                i0 = g0.G();
                l = tz.a();
            }
            return b0.l(i0, l, a.b());
        }
        return e(clazz, a);
    }
    
    public static class b implements wv0
    {
        public wv0[] a;
        
        public b(final wv0... a) {
            this.a = a;
        }
        
        @Override
        public uv0 a(final Class clazz) {
            for (final wv0 wv0 : this.a) {
                if (wv0.b(clazz)) {
                    return wv0.a(clazz);
                }
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("No factory is available for message type: ");
            sb.append(clazz.getName());
            throw new UnsupportedOperationException(sb.toString());
        }
        
        @Override
        public boolean b(final Class clazz) {
            final wv0[] a = this.a;
            for (int length = a.length, i = 0; i < length; ++i) {
                if (a[i].b(clazz)) {
                    return true;
                }
            }
            return false;
        }
    }
}

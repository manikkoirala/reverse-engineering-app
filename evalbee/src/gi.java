import android.os.BaseBundle;
import android.content.res.Resources$Theme;
import android.media.RingtoneManager;
import android.app.NotificationManager;
import android.os.Build$VERSION;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager$NameNotFoundException;
import android.content.res.Resources$NotFoundException;
import android.graphics.Color;
import android.util.Log;
import android.net.Uri;
import android.content.res.Resources;
import android.text.TextUtils;
import android.os.Bundle;
import android.os.Parcelable;
import android.content.Intent;
import android.app.PendingIntent;
import android.content.pm.PackageManager;
import com.google.firebase.messaging.c;
import android.content.Context;
import android.os.SystemClock;
import java.util.concurrent.atomic.AtomicInteger;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class gi
{
    public static final AtomicInteger a;
    
    static {
        a = new AtomicInteger((int)SystemClock.elapsedRealtime());
    }
    
    public static PendingIntent a(final Context context, final c c, final String s, final PackageManager packageManager) {
        final Intent f = f(s, c, packageManager);
        if (f == null) {
            return null;
        }
        f.addFlags(67108864);
        f.putExtras(c.y());
        if (q(c)) {
            f.putExtra("gcm.n.analytics_data", c.x());
        }
        return PendingIntent.getActivity(context, g(), f, l(1073741824));
    }
    
    public static PendingIntent b(final Context context, final Context context2, final c c) {
        if (!q(c)) {
            return null;
        }
        return c(context, context2, new Intent("com.google.firebase.messaging.NOTIFICATION_DISMISS").putExtras(c.x()));
    }
    
    public static PendingIntent c(final Context context, final Context context2, final Intent intent) {
        return PendingIntent.getBroadcast(context, g(), new Intent("com.google.android.c2dm.intent.RECEIVE").setPackage(context2.getPackageName()).putExtra("wrapped_intent", (Parcelable)intent), l(1073741824));
    }
    
    public static a d(final Context context, final Context context2, final c c, final String s, final Bundle bundle) {
        final String packageName = context2.getPackageName();
        final Resources resources = context2.getResources();
        final PackageManager packageManager = context2.getPackageManager();
        final rz0.e e = new rz0.e(context2, s);
        final String n = c.n(resources, packageName, "gcm.n.title");
        if (!TextUtils.isEmpty((CharSequence)n)) {
            e.k(n);
        }
        final String n2 = c.n(resources, packageName, "gcm.n.body");
        if (!TextUtils.isEmpty((CharSequence)n2)) {
            e.j(n2);
            e.x(new rz0.c().h(n2));
        }
        e.v(m(packageManager, resources, packageName, c.p("gcm.n.icon"), bundle));
        final Uri n3 = n(packageName, c, resources);
        if (n3 != null) {
            e.w(n3);
        }
        e.i(a(context, c, packageName, packageManager));
        final PendingIntent b = b(context, context2, c);
        if (b != null) {
            e.m(b);
        }
        final Integer h = h(context2, c.p("gcm.n.color"), bundle);
        if (h != null) {
            e.h(h);
        }
        e.f(c.a("gcm.n.sticky") ^ true);
        e.q(c.a("gcm.n.local_only"));
        final String p5 = c.p("gcm.n.ticker");
        if (p5 != null) {
            e.y(p5);
        }
        final Integer m = c.m();
        if (m != null) {
            e.s(m);
        }
        final Integer r = c.r();
        if (r != null) {
            e.A(r);
        }
        final Integer l = c.l();
        if (l != null) {
            e.r(l);
        }
        final Long j = c.j("gcm.n.event_time");
        if (j != null) {
            e.u(true);
            e.B(j);
        }
        final long[] q = c.q();
        if (q != null) {
            e.z(q);
        }
        final int[] e2 = c.e();
        if (e2 != null) {
            e.p(e2[0], e2[1], e2[2]);
        }
        e.l(i(c));
        return new a(e, o(c), 0);
    }
    
    public static a e(final Context context, final c c) {
        final Bundle j = j(context.getPackageManager(), context.getPackageName());
        return d(context, context, c, k(context, c.k(), j), j);
    }
    
    public static Intent f(final String s, final c c, final PackageManager packageManager) {
        final String p3 = c.p("gcm.n.click_action");
        if (!TextUtils.isEmpty((CharSequence)p3)) {
            final Intent intent = new Intent(p3);
            intent.setPackage(s);
            intent.setFlags(268435456);
            return intent;
        }
        final Uri f = c.f();
        if (f != null) {
            final Intent intent2 = new Intent("android.intent.action.VIEW");
            intent2.setPackage(s);
            intent2.setData(f);
            return intent2;
        }
        final Intent launchIntentForPackage = packageManager.getLaunchIntentForPackage(s);
        if (launchIntentForPackage == null) {
            Log.w("FirebaseMessaging", "No activity found to launch app");
        }
        return launchIntentForPackage;
    }
    
    public static int g() {
        return gi.a.incrementAndGet();
    }
    
    public static Integer h(final Context context, final String str, final Bundle bundle) {
        if (!TextUtils.isEmpty((CharSequence)str)) {
            try {
                return Color.parseColor(str);
            }
            catch (final IllegalArgumentException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Color is invalid: ");
                sb.append(str);
                sb.append(". Notification will use default color.");
                Log.w("FirebaseMessaging", sb.toString());
            }
        }
        final int int1 = ((BaseBundle)bundle).getInt("com.google.firebase.messaging.default_notification_color", 0);
        if (int1 != 0) {
            try {
                return sl.getColor(context, int1);
            }
            catch (final Resources$NotFoundException ex2) {
                Log.w("FirebaseMessaging", "Cannot find the color resource referenced in AndroidManifest.");
            }
        }
        return null;
    }
    
    public static int i(final c c) {
        int a;
        final boolean b = (a = (c.a("gcm.n.default_sound") ? 1 : 0)) != 0;
        if (c.a("gcm.n.default_vibrate_timings")) {
            a = ((b ? 1 : 0) | 0x2);
        }
        int n = a;
        if (c.a("gcm.n.default_light_settings")) {
            n = (a | 0x4);
        }
        return n;
    }
    
    public static Bundle j(final PackageManager packageManager, final String s) {
        try {
            final ApplicationInfo applicationInfo = packageManager.getApplicationInfo(s, 128);
            if (applicationInfo != null) {
                final Bundle metaData = applicationInfo.metaData;
                if (metaData != null) {
                    return metaData;
                }
            }
        }
        catch (final PackageManager$NameNotFoundException obj) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Couldn't get own application info: ");
            sb.append(obj);
            Log.w("FirebaseMessaging", sb.toString());
        }
        return Bundle.EMPTY;
    }
    
    public static String k(final Context context, String string, final Bundle bundle) {
        if (Build$VERSION.SDK_INT < 26) {
            return null;
        }
        try {
            if (context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).targetSdkVersion < 26) {
                return null;
            }
            final NotificationManager notificationManager = (NotificationManager)context.getSystemService((Class)NotificationManager.class);
            if (!TextUtils.isEmpty((CharSequence)string)) {
                if (pj2.a(notificationManager, string) != null) {
                    return string;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Notification Channel requested (");
                sb.append(string);
                sb.append(") has not been created by the app. Manifest configuration, or default, value will be used.");
                Log.w("FirebaseMessaging", sb.toString());
            }
            string = ((BaseBundle)bundle).getString("com.google.firebase.messaging.default_notification_channel_id");
            if (!TextUtils.isEmpty((CharSequence)string)) {
                if (pj2.a(notificationManager, string) != null) {
                    return string;
                }
                string = "Notification Channel set in AndroidManifest.xml has not been created by the app. Default value will be used.";
            }
            else {
                string = "Missing Default Notification Channel metadata in AndroidManifest. Default value will be used.";
            }
            Log.w("FirebaseMessaging", string);
            if (pj2.a(notificationManager, "fcm_fallback_notification_channel") == null) {
                final int identifier = context.getResources().getIdentifier("fcm_fallback_notification_channel_label", "string", context.getPackageName());
                String string2;
                if (identifier == 0) {
                    Log.e("FirebaseMessaging", "String resource \"fcm_fallback_notification_channel_label\" is not found. Using default string channel name.");
                    string2 = "Misc";
                }
                else {
                    string2 = context.getString(identifier);
                }
                fh.a(notificationManager, eh.a("fcm_fallback_notification_channel", string2, 3));
            }
            return "fcm_fallback_notification_channel";
        }
        catch (final PackageManager$NameNotFoundException ex) {
            return null;
        }
    }
    
    public static int l(final int n) {
        return n | 0x4000000;
    }
    
    public static int m(final PackageManager packageManager, final Resources resources, final String s, final String str, final Bundle bundle) {
        if (!TextUtils.isEmpty((CharSequence)str)) {
            final int identifier = resources.getIdentifier(str, "drawable", s);
            if (identifier != 0 && p(resources, identifier)) {
                return identifier;
            }
            final int identifier2 = resources.getIdentifier(str, "mipmap", s);
            if (identifier2 != 0 && p(resources, identifier2)) {
                return identifier2;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Icon resource ");
            sb.append(str);
            sb.append(" not found. Notification will use default icon.");
            Log.w("FirebaseMessaging", sb.toString());
        }
        final int int1 = ((BaseBundle)bundle).getInt("com.google.firebase.messaging.default_notification_icon", 0);
        int icon = 0;
        Label_0190: {
            if (int1 != 0) {
                icon = int1;
                if (p(resources, int1)) {
                    break Label_0190;
                }
            }
            try {
                icon = packageManager.getApplicationInfo(s, 0).icon;
            }
            catch (final PackageManager$NameNotFoundException obj) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Couldn't get own application info: ");
                sb2.append(obj);
                Log.w("FirebaseMessaging", sb2.toString());
                icon = int1;
            }
        }
        if (icon != 0) {
            final int n = icon;
            if (p(resources, icon)) {
                return n;
            }
        }
        return 17301651;
    }
    
    public static Uri n(final String str, final c c, final Resources resources) {
        final String o = c.o();
        if (TextUtils.isEmpty((CharSequence)o)) {
            return null;
        }
        if (!"default".equals(o) && resources.getIdentifier(o, "raw", str) != 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("android.resource://");
            sb.append(str);
            sb.append("/raw/");
            sb.append(o);
            return Uri.parse(sb.toString());
        }
        return RingtoneManager.getDefaultUri(2);
    }
    
    public static String o(final c c) {
        final String p = c.p("gcm.n.tag");
        if (!TextUtils.isEmpty((CharSequence)p)) {
            return p;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("FCM-Notification:");
        sb.append(SystemClock.uptimeMillis());
        return sb.toString();
    }
    
    public static boolean p(final Resources resources, final int n) {
        if (Build$VERSION.SDK_INT != 26) {
            return true;
        }
        try {
            if (fi.a((Object)resources.getDrawable(n, (Resources$Theme)null))) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Adaptive icons cannot be used in notifications. Ignoring icon id: ");
                sb.append(n);
                Log.e("FirebaseMessaging", sb.toString());
                return false;
            }
            return true;
        }
        catch (final Resources$NotFoundException ex) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Couldn't find resource ");
            sb2.append(n);
            sb2.append(", treating it as an invalid icon");
            Log.e("FirebaseMessaging", sb2.toString());
            return false;
        }
    }
    
    public static boolean q(final c c) {
        return c.a("google.c.a.e");
    }
    
    public static class a
    {
        public final rz0.e a;
        public final String b;
        public final int c;
        
        public a(final rz0.e a, final String b, final int c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
    }
}

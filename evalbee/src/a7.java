import android.widget.AutoCompleteTextView;
import android.graphics.drawable.Drawable;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.EditorInfo;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import android.text.method.KeyListener;
import android.widget.EditText;
import android.widget.TextView;
import android.view.View;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.MultiAutoCompleteTextView;

// 
// Decompiled by Procyon v0.6.0
// 

public class a7 extends MultiAutoCompleteTextView
{
    public static final int[] d;
    public final x5 a;
    public final l7 b;
    public final u6 c;
    
    static {
        d = new int[] { 16843126 };
    }
    
    public a7(final Context context, final AttributeSet set) {
        this(context, set, sa1.m);
    }
    
    public a7(final Context context, final AttributeSet set, final int n) {
        super(qw1.b(context), set, n);
        pv1.a((View)this, ((View)this).getContext());
        final tw1 v = tw1.v(((View)this).getContext(), set, a7.d, n, 0);
        if (v.s(0)) {
            ((AutoCompleteTextView)this).setDropDownBackgroundDrawable(v.g(0));
        }
        v.w();
        (this.a = new x5((View)this)).e(set, n);
        final l7 b = new l7((TextView)this);
        (this.b = b).m(set, n);
        b.b();
        final u6 c = new u6((EditText)this);
        (this.c = c).d(set, n);
        this.a(c);
    }
    
    public void a(final u6 u6) {
        final KeyListener keyListener = ((TextView)this).getKeyListener();
        if (u6.b(keyListener)) {
            final boolean focusable = super.isFocusable();
            final boolean clickable = super.isClickable();
            final boolean longClickable = super.isLongClickable();
            final int inputType = super.getInputType();
            final KeyListener a = u6.a(keyListener);
            if (a == keyListener) {
                return;
            }
            super.setKeyListener(a);
            super.setRawInputType(inputType);
            super.setFocusable(focusable);
            super.setClickable(clickable);
            super.setLongClickable(longClickable);
        }
    }
    
    public void drawableStateChanged() {
        super.drawableStateChanged();
        final x5 a = this.a;
        if (a != null) {
            a.b();
        }
        final l7 b = this.b;
        if (b != null) {
            b.b();
        }
    }
    
    public ColorStateList getSupportBackgroundTintList() {
        final x5 a = this.a;
        ColorStateList c;
        if (a != null) {
            c = a.c();
        }
        else {
            c = null;
        }
        return c;
    }
    
    public PorterDuff$Mode getSupportBackgroundTintMode() {
        final x5 a = this.a;
        PorterDuff$Mode d;
        if (a != null) {
            d = a.d();
        }
        else {
            d = null;
        }
        return d;
    }
    
    public ColorStateList getSupportCompoundDrawablesTintList() {
        return this.b.j();
    }
    
    public PorterDuff$Mode getSupportCompoundDrawablesTintMode() {
        return this.b.k();
    }
    
    public InputConnection onCreateInputConnection(final EditorInfo editorInfo) {
        return this.c.e(w6.a(super.onCreateInputConnection(editorInfo), editorInfo, (View)this), editorInfo);
    }
    
    public void setBackgroundDrawable(final Drawable backgroundDrawable) {
        super.setBackgroundDrawable(backgroundDrawable);
        final x5 a = this.a;
        if (a != null) {
            a.f(backgroundDrawable);
        }
    }
    
    public void setBackgroundResource(final int backgroundResource) {
        super.setBackgroundResource(backgroundResource);
        final x5 a = this.a;
        if (a != null) {
            a.g(backgroundResource);
        }
    }
    
    public void setCompoundDrawables(final Drawable drawable, final Drawable drawable2, final Drawable drawable3, final Drawable drawable4) {
        super.setCompoundDrawables(drawable, drawable2, drawable3, drawable4);
        final l7 b = this.b;
        if (b != null) {
            b.p();
        }
    }
    
    public void setCompoundDrawablesRelative(final Drawable drawable, final Drawable drawable2, final Drawable drawable3, final Drawable drawable4) {
        super.setCompoundDrawablesRelative(drawable, drawable2, drawable3, drawable4);
        final l7 b = this.b;
        if (b != null) {
            b.p();
        }
    }
    
    public void setDropDownBackgroundResource(final int n) {
        ((AutoCompleteTextView)this).setDropDownBackgroundDrawable(g7.b(((View)this).getContext(), n));
    }
    
    public void setEmojiCompatEnabled(final boolean b) {
        this.c.f(b);
    }
    
    public void setKeyListener(final KeyListener keyListener) {
        super.setKeyListener(this.c.a(keyListener));
    }
    
    public void setSupportBackgroundTintList(final ColorStateList list) {
        final x5 a = this.a;
        if (a != null) {
            a.i(list);
        }
    }
    
    public void setSupportBackgroundTintMode(final PorterDuff$Mode porterDuff$Mode) {
        final x5 a = this.a;
        if (a != null) {
            a.j(porterDuff$Mode);
        }
    }
    
    public void setSupportCompoundDrawablesTintList(final ColorStateList list) {
        this.b.w(list);
        this.b.b();
    }
    
    public void setSupportCompoundDrawablesTintMode(final PorterDuff$Mode porterDuff$Mode) {
        this.b.x(porterDuff$Mode);
        this.b.b();
    }
    
    public void setTextAppearance(final Context context, final int n) {
        super.setTextAppearance(context, n);
        final l7 b = this.b;
        if (b != null) {
            b.q(context, n);
        }
    }
}

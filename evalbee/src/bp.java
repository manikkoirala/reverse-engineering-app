import java.nio.channels.FileChannel;
import android.database.AbstractCursor;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.io.FileInputStream;
import java.io.File;
import android.database.AbstractWindowedCursor;
import android.database.Cursor;
import android.os.CancellationSignal;
import androidx.room.RoomDatabase;
import java.util.Iterator;
import java.util.List;
import java.io.Closeable;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class bp
{
    public static final void a(final ss1 ss1) {
        fg0.e((Object)ss1, "db");
        final List c = mh.c();
        Object h0 = ss1.h0("SELECT name FROM sqlite_master WHERE type = 'trigger'");
        try {
            while (((Cursor)h0).moveToNext()) {
                c.add(((Cursor)h0).getString(0));
            }
            final u02 a = u02.a;
            dh.a((Closeable)h0, (Throwable)null);
            final Iterator iterator = mh.a(c).iterator();
            while (iterator.hasNext()) {
                h0 = iterator.next();
                fg0.d(h0, "triggerName");
                if (qr1.t((String)h0, "room_fts_content_sync_", false, 2, (Object)null)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("DROP TRIGGER IF EXISTS ");
                    sb.append((String)h0);
                    ss1.M(sb.toString());
                }
            }
        }
        finally {
            try {}
            finally {
                final Throwable t;
                dh.a((Closeable)h0, t);
            }
        }
    }
    
    public static final Cursor b(final RoomDatabase roomDatabase, final vs1 vs1, final boolean b, final CancellationSignal cancellationSignal) {
        fg0.e((Object)roomDatabase, "db");
        fg0.e((Object)vs1, "sqLiteQuery");
        Cursor cursor2;
        final Cursor cursor = cursor2 = roomDatabase.A(vs1, cancellationSignal);
        if (b) {
            cursor2 = cursor;
            if (cursor instanceof AbstractWindowedCursor) {
                final AbstractWindowedCursor abstractWindowedCursor = (AbstractWindowedCursor)cursor;
                final int count = ((AbstractCursor)abstractWindowedCursor).getCount();
                int numRows;
                if (abstractWindowedCursor.hasWindow()) {
                    numRows = abstractWindowedCursor.getWindow().getNumRows();
                }
                else {
                    numRows = count;
                }
                cursor2 = cursor;
                if (numRows < count) {
                    cursor2 = lo.a(cursor);
                }
            }
        }
        return cursor2;
    }
    
    public static final int c(File channel) {
        fg0.e((Object)channel, "databaseFile");
        channel = (File)new FileInputStream(channel).getChannel();
        try {
            final ByteBuffer allocate = ByteBuffer.allocate(4);
            ((FileChannel)channel).tryLock(60L, 4L, true);
            ((FileChannel)channel).position(60L);
            if (((FileChannel)channel).read(allocate) == 4) {
                allocate.rewind();
                final int int1 = allocate.getInt();
                dh.a((Closeable)channel, (Throwable)null);
                return int1;
            }
            throw new IOException("Bad database header, unable to read 4 bytes at offset 60");
        }
        finally {
            try {}
            finally {
                final Throwable t;
                dh.a((Closeable)channel, t);
            }
        }
    }
}

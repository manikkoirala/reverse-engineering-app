import android.widget.Adapter;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import android.widget.AdapterView;
import java.lang.reflect.Method;
import android.os.Build$VERSION;
import android.widget.AbsListView;
import android.view.MotionEvent;
import android.view.ViewGroup$LayoutParams;
import android.widget.ListAdapter;
import android.view.View$MeasureSpec;
import android.view.ViewGroup;
import android.graphics.drawable.Drawable;
import android.graphics.Canvas;
import android.view.View;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.Rect;
import android.widget.ListView;

// 
// Decompiled by Procyon v0.6.0
// 

public class kv extends ListView
{
    public final Rect a;
    public int b;
    public int c;
    public int d;
    public int e;
    public int f;
    public d g;
    public boolean h;
    public boolean i;
    public boolean j;
    public i42 k;
    public fk0 l;
    public f m;
    
    public kv(final Context context, final boolean i) {
        super(context, (AttributeSet)null, sa1.z);
        this.a = new Rect();
        this.b = 0;
        this.c = 0;
        this.d = 0;
        this.e = 0;
        this.i = i;
        ((AbsListView)this).setCacheColorHint(0);
    }
    
    public final void a() {
        ((View)this).setPressed(this.j = false);
        this.drawableStateChanged();
        final View child = ((ViewGroup)this).getChildAt(this.f - ((AdapterView)this).getFirstVisiblePosition());
        if (child != null) {
            child.setPressed(false);
        }
        final i42 k = this.k;
        if (k != null) {
            k.c();
            this.k = null;
        }
    }
    
    public final void b(final View view, final int n) {
        ((AdapterView)this).performItemClick(view, n, ((AdapterView)this).getItemIdAtPosition(n));
    }
    
    public final void c(final Canvas canvas) {
        if (!this.a.isEmpty()) {
            final Drawable selector = ((AbsListView)this).getSelector();
            if (selector != null) {
                selector.setBounds(this.a);
                selector.draw(canvas);
            }
        }
    }
    
    public int d(int n, int listPaddingTop, int listPaddingBottom, final int n2, final int n3) {
        listPaddingTop = ((AbsListView)this).getListPaddingTop();
        listPaddingBottom = ((AbsListView)this).getListPaddingBottom();
        int dividerHeight = this.getDividerHeight();
        final Drawable divider = this.getDivider();
        final ListAdapter adapter = this.getAdapter();
        listPaddingBottom += listPaddingTop;
        if (adapter == null) {
            return listPaddingBottom;
        }
        if (dividerHeight <= 0 || divider == null) {
            dividerHeight = 0;
        }
        final int count = ((Adapter)adapter).getCount();
        int i = 0;
        int n4 = listPaddingTop = 0;
        View view = null;
        while (i < count) {
            final int itemViewType = ((Adapter)adapter).getItemViewType(i);
            int n5;
            if (itemViewType != (n5 = n4)) {
                view = null;
                n5 = itemViewType;
            }
            final View view2 = ((Adapter)adapter).getView(i, view, (ViewGroup)this);
            ViewGroup$LayoutParams layoutParams;
            if ((layoutParams = view2.getLayoutParams()) == null) {
                layoutParams = ((ViewGroup)this).generateDefaultLayoutParams();
                view2.setLayoutParams(layoutParams);
            }
            final int height = layoutParams.height;
            int n6;
            if (height > 0) {
                n6 = View$MeasureSpec.makeMeasureSpec(height, 1073741824);
            }
            else {
                n6 = View$MeasureSpec.makeMeasureSpec(0, 0);
            }
            view2.measure(n, n6);
            view2.forceLayout();
            int n7 = listPaddingBottom;
            if (i > 0) {
                n7 = listPaddingBottom + dividerHeight;
            }
            listPaddingBottom = n7 + view2.getMeasuredHeight();
            if (listPaddingBottom >= n2) {
                n = n2;
                if (n3 >= 0) {
                    n = n2;
                    if (i > n3) {
                        n = n2;
                        if (listPaddingTop > 0 && listPaddingBottom != (n = n2)) {
                            n = listPaddingTop;
                        }
                    }
                }
                return n;
            }
            int n8 = listPaddingTop;
            if (n3 >= 0) {
                n8 = listPaddingTop;
                if (i >= n3) {
                    n8 = listPaddingBottom;
                }
            }
            ++i;
            n4 = n5;
            view = view2;
            listPaddingTop = n8;
        }
        return listPaddingBottom;
    }
    
    public void dispatchDraw(final Canvas canvas) {
        this.c(canvas);
        super.dispatchDraw(canvas);
    }
    
    public void drawableStateChanged() {
        if (this.m != null) {
            return;
        }
        super.drawableStateChanged();
        this.j(true);
        this.n();
    }
    
    public boolean e(final MotionEvent motionEvent, int n) {
        final int actionMasked = motionEvent.getActionMasked();
        boolean b = false;
    Label_0139:
        while (true) {
            int pointerIndex;
            while (true) {
                Label_0045: {
                    if (actionMasked == 1) {
                        b = false;
                        break Label_0045;
                    }
                    if (actionMasked == 2) {
                        b = true;
                        break Label_0045;
                    }
                    if (actionMasked == 3) {
                        break Label_0028;
                    }
                    b = true;
                    n = 0;
                    break Label_0139;
                    n = 0;
                    b = false;
                    break Label_0139;
                }
                pointerIndex = motionEvent.findPointerIndex(n);
                if (pointerIndex < 0) {
                    continue;
                }
                break;
            }
            n = (int)motionEvent.getX(pointerIndex);
            final int n2 = (int)motionEvent.getY(pointerIndex);
            final int pointToPosition = ((AbsListView)this).pointToPosition(n, n2);
            if (pointToPosition == -1) {
                n = 1;
            }
            else {
                final View child = ((ViewGroup)this).getChildAt(pointToPosition - ((AdapterView)this).getFirstVisiblePosition());
                this.i(child, pointToPosition, (float)n, (float)n2);
                if (actionMasked == 1) {
                    this.b(child, pointToPosition);
                }
                continue;
            }
            break;
        }
        if (!b || n != 0) {
            this.a();
        }
        if (b) {
            if (this.l == null) {
                this.l = new fk0(this);
            }
            this.l.m(true);
            this.l.onTouch((View)this, motionEvent);
        }
        else {
            final fk0 l = this.l;
            if (l != null) {
                l.m(false);
            }
        }
        return b;
    }
    
    public final void f(final int n, final View view) {
        final Rect a = this.a;
        a.set(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
        a.left -= this.b;
        a.top -= this.c;
        a.right += this.d;
        a.bottom += this.e;
        final boolean k = this.k();
        if (view.isEnabled() != k) {
            this.l(k ^ true);
            if (n != -1) {
                ((View)this).refreshDrawableState();
            }
        }
    }
    
    public final void g(final int n, final View view) {
        final Drawable selector = ((AbsListView)this).getSelector();
        boolean b = true;
        final boolean b2 = selector != null && n != -1;
        if (b2) {
            selector.setVisible(false, false);
        }
        this.f(n, view);
        if (b2) {
            final Rect a = this.a;
            final float exactCenterX = a.exactCenterX();
            final float exactCenterY = a.exactCenterY();
            if (((View)this).getVisibility() != 0) {
                b = false;
            }
            selector.setVisible(b, false);
            wu.k(selector, exactCenterX, exactCenterY);
        }
    }
    
    public final void h(final int n, final View view, final float n2, final float n3) {
        this.g(n, view);
        final Drawable selector = ((AbsListView)this).getSelector();
        if (selector != null && n != -1) {
            wu.k(selector, n2, n3);
        }
    }
    
    public boolean hasFocus() {
        return this.i || super.hasFocus();
    }
    
    public boolean hasWindowFocus() {
        return this.i || super.hasWindowFocus();
    }
    
    public final void i(final View view, final int f, final float n, final float n2) {
        this.j = true;
        kv.a.a((View)this, n, n2);
        if (!((View)this).isPressed()) {
            ((View)this).setPressed(true);
        }
        ((AbsListView)this).layoutChildren();
        final int f2 = this.f;
        if (f2 != -1) {
            final View child = ((ViewGroup)this).getChildAt(f2 - ((AdapterView)this).getFirstVisiblePosition());
            if (child != null && child != view && child.isPressed()) {
                child.setPressed(false);
            }
        }
        this.f = f;
        kv.a.a(view, n - view.getLeft(), n2 - view.getTop());
        if (!view.isPressed()) {
            view.setPressed(true);
        }
        this.h(f, view, n, n2);
        this.j(false);
        ((View)this).refreshDrawableState();
    }
    
    public boolean isFocused() {
        return this.i || super.isFocused();
    }
    
    public boolean isInTouchMode() {
        return (this.i && this.h) || super.isInTouchMode();
    }
    
    public final void j(final boolean b) {
        final d g = this.g;
        if (g != null) {
            g.a(b);
        }
    }
    
    public final boolean k() {
        if (yc.c()) {
            return kv.c.a((AbsListView)this);
        }
        return kv.e.a((AbsListView)this);
    }
    
    public final void l(final boolean b) {
        if (yc.c()) {
            kv.c.b((AbsListView)this, b);
        }
        else {
            kv.e.b((AbsListView)this, b);
        }
    }
    
    public final boolean m() {
        return this.j;
    }
    
    public final void n() {
        final Drawable selector = ((AbsListView)this).getSelector();
        if (selector != null && this.m() && ((View)this).isPressed()) {
            selector.setState(((View)this).getDrawableState());
        }
    }
    
    public void onDetachedFromWindow() {
        this.m = null;
        super.onDetachedFromWindow();
    }
    
    public boolean onHoverEvent(final MotionEvent motionEvent) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT < 26) {
            return super.onHoverEvent(motionEvent);
        }
        final int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 10 && this.m == null) {
            (this.m = new f()).b();
        }
        final boolean onHoverEvent = super.onHoverEvent(motionEvent);
        if (actionMasked != 9 && actionMasked != 7) {
            ((AdapterView)this).setSelection(-1);
        }
        else {
            final int pointToPosition = ((AbsListView)this).pointToPosition((int)motionEvent.getX(), (int)motionEvent.getY());
            if (pointToPosition != -1 && pointToPosition != ((AdapterView)this).getSelectedItemPosition()) {
                final View child = ((ViewGroup)this).getChildAt(pointToPosition - ((AdapterView)this).getFirstVisiblePosition());
                if (child.isEnabled()) {
                    ((View)this).requestFocus();
                    if (sdk_INT >= 30 && kv.b.a()) {
                        kv.b.b(this, pointToPosition, child);
                    }
                    else {
                        ((AbsListView)this).setSelectionFromTop(pointToPosition, child.getTop() - ((View)this).getTop());
                    }
                }
                this.n();
            }
        }
        return onHoverEvent;
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            this.f = ((AbsListView)this).pointToPosition((int)motionEvent.getX(), (int)motionEvent.getY());
        }
        final f m = this.m;
        if (m != null) {
            m.a();
        }
        return super.onTouchEvent(motionEvent);
    }
    
    public void setListSelectionHidden(final boolean h) {
        this.h = h;
    }
    
    public void setSelector(final Drawable drawable) {
        d g;
        if (drawable != null) {
            g = new d(drawable);
        }
        else {
            g = null;
        }
        super.setSelector((Drawable)(this.g = g));
        final Rect rect = new Rect();
        if (drawable != null) {
            drawable.getPadding(rect);
        }
        this.b = rect.left;
        this.c = rect.top;
        this.d = rect.right;
        this.e = rect.bottom;
    }
    
    public abstract static class a
    {
        public static void a(final View view, final float n, final float n2) {
            view.drawableHotspotChanged(n, n2);
        }
    }
    
    public abstract static class b
    {
        public static Method a;
        public static Method b;
        public static Method c;
        public static boolean d;
        
        static {
            try {
                final Class<Integer> type = Integer.TYPE;
                final Class<Boolean> type2 = Boolean.TYPE;
                final Class<Float> type3 = Float.TYPE;
                (kv.b.a = AbsListView.class.getDeclaredMethod("positionSelector", type, View.class, type2, type3, type3)).setAccessible(true);
                (kv.b.b = AdapterView.class.getDeclaredMethod("setSelectedPositionInt", type)).setAccessible(true);
                (kv.b.c = AdapterView.class.getDeclaredMethod("setNextSelectedPositionInt", type)).setAccessible(true);
                kv.b.d = true;
            }
            catch (final NoSuchMethodException ex) {
                ex.printStackTrace();
            }
        }
        
        public static boolean a() {
            return kv.b.d;
        }
        
        public static void b(final kv obj, final int i, final View view) {
            try {
                kv.b.a.invoke(obj, i, view, Boolean.FALSE, -1, -1);
                kv.b.b.invoke(obj, i);
                kv.b.c.invoke(obj, i);
                return;
            }
            catch (final InvocationTargetException obj) {}
            catch (final IllegalAccessException ex) {}
            obj.printStackTrace();
        }
    }
    
    public abstract static class c
    {
        public static boolean a(final AbsListView absListView) {
            return absListView.isSelectedChildViewEnabled();
        }
        
        public static void b(final AbsListView absListView, final boolean selectedChildViewEnabled) {
            absListView.setSelectedChildViewEnabled(selectedChildViewEnabled);
        }
    }
    
    public static class d extends gv
    {
        public boolean a;
        
        public d(final Drawable drawable) {
            super(drawable);
            this.a = true;
        }
        
        public void a(final boolean a) {
            this.a = a;
        }
        
        @Override
        public void draw(final Canvas canvas) {
            if (this.a) {
                super.draw(canvas);
            }
        }
        
        @Override
        public void setHotspot(final float n, final float n2) {
            if (this.a) {
                super.setHotspot(n, n2);
            }
        }
        
        @Override
        public void setHotspotBounds(final int n, final int n2, final int n3, final int n4) {
            if (this.a) {
                super.setHotspotBounds(n, n2, n3, n4);
            }
        }
        
        @Override
        public boolean setState(final int[] state) {
            return this.a && super.setState(state);
        }
        
        @Override
        public boolean setVisible(final boolean b, final boolean b2) {
            return this.a && super.setVisible(b, b2);
        }
    }
    
    public abstract static class e
    {
        public static final Field a;
        
        static {
            Field declaredField = null;
            try {
                final Field field = declaredField = AbsListView.class.getDeclaredField("mIsChildViewEnabled");
                field.setAccessible(true);
                declaredField = field;
            }
            catch (final NoSuchFieldException ex) {
                ex.printStackTrace();
            }
            a = declaredField;
        }
        
        public static boolean a(final AbsListView obj) {
            final Field a = e.a;
            if (a != null) {
                try {
                    return a.getBoolean(obj);
                }
                catch (final IllegalAccessException ex) {
                    ex.printStackTrace();
                }
            }
            return false;
        }
        
        public static void b(final AbsListView obj, final boolean b) {
            final Field a = e.a;
            if (a != null) {
                try {
                    a.set(obj, b);
                }
                catch (final IllegalAccessException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
    
    public class f implements Runnable
    {
        public final kv a;
        
        public f(final kv a) {
            this.a = a;
        }
        
        public void a() {
            final kv a = this.a;
            a.m = null;
            ((View)a).removeCallbacks((Runnable)this);
        }
        
        public void b() {
            ((View)this.a).post((Runnable)this);
        }
        
        @Override
        public void run() {
            final kv a = this.a;
            a.m = null;
            a.drawableStateChanged();
        }
    }
}

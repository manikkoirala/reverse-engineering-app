import java.util.Set;
import com.google.gson.internal.LinkedTreeMap;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ph0 extends nh0
{
    public final LinkedTreeMap a;
    
    public ph0() {
        this.a = new LinkedTreeMap(false);
    }
    
    public Set entrySet() {
        return this.a.entrySet();
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof ph0 && ((ph0)o).a.equals(this.a));
    }
    
    @Override
    public int hashCode() {
        return this.a.hashCode();
    }
    
    public void n(final String s, final nh0 nh0) {
        final LinkedTreeMap a = this.a;
        nh0 a2 = nh0;
        if (nh0 == null) {
            a2 = oh0.a;
        }
        a.put(s, a2);
    }
}

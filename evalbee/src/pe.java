import com.google.common.base.a;

// 
// Decompiled by Procyon v0.6.0
// 

public final class pe
{
    public final long a;
    public final long b;
    public final long c;
    public final long d;
    public final long e;
    public final long f;
    
    public pe(final long a, final long b, final long c, final long d, final long e, final long f) {
        final boolean b2 = true;
        i71.d(a >= 0L);
        i71.d(b >= 0L);
        i71.d(c >= 0L);
        i71.d(d >= 0L);
        i71.d(e >= 0L);
        i71.d(f >= 0L && b2);
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
    }
    
    public long a() {
        return this.f;
    }
    
    public long b() {
        return this.a;
    }
    
    public long c() {
        return this.d;
    }
    
    public long d() {
        return this.c;
    }
    
    public long e() {
        return this.b;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof pe;
        boolean b3;
        final boolean b2 = b3 = false;
        if (b) {
            final pe pe = (pe)o;
            b3 = b2;
            if (this.a == pe.a) {
                b3 = b2;
                if (this.b == pe.b) {
                    b3 = b2;
                    if (this.c == pe.c) {
                        b3 = b2;
                        if (this.d == pe.d) {
                            b3 = b2;
                            if (this.e == pe.e) {
                                b3 = b2;
                                if (this.f == pe.f) {
                                    b3 = true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return b3;
    }
    
    public long f() {
        return this.e;
    }
    
    @Override
    public int hashCode() {
        return b11.b(this.a, this.b, this.c, this.d, this.e, this.f);
    }
    
    @Override
    public String toString() {
        return com.google.common.base.a.c(this).c("hitCount", this.a).c("missCount", this.b).c("loadSuccessCount", this.c).c("loadExceptionCount", this.d).c("totalLoadTime", this.e).c("evictionCount", this.f).toString();
    }
}

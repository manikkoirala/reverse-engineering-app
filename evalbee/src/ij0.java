import java.io.OutputStream;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ij0 extends OutputStream
{
    public long a;
    
    public ij0() {
        this.a = 0L;
    }
    
    public long a() {
        return this.a;
    }
    
    @Override
    public void write(final int n) {
        ++this.a;
    }
    
    @Override
    public void write(final byte[] array) {
        this.a += array.length;
    }
    
    @Override
    public void write(final byte[] array, int n, final int n2) {
        if (n >= 0 && n <= array.length && n2 >= 0) {
            n += n2;
            if (n <= array.length && n >= 0) {
                this.a += n2;
                return;
            }
        }
        throw new IndexOutOfBoundsException();
    }
}

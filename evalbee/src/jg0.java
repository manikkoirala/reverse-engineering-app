import java.util.Map;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Set;
import androidx.room.RoomDatabase;

// 
// Decompiled by Procyon v0.6.0
// 

public final class jg0
{
    public final RoomDatabase a;
    public final Set b;
    
    public jg0(final RoomDatabase a) {
        fg0.e((Object)a, "database");
        this.a = a;
        final Set<Object> setFromMap = Collections.newSetFromMap(new IdentityHashMap<Object, Boolean>());
        fg0.d((Object)setFromMap, "newSetFromMap(IdentityHashMap())");
        this.b = setFromMap;
    }
}

import android.content.SharedPreferences$Editor;
import android.content.SharedPreferences;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class w91
{
    public static SharedPreferences a(Context context) {
        final Context applicationContext = context.getApplicationContext();
        if (applicationContext != null) {
            context = applicationContext;
        }
        return context.getSharedPreferences("com.google.firebase.messaging", 0);
    }
    
    public static boolean b(final Context context) {
        return a(context).getBoolean("proxy_notification_initialized", false);
    }
    
    public static void c(final Context context, final boolean b) {
        final SharedPreferences$Editor edit = a(context).edit();
        edit.putBoolean("proxy_notification_initialized", b);
        edit.apply();
    }
}

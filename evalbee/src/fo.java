import java.io.IOException;
import android.util.Log;
import android.content.Context;
import java.nio.channels.FileLock;
import java.nio.channels.FileChannel;

// 
// Decompiled by Procyon v0.6.0
// 

public class fo
{
    public final FileChannel a;
    public final FileLock b;
    
    public fo(final FileChannel a, final FileLock b) {
        this.a = a;
        this.b = b;
    }
    
    public static fo a(final Context p0, final String p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: astore_2       
        //     4: aload_2        
        //     5: aload_0        
        //     6: invokevirtual   android/content/Context.getFilesDir:()Ljava/io/File;
        //     9: aload_1        
        //    10: invokespecial   java/io/File.<init>:(Ljava/io/File;Ljava/lang/String;)V
        //    13: new             Ljava/io/RandomAccessFile;
        //    16: astore_0       
        //    17: aload_0        
        //    18: aload_2        
        //    19: ldc             "rw"
        //    21: invokespecial   java/io/RandomAccessFile.<init>:(Ljava/io/File;Ljava/lang/String;)V
        //    24: aload_0        
        //    25: invokevirtual   java/io/RandomAccessFile.getChannel:()Ljava/nio/channels/FileChannel;
        //    28: astore_2       
        //    29: aload_2        
        //    30: invokevirtual   java/nio/channels/FileChannel.lock:()Ljava/nio/channels/FileLock;
        //    33: astore_1       
        //    34: new             Lfo;
        //    37: dup            
        //    38: aload_2        
        //    39: aload_1        
        //    40: invokespecial   fo.<init>:(Ljava/nio/channels/FileChannel;Ljava/nio/channels/FileLock;)V
        //    43: astore_0       
        //    44: aload_0        
        //    45: areturn        
        //    46: astore_0       
        //    47: goto            85
        //    50: astore_0       
        //    51: goto            85
        //    54: astore_0       
        //    55: goto            85
        //    58: astore_0       
        //    59: goto            67
        //    62: astore_0       
        //    63: goto            67
        //    66: astore_0       
        //    67: aconst_null    
        //    68: astore_1       
        //    69: goto            85
        //    72: astore_0       
        //    73: goto            81
        //    76: astore_0       
        //    77: goto            81
        //    80: astore_0       
        //    81: aconst_null    
        //    82: astore_2       
        //    83: aconst_null    
        //    84: astore_1       
        //    85: ldc             "CrossProcessLock"
        //    87: ldc             "encountered error while creating and acquiring the lock, ignoring"
        //    89: aload_0        
        //    90: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //    93: pop            
        //    94: aload_1        
        //    95: ifnull          102
        //    98: aload_1        
        //    99: invokevirtual   java/nio/channels/FileLock.release:()V
        //   102: aload_2        
        //   103: ifnull          110
        //   106: aload_2        
        //   107: invokevirtual   java/nio/channels/spi/AbstractInterruptibleChannel.close:()V
        //   110: aconst_null    
        //   111: areturn        
        //   112: astore_0       
        //   113: goto            102
        //   116: astore_0       
        //   117: goto            110
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                            
        //  -----  -----  -----  -----  ------------------------------------------------
        //  0      29     80     81     Ljava/io/IOException;
        //  0      29     76     80     Ljava/lang/Error;
        //  0      29     72     76     Ljava/nio/channels/OverlappingFileLockException;
        //  29     34     66     67     Ljava/io/IOException;
        //  29     34     62     66     Ljava/lang/Error;
        //  29     34     58     62     Ljava/nio/channels/OverlappingFileLockException;
        //  34     44     54     58     Ljava/io/IOException;
        //  34     44     50     54     Ljava/lang/Error;
        //  34     44     46     50     Ljava/nio/channels/OverlappingFileLockException;
        //  98     102    112    116    Ljava/io/IOException;
        //  106    110    116    120    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0110:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void b() {
        try {
            this.b.release();
            this.a.close();
        }
        catch (final IOException ex) {
            Log.e("CrossProcessLock", "encountered error while releasing, ignoring", (Throwable)ex);
        }
    }
}

import android.graphics.drawable.VectorDrawable;
import android.graphics.Bitmap$Config;
import android.graphics.Bitmap;
import android.graphics.Shader;
import android.graphics.Paint$Style;
import android.graphics.Path$FillType;
import android.graphics.PathMeasure;
import android.graphics.Paint;
import java.util.ArrayList;
import android.graphics.Paint$Join;
import android.graphics.Paint$Cap;
import android.graphics.Path;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Region;
import android.graphics.Canvas;
import org.xmlpull.v1.XmlPullParserException;
import java.util.ArrayDeque;
import android.util.AttributeSet;
import org.xmlpull.v1.XmlPullParser;
import android.graphics.drawable.Drawable;
import android.content.res.Resources$Theme;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable$ConstantState;
import android.graphics.ColorFilter;
import android.graphics.PorterDuffColorFilter;
import android.graphics.PorterDuff$Mode;

// 
// Decompiled by Procyon v0.6.0
// 

public class e32 extends d32
{
    public static final PorterDuff$Mode k;
    public h b;
    public PorterDuffColorFilter c;
    public ColorFilter d;
    public boolean e;
    public boolean f;
    public Drawable$ConstantState g;
    public final float[] h;
    public final Matrix i;
    public final Rect j;
    
    static {
        k = PorterDuff$Mode.SRC_IN;
    }
    
    public e32() {
        this.f = true;
        this.h = new float[9];
        this.i = new Matrix();
        this.j = new Rect();
        this.b = new h();
    }
    
    public e32(final h b) {
        this.f = true;
        this.h = new float[9];
        this.i = new Matrix();
        this.j = new Rect();
        this.b = b;
        this.c = this.i(this.c, b.c, b.d);
    }
    
    public static int a(final int n, final float n2) {
        return (n & 0xFFFFFF) | (int)(Color.alpha(n) * n2) << 24;
    }
    
    public static e32 b(final Resources resources, final int n, final Resources$Theme resources$Theme) {
        final e32 e32 = new e32();
        e32.a = le1.e(resources, n, resources$Theme);
        e32.g = new i(e32.a.getConstantState());
        return e32;
    }
    
    public static PorterDuff$Mode f(final int n, final PorterDuff$Mode porterDuff$Mode) {
        if (n == 3) {
            return PorterDuff$Mode.SRC_OVER;
        }
        if (n == 5) {
            return PorterDuff$Mode.SRC_IN;
        }
        if (n == 9) {
            return PorterDuff$Mode.SRC_ATOP;
        }
        switch (n) {
            default: {
                return porterDuff$Mode;
            }
            case 16: {
                return PorterDuff$Mode.ADD;
            }
            case 15: {
                return PorterDuff$Mode.SCREEN;
            }
            case 14: {
                return PorterDuff$Mode.MULTIPLY;
            }
        }
    }
    
    public Object c(final String s) {
        return this.b.b.p.get(s);
    }
    
    public boolean canApplyTheme() {
        final Drawable a = super.a;
        if (a != null) {
            wu.b(a);
        }
        return false;
    }
    
    public final void d(final Resources resources, final XmlPullParser xmlPullParser, final AttributeSet set, final Resources$Theme resources$Theme) {
        final h b = this.b;
        final g b2 = b.b;
        final ArrayDeque arrayDeque = new ArrayDeque();
        arrayDeque.push(b2.h);
        int n = xmlPullParser.getEventType();
        final int depth = xmlPullParser.getDepth();
        int n2 = 1;
        while (n != 1 && (xmlPullParser.getDepth() >= depth + 1 || n != 3)) {
            int n3 = 0;
            Label_0415: {
                if (n == 2) {
                    final String name = xmlPullParser.getName();
                    final d d = arrayDeque.peek();
                    if ("path".equals(name)) {
                        final c e = new c();
                        e.g(resources, set, resources$Theme, xmlPullParser);
                        d.b.add(e);
                        if (((f)e).getPathName() != null) {
                            b2.p.put(((f)e).getPathName(), e);
                        }
                        b.a |= ((f)e).d;
                        n3 = 0;
                    }
                    else {
                        int n4;
                        int n5;
                        if ("clip-path".equals(name)) {
                            final b e2 = new b();
                            e2.e(resources, set, resources$Theme, xmlPullParser);
                            d.b.add(e2);
                            if (((f)e2).getPathName() != null) {
                                b2.p.put(((f)e2).getPathName(), e2);
                            }
                            n4 = b.a;
                            n5 = ((f)e2).d;
                        }
                        else {
                            n3 = n2;
                            if (!"group".equals(name)) {
                                break Label_0415;
                            }
                            final d d2 = new d();
                            d2.c(resources, set, resources$Theme, xmlPullParser);
                            d.b.add(d2);
                            arrayDeque.push(d2);
                            if (d2.getGroupName() != null) {
                                b2.p.put(d2.getGroupName(), d2);
                            }
                            n4 = b.a;
                            n5 = d2.k;
                        }
                        b.a = (n5 | n4);
                        n3 = n2;
                    }
                }
                else {
                    n3 = n2;
                    if (n == 3) {
                        n3 = n2;
                        if ("group".equals(xmlPullParser.getName())) {
                            arrayDeque.pop();
                            n3 = n2;
                        }
                    }
                }
            }
            n = xmlPullParser.next();
            n2 = n3;
        }
        if (n2 == 0) {
            return;
        }
        throw new XmlPullParserException("no path defined");
    }
    
    public void draw(final Canvas canvas) {
        final Drawable a = super.a;
        if (a != null) {
            a.draw(canvas);
            return;
        }
        this.copyBounds(this.j);
        if (this.j.width() > 0) {
            if (this.j.height() > 0) {
                Object o;
                if ((o = this.d) == null) {
                    o = this.c;
                }
                canvas.getMatrix(this.i);
                this.i.getValues(this.h);
                float abs = Math.abs(this.h[0]);
                float abs2 = Math.abs(this.h[4]);
                final float abs3 = Math.abs(this.h[1]);
                final float abs4 = Math.abs(this.h[3]);
                if (abs3 != 0.0f || abs4 != 0.0f) {
                    abs = 1.0f;
                    abs2 = 1.0f;
                }
                final int b = (int)(this.j.width() * abs);
                final int b2 = (int)(this.j.height() * abs2);
                final int min = Math.min(2048, b);
                final int min2 = Math.min(2048, b2);
                if (min > 0) {
                    if (min2 > 0) {
                        final int save = canvas.save();
                        final Rect j = this.j;
                        canvas.translate((float)j.left, (float)j.top);
                        if (this.e()) {
                            canvas.translate((float)this.j.width(), 0.0f);
                            canvas.scale(-1.0f, 1.0f);
                        }
                        this.j.offsetTo(0, 0);
                        this.b.c(min, min2);
                        if (!this.f) {
                            this.b.j(min, min2);
                        }
                        else if (!this.b.b()) {
                            this.b.j(min, min2);
                            this.b.i();
                        }
                        this.b.d(canvas, (ColorFilter)o, this.j);
                        canvas.restoreToCount(save);
                    }
                }
            }
        }
    }
    
    public final boolean e() {
        if (this.isAutoMirrored()) {
            final int f = wu.f(this);
            final boolean b = true;
            if (f == 1) {
                return b;
            }
        }
        return false;
    }
    
    public void g(final boolean f) {
        this.f = f;
    }
    
    public int getAlpha() {
        final Drawable a = super.a;
        if (a != null) {
            return wu.d(a);
        }
        return this.b.b.getRootAlpha();
    }
    
    public int getChangingConfigurations() {
        final Drawable a = super.a;
        if (a != null) {
            return a.getChangingConfigurations();
        }
        return super.getChangingConfigurations() | this.b.getChangingConfigurations();
    }
    
    public ColorFilter getColorFilter() {
        final Drawable a = super.a;
        if (a != null) {
            return wu.e(a);
        }
        return this.d;
    }
    
    public Drawable$ConstantState getConstantState() {
        if (super.a != null) {
            return new i(super.a.getConstantState());
        }
        this.b.a = this.getChangingConfigurations();
        return this.b;
    }
    
    public int getIntrinsicHeight() {
        final Drawable a = super.a;
        if (a != null) {
            return a.getIntrinsicHeight();
        }
        return (int)this.b.b.j;
    }
    
    public int getIntrinsicWidth() {
        final Drawable a = super.a;
        if (a != null) {
            return a.getIntrinsicWidth();
        }
        return (int)this.b.b.i;
    }
    
    public int getOpacity() {
        final Drawable a = super.a;
        if (a != null) {
            return a.getOpacity();
        }
        return -3;
    }
    
    public final void h(final TypedArray typedArray, final XmlPullParser xmlPullParser, final Resources$Theme resources$Theme) {
        final h b = this.b;
        final g b2 = b.b;
        b.d = f(rz1.g(typedArray, xmlPullParser, "tintMode", 6, -1), PorterDuff$Mode.SRC_IN);
        final ColorStateList c = rz1.c(typedArray, xmlPullParser, resources$Theme, "tint", 1);
        if (c != null) {
            b.c = c;
        }
        b.e = rz1.a(typedArray, xmlPullParser, "autoMirrored", 5, b.e);
        b2.k = rz1.f(typedArray, xmlPullParser, "viewportWidth", 7, b2.k);
        final float f = rz1.f(typedArray, xmlPullParser, "viewportHeight", 8, b2.l);
        b2.l = f;
        if (b2.k <= 0.0f) {
            final StringBuilder sb = new StringBuilder();
            sb.append(typedArray.getPositionDescription());
            sb.append("<vector> tag requires viewportWidth > 0");
            throw new XmlPullParserException(sb.toString());
        }
        if (f <= 0.0f) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(typedArray.getPositionDescription());
            sb2.append("<vector> tag requires viewportHeight > 0");
            throw new XmlPullParserException(sb2.toString());
        }
        b2.i = typedArray.getDimension(3, b2.i);
        final float dimension = typedArray.getDimension(2, b2.j);
        b2.j = dimension;
        if (b2.i <= 0.0f) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(typedArray.getPositionDescription());
            sb3.append("<vector> tag requires width > 0");
            throw new XmlPullParserException(sb3.toString());
        }
        if (dimension > 0.0f) {
            b2.setAlpha(rz1.f(typedArray, xmlPullParser, "alpha", 4, b2.getAlpha()));
            final String string = typedArray.getString(0);
            if (string != null) {
                b2.n = string;
                b2.p.put(string, b2);
            }
            return;
        }
        final StringBuilder sb4 = new StringBuilder();
        sb4.append(typedArray.getPositionDescription());
        sb4.append("<vector> tag requires height > 0");
        throw new XmlPullParserException(sb4.toString());
    }
    
    public PorterDuffColorFilter i(final PorterDuffColorFilter porterDuffColorFilter, final ColorStateList list, final PorterDuff$Mode porterDuff$Mode) {
        if (list != null && porterDuff$Mode != null) {
            return new PorterDuffColorFilter(list.getColorForState(this.getState(), 0), porterDuff$Mode);
        }
        return null;
    }
    
    public void inflate(final Resources resources, final XmlPullParser xmlPullParser, final AttributeSet set) {
        final Drawable a = super.a;
        if (a != null) {
            a.inflate(resources, xmlPullParser, set);
            return;
        }
        this.inflate(resources, xmlPullParser, set, null);
    }
    
    public void inflate(final Resources resources, final XmlPullParser xmlPullParser, final AttributeSet set, final Resources$Theme resources$Theme) {
        final Drawable a = super.a;
        if (a != null) {
            wu.g(a, resources, xmlPullParser, set, resources$Theme);
            return;
        }
        final h b = this.b;
        b.b = new g();
        final TypedArray i = rz1.i(resources, resources$Theme, set, w4.a);
        this.h(i, xmlPullParser, resources$Theme);
        i.recycle();
        b.a = this.getChangingConfigurations();
        b.k = true;
        this.d(resources, xmlPullParser, set, resources$Theme);
        this.c = this.i(this.c, b.c, b.d);
    }
    
    public void invalidateSelf() {
        final Drawable a = super.a;
        if (a != null) {
            a.invalidateSelf();
            return;
        }
        super.invalidateSelf();
    }
    
    public boolean isAutoMirrored() {
        final Drawable a = super.a;
        if (a != null) {
            return wu.h(a);
        }
        return this.b.e;
    }
    
    public boolean isStateful() {
        final Drawable a = super.a;
        if (a != null) {
            return a.isStateful();
        }
        if (!super.isStateful()) {
            final h b = this.b;
            if (b != null) {
                if (b.g()) {
                    return true;
                }
                final ColorStateList c = this.b.c;
                if (c != null && c.isStateful()) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }
    
    public Drawable mutate() {
        final Drawable a = super.a;
        if (a != null) {
            a.mutate();
            return this;
        }
        if (!this.e && super.mutate() == this) {
            this.b = new h(this.b);
            this.e = true;
        }
        return this;
    }
    
    public void onBoundsChange(final Rect bounds) {
        final Drawable a = super.a;
        if (a != null) {
            a.setBounds(bounds);
        }
    }
    
    public boolean onStateChange(final int[] state) {
        final Drawable a = super.a;
        if (a != null) {
            return a.setState(state);
        }
        final h b = this.b;
        final ColorStateList c = b.c;
        final boolean b2 = true;
        boolean b3 = false;
        Label_0077: {
            if (c != null) {
                final PorterDuff$Mode d = b.d;
                if (d != null) {
                    this.c = this.i(this.c, c, d);
                    this.invalidateSelf();
                    b3 = true;
                    break Label_0077;
                }
            }
            b3 = false;
        }
        if (b.g() && b.h(state)) {
            this.invalidateSelf();
            b3 = b2;
        }
        return b3;
    }
    
    public void scheduleSelf(final Runnable runnable, final long n) {
        final Drawable a = super.a;
        if (a != null) {
            a.scheduleSelf(runnable, n);
            return;
        }
        super.scheduleSelf(runnable, n);
    }
    
    public void setAlpha(final int n) {
        final Drawable a = super.a;
        if (a != null) {
            a.setAlpha(n);
            return;
        }
        if (this.b.b.getRootAlpha() != n) {
            this.b.b.setRootAlpha(n);
            this.invalidateSelf();
        }
    }
    
    public void setAutoMirrored(final boolean e) {
        final Drawable a = super.a;
        if (a != null) {
            wu.j(a, e);
            return;
        }
        this.b.e = e;
    }
    
    public void setColorFilter(final ColorFilter colorFilter) {
        final Drawable a = super.a;
        if (a != null) {
            a.setColorFilter(colorFilter);
            return;
        }
        this.d = colorFilter;
        this.invalidateSelf();
    }
    
    public void setTint(final int n) {
        final Drawable a = super.a;
        if (a != null) {
            wu.n(a, n);
            return;
        }
        this.setTintList(ColorStateList.valueOf(n));
    }
    
    public void setTintList(final ColorStateList c) {
        final Drawable a = super.a;
        if (a != null) {
            wu.o(a, c);
            return;
        }
        final h b = this.b;
        if (b.c != c) {
            b.c = c;
            this.c = this.i(this.c, c, b.d);
            this.invalidateSelf();
        }
    }
    
    public void setTintMode(final PorterDuff$Mode d) {
        final Drawable a = super.a;
        if (a != null) {
            wu.p(a, d);
            return;
        }
        final h b = this.b;
        if (b.d != d) {
            b.d = d;
            this.c = this.i(this.c, b.c, d);
            this.invalidateSelf();
        }
    }
    
    public boolean setVisible(final boolean b, final boolean b2) {
        final Drawable a = super.a;
        if (a != null) {
            return a.setVisible(b, b2);
        }
        return super.setVisible(b, b2);
    }
    
    public void unscheduleSelf(final Runnable runnable) {
        final Drawable a = super.a;
        if (a != null) {
            a.unscheduleSelf(runnable);
            return;
        }
        super.unscheduleSelf(runnable);
    }
    
    public static class b extends f
    {
        public b() {
        }
        
        public b(final b b) {
            super((f)b);
        }
        
        @Override
        public boolean c() {
            return true;
        }
        
        public void e(final Resources resources, final AttributeSet set, final Resources$Theme resources$Theme, final XmlPullParser xmlPullParser) {
            if (!rz1.h(xmlPullParser, "pathData")) {
                return;
            }
            final TypedArray i = rz1.i(resources, resources$Theme, set, w4.d);
            this.f(i, xmlPullParser);
            i.recycle();
        }
        
        public final void f(final TypedArray typedArray, final XmlPullParser xmlPullParser) {
            final String string = typedArray.getString(0);
            if (string != null) {
                super.b = string;
            }
            final String string2 = typedArray.getString(1);
            if (string2 != null) {
                super.a = i31.d(string2);
            }
            super.c = rz1.g(typedArray, xmlPullParser, "fillType", 2, 0);
        }
    }
    
    public abstract static class f extends e
    {
        public i31.b[] a;
        public String b;
        public int c;
        public int d;
        
        public f() {
            super(null);
            this.a = null;
            this.c = 0;
        }
        
        public f(final f f) {
            super(null);
            this.a = null;
            this.c = 0;
            this.b = f.b;
            this.d = f.d;
            this.a = i31.f(f.a);
        }
        
        public boolean c() {
            return false;
        }
        
        public void d(final Path path) {
            path.reset();
            final i31.b[] a = this.a;
            if (a != null) {
                i31.b.d(a, path);
            }
        }
        
        public i31.b[] getPathData() {
            return this.a;
        }
        
        public String getPathName() {
            return this.b;
        }
        
        public void setPathData(final i31.b[] array) {
            if (!i31.b(this.a, array)) {
                this.a = i31.f(array);
            }
            else {
                i31.j(this.a, array);
            }
        }
    }
    
    public abstract static class e
    {
        public boolean a() {
            return false;
        }
        
        public boolean b(final int[] array) {
            return false;
        }
    }
    
    public static class c extends f
    {
        public int[] e;
        public wi f;
        public float g;
        public wi h;
        public float i;
        public float j;
        public float k;
        public float l;
        public float m;
        public Paint$Cap n;
        public Paint$Join o;
        public float p;
        
        public c() {
            this.g = 0.0f;
            this.i = 1.0f;
            this.j = 1.0f;
            this.k = 0.0f;
            this.l = 1.0f;
            this.m = 0.0f;
            this.n = Paint$Cap.BUTT;
            this.o = Paint$Join.MITER;
            this.p = 4.0f;
        }
        
        public c(final c c) {
            super((f)c);
            this.g = 0.0f;
            this.i = 1.0f;
            this.j = 1.0f;
            this.k = 0.0f;
            this.l = 1.0f;
            this.m = 0.0f;
            this.n = Paint$Cap.BUTT;
            this.o = Paint$Join.MITER;
            this.p = 4.0f;
            this.e = c.e;
            this.f = c.f;
            this.g = c.g;
            this.i = c.i;
            this.h = c.h;
            super.c = c.c;
            this.j = c.j;
            this.k = c.k;
            this.l = c.l;
            this.m = c.m;
            this.n = c.n;
            this.o = c.o;
            this.p = c.p;
        }
        
        @Override
        public boolean a() {
            return this.h.i() || this.f.i();
        }
        
        @Override
        public boolean b(final int[] array) {
            return this.f.j(array) | this.h.j(array);
        }
        
        public final Paint$Cap e(final int n, final Paint$Cap paint$Cap) {
            if (n == 0) {
                return Paint$Cap.BUTT;
            }
            if (n == 1) {
                return Paint$Cap.ROUND;
            }
            if (n != 2) {
                return paint$Cap;
            }
            return Paint$Cap.SQUARE;
        }
        
        public final Paint$Join f(final int n, final Paint$Join paint$Join) {
            if (n == 0) {
                return Paint$Join.MITER;
            }
            if (n == 1) {
                return Paint$Join.ROUND;
            }
            if (n != 2) {
                return paint$Join;
            }
            return Paint$Join.BEVEL;
        }
        
        public void g(final Resources resources, final AttributeSet set, final Resources$Theme resources$Theme, final XmlPullParser xmlPullParser) {
            final TypedArray i = rz1.i(resources, resources$Theme, set, w4.c);
            this.h(i, xmlPullParser, resources$Theme);
            i.recycle();
        }
        
        public float getFillAlpha() {
            return this.j;
        }
        
        public int getFillColor() {
            return this.h.e();
        }
        
        public float getStrokeAlpha() {
            return this.i;
        }
        
        public int getStrokeColor() {
            return this.f.e();
        }
        
        public float getStrokeWidth() {
            return this.g;
        }
        
        public float getTrimPathEnd() {
            return this.l;
        }
        
        public float getTrimPathOffset() {
            return this.m;
        }
        
        public float getTrimPathStart() {
            return this.k;
        }
        
        public final void h(final TypedArray typedArray, final XmlPullParser xmlPullParser, final Resources$Theme resources$Theme) {
            this.e = null;
            if (!rz1.h(xmlPullParser, "pathData")) {
                return;
            }
            final String string = typedArray.getString(0);
            if (string != null) {
                super.b = string;
            }
            final String string2 = typedArray.getString(2);
            if (string2 != null) {
                super.a = i31.d(string2);
            }
            this.h = rz1.e(typedArray, xmlPullParser, resources$Theme, "fillColor", 1, 0);
            this.j = rz1.f(typedArray, xmlPullParser, "fillAlpha", 12, this.j);
            this.n = this.e(rz1.g(typedArray, xmlPullParser, "strokeLineCap", 8, -1), this.n);
            this.o = this.f(rz1.g(typedArray, xmlPullParser, "strokeLineJoin", 9, -1), this.o);
            this.p = rz1.f(typedArray, xmlPullParser, "strokeMiterLimit", 10, this.p);
            this.f = rz1.e(typedArray, xmlPullParser, resources$Theme, "strokeColor", 3, 0);
            this.i = rz1.f(typedArray, xmlPullParser, "strokeAlpha", 11, this.i);
            this.g = rz1.f(typedArray, xmlPullParser, "strokeWidth", 4, this.g);
            this.l = rz1.f(typedArray, xmlPullParser, "trimPathEnd", 6, this.l);
            this.m = rz1.f(typedArray, xmlPullParser, "trimPathOffset", 7, this.m);
            this.k = rz1.f(typedArray, xmlPullParser, "trimPathStart", 5, this.k);
            super.c = rz1.g(typedArray, xmlPullParser, "fillType", 13, super.c);
        }
        
        public void setFillAlpha(final float j) {
            this.j = j;
        }
        
        public void setFillColor(final int n) {
            this.h.k(n);
        }
        
        public void setStrokeAlpha(final float i) {
            this.i = i;
        }
        
        public void setStrokeColor(final int n) {
            this.f.k(n);
        }
        
        public void setStrokeWidth(final float g) {
            this.g = g;
        }
        
        public void setTrimPathEnd(final float l) {
            this.l = l;
        }
        
        public void setTrimPathOffset(final float m) {
            this.m = m;
        }
        
        public void setTrimPathStart(final float k) {
            this.k = k;
        }
    }
    
    public static class d extends e
    {
        public final Matrix a;
        public final ArrayList b;
        public float c;
        public float d;
        public float e;
        public float f;
        public float g;
        public float h;
        public float i;
        public final Matrix j;
        public int k;
        public int[] l;
        public String m;
        
        public d() {
            super(null);
            this.a = new Matrix();
            this.b = new ArrayList();
            this.c = 0.0f;
            this.d = 0.0f;
            this.e = 0.0f;
            this.f = 1.0f;
            this.g = 1.0f;
            this.h = 0.0f;
            this.i = 0.0f;
            this.j = new Matrix();
            this.m = null;
        }
        
        public d(d d, final r8 r8) {
            super(null);
            this.a = new Matrix();
            this.b = new ArrayList();
            this.c = 0.0f;
            this.d = 0.0f;
            this.e = 0.0f;
            this.f = 1.0f;
            this.g = 1.0f;
            this.h = 0.0f;
            this.i = 0.0f;
            final Matrix j = new Matrix();
            this.j = j;
            this.m = null;
            this.c = d.c;
            this.d = d.d;
            this.e = d.e;
            this.f = d.f;
            this.g = d.g;
            this.h = d.h;
            this.i = d.i;
            this.l = d.l;
            final String m = d.m;
            this.m = m;
            this.k = d.k;
            if (m != null) {
                r8.put(m, this);
            }
            j.set(d.j);
            final ArrayList b = d.b;
            for (int i = 0; i < b.size(); ++i) {
                final Object value = b.get(i);
                if (value instanceof d) {
                    d = (d)value;
                    this.b.add(new d(d, r8));
                }
                else {
                    f e;
                    if (value instanceof c) {
                        e = new c((c)value);
                    }
                    else {
                        if (!(value instanceof b)) {
                            throw new IllegalStateException("Unknown object in the tree!");
                        }
                        e = new b((b)value);
                    }
                    this.b.add(e);
                    final String b2 = e.b;
                    if (b2 != null) {
                        r8.put(b2, e);
                    }
                }
            }
        }
        
        @Override
        public boolean a() {
            for (int i = 0; i < this.b.size(); ++i) {
                if (((e)this.b.get(i)).a()) {
                    return true;
                }
            }
            return false;
        }
        
        @Override
        public boolean b(final int[] array) {
            int i = 0;
            boolean b = false;
            while (i < this.b.size()) {
                b |= ((e)this.b.get(i)).b(array);
                ++i;
            }
            return b;
        }
        
        public void c(final Resources resources, final AttributeSet set, final Resources$Theme resources$Theme, final XmlPullParser xmlPullParser) {
            final TypedArray i = rz1.i(resources, resources$Theme, set, w4.b);
            this.e(i, xmlPullParser);
            i.recycle();
        }
        
        public final void d() {
            this.j.reset();
            this.j.postTranslate(-this.d, -this.e);
            this.j.postScale(this.f, this.g);
            this.j.postRotate(this.c, 0.0f, 0.0f);
            this.j.postTranslate(this.h + this.d, this.i + this.e);
        }
        
        public final void e(final TypedArray typedArray, final XmlPullParser xmlPullParser) {
            this.l = null;
            this.c = rz1.f(typedArray, xmlPullParser, "rotation", 5, this.c);
            this.d = typedArray.getFloat(1, this.d);
            this.e = typedArray.getFloat(2, this.e);
            this.f = rz1.f(typedArray, xmlPullParser, "scaleX", 3, this.f);
            this.g = rz1.f(typedArray, xmlPullParser, "scaleY", 4, this.g);
            this.h = rz1.f(typedArray, xmlPullParser, "translateX", 6, this.h);
            this.i = rz1.f(typedArray, xmlPullParser, "translateY", 7, this.i);
            final String string = typedArray.getString(0);
            if (string != null) {
                this.m = string;
            }
            this.d();
        }
        
        public String getGroupName() {
            return this.m;
        }
        
        public Matrix getLocalMatrix() {
            return this.j;
        }
        
        public float getPivotX() {
            return this.d;
        }
        
        public float getPivotY() {
            return this.e;
        }
        
        public float getRotation() {
            return this.c;
        }
        
        public float getScaleX() {
            return this.f;
        }
        
        public float getScaleY() {
            return this.g;
        }
        
        public float getTranslateX() {
            return this.h;
        }
        
        public float getTranslateY() {
            return this.i;
        }
        
        public void setPivotX(final float d) {
            if (d != this.d) {
                this.d = d;
                this.d();
            }
        }
        
        public void setPivotY(final float e) {
            if (e != this.e) {
                this.e = e;
                this.d();
            }
        }
        
        public void setRotation(final float c) {
            if (c != this.c) {
                this.c = c;
                this.d();
            }
        }
        
        public void setScaleX(final float f) {
            if (f != this.f) {
                this.f = f;
                this.d();
            }
        }
        
        public void setScaleY(final float g) {
            if (g != this.g) {
                this.g = g;
                this.d();
            }
        }
        
        public void setTranslateX(final float h) {
            if (h != this.h) {
                this.h = h;
                this.d();
            }
        }
        
        public void setTranslateY(final float i) {
            if (i != this.i) {
                this.i = i;
                this.d();
            }
        }
    }
    
    public static class g
    {
        public static final Matrix q;
        public final Path a;
        public final Path b;
        public final Matrix c;
        public Paint d;
        public Paint e;
        public PathMeasure f;
        public int g;
        public final d h;
        public float i;
        public float j;
        public float k;
        public float l;
        public int m;
        public String n;
        public Boolean o;
        public final r8 p;
        
        static {
            q = new Matrix();
        }
        
        public g() {
            this.c = new Matrix();
            this.i = 0.0f;
            this.j = 0.0f;
            this.k = 0.0f;
            this.l = 0.0f;
            this.m = 255;
            this.n = null;
            this.o = null;
            this.p = new r8();
            this.h = new d();
            this.a = new Path();
            this.b = new Path();
        }
        
        public g(final g g) {
            this.c = new Matrix();
            this.i = 0.0f;
            this.j = 0.0f;
            this.k = 0.0f;
            this.l = 0.0f;
            this.m = 255;
            this.n = null;
            this.o = null;
            final r8 p = new r8();
            this.p = p;
            this.h = new d(g.h, p);
            this.a = new Path(g.a);
            this.b = new Path(g.b);
            this.i = g.i;
            this.j = g.j;
            this.k = g.k;
            this.l = g.l;
            this.g = g.g;
            this.m = g.m;
            this.n = g.n;
            final String n = g.n;
            if (n != null) {
                p.put(n, this);
            }
            this.o = g.o;
        }
        
        public static float a(final float n, final float n2, final float n3, final float n4) {
            return n * n4 - n2 * n3;
        }
        
        public void b(final Canvas canvas, final int n, final int n2, final ColorFilter colorFilter) {
            this.c(this.h, e32.g.q, canvas, n, n2, colorFilter);
        }
        
        public final void c(final d d, final Matrix matrix, final Canvas canvas, final int n, final int n2, final ColorFilter colorFilter) {
            d.a.set(matrix);
            d.a.preConcat(d.j);
            canvas.save();
            for (int i = 0; i < d.b.size(); ++i) {
                final e e = d.b.get(i);
                if (e instanceof d) {
                    this.c((d)e, d.a, canvas, n, n2, colorFilter);
                }
                else if (e instanceof f) {
                    this.d(d, (f)e, canvas, n, n2, colorFilter);
                }
            }
            canvas.restore();
        }
        
        public final void d(final d d, final f f, final Canvas canvas, final int n, final int n2, final ColorFilter colorFilter) {
            final float a = n / this.k;
            final float b = n2 / this.l;
            final float min = Math.min(a, b);
            final Matrix a2 = d.a;
            this.c.set(a2);
            this.c.postScale(a, b);
            final float e = this.e(a2);
            if (e == 0.0f) {
                return;
            }
            f.d(this.a);
            final Path a3 = this.a;
            this.b.reset();
            if (f.c()) {
                final Path b2 = this.b;
                Path$FillType fillType;
                if (f.c == 0) {
                    fillType = Path$FillType.WINDING;
                }
                else {
                    fillType = Path$FillType.EVEN_ODD;
                }
                b2.setFillType(fillType);
                this.b.addPath(a3, this.c);
                canvas.clipPath(this.b);
            }
            else {
                final c c = (c)f;
                final float k = c.k;
                if (k != 0.0f || c.l != 1.0f) {
                    final float m = c.m;
                    final float l = c.l;
                    if (this.f == null) {
                        this.f = new PathMeasure();
                    }
                    this.f.setPath(this.a, false);
                    final float length = this.f.getLength();
                    final float n3 = (k + m) % 1.0f * length;
                    final float n4 = (l + m) % 1.0f * length;
                    a3.reset();
                    if (n3 > n4) {
                        this.f.getSegment(n3, length, a3, true);
                        this.f.getSegment(0.0f, n4, a3, true);
                    }
                    else {
                        this.f.getSegment(n3, n4, a3, true);
                    }
                    a3.rLineTo(0.0f, 0.0f);
                }
                this.b.addPath(a3, this.c);
                if (c.h.l()) {
                    final wi h = c.h;
                    if (this.e == null) {
                        (this.e = new Paint(1)).setStyle(Paint$Style.FILL);
                    }
                    final Paint e2 = this.e;
                    if (h.h()) {
                        final Shader f2 = h.f();
                        f2.setLocalMatrix(this.c);
                        e2.setShader(f2);
                        e2.setAlpha(Math.round(c.j * 255.0f));
                    }
                    else {
                        e2.setShader((Shader)null);
                        e2.setAlpha(255);
                        e2.setColor(e32.a(h.e(), c.j));
                    }
                    e2.setColorFilter(colorFilter);
                    final Path b3 = this.b;
                    Path$FillType fillType2;
                    if (c.c == 0) {
                        fillType2 = Path$FillType.WINDING;
                    }
                    else {
                        fillType2 = Path$FillType.EVEN_ODD;
                    }
                    b3.setFillType(fillType2);
                    canvas.drawPath(this.b, e2);
                }
                if (c.f.l()) {
                    final wi f3 = c.f;
                    if (this.d == null) {
                        (this.d = new Paint(1)).setStyle(Paint$Style.STROKE);
                    }
                    final Paint d2 = this.d;
                    final Paint$Join o = c.o;
                    if (o != null) {
                        d2.setStrokeJoin(o);
                    }
                    final Paint$Cap n5 = c.n;
                    if (n5 != null) {
                        d2.setStrokeCap(n5);
                    }
                    d2.setStrokeMiter(c.p);
                    if (f3.h()) {
                        final Shader f4 = f3.f();
                        f4.setLocalMatrix(this.c);
                        d2.setShader(f4);
                        d2.setAlpha(Math.round(c.i * 255.0f));
                    }
                    else {
                        d2.setShader((Shader)null);
                        d2.setAlpha(255);
                        d2.setColor(e32.a(f3.e(), c.i));
                    }
                    d2.setColorFilter(colorFilter);
                    d2.setStrokeWidth(c.g * (min * e));
                    canvas.drawPath(this.b, d2);
                }
            }
        }
        
        public final float e(final Matrix matrix) {
            final float[] array2;
            final float[] array = array2 = new float[4];
            array2[0] = 0.0f;
            array2[2] = (array2[1] = 1.0f);
            array2[3] = 0.0f;
            matrix.mapVectors(array);
            final float a = (float)Math.hypot(array[0], array[1]);
            final float b = (float)Math.hypot(array[2], array[3]);
            final float a2 = a(array[0], array[1], array[2], array[3]);
            final float max = Math.max(a, b);
            float n = 0.0f;
            if (max > 0.0f) {
                n = Math.abs(a2) / max;
            }
            return n;
        }
        
        public boolean f() {
            if (this.o == null) {
                this.o = this.h.a();
            }
            return this.o;
        }
        
        public boolean g(final int[] array) {
            return this.h.b(array);
        }
        
        public float getAlpha() {
            return this.getRootAlpha() / 255.0f;
        }
        
        public int getRootAlpha() {
            return this.m;
        }
        
        public void setAlpha(final float n) {
            this.setRootAlpha((int)(n * 255.0f));
        }
        
        public void setRootAlpha(final int m) {
            this.m = m;
        }
    }
    
    public static class h extends Drawable$ConstantState
    {
        public int a;
        public g b;
        public ColorStateList c;
        public PorterDuff$Mode d;
        public boolean e;
        public Bitmap f;
        public ColorStateList g;
        public PorterDuff$Mode h;
        public int i;
        public boolean j;
        public boolean k;
        public Paint l;
        
        public h() {
            this.c = null;
            this.d = e32.k;
            this.b = new g();
        }
        
        public h(final h h) {
            this.c = null;
            this.d = e32.k;
            if (h != null) {
                this.a = h.a;
                final g b = new g(h.b);
                this.b = b;
                if (h.b.e != null) {
                    b.e = new Paint(h.b.e);
                }
                if (h.b.d != null) {
                    this.b.d = new Paint(h.b.d);
                }
                this.c = h.c;
                this.d = h.d;
                this.e = h.e;
            }
        }
        
        public boolean a(final int n, final int n2) {
            return n == this.f.getWidth() && n2 == this.f.getHeight();
        }
        
        public boolean b() {
            return !this.k && this.g == this.c && this.h == this.d && this.j == this.e && this.i == this.b.getRootAlpha();
        }
        
        public void c(final int n, final int n2) {
            if (this.f == null || !this.a(n, n2)) {
                this.f = Bitmap.createBitmap(n, n2, Bitmap$Config.ARGB_8888);
                this.k = true;
            }
        }
        
        public void d(final Canvas canvas, final ColorFilter colorFilter, final Rect rect) {
            canvas.drawBitmap(this.f, (Rect)null, rect, this.e(colorFilter));
        }
        
        public Paint e(final ColorFilter colorFilter) {
            if (!this.f() && colorFilter == null) {
                return null;
            }
            if (this.l == null) {
                (this.l = new Paint()).setFilterBitmap(true);
            }
            this.l.setAlpha(this.b.getRootAlpha());
            this.l.setColorFilter(colorFilter);
            return this.l;
        }
        
        public boolean f() {
            return this.b.getRootAlpha() < 255;
        }
        
        public boolean g() {
            return this.b.f();
        }
        
        public int getChangingConfigurations() {
            return this.a;
        }
        
        public boolean h(final int[] array) {
            final boolean g = this.b.g(array);
            this.k |= g;
            return g;
        }
        
        public void i() {
            this.g = this.c;
            this.h = this.d;
            this.i = this.b.getRootAlpha();
            this.j = this.e;
            this.k = false;
        }
        
        public void j(final int n, final int n2) {
            this.f.eraseColor(0);
            this.b.b(new Canvas(this.f), n, n2, null);
        }
        
        public Drawable newDrawable() {
            return new e32(this);
        }
        
        public Drawable newDrawable(final Resources resources) {
            return new e32(this);
        }
    }
    
    public static class i extends Drawable$ConstantState
    {
        public final Drawable$ConstantState a;
        
        public i(final Drawable$ConstantState a) {
            this.a = a;
        }
        
        public boolean canApplyTheme() {
            return this.a.canApplyTheme();
        }
        
        public int getChangingConfigurations() {
            return this.a.getChangingConfigurations();
        }
        
        public Drawable newDrawable() {
            final e32 e32 = new e32();
            e32.a = this.a.newDrawable();
            return e32;
        }
        
        public Drawable newDrawable(final Resources resources) {
            final e32 e32 = new e32();
            e32.a = this.a.newDrawable(resources);
            return e32;
        }
        
        public Drawable newDrawable(final Resources resources, final Resources$Theme resources$Theme) {
            final e32 e32 = new e32();
            e32.a = this.a.newDrawable(resources, resources$Theme);
            return e32;
        }
    }
}

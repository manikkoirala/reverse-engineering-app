import android.util.Log;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.DefaultClock;
import android.content.Context;
import com.google.android.gms.common.util.Clock;
import java.util.Random;

// 
// Decompiled by Procyon v0.6.0
// 

public class mz
{
    public static final Random f;
    public static jo1 g;
    public static Clock h;
    public final Context a;
    public final zf0 b;
    public final dg0 c;
    public long d;
    public volatile boolean e;
    
    static {
        f = new Random();
        mz.g = new ko1();
        mz.h = DefaultClock.getInstance();
    }
    
    public mz(final Context a, final zf0 b, final dg0 c, final long d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
    
    public void a() {
        this.e = true;
    }
    
    public boolean b(final int n) {
        return (n >= 500 && n < 600) || n == -2 || n == 429 || n == 408;
    }
    
    public void c() {
        this.e = false;
    }
    
    public void d(final xy0 xy0) {
        this.e(xy0, true);
    }
    
    public void e(final xy0 xy0, final boolean b) {
        Preconditions.checkNotNull(xy0);
        final long elapsedRealtime = mz.h.elapsedRealtime();
        final long d = this.d;
        final String c = m22.c(this.b);
        final String b2 = m22.b(this.c);
        if (b) {
            xy0.B(c, b2, this.a);
        }
        else {
            xy0.D(c, b2);
        }
        int n = 1000;
        while (mz.h.elapsedRealtime() + n <= elapsedRealtime + d && !xy0.v() && this.b(xy0.o())) {
            try {
                mz.g.a(mz.f.nextInt(250) + n);
                int n2 = n;
                if (n < 30000) {
                    if (xy0.o() != -2) {
                        n2 = n * 2;
                        Log.w("ExponenentialBackoff", "network error occurred, backing off/sleeping.");
                    }
                    else {
                        Log.w("ExponenentialBackoff", "network unavailable, sleeping.");
                        n2 = 1000;
                    }
                }
                if (this.e) {
                    return;
                }
                xy0.F();
                final String c2 = m22.c(this.b);
                final String b3 = m22.b(this.c);
                if (b) {
                    xy0.B(c2, b3, this.a);
                    n = n2;
                    continue;
                }
                xy0.D(c2, b3);
                n = n2;
                continue;
            }
            catch (final InterruptedException ex) {
                Log.w("ExponenentialBackoff", "thread interrupted during exponential backoff.");
                Thread.currentThread().interrupt();
            }
            break;
        }
    }
}

import com.ekodroid.omrevaluator.exams.models.RollNoListDataModel;
import com.ekodroid.omrevaluator.database.ResultDataJsonModel;

// 
// Decompiled by Procyon v0.6.0
// 

public class d8
{
    public ResultDataJsonModel a;
    public ye1 b;
    
    public d8(final ResultDataJsonModel a, final ye1 b) {
        this.a = a;
        this.b = b;
    }
    
    public ResultDataJsonModel a() {
        return this.a;
    }
    
    public ye1 b() {
        return this.b;
    }
    
    public RollNoListDataModel c() {
        return new RollNoListDataModel(this.b.g(), this.b.f());
    }
}

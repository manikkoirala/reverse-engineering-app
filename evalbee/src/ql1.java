import com.google.protobuf.GeneratedMessageLite;
import com.google.firestore.v1.k;
import com.google.protobuf.j0;
import com.google.firestore.v1.Value;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class ql1
{
    public static j0 a(final Value value) {
        return value.s0().f0("__local_write_time__").v0();
    }
    
    public static Value b(Value value) {
        final Value value2 = value = value.s0().e0("__previous_value__", null);
        if (c(value2)) {
            value = b(value2);
        }
        return value;
    }
    
    public static boolean c(Value e0) {
        final Value value = null;
        if (e0 == null) {
            e0 = value;
        }
        else {
            e0 = e0.s0().e0("__type__", null);
        }
        return e0 != null && "server_timestamp".equals(e0.u0());
    }
    
    public static Value d(final pw1 pw1, final Value value) {
        final k.b c = k.j0().C("__type__", (Value)((GeneratedMessageLite.a)Value.x0().L("server_timestamp")).p()).C("__local_write_time__", (Value)((GeneratedMessageLite.a)Value.x0().M(j0.f0().B(pw1.e()).A(pw1.d()))).p());
        Value b = value;
        if (c(value)) {
            b = b(value);
        }
        if (b != null) {
            c.C("__previous_value__", b);
        }
        return (Value)((GeneratedMessageLite.a)Value.x0().H(c)).p();
    }
}

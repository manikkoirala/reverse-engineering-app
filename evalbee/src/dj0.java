import java.util.Iterator;
import java.util.Map;
import java.util.Collections;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Collection;
import java.util.Set;

// 
// Decompiled by Procyon v0.6.0
// 

public class dj0 implements r91
{
    public volatile Set a;
    public volatile Set b;
    
    public dj0(final Collection collection) {
        this.b = null;
        (this.a = Collections.newSetFromMap(new ConcurrentHashMap<Object, Boolean>())).addAll(collection);
    }
    
    public static dj0 b(final Collection collection) {
        return new dj0(collection);
    }
    
    public void a(final r91 r91) {
        synchronized (this) {
            r91 r92;
            Set set;
            if (this.b == null) {
                final Set a = this.a;
                r92 = r91;
                set = a;
            }
            else {
                final Set b = this.b;
                final Object value = r91.get();
                set = b;
                r92 = (r91)value;
            }
            set.add(r92);
        }
    }
    
    public Set c() {
        if (this.b == null) {
            synchronized (this) {
                if (this.b == null) {
                    this.b = Collections.newSetFromMap(new ConcurrentHashMap<Object, Boolean>());
                    this.d();
                }
            }
        }
        return Collections.unmodifiableSet((Set<?>)this.b);
    }
    
    public final void d() {
        synchronized (this) {
            final Iterator iterator = this.a.iterator();
            while (iterator.hasNext()) {
                this.b.add(((r91)iterator.next()).get());
            }
            this.a = null;
        }
    }
}

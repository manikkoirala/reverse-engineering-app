// 
// Decompiled by Procyon v0.6.0
// 

public abstract class hz0
{
    public static final ez0 a;
    public static final ez0 b;
    
    static {
        a = c();
        b = new gz0();
    }
    
    public static ez0 a() {
        return hz0.a;
    }
    
    public static ez0 b() {
        return hz0.b;
    }
    
    public static ez0 c() {
        try {
            return (ez0)Class.forName("com.google.protobuf.NewInstanceSchemaFull").getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
        }
        catch (final Exception ex) {
            return null;
        }
    }
}

import java.util.List;
import com.google.firebase.firestore.core.ViewSnapshot;

// 
// Decompiled by Procyon v0.6.0
// 

public class m32
{
    public final ViewSnapshot a;
    public final List b;
    
    public m32(final ViewSnapshot a, final List b) {
        this.a = a;
        this.b = b;
    }
    
    public List a() {
        return this.b;
    }
    
    public ViewSnapshot b() {
        return this.a;
    }
}

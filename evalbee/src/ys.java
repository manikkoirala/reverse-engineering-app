import java.util.ArrayList;
import androidx.constraintlayout.core.widgets.a;
import java.util.Iterator;
import androidx.constraintlayout.core.widgets.f;
import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.widgets.d;
import androidx.constraintlayout.core.widgets.ConstraintWidget;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class ys
{
    public static lb.a a;
    public static int b;
    public static int c;
    
    static {
        ys.a = new lb.a();
        ys.b = 0;
        ys.c = 0;
    }
    
    public static boolean a(int n, final ConstraintWidget constraintWidget) {
        final ConstraintWidget.DimensionBehaviour a = constraintWidget.A();
        final ConstraintWidget.DimensionBehaviour t = constraintWidget.T();
        d d;
        if (constraintWidget.K() != null) {
            d = (d)constraintWidget.K();
        }
        else {
            d = null;
        }
        if (d != null) {
            d.A();
            final ConstraintWidget.DimensionBehaviour fixed = ConstraintWidget.DimensionBehaviour.FIXED;
        }
        if (d != null) {
            d.T();
            final ConstraintWidget.DimensionBehaviour fixed2 = ConstraintWidget.DimensionBehaviour.FIXED;
        }
        final ConstraintWidget.DimensionBehaviour fixed3 = ConstraintWidget.DimensionBehaviour.FIXED;
        final boolean b = false;
        Label_0169: {
            if (a != fixed3 && !constraintWidget.n0() && a != ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                final ConstraintWidget.DimensionBehaviour match_CONSTRAINT = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                if (a != match_CONSTRAINT || constraintWidget.w != 0 || constraintWidget.d0 != 0.0f || !constraintWidget.a0(0)) {
                    if (a != match_CONSTRAINT || constraintWidget.w != 1 || !constraintWidget.d0(0, constraintWidget.W())) {
                        n = 0;
                        break Label_0169;
                    }
                }
            }
            n = 1;
        }
        boolean b2 = false;
        Label_0264: {
            if (t != fixed3 && !constraintWidget.o0() && t != ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                final ConstraintWidget.DimensionBehaviour match_CONSTRAINT2 = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                if (t != match_CONSTRAINT2 || constraintWidget.x != 0 || constraintWidget.d0 != 0.0f || !constraintWidget.a0(1)) {
                    if (t != match_CONSTRAINT2 || constraintWidget.x != 1 || !constraintWidget.d0(1, constraintWidget.x())) {
                        b2 = false;
                        break Label_0264;
                    }
                }
            }
            b2 = true;
        }
        if (constraintWidget.d0 > 0.0f && (n != 0 || b2)) {
            return true;
        }
        boolean b3 = b;
        if (n != 0) {
            b3 = b;
            if (b2) {
                b3 = true;
            }
        }
        return b3;
    }
    
    public static void b(final int n, final ConstraintWidget constraintWidget, final lb.b b, final boolean b2) {
        if (constraintWidget.g0()) {
            return;
        }
        ++ys.b;
        if (!(constraintWidget instanceof d) && constraintWidget.m0()) {
            final int n2 = n + 1;
            if (a(n2, constraintWidget)) {
                d.T1(n2, constraintWidget, b, new lb.a(), lb.a.k);
            }
        }
        final ConstraintAnchor o = constraintWidget.o(ConstraintAnchor.Type.LEFT);
        final ConstraintAnchor o2 = constraintWidget.o(ConstraintAnchor.Type.RIGHT);
        final int e = o.e();
        final int e2 = o2.e();
        if (o.d() != null && o.n()) {
            for (final ConstraintAnchor constraintAnchor : o.d()) {
                final ConstraintWidget d = constraintAnchor.d;
                final int n3 = n + 1;
                final boolean a = a(n3, d);
                if (d.m0() && a) {
                    androidx.constraintlayout.core.widgets.d.T1(n3, d, b, new lb.a(), lb.a.k);
                }
                boolean b3 = false;
                Label_0270: {
                    Label_0261: {
                        if (constraintAnchor == d.O) {
                            final ConstraintAnchor f = d.Q.f;
                            if (f != null && f.n()) {
                                break Label_0261;
                            }
                        }
                        if (constraintAnchor == d.Q) {
                            final ConstraintAnchor f2 = d.O.f;
                            if (f2 != null && f2.n()) {
                                break Label_0261;
                            }
                        }
                        b3 = false;
                        break Label_0270;
                    }
                    b3 = true;
                }
                final ConstraintWidget.DimensionBehaviour a2 = d.A();
                final ConstraintWidget.DimensionBehaviour match_CONSTRAINT = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                if (a2 == match_CONSTRAINT && !a) {
                    if (d.A() != match_CONSTRAINT || d.A < 0 || d.z < 0 || (d.V() != 8 && (d.w != 0 || d.v() != 0.0f)) || d.i0() || d.l0() || !b3 || d.i0()) {
                        continue;
                    }
                    e(n3, constraintWidget, b, d, b2);
                }
                else {
                    if (d.m0()) {
                        continue;
                    }
                    final ConstraintAnchor o3 = d.O;
                    if (constraintAnchor == o3 && d.Q.f == null) {
                        final int n4 = o3.f() + e;
                        d.F0(n4, d.W() + n4);
                    }
                    else {
                        final ConstraintAnchor q = d.Q;
                        if (constraintAnchor == q && o3.f == null) {
                            final int n5 = e - q.f();
                            d.F0(n5 - d.W(), n5);
                        }
                        else {
                            if (b3 && !d.i0()) {
                                d(n3, b, d, b2);
                                continue;
                            }
                            continue;
                        }
                    }
                    b(n3, d, b, b2);
                }
            }
        }
        if (constraintWidget instanceof f) {
            return;
        }
        if (o2.d() != null && o2.n()) {
            for (final ConstraintAnchor constraintAnchor2 : o2.d()) {
                final ConstraintWidget d2 = constraintAnchor2.d;
                final int n6 = n + 1;
                final boolean a3 = a(n6, d2);
                if (d2.m0() && a3) {
                    d.T1(n6, d2, b, new lb.a(), lb.a.k);
                }
                boolean b4 = false;
                Label_0725: {
                    Label_0716: {
                        if (constraintAnchor2 == d2.O) {
                            final ConstraintAnchor f3 = d2.Q.f;
                            if (f3 != null && f3.n()) {
                                break Label_0716;
                            }
                        }
                        if (constraintAnchor2 == d2.Q) {
                            final ConstraintAnchor f4 = d2.O.f;
                            if (f4 != null && f4.n()) {
                                break Label_0716;
                            }
                        }
                        b4 = false;
                        break Label_0725;
                    }
                    b4 = true;
                }
                final ConstraintWidget.DimensionBehaviour a4 = d2.A();
                final ConstraintWidget.DimensionBehaviour match_CONSTRAINT2 = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                if (a4 == match_CONSTRAINT2 && !a3) {
                    if (d2.A() != match_CONSTRAINT2 || d2.A < 0 || d2.z < 0 || (d2.V() != 8 && (d2.w != 0 || d2.v() != 0.0f)) || d2.i0() || d2.l0() || !b4 || d2.i0()) {
                        continue;
                    }
                    e(n6, constraintWidget, b, d2, b2);
                }
                else {
                    if (d2.m0()) {
                        continue;
                    }
                    final ConstraintAnchor o4 = d2.O;
                    if (constraintAnchor2 == o4 && d2.Q.f == null) {
                        final int n7 = o4.f() + e2;
                        d2.F0(n7, d2.W() + n7);
                    }
                    else {
                        final ConstraintAnchor q2 = d2.Q;
                        if (constraintAnchor2 == q2 && o4.f == null) {
                            final int n8 = e2 - q2.f();
                            d2.F0(n8 - d2.W(), n8);
                        }
                        else {
                            if (b4 && !d2.i0()) {
                                d(n6, b, d2, b2);
                                continue;
                            }
                            continue;
                        }
                    }
                    b(n6, d2, b, b2);
                }
            }
        }
        constraintWidget.q0();
    }
    
    public static void c(int n, final a a, final lb.b b, final int n2, final boolean b2) {
        if (a.t1()) {
            ++n;
            if (n2 == 0) {
                b(n, a, b, b2);
            }
            else {
                i(n, a, b);
            }
        }
    }
    
    public static void d(final int n, final lb.b b, final ConstraintWidget constraintWidget, final boolean b2) {
        float y = constraintWidget.y();
        int e = constraintWidget.O.f.e();
        int e2 = constraintWidget.Q.f.e();
        final int f = constraintWidget.O.f();
        final int f2 = constraintWidget.Q.f();
        if (e == e2) {
            y = 0.5f;
        }
        else {
            e += f;
            e2 -= f2;
        }
        final int w = constraintWidget.W();
        int n2 = e2 - e - w;
        if (e > e2) {
            n2 = e - e2 - w;
        }
        float n3;
        if (n2 > 0) {
            n3 = y * n2 + 0.5f;
        }
        else {
            n3 = y * n2;
        }
        final int n4 = (int)n3 + e;
        int n5 = n4 + w;
        if (e > e2) {
            n5 = n4 - w;
        }
        constraintWidget.F0(n4, n5);
        b(n + 1, constraintWidget, b, b2);
    }
    
    public static void e(final int n, ConstraintWidget k, final lb.b b, final ConstraintWidget constraintWidget, final boolean b2) {
        final float y = constraintWidget.y();
        final int n2 = constraintWidget.O.f.e() + constraintWidget.O.f();
        final int n3 = constraintWidget.Q.f.e() - constraintWidget.Q.f();
        if (n3 >= n2) {
            int n5;
            final int n4 = n5 = constraintWidget.W();
            if (constraintWidget.V() != 8) {
                final int w = constraintWidget.w;
                int b3;
                if (w == 2) {
                    if (!(k instanceof d)) {
                        k = k.K();
                    }
                    b3 = (int)(constraintWidget.y() * 0.5f * k.W());
                }
                else {
                    b3 = n4;
                    if (w == 0) {
                        b3 = n3 - n2;
                    }
                }
                final int max = Math.max(constraintWidget.z, b3);
                final int a = constraintWidget.A;
                n5 = max;
                if (a > 0) {
                    n5 = Math.min(a, max);
                }
            }
            final int n6 = n2 + (int)(y * (n3 - n2 - n5) + 0.5f);
            constraintWidget.F0(n6, n5 + n6);
            b(n + 1, constraintWidget, b, b2);
        }
    }
    
    public static void f(final int n, final lb.b b, final ConstraintWidget constraintWidget) {
        float r = constraintWidget.R();
        int e = constraintWidget.P.f.e();
        int e2 = constraintWidget.R.f.e();
        final int f = constraintWidget.P.f();
        final int f2 = constraintWidget.R.f();
        if (e == e2) {
            r = 0.5f;
        }
        else {
            e += f;
            e2 -= f2;
        }
        final int x = constraintWidget.x();
        int n2 = e2 - e - x;
        if (e > e2) {
            n2 = e - e2 - x;
        }
        float n3;
        if (n2 > 0) {
            n3 = r * n2 + 0.5f;
        }
        else {
            n3 = r * n2;
        }
        final int n4 = (int)n3;
        int n5 = e + n4;
        int n6 = n5 + x;
        if (e > e2) {
            n5 = e - n4;
            n6 = n5 - x;
        }
        constraintWidget.I0(n5, n6);
        i(n + 1, constraintWidget, b);
    }
    
    public static void g(final int n, ConstraintWidget k, final lb.b b, final ConstraintWidget constraintWidget) {
        final float r = constraintWidget.R();
        final int n2 = constraintWidget.P.f.e() + constraintWidget.P.f();
        final int n3 = constraintWidget.R.f.e() - constraintWidget.R.f();
        if (n3 >= n2) {
            int n5;
            final int n4 = n5 = constraintWidget.x();
            if (constraintWidget.V() != 8) {
                final int x = constraintWidget.x;
                int b2;
                if (x == 2) {
                    if (!(k instanceof d)) {
                        k = k.K();
                    }
                    b2 = (int)(r * 0.5f * k.x());
                }
                else {
                    b2 = n4;
                    if (x == 0) {
                        b2 = n3 - n2;
                    }
                }
                final int max = Math.max(constraintWidget.C, b2);
                final int d = constraintWidget.D;
                n5 = max;
                if (d > 0) {
                    n5 = Math.min(d, max);
                }
            }
            final int n6 = n2 + (int)(r * (n3 - n2 - n5) + 0.5f);
            constraintWidget.I0(n6, n5 + n6);
            i(n + 1, constraintWidget, b);
        }
    }
    
    public static void h(final d d, final lb.b b) {
        final ConstraintWidget.DimensionBehaviour a = d.A();
        final ConstraintWidget.DimensionBehaviour t = d.T();
        ys.b = 0;
        ys.c = 0;
        d.v0();
        final ArrayList r1 = d.r1();
        final int size = r1.size();
        for (int i = 0; i < size; ++i) {
            ((ConstraintWidget)r1.get(i)).v0();
        }
        final boolean q1 = d.Q1();
        if (a == ConstraintWidget.DimensionBehaviour.FIXED) {
            d.F0(0, d.W());
        }
        else {
            d.G0(0);
        }
        int j = 0;
        int n2;
        int n = n2 = 0;
        while (j < size) {
            final ConstraintWidget constraintWidget = r1.get(j);
            int n3;
            int n4;
            if (constraintWidget instanceof f) {
                final f f = (f)constraintWidget;
                n3 = n;
                n4 = n2;
                if (f.s1() == 1) {
                    Label_0230: {
                        int t2;
                        if (f.t1() != -1) {
                            t2 = f.t1();
                        }
                        else if (f.u1() != -1 && d.n0()) {
                            t2 = d.W() - f.u1();
                        }
                        else {
                            if (!d.n0()) {
                                break Label_0230;
                            }
                            t2 = (int)(f.v1() * d.W() + 0.5f);
                        }
                        f.w1(t2);
                    }
                    n3 = 1;
                    n4 = n2;
                }
            }
            else {
                n3 = n;
                n4 = n2;
                if (constraintWidget instanceof a) {
                    n3 = n;
                    n4 = n2;
                    if (((a)constraintWidget).x1() == 0) {
                        n4 = 1;
                        n3 = n;
                    }
                }
            }
            ++j;
            n = n3;
            n2 = n4;
        }
        if (n != 0) {
            for (int k = 0; k < size; ++k) {
                final ConstraintWidget constraintWidget2 = r1.get(k);
                if (constraintWidget2 instanceof f) {
                    final f f2 = (f)constraintWidget2;
                    if (f2.s1() == 1) {
                        b(0, f2, b, q1);
                    }
                }
            }
        }
        b(0, d, b, q1);
        if (n2 != 0) {
            for (int l = 0; l < size; ++l) {
                final ConstraintWidget constraintWidget3 = r1.get(l);
                if (constraintWidget3 instanceof a) {
                    final a a2 = (a)constraintWidget3;
                    if (a2.x1() == 0) {
                        c(0, a2, b, 0, q1);
                    }
                }
            }
        }
        if (t == ConstraintWidget.DimensionBehaviour.FIXED) {
            d.I0(0, d.x());
        }
        else {
            d.H0(0);
        }
        int index = 0;
        int n6;
        int n5 = n6 = 0;
        while (index < size) {
            final ConstraintWidget constraintWidget4 = r1.get(index);
            int n7;
            int n8;
            if (constraintWidget4 instanceof f) {
                final f f3 = (f)constraintWidget4;
                n7 = n5;
                n8 = n6;
                if (f3.s1() == 0) {
                    Label_0580: {
                        int t3;
                        if (f3.t1() != -1) {
                            t3 = f3.t1();
                        }
                        else if (f3.u1() != -1 && d.o0()) {
                            t3 = d.x() - f3.u1();
                        }
                        else {
                            if (!d.o0()) {
                                break Label_0580;
                            }
                            t3 = (int)(f3.v1() * d.x() + 0.5f);
                        }
                        f3.w1(t3);
                    }
                    n7 = 1;
                    n8 = n6;
                }
            }
            else {
                n7 = n5;
                n8 = n6;
                if (constraintWidget4 instanceof a) {
                    n7 = n5;
                    n8 = n6;
                    if (((a)constraintWidget4).x1() == 1) {
                        n8 = 1;
                        n7 = n5;
                    }
                }
            }
            ++index;
            n5 = n7;
            n6 = n8;
        }
        if (n5 != 0) {
            for (int index2 = 0; index2 < size; ++index2) {
                final ConstraintWidget constraintWidget5 = r1.get(index2);
                if (constraintWidget5 instanceof f) {
                    final f f4 = (f)constraintWidget5;
                    if (f4.s1() == 0) {
                        i(1, f4, b);
                    }
                }
            }
        }
        i(0, d, b);
        if (n6 != 0) {
            for (int index3 = 0; index3 < size; ++index3) {
                final ConstraintWidget constraintWidget6 = r1.get(index3);
                if (constraintWidget6 instanceof a) {
                    final a a3 = (a)constraintWidget6;
                    if (a3.x1() == 1) {
                        c(0, a3, b, 1, q1);
                    }
                }
            }
        }
        for (int index4 = 0; index4 < size; ++index4) {
            final ConstraintWidget constraintWidget7 = r1.get(index4);
            if (constraintWidget7.m0() && a(0, constraintWidget7)) {
                d.T1(0, constraintWidget7, b, ys.a, lb.a.k);
                if (constraintWidget7 instanceof f) {
                    if (((f)constraintWidget7).s1() != 0) {
                        b(0, constraintWidget7, b, q1);
                        continue;
                    }
                }
                else {
                    b(0, constraintWidget7, b, q1);
                }
                i(0, constraintWidget7, b);
            }
        }
    }
    
    public static void i(final int n, final ConstraintWidget constraintWidget, final lb.b b) {
        if (constraintWidget.p0()) {
            return;
        }
        ++ys.c;
        if (!(constraintWidget instanceof d) && constraintWidget.m0()) {
            final int n2 = n + 1;
            if (a(n2, constraintWidget)) {
                d.T1(n2, constraintWidget, b, new lb.a(), lb.a.k);
            }
        }
        final ConstraintAnchor o = constraintWidget.o(ConstraintAnchor.Type.TOP);
        final ConstraintAnchor o2 = constraintWidget.o(ConstraintAnchor.Type.BOTTOM);
        final int e = o.e();
        final int e2 = o2.e();
        if (o.d() != null && o.n()) {
            for (final ConstraintAnchor constraintAnchor : o.d()) {
                final ConstraintWidget d = constraintAnchor.d;
                final int n3 = n + 1;
                final boolean a = a(n3, d);
                if (d.m0() && a) {
                    androidx.constraintlayout.core.widgets.d.T1(n3, d, b, new lb.a(), lb.a.k);
                }
                boolean b2 = false;
                Label_0265: {
                    Label_0258: {
                        if (constraintAnchor == d.P) {
                            final ConstraintAnchor f = d.R.f;
                            if (f != null && f.n()) {
                                break Label_0258;
                            }
                        }
                        if (constraintAnchor == d.R) {
                            final ConstraintAnchor f2 = d.P.f;
                            if (f2 != null && f2.n()) {
                                break Label_0258;
                            }
                        }
                        b2 = false;
                        break Label_0265;
                    }
                    b2 = true;
                }
                final ConstraintWidget.DimensionBehaviour t = d.T();
                final ConstraintWidget.DimensionBehaviour match_CONSTRAINT = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                if (t == match_CONSTRAINT && !a) {
                    if (d.T() != match_CONSTRAINT || d.D < 0 || d.C < 0 || (d.V() != 8 && (d.x != 0 || d.v() != 0.0f)) || d.k0() || d.l0() || !b2 || d.k0()) {
                        continue;
                    }
                    g(n3, constraintWidget, b, d);
                }
                else {
                    if (d.m0()) {
                        continue;
                    }
                    final ConstraintAnchor p3 = d.P;
                    if (constraintAnchor == p3 && d.R.f == null) {
                        final int n4 = p3.f() + e;
                        d.I0(n4, d.x() + n4);
                    }
                    else {
                        final ConstraintAnchor r = d.R;
                        if (constraintAnchor == r && p3.f == null) {
                            final int n5 = e - r.f();
                            d.I0(n5 - d.x(), n5);
                        }
                        else {
                            if (b2 && !d.k0()) {
                                f(n3, b, d);
                                continue;
                            }
                            continue;
                        }
                    }
                    i(n3, d, b);
                }
            }
        }
        if (constraintWidget instanceof f) {
            return;
        }
        if (o2.d() != null && o2.n()) {
            for (final ConstraintAnchor constraintAnchor2 : o2.d()) {
                final ConstraintWidget d2 = constraintAnchor2.d;
                final int n6 = n + 1;
                final boolean a2 = a(n6, d2);
                if (d2.m0() && a2) {
                    d.T1(n6, d2, b, new lb.a(), lb.a.k);
                }
                boolean b3 = false;
                Label_0707: {
                    Label_0700: {
                        if (constraintAnchor2 == d2.P) {
                            final ConstraintAnchor f3 = d2.R.f;
                            if (f3 != null && f3.n()) {
                                break Label_0700;
                            }
                        }
                        if (constraintAnchor2 == d2.R) {
                            final ConstraintAnchor f4 = d2.P.f;
                            if (f4 != null && f4.n()) {
                                break Label_0700;
                            }
                        }
                        b3 = false;
                        break Label_0707;
                    }
                    b3 = true;
                }
                final ConstraintWidget.DimensionBehaviour t2 = d2.T();
                final ConstraintWidget.DimensionBehaviour match_CONSTRAINT2 = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                if (t2 == match_CONSTRAINT2 && !a2) {
                    if (d2.T() != match_CONSTRAINT2 || d2.D < 0 || d2.C < 0 || (d2.V() != 8 && (d2.x != 0 || d2.v() != 0.0f)) || d2.k0() || d2.l0() || !b3 || d2.k0()) {
                        continue;
                    }
                    g(n6, constraintWidget, b, d2);
                }
                else {
                    if (d2.m0()) {
                        continue;
                    }
                    final ConstraintAnchor p4 = d2.P;
                    if (constraintAnchor2 == p4 && d2.R.f == null) {
                        final int n7 = p4.f() + e2;
                        d2.I0(n7, d2.x() + n7);
                    }
                    else {
                        final ConstraintAnchor r2 = d2.R;
                        if (constraintAnchor2 == r2 && p4.f == null) {
                            final int n8 = e2 - r2.f();
                            d2.I0(n8 - d2.x(), n8);
                        }
                        else {
                            if (b3 && !d2.k0()) {
                                f(n6, b, d2);
                                continue;
                            }
                            continue;
                        }
                    }
                    i(n6, d2, b);
                }
            }
        }
        final ConstraintAnchor o3 = constraintWidget.o(ConstraintAnchor.Type.BASELINE);
        if (o3.d() != null && o3.n()) {
            final int e3 = o3.e();
            for (final ConstraintAnchor constraintAnchor3 : o3.d()) {
                final ConstraintWidget d3 = constraintAnchor3.d;
                final int n9 = n + 1;
                final boolean a3 = a(n9, d3);
                if (d3.m0() && a3) {
                    d.T1(n9, d3, b, new lb.a(), lb.a.k);
                }
                if (d3.T() != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT || a3) {
                    if (d3.m0()) {
                        continue;
                    }
                    if (constraintAnchor3 != d3.S) {
                        continue;
                    }
                    d3.E0(constraintAnchor3.f() + e3);
                    i(n9, d3, b);
                }
            }
        }
        constraintWidget.r0();
    }
}

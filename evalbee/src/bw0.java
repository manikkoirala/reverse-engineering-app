import java.io.Serializable;
import java.io.Writer;
import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.Closeable;
import java.io.InputStream;
import com.google.firebase.crashlytics.internal.common.CommonUtils;
import java.io.FileInputStream;
import java.util.Collections;
import java.io.File;
import org.json.JSONException;
import org.json.JSONArray;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.util.HashMap;
import org.json.JSONObject;
import java.util.Map;
import java.nio.charset.Charset;

// 
// Decompiled by Procyon v0.6.0
// 

public class bw0
{
    public static final Charset b;
    public final z00 a;
    
    static {
        b = Charset.forName("UTF-8");
    }
    
    public bw0(final z00 a) {
        this.a = a;
    }
    
    public static Map e(final String s) {
        final JSONObject jsonObject = new JSONObject(s);
        final HashMap hashMap = new HashMap();
        final Iterator keys = jsonObject.keys();
        while (keys.hasNext()) {
            final String s2 = keys.next();
            hashMap.put(s2, o(jsonObject, s2));
        }
        return hashMap;
    }
    
    public static List f(String jsonArray) {
        jsonArray = (String)new JSONObject(jsonArray).getJSONArray("rolloutsState");
        final ArrayList list = new ArrayList();
        for (int i = 0; i < ((JSONArray)jsonArray).length(); ++i) {
            final String string = ((JSONArray)jsonArray).getString(i);
            try {
                list.add(nf1.a(string));
            }
            catch (final Exception ex) {
                final zl0 f = zl0.f();
                final StringBuilder sb = new StringBuilder();
                sb.append("Failed de-serializing rollouts state. ");
                sb.append(string);
                f.l(sb.toString(), ex);
            }
        }
        return list;
    }
    
    public static String h(final Map map) {
        return new JSONObject(map).toString();
    }
    
    public static String l(final List list) {
        final HashMap hashMap = new HashMap();
        final JSONArray value = new JSONArray();
        for (int i = 0; i < list.size(); ++i) {
            final String encode = nf1.a.encode(list.get(i));
            try {
                value.put((Object)new JSONObject(encode));
            }
            catch (final JSONException ex) {
                zl0.f().l("Exception parsing rollout assignment!", (Throwable)ex);
            }
        }
        hashMap.put("rolloutsState", value);
        return new JSONObject((Map)hashMap).toString();
    }
    
    public static void m(final File file) {
        if (file.exists() && file.delete()) {
            final zl0 f = zl0.f();
            final StringBuilder sb = new StringBuilder();
            sb.append("Deleted corrupt file: ");
            sb.append(file.getAbsolutePath());
            f.g(sb.toString());
        }
    }
    
    public static String n(final String s) {
        return new JSONObject(s) {
            public final String a;
            
            {
                this.put("userId", (Object)a);
            }
        }.toString();
    }
    
    public static String o(final JSONObject jsonObject, final String s) {
        final boolean null = jsonObject.isNull(s);
        String optString = null;
        if (!null) {
            optString = jsonObject.optString(s, (String)null);
        }
        return optString;
    }
    
    public File a(final String s) {
        return this.a.o(s, "internal-keys");
    }
    
    public File b(final String s) {
        return this.a.o(s, "keys");
    }
    
    public File c(final String s) {
        return this.a.o(s, "rollouts-state");
    }
    
    public File d(final String s) {
        return this.a.o(s, "user-data");
    }
    
    public final String g(final String s) {
        return o(new JSONObject(s), "userId");
    }
    
    public Map i(String s, final boolean b) {
        File file;
        if (b) {
            file = this.a(s);
        }
        else {
            file = this.b(s);
        }
        if (!file.exists() || file.length() == 0L) {
            m(file);
            return Collections.emptyMap();
        }
        s = null;
        Label_0117: {
            Object o;
            try {
                o = (s = (String)new FileInputStream(file));
                try {
                    final Object o2 = o;
                    final String s2 = CommonUtils.A((InputStream)o2);
                    final Map map = e(s2);
                    final Object o3 = o;
                    final String s3 = "Failed to close user metadata file.";
                    CommonUtils.f((Closeable)o3, s3);
                    return map;
                }
                catch (final Exception ex) {}
            }
            catch (final Exception ex) {
                o = null;
            }
            finally {
                break Label_0117;
            }
            try {
                final Object o2 = o;
                final String s2 = CommonUtils.A((InputStream)o2);
                final Map map = e(s2);
                final Object o3 = o;
                final String s3 = "Failed to close user metadata file.";
                CommonUtils.f((Closeable)o3, s3);
                return map;
                s = (String)o;
                final Exception ex;
                zl0.f().l("Error deserializing user metadata.", ex);
                s = (String)o;
                m(file);
                CommonUtils.f((Closeable)o, "Failed to close user metadata file.");
                return Collections.emptyMap();
            }
            finally {}
        }
        CommonUtils.f((Closeable)s, "Failed to close user metadata file.");
    }
    
    public List j(final String str) {
        final File c = this.c(str);
        if (!c.exists() || c.length() == 0L) {
            m(c);
            return Collections.emptyList();
        }
        Object f = null;
        Object o2;
        final Object o = o2 = null;
        FileInputStream fileInputStream = null;
        zl0 zl0;
        try {
            try {
                o2 = o;
                fileInputStream = new FileInputStream(c);
                try {
                    final List f2 = f(CommonUtils.A(fileInputStream));
                    f = zl0.f();
                    o2 = new StringBuilder();
                    ((StringBuilder)o2).append("Loaded rollouts state:\n");
                    ((StringBuilder)o2).append(f2);
                    ((StringBuilder)o2).append("\nfor session ");
                    ((StringBuilder)o2).append(str);
                    ((zl0)f).b(((StringBuilder)o2).toString());
                    CommonUtils.f(fileInputStream, "Failed to close rollouts state file.");
                    return f2;
                }
                catch (final Exception o2) {}
                finally {
                    o2 = fileInputStream;
                }
            }
            finally {}
        }
        catch (final Exception fileInputStream) {
            zl0 = (zl0)f;
        }
        zl0.f().l("Error deserializing rollouts state.", (Throwable)fileInputStream);
        m(c);
        CommonUtils.f((Closeable)zl0, "Failed to close rollouts state file.");
        return Collections.emptyList();
        CommonUtils.f((Closeable)o2, "Failed to close rollouts state file.");
    }
    
    public String k(final String str) {
        final File d = this.d(str);
        final boolean exists = d.exists();
        Object o = null;
        if (!exists || d.length() == 0L) {
            final zl0 f = zl0.f();
            final StringBuilder sb = new StringBuilder();
            sb.append("No userId set for session ");
            sb.append(str);
            f.b(sb.toString());
            m(d);
            return null;
        }
        Label_0196: {
            FileInputStream fileInputStream = null;
            try {
                fileInputStream = (FileInputStream)(o = new FileInputStream(d));
                try {
                    final bw0 bw0 = this;
                    final FileInputStream fileInputStream2 = fileInputStream;
                    final String s = CommonUtils.A(fileInputStream2);
                    final String s2 = bw0.g(s);
                    final zl0 zl0 = zl0.f();
                    final StringBuilder sb2 = new(java.lang.StringBuilder.class)();
                    final StringBuilder sb3 = sb2;
                    final StringBuilder sb4 = sb3;
                    new StringBuilder();
                    final StringBuilder sb5 = sb3;
                    final String s3 = "Loaded userId ";
                    sb5.append(s3);
                    final StringBuilder sb6 = sb3;
                    final String s4 = s2;
                    sb6.append(s4);
                    final StringBuilder sb7 = sb3;
                    final String s5 = " for session ";
                    sb7.append(s5);
                    final StringBuilder sb8 = sb3;
                    final String s6 = str;
                    sb8.append(s6);
                    final zl0 zl2 = zl0;
                    final StringBuilder sb9 = sb3;
                    final String s7 = sb9.toString();
                    zl2.b(s7);
                    final FileInputStream fileInputStream3 = fileInputStream;
                    final String s8 = "Failed to close user metadata file.";
                    CommonUtils.f(fileInputStream3, s8);
                    return s2;
                }
                catch (final Exception o) {
                    final Object o2 = fileInputStream;
                    final Object o3 = o;
                }
            }
            catch (final Exception o3) {
                final Object o2 = null;
            }
            finally {
                break Label_0196;
            }
            try {
                final bw0 bw0 = this;
                final FileInputStream fileInputStream2 = fileInputStream;
                final String s = CommonUtils.A(fileInputStream2);
                final String s2 = bw0.g(s);
                final zl0 zl0 = zl0.f();
                final StringBuilder sb2 = new(java.lang.StringBuilder.class)();
                final StringBuilder sb4;
                final StringBuilder sb3 = sb4 = sb2;
                new StringBuilder();
                final StringBuilder sb5 = sb3;
                final String s3 = "Loaded userId ";
                sb5.append(s3);
                final StringBuilder sb6 = sb3;
                final String s4 = s2;
                sb6.append(s4);
                final StringBuilder sb7 = sb3;
                final String s5 = " for session ";
                sb7.append(s5);
                final StringBuilder sb8 = sb3;
                final String s6 = str;
                sb8.append(s6);
                final zl0 zl2 = zl0;
                final StringBuilder sb9 = sb3;
                final String s7 = sb9.toString();
                zl2.b(s7);
                final FileInputStream fileInputStream3 = fileInputStream;
                final String s8 = "Failed to close user metadata file.";
                CommonUtils.f(fileInputStream3, s8);
                return s2;
                final Object o2;
                o = o2;
                final Object o3;
                zl0.f().l("Error deserializing user metadata.", (Throwable)o3);
                o = o2;
                m(d);
                CommonUtils.f((Closeable)o2, "Failed to close user metadata file.");
                return null;
            }
            finally {}
        }
        CommonUtils.f((Closeable)o, "Failed to close user metadata file.");
    }
    
    public void p(final String s, final Map map) {
        this.q(s, map, false);
    }
    
    public void q(String s, Map bufferedWriter, final boolean b) {
        Serializable file;
        if (b) {
            file = this.a(s);
        }
        else {
            file = this.b(s);
        }
        final Closeable closeable = null;
        final Serializable s2 = s = null;
        Closeable closeable2;
        try {
            try {
                final String h = h((Map)bufferedWriter);
                s = (String)s2;
                s = (String)s2;
                s = (String)s2;
                s = (String)s2;
                final FileOutputStream out = new FileOutputStream((File)file);
                s = (String)s2;
                final OutputStreamWriter out2 = new OutputStreamWriter(out, bw0.b);
                s = (String)s2;
                bufferedWriter = new BufferedWriter(out2);
                try {
                    bufferedWriter.write(h);
                    bufferedWriter.flush();
                    CommonUtils.f(bufferedWriter, "Failed to close key/value metadata file.");
                }
                catch (final Exception s2) {}
                finally {
                    file = (s = (String)bufferedWriter);
                }
            }
            finally {}
        }
        catch (final Exception s2) {
            closeable2 = closeable;
        }
        zl0.f().l("Error serializing key/value metadata.", (Throwable)s2);
        m((File)file);
        CommonUtils.f(closeable2, "Failed to close key/value metadata file.");
        return;
        CommonUtils.f((Closeable)s, "Failed to close key/value metadata file.");
    }
    
    public void r(String s, List o) {
        final File c = this.c(s);
        if (((List)o).isEmpty()) {
            m(c);
            return;
        }
        final Closeable closeable = null;
        final Serializable s2 = s = null;
        Closeable closeable2;
        try {
            try {
                final String l = l((List)o);
                s = (String)s2;
                s = (String)s2;
                s = (String)s2;
                s = (String)s2;
                final FileOutputStream out = new FileOutputStream(c);
                s = (String)s2;
                final OutputStreamWriter out2 = new OutputStreamWriter(out, bw0.b);
                s = (String)s2;
                o = new BufferedWriter(out2);
                try {
                    ((Writer)o).write(l);
                    ((Writer)o).flush();
                    CommonUtils.f((Closeable)o, "Failed to close rollouts state file.");
                }
                catch (final Exception s2) {}
                finally {
                    s = (String)o;
                }
            }
            finally {}
        }
        catch (final Exception s2) {
            closeable2 = closeable;
        }
        zl0.f().l("Error serializing rollouts state.", (Throwable)s2);
        m(c);
        CommonUtils.f(closeable2, "Failed to close rollouts state file.");
        return;
        CommonUtils.f((Closeable)s, "Failed to close rollouts state file.");
    }
    
    public void s(String s, final String s2) {
        final File d = this.d(s);
        final Closeable closeable = null;
        final String s3 = s = null;
        Object o;
        final Exception ex3;
        try {
            try {
                final String n = n(s2);
                s = s3;
                s = s3;
                s = s3;
                s = s3;
                final FileOutputStream out = new FileOutputStream(d);
                s = s3;
                final OutputStreamWriter out2 = new OutputStreamWriter(out, bw0.b);
                s = s3;
                final BufferedWriter bufferedWriter = new BufferedWriter(out2);
                try {
                    bufferedWriter.write(n);
                    bufferedWriter.flush();
                    CommonUtils.f(bufferedWriter, "Failed to close user metadata file.");
                }
                catch (final Exception ex) {}
            }
            finally {
                o = s;
                final Exception ex2 = ex3;
            }
        }
        catch (final Exception ex3) {
            o = closeable;
        }
        zl0.f().l("Error serializing user metadata.", ex3);
        CommonUtils.f((Closeable)o, "Failed to close user metadata file.");
        return;
        CommonUtils.f((Closeable)o, "Failed to close user metadata file.");
        throw null;
    }
}

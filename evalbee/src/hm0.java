// 
// Decompiled by Procyon v0.6.0
// 

public class hm0 implements Cloneable
{
    public static final Object e;
    public boolean a;
    public long[] b;
    public Object[] c;
    public int d;
    
    static {
        e = new Object();
    }
    
    public hm0() {
        this(10);
    }
    
    public hm0(int f) {
        this.a = false;
        if (f == 0) {
            this.b = el.b;
            this.c = el.c;
        }
        else {
            f = el.f(f);
            this.b = new long[f];
            this.c = new Object[f];
        }
    }
    
    public void a() {
        final int d = this.d;
        final Object[] c = this.c;
        for (int i = 0; i < d; ++i) {
            c[i] = null;
        }
        this.d = 0;
        this.a = false;
    }
    
    public hm0 b() {
        try {
            final hm0 hm0 = (hm0)super.clone();
            hm0.b = this.b.clone();
            hm0.c = this.c.clone();
            return hm0;
        }
        catch (final CloneNotSupportedException detailMessage) {
            throw new AssertionError((Object)detailMessage);
        }
    }
    
    public final void d() {
        final int d = this.d;
        final long[] b = this.b;
        final Object[] c = this.c;
        int i = 0;
        int d2 = 0;
        while (i < d) {
            final Object o = c[i];
            int n = d2;
            if (o != hm0.e) {
                if (i != d2) {
                    b[d2] = b[i];
                    c[d2] = o;
                    c[i] = null;
                }
                n = d2 + 1;
            }
            ++i;
            d2 = n;
        }
        this.a = false;
        this.d = d2;
    }
    
    public Object e(final long n) {
        return this.f(n, null);
    }
    
    public Object f(final long n, final Object o) {
        final int b = el.b(this.b, this.d, n);
        if (b >= 0) {
            final Object o2 = this.c[b];
            if (o2 != hm0.e) {
                return o2;
            }
        }
        return o;
    }
    
    public int h(final long n) {
        if (this.a) {
            this.d();
        }
        return el.b(this.b, this.d, n);
    }
    
    public long i(final int n) {
        if (this.a) {
            this.d();
        }
        return this.b[n];
    }
    
    public void j(final long n, final Object o) {
        final int b = el.b(this.b, this.d, n);
        if (b >= 0) {
            this.c[b] = o;
        }
        else {
            final int n2 = ~b;
            final int d = this.d;
            if (n2 < d) {
                final Object[] c = this.c;
                if (c[n2] == hm0.e) {
                    this.b[n2] = n;
                    c[n2] = o;
                    return;
                }
            }
            int n3 = n2;
            if (this.a) {
                n3 = n2;
                if (d >= this.b.length) {
                    this.d();
                    n3 = ~el.b(this.b, this.d, n);
                }
            }
            final int d2 = this.d;
            if (d2 >= this.b.length) {
                final int f = el.f(d2 + 1);
                final long[] b2 = new long[f];
                final Object[] c2 = new Object[f];
                final long[] b3 = this.b;
                System.arraycopy(b3, 0, b2, 0, b3.length);
                final Object[] c3 = this.c;
                System.arraycopy(c3, 0, c2, 0, c3.length);
                this.b = b2;
                this.c = c2;
            }
            final int d3 = this.d;
            if (d3 - n3 != 0) {
                final long[] b4 = this.b;
                final int n4 = n3 + 1;
                System.arraycopy(b4, n3, b4, n4, d3 - n3);
                final Object[] c4 = this.c;
                System.arraycopy(c4, n3, c4, n4, this.d - n3);
            }
            this.b[n3] = n;
            this.c[n3] = o;
            ++this.d;
        }
    }
    
    public void k(final long n) {
        final int b = el.b(this.b, this.d, n);
        if (b >= 0) {
            final Object[] c = this.c;
            final Object o = c[b];
            final Object e = hm0.e;
            if (o != e) {
                c[b] = e;
                this.a = true;
            }
        }
    }
    
    public void l(final int n) {
        final Object[] c = this.c;
        final Object o = c[n];
        final Object e = hm0.e;
        if (o != e) {
            c[n] = e;
            this.a = true;
        }
    }
    
    public int m() {
        if (this.a) {
            this.d();
        }
        return this.d;
    }
    
    public Object n(final int n) {
        if (this.a) {
            this.d();
        }
        return this.c[n];
    }
    
    @Override
    public String toString() {
        if (this.m() <= 0) {
            return "{}";
        }
        final StringBuilder sb = new StringBuilder(this.d * 28);
        sb.append('{');
        for (int i = 0; i < this.d; ++i) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(this.i(i));
            sb.append('=');
            final Object n = this.n(i);
            if (n != this) {
                sb.append(n);
            }
            else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }
}

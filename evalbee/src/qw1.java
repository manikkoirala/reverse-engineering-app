import android.content.res.AssetManager;
import java.lang.ref.WeakReference;
import android.content.Context;
import android.content.res.Resources$Theme;
import android.content.res.Resources;
import java.util.ArrayList;
import android.content.ContextWrapper;

// 
// Decompiled by Procyon v0.6.0
// 

public class qw1 extends ContextWrapper
{
    public static final Object c;
    public static ArrayList d;
    public final Resources a;
    public final Resources$Theme b;
    
    static {
        c = new Object();
    }
    
    public qw1(final Context context) {
        super(context);
        if (f32.c()) {
            final f32 a = new f32((Context)this, context.getResources());
            this.a = a;
            (this.b = a.newTheme()).setTo(context.getTheme());
        }
        else {
            this.a = new sw1((Context)this, context.getResources());
            this.b = null;
        }
    }
    
    public static boolean a(final Context context) {
        final boolean b = context instanceof qw1;
        boolean b3;
        final boolean b2 = b3 = false;
        if (!b) {
            b3 = b2;
            if (!(context.getResources() instanceof sw1)) {
                if (context.getResources() instanceof f32) {
                    b3 = b2;
                }
                else {
                    b3 = b2;
                    if (f32.c()) {
                        b3 = true;
                    }
                }
            }
        }
        return b3;
    }
    
    public static Context b(final Context context) {
        if (a(context)) {
            synchronized (qw1.c) {
                final ArrayList d = qw1.d;
                if (d == null) {
                    qw1.d = new ArrayList();
                }
                else {
                    for (int i = d.size() - 1; i >= 0; --i) {
                        final WeakReference weakReference = qw1.d.get(i);
                        if (weakReference == null || weakReference.get() == null) {
                            qw1.d.remove(i);
                        }
                    }
                    for (int j = qw1.d.size() - 1; j >= 0; --j) {
                        final WeakReference weakReference2 = qw1.d.get(j);
                        ContextWrapper contextWrapper;
                        if (weakReference2 != null) {
                            contextWrapper = (qw1)weakReference2.get();
                        }
                        else {
                            contextWrapper = null;
                        }
                        if (contextWrapper != null && contextWrapper.getBaseContext() == context) {
                            return (Context)contextWrapper;
                        }
                    }
                }
                final qw1 referent = new qw1(context);
                qw1.d.add(new WeakReference(referent));
                return (Context)referent;
            }
        }
        return context;
    }
    
    public AssetManager getAssets() {
        return this.a.getAssets();
    }
    
    public Resources getResources() {
        return this.a;
    }
    
    public Resources$Theme getTheme() {
        Resources$Theme resources$Theme;
        if ((resources$Theme = this.b) == null) {
            resources$Theme = super.getTheme();
        }
        return resources$Theme;
    }
    
    public void setTheme(final int theme) {
        final Resources$Theme b = this.b;
        if (b == null) {
            super.setTheme(theme);
        }
        else {
            b.applyStyle(theme, true);
        }
    }
}

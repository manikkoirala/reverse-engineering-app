import android.os.IBinder;
import android.content.Intent;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.n;
import android.app.Service;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class rj0 extends Service implements qj0
{
    public final n a;
    
    public rj0() {
        this.a = new n(this);
    }
    
    public Lifecycle getLifecycle() {
        return this.a.a();
    }
    
    public IBinder onBind(final Intent intent) {
        fg0.e((Object)intent, "intent");
        this.a.b();
        return null;
    }
    
    public void onCreate() {
        this.a.c();
        super.onCreate();
    }
    
    public void onDestroy() {
        this.a.d();
        super.onDestroy();
    }
    
    public void onStart(final Intent intent, final int n) {
        this.a.e();
        super.onStart(intent, n);
    }
    
    public int onStartCommand(final Intent intent, final int n, final int n2) {
        return super.onStartCommand(intent, n, n2);
    }
}

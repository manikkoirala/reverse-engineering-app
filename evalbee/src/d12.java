import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.security.AccessController;
import java.lang.reflect.Field;
import java.security.PrivilegedExceptionAction;
import java.nio.ByteOrder;
import sun.misc.Unsafe;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class d12
{
    public static final Unsafe a;
    public static final Class b;
    public static final boolean c;
    public static final boolean d;
    public static final e e;
    public static final boolean f;
    public static final boolean g;
    public static final long h;
    public static final long i;
    public static final long j;
    public static final long k;
    public static final long l;
    public static final long m;
    public static final long n;
    public static final long o;
    public static final long p;
    public static final long q;
    public static final long r;
    public static final long s;
    public static final long t;
    public static final long u;
    public static final int v;
    public static final boolean w;
    
    static {
        a = H();
        b = p4.b();
        c = q(Long.TYPE);
        d = q(Integer.TYPE);
        e = F();
        f = Y();
        g = X();
        final long n2 = h = m(byte[].class);
        i = m(boolean[].class);
        j = n(boolean[].class);
        k = m(int[].class);
        l = n(int[].class);
        m = m(long[].class);
        n = n(long[].class);
        o = m(float[].class);
        p = n(float[].class);
        q = m(double[].class);
        r = n(double[].class);
        s = m(Object[].class);
        t = n(Object[].class);
        u = s(o());
        v = (int)(n2 & 0x7L);
        w = (ByteOrder.nativeOrder() == ByteOrder.BIG_ENDIAN);
    }
    
    public static double A(final Object o, final long n) {
        return d12.e.g(o, n);
    }
    
    public static float B(final Object o, final long n) {
        return d12.e.h(o, n);
    }
    
    public static int C(final Object o, final long n) {
        return d12.e.i(o, n);
    }
    
    public static long D(final long n) {
        return d12.e.j(n);
    }
    
    public static long E(final Object o, final long n) {
        return d12.e.k(o, n);
    }
    
    public static e F() {
        final Unsafe a = d12.a;
        Object o = null;
        if (a == null) {
            return null;
        }
        if (!p4.c()) {
            return (e)new d(a);
        }
        if (d12.c) {
            return (e)new c(a);
        }
        if (d12.d) {
            o = new b(a);
        }
        return (e)o;
    }
    
    public static Object G(final Object o, final long n) {
        return d12.e.l(o, n);
    }
    
    public static Unsafe H() {
        Unsafe unsafe2;
        try {
            final Unsafe unsafe = AccessController.doPrivileged((PrivilegedExceptionAction<Unsafe>)new PrivilegedExceptionAction() {
                public Unsafe a() {
                    for (final Field field : Unsafe.class.getDeclaredFields()) {
                        field.setAccessible(true);
                        final Object value = field.get(null);
                        if (Unsafe.class.isInstance(value)) {
                            return Unsafe.class.cast(value);
                        }
                    }
                    return null;
                }
            });
        }
        finally {
            unsafe2 = null;
        }
        return unsafe2;
    }
    
    public static boolean I() {
        return d12.g;
    }
    
    public static boolean J() {
        return d12.f;
    }
    
    public static void K(final Throwable obj) {
        final Logger logger = Logger.getLogger(d12.class.getName());
        final Level warning = Level.WARNING;
        final StringBuilder sb = new StringBuilder();
        sb.append("platform method missing - proto runtime falling back to safer methods: ");
        sb.append(obj);
        logger.log(warning, sb.toString());
    }
    
    public static long L(final Field field) {
        return d12.e.m(field);
    }
    
    public static void M(final Object o, final long n, final boolean b) {
        d12.e.n(o, n, b);
    }
    
    public static void N(final Object o, final long n, final boolean b) {
        Q(o, n, (byte)(b ? 1 : 0));
    }
    
    public static void O(final Object o, final long n, final boolean b) {
        R(o, n, (byte)(b ? 1 : 0));
    }
    
    public static void P(final byte[] array, final long n, final byte b) {
        d12.e.o(array, d12.h + n, b);
    }
    
    public static void Q(final Object o, final long n, final byte b) {
        final long n2 = 0xFFFFFFFFFFFFFFFCL & n;
        final int c = C(o, n2);
        final int n3 = (~(int)n & 0x3) << 3;
        U(o, n2, (0xFF & b) << n3 | (c & ~(255 << n3)));
    }
    
    public static void R(final Object o, final long n, final byte b) {
        final long n2 = 0xFFFFFFFFFFFFFFFCL & n;
        final int c = C(o, n2);
        final int n3 = ((int)n & 0x3) << 3;
        U(o, n2, (0xFF & b) << n3 | (c & ~(255 << n3)));
    }
    
    public static void S(final Object o, final long n, final double n2) {
        d12.e.p(o, n, n2);
    }
    
    public static void T(final Object o, final long n, final float n2) {
        d12.e.q(o, n, n2);
    }
    
    public static void U(final Object o, final long n, final int n2) {
        d12.e.r(o, n, n2);
    }
    
    public static void V(final Object o, final long n, final long n2) {
        d12.e.s(o, n, n2);
    }
    
    public static void W(final Object o, final long n, final Object o2) {
        d12.e.t(o, n, o2);
    }
    
    public static boolean X() {
        final e e = d12.e;
        return e != null && e.u();
    }
    
    public static boolean Y() {
        final e e = d12.e;
        return e != null && e.v();
    }
    
    public static long k(final ByteBuffer byteBuffer) {
        return d12.e.k(byteBuffer, d12.u);
    }
    
    public static Object l(final Class cls) {
        try {
            return d12.a.allocateInstance(cls);
        }
        catch (final InstantiationException cause) {
            throw new IllegalStateException(cause);
        }
    }
    
    public static int m(final Class clazz) {
        int a;
        if (d12.g) {
            a = d12.e.a(clazz);
        }
        else {
            a = -1;
        }
        return a;
    }
    
    public static int n(final Class clazz) {
        int b;
        if (d12.g) {
            b = d12.e.b(clazz);
        }
        else {
            b = -1;
        }
        return b;
    }
    
    public static Field o() {
        if (p4.c()) {
            final Field r = r(Buffer.class, "effectiveDirectAddress");
            if (r != null) {
                return r;
            }
        }
        Field r2 = r(Buffer.class, "address");
        if (r2 == null || r2.getType() != Long.TYPE) {
            r2 = null;
        }
        return r2;
    }
    
    public static void p(final long n, final byte[] array, final long n2, final long n3) {
        d12.e.c(n, array, n2, n3);
    }
    
    public static boolean q(final Class clazz) {
        if (!p4.c()) {
            return false;
        }
        try {
            final Class b = d12.b;
            final Class<Boolean> type = Boolean.TYPE;
            b.getMethod("peekLong", clazz, type);
            b.getMethod("pokeLong", clazz, Long.TYPE, type);
            final Class<Integer> type2 = Integer.TYPE;
            b.getMethod("pokeInt", clazz, type2, type);
            b.getMethod("peekInt", clazz, type);
            b.getMethod("pokeByte", clazz, Byte.TYPE);
            b.getMethod("peekByte", clazz);
            b.getMethod("pokeByteArray", clazz, byte[].class, type2, type2);
            b.getMethod("peekByteArray", clazz, byte[].class, type2, type2);
            return true;
        }
        finally {
            return false;
        }
    }
    
    public static Field r(final Class clazz, final String name) {
        Field field;
        try {
            clazz.getDeclaredField(name);
        }
        finally {
            field = null;
        }
        return field;
    }
    
    public static long s(final Field field) {
        if (field != null) {
            final e e = d12.e;
            if (e != null) {
                return e.m(field);
            }
        }
        return -1L;
    }
    
    public static boolean t(final Object o, final long n) {
        return d12.e.d(o, n);
    }
    
    public static boolean u(final Object o, final long n) {
        return y(o, n) != 0;
    }
    
    public static boolean v(final Object o, final long n) {
        return z(o, n) != 0;
    }
    
    public static byte w(final long n) {
        return d12.e.e(n);
    }
    
    public static byte x(final byte[] array, final long n) {
        return d12.e.f(array, d12.h + n);
    }
    
    public static byte y(final Object o, final long n) {
        return (byte)(C(o, 0xFFFFFFFFFFFFFFFCL & n) >>> (int)((~n & 0x3L) << 3) & 0xFF);
    }
    
    public static byte z(final Object o, final long n) {
        return (byte)(C(o, 0xFFFFFFFFFFFFFFFCL & n) >>> (int)((n & 0x3L) << 3) & 0xFF);
    }
    
    public static final class b extends e
    {
        public b(final Unsafe unsafe) {
            super(unsafe);
        }
        
        @Override
        public void c(final long n, final byte[] array, final long n2, final long n3) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public boolean d(final Object o, final long n) {
            if (d12.w) {
                return u(o, n);
            }
            return v(o, n);
        }
        
        @Override
        public byte e(final long n) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public byte f(final Object o, final long n) {
            if (d12.w) {
                return y(o, n);
            }
            return z(o, n);
        }
        
        @Override
        public double g(final Object o, final long n) {
            return Double.longBitsToDouble(((e)this).k(o, n));
        }
        
        @Override
        public float h(final Object o, final long n) {
            return Float.intBitsToFloat(((e)this).i(o, n));
        }
        
        @Override
        public long j(final long n) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void n(final Object o, final long n, final boolean b) {
            if (d12.w) {
                N(o, n, b);
            }
            else {
                O(o, n, b);
            }
        }
        
        @Override
        public void o(final Object o, final long n, final byte b) {
            if (d12.w) {
                Q(o, n, b);
            }
            else {
                R(o, n, b);
            }
        }
        
        @Override
        public void p(final Object o, final long n, final double value) {
            ((e)this).s(o, n, Double.doubleToLongBits(value));
        }
        
        @Override
        public void q(final Object o, final long n, final float value) {
            ((e)this).r(o, n, Float.floatToIntBits(value));
        }
        
        @Override
        public boolean v() {
            return false;
        }
    }
    
    public static final class c extends e
    {
        public c(final Unsafe unsafe) {
            super(unsafe);
        }
        
        @Override
        public void c(final long n, final byte[] array, final long n2, final long n3) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public boolean d(final Object o, final long n) {
            if (d12.w) {
                return u(o, n);
            }
            return v(o, n);
        }
        
        @Override
        public byte e(final long n) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public byte f(final Object o, final long n) {
            if (d12.w) {
                return y(o, n);
            }
            return z(o, n);
        }
        
        @Override
        public double g(final Object o, final long n) {
            return Double.longBitsToDouble(((e)this).k(o, n));
        }
        
        @Override
        public float h(final Object o, final long n) {
            return Float.intBitsToFloat(((e)this).i(o, n));
        }
        
        @Override
        public long j(final long n) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void n(final Object o, final long n, final boolean b) {
            if (d12.w) {
                N(o, n, b);
            }
            else {
                O(o, n, b);
            }
        }
        
        @Override
        public void o(final Object o, final long n, final byte b) {
            if (d12.w) {
                Q(o, n, b);
            }
            else {
                R(o, n, b);
            }
        }
        
        @Override
        public void p(final Object o, final long n, final double value) {
            ((e)this).s(o, n, Double.doubleToLongBits(value));
        }
        
        @Override
        public void q(final Object o, final long n, final float value) {
            ((e)this).r(o, n, Float.floatToIntBits(value));
        }
        
        @Override
        public boolean v() {
            return false;
        }
    }
    
    public static final class d extends e
    {
        public d(final Unsafe unsafe) {
            super(unsafe);
        }
        
        @Override
        public void c(final long srcOffset, final byte[] destBase, final long n, final long bytes) {
            super.a.copyMemory(null, srcOffset, destBase, d12.h + n, bytes);
        }
        
        @Override
        public boolean d(final Object o, final long offset) {
            return super.a.getBoolean(o, offset);
        }
        
        @Override
        public byte e(final long address) {
            return super.a.getByte(address);
        }
        
        @Override
        public byte f(final Object o, final long offset) {
            return super.a.getByte(o, offset);
        }
        
        @Override
        public double g(final Object o, final long offset) {
            return super.a.getDouble(o, offset);
        }
        
        @Override
        public float h(final Object o, final long offset) {
            return super.a.getFloat(o, offset);
        }
        
        @Override
        public long j(final long address) {
            return super.a.getLong(address);
        }
        
        @Override
        public void n(final Object o, final long offset, final boolean x) {
            super.a.putBoolean(o, offset, x);
        }
        
        @Override
        public void o(final Object o, final long offset, final byte x) {
            super.a.putByte(o, offset, x);
        }
        
        @Override
        public void p(final Object o, final long offset, final double x) {
            super.a.putDouble(o, offset, x);
        }
        
        @Override
        public void q(final Object o, final long offset, final float x) {
            super.a.putFloat(o, offset, x);
        }
        
        @Override
        public boolean u() {
            if (!super.u()) {
                return false;
            }
            try {
                final Class<? extends Unsafe> class1 = super.a.getClass();
                final Class<Long> type = Long.TYPE;
                class1.getMethod("getByte", Object.class, type);
                class1.getMethod("putByte", Object.class, type, Byte.TYPE);
                class1.getMethod("getBoolean", Object.class, type);
                class1.getMethod("putBoolean", Object.class, type, Boolean.TYPE);
                class1.getMethod("getFloat", Object.class, type);
                class1.getMethod("putFloat", Object.class, type, Float.TYPE);
                class1.getMethod("getDouble", Object.class, type);
                class1.getMethod("putDouble", Object.class, type, Double.TYPE);
                return true;
            }
            finally {
                final Throwable t;
                K(t);
                return false;
            }
        }
        
        @Override
        public boolean v() {
            if (!super.v()) {
                return false;
            }
            try {
                final Class<? extends Unsafe> class1 = super.a.getClass();
                final Class<Long> type = Long.TYPE;
                class1.getMethod("getByte", type);
                class1.getMethod("putByte", type, Byte.TYPE);
                class1.getMethod("getInt", type);
                class1.getMethod("putInt", type, Integer.TYPE);
                class1.getMethod("getLong", type);
                class1.getMethod("putLong", type, type);
                class1.getMethod("copyMemory", type, type, type);
                class1.getMethod("copyMemory", Object.class, type, Object.class, type, type);
                return true;
            }
            finally {
                final Throwable t;
                K(t);
                return false;
            }
        }
    }
    
    public abstract static class e
    {
        public Unsafe a;
        
        public e(final Unsafe a) {
            this.a = a;
        }
        
        public final int a(final Class arrayClass) {
            return this.a.arrayBaseOffset(arrayClass);
        }
        
        public final int b(final Class arrayClass) {
            return this.a.arrayIndexScale(arrayClass);
        }
        
        public abstract void c(final long p0, final byte[] p1, final long p2, final long p3);
        
        public abstract boolean d(final Object p0, final long p1);
        
        public abstract byte e(final long p0);
        
        public abstract byte f(final Object p0, final long p1);
        
        public abstract double g(final Object p0, final long p1);
        
        public abstract float h(final Object p0, final long p1);
        
        public final int i(final Object o, final long offset) {
            return this.a.getInt(o, offset);
        }
        
        public abstract long j(final long p0);
        
        public final long k(final Object o, final long offset) {
            return this.a.getLong(o, offset);
        }
        
        public final Object l(final Object o, final long offset) {
            return this.a.getObject(o, offset);
        }
        
        public final long m(final Field f) {
            return this.a.objectFieldOffset(f);
        }
        
        public abstract void n(final Object p0, final long p1, final boolean p2);
        
        public abstract void o(final Object p0, final long p1, final byte p2);
        
        public abstract void p(final Object p0, final long p1, final double p2);
        
        public abstract void q(final Object p0, final long p1, final float p2);
        
        public final void r(final Object o, final long offset, final int x) {
            this.a.putInt(o, offset, x);
        }
        
        public final void s(final Object o, final long offset, final long x) {
            this.a.putLong(o, offset, x);
        }
        
        public final void t(final Object o, final long offset, final Object x) {
            this.a.putObject(o, offset, x);
        }
        
        public boolean u() {
            final Unsafe a = this.a;
            if (a == null) {
                return false;
            }
            try {
                final Class<? extends Unsafe> class1 = a.getClass();
                class1.getMethod("objectFieldOffset", Field.class);
                class1.getMethod("arrayBaseOffset", Class.class);
                class1.getMethod("arrayIndexScale", Class.class);
                final Class<Long> type = Long.TYPE;
                class1.getMethod("getInt", Object.class, type);
                class1.getMethod("putInt", Object.class, type, Integer.TYPE);
                class1.getMethod("getLong", Object.class, type);
                class1.getMethod("putLong", Object.class, type, type);
                class1.getMethod("getObject", Object.class, type);
                class1.getMethod("putObject", Object.class, type, Object.class);
                return true;
            }
            finally {
                final Throwable t;
                K(t);
                return false;
            }
        }
        
        public boolean v() {
            final Unsafe a = this.a;
            if (a == null) {
                return false;
            }
            try {
                final Class<? extends Unsafe> class1 = a.getClass();
                class1.getMethod("objectFieldOffset", Field.class);
                class1.getMethod("getLong", Object.class, Long.TYPE);
                return o() != null;
            }
            finally {
                final Throwable t;
                K(t);
                return false;
            }
        }
    }
}

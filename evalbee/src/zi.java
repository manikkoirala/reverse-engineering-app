import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import java.util.Collections;
import java.util.Set;

// 
// Decompiled by Procyon v0.6.0
// 

public final class zi
{
    public final String a;
    public final Set b;
    public final Set c;
    public final int d;
    public final int e;
    public final mj f;
    public final Set g;
    
    public zi(final String a, final Set s, final Set s2, final int d, final int e, final mj f, final Set s3) {
        this.a = a;
        this.b = Collections.unmodifiableSet((Set<?>)s);
        this.c = Collections.unmodifiableSet((Set<?>)s2);
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = Collections.unmodifiableSet((Set<?>)s3);
    }
    
    public static b c(final da1 da1) {
        return new b(da1, new da1[0], null);
    }
    
    public static b d(final da1 da1, final da1... array) {
        return new b(da1, array, null);
    }
    
    public static b e(final Class clazz) {
        return new b(clazz, new Class[0], null);
    }
    
    public static b f(final Class clazz, final Class... array) {
        return new b(clazz, array, null);
    }
    
    public static zi l(final Object o, final Class clazz) {
        return m(clazz).f(new xi(o)).d();
    }
    
    public static b m(final Class clazz) {
        return e(clazz).g();
    }
    
    public static zi s(final Object o, final Class clazz, final Class... array) {
        return f(clazz, array).f(new yi(o)).d();
    }
    
    public Set g() {
        return this.c;
    }
    
    public mj h() {
        return this.f;
    }
    
    public String i() {
        return this.a;
    }
    
    public Set j() {
        return this.b;
    }
    
    public Set k() {
        return this.g;
    }
    
    public boolean n() {
        final int d = this.d;
        boolean b = true;
        if (d != 1) {
            b = false;
        }
        return b;
    }
    
    public boolean o() {
        return this.d == 2;
    }
    
    public boolean p() {
        return this.e == 0;
    }
    
    public zi t(final mj mj) {
        return new zi(this.a, this.b, this.c, this.d, this.e, mj, this.g);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Component<");
        sb.append(Arrays.toString(this.b.toArray()));
        sb.append(">{");
        sb.append(this.d);
        sb.append(", type=");
        sb.append(this.e);
        sb.append(", deps=");
        sb.append(Arrays.toString(this.c.toArray()));
        sb.append("}");
        return sb.toString();
    }
    
    public static class b
    {
        public String a;
        public final Set b;
        public final Set c;
        public int d;
        public int e;
        public mj f;
        public final Set g;
        
        public b(final da1 da1, final da1... elements) {
            this.a = null;
            final HashSet b = new HashSet();
            this.b = b;
            this.c = new HashSet();
            int i = 0;
            this.d = 0;
            this.e = 0;
            this.g = new HashSet();
            j71.c(da1, "Null interface");
            b.add(da1);
            while (i < elements.length) {
                j71.c(elements[i], "Null interface");
                ++i;
            }
            Collections.addAll(this.b, elements);
        }
        
        public b(Class clazz, final Class... array) {
            this.a = null;
            final HashSet b = new HashSet();
            this.b = b;
            this.c = new HashSet();
            int i = 0;
            this.d = 0;
            this.e = 0;
            this.g = new HashSet();
            j71.c(clazz, "Null interface");
            b.add(da1.b(clazz));
            while (i < array.length) {
                clazz = array[i];
                j71.c(clazz, "Null interface");
                this.b.add(da1.b(clazz));
                ++i;
            }
        }
        
        public b b(final os os) {
            j71.c(os, "Null dependency");
            this.j(os.c());
            this.c.add(os);
            return this;
        }
        
        public b c() {
            return this.i(1);
        }
        
        public zi d() {
            j71.d(this.f != null, "Missing required property: factory.");
            return new zi(this.a, new HashSet(this.b), new HashSet(this.c), this.d, this.e, this.f, this.g, null);
        }
        
        public b e() {
            return this.i(2);
        }
        
        public b f(final mj mj) {
            this.f = (mj)j71.c(mj, "Null factory");
            return this;
        }
        
        public final b g() {
            this.e = 1;
            return this;
        }
        
        public b h(final String a) {
            this.a = a;
            return this;
        }
        
        public final b i(final int d) {
            j71.d(this.d == 0, "Instantiation type has already been set.");
            this.d = d;
            return this;
        }
        
        public final void j(final da1 da1) {
            j71.a(this.b.contains(da1) ^ true, "Components are not allowed to depend on interfaces they themselves provide.");
        }
    }
}

import java.lang.reflect.InvocationTargetException;
import android.util.Log;
import android.graphics.Rect;
import android.view.View;
import android.os.Build$VERSION;
import java.lang.reflect.Method;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class u42
{
    public static Method a;
    public static final boolean b;
    
    static {
        b = (Build$VERSION.SDK_INT >= 27);
        try {
            if (!(u42.a = View.class.getDeclaredMethod("computeFitSystemWindows", Rect.class, Rect.class)).isAccessible()) {
                u42.a.setAccessible(true);
            }
        }
        catch (final NoSuchMethodException ex) {
            Log.d("ViewUtils", "Could not find method computeFitSystemWindows. Oh well.");
        }
    }
    
    public static void a(final View obj, final Rect rect, final Rect rect2) {
        final Method a = u42.a;
        if (a != null) {
            try {
                a.invoke(obj, rect, rect2);
            }
            catch (final Exception ex) {
                Log.d("ViewUtils", "Could not invoke computeFitSystemWindows", (Throwable)ex);
            }
        }
    }
    
    public static boolean b(final View view) {
        final int b = o32.B(view);
        boolean b2 = true;
        if (b != 1) {
            b2 = false;
        }
        return b2;
    }
    
    public static void c(final View obj) {
        try {
            final Method method = obj.getClass().getMethod("makeOptionalFitsSystemWindows", (Class<?>[])new Class[0]);
            if (!method.isAccessible()) {
                method.setAccessible(true);
            }
            method.invoke(obj, new Object[0]);
        }
        catch (final IllegalAccessException obj) {
            goto Label_0044;
        }
        catch (final InvocationTargetException ex) {}
        catch (final NoSuchMethodException ex2) {
            Log.d("ViewUtils", "Could not find method makeOptionalFitsSystemWindows. Oh well...");
        }
    }
}

import android.net.Uri;
import android.os.CancellationSignal;
import android.content.res.Resources;
import android.content.Context;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Executable;
import android.util.Log;
import java.lang.reflect.Array;
import android.graphics.Typeface;
import java.util.List;
import java.nio.ByteBuffer;
import java.lang.reflect.Method;
import java.lang.reflect.Constructor;

// 
// Decompiled by Procyon v0.6.0
// 

public class uz1 extends yz1
{
    public static final Class b;
    public static final Constructor c;
    public static final Method d;
    public static final Method e;
    
    static {
        Class<?> forName = null;
        Executable constructor = null;
        Method method = null;
        Method method2 = null;
        Label_0107: {
            try {
                forName = Class.forName("android.graphics.FontFamily");
                constructor = forName.getConstructor((Class[])new Class[0]);
                final Class<Integer> type = Integer.TYPE;
                method = forName.getMethod("addFontWeightStyle", ByteBuffer.class, type, List.class, type, Boolean.TYPE);
                method2 = Typeface.class.getMethod("createFromFamiliesWithDefault", Array.newInstance(forName, 1).getClass());
                break Label_0107;
            }
            catch (final NoSuchMethodException constructor) {}
            catch (final ClassNotFoundException ex) {}
            Log.e("TypefaceCompatApi24Impl", ((NoSuchMethodException)constructor).getClass().getName(), (Throwable)constructor);
            forName = null;
            method2 = null;
            constructor = (method = null);
        }
        c = (Constructor)constructor;
        b = forName;
        d = method;
        e = method2;
    }
    
    public static boolean h(final Object obj, final ByteBuffer byteBuffer, final int i, final int j, final boolean b) {
        try {
            return (boolean)uz1.d.invoke(obj, byteBuffer, i, null, j, b);
        }
        catch (final IllegalAccessException | InvocationTargetException ex) {
            return false;
        }
    }
    
    public static Typeface i(final Object o) {
        try {
            final Object instance = Array.newInstance(uz1.b, 1);
            Array.set(instance, 0, o);
            return (Typeface)uz1.e.invoke(null, instance);
        }
        catch (final IllegalAccessException | InvocationTargetException ex) {
            return null;
        }
    }
    
    public static boolean j() {
        final Method d = uz1.d;
        if (d == null) {
            Log.w("TypefaceCompatApi24Impl", "Unable to collect necessary private methods.Fallback to legacy implementation.");
        }
        return d != null;
    }
    
    public static Object k() {
        try {
            return uz1.c.newInstance(new Object[0]);
        }
        catch (final IllegalAccessException | InstantiationException | InvocationTargetException ex) {
            return null;
        }
    }
    
    @Override
    public Typeface a(final Context context, final j70.c c, final Resources resources, int i) {
        final Object k = k();
        if (k == null) {
            return null;
        }
        final j70.d[] a = c.a();
        int length;
        j70.d d;
        ByteBuffer b;
        for (length = a.length, i = 0; i < length; ++i) {
            d = a[i];
            b = zz1.b(context, resources, d.b());
            if (b == null) {
                return null;
            }
            if (!h(k, b, d.c(), d.e(), d.f())) {
                return null;
            }
        }
        return i(k);
    }
    
    @Override
    public Typeface b(final Context context, final CancellationSignal cancellationSignal, final k70.b[] array, final int n) {
        final Object k = k();
        if (k == null) {
            return null;
        }
        final co1 co1 = new co1();
        for (final k70.b b : array) {
            final Uri d = b.d();
            ByteBuffer f;
            if ((f = (ByteBuffer)co1.get(d)) == null) {
                f = zz1.f(context, cancellationSignal, d);
                co1.put(d, f);
            }
            if (f == null) {
                return null;
            }
            if (!h(k, f, b.c(), b.e(), b.f())) {
                return null;
            }
        }
        final Typeface j = i(k);
        if (j == null) {
            return null;
        }
        return Typeface.create(j, n);
    }
}

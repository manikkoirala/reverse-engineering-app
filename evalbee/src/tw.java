import org.jetbrains.annotations.Nullable;
import android.os.Bundle;
import org.jetbrains.annotations.NotNull;
import android.app.Activity;
import android.app.Application$ActivityLifecycleCallbacks;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class tw implements Application$ActivityLifecycleCallbacks
{
    public void onActivityCreated(@NotNull final Activity activity, @Nullable final Bundle bundle) {
        fg0.e((Object)activity, "activity");
    }
    
    public void onActivityDestroyed(@NotNull final Activity activity) {
        fg0.e((Object)activity, "activity");
    }
    
    public void onActivityPaused(@NotNull final Activity activity) {
        fg0.e((Object)activity, "activity");
    }
    
    public void onActivityResumed(@NotNull final Activity activity) {
        fg0.e((Object)activity, "activity");
    }
    
    public void onActivitySaveInstanceState(@NotNull final Activity activity, @NotNull final Bundle bundle) {
        fg0.e((Object)activity, "activity");
        fg0.e((Object)bundle, "outState");
    }
    
    public void onActivityStarted(@NotNull final Activity activity) {
        fg0.e((Object)activity, "activity");
    }
    
    public void onActivityStopped(@NotNull final Activity activity) {
        fg0.e((Object)activity, "activity");
    }
}

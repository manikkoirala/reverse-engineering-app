import android.os.Parcel;
import android.os.Parcelable$ClassLoaderCreator;
import android.os.Parcelable$Creator;
import android.os.Parcelable;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class e implements Parcelable
{
    public static final Parcelable$Creator<e> CREATOR;
    public static final e EMPTY_STATE;
    private final Parcelable mSuperState;
    
    static {
        EMPTY_STATE = new e() {};
        CREATOR = (Parcelable$Creator)new Parcelable$ClassLoaderCreator() {
            public e a(final Parcel parcel) {
                return this.b(parcel, null);
            }
            
            public e b(final Parcel parcel, final ClassLoader classLoader) {
                if (parcel.readParcelable(classLoader) == null) {
                    return e.EMPTY_STATE;
                }
                throw new IllegalStateException("superState must be null");
            }
            
            public e[] c(final int n) {
                return new e[n];
            }
        };
    }
    
    public e() {
        this.mSuperState = null;
    }
    
    public e(final Parcel parcel, final ClassLoader classLoader) {
        Object mSuperState = parcel.readParcelable(classLoader);
        if (mSuperState == null) {
            mSuperState = e.EMPTY_STATE;
        }
        this.mSuperState = (Parcelable)mSuperState;
    }
    
    public e(Parcelable mSuperState) {
        if (mSuperState != null) {
            if (mSuperState == e.EMPTY_STATE) {
                mSuperState = null;
            }
            this.mSuperState = mSuperState;
            return;
        }
        throw new IllegalArgumentException("superState must not be null");
    }
    
    public int describeContents() {
        return 0;
    }
    
    public final Parcelable getSuperState() {
        return this.mSuperState;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeParcelable(this.mSuperState, n);
    }
}

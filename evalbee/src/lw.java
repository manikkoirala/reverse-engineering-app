import android.view.View;
import java.lang.ref.WeakReference;
import java.lang.ref.Reference;
import android.text.Spanned;
import android.text.Selection;
import android.text.Spannable;
import androidx.emoji2.text.c;
import android.widget.TextView;
import android.text.InputFilter;

// 
// Decompiled by Procyon v0.6.0
// 

public final class lw implements InputFilter
{
    public final TextView a;
    public c.e b;
    
    public lw(final TextView a) {
        this.a = a;
    }
    
    public static void b(final Spannable spannable, final int n, final int n2) {
        if (n >= 0 && n2 >= 0) {
            Selection.setSelection(spannable, n, n2);
        }
        else if (n >= 0) {
            Selection.setSelection(spannable, n);
        }
        else if (n2 >= 0) {
            Selection.setSelection(spannable, n2);
        }
    }
    
    public final c.e a() {
        if (this.b == null) {
            this.b = new a(this.a, this);
        }
        return this.b;
    }
    
    public CharSequence filter(CharSequence subSequence, final int n, final int n2, final Spanned spanned, final int n3, final int n4) {
        if (((View)this.a).isInEditMode()) {
            return subSequence;
        }
        final int d = c.b().d();
        if (d != 0) {
            final int n5 = 1;
            if (d == 1) {
                int n6 = n5;
                if (n4 == 0) {
                    n6 = n5;
                    if (n3 == 0) {
                        n6 = n5;
                        if (((CharSequence)spanned).length() == 0) {
                            n6 = n5;
                            if (subSequence == this.a.getText()) {
                                n6 = 0;
                            }
                        }
                    }
                }
                CharSequence p6 = subSequence;
                if (n6 != 0 && (p6 = subSequence) != null) {
                    if (n != 0 || n2 != subSequence.length()) {
                        subSequence = subSequence.subSequence(n, n2);
                    }
                    p6 = c.b().p(subSequence, 0, subSequence.length());
                }
                return p6;
            }
            if (d != 3) {
                return subSequence;
            }
        }
        c.b().s(this.a());
        return subSequence;
    }
    
    public static class a extends e
    {
        public final Reference a;
        public final Reference b;
        
        public a(final TextView referent, final lw referent2) {
            this.a = new WeakReference(referent);
            this.b = new WeakReference(referent2);
        }
        
        @Override
        public void b() {
            super.b();
            final TextView textView = this.a.get();
            if (!this.c(textView, this.b.get())) {
                return;
            }
            if (((View)textView).isAttachedToWindow()) {
                final CharSequence text = textView.getText();
                final CharSequence o = androidx.emoji2.text.c.b().o(text);
                if (text == o) {
                    return;
                }
                final int selectionStart = Selection.getSelectionStart(o);
                final int selectionEnd = Selection.getSelectionEnd(o);
                textView.setText(o);
                if (o instanceof Spannable) {
                    lw.b((Spannable)o, selectionStart, selectionEnd);
                }
            }
        }
        
        public final boolean c(final TextView textView, final InputFilter inputFilter) {
            if (inputFilter != null) {
                if (textView != null) {
                    final InputFilter[] filters = textView.getFilters();
                    if (filters == null) {
                        return false;
                    }
                    for (int i = 0; i < filters.length; ++i) {
                        if (filters[i] == inputFilter) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
    }
}

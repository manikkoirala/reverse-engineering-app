import android.net.Uri;

// 
// Decompiled by Procyon v0.6.0
// 

public class ef1 extends bf1
{
    public final Uri m;
    
    public ef1(final kq1 kq1, final r10 r10, final Uri m) {
        super(kq1, r10);
        this.m = m;
        super.G("X-Goog-Upload-Protocol", "resumable");
        super.G("X-Goog-Upload-Command", "query");
    }
    
    @Override
    public String e() {
        return "POST";
    }
    
    @Override
    public Uri u() {
        return this.m;
    }
}

import android.os.Handler;
import android.os.Process;
import java.util.concurrent.Future;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ThreadPoolExecutor;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class ce1
{
    public static ThreadPoolExecutor a(final String s, final int n, final int n2) {
        final ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(0, 1, n2, TimeUnit.MILLISECONDS, new LinkedBlockingDeque<Runnable>(), new a(s, n));
        threadPoolExecutor.allowCoreThreadTimeOut(true);
        return threadPoolExecutor;
    }
    
    public static void b(final Executor executor, final Callable callable, final dl dl) {
        executor.execute(new b(se.a(), callable, dl));
    }
    
    public static Object c(final ExecutorService executorService, final Callable callable, final int n) {
        final Future<Object> submit = executorService.submit((Callable<Object>)callable);
        final long n2 = n;
        try {
            return submit.get(n2, TimeUnit.MILLISECONDS);
        }
        catch (final TimeoutException ex) {
            throw new InterruptedException("timeout");
        }
        catch (final InterruptedException ex2) {
            throw ex2;
        }
        catch (final ExecutionException cause) {
            throw new RuntimeException(cause);
        }
    }
    
    public static class a implements ThreadFactory
    {
        public String a;
        public int b;
        
        public a(final String a, final int b) {
            this.a = a;
            this.b = b;
        }
        
        @Override
        public Thread newThread(final Runnable runnable) {
            return new ce1.a.a(runnable, this.a, this.b);
        }
        
        public static class a extends Thread
        {
            public final int a;
            
            public a(final Runnable target, final String name, final int a) {
                super(target, name);
                this.a = a;
            }
            
            @Override
            public void run() {
                Process.setThreadPriority(this.a);
                super.run();
            }
        }
    }
    
    public static class b implements Runnable
    {
        public Callable a;
        public dl b;
        public Handler c;
        
        public b(final Handler c, final Callable a, final dl b) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
        
        @Override
        public void run() {
            Object call;
            try {
                call = this.a.call();
            }
            catch (final Exception ex) {
                call = null;
            }
            this.c.post((Runnable)new Runnable(this, this.b, call) {
                public final dl a;
                public final Object b;
                public final b c;
                
                @Override
                public void run() {
                    this.a.accept(this.b);
                }
            });
        }
    }
}

import java.util.Objects;
import java.lang.reflect.Field;

// 
// Decompiled by Procyon v0.6.0
// 

public final class m00
{
    public final Field a;
    
    public m00(final Field field) {
        Objects.requireNonNull(field);
        this.a = field;
    }
    
    @Override
    public String toString() {
        return this.a.toString();
    }
}

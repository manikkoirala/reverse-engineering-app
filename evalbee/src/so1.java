import com.google.common.collect.Ordering;
import java.util.Comparator;
import java.util.SortedSet;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class so1
{
    public static Comparator a(final SortedSet set) {
        Comparator comparator;
        if ((comparator = set.comparator()) == null) {
            comparator = Ordering.natural();
        }
        return comparator;
    }
    
    public static boolean b(final Comparator comparator, final Iterable iterable) {
        i71.r(comparator);
        i71.r(iterable);
        Comparator comparator2;
        if (iterable instanceof SortedSet) {
            comparator2 = a((SortedSet)iterable);
        }
        else {
            if (!(iterable instanceof ro1)) {
                return false;
            }
            comparator2 = ((ro1)iterable).comparator();
        }
        return comparator.equals(comparator2);
    }
}

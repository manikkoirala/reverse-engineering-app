import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import com.google.android.gms.internal.firebase_auth_api.zzxw;
import java.util.List;
import java.util.ArrayList;
import com.google.android.gms.internal.firebase-auth-api.zzafn;
import org.json.JSONObject;
import android.text.TextUtils;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.logging.Logger;
import android.content.SharedPreferences;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public final class be2
{
    public Context a;
    public String b;
    public SharedPreferences c;
    public Logger d;
    
    public be2(final Context context, final String s) {
        Preconditions.checkNotNull(context);
        this.b = Preconditions.checkNotEmpty(s);
        this.a = context.getApplicationContext();
        this.c = this.a.getSharedPreferences(String.format("com.google.firebase.auth.api.Store.%s", this.b), 0);
        this.d = new Logger("StorageHelpers", new String[0]);
    }
    
    public final r30 a() {
        final String string = this.c.getString("com.google.firebase.auth.FIREBASE_USER", (String)null);
        if (TextUtils.isEmpty((CharSequence)string)) {
            return null;
        }
        try {
            final JSONObject jsonObject = new JSONObject(string);
            if (jsonObject.has("type") && "com.google.firebase.auth.internal.DefaultFirebaseUser".equalsIgnoreCase(jsonObject.optString("type"))) {
                return this.c(jsonObject);
            }
            return null;
        }
        catch (final Exception ex) {
            return null;
        }
    }
    
    public final zzafn b(final r30 r30) {
        Preconditions.checkNotNull(r30);
        final String string = this.c.getString(String.format("com.google.firebase.auth.GET_TOKEN_RESPONSE.%s", r30.O()), (String)null);
        if (string != null) {
            return com.google.android.gms.internal.firebase_auth_api.zzafn.zzb(string);
        }
        return null;
    }
    
    public final na2 c(JSONObject zzxw) {
        try {
            final String string = ((JSONObject)zzxw).getString("cachedTokenState");
            final String string2 = ((JSONObject)zzxw).getString("applicationName");
            final boolean boolean1 = ((JSONObject)zzxw).getBoolean("anonymous");
            String s = "2";
            final String string3 = ((JSONObject)zzxw).getString("version");
            if (string3 != null) {
                s = string3;
            }
            final JSONArray jsonArray = ((JSONObject)zzxw).getJSONArray("userInfos");
            final int length = jsonArray.length();
            if (length == 0) {
                return null;
            }
            final ArrayList list = new ArrayList(length);
            for (int i = 0; i < length; ++i) {
                list.add((Object)tj2.H(jsonArray.getString(i)));
            }
            final na2 na2 = new na2(r10.n(string2), list);
            if (!TextUtils.isEmpty((CharSequence)string)) {
                na2.o0(com.google.android.gms.internal.firebase_auth_api.zzafn.zzb(string));
            }
            if (!boolean1) {
                na2.p0();
            }
            na2.t0(s);
            if (((JSONObject)zzxw).has("userMetadata")) {
                final ra2 e = ra2.e(((JSONObject)zzxw).getJSONObject("userMetadata"));
                if (e != null) {
                    na2.u0(e);
                }
            }
            if (((JSONObject)zzxw).has("userMultiFactorInfo")) {
                final JSONArray jsonArray2 = ((JSONObject)zzxw).getJSONArray("userMultiFactorInfo");
                if (jsonArray2 != null) {
                    final ArrayList list2 = new ArrayList();
                    for (int j = 0; j < jsonArray2.length(); ++j) {
                        zzxw = (zzxw)new JSONObject(jsonArray2.getString(j));
                        final String optString = ((JSONObject)zzxw).optString("factorIdKey");
                        if ("phone".equals(optString)) {
                            zzxw = (zzxw)i51.J((JSONObject)zzxw);
                        }
                        else if (optString == "totp" || (optString != null && optString.equals("totp"))) {
                            zzxw = (zzxw)fy1.J((JSONObject)zzxw);
                        }
                        else {
                            zzxw = null;
                        }
                        list2.add(zzxw);
                    }
                    na2.q0(list2);
                }
            }
            return na2;
        }
        catch (final zzxw zzxw) {}
        catch (final IllegalArgumentException zzxw) {}
        catch (final ArrayIndexOutOfBoundsException zzxw) {}
        catch (final JSONException ex) {}
        this.d.wtf((Throwable)zzxw);
        return null;
    }
    
    public final void d(final r30 r30, final zzafn zzafn) {
        Preconditions.checkNotNull(r30);
        Preconditions.checkNotNull(zzafn);
        this.c.edit().putString(String.format("com.google.firebase.auth.GET_TOKEN_RESPONSE.%s", r30.O()), ((com.google.android.gms.internal.firebase_auth_api.zzafn)zzafn).zzf()).apply();
    }
    
    public final void e(final String s) {
        this.c.edit().remove(s).apply();
    }
    
    public final void f(final r30 r30) {
        Preconditions.checkNotNull(r30);
        final String g = this.g(r30);
        if (!TextUtils.isEmpty((CharSequence)g)) {
            this.c.edit().putString("com.google.firebase.auth.FIREBASE_USER", g).apply();
        }
    }
    
    public final String g(final r30 r30) {
        final JSONObject jsonObject = new JSONObject();
        if (na2.class.isAssignableFrom(r30.getClass())) {
            final na2 na2 = (na2)r30;
            try {
                jsonObject.put("cachedTokenState", (Object)na2.zze());
                jsonObject.put("applicationName", (Object)na2.i0().o());
                jsonObject.put("type", (Object)"com.google.firebase.auth.internal.DefaultFirebaseUser");
                if (na2.z0() != null) {
                    final JSONArray jsonArray = new JSONArray();
                    final List z0 = na2.z0();
                    int size = z0.size();
                    if (z0.size() > 30) {
                        this.d.w("Provider user info list size larger than max size, truncating list to %d. Actual list size: %d", 30, z0.size());
                        size = 30;
                    }
                    int n = 0;
                    int n2 = 0;
                    int n3;
                    while (true) {
                        n3 = n2;
                        if (n >= size) {
                            break;
                        }
                        final tj2 tj2 = z0.get(n);
                        if (tj2.b().equals("firebase")) {
                            n2 = 1;
                        }
                        if (n == size - 1 && (n3 = n2) == 0) {
                            break;
                        }
                        jsonArray.put((Object)tj2.zzb());
                        ++n;
                    }
                    if (n3 == 0) {
                        int n4 = size - 1;
                        int n5;
                        while (true) {
                            n5 = n3;
                            if (n4 >= z0.size()) {
                                break;
                            }
                            n5 = n3;
                            if (n4 < 0) {
                                break;
                            }
                            final tj2 tj3 = z0.get(n4);
                            if (tj3.b().equals("firebase")) {
                                jsonArray.put((Object)tj3.zzb());
                                n5 = 1;
                                break;
                            }
                            if (n4 == z0.size() - 1) {
                                jsonArray.put((Object)tj3.zzb());
                            }
                            ++n4;
                        }
                        if (n5 == 0) {
                            this.d.w("Malformed user object! No Firebase Auth provider id found. Provider user info list size: %d, trimmed size: %d", z0.size(), size);
                            if (z0.size() < 5) {
                                final StringBuilder sb = new StringBuilder("Provider user info list:\n");
                                final Iterator iterator = z0.iterator();
                                while (iterator.hasNext()) {
                                    sb.append(String.format("Provider - %s\n", ((tj2)iterator.next()).b()));
                                }
                                this.d.w(sb.toString(), new Object[0]);
                            }
                        }
                    }
                    jsonObject.put("userInfos", (Object)jsonArray);
                }
                jsonObject.put("anonymous", na2.R());
                jsonObject.put("version", (Object)"2");
                if (na2.E() != null) {
                    jsonObject.put("userMetadata", (Object)((ra2)na2.E()).d());
                }
                final List a = na2.H().a();
                if (a != null && !a.isEmpty()) {
                    final JSONArray jsonArray2 = new JSONArray();
                    for (int i = 0; i < a.size(); ++i) {
                        jsonArray2.put((Object)((gx0)a.get(i)).toJson());
                    }
                    jsonObject.put("userMultiFactorInfo", (Object)jsonArray2);
                }
                return jsonObject.toString();
            }
            catch (final Exception ex) {
                this.d.wtf("Failed to turn object into JSON", ex, new Object[0]);
                throw new zzxw((Throwable)ex);
            }
        }
        return null;
    }
}

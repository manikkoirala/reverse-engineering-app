import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import org.json.JSONException;
import com.google.android.gms.internal.firebase_auth_api.zzxw;
import android.util.Log;
import org.json.JSONObject;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.firebase-auth-api.zzagr;
import android.os.Parcelable$Creator;

// 
// Decompiled by Procyon v0.6.0
// 

public class fy1 extends gx0
{
    public static final Parcelable$Creator<fy1> CREATOR;
    public final String a;
    public final String b;
    public final long c;
    public final zzagr d;
    
    static {
        CREATOR = (Parcelable$Creator)new ac2();
    }
    
    public fy1(final String s, final String b, final long c, final zzagr zzagr) {
        this.a = Preconditions.checkNotEmpty(s);
        this.b = b;
        this.c = c;
        this.d = (zzagr)Preconditions.checkNotNull((com.google.android.gms.internal.firebase_auth_api.zzagr)zzagr, "totpInfo cannot be null.");
    }
    
    public static fy1 J(final JSONObject jsonObject) {
        if (!jsonObject.has("enrollmentTimestamp")) {
            throw new IllegalArgumentException("An enrollment timestamp in seconds of UTC time since Unix epoch is required to build a TotpMultiFactorInfo instance.");
        }
        final long optLong = jsonObject.optLong("enrollmentTimestamp");
        if (jsonObject.opt("totpInfo") != null) {
            return new fy1(jsonObject.optString("uid"), jsonObject.optString("displayName"), optLong, (zzagr)new com.google.android.gms.internal.firebase_auth_api.zzagr());
        }
        throw new IllegalArgumentException("A totpInfo is required to build a TotpMultiFactorInfo instance.");
    }
    
    @Override
    public String E() {
        return "totp";
    }
    
    @Override
    public String H() {
        return this.a;
    }
    
    @Override
    public String getDisplayName() {
        return this.b;
    }
    
    @Override
    public long i() {
        return this.c;
    }
    
    @Override
    public JSONObject toJson() {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.putOpt("factorIdKey", (Object)"totp");
            jsonObject.putOpt("uid", (Object)this.a);
            jsonObject.putOpt("displayName", (Object)this.b);
            jsonObject.putOpt("enrollmentTimestamp", (Object)this.c);
            jsonObject.putOpt("totpInfo", (Object)this.d);
            return jsonObject;
        }
        catch (final JSONException ex) {
            Log.d("TotpMultiFactorInfo", "Failed to jsonify this object");
            throw new zzxw((Throwable)ex);
        }
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.H(), false);
        SafeParcelWriter.writeString(parcel, 2, this.getDisplayName(), false);
        SafeParcelWriter.writeLong(parcel, 3, this.i());
        SafeParcelWriter.writeParcelable(parcel, 4, (Parcelable)this.d, n, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

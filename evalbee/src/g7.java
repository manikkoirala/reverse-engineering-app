import android.graphics.drawable.Drawable;
import android.content.res.ColorStateList;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class g7
{
    public static ColorStateList a(final Context context, final int n) {
        return sl.getColorStateList(context, n);
    }
    
    public static Drawable b(final Context context, final int n) {
        return je1.g().i(context, n);
    }
}

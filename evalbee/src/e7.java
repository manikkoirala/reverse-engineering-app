import android.graphics.Bitmap;
import android.widget.ProgressBar;
import android.view.View;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.RatingBar;

// 
// Decompiled by Procyon v0.6.0
// 

public class e7 extends RatingBar
{
    public final c7 a;
    
    public e7(final Context context, final AttributeSet set) {
        this(context, set, sa1.H);
    }
    
    public e7(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        pv1.a((View)this, ((View)this).getContext());
        (this.a = new c7((ProgressBar)this)).c(set, n);
    }
    
    public void onMeasure(final int n, final int n2) {
        synchronized (this) {
            super.onMeasure(n, n2);
            final Bitmap b = this.a.b();
            if (b != null) {
                ((View)this).setMeasuredDimension(View.resolveSizeAndState(b.getWidth() * this.getNumStars(), n, 0), ((View)this).getMeasuredHeight());
            }
        }
    }
}

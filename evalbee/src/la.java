// 
// Decompiled by Procyon v0.6.0
// 

public final class la extends a
{
    public final String a;
    public final String b;
    
    public la(final String a, final String b) {
        if (a != null) {
            this.a = a;
            this.b = b;
            return;
        }
        throw new NullPointerException("Null crashlyticsInstallId");
    }
    
    @Override
    public String c() {
        return this.a;
    }
    
    @Override
    public String d() {
        return this.b;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof a) {
            final a a = (a)o;
            if (this.a.equals(a.c())) {
                final String b2 = this.b;
                final String d = a.d();
                if (b2 == null) {
                    if (d == null) {
                        return b;
                    }
                }
                else if (b2.equals(d)) {
                    return b;
                }
            }
            b = false;
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.a.hashCode();
        final String b = this.b;
        int hashCode2;
        if (b == null) {
            hashCode2 = 0;
        }
        else {
            hashCode2 = b.hashCode();
        }
        return (hashCode ^ 0xF4243) * 1000003 ^ hashCode2;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("InstallIds{crashlyticsInstallId=");
        sb.append(this.a);
        sb.append(", firebaseInstallationId=");
        sb.append(this.b);
        sb.append("}");
        return sb.toString();
    }
}

import com.google.firebase.firestore.core.r;
import com.google.firebase.firestore.core.Query;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ka1
{
    public final Query a;
    public final int b;
    public final r c;
    
    public ka1(final Query a, final int b, final r c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public Query a() {
        return this.a;
    }
    
    public int b() {
        return this.b;
    }
    
    public r c() {
        return this.c;
    }
}

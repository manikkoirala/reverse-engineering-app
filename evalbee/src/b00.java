import java.util.Map;
import java.util.HashMap;

// 
// Decompiled by Procyon v0.6.0
// 

public class b00 extends si1
{
    public final HashMap e;
    
    public b00() {
        this.e = new HashMap();
    }
    
    @Override
    public c b(final Object key) {
        return this.e.get(key);
    }
    
    public boolean contains(final Object key) {
        return this.e.containsKey(key);
    }
    
    @Override
    public Object l(final Object key, final Object o) {
        final c b = this.b(key);
        if (b != null) {
            return b.b;
        }
        this.e.put(key, this.i(key, o));
        return null;
    }
    
    @Override
    public Object m(final Object key) {
        final Object m = super.m(key);
        this.e.remove(key);
        return m;
    }
    
    public Map.Entry n(final Object key) {
        if (this.contains(key)) {
            return this.e.get(key).d;
        }
        return null;
    }
}

import com.google.firebase.auth.FirebaseAuth;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ne2
{
    public static final ne2 c;
    public final ed2 a;
    public final xb2 b;
    
    static {
        c = new ne2();
    }
    
    public ne2() {
        this(ed2.g(), xb2.a());
    }
    
    public ne2(final ed2 a, final xb2 b) {
        this.a = a;
        this.b = b;
    }
    
    public static ne2 c() {
        return ne2.c;
    }
    
    public final void a(final Context context) {
        this.a.a(context);
    }
    
    public final void b(final FirebaseAuth firebaseAuth) {
        this.a.f(firebaseAuth);
    }
}

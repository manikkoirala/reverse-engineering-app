import java.nio.charset.StandardCharsets;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.DeflaterOutputStream;
import java.io.ByteArrayOutputStream;
import java.util.zip.Deflater;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class ax
{
    public static int a(final int n) {
        return (n + 8 - 1 & 0xFFFFFFF8) / 8;
    }
    
    public static byte[] b(final byte[] b) {
        final Deflater def = new Deflater(1);
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            final DeflaterOutputStream deflaterOutputStream = new DeflaterOutputStream(out, def);
            try {
                deflaterOutputStream.write(b);
                deflaterOutputStream.close();
                def.end();
                return out.toByteArray();
            }
            finally {
                try {
                    deflaterOutputStream.close();
                }
                finally {
                    final Throwable exception;
                    ((Throwable)(Object)b).addSuppressed(exception);
                }
            }
        }
        finally {
            def.end();
        }
    }
    
    public static RuntimeException c(final String s) {
        return new IllegalStateException(s);
    }
    
    public static byte[] d(final InputStream inputStream, final int i) {
        final byte[] b = new byte[i];
        int read;
        for (int j = 0; j < i; j += read) {
            read = inputStream.read(b, j, i - j);
            if (read < 0) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Not enough bytes to read: ");
                sb.append(i);
                throw c(sb.toString());
            }
        }
        return b;
    }
    
    public static byte[] e(final InputStream inputStream, final int n, final int n2) {
        final Inflater inflater = new Inflater();
        try {
            final byte[] output = new byte[n2];
            final byte[] array = new byte[2048];
            int i = 0;
            int off = 0;
            while (!inflater.finished() && !inflater.needsDictionary() && i < n) {
                final int read = inputStream.read(array);
                if (read >= 0) {
                    inflater.setInput(array, 0, read);
                    try {
                        off += inflater.inflate(output, off, n2 - off);
                        i += read;
                        continue;
                    }
                    catch (final DataFormatException ex) {
                        throw c(ex.getMessage());
                    }
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid zip data. Stream ended after $totalBytesRead bytes. Expected ");
                sb.append(n);
                sb.append(" bytes");
                throw c(sb.toString());
            }
            if (i != n) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Didn't read enough bytes during decompression. expected=");
                sb2.append(n);
                sb2.append(" actual=");
                sb2.append(i);
                throw c(sb2.toString());
            }
            if (inflater.finished()) {
                return output;
            }
            throw c("Inflater did not finish");
        }
        finally {
            inflater.end();
        }
    }
    
    public static String f(final InputStream inputStream, final int n) {
        return new String(d(inputStream, n), StandardCharsets.UTF_8);
    }
    
    public static long g(final InputStream inputStream, final int n) {
        final byte[] d = d(inputStream, n);
        long n2 = 0L;
        for (int i = 0; i < n; ++i) {
            n2 += (long)(d[i] & 0xFF) << i * 8;
        }
        return n2;
    }
    
    public static int h(final InputStream inputStream) {
        return (int)g(inputStream, 2);
    }
    
    public static long i(final InputStream inputStream) {
        return g(inputStream, 4);
    }
    
    public static int j(final InputStream inputStream) {
        return (int)g(inputStream, 1);
    }
    
    public static int k(final String s) {
        return s.getBytes(StandardCharsets.UTF_8).length;
    }
    
    public static void l(final InputStream inputStream, final OutputStream outputStream) {
        final byte[] array = new byte[512];
        while (true) {
            final int read = inputStream.read(array);
            if (read <= 0) {
                break;
            }
            outputStream.write(array, 0, read);
        }
    }
    
    public static void m(final OutputStream outputStream, byte[] b) {
        q(outputStream, b.length);
        b = b(b);
        q(outputStream, b.length);
        outputStream.write(b);
    }
    
    public static void n(final OutputStream outputStream, final String s) {
        outputStream.write(s.getBytes(StandardCharsets.UTF_8));
    }
    
    public static void o(final OutputStream outputStream, final long n, final int n2) {
        final byte[] b = new byte[n2];
        for (int i = 0; i < n2; ++i) {
            b[i] = (byte)(n >> i * 8 & 0xFFL);
        }
        outputStream.write(b);
    }
    
    public static void p(final OutputStream outputStream, final int n) {
        o(outputStream, n, 2);
    }
    
    public static void q(final OutputStream outputStream, final long n) {
        o(outputStream, n, 4);
    }
    
    public static void r(final OutputStream outputStream, final int n) {
        o(outputStream, n, 1);
    }
}

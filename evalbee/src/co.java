import java.util.concurrent.atomic.AtomicBoolean;

// 
// Decompiled by Procyon v0.6.0
// 

public class co implements UncaughtExceptionHandler
{
    public final a a;
    public final zm1 b;
    public final UncaughtExceptionHandler c;
    public final dn d;
    public final AtomicBoolean e;
    
    public co(final a a, final zm1 b, final UncaughtExceptionHandler c, final dn d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.e = new AtomicBoolean(false);
        this.d = d;
    }
    
    public boolean a() {
        return this.e.get();
    }
    
    public final boolean b(final Thread thread, final Throwable t) {
        if (thread == null) {
            zl0.f().d("Crashlytics will not record uncaught exception; null thread");
            return false;
        }
        if (t == null) {
            zl0.f().d("Crashlytics will not record uncaught exception; null throwable");
            return false;
        }
        if (this.d.c()) {
            zl0.f().b("Crashlytics will not record uncaught exception; native crash exists for session.");
            return false;
        }
        return true;
    }
    
    @Override
    public void uncaughtException(final Thread thread, final Throwable t) {
        this.e.set(true);
        try {
            try {
                if (this.b(thread, t)) {
                    this.a.a(this.b, thread, t);
                }
                zl0.f().b("Uncaught exception will not be recorded by Crashlytics.");
            }
            finally {}
        }
        catch (final Exception ex) {
            zl0.f().e("An error occurred in the uncaught exception handler", ex);
        }
        zl0.f().b("Completed exception processing. Invoking default exception handler.");
        this.c.uncaughtException(thread, t);
        this.e.set(false);
        return;
        zl0.f().b("Completed exception processing. Invoking default exception handler.");
        this.c.uncaughtException(thread, t);
        this.e.set(false);
    }
    
    public interface a
    {
        void a(final zm1 p0, final Thread p1, final Throwable p2);
    }
}

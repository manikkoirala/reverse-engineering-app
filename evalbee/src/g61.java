import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.FirebaseAuth;
import java.util.HashMap;
import java.util.Map;
import java.nio.charset.StandardCharsets;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.d;
import com.ekodroid.omrevaluator.serializable.ResultData;

// 
// Decompiled by Procyon v0.6.0
// 

public class g61
{
    public ee1 a;
    public zg b;
    public ResultData c;
    public String d;
    public String e;
    
    public g61(final ResultData c, final ee1 a, final String e, final zg b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("https://");
        sb.append(a91.w());
        sb.append(":8759/excel_email3");
        this.d = sb.toString();
        this.a = a;
        this.b = b;
        this.c = c;
        this.e = e;
        this.f();
    }
    
    public static /* synthetic */ ResultData c(final g61 g61) {
        return g61.c;
    }
    
    public final void d(final String s) {
        final Request request = new Request(this, 1, this.d, new d.a(this) {
            public final g61 a;
            
            @Override
            public void a(final VolleyError volleyError) {
                this.a.e(false, 400, null);
            }
        }, s) {
            public final String t;
            public final g61 v;
            
            @Override
            public d G(final yy0 yy0) {
                return d.c(yy0.b, id0.e(yy0));
            }
            
            public void Q(final byte[] bytes) {
                if (bytes.length > 5) {
                    this.v.e(true, 200, bytes);
                }
                else {
                    this.v.e(true, 218, Integer.parseInt(new String(bytes, StandardCharsets.UTF_8)));
                }
            }
            
            @Override
            public byte[] k() {
                try {
                    return new gc0().s(g61.c(this.v)).getBytes("UTF-8");
                }
                catch (final Exception ex) {
                    ex.printStackTrace();
                    return null;
                }
            }
            
            @Override
            public String l() {
                return "application/json";
            }
            
            @Override
            public Map o() {
                final HashMap hashMap = new HashMap();
                hashMap.put("x-user-token", this.t);
                return hashMap;
            }
        };
        request.L(new wq(100000, 0, 1.0f));
        this.a.a(request);
    }
    
    public final void e(final boolean b, final int n, final Object o) {
        final zg b2 = this.b;
        if (b2 != null) {
            b2.a(b, n, o);
        }
    }
    
    public final void f() {
        FirebaseAuth.getInstance().e().i(false).addOnCompleteListener((OnCompleteListener)new OnCompleteListener(this) {
            public final g61 a;
            
            public void onComplete(final Task task) {
                if (task.isSuccessful()) {
                    this.a.d(((ya0)task.getResult()).c());
                }
                else {
                    this.a.e(false, 400, null);
                }
            }
        });
    }
    
    public void g() {
        this.b = null;
    }
}

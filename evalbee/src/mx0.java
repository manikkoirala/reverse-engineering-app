import android.os.RemoteException;
import android.util.Log;
import java.util.Set;
import java.util.Collection;
import android.os.IBinder;
import android.content.ComponentName;
import java.util.Arrays;
import android.content.Intent;
import android.content.ServiceConnection;
import java.util.concurrent.atomic.AtomicBoolean;
import android.content.Context;
import java.util.concurrent.Executor;

// 
// Decompiled by Procyon v0.6.0
// 

public final class mx0
{
    public final String a;
    public final lg0 b;
    public final Executor c;
    public final Context d;
    public int e;
    public lg0.c f;
    public wd0 g;
    public final vd0 h;
    public final AtomicBoolean i;
    public final ServiceConnection j;
    public final Runnable k;
    public final Runnable l;
    
    public mx0(Context applicationContext, final String a, final Intent intent, final lg0 b, final Executor c) {
        fg0.e((Object)applicationContext, "context");
        fg0.e((Object)a, "name");
        fg0.e((Object)intent, "serviceIntent");
        fg0.e((Object)b, "invalidationTracker");
        fg0.e((Object)c, "executor");
        this.a = a;
        this.b = b;
        this.c = c;
        applicationContext = applicationContext.getApplicationContext();
        this.d = applicationContext;
        this.h = new vd0.a(this) {
            public final mx0 a;
            
            public static final void r(final mx0 mx0, final String[] original) {
                fg0.e((Object)mx0, "this$0");
                fg0.e((Object)original, "$tables");
                mx0.e().j((String[])Arrays.copyOf(original, original.length));
            }
            
            public void a(final String[] array) {
                fg0.e((Object)array, "tables");
                this.a.d().execute(new nx0(this.a, array));
            }
        };
        this.i = new AtomicBoolean(false);
        final ServiceConnection j = (ServiceConnection)new ServiceConnection(this) {
            public final mx0 a;
            
            public void onServiceConnected(final ComponentName componentName, final IBinder binder) {
                fg0.e((Object)componentName, "name");
                fg0.e((Object)binder, "service");
                this.a.m(wd0.a.p(binder));
                this.a.d().execute(this.a.i());
            }
            
            public void onServiceDisconnected(final ComponentName componentName) {
                fg0.e((Object)componentName, "name");
                this.a.d().execute(this.a.g());
                this.a.m(null);
            }
        };
        this.j = (ServiceConnection)j;
        this.k = new kx0(this);
        this.l = new lx0(this);
        final String[] array = b.h().keySet().toArray(new String[0]);
        fg0.c((Object)array, "null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
        this.l(new lg0.c(this, (String[])array) {
            public final mx0 b;
            
            @Override
            public boolean b() {
                return true;
            }
            
            @Override
            public void c(final Set set) {
                fg0.e((Object)set, "tables");
                if (this.b.j().get()) {
                    return;
                }
                try {
                    final wd0 h = this.b.h();
                    if (h != null) {
                        final int c = this.b.c();
                        final String[] array = set.toArray(new String[0]);
                        fg0.c((Object)array, "null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
                        h.k(c, array);
                    }
                }
                catch (final RemoteException ex) {
                    Log.w("ROOM", "Cannot broadcast invalidation", (Throwable)ex);
                }
            }
        });
        applicationContext.bindService(intent, (ServiceConnection)j, 1);
    }
    
    public static final void k(final mx0 mx0) {
        fg0.e((Object)mx0, "this$0");
        mx0.b.m(mx0.f());
    }
    
    public static final void n(final mx0 mx0) {
        fg0.e((Object)mx0, "this$0");
        try {
            final wd0 g = mx0.g;
            if (g != null) {
                mx0.e = g.i(mx0.h, mx0.a);
                mx0.b.b(mx0.f());
            }
        }
        catch (final RemoteException ex) {
            Log.w("ROOM", "Cannot register multi-instance invalidation callback", (Throwable)ex);
        }
    }
    
    public final int c() {
        return this.e;
    }
    
    public final Executor d() {
        return this.c;
    }
    
    public final lg0 e() {
        return this.b;
    }
    
    public final lg0.c f() {
        final lg0.c f = this.f;
        if (f != null) {
            return f;
        }
        fg0.t("observer");
        return null;
    }
    
    public final Runnable g() {
        return this.l;
    }
    
    public final wd0 h() {
        return this.g;
    }
    
    public final Runnable i() {
        return this.k;
    }
    
    public final AtomicBoolean j() {
        return this.i;
    }
    
    public final void l(final lg0.c f) {
        fg0.e((Object)f, "<set-?>");
        this.f = f;
    }
    
    public final void m(final wd0 g) {
        this.g = g;
    }
}

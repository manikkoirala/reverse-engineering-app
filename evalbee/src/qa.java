// 
// Decompiled by Procyon v0.6.0
// 

public final class qa extends tp1
{
    public final long a;
    public final long b;
    public final long c;
    
    public qa(final long a, final long b, final long c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    @Override
    public long b() {
        return this.b;
    }
    
    @Override
    public long c() {
        return this.a;
    }
    
    @Override
    public long d() {
        return this.c;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof tp1) {
            final tp1 tp1 = (tp1)o;
            if (this.a != tp1.c() || this.b != tp1.b() || this.c != tp1.d()) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        final long a = this.a;
        final int n = (int)(a ^ a >>> 32);
        final long b = this.b;
        final int n2 = (int)(b ^ b >>> 32);
        final long c = this.c;
        return ((n ^ 0xF4243) * 1000003 ^ n2) * 1000003 ^ (int)(c >>> 32 ^ c);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("StartupTime{epochMillis=");
        sb.append(this.a);
        sb.append(", elapsedRealtime=");
        sb.append(this.b);
        sb.append(", uptimeMillis=");
        sb.append(this.c);
        sb.append("}");
        return sb.toString();
    }
}

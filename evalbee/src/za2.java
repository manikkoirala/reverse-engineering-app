import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import java.util.Iterator;
import java.util.ArrayList;
import com.google.firebase.auth.FirebaseAuth;
import com.google.android.gms.internal.firebase-auth-api.zzyk;
import com.google.android.gms.common.internal.Preconditions;
import java.util.List;
import android.os.Parcelable$Creator;

// 
// Decompiled by Procyon v0.6.0
// 

public final class za2 extends hx0
{
    public static final Parcelable$Creator<za2> CREATOR;
    public final List a;
    public final kb2 b;
    public final String c;
    public final lf2 d;
    public final na2 e;
    public final List f;
    
    static {
        CREATOR = (Parcelable$Creator)new gb2();
    }
    
    public za2(final List list, final kb2 kb2, final String s, final lf2 d, final na2 e, final List list2) {
        this.a = Preconditions.checkNotNull(list);
        this.b = Preconditions.checkNotNull(kb2);
        this.c = Preconditions.checkNotEmpty(s);
        this.d = d;
        this.e = e;
        this.f = Preconditions.checkNotNull(list2);
    }
    
    public static za2 E(final zzyk zzyk, final FirebaseAuth firebaseAuth, final r30 r30) {
        final List zzc = ((com.google.android.gms.internal.firebase_auth_api.zzyk)zzyk).zzc();
        final ArrayList list = new ArrayList();
        for (final gx0 gx0 : zzc) {
            if (gx0 instanceof i51) {
                list.add(gx0);
            }
        }
        final List zzc2 = ((com.google.android.gms.internal.firebase_auth_api.zzyk)zzyk).zzc();
        final ArrayList list2 = new ArrayList();
        for (final gx0 gx2 : zzc2) {
            if (gx2 instanceof fy1) {
                list2.add(gx2);
            }
        }
        return new za2(list, kb2.i(((com.google.android.gms.internal.firebase_auth_api.zzyk)zzyk).zzc(), ((com.google.android.gms.internal.firebase_auth_api.zzyk)zzyk).zzb()), firebaseAuth.d().o(), ((com.google.android.gms.internal.firebase_auth_api.zzyk)zzyk).zza(), (na2)r30, list2);
    }
    
    @Override
    public final ix0 i() {
        return this.b;
    }
    
    public final void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeTypedList(parcel, 1, (List<Parcelable>)this.a, false);
        SafeParcelWriter.writeParcelable(parcel, 2, (Parcelable)this.i(), n, false);
        SafeParcelWriter.writeString(parcel, 3, this.c, false);
        SafeParcelWriter.writeParcelable(parcel, 4, (Parcelable)this.d, n, false);
        SafeParcelWriter.writeParcelable(parcel, 5, (Parcelable)this.e, n, false);
        SafeParcelWriter.writeTypedList(parcel, 6, (List<Parcelable>)this.f, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

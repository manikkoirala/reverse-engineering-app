// 
// Decompiled by Procyon v0.6.0
// 

public final class am1
{
    public final String a;
    public final String b;
    public final int c;
    public final long d;
    public final gp e;
    public final String f;
    
    public am1(final String a, final String b, final int c, final long d, final gp e, final String f) {
        fg0.e((Object)a, "sessionId");
        fg0.e((Object)b, "firstSessionId");
        fg0.e((Object)e, "dataCollectionStatus");
        fg0.e((Object)f, "firebaseInstallationId");
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
    }
    
    public final gp a() {
        return this.e;
    }
    
    public final long b() {
        return this.d;
    }
    
    public final String c() {
        return this.f;
    }
    
    public final String d() {
        return this.b;
    }
    
    public final String e() {
        return this.a;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof am1)) {
            return false;
        }
        final am1 am1 = (am1)o;
        return fg0.a((Object)this.a, (Object)am1.a) && fg0.a((Object)this.b, (Object)am1.b) && this.c == am1.c && this.d == am1.d && fg0.a((Object)this.e, (Object)am1.e) && fg0.a((Object)this.f, (Object)am1.f);
    }
    
    public final int f() {
        return this.c;
    }
    
    @Override
    public int hashCode() {
        return ((((this.a.hashCode() * 31 + this.b.hashCode()) * 31 + Integer.hashCode(this.c)) * 31 + Long.hashCode(this.d)) * 31 + this.e.hashCode()) * 31 + this.f.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SessionInfo(sessionId=");
        sb.append(this.a);
        sb.append(", firstSessionId=");
        sb.append(this.b);
        sb.append(", sessionIndex=");
        sb.append(this.c);
        sb.append(", eventTimestampUs=");
        sb.append(this.d);
        sb.append(", dataCollectionStatus=");
        sb.append(this.e);
        sb.append(", firebaseInstallationId=");
        sb.append(this.f);
        sb.append(')');
        return sb.toString();
    }
}

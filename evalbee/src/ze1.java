import com.ekodroid.omrevaluator.serializable.ReportData;
import android.content.pm.PackageManager$NameNotFoundException;
import android.os.AsyncTask;
import android.content.SharedPreferences;
import java.io.PrintStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.FileInputStream;
import java.util.Iterator;
import java.io.File;
import com.ekodroid.omrevaluator.templateui.scanner.b;
import android.graphics.BitmapFactory;
import com.ekodroid.omrevaluator.database.SheetImageModel;
import com.ekodroid.omrevaluator.templateui.scanner.SheetDimension;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import java.io.InputStream;
import org.apache.pdfbox.pdmodel.graphics.image.JPEGFactory;
import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import android.graphics.Bitmap$CompressFormat;
import java.io.ByteArrayOutputStream;
import android.graphics.Bitmap;
import com.ekodroid.omrevaluator.database.repositories.ResultRepository;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import org.apache.pdfbox.pdmodel.PDDocument;
import android.os.Environment;
import com.ekodroid.omrevaluator.templateui.models.TemplateParams2;
import android.util.Log;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import com.ekodroid.omrevaluator.templateui.models.Subject2;
import com.ekodroid.omrevaluator.templateui.models.LabelProfile;
import java.util.ArrayList;
import com.ekodroid.omrevaluator.templateui.models.AnswerOptionKey;
import com.ekodroid.omrevaluator.templateui.scanner.AnswerValue;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import com.ekodroid.omrevaluator.templateui.scanner.ResultItem;
import android.os.Bundle;
import com.google.firebase.analytics.FirebaseAnalytics;
import android.provider.Settings$Secure;
import android.content.Context;
import com.ekodroid.omrevaluator.exams.models.MessageReport;

// 
// Decompiled by Procyon v0.6.0
// 

public class ze1
{
    public y01 a;
    public td1 b;
    public MessageReport c;
    public Context d;
    public y01 e;
    public byte[] f;
    
    public ze1(final Context d, final td1 b, final y01 a) {
        this.e = new y01(this) {
            public final ze1 a;
            
            @Override
            public void a(final Object o) {
                ze1 ze1;
                boolean b;
                if (o != null) {
                    Settings$Secure.getString(ze1.a(this.a).getContentResolver(), "android_id");
                    FirebaseAnalytics.getInstance(ze1.a(this.a)).a("EMAIL_SENT", null);
                    this.a.o();
                    ze1 = this.a;
                    b = true;
                }
                else {
                    ze1 = this.a;
                    b = false;
                }
                ze1.p(b);
            }
        };
        this.d = d;
        this.b = b;
        this.a = a;
        if (b.h && b.b.length() > 3) {
            new b(null).execute((Object[])new String[0]);
        }
        else {
            this.p(false);
        }
    }
    
    public static /* synthetic */ Context a(final ze1 ze1) {
        return ze1.d;
    }
    
    public static /* synthetic */ byte[] d(final ze1 ze1) {
        return ze1.f;
    }
    
    public static /* synthetic */ byte[] e(final ze1 ze1, final byte[] f) {
        return ze1.f = f;
    }
    
    public final void g(final wd1 wd1, final ResultItem resultItem, final SheetTemplate2 sheetTemplate2, final int index, final AnswerValue answerValue) {
        final ArrayList b = ve1.b(sheetTemplate2, resultItem.getExamSet());
        final String questionNumberLabel = sheetTemplate2.getLabelProfile().getQuestionNumberLabel(answerValue.getQuestionNumber());
        final String r = e5.r(answerValue, sheetTemplate2.getLabelProfile());
        final String q = e5.q(b.get(index), sheetTemplate2.getLabelProfile());
        final StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(ve1.o(answerValue.getMarksForAnswer()));
        wd1.e(questionNumberLabel, r, q, sb.toString(), false, answerValue.getMarkedState());
    }
    
    public final void h(final wd1 wd1, final ResultItem resultItem, final SheetTemplate2 sheetTemplate2) {
        final LabelProfile labelProfile = sheetTemplate2.getLabelProfile();
        wd1.f(labelProfile.getQueNoString(), labelProfile.getAttemptedString(), labelProfile.getCorrectString(), labelProfile.getMarksString(), false);
        final Subject2[] subjects = sheetTemplate2.getTemplateParams().getSubjects();
        int i = 0;
        this.j(wd1, subjects, 0, 0);
        int subjectId = 0;
        int n = 0;
        while (i < resultItem.getAnswerValue2s().size()) {
            final AnswerValue answerValue = resultItem.getAnswerValue2s().get(i);
            int sectionId;
            if (subjectId != answerValue.getSubjectId() || (sectionId = n) != answerValue.getSectionId()) {
                subjectId = answerValue.getSubjectId();
                sectionId = answerValue.getSectionId();
                this.j(wd1, sheetTemplate2.getTemplateParams().getSubjects(), subjectId, sectionId);
            }
            this.g(wd1, resultItem, sheetTemplate2, i, answerValue);
            ++i;
            n = sectionId;
        }
    }
    
    public final void i(final PDImageXObject pdImageXObject, final PDPage pdPage, final PDPageContentStream pdPageContentStream) {
        final float n = pdPage.getBBox().getWidth() - 20.0f;
        final float n2 = pdPage.getBBox().getHeight() - 20.0f;
        final int width = pdImageXObject.getWidth();
        int width2 = 800;
        if (width > 800) {
            width2 = pdImageXObject.getWidth();
        }
        final double n3 = n / n2;
        try {
            float n4;
            if (n3 > pdImageXObject.getWidth() * 1.0 / pdImageXObject.getHeight()) {
                n4 = n2 / pdImageXObject.getHeight();
            }
            else {
                n4 = n / pdImageXObject.getWidth();
            }
            final float n5 = n4 * pdImageXObject.getWidth() / width2;
            pdPageContentStream.drawImage(pdImageXObject, (pdPage.getBBox().getWidth() - pdImageXObject.getWidth() * n5) / 2.0f, (pdPage.getBBox().getHeight() - pdImageXObject.getHeight() * n5) / 2.0f, pdImageXObject.getWidth() * n5, pdImageXObject.getHeight() * n5);
            Log.d("TAG", "image converted to pdf");
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public final void j(final wd1 wd1, final Subject2[] array, final int n, final int n2) {
        final Subject2 subject2 = array[n];
        final StringBuilder sb = new StringBuilder();
        sb.append(subject2.getSubName());
        sb.append(" - ");
        sb.append(subject2.getSections()[n2].getName());
        wd1.g(sb.toString(), false);
    }
    
    public final void k(final wd1 wd1, final td1 td1, final ResultItem resultItem, final SheetTemplate2 sheetTemplate2) {
        final LabelProfile labelProfile = sheetTemplate2.getLabelProfile();
        final TemplateParams2 templateParams = sheetTemplate2.getTemplateParams();
        final double[] marksForEachSubject = resultItem.getMarksForEachSubject();
        final double[][] k = ve1.k(resultItem, sheetTemplate2);
        final int[][] c = ve1.c(resultItem, sheetTemplate2);
        final int[][] j = ve1.j(resultItem, sheetTemplate2);
        final int[] h = ve1.h(c);
        final int[] h2 = ve1.h(j);
        final double[][] r = ve1.r(sheetTemplate2);
        final double[][] n = ve1.n(k, r);
        final double[] g = ve1.g(r);
        final double[] m = ve1.m(marksForEachSubject, g);
        final double totalMarks = resultItem.getTotalMarks();
        final double d = ve1.d(g);
        final double n2 = totalMarks / d;
        final String[] examSetLabels = labelProfile.getExamSetLabels();
        String i;
        if (sheetTemplate2.getGradeLevels() != null) {
            i = ve1.i(totalMarks, sheetTemplate2.getGradeLevels());
        }
        else {
            i = null;
        }
        final String reportCardString = labelProfile.getReportCardString();
        final StringBuilder sb = new StringBuilder();
        sb.append(td1.l);
        sb.append("");
        wd1.a(reportCardString, sb.toString());
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(labelProfile.getNameString());
        sb2.append(" : ");
        sb2.append(td1.a);
        wd1.b(sb2.toString());
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(labelProfile.getExamNameString());
        sb3.append(" : ");
        sb3.append(td1.d);
        wd1.b(sb3.toString());
        if (resultItem.getExamSet() > 0) {
            final StringBuilder sb4 = new StringBuilder();
            sb4.append(labelProfile.getExamSetString());
            sb4.append(" : ");
            sb4.append(examSetLabels[resultItem.getExamSet() - 1]);
            wd1.b(sb4.toString());
        }
        final StringBuilder sb5 = new StringBuilder();
        sb5.append(labelProfile.getDateString());
        sb5.append(" : ");
        sb5.append(td1.g);
        wd1.b(sb5.toString());
        if (td1.k) {
            final StringBuilder sb6 = new StringBuilder();
            sb6.append(labelProfile.getRankString());
            sb6.append(" : ");
            sb6.append(td1.m);
            wd1.b(sb6.toString());
        }
        if (i != null) {
            final StringBuilder sb7 = new StringBuilder();
            sb7.append(labelProfile.getGradeString());
            sb7.append(" : ");
            sb7.append(i);
            wd1.b(sb7.toString());
        }
        wd1.h(50);
        final String subjectString = labelProfile.getSubjectString();
        final StringBuilder sb8 = new StringBuilder();
        sb8.append(labelProfile.getMarksString());
        sb8.append(" (");
        sb8.append(d);
        sb8.append(")");
        final String string = sb8.toString();
        final String percentageString = labelProfile.getPercentageString();
        final String correctAnswerString = labelProfile.getCorrectAnswerString();
        final String incorrectAnswerString = labelProfile.getIncorrectAnswerString();
        int l = 0;
        wd1.c(subjectString, string, percentageString, correctAnswerString, incorrectAnswerString, false);
        for (double[] array = marksForEachSubject; l < array.length; ++l) {
            final String subName = templateParams.getSubjects()[l].getSubName();
            final StringBuilder sb9 = new StringBuilder();
            sb9.append("");
            sb9.append(ve1.o(array[l]));
            final String string2 = sb9.toString();
            final StringBuilder sb10 = new StringBuilder();
            sb10.append(ve1.o(m[l]));
            sb10.append("%");
            final String string3 = sb10.toString();
            final StringBuilder sb11 = new StringBuilder();
            sb11.append(h[l]);
            sb11.append("");
            final String string4 = sb11.toString();
            final StringBuilder sb12 = new StringBuilder();
            sb12.append(h2[l]);
            sb12.append("");
            wd1.c(subName, string2, string3, string4, sb12.toString(), false);
            if (k[l].length > 1) {
                for (int n3 = 0; n3 < k[l].length; ++n3) {
                    final String name = templateParams.getSubjects()[l].getSections()[n3].getName();
                    final StringBuilder sb13 = new StringBuilder();
                    sb13.append("");
                    sb13.append(ve1.o(k[l][n3]));
                    final String string5 = sb13.toString();
                    final StringBuilder sb14 = new StringBuilder();
                    sb14.append(ve1.o(n[l][n3]));
                    sb14.append("%");
                    final String string6 = sb14.toString();
                    final StringBuilder sb15 = new StringBuilder();
                    sb15.append(c[l][n3]);
                    sb15.append("");
                    final String string7 = sb15.toString();
                    final StringBuilder sb16 = new StringBuilder();
                    sb16.append(j[l][n3]);
                    sb16.append("");
                    wd1.c(name, string5, string6, string7, sb16.toString(), false);
                }
            }
        }
        final String totalMarksString = labelProfile.getTotalMarksString();
        final StringBuilder sb17 = new StringBuilder();
        sb17.append("");
        sb17.append(ve1.o(totalMarks));
        final String string8 = sb17.toString();
        final StringBuilder sb18 = new StringBuilder();
        sb18.append(ve1.o(ve1.o(100.0 * n2)));
        sb18.append("%");
        final String string9 = sb18.toString();
        final StringBuilder sb19 = new StringBuilder();
        sb19.append(ve1.e(h));
        sb19.append("");
        final String string10 = sb19.toString();
        final StringBuilder sb20 = new StringBuilder();
        sb20.append(ve1.e(h2));
        sb20.append("");
        wd1.c(totalMarksString, string8, string9, string10, sb20.toString(), true);
        wd1.h(50);
    }
    
    public void l() {
        this.a.a(null);
        this.a = null;
    }
    
    public final byte[] m(final td1 td1) {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.d.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS));
        sb.append("/temp_report.pdf");
        final String string = sb.toString();
        final PDDocument pdDocument = new PDDocument();
        final wd1 wd1 = new wd1();
        try {
            final SheetTemplate2 sheetTemplate = TemplateRepository.getInstance(this.d).getTemplateJson(new ExamId(td1.d, td1.e, td1.g)).getSheetTemplate();
            final ResultRepository instance = ResultRepository.getInstance(this.d);
            final ResultItem resultItem = instance.getResult(new ExamId(td1.d, td1.e, td1.g), td1.l).getResultItem(sheetTemplate);
            this.k(wd1, td1, resultItem, sheetTemplate);
            this.h(wd1, resultItem, sheetTemplate);
            for (final Bitmap bitmap : wd1.j()) {
                final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap$CompressFormat.JPEG, 100, (OutputStream)byteArrayOutputStream);
                final PDImageXObject fromStream = JPEGFactory.createFromStream(pdDocument, (InputStream)new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));
                final PDPage pdPage = new PDPage(PDRectangle.A4);
                pdDocument.addPage(pdPage);
                final PDPageContentStream pdPageContentStream = new PDPageContentStream(pdDocument, pdPage);
                this.i(fromStream, pdPage, pdPageContentStream);
                pdPageContentStream.close();
                bitmap.recycle();
            }
            if (td1.j) {
                final ArrayList<SheetImageModel> sheetImages = instance.getSheetImages(new ExamId(td1.d, td1.e, td1.g), td1.l);
                ArrayList<Bitmap> list3 = null;
                Label_0691: {
                    Label_0687: {
                        try {
                            if (sheetImages.size() > 0 && sheetImages.size() == sheetTemplate.getPageLayouts().length) {
                                e5.E(sheetImages);
                                final ArrayList<Bitmap> list = new ArrayList<Bitmap>();
                                try {
                                    final ArrayList<SheetDimension> list2 = new ArrayList<SheetDimension>();
                                    for (final SheetImageModel sheetImageModel : sheetImages) {
                                        sheetImageModel.getImagePath();
                                        final Bitmap decodeFile = BitmapFactory.decodeFile(sheetImageModel.getImagePath());
                                        if (decodeFile == null) {
                                            throw new Exception("Image Missing");
                                        }
                                        list.add(decodeFile);
                                        final SheetDimension sheetDimension = sheetImageModel.getSheetDimension();
                                        if (sheetDimension == null) {
                                            throw new Exception("Image Missing");
                                        }
                                        list2.add(sheetDimension);
                                    }
                                    final com.ekodroid.omrevaluator.templateui.scanner.b b = new com.ekodroid.omrevaluator.templateui.scanner.b();
                                    int index = 0;
                                    while (true) {
                                        list3 = list;
                                        if (index >= list.size()) {
                                            break Label_0691;
                                        }
                                        final Bitmap i = b.i(list.get(index), sheetTemplate, resultItem, list2.get(index), td1.l, sheetImages.get(index).getPageIndex());
                                        final ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
                                        i.compress(Bitmap$CompressFormat.JPEG, 50, (OutputStream)byteArrayOutputStream2);
                                        final PDImageXObject fromStream2 = JPEGFactory.createFromStream(pdDocument, (InputStream)new ByteArrayInputStream(byteArrayOutputStream2.toByteArray()));
                                        final PDPage pdPage2 = new PDPage(PDRectangle.A4);
                                        pdDocument.addPage(pdPage2);
                                        final PDPageContentStream pdPageContentStream2 = new PDPageContentStream(pdDocument, pdPage2);
                                        this.i(fromStream2, pdPage2, pdPageContentStream2);
                                        pdPageContentStream2.close();
                                        i.recycle();
                                        ++index;
                                    }
                                }
                                catch (final Exception ex) {
                                    list3 = list;
                                    break Label_0687;
                                }
                            }
                            list3 = null;
                            break Label_0691;
                        }
                        catch (final Exception ex) {
                            list3 = null;
                        }
                    }
                    final Exception ex;
                    ex.printStackTrace();
                }
                if (list3 != null && list3.size() > 0) {
                    final Iterator<Bitmap> iterator3 = list3.iterator();
                    while (iterator3.hasNext()) {
                        iterator3.next().recycle();
                    }
                }
            }
            pdDocument.save(string);
            pdDocument.close();
            return this.n(new File(string));
        }
        catch (final Exception ex2) {
            ex2.printStackTrace();
            return null;
        }
    }
    
    public final byte[] n(final File file) {
        PrintStream printStream;
        String x;
        try {
            final byte[] b = new byte[(int)file.length()];
            new FileInputStream(file).read(b);
            return b;
        }
        catch (final IOException ex) {
            printStream = System.out;
            x = "Error Reading The File.";
        }
        catch (final FileNotFoundException ex) {
            printStream = System.out;
            x = "File Not Found.";
        }
        printStream.println(x);
        final IOException ex;
        ex.printStackTrace();
        return null;
    }
    
    public final void o() {
        final SharedPreferences sharedPreferences = this.d.getSharedPreferences("MyPref", 0);
        sharedPreferences.edit().putInt("count_email", sharedPreferences.getInt("count_email", 0) + 1).commit();
    }
    
    public final void p(final boolean b) {
        y01 y01;
        Boolean b2;
        if (b) {
            y01 = this.a;
            if (y01 == null) {
                return;
            }
            b2 = new Boolean(true);
        }
        else {
            y01 = this.a;
            if (y01 == null) {
                return;
            }
            b2 = new Boolean(false);
        }
        y01.a(b2);
    }
    
    public class b extends AsyncTask
    {
        public final ze1 a;
        
        public b(final ze1 a) {
            this.a = a;
        }
        
        public Void a(String... replaceAll) {
            final ze1 a = this.a;
            ze1.e(a, a.m(a.b));
            if (ze1.d(this.a) != null) {
                replaceAll = (String[])(Object)this.a.b.d.replaceAll("[^a-zA-Z0-9_-]", "_");
                int versionCode = 0;
                try {
                    versionCode = ze1.a(this.a).getPackageManager().getPackageInfo(ze1.a(this.a).getPackageName(), 0).versionCode;
                }
                catch (final PackageManager$NameNotFoundException ex) {
                    ((Throwable)ex).printStackTrace();
                }
                final ze1 a2 = this.a;
                final td1 b = a2.b;
                final String b2 = b.b;
                final String c = b.c;
                final byte[] d = ze1.d(a2);
                final StringBuilder sb = new StringBuilder();
                sb.append("Report_");
                sb.append((String)(Object)replaceAll);
                sb.append("_");
                sb.append(this.a.b.l);
                a2.c = new MessageReport(b2, c, 3, d, sb.toString(), versionCode, this.a.b.a);
            }
            return null;
        }
        
        public void b(final Void void1) {
            final ze1 a = this.a;
            if (a.c != null) {
                final ee1 b = new n52(ze1.a(a), a91.w()).b();
                final MessageReport c = this.a.c;
                final ReportData reportData = new ReportData(c.emailId, c.ccEmailID, c.mailSubject, c.appVersion, c.studentName);
                final ze1 a2 = this.a;
                new l61(reportData, a2.c.data, b, a2.e);
            }
            else {
                a.p(false);
            }
        }
        
        public void c(final Integer... array) {
        }
        
        public void onPreExecute() {
        }
    }
}

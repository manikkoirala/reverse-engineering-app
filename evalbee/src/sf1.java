import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

// 
// Decompiled by Procyon v0.6.0
// 

public final class sf1 implements vs1, us1
{
    public static final a i;
    public static final TreeMap j;
    public final int a;
    public volatile String b;
    public final long[] c;
    public final double[] d;
    public final String[] e;
    public final byte[][] f;
    public final int[] g;
    public int h;
    
    static {
        i = new a(null);
        j = new TreeMap();
    }
    
    public sf1(int a) {
        this.a = a;
        ++a;
        this.g = new int[a];
        this.c = new long[a];
        this.d = new double[a];
        this.e = new String[a];
        this.f = new byte[a][];
    }
    
    public static final sf1 c(final String s, final int n) {
        return sf1.i.a(s, n);
    }
    
    @Override
    public void A(final int n, final long n2) {
        this.g[n] = 2;
        this.c[n] = n2;
    }
    
    @Override
    public void D(final int n, final byte[] array) {
        fg0.e((Object)array, "value");
        this.g[n] = 5;
        this.f[n] = array;
    }
    
    @Override
    public void I(final int n) {
        this.g[n] = 1;
    }
    
    @Override
    public void P(final int n, final double n2) {
        this.g[n] = 3;
        this.d[n] = n2;
    }
    
    @Override
    public String a() {
        final String b = this.b;
        if (b != null) {
            return b;
        }
        throw new IllegalStateException("Required value was null.".toString());
    }
    
    @Override
    public void b(final us1 us1) {
        fg0.e((Object)us1, "statement");
        final int d = this.d();
        if (1 <= d) {
            int n = 1;
            while (true) {
                final int n2 = this.g[n];
                if (n2 != 1) {
                    if (n2 != 2) {
                        if (n2 != 3) {
                            if (n2 != 4) {
                                if (n2 == 5) {
                                    final byte[] array = this.f[n];
                                    if (array == null) {
                                        throw new IllegalArgumentException("Required value was null.".toString());
                                    }
                                    us1.D(n, array);
                                }
                            }
                            else {
                                final String s = this.e[n];
                                if (s == null) {
                                    throw new IllegalArgumentException("Required value was null.".toString());
                                }
                                us1.y(n, s);
                            }
                        }
                        else {
                            us1.P(n, this.d[n]);
                        }
                    }
                    else {
                        us1.A(n, this.c[n]);
                    }
                }
                else {
                    us1.I(n);
                }
                if (n == d) {
                    break;
                }
                ++n;
            }
        }
    }
    
    @Override
    public void close() {
    }
    
    public int d() {
        return this.h;
    }
    
    public final void e(final String b, final int h) {
        fg0.e((Object)b, "query");
        this.b = b;
        this.h = h;
    }
    
    public final void release() {
        final TreeMap j = sf1.j;
        synchronized (j) {
            j.put(this.a, this);
            sf1.i.b();
            final u02 a = u02.a;
        }
    }
    
    @Override
    public void y(final int n, final String s) {
        fg0.e((Object)s, "value");
        this.g[n] = 4;
        this.e[n] = s;
    }
    
    public static final class a
    {
        public final sf1 a(final String s, final int i) {
            fg0.e((Object)s, "query");
            Object j = sf1.j;
            synchronized (j) {
                final Map.Entry<Integer, Object> ceilingEntry = (Map.Entry<Integer, Object>)((TreeMap<Integer, sf1>)j).ceilingEntry(Integer.valueOf(i));
                if (ceilingEntry != null) {
                    ((TreeMap<Integer, sf1>)j).remove(ceilingEntry.getKey());
                    final sf1 sf1 = ceilingEntry.getValue();
                    sf1.e(s, i);
                    fg0.d((Object)sf1, "sqliteQuery");
                    return sf1;
                }
                final u02 a = u02.a;
                monitorexit(j);
                j = new sf1(i, null);
                ((sf1)j).e(s, i);
                return (sf1)j;
            }
        }
        
        public final void b() {
            final TreeMap j = sf1.j;
            if (j.size() > 15) {
                int i = j.size() - 10;
                final Iterator iterator = j.descendingKeySet().iterator();
                fg0.d((Object)iterator, "queryPool.descendingKeySet().iterator()");
                while (i > 0) {
                    iterator.next();
                    iterator.remove();
                    --i;
                }
            }
        }
    }
}

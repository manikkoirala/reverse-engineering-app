import androidx.work.c;
import androidx.work.WorkerParameters;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class aa2
{
    public static final String a;
    
    static {
        a = xl0.i("WorkerFactory");
    }
    
    public static aa2 c() {
        return new aa2() {
            @Override
            public c a(final Context context, final String s, final WorkerParameters workerParameters) {
                return null;
            }
        };
    }
    
    public abstract c a(final Context p0, final String p1, final WorkerParameters p2);
    
    public final c b(final Context context, final String s, final WorkerParameters workerParameters) {
        c a;
        final c c = a = this.a(context, s, workerParameters);
        if (c == null) {
            Class clazz = null;
            try {
                Class.forName(s).asSubclass(c.class);
            }
            finally {
                final xl0 e = xl0.e();
                final String a2 = aa2.a;
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid class: ");
                sb.append(s);
                final Throwable t;
                e.d(a2, sb.toString(), t);
                clazz = null;
            }
            a = c;
            if (clazz != null) {
                try {
                    final c c2 = (c)clazz.getDeclaredConstructor(Context.class, WorkerParameters.class).newInstance(context, workerParameters);
                }
                finally {
                    final xl0 e2 = xl0.e();
                    final String a3 = aa2.a;
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Could not instantiate ");
                    sb2.append(s);
                    final Throwable t2;
                    e2.d(a3, sb2.toString(), t2);
                    a = c;
                }
            }
        }
        if (a != null && a.isUsed()) {
            final String name = this.getClass().getName();
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("WorkerFactory (");
            sb3.append(name);
            sb3.append(") returned an instance of a ListenableWorker (");
            sb3.append(s);
            sb3.append(") which has already been invoked. createWorker() must always return a new instance of a ListenableWorker.");
            throw new IllegalStateException(sb3.toString());
        }
        return a;
    }
}

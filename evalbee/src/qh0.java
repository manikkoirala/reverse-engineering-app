import com.google.gson.internal.LazilyParsedNumber;
import java.math.BigInteger;
import java.util.Objects;

// 
// Decompiled by Procyon v0.6.0
// 

public final class qh0 extends nh0
{
    public final Object a;
    
    public qh0(final Boolean b) {
        Objects.requireNonNull(b);
        this.a = b;
    }
    
    public qh0(final Number n) {
        Objects.requireNonNull(n);
        this.a = n;
    }
    
    public qh0(final String s) {
        Objects.requireNonNull(s);
        this.a = s;
    }
    
    public static boolean s(final qh0 qh0) {
        final Object a = qh0.a;
        final boolean b = a instanceof Number;
        boolean b2 = false;
        if (b) {
            final Number n = (Number)a;
            if (!(n instanceof BigInteger) && !(n instanceof Long) && !(n instanceof Integer) && !(n instanceof Short)) {
                b2 = b2;
                if (!(n instanceof Byte)) {
                    return b2;
                }
            }
            b2 = true;
        }
        return b2;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = true;
        final boolean b2 = true;
        boolean b3 = true;
        if (this == o) {
            return true;
        }
        if (o == null || qh0.class != o.getClass()) {
            return false;
        }
        final qh0 qh0 = (qh0)o;
        if (this.a == null) {
            if (qh0.a != null) {
                b3 = false;
            }
            return b3;
        }
        if (s(this) && s(qh0)) {
            return this.o().longValue() == qh0.o().longValue() && b;
        }
        final Object a = this.a;
        if (a instanceof Number && qh0.a instanceof Number) {
            final double doubleValue = this.o().doubleValue();
            final double doubleValue2 = qh0.o().doubleValue();
            boolean b4 = b2;
            if (doubleValue != doubleValue2) {
                b4 = (Double.isNaN(doubleValue) && Double.isNaN(doubleValue2) && b2);
            }
            return b4;
        }
        return a.equals(qh0.a);
    }
    
    @Override
    public int hashCode() {
        if (this.a == null) {
            return 31;
        }
        long n;
        if (s(this)) {
            n = this.o().longValue();
        }
        else {
            final Object a = this.a;
            if (!(a instanceof Number)) {
                return a.hashCode();
            }
            n = Double.doubleToLongBits(this.o().doubleValue());
        }
        return (int)(n >>> 32 ^ n);
    }
    
    public boolean n() {
        if (this.r()) {
            return (boolean)this.a;
        }
        return Boolean.parseBoolean(this.p());
    }
    
    public Number o() {
        final Object a = this.a;
        if (a instanceof Number) {
            return (Number)a;
        }
        if (a instanceof String) {
            return new LazilyParsedNumber((String)a);
        }
        throw new UnsupportedOperationException("Primitive is neither a number nor a string");
    }
    
    public String p() {
        final Object a = this.a;
        if (a instanceof String) {
            return (String)a;
        }
        if (this.t()) {
            return this.o().toString();
        }
        if (this.r()) {
            return ((Boolean)this.a).toString();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unexpected value type: ");
        sb.append(this.a.getClass());
        throw new AssertionError((Object)sb.toString());
    }
    
    public boolean r() {
        return this.a instanceof Boolean;
    }
    
    public boolean t() {
        return this.a instanceof Number;
    }
    
    public boolean u() {
        return this.a instanceof String;
    }
}

import java.util.Iterator;
import com.google.firebase.auth.FirebaseAuth;

// 
// Decompiled by Procyon v0.6.0
// 

public final class jk2 implements Runnable
{
    public final FirebaseAuth a;
    
    public jk2(final FirebaseAuth a) {
        this.a = a;
    }
    
    @Override
    public final void run() {
        final Iterator iterator = FirebaseAuth.J(this.a).iterator();
        if (!iterator.hasNext()) {
            return;
        }
        zu0.a(iterator.next());
        throw null;
    }
}

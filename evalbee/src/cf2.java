import android.os.BaseBundle;
import android.os.Bundle;
import com.google.android.gms.measurement.api.AppMeasurementSdk$OnEventListener;

// 
// Decompiled by Procyon v0.6.0
// 

public final class cf2 implements AppMeasurementSdk$OnEventListener
{
    public final ff2 a;
    
    public cf2(final ff2 a) {
        this.a = a;
    }
    
    public final void onEvent(final String s, final String s2, final Bundle bundle, final long n) {
        if (!this.a.a.contains(s2)) {
            return;
        }
        final Bundle bundle2 = new Bundle();
        ((BaseBundle)bundle2).putString("events", lc2.a(s2));
        ff2.a(this.a).a(2, bundle2);
    }
}

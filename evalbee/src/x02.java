import com.google.protobuf.ByteString;
import java.util.ListIterator;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;
import java.util.AbstractList;

// 
// Decompiled by Procyon v0.6.0
// 

public class x02 extends AbstractList implements fj0, RandomAccess
{
    public final fj0 a;
    
    public x02(final fj0 a) {
        this.a = a;
    }
    
    public static /* synthetic */ fj0 a(final x02 x02) {
        return x02.a;
    }
    
    public String b(final int n) {
        return this.a.get(n);
    }
    
    @Override
    public fj0 d() {
        return this;
    }
    
    @Override
    public List f() {
        return this.a.f();
    }
    
    @Override
    public Iterator iterator() {
        return new Iterator(this) {
            public Iterator a = x02.a(b).iterator();
            public final x02 b;
            
            public String b() {
                return this.a.next();
            }
            
            @Override
            public boolean hasNext() {
                return this.a.hasNext();
            }
            
            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }
    
    @Override
    public Object k(final int n) {
        return this.a.k(n);
    }
    
    @Override
    public ListIterator listIterator(final int n) {
        return new ListIterator(this, n) {
            public ListIterator a = x02.a(c).listIterator(b);
            public final int b;
            public final x02 c;
            
            public void b(final String s) {
                throw new UnsupportedOperationException();
            }
            
            public String c() {
                return this.a.next();
            }
            
            public String d() {
                return this.a.previous();
            }
            
            public void e(final String s) {
                throw new UnsupportedOperationException();
            }
            
            @Override
            public boolean hasNext() {
                return this.a.hasNext();
            }
            
            @Override
            public boolean hasPrevious() {
                return this.a.hasPrevious();
            }
            
            @Override
            public int nextIndex() {
                return this.a.nextIndex();
            }
            
            @Override
            public int previousIndex() {
                return this.a.previousIndex();
            }
            
            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }
    
    @Override
    public void q(final ByteString byteString) {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public int size() {
        return this.a.size();
    }
}

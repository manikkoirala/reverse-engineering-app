import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.FirebaseException;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.ArrayList;
import com.google.android.gms.common.internal.Preconditions;
import java.util.concurrent.ScheduledExecutorService;
import com.google.android.gms.tasks.Task;
import java.util.concurrent.Executor;
import com.google.firebase.appcheck.internal.StorageHelper;
import java.util.List;

// 
// Decompiled by Procyon v0.6.0
// 

public class pq extends s10
{
    public final r10 a;
    public final r91 b;
    public final List c;
    public final List d;
    public final StorageHelper e;
    public final xw1 f;
    public final Executor g;
    public final Executor h;
    public final Executor i;
    public final Task j;
    public final ah k;
    public s5 l;
    
    public pq(final r10 a, final r91 b, final Executor g, final Executor h, final Executor i, final ScheduledExecutorService scheduledExecutorService) {
        Preconditions.checkNotNull(a);
        Preconditions.checkNotNull(b);
        this.a = a;
        this.b = b;
        this.c = new ArrayList();
        this.d = new ArrayList();
        this.e = new StorageHelper(a.l(), a.q());
        this.f = new xw1(a.l(), this, h, scheduledExecutorService);
        this.g = g;
        this.h = h;
        this.i = i;
        this.j = this.i(i);
        this.k = new ah.a();
    }
    
    @Override
    public Task a(final boolean b) {
        return this.j.continueWithTask(this.h, (Continuation)new nq(this, b));
    }
    
    @Override
    public void b(final t5 t5) {
        Preconditions.checkNotNull(t5);
        this.c.add(t5);
        this.f.d(this.c.size() + this.d.size());
        if (this.f()) {
            t5.a(fq.c(this.l));
        }
    }
    
    public Task e() {
        throw null;
    }
    
    public final boolean f() {
        final s5 l = this.l;
        return l != null && l.a() - this.k.currentTimeMillis() > 300000L;
    }
    
    public final Task i(final Executor executor) {
        final TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        executor.execute(new oq(this, taskCompletionSource));
        return taskCompletionSource.getTask();
    }
    
    public void j(final s5 l) {
        this.l = l;
    }
}

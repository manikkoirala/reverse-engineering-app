import androidx.datastore.preferences.protobuf.k;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class rz
{
    public static final Class a;
    
    static {
        a = c();
    }
    
    public static k a() {
        Label_0014: {
            if (rz.a == null) {
                break Label_0014;
            }
            try {
                return b("getEmptyRegistry");
                return k.e;
            }
            catch (final Exception ex) {
                return k.e;
            }
        }
    }
    
    public static final k b(final String name) {
        return (k)rz.a.getDeclaredMethod(name, (Class[])new Class[0]).invoke(null, new Object[0]);
    }
    
    public static Class c() {
        try {
            return Class.forName("androidx.datastore.preferences.protobuf.ExtensionRegistry");
        }
        catch (final ClassNotFoundException ex) {
            return null;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class bk0
{
    public a a;
    public a b;
    public int c;
    
    public bk0() {
        this.a = null;
        this.b = null;
        this.c = 0;
    }
    
    public void a(final Object o) {
        this.e(this.a, o);
    }
    
    public void b(final Object o) {
        this.d(this.b, o);
    }
    
    public a c(final Object o) {
        return new a(this, o);
    }
    
    public void d(final a c, final Object o) {
        final a c2 = this.c(o);
        final int c3 = this.c;
        Label_0083: {
            if (c3 == 0) {
                this.a = c2;
            }
            else {
                final a b = this.b;
                if (c != b) {
                    final a b2 = c.b;
                    b2.c = c2;
                    c2.b = b2;
                    c.b = c2;
                    c2.c = c;
                    break Label_0083;
                }
                c2.c = b;
                b.b = c2;
            }
            this.b = c2;
        }
        this.c = c3 + 1;
    }
    
    public void e(final a b, final Object o) {
        final a c = this.c(o);
        final int c2 = this.c;
        if (c2 == 0) {
            this.a = c;
            this.b = c;
        }
        else {
            final a a = this.a;
            if (b == a) {
                c.b = a;
                a.c = c;
                this.a = c;
            }
            else {
                final a c3 = b.c;
                c3.b = c;
                c.c = c3;
                b.c = c;
                c.b = b;
            }
        }
        this.c = c2 + 1;
    }
    
    public boolean f() {
        return this.c == 0;
    }
    
    public Object g() {
        return this.h(this.a);
    }
    
    public Object h(final a a) {
        final int c = this.c;
        if (c == 0) {
            return null;
        }
        final Object d = a.d;
        final a a2 = this.a;
        if (a == a2) {
            final a b = a2.b;
            if ((this.a = b) == null) {
                this.b = null;
            }
            else {
                b.c = null;
            }
        }
        else {
            final a b2 = this.b;
            if (a == b2) {
                final a c2 = b2.c;
                this.b = c2;
                c2.b = null;
            }
            else {
                final a c3 = a.c;
                c3.b = a.b;
                a.b.c = c3;
            }
        }
        a.a = null;
        this.c = c - 1;
        return d;
    }
    
    public Object i() {
        return this.h(this.b);
    }
    
    public int j() {
        return this.c;
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer(this.c * 6);
        sb.append("[");
        a a2;
        final a a = a2 = this.a;
        while (true) {
            Label_0049: {
                if (a == null) {
                    break Label_0049;
                }
                a2 = a;
                sb.append(a2.d);
                a2 = a2.b;
            }
            if (a2 != null) {
                sb.append(", ");
                continue;
            }
            break;
        }
        sb.append("]");
        return sb.toString();
    }
    
    public static class a
    {
        public bk0 a;
        public a b;
        public a c;
        public Object d;
        
        public a(final bk0 a, final Object d) {
            this.b = null;
            this.c = null;
            this.a = a;
            this.d = d;
        }
    }
}

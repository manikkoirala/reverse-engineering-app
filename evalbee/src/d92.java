import android.os.Looper;
import java.util.concurrent.Executor;
import android.os.Handler;
import kotlinx.coroutines.CoroutineDispatcher;

// 
// Decompiled by Procyon v0.6.0
// 

public class d92 implements hu1
{
    public final kl1 a;
    public final CoroutineDispatcher b;
    public final Handler c;
    public final Executor d;
    
    public d92(final Executor executor) {
        this.c = new Handler(Looper.getMainLooper());
        this.d = new Executor(this) {
            public final d92 a;
            
            @Override
            public void execute(final Runnable runnable) {
                this.a.c.post(runnable);
            }
        };
        final kl1 a = new kl1(executor);
        this.a = a;
        this.b = xy.a((Executor)a);
    }
    
    @Override
    public CoroutineDispatcher a() {
        return this.b;
    }
    
    @Override
    public Executor c() {
        return this.d;
    }
    
    public kl1 e() {
        return this.a;
    }
}

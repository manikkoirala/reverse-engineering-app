import android.view.SubMenu;
import android.view.MenuItem;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class ib
{
    public final Context a;
    public co1 b;
    public co1 c;
    
    public ib(final Context a) {
        this.a = a;
    }
    
    public final MenuItem c(MenuItem o) {
        if (o instanceof ms1) {
            final ms1 ms1 = (ms1)o;
            if (this.b == null) {
                this.b = new co1();
            }
            if ((o = this.b.get(ms1)) == null) {
                o = new ov0(this.a, ms1);
                this.b.put(ms1, o);
            }
            return (MenuItem)o;
        }
        return (MenuItem)o;
    }
    
    public final SubMenu d(final SubMenu subMenu) {
        return subMenu;
    }
    
    public final void e() {
        final co1 b = this.b;
        if (b != null) {
            b.clear();
        }
        final co1 c = this.c;
        if (c != null) {
            c.clear();
        }
    }
    
    public final void f(final int n) {
        if (this.b == null) {
            return;
        }
        int n2;
        for (int i = 0; i < this.b.size(); i = n2 + 1) {
            n2 = i;
            if (((MenuItem)this.b.i(i)).getGroupId() == n) {
                this.b.k(i);
                n2 = i - 1;
            }
        }
    }
    
    public final void g(final int n) {
        if (this.b == null) {
            return;
        }
        for (int i = 0; i < this.b.size(); ++i) {
            if (((MenuItem)this.b.i(i)).getItemId() == n) {
                this.b.k(i);
                break;
            }
        }
    }
}

import android.view.ViewParent;
import android.view.MotionEvent;
import android.os.SystemClock;
import android.view.ViewConfiguration;
import android.view.View;
import android.view.View$OnAttachStateChangeListener;
import android.view.View$OnTouchListener;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class w70 implements View$OnTouchListener, View$OnAttachStateChangeListener
{
    public final float a;
    public final int b;
    public final int c;
    public final View d;
    public Runnable e;
    public Runnable f;
    public boolean g;
    public int h;
    public final int[] i;
    
    public w70(final View d) {
        this.i = new int[2];
        (this.d = d).setLongClickable(true);
        d.addOnAttachStateChangeListener((View$OnAttachStateChangeListener)this);
        this.a = (float)ViewConfiguration.get(d.getContext()).getScaledTouchSlop();
        final int tapTimeout = ViewConfiguration.getTapTimeout();
        this.b = tapTimeout;
        this.c = (tapTimeout + ViewConfiguration.getLongPressTimeout()) / 2;
    }
    
    public static boolean h(final View view, final float n, final float n2, final float n3) {
        final float n4 = -n3;
        return n >= n4 && n2 >= n4 && n < view.getRight() - view.getLeft() + n3 && n2 < view.getBottom() - view.getTop() + n3;
    }
    
    public final void a() {
        final Runnable f = this.f;
        if (f != null) {
            this.d.removeCallbacks(f);
        }
        final Runnable e = this.e;
        if (e != null) {
            this.d.removeCallbacks(e);
        }
    }
    
    public abstract wn1 b();
    
    public abstract boolean c();
    
    public boolean d() {
        final wn1 b = this.b();
        if (b != null && b.a()) {
            b.dismiss();
        }
        return true;
    }
    
    public void e() {
        this.a();
        final View d = this.d;
        if (d.isEnabled()) {
            if (!d.isLongClickable()) {
                if (!this.c()) {
                    return;
                }
                d.getParent().requestDisallowInterceptTouchEvent(true);
                final long uptimeMillis = SystemClock.uptimeMillis();
                final MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
                d.onTouchEvent(obtain);
                obtain.recycle();
                this.g = true;
            }
        }
    }
    
    public final boolean f(final MotionEvent motionEvent) {
        final View d = this.d;
        final wn1 b = this.b();
        boolean b3;
        final boolean b2 = b3 = false;
        if (b != null) {
            if (!b.a()) {
                b3 = b2;
            }
            else {
                final kv kv = (kv)b.h();
                b3 = b2;
                if (kv != null) {
                    if (!((View)kv).isShown()) {
                        b3 = b2;
                    }
                    else {
                        final MotionEvent obtainNoHistory = MotionEvent.obtainNoHistory(motionEvent);
                        this.i(d, obtainNoHistory);
                        this.j((View)kv, obtainNoHistory);
                        final boolean e = kv.e(obtainNoHistory, this.h);
                        obtainNoHistory.recycle();
                        final int actionMasked = motionEvent.getActionMasked();
                        final boolean b4 = actionMasked != 1 && actionMasked != 3;
                        b3 = b2;
                        if (e) {
                            b3 = b2;
                            if (b4) {
                                b3 = true;
                            }
                        }
                    }
                }
            }
        }
        return b3;
    }
    
    public final boolean g(final MotionEvent motionEvent) {
        final View d = this.d;
        if (!d.isEnabled()) {
            return false;
        }
        final int actionMasked = motionEvent.getActionMasked();
        if (actionMasked != 0) {
            if (actionMasked != 1) {
                if (actionMasked != 2) {
                    if (actionMasked != 3) {
                        return false;
                    }
                }
                else {
                    final int pointerIndex = motionEvent.findPointerIndex(this.h);
                    if (pointerIndex >= 0 && !h(d, motionEvent.getX(pointerIndex), motionEvent.getY(pointerIndex), this.a)) {
                        this.a();
                        d.getParent().requestDisallowInterceptTouchEvent(true);
                        return true;
                    }
                    return false;
                }
            }
            this.a();
        }
        else {
            this.h = motionEvent.getPointerId(0);
            if (this.e == null) {
                this.e = new a();
            }
            d.postDelayed(this.e, (long)this.b);
            if (this.f == null) {
                this.f = new b();
            }
            d.postDelayed(this.f, (long)this.c);
        }
        return false;
    }
    
    public final boolean i(final View view, final MotionEvent motionEvent) {
        final int[] i = this.i;
        view.getLocationOnScreen(i);
        motionEvent.offsetLocation((float)i[0], (float)i[1]);
        return true;
    }
    
    public final boolean j(final View view, final MotionEvent motionEvent) {
        final int[] i = this.i;
        view.getLocationOnScreen(i);
        motionEvent.offsetLocation((float)(-i[0]), (float)(-i[1]));
        return true;
    }
    
    public boolean onTouch(final View view, final MotionEvent motionEvent) {
        final boolean g = this.g;
        final boolean b = true;
        boolean g2;
        if (g) {
            g2 = (this.f(motionEvent) || !this.d());
        }
        else {
            final boolean b2 = g2 = (this.g(motionEvent) && this.c());
            if (b2) {
                final long uptimeMillis = SystemClock.uptimeMillis();
                final MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
                this.d.onTouchEvent(obtain);
                obtain.recycle();
                g2 = b2;
            }
        }
        this.g = g2;
        boolean b3 = b;
        if (!g2) {
            b3 = (g && b);
        }
        return b3;
    }
    
    public void onViewAttachedToWindow(final View view) {
    }
    
    public void onViewDetachedFromWindow(final View view) {
        this.g = false;
        this.h = -1;
        final Runnable e = this.e;
        if (e != null) {
            this.d.removeCallbacks(e);
        }
    }
    
    public class a implements Runnable
    {
        public final w70 a;
        
        public a(final w70 a) {
            this.a = a;
        }
        
        @Override
        public void run() {
            final ViewParent parent = this.a.d.getParent();
            if (parent != null) {
                parent.requestDisallowInterceptTouchEvent(true);
            }
        }
    }
    
    public class b implements Runnable
    {
        public final w70 a;
        
        public b(final w70 a) {
            this.a = a;
        }
        
        @Override
        public void run() {
            this.a.e();
        }
    }
}

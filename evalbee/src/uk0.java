import android.util.Log;
import android.content.Intent;
import android.os.Message;
import android.os.Looper;
import android.os.Handler;
import java.util.ArrayList;
import java.util.HashMap;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public final class uk0
{
    public static final Object f;
    public static uk0 g;
    public final Context a;
    public final HashMap b;
    public final HashMap c;
    public final ArrayList d;
    public final Handler e;
    
    static {
        f = new Object();
    }
    
    public uk0(final Context a) {
        this.b = new HashMap();
        this.c = new HashMap();
        this.d = new ArrayList();
        this.a = a;
        this.e = new Handler(this, a.getMainLooper()) {
            public final uk0 a;
            
            public void handleMessage(final Message message) {
                if (message.what != 1) {
                    super.handleMessage(message);
                }
                else {
                    this.a.a();
                }
            }
        };
    }
    
    public static uk0 b(final Context context) {
        synchronized (uk0.f) {
            if (uk0.g == null) {
                uk0.g = new uk0(context.getApplicationContext());
            }
            return uk0.g;
        }
    }
    
    public void a() {
        while (true) {
            Object b = this.b;
            synchronized (b) {
                final int size = this.d.size();
                if (size <= 0) {
                    return;
                }
                final b[] a = new b[size];
                this.d.toArray(a);
                this.d.clear();
                monitorexit(b);
                Label_0078: {
                    for (int i = 0; i < size; ++i) {
                        b = a[i];
                        if (((b)b).a.size() > 0) {
                            break Label_0078;
                        }
                    }
                    continue;
                }
                zu0.a(((b)b).a.get(0));
                throw null;
            }
        }
    }
    
    public boolean c(final Intent obj) {
        synchronized (this.b) {
            obj.getAction();
            final String resolveTypeIfNeeded = obj.resolveTypeIfNeeded(this.a.getContentResolver());
            obj.getData();
            final String scheme = obj.getScheme();
            obj.getCategories();
            final boolean b = (obj.getFlags() & 0x8) != 0x0;
            if (b) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Resolving type ");
                sb.append(resolveTypeIfNeeded);
                sb.append(" scheme ");
                sb.append(scheme);
                sb.append(" of intent ");
                sb.append(obj);
                Log.v("LocalBroadcastManager", sb.toString());
            }
            final ArrayList obj2 = this.c.get(obj.getAction());
            if (obj2 != null) {
                if (b) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Action list: ");
                    sb2.append(obj2);
                    Log.v("LocalBroadcastManager", sb2.toString());
                }
                if (obj2.size() > 0) {
                    zu0.a(obj2.get(0));
                    if (b) {
                        new StringBuilder().append("Matching against filter ");
                        throw null;
                    }
                    throw null;
                }
            }
            return false;
        }
    }
    
    public abstract static final class b
    {
        public final ArrayList a;
    }
}

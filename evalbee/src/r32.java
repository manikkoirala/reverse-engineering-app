import android.util.Log;
import java.util.Arrays;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.content.Context;
import android.view.ViewGroup;
import android.view.View;
import android.widget.OverScroller;
import android.view.VelocityTracker;
import android.view.animation.Interpolator;

// 
// Decompiled by Procyon v0.6.0
// 

public class r32
{
    public static final Interpolator x;
    public int a;
    public int b;
    public int c;
    public float[] d;
    public float[] e;
    public float[] f;
    public float[] g;
    public int[] h;
    public int[] i;
    public int[] j;
    public int k;
    public VelocityTracker l;
    public float m;
    public float n;
    public int o;
    public final int p;
    public int q;
    public OverScroller r;
    public final c s;
    public View t;
    public boolean u;
    public final ViewGroup v;
    public final Runnable w;
    
    static {
        x = (Interpolator)new Interpolator() {
            public float getInterpolation(float n) {
                --n;
                return n * n * n * n * n + 1.0f;
            }
        };
    }
    
    public r32(final Context context, final ViewGroup v, final c s) {
        this.c = -1;
        this.w = new Runnable(this) {
            public final r32 a;
            
            @Override
            public void run() {
                this.a.F(0);
            }
        };
        if (v == null) {
            throw new IllegalArgumentException("Parent view may not be null");
        }
        if (s != null) {
            this.v = v;
            this.s = s;
            final ViewConfiguration value = ViewConfiguration.get(context);
            final int n = (int)(context.getResources().getDisplayMetrics().density * 20.0f + 0.5f);
            this.p = n;
            this.o = n;
            this.b = value.getScaledTouchSlop();
            this.m = (float)value.getScaledMaximumFlingVelocity();
            this.n = (float)value.getScaledMinimumFlingVelocity();
            this.r = new OverScroller(context, r32.x);
            return;
        }
        throw new IllegalArgumentException("Callback may not be null");
    }
    
    public static r32 l(final ViewGroup viewGroup, final float n, final c c) {
        final r32 m = m(viewGroup, c);
        m.b *= (int)(1.0f / n);
        return m;
    }
    
    public static r32 m(final ViewGroup viewGroup, final c c) {
        return new r32(((View)viewGroup).getContext(), viewGroup, c);
    }
    
    public void A(final MotionEvent motionEvent) {
        final int actionMasked = motionEvent.getActionMasked();
        final int actionIndex = motionEvent.getActionIndex();
        if (actionMasked == 0) {
            this.a();
        }
        if (this.l == null) {
            this.l = VelocityTracker.obtain();
        }
        this.l.addMovement(motionEvent);
        final int n = 0;
        int i = 0;
        if (actionMasked != 0) {
            if (actionMasked != 1) {
                if (actionMasked == 2) {
                    if (this.a == 1) {
                        if (!this.y(this.c)) {
                            return;
                        }
                        final int pointerIndex = motionEvent.findPointerIndex(this.c);
                        final float x = motionEvent.getX(pointerIndex);
                        final float y = motionEvent.getY(pointerIndex);
                        final float[] f = this.f;
                        final int c = this.c;
                        final int n2 = (int)(x - f[c]);
                        final int n3 = (int)(y - this.g[c]);
                        this.p(this.t.getLeft() + n2, this.t.getTop() + n3, n2, n3);
                    }
                    else {
                        for (int pointerCount = motionEvent.getPointerCount(), j = n; j < pointerCount; ++j) {
                            final int pointerId = motionEvent.getPointerId(j);
                            if (this.y(pointerId)) {
                                final float x2 = motionEvent.getX(j);
                                final float y2 = motionEvent.getY(j);
                                final float n4 = x2 - this.d[pointerId];
                                final float n5 = y2 - this.e[pointerId];
                                this.C(n4, n5, pointerId);
                                if (this.a == 1) {
                                    break;
                                }
                                final View r = this.r((int)x2, (int)y2);
                                if (this.d(r, n4, n5) && this.J(r, pointerId)) {
                                    break;
                                }
                            }
                        }
                    }
                    this.E(motionEvent);
                    return;
                }
                if (actionMasked != 3) {
                    if (actionMasked != 5) {
                        if (actionMasked != 6) {
                            return;
                        }
                        final int pointerId2 = motionEvent.getPointerId(actionIndex);
                        Label_0222: {
                            if (this.a == 1 && pointerId2 == this.c) {
                                while (true) {
                                    while (i < motionEvent.getPointerCount()) {
                                        final int pointerId3 = motionEvent.getPointerId(i);
                                        if (pointerId3 != this.c) {
                                            final View r2 = this.r((int)motionEvent.getX(i), (int)motionEvent.getY(i));
                                            final View t = this.t;
                                            if (r2 == t && this.J(t, pointerId3)) {
                                                final int c2 = this.c;
                                                if (c2 == -1) {
                                                    this.B();
                                                }
                                                break Label_0222;
                                            }
                                        }
                                        ++i;
                                    }
                                    final int c2 = -1;
                                    continue;
                                }
                            }
                        }
                        this.h(pointerId2);
                        return;
                    }
                    else {
                        final int pointerId4 = motionEvent.getPointerId(actionIndex);
                        final float x3 = motionEvent.getX(actionIndex);
                        final float y3 = motionEvent.getY(actionIndex);
                        this.D(x3, y3, pointerId4);
                        if (this.a == 0) {
                            this.J(this.r((int)x3, (int)y3), pointerId4);
                            final int n6 = this.h[pointerId4];
                            final int q = this.q;
                            if ((n6 & q) != 0x0) {
                                this.s.onEdgeTouched(n6 & q, pointerId4);
                            }
                            return;
                        }
                        else {
                            if (this.w((int)x3, (int)y3)) {
                                this.J(this.t, pointerId4);
                            }
                            return;
                        }
                    }
                }
                else if (this.a == 1) {
                    this.n(0.0f, 0.0f);
                }
            }
            else if (this.a == 1) {
                this.B();
            }
            this.a();
        }
        else {
            final float x4 = motionEvent.getX();
            final float y4 = motionEvent.getY();
            final int pointerId5 = motionEvent.getPointerId(0);
            final View r3 = this.r((int)x4, (int)y4);
            this.D(x4, y4, pointerId5);
            this.J(r3, pointerId5);
            final int n7 = this.h[pointerId5];
            final int q2 = this.q;
            if ((n7 & q2) != 0x0) {
                this.s.onEdgeTouched(n7 & q2, pointerId5);
            }
        }
    }
    
    public final void B() {
        this.l.computeCurrentVelocity(1000, this.m);
        this.n(this.e(this.l.getXVelocity(this.c), this.n, this.m), this.e(this.l.getYVelocity(this.c), this.n, this.m));
    }
    
    public final void C(final float n, final float n2, final int n3) {
        int c;
        final boolean b = (c = (this.c(n, n2, n3, 1) ? 1 : 0)) != 0;
        if (this.c(n2, n, n3, 4)) {
            c = ((b ? 1 : 0) | 0x4);
        }
        int n4 = c;
        if (this.c(n, n2, n3, 2)) {
            n4 = (c | 0x2);
        }
        int n5 = n4;
        if (this.c(n2, n, n3, 8)) {
            n5 = (n4 | 0x8);
        }
        if (n5 != 0) {
            final int[] i = this.i;
            i[n3] |= n5;
            this.s.onEdgeDragStarted(n5, n3);
        }
    }
    
    public final void D(final float n, final float n2, final int n3) {
        this.q(n3);
        this.d[n3] = (this.f[n3] = n);
        this.e[n3] = (this.g[n3] = n2);
        this.h[n3] = this.t((int)n, (int)n2);
        this.k |= 1 << n3;
    }
    
    public final void E(final MotionEvent motionEvent) {
        for (int pointerCount = motionEvent.getPointerCount(), i = 0; i < pointerCount; ++i) {
            final int pointerId = motionEvent.getPointerId(i);
            if (this.y(pointerId)) {
                final float x = motionEvent.getX(i);
                final float y = motionEvent.getY(i);
                this.f[pointerId] = x;
                this.g[pointerId] = y;
            }
        }
    }
    
    public void F(final int a) {
        ((View)this.v).removeCallbacks(this.w);
        if (this.a != a) {
            this.a = a;
            this.s.onViewDragStateChanged(a);
            if (this.a == 0) {
                this.t = null;
            }
        }
    }
    
    public boolean G(final int n, final int n2) {
        if (this.u) {
            return this.s(n, n2, (int)this.l.getXVelocity(this.c), (int)this.l.getYVelocity(this.c));
        }
        throw new IllegalStateException("Cannot settleCapturedViewAt outside of a call to Callback#onViewReleased");
    }
    
    public boolean H(final MotionEvent motionEvent) {
        final int actionMasked = motionEvent.getActionMasked();
        final int actionIndex = motionEvent.getActionIndex();
        if (actionMasked == 0) {
            this.a();
        }
        if (this.l == null) {
            this.l = VelocityTracker.obtain();
        }
        this.l.addMovement(motionEvent);
        Label_0615: {
            if (actionMasked != 0) {
                Label_0513: {
                    if (actionMasked == 1) {
                        break Label_0513;
                    }
                    if (actionMasked != 2) {
                        if (actionMasked == 3) {
                            break Label_0513;
                        }
                        if (actionMasked != 5) {
                            if (actionMasked == 6) {
                                this.h(motionEvent.getPointerId(actionIndex));
                            }
                        }
                        else {
                            final int pointerId = motionEvent.getPointerId(actionIndex);
                            final float x = motionEvent.getX(actionIndex);
                            final float y = motionEvent.getY(actionIndex);
                            this.D(x, y, pointerId);
                            final int a = this.a;
                            if (a == 0) {
                                final int n = this.h[pointerId];
                                final int q = this.q;
                                if ((n & q) != 0x0) {
                                    this.s.onEdgeTouched(n & q, pointerId);
                                }
                            }
                            else if (a == 2) {
                                final View r = this.r((int)x, (int)y);
                                if (r == this.t) {
                                    this.J(r, pointerId);
                                }
                            }
                        }
                    }
                    else if (this.d != null) {
                        if (this.e != null) {
                            for (int pointerCount = motionEvent.getPointerCount(), i = 0; i < pointerCount; ++i) {
                                final int pointerId2 = motionEvent.getPointerId(i);
                                if (this.y(pointerId2)) {
                                    final float x2 = motionEvent.getX(i);
                                    final float y2 = motionEvent.getY(i);
                                    final float n2 = x2 - this.d[pointerId2];
                                    final float n3 = y2 - this.e[pointerId2];
                                    final View r2 = this.r((int)x2, (int)y2);
                                    final boolean b = r2 != null && this.d(r2, n2, n3);
                                    if (b) {
                                        final int left = r2.getLeft();
                                        final int n4 = (int)n2;
                                        final int clampViewPositionHorizontal = this.s.clampViewPositionHorizontal(r2, left + n4, n4);
                                        final int top = r2.getTop();
                                        final int n5 = (int)n3;
                                        final int clampViewPositionVertical = this.s.clampViewPositionVertical(r2, top + n5, n5);
                                        final int viewHorizontalDragRange = this.s.getViewHorizontalDragRange(r2);
                                        final int viewVerticalDragRange = this.s.getViewVerticalDragRange(r2);
                                        if (viewHorizontalDragRange == 0 || (viewHorizontalDragRange > 0 && clampViewPositionHorizontal == left)) {
                                            if (viewVerticalDragRange == 0) {
                                                break;
                                            }
                                            if (viewVerticalDragRange > 0 && clampViewPositionVertical == top) {
                                                break;
                                            }
                                        }
                                    }
                                    this.C(n2, n3, pointerId2);
                                    if (this.a == 1) {
                                        break;
                                    }
                                    if (b && this.J(r2, pointerId2)) {
                                        break;
                                    }
                                }
                            }
                            this.E(motionEvent);
                        }
                    }
                    break Label_0615;
                }
                this.a();
            }
            else {
                final float x3 = motionEvent.getX();
                final float y3 = motionEvent.getY();
                final int pointerId3 = motionEvent.getPointerId(0);
                this.D(x3, y3, pointerId3);
                final View r3 = this.r((int)x3, (int)y3);
                if (r3 == this.t && this.a == 2) {
                    this.J(r3, pointerId3);
                }
                final int n6 = this.h[pointerId3];
                final int q2 = this.q;
                if ((n6 & q2) != 0x0) {
                    this.s.onEdgeTouched(n6 & q2, pointerId3);
                }
            }
        }
        boolean b2 = false;
        if (this.a == 1) {
            b2 = true;
        }
        return b2;
    }
    
    public boolean I(final View t, final int n, final int n2) {
        this.t = t;
        this.c = -1;
        final boolean s = this.s(n, n2, 0, 0);
        if (!s && this.a == 0 && this.t != null) {
            this.t = null;
        }
        return s;
    }
    
    public boolean J(final View view, final int c) {
        if (view == this.t && this.c == c) {
            return true;
        }
        if (view != null && this.s.tryCaptureView(view, c)) {
            this.b(view, this.c = c);
            return true;
        }
        return false;
    }
    
    public void a() {
        this.c = -1;
        this.g();
        final VelocityTracker l = this.l;
        if (l != null) {
            l.recycle();
            this.l = null;
        }
    }
    
    public void b(final View t, final int c) {
        if (t.getParent() == this.v) {
            this.t = t;
            this.c = c;
            this.s.onViewCaptured(t, c);
            this.F(1);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("captureChildView: parameter must be a descendant of the ViewDragHelper's tracked parent view (");
        sb.append(this.v);
        sb.append(")");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public final boolean c(float abs, float abs2, final int n, final int n2) {
        abs = Math.abs(abs);
        abs2 = Math.abs(abs2);
        final int n3 = this.h[n];
        boolean b2;
        final boolean b = b2 = false;
        if ((n3 & n2) == n2) {
            b2 = b;
            if ((this.q & n2) != 0x0) {
                b2 = b;
                if ((this.j[n] & n2) != n2) {
                    b2 = b;
                    if ((this.i[n] & n2) != n2) {
                        final int b3 = this.b;
                        if (abs <= b3 && abs2 <= b3) {
                            b2 = b;
                        }
                        else {
                            if (abs < abs2 * 0.5f && this.s.onEdgeLock(n2)) {
                                final int[] j = this.j;
                                j[n] |= n2;
                                return false;
                            }
                            b2 = b;
                            if ((this.i[n] & n2) == 0x0) {
                                b2 = b;
                                if (abs > this.b) {
                                    b2 = true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return b2;
    }
    
    public final boolean d(final View view, final float a, final float a2) {
        boolean b = false;
        final boolean b2 = false;
        final boolean b3 = false;
        if (view == null) {
            return false;
        }
        final boolean b4 = this.s.getViewHorizontalDragRange(view) > 0;
        final boolean b5 = this.s.getViewVerticalDragRange(view) > 0;
        if (b4 && b5) {
            final int b6 = this.b;
            boolean b7 = b3;
            if (a * a + a2 * a2 > b6 * b6) {
                b7 = true;
            }
            return b7;
        }
        if (b4) {
            if (Math.abs(a) > this.b) {
                b = true;
            }
            return b;
        }
        boolean b8 = b2;
        if (b5) {
            b8 = b2;
            if (Math.abs(a2) > this.b) {
                b8 = true;
            }
        }
        return b8;
    }
    
    public final float e(final float a, final float n, float n2) {
        final float abs = Math.abs(a);
        if (abs < n) {
            return 0.0f;
        }
        if (abs > n2) {
            if (a <= 0.0f) {
                n2 = -n2;
            }
            return n2;
        }
        return a;
    }
    
    public final int f(final int a, final int n, int n2) {
        final int abs = Math.abs(a);
        if (abs < n) {
            return 0;
        }
        if (abs > n2) {
            if (a <= 0) {
                n2 = -n2;
            }
            return n2;
        }
        return a;
    }
    
    public final void g() {
        final float[] d = this.d;
        if (d == null) {
            return;
        }
        Arrays.fill(d, 0.0f);
        Arrays.fill(this.e, 0.0f);
        Arrays.fill(this.f, 0.0f);
        Arrays.fill(this.g, 0.0f);
        Arrays.fill(this.h, 0);
        Arrays.fill(this.i, 0);
        Arrays.fill(this.j, 0);
        this.k = 0;
    }
    
    public final void h(final int n) {
        if (this.d != null) {
            if (this.x(n)) {
                this.d[n] = 0.0f;
                this.e[n] = 0.0f;
                this.f[n] = 0.0f;
                this.g[n] = 0.0f;
                this.h[n] = 0;
                this.i[n] = 0;
                this.j[n] = 0;
                this.k &= ~(1 << n);
            }
        }
    }
    
    public final int i(int a, int abs, final int n) {
        if (a == 0) {
            return 0;
        }
        final int width = ((View)this.v).getWidth();
        final int n2 = width / 2;
        final float min = Math.min(1.0f, Math.abs(a) / (float)width);
        final float n3 = (float)n2;
        final float o = this.o(min);
        abs = Math.abs(abs);
        if (abs > 0) {
            a = Math.round(Math.abs((n3 + o * n3) / abs) * 1000.0f) * 4;
        }
        else {
            a = (int)((Math.abs(a) / (float)n + 1.0f) * 256.0f);
        }
        return Math.min(a, 600);
    }
    
    public final int j(final View view, int i, int j, int f, int f2) {
        f = this.f(f, (int)this.n, (int)this.m);
        f2 = this.f(f2, (int)this.n, (int)this.m);
        final int abs = Math.abs(i);
        final int abs2 = Math.abs(j);
        final int abs3 = Math.abs(f);
        final int abs4 = Math.abs(f2);
        final int n = abs3 + abs4;
        final int n2 = abs + abs2;
        float n3;
        float n4;
        if (f != 0) {
            n3 = (float)abs3;
            n4 = (float)n;
        }
        else {
            n3 = (float)abs;
            n4 = (float)n2;
        }
        final float n5 = n3 / n4;
        float n6;
        float n7;
        if (f2 != 0) {
            n6 = (float)abs4;
            n7 = (float)n;
        }
        else {
            n6 = (float)abs2;
            n7 = (float)n2;
        }
        final float n8 = n6 / n7;
        i = this.i(i, f, this.s.getViewHorizontalDragRange(view));
        j = this.i(j, f2, this.s.getViewVerticalDragRange(view));
        return (int)(i * n5 + j * n8);
    }
    
    public boolean k(final boolean b) {
        final int a = this.a;
        final boolean b2 = false;
        if (a == 2) {
            final boolean computeScrollOffset = this.r.computeScrollOffset();
            final int currX = this.r.getCurrX();
            final int currY = this.r.getCurrY();
            final int n = currX - this.t.getLeft();
            final int n2 = currY - this.t.getTop();
            if (n != 0) {
                o32.a0(this.t, n);
            }
            if (n2 != 0) {
                o32.b0(this.t, n2);
            }
            if (n != 0 || n2 != 0) {
                this.s.onViewPositionChanged(this.t, currX, currY, n, n2);
            }
            boolean b3 = computeScrollOffset;
            if (computeScrollOffset) {
                b3 = computeScrollOffset;
                if (currX == this.r.getFinalX()) {
                    b3 = computeScrollOffset;
                    if (currY == this.r.getFinalY()) {
                        this.r.abortAnimation();
                        b3 = false;
                    }
                }
            }
            if (!b3) {
                if (b) {
                    ((View)this.v).post(this.w);
                }
                else {
                    this.F(0);
                }
            }
        }
        boolean b4 = b2;
        if (this.a == 2) {
            b4 = true;
        }
        return b4;
    }
    
    public final void n(final float n, final float n2) {
        this.u = true;
        this.s.onViewReleased(this.t, n, n2);
        this.u = false;
        if (this.a == 1) {
            this.F(0);
        }
    }
    
    public final float o(final float n) {
        return (float)Math.sin((n - 0.5f) * 0.47123894f);
    }
    
    public final void p(int clampViewPositionVertical, final int n, final int n2, final int n3) {
        final int left = this.t.getLeft();
        final int top = this.t.getTop();
        int clampViewPositionHorizontal = clampViewPositionVertical;
        if (n2 != 0) {
            clampViewPositionHorizontal = this.s.clampViewPositionHorizontal(this.t, clampViewPositionVertical, n2);
            o32.a0(this.t, clampViewPositionHorizontal - left);
        }
        clampViewPositionVertical = n;
        if (n3 != 0) {
            clampViewPositionVertical = this.s.clampViewPositionVertical(this.t, n, n3);
            o32.b0(this.t, clampViewPositionVertical - top);
        }
        if (n2 != 0 || n3 != 0) {
            this.s.onViewPositionChanged(this.t, clampViewPositionHorizontal, clampViewPositionVertical, clampViewPositionHorizontal - left, clampViewPositionVertical - top);
        }
    }
    
    public final void q(int n) {
        final float[] d = this.d;
        if (d == null || d.length <= n) {
            final float[] d2 = new float[++n];
            final float[] e = new float[n];
            final float[] f = new float[n];
            final float[] g = new float[n];
            final int[] h = new int[n];
            final int[] i = new int[n];
            final int[] j = new int[n];
            if (d != null) {
                System.arraycopy(d, 0, d2, 0, d.length);
                final float[] e2 = this.e;
                System.arraycopy(e2, 0, e, 0, e2.length);
                final float[] f2 = this.f;
                System.arraycopy(f2, 0, f, 0, f2.length);
                final float[] g2 = this.g;
                System.arraycopy(g2, 0, g, 0, g2.length);
                final int[] h2 = this.h;
                System.arraycopy(h2, 0, h, 0, h2.length);
                final int[] k = this.i;
                System.arraycopy(k, 0, i, 0, k.length);
                final int[] l = this.j;
                System.arraycopy(l, 0, j, 0, l.length);
            }
            this.d = d2;
            this.e = e;
            this.f = f;
            this.g = g;
            this.h = h;
            this.i = i;
            this.j = j;
        }
    }
    
    public View r(final int n, final int n2) {
        for (int i = this.v.getChildCount() - 1; i >= 0; --i) {
            final View child = this.v.getChildAt(this.s.getOrderedChildIndex(i));
            if (n >= child.getLeft() && n < child.getRight() && n2 >= child.getTop() && n2 < child.getBottom()) {
                return child;
            }
        }
        return null;
    }
    
    public final boolean s(int n, int n2, int j, final int n3) {
        final int left = this.t.getLeft();
        final int top = this.t.getTop();
        n -= left;
        n2 -= top;
        if (n == 0 && n2 == 0) {
            this.r.abortAnimation();
            this.F(0);
            return false;
        }
        j = this.j(this.t, n, n2, j, n3);
        this.r.startScroll(left, top, n, n2, j);
        this.F(2);
        return true;
    }
    
    public final int t(int n, final int n2) {
        int n3;
        final boolean b = (n3 = ((n < ((View)this.v).getLeft() + this.o) ? 1 : 0)) != 0;
        if (n2 < ((View)this.v).getTop() + this.o) {
            n3 = ((b ? 1 : 0) | 0x4);
        }
        int n4 = n3;
        if (n > ((View)this.v).getRight() - this.o) {
            n4 = (n3 | 0x2);
        }
        n = n4;
        if (n2 > ((View)this.v).getBottom() - this.o) {
            n = (n4 | 0x8);
        }
        return n;
    }
    
    public int u() {
        return this.b;
    }
    
    public int v() {
        return this.a;
    }
    
    public boolean w(final int n, final int n2) {
        return this.z(this.t, n, n2);
    }
    
    public boolean x(final int n) {
        final int k = this.k;
        boolean b = true;
        if ((1 << n & k) == 0x0) {
            b = false;
        }
        return b;
    }
    
    public final boolean y(final int i) {
        if (!this.x(i)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Ignoring pointerId=");
            sb.append(i);
            sb.append(" because ACTION_DOWN was not received for this pointer before ACTION_MOVE. It likely happened because  ViewDragHelper did not receive all the events in the event stream.");
            Log.e("ViewDragHelper", sb.toString());
            return false;
        }
        return true;
    }
    
    public boolean z(final View view, final int n, final int n2) {
        final boolean b = false;
        if (view == null) {
            return false;
        }
        boolean b2 = b;
        if (n >= view.getLeft()) {
            b2 = b;
            if (n < view.getRight()) {
                b2 = b;
                if (n2 >= view.getTop()) {
                    b2 = b;
                    if (n2 < view.getBottom()) {
                        b2 = true;
                    }
                }
            }
        }
        return b2;
    }
    
    public abstract static class c
    {
        public abstract int clampViewPositionHorizontal(final View p0, final int p1, final int p2);
        
        public abstract int clampViewPositionVertical(final View p0, final int p1, final int p2);
        
        public int getOrderedChildIndex(final int n) {
            return n;
        }
        
        public int getViewHorizontalDragRange(final View view) {
            return 0;
        }
        
        public int getViewVerticalDragRange(final View view) {
            return 0;
        }
        
        public void onEdgeDragStarted(final int n, final int n2) {
        }
        
        public boolean onEdgeLock(final int n) {
            return false;
        }
        
        public void onEdgeTouched(final int n, final int n2) {
        }
        
        public void onViewCaptured(final View view, final int n) {
        }
        
        public abstract void onViewDragStateChanged(final int p0);
        
        public abstract void onViewPositionChanged(final View p0, final int p1, final int p2, final int p3, final int p4);
        
        public abstract void onViewReleased(final View p0, final float p1, final float p2);
        
        public abstract boolean tryCaptureView(final View p0, final int p1);
    }
}

import java.util.HashMap;
import java.lang.annotation.Annotation;
import java.util.Collections;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public final class n00
{
    public final String a;
    public final Map b;
    
    public n00(final String a, final Map b) {
        this.a = a;
        this.b = b;
    }
    
    public static b a(final String s) {
        return new b(s);
    }
    
    public static n00 d(final String s) {
        return new n00(s, Collections.emptyMap());
    }
    
    public String b() {
        return this.a;
    }
    
    public Annotation c(final Class clazz) {
        return this.b.get(clazz);
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof n00)) {
            return false;
        }
        final n00 n00 = (n00)o;
        if (!this.a.equals(n00.a) || !this.b.equals(n00.b)) {
            b = false;
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        return this.a.hashCode() * 31 + this.b.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("FieldDescriptor{name=");
        sb.append(this.a);
        sb.append(", properties=");
        sb.append(this.b.values());
        sb.append("}");
        return sb.toString();
    }
    
    public static final class b
    {
        public final String a;
        public Map b;
        
        public b(final String a) {
            this.b = null;
            this.a = a;
        }
        
        public n00 a() {
            final String a = this.a;
            Map<Object, Object> map;
            if (this.b == null) {
                map = Collections.emptyMap();
            }
            else {
                map = Collections.unmodifiableMap((Map<?, ?>)new HashMap<Object, Object>(this.b));
            }
            return new n00(a, map, null);
        }
        
        public b b(final Annotation annotation) {
            if (this.b == null) {
                this.b = new HashMap();
            }
            this.b.put(annotation.annotationType(), annotation);
            return this;
        }
    }
}

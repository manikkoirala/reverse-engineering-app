import com.ekodroid.omrevaluator.templateui.models.PartialAnswerOptionKey;
import com.ekodroid.omrevaluator.templateui.models.AnswerOptionKey;
import com.ekodroid.omrevaluator.templateui.models.NumericalOptionPayload;
import com.ekodroid.omrevaluator.templateui.models.MatrixOptionPayload;
import java.text.DecimalFormat;
import com.ekodroid.omrevaluator.templateui.models.DecimalRange;
import com.ekodroid.omrevaluator.templateui.models.AnswerOption;
import com.ekodroid.omrevaluator.templateui.models.DecimalOptionPayload;
import com.ekodroid.omrevaluator.database.SheetImageModel;
import java.util.List;
import java.util.Collections;
import com.ekodroid.omrevaluator.templateui.scanner.AnswerValue;
import java.util.Comparator;
import java.util.ArrayList;
import com.ekodroid.omrevaluator.templateui.models.LabelProfile;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class e5
{
    public static boolean[] A(final String s, final LabelProfile labelProfile) {
        final String[] threeOptionLabels = labelProfile.getThreeOptionLabels();
        final boolean[] array = new boolean[threeOptionLabels.length];
        for (int i = 0; i < threeOptionLabels.length; ++i) {
            if (s.contains(threeOptionLabels[i])) {
                array[i] = true;
            }
        }
        return array;
    }
    
    public static String B(final boolean[] array, final LabelProfile labelProfile) {
        final String[] trueOrFalseLabel = labelProfile.getTrueOrFalseLabel();
        String s = "";
        String string;
        for (int i = 0; i < array.length; ++i, s = string) {
            string = s;
            if (array[i]) {
                StringBuilder sb;
                String str;
                if (s.length() > 0) {
                    sb = new StringBuilder();
                    sb.append(s);
                    sb.append(", ");
                    str = trueOrFalseLabel[i];
                }
                else {
                    sb = new StringBuilder();
                    sb.append(s);
                    str = trueOrFalseLabel[i];
                }
                sb.append(str);
                string = sb.toString();
            }
        }
        return s;
    }
    
    public static boolean[] C(final String s, final LabelProfile labelProfile) {
        final String[] trueOrFalseLabel = labelProfile.getTrueOrFalseLabel();
        final boolean[] array = new boolean[trueOrFalseLabel.length];
        for (int i = 0; i < trueOrFalseLabel.length; ++i) {
            if (s.contains(trueOrFalseLabel[i])) {
                array[i] = true;
            }
        }
        return array;
    }
    
    public static void D(final ArrayList list) {
        Collections.sort((List<Object>)list, new Comparator() {
            public int a(final AnswerValue answerValue, final AnswerValue answerValue2) {
                return Integer.compare(answerValue.getQuestionNumber(), answerValue2.getQuestionNumber());
            }
        });
    }
    
    public static void E(final ArrayList list) {
        Collections.sort((List<Object>)list, new Comparator() {
            public int a(final SheetImageModel sheetImageModel, final SheetImageModel sheetImageModel2) {
                return Integer.compare(sheetImageModel.getPageIndex(), sheetImageModel2.getPageIndex());
            }
        });
    }
    
    public static boolean[] a(final boolean[] array) {
        if (array == null) {
            return null;
        }
        final int length = array.length;
        final boolean[] array2 = new boolean[length];
        for (int i = 0; i < length; ++i) {
            array2[i] = array[i];
        }
        return array2;
    }
    
    public static double b(final AnswerValue answerValue) {
        if (answerValue.getMarkedState() == AnswerValue.MarkedState.UNATTEMPTED) {
            return 0.0;
        }
        final boolean[] markedValues = answerValue.getMarkedValues();
        final DecimalOptionPayload b = bc.b(answerValue.getPayload());
        final int digits = b.digits;
        final boolean decimalAllowed = b.decimalAllowed;
        int n;
        if (decimalAllowed) {
            n = 11;
        }
        else {
            n = 10;
        }
        String[] array;
        if (decimalAllowed) {
            array = new String[] { " ", ".", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
        }
        else {
            array = new String[] { " ", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
        }
        String string = "";
        for (int i = 0; i < digits; ++i) {
            int j = 0;
            int n2 = 0;
            while (j < n) {
                if (markedValues[j * digits + i]) {
                    n2 = j + 1;
                }
                ++j;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append(string);
            sb.append(array[n2]);
            string = sb.toString();
        }
        String s2;
        final String s = s2 = string.trim().replaceAll("[ ]", "0");
        if (b.decimalAllowed) {
            s2 = s.replaceFirst("[.]", "#").replaceAll("[.]", "0").replaceAll("[ ]", "0").replaceFirst("[#]", ".");
        }
        if (s2.length() == 0) {
            return 0.0;
        }
        double double1;
        final double n3 = double1 = Double.parseDouble(s2);
        if (b.hasNegetive) {
            double1 = n3;
            if (markedValues[markedValues.length - 1]) {
                double1 = n3 * -1.0;
            }
        }
        return double1;
    }
    
    public static boolean[] c(final String s, final AnswerOption.AnswerOptionType answerOptionType, final String s2, final LabelProfile labelProfile) {
        switch (e5$c.a[answerOptionType.ordinal()]) {
            default: {
                return null;
            }
            case 9: {
                return y(s, labelProfile);
            }
            case 8: {
                return f(s, labelProfile);
            }
            case 7: {
                return w(s, labelProfile);
            }
            case 6: {
                return o(s, labelProfile, s2);
            }
            case 5: {
                return n(s, labelProfile, s2);
            }
            case 4: {
                return C(s, labelProfile);
            }
            case 3: {
                return A(s, labelProfile);
            }
            case 2: {
                return h(s, labelProfile);
            }
            case 1: {
                return k(s, labelProfile);
            }
        }
    }
    
    public static String d(String s) {
        try {
            final String[] split = s.split("[,]");
            if (split.length == 1) {
                final double double1 = Double.parseDouble(split[0]);
                return new gc0().s(new DecimalRange(double1, double1));
            }
            if (split.length == 2) {
                s = new gc0().s(new DecimalRange(Double.parseDouble(split[0]), Double.parseDouble(split[1])));
                return s;
            }
            return null;
        }
        catch (final Exception ex) {
            return null;
        }
    }
    
    public static String e(final boolean[] array, final LabelProfile labelProfile) {
        final String[] eightOptionLabels = labelProfile.getEightOptionLabels();
        String s = "";
        String string;
        for (int i = 0; i < array.length; ++i, s = string) {
            string = s;
            if (array[i]) {
                StringBuilder sb;
                String str;
                if (s.length() > 0) {
                    sb = new StringBuilder();
                    sb.append(s);
                    sb.append(", ");
                    str = eightOptionLabels[i];
                }
                else {
                    sb = new StringBuilder();
                    sb.append(s);
                    str = eightOptionLabels[i];
                }
                sb.append(str);
                string = sb.toString();
            }
        }
        return s;
    }
    
    public static boolean[] f(final String s, final LabelProfile labelProfile) {
        final String[] eightOptionLabels = labelProfile.getEightOptionLabels();
        final boolean[] array = new boolean[eightOptionLabels.length];
        for (int i = 0; i < eightOptionLabels.length; ++i) {
            if (s.contains(eightOptionLabels[i])) {
                array[i] = true;
            }
        }
        return array;
    }
    
    public static String g(final boolean[] array, final LabelProfile labelProfile) {
        final String[] fiveOptionLabels = labelProfile.getFiveOptionLabels();
        String s = "";
        String string;
        for (int i = 0; i < array.length; ++i, s = string) {
            string = s;
            if (array[i]) {
                String str;
                StringBuilder sb2;
                if (s.length() > 0) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(s);
                    sb.append(", ");
                    str = fiveOptionLabels[i];
                    sb2 = sb;
                }
                else {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append(s);
                    str = fiveOptionLabels[i];
                    sb2 = sb3;
                }
                sb2.append(str);
                string = sb2.toString();
            }
        }
        return s;
    }
    
    public static boolean[] h(final String s, final LabelProfile labelProfile) {
        final String[] fiveOptionLabels = labelProfile.getFiveOptionLabels();
        final boolean[] array = new boolean[fiveOptionLabels.length];
        for (int i = 0; i < fiveOptionLabels.length; ++i) {
            if (s.contains(fiveOptionLabels[i])) {
                array[i] = true;
            }
        }
        return array;
    }
    
    public static String i(final double number) {
        return new DecimalFormat("0.#").format(number);
    }
    
    public static String j(final boolean[] array, final LabelProfile labelProfile) {
        final String[] fourOptionLabels = labelProfile.getFourOptionLabels();
        String s = "";
        String string;
        for (int i = 0; i < array.length; ++i, s = string) {
            string = s;
            if (array[i]) {
                String str;
                StringBuilder sb2;
                if (s.length() > 0) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(s);
                    sb.append(", ");
                    str = fourOptionLabels[i];
                    sb2 = sb;
                }
                else {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append(s);
                    final String s2 = fourOptionLabels[i];
                    sb2 = sb3;
                    str = s2;
                }
                sb2.append(str);
                string = sb2.toString();
            }
        }
        return s;
    }
    
    public static boolean[] k(final String s, final LabelProfile labelProfile) {
        final String[] fourOptionLabels = labelProfile.getFourOptionLabels();
        final boolean[] array = new boolean[fourOptionLabels.length];
        for (int i = 0; i < fourOptionLabels.length; ++i) {
            if (s.contains(fourOptionLabels[i])) {
                array[i] = true;
            }
        }
        return array;
    }
    
    public static int l(final char c) {
        try {
            return Integer.parseInt(String.valueOf(c));
        }
        catch (final Exception ex) {
            return 0;
        }
    }
    
    public static String m(final boolean[] array, final LabelProfile labelProfile, String string) {
        final MatrixOptionPayload c = bc.c(string);
        int secondaryOptions;
        if (c != null) {
            secondaryOptions = c.secondaryOptions;
        }
        else {
            secondaryOptions = 5;
        }
        final String[] matrixPrimary = labelProfile.getMatrixPrimary();
        final String[] matrixSecondary = labelProfile.getMatrixSecondary();
        String s = "";
        for (int i = 0; i < array.length; ++i, s = string) {
            final int n = i / secondaryOptions;
            final int n2 = i % secondaryOptions;
            string = s;
            if (array[i]) {
                StringBuilder sb2;
                if (s.length() > 0) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(s);
                    sb.append(", ");
                    sb.append(matrixPrimary[n]);
                    string = matrixSecondary[n2];
                    sb2 = sb;
                }
                else {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append(s);
                    sb3.append(matrixPrimary[n]);
                    final String s2 = matrixSecondary[n2];
                    sb2 = sb3;
                    string = s2;
                }
                sb2.append(string);
                string = sb2.toString();
            }
        }
        return s;
    }
    
    public static boolean[] n(final String s, final LabelProfile labelProfile, final String s2) {
        final MatrixOptionPayload c = bc.c(s2);
        int primaryOptions;
        int secondaryOptions;
        if (c != null) {
            primaryOptions = c.primaryOptions;
            secondaryOptions = c.secondaryOptions;
        }
        else {
            primaryOptions = 4;
            secondaryOptions = 5;
        }
        final int n = primaryOptions * secondaryOptions;
        final boolean[] array = new boolean[n];
        final String[] array2 = new String[n];
        final String[] matrixPrimary = labelProfile.getMatrixPrimary();
        final String[] matrixSecondary = labelProfile.getMatrixSecondary();
        final int n2 = 0;
        int n3 = 0;
        int i;
        while (true) {
            i = n2;
            if (n3 >= n) {
                break;
            }
            final int n4 = n3 / secondaryOptions;
            final StringBuilder sb = new StringBuilder();
            sb.append(matrixPrimary[n4]);
            sb.append(matrixSecondary[n3 % secondaryOptions]);
            array2[n3] = sb.toString();
            ++n3;
        }
        while (i < n) {
            if (s.contains(array2[i])) {
                array[i] = true;
            }
            ++i;
        }
        return array;
    }
    
    public static boolean[] o(String str, final LabelProfile labelProfile, final String s) {
        final NumericalOptionPayload d = bc.d(s);
        int n2;
        final int n = n2 = d.digits * 10;
        if (d.hasNegetive) {
            n2 = n + 1;
        }
        final boolean[] array = new boolean[n2];
        final int n3 = 0;
        String substring = str;
        if (str.charAt(0) == '-') {
            array[n2 - 1] = true;
            substring = str.substring(1);
        }
        final String[] split = substring.split("[.]");
        final int length = split[0].length();
        final int digits = d.digits;
        final int decimalDigits = d.decimalDigits;
        str = "";
        Label_0245: {
            StringBuilder sb;
            if (length > digits - decimalDigits) {
                sb = new StringBuilder();
                sb.append("");
                str = split[0].substring(d.digits - d.decimalDigits);
            }
            else {
                if (split[0].length() != d.digits - d.decimalDigits) {
                    for (int i = 0; i < d.digits - d.decimalDigits - split[0].length(); ++i) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append(str);
                        sb2.append("0");
                        str = sb2.toString();
                    }
                    break Label_0245;
                }
                sb = new StringBuilder();
                sb.append("");
                str = split[0];
            }
            sb.append(str);
            str = sb.toString();
        }
        int j = n3;
        String s2 = str;
        if (d.decimalDigits > 0) {
            if (split.length == 1) {
                int n4 = 0;
                while (true) {
                    j = n3;
                    s2 = str;
                    if (n4 >= d.decimalDigits) {
                        break;
                    }
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append(str);
                    sb3.append("0");
                    str = sb3.toString();
                    ++n4;
                }
            }
            else {
                int index = 0;
                while (true) {
                    j = n3;
                    s2 = str;
                    if (index >= d.decimalDigits) {
                        break;
                    }
                    StringBuilder sb5;
                    if (index < split[1].length()) {
                        final StringBuilder sb4 = new StringBuilder();
                        sb4.append(str);
                        sb4.append(String.valueOf(split[1].charAt(index)));
                        sb5 = sb4;
                    }
                    else {
                        final StringBuilder sb6 = new StringBuilder();
                        sb6.append(str);
                        sb6.append("0");
                        sb5 = sb6;
                    }
                    str = sb5.toString();
                    ++index;
                }
            }
        }
        while (j < s2.length()) {
            array[l(s2.charAt(j)) * d.digits + j] = true;
            ++j;
        }
        return array;
    }
    
    public static String p(final boolean[] array, final LabelProfile labelProfile, String s) {
        final NumericalOptionPayload d = bc.d(s);
        if (d.hasNegetive && array[array.length - 1]) {
            final StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append("-");
            s = sb.toString();
        }
        else {
            s = "";
        }
        final String[] numericalOptionLabels = labelProfile.getNumericalOptionLabels();
        int n = 0;
        String string = s;
        while (true) {
            final int digits = d.digits;
            if (n >= digits) {
                break;
            }
            s = string;
            if (digits - d.decimalDigits == n) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(string);
                sb2.append(".");
                s = sb2.toString();
            }
            int i = 0;
            int n2 = -1;
            while (i < 10) {
                if (array[d.digits * i + n]) {
                    n2 = i;
                }
                ++i;
            }
            String str;
            StringBuilder sb4;
            if (n2 > -1) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(s);
                sb3.append("");
                str = numericalOptionLabels[n2];
                sb4 = sb3;
            }
            else {
                final StringBuilder sb5 = new StringBuilder();
                sb5.append(s);
                str = " ";
                sb4 = sb5;
            }
            sb4.append(str);
            string = sb4.toString();
            ++n;
        }
        return string;
    }
    
    public static String q(final AnswerOptionKey answerOptionKey, final LabelProfile labelProfile) {
        if (answerOptionKey.getType() == AnswerOption.AnswerOptionType.DECIMAL) {
            return u(answerOptionKey.getMarkedPayload());
        }
        return s(answerOptionKey.getMarkedValues(), answerOptionKey.getType(), answerOptionKey.getPayload(), labelProfile);
    }
    
    public static String r(final AnswerValue answerValue, final LabelProfile labelProfile) {
        if (answerValue.getType() == AnswerOption.AnswerOptionType.DECIMAL) {
            if (answerValue.getMarkedState() == AnswerValue.MarkedState.UNATTEMPTED) {
                return "";
            }
            try {
                return i(b(answerValue));
            }
            catch (final Exception ex) {
                return "";
            }
        }
        return s(answerValue.getMarkedValues(), answerValue.getType(), answerValue.getPayload(), labelProfile);
    }
    
    public static String s(final boolean[] array, final AnswerOption.AnswerOptionType answerOptionType, final String s, final LabelProfile labelProfile) {
        switch (e5$c.a[answerOptionType.ordinal()]) {
            default: {
                return null;
            }
            case 9: {
                return x(array, labelProfile);
            }
            case 8: {
                return e(array, labelProfile);
            }
            case 7: {
                return v(array, labelProfile);
            }
            case 6: {
                return p(array, labelProfile, s);
            }
            case 5: {
                return m(array, labelProfile, s);
            }
            case 4: {
                return B(array, labelProfile);
            }
            case 3: {
                return z(array, labelProfile);
            }
            case 2: {
                return g(array, labelProfile);
            }
            case 1: {
                return j(array, labelProfile);
            }
        }
    }
    
    public static String t(final PartialAnswerOptionKey partialAnswerOptionKey, final LabelProfile labelProfile) {
        if (partialAnswerOptionKey.getType() == AnswerOption.AnswerOptionType.DECIMAL) {
            return u(partialAnswerOptionKey.getMarkedPayload());
        }
        return s(partialAnswerOptionKey.getMarkedValues(), partialAnswerOptionKey.getType(), partialAnswerOptionKey.getPayload(), labelProfile);
    }
    
    public static String u(final String s) {
        final DecimalRange decimalRange = (DecimalRange)new gc0().j(s, DecimalRange.class);
        if (decimalRange.getLower() == decimalRange.getUpper()) {
            return i(decimalRange.getLower());
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(i(decimalRange.getLower()));
        sb.append(",");
        sb.append(i(decimalRange.getUpper()));
        return sb.toString();
    }
    
    public static String v(final boolean[] array, final LabelProfile labelProfile) {
        final String[] sixOptionLabels = labelProfile.getSixOptionLabels();
        String s = "";
        String string;
        for (int i = 0; i < array.length; ++i, s = string) {
            string = s;
            if (array[i]) {
                StringBuilder sb;
                String str;
                if (s.length() > 0) {
                    sb = new StringBuilder();
                    sb.append(s);
                    sb.append(", ");
                    str = sixOptionLabels[i];
                }
                else {
                    sb = new StringBuilder();
                    sb.append(s);
                    str = sixOptionLabels[i];
                }
                sb.append(str);
                string = sb.toString();
            }
        }
        return s;
    }
    
    public static boolean[] w(final String s, final LabelProfile labelProfile) {
        final String[] sixOptionLabels = labelProfile.getSixOptionLabels();
        final boolean[] array = new boolean[sixOptionLabels.length];
        for (int i = 0; i < sixOptionLabels.length; ++i) {
            if (s.contains(sixOptionLabels[i])) {
                array[i] = true;
            }
        }
        return array;
    }
    
    public static String x(final boolean[] array, final LabelProfile labelProfile) {
        final String[] tenOptionLabels = labelProfile.getTenOptionLabels();
        String s = "";
        String string;
        for (int i = 0; i < array.length; ++i, s = string) {
            string = s;
            if (array[i]) {
                StringBuilder sb;
                String str;
                if (s.length() > 0) {
                    sb = new StringBuilder();
                    sb.append(s);
                    sb.append(", ");
                    str = tenOptionLabels[i];
                }
                else {
                    sb = new StringBuilder();
                    sb.append(s);
                    str = tenOptionLabels[i];
                }
                sb.append(str);
                string = sb.toString();
            }
        }
        return s;
    }
    
    public static boolean[] y(final String s, final LabelProfile labelProfile) {
        final String[] tenOptionLabels = labelProfile.getTenOptionLabels();
        final boolean[] array = new boolean[tenOptionLabels.length];
        for (int i = 0; i < tenOptionLabels.length; ++i) {
            if (s.contains(tenOptionLabels[i])) {
                array[i] = true;
            }
        }
        return array;
    }
    
    public static String z(final boolean[] array, final LabelProfile labelProfile) {
        final String[] threeOptionLabels = labelProfile.getThreeOptionLabels();
        String s = "";
        String string;
        for (int i = 0; i < array.length; ++i, s = string) {
            string = s;
            if (array[i]) {
                StringBuilder sb;
                String str;
                if (s.length() > 0) {
                    sb = new StringBuilder();
                    sb.append(s);
                    sb.append(", ");
                    str = threeOptionLabels[i];
                }
                else {
                    sb = new StringBuilder();
                    sb.append(s);
                    str = threeOptionLabels[i];
                }
                sb.append(str);
                string = sb.toString();
            }
        }
        return s;
    }
}

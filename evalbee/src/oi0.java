import java.util.Iterator;
import com.google.firebase.crashlytics.internal.common.CommonUtils;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public class oi0
{
    public final Map a;
    public final int b;
    public final int c;
    
    public oi0(final int b, final int c) {
        this.a = new HashMap();
        this.b = b;
        this.c = c;
    }
    
    public static String c(String s, final int endIndex) {
        String s2 = s;
        if (s != null) {
            s = (s2 = s.trim());
            if (s.length() > endIndex) {
                s2 = s.substring(0, endIndex);
            }
        }
        return s2;
    }
    
    public Map a() {
        synchronized (this) {
            return Collections.unmodifiableMap((Map<?, ?>)new HashMap<Object, Object>(this.a));
        }
    }
    
    public final String b(final String s) {
        if (s != null) {
            return c(s, this.c);
        }
        throw new IllegalArgumentException("Custom attribute key must not be null.");
    }
    
    public boolean d(String c, final String s) {
        synchronized (this) {
            final String b = this.b(c);
            if (this.a.size() >= this.b && !this.a.containsKey(b)) {
                final zl0 f = zl0.f();
                final StringBuilder sb = new StringBuilder();
                sb.append("Ignored entry \"");
                sb.append(c);
                sb.append("\" when adding custom keys. Maximum allowable: ");
                sb.append(this.b);
                f.k(sb.toString());
                return false;
            }
            c = c(s, this.c);
            if (CommonUtils.y((String)this.a.get(b), c)) {
                return false;
            }
            final Map a = this.a;
            if (s == null) {
                c = "";
            }
            a.put(b, c);
            return true;
        }
    }
    
    public void e(final Map map) {
        synchronized (this) {
            final Iterator iterator = map.entrySet().iterator();
            int i = 0;
            while (iterator.hasNext()) {
                final Map.Entry<String, V> entry = (Map.Entry<String, V>)iterator.next();
                final String b = this.b(entry.getKey());
                if (this.a.size() >= this.b && !this.a.containsKey(b)) {
                    ++i;
                }
                else {
                    final String s = (String)entry.getValue();
                    final Map a = this.a;
                    String c;
                    if (s == null) {
                        c = "";
                    }
                    else {
                        c = c(s, this.c);
                    }
                    a.put(b, c);
                }
            }
            if (i > 0) {
                final zl0 f = zl0.f();
                final StringBuilder sb = new StringBuilder();
                sb.append("Ignored ");
                sb.append(i);
                sb.append(" entries when adding custom keys. Maximum allowable: ");
                sb.append(this.b);
                f.k(sb.toString());
            }
        }
    }
}

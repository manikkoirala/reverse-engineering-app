import java.lang.reflect.Proxy;
import java.lang.reflect.InvocationHandler;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class zc1
{
    public static Object a(final Class clazz, final InvocationHandler h) {
        i71.r(h);
        i71.m(clazz.isInterface(), "%s is not an interface", clazz);
        return clazz.cast(Proxy.newProxyInstance(clazz.getClassLoader(), new Class[] { clazz }, h));
    }
}

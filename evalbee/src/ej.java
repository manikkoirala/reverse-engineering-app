import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.k;
import android.os.Bundle;
import android.view.View;
import android.view.Window$Callback;
import android.view.KeyEvent;
import android.os.Build$VERSION;
import androidx.lifecycle.g;
import android.app.Activity;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class ej extends Activity implements qj0, mi0.a
{
    private co1 mExtraDataMap;
    private g mLifecycleRegistry;
    
    public ej() {
        this.mExtraDataMap = new co1();
        this.mLifecycleRegistry = new g(this);
    }
    
    public static boolean c(final String[] array) {
        final boolean b = false;
        final boolean b2 = false;
        final boolean b3 = false;
        boolean b4 = b2;
        if (array != null) {
            b4 = b2;
            if (array.length > 0) {
                final String s = array[0];
                s.hashCode();
                final int hashCode = s.hashCode();
                int n = -1;
                switch (hashCode) {
                    case 1455016274: {
                        if (!s.equals("--autofill")) {
                            break;
                        }
                        n = 4;
                        break;
                    }
                    case 1159329357: {
                        if (!s.equals("--contentcapture")) {
                            break;
                        }
                        n = 3;
                        break;
                    }
                    case 472614934: {
                        if (!s.equals("--list-dumpables")) {
                            break;
                        }
                        n = 2;
                        break;
                    }
                    case 100470631: {
                        if (!s.equals("--dump-dumpable")) {
                            break;
                        }
                        n = 1;
                        break;
                    }
                    case -645125871: {
                        if (!s.equals("--translation")) {
                            break;
                        }
                        n = 0;
                        break;
                    }
                }
                switch (n) {
                    default: {
                        b4 = b2;
                        break;
                    }
                    case 4: {
                        boolean b5 = b3;
                        if (Build$VERSION.SDK_INT >= 26) {
                            b5 = true;
                        }
                        return b5;
                    }
                    case 3: {
                        boolean b6 = b;
                        if (Build$VERSION.SDK_INT >= 29) {
                            b6 = true;
                        }
                        return b6;
                    }
                    case 1:
                    case 2: {
                        return yc.c();
                    }
                    case 0: {
                        b4 = b2;
                        if (Build$VERSION.SDK_INT >= 31) {
                            b4 = true;
                            break;
                        }
                        break;
                    }
                }
            }
        }
        return b4;
    }
    
    public boolean dispatchKeyEvent(final KeyEvent keyEvent) {
        final View decorView = this.getWindow().getDecorView();
        return (decorView != null && mi0.d(decorView, keyEvent)) || mi0.e((mi0.a)this, decorView, (Window$Callback)this, keyEvent);
    }
    
    public boolean dispatchKeyShortcutEvent(final KeyEvent keyEvent) {
        final View decorView = this.getWindow().getDecorView();
        return (decorView != null && mi0.d(decorView, keyEvent)) || super.dispatchKeyShortcutEvent(keyEvent);
    }
    
    @Deprecated
    public <T extends a> T getExtraData(final Class<T> clazz) {
        zu0.a(this.mExtraDataMap.get(clazz));
        return null;
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        k.e(this);
    }
    
    public void onSaveInstanceState(final Bundle bundle) {
        this.mLifecycleRegistry.j(Lifecycle.State.CREATED);
        super.onSaveInstanceState(bundle);
    }
    
    @Deprecated
    public void putExtraData(final a a) {
        throw null;
    }
    
    public final boolean shouldDumpInternalState(final String[] array) {
        return c(array) ^ true;
    }
    
    public boolean superDispatchKeyEvent(final KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent);
    }
    
    public abstract static class a
    {
    }
}

import android.os.BaseBundle;
import android.widget.ImageView;
import com.ekodroid.omrevaluator.database.ResultDataJsonModel;
import com.ekodroid.omrevaluator.database.repositories.ResultRepository;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.widget.ListAdapter;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemSelectedListener;
import android.widget.SpinnerAdapter;
import android.widget.ArrayAdapter;
import android.view.MenuItem;
import android.view.View;
import android.view.View$OnClickListener;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.ekodroid.omrevaluator.util.DataModels.SortAnswerResponseList;
import java.util.ArrayList;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.ListView;
import android.content.Context;
import androidx.fragment.app.Fragment;

// 
// Decompiled by Procyon v0.6.0
// 

public class g5 extends Fragment
{
    public Context a;
    public ListView b;
    public Spinner c;
    public ImageButton d;
    public ImageButton e;
    public ArrayList f;
    public i3 g;
    public SortAnswerResponseList.AnswerResponseSortBy h;
    public boolean i;
    public FirebaseAnalytics j;
    public ArrayList k;
    public SheetTemplate2 l;
    public ExamId m;
    public int n;
    public f5.b p;
    
    public g5() {
        this.f = new ArrayList();
        this.h = SortAnswerResponseList.AnswerResponseSortBy.QUESTION;
        this.i = true;
        this.n = 0;
        this.p = new f5.b(this) {
            public final g5 a;
            
            @Override
            public void a(final ArrayList f) {
                final g5 a = this.a;
                a.f = f;
                a.n();
            }
        };
    }
    
    public static /* synthetic */ int h(final g5 g5) {
        return g5.n;
    }
    
    public static /* synthetic */ int i(final g5 g5, final int n) {
        return g5.n = n;
    }
    
    public static /* synthetic */ f5.b j(final g5 g5) {
        return g5.p;
    }
    
    public final void k() {
        ((View)this.e).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final g5 a;
            
            public void onClick(final View view) {
                final b61 b61 = new b61(this.a.a, view);
                b61.b(2131623940);
                b61.d((b61.c)new b61.c(this) {
                    public final g5$d a;
                    
                    @Override
                    public boolean onMenuItemClick(final MenuItem menuItem) {
                        while (true) {
                            g5 g5 = null;
                            SortAnswerResponseList.AnswerResponseSortBy h = null;
                            Label_0062: {
                                switch (menuItem.getItemId()) {
                                    default: {
                                        g5 = this.a.a;
                                        break;
                                    }
                                    case 2131296340: {
                                        g5 = this.a.a;
                                        h = SortAnswerResponseList.AnswerResponseSortBy.UNATTEMPTED;
                                        break Label_0062;
                                    }
                                    case 2131296339: {
                                        g5 = this.a.a;
                                        h = SortAnswerResponseList.AnswerResponseSortBy.QUESTION;
                                        break Label_0062;
                                    }
                                    case 2131296338: {
                                        g5 = this.a.a;
                                        h = SortAnswerResponseList.AnswerResponseSortBy.INCORRECT;
                                        break Label_0062;
                                    }
                                    case 2131296337: {
                                        g5 = this.a.a;
                                        h = SortAnswerResponseList.AnswerResponseSortBy.CORRECT;
                                        break Label_0062;
                                    }
                                }
                                g5.n();
                                return true;
                            }
                            g5.h = h;
                            continue;
                        }
                    }
                });
                b61.e();
            }
        });
    }
    
    public final void l() {
        ((View)this.d).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final g5 a;
            
            public void onClick(final View view) {
                final g5 a = this.a;
                final boolean i = a.i ^ true;
                a.i = i;
                final ImageButton d = a.d;
                int imageResource;
                if (i) {
                    imageResource = 2131230933;
                }
                else {
                    imageResource = 2131230934;
                }
                ((ImageView)d).setImageResource(imageResource);
                this.a.n();
            }
        });
    }
    
    public final void m(final int n, final String[] array) {
        final String[] array2 = new String[n];
        for (int i = 0; i < n; ++i) {
            array2[i] = array[i];
        }
        this.c.setAdapter((SpinnerAdapter)new ArrayAdapter(this.a, 2131493104, (Object[])array2));
        ((AdapterView)this.c).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener(this) {
            public final g5 a;
            
            public void onItemSelected(final AdapterView adapterView, final View view, int h, final long n) {
                g5.i(this.a, h + 1);
                h = g5.h(this.a);
                final f5.b j = g5.j(this.a);
                final g5 a = this.a;
                new f5(h, j, a.l, a.k);
            }
            
            public void onNothingSelected(final AdapterView adapterView) {
            }
        });
    }
    
    public final void n() {
        new SortAnswerResponseList().e(this.h, this.f, this.i);
        final i3 i3 = new i3(this.a, this.f, this.h);
        this.g = i3;
        this.b.setAdapter((ListAdapter)i3);
    }
    
    @Override
    public void onAttach(final Context a) {
        super.onAttach(a);
        this.a = a;
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        final View inflate = layoutInflater.inflate(2131493009, viewGroup, false);
        this.j = FirebaseAnalytics.getInstance(this.a);
        this.b = (ListView)inflate.findViewById(2131296836);
        this.c = (Spinner)inflate.findViewById(2131297096);
        this.d = (ImageButton)inflate.findViewById(2131296676);
        this.e = (ImageButton)inflate.findViewById(2131296675);
        if (this.getArguments() != null) {
            this.m = (ExamId)this.getArguments().getSerializable("EXAM_ID");
            ArrayList<ResultDataJsonModel> k;
            if ("ARCHIVED".equals(((BaseBundle)this.getArguments()).getString("EXAM_TYPE"))) {
                this.l = e8.a;
                k = e8.a();
            }
            else {
                this.l = TemplateRepository.getInstance(this.a).getTemplateJson(this.m).getSheetTemplate();
                k = ResultRepository.getInstance(this.a).getAllResultJson(this.m);
            }
            this.k = k;
            final int examSets = this.l.getTemplateParams().getExamSets();
            if (examSets == 1) {
                this.n = 0;
                inflate.findViewById(2131297280).setVisibility(8);
                ((View)this.c).setVisibility(8);
            }
            else {
                this.m(examSets, this.l.getLabelProfile().getExamSetLabels());
            }
            new f5(this.n, this.p, this.l, this.k);
        }
        this.l();
        this.k();
        return inflate;
    }
}

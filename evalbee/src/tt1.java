import androidx.emoji2.text.flatbuffer.Utf8;
import java.nio.ByteBuffer;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class tt1
{
    public int a;
    public ByteBuffer b;
    public int c;
    public int d;
    public Utf8 e;
    
    public tt1() {
        this.e = Utf8.a();
    }
    
    public int a(final int n) {
        return n + this.b.getInt(n);
    }
    
    public int b(int short1) {
        if (short1 < this.d) {
            short1 = this.b.getShort(this.c + short1);
        }
        else {
            short1 = 0;
        }
        return short1;
    }
    
    public void c(int short1, final ByteBuffer b) {
        this.b = b;
        if (b != null) {
            this.a = short1;
            short1 -= b.getInt(short1);
            this.c = short1;
            short1 = this.b.getShort(short1);
        }
        else {
            short1 = 0;
            this.a = 0;
            this.c = 0;
        }
        this.d = short1;
    }
    
    public int d(int n) {
        n += this.a;
        return n + this.b.getInt(n) + 4;
    }
    
    public int e(int n) {
        n += this.a;
        return this.b.getInt(n + this.b.getInt(n));
    }
}

import com.google.firebase.firestore.model.MutableDocument;

// 
// Decompiled by Procyon v0.6.0
// 

public final class h71
{
    public static final h71 c;
    public final qo1 a;
    public final Boolean b;
    
    static {
        c = new h71(null, null);
    }
    
    public h71(final qo1 a, final Boolean b) {
        g9.d(a == null || b == null, "Precondition can specify \"exists\" or \"updateTime\" but not both", new Object[0]);
        this.a = a;
        this.b = b;
    }
    
    public static h71 a(final boolean b) {
        return new h71(null, b);
    }
    
    public static h71 f(final qo1 qo1) {
        return new h71(qo1, null);
    }
    
    public Boolean b() {
        return this.b;
    }
    
    public qo1 c() {
        return this.a;
    }
    
    public boolean d() {
        return this.a == null && this.b == null;
    }
    
    public boolean e(final MutableDocument mutableDocument) {
        final qo1 a = this.a;
        boolean b = true;
        final boolean b2 = true;
        if (a != null) {
            return mutableDocument.d() && mutableDocument.getVersion().equals(this.a) && b2;
        }
        final Boolean b3 = this.b;
        if (b3 != null) {
            if (b3 != mutableDocument.d()) {
                b = false;
            }
            return b;
        }
        g9.d(this.d(), "Precondition should be empty", new Object[0]);
        return true;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean equals = true;
        if (this == o) {
            return true;
        }
        if (o != null && h71.class == o.getClass()) {
            final h71 h71 = (h71)o;
            final qo1 a = this.a;
            Label_0062: {
                if (a != null) {
                    if (a.equals(h71.a)) {
                        break Label_0062;
                    }
                }
                else if (h71.a == null) {
                    break Label_0062;
                }
                return false;
            }
            final Boolean b = this.b;
            final Boolean b2 = h71.b;
            if (b != null) {
                equals = b.equals(b2);
            }
            else if (b2 != null) {
                equals = false;
            }
            return equals;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        final qo1 a = this.a;
        int hashCode = 0;
        int hashCode2;
        if (a != null) {
            hashCode2 = a.hashCode();
        }
        else {
            hashCode2 = 0;
        }
        final Boolean b = this.b;
        if (b != null) {
            hashCode = b.hashCode();
        }
        return hashCode2 * 31 + hashCode;
    }
    
    @Override
    public String toString() {
        if (this.d()) {
            return "Precondition{<none>}";
        }
        StringBuilder sb;
        Comparable obj;
        if (this.a != null) {
            sb = new StringBuilder();
            sb.append("Precondition{updateTime=");
            obj = this.a;
        }
        else {
            if (this.b == null) {
                throw g9.a("Invalid Precondition", new Object[0]);
            }
            sb = new StringBuilder();
            sb.append("Precondition{exists=");
            obj = this.b;
        }
        sb.append(obj);
        sb.append("}");
        return sb.toString();
    }
}

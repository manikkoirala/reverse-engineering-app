import android.os.IInterface;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Handler;
import android.os.Parcelable$Creator;
import android.os.Parcelable;

// 
// Decompiled by Procyon v0.6.0
// 

public class af1 implements Parcelable
{
    public static final Parcelable$Creator<af1> CREATOR;
    public final boolean a;
    public final Handler b;
    public xd0 c;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator() {
            public af1 a(final Parcel parcel) {
                return new af1(parcel);
            }
            
            public af1[] b(final int n) {
                return new af1[n];
            }
        };
    }
    
    public af1(final Parcel parcel) {
        this.a = false;
        this.b = null;
        this.c = xd0.a.p(parcel.readStrongBinder());
    }
    
    public void b(final int n, final Bundle bundle) {
    }
    
    public int describeContents() {
        return 0;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        synchronized (this) {
            if (this.c == null) {
                this.c = new b();
            }
            parcel.writeStrongBinder(((IInterface)this.c).asBinder());
        }
    }
    
    public class b extends xd0.a
    {
        public final af1 a;
        
        public b(final af1 a) {
            this.a = a;
        }
        
        public void e(final int n, final Bundle bundle) {
            final af1 a = this.a;
            final Handler b = a.b;
            if (b != null) {
                b.post((Runnable)a.new c(n, bundle));
            }
            else {
                a.b(n, bundle);
            }
        }
    }
    
    public class c implements Runnable
    {
        public final int a;
        public final Bundle b;
        public final af1 c;
        
        public c(final af1 c, final int a, final Bundle b) {
            this.c = c;
            this.a = a;
            this.b = b;
        }
        
        @Override
        public void run() {
            this.c.b(this.a, this.b);
        }
    }
}

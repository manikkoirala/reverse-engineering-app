import android.util.SparseArray;
import com.google.firebase.firestore.core.q;
import java.util.Iterator;
import com.google.firebase.database.collection.c;
import java.util.HashMap;
import com.google.firebase.firestore.local.e;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public final class iv0 implements yt1
{
    public final Map a;
    public final xc1 b;
    public int c;
    public qo1 d;
    public long e;
    public final e f;
    
    public iv0(final e f) {
        this.a = new HashMap();
        this.b = new xc1();
        this.d = qo1.b;
        this.e = 0L;
        this.f = f;
    }
    
    @Override
    public void a(final au1 au1) {
        this.e(au1);
    }
    
    @Override
    public void b(final c c, final int n) {
        this.b.g(c, n);
        final wc1 f = this.f.f();
        final Iterator iterator = c.iterator();
        while (iterator.hasNext()) {
            f.a((du)iterator.next());
        }
    }
    
    @Override
    public au1 c(final q q) {
        return this.a.get(q);
    }
    
    @Override
    public int d() {
        return this.c;
    }
    
    @Override
    public void e(final au1 au1) {
        this.a.put(au1.g(), au1);
        final int h = au1.h();
        if (h > this.c) {
            this.c = h;
        }
        if (au1.e() > this.e) {
            this.e = au1.e();
        }
    }
    
    @Override
    public void f(final c c, final int n) {
        this.b.b(c, n);
        final wc1 f = this.f.f();
        final Iterator iterator = c.iterator();
        while (iterator.hasNext()) {
            f.c((du)iterator.next());
        }
    }
    
    @Override
    public void g(final qo1 d) {
        this.d = d;
    }
    
    @Override
    public c h(final int n) {
        return this.b.d(n);
    }
    
    @Override
    public qo1 i() {
        return this.d;
    }
    
    public boolean j(final du du) {
        return this.b.c(du);
    }
    
    public void k(final cl cl) {
        final Iterator iterator = this.a.values().iterator();
        while (iterator.hasNext()) {
            cl.accept(iterator.next());
        }
    }
    
    public long l(final zk0 zk0) {
        final Iterator iterator = this.a.entrySet().iterator();
        long n = 0L;
        while (iterator.hasNext()) {
            n += zk0.o(((Map.Entry<K, au1>)iterator.next()).getValue()).b();
        }
        return n;
    }
    
    public long m() {
        return this.e;
    }
    
    public long n() {
        return this.a.size();
    }
    
    public void o(final int n) {
        this.b.h(n);
    }
    
    public int p(final long n, final SparseArray sparseArray) {
        final Iterator iterator = this.a.entrySet().iterator();
        int n2 = 0;
        while (iterator.hasNext()) {
            final Map.Entry<K, au1> entry = (Map.Entry<K, au1>)iterator.next();
            final int h = entry.getValue().h();
            if (entry.getValue().e() <= n && sparseArray.get(h) == null) {
                iterator.remove();
                this.o(h);
                ++n2;
            }
        }
        return n2;
    }
    
    public void q(final au1 au1) {
        this.a.remove(au1.g());
        this.b.h(au1.h());
    }
}

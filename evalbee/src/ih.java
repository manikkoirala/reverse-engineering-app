import com.google.firebase.firestore.a;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

// 
// Decompiled by Procyon v0.6.0
// 

public class ih extends Query
{
    public ih(final ke1 ke1, final FirebaseFirestore firebaseFirestore) {
        super(com.google.firebase.firestore.core.Query.b(ke1), firebaseFirestore);
        if (ke1.l() % 2 == 1) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid collection reference. Collection references must have an odd number of segments, but ");
        sb.append(ke1.d());
        sb.append(" has ");
        sb.append(ke1.l());
        throw new IllegalArgumentException(sb.toString());
    }
    
    public a g(final String s) {
        k71.c(s, "Provided document path must not be null.");
        return com.google.firebase.firestore.a.g((ke1)super.a.l().a(ke1.q(s)), super.b);
    }
}

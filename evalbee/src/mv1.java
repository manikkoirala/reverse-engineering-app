import android.text.TextUtils;
import java.util.Locale;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class mv1
{
    public static final Locale a;
    
    static {
        a = new Locale("", "");
    }
    
    public static int a(final Locale locale) {
        return mv1.a.a(locale);
    }
    
    public abstract static class a
    {
        public static int a(final Locale locale) {
            return TextUtils.getLayoutDirectionFromLocale(locale);
        }
    }
}

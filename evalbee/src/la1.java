import java.util.logging.Level;
import java.io.InputStream;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;
import java.util.NoSuchElementException;
import java.io.IOException;
import java.io.File;
import java.io.RandomAccessFile;
import java.util.logging.Logger;
import java.io.Closeable;

// 
// Decompiled by Procyon v0.6.0
// 

public class la1 implements Closeable
{
    public static final Logger g;
    public final RandomAccessFile a;
    public int b;
    public int c;
    public b d;
    public b e;
    public final byte[] f;
    
    static {
        g = Logger.getLogger(la1.class.getName());
    }
    
    public la1(final File file) {
        this.f = new byte[16];
        if (!file.exists()) {
            j(file);
        }
        this.a = q(file);
        this.x();
    }
    
    public static int E(final byte[] array, final int n) {
        return ((array[n] & 0xFF) << 24) + ((array[n + 1] & 0xFF) << 16) + ((array[n + 2] & 0xFF) << 8) + (array[n + 3] & 0xFF);
    }
    
    public static /* synthetic */ RandomAccessFile d(final la1 la1) {
        return la1.a;
    }
    
    public static void j(final File dest) {
        final StringBuilder sb = new StringBuilder();
        sb.append(dest.getPath());
        sb.append(".tmp");
        final File file = new File(sb.toString());
        final RandomAccessFile q = q(file);
        try {
            q.setLength(4096L);
            q.seek(0L);
            final byte[] b = new byte[16];
            p0(b, 4096, 0, 0, 0);
            q.write(b);
            q.close();
            if (file.renameTo(dest)) {
                return;
            }
            throw new IOException("Rename failed!");
        }
        finally {
            q.close();
        }
    }
    
    public static Object o(final Object o, final String s) {
        if (o != null) {
            return o;
        }
        throw new NullPointerException(s);
    }
    
    public static void o0(final byte[] array, final int n, final int n2) {
        array[n] = (byte)(n2 >> 24);
        array[n + 1] = (byte)(n2 >> 16);
        array[n + 2] = (byte)(n2 >> 8);
        array[n + 3] = (byte)n2;
    }
    
    public static void p0(final byte[] array, final int... array2) {
        final int length = array2.length;
        int i = 0;
        int n = 0;
        while (i < length) {
            o0(array, n, array2[i]);
            n += 4;
            ++i;
        }
    }
    
    public static RandomAccessFile q(final File file) {
        return new RandomAccessFile(file, "rwd");
    }
    
    public final int H() {
        return this.b - this.Z();
    }
    
    public void J() {
        synchronized (this) {
            if (!this.k()) {
                if (this.c == 1) {
                    this.g();
                }
                else {
                    final b d = this.d;
                    final int i0 = this.i0(d.a + 4 + d.b);
                    this.K(i0, this.f, 0, 4);
                    final int e = E(this.f, 0);
                    this.m0(this.b, this.c - 1, i0, this.e.a);
                    --this.c;
                    this.d = new b(i0, e);
                }
                return;
            }
            throw new NoSuchElementException();
        }
    }
    
    public final void K(int i0, final byte[] array, int n, int len) {
        i0 = this.i0(i0);
        final int b = this.b;
        RandomAccessFile randomAccessFile;
        if (i0 + len <= b) {
            this.a.seek(i0);
            randomAccessFile = this.a;
        }
        else {
            final int len2 = b - i0;
            this.a.seek(i0);
            this.a.readFully(array, n, len2);
            this.a.seek(16L);
            randomAccessFile = this.a;
            n += len2;
            len -= len2;
        }
        randomAccessFile.readFully(array, n, len);
    }
    
    public final void O(int b, final byte[] array, int n, int len) {
        final int i0 = this.i0(b);
        b = this.b;
        RandomAccessFile randomAccessFile;
        if (i0 + len <= b) {
            this.a.seek(i0);
            randomAccessFile = this.a;
        }
        else {
            b -= i0;
            this.a.seek(i0);
            this.a.write(array, n, b);
            this.a.seek(16L);
            randomAccessFile = this.a;
            n += b;
            len -= b;
        }
        randomAccessFile.write(array, n, len);
    }
    
    public final void R(final int n) {
        this.a.setLength(n);
        this.a.getChannel().force(true);
    }
    
    public int Z() {
        if (this.c == 0) {
            return 16;
        }
        final b e = this.e;
        final int a = e.a;
        final int a2 = this.d.a;
        if (a >= a2) {
            return a - a2 + 4 + e.b + 16;
        }
        return a + 4 + e.b + this.b - a2;
    }
    
    @Override
    public void close() {
        synchronized (this) {
            this.a.close();
        }
    }
    
    public void e(final byte[] array) {
        this.f(array, 0, array.length);
    }
    
    public void f(final byte[] array, int n, final int n2) {
        synchronized (this) {
            o(array, "buffer");
            if ((n | n2) >= 0 && n2 <= array.length - n) {
                this.h(n2);
                final boolean k = this.k();
                int i0;
                if (k) {
                    i0 = 16;
                }
                else {
                    final b e = this.e;
                    i0 = this.i0(e.a + 4 + e.b);
                }
                final b b = new b(i0, n2);
                o0(this.f, 0, n2);
                this.O(b.a, this.f, 0, 4);
                this.O(b.a + 4, array, n, n2);
                if (k) {
                    n = b.a;
                }
                else {
                    n = this.d.a;
                }
                this.m0(this.b, this.c + 1, n, b.a);
                this.e = b;
                ++this.c;
                if (k) {
                    this.d = b;
                }
                return;
            }
            throw new IndexOutOfBoundsException();
        }
    }
    
    public void g() {
        synchronized (this) {
            this.m0(4096, 0, 0, 0);
            this.c = 0;
            final b c = la1.b.c;
            this.d = c;
            this.e = c;
            if (this.b > 4096) {
                this.R(4096);
            }
            this.b = 4096;
        }
    }
    
    public final void h(int n) {
        final int n2 = n + 4;
        int h = this.H();
        if (h >= n2) {
            return;
        }
        n = this.b;
        int i;
        int b;
        do {
            i = h + n;
            b = n << 1;
            h = i;
            n = b;
        } while (i < n2);
        this.R(b);
        final b e = this.e;
        n = this.i0(e.a + 4 + e.b);
        if (n < this.d.a) {
            final FileChannel channel = this.a.getChannel();
            channel.position(this.b);
            final long n3 = n - 4;
            if (channel.transferTo(16L, n3, channel) != n3) {
                throw new AssertionError((Object)"Copied insufficient number of bytes!");
            }
        }
        final int a = this.e.a;
        n = this.d.a;
        if (a < n) {
            final int n4 = this.b + a - 16;
            this.m0(b, this.c, n, n4);
            this.e = new b(n4, this.e.b);
        }
        else {
            this.m0(b, this.c, n, a);
        }
        this.b = b;
    }
    
    public void i(final d d) {
        synchronized (this) {
            int n = this.d.a;
            for (int i = 0; i < this.c; ++i) {
                final b u = this.u(n);
                d.a(new c(u, null), u.b);
                n = this.i0(u.a + 4 + u.b);
            }
        }
    }
    
    public final int i0(int n) {
        final int b = this.b;
        if (n >= b) {
            n = n + 16 - b;
        }
        return n;
    }
    
    public boolean k() {
        synchronized (this) {
            return this.c == 0;
        }
    }
    
    public final void m0(final int n, final int n2, final int n3, final int n4) {
        p0(this.f, n, n2, n3, n4);
        this.a.seek(0L);
        this.a.write(this.f);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getSimpleName());
        sb.append('[');
        sb.append("fileLength=");
        sb.append(this.b);
        sb.append(", size=");
        sb.append(this.c);
        sb.append(", first=");
        sb.append(this.d);
        sb.append(", last=");
        sb.append(this.e);
        sb.append(", element lengths=[");
        try {
            this.i((d)new d(this, sb) {
                public boolean a = true;
                public final StringBuilder b;
                public final la1 c;
                
                @Override
                public void a(final InputStream inputStream, final int i) {
                    if (this.a) {
                        this.a = false;
                    }
                    else {
                        this.b.append(", ");
                    }
                    this.b.append(i);
                }
            });
        }
        catch (final IOException thrown) {
            la1.g.log(Level.WARNING, "read error", thrown);
        }
        sb.append("]]");
        return sb.toString();
    }
    
    public final b u(final int n) {
        if (n == 0) {
            return la1.b.c;
        }
        this.a.seek(n);
        return new b(n, this.a.readInt());
    }
    
    public final void x() {
        this.a.seek(0L);
        this.a.readFully(this.f);
        final int e = E(this.f, 0);
        this.b = e;
        if (e <= this.a.length()) {
            this.c = E(this.f, 4);
            final int e2 = E(this.f, 8);
            final int e3 = E(this.f, 12);
            this.d = this.u(e2);
            this.e = this.u(e3);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("File is truncated. Expected length: ");
        sb.append(this.b);
        sb.append(", Actual length: ");
        sb.append(this.a.length());
        throw new IOException(sb.toString());
    }
    
    public static class b
    {
        public static final b c;
        public final int a;
        public final int b;
        
        static {
            c = new b(0, 0);
        }
        
        public b(final int a, final int b) {
            this.a = a;
            this.b = b;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.getClass().getSimpleName());
            sb.append("[position = ");
            sb.append(this.a);
            sb.append(", length = ");
            sb.append(this.b);
            sb.append("]");
            return sb.toString();
        }
    }
    
    public final class c extends InputStream
    {
        public int a;
        public int b;
        public final la1 c;
        
        public c(final la1 c, final b b) {
            this.c = c;
            this.a = c.i0(b.a + 4);
            this.b = b.b;
        }
        
        @Override
        public int read() {
            if (this.b == 0) {
                return -1;
            }
            la1.d(this.c).seek(this.a);
            final int read = la1.d(this.c).read();
            this.a = this.c.i0(this.a + 1);
            --this.b;
            return read;
        }
        
        @Override
        public int read(final byte[] array, final int n, final int n2) {
            o(array, "buffer");
            if ((n | n2) < 0 || n2 > array.length - n) {
                throw new ArrayIndexOutOfBoundsException();
            }
            final int b = this.b;
            if (b > 0) {
                int n3;
                if ((n3 = n2) > b) {
                    n3 = b;
                }
                this.c.K(this.a, array, n, n3);
                this.a = this.c.i0(this.a + n3);
                this.b -= n3;
                return n3;
            }
            return -1;
        }
    }
    
    public interface d
    {
        void a(final InputStream p0, final int p1);
    }
}

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Comparator;
import java.util.SortedSet;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class e80 extends c80 implements SortedSet
{
    @Override
    public Comparator<Object> comparator() {
        return this.delegate().comparator();
    }
    
    @Override
    public abstract SortedSet delegate();
    
    @Override
    public Object first() {
        return this.delegate().first();
    }
    
    @Override
    public SortedSet<Object> headSet(final Object o) {
        return this.delegate().headSet(o);
    }
    
    @Override
    public Object last() {
        return this.delegate().last();
    }
    
    @Override
    public boolean standardContains(final Object o) {
        boolean b = false;
        try {
            if (d80.unsafeCompare(this.comparator(), this.tailSet(o).first(), o) == 0) {
                b = true;
            }
            return b;
        }
        catch (final ClassCastException | NoSuchElementException | NullPointerException ex) {
            return b;
        }
    }
    
    @Override
    public boolean standardRemove(final Object o) {
        try {
            final Iterator<Object> iterator = this.tailSet(o).iterator();
            if (iterator.hasNext() && d80.unsafeCompare(this.comparator(), iterator.next(), o) == 0) {
                iterator.remove();
                return true;
            }
            return false;
        }
        catch (final ClassCastException | NullPointerException ex) {
            return false;
        }
    }
    
    public SortedSet<Object> standardSubSet(final Object o, final Object o2) {
        return this.tailSet(o).headSet(o2);
    }
    
    @Override
    public SortedSet<Object> subSet(final Object o, final Object o2) {
        return this.delegate().subSet(o, o2);
    }
    
    @Override
    public SortedSet<Object> tailSet(final Object o) {
        return this.delegate().tailSet(o);
    }
}

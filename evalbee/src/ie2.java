import com.google.android.gms.measurement.internal.zzim;
import com.google.android.gms.measurement.internal.zzil;
import android.os.Bundle;
import java.util.Map;
import java.util.List;
import com.google.android.gms.internal.measurement.zzdf;
import com.google.android.gms.measurement.internal.zzjz;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ie2 implements zzjz
{
    public final zzdf a;
    
    public ie2(final zzdf a) {
        this.a = a;
    }
    
    public final int zza(final String s) {
        return this.a.zza(s);
    }
    
    public final long zza() {
        return this.a.zza();
    }
    
    public final Object zza(final int n) {
        return this.a.zza(n);
    }
    
    public final List zza(final String s, final String s2) {
        return this.a.zza(s, s2);
    }
    
    public final Map zza(final String s, final String s2, final boolean b) {
        return this.a.zza(s, s2, b);
    }
    
    public final void zza(final Bundle bundle) {
        this.a.zza(bundle);
    }
    
    public final void zza(final zzil zzil) {
        this.a.zza(zzil);
    }
    
    public final void zza(final zzim zzim) {
        this.a.zza(zzim);
    }
    
    public final void zza(final String s, final String s2, final Bundle bundle) {
        this.a.zza(s, s2, bundle);
    }
    
    public final void zza(final String s, final String s2, final Bundle bundle, final long n) {
        this.a.zza(s, s2, bundle, n);
    }
    
    public final void zzb(final zzil zzil) {
        this.a.zzb(zzil);
    }
    
    public final void zzb(final String s) {
        this.a.zzb(s);
    }
    
    public final void zzb(final String s, final String s2, final Bundle bundle) {
        this.a.zzb(s, s2, bundle);
    }
    
    public final void zzc(final String s) {
        this.a.zzc(s);
    }
    
    public final String zzf() {
        return this.a.zzf();
    }
    
    public final String zzg() {
        return this.a.zzg();
    }
    
    public final String zzh() {
        return this.a.zzh();
    }
    
    public final String zzi() {
        return this.a.zzi();
    }
}

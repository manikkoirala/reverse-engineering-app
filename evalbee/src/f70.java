import android.database.Cursor;
import android.content.ContentResolver;
import android.net.Uri;
import android.content.ContentUris;
import android.net.Uri$Builder;
import java.util.Collection;
import java.util.Collections;
import android.content.pm.PackageManager$NameNotFoundException;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.os.CancellationSignal;
import android.content.Context;
import android.content.res.Resources;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import android.content.pm.Signature;
import java.util.Comparator;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class f70
{
    public static final Comparator a;
    
    static {
        a = new e70();
    }
    
    public static List b(final Signature[] array) {
        final ArrayList list = new ArrayList();
        for (int length = array.length, i = 0; i < length; ++i) {
            list.add(array[i].toByteArray());
        }
        return list;
    }
    
    public static boolean c(final List list, final List list2) {
        if (list.size() != list2.size()) {
            return false;
        }
        for (int i = 0; i < list.size(); ++i) {
            if (!Arrays.equals((byte[])list.get(i), (byte[])list2.get(i))) {
                return false;
            }
        }
        return true;
    }
    
    public static List d(final g70 g70, final Resources resources) {
        if (g70.b() != null) {
            return g70.b();
        }
        return j70.c(resources, g70.c());
    }
    
    public static k70.a e(final Context context, final g70 g70, final CancellationSignal cancellationSignal) {
        final ProviderInfo f = f(context.getPackageManager(), g70, context.getResources());
        if (f == null) {
            return k70.a.a(1, null);
        }
        return k70.a.a(0, h(context, g70, f.authority, cancellationSignal));
    }
    
    public static ProviderInfo f(final PackageManager packageManager, final g70 g70, final Resources resources) {
        final String e = g70.e();
        int i = 0;
        final ProviderInfo resolveContentProvider = packageManager.resolveContentProvider(e, 0);
        if (resolveContentProvider == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("No package found for authority: ");
            sb.append(e);
            throw new PackageManager$NameNotFoundException(sb.toString());
        }
        if (resolveContentProvider.packageName.equals(g70.f())) {
            final List b = b(packageManager.getPackageInfo(resolveContentProvider.packageName, 64).signatures);
            Collections.sort((List<Object>)b, f70.a);
            for (List d = d(g70, resources); i < d.size(); ++i) {
                final ArrayList list = new ArrayList<Object>(d.get(i));
                Collections.sort((List<E>)list, f70.a);
                if (c(b, list)) {
                    return resolveContentProvider;
                }
            }
            return null;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Found content provider ");
        sb2.append(e);
        sb2.append(", but package was not ");
        sb2.append(g70.f());
        throw new PackageManager$NameNotFoundException(sb2.toString());
    }
    
    public static k70.b[] h(final Context context, g70 a, String s, final CancellationSignal cancellationSignal) {
        final ArrayList list = new ArrayList();
        final Uri build = new Uri$Builder().scheme("content").authority(s).build();
        final Uri build2 = new Uri$Builder().scheme("content").authority(s).appendPath("file").build();
        s = null;
        String s2 = null;
        Label_0407: {
            try {
                a = (g70)f70.a.a(context.getContentResolver(), build, new String[] { "_id", "file_id", "font_ttc_index", "font_variation_settings", "font_weight", "font_italic", "result_code" }, "query = ?", new String[] { a.g() }, null, cancellationSignal);
                final ArrayList list2 = list;
                if (a != null) {
                    try {
                        if (((Cursor)a).getCount() > 0) {
                            final int columnIndex = ((Cursor)a).getColumnIndex("result_code");
                            s = (String)new ArrayList();
                            final int columnIndex2 = ((Cursor)a).getColumnIndex("_id");
                            final int columnIndex3 = ((Cursor)a).getColumnIndex("file_id");
                            final int columnIndex4 = ((Cursor)a).getColumnIndex("font_ttc_index");
                            final int columnIndex5 = ((Cursor)a).getColumnIndex("font_weight");
                            final int columnIndex6 = ((Cursor)a).getColumnIndex("font_italic");
                            while (((Cursor)a).moveToNext()) {
                                int int1;
                                if (columnIndex != -1) {
                                    int1 = ((Cursor)a).getInt(columnIndex);
                                }
                                else {
                                    int1 = 0;
                                }
                                int int2;
                                if (columnIndex4 != -1) {
                                    int2 = ((Cursor)a).getInt(columnIndex4);
                                }
                                else {
                                    int2 = 0;
                                }
                                Uri uri;
                                if (columnIndex3 == -1) {
                                    uri = ContentUris.withAppendedId(build, ((Cursor)a).getLong(columnIndex2));
                                }
                                else {
                                    uri = ContentUris.withAppendedId(build2, ((Cursor)a).getLong(columnIndex3));
                                }
                                int int3;
                                if (columnIndex5 != -1) {
                                    int3 = ((Cursor)a).getInt(columnIndex5);
                                }
                                else {
                                    int3 = 400;
                                }
                                ((ArrayList<k70.b>)s).add(k70.b.a(uri, int2, int3, columnIndex6 != -1 && ((Cursor)a).getInt(columnIndex6) == 1, int1));
                            }
                        }
                    }
                    finally {
                        break Label_0407;
                    }
                }
                if (a != null) {
                    ((Cursor)a).close();
                }
                return list2.toArray(new k70.b[0]);
            }
            finally {
                s2 = s;
            }
        }
        if (s2 != null) {
            ((Cursor)s2).close();
        }
    }
    
    public abstract static class a
    {
        public static Cursor a(final ContentResolver contentResolver, final Uri uri, final String[] array, final String s, final String[] array2, final String s2, final Object o) {
            return contentResolver.query(uri, array, s, array2, s2, (CancellationSignal)o);
        }
    }
}

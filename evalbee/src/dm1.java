import android.content.Context;
import android.os.Parcelable;
import android.os.Process;
import android.util.Log;
import android.content.Intent;
import com.google.firebase.sessions.SessionLifecycleService;
import android.content.ServiceConnection;
import android.os.Messenger;

// 
// Decompiled by Procyon v0.6.0
// 

public final class dm1 implements cm1
{
    public static final a c;
    public final r10 b;
    
    static {
        c = new a(null);
    }
    
    public dm1(final r10 b) {
        fg0.e((Object)b, "firebaseApp");
        this.b = b;
    }
    
    @Override
    public void a(final Messenger messenger, final ServiceConnection serviceConnection) {
        fg0.e((Object)messenger, "callback");
        fg0.e((Object)serviceConnection, "serviceConnection");
        final Context applicationContext = this.b.l().getApplicationContext();
        final Intent intent = new Intent(applicationContext, (Class)SessionLifecycleService.class);
        Log.d("LifecycleServiceBinder", "Binding service to application.");
        intent.setAction(String.valueOf(Process.myPid()));
        intent.putExtra("ClientCallbackMessenger", (Parcelable)messenger);
        applicationContext.bindService(intent, serviceConnection, 65);
    }
    
    public static final class a
    {
    }
}

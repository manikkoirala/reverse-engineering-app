import java.util.Comparator;
import java.util.TreeMap;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public class yy0
{
    public final int a;
    public final byte[] b;
    public final Map c;
    public final List d;
    public final boolean e;
    public final long f;
    
    public yy0(final int a, final byte[] b, final Map c, final List list, final boolean e, final long f) {
        this.a = a;
        this.b = b;
        this.c = c;
        List<Object> unmodifiableList;
        if (list == null) {
            unmodifiableList = null;
        }
        else {
            unmodifiableList = Collections.unmodifiableList((List<?>)list);
        }
        this.d = unmodifiableList;
        this.e = e;
        this.f = f;
    }
    
    public yy0(final int n, final byte[] array, final Map map, final boolean b, final long n2) {
        this(n, array, map, a(map), b, n2);
    }
    
    public yy0(final int n, final byte[] array, final boolean b, final long n2, final List list) {
        this(n, array, b(list), list, b, n2);
    }
    
    public yy0(final byte[] array, final Map map) {
        this(200, array, map, false, 0L);
    }
    
    public static List a(final Map map) {
        if (map == null) {
            return null;
        }
        if (map.isEmpty()) {
            return Collections.emptyList();
        }
        final ArrayList list = new ArrayList(map.size());
        for (final Map.Entry<String, V> entry : map.entrySet()) {
            list.add(new sc0(entry.getKey(), (String)entry.getValue()));
        }
        return list;
    }
    
    public static Map b(final List list) {
        if (list == null) {
            return null;
        }
        if (list.isEmpty()) {
            return Collections.emptyMap();
        }
        final TreeMap treeMap = new TreeMap((Comparator<? super K>)String.CASE_INSENSITIVE_ORDER);
        for (final sc0 sc0 : list) {
            treeMap.put(sc0.a(), sc0.b());
        }
        return treeMap;
    }
}

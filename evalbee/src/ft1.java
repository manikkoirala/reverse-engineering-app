// 
// Decompiled by Procyon v0.6.0
// 

public final class ft1
{
    public final String a;
    public final int b;
    public final int c;
    
    public ft1(final String a, final int b, final int c) {
        fg0.e((Object)a, "workSpecId");
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public final int a() {
        return this.b;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ft1)) {
            return false;
        }
        final ft1 ft1 = (ft1)o;
        return fg0.a((Object)this.a, (Object)ft1.a) && this.b == ft1.b && this.c == ft1.c;
    }
    
    @Override
    public int hashCode() {
        return (this.a.hashCode() * 31 + Integer.hashCode(this.b)) * 31 + Integer.hashCode(this.c);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SystemIdInfo(workSpecId=");
        sb.append(this.a);
        sb.append(", generation=");
        sb.append(this.b);
        sb.append(", systemId=");
        sb.append(this.c);
        sb.append(')');
        return sb.toString();
    }
}

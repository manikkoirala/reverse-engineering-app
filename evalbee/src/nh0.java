import java.io.IOException;
import java.io.Writer;
import java.io.StringWriter;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class nh0
{
    public ih0 a() {
        if (this.g()) {
            return (ih0)this;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Not a JSON Array: ");
        sb.append(this);
        throw new IllegalStateException(sb.toString());
    }
    
    public ph0 b() {
        if (this.l()) {
            return (ph0)this;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Not a JSON Object: ");
        sb.append(this);
        throw new IllegalStateException(sb.toString());
    }
    
    public qh0 c() {
        if (this.m()) {
            return (qh0)this;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Not a JSON Primitive: ");
        sb.append(this);
        throw new IllegalStateException(sb.toString());
    }
    
    public boolean g() {
        return this instanceof ih0;
    }
    
    public boolean i() {
        return this instanceof oh0;
    }
    
    public boolean l() {
        return this instanceof ph0;
    }
    
    public boolean m() {
        return this instanceof qh0;
    }
    
    @Override
    public String toString() {
        try {
            final StringWriter stringWriter = new StringWriter();
            final vh0 vh0 = new vh0(stringWriter);
            vh0.R(true);
            er1.a(this, vh0);
            return stringWriter.toString();
        }
        catch (final IOException detailMessage) {
            throw new AssertionError((Object)detailMessage);
        }
    }
}

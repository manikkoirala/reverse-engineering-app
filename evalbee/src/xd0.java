import android.os.Parcelable$Creator;
import android.os.Parcel;
import android.os.IBinder;
import android.os.Binder;
import android.os.Bundle;
import android.os.IInterface;

// 
// Decompiled by Procyon v0.6.0
// 

public interface xd0 extends IInterface
{
    void e(final int p0, final Bundle p1);
    
    public abstract static class a extends Binder implements xd0
    {
        public a() {
            this.attachInterface((IInterface)this, "android.support.v4.os.IResultReceiver");
        }
        
        public static xd0 p(final IBinder binder) {
            if (binder == null) {
                return null;
            }
            final IInterface queryLocalInterface = binder.queryLocalInterface("android.support.v4.os.IResultReceiver");
            if (queryLocalInterface != null && queryLocalInterface instanceof xd0) {
                return (xd0)queryLocalInterface;
            }
            return new xd0.a.a(binder);
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) {
            if (n >= 1 && n <= 16777215) {
                parcel.enforceInterface("android.support.v4.os.IResultReceiver");
            }
            if (n == 1598968902) {
                parcel2.writeString("android.support.v4.os.IResultReceiver");
                return true;
            }
            if (n != 1) {
                return super.onTransact(n, parcel, parcel2, n2);
            }
            this.e(parcel.readInt(), (Bundle)b(parcel, Bundle.CREATOR));
            return true;
        }
        
        public static class a implements xd0
        {
            public IBinder a;
            
            public a(final IBinder a) {
                this.a = a;
            }
            
            public IBinder asBinder() {
                return this.a;
            }
        }
    }
    
    public abstract static class b
    {
        public static Object b(final Parcel parcel, final Parcelable$Creator parcelable$Creator) {
            if (parcel.readInt() != 0) {
                return parcelable$Creator.createFromParcel(parcel);
            }
            return null;
        }
    }
}

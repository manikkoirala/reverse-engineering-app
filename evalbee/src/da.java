import java.lang.annotation.Annotation;
import com.google.firebase.encoders.proto.a;
import com.google.firebase.messaging.reporting.MessagingClientEvent;

// 
// Decompiled by Procyon v0.6.0
// 

public final class da implements jk
{
    public static final jk a;
    
    static {
        a = new da();
    }
    
    @Override
    public void configure(final zw zw) {
        zw.a(i91.class, c.a);
        zw.a(aw0.class, b.a);
        zw.a(MessagingClientEvent.class, da.a.a);
    }
    
    public static final class a implements w01
    {
        public static final a a;
        public static final n00 b;
        public static final n00 c;
        public static final n00 d;
        public static final n00 e;
        public static final n00 f;
        public static final n00 g;
        public static final n00 h;
        public static final n00 i;
        public static final n00 j;
        public static final n00 k;
        public static final n00 l;
        public static final n00 m;
        public static final n00 n;
        public static final n00 o;
        public static final n00 p;
        
        static {
            a = new a();
            b = n00.a("projectNumber").b(com.google.firebase.encoders.proto.a.b().c(1).a()).a();
            c = n00.a("messageId").b(com.google.firebase.encoders.proto.a.b().c(2).a()).a();
            d = n00.a("instanceId").b(com.google.firebase.encoders.proto.a.b().c(3).a()).a();
            e = n00.a("messageType").b(com.google.firebase.encoders.proto.a.b().c(4).a()).a();
            f = n00.a("sdkPlatform").b(com.google.firebase.encoders.proto.a.b().c(5).a()).a();
            g = n00.a("packageName").b(com.google.firebase.encoders.proto.a.b().c(6).a()).a();
            h = n00.a("collapseKey").b(com.google.firebase.encoders.proto.a.b().c(7).a()).a();
            i = n00.a("priority").b(com.google.firebase.encoders.proto.a.b().c(8).a()).a();
            j = n00.a("ttl").b(com.google.firebase.encoders.proto.a.b().c(9).a()).a();
            k = n00.a("topic").b(com.google.firebase.encoders.proto.a.b().c(10).a()).a();
            l = n00.a("bulkId").b(com.google.firebase.encoders.proto.a.b().c(11).a()).a();
            m = n00.a("event").b(com.google.firebase.encoders.proto.a.b().c(12).a()).a();
            n = n00.a("analyticsLabel").b(com.google.firebase.encoders.proto.a.b().c(13).a()).a();
            o = n00.a("campaignId").b(com.google.firebase.encoders.proto.a.b().c(14).a()).a();
            p = n00.a("composerLabel").b(com.google.firebase.encoders.proto.a.b().c(15).a()).a();
        }
        
        public void a(final MessagingClientEvent messagingClientEvent, final x01 x01) {
            x01.e(da.a.b, messagingClientEvent.l());
            x01.f(da.a.c, messagingClientEvent.h());
            x01.f(da.a.d, messagingClientEvent.g());
            x01.f(da.a.e, messagingClientEvent.i());
            x01.f(da.a.f, messagingClientEvent.m());
            x01.f(da.a.g, messagingClientEvent.j());
            x01.f(da.a.h, messagingClientEvent.d());
            x01.c(da.a.i, messagingClientEvent.k());
            x01.c(da.a.j, messagingClientEvent.o());
            x01.f(da.a.k, messagingClientEvent.n());
            x01.e(da.a.l, messagingClientEvent.b());
            x01.f(da.a.m, messagingClientEvent.f());
            x01.f(da.a.n, messagingClientEvent.a());
            x01.e(da.a.o, messagingClientEvent.c());
            x01.f(da.a.p, messagingClientEvent.e());
        }
    }
    
    public static final class b implements w01
    {
        public static final b a;
        public static final n00 b;
        
        static {
            a = new b();
            b = n00.a("messagingClientEvent").b(com.google.firebase.encoders.proto.a.b().c(1).a()).a();
        }
        
        public void a(final aw0 aw0, final x01 x01) {
            x01.f(da.b.b, aw0.a());
        }
    }
    
    public static final class c implements w01
    {
        public static final c a;
        public static final n00 b;
        
        static {
            a = new c();
            b = n00.d("messagingClientEventExtension");
        }
        
        public void a(final i91 i91, final x01 x01) {
            throw null;
        }
    }
}

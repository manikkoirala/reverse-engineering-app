import android.os.AsyncTask;
import android.net.Uri;
import android.os.Parcelable;
import android.content.Intent;
import androidx.core.content.FileProvider;
import java.io.File;
import java.util.ArrayList;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class go
{
    public Context a;
    public ArrayList b;
    public y01 c;
    
    public go(final Context a, final ArrayList b, final y01 c) {
        new ArrayList();
        this.a = a;
        this.b = b;
        this.c = c;
        new b(null).execute((Object[])new Void[0]);
    }
    
    public static /* synthetic */ Context a(final go go) {
        return go.a;
    }
    
    public final void c() {
        final y01 y01 = new y01(this, new File(this.a.getExternalFilesDir(ok.c), "student_export.csv")) {
            public final File a;
            public final go b;
            
            @Override
            public void a(final Object o) {
                if (this.a.exists()) {
                    final Uri f = FileProvider.f(go.a(this.b), "com.ekodroid.omrevaluator.fileprovider", this.a);
                    final Intent intent = new Intent("android.intent.action.SEND");
                    intent.setFlags(268435456);
                    intent.putExtra("android.intent.extra.STREAM", (Parcelable)f);
                    intent.setDataAndType(f, "application/octet-stream");
                    intent.setType("application/octet-stream");
                    intent.setFlags(1073741824);
                    intent.setFlags(1);
                    go.a(this.b).startActivity(Intent.createChooser(intent, (CharSequence)"Share with"));
                }
            }
        };
        final Context a = this.a;
        final StringBuilder sb = new StringBuilder();
        sb.append(this.a.getResources().getString(2131886311));
        sb.append(" : ");
        sb.append(ok.c);
        sb.append("/student_export.csv");
        xs.d(a, y01, 2131886312, sb.toString(), 2131886824, 2131886176, 0, 0);
    }
    
    public class b extends AsyncTask
    {
        public File a;
        public final go b;
        
        public b(final go b) {
            this.b = b;
        }
        
        public Void a(final Void... p0) {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: getfield        go$b.b:Lgo;
            //     4: invokestatic    go.a:(Lgo;)Landroid/content/Context;
            //     7: getstatic       ok.c:Ljava/lang/String;
            //    10: invokevirtual   android/content/Context.getExternalFilesDir:(Ljava/lang/String;)Ljava/io/File;
            //    13: astore_1       
            //    14: aload_1        
            //    15: invokevirtual   java/io/File.exists:()Z
            //    18: ifne            26
            //    21: aload_1        
            //    22: invokevirtual   java/io/File.mkdirs:()Z
            //    25: pop            
            //    26: aload_0        
            //    27: new             Ljava/io/File;
            //    30: dup            
            //    31: aload_1        
            //    32: ldc             "student_export.csv"
            //    34: invokespecial   java/io/File.<init>:(Ljava/io/File;Ljava/lang/String;)V
            //    37: putfield        go$b.a:Ljava/io/File;
            //    40: aconst_null    
            //    41: astore_3       
            //    42: new             Ljava/io/FileOutputStream;
            //    45: astore_2       
            //    46: aload_2        
            //    47: aload_0        
            //    48: getfield        go$b.a:Ljava/io/File;
            //    51: invokespecial   java/io/FileOutputStream.<init>:(Ljava/io/File;)V
            //    54: aload_2        
            //    55: astore_1       
            //    56: new             Lzd;
            //    59: astore_3       
            //    60: aload_2        
            //    61: astore_1       
            //    62: new             Ljava/io/OutputStreamWriter;
            //    65: astore          4
            //    67: aload_2        
            //    68: astore_1       
            //    69: aload           4
            //    71: aload_2        
            //    72: invokespecial   java/io/OutputStreamWriter.<init>:(Ljava/io/OutputStream;)V
            //    75: aload_2        
            //    76: astore_1       
            //    77: aload_3        
            //    78: aload           4
            //    80: invokespecial   zd.<init>:(Ljava/io/Writer;)V
            //    83: aload_2        
            //    84: astore_1       
            //    85: aload_3        
            //    86: iconst_5       
            //    87: anewarray       Ljava/lang/String;
            //    90: dup            
            //    91: iconst_0       
            //    92: ldc             "ROLLNO"
            //    94: aastore        
            //    95: dup            
            //    96: iconst_1       
            //    97: ldc             "NAME"
            //    99: aastore        
            //   100: dup            
            //   101: iconst_2       
            //   102: ldc             "CLASS"
            //   104: aastore        
            //   105: dup            
            //   106: iconst_3       
            //   107: ldc             "EMAILID"
            //   109: aastore        
            //   110: dup            
            //   111: iconst_4       
            //   112: ldc             "PHONENO"
            //   114: aastore        
            //   115: invokevirtual   h.a:([Ljava/lang/String;)V
            //   118: aload_2        
            //   119: astore_1       
            //   120: aload_0        
            //   121: getfield        go$b.b:Lgo;
            //   124: getfield        go.b:Ljava/util/ArrayList;
            //   127: invokevirtual   java/util/ArrayList.iterator:()Ljava/util/Iterator;
            //   130: astore          4
            //   132: aload_2        
            //   133: astore_1       
            //   134: aload           4
            //   136: invokeinterface java/util/Iterator.hasNext:()Z
            //   141: ifeq            307
            //   144: aload_2        
            //   145: astore_1       
            //   146: aload           4
            //   148: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
            //   153: checkcast       Lcom/ekodroid/omrevaluator/database/StudentDataModel;
            //   156: astore          5
            //   158: aload_2        
            //   159: astore_1       
            //   160: iconst_5       
            //   161: anewarray       Ljava/lang/String;
            //   164: astore          6
            //   166: aload_2        
            //   167: astore_1       
            //   168: new             Ljava/lang/StringBuilder;
            //   171: astore          7
            //   173: aload_2        
            //   174: astore_1       
            //   175: aload           7
            //   177: invokespecial   java/lang/StringBuilder.<init>:()V
            //   180: aload_2        
            //   181: astore_1       
            //   182: aload           7
            //   184: aload           5
            //   186: invokevirtual   com/ekodroid/omrevaluator/database/StudentDataModel.getRollNO:()Ljava/lang/Integer;
            //   189: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
            //   192: pop            
            //   193: aload_2        
            //   194: astore_1       
            //   195: aload           7
            //   197: ldc             ""
            //   199: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   202: pop            
            //   203: aload_2        
            //   204: astore_1       
            //   205: aload           6
            //   207: iconst_0       
            //   208: aload           7
            //   210: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
            //   213: aastore        
            //   214: aload_2        
            //   215: astore_1       
            //   216: aload           6
            //   218: iconst_1       
            //   219: aload           5
            //   221: invokevirtual   com/ekodroid/omrevaluator/database/StudentDataModel.getStudentName:()Ljava/lang/String;
            //   224: aastore        
            //   225: aload_2        
            //   226: astore_1       
            //   227: aload           6
            //   229: iconst_2       
            //   230: aload           5
            //   232: invokevirtual   com/ekodroid/omrevaluator/database/StudentDataModel.getClassName:()Ljava/lang/String;
            //   235: aastore        
            //   236: aload_2        
            //   237: astore_1       
            //   238: aload           5
            //   240: invokevirtual   com/ekodroid/omrevaluator/database/StudentDataModel.getEmailId:()Ljava/lang/String;
            //   243: ifnull          260
            //   246: aload_2        
            //   247: astore_1       
            //   248: aload           6
            //   250: iconst_3       
            //   251: aload           5
            //   253: invokevirtual   com/ekodroid/omrevaluator/database/StudentDataModel.getEmailId:()Ljava/lang/String;
            //   256: aastore        
            //   257: goto            266
            //   260: aload           6
            //   262: iconst_3       
            //   263: ldc             ""
            //   265: aastore        
            //   266: aload_2        
            //   267: astore_1       
            //   268: aload           5
            //   270: invokevirtual   com/ekodroid/omrevaluator/database/StudentDataModel.getPhoneNo:()Ljava/lang/String;
            //   273: ifnull          290
            //   276: aload_2        
            //   277: astore_1       
            //   278: aload           6
            //   280: iconst_4       
            //   281: aload           5
            //   283: invokevirtual   com/ekodroid/omrevaluator/database/StudentDataModel.getPhoneNo:()Ljava/lang/String;
            //   286: aastore        
            //   287: goto            296
            //   290: aload           6
            //   292: iconst_4       
            //   293: ldc             ""
            //   295: aastore        
            //   296: aload_2        
            //   297: astore_1       
            //   298: aload_3        
            //   299: aload           6
            //   301: invokevirtual   h.a:([Ljava/lang/String;)V
            //   304: goto            132
            //   307: aload_2        
            //   308: astore_1       
            //   309: aload_3        
            //   310: invokevirtual   h.flush:()V
            //   313: aload_2        
            //   314: invokevirtual   java/io/OutputStream.flush:()V
            //   317: aload_2        
            //   318: invokevirtual   java/io/FileOutputStream.close:()V
            //   321: goto            372
            //   324: astore_3       
            //   325: goto            337
            //   328: astore_1       
            //   329: aload_3        
            //   330: astore_2       
            //   331: goto            379
            //   334: astore_3       
            //   335: aconst_null    
            //   336: astore_2       
            //   337: aload_2        
            //   338: astore_1       
            //   339: aload_3        
            //   340: invokevirtual   java/lang/Throwable.printStackTrace:()V
            //   343: aload_2        
            //   344: astore_1       
            //   345: aload_0        
            //   346: getfield        go$b.a:Ljava/io/File;
            //   349: invokevirtual   java/io/File.deleteOnExit:()V
            //   352: aload_2        
            //   353: ifnull          372
            //   356: aload_2        
            //   357: invokevirtual   java/io/OutputStream.flush:()V
            //   360: aload_2        
            //   361: invokevirtual   java/io/FileOutputStream.close:()V
            //   364: goto            372
            //   367: astore_1       
            //   368: aload_1        
            //   369: invokevirtual   java/lang/Throwable.printStackTrace:()V
            //   372: aconst_null    
            //   373: areturn        
            //   374: astore_3       
            //   375: aload_1        
            //   376: astore_2       
            //   377: aload_3        
            //   378: astore_1       
            //   379: aload_2        
            //   380: ifnull          399
            //   383: aload_2        
            //   384: invokevirtual   java/io/OutputStream.flush:()V
            //   387: aload_2        
            //   388: invokevirtual   java/io/FileOutputStream.close:()V
            //   391: goto            399
            //   394: astore_2       
            //   395: aload_2        
            //   396: invokevirtual   java/lang/Throwable.printStackTrace:()V
            //   399: aload_1        
            //   400: athrow         
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                 
            //  -----  -----  -----  -----  ---------------------
            //  42     54     334    337    Ljava/lang/Exception;
            //  42     54     328    334    Any
            //  56     60     324    328    Ljava/lang/Exception;
            //  56     60     374    379    Any
            //  62     67     324    328    Ljava/lang/Exception;
            //  62     67     374    379    Any
            //  69     75     324    328    Ljava/lang/Exception;
            //  69     75     374    379    Any
            //  77     83     324    328    Ljava/lang/Exception;
            //  77     83     374    379    Any
            //  85     118    324    328    Ljava/lang/Exception;
            //  85     118    374    379    Any
            //  120    132    324    328    Ljava/lang/Exception;
            //  120    132    374    379    Any
            //  134    144    324    328    Ljava/lang/Exception;
            //  134    144    374    379    Any
            //  146    158    324    328    Ljava/lang/Exception;
            //  146    158    374    379    Any
            //  160    166    324    328    Ljava/lang/Exception;
            //  160    166    374    379    Any
            //  168    173    324    328    Ljava/lang/Exception;
            //  168    173    374    379    Any
            //  175    180    324    328    Ljava/lang/Exception;
            //  175    180    374    379    Any
            //  182    193    324    328    Ljava/lang/Exception;
            //  182    193    374    379    Any
            //  195    203    324    328    Ljava/lang/Exception;
            //  195    203    374    379    Any
            //  205    214    324    328    Ljava/lang/Exception;
            //  205    214    374    379    Any
            //  216    225    324    328    Ljava/lang/Exception;
            //  216    225    374    379    Any
            //  227    236    324    328    Ljava/lang/Exception;
            //  227    236    374    379    Any
            //  238    246    324    328    Ljava/lang/Exception;
            //  238    246    374    379    Any
            //  248    257    324    328    Ljava/lang/Exception;
            //  248    257    374    379    Any
            //  268    276    324    328    Ljava/lang/Exception;
            //  268    276    374    379    Any
            //  278    287    324    328    Ljava/lang/Exception;
            //  278    287    374    379    Any
            //  298    304    324    328    Ljava/lang/Exception;
            //  298    304    374    379    Any
            //  309    313    324    328    Ljava/lang/Exception;
            //  309    313    374    379    Any
            //  313    321    367    372    Ljava/io/IOException;
            //  339    343    374    379    Any
            //  345    352    374    379    Any
            //  356    364    367    372    Ljava/io/IOException;
            //  383    391    394    399    Ljava/io/IOException;
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0132:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        public void b(final Void void1) {
            super.onPostExecute((Object)void1);
            if (this.a.exists()) {
                this.b.c();
            }
        }
    }
}

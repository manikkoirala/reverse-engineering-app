import android.widget.CompoundButton;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemSelectedListener;
import java.util.Iterator;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.SharedExamPartial;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.UserRole;
import com.ekodroid.omrevaluator.serializable.ResponseModel.PurchaseAccount;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.LinearLayout;
import android.widget.SpinnerAdapter;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import com.google.firebase.auth.FirebaseAuth;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.OrgProfile;
import com.ekodroid.omrevaluator.more.models.Teacher;
import java.util.ArrayList;
import android.os.Bundle;
import android.content.Intent;
import com.ekodroid.omrevaluator.more.ProductAndServicesActivity;
import android.view.View;
import android.view.View$OnClickListener;
import androidx.appcompat.widget.Toolbar;
import android.widget.CheckBox;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.SharedExamDetailsDto;
import com.ekodroid.omrevaluator.serializable.SharedType;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class vx extends u80
{
    public Context a;
    public y01 b;
    public SharedType c;
    public SharedExamDetailsDto d;
    public CheckBox e;
    
    public vx(final Context a, final SharedExamDetailsDto d, final y01 b) {
        super(a);
        this.c = SharedType.PRIVATE;
        this.a = a;
        this.b = b;
        this.d = d;
    }
    
    public static /* synthetic */ y01 a(final vx vx) {
        return vx.b;
    }
    
    public static /* synthetic */ SharedExamDetailsDto c(final vx vx) {
        return vx.d;
    }
    
    public static /* synthetic */ SharedType d(final vx vx) {
        return vx.c;
    }
    
    public static /* synthetic */ SharedType e(final vx vx, final SharedType c) {
        return vx.c = c;
    }
    
    public final int f(final SharedType sharedType) {
        final int n = vx$g.a[sharedType.ordinal()];
        if (n == 2) {
            return 1;
        }
        if (n != 3) {
            return 0;
        }
        return 2;
    }
    
    public final void g() {
        ((Toolbar)this.findViewById(2131297343)).setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final vx a;
            
            public void onClick(final View view) {
                vx.a(this.a).a(null);
                this.a.dismiss();
            }
        });
    }
    
    public final void h() {
        ((CompoundButton)this.e).setChecked(false);
        xs.c(this.a, new y01(this) {
            public final vx a;
            
            @Override
            public void a(final Object o) {
                this.a.a.startActivity(new Intent(this.a.a, (Class)ProductAndServicesActivity.class));
            }
        }, 2131886280, 2131886493, 2131886886, 2131886163, 0, 0);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setCancelable(false);
        this.setContentView(2131492970);
        this.g();
        ArrayList<Teacher> teacherList = new ArrayList<Teacher>();
        final OrgProfile instance = OrgProfile.getInstance(this.a);
        if (instance != null) {
            teacherList = instance.getTeacherList();
        }
        final ArrayList<su1> list = new ArrayList<su1>();
        final r30 e = FirebaseAuth.getInstance().e();
        for (final Teacher teacher : teacherList) {
            if (!e.O().equals(teacher.getUserId())) {
                list.add(new su1(teacher, false));
            }
        }
        final TextView textView = (TextView)this.findViewById(2131297253);
        final Spinner spinner = (Spinner)this.findViewById(2131297073);
        spinner.setAdapter((SpinnerAdapter)new ArrayAdapter(this.a, 2131493104, (Object[])gr1.f));
        final LinearLayout linearLayout = (LinearLayout)this.findViewById(2131296811);
        final ListView listView = (ListView)this.findViewById(2131296847);
        final TextView textView2 = (TextView)this.findViewById(2131297281);
        final CheckBox e2 = (CheckBox)this.findViewById(2131296510);
        this.e = e2;
        final SharedExamDetailsDto d = this.d;
        if (d != null) {
            ((CompoundButton)e2).setChecked(d.syncImages);
            if (this.d.syncImages) {
                ((View)this.e).setEnabled(false);
            }
            final SharedExamDetailsDto d2 = this.d;
            if ((this.c = d2.sharedType) == SharedType.SHARED) {
                for (final String s : d2.teachersUid) {
                    for (final su1 su1 : list) {
                        final Teacher a = su1.a;
                        if (a != null && s.equals(a.getUserId())) {
                            su1.b = true;
                        }
                    }
                }
            }
        }
        ((View)textView2).setVisibility(8);
        ((AdapterView)spinner).setSelection(this.f(this.c));
        listView.setAdapter((ListAdapter)new c3(list, this.a));
        final Button button = (Button)this.findViewById(2131296400);
        ((View)this.findViewById(2131296388)).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final vx a;
            
            public void onClick(final View view) {
                vx.a(this.a).a(null);
                this.a.dismiss();
            }
        });
        ((View)this.e).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final vx a;
            
            public void onClick(final View view) {
                final PurchaseAccount r = a91.r(this.a.a);
                if (r == null || !r.getAccountType().equals(ok.S)) {
                    this.a.h();
                }
            }
        });
        ((View)button).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, button, list, spinner, textView2) {
            public final Button a;
            public final ArrayList b;
            public final Spinner c;
            public final TextView d;
            public final vx e;
            
            public void onClick(final View view) {
                if (vx.c(this.e) != null && OrgProfile.getInstance(this.e.a).getRole() != UserRole.OWNER && !FirebaseAuth.getInstance().e().O().equals(vx.c(this.e).ownerUid)) {
                    ((View)this.a).setEnabled(false);
                    a91.G(this.e.a, 2131886498, 2131230909, 2131231086);
                }
                final ArrayList teachers = new ArrayList();
                for (final su1 su1 : this.b) {
                    if (su1.b) {
                        teachers.add(su1.a);
                    }
                }
                if (((AdapterView)this.c).getSelectedItemPosition() == 2 && teachers.size() == 0) {
                    ((View)this.d).setVisibility(0);
                    this.d.setText(2131886564);
                    return;
                }
                final SharedExamPartial sharedExamPartial = new SharedExamPartial();
                if (vx.c(this.e) != null) {
                    sharedExamPartial.setExamId(vx.c(this.e).examId);
                }
                sharedExamPartial.setSharedType(vx.d(this.e));
                sharedExamPartial.setTeachers(teachers);
                sharedExamPartial.setSyncImages(((CompoundButton)this.e.e).isChecked());
                vx.a(this.e).a(sharedExamPartial);
                this.e.dismiss();
            }
        });
        ((AdapterView)spinner).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener(this, textView2, spinner, textView, linearLayout, list) {
            public final TextView a;
            public final Spinner b;
            public final TextView c;
            public final LinearLayout d;
            public final ArrayList e;
            public final vx f;
            
            public void onItemSelected(final AdapterView adapterView, final View view, int selectedItemPosition, final long n) {
                ((View)this.a).setVisibility(8);
                selectedItemPosition = ((AdapterView)this.b).getSelectedItemPosition();
                TextView textView = null;
                String text = null;
                Label_0152: {
                    if (selectedItemPosition != 0) {
                        TextView textView2;
                        String text2;
                        if (selectedItemPosition != 1) {
                            if (selectedItemPosition != 2) {
                                textView = this.c;
                                text = "Only you have access to this exam";
                                break Label_0152;
                            }
                            ((View)this.c).setVisibility(8);
                            ((View)this.d).setVisibility(0);
                            vx.e(this.f, SharedType.SHARED);
                            if (this.e.size() >= 1) {
                                return;
                            }
                            ((View)this.a).setVisibility(0);
                            textView2 = this.a;
                            text2 = "No teachers found, please add teachers to your account";
                        }
                        else {
                            ((View)this.c).setVisibility(0);
                            ((View)this.d).setVisibility(8);
                            vx.e(this.f, SharedType.PUBLIC);
                            textView2 = this.c;
                            text2 = "All teachers in your organization account will have access to the exam";
                        }
                        textView2.setText((CharSequence)text2);
                        return;
                    }
                    textView = this.c;
                    text = "Only you have access to the exam";
                }
                textView.setText((CharSequence)text);
                ((View)this.c).setVisibility(0);
                ((View)this.d).setVisibility(8);
                vx.e(this.f, SharedType.PRIVATE);
            }
            
            public void onNothingSelected(final AdapterView adapterView) {
            }
        });
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class v00
{
    public static final a a;
    public static final b b;
    
    static {
        a = new a();
        b = new b();
    }
    
    public static v00 a() {
        return v00.a;
    }
    
    public static v00 c() {
        return v00.b;
    }
    
    public abstract String b();
    
    public static class a extends v00
    {
        @Override
        public String b() {
            return "FieldValue.delete";
        }
    }
    
    public static class b extends v00
    {
        @Override
        public String b() {
            return "FieldValue.serverTimestamp";
        }
    }
}

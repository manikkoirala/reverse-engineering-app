import java.util.Locale;
import com.google.android.datatransport.TransportScheduleCallback;
import com.google.android.datatransport.Event;
import android.database.SQLException;
import com.google.android.datatransport.runtime.ForcedSender;
import com.google.android.datatransport.Priority;
import java.util.concurrent.CountDownLatch;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ArrayBlockingQueue;
import android.os.SystemClock;
import com.google.android.datatransport.Transport;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.BlockingQueue;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ae1
{
    public final double a;
    public final double b;
    public final long c;
    public final long d;
    public final int e;
    public final BlockingQueue f;
    public final ThreadPoolExecutor g;
    public final Transport h;
    public final m11 i;
    public int j;
    public long k;
    
    public ae1(final double a, final double b, final long c, final Transport h, final m11 i) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.h = h;
        this.i = i;
        this.d = SystemClock.elapsedRealtime();
        final int n = (int)a;
        this.e = n;
        final ArrayBlockingQueue arrayBlockingQueue = new ArrayBlockingQueue(n);
        this.f = arrayBlockingQueue;
        this.g = new ThreadPoolExecutor(1, 1, 0L, TimeUnit.MILLISECONDS, (BlockingQueue<Runnable>)arrayBlockingQueue);
        this.j = 0;
        this.k = 0L;
    }
    
    public ae1(final Transport transport, final vm1 vm1, final m11 m11) {
        this(vm1.f, vm1.g, vm1.h * 1000L, transport, m11);
    }
    
    public static /* synthetic */ m11 d(final ae1 ae1) {
        return ae1.i;
    }
    
    public static void q(final double n) {
        final long n2 = (long)n;
        try {
            Thread.sleep(n2);
        }
        catch (final InterruptedException ex) {}
    }
    
    public final double g() {
        return Math.min(3600000.0, 60000.0 / this.a * Math.pow(this.b, this.h()));
    }
    
    public final int h() {
        if (this.k == 0L) {
            this.k = this.o();
        }
        final int n = (int)((this.o() - this.k) / this.c);
        int j;
        if (this.l()) {
            j = Math.min(100, this.j + n);
        }
        else {
            j = Math.max(0, this.j - n);
        }
        if (this.j != j) {
            this.j = j;
            this.k = this.o();
        }
        return j;
    }
    
    public TaskCompletionSource i(final ao ao, final boolean b) {
        synchronized (this.f) {
            final TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
            if (!b) {
                this.p(ao, taskCompletionSource);
                return taskCompletionSource;
            }
            this.i.b();
            if (this.k()) {
                final zl0 f = zl0.f();
                final StringBuilder sb = new StringBuilder();
                sb.append("Enqueueing report: ");
                sb.append(ao.d());
                f.b(sb.toString());
                final zl0 f2 = zl0.f();
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Queue size: ");
                sb2.append(this.f.size());
                f2.b(sb2.toString());
                this.g.execute(new b(ao, taskCompletionSource, null));
                final zl0 f3 = zl0.f();
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Closing task for report: ");
                sb3.append(ao.d());
                f3.b(sb3.toString());
                taskCompletionSource.trySetResult((Object)ao);
                return taskCompletionSource;
            }
            this.h();
            final zl0 f4 = zl0.f();
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("Dropping report due to queue being full: ");
            sb4.append(ao.d());
            f4.b(sb4.toString());
            this.i.a();
            taskCompletionSource.trySetResult((Object)ao);
            return taskCompletionSource;
        }
    }
    
    public void j() {
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        new Thread(new zd1(this, countDownLatch)).start();
        v22.g(countDownLatch, 2L, TimeUnit.SECONDS);
    }
    
    public final boolean k() {
        return this.f.size() < this.e;
    }
    
    public final boolean l() {
        return this.f.size() == this.e;
    }
    
    public final long o() {
        return System.currentTimeMillis();
    }
    
    public final void p(final ao ao, final TaskCompletionSource taskCompletionSource) {
        final zl0 f = zl0.f();
        final StringBuilder sb = new StringBuilder();
        sb.append("Sending report through Google DataTransport: ");
        sb.append(ao.d());
        f.b(sb.toString());
        this.h.schedule(Event.ofUrgent(ao.b()), new yd1(this, taskCompletionSource, SystemClock.elapsedRealtime() - this.d < 2000L, ao));
    }
    
    public final class b implements Runnable
    {
        public final ao a;
        public final TaskCompletionSource b;
        public final ae1 c;
        
        public b(final ae1 c, final ao a, final TaskCompletionSource b) {
            this.c = c;
            this.a = a;
            this.b = b;
        }
        
        @Override
        public void run() {
            this.c.p(this.a, this.b);
            ae1.d(this.c).c();
            final double e = this.c.g();
            final zl0 f = zl0.f();
            final StringBuilder sb = new StringBuilder();
            sb.append("Delay for: ");
            sb.append(String.format(Locale.US, "%.2f", e / 1000.0));
            sb.append(" s for report: ");
            sb.append(this.a.d());
            f.b(sb.toString());
            q(e);
        }
    }
}

import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineDispatcher;

// 
// Decompiled by Procyon v0.6.0
// 

public final class a51 extends CoroutineDispatcher
{
    public final lt c;
    
    public a51() {
        this.c = new lt();
    }
    
    public void f(final CoroutineContext coroutineContext, final Runnable runnable) {
        fg0.e((Object)coroutineContext, "context");
        fg0.e((Object)runnable, "block");
        this.c.c(coroutineContext, runnable);
    }
    
    public boolean q0(final CoroutineContext coroutineContext) {
        fg0.e((Object)coroutineContext, "context");
        return ((CoroutineDispatcher)pt.c().s0()).q0(coroutineContext) || (this.c.b() ^ true);
    }
}

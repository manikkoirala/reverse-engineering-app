import java.util.Date;
import java.sql.Timestamp;
import com.google.gson.reflect.TypeToken;

// 
// Decompiled by Procyon v0.6.0
// 

public class ep1 extends hz1
{
    public static final iz1 b;
    public final hz1 a;
    
    static {
        b = new iz1() {
            @Override
            public hz1 a(final gc0 gc0, final TypeToken typeToken) {
                if (typeToken.getRawType() == Timestamp.class) {
                    return new ep1(gc0.m(Date.class), null);
                }
                return null;
            }
        };
    }
    
    public ep1(final hz1 a) {
        this.a = a;
    }
    
    public Timestamp e(final rh0 rh0) {
        final Date date = (Date)this.a.b(rh0);
        Timestamp timestamp;
        if (date != null) {
            timestamp = new Timestamp(date.getTime());
        }
        else {
            timestamp = null;
        }
        return timestamp;
    }
    
    public void f(final vh0 vh0, final Timestamp timestamp) {
        this.a.d(vh0, timestamp);
    }
}

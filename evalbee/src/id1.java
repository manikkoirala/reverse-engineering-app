import java.util.Collection;
import com.google.firebase.firestore.local.IndexManager;
import com.google.firebase.firestore.model.MutableDocument;
import java.util.Map;
import java.util.Set;
import com.google.firebase.firestore.model.FieldIndex;
import com.google.firebase.firestore.core.Query;

// 
// Decompiled by Procyon v0.6.0
// 

public interface id1
{
    Map a(final Query p0, final FieldIndex.a p1, final Set p2, final ga1 p3);
    
    Map b(final String p0, final FieldIndex.a p1, final int p2);
    
    void c(final MutableDocument p0, final qo1 p1);
    
    MutableDocument d(final du p0);
    
    void e(final IndexManager p0);
    
    Map getAll(final Iterable p0);
    
    void removeAll(final Collection p0);
}

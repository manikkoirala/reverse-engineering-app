import androidx.work.impl.foreground.a;
import java.util.UUID;
import android.content.Context;
import androidx.work.impl.WorkDatabase;

// 
// Decompiled by Procyon v0.6.0
// 

public class w82 implements r70
{
    public static final String d;
    public final hu1 a;
    public final q70 b;
    public final q92 c;
    
    static {
        d = xl0.i("WMFgUpdater");
    }
    
    public w82(final WorkDatabase workDatabase, final q70 b, final hu1 a) {
        this.b = b;
        this.a = a;
        this.c = workDatabase.K();
    }
    
    @Override
    public ik0 a(final Context context, final UUID uuid, final p70 p3) {
        final um1 s = um1.s();
        this.a.b(new Runnable(this, s, uuid, p3, context) {
            public final um1 a;
            public final UUID b;
            public final p70 c;
            public final Context d;
            public final w82 e;
            
            @Override
            public void run() {
                try {
                    if (!this.a.isCancelled()) {
                        final String string = this.b.toString();
                        final p92 s = this.e.c.s(string);
                        if (s == null || s.b.isFinished()) {
                            throw new IllegalStateException("Calls to setForegroundAsync() must complete before a ListenableWorker signals completion of work by returning an instance of Result.");
                        }
                        this.e.b.a(string, this.c);
                        this.d.startService(androidx.work.impl.foreground.a.e(this.d, u92.a(s), this.c));
                    }
                    this.a.o(null);
                }
                finally {
                    final Throwable t;
                    this.a.p(t);
                }
            }
        });
        return s;
    }
}

import android.os.Build$VERSION;
import android.view.View;

// 
// Decompiled by Procyon v0.6.0
// 

public class e52 extends c52
{
    public static boolean g = true;
    
    @Override
    public void f(final View view, final int n) {
        if (Build$VERSION.SDK_INT == 28) {
            super.f(view, n);
        }
        else if (e52.g) {
            try {
                d52.a(view, n);
            }
            catch (final NoSuchMethodError noSuchMethodError) {
                e52.g = false;
            }
        }
    }
}

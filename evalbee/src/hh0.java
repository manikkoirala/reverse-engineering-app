import com.google.gson.reflect.TypeToken;

// 
// Decompiled by Procyon v0.6.0
// 

public final class hh0 implements iz1
{
    public final bl a;
    
    public hh0(final bl a) {
        this.a = a;
    }
    
    @Override
    public hz1 a(final gc0 gc0, final TypeToken typeToken) {
        final gh0 gh0 = (gh0)typeToken.getRawType().getAnnotation(gh0.class);
        if (gh0 == null) {
            return null;
        }
        return this.b(this.a, gc0, typeToken, gh0);
    }
    
    public hz1 b(final bl bl, final gc0 gc0, final TypeToken typeToken, final gh0 gh0) {
        final Object a = bl.b(TypeToken.get((Class<Object>)gh0.value())).a();
        final boolean nullSafe = gh0.nullSafe();
        hz1 a2;
        if (a instanceof hz1) {
            a2 = (hz1)a;
        }
        else {
            if (!(a instanceof iz1)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid attempt to bind an instance of ");
                sb.append(((iz1)a).getClass().getName());
                sb.append(" as a @JsonAdapter for ");
                sb.append(typeToken.toString());
                sb.append(". @JsonAdapter value must be a TypeAdapter, TypeAdapterFactory, JsonSerializer or JsonDeserializer.");
                throw new IllegalArgumentException(sb.toString());
            }
            a2 = ((iz1)a).a(gc0, typeToken);
        }
        hz1 a3 = a2;
        if (a2 != null) {
            a3 = a2;
            if (nullSafe) {
                a3 = a2.a();
            }
        }
        return a3;
    }
}

import java.util.Stack;

// 
// Decompiled by Procyon v0.6.0
// 

public class cz1
{
    public final String a;
    public final String b;
    public final StackTraceElement[] c;
    public final cz1 d;
    
    public cz1(final String a, final String b, final StackTraceElement[] c, final cz1 d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
    
    public static cz1 a(Throwable cause, final lp1 lp1) {
        final Stack stack = new Stack();
        while (cause != null) {
            stack.push(cause);
            cause = cause.getCause();
        }
        cz1 cz1 = null;
        while (!stack.isEmpty()) {
            final Throwable t = stack.pop();
            cz1 = new cz1(t.getLocalizedMessage(), t.getClass().getName(), lp1.a(t.getStackTrace()), cz1);
        }
        return cz1;
    }
}

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ke1 extends jb
{
    public static final ke1 b;
    
    static {
        b = new ke1(Collections.emptyList());
    }
    
    public ke1(final List list) {
        super(list);
    }
    
    public static ke1 p(final List list) {
        ke1 b;
        if (list.isEmpty()) {
            b = ke1.b;
        }
        else {
            b = new ke1(list);
        }
        return b;
    }
    
    public static ke1 q(String s) {
        if (!s.contains("//")) {
            final String[] split = s.split("/");
            final ArrayList list = new ArrayList<String>(split.length);
            for (int length = split.length, i = 0; i < length; ++i) {
                s = split[i];
                if (!s.isEmpty()) {
                    list.add(s);
                }
            }
            return new ke1(list);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid path (");
        sb.append(s);
        sb.append("). Paths must not contain // in them.");
        throw new IllegalArgumentException(sb.toString());
    }
    
    @Override
    public String d() {
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < super.a.size(); ++i) {
            if (i > 0) {
                sb.append("/");
            }
            sb.append((String)super.a.get(i));
        }
        return sb.toString();
    }
    
    public ke1 o(final List list) {
        return new ke1(list);
    }
}

import com.google.protobuf.GeneratedMessageLite;
import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;
import java.util.HashMap;
import com.google.firestore.v1.k;
import java.util.Map;
import com.google.firestore.v1.Value;

// 
// Decompiled by Procyon v0.6.0
// 

public final class a11 implements Cloneable
{
    public Value a;
    public final Map b;
    
    public a11() {
        this((Value)((GeneratedMessageLite.a)Value.x0().I(k.b0())).p());
    }
    
    public a11(final Value a) {
        this.b = new HashMap();
        g9.d(a.w0() == Value.ValueTypeCase.MAP_VALUE, "ObjectValues should be backed by a MapValue", new Object[0]);
        g9.d(ql1.c(a) ^ true, "ServerTimestamps should not be used as an ObjectValue", new Object[0]);
        this.a = a;
    }
    
    public static a11 i(final Map map) {
        return new a11((Value)((GeneratedMessageLite.a)Value.x0().H(k.j0().B(map))).p());
    }
    
    public final k a(final s00 s00, final Map map) {
        final Value h = this.h(this.a, s00);
        xv0 j0;
        if (a32.w(h)) {
            j0 = h.s0().Y();
        }
        else {
            j0 = k.j0();
        }
        final Iterator iterator = map.entrySet().iterator();
        boolean b = false;
        while (iterator.hasNext()) {
            final Map.Entry<String, V> entry = (Map.Entry<String, V>)iterator.next();
            final String s2 = entry.getKey();
            final V value = entry.getValue();
            if (value instanceof Map) {
                final k a = this.a((s00)s00.c(s2), (Map)value);
                if (a == null) {
                    continue;
                }
                ((k.b)j0).C(s2, (Value)((GeneratedMessageLite.a)Value.x0().I(a)).p());
            }
            else if (value instanceof Value) {
                ((k.b)j0).C(s2, (Value)value);
            }
            else {
                if (!((k.b)j0).A(s2)) {
                    continue;
                }
                g9.d(value == null, "Expected entry to be a Map, a Value or null", new Object[0]);
                ((k.b)j0).D(s2);
            }
            b = true;
        }
        k k;
        if (b) {
            k = (k)((GeneratedMessageLite.a)j0).p();
        }
        else {
            k = null;
        }
        return k;
    }
    
    public final Value b() {
        synchronized (this.b) {
            final k a = this.a(s00.c, this.b);
            if (a != null) {
                this.a = (Value)((GeneratedMessageLite.a)Value.x0().I(a)).p();
                this.b.clear();
            }
            monitorexit(this.b);
            return this.a;
        }
    }
    
    public a11 d() {
        return new a11(this.b());
    }
    
    public void e(final s00 s00) {
        g9.d(s00.j() ^ true, "Cannot delete field for empty path on ObjectValue", new Object[0]);
        this.p(s00, null);
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof a11 && a32.q(this.b(), ((a11)o).b()));
    }
    
    public final q00 f(final k k) {
        final HashSet set = new HashSet();
        for (final Map.Entry<String, V> entry : k.d0().entrySet()) {
            final s00 r = s00.r(entry.getKey());
            if (a32.w((Value)entry.getValue())) {
                final Set c = this.f(((Value)entry.getValue()).s0()).c();
                if (!c.isEmpty()) {
                    final Iterator iterator2 = c.iterator();
                    while (iterator2.hasNext()) {
                        set.add(r.a((jb)iterator2.next()));
                    }
                    continue;
                }
            }
            set.add(r);
        }
        return q00.b(set);
    }
    
    public final Value h(Value e0, final s00 s00) {
        if (s00.j()) {
            return e0;
        }
        int n = 0;
        while (true) {
            final int l = s00.l();
            final k s2 = e0.s0();
            if (n >= l - 1) {
                return s2.e0(s00.g(), null);
            }
            e0 = s2.e0(s00.h(n), null);
            if (!a32.w(e0)) {
                return null;
            }
            ++n;
        }
    }
    
    @Override
    public int hashCode() {
        return this.b().hashCode();
    }
    
    public Value j(final s00 s00) {
        return this.h(this.b(), s00);
    }
    
    public q00 k() {
        return this.f(this.b().s0());
    }
    
    public Map l() {
        return this.b().s0().d0();
    }
    
    public void m(final s00 s00, final Value value) {
        g9.d(s00.j() ^ true, "Cannot set field for empty path on ObjectValue", new Object[0]);
        this.p(s00, value);
    }
    
    public void n(final Map map) {
        for (final Map.Entry<s00, Value> entry : map.entrySet()) {
            final s00 s00 = entry.getKey();
            if (entry.getValue() == null) {
                this.e(s00);
            }
            else {
                this.m(s00, entry.getValue());
            }
        }
    }
    
    public final void p(final s00 s00, final Value value) {
        Map b = this.b;
        for (int i = 0; i < s00.l() - 1; ++i) {
            final String h = s00.h(i);
            final Object value2 = b.get(h);
            if (value2 instanceof Map) {
                b = (Map)value2;
            }
            else {
                if (value2 instanceof Value) {
                    final Value value3 = (Value)value2;
                    if (value3.w0() == Value.ValueTypeCase.MAP_VALUE) {
                        final HashMap hashMap = new HashMap(value3.s0().d0());
                        b.put(h, hashMap);
                        b = hashMap;
                        continue;
                    }
                }
                final HashMap hashMap2 = new HashMap();
                b.put(h, hashMap2);
                b = hashMap2;
            }
        }
        b.put(s00.g(), value);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ObjectValue{internalValue=");
        sb.append(a32.b(this.b()));
        sb.append('}');
        return sb.toString();
    }
}

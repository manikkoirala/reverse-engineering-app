import java.util.Date;
import android.graphics.Color;
import java.text.ParseException;
import android.text.format.DateFormat;
import java.text.SimpleDateFormat;
import android.widget.TextView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import android.content.Context;
import java.util.ArrayList;
import android.util.SparseBooleanArray;
import android.widget.BaseAdapter;

// 
// Decompiled by Procyon v0.6.0
// 

public class d3 extends BaseAdapter
{
    public SparseBooleanArray a;
    public ArrayList b;
    public Context c;
    public boolean d;
    
    public d3(final Context c, final ArrayList b, final boolean d) {
        this.a = new SparseBooleanArray();
        this.b = b;
        this.c = c;
        this.d = d;
    }
    
    public SparseBooleanArray a() {
        return this.a;
    }
    
    public void b() {
        this.a = new SparseBooleanArray();
        this.notifyDataSetChanged();
    }
    
    public final void c(final int n, final boolean b) {
        if (b) {
            this.a.put(n, b);
        }
        else {
            this.a.delete(n);
        }
        this.notifyDataSetChanged();
    }
    
    public void d(final int n) {
        this.c(n, this.a.get(n) ^ true);
    }
    
    public int getCount() {
        if (this.b.size() >= 2 && !this.d) {
            return 2;
        }
        return this.b.size();
    }
    
    public Object getItem(final int index) {
        return this.b.get(index);
    }
    
    public long getItemId(final int n) {
        return n;
    }
    
    public View getView(int n, View view, ViewGroup inflate) {
        view = (View)this.b.get(n);
        inflate = (ViewGroup)((LayoutInflater)this.c.getSystemService("layout_inflater")).inflate(2131493029, (ViewGroup)null);
        final TextView textView = (TextView)((View)inflate).findViewById(2131297229);
        final TextView textView2 = (TextView)((View)inflate).findViewById(2131297306);
        final TextView textView3 = (TextView)((View)inflate).findViewById(2131297327);
        final TextView textView4 = (TextView)((View)inflate).findViewById(2131297377);
        final TextView textView5 = (TextView)((View)inflate).findViewById(2131297378);
        final TextView textView6 = (TextView)((View)inflate).findViewById(2131297212);
        textView.setText((CharSequence)((ay)view).b().getExamName());
        final StringBuilder sb = new StringBuilder();
        sb.append(((ay)view).c());
        sb.append("");
        textView2.setText((CharSequence)sb.toString());
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(((ay)view).d());
        sb2.append("");
        textView3.setText((CharSequence)sb2.toString());
        textView6.setText((CharSequence)((ay)view).b().getClassName());
        final String examDate = ((ay)view).b().getExamDate();
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            final Date parse = simpleDateFormat.parse(examDate);
            textView4.setText((CharSequence)DateFormat.format((CharSequence)"dd", parse));
            textView5.setText((CharSequence)DateFormat.format((CharSequence)"MMM", parse));
        }
        catch (final ParseException ex) {
            ex.printStackTrace();
        }
        ((View)inflate).findViewById(2131296829).setVisibility(8);
        ((View)inflate).findViewById(2131296828).setVisibility(8);
        ((View)inflate).findViewById(2131296830).setVisibility(8);
        ((View)inflate).findViewById(2131296827).setVisibility(8);
        Label_0386: {
            if (((ay)view).f()) {
                final int n2 = d3$a.a[((ay)view).e().ordinal()];
                if (n2 == 1) {
                    view = ((View)inflate).findViewById(2131296829);
                    break Label_0386;
                }
                if (n2 == 2) {
                    view = ((View)inflate).findViewById(2131296828);
                    break Label_0386;
                }
                if (n2 == 3) {
                    view = ((View)inflate).findViewById(2131296830);
                    break Label_0386;
                }
            }
            view = ((View)inflate).findViewById(2131296827);
        }
        view.setVisibility(0);
        if (this.a.get(n, false)) {
            n = this.c.getResources().getColor(2131099718);
        }
        else {
            n = Color.rgb(255, 255, 255);
        }
        ((View)inflate).setBackgroundColor(n);
        return (View)inflate;
    }
}

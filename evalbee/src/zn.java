import java.util.TreeSet;
import java.util.Collection;
import java.util.SortedSet;
import java.util.ArrayList;
import java.util.Collections;
import java.io.IOException;
import com.google.firebase.crashlytics.internal.model.CrashlyticsReport;
import java.util.Locale;
import java.util.Iterator;
import java.util.List;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.concurrent.atomic.AtomicInteger;
import java.io.FilenameFilter;
import java.util.Comparator;
import java.nio.charset.Charset;

// 
// Decompiled by Procyon v0.6.0
// 

public class zn
{
    public static final Charset e;
    public static final int f;
    public static final un g;
    public static final Comparator h;
    public static final FilenameFilter i;
    public final AtomicInteger a;
    public final z00 b;
    public final zm1 c;
    public final wm d;
    
    static {
        e = Charset.forName("UTF-8");
        f = 15;
        g = new un();
        h = new xn();
        i = new yn();
    }
    
    public zn(final z00 b, final zm1 c, final wm d) {
        this.a = new AtomicInteger(0);
        this.b = b;
        this.c = c;
        this.d = d;
    }
    
    public static String A(File file) {
        final byte[] array = new byte[8192];
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        file = (File)new FileInputStream(file);
        try {
            while (true) {
                final int read = ((FileInputStream)file).read(array);
                if (read <= 0) {
                    break;
                }
                byteArrayOutputStream.write(array, 0, read);
            }
            final String s = new String(byteArrayOutputStream.toByteArray(), zn.e);
            ((FileInputStream)file).close();
            return s;
        }
        finally {
            try {
                ((FileInputStream)file).close();
            }
            finally {
                final Throwable t;
                final Throwable exception;
                t.addSuppressed(exception);
            }
        }
    }
    
    public static void F(final File file, final String str) {
        final OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(file), zn.e);
        try {
            outputStreamWriter.write(str);
            outputStreamWriter.close();
        }
        finally {
            try {
                outputStreamWriter.close();
            }
            finally {
                final Throwable exception;
                ((Throwable)file).addSuppressed(exception);
            }
        }
    }
    
    public static void G(final File file, final String str, final long n) {
        final OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(file), zn.e);
        try {
            outputStreamWriter.write(str);
            file.setLastModified(h(n));
            outputStreamWriter.close();
        }
        finally {
            try {
                outputStreamWriter.close();
            }
            finally {
                final Throwable exception;
                ((Throwable)file).addSuppressed(exception);
            }
        }
    }
    
    public static int f(final List list, final int n) {
        int size = list.size();
        for (final File file : list) {
            if (size <= n) {
                return size;
            }
            z00.s(file);
            --size;
        }
        return size;
    }
    
    public static long h(final long n) {
        return n * 1000L;
    }
    
    public static String m(final int i, final boolean b) {
        final String format = String.format(Locale.US, "%010d", i);
        String str;
        if (b) {
            str = "_";
        }
        else {
            str = "";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("event");
        sb.append(format);
        sb.append(str);
        return sb.toString();
    }
    
    public static String o(final String s) {
        return s.substring(0, zn.f);
    }
    
    public static boolean s(final String s) {
        return s.startsWith("event") && s.endsWith("_");
    }
    
    public static boolean t(final File file, final String s) {
        return s.startsWith("event") && !s.endsWith("_");
    }
    
    public static int x(final File file, final File file2) {
        return o(file.getName()).compareTo(o(file2.getName()));
    }
    
    public final void B(final File obj, final CrashlyticsReport.d d, final String s, final CrashlyticsReport.a a) {
        final String d2 = this.d.d(s);
        try {
            final un g = zn.g;
            F(this.b.g(s), g.M(g.L(A(obj)).s(d).p(a).o(d2)));
        }
        catch (final IOException ex) {
            final zl0 f = zl0.f();
            final StringBuilder sb = new StringBuilder();
            sb.append("Could not synthesize final native report file for ");
            sb.append(obj);
            f.l(sb.toString(), ex);
        }
    }
    
    public final void C(final String s, final long n) {
        final List p2 = this.b.p(s, zn.i);
        if (p2.isEmpty()) {
            final zl0 f = zl0.f();
            final StringBuilder sb = new StringBuilder();
            sb.append("Session ");
            sb.append(s);
            sb.append(" has no events.");
            f.i(sb.toString());
            return;
        }
        Collections.sort((List<Comparable>)p2);
        final ArrayList list = new ArrayList();
        final Iterator iterator = p2.iterator();
        boolean b = false;
    Label_0096:
        while (true) {
            b = false;
            while (iterator.hasNext()) {
                final File obj = (File)iterator.next();
                try {
                    list.add(zn.g.j(A(obj)));
                    if (!b && !s(obj.getName())) {
                        continue Label_0096;
                    }
                    b = true;
                }
                catch (final IOException ex) {
                    final zl0 f2 = zl0.f();
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Could not add event to report for ");
                    sb2.append(obj);
                    f2.l(sb2.toString(), ex);
                }
            }
            break;
        }
        if (list.isEmpty()) {
            final zl0 f3 = zl0.f();
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Could not parse event files for session ");
            sb3.append(s);
            f3.k(sb3.toString());
            return;
        }
        this.D(this.b.o(s, "report"), list, n, b, f22.i(s, this.b), this.d.d(s));
    }
    
    public final void D(final File obj, final List list, final long n, final boolean b, final String s, final String str) {
        try {
            final un g = zn.g;
            final CrashlyticsReport q = g.L(A(obj)).t(n, b, s).o(str).q(list);
            final CrashlyticsReport.e m = q.m();
            if (m == null) {
                return;
            }
            final zl0 f = zl0.f();
            final StringBuilder sb = new StringBuilder();
            sb.append("appQualitySessionId: ");
            sb.append(str);
            f.b(sb.toString());
            File file;
            if (b) {
                file = this.b.j(m.i());
            }
            else {
                file = this.b.l(m.i());
            }
            F(file, g.M(q));
        }
        catch (final IOException ex) {
            final zl0 f2 = zl0.f();
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Could not synthesize final report file for ");
            sb2.append(obj);
            f2.l(sb2.toString(), ex);
        }
    }
    
    public final int E(final String s, final int n) {
        final List p2 = this.b.p(s, new vn());
        Collections.sort((List<Object>)p2, new wn());
        return f(p2, n);
    }
    
    public final SortedSet e(String str) {
        this.b.b();
        final SortedSet p = this.p();
        if (str != null) {
            p.remove(str);
        }
        if (p.size() <= 8) {
            return p;
        }
        while (p.size() > 8) {
            str = (String)p.last();
            final zl0 f = zl0.f();
            final StringBuilder sb = new StringBuilder();
            sb.append("Removing session over cap: ");
            sb.append(str);
            f.b(sb.toString());
            this.b.c(str);
            p.remove(str);
        }
        return p;
    }
    
    public final void g() {
        final int b = this.c.a().a.b;
        final List n = this.n();
        final int size = n.size();
        if (size <= b) {
            return;
        }
        final Iterator iterator = n.subList(b, size).iterator();
        while (iterator.hasNext()) {
            ((File)iterator.next()).delete();
        }
    }
    
    public void i() {
        this.j(this.b.m());
        this.j(this.b.k());
        this.j(this.b.h());
    }
    
    public final void j(final List list) {
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            ((File)iterator.next()).delete();
        }
    }
    
    public void k(final String s, final long n) {
        for (final String str : this.e(s)) {
            final zl0 f = zl0.f();
            final StringBuilder sb = new StringBuilder();
            sb.append("Finalizing report for session ");
            sb.append(str);
            f.i(sb.toString());
            this.C(str, n);
            this.b.c(str);
        }
        this.g();
    }
    
    public void l(final String str, final CrashlyticsReport.d d, final CrashlyticsReport.a a) {
        final File o = this.b.o(str, "report");
        final zl0 f = zl0.f();
        final StringBuilder sb = new StringBuilder();
        sb.append("Writing native session report for ");
        sb.append(str);
        sb.append(" to file: ");
        sb.append(o);
        f.b(sb.toString());
        this.B(o, d, str, a);
    }
    
    public final List n() {
        final ArrayList list = new ArrayList();
        list.addAll(this.b.k());
        list.addAll(this.b.h());
        final Comparator h = zn.h;
        Collections.sort((List<Object>)list, h);
        final List m = this.b.m();
        Collections.sort((List<Object>)m, h);
        list.addAll(m);
        return list;
    }
    
    public SortedSet p() {
        return new TreeSet(this.b.d()).descendingSet();
    }
    
    public long q(final String s) {
        return this.b.o(s, "start-time").lastModified();
    }
    
    public boolean r() {
        return !this.b.m().isEmpty() || !this.b.k().isEmpty() || !this.b.h().isEmpty();
    }
    
    public List w() {
        final List n = this.n();
        final ArrayList list = new ArrayList();
        for (final File obj : n) {
            try {
                list.add(ao.a(zn.g.L(A(obj)), obj.getName(), obj));
            }
            catch (final IOException ex) {
                final zl0 f = zl0.f();
                final StringBuilder sb = new StringBuilder();
                sb.append("Could not load report file ");
                sb.append(obj);
                sb.append("; deleting");
                f.l(sb.toString(), ex);
                obj.delete();
            }
        }
        return list;
    }
    
    public void y(final CrashlyticsReport.e.d d, final String str, final boolean b) {
        final int a = this.c.a().a.a;
        final String k = zn.g.k(d);
        final String m = m(this.a.getAndIncrement(), b);
        try {
            F(this.b.o(str, m), k);
        }
        catch (final IOException ex) {
            final zl0 f = zl0.f();
            final StringBuilder sb = new StringBuilder();
            sb.append("Could not persist event for session ");
            sb.append(str);
            f.l(sb.toString(), ex);
        }
        this.E(str, a);
    }
    
    public void z(final CrashlyticsReport crashlyticsReport) {
        final CrashlyticsReport.e m = crashlyticsReport.m();
        if (m == null) {
            zl0.f().b("Could not get session for report");
            return;
        }
        final String i = m.i();
        try {
            F(this.b.o(i, "report"), zn.g.M(crashlyticsReport));
            G(this.b.o(i, "start-time"), "", m.l());
        }
        catch (final IOException ex) {
            final zl0 f = zl0.f();
            final StringBuilder sb = new StringBuilder();
            sb.append("Could not persist report for session ");
            sb.append(i);
            f.c(sb.toString(), ex);
        }
    }
}

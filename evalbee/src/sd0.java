import android.os.Parcelable$Creator;
import android.os.Parcelable;
import android.os.Parcel;
import android.os.IBinder;
import android.os.Binder;
import android.net.Uri;
import android.os.Bundle;
import android.os.IInterface;

// 
// Decompiled by Procyon v0.6.0
// 

public interface sd0 extends IInterface
{
    public static final String b0 = "android$support$customtabs$ICustomTabsCallback".replace('$', '.');
    
    Bundle b(final String p0, final Bundle p1);
    
    void g(final int p0, final int p1, final Bundle p2);
    
    void h(final int p0, final Bundle p1);
    
    void j(final String p0, final Bundle p1);
    
    void m(final String p0, final Bundle p1);
    
    void n(final Bundle p0);
    
    void o(final int p0, final Uri p1, final boolean p2, final Bundle p3);
    
    public abstract static class a extends Binder implements sd0
    {
        public a() {
            this.attachInterface((IInterface)this, sd0.b0);
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(int int1, final Parcel parcel, final Parcel parcel2, final int n) {
            final String b0 = sd0.b0;
            if (int1 >= 1 && int1 <= 16777215) {
                parcel.enforceInterface(b0);
            }
            if (int1 == 1598968902) {
                parcel2.writeString(b0);
                return true;
            }
            switch (int1) {
                default: {
                    return super.onTransact(int1, parcel, parcel2, n);
                }
                case 8: {
                    this.g(parcel.readInt(), parcel.readInt(), (Bundle)c(parcel, Bundle.CREATOR));
                    return true;
                }
                case 7: {
                    final Bundle b2 = this.b(parcel.readString(), (Bundle)c(parcel, Bundle.CREATOR));
                    parcel2.writeNoException();
                    d(parcel2, (Parcelable)b2, 1);
                    return true;
                }
                case 6: {
                    int1 = parcel.readInt();
                    this.o(int1, (Uri)c(parcel, Uri.CREATOR), parcel.readInt() != 0, (Bundle)c(parcel, Bundle.CREATOR));
                    return true;
                }
                case 5: {
                    this.m(parcel.readString(), (Bundle)c(parcel, Bundle.CREATOR));
                    break;
                }
                case 4: {
                    this.n((Bundle)c(parcel, Bundle.CREATOR));
                    break;
                }
                case 3: {
                    this.j(parcel.readString(), (Bundle)c(parcel, Bundle.CREATOR));
                    return true;
                }
                case 2: {
                    this.h(parcel.readInt(), (Bundle)c(parcel, Bundle.CREATOR));
                    return true;
                }
            }
            parcel2.writeNoException();
            return true;
        }
    }
    
    public abstract static class b
    {
        public static Object c(final Parcel parcel, final Parcelable$Creator parcelable$Creator) {
            if (parcel.readInt() != 0) {
                return parcelable$Creator.createFromParcel(parcel);
            }
            return null;
        }
        
        public static void d(final Parcel parcel, final Parcelable parcelable, final int n) {
            if (parcelable != null) {
                parcel.writeInt(1);
                parcelable.writeToParcel(parcel, n);
            }
            else {
                parcel.writeInt(0);
            }
        }
    }
}

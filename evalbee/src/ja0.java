// 
// Decompiled by Procyon v0.6.0
// 

public abstract class ja0
{
    public static final Object a;
    public static final Object b;
    
    static {
        a = new Object();
        b = new Object();
    }
    
    public static double a(final double[] array, final double[] array2, final double[] array3, final double[] array4, final int n) {
        final int n2 = 0;
        for (int i = 0; i < n; ++i) {
            array4[i] = array2[i] - array[i];
        }
        final double n3 = 0.0;
        int j = 0;
        double n4 = 0.0;
        while (j < n) {
            final double n5 = array4[j];
            n4 += n5 * n5;
            ++j;
        }
        double n7;
        if (n4 != 0.0) {
            int k = 0;
            double n6 = 0.0;
            while (k < n) {
                n6 += array4[k] * (array3[k] - array[k]);
                ++k;
            }
            n7 = n6 / n4;
        }
        else {
            n7 = 0.0;
        }
        double n8;
        if (n7 < 0.0) {
            n8 = 0.0;
        }
        else {
            n8 = n7;
            if (n7 > 1.0) {
                n8 = 1.0;
            }
        }
        for (int l = 0; l < n; ++l) {
            array4[l] = array[l] + array4[l] * n8;
        }
        array4[n] = n8;
        double n9 = n3;
        for (int n10 = n2; n10 < n; ++n10) {
            final double n11 = array3[n10] - array4[n10];
            n9 += n11 * n11;
        }
        return n9;
    }
}

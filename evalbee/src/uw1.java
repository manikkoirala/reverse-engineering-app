import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;

// 
// Decompiled by Procyon v0.6.0
// 

public interface uw1
{
    ColorStateList getSupportButtonTintList();
    
    void setSupportButtonTintList(final ColorStateList p0);
    
    void setSupportButtonTintMode(final PorterDuff$Mode p0);
}

import com.google.common.cache.LongAddables;

// 
// Decompiled by Procyon v0.6.0
// 

public final class i implements j
{
    public final cm0 a;
    public final cm0 b;
    public final cm0 c;
    public final cm0 d;
    public final cm0 e;
    public final cm0 f;
    
    public i() {
        this.a = LongAddables.a();
        this.b = LongAddables.a();
        this.c = LongAddables.a();
        this.d = LongAddables.a();
        this.e = LongAddables.a();
        this.f = LongAddables.a();
    }
    
    public static long h(long n) {
        if (n < 0L) {
            n = Long.MAX_VALUE;
        }
        return n;
    }
    
    @Override
    public void a(final int n) {
        this.a.add(n);
    }
    
    @Override
    public void b() {
        this.f.increment();
    }
    
    @Override
    public void c(final long n) {
        this.c.increment();
        this.e.add(n);
    }
    
    @Override
    public void d(final int n) {
        this.b.add(n);
    }
    
    @Override
    public void e(final long n) {
        this.d.increment();
        this.e.add(n);
    }
    
    @Override
    public pe f() {
        return new pe(h(this.a.sum()), h(this.b.sum()), h(this.c.sum()), h(this.d.sum()), h(this.e.sum()), h(this.f.sum()));
    }
    
    public void g(final j j) {
        final pe f = j.f();
        this.a.add(f.b());
        this.b.add(f.e());
        this.c.add(f.d());
        this.d.add(f.c());
        this.e.add(f.f());
        this.f.add(f.a());
    }
}

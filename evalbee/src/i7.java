import android.widget.AbsSeekBar;
import android.graphics.drawable.Drawable$Callback;
import android.graphics.Canvas;
import android.content.Context;
import android.view.View;
import android.util.AttributeSet;
import android.widget.ProgressBar;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.widget.SeekBar;

// 
// Decompiled by Procyon v0.6.0
// 

public class i7 extends c7
{
    public final SeekBar d;
    public Drawable e;
    public ColorStateList f;
    public PorterDuff$Mode g;
    public boolean h;
    public boolean i;
    
    public i7(final SeekBar d) {
        super((ProgressBar)d);
        this.f = null;
        this.g = null;
        this.h = false;
        this.i = false;
        this.d = d;
    }
    
    @Override
    public void c(final AttributeSet set, int n) {
        super.c(set, n);
        final Context context = ((View)this.d).getContext();
        final int[] t = bc1.T;
        final tw1 v = tw1.v(context, set, t, n, 0);
        final SeekBar d = this.d;
        o32.o0((View)d, ((View)d).getContext(), t, set, v.r(), n, 0);
        final Drawable h = v.h(bc1.U);
        if (h != null) {
            ((AbsSeekBar)this.d).setThumb(h);
        }
        this.j(v.g(bc1.V));
        n = bc1.X;
        if (v.s(n)) {
            this.g = fv.e(v.k(n, -1), this.g);
            this.i = true;
        }
        n = bc1.W;
        if (v.s(n)) {
            this.f = v.c(n);
            this.h = true;
        }
        v.w();
        this.f();
    }
    
    public final void f() {
        final Drawable e = this.e;
        if (e != null && (this.h || this.i)) {
            final Drawable r = wu.r(e.mutate());
            this.e = r;
            if (this.h) {
                wu.o(r, this.f);
            }
            if (this.i) {
                wu.p(this.e, this.g);
            }
            if (this.e.isStateful()) {
                this.e.setState(((View)this.d).getDrawableState());
            }
        }
    }
    
    public void g(final Canvas canvas) {
        if (this.e != null) {
            final int max = ((ProgressBar)this.d).getMax();
            int n = 1;
            if (max > 1) {
                final int intrinsicWidth = this.e.getIntrinsicWidth();
                final int intrinsicHeight = this.e.getIntrinsicHeight();
                int n2;
                if (intrinsicWidth >= 0) {
                    n2 = intrinsicWidth / 2;
                }
                else {
                    n2 = 1;
                }
                if (intrinsicHeight >= 0) {
                    n = intrinsicHeight / 2;
                }
                this.e.setBounds(-n2, -n, n2, n);
                final float n3 = (((View)this.d).getWidth() - ((View)this.d).getPaddingLeft() - ((View)this.d).getPaddingRight()) / (float)max;
                final int save = canvas.save();
                canvas.translate((float)((View)this.d).getPaddingLeft(), (float)(((View)this.d).getHeight() / 2));
                for (int i = 0; i <= max; ++i) {
                    this.e.draw(canvas);
                    canvas.translate(n3, 0.0f);
                }
                canvas.restoreToCount(save);
            }
        }
    }
    
    public void h() {
        final Drawable e = this.e;
        if (e != null && e.isStateful() && e.setState(((View)this.d).getDrawableState())) {
            ((View)this.d).invalidateDrawable(e);
        }
    }
    
    public void i() {
        final Drawable e = this.e;
        if (e != null) {
            e.jumpToCurrentState();
        }
    }
    
    public void j(final Drawable e) {
        final Drawable e2 = this.e;
        if (e2 != null) {
            e2.setCallback((Drawable$Callback)null);
        }
        if ((this.e = e) != null) {
            e.setCallback((Drawable$Callback)this.d);
            wu.m(e, o32.B((View)this.d));
            if (e.isStateful()) {
                e.setState(((View)this.d).getDrawableState());
            }
            this.f();
        }
        ((View)this.d).invalidate();
    }
}

import java.util.concurrent.TimeUnit;
import java.util.concurrent.Delayed;
import java.util.concurrent.ScheduledFuture;
import androidx.concurrent.futures.AbstractResolvableFuture;

// 
// Decompiled by Procyon v0.6.0
// 

public class ds extends AbstractResolvableFuture implements ScheduledFuture
{
    public final ScheduledFuture h;
    
    public ds(final c c) {
        this.h = c.a(new b(this) {
            public final ds a;
            
            @Override
            public void a(final Throwable t) {
                this.a.q(t);
            }
            
            @Override
            public void set(final Object o) {
                this.a.p(o);
            }
        });
    }
    
    @Override
    public void c() {
        this.h.cancel(this.s());
    }
    
    @Override
    public long getDelay(final TimeUnit timeUnit) {
        return this.h.getDelay(timeUnit);
    }
    
    public int v(final Delayed delayed) {
        return this.h.compareTo(delayed);
    }
    
    public interface b
    {
        void a(final Throwable p0);
        
        void set(final Object p0);
    }
    
    public interface c
    {
        ScheduledFuture a(final b p0);
    }
}

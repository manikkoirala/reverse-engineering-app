import android.graphics.BlendModeColorFilter;
import android.graphics.BlendMode;
import android.graphics.PorterDuff$Mode;
import android.graphics.PorterDuffColorFilter;
import android.os.Build$VERSION;
import android.graphics.ColorFilter;
import androidx.core.graphics.BlendModeCompat;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class cc
{
    public static ColorFilter a(final int n, final BlendModeCompat blendModeCompat) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        final ColorFilter colorFilter = null;
        final ColorFilter colorFilter2 = null;
        if (sdk_INT >= 29) {
            final Object a = dc.b.a(blendModeCompat);
            ColorFilter a2 = colorFilter2;
            if (a != null) {
                a2 = cc.a.a(n, a);
            }
            return a2;
        }
        final PorterDuff$Mode a3 = dc.a(blendModeCompat);
        Object o = colorFilter;
        if (a3 != null) {
            o = new PorterDuffColorFilter(n, a3);
        }
        return (ColorFilter)o;
    }
    
    public abstract static class a
    {
        public static ColorFilter a(final int n, final Object o) {
            return (ColorFilter)new BlendModeColorFilter(n, (BlendMode)o);
        }
    }
}

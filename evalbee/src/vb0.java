import java.util.Iterator;
import androidx.constraintlayout.core.widgets.a;
import androidx.constraintlayout.core.widgets.e;
import androidx.constraintlayout.core.widgets.d;
import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.widgets.f;
import java.util.ArrayList;
import androidx.constraintlayout.core.widgets.ConstraintWidget;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class vb0
{
    public static b62 a(final ConstraintWidget constraintWidget, final int n, final ArrayList list, b62 o) {
        int n2;
        if (n == 0) {
            n2 = constraintWidget.I0;
        }
        else {
            n2 = constraintWidget.J0;
        }
        final int n3 = 0;
        b62 b62;
        if (n2 != -1 && (o == null || n2 != o.b)) {
            int index = 0;
            while (true) {
                b62 = o;
                if (index >= list.size()) {
                    break;
                }
                b62 = list.get(index);
                if (b62.c() == n2) {
                    if (o != null) {
                        o.g(n, b62);
                        list.remove(o);
                    }
                    break;
                }
                ++index;
            }
        }
        else {
            b62 = o;
            if (n2 != -1) {
                return o;
            }
        }
        if ((o = b62) == null) {
            o = b62;
            if (constraintWidget instanceof gd0) {
                final int s1 = ((gd0)constraintWidget).s1(n);
                o = b62;
                if (s1 != -1) {
                    int index2 = 0;
                    while (true) {
                        o = b62;
                        if (index2 >= list.size()) {
                            break;
                        }
                        o = (b62)list.get(index2);
                        if (o.c() == s1) {
                            break;
                        }
                        ++index2;
                    }
                }
            }
            b62 e;
            if ((e = o) == null) {
                e = new b62(n);
            }
            list.add(e);
            o = e;
        }
        if (o.a(constraintWidget)) {
            if (constraintWidget instanceof f) {
                final f f = (f)constraintWidget;
                final ConstraintAnchor r1 = f.r1();
                int n4 = n3;
                if (f.s1() == 0) {
                    n4 = 1;
                }
                r1.c(n4, list, o);
            }
            final int c = o.c();
            ConstraintAnchor constraintAnchor;
            if (n == 0) {
                constraintWidget.I0 = c;
                constraintWidget.O.c(n, list, o);
                constraintAnchor = constraintWidget.Q;
            }
            else {
                constraintWidget.J0 = c;
                constraintWidget.P.c(n, list, o);
                constraintWidget.S.c(n, list, o);
                constraintAnchor = constraintWidget.R;
            }
            constraintAnchor.c(n, list, o);
            constraintWidget.V.c(n, list, o);
        }
        return o;
    }
    
    public static b62 b(final ArrayList list, final int n) {
        for (int size = list.size(), i = 0; i < size; ++i) {
            final b62 b62 = list.get(i);
            if (n == b62.b) {
                return b62;
            }
        }
        return null;
    }
    
    public static boolean c(final d d, final lb.b b) {
        final ArrayList r1 = d.r1();
        final int size = r1.size();
        for (int i = 0; i < size; ++i) {
            final ConstraintWidget constraintWidget = r1.get(i);
            if (!d(d.A(), d.T(), constraintWidget.A(), constraintWidget.T())) {
                return false;
            }
            if (constraintWidget instanceof e) {
                return false;
            }
        }
        int j = 0;
        ArrayList list = null;
        ArrayList list2 = null;
        ArrayList list3 = null;
        ArrayList list4 = null;
        ArrayList<a> list5 = null;
        ArrayList<a> list6 = null;
        while (j < size) {
            final ConstraintWidget constraintWidget2 = r1.get(j);
            if (!d(d.A(), d.T(), constraintWidget2.A(), constraintWidget2.T())) {
                d.T1(0, constraintWidget2, b, d.p1, lb.a.k);
            }
            final boolean b2 = constraintWidget2 instanceof f;
            ArrayList list7 = list;
            ArrayList list8 = list3;
            if (b2) {
                final f f = (f)constraintWidget2;
                ArrayList list9 = list3;
                if (f.s1() == 0) {
                    if ((list9 = list3) == null) {
                        list9 = new ArrayList();
                    }
                    list9.add(f);
                }
                list7 = list;
                list8 = list9;
                if (f.s1() == 1) {
                    ArrayList list10;
                    if ((list10 = list) == null) {
                        list10 = new ArrayList();
                    }
                    list10.add(f);
                    list8 = list9;
                    list7 = list10;
                }
            }
            ArrayList list11 = list2;
            ArrayList list12 = list4;
            Label_0468: {
                if (constraintWidget2 instanceof gd0) {
                    gd0 e2;
                    ArrayList list14;
                    if (constraintWidget2 instanceof a) {
                        final a e = (a)constraintWidget2;
                        ArrayList list13 = list2;
                        if (e.x1() == 0) {
                            if ((list13 = list2) == null) {
                                list13 = new ArrayList();
                            }
                            list13.add(e);
                        }
                        list11 = list13;
                        list12 = list4;
                        if (e.x1() != 1) {
                            break Label_0468;
                        }
                        e2 = e;
                        list11 = list13;
                        if ((list14 = list4) == null) {
                            list14 = new ArrayList();
                            e2 = e;
                            list11 = list13;
                        }
                    }
                    else {
                        final gd0 e3 = (gd0)constraintWidget2;
                        ArrayList list15;
                        if ((list15 = list2) == null) {
                            list15 = new ArrayList();
                        }
                        list15.add(e3);
                        e2 = e3;
                        list11 = list15;
                        if ((list14 = list4) == null) {
                            list14 = new ArrayList();
                            list11 = list15;
                            e2 = e3;
                        }
                    }
                    list14.add(e2);
                    list12 = list14;
                }
            }
            ArrayList<a> list16 = list5;
            if (constraintWidget2.O.f == null) {
                list16 = list5;
                if (constraintWidget2.Q.f == null) {
                    list16 = list5;
                    if (!b2) {
                        list16 = list5;
                        if (!(constraintWidget2 instanceof a)) {
                            ArrayList<a> list17;
                            if ((list17 = list5) == null) {
                                list17 = new ArrayList<a>();
                            }
                            list17.add((a)constraintWidget2);
                            list16 = list17;
                        }
                    }
                }
            }
            ArrayList<a> list18 = list6;
            if (constraintWidget2.P.f == null) {
                list18 = list6;
                if (constraintWidget2.R.f == null) {
                    list18 = list6;
                    if (constraintWidget2.S.f == null) {
                        list18 = list6;
                        if (!b2) {
                            list18 = list6;
                            if (!(constraintWidget2 instanceof a)) {
                                ArrayList<a> list19;
                                if ((list19 = list6) == null) {
                                    list19 = new ArrayList<a>();
                                }
                                list19.add((a)constraintWidget2);
                                list18 = list19;
                            }
                        }
                    }
                }
            }
            ++j;
            list = list7;
            list2 = list11;
            list3 = list8;
            list4 = list12;
            list5 = list16;
            list6 = list18;
        }
        final ArrayList list20 = new ArrayList();
        if (list != null) {
            final Iterator iterator = list.iterator();
            while (iterator.hasNext()) {
                a((ConstraintWidget)iterator.next(), 0, list20, null);
            }
        }
        if (list2 != null) {
            for (final gd0 gd0 : list2) {
                final b62 a = a(gd0, 0, list20, null);
                gd0.r1(list20, 0, a);
                a.b(list20);
            }
        }
        final ConstraintAnchor o = d.o(ConstraintAnchor.Type.LEFT);
        if (o.d() != null) {
            final Iterator iterator3 = o.d().iterator();
            while (iterator3.hasNext()) {
                a(((ConstraintAnchor)iterator3.next()).d, 0, list20, null);
            }
        }
        final ConstraintAnchor o2 = d.o(ConstraintAnchor.Type.RIGHT);
        if (o2.d() != null) {
            final Iterator iterator4 = o2.d().iterator();
            while (iterator4.hasNext()) {
                a(((ConstraintAnchor)iterator4.next()).d, 0, list20, null);
            }
        }
        final ConstraintAnchor o3 = d.o(ConstraintAnchor.Type.CENTER);
        if (o3.d() != null) {
            final Iterator iterator5 = o3.d().iterator();
            while (iterator5.hasNext()) {
                a(((ConstraintAnchor)iterator5.next()).d, 0, list20, null);
            }
        }
        if (list5 != null) {
            final Iterator<a> iterator6 = list5.iterator();
            while (iterator6.hasNext()) {
                a(iterator6.next(), 0, list20, null);
            }
        }
        if (list3 != null) {
            final Iterator iterator7 = list3.iterator();
            while (iterator7.hasNext()) {
                a((ConstraintWidget)iterator7.next(), 1, list20, null);
            }
        }
        if (list4 != null) {
            for (final gd0 gd2 : list4) {
                final b62 a2 = a(gd2, 1, list20, null);
                gd2.r1(list20, 1, a2);
                a2.b(list20);
            }
        }
        final ConstraintAnchor o4 = d.o(ConstraintAnchor.Type.TOP);
        if (o4.d() != null) {
            final Iterator iterator9 = o4.d().iterator();
            while (iterator9.hasNext()) {
                a(((ConstraintAnchor)iterator9.next()).d, 1, list20, null);
            }
        }
        final ConstraintAnchor o5 = d.o(ConstraintAnchor.Type.BASELINE);
        if (o5.d() != null) {
            final Iterator iterator10 = o5.d().iterator();
            while (iterator10.hasNext()) {
                a(((ConstraintAnchor)iterator10.next()).d, 1, list20, null);
            }
        }
        final ConstraintAnchor o6 = d.o(ConstraintAnchor.Type.BOTTOM);
        if (o6.d() != null) {
            final Iterator iterator11 = o6.d().iterator();
            while (iterator11.hasNext()) {
                a(((ConstraintAnchor)iterator11.next()).d, 1, list20, null);
            }
        }
        final ConstraintAnchor o7 = d.o(ConstraintAnchor.Type.CENTER);
        if (o7.d() != null) {
            final Iterator iterator12 = o7.d().iterator();
            while (iterator12.hasNext()) {
                a(((ConstraintAnchor)iterator12.next()).d, 1, list20, null);
            }
        }
        if (list6 != null) {
            final Iterator<a> iterator13 = list6.iterator();
            while (iterator13.hasNext()) {
                a(iterator13.next(), 1, list20, null);
            }
        }
        for (int k = 0; k < size; ++k) {
            final ConstraintWidget constraintWidget3 = r1.get(k);
            if (constraintWidget3.s0()) {
                final b62 b3 = b(list20, constraintWidget3.I0);
                final b62 b4 = b(list20, constraintWidget3.J0);
                if (b3 != null && b4 != null) {
                    b3.g(0, b4);
                    b4.i(2);
                    list20.remove(b3);
                }
            }
        }
        if (list20.size() <= 1) {
            return false;
        }
        b62 b7 = null;
        Label_1563: {
            if (d.A() == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                final Iterator iterator14 = list20.iterator();
                b62 b5 = null;
                int n = 0;
                while (iterator14.hasNext()) {
                    final b62 b6 = (b62)iterator14.next();
                    if (b6.d() == 1) {
                        continue;
                    }
                    b6.h(false);
                    final int f2 = b6.f(d.L1(), 0);
                    if (f2 <= n) {
                        continue;
                    }
                    b5 = b6;
                    n = f2;
                }
                if (b5 != null) {
                    d.P0(ConstraintWidget.DimensionBehaviour.FIXED);
                    d.k1(n);
                    b5.h(true);
                    b7 = b5;
                    break Label_1563;
                }
            }
            b7 = null;
        }
        if (d.T() == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
            final Iterator iterator15 = list20.iterator();
            b62 b8 = null;
            int n2 = 0;
            while (iterator15.hasNext()) {
                final b62 b9 = (b62)iterator15.next();
                if (b9.d() == 0) {
                    continue;
                }
                b9.h(false);
                final int f3 = b9.f(d.L1(), 1);
                if (f3 <= n2) {
                    continue;
                }
                b8 = b9;
                n2 = f3;
            }
            if (b8 != null) {
                d.g1(ConstraintWidget.DimensionBehaviour.FIXED);
                d.L0(n2);
                b8.h(true);
                final b62 b10 = b8;
                return b7 != null || b10 != null;
            }
        }
        final b62 b10 = null;
        return b7 != null || b10 != null;
    }
    
    public static boolean d(ConstraintWidget.DimensionBehaviour wrap_CONTENT, final ConstraintWidget.DimensionBehaviour dimensionBehaviour, final ConstraintWidget.DimensionBehaviour dimensionBehaviour2, final ConstraintWidget.DimensionBehaviour dimensionBehaviour3) {
        final ConstraintWidget.DimensionBehaviour fixed = ConstraintWidget.DimensionBehaviour.FIXED;
        boolean b = false;
        Label_0047: {
            if (dimensionBehaviour2 != fixed) {
                final ConstraintWidget.DimensionBehaviour wrap_CONTENT2 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                if (dimensionBehaviour2 != wrap_CONTENT2) {
                    if (dimensionBehaviour2 != ConstraintWidget.DimensionBehaviour.MATCH_PARENT || wrap_CONTENT == wrap_CONTENT2) {
                        b = false;
                        break Label_0047;
                    }
                }
            }
            b = true;
        }
        if (dimensionBehaviour3 != fixed) {
            wrap_CONTENT = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
            if (dimensionBehaviour3 != wrap_CONTENT) {
                if (dimensionBehaviour3 != ConstraintWidget.DimensionBehaviour.MATCH_PARENT || dimensionBehaviour == wrap_CONTENT) {
                    final boolean b2 = false;
                    return b || b2;
                }
            }
        }
        final boolean b2 = true;
        return b || b2;
    }
}

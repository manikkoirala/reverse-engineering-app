import android.adservices.topics.GetTopicsRequest;
import android.content.Context;
import androidx.privacysandbox.ads.adservices.topics.TopicsManagerImplCommon;

// 
// Decompiled by Procyon v0.6.0
// 

public final class sx1 extends TopicsManagerImplCommon
{
    public sx1(final Context context) {
        fg0.e((Object)context, "context");
        final Object systemService = context.getSystemService(lx1.a());
        fg0.d(systemService, "context.getSystemService\u2026opicsManager::class.java)");
        super(mx1.a(systemService));
    }
    
    @Override
    public GetTopicsRequest c(final za0 za0) {
        fg0.e((Object)za0, "request");
        final GetTopicsRequest a = rx1.a(qx1.a(px1.a(ox1.a(), za0.a()), za0.b()));
        fg0.d((Object)a, "Builder()\n            .s\u2026ion)\n            .build()");
        return a;
    }
}

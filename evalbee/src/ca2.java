import androidx.work.b;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.UUID;
import java.util.ArrayList;
import androidx.work.impl.background.systemalarm.RescheduleReceiver;
import java.util.Collection;
import androidx.work.WorkInfo$State;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.List;
import androidx.work.impl.WorkDatabase;
import androidx.work.a;
import androidx.work.c;
import androidx.work.WorkerParameters;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class ca2 implements Runnable
{
    public static final String w;
    public Context a;
    public final String b;
    public WorkerParameters.a c;
    public p92 d;
    public androidx.work.c e;
    public hu1 f;
    public androidx.work.c.a g;
    public a h;
    public ch i;
    public q70 j;
    public WorkDatabase k;
    public q92 l;
    public qs m;
    public List n;
    public String p;
    public um1 q;
    public final um1 t;
    public volatile int v;
    
    static {
        w = xl0.i("WorkerWrapper");
    }
    
    public ca2(final c c) {
        this.g = androidx.work.c.a.a();
        this.q = um1.s();
        this.t = um1.s();
        this.v = -256;
        this.a = c.a;
        this.f = c.d;
        this.j = c.c;
        final p92 g = c.g;
        this.d = g;
        this.b = g.a;
        this.c = c.i;
        this.e = c.b;
        final a e = c.e;
        this.h = e;
        this.i = e.a();
        final WorkDatabase f = c.f;
        this.k = f;
        this.l = f.K();
        this.m = this.k.F();
        this.n = ca2.c.a(c);
    }
    
    public final String b(final List list) {
        final StringBuilder sb = new StringBuilder("Work [ id=");
        sb.append(this.b);
        sb.append(", tags={ ");
        final Iterator iterator = list.iterator();
        int n = 1;
        while (iterator.hasNext()) {
            final String str = (String)iterator.next();
            if (n != 0) {
                n = 0;
            }
            else {
                sb.append(", ");
            }
            sb.append(str);
        }
        sb.append(" } ]");
        return sb.toString();
    }
    
    public ik0 c() {
        return this.q;
    }
    
    public x82 d() {
        return u92.a(this.d);
    }
    
    public p92 e() {
        return this.d;
    }
    
    public final void f(final androidx.work.c.a a) {
        if (a instanceof androidx.work.c.a.c) {
            final xl0 e = xl0.e();
            final String w = ca2.w;
            final StringBuilder sb = new StringBuilder();
            sb.append("Worker result SUCCESS for ");
            sb.append(this.p);
            e.f(w, sb.toString());
            if (!this.d.m()) {
                this.q();
                return;
            }
        }
        else {
            if (a instanceof androidx.work.c.a.b) {
                final xl0 e2 = xl0.e();
                final String w2 = ca2.w;
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Worker result RETRY for ");
                sb2.append(this.p);
                e2.f(w2, sb2.toString());
                this.k();
                return;
            }
            final xl0 e3 = xl0.e();
            final String w3 = ca2.w;
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Worker result FAILURE for ");
            sb3.append(this.p);
            e3.f(w3, sb3.toString());
            if (!this.d.m()) {
                this.p();
                return;
            }
        }
        this.l();
    }
    
    public void g(final int v) {
        this.v = v;
        this.r();
        this.t.cancel(true);
        if (this.e != null && this.t.isCancelled()) {
            this.e.stop(v);
        }
        else {
            final StringBuilder sb = new StringBuilder();
            sb.append("WorkSpec ");
            sb.append(this.d);
            sb.append(" is already done. Not interrupting.");
            xl0.e().a(ca2.w, sb.toString());
        }
    }
    
    public final void h(String e) {
        final LinkedList list = new LinkedList();
        list.add(e);
        while (!list.isEmpty()) {
            e = (String)list.remove();
            if (this.l.d(e) != WorkInfo$State.CANCELLED) {
                this.l.i(WorkInfo$State.FAILED, e);
            }
            list.addAll(this.m.a(e));
        }
    }
    
    public void j() {
        if (!this.r()) {
            this.k.e();
            try {
                final WorkInfo$State d = this.l.d(this.b);
                this.k.J().a(this.b);
                if (d == null) {
                    this.m(false);
                }
                else if (d == WorkInfo$State.RUNNING) {
                    this.f(this.g);
                }
                else if (!d.isFinished()) {
                    this.v = -512;
                    this.k();
                }
                this.k.D();
            }
            finally {
                this.k.i();
            }
        }
    }
    
    public final void k() {
        this.k.e();
        try {
            this.l.i(WorkInfo$State.ENQUEUED, this.b);
            this.l.j(this.b, this.i.currentTimeMillis());
            this.l.o(this.b, this.d.h());
            this.l.v(this.b, -1L);
            this.k.D();
        }
        finally {
            this.k.i();
            this.m(true);
        }
    }
    
    public final void l() {
        this.k.e();
        try {
            this.l.j(this.b, this.i.currentTimeMillis());
            this.l.i(WorkInfo$State.ENQUEUED, this.b);
            this.l.m(this.b);
            this.l.o(this.b, this.d.h());
            this.l.p(this.b);
            this.l.v(this.b, -1L);
            this.k.D();
        }
        finally {
            this.k.i();
            this.m(false);
        }
    }
    
    public final void m(final boolean b) {
        this.k.e();
        try {
            if (!this.k.K().k()) {
                n21.c(this.a, RescheduleReceiver.class, false);
            }
            if (b) {
                this.l.i(WorkInfo$State.ENQUEUED, this.b);
                this.l.b(this.b, this.v);
                this.l.v(this.b, -1L);
            }
            this.k.D();
            this.k.i();
            this.q.o(b);
        }
        finally {
            this.k.i();
        }
    }
    
    public final void n() {
        final WorkInfo$State d = this.l.d(this.b);
        boolean b;
        if (d == WorkInfo$State.RUNNING) {
            final xl0 e = xl0.e();
            final String w = ca2.w;
            final StringBuilder sb = new StringBuilder();
            sb.append("Status for ");
            sb.append(this.b);
            sb.append(" is RUNNING; not doing any work and rescheduling for later execution");
            e.a(w, sb.toString());
            b = true;
        }
        else {
            final xl0 e2 = xl0.e();
            final String w2 = ca2.w;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Status for ");
            sb2.append(this.b);
            sb2.append(" is ");
            sb2.append(d);
            sb2.append(" ; not doing any work");
            e2.a(w2, sb2.toString());
            b = false;
        }
        this.m(b);
    }
    
    public final void o() {
        if (this.r()) {
            return;
        }
        this.k.e();
        try {
            final p92 d = this.d;
            if (d.b != WorkInfo$State.ENQUEUED) {
                this.n();
                this.k.D();
                final xl0 e = xl0.e();
                final String w = ca2.w;
                final StringBuilder sb = new StringBuilder();
                sb.append(this.d.c);
                sb.append(" is not in ENQUEUED state. Nothing more to do");
                e.a(w, sb.toString());
                return;
            }
            if ((d.m() || this.d.l()) && this.i.currentTimeMillis() < this.d.c()) {
                xl0.e().a(ca2.w, String.format("Delaying execution for %s because it is being executed before schedule.", this.d.c));
                this.m(true);
                this.k.D();
                return;
            }
            this.k.D();
            this.k.i();
            b b;
            if (this.d.m()) {
                b = this.d.e;
            }
            else {
                final kf0 b2 = this.h.f().b(this.d.d);
                if (b2 == null) {
                    final xl0 e2 = xl0.e();
                    final String w2 = ca2.w;
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Could not create Input Merger ");
                    sb2.append(this.d.d);
                    e2.c(w2, sb2.toString());
                    this.p();
                    return;
                }
                final ArrayList list = new ArrayList();
                list.add(this.d.e);
                list.addAll(this.l.g(this.b));
                b = b2.a(list);
            }
            final UUID fromString = UUID.fromString(this.b);
            final List n = this.n;
            final WorkerParameters.a c = this.c;
            final p92 d2 = this.d;
            final WorkerParameters workerParameters = new WorkerParameters(fromString, b, n, c, d2.k, d2.f(), this.h.d(), this.f, this.h.n(), new l92(this.k, this.f), new w82(this.k, this.j, this.f));
            if (this.e == null) {
                this.e = this.h.n().b(this.a, this.d.c, workerParameters);
            }
            final androidx.work.c e3 = this.e;
            if (e3 == null) {
                final xl0 e4 = xl0.e();
                final String w3 = ca2.w;
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Could not create Worker ");
                sb3.append(this.d.c);
                e4.c(w3, sb3.toString());
                this.p();
                return;
            }
            if (e3.isUsed()) {
                final xl0 e5 = xl0.e();
                final String w4 = ca2.w;
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("Received an already-used Worker ");
                sb4.append(this.d.c);
                sb4.append("; Worker Factory should return new instances");
                e5.c(w4, sb4.toString());
                this.p();
                return;
            }
            this.e.setUsed();
            if (this.s()) {
                if (this.r()) {
                    return;
                }
                final v82 v82 = new v82(this.a, this.d, this.e, workerParameters.b(), this.f);
                this.f.c().execute(v82);
                final ik0 b3 = v82.b();
                this.t.addListener(new ba2(this, b3), new bt1());
                b3.addListener(new Runnable(this, b3) {
                    public final ik0 a;
                    public final ca2 b;
                    
                    @Override
                    public void run() {
                        if (this.b.t.isCancelled()) {
                            return;
                        }
                        try {
                            this.a.get();
                            final xl0 e = xl0.e();
                            final String w = ca2.w;
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Starting work for ");
                            sb.append(this.b.d.c);
                            e.a(w, sb.toString());
                            final ca2 b = this.b;
                            b.t.q(b.e.startWork());
                        }
                        finally {
                            final Throwable t;
                            this.b.t.p(t);
                        }
                    }
                }, this.f.c());
                this.t.addListener(new Runnable(this, this.p) {
                    public final String a;
                    public final ca2 b;
                    
                    @Override
                    public void run() {
                        try {
                            try {
                                final androidx.work.c.a a = (androidx.work.c.a)this.b.t.get();
                                if (a == null) {
                                    final xl0 e = xl0.e();
                                    final String w = ca2.w;
                                    final StringBuilder sb = new StringBuilder();
                                    sb.append(this.b.d.c);
                                    sb.append(" returned a null result. Treating it as a failure.");
                                    e.c(w, sb.toString());
                                }
                                final xl0 e2 = xl0.e();
                                final String w2 = ca2.w;
                                final StringBuilder sb2 = new StringBuilder();
                                sb2.append(this.b.d.c);
                                sb2.append(" returned a ");
                                sb2.append(a);
                                sb2.append(".");
                                e2.a(w2, sb2.toString());
                                this.b.g = a;
                            }
                            finally {}
                        }
                        catch (final ExecutionException t) {
                            goto Label_0155;
                        }
                        catch (final InterruptedException ex) {}
                        catch (final CancellationException ex2) {
                            final xl0 e3 = xl0.e();
                            final String w3 = ca2.w;
                            final StringBuilder sb3 = new StringBuilder();
                            sb3.append(this.a);
                            sb3.append(" was cancelled");
                            e3.g(w3, sb3.toString(), ex2);
                        }
                        this.b.j();
                        return;
                        this.b.j();
                    }
                }, this.f.d());
            }
            else {
                this.n();
            }
        }
        finally {
            this.k.i();
        }
    }
    
    public void p() {
        this.k.e();
        try {
            this.h(this.b);
            final b e = ((androidx.work.c.a.a)this.g).e();
            this.l.o(this.b, this.d.h());
            this.l.y(this.b, e);
            this.k.D();
        }
        finally {
            this.k.i();
            this.m(false);
        }
    }
    
    public final void q() {
        this.k.e();
        try {
            this.l.i(WorkInfo$State.SUCCEEDED, this.b);
            this.l.y(this.b, ((androidx.work.c.a.c)this.g).e());
            final long currentTimeMillis = this.i.currentTimeMillis();
            for (final String str : this.m.a(this.b)) {
                if (this.l.d(str) == WorkInfo$State.BLOCKED && this.m.b(str)) {
                    final xl0 e = xl0.e();
                    final String w = ca2.w;
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Setting status to enqueued for ");
                    sb.append(str);
                    e.f(w, sb.toString());
                    this.l.i(WorkInfo$State.ENQUEUED, str);
                    this.l.j(str, currentTimeMillis);
                }
            }
            this.k.D();
        }
        finally {
            this.k.i();
            this.m(false);
        }
    }
    
    public final boolean r() {
        if (this.v != -256) {
            final xl0 e = xl0.e();
            final String w = ca2.w;
            final StringBuilder sb = new StringBuilder();
            sb.append("Work interrupted for ");
            sb.append(this.p);
            e.a(w, sb.toString());
            final WorkInfo$State d = this.l.d(this.b);
            if (d == null) {
                this.m(false);
            }
            else {
                this.m(d.isFinished() ^ true);
            }
            return true;
        }
        return false;
    }
    
    @Override
    public void run() {
        this.p = this.b(this.n);
        this.o();
    }
    
    public final boolean s() {
        this.k.e();
        try {
            boolean b;
            if (this.l.d(this.b) == WorkInfo$State.ENQUEUED) {
                this.l.i(WorkInfo$State.RUNNING, this.b);
                this.l.A(this.b);
                this.l.b(this.b, -256);
                b = true;
            }
            else {
                b = false;
            }
            this.k.D();
            return b;
        }
        finally {
            this.k.i();
        }
    }
    
    public static class c
    {
        public Context a;
        public androidx.work.c b;
        public q70 c;
        public hu1 d;
        public a e;
        public WorkDatabase f;
        public p92 g;
        public final List h;
        public WorkerParameters.a i;
        
        public c(final Context context, final a e, final hu1 d, final q70 c, final WorkDatabase f, final p92 g, final List h) {
            this.i = new WorkerParameters.a();
            this.a = context.getApplicationContext();
            this.d = d;
            this.c = c;
            this.e = e;
            this.f = f;
            this.g = g;
            this.h = h;
        }
        
        public static /* synthetic */ List a(final c c) {
            return c.h;
        }
        
        public ca2 b() {
            return new ca2(this);
        }
        
        public c c(final WorkerParameters.a i) {
            if (i != null) {
                this.i = i;
            }
            return this;
        }
    }
}

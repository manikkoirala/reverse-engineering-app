import androidx.savedstate.a;
import android.os.Bundle;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.g;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.c;

// 
// Decompiled by Procyon v0.6.0
// 

public class n80 implements c, aj1, c42
{
    public final Fragment a;
    public final b42 b;
    public g c;
    public zi1 d;
    
    public n80(final Fragment a, final b42 b) {
        this.c = null;
        this.d = null;
        this.a = a;
        this.b = b;
    }
    
    public void a(final Lifecycle.Event event) {
        this.c.h(event);
    }
    
    public void b() {
        if (this.c == null) {
            this.c = new g(this);
            this.d = zi1.a(this);
        }
    }
    
    public boolean c() {
        return this.c != null;
    }
    
    public void d(final Bundle bundle) {
        this.d.d(bundle);
    }
    
    public void e(final Bundle bundle) {
        this.d.e(bundle);
    }
    
    public void f(final Lifecycle.State state) {
        this.c.n(state);
    }
    
    @Override
    public Lifecycle getLifecycle() {
        this.b();
        return this.c;
    }
    
    @Override
    public a getSavedStateRegistry() {
        this.b();
        return this.d.b();
    }
    
    @Override
    public b42 getViewModelStore() {
        this.b();
        return this.b;
    }
}

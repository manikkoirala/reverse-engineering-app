import java.util.Iterator;
import java.util.HashSet;
import java.util.Set;

// 
// Decompiled by Procyon v0.6.0
// 

public final class q00
{
    public static q00 b;
    public final Set a;
    
    static {
        q00.b = b(new HashSet());
    }
    
    public q00(final Set a) {
        this.a = a;
    }
    
    public static q00 b(final Set set) {
        return new q00(set);
    }
    
    public boolean a(final s00 s00) {
        final Iterator iterator = this.a.iterator();
        while (iterator.hasNext()) {
            if (((s00)iterator.next()).k(s00)) {
                return true;
            }
        }
        return false;
    }
    
    public Set c() {
        return this.a;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o != null && q00.class == o.getClass() && this.a.equals(((q00)o).a));
    }
    
    @Override
    public int hashCode() {
        return this.a.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("FieldMask{mask=");
        sb.append(this.a.toString());
        sb.append("}");
        return sb.toString();
    }
}

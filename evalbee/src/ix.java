import androidx.room.RoomDatabase;
import androidx.room.SharedSQLiteStatement;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class ix extends SharedSQLiteStatement
{
    public ix(final RoomDatabase roomDatabase) {
        fg0.e((Object)roomDatabase, "database");
        super(roomDatabase);
    }
    
    public abstract void i(final ws1 p0, final Object p1);
    
    public final void j(final Object o) {
        final ws1 b = this.b();
        try {
            this.i(b, o);
            b.b0();
        }
        finally {
            this.h(b);
        }
    }
}

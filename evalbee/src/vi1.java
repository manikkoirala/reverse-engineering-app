import android.widget.TextView;
import android.view.View;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.widget.EditText;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.ekodroid.omrevaluator.database.SmsTemplateDataModel;
import com.ekodroid.omrevaluator.database.repositories.SmsTemplateRepository;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class vi1
{
    public y01 a;
    public Context b;
    public String c;
    public String d;
    
    public vi1(final Context b, final y01 a, final String d, final String c) {
        this.b = b;
        this.a = a;
        this.c = c;
        this.d = d;
        this.g();
    }
    
    public final boolean d(final String s) {
        return SmsTemplateRepository.getInstance(this.b).getSmsTemplate(s) != null;
    }
    
    public final boolean e(final String s) {
        SmsTemplateRepository.getInstance(this.b).deleteSmsTemplate(s);
        return SmsTemplateRepository.getInstance(this.b).saveOrUpdateSmsTemplate(new SmsTemplateDataModel(s, this.c));
    }
    
    public final void f(final String str) {
        final y01 y01 = new y01(this, str) {
            public final String a;
            public final vi1 b;
            
            @Override
            public void a(final Object o) {
                if (this.b.e(this.a)) {
                    a91.G(this.b.b, 2131886765, 2131230927, 2131231085);
                    this.b.a.a(this.a);
                }
            }
        };
        final Context b = this.b;
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(this.b.getString(2131886451));
        xs.d(b, y01, 0, sb.toString(), 2131886903, 2131886657, 0, 0);
    }
    
    public final void g() {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(this.b, 2131951953);
        final View inflate = ((LayoutInflater)this.b.getSystemService("layout_inflater")).inflate(2131492986, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        materialAlertDialogBuilder.setTitle(2131886764);
        final EditText editText = (EditText)inflate.findViewById(2131296595);
        if (!this.d.toUpperCase().equals("DEFAULT")) {
            ((TextView)editText).setText((CharSequence)this.d);
            editText.setSelection(this.d.length());
        }
        materialAlertDialogBuilder.setPositiveButton(2131886759, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this, editText) {
            public final EditText a;
            public final vi1 b;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                final String string = this.a.getText().toString();
                if (string.trim().equals("")) {
                    a91.G(this.b.b, 2131886274, 2131230909, 2131231086);
                    return;
                }
                if (string.toUpperCase().equals("DEFAULT")) {
                    xs.c(this.b.b, null, 0, 2131886228, 2131886679, 0, 0, 0);
                    ((TextView)this.a).setText((CharSequence)"");
                    return;
                }
                if (this.b.d(string)) {
                    this.b.f(string);
                    dialogInterface.dismiss();
                    return;
                }
                if (this.b.e(string)) {
                    a91.G(this.b.b, 2131886765, 2131230927, 2131231085);
                    this.b.a.a(string);
                }
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.setNegativeButton(2131886163, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final vi1 a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.create().show();
    }
}

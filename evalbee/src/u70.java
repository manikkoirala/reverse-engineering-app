import java.util.concurrent.ConcurrentMap;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class u70 extends x70 implements ConcurrentMap
{
    @Override
    public abstract ConcurrentMap delegate();
    
    @Override
    public Object putIfAbsent(final Object o, final Object o2) {
        return this.delegate().putIfAbsent(o, o2);
    }
    
    @Override
    public boolean remove(final Object o, final Object o2) {
        return this.delegate().remove(o, o2);
    }
    
    @Override
    public Object replace(final Object o, final Object o2) {
        return this.delegate().replace(o, o2);
    }
    
    @Override
    public boolean replace(final Object o, final Object o2, final Object o3) {
        return this.delegate().replace(o, o2, o3);
    }
}

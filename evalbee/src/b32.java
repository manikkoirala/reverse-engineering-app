// 
// Decompiled by Procyon v0.6.0
// 

public class b32
{
    public boolean a;
    public String[] b;
    public double[] c;
    public int d;
    
    public b32() {
        this(true);
    }
    
    public b32(final boolean a) {
        this.b = new String[2];
        this.c = new double[2];
        this.d = 0;
        this.a = a;
    }
    
    public double a(final String str) {
        for (int i = 0; i < this.d; ++i) {
            if ((this.a && this.b[i].equals(str)) || (!this.a && this.b[i].equalsIgnoreCase(str))) {
                return this.c[i];
            }
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("variable value has not been set: ");
        sb.append(str);
        throw new RuntimeException(sb.toString());
    }
    
    public void b(final String s, final double n) {
        if (s == null) {
            throw new IllegalArgumentException("varName cannot be null");
        }
        final int n2 = 0;
        int n3 = 0;
        while (true) {
            final int d = this.d;
            if (n3 >= d) {
                if (d == this.b.length) {
                    final int n4 = d * 2;
                    final String[] b = new String[n4];
                    final double[] c = new double[n4];
                    for (int i = n2; i < this.d; ++i) {
                        b[i] = this.b[i];
                        c[i] = this.c[i];
                    }
                    this.b = b;
                    this.c = c;
                }
                final String[] b2 = this.b;
                final int d2 = this.d;
                b2[d2] = s;
                this.c[d2] = n;
                this.d = d2 + 1;
                return;
            }
            if ((this.a && this.b[n3].equals(s)) || (!this.a && this.b[n3].equalsIgnoreCase(s))) {
                this.c[n3] = n;
                return;
            }
            ++n3;
        }
    }
}

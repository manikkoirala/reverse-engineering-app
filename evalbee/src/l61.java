import com.android.volley.Request;
import java.util.HashMap;
import java.util.Map;
import com.android.volley.VolleyError;
import com.android.volley.d;
import com.ekodroid.omrevaluator.serializable.ReportData;

// 
// Decompiled by Procyon v0.6.0
// 

public class l61
{
    public ee1 a;
    public y01 b;
    public ReportData c;
    public byte[] d;
    public String e;
    
    public l61(final ReportData c, final byte[] d, final ee1 a, final y01 b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("https://");
        sb.append(a91.w());
        sb.append(":8759/report");
        this.e = sb.toString();
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e();
    }
    
    public static /* synthetic */ byte[] b(final l61 l61) {
        return l61.d;
    }
    
    public static /* synthetic */ ReportData c(final l61 l61) {
        return l61.c;
    }
    
    public final void d(final Object o) {
        final y01 b = this.b;
        if (b != null) {
            b.a(o);
        }
    }
    
    public final void e() {
        final hr1 hr1 = new hr1(this, 1, this.e, new d.b(this) {
            public final l61 a;
            
            public void b(final String s) {
                this.a.d("Success");
            }
        }, new d.a(this) {
            public final l61 a;
            
            @Override
            public void a(final VolleyError volleyError) {
                this.a.d(null);
            }
        }) {
            public final l61 w;
            
            @Override
            public byte[] k() {
                return l61.b(this.w);
            }
            
            @Override
            public String l() {
                return "application/pdf";
            }
            
            @Override
            public Map o() {
                final HashMap hashMap = new HashMap();
                hashMap.put("reportData", new gc0().s(l61.c(this.w)));
                return hashMap;
            }
        };
        hr1.L(new wq(100000, 0, 1.0f));
        this.a.a(hr1);
    }
}

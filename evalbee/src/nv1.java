import android.view.ActionMode;
import android.view.MenuItem;
import java.lang.reflect.InvocationTargetException;
import android.view.Menu;
import android.text.Editable;
import java.util.Iterator;
import android.app.Activity;
import java.util.ArrayList;
import java.util.List;
import android.content.pm.PackageManager;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.content.Intent;
import java.lang.reflect.Method;
import android.text.PrecomputedText$Params;
import android.icu.text.DecimalFormatSymbols;
import java.util.Locale;
import android.view.ActionMode$Callback;
import android.graphics.Paint$FontMetricsInt;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import android.graphics.Paint;
import android.text.TextPaint;
import android.view.View;
import android.os.Build$VERSION;
import android.text.method.PasswordTransformationMethod;
import android.text.TextDirectionHeuristics;
import android.text.TextDirectionHeuristic;
import android.graphics.drawable.Drawable;
import android.widget.TextView;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class nv1
{
    public static Drawable[] a(final TextView textView) {
        return b.a(textView);
    }
    
    public static int b(final TextView textView) {
        return ((View)textView).getPaddingTop() - ((Paint)textView.getPaint()).getFontMetricsInt().top;
    }
    
    public static int c(final TextView textView) {
        return ((View)textView).getPaddingBottom() + ((Paint)textView.getPaint()).getFontMetricsInt().bottom;
    }
    
    public static int d(final TextView textView) {
        return a.b(textView);
    }
    
    public static int e(final TextDirectionHeuristic textDirectionHeuristic) {
        if (textDirectionHeuristic == TextDirectionHeuristics.FIRSTSTRONG_RTL) {
            return 1;
        }
        if (textDirectionHeuristic == TextDirectionHeuristics.FIRSTSTRONG_LTR) {
            return 1;
        }
        if (textDirectionHeuristic == TextDirectionHeuristics.ANYRTL_LTR) {
            return 2;
        }
        if (textDirectionHeuristic == TextDirectionHeuristics.LTR) {
            return 3;
        }
        if (textDirectionHeuristic == TextDirectionHeuristics.RTL) {
            return 4;
        }
        if (textDirectionHeuristic == TextDirectionHeuristics.LOCALE) {
            return 5;
        }
        if (textDirectionHeuristic == TextDirectionHeuristics.FIRSTSTRONG_LTR) {
            return 6;
        }
        if (textDirectionHeuristic == TextDirectionHeuristics.FIRSTSTRONG_RTL) {
            return 7;
        }
        return 1;
    }
    
    public static TextDirectionHeuristic f(final TextView textView) {
        if (textView.getTransformationMethod() instanceof PasswordTransformationMethod) {
            return TextDirectionHeuristics.LTR;
        }
        final int sdk_INT = Build$VERSION.SDK_INT;
        boolean b = false;
        if (sdk_INT >= 28 && (textView.getInputType() & 0xF) == 0x3) {
            final byte directionality = Character.getDirectionality(e.a(d.a(nv1.b.d(textView)))[0].codePointAt(0));
            if (directionality != 1 && directionality != 2) {
                return TextDirectionHeuristics.LTR;
            }
            return TextDirectionHeuristics.RTL;
        }
        else {
            if (nv1.b.b((View)textView) == 1) {
                b = true;
            }
            switch (nv1.b.c((View)textView)) {
                default: {
                    TextDirectionHeuristic textDirectionHeuristic;
                    if (b) {
                        textDirectionHeuristic = TextDirectionHeuristics.FIRSTSTRONG_RTL;
                    }
                    else {
                        textDirectionHeuristic = TextDirectionHeuristics.FIRSTSTRONG_LTR;
                    }
                    return textDirectionHeuristic;
                }
                case 7: {
                    return TextDirectionHeuristics.FIRSTSTRONG_RTL;
                }
                case 6: {
                    return TextDirectionHeuristics.FIRSTSTRONG_LTR;
                }
                case 5: {
                    return TextDirectionHeuristics.LOCALE;
                }
                case 4: {
                    return TextDirectionHeuristics.RTL;
                }
                case 3: {
                    return TextDirectionHeuristics.LTR;
                }
                case 2: {
                    return TextDirectionHeuristics.ANYRTL_LTR;
                }
            }
        }
    }
    
    public static g71.a g(final TextView textView) {
        if (Build$VERSION.SDK_INT >= 28) {
            return new g71.a(e.b(textView));
        }
        final g71.a.a a = new g71.a.a(new TextPaint((Paint)textView.getPaint()));
        a.b(c.a(textView));
        a.c(c.d(textView));
        a.d(f(textView));
        return a.a();
    }
    
    public static void h(final TextView textView, final ColorStateList list) {
        l71.g(textView);
        c.f(textView, list);
    }
    
    public static void i(final TextView textView, final PorterDuff$Mode porterDuff$Mode) {
        l71.g(textView);
        c.g(textView, porterDuff$Mode);
    }
    
    public static void j(final TextView textView, final Drawable drawable, final Drawable drawable2, final Drawable drawable3, final Drawable drawable4) {
        b.e(textView, drawable, drawable2, drawable3, drawable4);
    }
    
    public static void k(final TextView textView, final int n) {
        l71.d(n);
        if (Build$VERSION.SDK_INT >= 28) {
            e.c(textView, n);
            return;
        }
        final Paint$FontMetricsInt fontMetricsInt = ((Paint)textView.getPaint()).getFontMetricsInt();
        int a;
        if (nv1.a.a(textView)) {
            a = fontMetricsInt.top;
        }
        else {
            a = fontMetricsInt.ascent;
        }
        if (n > Math.abs(a)) {
            textView.setPadding(((View)textView).getPaddingLeft(), n + a, ((View)textView).getPaddingRight(), ((View)textView).getPaddingBottom());
        }
    }
    
    public static void l(final TextView textView, final int n) {
        l71.d(n);
        final Paint$FontMetricsInt fontMetricsInt = ((Paint)textView.getPaint()).getFontMetricsInt();
        int a;
        if (nv1.a.a(textView)) {
            a = fontMetricsInt.bottom;
        }
        else {
            a = fontMetricsInt.descent;
        }
        if (n > Math.abs(a)) {
            textView.setPadding(((View)textView).getPaddingLeft(), ((View)textView).getPaddingTop(), ((View)textView).getPaddingRight(), n - a);
        }
    }
    
    public static void m(final TextView textView, final int n) {
        l71.d(n);
        final int fontMetricsInt = ((Paint)textView.getPaint()).getFontMetricsInt((Paint$FontMetricsInt)null);
        if (n != fontMetricsInt) {
            textView.setLineSpacing((float)(n - fontMetricsInt), 1.0f);
        }
    }
    
    public static void n(final TextView textView, final g71 g71) {
        if (Build$VERSION.SDK_INT >= 29) {
            throw null;
        }
        g(textView);
        throw null;
    }
    
    public static void o(final TextView textView, final int textAppearance) {
        textView.setTextAppearance(textAppearance);
    }
    
    public static void p(final TextView textView, final g71.a a) {
        b.h((View)textView, e(a.d()));
        textView.getPaint().set(a.e());
        c.e(textView, a.b());
        c.h(textView, a.c());
    }
    
    public static ActionMode$Callback q(final ActionMode$Callback actionMode$Callback) {
        ActionMode$Callback d = actionMode$Callback;
        if (actionMode$Callback instanceof f) {
            d = actionMode$Callback;
            if (Build$VERSION.SDK_INT >= 26) {
                d = ((f)actionMode$Callback).d();
            }
        }
        return d;
    }
    
    public static ActionMode$Callback r(final TextView textView, final ActionMode$Callback actionMode$Callback) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 26 && sdk_INT <= 27 && !(actionMode$Callback instanceof f) && actionMode$Callback != null) {
            return (ActionMode$Callback)new f(actionMode$Callback, textView);
        }
        return actionMode$Callback;
    }
    
    public abstract static class a
    {
        public static boolean a(final TextView textView) {
            return textView.getIncludeFontPadding();
        }
        
        public static int b(final TextView textView) {
            return textView.getMaxLines();
        }
        
        public static int c(final TextView textView) {
            return textView.getMinLines();
        }
    }
    
    public abstract static class b
    {
        public static Drawable[] a(final TextView textView) {
            return textView.getCompoundDrawablesRelative();
        }
        
        public static int b(final View view) {
            return view.getLayoutDirection();
        }
        
        public static int c(final View view) {
            return view.getTextDirection();
        }
        
        public static Locale d(final TextView textView) {
            return textView.getTextLocale();
        }
        
        public static void e(final TextView textView, final Drawable drawable, final Drawable drawable2, final Drawable drawable3, final Drawable drawable4) {
            textView.setCompoundDrawablesRelative(drawable, drawable2, drawable3, drawable4);
        }
        
        public static void f(final TextView textView, final int n, final int n2, final int n3, final int n4) {
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds(n, n2, n3, n4);
        }
        
        public static void g(final TextView textView, final Drawable drawable, final Drawable drawable2, final Drawable drawable3, final Drawable drawable4) {
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable, drawable2, drawable3, drawable4);
        }
        
        public static void h(final View view, final int textDirection) {
            view.setTextDirection(textDirection);
        }
    }
    
    public abstract static class c
    {
        public static int a(final TextView textView) {
            return textView.getBreakStrategy();
        }
        
        public static ColorStateList b(final TextView textView) {
            return textView.getCompoundDrawableTintList();
        }
        
        public static PorterDuff$Mode c(final TextView textView) {
            return textView.getCompoundDrawableTintMode();
        }
        
        public static int d(final TextView textView) {
            return textView.getHyphenationFrequency();
        }
        
        public static void e(final TextView textView, final int breakStrategy) {
            textView.setBreakStrategy(breakStrategy);
        }
        
        public static void f(final TextView textView, final ColorStateList compoundDrawableTintList) {
            textView.setCompoundDrawableTintList(compoundDrawableTintList);
        }
        
        public static void g(final TextView textView, final PorterDuff$Mode compoundDrawableTintMode) {
            textView.setCompoundDrawableTintMode(compoundDrawableTintMode);
        }
        
        public static void h(final TextView textView, final int hyphenationFrequency) {
            textView.setHyphenationFrequency(hyphenationFrequency);
        }
    }
    
    public abstract static class d
    {
        public static DecimalFormatSymbols a(final Locale locale) {
            return DecimalFormatSymbols.getInstance(locale);
        }
    }
    
    public abstract static class e
    {
        public static String[] a(final DecimalFormatSymbols decimalFormatSymbols) {
            return decimalFormatSymbols.getDigitStrings();
        }
        
        public static PrecomputedText$Params b(final TextView textView) {
            return textView.getTextMetricsParams();
        }
        
        public static void c(final TextView textView, final int firstBaselineToTopHeight) {
            textView.setFirstBaselineToTopHeight(firstBaselineToTopHeight);
        }
    }
    
    public static class f implements ActionMode$Callback
    {
        public final ActionMode$Callback a;
        public final TextView b;
        public Class c;
        public Method d;
        public boolean e;
        public boolean f;
        
        public f(final ActionMode$Callback a, final TextView b) {
            this.a = a;
            this.b = b;
            this.f = false;
        }
        
        public final Intent a() {
            return new Intent().setAction("android.intent.action.PROCESS_TEXT").setType("text/plain");
        }
        
        public final Intent b(final ResolveInfo resolveInfo, final TextView textView) {
            final Intent putExtra = this.a().putExtra("android.intent.extra.PROCESS_TEXT_READONLY", this.e(textView) ^ true);
            final ActivityInfo activityInfo = resolveInfo.activityInfo;
            return putExtra.setClassName(activityInfo.packageName, activityInfo.name);
        }
        
        public final List c(final Context context, final PackageManager packageManager) {
            final ArrayList list = new ArrayList();
            if (!(context instanceof Activity)) {
                return list;
            }
            for (final ResolveInfo resolveInfo : packageManager.queryIntentActivities(this.a(), 0)) {
                if (this.f(resolveInfo, context)) {
                    list.add(resolveInfo);
                }
            }
            return list;
        }
        
        public ActionMode$Callback d() {
            return this.a;
        }
        
        public final boolean e(final TextView textView) {
            return textView instanceof Editable && textView.onCheckIsTextEditor() && ((View)textView).isEnabled();
        }
        
        public final boolean f(final ResolveInfo resolveInfo, final Context context) {
            final boolean equals = context.getPackageName().equals(resolveInfo.activityInfo.packageName);
            final boolean b = true;
            if (equals) {
                return true;
            }
            final ActivityInfo activityInfo = resolveInfo.activityInfo;
            if (!activityInfo.exported) {
                return false;
            }
            final String permission = activityInfo.permission;
            boolean b2 = b;
            if (permission != null) {
                b2 = (context.checkSelfPermission(permission) == 0 && b);
            }
            return b2;
        }
        
        public final void g(final Menu obj) {
            final Context context = ((View)this.b).getContext();
            final PackageManager packageManager = context.getPackageManager();
            if (!this.f) {
                this.f = true;
                try {
                    final Class<?> forName = Class.forName("com.android.internal.view.menu.MenuBuilder");
                    this.c = forName;
                    this.d = forName.getDeclaredMethod("removeItemAt", Integer.TYPE);
                    this.e = true;
                }
                catch (final ClassNotFoundException | NoSuchMethodException ex) {
                    this.c = null;
                    this.d = null;
                    this.e = false;
                }
            }
            try {
                Method method;
                if (this.e && this.c.isInstance(obj)) {
                    method = this.d;
                }
                else {
                    method = obj.getClass().getDeclaredMethod("removeItemAt", Integer.TYPE);
                }
                for (int i = obj.size() - 1; i >= 0; --i) {
                    final MenuItem item = obj.getItem(i);
                    if (item.getIntent() != null && "android.intent.action.PROCESS_TEXT".equals(item.getIntent().getAction())) {
                        method.invoke(obj, i);
                    }
                }
                final List c = this.c(context, packageManager);
                for (int j = 0; j < c.size(); ++j) {
                    final ResolveInfo resolveInfo = c.get(j);
                    obj.add(0, 0, j + 100, resolveInfo.loadLabel(packageManager)).setIntent(this.b(resolveInfo, this.b)).setShowAsAction(1);
                }
            }
            catch (final NoSuchMethodException | IllegalAccessException | InvocationTargetException ex2) {}
        }
        
        public boolean onActionItemClicked(final ActionMode actionMode, final MenuItem menuItem) {
            return this.a.onActionItemClicked(actionMode, menuItem);
        }
        
        public boolean onCreateActionMode(final ActionMode actionMode, final Menu menu) {
            return this.a.onCreateActionMode(actionMode, menu);
        }
        
        public void onDestroyActionMode(final ActionMode actionMode) {
            this.a.onDestroyActionMode(actionMode);
        }
        
        public boolean onPrepareActionMode(final ActionMode actionMode, final Menu menu) {
            this.g(menu);
            return this.a.onPrepareActionMode(actionMode, menu);
        }
    }
}

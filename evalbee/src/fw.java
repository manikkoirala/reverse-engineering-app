import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import android.text.TextUtils;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;

// 
// Decompiled by Procyon v0.6.0
// 

public class fw extends v9
{
    public static final Parcelable$Creator<fw> CREATOR;
    public String a;
    public String b;
    public final String c;
    public String d;
    public boolean e;
    
    static {
        CREATOR = (Parcelable$Creator)new vf2();
    }
    
    public fw(final String s, final String b, final String c, final String d, final boolean e) {
        this.a = Preconditions.checkNotEmpty(s);
        if (TextUtils.isEmpty((CharSequence)b) && TextUtils.isEmpty((CharSequence)c)) {
            throw new IllegalArgumentException("Cannot create an EmailAuthCredential without a password or emailLink.");
        }
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
    }
    
    public static boolean K(final String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return false;
        }
        final a2 c = a2.c(s);
        return c != null && c.b() == 4;
    }
    
    @Override
    public String E() {
        if (!TextUtils.isEmpty((CharSequence)this.b)) {
            return "password";
        }
        return "emailLink";
    }
    
    @Override
    public final v9 H() {
        return new fw(this.a, this.b, this.c, this.d, this.e);
    }
    
    public final fw J(final r30 r30) {
        this.d = r30.zze();
        this.e = true;
        return this;
    }
    
    @Override
    public String i() {
        return "password";
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.a, false);
        SafeParcelWriter.writeString(parcel, 2, this.b, false);
        SafeParcelWriter.writeString(parcel, 3, this.c, false);
        SafeParcelWriter.writeString(parcel, 4, this.d, false);
        SafeParcelWriter.writeBoolean(parcel, 5, this.e);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public final String zzb() {
        return this.d;
    }
    
    public final String zzc() {
        return this.a;
    }
    
    public final String zzd() {
        return this.b;
    }
    
    public final String zze() {
        return this.c;
    }
    
    public final boolean zzf() {
        return !TextUtils.isEmpty((CharSequence)this.c);
    }
    
    public final boolean zzg() {
        return this.e;
    }
}

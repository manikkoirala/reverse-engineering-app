import android.graphics.Rect;
import androidx.emoji2.text.c;
import android.view.View;
import android.text.method.TransformationMethod;

// 
// Decompiled by Procyon v0.6.0
// 

public class rw implements TransformationMethod
{
    public final TransformationMethod a;
    
    public rw(final TransformationMethod a) {
        this.a = a;
    }
    
    public TransformationMethod a() {
        return this.a;
    }
    
    public CharSequence getTransformation(CharSequence o, final View view) {
        if (view.isInEditMode()) {
            return o;
        }
        final TransformationMethod a = this.a;
        CharSequence transformation = o;
        if (a != null) {
            transformation = a.getTransformation(o, view);
        }
        if ((o = transformation) != null) {
            if (c.b().d() != 1) {
                o = transformation;
            }
            else {
                o = c.b().o(transformation);
            }
        }
        return o;
    }
    
    public void onFocusChanged(final View view, final CharSequence charSequence, final boolean b, final int n, final Rect rect) {
        final TransformationMethod a = this.a;
        if (a != null) {
            a.onFocusChanged(view, charSequence, b, n, rect);
        }
    }
}

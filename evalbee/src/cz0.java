import android.os.Build$VERSION;
import androidx.work.NetworkType;
import androidx.work.impl.constraints.controllers.ConstraintController;

// 
// Decompiled by Procyon v0.6.0
// 

public final class cz0 extends ConstraintController
{
    public final int b;
    
    public cz0(final tk tk) {
        fg0.e((Object)tk, "tracker");
        super(tk);
        this.b = 7;
    }
    
    @Override
    public int b() {
        return this.b;
    }
    
    @Override
    public boolean c(final p92 p) {
        fg0.e((Object)p, "workSpec");
        final NetworkType d = p.j.d();
        return d == NetworkType.UNMETERED || (Build$VERSION.SDK_INT >= 30 && d == NetworkType.TEMPORARILY_UNMETERED);
    }
    
    public boolean g(final zy0 zy0) {
        fg0.e((Object)zy0, "value");
        return !zy0.a() || zy0.b();
    }
}

import android.text.Layout;
import android.graphics.Paint;
import android.text.TextDirectionHeuristics;
import android.text.TextDirectionHeuristic;
import android.text.StaticLayout$Builder;
import android.text.method.TransformationMethod;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.content.res.TypedArray;
import android.util.TypedValue;
import android.view.View;
import android.util.AttributeSet;
import android.text.StaticLayout;
import android.text.Layout$Alignment;
import java.util.List;
import java.util.Collections;
import java.util.ArrayList;
import java.util.Arrays;
import android.util.Log;
import java.lang.reflect.Method;
import android.os.Build$VERSION;
import android.content.Context;
import android.widget.TextView;
import android.text.TextPaint;
import java.util.concurrent.ConcurrentHashMap;
import android.graphics.RectF;

// 
// Decompiled by Procyon v0.6.0
// 

public class n7
{
    public static final RectF l;
    public static ConcurrentHashMap m;
    public static ConcurrentHashMap n;
    public int a;
    public boolean b;
    public float c;
    public float d;
    public float e;
    public int[] f;
    public boolean g;
    public TextPaint h;
    public final TextView i;
    public final Context j;
    public final f k;
    
    static {
        l = new RectF();
        n7.m = new ConcurrentHashMap();
        n7.n = new ConcurrentHashMap();
    }
    
    public n7(final TextView i) {
        this.a = 0;
        this.b = false;
        this.c = -1.0f;
        this.d = -1.0f;
        this.e = -1.0f;
        this.f = new int[0];
        this.g = false;
        this.i = i;
        this.j = ((View)i).getContext();
        f k;
        if (Build$VERSION.SDK_INT >= 29) {
            k = new e();
        }
        else {
            k = new d();
        }
        this.k = k;
    }
    
    public static Method k(final String s) {
        try {
            Method method;
            if ((method = n7.m.get(s)) == null) {
                final Method declaredMethod = TextView.class.getDeclaredMethod(s, (Class<?>[])new Class[0]);
                if ((method = declaredMethod) != null) {
                    declaredMethod.setAccessible(true);
                    n7.m.put(s, declaredMethod);
                    method = declaredMethod;
                }
            }
            return method;
        }
        catch (final Exception ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed to retrieve TextView#");
            sb.append(s);
            sb.append("() method");
            Log.w("ACTVAutoSizeHelper", sb.toString(), (Throwable)ex);
            return null;
        }
    }
    
    public static Object m(Object obj, final String str, Object invoke) {
        try {
            obj = (invoke = k(str).invoke(obj, new Object[0]));
        }
        catch (final Exception ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed to invoke TextView#");
            sb.append(str);
            sb.append("() method");
            Log.w("ACTVAutoSizeHelper", sb.toString(), (Throwable)ex);
        }
        return invoke;
    }
    
    public void a() {
        if (!this.n()) {
            return;
        }
        Label_0185: {
            if (this.b) {
                if (((View)this.i).getMeasuredHeight() > 0) {
                    if (((View)this.i).getMeasuredWidth() > 0) {
                        int n;
                        if (this.k.b(this.i)) {
                            n = 1048576;
                        }
                        else {
                            n = ((View)this.i).getMeasuredWidth() - this.i.getTotalPaddingLeft() - this.i.getTotalPaddingRight();
                        }
                        final int n2 = ((View)this.i).getHeight() - this.i.getCompoundPaddingBottom() - this.i.getCompoundPaddingTop();
                        if (n > 0) {
                            if (n2 > 0) {
                                final RectF l = n7.l;
                                synchronized (l) {
                                    l.setEmpty();
                                    l.right = (float)n;
                                    l.bottom = (float)n2;
                                    final float n3 = (float)this.e(l);
                                    if (n3 != this.i.getTextSize()) {
                                        this.t(0, n3);
                                    }
                                    break Label_0185;
                                }
                            }
                        }
                    }
                }
                return;
            }
        }
        this.b = true;
    }
    
    public final int[] b(int[] a) {
        final int length = a.length;
        if (length == 0) {
            return a;
        }
        Arrays.sort(a);
        final ArrayList list = new ArrayList();
        final int n = 0;
        for (final int n2 : a) {
            if (n2 > 0 && Collections.binarySearch(list, n2) < 0) {
                list.add(n2);
            }
        }
        if (length == list.size()) {
            return a;
        }
        final int size = list.size();
        a = new int[size];
        for (int j = n; j < size; ++j) {
            a[j] = (int)list.get(j);
        }
        return a;
    }
    
    public final void c() {
        this.a = 0;
        this.d = -1.0f;
        this.e = -1.0f;
        this.c = -1.0f;
        this.f = new int[0];
        this.b = false;
    }
    
    public StaticLayout d(final CharSequence charSequence, final Layout$Alignment layout$Alignment, final int n, final int n2) {
        return n7.c.a(charSequence, layout$Alignment, n, n2, this.i, this.h, this.k);
    }
    
    public final int e(final RectF rectF) {
        final int length = this.f.length;
        if (length != 0) {
            int i = 1;
            int n = length - 1;
            int n2 = 0;
            while (i <= n) {
                final int n3 = (i + n) / 2;
                if (this.x(this.f[n3], rectF)) {
                    n2 = i;
                    i = n3 + 1;
                }
                else {
                    n2 = (n = n3 - 1);
                }
            }
            return this.f[n2];
        }
        throw new IllegalStateException("No available text sizes to choose from.");
    }
    
    public int f() {
        return Math.round(this.e);
    }
    
    public int g() {
        return Math.round(this.d);
    }
    
    public int h() {
        return Math.round(this.c);
    }
    
    public int[] i() {
        return this.f;
    }
    
    public int j() {
        return this.a;
    }
    
    public void l(final int n) {
        final TextPaint h = this.h;
        if (h == null) {
            this.h = new TextPaint();
        }
        else {
            ((Paint)h).reset();
        }
        this.h.set(this.i.getPaint());
        ((Paint)this.h).setTextSize((float)n);
    }
    
    public boolean n() {
        return this.y() && this.a != 0;
    }
    
    public void o(final AttributeSet set, int n) {
        final Context j = this.j;
        final int[] g0 = bc1.g0;
        final TypedArray obtainStyledAttributes = j.obtainStyledAttributes(set, g0, n, 0);
        final TextView i = this.i;
        o32.o0((View)i, ((View)i).getContext(), g0, set, obtainStyledAttributes, n, 0);
        n = bc1.l0;
        if (obtainStyledAttributes.hasValue(n)) {
            this.a = obtainStyledAttributes.getInt(n, 0);
        }
        n = bc1.k0;
        float dimension;
        if (obtainStyledAttributes.hasValue(n)) {
            dimension = obtainStyledAttributes.getDimension(n, -1.0f);
        }
        else {
            dimension = -1.0f;
        }
        n = bc1.i0;
        float dimension2;
        if (obtainStyledAttributes.hasValue(n)) {
            dimension2 = obtainStyledAttributes.getDimension(n, -1.0f);
        }
        else {
            dimension2 = -1.0f;
        }
        n = bc1.h0;
        float dimension3;
        if (obtainStyledAttributes.hasValue(n)) {
            dimension3 = obtainStyledAttributes.getDimension(n, -1.0f);
        }
        else {
            dimension3 = -1.0f;
        }
        n = bc1.j0;
        if (obtainStyledAttributes.hasValue(n)) {
            n = obtainStyledAttributes.getResourceId(n, 0);
            if (n > 0) {
                final TypedArray obtainTypedArray = obtainStyledAttributes.getResources().obtainTypedArray(n);
                this.v(obtainTypedArray);
                obtainTypedArray.recycle();
            }
        }
        obtainStyledAttributes.recycle();
        if (this.y()) {
            if (this.a == 1) {
                if (!this.g) {
                    final DisplayMetrics displayMetrics = this.j.getResources().getDisplayMetrics();
                    float applyDimension = dimension2;
                    if (dimension2 == -1.0f) {
                        applyDimension = TypedValue.applyDimension(2, 12.0f, displayMetrics);
                    }
                    float applyDimension2 = dimension3;
                    if (dimension3 == -1.0f) {
                        applyDimension2 = TypedValue.applyDimension(2, 112.0f, displayMetrics);
                    }
                    float n2 = dimension;
                    if (dimension == -1.0f) {
                        n2 = 1.0f;
                    }
                    this.z(applyDimension, applyDimension2, n2);
                }
                this.u();
            }
        }
        else {
            this.a = 0;
        }
    }
    
    public void p(final int n, final int n2, final int n3, final int n4) {
        if (this.y()) {
            final DisplayMetrics displayMetrics = this.j.getResources().getDisplayMetrics();
            this.z(TypedValue.applyDimension(n4, (float)n, displayMetrics), TypedValue.applyDimension(n4, (float)n2, displayMetrics), TypedValue.applyDimension(n4, (float)n3, displayMetrics));
            if (this.u()) {
                this.a();
            }
        }
    }
    
    public void q(final int[] array, final int n) {
        if (this.y()) {
            final int length = array.length;
            int n2 = 0;
            if (length > 0) {
                final int[] array2 = new int[length];
                int[] copy;
                if (n == 0) {
                    copy = Arrays.copyOf(array, length);
                }
                else {
                    final DisplayMetrics displayMetrics = this.j.getResources().getDisplayMetrics();
                    while (true) {
                        copy = array2;
                        if (n2 >= length) {
                            break;
                        }
                        array2[n2] = Math.round(TypedValue.applyDimension(n, (float)array[n2], displayMetrics));
                        ++n2;
                    }
                }
                this.f = this.b(copy);
                if (!this.w()) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("None of the preset sizes is valid: ");
                    sb.append(Arrays.toString(array));
                    throw new IllegalArgumentException(sb.toString());
                }
            }
            else {
                this.g = false;
            }
            if (this.u()) {
                this.a();
            }
        }
    }
    
    public void r(final int i) {
        if (this.y()) {
            if (i != 0) {
                if (i != 1) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unknown auto-size text type: ");
                    sb.append(i);
                    throw new IllegalArgumentException(sb.toString());
                }
                final DisplayMetrics displayMetrics = this.j.getResources().getDisplayMetrics();
                this.z(TypedValue.applyDimension(2, 12.0f, displayMetrics), TypedValue.applyDimension(2, 112.0f, displayMetrics), 1.0f);
                if (this.u()) {
                    this.a();
                }
            }
            else {
                this.c();
            }
        }
    }
    
    public final void s(final float textSize) {
        if (textSize != ((Paint)this.i.getPaint()).getTextSize()) {
            ((Paint)this.i.getPaint()).setTextSize(textSize);
            final boolean a = n7.b.a((View)this.i);
            if (this.i.getLayout() != null) {
                this.b = false;
                try {
                    final Method k = k("nullLayouts");
                    if (k != null) {
                        k.invoke(this.i, new Object[0]);
                    }
                }
                catch (final Exception ex) {
                    Log.w("ACTVAutoSizeHelper", "Failed to invoke TextView#nullLayouts() method", (Throwable)ex);
                }
                if (!a) {
                    ((View)this.i).requestLayout();
                }
                else {
                    ((View)this.i).forceLayout();
                }
                ((View)this.i).invalidate();
            }
        }
    }
    
    public void t(final int n, final float n2) {
        final Context j = this.j;
        Resources resources;
        if (j == null) {
            resources = Resources.getSystem();
        }
        else {
            resources = j.getResources();
        }
        this.s(TypedValue.applyDimension(n, n2, resources.getDisplayMetrics()));
    }
    
    public final boolean u() {
        final boolean y = this.y();
        int i = 0;
        if (y && this.a == 1) {
            if (!this.g || this.f.length == 0) {
                final int n = (int)Math.floor((this.e - this.d) / this.c) + 1;
                final int[] array = new int[n];
                while (i < n) {
                    array[i] = Math.round(this.d + i * this.c);
                    ++i;
                }
                this.f = this.b(array);
            }
            this.b = true;
        }
        else {
            this.b = false;
        }
        return this.b;
    }
    
    public final void v(final TypedArray typedArray) {
        final int length = typedArray.length();
        final int[] array = new int[length];
        if (length > 0) {
            for (int i = 0; i < length; ++i) {
                array[i] = typedArray.getDimensionPixelSize(i, -1);
            }
            this.f = this.b(array);
            this.w();
        }
    }
    
    public final boolean w() {
        final int[] f = this.f;
        final int length = f.length;
        final boolean g = length > 0;
        this.g = g;
        if (g) {
            this.a = 1;
            this.d = (float)f[0];
            this.e = (float)f[length - 1];
            this.c = -1.0f;
        }
        return g;
    }
    
    public final boolean x(final int n, final RectF rectF) {
        final CharSequence text = this.i.getText();
        final TransformationMethod transformationMethod = this.i.getTransformationMethod();
        CharSequence charSequence = text;
        if (transformationMethod != null) {
            final CharSequence transformation = transformationMethod.getTransformation(text, (View)this.i);
            charSequence = text;
            if (transformation != null) {
                charSequence = transformation;
            }
        }
        final int b = n7.a.b(this.i);
        this.l(n);
        final StaticLayout d = this.d(charSequence, (Layout$Alignment)m(this.i, "getLayoutAlignment", Layout$Alignment.ALIGN_NORMAL), Math.round(rectF.right), b);
        return (b == -1 || (d.getLineCount() <= b && ((Layout)d).getLineEnd(d.getLineCount() - 1) == charSequence.length())) && ((Layout)d).getHeight() <= rectF.bottom;
    }
    
    public final boolean y() {
        return this.i instanceof t6 ^ true;
    }
    
    public final void z(final float d, final float n, final float n2) {
        if (d <= 0.0f) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Minimum auto-size text size (");
            sb.append(d);
            sb.append("px) is less or equal to (0px)");
            throw new IllegalArgumentException(sb.toString());
        }
        if (n <= d) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Maximum auto-size text size (");
            sb2.append(n);
            sb2.append("px) is less or equal to minimum auto-size text size (");
            sb2.append(d);
            sb2.append("px)");
            throw new IllegalArgumentException(sb2.toString());
        }
        if (n2 > 0.0f) {
            this.a = 1;
            this.d = d;
            this.e = n;
            this.c = n2;
            this.g = false;
            return;
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("The auto-size step granularity (");
        sb3.append(n2);
        sb3.append("px) is less or equal to (0px)");
        throw new IllegalArgumentException(sb3.toString());
    }
    
    public abstract static final class a
    {
        public static StaticLayout a(final CharSequence charSequence, final Layout$Alignment layout$Alignment, final int n, final TextView textView, final TextPaint textPaint) {
            return new StaticLayout(charSequence, textPaint, n, layout$Alignment, textView.getLineSpacingMultiplier(), textView.getLineSpacingExtra(), textView.getIncludeFontPadding());
        }
        
        public static int b(final TextView textView) {
            return textView.getMaxLines();
        }
    }
    
    public abstract static final class b
    {
        public static boolean a(final View view) {
            return view.isInLayout();
        }
    }
    
    public abstract static final class c
    {
        public static StaticLayout a(CharSequence obtain, final Layout$Alignment alignment, int maxLines, final int n, final TextView textView, final TextPaint textPaint, final f f) {
            obtain = (CharSequence)StaticLayout$Builder.obtain(obtain, 0, obtain.length(), textPaint, maxLines);
            final StaticLayout$Builder setHyphenationFrequency = ((StaticLayout$Builder)obtain).setAlignment(alignment).setLineSpacing(textView.getLineSpacingExtra(), textView.getLineSpacingMultiplier()).setIncludePad(textView.getIncludeFontPadding()).setBreakStrategy(textView.getBreakStrategy()).setHyphenationFrequency(textView.getHyphenationFrequency());
            maxLines = n;
            if (n == -1) {
                maxLines = Integer.MAX_VALUE;
            }
            setHyphenationFrequency.setMaxLines(maxLines);
            try {
                f.a((StaticLayout$Builder)obtain, textView);
            }
            catch (final ClassCastException ex) {
                Log.w("ACTVAutoSizeHelper", "Failed to obtain TextDirectionHeuristic, auto size may be incorrect");
            }
            return ((StaticLayout$Builder)obtain).build();
        }
    }
    
    public static class d extends f
    {
        @Override
        public void a(final StaticLayout$Builder staticLayout$Builder, final TextView textView) {
            staticLayout$Builder.setTextDirection((TextDirectionHeuristic)n7.m(textView, "getTextDirectionHeuristic", TextDirectionHeuristics.FIRSTSTRONG_LTR));
        }
    }
    
    public abstract static class f
    {
        public abstract void a(final StaticLayout$Builder p0, final TextView p1);
        
        public boolean b(final TextView textView) {
            return (boolean)n7.m(textView, "getHorizontallyScrolling", Boolean.FALSE);
        }
    }
    
    public static class e extends d
    {
        @Override
        public void a(final StaticLayout$Builder staticLayout$Builder, final TextView textView) {
            staticLayout$Builder.setTextDirection(p7.a(textView));
        }
        
        @Override
        public boolean b(final TextView textView) {
            return o7.a(textView);
        }
    }
}

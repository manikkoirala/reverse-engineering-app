import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.OnCompleteListener;
import com.android.volley.Request;
import java.util.HashMap;
import com.google.firebase.auth.FirebaseAuth;
import java.util.Map;
import com.android.volley.VolleyError;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.ReportStorageDto;
import com.android.volley.d;
import android.content.Context;
import android.content.SharedPreferences;

// 
// Decompiled by Procyon v0.6.0
// 

public class ta0
{
    public String a;
    public SharedPreferences b;
    public ee1 c;
    public zg d;
    public Context e;
    
    public ta0(final Context e, final zg d) {
        final StringBuilder sb = new StringBuilder();
        sb.append("https://");
        sb.append(a91.v());
        sb.append(":");
        sb.append(a91.p());
        sb.append("/api/web/all-exams/student-report-count");
        this.a = sb.toString();
        this.c = new n52(e, a91.v()).b();
        this.d = d;
        this.e = e;
        this.b = e.getApplicationContext().getSharedPreferences("MyPref", 0);
        this.f();
    }
    
    public static /* synthetic */ Context c(final ta0 ta0) {
        return ta0.e;
    }
    
    public final void d(final String s) {
        final hr1 hr1 = new hr1(this, 0, this.a, new d.b(this) {
            public final ta0 a;
            
            public void b(final String s) {
                try {
                    final ReportStorageDto reportStorageDto = (ReportStorageDto)new gc0().j(s, ReportStorageDto.class);
                    if (reportStorageDto != null) {
                        reportStorageDto.save(ta0.c(this.a));
                    }
                    this.a.e(true, 200, reportStorageDto);
                }
                catch (final Exception ex) {
                    ex.printStackTrace();
                    this.a.e(false, 400, null);
                }
            }
        }, new d.a(this) {
            public final ta0 a;
            
            @Override
            public void a(final VolleyError volleyError) {
                volleyError.printStackTrace();
                this.a.e(false, 400, null);
            }
        }, s) {
            public final String w;
            public final ta0 x;
            
            @Override
            public byte[] k() {
                return null;
            }
            
            @Override
            public String l() {
                return "application/json";
            }
            
            @Override
            public Map o() {
                final String o = FirebaseAuth.getInstance().e().O();
                final String email = FirebaseAuth.getInstance().e().getEmail();
                final HashMap hashMap = new HashMap();
                hashMap.put("x-user-id", o);
                hashMap.put("x-user-email", email);
                hashMap.put("x-user-token", this.w);
                return hashMap;
            }
        };
        hr1.L(new wq(30000, 0, 1.0f));
        this.c.a(hr1);
    }
    
    public final void e(final boolean b, final int n, final Object o) {
        final zg d = this.d;
        if (d != null) {
            d.a(b, n, o);
        }
    }
    
    public final void f() {
        FirebaseAuth.getInstance().e().i(true).addOnCompleteListener((OnCompleteListener)new OnCompleteListener(this) {
            public final ta0 a;
            
            public void onComplete(final Task task) {
                if (task.isSuccessful()) {
                    this.a.d(((ya0)task.getResult()).c());
                }
                else {
                    this.a.e(false, 400, null);
                }
            }
        });
    }
}

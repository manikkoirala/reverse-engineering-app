import com.google.android.gms.common.internal.Preconditions;
import java.util.concurrent.Executor;

// 
// Decompiled by Procyon v0.6.0
// 

public class lo1
{
    public static boolean b = false;
    public final Executor a;
    
    public lo1(final Executor executor) {
        Executor b = executor;
        if (executor == null) {
            if (!lo1.b) {
                b = br1.a().b();
            }
            else {
                b = null;
            }
        }
        this.a = b;
    }
    
    public void a(final Runnable runnable) {
        Preconditions.checkNotNull(runnable);
        final Executor a = this.a;
        if (a != null) {
            a.execute(runnable);
        }
        else {
            br1.a().d(runnable);
        }
    }
}

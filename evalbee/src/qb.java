import androidx.work.impl.constraints.controllers.ConstraintController;

// 
// Decompiled by Procyon v0.6.0
// 

public final class qb extends ConstraintController
{
    public final int b;
    
    public qb(final rb rb) {
        fg0.e((Object)rb, "tracker");
        super(rb);
        this.b = 5;
    }
    
    @Override
    public int b() {
        return this.b;
    }
    
    @Override
    public boolean c(final p92 p) {
        fg0.e((Object)p, "workSpec");
        return p.j.f();
    }
    
    public boolean g(final boolean b) {
        return b ^ true;
    }
}

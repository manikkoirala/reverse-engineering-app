import java.io.InputStream;
import java.util.List;
import java.io.IOException;
import java.util.Collections;
import android.os.SystemClock;
import com.android.volley.Request;

// 
// Decompiled by Procyon v0.6.0
// 

public class mb implements qy0
{
    public final md0 a;
    public final gb b;
    public final ld c;
    
    public mb(final gb gb) {
        this(gb, new ld(4096));
    }
    
    public mb(final gb gb, final ld c) {
        this.b = gb;
        this.a = gb;
        this.c = c;
    }
    
    @Override
    public yy0 a(final Request request) {
        final long elapsedRealtime = SystemClock.elapsedRealtime();
        while (true) {
            Collections.emptyList();
            final byte[] array = null;
            ld0 ld0;
            byte[] array3;
            IOException ex2 = null;
            try {
                final ld0 a = this.b.a(request, id0.c(request.m()));
                byte[] array2 = array;
                try {
                    final int d = a.d();
                    array2 = array;
                    final List c = a.c();
                    if (d == 304) {
                        array2 = array;
                        return dz0.b(request, SystemClock.elapsedRealtime() - elapsedRealtime, c);
                    }
                    array2 = array;
                    final InputStream a2 = a.a();
                    byte[] c2;
                    if (a2 != null) {
                        array2 = array;
                        c2 = dz0.c(a2, a.b(), this.c);
                    }
                    else {
                        array2 = array;
                        c2 = new byte[0];
                    }
                    array2 = c2;
                    dz0.d(SystemClock.elapsedRealtime() - elapsedRealtime, request, c2, d);
                    if (d >= 200 && d <= 299) {
                        array2 = c2;
                        return new yy0(d, c2, false, SystemClock.elapsedRealtime() - elapsedRealtime, c);
                    }
                    array2 = c2;
                    array2 = c2;
                    final IOException ex = new IOException();
                    array2 = c2;
                    throw ex;
                }
                catch (final IOException ex2) {
                    ld0 = a;
                    array3 = array2;
                }
            }
            catch (final IOException ex3) {
                ld0 = null;
                array3 = null;
                ex2 = ex3;
            }
            dz0.a(request, dz0.e(request, ex2, elapsedRealtime, ld0, array3));
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

public class wo1 implements Cloneable
{
    public static final Object e;
    public boolean a;
    public int[] b;
    public Object[] c;
    public int d;
    
    static {
        e = new Object();
    }
    
    public wo1() {
        this(10);
    }
    
    public wo1(int e) {
        this.a = false;
        if (e == 0) {
            this.b = el.a;
            this.c = el.c;
        }
        else {
            e = el.e(e);
            this.b = new int[e];
            this.c = new Object[e];
        }
    }
    
    public void a(final int n, final Object o) {
        final int d = this.d;
        if (d != 0 && n <= this.b[d - 1]) {
            this.k(n, o);
            return;
        }
        if (this.a && d >= this.b.length) {
            this.e();
        }
        final int d2 = this.d;
        if (d2 >= this.b.length) {
            final int e = el.e(d2 + 1);
            final int[] b = new int[e];
            final Object[] c = new Object[e];
            final int[] b2 = this.b;
            System.arraycopy(b2, 0, b, 0, b2.length);
            final Object[] c2 = this.c;
            System.arraycopy(c2, 0, c, 0, c2.length);
            this.b = b;
            this.c = c;
        }
        this.b[d2] = n;
        this.c[d2] = o;
        this.d = d2 + 1;
    }
    
    public void b() {
        final int d = this.d;
        final Object[] c = this.c;
        for (int i = 0; i < d; ++i) {
            c[i] = null;
        }
        this.d = 0;
        this.a = false;
    }
    
    public wo1 d() {
        try {
            final wo1 wo1 = (wo1)super.clone();
            wo1.b = this.b.clone();
            wo1.c = this.c.clone();
            return wo1;
        }
        catch (final CloneNotSupportedException detailMessage) {
            throw new AssertionError((Object)detailMessage);
        }
    }
    
    public final void e() {
        final int d = this.d;
        final int[] b = this.b;
        final Object[] c = this.c;
        int i = 0;
        int d2 = 0;
        while (i < d) {
            final Object o = c[i];
            int n = d2;
            if (o != wo1.e) {
                if (i != d2) {
                    b[d2] = b[i];
                    c[d2] = o;
                    c[i] = null;
                }
                n = d2 + 1;
            }
            ++i;
            d2 = n;
        }
        this.a = false;
        this.d = d2;
    }
    
    public Object f(final int n) {
        return this.h(n, null);
    }
    
    public Object h(int a, final Object o) {
        a = el.a(this.b, this.d, a);
        if (a >= 0) {
            final Object o2 = this.c[a];
            if (o2 != wo1.e) {
                return o2;
            }
        }
        return o;
    }
    
    public int i(final Object o) {
        if (this.a) {
            this.e();
        }
        for (int i = 0; i < this.d; ++i) {
            if (this.c[i] == o) {
                return i;
            }
        }
        return -1;
    }
    
    public int j(final int n) {
        if (this.a) {
            this.e();
        }
        return this.b[n];
    }
    
    public void k(final int n, final Object o) {
        final int a = el.a(this.b, this.d, n);
        if (a >= 0) {
            this.c[a] = o;
        }
        else {
            final int n2 = ~a;
            final int d = this.d;
            if (n2 < d) {
                final Object[] c = this.c;
                if (c[n2] == wo1.e) {
                    this.b[n2] = n;
                    c[n2] = o;
                    return;
                }
            }
            int n3 = n2;
            if (this.a) {
                n3 = n2;
                if (d >= this.b.length) {
                    this.e();
                    n3 = ~el.a(this.b, this.d, n);
                }
            }
            final int d2 = this.d;
            if (d2 >= this.b.length) {
                final int e = el.e(d2 + 1);
                final int[] b = new int[e];
                final Object[] c2 = new Object[e];
                final int[] b2 = this.b;
                System.arraycopy(b2, 0, b, 0, b2.length);
                final Object[] c3 = this.c;
                System.arraycopy(c3, 0, c2, 0, c3.length);
                this.b = b;
                this.c = c2;
            }
            final int d3 = this.d;
            if (d3 - n3 != 0) {
                final int[] b3 = this.b;
                final int n4 = n3 + 1;
                System.arraycopy(b3, n3, b3, n4, d3 - n3);
                final Object[] c4 = this.c;
                System.arraycopy(c4, n3, c4, n4, this.d - n3);
            }
            this.b[n3] = n;
            this.c[n3] = o;
            ++this.d;
        }
    }
    
    public int l() {
        if (this.a) {
            this.e();
        }
        return this.d;
    }
    
    public Object m(final int n) {
        if (this.a) {
            this.e();
        }
        return this.c[n];
    }
    
    @Override
    public String toString() {
        if (this.l() <= 0) {
            return "{}";
        }
        final StringBuilder sb = new StringBuilder(this.d * 28);
        sb.append('{');
        for (int i = 0; i < this.d; ++i) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(this.j(i));
            sb.append('=');
            final Object m = this.m(i);
            if (m != this) {
                sb.append(m);
            }
            else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }
}

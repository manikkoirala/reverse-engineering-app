import java.util.Iterator;
import com.google.firebase.database.collection.c;
import com.google.firebase.firestore.core.DocumentViewChange;
import java.util.HashMap;
import com.google.protobuf.ByteString;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public final class du1
{
    public int a;
    public final Map b;
    public boolean c;
    public ByteString d;
    public boolean e;
    
    public du1() {
        this.a = 0;
        this.b = new HashMap();
        this.c = true;
        this.d = ByteString.EMPTY;
        this.e = false;
    }
    
    public void a(final du du, final DocumentViewChange.Type type) {
        this.c = true;
        this.b.put(du, type);
    }
    
    public void b() {
        this.c = false;
        this.b.clear();
    }
    
    public boolean c() {
        return this.c;
    }
    
    public boolean d() {
        return this.e;
    }
    
    public boolean e() {
        return this.a != 0;
    }
    
    public void f() {
        this.c = true;
        this.e = true;
    }
    
    public void g() {
        ++this.a;
    }
    
    public void h() {
        --this.a;
    }
    
    public void i(final du du) {
        this.c = true;
        this.b.remove(du);
    }
    
    public zt1 j() {
        c c = du.e();
        c c2 = du.e();
        c c3 = du.e();
        for (final Map.Entry<du, V> entry : this.b.entrySet()) {
            final du du = entry.getKey();
            final DocumentViewChange.Type type = (DocumentViewChange.Type)entry.getValue();
            final int n = du1$a.a[type.ordinal()];
            if (n != 1) {
                if (n != 2) {
                    if (n != 3) {
                        throw g9.a("Encountered invalid change type: %s", type);
                    }
                    c3 = c3.c(du);
                }
                else {
                    c2 = c2.c(du);
                }
            }
            else {
                c = c.c(du);
            }
        }
        return new zt1(this.d, this.e, c, c2, c3);
    }
    
    public void k(final ByteString d) {
        if (!d.isEmpty()) {
            this.c = true;
            this.d = d;
        }
    }
}

import android.app.Dialog;
import android.net.Uri;
import android.os.Parcelable;
import androidx.core.content.FileProvider;
import java.io.File;
import android.content.Intent;
import java.util.Iterator;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import java.io.InputStream;
import org.apache.pdfbox.pdmodel.graphics.image.JPEGFactory;
import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import android.graphics.Bitmap$CompressFormat;
import java.io.ByteArrayOutputStream;
import android.graphics.Bitmap;
import org.apache.pdfbox.pdmodel.PDDocument;
import com.ekodroid.omrevaluator.templateui.models.Subject2;
import android.util.Log;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import com.ekodroid.omrevaluator.templateui.models.InvalidQuestionSet;
import com.ekodroid.omrevaluator.templateui.models.AnswerSetKey;
import com.ekodroid.omrevaluator.templateui.models.LabelProfile;
import com.ekodroid.omrevaluator.templateui.models.AnswerOptionKey;
import android.os.Environment;
import android.app.ProgressDialog;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import android.content.Context;
import android.os.AsyncTask;

// 
// Decompiled by Procyon v0.6.0
// 

public class uu extends AsyncTask
{
    public Context a;
    public SheetTemplate2 b;
    public boolean c;
    public String d;
    public String e;
    public ExamId f;
    public ProgressDialog g;
    
    public uu(final Context a, final ExamId f, final SheetTemplate2 b) {
        this.a = a;
        this.b = b;
        this.f = f;
        this.d = b.getName().replaceAll("[^a-zA-Z0-9_-]", "_");
        final StringBuilder sb = new StringBuilder();
        sb.append(a.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS));
        sb.append("/");
        sb.append(this.d);
        sb.append("_key.pdf");
        this.e = sb.toString();
    }
    
    public final void a(final ni0 ni0, final SheetTemplate2 sheetTemplate2) {
        final LabelProfile labelProfile = sheetTemplate2.getLabelProfile();
        ni0.d(labelProfile.getQueNoString(), labelProfile.getCorrectString(), false);
        final AnswerSetKey[] answerKeys = sheetTemplate2.getAnswerKeys();
        if (answerKeys != null) {
            if (answerKeys.length != 0) {
                for (int i = 0; i < answerKeys.length; ++i) {
                    if (answerKeys.length > 1) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append(labelProfile.getExamSetString());
                        sb.append(" - ");
                        sb.append(labelProfile.getExamSetLabels()[i]);
                        ni0.e(sb.toString(), true);
                    }
                    final InvalidQuestionSet[] invalidQuestionSets = sheetTemplate2.getInvalidQuestionSets();
                    int[] invalidQuestions;
                    if (invalidQuestionSets != null && invalidQuestionSets.length > i) {
                        invalidQuestions = invalidQuestionSets[i].getInvalidQuestions();
                    }
                    else {
                        invalidQuestions = null;
                    }
                    this.e(ni0, sheetTemplate2.getTemplateParams().getSubjects(), 0, 0);
                    int subjectId = 0;
                    int j;
                    int sectionId;
                    for (int n = j = 0; j < answerKeys[i].getAnswerOptionKeys().size(); ++j, n = sectionId) {
                        final AnswerOptionKey answerOptionKey = answerKeys[i].getAnswerOptionKeys().get(j);
                        if (subjectId != answerOptionKey.getSubjectId() || (sectionId = n) != answerOptionKey.getSectionId()) {
                            subjectId = answerOptionKey.getSubjectId();
                            sectionId = answerOptionKey.getSectionId();
                            this.e(ni0, sheetTemplate2.getTemplateParams().getSubjects(), subjectId, sectionId);
                        }
                        this.c(ni0, answerOptionKey, sheetTemplate2, j, invalidQuestions);
                    }
                }
            }
        }
    }
    
    public final void b(final ni0 ni0, final SheetTemplate2 sheetTemplate2) {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.f.getExamName());
        sb.append("_");
        sb.append(this.f.getExamDate());
        sb.append("_key");
        ni0.a(sb.toString());
    }
    
    public final void c(final ni0 ni0, final AnswerOptionKey answerOptionKey, final SheetTemplate2 sheetTemplate2, final int n, final int[] array) {
        final String questionNumberLabel = sheetTemplate2.getLabelProfile().getQuestionNumberLabel(answerOptionKey.getQuestionNumber());
        final String q = e5.q(answerOptionKey, sheetTemplate2.getLabelProfile());
        if (array != null && array.length > 0) {
            for (int length = array.length, i = 0; i < length; ++i) {
                if (array[i] == n + 1) {
                    ni0.c(questionNumberLabel, "Invalid", false);
                    return;
                }
            }
        }
        ni0.c(questionNumberLabel, q, false);
    }
    
    public final void d(final PDImageXObject pdImageXObject, final PDPage pdPage, final PDPageContentStream pdPageContentStream) {
        final float n = pdPage.getBBox().getWidth() - 20.0f;
        final float n2 = pdPage.getBBox().getHeight() - 20.0f;
        final int width = pdImageXObject.getWidth();
        int width2 = 800;
        if (width > 800) {
            width2 = pdImageXObject.getWidth();
        }
        final double n3 = n / n2;
        try {
            float n4;
            if (n3 > pdImageXObject.getWidth() * 1.0 / pdImageXObject.getHeight()) {
                n4 = n2 / pdImageXObject.getHeight();
            }
            else {
                n4 = n / pdImageXObject.getWidth();
            }
            final float n5 = n4 * pdImageXObject.getWidth() / width2;
            pdPageContentStream.drawImage(pdImageXObject, (pdPage.getBBox().getWidth() - pdImageXObject.getWidth() * n5) / 2.0f, (pdPage.getBBox().getHeight() - pdImageXObject.getHeight() * n5) / 2.0f, pdImageXObject.getWidth() * n5, pdImageXObject.getHeight() * n5);
            Log.d("TAG", "image converted to pdf");
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public final void e(final ni0 ni0, final Subject2[] array, final int n, final int n2) {
        final Subject2 subject2 = array[n];
        final StringBuilder sb = new StringBuilder();
        sb.append(subject2.getSubName());
        sb.append(" - ");
        sb.append(subject2.getSections()[n2].getName());
        ni0.e(sb.toString(), false);
    }
    
    public final boolean f(final String s) {
        final PDDocument pdDocument = new PDDocument();
        final ni0 ni0 = new ni0();
        try {
            this.b(ni0, this.b);
            this.a(ni0, this.b);
            for (final Bitmap bitmap : ni0.g()) {
                final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap$CompressFormat.JPEG, 100, (OutputStream)byteArrayOutputStream);
                final PDImageXObject fromStream = JPEGFactory.createFromStream(pdDocument, (InputStream)new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));
                final PDPage pdPage = new PDPage(PDRectangle.A4);
                pdDocument.addPage(pdPage);
                final PDPageContentStream pdPageContentStream = new PDPageContentStream(pdDocument, pdPage);
                this.d(fromStream, pdPage, pdPageContentStream);
                pdPageContentStream.close();
                bitmap.recycle();
            }
            pdDocument.save(s);
            pdDocument.close();
            return true;
        }
        catch (final Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public Void g(final Void... array) {
        this.c = this.f(this.e);
        return null;
    }
    
    public void h(final Void void1) {
        if (((Dialog)this.g).isShowing()) {
            ((Dialog)this.g).dismiss();
        }
        if (this.c) {
            final Intent intent = new Intent("android.intent.action.VIEW");
            final Uri f = FileProvider.f(this.a, "com.ekodroid.omrevaluator.fileprovider", new File(this.e));
            intent.putExtra("android.intent.extra.STREAM", (Parcelable)f);
            intent.setDataAndType(f, "application/pdf");
            intent.setFlags(1073741824);
            intent.setFlags(1);
            if (this.a.getPackageManager().queryIntentActivities(intent, 0).size() > 0) {
                this.a.startActivity(intent);
            }
            else {
                a91.G(this.a, 2131886699, 2131230909, 2131231086);
            }
        }
    }
    
    public void i(final Integer... array) {
    }
    
    public void onPreExecute() {
        (this.g = new ProgressDialog(this.a)).setMessage((CharSequence)"Creating pdf, please wait...");
        ((Dialog)this.g).setCancelable(false);
        ((Dialog)this.g).show();
    }
}

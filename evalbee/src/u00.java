// 
// Decompiled by Procyon v0.6.0
// 

public final class u00
{
    public final s00 a;
    public final oy1 b;
    
    public u00(final s00 a, final oy1 b) {
        this.a = a;
        this.b = b;
    }
    
    public s00 a() {
        return this.a;
    }
    
    public oy1 b() {
        return this.b;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o != null && u00.class == o.getClass()) {
            final u00 u00 = (u00)o;
            return this.a.equals(u00.a) && this.b.equals(u00.b);
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return this.a.hashCode() * 31 + this.b.hashCode();
    }
}

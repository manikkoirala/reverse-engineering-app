// 
// Decompiled by Procyon v0.6.0
// 

public abstract class gw
{
    public static v9 a(final String s, final String s2) {
        if (fw.K(s2)) {
            return new fw(s, null, s2, null, false);
        }
        throw new IllegalArgumentException("Given link is not a valid email link. Please use FirebaseAuth#isSignInWithEmailLink(String) to determine this before calling this function");
    }
}

import android.os.Parcel;
import android.content.Intent;
import android.os.Parcelable$Creator;
import android.os.Parcelable;

// 
// Decompiled by Procyon v0.6.0
// 

public final class l2 implements Parcelable
{
    public static final Parcelable$Creator<l2> CREATOR;
    public final int a;
    public final Intent b;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator() {
            public l2 a(final Parcel parcel) {
                return new l2(parcel);
            }
            
            public l2[] b(final int n) {
                return new l2[n];
            }
        };
    }
    
    public l2(final int a, final Intent b) {
        this.a = a;
        this.b = b;
    }
    
    public l2(final Parcel parcel) {
        this.a = parcel.readInt();
        Intent b;
        if (parcel.readInt() == 0) {
            b = null;
        }
        else {
            b = (Intent)Intent.CREATOR.createFromParcel(parcel);
        }
        this.b = b;
    }
    
    public static String d(final int i) {
        if (i == -1) {
            return "RESULT_OK";
        }
        if (i != 0) {
            return String.valueOf(i);
        }
        return "RESULT_CANCELED";
    }
    
    public Intent b() {
        return this.b;
    }
    
    public int c() {
        return this.a;
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ActivityResult{resultCode=");
        sb.append(d(this.a));
        sb.append(", data=");
        sb.append(this.b);
        sb.append('}');
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeInt(this.a);
        int n2;
        if (this.b == null) {
            n2 = 0;
        }
        else {
            n2 = 1;
        }
        parcel.writeInt(n2);
        final Intent b = this.b;
        if (b != null) {
            b.writeToParcel(parcel, n);
        }
    }
}

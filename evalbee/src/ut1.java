import androidx.room.Index$Order;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.text.StringsKt__StringsKt;
import java.util.Locale;
import java.util.Set;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ut1
{
    public static final b e;
    public final String a;
    public final Map b;
    public final Set c;
    public final Set d;
    
    static {
        e = new b(null);
    }
    
    public ut1(final String a, final Map b, final Set c, final Set d) {
        fg0.e((Object)a, "name");
        fg0.e((Object)b, "columns");
        fg0.e((Object)c, "foreignKeys");
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
    
    public static final ut1 a(final ss1 ss1, final String s) {
        return ut1.e.a(ss1, s);
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof ut1)) {
            return false;
        }
        final String a = this.a;
        final ut1 ut1 = (ut1)o;
        if (!fg0.a((Object)a, (Object)ut1.a)) {
            return false;
        }
        if (!fg0.a((Object)this.b, (Object)ut1.b)) {
            return false;
        }
        if (!fg0.a((Object)this.c, (Object)ut1.c)) {
            return false;
        }
        final Set d = this.d;
        boolean a2 = b;
        if (d != null) {
            final Set d2 = ut1.d;
            if (d2 == null) {
                a2 = b;
            }
            else {
                a2 = fg0.a((Object)d, (Object)d2);
            }
        }
        return a2;
    }
    
    @Override
    public int hashCode() {
        return (this.a.hashCode() * 31 + this.b.hashCode()) * 31 + this.c.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("TableInfo{name='");
        sb.append(this.a);
        sb.append("', columns=");
        sb.append(this.b);
        sb.append(", foreignKeys=");
        sb.append(this.c);
        sb.append(", indices=");
        sb.append(this.d);
        sb.append('}');
        return sb.toString();
    }
    
    public static final class a
    {
        public static final ut1.a.a h;
        public final String a;
        public final String b;
        public final boolean c;
        public final int d;
        public final String e;
        public final int f;
        public final int g;
        
        static {
            h = new ut1.a.a(null);
        }
        
        public a(final String a, final String b, final boolean c, final int d, final String e, final int f) {
            fg0.e((Object)a, "name");
            fg0.e((Object)b, "type");
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
            this.f = f;
            this.g = this.a(b);
        }
        
        public final int a(String upperCase) {
            if (upperCase == null) {
                return 5;
            }
            final Locale us = Locale.US;
            fg0.d((Object)us, "US");
            upperCase = upperCase.toUpperCase(us);
            fg0.d((Object)upperCase, "this as java.lang.String).toUpperCase(locale)");
            if (StringsKt__StringsKt.w((CharSequence)upperCase, (CharSequence)"INT", false, 2, (Object)null)) {
                return 3;
            }
            if (StringsKt__StringsKt.w((CharSequence)upperCase, (CharSequence)"CHAR", false, 2, (Object)null) || StringsKt__StringsKt.w((CharSequence)upperCase, (CharSequence)"CLOB", false, 2, (Object)null) || StringsKt__StringsKt.w((CharSequence)upperCase, (CharSequence)"TEXT", false, 2, (Object)null)) {
                return 2;
            }
            if (StringsKt__StringsKt.w((CharSequence)upperCase, (CharSequence)"BLOB", false, 2, (Object)null)) {
                return 5;
            }
            if (!StringsKt__StringsKt.w((CharSequence)upperCase, (CharSequence)"REAL", false, 2, (Object)null) && !StringsKt__StringsKt.w((CharSequence)upperCase, (CharSequence)"FLOA", false, 2, (Object)null) && !StringsKt__StringsKt.w((CharSequence)upperCase, (CharSequence)"DOUB", false, 2, (Object)null)) {
                return 1;
            }
            return 4;
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (!(o instanceof ut1.a)) {
                return false;
            }
            if (this.d != ((ut1.a)o).d) {
                return false;
            }
            final String a = this.a;
            final ut1.a a2 = (ut1.a)o;
            if (!fg0.a((Object)a, (Object)a2.a)) {
                return false;
            }
            if (this.c != a2.c) {
                return false;
            }
            if (this.f == 1 && a2.f == 2) {
                final String e = this.e;
                if (e != null && !ut1.a.h.b(e, a2.e)) {
                    return false;
                }
            }
            if (this.f == 2 && a2.f == 1) {
                final String e2 = a2.e;
                if (e2 != null && !ut1.a.h.b(e2, this.e)) {
                    return false;
                }
            }
            final int f = this.f;
            if (f != 0 && f == a2.f) {
                final String e3 = this.e;
                if ((e3 != null) ? (!ut1.a.h.b(e3, a2.e)) : (a2.e != null)) {
                    return false;
                }
            }
            if (this.g != a2.g) {
                b = false;
            }
            return b;
        }
        
        @Override
        public int hashCode() {
            final int hashCode = this.a.hashCode();
            final int g = this.g;
            int n;
            if (this.c) {
                n = 1231;
            }
            else {
                n = 1237;
            }
            return ((hashCode * 31 + g) * 31 + n) * 31 + this.d;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Column{name='");
            sb.append(this.a);
            sb.append("', type='");
            sb.append(this.b);
            sb.append("', affinity='");
            sb.append(this.g);
            sb.append("', notNull=");
            sb.append(this.c);
            sb.append(", primaryKeyPosition=");
            sb.append(this.d);
            sb.append(", defaultValue='");
            String e;
            if ((e = this.e) == null) {
                e = "undefined";
            }
            sb.append(e);
            sb.append("'}");
            return sb.toString();
        }
        
        public static final class a
        {
            public final boolean a(final String s) {
                final int length = s.length();
                boolean b = true;
                if (length == 0) {
                    return false;
                }
                int i;
                int n2;
                int n;
                int n3;
                for (i = 0, n = (n2 = 0); i < s.length(); ++i, ++n2, n = n3) {
                    final char char1 = s.charAt(i);
                    if (n2 == 0 && char1 != '(') {
                        return false;
                    }
                    if (char1 == '(') {
                        n3 = n + 1;
                    }
                    else {
                        n3 = n;
                        if (char1 == ')') {
                            n3 = --n;
                            if (n == 0) {
                                n3 = n;
                                if (n2 != s.length() - 1) {
                                    return false;
                                }
                            }
                        }
                    }
                }
                if (n != 0) {
                    b = false;
                }
                return b;
            }
            
            public final boolean b(String substring, final String s) {
                fg0.e((Object)substring, "current");
                if (fg0.a((Object)substring, (Object)s)) {
                    return true;
                }
                if (this.a(substring)) {
                    substring = substring.substring(1, substring.length() - 1);
                    fg0.d((Object)substring, "this as java.lang.String\u2026ing(startIndex, endIndex)");
                    return fg0.a((Object)StringsKt__StringsKt.q0((CharSequence)substring).toString(), (Object)s);
                }
                return false;
            }
        }
    }
    
    public static final class b
    {
        public final ut1 a(final ss1 ss1, final String s) {
            fg0.e((Object)ss1, "database");
            fg0.e((Object)s, "tableName");
            return vt1.f(ss1, s);
        }
    }
    
    public static final class c
    {
        public final String a;
        public final String b;
        public final String c;
        public final List d;
        public final List e;
        
        public c(final String a, final String b, final String c, final List d, final List e) {
            fg0.e((Object)a, "referenceTable");
            fg0.e((Object)b, "onDelete");
            fg0.e((Object)c, "onUpdate");
            fg0.e((Object)d, "columnNames");
            fg0.e((Object)e, "referenceColumnNames");
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            final boolean b = o instanceof c;
            boolean a = false;
            if (!b) {
                return false;
            }
            final String a2 = this.a;
            final c c = (c)o;
            if (!fg0.a((Object)a2, (Object)c.a)) {
                return false;
            }
            if (!fg0.a((Object)this.b, (Object)c.b)) {
                return false;
            }
            if (!fg0.a((Object)this.c, (Object)c.c)) {
                return false;
            }
            if (fg0.a((Object)this.d, (Object)c.d)) {
                a = fg0.a((Object)this.e, (Object)c.e);
            }
            return a;
        }
        
        @Override
        public int hashCode() {
            return (((this.a.hashCode() * 31 + this.b.hashCode()) * 31 + this.c.hashCode()) * 31 + this.d.hashCode()) * 31 + this.e.hashCode();
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("ForeignKey{referenceTable='");
            sb.append(this.a);
            sb.append("', onDelete='");
            sb.append(this.b);
            sb.append(" +', onUpdate='");
            sb.append(this.c);
            sb.append("', columnNames=");
            sb.append(this.d);
            sb.append(", referenceColumnNames=");
            sb.append(this.e);
            sb.append('}');
            return sb.toString();
        }
    }
    
    public static final class d implements Comparable
    {
        public final int a;
        public final int b;
        public final String c;
        public final String d;
        
        public d(final int a, final int b, final String c, final String d) {
            fg0.e((Object)c, "from");
            fg0.e((Object)d, "to");
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
        }
        
        public int a(final d d) {
            fg0.e((Object)d, "other");
            int n;
            if ((n = this.a - d.a) == 0) {
                n = this.b - d.b;
            }
            return n;
        }
        
        public final String c() {
            return this.c;
        }
        
        public final int d() {
            return this.a;
        }
        
        public final String e() {
            return this.d;
        }
    }
    
    public static final class e
    {
        public static final a e;
        public final String a;
        public final boolean b;
        public final List c;
        public List d;
        
        static {
            e = new a(null);
        }
        
        public e(final String a, final boolean b, final List c, final List d) {
            fg0.e((Object)a, "name");
            fg0.e((Object)c, "columns");
            fg0.e((Object)d, "orders");
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            Collection collection;
            if ((collection = d).isEmpty()) {
                final int size = c.size();
                final ArrayList list = new ArrayList(size);
                int n = 0;
                while (true) {
                    collection = list;
                    if (n >= size) {
                        break;
                    }
                    list.add((Object)Index$Order.ASC.name());
                    ++n;
                }
            }
            this.d = (List)collection;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof e)) {
                return false;
            }
            final boolean b = this.b;
            final e e = (e)o;
            if (b != e.b) {
                return false;
            }
            if (!fg0.a((Object)this.c, (Object)e.c)) {
                return false;
            }
            if (!fg0.a((Object)this.d, (Object)e.d)) {
                return false;
            }
            boolean b2;
            if (qr1.t(this.a, "index_", false, 2, (Object)null)) {
                b2 = qr1.t(e.a, "index_", false, 2, (Object)null);
            }
            else {
                b2 = fg0.a((Object)this.a, (Object)e.a);
            }
            return b2;
        }
        
        @Override
        public int hashCode() {
            int hashCode;
            if (qr1.t(this.a, "index_", false, 2, (Object)null)) {
                hashCode = -1184239155;
            }
            else {
                hashCode = this.a.hashCode();
            }
            return ((hashCode * 31 + (this.b ? 1 : 0)) * 31 + this.c.hashCode()) * 31 + this.d.hashCode();
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Index{name='");
            sb.append(this.a);
            sb.append("', unique=");
            sb.append(this.b);
            sb.append(", columns=");
            sb.append(this.c);
            sb.append(", orders=");
            sb.append(this.d);
            sb.append("'}");
            return sb.toString();
        }
        
        public static final class a
        {
        }
    }
}

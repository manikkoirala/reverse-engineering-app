import com.google.android.gms.tasks.OnCompleteListener;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ScheduledFuture;
import com.google.android.gms.tasks.TaskCompletionSource;
import android.os.IBinder;
import android.content.ComponentName;
import com.google.android.gms.common.stats.ConnectionTracker;
import com.google.android.gms.tasks.Task;
import android.util.Log;
import java.util.ArrayDeque;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import com.google.android.gms.common.util.concurrent.NamedThreadFactory;
import java.util.Queue;
import java.util.concurrent.ScheduledExecutorService;
import android.content.Intent;
import android.content.Context;
import android.content.ServiceConnection;

// 
// Decompiled by Procyon v0.6.0
// 

public class h82 implements ServiceConnection
{
    public final Context a;
    public final Intent b;
    public final ScheduledExecutorService c;
    public final Queue d;
    public e82 e;
    public boolean f;
    
    public h82(final Context context, final String s) {
        this(context, s, new ScheduledThreadPoolExecutor(0, new NamedThreadFactory("Firebase-FirebaseInstanceIdServiceConnection")));
    }
    
    public h82(Context applicationContext, final String s, final ScheduledExecutorService c) {
        this.d = new ArrayDeque();
        this.f = false;
        applicationContext = applicationContext.getApplicationContext();
        this.a = applicationContext;
        this.b = new Intent(s).setPackage(applicationContext.getPackageName());
        this.c = c;
    }
    
    public final void a() {
        while (!this.d.isEmpty()) {
            this.d.poll().d();
        }
    }
    
    public final void b() {
        synchronized (this) {
            if (Log.isLoggable("FirebaseMessaging", 3)) {
                Log.d("FirebaseMessaging", "flush queue called");
            }
            while (!this.d.isEmpty()) {
                if (Log.isLoggable("FirebaseMessaging", 3)) {
                    Log.d("FirebaseMessaging", "found intent to be delivered");
                }
                final e82 e = this.e;
                if (e == null || !e.isBinderAlive()) {
                    this.d();
                    return;
                }
                if (Log.isLoggable("FirebaseMessaging", 3)) {
                    Log.d("FirebaseMessaging", "binder is alive, sending the intent.");
                }
                this.e.c(this.d.poll());
            }
        }
    }
    
    public Task c(final Intent intent) {
        synchronized (this) {
            if (Log.isLoggable("FirebaseMessaging", 3)) {
                Log.d("FirebaseMessaging", "new intent queued in the bind-strategy delivery");
            }
            final a a = new a(intent);
            a.c(this.c);
            this.d.add(a);
            this.b();
            return a.e();
        }
    }
    
    public final void d() {
        if (Log.isLoggable("FirebaseMessaging", 3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("binder is dead. start connection? ");
            sb.append(this.f ^ true);
            Log.d("FirebaseMessaging", sb.toString());
        }
        if (this.f) {
            return;
        }
        this.f = true;
        try {
            if (ConnectionTracker.getInstance().bindService(this.a, this.b, (ServiceConnection)this, 65)) {
                return;
            }
            Log.e("FirebaseMessaging", "binding to the service failed");
        }
        catch (final SecurityException ex) {
            Log.e("FirebaseMessaging", "Exception while binding the service", (Throwable)ex);
        }
        this.f = false;
        this.a();
    }
    
    public void onServiceConnected(final ComponentName obj, final IBinder obj2) {
        synchronized (this) {
            if (Log.isLoggable("FirebaseMessaging", 3)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("onServiceConnected: ");
                sb.append(obj);
                Log.d("FirebaseMessaging", sb.toString());
            }
            this.f = false;
            if (!(obj2 instanceof e82)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Invalid service connection: ");
                sb2.append(obj2);
                Log.e("FirebaseMessaging", sb2.toString());
                this.a();
                return;
            }
            this.e = (e82)obj2;
            this.b();
        }
    }
    
    public void onServiceDisconnected(final ComponentName obj) {
        if (Log.isLoggable("FirebaseMessaging", 3)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("onServiceDisconnected: ");
            sb.append(obj);
            Log.d("FirebaseMessaging", sb.toString());
        }
        this.b();
    }
    
    public static class a
    {
        public final Intent a;
        public final TaskCompletionSource b;
        
        public a(final Intent a) {
            this.b = new TaskCompletionSource();
            this.a = a;
        }
        
        public void c(final ScheduledExecutorService scheduledExecutorService) {
            this.e().addOnCompleteListener((Executor)scheduledExecutorService, (OnCompleteListener)new g82(scheduledExecutorService.schedule(new f82(this), 20L, TimeUnit.SECONDS)));
        }
        
        public void d() {
            this.b.trySetResult((Object)null);
        }
        
        public Task e() {
            return this.b.getTask();
        }
    }
}

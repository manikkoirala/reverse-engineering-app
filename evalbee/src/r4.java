import java.util.List;

// 
// Decompiled by Procyon v0.6.0
// 

public final class r4
{
    public final String a;
    public final String b;
    public final String c;
    public final String d;
    public final f81 e;
    public final List f;
    
    public r4(final String a, final String b, final String c, final String d, final f81 e, final List f) {
        fg0.e((Object)a, "packageName");
        fg0.e((Object)b, "versionName");
        fg0.e((Object)c, "appBuildVersion");
        fg0.e((Object)d, "deviceManufacturer");
        fg0.e((Object)e, "currentProcessDetails");
        fg0.e((Object)f, "appProcessDetails");
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
    }
    
    public final String a() {
        return this.c;
    }
    
    public final List b() {
        return this.f;
    }
    
    public final f81 c() {
        return this.e;
    }
    
    public final String d() {
        return this.d;
    }
    
    public final String e() {
        return this.a;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof r4)) {
            return false;
        }
        final r4 r4 = (r4)o;
        return fg0.a((Object)this.a, (Object)r4.a) && fg0.a((Object)this.b, (Object)r4.b) && fg0.a((Object)this.c, (Object)r4.c) && fg0.a((Object)this.d, (Object)r4.d) && fg0.a((Object)this.e, (Object)r4.e) && fg0.a((Object)this.f, (Object)r4.f);
    }
    
    public final String f() {
        return this.b;
    }
    
    @Override
    public int hashCode() {
        return ((((this.a.hashCode() * 31 + this.b.hashCode()) * 31 + this.c.hashCode()) * 31 + this.d.hashCode()) * 31 + this.e.hashCode()) * 31 + this.f.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("AndroidApplicationInfo(packageName=");
        sb.append(this.a);
        sb.append(", versionName=");
        sb.append(this.b);
        sb.append(", appBuildVersion=");
        sb.append(this.c);
        sb.append(", deviceManufacturer=");
        sb.append(this.d);
        sb.append(", currentProcessDetails=");
        sb.append(this.e);
        sb.append(", appProcessDetails=");
        sb.append(this.f);
        sb.append(')');
        return sb.toString();
    }
}

import org.json.JSONException;
import org.json.JSONObject;
import android.text.TextUtils;
import java.io.IOException;
import android.util.Log;
import com.google.firebase.storage.StorageException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.util.DefaultClock;
import java.util.concurrent.atomic.AtomicLong;
import android.net.Uri;
import com.google.android.gms.common.util.Clock;
import java.util.Random;

// 
// Decompiled by Procyon v0.6.0
// 

public class i12 extends zq1
{
    public static final Random E;
    public static jo1 F;
    public static Clock G;
    public volatile String A;
    public volatile long B;
    public int C;
    public final int D;
    public final jq1 l;
    public final Uri m;
    public final long n;
    public final s3 o;
    public final AtomicLong p;
    public final zf0 q;
    public final dg0 r;
    public int s;
    public mz t;
    public boolean u;
    public volatile fq1 v;
    public volatile Uri w;
    public volatile Exception x;
    public volatile Exception y;
    public volatile int z;
    
    static {
        E = new Random();
        i12.F = new ko1();
        i12.G = DefaultClock.getInstance();
    }
    
    public i12(final jq1 p0, final fq1 p1, final Uri p2, final Uri p3) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokespecial   zq1.<init>:()V
        //     4: aload_0        
        //     5: new             Ljava/util/concurrent/atomic/AtomicLong;
        //     8: dup            
        //     9: lconst_0       
        //    10: invokespecial   java/util/concurrent/atomic/AtomicLong.<init>:(J)V
        //    13: putfield        i12.p:Ljava/util/concurrent/atomic/AtomicLong;
        //    16: aload_0        
        //    17: ldc             262144
        //    19: putfield        i12.s:I
        //    22: aconst_null    
        //    23: astore          10
        //    25: aconst_null    
        //    26: astore          11
        //    28: aload_0        
        //    29: aconst_null    
        //    30: putfield        i12.w:Landroid/net/Uri;
        //    33: aload_0        
        //    34: aconst_null    
        //    35: putfield        i12.x:Ljava/lang/Exception;
        //    38: aload_0        
        //    39: aconst_null    
        //    40: putfield        i12.y:Ljava/lang/Exception;
        //    43: aload_0        
        //    44: iconst_0       
        //    45: putfield        i12.z:I
        //    48: aload_0        
        //    49: iconst_0       
        //    50: putfield        i12.C:I
        //    53: aload_0        
        //    54: sipush          1000
        //    57: putfield        i12.D:I
        //    60: aload_1        
        //    61: invokestatic    com/google/android/gms/common/internal/Preconditions.checkNotNull:(Ljava/lang/Object;)Ljava/lang/Object;
        //    64: pop            
        //    65: aload_3        
        //    66: invokestatic    com/google/android/gms/common/internal/Preconditions.checkNotNull:(Ljava/lang/Object;)Ljava/lang/Object;
        //    69: pop            
        //    70: aload_1        
        //    71: invokevirtual   jq1.j:()Lo30;
        //    74: astore          12
        //    76: aload_0        
        //    77: aload_1        
        //    78: putfield        i12.l:Ljq1;
        //    81: aload_0        
        //    82: aload_2        
        //    83: putfield        i12.v:Lfq1;
        //    86: aload           12
        //    88: invokevirtual   o30.c:()Lzf0;
        //    91: astore_2       
        //    92: aload_0        
        //    93: aload_2        
        //    94: putfield        i12.q:Lzf0;
        //    97: aload           12
        //    99: invokevirtual   o30.b:()Ldg0;
        //   102: astore          13
        //   104: aload_0        
        //   105: aload           13
        //   107: putfield        i12.r:Ldg0;
        //   110: aload_0        
        //   111: aload_3        
        //   112: putfield        i12.m:Landroid/net/Uri;
        //   115: aload_0        
        //   116: aload           12
        //   118: invokevirtual   o30.k:()J
        //   121: putfield        i12.B:J
        //   124: aload_0        
        //   125: new             Lmz;
        //   128: dup            
        //   129: aload_1        
        //   130: invokevirtual   jq1.e:()Lr10;
        //   133: invokevirtual   r10.l:()Landroid/content/Context;
        //   136: aload_2        
        //   137: aload           13
        //   139: aload           12
        //   141: invokevirtual   o30.m:()J
        //   144: invokespecial   mz.<init>:(Landroid/content/Context;Lzf0;Ldg0;J)V
        //   147: putfield        i12.t:Lmz;
        //   150: ldc2_w          -1
        //   153: lstore          6
        //   155: aload           11
        //   157: astore_2       
        //   158: aload_1        
        //   159: invokevirtual   jq1.j:()Lo30;
        //   162: invokevirtual   o30.a:()Lr10;
        //   165: invokevirtual   r10.l:()Landroid/content/Context;
        //   168: invokevirtual   android/content/Context.getContentResolver:()Landroid/content/ContentResolver;
        //   171: astore          11
        //   173: aload           11
        //   175: aload_3        
        //   176: ldc             "r"
        //   178: invokevirtual   android/content/ContentResolver.openFileDescriptor:(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
        //   181: astore_1       
        //   182: aload_1        
        //   183: ifnull          293
        //   186: aload_1        
        //   187: invokevirtual   android/os/ParcelFileDescriptor.getStatSize:()J
        //   190: lstore          6
        //   192: aload_1        
        //   193: invokevirtual   android/os/ParcelFileDescriptor.close:()V
        //   196: goto            298
        //   199: astore_1       
        //   200: goto            213
        //   203: astore_1       
        //   204: goto            280
        //   207: astore_1       
        //   208: ldc2_w          -1
        //   211: lstore          6
        //   213: lload           6
        //   215: lstore          8
        //   217: new             Ljava/lang/StringBuilder;
        //   220: astore_2       
        //   221: lload           6
        //   223: lstore          8
        //   225: aload_2        
        //   226: invokespecial   java/lang/StringBuilder.<init>:()V
        //   229: lload           6
        //   231: lstore          8
        //   233: aload_2        
        //   234: ldc             "could not retrieve file size for upload "
        //   236: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   239: pop            
        //   240: lload           6
        //   242: lstore          8
        //   244: aload_2        
        //   245: aload_0        
        //   246: getfield        i12.m:Landroid/net/Uri;
        //   249: invokevirtual   android/net/Uri.toString:()Ljava/lang/String;
        //   252: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   255: pop            
        //   256: lload           6
        //   258: lstore          8
        //   260: ldc             "UploadTask"
        //   262: aload_2        
        //   263: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   266: aload_1        
        //   267: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   270: pop            
        //   271: goto            298
        //   274: astore_1       
        //   275: ldc2_w          -1
        //   278: lstore          6
        //   280: lload           6
        //   282: lstore          8
        //   284: ldc             "UploadTask"
        //   286: ldc             "NullPointerException during file size calculation."
        //   288: aload_1        
        //   289: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   292: pop            
        //   293: ldc2_w          -1
        //   296: lstore          6
        //   298: lload           6
        //   300: lstore          8
        //   302: aload           11
        //   304: aload_0        
        //   305: getfield        i12.m:Landroid/net/Uri;
        //   308: invokevirtual   android/content/ContentResolver.openInputStream:(Landroid/net/Uri;)Ljava/io/InputStream;
        //   311: astore_1       
        //   312: aload_1        
        //   313: astore_2       
        //   314: lload           6
        //   316: lstore          8
        //   318: aload_1        
        //   319: ifnull          439
        //   322: lload           6
        //   324: lstore          8
        //   326: lload           6
        //   328: ldc2_w          -1
        //   331: lcmp           
        //   332: ifne            355
        //   335: aload_1        
        //   336: invokevirtual   java/io/InputStream.available:()I
        //   339: istore          5
        //   341: lload           6
        //   343: lstore          8
        //   345: iload           5
        //   347: iflt            355
        //   350: iload           5
        //   352: i2l            
        //   353: lstore          8
        //   355: aload_1        
        //   356: astore_2       
        //   357: lload           8
        //   359: lstore          6
        //   361: new             Ljava/io/BufferedInputStream;
        //   364: astore_3       
        //   365: aload_1        
        //   366: astore_2       
        //   367: lload           8
        //   369: lstore          6
        //   371: aload_3        
        //   372: aload_1        
        //   373: invokespecial   java/io/BufferedInputStream.<init>:(Ljava/io/InputStream;)V
        //   376: aload_3        
        //   377: astore_2       
        //   378: goto            439
        //   381: astore_1       
        //   382: lload           8
        //   384: lstore          6
        //   386: aload           10
        //   388: astore_2       
        //   389: goto            393
        //   392: astore_1       
        //   393: new             Ljava/lang/StringBuilder;
        //   396: dup            
        //   397: invokespecial   java/lang/StringBuilder.<init>:()V
        //   400: astore_3       
        //   401: aload_3        
        //   402: ldc             "could not locate file for uploading:"
        //   404: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   407: pop            
        //   408: aload_3        
        //   409: aload_0        
        //   410: getfield        i12.m:Landroid/net/Uri;
        //   413: invokevirtual   android/net/Uri.toString:()Ljava/lang/String;
        //   416: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   419: pop            
        //   420: ldc             "UploadTask"
        //   422: aload_3        
        //   423: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   426: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;)I
        //   429: pop            
        //   430: aload_0        
        //   431: aload_1        
        //   432: putfield        i12.x:Ljava/lang/Exception;
        //   435: lload           6
        //   437: lstore          8
        //   439: aload_0        
        //   440: lload           8
        //   442: putfield        i12.n:J
        //   445: aload_0        
        //   446: new             Ls3;
        //   449: dup            
        //   450: aload_2        
        //   451: ldc             262144
        //   453: invokespecial   s3.<init>:(Ljava/io/InputStream;I)V
        //   456: putfield        i12.o:Ls3;
        //   459: aload_0        
        //   460: iconst_1       
        //   461: putfield        i12.u:Z
        //   464: aload_0        
        //   465: aload           4
        //   467: putfield        i12.w:Landroid/net/Uri;
        //   470: return         
        //   471: astore_2       
        //   472: lload           6
        //   474: lstore          8
        //   476: goto            355
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                            
        //  -----  -----  -----  -----  --------------------------------
        //  158    173    392    393    Ljava/io/FileNotFoundException;
        //  173    182    274    280    Ljava/lang/NullPointerException;
        //  173    182    207    213    Ljava/io/IOException;
        //  186    192    274    280    Ljava/lang/NullPointerException;
        //  186    192    207    213    Ljava/io/IOException;
        //  192    196    203    207    Ljava/lang/NullPointerException;
        //  192    196    199    203    Ljava/io/IOException;
        //  217    221    381    392    Ljava/io/FileNotFoundException;
        //  225    229    381    392    Ljava/io/FileNotFoundException;
        //  233    240    381    392    Ljava/io/FileNotFoundException;
        //  244    256    381    392    Ljava/io/FileNotFoundException;
        //  260    271    381    392    Ljava/io/FileNotFoundException;
        //  284    293    381    392    Ljava/io/FileNotFoundException;
        //  302    312    381    392    Ljava/io/FileNotFoundException;
        //  335    341    471    479    Ljava/io/IOException;
        //  361    365    392    393    Ljava/io/FileNotFoundException;
        //  371    376    392    393    Ljava/io/FileNotFoundException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0355:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createConstructor(AstBuilder.java:799)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:635)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static /* synthetic */ zf0 i0(final i12 i12) {
        return i12.q;
    }
    
    public static /* synthetic */ dg0 j0(final i12 i12) {
        return i12.r;
    }
    
    public static /* synthetic */ jq1 k0(final i12 i12) {
        return i12.l;
    }
    
    @Override
    public jq1 G() {
        return this.l;
    }
    
    @Override
    public void R() {
        this.t.a();
        df1 df1;
        if (this.w != null) {
            df1 = new df1(this.l.k(), this.l.e(), this.w);
        }
        else {
            df1 = null;
        }
        if (df1 != null) {
            br1.a().e(new Runnable(this, df1) {
                public final xy0 a;
                public final i12 b;
                
                @Override
                public void run() {
                    this.a.B(m22.c(i12.i0(this.b)), m22.b(i12.j0(this.b)), i12.k0(this.b).e().l());
                }
            });
        }
        this.x = StorageException.fromErrorStatus(Status.RESULT_CANCELED);
        super.R();
    }
    
    @Override
    public void b0() {
        this.t.c();
        if (!this.g0(4, false)) {
            Log.d("UploadTask", "The upload cannot continue as it is not in a valid state.");
            return;
        }
        if (this.l.h() == null) {
            this.x = new IllegalArgumentException("Cannot upload to getRoot. You should upload to a storage location such as .getReference('image.png').putFile...");
        }
        if (this.x != null) {
            return;
        }
        if (this.w == null) {
            this.l0();
        }
        else {
            this.q0(false);
        }
        boolean b2;
        for (boolean b = this.u0(); b; b = b2) {
            this.w0();
            b2 = (b = this.u0());
            if (b2) {
                this.g0(4, false);
            }
        }
        if (this.u && this.A() != 16) {
            try {
                this.o.c();
            }
            catch (final IOException ex) {
                Log.e("UploadTask", "Unable to close stream.", (Throwable)ex);
            }
        }
    }
    
    @Override
    public void c0() {
        br1.a().g(this.D());
    }
    
    public final void l0() {
        final fq1 v = this.v;
        final JSONObject jsonObject = null;
        String v2;
        if (v != null) {
            v2 = this.v.v();
        }
        else {
            v2 = null;
        }
        String type = v2;
        if (this.m != null) {
            type = v2;
            if (TextUtils.isEmpty((CharSequence)v2)) {
                type = this.l.j().a().l().getContentResolver().getType(this.m);
            }
        }
        String s = type;
        if (TextUtils.isEmpty((CharSequence)type)) {
            s = "application/octet-stream";
        }
        final kq1 k = this.l.k();
        final r10 e = this.l.e();
        JSONObject q = jsonObject;
        if (this.v != null) {
            q = this.v.q();
        }
        final ff1 ff1 = new ff1(k, e, q, s);
        if (!this.s0(ff1)) {
            return;
        }
        final String q2 = ff1.q("X-Goog-Upload-URL");
        if (!TextUtils.isEmpty((CharSequence)q2)) {
            this.w = Uri.parse(q2);
        }
    }
    
    public final boolean m0(final xy0 xy0) {
        try {
            final StringBuilder sb = new StringBuilder();
            sb.append("Waiting ");
            sb.append(this.C);
            sb.append(" milliseconds");
            Log.d("UploadTask", sb.toString());
            i12.F.a(this.C + i12.E.nextInt(250));
            final boolean r0 = this.r0(xy0);
            if (r0) {
                this.C = 0;
            }
            return r0;
        }
        catch (final InterruptedException y) {
            Log.w("UploadTask", "thread interrupted during exponential backoff.");
            Thread.currentThread().interrupt();
            this.y = y;
            return false;
        }
    }
    
    public long n0() {
        return this.n;
    }
    
    public final boolean o0(final int n) {
        return n == 308 || (n >= 200 && n < 300);
    }
    
    public final boolean p0(final xy0 xy0) {
        int o;
        if (this.t.b(o = xy0.o())) {
            o = -2;
        }
        this.z = o;
        this.y = xy0.f();
        this.A = xy0.q("X-Goog-Upload-Status");
        return this.o0(this.z) && this.y == null;
    }
    
    public final boolean q0(final boolean b) {
        final ef1 ef1 = new ef1(this.l.k(), this.l.e(), this.w);
        if ("final".equals(this.A)) {
            return false;
        }
        if (b) {
            if (!this.s0(ef1)) {
                return false;
            }
        }
        else if (!this.r0(ef1)) {
            return false;
        }
        IOException x = null;
        Label_0097: {
            if ("final".equals(ef1.q("X-Goog-Upload-Status"))) {
                x = new IOException("The server has terminated the upload session");
            }
            else {
                final String q = ef1.q("X-Goog-Upload-Size-Received");
                long long1;
                if (!TextUtils.isEmpty((CharSequence)q)) {
                    long1 = Long.parseLong(q);
                }
                else {
                    long1 = 0L;
                }
                final long value = this.p.get();
                final long n = lcmp(value, long1);
                if (n <= 0) {
                    if (n < 0) {
                        try {
                            final s3 o = this.o;
                            final long n2 = long1 - value;
                            if (o.a((int)n2) != n2) {
                                this.x = new IOException("Unexpected end of stream encountered.");
                                return false;
                            }
                            if (!this.p.compareAndSet(value, long1)) {
                                Log.e("UploadTask", "Somehow, the uploaded bytes changed during an uploaded.  This should nothappen");
                                this.x = new IllegalStateException("uploaded bytes changed unexpectedly.");
                                return false;
                            }
                        }
                        catch (final IOException x) {
                            Log.e("UploadTask", "Unable to recover position in Stream during resumable upload", (Throwable)x);
                            break Label_0097;
                        }
                    }
                    return true;
                }
                x = new IOException("Unexpected error. The server lost a chunk update.");
            }
        }
        this.x = x;
        return false;
    }
    
    public final boolean r0(final xy0 xy0) {
        xy0.B(m22.c(this.q), m22.b(this.r), this.l.e().l());
        return this.p0(xy0);
    }
    
    public final boolean s0(final xy0 xy0) {
        this.t.d(xy0);
        return this.p0(xy0);
    }
    
    public final boolean t0() {
        if ("final".equals(this.A)) {
            if (this.x == null) {
                this.x = new IOException("The server has terminated the upload session", this.y);
            }
            this.g0(64, false);
            return false;
        }
        return true;
    }
    
    public final boolean u0() {
        if (this.A() == 128) {
            return false;
        }
        if (Thread.interrupted()) {
            this.x = new InterruptedException();
            this.g0(64, false);
            return false;
        }
        if (this.A() == 32) {
            this.g0(256, false);
            return false;
        }
        if (this.A() == 8) {
            this.g0(16, false);
            return false;
        }
        if (!this.t0()) {
            return false;
        }
        if (this.w == null) {
            if (this.x == null) {
                this.x = new IllegalStateException("Unable to obtain an upload URL.");
            }
            this.g0(64, false);
            return false;
        }
        if (this.x != null) {
            this.g0(64, false);
            return false;
        }
        final boolean b = this.y != null || this.z < 200 || this.z >= 300;
        final long elapsedRealtime = i12.G.elapsedRealtime();
        final long b2 = this.B;
        final long elapsedRealtime2 = i12.G.elapsedRealtime();
        final long n = this.C;
        if (b) {
            if (elapsedRealtime2 + n > elapsedRealtime + b2 || !this.q0(true)) {
                if (this.t0()) {
                    this.g0(64, false);
                }
                return false;
            }
            this.C = Math.max(this.C * 2, 1000);
        }
        return true;
    }
    
    public b v0() {
        Exception ex;
        if (this.x != null) {
            ex = this.x;
        }
        else {
            ex = this.y;
        }
        return new b(StorageException.fromExceptionAndHttpCode(ex, this.z), this.p.get(), this.w, this.v);
    }
    
    public final void w0() {
        try {
            this.o.d(this.s);
            final int min = Math.min(this.s, this.o.b());
            final cf1 cf1 = new cf1(this.l.k(), this.l.e(), this.w, this.o.e(), this.p.get(), min, this.o.f());
            if (!this.m0(cf1)) {
                this.s = 262144;
                final StringBuilder sb = new StringBuilder();
                sb.append("Resetting chunk size to ");
                sb.append(this.s);
                Log.d("UploadTask", sb.toString());
                return;
            }
            this.p.getAndAdd(min);
            if (!this.o.f()) {
                this.o.a(min);
                final int s = this.s;
                if (s < 33554432) {
                    this.s = s * 2;
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Increasing chunk size to ");
                    sb2.append(this.s);
                    Log.d("UploadTask", sb2.toString());
                }
            }
            else {
                try {
                    this.v = new fq1.b(cf1.n(), this.l).a();
                    this.g0(4, false);
                    this.g0(128, false);
                }
                catch (final JSONException x) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("Unable to parse resulting metadata from upload:");
                    sb3.append(cf1.m());
                    Log.e("UploadTask", sb3.toString(), (Throwable)x);
                    this.x = (Exception)x;
                }
            }
        }
        catch (final IOException x2) {
            Log.e("UploadTask", "Unable to read bytes for uploading", (Throwable)x2);
            this.x = x2;
        }
    }
    
    public class b extends zq1.b
    {
        public final long c;
        public final Uri d;
        public final fq1 e;
        public final i12 f;
        
        public b(final i12 f, final Exception ex, final long c, final Uri d, final fq1 e) {
            this.f = f.super(ex);
            this.c = c;
            this.d = d;
            this.e = e;
        }
        
        public long a() {
            return this.c;
        }
        
        public long b() {
            return this.f.n0();
        }
    }
}

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.security.AccessController;
import java.lang.reflect.Field;
import java.security.PrivilegedExceptionAction;
import java.nio.ByteOrder;
import sun.misc.Unsafe;
import java.util.logging.Logger;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class e12
{
    public static final Logger a;
    public static final Unsafe b;
    public static final Class c;
    public static final boolean d;
    public static final boolean e;
    public static final e f;
    public static final boolean g;
    public static final boolean h;
    public static final long i;
    public static final long j;
    public static final long k;
    public static final long l;
    public static final long m;
    public static final long n;
    public static final long o;
    public static final long p;
    public static final long q;
    public static final long r;
    public static final long s;
    public static final long t;
    public static final long u;
    public static final long v;
    public static final int w;
    public static final boolean x;
    
    static {
        a = Logger.getLogger(e12.class.getName());
        b = F();
        c = q4.b();
        d = o(Long.TYPE);
        e = o(Integer.TYPE);
        f = D();
        g = V();
        h = U();
        final long n2 = i = k(byte[].class);
        j = k(boolean[].class);
        k = l(boolean[].class);
        l = k(int[].class);
        m = l(int[].class);
        n = k(long[].class);
        o = l(long[].class);
        p = k(float[].class);
        q = l(float[].class);
        r = k(double[].class);
        s = l(double[].class);
        t = k(Object[].class);
        u = l(Object[].class);
        v = q(m());
        w = (int)(n2 & 0x7L);
        x = (ByteOrder.nativeOrder() == ByteOrder.BIG_ENDIAN);
    }
    
    public static int A(final Object o, final long n) {
        return e12.f.i(o, n);
    }
    
    public static long B(final long n) {
        return e12.f.j(n);
    }
    
    public static long C(final Object o, final long n) {
        return e12.f.k(o, n);
    }
    
    public static e D() {
        final Unsafe b = e12.b;
        Object o = null;
        if (b == null) {
            return null;
        }
        if (!q4.c()) {
            return (e)new d(b);
        }
        if (e12.d) {
            return (e)new c(b);
        }
        if (e12.e) {
            o = new b(b);
        }
        return (e)o;
    }
    
    public static Object E(final Object o, final long n) {
        return e12.f.l(o, n);
    }
    
    public static Unsafe F() {
        Unsafe unsafe2;
        try {
            final Unsafe unsafe = AccessController.doPrivileged((PrivilegedExceptionAction<Unsafe>)new PrivilegedExceptionAction() {
                public Unsafe a() {
                    for (final Field field : Unsafe.class.getDeclaredFields()) {
                        field.setAccessible(true);
                        final Object value = field.get(null);
                        if (Unsafe.class.isInstance(value)) {
                            return Unsafe.class.cast(value);
                        }
                    }
                    return null;
                }
            });
        }
        finally {
            unsafe2 = null;
        }
        return unsafe2;
    }
    
    public static boolean G() {
        return e12.h;
    }
    
    public static boolean H() {
        return e12.g;
    }
    
    public static long I(final Field field) {
        return e12.f.m(field);
    }
    
    public static void J(final Object o, final long n, final boolean b) {
        e12.f.n(o, n, b);
    }
    
    public static void K(final Object o, final long n, final boolean b) {
        N(o, n, (byte)(b ? 1 : 0));
    }
    
    public static void L(final Object o, final long n, final boolean b) {
        O(o, n, (byte)(b ? 1 : 0));
    }
    
    public static void M(final byte[] array, final long n, final byte b) {
        e12.f.o(array, e12.i + n, b);
    }
    
    public static void N(final Object o, final long n, final byte b) {
        final long n2 = 0xFFFFFFFFFFFFFFFCL & n;
        final int a = A(o, n2);
        final int n3 = (~(int)n & 0x3) << 3;
        R(o, n2, (0xFF & b) << n3 | (a & ~(255 << n3)));
    }
    
    public static void O(final Object o, final long n, final byte b) {
        final long n2 = 0xFFFFFFFFFFFFFFFCL & n;
        final int a = A(o, n2);
        final int n3 = ((int)n & 0x3) << 3;
        R(o, n2, (0xFF & b) << n3 | (a & ~(255 << n3)));
    }
    
    public static void P(final Object o, final long n, final double n2) {
        e12.f.p(o, n, n2);
    }
    
    public static void Q(final Object o, final long n, final float n2) {
        e12.f.q(o, n, n2);
    }
    
    public static void R(final Object o, final long n, final int n2) {
        e12.f.r(o, n, n2);
    }
    
    public static void S(final Object o, final long n, final long n2) {
        e12.f.s(o, n, n2);
    }
    
    public static void T(final Object o, final long n, final Object o2) {
        e12.f.t(o, n, o2);
    }
    
    public static boolean U() {
        final Unsafe b = e12.b;
        if (b == null) {
            return false;
        }
        try {
            final Class<? extends Unsafe> class1 = b.getClass();
            class1.getMethod("objectFieldOffset", Field.class);
            class1.getMethod("arrayBaseOffset", Class.class);
            class1.getMethod("arrayIndexScale", Class.class);
            final Class<Long> type = Long.TYPE;
            class1.getMethod("getInt", Object.class, type);
            class1.getMethod("putInt", Object.class, type, Integer.TYPE);
            class1.getMethod("getLong", Object.class, type);
            class1.getMethod("putLong", Object.class, type, type);
            class1.getMethod("getObject", Object.class, type);
            class1.getMethod("putObject", Object.class, type, Object.class);
            if (q4.c()) {
                return true;
            }
            class1.getMethod("getByte", Object.class, type);
            class1.getMethod("putByte", Object.class, type, Byte.TYPE);
            class1.getMethod("getBoolean", Object.class, type);
            class1.getMethod("putBoolean", Object.class, type, Boolean.TYPE);
            class1.getMethod("getFloat", Object.class, type);
            class1.getMethod("putFloat", Object.class, type, Float.TYPE);
            class1.getMethod("getDouble", Object.class, type);
            class1.getMethod("putDouble", Object.class, type, Double.TYPE);
            return true;
        }
        finally {
            final Logger a = e12.a;
            final Level warning = Level.WARNING;
            final StringBuilder sb = new StringBuilder();
            sb.append("platform method missing - proto runtime falling back to safer methods: ");
            final Throwable obj;
            sb.append(obj);
            a.log(warning, sb.toString());
            return false;
        }
    }
    
    public static boolean V() {
        final Unsafe b = e12.b;
        if (b == null) {
            return false;
        }
        try {
            final Class<? extends Unsafe> class1 = b.getClass();
            class1.getMethod("objectFieldOffset", Field.class);
            final Class<Long> type = Long.TYPE;
            class1.getMethod("getLong", Object.class, type);
            if (m() == null) {
                return false;
            }
            if (q4.c()) {
                return true;
            }
            class1.getMethod("getByte", type);
            class1.getMethod("putByte", type, Byte.TYPE);
            class1.getMethod("getInt", type);
            class1.getMethod("putInt", type, Integer.TYPE);
            class1.getMethod("getLong", type);
            class1.getMethod("putLong", type, type);
            class1.getMethod("copyMemory", type, type, type);
            class1.getMethod("copyMemory", Object.class, type, Object.class, type, type);
            return true;
        }
        finally {
            final Logger a = e12.a;
            final Level warning = Level.WARNING;
            final StringBuilder sb = new StringBuilder();
            sb.append("platform method missing - proto runtime falling back to safer methods: ");
            final Throwable obj;
            sb.append(obj);
            a.log(warning, sb.toString());
            return false;
        }
    }
    
    public static long i(final ByteBuffer byteBuffer) {
        return e12.f.k(byteBuffer, e12.v);
    }
    
    public static Object j(final Class cls) {
        try {
            return e12.b.allocateInstance(cls);
        }
        catch (final InstantiationException cause) {
            throw new IllegalStateException(cause);
        }
    }
    
    public static int k(final Class clazz) {
        int a;
        if (e12.h) {
            a = e12.f.a(clazz);
        }
        else {
            a = -1;
        }
        return a;
    }
    
    public static int l(final Class clazz) {
        int b;
        if (e12.h) {
            b = e12.f.b(clazz);
        }
        else {
            b = -1;
        }
        return b;
    }
    
    public static Field m() {
        if (q4.c()) {
            final Field p = p(Buffer.class, "effectiveDirectAddress");
            if (p != null) {
                return p;
            }
        }
        Field p2 = p(Buffer.class, "address");
        if (p2 == null || p2.getType() != Long.TYPE) {
            p2 = null;
        }
        return p2;
    }
    
    public static void n(final long n, final byte[] array, final long n2, final long n3) {
        e12.f.c(n, array, n2, n3);
    }
    
    public static boolean o(final Class clazz) {
        if (!q4.c()) {
            return false;
        }
        try {
            final Class c = e12.c;
            final Class<Boolean> type = Boolean.TYPE;
            c.getMethod("peekLong", clazz, type);
            c.getMethod("pokeLong", clazz, Long.TYPE, type);
            final Class<Integer> type2 = Integer.TYPE;
            c.getMethod("pokeInt", clazz, type2, type);
            c.getMethod("peekInt", clazz, type);
            c.getMethod("pokeByte", clazz, Byte.TYPE);
            c.getMethod("peekByte", clazz);
            c.getMethod("pokeByteArray", clazz, byte[].class, type2, type2);
            c.getMethod("peekByteArray", clazz, byte[].class, type2, type2);
            return true;
        }
        finally {
            return false;
        }
    }
    
    public static Field p(final Class clazz, final String name) {
        Field field;
        try {
            clazz.getDeclaredField(name);
        }
        finally {
            field = null;
        }
        return field;
    }
    
    public static long q(final Field field) {
        if (field != null) {
            final e f = e12.f;
            if (f != null) {
                return f.m(field);
            }
        }
        return -1L;
    }
    
    public static boolean r(final Object o, final long n) {
        return e12.f.d(o, n);
    }
    
    public static boolean s(final Object o, final long n) {
        return w(o, n) != 0;
    }
    
    public static boolean t(final Object o, final long n) {
        return x(o, n) != 0;
    }
    
    public static byte u(final long n) {
        return e12.f.e(n);
    }
    
    public static byte v(final byte[] array, final long n) {
        return e12.f.f(array, e12.i + n);
    }
    
    public static byte w(final Object o, final long n) {
        return (byte)(A(o, 0xFFFFFFFFFFFFFFFCL & n) >>> (int)((~n & 0x3L) << 3) & 0xFF);
    }
    
    public static byte x(final Object o, final long n) {
        return (byte)(A(o, 0xFFFFFFFFFFFFFFFCL & n) >>> (int)((n & 0x3L) << 3) & 0xFF);
    }
    
    public static double y(final Object o, final long n) {
        return e12.f.g(o, n);
    }
    
    public static float z(final Object o, final long n) {
        return e12.f.h(o, n);
    }
    
    public static final class b extends e
    {
        public b(final Unsafe unsafe) {
            super(unsafe);
        }
        
        @Override
        public void c(final long n, final byte[] array, final long n2, final long n3) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public boolean d(final Object o, final long n) {
            if (e12.x) {
                return s(o, n);
            }
            return t(o, n);
        }
        
        @Override
        public byte e(final long n) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public byte f(final Object o, final long n) {
            if (e12.x) {
                return w(o, n);
            }
            return x(o, n);
        }
        
        @Override
        public double g(final Object o, final long n) {
            return Double.longBitsToDouble(((e)this).k(o, n));
        }
        
        @Override
        public float h(final Object o, final long n) {
            return Float.intBitsToFloat(((e)this).i(o, n));
        }
        
        @Override
        public long j(final long n) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void n(final Object o, final long n, final boolean b) {
            if (e12.x) {
                K(o, n, b);
            }
            else {
                L(o, n, b);
            }
        }
        
        @Override
        public void o(final Object o, final long n, final byte b) {
            if (e12.x) {
                N(o, n, b);
            }
            else {
                O(o, n, b);
            }
        }
        
        @Override
        public void p(final Object o, final long n, final double value) {
            ((e)this).s(o, n, Double.doubleToLongBits(value));
        }
        
        @Override
        public void q(final Object o, final long n, final float value) {
            ((e)this).r(o, n, Float.floatToIntBits(value));
        }
    }
    
    public static final class c extends e
    {
        public c(final Unsafe unsafe) {
            super(unsafe);
        }
        
        @Override
        public void c(final long n, final byte[] array, final long n2, final long n3) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public boolean d(final Object o, final long n) {
            if (e12.x) {
                return s(o, n);
            }
            return t(o, n);
        }
        
        @Override
        public byte e(final long n) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public byte f(final Object o, final long n) {
            if (e12.x) {
                return w(o, n);
            }
            return x(o, n);
        }
        
        @Override
        public double g(final Object o, final long n) {
            return Double.longBitsToDouble(((e)this).k(o, n));
        }
        
        @Override
        public float h(final Object o, final long n) {
            return Float.intBitsToFloat(((e)this).i(o, n));
        }
        
        @Override
        public long j(final long n) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public void n(final Object o, final long n, final boolean b) {
            if (e12.x) {
                K(o, n, b);
            }
            else {
                L(o, n, b);
            }
        }
        
        @Override
        public void o(final Object o, final long n, final byte b) {
            if (e12.x) {
                N(o, n, b);
            }
            else {
                O(o, n, b);
            }
        }
        
        @Override
        public void p(final Object o, final long n, final double value) {
            ((e)this).s(o, n, Double.doubleToLongBits(value));
        }
        
        @Override
        public void q(final Object o, final long n, final float value) {
            ((e)this).r(o, n, Float.floatToIntBits(value));
        }
    }
    
    public static final class d extends e
    {
        public d(final Unsafe unsafe) {
            super(unsafe);
        }
        
        @Override
        public void c(final long srcOffset, final byte[] destBase, final long n, final long bytes) {
            super.a.copyMemory(null, srcOffset, destBase, e12.i + n, bytes);
        }
        
        @Override
        public boolean d(final Object o, final long offset) {
            return super.a.getBoolean(o, offset);
        }
        
        @Override
        public byte e(final long address) {
            return super.a.getByte(address);
        }
        
        @Override
        public byte f(final Object o, final long offset) {
            return super.a.getByte(o, offset);
        }
        
        @Override
        public double g(final Object o, final long offset) {
            return super.a.getDouble(o, offset);
        }
        
        @Override
        public float h(final Object o, final long offset) {
            return super.a.getFloat(o, offset);
        }
        
        @Override
        public long j(final long address) {
            return super.a.getLong(address);
        }
        
        @Override
        public void n(final Object o, final long offset, final boolean x) {
            super.a.putBoolean(o, offset, x);
        }
        
        @Override
        public void o(final Object o, final long offset, final byte x) {
            super.a.putByte(o, offset, x);
        }
        
        @Override
        public void p(final Object o, final long offset, final double x) {
            super.a.putDouble(o, offset, x);
        }
        
        @Override
        public void q(final Object o, final long offset, final float x) {
            super.a.putFloat(o, offset, x);
        }
    }
    
    public abstract static class e
    {
        public Unsafe a;
        
        public e(final Unsafe a) {
            this.a = a;
        }
        
        public final int a(final Class arrayClass) {
            return this.a.arrayBaseOffset(arrayClass);
        }
        
        public final int b(final Class arrayClass) {
            return this.a.arrayIndexScale(arrayClass);
        }
        
        public abstract void c(final long p0, final byte[] p1, final long p2, final long p3);
        
        public abstract boolean d(final Object p0, final long p1);
        
        public abstract byte e(final long p0);
        
        public abstract byte f(final Object p0, final long p1);
        
        public abstract double g(final Object p0, final long p1);
        
        public abstract float h(final Object p0, final long p1);
        
        public final int i(final Object o, final long offset) {
            return this.a.getInt(o, offset);
        }
        
        public abstract long j(final long p0);
        
        public final long k(final Object o, final long offset) {
            return this.a.getLong(o, offset);
        }
        
        public final Object l(final Object o, final long offset) {
            return this.a.getObject(o, offset);
        }
        
        public final long m(final Field f) {
            return this.a.objectFieldOffset(f);
        }
        
        public abstract void n(final Object p0, final long p1, final boolean p2);
        
        public abstract void o(final Object p0, final long p1, final byte p2);
        
        public abstract void p(final Object p0, final long p1, final double p2);
        
        public abstract void q(final Object p0, final long p1, final float p2);
        
        public final void r(final Object o, final long offset, final int x) {
            this.a.putInt(o, offset, x);
        }
        
        public final void s(final Object o, final long offset, final long x) {
            this.a.putLong(o, offset, x);
        }
        
        public final void t(final Object o, final long offset, final Object x) {
            this.a.putObject(o, offset, x);
        }
    }
}

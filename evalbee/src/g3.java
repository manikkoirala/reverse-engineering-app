import android.widget.AdapterView;
import android.graphics.Color;
import android.widget.ListView;
import android.view.View$OnClickListener;
import com.ekodroid.omrevaluator.database.repositories.ClassRepository;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.LayoutInflater;
import com.ekodroid.omrevaluator.database.ClassDataModel;
import android.view.ViewGroup;
import android.view.View;
import java.util.ArrayList;
import android.content.Context;
import android.util.SparseBooleanArray;
import android.widget.BaseAdapter;

// 
// Decompiled by Procyon v0.6.0
// 

public class g3 extends BaseAdapter
{
    public SparseBooleanArray a;
    public Context b;
    public ArrayList c;
    
    public g3(final Context b, final ArrayList c) {
        this.a = new SparseBooleanArray();
        this.b = b;
        this.c = c;
    }
    
    public SparseBooleanArray a() {
        return this.a;
    }
    
    public void b() {
        this.a = new SparseBooleanArray();
        this.notifyDataSetChanged();
    }
    
    public final void c(final int n, final boolean b) {
        if (b) {
            this.a.put(n, b);
        }
        else {
            this.a.delete(n);
        }
        this.notifyDataSetChanged();
    }
    
    public void d(final int n) {
        this.c(n, this.a.get(n) ^ true);
    }
    
    public int getCount() {
        return this.c.size();
    }
    
    public Object getItem(final int index) {
        return this.c.get(index);
    }
    
    public long getItemId(final int n) {
        return n;
    }
    
    public View getView(int n, final View view, final ViewGroup viewGroup) {
        final ClassDataModel classDataModel = this.c.get(n);
        final String className = classDataModel.getClassName();
        final View inflate = ((LayoutInflater)this.b.getSystemService("layout_inflater")).inflate(2131493021, (ViewGroup)null);
        final TextView textView = (TextView)inflate.findViewById(2131297212);
        final TextView textView2 = (TextView)inflate.findViewById(2131297307);
        final ImageView imageView = (ImageView)inflate.findViewById(2131296691);
        final ImageView imageView2 = (ImageView)inflate.findViewById(2131296686);
        textView.setText((CharSequence)className);
        textView2.setText((CharSequence)String.valueOf(ClassRepository.getInstance(this.b).getNumberOfStudentForAClass(className)));
        if (classDataModel.isSynced()) {
            imageView.setVisibility(0);
            imageView2.setVisibility(8);
        }
        else {
            imageView.setVisibility(8);
            imageView2.setVisibility(0);
        }
        inflate.findViewById(2131296484).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, viewGroup, n) {
            public final ViewGroup a;
            public final int b;
            public final g3 c;
            
            public void onClick(final View view) {
                ((AdapterView)this.a).performItemClick(view, this.b, (long)view.getId());
            }
        });
        if (this.a.get(n, false)) {
            n = this.b.getResources().getColor(2131099718);
        }
        else {
            n = Color.rgb(255, 255, 255);
        }
        inflate.setBackgroundColor(n);
        return inflate;
    }
}

import android.widget.AdapterView;
import android.widget.Adapter;
import android.util.SparseArray;
import java.util.Iterator;
import java.util.List;
import android.graphics.Rect;
import java.util.Map;
import android.util.SparseIntArray;
import android.animation.Animator$AnimatorListener;
import android.animation.AnimatorListenerAdapter;
import android.widget.ListView;
import android.graphics.Path;
import android.view.View;
import android.view.ViewGroup;
import android.animation.TimeInterpolator;
import android.animation.Animator;
import java.util.ArrayList;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class qy1 implements Cloneable
{
    static final boolean DBG = false;
    private static final int[] DEFAULT_MATCH_ORDER;
    private static final String LOG_TAG = "Transition";
    private static final int MATCH_FIRST = 1;
    public static final int MATCH_ID = 3;
    private static final String MATCH_ID_STR = "id";
    public static final int MATCH_INSTANCE = 1;
    private static final String MATCH_INSTANCE_STR = "instance";
    public static final int MATCH_ITEM_ID = 4;
    private static final String MATCH_ITEM_ID_STR = "itemId";
    private static final int MATCH_LAST = 4;
    public static final int MATCH_NAME = 2;
    private static final String MATCH_NAME_STR = "name";
    private static final g31 STRAIGHT_PATH_MOTION;
    private static ThreadLocal<r8> sRunningAnimators;
    private ArrayList<Animator> mAnimators;
    boolean mCanRemoveViews;
    ArrayList<Animator> mCurrentAnimators;
    long mDuration;
    private yy1 mEndValues;
    private ArrayList<xy1> mEndValuesList;
    private boolean mEnded;
    private f mEpicenterCallback;
    private TimeInterpolator mInterpolator;
    private ArrayList<g> mListeners;
    private int[] mMatchOrder;
    private String mName;
    private r8 mNameOverrides;
    private int mNumInstances;
    uy1 mParent;
    private g31 mPathMotion;
    private boolean mPaused;
    ty1 mPropagation;
    private ViewGroup mSceneRoot;
    private long mStartDelay;
    private yy1 mStartValues;
    private ArrayList<xy1> mStartValuesList;
    private ArrayList<View> mTargetChildExcludes;
    private ArrayList<View> mTargetExcludes;
    private ArrayList<Integer> mTargetIdChildExcludes;
    private ArrayList<Integer> mTargetIdExcludes;
    ArrayList<Integer> mTargetIds;
    private ArrayList<String> mTargetNameExcludes;
    private ArrayList<String> mTargetNames;
    private ArrayList<Class<?>> mTargetTypeChildExcludes;
    private ArrayList<Class<?>> mTargetTypeExcludes;
    private ArrayList<Class<?>> mTargetTypes;
    ArrayList<View> mTargets;
    
    static {
        DEFAULT_MATCH_ORDER = new int[] { 2, 1, 3, 4 };
        STRAIGHT_PATH_MOTION = new g31() {
            @Override
            public Path getPath(final float n, final float n2, final float n3, final float n4) {
                final Path path = new Path();
                path.moveTo(n, n2);
                path.lineTo(n3, n4);
                return path;
            }
        };
        qy1.sRunningAnimators = new ThreadLocal<r8>();
    }
    
    public qy1() {
        this.mName = this.getClass().getName();
        this.mStartDelay = -1L;
        this.mDuration = -1L;
        this.mInterpolator = null;
        this.mTargetIds = new ArrayList<Integer>();
        this.mTargets = new ArrayList<View>();
        this.mTargetNames = null;
        this.mTargetTypes = null;
        this.mTargetIdExcludes = null;
        this.mTargetExcludes = null;
        this.mTargetTypeExcludes = null;
        this.mTargetNameExcludes = null;
        this.mTargetIdChildExcludes = null;
        this.mTargetChildExcludes = null;
        this.mTargetTypeChildExcludes = null;
        this.mStartValues = new yy1();
        this.mEndValues = new yy1();
        this.mParent = null;
        this.mMatchOrder = qy1.DEFAULT_MATCH_ORDER;
        this.mSceneRoot = null;
        this.mCanRemoveViews = false;
        this.mCurrentAnimators = new ArrayList<Animator>();
        this.mNumInstances = 0;
        this.mPaused = false;
        this.mEnded = false;
        this.mListeners = null;
        this.mAnimators = new ArrayList<Animator>();
        this.mPathMotion = qy1.STRAIGHT_PATH_MOTION;
    }
    
    public static void b(final yy1 yy1, View view, final xy1 xy1) {
        yy1.a.put(view, xy1);
        final int id = view.getId();
        if (id >= 0) {
            if (yy1.b.indexOfKey(id) >= 0) {
                yy1.b.put(id, (Object)null);
            }
            else {
                yy1.b.put(id, (Object)view);
            }
        }
        final String j = o32.J(view);
        if (j != null) {
            if (yy1.d.containsKey(j)) {
                yy1.d.put(j, null);
            }
            else {
                yy1.d.put(j, view);
            }
        }
        if (view.getParent() instanceof ListView) {
            final ListView listView = (ListView)view.getParent();
            if (((Adapter)listView.getAdapter()).hasStableIds()) {
                final long itemIdAtPosition = ((AdapterView)listView).getItemIdAtPosition(((AdapterView)listView).getPositionForView(view));
                if (yy1.c.h(itemIdAtPosition) >= 0) {
                    view = (View)yy1.c.e(itemIdAtPosition);
                    if (view != null) {
                        o32.A0(view, false);
                        yy1.c.j(itemIdAtPosition, null);
                    }
                }
                else {
                    o32.A0(view, true);
                    yy1.c.j(itemIdAtPosition, view);
                }
            }
        }
    }
    
    public static boolean d(final int[] array, final int n) {
        final int n2 = array[n];
        for (int i = 0; i < n; ++i) {
            if (array[i] == n2) {
                return true;
            }
        }
        return false;
    }
    
    public static ArrayList h(final ArrayList list, final Object o, final boolean b) {
        ArrayList list2 = list;
        if (o != null) {
            if (b) {
                list2 = e.a(list, o);
            }
            else {
                list2 = e.b(list, o);
            }
        }
        return list2;
    }
    
    public static r8 k() {
        r8 value;
        if ((value = qy1.sRunningAnimators.get()) == null) {
            value = new r8();
            qy1.sRunningAnimators.set(value);
        }
        return value;
    }
    
    public static boolean l(final int n) {
        boolean b = true;
        if (n < 1 || n > 4) {
            b = false;
        }
        return b;
    }
    
    public static boolean m(final xy1 xy1, final xy1 xy2, final String s) {
        final Object value = xy1.a.get(s);
        final Object value2 = xy2.a.get(s);
        return (value != null || value2 != null) && (value == null || value2 == null || (value.equals(value2) ^ true));
    }
    
    public final void a(final r8 r8, final r8 r9) {
        final int n = 0;
        int n2 = 0;
        int i;
        while (true) {
            i = n;
            if (n2 >= r8.size()) {
                break;
            }
            final xy1 e = (xy1)r8.m(n2);
            if (this.isValidTarget(e.b)) {
                this.mStartValuesList.add(e);
                this.mEndValuesList.add(null);
            }
            ++n2;
        }
        while (i < r9.size()) {
            final xy1 e2 = (xy1)r9.m(i);
            if (this.isValidTarget(e2.b)) {
                this.mEndValuesList.add(e2);
                this.mStartValuesList.add(null);
            }
            ++i;
        }
    }
    
    public qy1 addListener(final g e) {
        if (this.mListeners == null) {
            this.mListeners = new ArrayList<g>();
        }
        this.mListeners.add(e);
        return this;
    }
    
    public qy1 addTarget(final int i) {
        if (i != 0) {
            this.mTargetIds.add(i);
        }
        return this;
    }
    
    public qy1 addTarget(final View e) {
        this.mTargets.add(e);
        return this;
    }
    
    public qy1 addTarget(final Class<?> e) {
        if (this.mTargetTypes == null) {
            this.mTargetTypes = new ArrayList<Class<?>>();
        }
        this.mTargetTypes.add(e);
        return this;
    }
    
    public qy1 addTarget(final String e) {
        if (this.mTargetNames == null) {
            this.mTargetNames = new ArrayList<String>();
        }
        this.mTargetNames.add(e);
        return this;
    }
    
    public void animate(final Animator animator) {
        if (animator == null) {
            this.end();
        }
        else {
            if (this.getDuration() >= 0L) {
                animator.setDuration(this.getDuration());
            }
            if (this.getStartDelay() >= 0L) {
                animator.setStartDelay(this.getStartDelay() + animator.getStartDelay());
            }
            if (this.getInterpolator() != null) {
                animator.setInterpolator(this.getInterpolator());
            }
            animator.addListener((Animator$AnimatorListener)new AnimatorListenerAdapter(this) {
                public final qy1 a;
                
                public void onAnimationEnd(final Animator animator) {
                    this.a.end();
                    animator.removeListener((Animator$AnimatorListener)this);
                }
            });
            animator.start();
        }
    }
    
    public void cancel() {
        for (int i = this.mCurrentAnimators.size() - 1; i >= 0; --i) {
            this.mCurrentAnimators.get(i).cancel();
        }
        final ArrayList<g> mListeners = this.mListeners;
        if (mListeners != null && mListeners.size() > 0) {
            final ArrayList list = (ArrayList)this.mListeners.clone();
            for (int size = list.size(), j = 0; j < size; ++j) {
                ((g)list.get(j)).onTransitionCancel(this);
            }
        }
    }
    
    public abstract void captureEndValues(final xy1 p0);
    
    public void capturePropagationValues(final xy1 xy1) {
    }
    
    public abstract void captureStartValues(final xy1 p0);
    
    public void captureValues(final ViewGroup viewGroup, final boolean b) {
        this.clearValues(b);
        final int size = this.mTargetIds.size();
        final int n = 0;
        Label_0301: {
            if (size > 0 || this.mTargets.size() > 0) {
                final ArrayList<String> mTargetNames = this.mTargetNames;
                if (mTargetNames == null || mTargetNames.isEmpty()) {
                    final ArrayList<Class<?>> mTargetTypes = this.mTargetTypes;
                    if (mTargetTypes == null || mTargetTypes.isEmpty()) {
                        for (int i = 0; i < this.mTargetIds.size(); ++i) {
                            final View viewById = ((View)viewGroup).findViewById((int)this.mTargetIds.get(i));
                            if (viewById != null) {
                                final xy1 xy1 = new xy1(viewById);
                                if (b) {
                                    this.captureStartValues(xy1);
                                }
                                else {
                                    this.captureEndValues(xy1);
                                }
                                xy1.c.add(this);
                                this.capturePropagationValues(xy1);
                                yy1 yy1;
                                if (b) {
                                    yy1 = this.mStartValues;
                                }
                                else {
                                    yy1 = this.mEndValues;
                                }
                                b(yy1, viewById, xy1);
                            }
                        }
                        for (int j = 0; j < this.mTargets.size(); ++j) {
                            final View view = this.mTargets.get(j);
                            final xy1 xy2 = new xy1(view);
                            if (b) {
                                this.captureStartValues(xy2);
                            }
                            else {
                                this.captureEndValues(xy2);
                            }
                            xy2.c.add(this);
                            this.capturePropagationValues(xy2);
                            yy1 yy2;
                            if (b) {
                                yy2 = this.mStartValues;
                            }
                            else {
                                yy2 = this.mEndValues;
                            }
                            b(yy2, view, xy2);
                        }
                        break Label_0301;
                    }
                }
            }
            this.e((View)viewGroup, b);
        }
        if (!b) {
            final r8 mNameOverrides = this.mNameOverrides;
            if (mNameOverrides != null) {
                final int size2 = mNameOverrides.size();
                final ArrayList list = new ArrayList<View>(size2);
                int n2 = 0;
                int k;
                while (true) {
                    k = n;
                    if (n2 >= size2) {
                        break;
                    }
                    list.add(this.mStartValues.d.remove(this.mNameOverrides.i(n2)));
                    ++n2;
                }
                while (k < size2) {
                    final View view2 = list.get(k);
                    if (view2 != null) {
                        this.mStartValues.d.put(this.mNameOverrides.m(k), view2);
                    }
                    ++k;
                }
            }
        }
    }
    
    public void clearValues(final boolean b) {
        yy1 yy1;
        if (b) {
            this.mStartValues.a.clear();
            this.mStartValues.b.clear();
            yy1 = this.mStartValues;
        }
        else {
            this.mEndValues.a.clear();
            this.mEndValues.b.clear();
            yy1 = this.mEndValues;
        }
        yy1.c.a();
    }
    
    public qy1 clone() {
        try {
            final qy1 qy1 = (qy1)super.clone();
            qy1.mAnimators = new ArrayList<Animator>();
            qy1.mStartValues = new yy1();
            qy1.mEndValues = new yy1();
            qy1.mStartValuesList = null;
            qy1.mEndValuesList = null;
            return qy1;
        }
        catch (final CloneNotSupportedException ex) {
            return null;
        }
    }
    
    public Animator createAnimator(final ViewGroup viewGroup, final xy1 xy1, final xy1 xy2) {
        return null;
    }
    
    public void createAnimators(final ViewGroup viewGroup, final yy1 yy1, final yy1 yy2, final ArrayList<xy1> list, final ArrayList<xy1> list2) {
        final r8 k = k();
        final SparseIntArray sparseIntArray = new SparseIntArray();
        for (int size = list.size(), i = 0; i < size; ++i) {
            final xy1 xy1 = list.get(i);
            final xy1 xy2 = list2.get(i);
            xy1 xy3;
            if ((xy3 = xy1) != null) {
                xy3 = xy1;
                if (!xy1.c.contains(this)) {
                    xy3 = null;
                }
            }
            xy1 xy4;
            if ((xy4 = xy2) != null) {
                xy4 = xy2;
                if (!xy2.c.contains(this)) {
                    xy4 = null;
                }
            }
            if ((xy3 != null || xy4 != null) && (xy3 == null || xy4 == null || this.isTransitionRequired(xy3, xy4))) {
                Animator animator = this.createAnimator(viewGroup, xy3, xy4);
                if (animator != null) {
                    View b2;
                    xy1 xy9;
                    if (xy4 != null) {
                        final View b = xy4.b;
                        final String[] transitionProperties = this.getTransitionProperties();
                        xy1 xy7 = null;
                        Label_0395: {
                            if (transitionProperties != null && transitionProperties.length > 0) {
                                final xy1 xy5 = new xy1(b);
                                final xy1 xy6 = (xy1)yy2.a.get(b);
                                Animator animator2 = animator;
                                if (xy6 != null) {
                                    int n = 0;
                                    while (true) {
                                        animator2 = animator;
                                        if (n >= transitionProperties.length) {
                                            break;
                                        }
                                        final Map a = xy5.a;
                                        final String s = transitionProperties[n];
                                        a.put(s, xy6.a.get(s));
                                        ++n;
                                    }
                                }
                                for (int size2 = k.size(), j = 0; j < size2; ++j) {
                                    final d d = (d)k.get(k.i(j));
                                    if (d.c != null && d.a == b && d.b.equals(this.getName()) && d.c.equals(xy5)) {
                                        animator = null;
                                        xy7 = xy5;
                                        break Label_0395;
                                    }
                                }
                                animator = animator2;
                                xy7 = xy5;
                            }
                            else {
                                xy7 = null;
                            }
                        }
                        final View view = b;
                        final xy1 xy8 = xy7;
                        b2 = view;
                        xy9 = xy8;
                    }
                    else {
                        b2 = xy3.b;
                        xy9 = null;
                    }
                    if (animator != null) {
                        k.put(animator, new d(b2, this.getName(), this, t42.d((View)viewGroup), xy9));
                        this.mAnimators.add(animator);
                    }
                }
            }
        }
        if (sparseIntArray.size() != 0) {
            for (int l = 0; l < sparseIntArray.size(); ++l) {
                final Animator animator3 = this.mAnimators.get(sparseIntArray.keyAt(l));
                animator3.setStartDelay(sparseIntArray.valueAt(l) - Long.MAX_VALUE + animator3.getStartDelay());
            }
        }
    }
    
    public final void e(final View view, final boolean b) {
        if (view == null) {
            return;
        }
        final int id = view.getId();
        final ArrayList<Integer> mTargetIdExcludes = this.mTargetIdExcludes;
        if (mTargetIdExcludes != null && mTargetIdExcludes.contains(id)) {
            return;
        }
        final ArrayList<View> mTargetExcludes = this.mTargetExcludes;
        if (mTargetExcludes != null && mTargetExcludes.contains(view)) {
            return;
        }
        final ArrayList<Class<?>> mTargetTypeExcludes = this.mTargetTypeExcludes;
        final int n = 0;
        if (mTargetTypeExcludes != null) {
            for (int size = mTargetTypeExcludes.size(), i = 0; i < size; ++i) {
                if (this.mTargetTypeExcludes.get(i).isInstance(view)) {
                    return;
                }
            }
        }
        if (view.getParent() instanceof ViewGroup) {
            final xy1 xy1 = new xy1(view);
            if (b) {
                this.captureStartValues(xy1);
            }
            else {
                this.captureEndValues(xy1);
            }
            xy1.c.add(this);
            this.capturePropagationValues(xy1);
            yy1 yy1;
            if (b) {
                yy1 = this.mStartValues;
            }
            else {
                yy1 = this.mEndValues;
            }
            b(yy1, view, xy1);
        }
        if (view instanceof ViewGroup) {
            final ArrayList<Integer> mTargetIdChildExcludes = this.mTargetIdChildExcludes;
            if (mTargetIdChildExcludes != null && mTargetIdChildExcludes.contains(id)) {
                return;
            }
            final ArrayList<View> mTargetChildExcludes = this.mTargetChildExcludes;
            if (mTargetChildExcludes != null && mTargetChildExcludes.contains(view)) {
                return;
            }
            final ArrayList<Class<?>> mTargetTypeChildExcludes = this.mTargetTypeChildExcludes;
            if (mTargetTypeChildExcludes != null) {
                for (int size2 = mTargetTypeChildExcludes.size(), j = 0; j < size2; ++j) {
                    if (this.mTargetTypeChildExcludes.get(j).isInstance(view)) {
                        return;
                    }
                }
            }
            final ViewGroup viewGroup = (ViewGroup)view;
            for (int k = n; k < viewGroup.getChildCount(); ++k) {
                this.e(viewGroup.getChildAt(k), b);
            }
        }
    }
    
    public void end() {
        final int mNumInstances = this.mNumInstances - 1;
        this.mNumInstances = mNumInstances;
        if (mNumInstances == 0) {
            final ArrayList<g> mListeners = this.mListeners;
            if (mListeners != null && mListeners.size() > 0) {
                final ArrayList list = (ArrayList)this.mListeners.clone();
                for (int size = list.size(), i = 0; i < size; ++i) {
                    ((g)list.get(i)).onTransitionEnd(this);
                }
            }
            for (int j = 0; j < this.mStartValues.c.m(); ++j) {
                final View view = (View)this.mStartValues.c.n(j);
                if (view != null) {
                    o32.A0(view, false);
                }
            }
            for (int k = 0; k < this.mEndValues.c.m(); ++k) {
                final View view2 = (View)this.mEndValues.c.n(k);
                if (view2 != null) {
                    o32.A0(view2, false);
                }
            }
            this.mEnded = true;
        }
    }
    
    public qy1 excludeChildren(final int n, final boolean b) {
        this.mTargetIdChildExcludes = this.f(this.mTargetIdChildExcludes, n, b);
        return this;
    }
    
    public qy1 excludeChildren(final View view, final boolean b) {
        this.mTargetChildExcludes = this.j(this.mTargetChildExcludes, view, b);
        return this;
    }
    
    public qy1 excludeChildren(final Class<?> clazz, final boolean b) {
        this.mTargetTypeChildExcludes = this.i(this.mTargetTypeChildExcludes, clazz, b);
        return this;
    }
    
    public qy1 excludeTarget(final int n, final boolean b) {
        this.mTargetIdExcludes = this.f(this.mTargetIdExcludes, n, b);
        return this;
    }
    
    public qy1 excludeTarget(final View view, final boolean b) {
        this.mTargetExcludes = this.j(this.mTargetExcludes, view, b);
        return this;
    }
    
    public qy1 excludeTarget(final Class<?> clazz, final boolean b) {
        this.mTargetTypeExcludes = this.i(this.mTargetTypeExcludes, clazz, b);
        return this;
    }
    
    public qy1 excludeTarget(final String s, final boolean b) {
        this.mTargetNameExcludes = h(this.mTargetNameExcludes, s, b);
        return this;
    }
    
    public final ArrayList f(final ArrayList list, final int i, final boolean b) {
        ArrayList list2 = list;
        if (i > 0) {
            final Integer value = i;
            if (b) {
                list2 = e.a(list, value);
            }
            else {
                list2 = e.b(list, value);
            }
        }
        return list2;
    }
    
    public void forceToEnd(final ViewGroup viewGroup) {
        final r8 k = k();
        int i = k.size();
        if (viewGroup != null) {
            if (i != 0) {
                final g62 d = t42.d((View)viewGroup);
                final r8 r8 = new r8(k);
                k.clear();
                --i;
                while (i >= 0) {
                    final d d2 = (d)r8.m(i);
                    if (d2.a != null && d != null && d.equals(d2.d)) {
                        ((Animator)r8.i(i)).end();
                    }
                    --i;
                }
            }
        }
    }
    
    public long getDuration() {
        return this.mDuration;
    }
    
    public Rect getEpicenter() {
        final f mEpicenterCallback = this.mEpicenterCallback;
        if (mEpicenterCallback == null) {
            return null;
        }
        return mEpicenterCallback.a(this);
    }
    
    public f getEpicenterCallback() {
        return this.mEpicenterCallback;
    }
    
    public TimeInterpolator getInterpolator() {
        return this.mInterpolator;
    }
    
    public xy1 getMatchedTransitionValues(final View view, final boolean b) {
        final uy1 mParent = this.mParent;
        if (mParent != null) {
            return mParent.getMatchedTransitionValues(view, b);
        }
        ArrayList<xy1> list;
        if (b) {
            list = this.mStartValuesList;
        }
        else {
            list = this.mEndValuesList;
        }
        final xy1 xy1 = null;
        if (list == null) {
            return null;
        }
        while (true) {
            for (int size = list.size(), i = 0; i < size; ++i) {
                final xy1 xy2 = list.get(i);
                if (xy2 == null) {
                    return null;
                }
                if (xy2.b == view) {
                    xy1 xy3 = xy1;
                    if (i >= 0) {
                        ArrayList<xy1> list2;
                        if (b) {
                            list2 = this.mEndValuesList;
                        }
                        else {
                            list2 = this.mStartValuesList;
                        }
                        xy3 = list2.get(i);
                    }
                    return xy3;
                }
            }
            int i = -1;
            continue;
        }
    }
    
    public String getName() {
        return this.mName;
    }
    
    public g31 getPathMotion() {
        return this.mPathMotion;
    }
    
    public ty1 getPropagation() {
        return null;
    }
    
    public long getStartDelay() {
        return this.mStartDelay;
    }
    
    public List<Integer> getTargetIds() {
        return this.mTargetIds;
    }
    
    public List<String> getTargetNames() {
        return this.mTargetNames;
    }
    
    public List<Class<?>> getTargetTypes() {
        return this.mTargetTypes;
    }
    
    public List<View> getTargets() {
        return this.mTargets;
    }
    
    public String[] getTransitionProperties() {
        return null;
    }
    
    public xy1 getTransitionValues(final View view, final boolean b) {
        final uy1 mParent = this.mParent;
        if (mParent != null) {
            return mParent.getTransitionValues(view, b);
        }
        yy1 yy1;
        if (b) {
            yy1 = this.mStartValues;
        }
        else {
            yy1 = this.mEndValues;
        }
        return (xy1)yy1.a.get(view);
    }
    
    public final ArrayList i(final ArrayList list, final Class clazz, final boolean b) {
        ArrayList list2 = list;
        if (clazz != null) {
            if (b) {
                list2 = e.a(list, clazz);
            }
            else {
                list2 = e.b(list, clazz);
            }
        }
        return list2;
    }
    
    public boolean isTransitionRequired(final xy1 xy1, final xy1 xy2) {
        boolean b2;
        final boolean b = b2 = false;
        if (xy1 != null) {
            b2 = b;
            if (xy2 != null) {
                final String[] transitionProperties = this.getTransitionProperties();
                if (transitionProperties != null) {
                    final int length = transitionProperties.length;
                    int n = 0;
                    while (true) {
                        b2 = b;
                        if (n >= length) {
                            return b2;
                        }
                        if (m(xy1, xy2, transitionProperties[n])) {
                            break;
                        }
                        ++n;
                    }
                }
                else {
                    final Iterator iterator = xy1.a.keySet().iterator();
                    do {
                        b2 = b;
                        if (iterator.hasNext()) {
                            continue;
                        }
                        return b2;
                    } while (!m(xy1, xy2, (String)iterator.next()));
                }
                b2 = true;
            }
        }
        return b2;
    }
    
    public boolean isValidTarget(final View view) {
        final int id = view.getId();
        final ArrayList<Integer> mTargetIdExcludes = this.mTargetIdExcludes;
        if (mTargetIdExcludes != null && mTargetIdExcludes.contains(id)) {
            return false;
        }
        final ArrayList<View> mTargetExcludes = this.mTargetExcludes;
        if (mTargetExcludes != null && mTargetExcludes.contains(view)) {
            return false;
        }
        final ArrayList<Class<?>> mTargetTypeExcludes = this.mTargetTypeExcludes;
        if (mTargetTypeExcludes != null) {
            for (int size = mTargetTypeExcludes.size(), i = 0; i < size; ++i) {
                if (this.mTargetTypeExcludes.get(i).isInstance(view)) {
                    return false;
                }
            }
        }
        if (this.mTargetNameExcludes != null && o32.J(view) != null && this.mTargetNameExcludes.contains(o32.J(view))) {
            return false;
        }
        if (this.mTargetIds.size() == 0 && this.mTargets.size() == 0) {
            final ArrayList<Class<?>> mTargetTypes = this.mTargetTypes;
            if (mTargetTypes == null || mTargetTypes.isEmpty()) {
                final ArrayList<String> mTargetNames = this.mTargetNames;
                if (mTargetNames == null || mTargetNames.isEmpty()) {
                    return true;
                }
            }
        }
        if (this.mTargetIds.contains(id) || this.mTargets.contains(view)) {
            return true;
        }
        final ArrayList<String> mTargetNames2 = this.mTargetNames;
        if (mTargetNames2 != null && mTargetNames2.contains(o32.J(view))) {
            return true;
        }
        if (this.mTargetTypes != null) {
            for (int j = 0; j < this.mTargetTypes.size(); ++j) {
                if (this.mTargetTypes.get(j).isInstance(view)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public final ArrayList j(final ArrayList list, final View view, final boolean b) {
        ArrayList list2 = list;
        if (view != null) {
            if (b) {
                list2 = e.a(list, view);
            }
            else {
                list2 = e.b(list, view);
            }
        }
        return list2;
    }
    
    public final void n(final r8 r8, final r8 r9, final SparseArray sparseArray, final SparseArray sparseArray2) {
        for (int size = sparseArray.size(), i = 0; i < size; ++i) {
            final View view = (View)sparseArray.valueAt(i);
            if (view != null && this.isValidTarget(view)) {
                final View view2 = (View)sparseArray2.get(sparseArray.keyAt(i));
                if (view2 != null && this.isValidTarget(view2)) {
                    final xy1 e = (xy1)r8.get(view);
                    final xy1 e2 = (xy1)r9.get(view2);
                    if (e != null && e2 != null) {
                        this.mStartValuesList.add(e);
                        this.mEndValuesList.add(e2);
                        r8.remove(view);
                        r9.remove(view2);
                    }
                }
            }
        }
    }
    
    public final void p(final r8 r8, final r8 r9) {
        for (int i = r8.size() - 1; i >= 0; --i) {
            final View view = (View)r8.i(i);
            if (view != null && this.isValidTarget(view)) {
                final xy1 e = (xy1)r9.remove(view);
                if (e != null && this.isValidTarget(e.b)) {
                    this.mStartValuesList.add((xy1)r8.k(i));
                    this.mEndValuesList.add(e);
                }
            }
        }
    }
    
    public void pause(final View view) {
        if (!this.mEnded) {
            final r8 k = k();
            int i = k.size();
            final g62 d = t42.d(view);
            --i;
            while (i >= 0) {
                final d d2 = (d)k.m(i);
                if (d2.a != null && d.equals(d2.d)) {
                    d5.b((Animator)k.i(i));
                }
                --i;
            }
            final ArrayList<g> mListeners = this.mListeners;
            if (mListeners != null && mListeners.size() > 0) {
                final ArrayList list = (ArrayList)this.mListeners.clone();
                for (int size = list.size(), j = 0; j < size; ++j) {
                    ((g)list.get(j)).onTransitionPause(this);
                }
            }
            this.mPaused = true;
        }
    }
    
    public void playTransition(final ViewGroup viewGroup) {
        this.mStartValuesList = new ArrayList<xy1>();
        this.mEndValuesList = new ArrayList<xy1>();
        this.s(this.mStartValues, this.mEndValues);
        final r8 k = k();
        int i = k.size();
        final g62 d = t42.d((View)viewGroup);
        --i;
        while (i >= 0) {
            final Animator animator = (Animator)k.i(i);
            if (animator != null) {
                final d d2 = (d)k.get(animator);
                if (d2 != null && d2.a != null && d.equals(d2.d)) {
                    final xy1 c = d2.c;
                    final View a = d2.a;
                    final xy1 transitionValues = this.getTransitionValues(a, true);
                    xy1 matchedTransitionValues;
                    final xy1 xy1 = matchedTransitionValues = this.getMatchedTransitionValues(a, (boolean)(1 != 0));
                    if (transitionValues == null && (matchedTransitionValues = xy1) == null) {
                        matchedTransitionValues = (xy1)this.mEndValues.a.get(a);
                    }
                    if ((transitionValues != null || matchedTransitionValues != null) && d2.e.isTransitionRequired(c, matchedTransitionValues)) {
                        if (!animator.isRunning() && !animator.isStarted()) {
                            k.remove(animator);
                        }
                        else {
                            animator.cancel();
                        }
                    }
                }
            }
            --i;
        }
        this.createAnimators(viewGroup, this.mStartValues, this.mEndValues, this.mStartValuesList, this.mEndValuesList);
        this.runAnimators();
    }
    
    public final void q(final r8 r8, final r8 r9, final hm0 hm0, final hm0 hm2) {
        for (int m = hm0.m(), i = 0; i < m; ++i) {
            final View view = (View)hm0.n(i);
            if (view != null && this.isValidTarget(view)) {
                final View view2 = (View)hm2.e(hm0.i(i));
                if (view2 != null && this.isValidTarget(view2)) {
                    final xy1 e = (xy1)r8.get(view);
                    final xy1 e2 = (xy1)r9.get(view2);
                    if (e != null && e2 != null) {
                        this.mStartValuesList.add(e);
                        this.mEndValuesList.add(e2);
                        r8.remove(view);
                        r9.remove(view2);
                    }
                }
            }
        }
    }
    
    public final void r(final r8 r8, final r8 r9, final r8 r10, final r8 r11) {
        for (int size = r10.size(), i = 0; i < size; ++i) {
            final View view = (View)r10.m(i);
            if (view != null && this.isValidTarget(view)) {
                final View view2 = (View)r11.get(r10.i(i));
                if (view2 != null && this.isValidTarget(view2)) {
                    final xy1 e = (xy1)r8.get(view);
                    final xy1 e2 = (xy1)r9.get(view2);
                    if (e != null && e2 != null) {
                        this.mStartValuesList.add(e);
                        this.mEndValuesList.add(e2);
                        r8.remove(view);
                        r9.remove(view2);
                    }
                }
            }
        }
    }
    
    public qy1 removeListener(final g o) {
        final ArrayList<g> mListeners = this.mListeners;
        if (mListeners == null) {
            return this;
        }
        mListeners.remove(o);
        if (this.mListeners.size() == 0) {
            this.mListeners = null;
        }
        return this;
    }
    
    public qy1 removeTarget(final int i) {
        if (i != 0) {
            this.mTargetIds.remove((Object)i);
        }
        return this;
    }
    
    public qy1 removeTarget(final View o) {
        this.mTargets.remove(o);
        return this;
    }
    
    public qy1 removeTarget(final Class<?> o) {
        final ArrayList<Class<?>> mTargetTypes = this.mTargetTypes;
        if (mTargetTypes != null) {
            mTargetTypes.remove(o);
        }
        return this;
    }
    
    public qy1 removeTarget(final String o) {
        final ArrayList<String> mTargetNames = this.mTargetNames;
        if (mTargetNames != null) {
            mTargetNames.remove(o);
        }
        return this;
    }
    
    public void resume(final View view) {
        if (this.mPaused) {
            if (!this.mEnded) {
                final r8 k = k();
                int i = k.size();
                final g62 d = t42.d(view);
                --i;
                while (i >= 0) {
                    final d d2 = (d)k.m(i);
                    if (d2.a != null && d.equals(d2.d)) {
                        d5.c((Animator)k.i(i));
                    }
                    --i;
                }
                final ArrayList<g> mListeners = this.mListeners;
                if (mListeners != null && mListeners.size() > 0) {
                    final ArrayList list = (ArrayList)this.mListeners.clone();
                    for (int size = list.size(), j = 0; j < size; ++j) {
                        ((g)list.get(j)).onTransitionResume(this);
                    }
                }
            }
            this.mPaused = false;
        }
    }
    
    public void runAnimators() {
        this.start();
        final r8 k = k();
        for (final Animator animator : this.mAnimators) {
            if (k.containsKey(animator)) {
                this.start();
                this.t(animator, k);
            }
        }
        this.mAnimators.clear();
        this.end();
    }
    
    public final void s(final yy1 yy1, final yy1 yy2) {
        final r8 r8 = new r8(yy1.a);
        final r8 r9 = new r8(yy2.a);
        int n = 0;
        while (true) {
            final int[] mMatchOrder = this.mMatchOrder;
            if (n >= mMatchOrder.length) {
                break;
            }
            final int n2 = mMatchOrder[n];
            if (n2 != 1) {
                if (n2 != 2) {
                    if (n2 != 3) {
                        if (n2 == 4) {
                            this.q(r8, r9, yy1.c, yy2.c);
                        }
                    }
                    else {
                        this.n(r8, r9, yy1.b, yy2.b);
                    }
                }
                else {
                    this.r(r8, r9, yy1.d, yy2.d);
                }
            }
            else {
                this.p(r8, r9);
            }
            ++n;
        }
        this.a(r8, r9);
    }
    
    public void setCanRemoveViews(final boolean mCanRemoveViews) {
        this.mCanRemoveViews = mCanRemoveViews;
    }
    
    public qy1 setDuration(final long mDuration) {
        this.mDuration = mDuration;
        return this;
    }
    
    public void setEpicenterCallback(final f mEpicenterCallback) {
        this.mEpicenterCallback = mEpicenterCallback;
    }
    
    public qy1 setInterpolator(final TimeInterpolator mInterpolator) {
        this.mInterpolator = mInterpolator;
        return this;
    }
    
    public void setMatchOrder(final int... array) {
        if (array != null && array.length != 0) {
            for (int i = 0; i < array.length; ++i) {
                if (!l(array[i])) {
                    throw new IllegalArgumentException("matches contains invalid value");
                }
                if (d(array, i)) {
                    throw new IllegalArgumentException("matches contains a duplicate value");
                }
            }
            this.mMatchOrder = array.clone();
        }
        else {
            this.mMatchOrder = qy1.DEFAULT_MATCH_ORDER;
        }
    }
    
    public void setPathMotion(final g31 g31) {
        g31 straight_PATH_MOTION = g31;
        if (g31 == null) {
            straight_PATH_MOTION = qy1.STRAIGHT_PATH_MOTION;
        }
        this.mPathMotion = straight_PATH_MOTION;
    }
    
    public void setPropagation(final ty1 ty1) {
    }
    
    public qy1 setSceneRoot(final ViewGroup mSceneRoot) {
        this.mSceneRoot = mSceneRoot;
        return this;
    }
    
    public qy1 setStartDelay(final long mStartDelay) {
        this.mStartDelay = mStartDelay;
        return this;
    }
    
    public void start() {
        if (this.mNumInstances == 0) {
            final ArrayList<g> mListeners = this.mListeners;
            if (mListeners != null && mListeners.size() > 0) {
                final ArrayList list = (ArrayList)this.mListeners.clone();
                for (int size = list.size(), i = 0; i < size; ++i) {
                    ((g)list.get(i)).onTransitionStart(this);
                }
            }
            this.mEnded = false;
        }
        ++this.mNumInstances;
    }
    
    public final void t(final Animator animator, final r8 r8) {
        if (animator != null) {
            animator.addListener((Animator$AnimatorListener)new AnimatorListenerAdapter(this, r8) {
                public final r8 a;
                public final qy1 b;
                
                public void onAnimationEnd(final Animator o) {
                    this.a.remove(o);
                    this.b.mCurrentAnimators.remove(o);
                }
                
                public void onAnimationStart(final Animator e) {
                    this.b.mCurrentAnimators.add(e);
                }
            });
            this.animate(animator);
        }
    }
    
    @Override
    public String toString() {
        return this.toString("");
    }
    
    public String toString(String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(this.getClass().getSimpleName());
        sb.append("@");
        sb.append(Integer.toHexString(this.hashCode()));
        sb.append(": ");
        String str2;
        str = (str2 = sb.toString());
        if (this.mDuration != -1L) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append("dur(");
            sb2.append(this.mDuration);
            sb2.append(") ");
            str2 = sb2.toString();
        }
        str = str2;
        if (this.mStartDelay != -1L) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(str2);
            sb3.append("dly(");
            sb3.append(this.mStartDelay);
            sb3.append(") ");
            str = sb3.toString();
        }
        String string = str;
        if (this.mInterpolator != null) {
            final StringBuilder sb4 = new StringBuilder();
            sb4.append(str);
            sb4.append("interp(");
            sb4.append(this.mInterpolator);
            sb4.append(") ");
            string = sb4.toString();
        }
        if (this.mTargetIds.size() <= 0) {
            str = string;
            if (this.mTargets.size() <= 0) {
                return str;
            }
        }
        final StringBuilder sb5 = new StringBuilder();
        sb5.append(string);
        sb5.append("tgts(");
        str = sb5.toString();
        final int size = this.mTargetIds.size();
        final int n = 0;
        String s = str;
        if (size > 0) {
            int index = 0;
            while (true) {
                s = str;
                if (index >= this.mTargetIds.size()) {
                    break;
                }
                String string2 = str;
                if (index > 0) {
                    final StringBuilder sb6 = new StringBuilder();
                    sb6.append(str);
                    sb6.append(", ");
                    string2 = sb6.toString();
                }
                final StringBuilder sb7 = new StringBuilder();
                sb7.append(string2);
                sb7.append(this.mTargetIds.get(index));
                str = sb7.toString();
                ++index;
            }
        }
        String str3 = s;
        if (this.mTargets.size() > 0) {
            str = s;
            int index2 = n;
            while (true) {
                str3 = str;
                if (index2 >= this.mTargets.size()) {
                    break;
                }
                String string3 = str;
                if (index2 > 0) {
                    final StringBuilder sb8 = new StringBuilder();
                    sb8.append(str);
                    sb8.append(", ");
                    string3 = sb8.toString();
                }
                final StringBuilder sb9 = new StringBuilder();
                sb9.append(string3);
                sb9.append(this.mTargets.get(index2));
                str = sb9.toString();
                ++index2;
            }
        }
        final StringBuilder sb10 = new StringBuilder();
        sb10.append(str3);
        sb10.append(")");
        str = sb10.toString();
        return str;
    }
    
    public static class d
    {
        public View a;
        public String b;
        public xy1 c;
        public g62 d;
        public qy1 e;
        
        public d(final View a, final String b, final qy1 e, final g62 d, final xy1 c) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
        }
    }
    
    public abstract static class e
    {
        public static ArrayList a(final ArrayList list, final Object o) {
            ArrayList list2 = list;
            if (list == null) {
                list2 = new ArrayList();
            }
            if (!list2.contains(o)) {
                list2.add(o);
            }
            return list2;
        }
        
        public static ArrayList b(final ArrayList list, final Object o) {
            ArrayList list2 = list;
            if (list != null) {
                list.remove(o);
                list2 = list;
                if (list.isEmpty()) {
                    list2 = null;
                }
            }
            return list2;
        }
    }
    
    public abstract static class f
    {
        public abstract Rect a(final qy1 p0);
    }
    
    public interface g
    {
        void onTransitionCancel(final qy1 p0);
        
        void onTransitionEnd(final qy1 p0);
        
        void onTransitionPause(final qy1 p0);
        
        void onTransitionResume(final qy1 p0);
        
        void onTransitionStart(final qy1 p0);
    }
}

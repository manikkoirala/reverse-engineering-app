import java.io.ByteArrayOutputStream;

// 
// Decompiled by Procyon v0.6.0
// 

public class v51 extends ByteArrayOutputStream
{
    public final ld a;
    
    public v51(final ld a, final int a2) {
        this.a = a;
        super.buf = a.a(Math.max(a2, 256));
    }
    
    public final void a(final int n) {
        final int count = super.count;
        if (count + n <= super.buf.length) {
            return;
        }
        final byte[] a = this.a.a((count + n) * 2);
        System.arraycopy(super.buf, 0, a, 0, super.count);
        this.a.b(super.buf);
        super.buf = a;
    }
    
    @Override
    public void close() {
        this.a.b(super.buf);
        super.buf = null;
        super.close();
    }
    
    public void finalize() {
        this.a.b(super.buf);
    }
    
    @Override
    public void write(final int b) {
        synchronized (this) {
            this.a(1);
            super.write(b);
        }
    }
    
    @Override
    public void write(final byte[] b, final int off, final int len) {
        synchronized (this) {
            this.a(len);
            super.write(b, off, len);
        }
    }
}

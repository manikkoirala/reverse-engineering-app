import java.util.concurrent.TimeUnit;

// 
// Decompiled by Procyon v0.6.0
// 

public class de1
{
    public static final long d;
    public static final long e;
    public final u22 a;
    public long b;
    public int c;
    
    static {
        d = TimeUnit.HOURS.toMillis(24L);
        e = TimeUnit.MINUTES.toMillis(30L);
    }
    
    public de1() {
        this.a = u22.c();
    }
    
    public static boolean c(final int n) {
        return n == 429 || (n >= 500 && n < 600);
    }
    
    public static boolean d(final int n) {
        return (n >= 200 && n < 300) || n == 401 || n == 404;
    }
    
    public final long a(final int n) {
        synchronized (this) {
            if (!c(n)) {
                return de1.d;
            }
            return (long)Math.min(Math.pow(2.0, this.c) + this.a.e(), (double)de1.e);
        }
    }
    
    public boolean b() {
        synchronized (this) {
            return this.c == 0 || this.a.a() > this.b;
        }
    }
    
    public final void e() {
        synchronized (this) {
            this.c = 0;
        }
    }
    
    public void f(final int n) {
        synchronized (this) {
            if (d(n)) {
                this.e();
                return;
            }
            ++this.c;
            this.b = this.a.a() + this.a(n);
        }
    }
}

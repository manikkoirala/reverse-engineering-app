import java.math.RoundingMode;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class uf0
{
    public static int a(int abs, int a, final RoundingMode roundingMode) {
        if (a == 0) {
            throw new ArithmeticException("/ by zero");
        }
        final int n = abs / a;
        final int a2 = abs - a * n;
        if (a2 == 0) {
            return n;
        }
        final int n2 = 1;
        final int n3 = (abs ^ a) >> 31 | 0x1;
        abs = n2;
        Label_0200: {
            switch (uf0$a.a[roundingMode.ordinal()]) {
                default: {
                    throw new AssertionError();
                }
                case 6:
                case 7:
                case 8: {
                    abs = Math.abs(a2);
                    abs -= Math.abs(a) - abs;
                    if (abs == 0) {
                        abs = n2;
                        if (roundingMode == RoundingMode.HALF_UP) {
                            break Label_0200;
                        }
                        if (roundingMode == RoundingMode.HALF_EVEN) {
                            abs = 1;
                        }
                        else {
                            abs = 0;
                        }
                        if ((n & 0x1) != 0x0) {
                            a = 1;
                        }
                        else {
                            a = 0;
                        }
                        if ((abs & a) != 0x0) {
                            abs = n2;
                            break Label_0200;
                        }
                        break Label_0200;
                    }
                    else {
                        if (abs > 0) {
                            abs = n2;
                            break Label_0200;
                        }
                        break Label_0200;
                    }
                    break;
                }
                case 5: {
                    if (n3 < 0) {
                        abs = n2;
                        break Label_0200;
                    }
                    break Label_0200;
                }
                case 4: {
                    if (n3 > 0) {
                        abs = n2;
                        break Label_0200;
                    }
                    break Label_0200;
                }
                case 1:
                case 2: {
                    abs = 0;
                }
                case 3: {
                    a = n;
                    if (abs != 0) {
                        a = n + n3;
                    }
                    return a;
                }
            }
        }
    }
}

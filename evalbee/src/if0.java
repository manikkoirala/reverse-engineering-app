import android.os.BaseBundle;
import android.content.ClipData;
import android.content.ClipData$Item;
import android.util.Log;
import android.os.Parcelable;
import android.content.ClipDescription;
import android.net.Uri;
import android.os.ResultReceiver;
import android.text.TextUtils;
import android.view.inputmethod.InputContentInfo;
import android.view.inputmethod.InputConnectionWrapper;
import android.os.Build$VERSION;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.os.Bundle;
import android.view.View;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class if0
{
    public static c b(final View view) {
        l71.g(view);
        return (c)new hf0(view);
    }
    
    public static InputConnection c(final View view, final InputConnection inputConnection, final EditorInfo editorInfo) {
        return d(inputConnection, editorInfo, b(view));
    }
    
    public static InputConnection d(final InputConnection inputConnection, final EditorInfo editorInfo, final c c) {
        c11.d(inputConnection, "inputConnection must be non-null");
        c11.d(editorInfo, "editorInfo must be non-null");
        c11.d(c, "onCommitContentListener must be non-null");
        if (Build$VERSION.SDK_INT >= 25) {
            return (InputConnection)new InputConnectionWrapper(inputConnection, false, c) {
                public final c a;
                
                public boolean commitContent(final InputContentInfo inputContentInfo, final int n, final Bundle bundle) {
                    return this.a.a(jf0.f(inputContentInfo), n, bundle) || super.commitContent(inputContentInfo, n, bundle);
                }
            };
        }
        if (ew.a(editorInfo).length == 0) {
            return inputConnection;
        }
        return (InputConnection)new InputConnectionWrapper(inputConnection, false, c) {
            public final c a;
            
            public boolean performPrivateCommand(final String s, final Bundle bundle) {
                return if0.e(s, bundle, this.a) || super.performPrivateCommand(s, bundle);
            }
        };
    }
    
    public static boolean e(String s, final Bundle bundle, final c c) {
        final int n = 0;
        if (bundle == null) {
            return false;
        }
        boolean b;
        if (TextUtils.equals((CharSequence)"androidx.core.view.inputmethod.InputConnectionCompat.COMMIT_CONTENT", (CharSequence)s)) {
            b = false;
        }
        else {
            if (!TextUtils.equals((CharSequence)"android.support.v13.view.inputmethod.InputConnectionCompat.COMMIT_CONTENT", (CharSequence)s)) {
                return false;
            }
            b = true;
        }
        if (b) {
            s = "android.support.v13.view.inputmethod.InputConnectionCompat.CONTENT_RESULT_RECEIVER";
        }
        else {
            s = "androidx.core.view.inputmethod.InputConnectionCompat.CONTENT_RESULT_RECEIVER";
        }
        ResultReceiver resultReceiver2;
        try {
            final ResultReceiver resultReceiver = (ResultReceiver)bundle.getParcelable(s);
            if (b) {
                s = "android.support.v13.view.inputmethod.InputConnectionCompat.CONTENT_URI";
            }
            else {
                s = "androidx.core.view.inputmethod.InputConnectionCompat.CONTENT_URI";
            }
            try {
                final Uri uri = (Uri)bundle.getParcelable(s);
                if (b) {
                    s = "android.support.v13.view.inputmethod.InputConnectionCompat.CONTENT_DESCRIPTION";
                }
                else {
                    s = "androidx.core.view.inputmethod.InputConnectionCompat.CONTENT_DESCRIPTION";
                }
                final ClipDescription clipDescription = (ClipDescription)bundle.getParcelable(s);
                if (b) {
                    s = "android.support.v13.view.inputmethod.InputConnectionCompat.CONTENT_LINK_URI";
                }
                else {
                    s = "androidx.core.view.inputmethod.InputConnectionCompat.CONTENT_LINK_URI";
                }
                final Uri uri2 = (Uri)bundle.getParcelable(s);
                if (b) {
                    s = "android.support.v13.view.inputmethod.InputConnectionCompat.CONTENT_FLAGS";
                }
                else {
                    s = "androidx.core.view.inputmethod.InputConnectionCompat.CONTENT_FLAGS";
                }
                final int int1 = ((BaseBundle)bundle).getInt(s);
                if (b) {
                    s = "android.support.v13.view.inputmethod.InputConnectionCompat.CONTENT_OPTS";
                }
                else {
                    s = "androidx.core.view.inputmethod.InputConnectionCompat.CONTENT_OPTS";
                }
                final Bundle bundle2 = (Bundle)bundle.getParcelable(s);
                int a = n;
                if (uri != null) {
                    a = n;
                    if (clipDescription != null) {
                        a = (c.a(new jf0(uri, clipDescription, uri2), int1, bundle2) ? 1 : 0);
                    }
                }
                if (resultReceiver != null) {
                    resultReceiver.send(a, (Bundle)null);
                }
                return a != 0;
            }
            finally {}
        }
        finally {
            resultReceiver2 = null;
        }
        if (resultReceiver2 != null) {
            resultReceiver2.send(0, (Bundle)null);
        }
    }
    
    public interface c
    {
        boolean a(final jf0 p0, final int p1, final Bundle p2);
    }
}

import android.view.ViewTreeObserver;
import android.view.View;
import android.view.View$OnAttachStateChangeListener;
import android.view.ViewTreeObserver$OnPreDrawListener;

// 
// Decompiled by Procyon v0.6.0
// 

public final class s11 implements ViewTreeObserver$OnPreDrawListener, View$OnAttachStateChangeListener
{
    public final View a;
    public ViewTreeObserver b;
    public final Runnable c;
    
    public s11(final View a, final Runnable c) {
        this.a = a;
        this.b = a.getViewTreeObserver();
        this.c = c;
    }
    
    public static s11 a(final View view, final Runnable runnable) {
        if (view == null) {
            throw new NullPointerException("view == null");
        }
        if (runnable != null) {
            final s11 s11 = new s11(view, runnable);
            view.getViewTreeObserver().addOnPreDrawListener((ViewTreeObserver$OnPreDrawListener)s11);
            view.addOnAttachStateChangeListener((View$OnAttachStateChangeListener)s11);
            return s11;
        }
        throw new NullPointerException("runnable == null");
    }
    
    public void b() {
        ViewTreeObserver viewTreeObserver;
        if (this.b.isAlive()) {
            viewTreeObserver = this.b;
        }
        else {
            viewTreeObserver = this.a.getViewTreeObserver();
        }
        viewTreeObserver.removeOnPreDrawListener((ViewTreeObserver$OnPreDrawListener)this);
        this.a.removeOnAttachStateChangeListener((View$OnAttachStateChangeListener)this);
    }
    
    public boolean onPreDraw() {
        this.b();
        this.c.run();
        return true;
    }
    
    public void onViewAttachedToWindow(final View view) {
        this.b = view.getViewTreeObserver();
    }
    
    public void onViewDetachedFromWindow(final View view) {
        this.b();
    }
}

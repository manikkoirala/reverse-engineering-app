import android.view.View;
import android.view.animation.Transformation;
import android.widget.TextView;
import android.widget.ProgressBar;
import android.view.animation.Animation;

// 
// Decompiled by Procyon v0.6.0
// 

public class c91 extends Animation
{
    public ProgressBar a;
    public TextView b;
    public float c;
    public float d;
    public float e;
    
    public c91(final ProgressBar a, final TextView b, final float c, final float d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = d * 0.7f;
    }
    
    public void applyTransformation(float n, final Transformation transformation) {
        super.applyTransformation(n, transformation);
        final float c = this.c;
        n = c + (this.d - c) * n;
        if (n > this.e) {
            final TextView b = this.b;
            if (b != null && ((View)b).getVisibility() == 8) {
                ((View)this.b).setVisibility(0);
            }
        }
        this.a.setProgress((int)n);
    }
}

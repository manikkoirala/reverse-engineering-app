import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.Executor;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class ik
{
    public static final int a = 8;
    
    public static final Executor b(final boolean b) {
        final ExecutorService fixedThreadPool = Executors.newFixedThreadPool(Math.max(2, Math.min(Runtime.getRuntime().availableProcessors() - 1, 4)), new ThreadFactory(b) {
            public final AtomicInteger a = new AtomicInteger(0);
            public final boolean b;
            
            @Override
            public Thread newThread(final Runnable target) {
                fg0.e((Object)target, "runnable");
                String str;
                if (this.b) {
                    str = "WM.task-";
                }
                else {
                    str = "androidx.work-";
                }
                final StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(this.a.incrementAndGet());
                return new Thread(target, sb.toString());
            }
        });
        fg0.d((Object)fixedThreadPool, "newFixedThreadPool(\n    \u2026)),\n        factory\n    )");
        return fixedThreadPool;
    }
    
    public static final int c() {
        return ik.a;
    }
}

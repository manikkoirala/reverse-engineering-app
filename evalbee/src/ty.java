import com.android.volley.VolleyError;
import com.android.volley.d;
import com.android.volley.Request;
import android.os.Handler;
import java.util.concurrent.Executor;

// 
// Decompiled by Procyon v0.6.0
// 

public class ty implements re1
{
    public final Executor a;
    
    public ty(final Handler handler) {
        this.a = new Executor(this, handler) {
            public final Handler a;
            public final ty b;
            
            @Override
            public void execute(final Runnable runnable) {
                this.a.post(runnable);
            }
        };
    }
    
    @Override
    public void a(final Request request, final d d) {
        this.b(request, d, null);
    }
    
    @Override
    public void b(final Request request, final d d, final Runnable runnable) {
        request.C();
        request.c("post-response");
        this.a.execute(new b(request, d, runnable));
    }
    
    @Override
    public void c(final Request request, final VolleyError volleyError) {
        request.c("post-error");
        this.a.execute(new b(request, d.a(volleyError), null));
    }
    
    public static class b implements Runnable
    {
        public final Request a;
        public final d b;
        public final Runnable c;
        
        public b(final Request a, final d b, final Runnable c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
        
        @Override
        public void run() {
            if (this.a.B()) {
                this.a.j("canceled-at-delivery");
                return;
            }
            if (this.b.b()) {
                this.a.f(this.b.a);
            }
            else {
                this.a.e(this.b.c);
            }
            if (this.b.d) {
                this.a.c("intermediate-response");
            }
            else {
                this.a.j("done");
            }
            final Runnable c = this.c;
            if (c != null) {
                c.run();
            }
        }
    }
}

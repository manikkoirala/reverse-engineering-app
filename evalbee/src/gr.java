import java.util.concurrent.TimeUnit;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.common.internal.Preconditions;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.Executor;

// 
// Decompiled by Procyon v0.6.0
// 

public class gr
{
    public final pq a;
    public final Executor b;
    public final ScheduledExecutorService c;
    public volatile ScheduledFuture d;
    public volatile long e;
    
    public gr(final pq pq, final Executor b, final ScheduledExecutorService c) {
        this.a = Preconditions.checkNotNull(pq);
        this.b = b;
        this.c = c;
        this.e = -1L;
    }
    
    public void c() {
        if (this.d != null && !this.d.isDone()) {
            this.d.cancel(false);
        }
    }
    
    public final long d() {
        if (this.e == -1L) {
            return 30L;
        }
        if (this.e * 2L < 960L) {
            return this.e * 2L;
        }
        return 960L;
    }
    
    public final void f() {
        this.a.e().addOnFailureListener(this.b, (OnFailureListener)new fr(this));
    }
    
    public void g(final long b) {
        this.c();
        this.e = -1L;
        this.d = this.c.schedule(new er(this), Math.max(0L, b), TimeUnit.MILLISECONDS);
    }
    
    public final void h() {
        this.c();
        this.e = this.d();
        this.d = this.c.schedule(new er(this), this.e, TimeUnit.SECONDS);
    }
}

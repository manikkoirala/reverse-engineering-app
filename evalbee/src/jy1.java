import android.util.Log;
import android.os.Trace;
import android.os.Build$VERSION;
import java.lang.reflect.Method;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class jy1
{
    public static long a;
    public static Method b;
    public static Method c;
    public static Method d;
    public static Method e;
    
    static {
        if (Build$VERSION.SDK_INT < 29) {
            try {
                jy1.a = Trace.class.getField("TRACE_TAG_APP").getLong(null);
                final Class<Long> type = Long.TYPE;
                jy1.b = Trace.class.getMethod("isTagEnabled", type);
                final Class<Integer> type2 = Integer.TYPE;
                jy1.c = Trace.class.getMethod("asyncTraceBegin", type, String.class, type2);
                jy1.d = Trace.class.getMethod("asyncTraceEnd", type, String.class, type2);
                jy1.e = Trace.class.getMethod("traceCounter", type, String.class, type2);
            }
            catch (final Exception ex) {
                Log.i("TraceCompat", "Unable to initialize via reflection.", (Throwable)ex);
            }
        }
    }
    
    public static void a(final String s) {
        jy1.a.a(s);
    }
    
    public static void b() {
        jy1.a.b();
    }
    
    public abstract static class a
    {
        public static void a(final String s) {
            Trace.beginSection(s);
        }
        
        public static void b() {
            Trace.endSection();
        }
    }
}

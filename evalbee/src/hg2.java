import android.text.TextUtils;
import com.google.android.gms.common.internal.Preconditions;
import java.util.Iterator;
import android.content.SharedPreferences$Editor;
import android.content.SharedPreferences;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public final class hg2
{
    public static hg2 a;
    
    static {
        hg2.a = new hg2();
    }
    
    public static SharedPreferences a(final Context context, final String s) {
        return context.getSharedPreferences(String.format("com.google.firebase.auth.internal.browserSignInSessionStore.%s", s), 0);
    }
    
    public static hg2 c() {
        return hg2.a;
    }
    
    public static void f(final SharedPreferences sharedPreferences) {
        final SharedPreferences$Editor edit = sharedPreferences.edit();
        final Iterator iterator = sharedPreferences.getAll().keySet().iterator();
        while (iterator.hasNext()) {
            edit.remove((String)iterator.next());
        }
        edit.apply();
    }
    
    public final gg2 b(final Context context, String format, String string) {
        synchronized (this) {
            Preconditions.checkNotEmpty(format);
            Preconditions.checkNotEmpty(string);
            final SharedPreferences a = a(context, format);
            format = String.format("com.google.firebase.auth.internal.EVENT_ID.%s.SESSION_ID", string);
            final String format2 = String.format("com.google.firebase.auth.internal.EVENT_ID.%s.OPERATION", string);
            final String format3 = String.format("com.google.firebase.auth.internal.EVENT_ID.%s.PROVIDER_ID", string);
            final String format4 = String.format("com.google.firebase.auth.internal.EVENT_ID.%s.FIREBASE_APP_NAME", string);
            string = a.getString(format, (String)null);
            final String string2 = a.getString(format2, (String)null);
            final String string3 = a.getString(format3, (String)null);
            final String string4 = a.getString("com.google.firebase.auth.api.gms.config.tenant.id", (String)null);
            final String string5 = a.getString(format4, (String)null);
            final SharedPreferences$Editor edit = a.edit();
            edit.remove(format);
            edit.remove(format2);
            edit.remove(format3);
            edit.remove(format4);
            edit.apply();
            if (string != null && string2 != null && string3 != null) {
                return new gg2(string, string2, string3, string4, string5);
            }
            return null;
        }
    }
    
    public final void d(final Context context, final String s, final String s2, final String s3, final String s4) {
        synchronized (this) {
            Preconditions.checkNotEmpty(s);
            Preconditions.checkNotEmpty(s2);
            final SharedPreferences a = a(context, s);
            f(a);
            final SharedPreferences$Editor edit = a.edit();
            edit.putString(String.format("com.google.firebase.auth.internal.EVENT_ID.%s.OPERATION", s2), s3);
            edit.putString(String.format("com.google.firebase.auth.internal.EVENT_ID.%s.FIREBASE_APP_NAME", s2), s4);
            edit.apply();
        }
    }
    
    public final void e(final Context context, final String s, final String s2, final String s3, final String s4, final String s5, final String s6, final String s7) {
        synchronized (this) {
            Preconditions.checkNotEmpty(s);
            Preconditions.checkNotEmpty(s2);
            Preconditions.checkNotEmpty(s3);
            Preconditions.checkNotEmpty(s7);
            final SharedPreferences a = a(context, s);
            f(a);
            final SharedPreferences$Editor edit = a.edit();
            edit.putString(String.format("com.google.firebase.auth.internal.EVENT_ID.%s.SESSION_ID", s2), s3);
            edit.putString(String.format("com.google.firebase.auth.internal.EVENT_ID.%s.OPERATION", s2), s4);
            edit.putString(String.format("com.google.firebase.auth.internal.EVENT_ID.%s.PROVIDER_ID", s2), s5);
            edit.putString(String.format("com.google.firebase.auth.internal.EVENT_ID.%s.FIREBASE_APP_NAME", s2), s7);
            edit.putString("com.google.firebase.auth.api.gms.config.tenant.id", s6);
            edit.apply();
        }
    }
    
    public final String g(final Context context, String format, String format2) {
        synchronized (this) {
            Preconditions.checkNotEmpty(format);
            Preconditions.checkNotEmpty(format2);
            final SharedPreferences a = a(context, format);
            format = String.format("com.google.firebase.auth.internal.EVENT_ID.%s.OPERATION", format2);
            final String string = a.getString(format, (String)null);
            format2 = String.format("com.google.firebase.auth.internal.EVENT_ID.%s.FIREBASE_APP_NAME", format2);
            final String string2 = a.getString(format2, (String)null);
            final SharedPreferences$Editor edit = a.edit();
            edit.remove(format);
            edit.remove(format2);
            edit.apply();
            final boolean empty = TextUtils.isEmpty((CharSequence)string);
            monitorexit(this);
            if (empty) {
                return null;
            }
            return string2;
        }
    }
}

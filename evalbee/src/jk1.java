import android.view.View$OnClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton$OnCheckedChangeListener;
import java.io.Serializable;
import com.ekodroid.omrevaluator.templateui.models.MatrixOptionPayload;
import com.ekodroid.omrevaluator.templateui.models.DecimalOptionPayload;
import com.ekodroid.omrevaluator.templateui.models.AnswerOption;
import java.util.Iterator;
import android.content.SharedPreferences;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.widget.TextView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemSelectedListener;
import android.widget.SpinnerAdapter;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import java.util.ArrayList;
import android.widget.ArrayAdapter;
import com.ekodroid.omrevaluator.templateui.models.Section2;
import android.content.Context;
import android.widget.LinearLayout;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.Spinner;

// 
// Decompiled by Procyon v0.6.0
// 

public class jk1
{
    public Spinner A;
    public CheckBox B;
    public CheckBox C;
    public CheckBox D;
    public CheckBox E;
    public EditText F;
    public LinearLayout G;
    public LinearLayout H;
    public LinearLayout I;
    public Context a;
    public Section2 b;
    public y01 c;
    public LinearLayout d;
    public ArrayAdapter e;
    public ArrayAdapter f;
    public ArrayAdapter g;
    public ArrayAdapter h;
    public ArrayAdapter i;
    public ArrayAdapter j;
    public ArrayAdapter k;
    public ArrayList l;
    public ArrayList m;
    public double n;
    public double o;
    public Integer[] p;
    public Integer[] q;
    public Integer[] r;
    public Spinner s;
    public Spinner t;
    public Spinner u;
    public Spinner v;
    public Spinner w;
    public Spinner x;
    public Spinner y;
    public Spinner z;
    
    public jk1(final Context a, final Section2 b, final String text, final Integer[] array, final ArrayList l, final ArrayList m, final y01 c) {
        this.n = 1.0;
        this.o = 0.0;
        final Integer value = 8;
        final Integer value2 = 2;
        final Integer value3 = 3;
        final Integer value4 = 1;
        final Integer value5 = 4;
        final Integer value6 = 5;
        final Integer value7 = 6;
        final Integer value8 = 7;
        final Integer value9 = 9;
        this.p = new Integer[] { value2, value3, value5, value6, value7, value8, value, value9 };
        final Integer value10 = 10;
        this.q = new Integer[] { value4, value2, value3, value5, value6, value7, value8, value, value9, value10 };
        this.r = new Integer[] { value4, value2, value3, value5, value6, value7, value8, value, value9, value10 };
        this.a = a;
        this.b = b;
        this.l = l;
        this.m = m;
        this.c = c;
        this.e = new ArrayAdapter(a, 2131493104, (Object[])gr1.a);
        this.f = new ArrayAdapter(a, 2131493104, (Object[])gr1.b);
        this.g = new ArrayAdapter(a, 2131493104, (Object[])array);
        this.h = new ArrayAdapter(a, 2131493104, (Object[])array);
        this.i = new ArrayAdapter(a, 2131493104, (Object[])this.p);
        this.j = new ArrayAdapter(a, 2131493104, (Object[])this.q);
        this.k = new ArrayAdapter(a, 2131493104, (Object[])this.r);
        final LayoutInflater layoutInflater = (LayoutInflater)a.getSystemService("layout_inflater");
        if (layoutInflater != null) {
            final LinearLayout d = (LinearLayout)layoutInflater.inflate(2131493013, (ViewGroup)null);
            this.d = d;
            this.F = (EditText)((View)d).findViewById(2131296589);
            this.s = (Spinner)((View)this.d).findViewById(2131297108);
            this.t = (Spinner)((View)this.d).findViewById(2131297110);
            this.u = (Spinner)((View)this.d).findViewById(2131297106);
            this.v = (Spinner)((View)this.d).findViewById(2131297107);
            this.w = (Spinner)((View)this.d).findViewById(2131297105);
            this.x = (Spinner)((View)this.d).findViewById(2131297111);
            this.y = (Spinner)((View)this.d).findViewById(2131297104);
            this.E = (CheckBox)((View)this.d).findViewById(2131296501);
            this.B = (CheckBox)((View)this.d).findViewById(2131296506);
            this.D = (CheckBox)((View)this.d).findViewById(2131296505);
            this.C = (CheckBox)((View)this.d).findViewById(2131296504);
            this.G = (LinearLayout)((View)this.d).findViewById(2131296774);
            this.H = (LinearLayout)((View)this.d).findViewById(2131296771);
            this.I = (LinearLayout)((View)this.d).findViewById(2131296776);
            this.A = (Spinner)((View)this.d).findViewById(2131297112);
            this.z = (Spinner)((View)this.d).findViewById(2131297109);
            this.w.setAdapter((SpinnerAdapter)this.i);
            this.s.setAdapter((SpinnerAdapter)this.g);
            this.z.setAdapter((SpinnerAdapter)this.h);
            this.t.setAdapter((SpinnerAdapter)this.e);
            this.y.setAdapter((SpinnerAdapter)this.k);
            this.x.setAdapter((SpinnerAdapter)this.j);
            this.A.setAdapter((SpinnerAdapter)this.f);
            ((TextView)this.F).setText((CharSequence)text);
            final EditText f = this.F;
            f.setSelection(((CharSequence)f.getText()).length());
            ((AdapterView)this.s).setSelection(0);
            ((AdapterView)this.t).setSelection(3);
            ((AdapterView)this.x).setSelection(3);
            ((AdapterView)this.y).setSelection(4);
            this.u(l, m);
            this.z();
            this.A();
            this.v();
            this.w();
        }
    }
    
    public static /* synthetic */ Spinner a(final jk1 jk1) {
        return jk1.z;
    }
    
    public static /* synthetic */ LinearLayout b(final jk1 jk1) {
        return jk1.I;
    }
    
    public static /* synthetic */ LinearLayout c(final jk1 jk1) {
        return jk1.G;
    }
    
    public static /* synthetic */ LinearLayout d(final jk1 jk1) {
        return jk1.H;
    }
    
    public static /* synthetic */ Spinner f(final jk1 jk1) {
        return jk1.u;
    }
    
    public static /* synthetic */ Spinner h(final jk1 jk1) {
        return jk1.v;
    }
    
    public final void A() {
        ((AdapterView)this.s).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener(this) {
            public final jk1 a;
            
            public void onItemSelected(final AdapterView adapterView, final View view, int i, final long n) {
                final int n2 = i + 1;
                final Integer[] array = new Integer[n2];
                int j;
                for (i = 0; i < n2; i = j) {
                    j = i + 1;
                    array[i] = j;
                }
                this.a.h = new ArrayAdapter(this.a.a, 2131493104, (Object[])array);
                jk1.a(this.a).setAdapter((SpinnerAdapter)this.a.h);
            }
            
            public void onNothingSelected(final AdapterView adapterView) {
            }
        });
    }
    
    public final void B() {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(this.a, 2131951953);
        final View inflate = ((LayoutInflater)this.a.getSystemService("layout_inflater")).inflate(2131492958, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        materialAlertDialogBuilder.setTitle(2131886114);
        ((TextView)inflate.findViewById(2131297248)).setText(2131886410);
        materialAlertDialogBuilder.setPositiveButton(2131886114, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this, (EditText)inflate.findViewById(2131296582)) {
            public final EditText a;
            public final jk1 b;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                if (this.a.getText().toString().trim().length() < 1) {
                    return;
                }
                final Double value = Double.valueOf(this.a.getText().toString().trim());
                if (value < 0.0) {
                    a91.G(this.b.a, 2131886266, 2131230909, 2131231086);
                    ((TextView)this.a).setText((CharSequence)"");
                    return;
                }
                if (this.b.l.contains(value)) {
                    dialogInterface.dismiss();
                    return;
                }
                final SharedPreferences sharedPreferences = this.b.a.getSharedPreferences("MyPref", 0);
                final ArrayList h = a91.h(sharedPreferences.getString("plus_mark_json", ""));
                h.add(value);
                sharedPreferences.edit().putString("plus_mark_json", new gc0().s(h)).commit();
                final jk1 b = this.b;
                b.c.a(b.b);
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.create().show();
    }
    
    public final void C() {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(this.a, 2131951953);
        final View inflate = ((LayoutInflater)this.a.getSystemService("layout_inflater")).inflate(2131492958, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        materialAlertDialogBuilder.setTitle(2131886114);
        ((TextView)inflate.findViewById(2131297248)).setText(2131886411);
        materialAlertDialogBuilder.setPositiveButton(2131886114, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this, (EditText)inflate.findViewById(2131296582)) {
            public final EditText a;
            public final jk1 b;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                if (this.a.getText().toString().trim().length() < 1) {
                    return;
                }
                final Double value = Double.valueOf(this.a.getText().toString().trim());
                if (value > 0.0) {
                    a91.G(this.b.a, 2131886265, 2131230909, 2131231086);
                    ((TextView)this.a).setText((CharSequence)"");
                    return;
                }
                if (this.b.m.contains(value)) {
                    dialogInterface.dismiss();
                    return;
                }
                final SharedPreferences sharedPreferences = this.b.a.getSharedPreferences("MyPref", 0);
                final ArrayList h = a91.h(sharedPreferences.getString("minus_mark_json", ""));
                h.add(value);
                sharedPreferences.edit().putString("minus_mark_json", new gc0().s(h)).commit();
                final jk1 b = this.b;
                b.c.a(b.b);
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.create().show();
    }
    
    public final ArrayList i(final ArrayList list) {
        final ArrayList list2 = new ArrayList();
        list2.add(ok.b);
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            list2.add(((Double)iterator.next()).toString());
        }
        return list2;
    }
    
    public int j() {
        return (int)((AdapterView)this.z).getSelectedItem();
    }
    
    public int k() {
        return (int)((AdapterView)this.s).getSelectedItem();
    }
    
    public String l() {
        return (String)((AdapterView)this.A).getSelectedItem();
    }
    
    public String m() {
        gc0 gc0;
        Serializable s;
        if (this.p() == AnswerOption.AnswerOptionType.DECIMAL) {
            gc0 = new gc0();
            s = new DecimalOptionPayload(((CompoundButton)this.C).isChecked(), ((AdapterView)this.w).getSelectedItemPosition() + 2, ((CompoundButton)this.E).isChecked());
        }
        else {
            if (this.p() != AnswerOption.AnswerOptionType.MATRIX) {
                return null;
            }
            gc0 = new gc0();
            s = new MatrixOptionPayload(((AdapterView)this.x).getSelectedItemPosition() + 1, ((AdapterView)this.y).getSelectedItemPosition() + 1);
        }
        return gc0.s(s);
    }
    
    public String n() {
        return this.F.getText().toString();
    }
    
    public LinearLayout o() {
        return this.d;
    }
    
    public AnswerOption.AnswerOptionType p() {
        switch (((AdapterView)this.t).getSelectedItemPosition()) {
            default: {
                return AnswerOption.AnswerOptionType.FOUROPTION;
            }
            case 8: {
                return AnswerOption.AnswerOptionType.DECIMAL;
            }
            case 7: {
                return AnswerOption.AnswerOptionType.MATRIX;
            }
            case 6: {
                return AnswerOption.AnswerOptionType.TENOPTION;
            }
            case 5: {
                return AnswerOption.AnswerOptionType.EIGHTOPTION;
            }
            case 4: {
                return AnswerOption.AnswerOptionType.SIXOPTION;
            }
            case 3: {
                return AnswerOption.AnswerOptionType.FIVEOPTION;
            }
            case 2: {
                return AnswerOption.AnswerOptionType.FOUROPTION;
            }
            case 1: {
                return AnswerOption.AnswerOptionType.THREEOPTION;
            }
            case 0: {
                return AnswerOption.AnswerOptionType.TRUEORFALSE;
            }
        }
    }
    
    public double q() {
        return this.m.get(((AdapterView)this.v).getSelectedItemPosition() - 1);
    }
    
    public double r() {
        return this.l.get(((AdapterView)this.u).getSelectedItemPosition() - 1);
    }
    
    public boolean s() {
        return ((CompoundButton)this.B).isChecked();
    }
    
    public boolean t() {
        return ((CompoundButton)this.D).isChecked();
    }
    
    public void u(final ArrayList l, final ArrayList m) {
        this.l = l;
        this.m = m;
        final p3 adapter = new p3(this.a, this.i(l));
        final p3 adapter2 = new p3(this.a, this.i(m));
        this.u.setAdapter((SpinnerAdapter)adapter);
        this.v.setAdapter((SpinnerAdapter)adapter2);
        final int index = l.indexOf(this.n);
        int index2 = 0;
        int index3 = index;
        if (index < 0) {
            index3 = 0;
        }
        this.n = (double)l.get(index3);
        final int index4 = m.indexOf(this.o);
        if (index4 >= 0) {
            index2 = index4;
        }
        this.o = (double)m.get(index2);
        ((AdapterView)this.u).setSelection(index3 + 1);
        ((AdapterView)this.v).setSelection(index2 + 1);
        this.x();
        this.y();
    }
    
    public final void v() {
        ((CompoundButton)this.D).setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)new CompoundButton$OnCheckedChangeListener(this) {
            public final jk1 a;
            
            public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
                final LinearLayout b2 = jk1.b(this.a);
                int visibility;
                if (b) {
                    visibility = 0;
                }
                else {
                    visibility = 8;
                }
                ((View)b2).setVisibility(visibility);
            }
        });
    }
    
    public final void w() {
        ((View)this.d).findViewById(2131296396).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final jk1 a;
            
            public void onClick(final View view) {
                xs.c(this.a.a, null, 2131886691, 2131886536, 2131886176, 0, 0, 0);
            }
        });
        ((View)this.d).findViewById(2131296395).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final jk1 a;
            
            public void onClick(final View view) {
                xs.c(this.a.a, null, 2131886685, 2131886533, 2131886176, 0, 0, 0);
            }
        });
    }
    
    public final void x() {
        ((AdapterView)this.u).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener(this) {
            public final jk1 a;
            
            public void onItemSelected(final AdapterView adapterView, final View view, final int n, final long n2) {
                final jk1 a = this.a;
                if (n == 0) {
                    a.B();
                    ((AdapterView)jk1.f(this.a)).setSelection(1);
                    final jk1 a2 = this.a;
                    a2.n = (double)a2.l.get(0);
                }
                else {
                    a.n = (double)a.l.get(n - 1);
                }
            }
            
            public void onNothingSelected(final AdapterView adapterView) {
            }
        });
    }
    
    public final void y() {
        ((AdapterView)this.v).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener(this) {
            public final jk1 a;
            
            public void onItemSelected(final AdapterView adapterView, final View view, final int n, final long n2) {
                final jk1 a = this.a;
                if (n == 0) {
                    a.C();
                    ((AdapterView)jk1.h(this.a)).setSelection(1);
                    final jk1 a2 = this.a;
                    a2.o = (double)a2.m.get(0);
                }
                else {
                    a.o = (double)a.m.get(n - 1);
                }
            }
            
            public void onNothingSelected(final AdapterView adapterView) {
            }
        });
    }
    
    public final void z() {
        ((AdapterView)this.t).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener(this) {
            public final jk1 a;
            
            public void onItemSelected(final AdapterView adapterView, final View view, final int n, final long n2) {
                final LinearLayout c = jk1.c(this.a);
                if (n == 8) {
                    ((View)c).setVisibility(0);
                }
                else {
                    ((View)c).setVisibility(8);
                }
                if (n == 7) {
                    ((View)jk1.d(this.a)).setVisibility(0);
                }
                else {
                    ((View)jk1.d(this.a)).setVisibility(8);
                }
            }
            
            public void onNothingSelected(final AdapterView adapterView) {
            }
        });
    }
}

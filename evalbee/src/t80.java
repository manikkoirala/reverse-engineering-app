import android.database.sqlite.SQLiteProgram;
import android.database.sqlite.SQLiteStatement;

// 
// Decompiled by Procyon v0.6.0
// 

public final class t80 extends s80 implements ws1
{
    public final SQLiteStatement b;
    
    public t80(final SQLiteStatement b) {
        fg0.e((Object)b, "delegate");
        super((SQLiteProgram)b);
        this.b = b;
    }
    
    @Override
    public String W() {
        return this.b.simpleQueryForString();
    }
    
    @Override
    public long b0() {
        return this.b.executeInsert();
    }
    
    @Override
    public void execute() {
        this.b.execute();
    }
    
    @Override
    public int p() {
        return this.b.executeUpdateDelete();
    }
    
    @Override
    public long v() {
        return this.b.simpleQueryForLong();
    }
}

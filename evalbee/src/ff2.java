import java.util.HashSet;
import com.google.android.gms.measurement.api.AppMeasurementSdk$OnEventListener;
import com.google.android.gms.measurement.api.AppMeasurementSdk;
import java.util.Set;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ff2
{
    public Set a;
    public g4.b b;
    public AppMeasurementSdk c;
    public cf2 d;
    
    public ff2(final AppMeasurementSdk c, final g4.b b) {
        this.b = b;
        this.c = c;
        final cf2 d = new cf2(this);
        this.d = d;
        this.c.registerOnMeasurementEventListener((AppMeasurementSdk$OnEventListener)d);
        this.a = new HashSet();
    }
}

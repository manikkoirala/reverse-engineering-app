import android.view.View;
import android.view.textclassifier.TextClassificationManager;
import android.view.textclassifier.TextClassifier;
import android.widget.TextView;

// 
// Decompiled by Procyon v0.6.0
// 

public final class k7
{
    public TextView a;
    public TextClassifier b;
    
    public k7(final TextView textView) {
        this.a = (TextView)l71.g(textView);
    }
    
    public TextClassifier a() {
        TextClassifier textClassifier;
        if ((textClassifier = this.b) == null) {
            textClassifier = k7.a.a(this.a);
        }
        return textClassifier;
    }
    
    public void b(final TextClassifier b) {
        this.b = b;
    }
    
    public abstract static final class a
    {
        public static TextClassifier a(final TextView textView) {
            final TextClassificationManager textClassificationManager = (TextClassificationManager)((View)textView).getContext().getSystemService((Class)TextClassificationManager.class);
            if (textClassificationManager != null) {
                return textClassificationManager.getTextClassifier();
            }
            return TextClassifier.NO_OP;
        }
    }
}

import android.view.ViewParent;
import java.util.ArrayList;
import java.util.List;
import android.view.accessibility.AccessibilityRecord;
import android.view.accessibility.AccessibilityEvent;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.os.Bundle;
import android.view.accessibility.AccessibilityManager;
import android.view.View;
import android.graphics.Rect;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class kz extends p0
{
    private static final String DEFAULT_CLASS_NAME = "android.view.View";
    public static final int HOST_ID = -1;
    public static final int INVALID_ID = Integer.MIN_VALUE;
    private static final Rect INVALID_PARENT_BOUNDS;
    private static final d70.a NODE_ADAPTER;
    private static final d70.b SPARSE_VALUES_ADAPTER;
    int mAccessibilityFocusedVirtualViewId;
    private final View mHost;
    private int mHoveredVirtualViewId;
    int mKeyboardFocusedVirtualViewId;
    private final AccessibilityManager mManager;
    private c mNodeProvider;
    private final int[] mTempGlobalRect;
    private final Rect mTempParentRect;
    private final Rect mTempScreenRect;
    private final Rect mTempVisibleRect;
    
    static {
        INVALID_PARENT_BOUNDS = new Rect(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);
        NODE_ADAPTER = new d70.a() {
            public void b(final n1 n1, final Rect rect) {
                n1.l(rect);
            }
        };
        SPARSE_VALUES_ADAPTER = new d70.b() {
            public n1 c(final wo1 wo1, final int n) {
                return (n1)wo1.m(n);
            }
            
            public int d(final wo1 wo1) {
                return wo1.l();
            }
        };
    }
    
    public kz(final View mHost) {
        this.mTempScreenRect = new Rect();
        this.mTempParentRect = new Rect();
        this.mTempVisibleRect = new Rect();
        this.mTempGlobalRect = new int[2];
        this.mAccessibilityFocusedVirtualViewId = Integer.MIN_VALUE;
        this.mKeyboardFocusedVirtualViewId = Integer.MIN_VALUE;
        this.mHoveredVirtualViewId = Integer.MIN_VALUE;
        if (mHost != null) {
            this.mHost = mHost;
            this.mManager = (AccessibilityManager)mHost.getContext().getSystemService("accessibility");
            mHost.setFocusable(true);
            if (o32.z(mHost) == 0) {
                o32.B0(mHost, 1);
            }
            return;
        }
        throw new IllegalArgumentException("View may not be null");
    }
    
    public static Rect l(final View view, final int n, final Rect rect) {
        final int width = view.getWidth();
        final int height = view.getHeight();
        if (n != 17) {
            if (n != 33) {
                if (n != 66) {
                    if (n != 130) {
                        throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
                    }
                    rect.set(0, -1, width, -1);
                }
                else {
                    rect.set(-1, 0, -1, height);
                }
            }
            else {
                rect.set(0, height, width, height);
            }
        }
        else {
            rect.set(width, 0, width, height);
        }
        return rect;
    }
    
    public static int n(final int n) {
        if (n == 19) {
            return 33;
        }
        if (n == 21) {
            return 17;
        }
        if (n != 22) {
            return 130;
        }
        return 66;
    }
    
    public final boolean c(final int n) {
        if (this.mAccessibilityFocusedVirtualViewId == n) {
            this.mAccessibilityFocusedVirtualViewId = Integer.MIN_VALUE;
            this.mHost.invalidate();
            this.sendEventForVirtualView(n, 65536);
            return true;
        }
        return false;
    }
    
    public final boolean clearKeyboardFocusForVirtualView(final int n) {
        if (this.mKeyboardFocusedVirtualViewId != n) {
            return false;
        }
        this.mKeyboardFocusedVirtualViewId = Integer.MIN_VALUE;
        this.onVirtualViewKeyboardFocusChanged(n, false);
        this.sendEventForVirtualView(n, 8);
        return true;
    }
    
    public final boolean d() {
        final int mKeyboardFocusedVirtualViewId = this.mKeyboardFocusedVirtualViewId;
        return mKeyboardFocusedVirtualViewId != Integer.MIN_VALUE && this.onPerformActionForVirtualView(mKeyboardFocusedVirtualViewId, 16, null);
    }
    
    public final boolean dispatchHoverEvent(final MotionEvent motionEvent) {
        final boolean enabled = this.mManager.isEnabled();
        boolean b2;
        final boolean b = b2 = false;
        if (enabled) {
            if (!this.mManager.isTouchExplorationEnabled()) {
                b2 = b;
            }
            else {
                final int action = motionEvent.getAction();
                if (action != 7 && action != 9) {
                    if (action != 10) {
                        return false;
                    }
                    if (this.mHoveredVirtualViewId != Integer.MIN_VALUE) {
                        this.s(Integer.MIN_VALUE);
                        return true;
                    }
                    return false;
                }
                else {
                    final int virtualView = this.getVirtualViewAt(motionEvent.getX(), motionEvent.getY());
                    this.s(virtualView);
                    b2 = b;
                    if (virtualView != Integer.MIN_VALUE) {
                        b2 = true;
                    }
                }
            }
        }
        return b2;
    }
    
    public final boolean dispatchKeyEvent(final KeyEvent keyEvent) {
        final int action = keyEvent.getAction();
        final boolean b = false;
        int n = 0;
        boolean b2 = b;
        if (action != 1) {
            final int keyCode = keyEvent.getKeyCode();
            if (keyCode != 61) {
                if (keyCode != 66) {
                    switch (keyCode) {
                        default: {
                            b2 = b;
                            return b2;
                        }
                        case 19:
                        case 20:
                        case 21:
                        case 22: {
                            b2 = b;
                            if (keyEvent.hasNoModifiers()) {
                                final int n2 = n(keyCode);
                                final int repeatCount = keyEvent.getRepeatCount();
                                b2 = false;
                                while (n < repeatCount + 1 && this.o(n2, null)) {
                                    ++n;
                                    b2 = true;
                                }
                                return b2;
                            }
                            return b2;
                        }
                        case 23: {
                            break;
                        }
                    }
                }
                b2 = b;
                if (keyEvent.hasNoModifiers()) {
                    b2 = b;
                    if (keyEvent.getRepeatCount() == 0) {
                        this.d();
                        b2 = true;
                    }
                }
            }
            else if (keyEvent.hasNoModifiers()) {
                b2 = this.o(2, null);
            }
            else {
                b2 = b;
                if (keyEvent.hasModifiers(1)) {
                    b2 = this.o(1, null);
                }
            }
        }
        return b2;
    }
    
    public final AccessibilityEvent e(final int n, final int n2) {
        if (n != -1) {
            return this.f(n, n2);
        }
        return this.g(n2);
    }
    
    public final AccessibilityEvent f(final int n, final int n2) {
        final AccessibilityEvent obtain = AccessibilityEvent.obtain(n2);
        final n1 obtainAccessibilityNodeInfo = this.obtainAccessibilityNodeInfo(n);
        ((AccessibilityRecord)obtain).getText().add(obtainAccessibilityNodeInfo.v());
        ((AccessibilityRecord)obtain).setContentDescription(obtainAccessibilityNodeInfo.q());
        ((AccessibilityRecord)obtain).setScrollable(obtainAccessibilityNodeInfo.I());
        ((AccessibilityRecord)obtain).setPassword(obtainAccessibilityNodeInfo.H());
        ((AccessibilityRecord)obtain).setEnabled(obtainAccessibilityNodeInfo.D());
        ((AccessibilityRecord)obtain).setChecked(obtainAccessibilityNodeInfo.B());
        this.onPopulateEventForVirtualView(n, obtain);
        if (((AccessibilityRecord)obtain).getText().isEmpty() && ((AccessibilityRecord)obtain).getContentDescription() == null) {
            throw new RuntimeException("Callbacks must add text or a content description in populateEventForVirtualViewId()");
        }
        ((AccessibilityRecord)obtain).setClassName(obtainAccessibilityNodeInfo.o());
        p1.c((AccessibilityRecord)obtain, this.mHost, n);
        obtain.setPackageName((CharSequence)this.mHost.getContext().getPackageName());
        return obtain;
    }
    
    public final AccessibilityEvent g(final int n) {
        final AccessibilityEvent obtain = AccessibilityEvent.obtain(n);
        this.mHost.onInitializeAccessibilityEvent(obtain);
        return obtain;
    }
    
    public final int getAccessibilityFocusedVirtualViewId() {
        return this.mAccessibilityFocusedVirtualViewId;
    }
    
    @Override
    public o1 getAccessibilityNodeProvider(final View view) {
        if (this.mNodeProvider == null) {
            this.mNodeProvider = new c();
        }
        return this.mNodeProvider;
    }
    
    @Deprecated
    public int getFocusedVirtualView() {
        return this.getAccessibilityFocusedVirtualViewId();
    }
    
    public final int getKeyboardFocusedVirtualViewId() {
        return this.mKeyboardFocusedVirtualViewId;
    }
    
    public abstract int getVirtualViewAt(final float p0, final float p1);
    
    public abstract void getVisibleVirtualViews(final List p0);
    
    public final n1 h(int i) {
        final n1 l = n1.L();
        l.e0(true);
        l.g0(true);
        l.Y("android.view.View");
        final Rect invalid_PARENT_BOUNDS = kz.INVALID_PARENT_BOUNDS;
        l.U(invalid_PARENT_BOUNDS);
        l.V(invalid_PARENT_BOUNDS);
        l.o0(this.mHost);
        this.onPopulateNodeForVirtualView(i, l);
        if (l.v() == null && l.q() == null) {
            throw new RuntimeException("Callbacks must add text or a content description in populateNodeForVirtualViewId()");
        }
        l.l(this.mTempParentRect);
        if (this.mTempParentRect.equals((Object)invalid_PARENT_BOUNDS)) {
            throw new RuntimeException("Callbacks must set parent bounds in populateNodeForVirtualViewId()");
        }
        final int j = l.j();
        if ((j & 0x40) != 0x0) {
            throw new RuntimeException("Callbacks must not add ACTION_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
        }
        if ((j & 0x80) == 0x0) {
            l.m0(this.mHost.getContext().getPackageName());
            l.v0(this.mHost, i);
            if (this.mAccessibilityFocusedVirtualViewId == i) {
                l.S(true);
                l.a(128);
            }
            else {
                l.S(false);
                l.a(64);
            }
            final boolean b = this.mKeyboardFocusedVirtualViewId == i;
            if (b) {
                l.a(2);
            }
            else if (l.E()) {
                l.a(1);
            }
            l.h0(b);
            this.mHost.getLocationOnScreen(this.mTempGlobalRect);
            l.m(this.mTempScreenRect);
            if (this.mTempScreenRect.equals((Object)invalid_PARENT_BOUNDS)) {
                l.l(this.mTempScreenRect);
                if (l.b != -1) {
                    n1 k;
                    Rect mTempScreenRect;
                    Rect mTempParentRect;
                    for (k = n1.L(), i = l.b; i != -1; i = k.b) {
                        k.p0(this.mHost, -1);
                        k.U(kz.INVALID_PARENT_BOUNDS);
                        this.onPopulateNodeForVirtualView(i, k);
                        k.l(this.mTempParentRect);
                        mTempScreenRect = this.mTempScreenRect;
                        mTempParentRect = this.mTempParentRect;
                        mTempScreenRect.offset(mTempParentRect.left, mTempParentRect.top);
                    }
                    k.P();
                }
                this.mTempScreenRect.offset(this.mTempGlobalRect[0] - this.mHost.getScrollX(), this.mTempGlobalRect[1] - this.mHost.getScrollY());
            }
            if (this.mHost.getLocalVisibleRect(this.mTempVisibleRect)) {
                this.mTempVisibleRect.offset(this.mTempGlobalRect[0] - this.mHost.getScrollX(), this.mTempGlobalRect[1] - this.mHost.getScrollY());
                if (this.mTempScreenRect.intersect(this.mTempVisibleRect)) {
                    l.V(this.mTempScreenRect);
                    if (this.m(this.mTempScreenRect)) {
                        l.z0(true);
                    }
                }
            }
            return l;
        }
        throw new RuntimeException("Callbacks must not add ACTION_CLEAR_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
    }
    
    public final n1 i() {
        final n1 n = n1.N(this.mHost);
        o32.d0(this.mHost, n);
        final ArrayList list = new ArrayList();
        this.getVisibleVirtualViews(list);
        if (n.n() > 0 && list.size() > 0) {
            throw new RuntimeException("Views cannot have both real and virtual children");
        }
        for (int size = list.size(), i = 0; i < size; ++i) {
            n.c(this.mHost, (int)list.get(i));
        }
        return n;
    }
    
    public final void invalidateRoot() {
        this.invalidateVirtualView(-1, 1);
    }
    
    public final void invalidateVirtualView(final int n) {
        this.invalidateVirtualView(n, 0);
    }
    
    public final void invalidateVirtualView(final int n, final int n2) {
        if (n != Integer.MIN_VALUE && this.mManager.isEnabled()) {
            final ViewParent parent = this.mHost.getParent();
            if (parent != null) {
                final AccessibilityEvent e = this.e(n, 2048);
                q0.b(e, n2);
                parent.requestSendAccessibilityEvent(this.mHost, e);
            }
        }
    }
    
    public final wo1 j() {
        final ArrayList list = new ArrayList();
        this.getVisibleVirtualViews(list);
        final wo1 wo1 = new wo1();
        for (int i = 0; i < list.size(); ++i) {
            wo1.k((int)list.get(i), this.h((int)list.get(i)));
        }
        return wo1;
    }
    
    public final void k(final int n, final Rect rect) {
        this.obtainAccessibilityNodeInfo(n).l(rect);
    }
    
    public final boolean m(final Rect rect) {
        boolean b2;
        final boolean b = b2 = false;
        if (rect != null) {
            if (rect.isEmpty()) {
                b2 = b;
            }
            else {
                if (this.mHost.getWindowVisibility() != 0) {
                    return false;
                }
                View mHost = this.mHost;
                View view;
                do {
                    final ViewParent parent = mHost.getParent();
                    if (parent instanceof View) {
                        view = (View)parent;
                        if (view.getAlpha() <= 0.0f) {
                            break;
                        }
                        mHost = view;
                    }
                    else {
                        b2 = b;
                        if (parent != null) {
                            b2 = true;
                            return b2;
                        }
                        return b2;
                    }
                } while (view.getVisibility() == 0);
                return false;
            }
        }
        return b2;
    }
    
    public final boolean o(int j, final Rect rect) {
        final wo1 i = this.j();
        final int mKeyboardFocusedVirtualViewId = this.mKeyboardFocusedVirtualViewId;
        final int n = Integer.MIN_VALUE;
        Object o;
        if (mKeyboardFocusedVirtualViewId == Integer.MIN_VALUE) {
            o = null;
        }
        else {
            o = i.f(mKeyboardFocusedVirtualViewId);
        }
        Object o2;
        if (j != 1 && j != 2) {
            if (j != 17 && j != 33 && j != 66 && j != 130) {
                throw new IllegalArgumentException("direction must be one of {FOCUS_FORWARD, FOCUS_BACKWARD, FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
            }
            final Rect rect2 = new Rect();
            final int mKeyboardFocusedVirtualViewId2 = this.mKeyboardFocusedVirtualViewId;
            if (mKeyboardFocusedVirtualViewId2 != Integer.MIN_VALUE) {
                this.k(mKeyboardFocusedVirtualViewId2, rect2);
            }
            else if (rect != null) {
                rect2.set(rect);
            }
            else {
                l(this.mHost, j, rect2);
            }
            o2 = d70.c(i, kz.SPARSE_VALUES_ADAPTER, kz.NODE_ADAPTER, o, rect2, j);
        }
        else {
            o2 = d70.d(i, kz.SPARSE_VALUES_ADAPTER, kz.NODE_ADAPTER, o, j, o32.B(this.mHost) == 1, false);
        }
        final n1 n2 = (n1)o2;
        if (n2 == null) {
            j = n;
        }
        else {
            j = i.j(i.i(n2));
        }
        return this.requestKeyboardFocusForVirtualView(j);
    }
    
    public n1 obtainAccessibilityNodeInfo(final int n) {
        if (n == -1) {
            return this.i();
        }
        return this.h(n);
    }
    
    public final void onFocusChanged(final boolean b, final int n, final Rect rect) {
        final int mKeyboardFocusedVirtualViewId = this.mKeyboardFocusedVirtualViewId;
        if (mKeyboardFocusedVirtualViewId != Integer.MIN_VALUE) {
            this.clearKeyboardFocusForVirtualView(mKeyboardFocusedVirtualViewId);
        }
        if (b) {
            this.o(n, rect);
        }
    }
    
    @Override
    public void onInitializeAccessibilityEvent(final View view, final AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(view, accessibilityEvent);
        this.onPopulateEventForHost(accessibilityEvent);
    }
    
    @Override
    public void onInitializeAccessibilityNodeInfo(final View view, final n1 n1) {
        super.onInitializeAccessibilityNodeInfo(view, n1);
        this.onPopulateNodeForHost(n1);
    }
    
    public abstract boolean onPerformActionForVirtualView(final int p0, final int p1, final Bundle p2);
    
    public void onPopulateEventForHost(final AccessibilityEvent accessibilityEvent) {
    }
    
    public void onPopulateEventForVirtualView(final int n, final AccessibilityEvent accessibilityEvent) {
    }
    
    public void onPopulateNodeForHost(final n1 n1) {
    }
    
    public abstract void onPopulateNodeForVirtualView(final int p0, final n1 p1);
    
    public void onVirtualViewKeyboardFocusChanged(final int n, final boolean b) {
    }
    
    public final boolean p(final int n, final int n2, final Bundle bundle) {
        if (n2 == 1) {
            return this.requestKeyboardFocusForVirtualView(n);
        }
        if (n2 == 2) {
            return this.clearKeyboardFocusForVirtualView(n);
        }
        if (n2 == 64) {
            return this.r(n);
        }
        if (n2 != 128) {
            return this.onPerformActionForVirtualView(n, n2, bundle);
        }
        return this.c(n);
    }
    
    public boolean performAction(final int n, final int n2, final Bundle bundle) {
        if (n != -1) {
            return this.p(n, n2, bundle);
        }
        return this.q(n2, bundle);
    }
    
    public final boolean q(final int n, final Bundle bundle) {
        return o32.f0(this.mHost, n, bundle);
    }
    
    public final boolean r(final int mAccessibilityFocusedVirtualViewId) {
        if (this.mManager.isEnabled()) {
            if (this.mManager.isTouchExplorationEnabled()) {
                final int mAccessibilityFocusedVirtualViewId2 = this.mAccessibilityFocusedVirtualViewId;
                if (mAccessibilityFocusedVirtualViewId2 != mAccessibilityFocusedVirtualViewId) {
                    if (mAccessibilityFocusedVirtualViewId2 != Integer.MIN_VALUE) {
                        this.c(mAccessibilityFocusedVirtualViewId2);
                    }
                    this.mAccessibilityFocusedVirtualViewId = mAccessibilityFocusedVirtualViewId;
                    this.mHost.invalidate();
                    this.sendEventForVirtualView(mAccessibilityFocusedVirtualViewId, 32768);
                    return true;
                }
            }
        }
        return false;
    }
    
    public final boolean requestKeyboardFocusForVirtualView(final int mKeyboardFocusedVirtualViewId) {
        if (!this.mHost.isFocused() && !this.mHost.requestFocus()) {
            return false;
        }
        final int mKeyboardFocusedVirtualViewId2 = this.mKeyboardFocusedVirtualViewId;
        if (mKeyboardFocusedVirtualViewId2 == mKeyboardFocusedVirtualViewId) {
            return false;
        }
        if (mKeyboardFocusedVirtualViewId2 != Integer.MIN_VALUE) {
            this.clearKeyboardFocusForVirtualView(mKeyboardFocusedVirtualViewId2);
        }
        if (mKeyboardFocusedVirtualViewId == Integer.MIN_VALUE) {
            return false;
        }
        this.onVirtualViewKeyboardFocusChanged(this.mKeyboardFocusedVirtualViewId = mKeyboardFocusedVirtualViewId, true);
        this.sendEventForVirtualView(mKeyboardFocusedVirtualViewId, 8);
        return true;
    }
    
    public final void s(final int mHoveredVirtualViewId) {
        final int mHoveredVirtualViewId2 = this.mHoveredVirtualViewId;
        if (mHoveredVirtualViewId2 == mHoveredVirtualViewId) {
            return;
        }
        this.sendEventForVirtualView(this.mHoveredVirtualViewId = mHoveredVirtualViewId, 128);
        this.sendEventForVirtualView(mHoveredVirtualViewId2, 256);
    }
    
    public final boolean sendEventForVirtualView(final int n, final int n2) {
        if (n != Integer.MIN_VALUE && this.mManager.isEnabled()) {
            final ViewParent parent = this.mHost.getParent();
            return parent != null && parent.requestSendAccessibilityEvent(this.mHost, this.e(n, n2));
        }
        return false;
    }
    
    public class c extends o1
    {
        public final kz b;
        
        public c(final kz b) {
            this.b = b;
        }
        
        @Override
        public n1 b(final int n) {
            return n1.M(this.b.obtainAccessibilityNodeInfo(n));
        }
        
        @Override
        public n1 d(int n) {
            if (n == 2) {
                n = this.b.mAccessibilityFocusedVirtualViewId;
            }
            else {
                n = this.b.mKeyboardFocusedVirtualViewId;
            }
            if (n == Integer.MIN_VALUE) {
                return null;
            }
            return this.b(n);
        }
        
        @Override
        public boolean f(final int n, final int n2, final Bundle bundle) {
            return this.b.performAction(n, n2, bundle);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

public class y80
{
    public String[] a;
    public t90[] b;
    public int c;
    public boolean d;
    
    public y80() {
        this.a = new String[50];
        this.b = new t90[50];
        this.c = 0;
        this.d = false;
    }
    
    public t90 a(final String str, final int i) {
        for (int j = 0; j < this.c; ++j) {
            if (this.b[j].b(i) && ((this.d && this.a[j].equals(str)) || (!this.d && this.a[j].equalsIgnoreCase(str)))) {
                return this.b[j];
            }
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("function not found: ");
        sb.append(str);
        sb.append(" ");
        sb.append(i);
        throw new RuntimeException(sb.toString());
    }
    
    public void b() {
        this.c("min", new vw0());
        this.c("max", new lu0());
        this.c("sum", new es1());
        this.c("avg", new va());
        this.c("pi", new j51());
        this.c("e", new wv());
        this.c("rand", new cc1());
        this.c("sin", new eo1());
        this.c("cos", new qm());
        this.c("tan", new wt1());
        this.c("sqrt", new gp1());
        this.c("abs", new d());
        this.c("ceil", new qf());
        this.c("floor", new x40());
        this.c("exp", new iz());
        this.c("lg", new jj0());
        this.c("ln", new pk0());
        this.c("sign", new ao1());
        this.c("round", new tf1());
        this.c("fact", new wz());
        this.c("cosh", new rm());
        this.c("sinh", new go1());
        this.c("tanh", new xt1());
        this.c("acos", new r1());
        this.c("asin", new e9());
        this.c("atan", new r9());
        this.c("acosh", new s1());
        this.c("asinh", new f9());
        this.c("atanh", new s9());
        this.c("pow", new t61());
        this.c("mod", new xw0());
        this.c("combin", new di());
        this.c("log", new vl0());
    }
    
    public void c(final String s, final t90 t90) {
        if (s == null) {
            throw new IllegalArgumentException("function name cannot be null");
        }
        if (t90 == null) {
            throw new IllegalArgumentException("function cannot be null");
        }
        final int n = 0;
        int n2 = 0;
        while (true) {
            final int c = this.c;
            if (n2 >= c) {
                if (c == this.a.length) {
                    final int n3 = c * 2;
                    final String[] a = new String[n3];
                    final t90[] b = new t90[n3];
                    for (int i = n; i < this.c; ++i) {
                        a[i] = this.a[i];
                        b[i] = this.b[i];
                    }
                    this.a = a;
                    this.b = b;
                }
                final String[] a2 = this.a;
                final int c2 = this.c;
                a2[c2] = s;
                this.b[c2] = t90;
                this.c = c2 + 1;
                return;
            }
            if ((this.d && this.a[n2].equals(s)) || (!this.d && this.a[n2].equalsIgnoreCase(s))) {
                this.b[n2] = t90;
                return;
            }
            ++n2;
        }
    }
}

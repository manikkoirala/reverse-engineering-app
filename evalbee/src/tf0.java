import com.google.common.primitives.Ints;
import java.math.RoundingMode;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class tf0
{
    public static final byte[] a;
    public static final int[] b;
    public static final int[] c;
    public static final int[] d;
    public static int[] e;
    
    static {
        a = new byte[] { 9, 9, 9, 8, 8, 8, 7, 7, 7, 6, 6, 6, 6, 5, 5, 5, 4, 4, 4, 3, 3, 3, 3, 2, 2, 2, 1, 1, 1, 0, 0, 0, 0 };
        b = new int[] { 1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000 };
        c = new int[] { 3, 31, 316, 3162, 31622, 316227, 3162277, 31622776, 316227766, Integer.MAX_VALUE };
        d = new int[] { 1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880, 3628800, 39916800, 479001600 };
        tf0.e = new int[] { Integer.MAX_VALUE, Integer.MAX_VALUE, 65536, 2345, 477, 193, 110, 75, 58, 49, 43, 39, 37, 35, 34, 34, 33 };
    }
    
    public static int a(final int n, final int n2) {
        final long n3 = n + (long)n2;
        final int n4 = (int)n3;
        ju0.b(n3 == n4, "checkedAdd", n, n2);
        return n4;
    }
    
    public static int b(final int n, final int n2) {
        final long n3 = n * (long)n2;
        final int n4 = (int)n3;
        ju0.b(n3 == n4, "checkedMultiply", n, n2);
        return n4;
    }
    
    public static int c(int abs, int a, final RoundingMode roundingMode) {
        i71.r(roundingMode);
        if (a == 0) {
            throw new ArithmeticException("/ by zero");
        }
        final int n = abs / a;
        final int a2 = abs - a * n;
        if (a2 == 0) {
            return n;
        }
        final int n2 = 1;
        boolean b = true;
        final int n3 = (abs ^ a) >> 31 | 0x1;
        abs = n2;
        Label_0224: {
            switch (tf0$a.a[roundingMode.ordinal()]) {
                default: {
                    throw new AssertionError();
                }
                case 6:
                case 7:
                case 8: {
                    abs = Math.abs(a2);
                    abs -= Math.abs(a) - abs;
                    if (abs == 0) {
                        abs = n2;
                        if (roundingMode == RoundingMode.HALF_UP) {
                            break Label_0224;
                        }
                        if (roundingMode == RoundingMode.HALF_EVEN) {
                            abs = 1;
                        }
                        else {
                            abs = 0;
                        }
                        if ((n & 0x1) != 0x0) {
                            a = 1;
                        }
                        else {
                            a = 0;
                        }
                        if ((abs & a) != 0x0) {
                            abs = n2;
                            break Label_0224;
                        }
                        break Label_0224;
                    }
                    else {
                        if (abs > 0) {
                            abs = n2;
                            break Label_0224;
                        }
                        break Label_0224;
                    }
                    break;
                }
                case 5: {
                    if (n3 > 0) {
                        abs = n2;
                        break Label_0224;
                    }
                    break Label_0224;
                }
                case 3: {
                    if (n3 < 0) {
                        abs = n2;
                        break Label_0224;
                    }
                    break Label_0224;
                }
                case 1: {
                    if (a2 != 0) {
                        b = false;
                    }
                    ju0.e(b);
                }
                case 2: {
                    abs = 0;
                }
                case 4: {
                    a = n;
                    if (abs != 0) {
                        a = n + n3;
                    }
                    return a;
                }
            }
        }
    }
    
    public static boolean d(final int n) {
        boolean b = false;
        final boolean b2 = n > 0;
        if ((n & n - 1) == 0x0) {
            b = true;
        }
        return b2 & b;
    }
    
    public static int e(final int n, final int n2) {
        return ~(~(n - n2)) >>> 31;
    }
    
    public static int f(final int n, final RoundingMode roundingMode) {
        ju0.d("x", n);
        switch (tf0$a.a[roundingMode.ordinal()]) {
            default: {
                throw new AssertionError();
            }
            case 6:
            case 7:
            case 8: {
                final int numberOfLeadingZeros = Integer.numberOfLeadingZeros(n);
                return 31 - numberOfLeadingZeros + e(-1257966797 >>> numberOfLeadingZeros, n);
            }
            case 4:
            case 5: {
                return 32 - Integer.numberOfLeadingZeros(n - 1);
            }
            case 1: {
                ju0.e(d(n));
            }
            case 2:
            case 3: {
                return 31 - Integer.numberOfLeadingZeros(n);
            }
        }
    }
    
    public static int g(final int n, final int n2) {
        return Ints.k(n + (long)n2);
    }
    
    public static int h(final int n, final int n2) {
        return Ints.k(n * (long)n2);
    }
}

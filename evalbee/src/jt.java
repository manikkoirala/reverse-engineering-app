import java.io.FilterInputStream;
import android.os.SystemClock;
import android.text.TextUtils;
import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.File;
import com.android.volley.e;
import java.io.BufferedOutputStream;
import java.util.Iterator;
import java.io.OutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.io.EOFException;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;
import com.android.volley.a;

// 
// Decompiled by Procyon v0.6.0
// 

public class jt implements com.android.volley.a
{
    public final Map a;
    public long b;
    public final c c;
    public final int d;
    
    public jt(final c c) {
        this(c, 5242880);
    }
    
    public jt(final c c, final int d) {
        this.a = new LinkedHashMap(16, 0.75f, true);
        this.b = 0L;
        this.c = c;
        this.d = d;
    }
    
    public static int j(final InputStream inputStream) {
        final int read = inputStream.read();
        if (read != -1) {
            return read;
        }
        throw new EOFException();
    }
    
    public static List k(final b b) {
        final int l = l(b);
        if (l >= 0) {
            List<Object> emptyList;
            if (l == 0) {
                emptyList = Collections.emptyList();
            }
            else {
                emptyList = new ArrayList<Object>();
            }
            for (int i = 0; i < l; ++i) {
                emptyList.add(new sc0(n(b).intern(), n(b).intern()));
            }
            return emptyList;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("readHeaderList size=");
        sb.append(l);
        throw new IOException(sb.toString());
    }
    
    public static int l(final InputStream inputStream) {
        return j(inputStream) << 24 | (j(inputStream) << 0 | 0x0 | j(inputStream) << 8 | j(inputStream) << 16);
    }
    
    public static long m(final InputStream inputStream) {
        return ((long)j(inputStream) & 0xFFL) << 0 | 0x0L | ((long)j(inputStream) & 0xFFL) << 8 | ((long)j(inputStream) & 0xFFL) << 16 | ((long)j(inputStream) & 0xFFL) << 24 | ((long)j(inputStream) & 0xFFL) << 32 | ((long)j(inputStream) & 0xFFL) << 40 | ((long)j(inputStream) & 0xFFL) << 48 | (0xFFL & (long)j(inputStream)) << 56;
    }
    
    public static String n(final b b) {
        return new String(q(b, m(b)), "UTF-8");
    }
    
    public static byte[] q(final b in, final long lng) {
        final long a = in.a();
        if (lng >= 0L && lng <= a) {
            final int n = (int)lng;
            if (n == lng) {
                final byte[] b = new byte[n];
                new DataInputStream(in).readFully(b);
                return b;
            }
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("streamToBytes length=");
        sb.append(lng);
        sb.append(", maxLength=");
        sb.append(a);
        throw new IOException(sb.toString());
    }
    
    public static void r(final List list, final OutputStream outputStream) {
        if (list != null) {
            s(outputStream, list.size());
            for (final sc0 sc0 : list) {
                u(outputStream, sc0.a());
                u(outputStream, sc0.b());
            }
        }
        else {
            s(outputStream, 0);
        }
    }
    
    public static void s(final OutputStream outputStream, final int n) {
        outputStream.write(n >> 0 & 0xFF);
        outputStream.write(n >> 8 & 0xFF);
        outputStream.write(n >> 16 & 0xFF);
        outputStream.write(n >> 24 & 0xFF);
    }
    
    public static void t(final OutputStream outputStream, final long n) {
        outputStream.write((byte)(n >>> 0));
        outputStream.write((byte)(n >>> 8));
        outputStream.write((byte)(n >>> 16));
        outputStream.write((byte)(n >>> 24));
        outputStream.write((byte)(n >>> 32));
        outputStream.write((byte)(n >>> 40));
        outputStream.write((byte)(n >>> 48));
        outputStream.write((byte)(n >>> 56));
    }
    
    public static void u(final OutputStream outputStream, final String s) {
        final byte[] bytes = s.getBytes("UTF-8");
        t(outputStream, bytes.length);
        outputStream.write(bytes, 0, bytes.length);
    }
    
    @Override
    public void a(final String s, final boolean b) {
        synchronized (this) {
            final com.android.volley.a.a value = this.get(s);
            if (value != null) {
                value.f = 0L;
                if (b) {
                    value.e = 0L;
                }
                this.b(s, value);
            }
        }
    }
    
    @Override
    public void b(final String s, final com.android.volley.a.a a) {
        synchronized (this) {
            final long b = this.b;
            final byte[] a2 = a.a;
            final long n = a2.length;
            final int d = this.d;
            if (b + n > d && a2.length > d * 0.9f) {
                return;
            }
            final File e = this.e(s);
            try {
                final BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(this.d(e));
                final a a3 = new a(s, a);
                if (!a3.d(bufferedOutputStream)) {
                    bufferedOutputStream.close();
                    com.android.volley.e.b("Failed to write header for %s", e.getAbsolutePath());
                    throw new IOException();
                }
                bufferedOutputStream.write(a.a);
                bufferedOutputStream.close();
                a3.a = e.length();
                this.i(s, a3);
                this.h();
            }
            catch (final IOException ex) {
                if (!e.delete()) {
                    com.android.volley.e.b("Could not clean up file %s", e.getAbsolutePath());
                }
                this.g();
            }
        }
    }
    
    public InputStream c(final File file) {
        return new FileInputStream(file);
    }
    
    @Override
    public void clear() {
        synchronized (this) {
            final File[] listFiles = this.c.get().listFiles();
            if (listFiles != null) {
                for (int length = listFiles.length, i = 0; i < length; ++i) {
                    listFiles[i].delete();
                }
            }
            this.a.clear();
            this.b = 0L;
            e.b("Cache cleared.", new Object[0]);
        }
    }
    
    public OutputStream d(final File file) {
        return new FileOutputStream(file);
    }
    
    public File e(final String s) {
        return new File(this.c.get(), this.f(s));
    }
    
    public final String f(final String s) {
        final int n = s.length() / 2;
        final int hashCode = s.substring(0, n).hashCode();
        final StringBuilder sb = new StringBuilder();
        sb.append(String.valueOf(hashCode));
        sb.append(String.valueOf(s.substring(n).hashCode()));
        return sb.toString();
    }
    
    public final void g() {
        if (!this.c.get().exists()) {
            e.b("Re-initializing cache after external clearing.", new Object[0]);
            this.a.clear();
            this.b = 0L;
            this.initialize();
        }
    }
    
    @Override
    public com.android.volley.a.a get(final String s) {
        synchronized (this) {
            final a a = this.a.get(s);
            if (a == null) {
                return null;
            }
            final File e = this.e(s);
            try {
                final b b = new b(new BufferedInputStream(this.c(e)), e.length());
                try {
                    final a b2 = jt.a.b(b);
                    if (!TextUtils.equals((CharSequence)s, (CharSequence)b2.b)) {
                        com.android.volley.e.b("%s: key=%s, found=%s", e.getAbsolutePath(), s, b2.b);
                        this.p(s);
                        return null;
                    }
                    return a.c(q(b, b.a()));
                }
                finally {
                    b.close();
                }
            }
            catch (final IOException ex) {
                com.android.volley.e.b("%s: %s", e.getAbsolutePath(), ex.toString());
                this.o(s);
                return null;
            }
        }
    }
    
    public final void h() {
        if (this.b < this.d) {
            return;
        }
        final boolean b = e.b;
        int n = 0;
        if (b) {
            e.e("Pruning old cache entries.", new Object[0]);
        }
        final long b2 = this.b;
        final long elapsedRealtime = SystemClock.elapsedRealtime();
        final Iterator iterator = this.a.entrySet().iterator();
        int i;
        do {
            i = n;
            if (!iterator.hasNext()) {
                break;
            }
            final a a = ((Map.Entry<K, a>)iterator.next()).getValue();
            if (this.e(a.b).delete()) {
                this.b -= a.a;
            }
            else {
                final String b3 = a.b;
                e.b("Could not delete cache entry for key=%s, filename=%s", b3, this.f(b3));
            }
            iterator.remove();
            i = ++n;
        } while (this.b >= this.d * 0.9f);
        if (e.b) {
            e.e("pruned %d files, %d bytes, %d ms", i, this.b - b2, SystemClock.elapsedRealtime() - elapsedRealtime);
        }
    }
    
    public final void i(final String s, final a a) {
        if (!this.a.containsKey(s)) {
            this.b += a.a;
        }
        else {
            this.b += a.a - this.a.get(s).a;
        }
        this.a.put(s, a);
    }
    
    @Override
    public void initialize() {
        synchronized (this) {
            final File value = this.c.get();
            final boolean exists = value.exists();
            int i = 0;
            if (!exists) {
                if (!value.mkdirs()) {
                    e.c("Unable to create cache dir %s", value.getAbsolutePath());
                }
                return;
            }
            final File[] listFiles = value.listFiles();
            if (listFiles == null) {
                return;
            }
            while (i < listFiles.length) {
                final File file = listFiles[i];
                try {
                    final long length = file.length();
                    final b b = new b(new BufferedInputStream(this.c(file)), length);
                    try {
                        final a b2 = jt.a.b(b);
                        b2.a = length;
                        this.i(b2.b, b2);
                    }
                    finally {
                        b.close();
                    }
                }
                catch (final IOException ex) {
                    file.delete();
                }
                ++i;
            }
        }
    }
    
    public void o(final String s) {
        synchronized (this) {
            final boolean delete = this.e(s).delete();
            this.p(s);
            if (!delete) {
                e.b("Could not delete cache entry for key=%s, filename=%s", s, this.f(s));
            }
        }
    }
    
    public final void p(final String s) {
        final a a = this.a.remove(s);
        if (a != null) {
            this.b -= a.a;
        }
    }
    
    public static class a
    {
        public long a;
        public final String b;
        public final String c;
        public final long d;
        public final long e;
        public final long f;
        public final long g;
        public final List h;
        
        public a(final String s, final com.android.volley.a.a a) {
            this(s, a.b, a.c, a.d, a.e, a.f, a(a));
        }
        
        public a(String s, final String anObject, final long d, final long e, final long f, final long g, final List h) {
            this.b = s;
            s = anObject;
            if ("".equals(anObject)) {
                s = null;
            }
            this.c = s;
            this.d = d;
            this.e = e;
            this.f = f;
            this.g = g;
            this.h = h;
        }
        
        public static List a(final com.android.volley.a.a a) {
            final List h = a.h;
            if (h != null) {
                return h;
            }
            return id0.i(a.g);
        }
        
        public static a b(final b b) {
            if (jt.l(b) == 538247942) {
                return new a(jt.n(b), jt.n(b), jt.m(b), jt.m(b), jt.m(b), jt.m(b), jt.k(b));
            }
            throw new IOException();
        }
        
        public com.android.volley.a.a c(final byte[] a) {
            final com.android.volley.a.a a2 = new com.android.volley.a.a();
            a2.a = a;
            a2.b = this.c;
            a2.c = this.d;
            a2.d = this.e;
            a2.e = this.f;
            a2.f = this.g;
            a2.g = id0.j(this.h);
            a2.h = Collections.unmodifiableList((List<?>)this.h);
            return a2;
        }
        
        public boolean d(final OutputStream outputStream) {
            try {
                jt.s(outputStream, 538247942);
                jt.u(outputStream, this.b);
                String c;
                if ((c = this.c) == null) {
                    c = "";
                }
                jt.u(outputStream, c);
                jt.t(outputStream, this.d);
                jt.t(outputStream, this.e);
                jt.t(outputStream, this.f);
                jt.t(outputStream, this.g);
                jt.r(this.h, outputStream);
                outputStream.flush();
                return true;
            }
            catch (final IOException ex) {
                com.android.volley.e.b("%s", ex.toString());
                return false;
            }
        }
    }
    
    public static class b extends FilterInputStream
    {
        public final long a;
        public long b;
        
        public b(final InputStream in, final long a) {
            super(in);
            this.a = a;
        }
        
        public long a() {
            return this.a - this.b;
        }
        
        @Override
        public int read() {
            final int read = super.read();
            if (read != -1) {
                ++this.b;
            }
            return read;
        }
        
        @Override
        public int read(final byte[] b, int read, final int len) {
            read = super.read(b, read, len);
            if (read != -1) {
                this.b += read;
            }
            return read;
        }
    }
    
    public interface c
    {
        File get();
    }
}

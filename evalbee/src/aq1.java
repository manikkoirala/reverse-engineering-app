import com.google.common.math.Stats;
import java.util.Iterator;
import com.google.common.primitives.Doubles;

// 
// Decompiled by Procyon v0.6.0
// 

public final class aq1
{
    public long a;
    public double b;
    public double c;
    public double d;
    public double e;
    
    public aq1() {
        this.a = 0L;
        this.b = 0.0;
        this.c = 0.0;
        this.d = Double.NaN;
        this.e = Double.NaN;
    }
    
    public static double g(final double n, final double n2) {
        if (Doubles.f(n)) {
            return n2;
        }
        double n3 = n;
        if (!Doubles.f(n2)) {
            if (n == n2) {
                n3 = n;
            }
            else {
                n3 = Double.NaN;
            }
        }
        return n3;
    }
    
    public void a(final double b) {
        final long a = this.a;
        if (a == 0L) {
            this.a = 1L;
            this.b = b;
            this.d = b;
            this.e = b;
            if (!Doubles.f(b)) {
                this.c = Double.NaN;
            }
        }
        else {
            this.a = a + 1L;
            if (Doubles.f(b) && Doubles.f(this.b)) {
                final double b2 = this.b;
                final double n = b - b2;
                final double b3 = b2 + n / this.a;
                this.b = b3;
                this.c += n * (b - b3);
            }
            else {
                this.b = g(this.b, b);
                this.c = Double.NaN;
            }
            this.d = Math.min(this.d, b);
            this.e = Math.max(this.e, b);
        }
    }
    
    public void b(final Iterable iterable) {
        final Iterator iterator = iterable.iterator();
        while (iterator.hasNext()) {
            this.a(((Number)iterator.next()).doubleValue());
        }
    }
    
    public void c(final Iterator iterator) {
        while (iterator.hasNext()) {
            this.a(iterator.next().doubleValue());
        }
    }
    
    public void d(final double... array) {
        for (int length = array.length, i = 0; i < length; ++i) {
            this.a(array[i]);
        }
    }
    
    public void e(final int... array) {
        for (int length = array.length, i = 0; i < length; ++i) {
            this.a(array[i]);
        }
    }
    
    public void f(final long... array) {
        for (int length = array.length, i = 0; i < length; ++i) {
            this.a((double)array[i]);
        }
    }
    
    public Stats h() {
        return new Stats(this.a, this.b, this.c, this.d, this.e);
    }
}

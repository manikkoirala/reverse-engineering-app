import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.d;

// 
// Decompiled by Procyon v0.6.0
// 

public class n61
{
    public String a;
    public ee1 b;
    public y01 c;
    
    public n61(final ee1 b, final y01 c) {
        final StringBuilder sb = new StringBuilder();
        sb.append("http://");
        sb.append(a91.u());
        sb.append(":8758/scanCount");
        this.a = sb.toString();
        this.b = b;
        this.c = c;
        this.b();
    }
    
    public final void b() {
        final hr1 hr1 = new hr1(this, 1, this.a, new d.b(this) {
            public final n61 a;
            
            public void b(final String s) {
                try {
                    this.a.c(s);
                }
                catch (final Exception ex) {
                    ex.printStackTrace();
                    this.a.c(null);
                }
            }
        }, new d.a(this) {
            public final n61 a;
            
            @Override
            public void a(final VolleyError volleyError) {
                this.a.c(null);
            }
        }) {
            public final n61 w;
            
            @Override
            public byte[] k() {
                return null;
            }
            
            @Override
            public String l() {
                return "application/json";
            }
        };
        hr1.L(new wq(20000, 0, 1.0f));
        this.b.a(hr1);
    }
    
    public final void c(final Object o) {
        final y01 c = this.c;
        if (c != null) {
            c.a(o);
        }
    }
}

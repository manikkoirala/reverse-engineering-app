import android.app.Dialog;
import java.io.IOException;
import com.ekodroid.omrevaluator.database.StudentDataModel;
import java.io.Reader;
import java.io.InputStreamReader;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import java.util.ArrayList;
import java.io.InputStream;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class ho
{
    public y01 a;
    public Context b;
    public int c;
    public int d;
    public int e;
    public int f;
    public int g;
    public InputStream h;
    public String i;
    public String j;
    public ho k;
    public ArrayList l;
    
    public ho(final Context b, final InputStream h, final y01 a) {
        this.c = -1;
        this.d = -1;
        this.e = -1;
        this.f = -1;
        this.g = -1;
        this.k = this;
        this.a = a;
        this.b = b;
        this.h = h;
        this.l = new ArrayList();
        new b(null).execute((Object[])new Void[0]);
    }
    
    public static /* synthetic */ Context a(final ho ho) {
        return ho.b;
    }
    
    public static /* synthetic */ InputStream b(final ho ho) {
        return ho.h;
    }
    
    public static /* synthetic */ int c(final ho ho) {
        return ho.d;
    }
    
    public static /* synthetic */ int d(final ho ho, final int d) {
        return ho.d = d;
    }
    
    public static /* synthetic */ int e(final ho ho) {
        return ho.c;
    }
    
    public static /* synthetic */ int f(final ho ho, final int c) {
        return ho.c = c;
    }
    
    public static /* synthetic */ int g(final ho ho) {
        return ho.e;
    }
    
    public static /* synthetic */ int h(final ho ho, final int e) {
        return ho.e = e;
    }
    
    public static /* synthetic */ int i(final ho ho) {
        return ho.f;
    }
    
    public static /* synthetic */ int j(final ho ho, final int f) {
        return ho.f = f;
    }
    
    public static /* synthetic */ int k(final ho ho) {
        return ho.g;
    }
    
    public static /* synthetic */ int l(final ho ho, final int g) {
        return ho.g = g;
    }
    
    public static /* synthetic */ y01 m(final ho ho) {
        return ho.a;
    }
    
    public class b extends AsyncTask
    {
        public int a;
        public int b;
        public ProgressDialog c;
        public final ho d;
        
        public b(final ho d) {
            this.d = d;
            this.a = 0;
            this.b = 0;
        }
        
        public Void a(final Void... array) {
            if (ho.b(this.d) == null) {
                final StringBuilder sb = new StringBuilder();
                final ho d = this.d;
                sb.append(d.i);
                sb.append("Header not found, make sure you are importing comma separated format");
                d.i = sb.toString();
                return null;
            }
            try {
                final yd yd = new yd((Reader)new InputStreamReader(ho.b(this.d)));
                final String[] c = yd.c();
                for (int i = 0; i < c.length; ++i) {
                    if (c[i].toUpperCase().contains("NAME")) {
                        ho.d(this.d, i);
                    }
                    else if (c[i].toUpperCase().contains("ROLLNO")) {
                        ho.f(this.d, i);
                    }
                    else if (c[i].toUpperCase().contains("EMAILID")) {
                        ho.h(this.d, i);
                    }
                    else if (c[i].toUpperCase().contains("PHONENO")) {
                        ho.j(this.d, i);
                    }
                    else if (c[i].toUpperCase().contains("CLASS")) {
                        ho.l(this.d, i);
                    }
                }
                if (ho.c(this.d) == -1) {
                    final StringBuilder sb2 = new StringBuilder();
                    final ho d2 = this.d;
                    sb2.append(d2.i);
                    sb2.append("column NAME not found in file");
                    d2.i = sb2.toString();
                    return null;
                }
                if (ho.e(this.d) == -1) {
                    final StringBuilder sb3 = new StringBuilder();
                    final ho d3 = this.d;
                    sb3.append(d3.i);
                    sb3.append("column ROLLNO not found in file");
                    d3.i = sb3.toString();
                    return null;
                }
                if (ho.k(this.d) == -1) {
                    final StringBuilder sb4 = new StringBuilder();
                    final ho d4 = this.d;
                    sb4.append(d4.i);
                    sb4.append("column CLASS not found in file");
                    d4.i = sb4.toString();
                    return null;
                }
                while (true) {
                    final String[] c2 = yd.c();
                    if (c2 == null) {
                        break;
                    }
                    int n;
                    if (c2.length - 1 >= ho.c(this.d) && c2[ho.c(this.d)].trim().length() >= 1) {
                        final String s = c2[ho.c(this.d)];
                        if (c2.length - 1 >= ho.e(this.d) && c2[ho.e(this.d)].matches("[0-9]+")) {
                            final int int1 = Integer.parseInt(c2[ho.e(this.d)]);
                            if (c2.length - 1 >= ho.k(this.d) && c2[ho.k(this.d)].trim().length() >= 1) {
                                final String s2 = c2[ho.k(this.d)];
                                String s3;
                                if (ho.g(this.d) != -1 && c2.length > ho.g(this.d) && c2[ho.g(this.d)].trim().indexOf(64) > 0 && c2[ho.g(this.d)].length() > 5) {
                                    s3 = c2[ho.g(this.d)];
                                }
                                else {
                                    s3 = "";
                                }
                                String s4;
                                if (ho.i(this.d) != -1 && c2.length > ho.i(this.d) && c2[ho.i(this.d)].matches("[0-9]+") && c2[ho.i(this.d)].length() > 7) {
                                    s4 = c2[ho.i(this.d)];
                                }
                                else {
                                    s4 = "";
                                }
                                this.d.l.add(new StudentDataModel(int1, s, s3, s4, s2));
                                n = this.a;
                            }
                            else {
                                ++this.b;
                                n = this.a;
                            }
                        }
                        else {
                            ++this.b;
                            n = this.a;
                        }
                    }
                    else {
                        ++this.b;
                        n = this.a;
                    }
                    this.a = n + 1;
                }
                final ho d5 = this.d;
                final StringBuilder sb5 = new StringBuilder();
                sb5.append("Valid rows : ");
                sb5.append(this.a - this.b);
                sb5.append(", Invalid rows : ");
                sb5.append(this.b);
                d5.j = sb5.toString();
            }
            catch (final IOException ex) {
                ex.printStackTrace();
            }
            return null;
        }
        
        public void b(final Void void1) {
            if (((Dialog)this.c).isShowing()) {
                ((Dialog)this.c).dismiss();
            }
            if (ho.m(this.d) != null) {
                ho.m(this.d).a(this.d.k);
            }
        }
        
        public void onPreExecute() {
            (this.c = new ProgressDialog(ho.a(this.d))).setMessage((CharSequence)"Importing Csv file, please wait...");
            ((Dialog)this.c).setCancelable(false);
            ((Dialog)this.c).show();
        }
    }
}

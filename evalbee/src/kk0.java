import java.util.concurrent.TimeUnit;
import java.util.concurrent.Executor;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

// 
// Decompiled by Procyon v0.6.0
// 

public class kk0 extends FutureTask implements ik0
{
    public final py a;
    
    public kk0(final Callable callable) {
        super(callable);
        this.a = new py();
    }
    
    public static kk0 a(final Callable callable) {
        return new kk0(callable);
    }
    
    @Override
    public void addListener(final Runnable runnable, final Executor executor) {
        this.a.a(runnable, executor);
    }
    
    public void done() {
        this.a.b();
    }
    
    @Override
    public Object get(final long n, final TimeUnit unit) {
        final long nanos = unit.toNanos(n);
        if (nanos <= 2147483647999999999L) {
            return super.get(n, unit);
        }
        return super.get(Math.min(nanos, 2147483647999999999L), TimeUnit.NANOSECONDS);
    }
}

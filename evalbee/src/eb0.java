import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

// 
// Decompiled by Procyon v0.6.0
// 

public class eb0
{
    public static volatile eb0 b;
    public final Set a;
    
    public eb0() {
        this.a = new HashSet();
    }
    
    public static eb0 a() {
        final eb0 b;
        if ((b = eb0.b) == null) {
            synchronized (eb0.class) {
                if (eb0.b == null) {
                    eb0.b = new eb0();
                }
            }
        }
        return b;
    }
    
    public Set b() {
        synchronized (this.a) {
            return Collections.unmodifiableSet((Set<?>)this.a);
        }
    }
}

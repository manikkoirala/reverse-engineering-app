import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.tasks.TaskCompletionSource;

// 
// Decompiled by Procyon v0.6.0
// 

public class js implements Runnable
{
    public jq1 a;
    public TaskCompletionSource b;
    public mz c;
    
    public js(final jq1 a, final TaskCompletionSource b) {
        Preconditions.checkNotNull(a);
        Preconditions.checkNotNull(b);
        this.a = a;
        this.b = b;
        final o30 j = a.j();
        this.c = new mz(j.a().l(), j.c(), j.b(), j.l());
    }
    
    @Override
    public void run() {
        final is is = new is(this.a.k(), this.a.e());
        this.c.d(is);
        is.a(this.b, null);
    }
}

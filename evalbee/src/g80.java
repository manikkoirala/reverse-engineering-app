import android.os.Parcelable;
import android.util.AttributeSet;
import android.content.Context;
import android.view.View;
import androidx.fragment.app.k;
import android.view.MenuInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.content.res.Configuration;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.h;

// 
// Decompiled by Procyon v0.6.0
// 

public class g80
{
    public final h a;
    
    public g80(final h a) {
        this.a = a;
    }
    
    public static g80 b(final h h) {
        return new g80((h)l71.h(h, "callbacks == null"));
    }
    
    public void a(final Fragment fragment) {
        final h a = this.a;
        a.e.k(a, a, fragment);
    }
    
    public void c() {
        this.a.e.y();
    }
    
    public void d(final Configuration configuration) {
        this.a.e.A(configuration);
    }
    
    public boolean e(final MenuItem menuItem) {
        return this.a.e.B(menuItem);
    }
    
    public void f() {
        this.a.e.C();
    }
    
    public boolean g(final Menu menu, final MenuInflater menuInflater) {
        return this.a.e.D(menu, menuInflater);
    }
    
    public void h() {
        this.a.e.E();
    }
    
    public void i() {
        this.a.e.G();
    }
    
    public void j(final boolean b) {
        this.a.e.H(b);
    }
    
    public boolean k(final MenuItem menuItem) {
        return this.a.e.J(menuItem);
    }
    
    public void l(final Menu menu) {
        this.a.e.K(menu);
    }
    
    public void m() {
        this.a.e.M();
    }
    
    public void n(final boolean b) {
        this.a.e.N(b);
    }
    
    public boolean o(final Menu menu) {
        return this.a.e.O(menu);
    }
    
    public void p() {
        this.a.e.Q();
    }
    
    public void q() {
        this.a.e.R();
    }
    
    public void r() {
        this.a.e.T();
    }
    
    public boolean s() {
        return this.a.e.a0(true);
    }
    
    public k t() {
        return this.a.e;
    }
    
    public void u() {
        this.a.e.T0();
    }
    
    public View v(final View view, final String s, final Context context, final AttributeSet set) {
        return this.a.e.u0().onCreateView(view, s, context, set);
    }
    
    public void w(final Parcelable parcelable) {
        final h a = this.a;
        if (a instanceof c42) {
            a.e.g1(parcelable);
            return;
        }
        throw new IllegalStateException("Your FragmentHostCallback must implement ViewModelStoreOwner to call restoreSaveState(). Call restoreAllState()  if you're still using retainNestedNonConfig().");
    }
    
    public Parcelable x() {
        return this.a.e.i1();
    }
}

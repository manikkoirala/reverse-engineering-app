import java.util.concurrent.Callable;
import java.util.ArrayList;
import java.util.concurrent.Executor;
import android.content.pm.PackageManager$NameNotFoundException;
import android.os.CancellationSignal;
import android.graphics.Typeface;
import android.content.Context;
import java.util.concurrent.ExecutorService;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class i70
{
    public static final jm0 a;
    public static final ExecutorService b;
    public static final Object c;
    public static final co1 d;
    
    static {
        a = new jm0(16);
        b = ce1.a("fonts-androidx", 10, 10000);
        c = new Object();
        d = new co1();
    }
    
    public static String a(final g70 g70, final int i) {
        final StringBuilder sb = new StringBuilder();
        sb.append(g70.d());
        sb.append("-");
        sb.append(i);
        return sb.toString();
    }
    
    public static int b(final k70.a a) {
        final int c = a.c();
        final int n = -3;
        final boolean b = true;
        if (c == 0) {
            final k70.b[] b2 = a.b();
            int n2 = b ? 1 : 0;
            if (b2 != null) {
                if (b2.length == 0) {
                    n2 = (b ? 1 : 0);
                }
                else {
                    final int length = b2.length;
                    final int n3 = 0;
                    int n4 = 0;
                    while (true) {
                        n2 = n3;
                        if (n4 >= length) {
                            break;
                        }
                        int b3 = b2[n4].b();
                        if (b3 != 0) {
                            if (b3 < 0) {
                                b3 = n;
                            }
                            return b3;
                        }
                        ++n4;
                    }
                }
            }
            return n2;
        }
        if (a.c() != 1) {
            return -3;
        }
        return -2;
    }
    
    public static e c(final String s, final Context context, final g70 g70, final int n) {
        final jm0 a = i70.a;
        final Typeface typeface = (Typeface)a.get(s);
        if (typeface != null) {
            return new e(typeface);
        }
        try {
            final k70.a e = f70.e(context, g70, null);
            final int b = b(e);
            if (b != 0) {
                return new e(b);
            }
            final Typeface b2 = sz1.b(context, null, e.b(), n);
            if (b2 != null) {
                a.put(s, b2);
                return new e(b2);
            }
            return new e(-3);
        }
        catch (final PackageManager$NameNotFoundException ex) {
            return new e(-1);
        }
    }
    
    public static Typeface d(final Context context, final g70 g70, final int n, final Executor executor, final re re) {
        final String a = a(g70, n);
        final Typeface typeface = (Typeface)i70.a.get(a);
        if (typeface != null) {
            re.b(new e(typeface));
            return typeface;
        }
        final dl dl = new dl(re) {
            public final re a;
            
            public void a(final e e) {
                e e2 = e;
                if (e == null) {
                    e2 = new e(-3);
                }
                this.a.b(e2);
            }
        };
        synchronized (i70.c) {
            final co1 d = i70.d;
            final ArrayList list = (ArrayList)d.get(a);
            if (list != null) {
                list.add(dl);
                return null;
            }
            final ArrayList<i70$b> list2 = new ArrayList<i70$b>();
            list2.add(dl);
            d.put(a, list2);
            monitorexit(i70.c);
            final Callable callable = new Callable(a, context, g70, n) {
                public final String a;
                public final Context b;
                public final g70 c;
                public final int d;
                
                public e a() {
                    try {
                        return i70.c(this.a, this.b, this.c, this.d);
                    }
                    finally {
                        return new e(-3);
                    }
                }
            };
            Executor b;
            if ((b = executor) == null) {
                b = i70.b;
            }
            ce1.b(b, callable, new dl(a) {
                public final String a;
                
                public void a(final e e) {
                    synchronized (i70.c) {
                        final co1 d = i70.d;
                        final ArrayList list = (ArrayList)d.get(this.a);
                        if (list == null) {
                            return;
                        }
                        d.remove(this.a);
                        monitorexit(i70.c);
                        for (int i = 0; i < list.size(); ++i) {
                            ((dl)list.get(i)).accept(e);
                        }
                    }
                }
            });
            return null;
        }
    }
    
    public static Typeface e(final Context context, final g70 g70, final re re, final int n, final int n2) {
        final String a = a(g70, n);
        final Typeface typeface = (Typeface)i70.a.get(a);
        if (typeface != null) {
            re.b(new e(typeface));
            return typeface;
        }
        if (n2 == -1) {
            final e c = c(a, context, g70, n);
            re.b(c);
            return c.a;
        }
        final Callable callable = new Callable(a, context, g70, n) {
            public final String a;
            public final Context b;
            public final g70 c;
            public final int d;
            
            public e a() {
                return i70.c(this.a, this.b, this.c, this.d);
            }
        };
        try {
            final e e = (e)ce1.c(i70.b, callable, n2);
            re.b(e);
            return e.a;
        }
        catch (final InterruptedException ex) {
            re.b(new e(-3));
            return null;
        }
    }
    
    public static final class e
    {
        public final Typeface a;
        public final int b;
        
        public e(final int b) {
            this.a = null;
            this.b = b;
        }
        
        public e(final Typeface a) {
            this.a = a;
            this.b = 0;
        }
        
        public boolean a() {
            return this.b == 0;
        }
    }
}

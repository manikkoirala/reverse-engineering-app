import java.util.Collections;
import java.util.ArrayList;
import java.util.List;
import java.util.Comparator;

// 
// Decompiled by Procyon v0.6.0
// 

public class ld
{
    public static final Comparator e;
    public final List a;
    public final List b;
    public int c;
    public final int d;
    
    static {
        e = new Comparator() {
            public int a(final byte[] array, final byte[] array2) {
                return array.length - array2.length;
            }
        };
    }
    
    public ld(final int d) {
        this.a = new ArrayList();
        this.b = new ArrayList(64);
        this.c = 0;
        this.d = d;
    }
    
    public byte[] a(final int n) {
        monitorenter(this);
        int i = 0;
        try {
            while (i < this.b.size()) {
                final byte[] array = this.b.get(i);
                if (array.length >= n) {
                    this.c -= array.length;
                    this.b.remove(i);
                    this.a.remove(array);
                    return array;
                }
                ++i;
            }
            return new byte[n];
        }
        finally {
            monitorexit(this);
        }
    }
    
    public void b(final byte[] key) {
        monitorenter(this);
        if (key != null) {
            try {
                if (key.length <= this.d) {
                    this.a.add(key);
                    final int binarySearch = Collections.binarySearch(this.b, key, ld.e);
                    int n;
                    if ((n = binarySearch) < 0) {
                        n = -binarySearch - 1;
                    }
                    this.b.add(n, key);
                    this.c += key.length;
                    this.c();
                    return;
                }
            }
            finally {
                monitorexit(this);
            }
        }
        monitorexit(this);
    }
    
    public final void c() {
        synchronized (this) {
            while (this.c > this.d) {
                final byte[] array = this.a.remove(0);
                this.b.remove(array);
                this.c -= array.length;
            }
        }
    }
}

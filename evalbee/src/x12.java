import java.util.List;
import java.util.Collections;
import java.util.Iterator;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Set;
import com.google.firebase.firestore.core.UserData$Source;

// 
// Decompiled by Procyon v0.6.0
// 

public class x12
{
    public final UserData$Source a;
    public final Set b;
    public final ArrayList c;
    
    public x12(final UserData$Source a) {
        this.a = a;
        this.b = new HashSet();
        this.c = new ArrayList();
    }
    
    public void b(final s00 s00) {
        this.b.add(s00);
    }
    
    public void c(final s00 s00, final oy1 oy1) {
        this.c.add(new u00(s00, oy1));
    }
    
    public boolean d(final s00 s00) {
        final Iterator iterator = this.b.iterator();
        while (iterator.hasNext()) {
            if (s00.k((jb)iterator.next())) {
                return true;
            }
        }
        final Iterator iterator2 = this.c.iterator();
        while (iterator2.hasNext()) {
            if (s00.k(((u00)iterator2.next()).a())) {
                return true;
            }
        }
        return false;
    }
    
    public y12 e() {
        return new y12(this, s00.c, false, null);
    }
    
    public z12 f(final a11 a11) {
        return new z12(a11, q00.b(this.b), Collections.unmodifiableList((List<?>)this.c));
    }
    
    public z12 g(final a11 a11, final q00 q00) {
        final ArrayList list = new ArrayList();
        for (final u00 e : this.c) {
            if (q00.a(e.a())) {
                list.add(e);
            }
        }
        return new z12(a11, q00, Collections.unmodifiableList((List<?>)list));
    }
    
    public z12 h(final a11 a11) {
        return new z12(a11, null, Collections.unmodifiableList((List<?>)this.c));
    }
    
    public a22 i(final a11 a11) {
        return new a22(a11, q00.b(this.b), Collections.unmodifiableList((List<?>)this.c));
    }
}

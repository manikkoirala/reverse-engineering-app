import com.google.firebase.sessions.LogEnvironment;

// 
// Decompiled by Procyon v0.6.0
// 

public final class y7
{
    public final String a;
    public final String b;
    public final String c;
    public final String d;
    public final LogEnvironment e;
    public final r4 f;
    
    public y7(final String a, final String b, final String c, final String d, final LogEnvironment e, final r4 f) {
        fg0.e((Object)a, "appId");
        fg0.e((Object)b, "deviceModel");
        fg0.e((Object)c, "sessionSdkVersion");
        fg0.e((Object)d, "osVersion");
        fg0.e((Object)e, "logEnvironment");
        fg0.e((Object)f, "androidAppInfo");
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
    }
    
    public final r4 a() {
        return this.f;
    }
    
    public final String b() {
        return this.a;
    }
    
    public final String c() {
        return this.b;
    }
    
    public final LogEnvironment d() {
        return this.e;
    }
    
    public final String e() {
        return this.d;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof y7)) {
            return false;
        }
        final y7 y7 = (y7)o;
        return fg0.a((Object)this.a, (Object)y7.a) && fg0.a((Object)this.b, (Object)y7.b) && fg0.a((Object)this.c, (Object)y7.c) && fg0.a((Object)this.d, (Object)y7.d) && this.e == y7.e && fg0.a((Object)this.f, (Object)y7.f);
    }
    
    public final String f() {
        return this.c;
    }
    
    @Override
    public int hashCode() {
        return ((((this.a.hashCode() * 31 + this.b.hashCode()) * 31 + this.c.hashCode()) * 31 + this.d.hashCode()) * 31 + this.e.hashCode()) * 31 + this.f.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ApplicationInfo(appId=");
        sb.append(this.a);
        sb.append(", deviceModel=");
        sb.append(this.b);
        sb.append(", sessionSdkVersion=");
        sb.append(this.c);
        sb.append(", osVersion=");
        sb.append(this.d);
        sb.append(", logEnvironment=");
        sb.append(this.e);
        sb.append(", androidAppInfo=");
        sb.append(this.f);
        sb.append(')');
        return sb.toString();
    }
}

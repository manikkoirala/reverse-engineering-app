// 
// Decompiled by Procyon v0.6.0
// 

public final class x82
{
    public final String a;
    public final int b;
    
    public x82(final String a, final int b) {
        fg0.e((Object)a, "workSpecId");
        this.a = a;
        this.b = b;
    }
    
    public final int a() {
        return this.b;
    }
    
    public final String b() {
        return this.a;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof x82)) {
            return false;
        }
        final x82 x82 = (x82)o;
        return fg0.a((Object)this.a, (Object)x82.a) && this.b == x82.b;
    }
    
    @Override
    public int hashCode() {
        return this.a.hashCode() * 31 + Integer.hashCode(this.b);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("WorkGenerationalId(workSpecId=");
        sb.append(this.a);
        sb.append(", generation=");
        sb.append(this.b);
        sb.append(')');
        return sb.toString();
    }
}

import android.database.Cursor;
import java.util.concurrent.locks.Lock;
import android.content.Intent;
import android.content.Context;
import android.util.Log;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Iterator;
import android.database.sqlite.SQLiteException;
import java.util.Collection;
import java.io.Closeable;
import android.os.CancellationSignal;
import java.util.Set;
import kotlin.collections.b;
import java.util.Locale;
import java.util.LinkedHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.Map;
import androidx.room.RoomDatabase;

// 
// Decompiled by Procyon v0.6.0
// 

public class lg0
{
    public static final a q;
    public static final String[] r;
    public final RoomDatabase a;
    public final Map b;
    public final Map c;
    public final Map d;
    public final String[] e;
    public y9 f;
    public final AtomicBoolean g;
    public volatile boolean h;
    public volatile ws1 i;
    public final b j;
    public final jg0 k;
    public final si1 l;
    public mx0 m;
    public final Object n;
    public final Object o;
    public final Runnable p;
    
    static {
        q = new a(null);
        r = new String[] { "UPDATE", "DELETE", "INSERT" };
    }
    
    public lg0(final RoomDatabase a, final Map b, final Map c, final String... array) {
        fg0.e((Object)a, "database");
        fg0.e((Object)b, "shadowTablesMap");
        fg0.e((Object)c, "viewTables");
        fg0.e((Object)array, "tableNames");
        this.a = a;
        this.b = b;
        this.c = c;
        int i = 0;
        this.g = new AtomicBoolean(false);
        this.j = new b(array.length);
        this.k = new jg0(a);
        this.l = new si1();
        this.n = new Object();
        this.o = new Object();
        this.d = new LinkedHashMap();
        final int length = array.length;
        final String[] e = new String[length];
        while (i < length) {
            final String s = array[i];
            final Locale us = Locale.US;
            fg0.d((Object)us, "US");
            final String lowerCase = s.toLowerCase(us);
            fg0.d((Object)lowerCase, "this as java.lang.String).toLowerCase(locale)");
            this.d.put(lowerCase, i);
            final String s2 = this.b.get(array[i]);
            String lowerCase2;
            if (s2 != null) {
                fg0.d((Object)us, "US");
                lowerCase2 = s2.toLowerCase(us);
                fg0.d((Object)lowerCase2, "this as java.lang.String).toLowerCase(locale)");
            }
            else {
                lowerCase2 = null;
            }
            if (lowerCase2 == null) {
                lowerCase2 = lowerCase;
            }
            e[i] = lowerCase2;
            ++i;
        }
        this.e = e;
        for (final Map.Entry<K, String> entry : this.b.entrySet()) {
            final String s3 = entry.getValue();
            final Locale us2 = Locale.US;
            fg0.d((Object)us2, "US");
            final String lowerCase3 = s3.toLowerCase(us2);
            fg0.d((Object)lowerCase3, "this as java.lang.String).toLowerCase(locale)");
            if (this.d.containsKey(lowerCase3)) {
                final String s4 = (String)entry.getKey();
                fg0.d((Object)us2, "US");
                final String lowerCase4 = s4.toLowerCase(us2);
                fg0.d((Object)lowerCase4, "this as java.lang.String).toLowerCase(locale)");
                final Map d = this.d;
                d.put(lowerCase4, kotlin.collections.b.i(d, (Object)lowerCase3));
            }
        }
        this.p = new Runnable(this) {
            public final lg0 a;
            
            public final Set a() {
                final lg0 a = this.a;
                final Set b = sm1.b();
                Object o = RoomDatabase.B(a.e(), new do1("SELECT * FROM room_table_modification_log WHERE invalidated = 1;"), null, 2, null);
                try {
                    while (((Cursor)o).moveToNext()) {
                        b.add(((Cursor)o).getInt(0));
                    }
                    final u02 a2 = u02.a;
                    dh.a((Closeable)o, (Throwable)null);
                    o = sm1.a(b);
                    if (((Collection)o).isEmpty() ^ true) {
                        if (this.a.d() == null) {
                            throw new IllegalStateException("Required value was null.".toString());
                        }
                        final ws1 d = this.a.d();
                        if (d == null) {
                            throw new IllegalArgumentException("Required value was null.".toString());
                        }
                        d.p();
                    }
                    return (Set)o;
                }
                finally {
                    try {}
                    finally {
                        final Throwable t;
                        dh.a((Closeable)o, t);
                    }
                }
            }
            
            @Override
            public void run() {
                Object o = this.a.e().l();
                ((Lock)o).lock();
            Block_20_Outer:
                while (true) {
                    Label_0305: {
                        try {
                            if (!this.a.c()) {
                                ((Lock)o).unlock();
                                o = lg0.a(this.a);
                                if (o != null) {
                                    ((y9)o).e();
                                }
                                return;
                            }
                            if (!this.a.g().compareAndSet(true, false)) {
                                ((Lock)o).unlock();
                                o = lg0.a(this.a);
                                if (o != null) {
                                    ((y9)o).e();
                                }
                                return;
                            }
                            if (this.a.e().t()) {
                                ((Lock)o).unlock();
                                o = lg0.a(this.a);
                                if (o != null) {
                                    ((y9)o).e();
                                }
                                return;
                            }
                            Object o2 = this.a.e().n().F();
                            ((ss1)o2).r();
                            try {
                                final Set a = this.a();
                                ((ss1)o2).S();
                                ((ss1)o2).X();
                                ((Lock)o).unlock();
                                o2 = lg0.a(this.a);
                                o = a;
                                if (o2 != null) {
                                    o = a;
                                    final ss1 ss1 = (ss1)o2;
                                    ((y9)ss1).e();
                                }
                                break Label_0305;
                            }
                            finally {
                                ((ss1)o2).X();
                            }
                        }
                        catch (final SQLiteException ex) {}
                        catch (final IllegalStateException ex2) {}
                        finally {
                            ((Lock)o).unlock();
                            o = lg0.a(this.a);
                            if (o != null) {
                                ((y9)o).e();
                            }
                            final Object o2;
                            o = o2;
                            continue Block_20_Outer;
                            Label_0396: {
                                return;
                            }
                            o = o2;
                            continue Block_20_Outer;
                            while (true) {
                                final si1 f = this.a.f();
                                final lg0 a2 = this.a;
                                synchronized (f) {
                                    final Iterator<Object> iterator = a2.f().iterator();
                                    while (iterator.hasNext()) {
                                        iterator.next().getValue().b((Set)o);
                                    }
                                    final u02 a3 = u02.a;
                                }
                                return;
                                iftrue(Label_0396:)(!(((Collection)o).isEmpty() ^ true));
                                continue;
                            }
                        }
                    }
                    break;
                }
            }
        };
    }
    
    public static final /* synthetic */ y9 a(final lg0 lg0) {
        return lg0.f;
    }
    
    public void b(final c c) {
        fg0.e((Object)c, "observer");
        final String[] n = this.n(c.a());
        final ArrayList list = new ArrayList(n.length);
        for (final String str : n) {
            final Map d = this.d;
            final Locale us = Locale.US;
            fg0.d((Object)us, "US");
            final String lowerCase = str.toLowerCase(us);
            fg0.d((Object)lowerCase, "this as java.lang.String).toLowerCase(locale)");
            final Integer n2 = d.get(lowerCase);
            if (n2 == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("There is no table with name ");
                sb.append(str);
                throw new IllegalArgumentException(sb.toString());
            }
            list.add((Object)n2);
        }
        final int[] o = vh.O((Collection)list);
        final d d2 = new d(c, o, n);
        synchronized (this.l) {
            final d d3 = (d)this.l.l(c, d2);
            monitorexit(this.l);
            if (d3 == null && this.j.b(Arrays.copyOf(o, o.length))) {
                this.s();
            }
        }
    }
    
    public final boolean c() {
        if (!this.a.z()) {
            return false;
        }
        if (!this.h) {
            this.a.n().F();
        }
        if (!this.h) {
            Log.e("ROOM", "database is not initialized even though it is open");
            return false;
        }
        return true;
    }
    
    public final ws1 d() {
        return this.i;
    }
    
    public final RoomDatabase e() {
        return this.a;
    }
    
    public final si1 f() {
        return this.l;
    }
    
    public final AtomicBoolean g() {
        return this.g;
    }
    
    public final Map h() {
        return this.d;
    }
    
    public final void i(final ss1 ss1) {
        fg0.e((Object)ss1, "database");
        synchronized (this.o) {
            if (this.h) {
                Log.e("ROOM", "Invalidation tracker is initialized twice :/.");
                return;
            }
            ss1.M("PRAGMA temp_store = MEMORY;");
            ss1.M("PRAGMA recursive_triggers='ON';");
            ss1.M("CREATE TEMP TABLE room_table_modification_log (table_id INTEGER PRIMARY KEY, invalidated INTEGER NOT NULL DEFAULT 0)");
            this.t(ss1);
            this.i = ss1.d0("UPDATE room_table_modification_log SET invalidated = 0 WHERE invalidated = 1");
            this.h = true;
            final u02 a = u02.a;
        }
    }
    
    public final void j(final String... array) {
        fg0.e((Object)array, "tables");
        synchronized (this.l) {
            for (final Map.Entry<c, V> entry : this.l) {
                fg0.d((Object)entry, "(observer, wrapper)");
                final c c = entry.getKey();
                final d d = (d)entry.getValue();
                if (!c.b()) {
                    d.c(array);
                }
            }
            final u02 a = u02.a;
        }
    }
    
    public final void k() {
        synchronized (this.o) {
            this.h = false;
            this.j.d();
            final u02 a = u02.a;
        }
    }
    
    public void l() {
        if (this.g.compareAndSet(false, true)) {
            final y9 f = this.f;
            if (f != null) {
                f.j();
            }
            this.a.o().execute(this.p);
        }
    }
    
    public void m(final c c) {
        fg0.e((Object)c, "observer");
        Object o = this.l;
        synchronized (o) {
            final d d = (d)this.l.m(c);
            monitorexit(o);
            if (d != null) {
                o = this.j;
                final int[] a = d.a();
                if (((b)o).c(Arrays.copyOf(a, a.length))) {
                    this.s();
                }
            }
        }
    }
    
    public final String[] n(final String[] array) {
        final Set b = sm1.b();
        for (final String s : array) {
            final Map c = this.c;
            final Locale us = Locale.US;
            fg0.d((Object)us, "US");
            final String lowerCase = s.toLowerCase(us);
            fg0.d((Object)lowerCase, "this as java.lang.String).toLowerCase(locale)");
            if (c.containsKey(lowerCase)) {
                final Map c2 = this.c;
                fg0.d((Object)us, "US");
                final String lowerCase2 = s.toLowerCase(us);
                fg0.d((Object)lowerCase2, "this as java.lang.String).toLowerCase(locale)");
                final Object value = c2.get(lowerCase2);
                fg0.b(value);
                b.addAll((Collection)value);
            }
            else {
                b.add(s);
            }
        }
        final String[] array2 = sm1.a(b).toArray(new String[0]);
        fg0.c((Object)array2, "null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
        return array2;
    }
    
    public final void o(final y9 f) {
        fg0.e((Object)f, "autoCloser");
        (this.f = f).m(new kg0(this));
    }
    
    public final void p(final Context context, final String s, final Intent intent) {
        fg0.e((Object)context, "context");
        fg0.e((Object)s, "name");
        fg0.e((Object)intent, "serviceIntent");
        this.m = new mx0(context, s, intent, this, this.a.o());
    }
    
    public final void q(final ss1 ss1, final int n) {
        final StringBuilder sb = new StringBuilder();
        sb.append("INSERT OR IGNORE INTO room_table_modification_log VALUES(");
        sb.append(n);
        sb.append(", 0)");
        ss1.M(sb.toString());
        final String str = this.e[n];
        for (final String str2 : lg0.r) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("CREATE TEMP TRIGGER IF NOT EXISTS ");
            sb2.append(lg0.q.b(str, str2));
            sb2.append(" AFTER ");
            sb2.append(str2);
            sb2.append(" ON `");
            sb2.append(str);
            sb2.append("` BEGIN UPDATE ");
            sb2.append("room_table_modification_log");
            sb2.append(" SET ");
            sb2.append("invalidated");
            sb2.append(" = 1");
            sb2.append(" WHERE ");
            sb2.append("table_id");
            sb2.append(" = ");
            sb2.append(n);
            sb2.append(" AND ");
            sb2.append("invalidated");
            sb2.append(" = 0");
            sb2.append("; END");
            final String string = sb2.toString();
            fg0.d((Object)string, "StringBuilder().apply(builderAction).toString()");
            ss1.M(string);
        }
    }
    
    public final void r(final ss1 ss1, int i) {
        final String s = this.e[i];
        final String[] r = lg0.r;
        int length;
        String s2;
        StringBuilder sb;
        String string;
        for (length = r.length, i = 0; i < length; ++i) {
            s2 = r[i];
            sb = new StringBuilder();
            sb.append("DROP TRIGGER IF EXISTS ");
            sb.append(lg0.q.b(s, s2));
            string = sb.toString();
            fg0.d((Object)string, "StringBuilder().apply(builderAction).toString()");
            ss1.M(string);
        }
    }
    
    public final void s() {
        if (!this.a.z()) {
            return;
        }
        this.t(this.a.n().F());
    }
    
    public final void t(ss1 a) {
        fg0.e((Object)a, "database");
        if (((ss1)a).j0()) {
            return;
        }
        try {
            final Lock l = this.a.l();
            l.lock();
            try {
                synchronized (this.n) {
                    final int[] a2 = this.j.a();
                    if (a2 == null) {
                        return;
                    }
                    lg0.q.a((ss1)a);
                    try {
                        for (int length = a2.length, i = 0, n = 0; i < length; ++i, ++n) {
                            final int n2 = a2[i];
                            if (n2 != 1) {
                                if (n2 == 2) {
                                    this.r((ss1)a, n);
                                }
                            }
                            else {
                                this.q((ss1)a, n);
                            }
                        }
                        ((ss1)a).S();
                        ((ss1)a).X();
                        a = (SQLiteException)u02.a;
                    }
                    finally {
                        ((ss1)a).X();
                    }
                }
            }
            finally {
                l.unlock();
            }
        }
        catch (final SQLiteException a) {}
        catch (final IllegalStateException ex) {}
        Log.e("ROOM", "Cannot run invalidation tracker. Is the db closed?", (Throwable)a);
    }
    
    public static final class a
    {
        public final void a(final ss1 ss1) {
            fg0.e((Object)ss1, "database");
            if (ss1.k0()) {
                ss1.r();
            }
            else {
                ss1.m();
            }
        }
        
        public final String b(final String str, final String str2) {
            fg0.e((Object)str, "tableName");
            fg0.e((Object)str2, "triggerType");
            final StringBuilder sb = new StringBuilder();
            sb.append("`room_table_modification_trigger_");
            sb.append(str);
            sb.append('_');
            sb.append(str2);
            sb.append('`');
            return sb.toString();
        }
    }
    
    public static final class b
    {
        public static final a e;
        public final long[] a;
        public final boolean[] b;
        public final int[] c;
        public boolean d;
        
        static {
            e = new a(null);
        }
        
        public b(final int n) {
            this.a = new long[n];
            this.b = new boolean[n];
            this.c = new int[n];
        }
        
        public final int[] a() {
            synchronized (this) {
                if (!this.d) {
                    return null;
                }
                final long[] a = this.a;
                for (int length = a.length, i = 0, n = 0; i < length; ++i, ++n) {
                    final long n2 = a[i];
                    int n3 = 1;
                    final boolean b = n2 > 0L;
                    final boolean[] b2 = this.b;
                    if (b != b2[n]) {
                        final int[] c = this.c;
                        if (!b) {
                            n3 = 2;
                        }
                        c[n] = n3;
                    }
                    else {
                        this.c[n] = 0;
                    }
                    b2[n] = b;
                }
                this.d = false;
                return this.c.clone();
            }
        }
        
        public final boolean b(final int... array) {
            fg0.e((Object)array, "tableIds");
            synchronized (this) {
                final int length = array.length;
                int i = 0;
                boolean b = false;
                while (i < length) {
                    final int n = array[i];
                    final long[] a = this.a;
                    final long n2 = a[n];
                    a[n] = 1L + n2;
                    if (n2 == 0L) {
                        b = true;
                        this.d = true;
                    }
                    ++i;
                }
                final u02 a2 = u02.a;
                return b;
            }
        }
        
        public final boolean c(final int... array) {
            fg0.e((Object)array, "tableIds");
            synchronized (this) {
                final int length = array.length;
                int i = 0;
                boolean b = false;
                while (i < length) {
                    if (this.a[array[i]]-- == 1L) {
                        b = true;
                        this.d = true;
                    }
                    ++i;
                }
                final u02 a = u02.a;
                return b;
            }
        }
        
        public final void d() {
            synchronized (this) {
                Arrays.fill(this.b, false);
                this.d = true;
                final u02 a = u02.a;
            }
        }
        
        public static final class a
        {
        }
    }
    
    public abstract static class c
    {
        public final String[] a;
        
        public c(final String[] a) {
            fg0.e((Object)a, "tables");
            this.a = a;
        }
        
        public final String[] a() {
            return this.a;
        }
        
        public boolean b() {
            return false;
        }
        
        public abstract void c(final Set p0);
    }
    
    public static final class d
    {
        public final c a;
        public final int[] b;
        public final String[] c;
        public final Set d;
        
        public d(final c a, final int[] b, final String[] c) {
            fg0.e((Object)a, "observer");
            fg0.e((Object)b, "tableIds");
            fg0.e((Object)c, "tableNames");
            this.a = a;
            this.b = b;
            this.c = c;
            final int length = c.length;
            final int n = 1;
            Set d;
            if (length == 0 ^ true) {
                d = sm1.d((Object)c[0]);
            }
            else {
                d = tm1.e();
            }
            this.d = d;
            int n2;
            if (b.length == c.length) {
                n2 = n;
            }
            else {
                n2 = 0;
            }
            if (n2 != 0) {
                return;
            }
            throw new IllegalStateException("Check failed.".toString());
        }
        
        public final int[] a() {
            return this.b;
        }
        
        public final void b(Set set) {
            fg0.e((Object)set, "invalidatedTablesIds");
            final int[] b = this.b;
            final int length = b.length;
            Label_0127: {
                if (length != 0) {
                    int i = 0;
                    if (length != 1) {
                        final Set b2 = sm1.b();
                        final int[] b3 = this.b;
                        for (int length2 = b3.length, n = 0; i < length2; ++i, ++n) {
                            if (set.contains(b3[i])) {
                                b2.add(this.c[n]);
                            }
                        }
                        set = sm1.a(b2);
                        break Label_0127;
                    }
                    if (set.contains(b[0])) {
                        set = this.d;
                        break Label_0127;
                    }
                }
                set = tm1.e();
            }
            if (set.isEmpty() ^ true) {
                this.a.c(set);
            }
        }
        
        public final void c(final String[] array) {
            fg0.e((Object)array, "tables");
            final int length = this.c.length;
            Set set = null;
            Label_0171: {
                if (length != 0) {
                    final int n = 0;
                    if (length != 1) {
                        final Set b = sm1.b();
                        for (final String s : array) {
                            for (final String s2 : this.c) {
                                if (qr1.k(s2, s, true)) {
                                    b.add(s2);
                                }
                            }
                        }
                        set = sm1.a(b);
                        break Label_0171;
                    }
                    final int length4 = array.length;
                    int n2 = 0;
                    int n3;
                    while (true) {
                        n3 = n;
                        if (n2 >= length4) {
                            break;
                        }
                        if (qr1.k(array[n2], this.c[0], true)) {
                            n3 = 1;
                            break;
                        }
                        ++n2;
                    }
                    if (n3 != 0) {
                        set = this.d;
                        break Label_0171;
                    }
                }
                set = tm1.e();
            }
            if (set.isEmpty() ^ true) {
                this.a.c(set);
            }
        }
    }
}

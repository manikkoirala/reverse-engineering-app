import java.util.Collection;
import androidx.work.WorkInfo$State;
import java.util.LinkedList;
import androidx.work.d;
import java.util.Iterator;
import androidx.work.impl.WorkDatabase;
import java.util.UUID;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class bf implements Runnable
{
    public final x11 a;
    
    public bf() {
        this.a = new x11();
    }
    
    public static bf b(final UUID uuid, final c92 c92) {
        return new bf(c92, uuid) {
            public final c92 b;
            public final UUID c;
            
            @Override
            public void h() {
                final WorkDatabase o = this.b.o();
                o.e();
                try {
                    this.a(this.b, this.c.toString());
                    o.D();
                    o.i();
                    this.g(this.b);
                }
                finally {
                    o.i();
                }
            }
        };
    }
    
    public static bf c(final String s, final c92 c92, final boolean b) {
        return new bf(c92, s, b) {
            public final c92 b;
            public final String c;
            public final boolean d;
            
            @Override
            public void h() {
                final WorkDatabase o = this.b.o();
                o.e();
                try {
                    final Iterator iterator = o.K().c(this.c).iterator();
                    while (iterator.hasNext()) {
                        this.a(this.b, (String)iterator.next());
                    }
                    o.D();
                    o.i();
                    if (this.d) {
                        this.g(this.b);
                    }
                }
                finally {
                    o.i();
                }
            }
        };
    }
    
    public static bf d(final String s, final c92 c92) {
        return new bf(c92, s) {
            public final c92 b;
            public final String c;
            
            @Override
            public void h() {
                final WorkDatabase o = this.b.o();
                o.e();
                try {
                    final Iterator iterator = o.K().f(this.c).iterator();
                    while (iterator.hasNext()) {
                        this.a(this.b, (String)iterator.next());
                    }
                    o.D();
                    o.i();
                    this.g(this.b);
                }
                finally {
                    o.i();
                }
            }
        };
    }
    
    public void a(final c92 c92, final String s) {
        this.f(c92.o(), s);
        c92.l().t(s, 1);
        final Iterator iterator = c92.m().iterator();
        while (iterator.hasNext()) {
            ((ij1)iterator.next()).e(s);
        }
    }
    
    public d e() {
        return this.a;
    }
    
    public final void f(final WorkDatabase workDatabase, final String e) {
        final q92 k = workDatabase.K();
        final qs f = workDatabase.F();
        final LinkedList list = new LinkedList();
        list.add(e);
        while (!list.isEmpty()) {
            final String s = list.remove();
            final WorkInfo$State d = k.d(s);
            if (d != WorkInfo$State.SUCCEEDED && d != WorkInfo$State.FAILED) {
                k.e(s);
            }
            list.addAll(f.a(s));
        }
    }
    
    public void g(final c92 c92) {
        nj1.h(c92.h(), c92.o(), c92.m());
    }
    
    public abstract void h();
    
    @Override
    public void run() {
        try {
            this.h();
            this.a.a((d.b)d.a);
        }
        finally {
            final Throwable t;
            this.a.a((d.b)new d.b.a(t));
        }
    }
}

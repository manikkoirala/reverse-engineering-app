import com.google.firebase.encoders.EncodingException;
import java.util.HashMap;
import java.io.IOException;
import java.io.ByteArrayOutputStream;
import com.google.firebase.encoders.proto.b;
import java.io.OutputStream;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public class o91
{
    public final Map a;
    public final Map b;
    public final w01 c;
    
    public o91(final Map a, final Map b, final w01 c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public static a a() {
        return new a();
    }
    
    public void b(final Object o, final OutputStream outputStream) {
        new b(outputStream, this.a, this.b, this.c).t(o);
    }
    
    public byte[] c(final Object o) {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            this.b(o, byteArrayOutputStream);
            return byteArrayOutputStream.toByteArray();
        }
        catch (final IOException ex) {
            return byteArrayOutputStream.toByteArray();
        }
    }
    
    public static final class a implements zw
    {
        public static final w01 d;
        public final Map a;
        public final Map b;
        public w01 c;
        
        static {
            d = new n91();
        }
        
        public a() {
            this.a = new HashMap();
            this.b = new HashMap();
            this.c = a.d;
        }
        
        public o91 c() {
            return new o91(new HashMap(this.a), new HashMap(this.b), this.c);
        }
        
        public a d(final jk jk) {
            jk.configure(this);
            return this;
        }
        
        public a f(final Class clazz, final w01 w01) {
            this.a.put(clazz, w01);
            this.b.remove(clazz);
            return this;
        }
    }
}

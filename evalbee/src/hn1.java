import android.net.Uri;
import android.os.Parcelable;
import android.content.Intent;
import androidx.core.content.FileProvider;
import android.view.View;
import androidx.appcompat.app.a;
import android.view.View$OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import java.io.File;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class hn1
{
    public Context a;
    public String b;
    public String c;
    
    public hn1(final Context a, final String b, final String c) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.b();
    }
    
    public static /* synthetic */ String a(final hn1 hn1) {
        return hn1.c;
    }
    
    public final void b() {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(this.a, 2131951953);
        final View inflate = ((LayoutInflater)this.a.getSystemService("layout_inflater")).inflate(2131492973, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        materialAlertDialogBuilder.setTitle(2131886312);
        final File file = new File(this.b);
        final TextView textView = (TextView)inflate.findViewById(2131297231);
        final StringBuilder sb = new StringBuilder();
        sb.append(this.a.getResources().getString(2131886311));
        sb.append(" : ");
        sb.append(ok.c);
        sb.append("/");
        sb.append(this.c);
        textView.setText((CharSequence)sb.toString());
        final Button button = (Button)inflate.findViewById(2131296476);
        final Button button2 = (Button)inflate.findViewById(2131296482);
        final Button button3 = (Button)inflate.findViewById(2131296424);
        final a create = materialAlertDialogBuilder.create();
        ((View)button).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, file, create) {
            public final File a;
            public final a b;
            public final hn1 c;
            
            public void onClick(final View view) {
                if (this.a.exists()) {
                    final Uri f = FileProvider.f(this.c.a, "com.ekodroid.omrevaluator.fileprovider", this.a);
                    final Intent intent = new Intent("android.intent.action.SEND");
                    intent.setFlags(268435456);
                    intent.putExtra("android.intent.extra.STREAM", (Parcelable)f);
                    intent.setType("application/vnd.ms-excel");
                    final StringBuilder sb = new StringBuilder();
                    sb.append(hn1.a(this.c));
                    sb.append(" Result");
                    intent.putExtra("android.intent.extra.SUBJECT", sb.toString());
                    intent.setFlags(1073741824);
                    intent.setFlags(1);
                    this.c.a.startActivity(Intent.createChooser(intent, (CharSequence)"Share with"));
                }
                this.b.dismiss();
            }
        });
        ((View)button2).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, file) {
            public final File a;
            public final hn1 b;
            
            public void onClick(final View view) {
                final Intent intent = new Intent("android.intent.action.VIEW");
                final Uri f = FileProvider.f(this.b.a, "com.ekodroid.omrevaluator.fileprovider", this.a);
                intent.putExtra("android.intent.extra.STREAM", (Parcelable)f);
                intent.setDataAndType(f, "application/vnd.ms-excel");
                intent.setFlags(1073741824);
                intent.setFlags(1);
                if (this.b.a.getPackageManager().queryIntentActivities(intent, 0).size() > 0) {
                    this.b.a.startActivity(intent);
                }
                else {
                    a91.G(this.b.a, 2131886500, 2131230909, 2131231086);
                }
            }
        });
        ((View)button3).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, create) {
            public final a a;
            public final hn1 b;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
        create.show();
    }
}

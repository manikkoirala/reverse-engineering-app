import androidx.work.NetworkType;
import androidx.work.impl.constraints.controllers.ConstraintController;

// 
// Decompiled by Procyon v0.6.0
// 

public final class wy0 extends ConstraintController
{
    public static final a c;
    public static final String d;
    public final int b;
    
    static {
        c = new a(null);
        final String i = xl0.i("NetworkNotRoamingCtrlr");
        fg0.d((Object)i, "tagWithPrefix(\"NetworkNotRoamingCtrlr\")");
        d = i;
    }
    
    public wy0(final tk tk) {
        fg0.e((Object)tk, "tracker");
        super(tk);
        this.b = 7;
    }
    
    @Override
    public int b() {
        return this.b;
    }
    
    @Override
    public boolean c(final p92 p) {
        fg0.e((Object)p, "workSpec");
        return p.j.d() == NetworkType.NOT_ROAMING;
    }
    
    public boolean g(final zy0 zy0) {
        fg0.e((Object)zy0, "value");
        return !zy0.a() || !zy0.c();
    }
    
    public static final class a
    {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

public class vl0 implements t90
{
    @Override
    public double a(final double[] array, final int n) {
        if (n == 1) {
            return Math.log(array[0]) / Math.log(10.0);
        }
        return Math.log(array[0]) / Math.log(array[1]);
    }
    
    @Override
    public boolean b(final int n) {
        boolean b = true;
        if (n != 1) {
            b = (n == 2 && b);
        }
        return b;
    }
    
    @Override
    public String toString() {
        return "log(x):log(x, y)";
    }
}

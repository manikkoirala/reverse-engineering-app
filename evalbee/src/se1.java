import com.google.firebase.components.DependencyException;
import java.util.Iterator;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

// 
// Decompiled by Procyon v0.6.0
// 

public final class se1 implements gj
{
    public final Set a;
    public final Set b;
    public final Set c;
    public final Set d;
    public final Set e;
    public final Set f;
    public final gj g;
    
    public se1(final zi zi, final gj g) {
        final HashSet s = new HashSet();
        final HashSet s2 = new HashSet();
        final HashSet s3 = new HashSet();
        final HashSet s4 = new HashSet();
        final HashSet s5 = new HashSet();
        for (final os os : zi.g()) {
            if (os.e()) {
                final boolean g2 = os.g();
                final da1 c = os.c();
                if (g2) {
                    s4.add(c);
                }
                else {
                    s.add(c);
                }
            }
            else if (os.d()) {
                s3.add(os.c());
            }
            else {
                final boolean g3 = os.g();
                final da1 c2 = os.c();
                if (g3) {
                    s5.add(c2);
                }
                else {
                    s2.add(c2);
                }
            }
        }
        if (!zi.k().isEmpty()) {
            s.add(da1.b(y91.class));
        }
        this.a = Collections.unmodifiableSet((Set<?>)s);
        this.b = Collections.unmodifiableSet((Set<?>)s2);
        this.c = Collections.unmodifiableSet((Set<?>)s3);
        this.d = Collections.unmodifiableSet((Set<?>)s4);
        this.e = Collections.unmodifiableSet((Set<?>)s5);
        this.f = zi.k();
        this.g = g;
    }
    
    @Override
    public Object a(final Class clazz) {
        if (!this.a.contains(da1.b(clazz))) {
            throw new DependencyException(String.format("Attempting to request an undeclared dependency %s.", clazz));
        }
        final Object a = this.g.a(clazz);
        if (!clazz.equals(y91.class)) {
            return a;
        }
        return new a(this.f, (y91)a);
    }
    
    @Override
    public r91 b(final da1 da1) {
        if (this.e.contains(da1)) {
            return this.g.b(da1);
        }
        throw new DependencyException(String.format("Attempting to request an undeclared dependency Provider<Set<%s>>.", da1));
    }
    
    @Override
    public Object d(final da1 da1) {
        if (this.a.contains(da1)) {
            return this.g.d(da1);
        }
        throw new DependencyException(String.format("Attempting to request an undeclared dependency %s.", da1));
    }
    
    @Override
    public r91 e(final Class clazz) {
        return this.g(da1.b(clazz));
    }
    
    @Override
    public jr f(final da1 da1) {
        if (this.c.contains(da1)) {
            return this.g.f(da1);
        }
        throw new DependencyException(String.format("Attempting to request an undeclared dependency Deferred<%s>.", da1));
    }
    
    @Override
    public r91 g(final da1 da1) {
        if (this.b.contains(da1)) {
            return this.g.g(da1);
        }
        throw new DependencyException(String.format("Attempting to request an undeclared dependency Provider<%s>.", da1));
    }
    
    @Override
    public Set h(final da1 da1) {
        if (this.d.contains(da1)) {
            return this.g.h(da1);
        }
        throw new DependencyException(String.format("Attempting to request an undeclared dependency Set<%s>.", da1));
    }
    
    @Override
    public jr i(final Class clazz) {
        return this.f(da1.b(clazz));
    }
    
    public static class a implements y91
    {
        public final Set a;
        public final y91 b;
        
        public a(final Set a, final y91 b) {
            this.a = a;
            this.b = b;
        }
    }
}

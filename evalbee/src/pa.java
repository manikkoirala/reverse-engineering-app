// 
// Decompiled by Procyon v0.6.0
// 

public final class pa extends nf1
{
    public final String b;
    public final String c;
    public final String d;
    public final String e;
    public final long f;
    
    public pa(final String b, final String c, final String d, final String e, final long f) {
        if (b == null) {
            throw new NullPointerException("Null rolloutId");
        }
        this.b = b;
        if (c == null) {
            throw new NullPointerException("Null parameterKey");
        }
        this.c = c;
        if (d == null) {
            throw new NullPointerException("Null parameterValue");
        }
        this.d = d;
        if (e != null) {
            this.e = e;
            this.f = f;
            return;
        }
        throw new NullPointerException("Null variantId");
    }
    
    @Override
    public String c() {
        return this.c;
    }
    
    @Override
    public String d() {
        return this.d;
    }
    
    @Override
    public String e() {
        return this.b;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof nf1) {
            final nf1 nf1 = (nf1)o;
            if (!this.b.equals(nf1.e()) || !this.c.equals(nf1.c()) || !this.d.equals(nf1.d()) || !this.e.equals(nf1.g()) || this.f != nf1.f()) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public long f() {
        return this.f;
    }
    
    @Override
    public String g() {
        return this.e;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.b.hashCode();
        final int hashCode2 = this.c.hashCode();
        final int hashCode3 = this.d.hashCode();
        final int hashCode4 = this.e.hashCode();
        final long f = this.f;
        return ((((hashCode ^ 0xF4243) * 1000003 ^ hashCode2) * 1000003 ^ hashCode3) * 1000003 ^ hashCode4) * 1000003 ^ (int)(f ^ f >>> 32);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("RolloutAssignment{rolloutId=");
        sb.append(this.b);
        sb.append(", parameterKey=");
        sb.append(this.c);
        sb.append(", parameterValue=");
        sb.append(this.d);
        sb.append(", variantId=");
        sb.append(this.e);
        sb.append(", templateVersion=");
        sb.append(this.f);
        sb.append("}");
        return sb.toString();
    }
}

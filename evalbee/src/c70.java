import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterators;
import java.util.Iterator;
import com.google.common.base.Optional;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class c70 implements Iterable
{
    public final Optional a;
    
    public c70() {
        this.a = Optional.absent();
    }
    
    public c70(final Iterable iterable) {
        this.a = Optional.of(iterable);
    }
    
    public static c70 a(final Iterable iterable, final Iterable iterable2) {
        return b(iterable, iterable2);
    }
    
    public static c70 b(final Iterable... array) {
        for (int length = array.length, i = 0; i < length; ++i) {
            i71.r(array[i]);
        }
        return new c70(array) {
            public final Iterable[] b;
            
            @Override
            public Iterator iterator() {
                return Iterators.e(new o(this, this.b.length) {
                    public final c70$b c;
                    
                    public Iterator c(final int n) {
                        return this.c.b[n].iterator();
                    }
                });
            }
        };
    }
    
    public static c70 g(final Iterable iterable) {
        c70 c70;
        if (iterable instanceof c70) {
            c70 = (c70)iterable;
        }
        else {
            c70 = new c70(iterable, iterable) {
                public final Iterable b;
                
                @Override
                public Iterator iterator() {
                    return this.b.iterator();
                }
            };
        }
        return c70;
    }
    
    public final c70 c(final m71 m71) {
        return g(rg0.d(this.i(), m71));
    }
    
    public final Iterable i() {
        return this.a.or(this);
    }
    
    public final ImmutableSet l() {
        return ImmutableSet.copyOf((Iterable<?>)this.i());
    }
    
    @Override
    public String toString() {
        return rg0.m(this.i());
    }
}

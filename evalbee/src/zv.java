import android.widget.CompoundButton;
import android.widget.AdapterView;
import android.content.SharedPreferences;
import android.widget.SpinnerAdapter;
import java.util.List;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.CheckBox;
import android.widget.TextView;
import android.os.Bundle;
import android.view.View;
import android.view.View$OnClickListener;
import androidx.appcompat.widget.Toolbar;
import com.ekodroid.omrevaluator.templateui.models.QueEditDataModel;
import android.content.Context;
import java.util.ArrayList;

// 
// Decompiled by Procyon v0.6.0
// 

public class zv extends u80
{
    public ArrayList a;
    public ArrayList b;
    public Context c;
    public d d;
    public QueEditDataModel e;
    
    public zv(final Context c, final d d, final QueEditDataModel e) {
        super(c);
        this.c = c;
        this.d = d;
        this.e = e;
    }
    
    public static /* synthetic */ QueEditDataModel a(final zv zv) {
        return zv.e;
    }
    
    public static /* synthetic */ d b(final zv zv) {
        return zv.d;
    }
    
    public final int c(final ArrayList list, final double n) {
        for (int i = 0; i < list.size(); ++i) {
            if ((double)list.get(i) == n) {
                return i;
            }
        }
        return 0;
    }
    
    public final void d() {
        ((Toolbar)this.findViewById(2131297342)).setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final zv a;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492969);
        this.d();
        final SharedPreferences sharedPreferences = this.c.getSharedPreferences("MyPref", 0);
        this.a = a91.o(sharedPreferences.getString("plus_mark_json", ""), ok.I);
        this.b = a91.m(sharedPreferences.getString("minus_mark_json", ""), ok.J);
        final TextView textView = (TextView)this.findViewById(2131297302);
        final CheckBox checkBox = (CheckBox)this.findViewById(2131296499);
        final LinearLayout linearLayout = (LinearLayout)this.findViewById(2131296784);
        final LinearLayout linearLayout2 = (LinearLayout)this.findViewById(2131296785);
        if (this.e.i() == QueEditDataModel.ViewType.LABEL) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.e.h());
            sb.append(" - ");
            sb.append(this.e.f());
            textView.setText((CharSequence)sb.toString());
            ((View)linearLayout2).setVisibility(8);
        }
        else {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(this.e.h());
            sb2.append(" - ");
            sb2.append(this.e.f());
            sb2.append(" - ");
            sb2.append(this.c.getResources().getString(2131886720));
            sb2.append(" ");
            sb2.append(this.e.d());
            textView.setText((CharSequence)sb2.toString());
            ((View)linearLayout).setVisibility(8);
        }
        final EditText editText = (EditText)this.findViewById(2131296591);
        final EditText editText2 = (EditText)this.findViewById(2131296588);
        final Spinner spinner = (Spinner)this.findViewById(2131297077);
        final Spinner spinner2 = (Spinner)this.findViewById(2131297084);
        final CheckBox checkBox2 = (CheckBox)this.findViewById(2131296512);
        ((TextView)editText).setText((CharSequence)this.e.h());
        ((TextView)editText2).setText((CharSequence)this.e.f());
        final ArrayAdapter adapter = new ArrayAdapter(this.c, 2131493104, (List)this.a);
        final ArrayAdapter adapter2 = new ArrayAdapter(this.c, 2131493104, (List)this.b);
        spinner.setAdapter((SpinnerAdapter)adapter);
        spinner2.setAdapter((SpinnerAdapter)adapter2);
        ((AdapterView)spinner).setSelection(this.c(this.a, this.e.a()));
        ((AdapterView)spinner2).setSelection(this.c(this.b, this.e.b()));
        ((CompoundButton)checkBox2).setChecked(this.e.j());
        this.findViewById(2131296464).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, editText, editText2, spinner, spinner2, checkBox2, checkBox) {
            public final EditText a;
            public final EditText b;
            public final Spinner c;
            public final Spinner d;
            public final CheckBox e;
            public final CheckBox f;
            public final zv g;
            
            public void onClick(final View view) {
                QueEditDataModel queEditDataModel;
                d d;
                boolean checked;
                if (zv.a(this.g).i() == QueEditDataModel.ViewType.LABEL) {
                    queEditDataModel = new QueEditDataModel(zv.a(this.g).i(), this.a.getText().toString(), this.b.getText().toString(), zv.a(this.g).d(), zv.a(this.g).g(), zv.a(this.g).e(), zv.a(this.g).a(), zv.a(this.g).b(), zv.a(this.g).j(), zv.a(this.g).c());
                    d = zv.b(this.g);
                    checked = true;
                }
                else {
                    queEditDataModel = new QueEditDataModel(zv.a(this.g).i(), zv.a(this.g).h(), zv.a(this.g).f(), zv.a(this.g).d(), zv.a(this.g).g(), zv.a(this.g).e(), this.g.a.get(((AdapterView)this.c).getSelectedItemPosition()), this.g.b.get(((AdapterView)this.d).getSelectedItemPosition()), ((CompoundButton)this.e).isChecked(), zv.a(this.g).c());
                    d = zv.b(this.g);
                    checked = ((CompoundButton)this.f).isChecked();
                }
                d.a(queEditDataModel, checked);
                this.g.dismiss();
            }
        });
        this.findViewById(2131296428).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final zv a;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
    }
    
    public interface d
    {
        void a(final QueEditDataModel p0, final boolean p1);
    }
}

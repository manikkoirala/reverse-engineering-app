import java.util.Locale;
import android.os.LocaleList;

// 
// Decompiled by Procyon v0.6.0
// 

public final class pl0 implements ol0
{
    public final LocaleList a;
    
    public pl0(final Object o) {
        this.a = (LocaleList)o;
    }
    
    @Override
    public String a() {
        return this.a.toLanguageTags();
    }
    
    @Override
    public Object b() {
        return this.a;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this.a.equals(((ol0)o).b());
    }
    
    @Override
    public Locale get(final int n) {
        return this.a.get(n);
    }
    
    @Override
    public int hashCode() {
        return this.a.hashCode();
    }
    
    @Override
    public boolean isEmpty() {
        return this.a.isEmpty();
    }
    
    @Override
    public int size() {
        return this.a.size();
    }
    
    @Override
    public String toString() {
        return this.a.toString();
    }
}

import android.net.Uri;
import java.util.LinkedHashSet;
import java.util.Collection;
import java.util.Set;
import androidx.work.NetworkType;

// 
// Decompiled by Procyon v0.6.0
// 

public final class zk
{
    public static final b i;
    public static final zk j;
    public final NetworkType a;
    public final boolean b;
    public final boolean c;
    public final boolean d;
    public final boolean e;
    public final long f;
    public final long g;
    public final Set h;
    
    static {
        i = new b(null);
        j = new zk(null, false, false, false, 15, null);
    }
    
    public zk(final NetworkType networkType, final boolean b, final boolean b2, final boolean b3) {
        fg0.e((Object)networkType, "requiredNetworkType");
        this(networkType, b, false, b2, b3);
    }
    
    public zk(final NetworkType networkType, final boolean b, final boolean b2, final boolean b3, final boolean b4) {
        fg0.e((Object)networkType, "requiredNetworkType");
        this(networkType, b, b2, b3, b4, -1L, 0L, null, 192, null);
    }
    
    public zk(final NetworkType a, final boolean b, final boolean c, final boolean d, final boolean e, final long f, final long g, final Set h) {
        fg0.e((Object)a, "requiredNetworkType");
        fg0.e((Object)h, "contentUriTriggers");
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = h;
    }
    
    public zk(final zk zk) {
        fg0.e((Object)zk, "other");
        this.b = zk.b;
        this.c = zk.c;
        this.a = zk.a;
        this.d = zk.d;
        this.e = zk.e;
        this.h = zk.h;
        this.f = zk.f;
        this.g = zk.g;
    }
    
    public final long a() {
        return this.g;
    }
    
    public final long b() {
        return this.f;
    }
    
    public final Set c() {
        return this.h;
    }
    
    public final NetworkType d() {
        return this.a;
    }
    
    public final boolean e() {
        final boolean empty = this.h.isEmpty();
        boolean b = true;
        if (!(empty ^ true)) {
            b = false;
        }
        return b;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        boolean a;
        final boolean b = a = false;
        if (o != null) {
            if (!fg0.a((Object)zk.class, (Object)o.getClass())) {
                a = b;
            }
            else {
                final zk zk = (zk)o;
                if (this.b != zk.b) {
                    return false;
                }
                if (this.c != zk.c) {
                    return false;
                }
                if (this.d != zk.d) {
                    return false;
                }
                if (this.e != zk.e) {
                    return false;
                }
                if (this.f != zk.f) {
                    return false;
                }
                if (this.g != zk.g) {
                    return false;
                }
                if (this.a != zk.a) {
                    a = b;
                }
                else {
                    a = fg0.a((Object)this.h, (Object)zk.h);
                }
            }
        }
        return a;
    }
    
    public final boolean f() {
        return this.d;
    }
    
    public final boolean g() {
        return this.b;
    }
    
    public final boolean h() {
        return this.c;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.a.hashCode();
        final int b = this.b ? 1 : 0;
        final int c = this.c ? 1 : 0;
        final int d = this.d ? 1 : 0;
        final int e = this.e ? 1 : 0;
        final long f = this.f;
        final int n = (int)(f ^ f >>> 32);
        final long g = this.g;
        return ((((((hashCode * 31 + b) * 31 + c) * 31 + d) * 31 + e) * 31 + n) * 31 + (int)(g ^ g >>> 32)) * 31 + this.h.hashCode();
    }
    
    public final boolean i() {
        return this.e;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Constraints{requiredNetworkType=");
        sb.append(this.a);
        sb.append(", requiresCharging=");
        sb.append(this.b);
        sb.append(", requiresDeviceIdle=");
        sb.append(this.c);
        sb.append(", requiresBatteryNotLow=");
        sb.append(this.d);
        sb.append(", requiresStorageNotLow=");
        sb.append(this.e);
        sb.append(", contentTriggerUpdateDelayMillis=");
        sb.append(this.f);
        sb.append(", contentTriggerMaxDelayMillis=");
        sb.append(this.g);
        sb.append(", contentUriTriggers=");
        sb.append(this.h);
        sb.append(", }");
        return sb.toString();
    }
    
    public static final class a
    {
        public boolean a;
        public boolean b;
        public NetworkType c;
        public boolean d;
        public boolean e;
        public long f;
        public long g;
        public Set h;
        
        public a() {
            this.c = NetworkType.NOT_REQUIRED;
            this.f = -1L;
            this.g = -1L;
            this.h = new LinkedHashSet();
        }
        
        public final zk a() {
            return new zk(this.c, this.a, this.b, this.d, this.e, this.f, this.g, vh.S((Iterable)this.h));
        }
        
        public final a b(final NetworkType c) {
            fg0.e((Object)c, "networkType");
            this.c = c;
            return this;
        }
    }
    
    public static final class b
    {
    }
    
    public static final class c
    {
        public final Uri a;
        public final boolean b;
        
        public c(final Uri a, final boolean b) {
            fg0.e((Object)a, "uri");
            this.a = a;
            this.b = b;
        }
        
        public final Uri a() {
            return this.a;
        }
        
        public final boolean b() {
            return this.b;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            Class<?> class1;
            if (o != null) {
                class1 = o.getClass();
            }
            else {
                class1 = null;
            }
            if (!fg0.a((Object)c.class, (Object)class1)) {
                return false;
            }
            fg0.c(o, "null cannot be cast to non-null type androidx.work.Constraints.ContentUriTrigger");
            final c c = (c)o;
            return fg0.a((Object)this.a, (Object)c.a) && this.b == c.b;
        }
        
        @Override
        public int hashCode() {
            return this.a.hashCode() * 31 + Boolean.hashCode(this.b);
        }
    }
}

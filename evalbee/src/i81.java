import android.os.Build$VERSION;
import android.os.Process;
import java.util.Iterator;
import android.app.ActivityManager$RunningAppProcessInfo;
import java.util.ArrayList;
import android.app.ActivityManager;
import java.util.List;
import android.content.Context;
import com.google.firebase.crashlytics.internal.model.CrashlyticsReport;

// 
// Decompiled by Procyon v0.6.0
// 

public final class i81
{
    public static final i81 a;
    
    static {
        a = new i81();
    }
    
    public static /* synthetic */ CrashlyticsReport.e.d.a.c c(final i81 i81, final String s, int n, int n2, boolean b, final int n3, final Object o) {
        if ((n3 & 0x2) != 0x0) {
            n = 0;
        }
        if ((n3 & 0x4) != 0x0) {
            n2 = 0;
        }
        if ((n3 & 0x8) != 0x0) {
            b = false;
        }
        return i81.b(s, n, n2, b);
    }
    
    public final CrashlyticsReport.e.d.a.c a(final String s, final int n, final int n2) {
        fg0.e((Object)s, "processName");
        return c(this, s, n, n2, false, 8, null);
    }
    
    public final CrashlyticsReport.e.d.a.c b(final String s, final int n, final int n2, final boolean b) {
        fg0.e((Object)s, "processName");
        final CrashlyticsReport.e.d.a.c a = CrashlyticsReport.e.d.a.c.a().e(s).d(n).c(n2).b(b).a();
        fg0.d((Object)a, "builder()\n      .setProc\u2026ltProcess)\n      .build()");
        return a;
    }
    
    public final List d(final Context context) {
        fg0.e((Object)context, "context");
        final int uid = context.getApplicationInfo().uid;
        final String processName = context.getApplicationInfo().processName;
        final Object systemService = context.getSystemService("activity");
        final boolean b = systemService instanceof ActivityManager;
        List runningAppProcesses = null;
        ActivityManager activityManager;
        if (b) {
            activityManager = (ActivityManager)systemService;
        }
        else {
            activityManager = null;
        }
        if (activityManager != null) {
            runningAppProcesses = activityManager.getRunningAppProcesses();
        }
        List g;
        if ((g = runningAppProcesses) == null) {
            g = nh.g();
        }
        final Iterable iterable = vh.w((Iterable)g);
        final ArrayList list = new ArrayList();
        for (final Object next : iterable) {
            if (((ActivityManager$RunningAppProcessInfo)next).uid == uid) {
                list.add(next);
            }
        }
        final ArrayList list2 = new ArrayList(oh.o((Iterable)list, 10));
        for (final ActivityManager$RunningAppProcessInfo activityManager$RunningAppProcessInfo : list) {
            list2.add((Object)CrashlyticsReport.e.d.a.c.a().e(activityManager$RunningAppProcessInfo.processName).d(activityManager$RunningAppProcessInfo.pid).c(activityManager$RunningAppProcessInfo.importance).b(fg0.a((Object)activityManager$RunningAppProcessInfo.processName, (Object)processName)).a());
        }
        return list2;
    }
    
    public final CrashlyticsReport.e.d.a.c e(final Context context) {
        fg0.e((Object)context, "context");
        final int myPid = Process.myPid();
        while (true) {
            for (final Object next : this.d(context)) {
                if (((CrashlyticsReport.e.d.a.c)next).c() == myPid) {
                    CrashlyticsReport.e.d.a.c c;
                    if ((c = (CrashlyticsReport.e.d.a.c)next) == null) {
                        c = c(this, this.f(), myPid, 0, false, 12, null);
                    }
                    return c;
                }
            }
            Object next = null;
            continue;
        }
    }
    
    public final String f() {
        final int sdk_INT = Build$VERSION.SDK_INT;
        String s;
        if (sdk_INT >= 33) {
            s = g81.a();
            fg0.d((Object)s, "{\n      Process.myProcessName()\n    }");
        }
        else if (sdk_INT < 28 || (s = l81.a()) == null) {
            s = "";
        }
        return s;
    }
}

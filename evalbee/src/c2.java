import android.view.MenuItem;
import android.view.MenuInflater;
import android.view.Menu;
import android.view.View;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class c2
{
    public Object a;
    public boolean b;
    
    public abstract void a();
    
    public abstract View b();
    
    public abstract Menu c();
    
    public abstract MenuInflater d();
    
    public abstract CharSequence e();
    
    public Object f() {
        return this.a;
    }
    
    public abstract CharSequence g();
    
    public boolean h() {
        return this.b;
    }
    
    public abstract void i();
    
    public abstract boolean j();
    
    public abstract void k(final View p0);
    
    public abstract void l(final int p0);
    
    public abstract void m(final CharSequence p0);
    
    public void n(final Object a) {
        this.a = a;
    }
    
    public abstract void o(final int p0);
    
    public abstract void p(final CharSequence p0);
    
    public void q(final boolean b) {
        this.b = b;
    }
    
    public interface a
    {
        boolean a(final c2 p0, final MenuItem p1);
        
        void b(final c2 p0);
        
        boolean c(final c2 p0, final Menu p1);
        
        boolean d(final c2 p0, final Menu p1);
    }
}

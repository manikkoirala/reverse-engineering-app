import android.content.IntentFilter;
import android.content.Intent;
import android.content.Context;
import android.content.BroadcastReceiver;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class uc extends tk
{
    public final BroadcastReceiver f;
    
    public uc(final Context context, final hu1 hu1) {
        fg0.e((Object)context, "context");
        fg0.e((Object)hu1, "taskExecutor");
        super(context, hu1);
        this.f = new BroadcastReceiver(this) {
            public final uc a;
            
            public void onReceive(final Context context, final Intent intent) {
                fg0.e((Object)context, "context");
                fg0.e((Object)intent, "intent");
                this.a.k(intent);
            }
        };
    }
    
    @Override
    public void h() {
        final xl0 e = xl0.e();
        final String a = vc.a();
        final StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getSimpleName());
        sb.append(": registering receiver");
        e.a(a, sb.toString());
        this.d().registerReceiver(this.f, this.j());
    }
    
    @Override
    public void i() {
        final xl0 e = xl0.e();
        final String a = vc.a();
        final StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getSimpleName());
        sb.append(": unregistering receiver");
        e.a(a, sb.toString());
        this.d().unregisterReceiver(this.f);
    }
    
    public abstract IntentFilter j();
    
    public abstract void k(final Intent p0);
}

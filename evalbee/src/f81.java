// 
// Decompiled by Procyon v0.6.0
// 

public final class f81
{
    public final String a;
    public final int b;
    public final int c;
    public final boolean d;
    
    public f81(final String a, final int b, final int c, final boolean d) {
        fg0.e((Object)a, "processName");
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
    
    public final int a() {
        return this.c;
    }
    
    public final int b() {
        return this.b;
    }
    
    public final String c() {
        return this.a;
    }
    
    public final boolean d() {
        return this.d;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof f81)) {
            return false;
        }
        final f81 f81 = (f81)o;
        return fg0.a((Object)this.a, (Object)f81.a) && this.b == f81.b && this.c == f81.c && this.d == f81.d;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.a.hashCode();
        final int hashCode2 = Integer.hashCode(this.b);
        final int hashCode3 = Integer.hashCode(this.c);
        int d;
        if ((d = (this.d ? 1 : 0)) != 0) {
            d = 1;
        }
        return ((hashCode * 31 + hashCode2) * 31 + hashCode3) * 31 + d;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ProcessDetails(processName=");
        sb.append(this.a);
        sb.append(", pid=");
        sb.append(this.b);
        sb.append(", importance=");
        sb.append(this.c);
        sb.append(", isDefaultProcess=");
        sb.append(this.d);
        sb.append(')');
        return sb.toString();
    }
}

import java.util.List;
import java.util.Arrays;
import android.text.TextUtils;
import com.google.android.gms.common.api.Status;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class ob2
{
    public static Status a(final String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return new Status(17499);
        }
        final String[] split = s.split(":", 2);
        split[0] = split[0].trim();
        if (split.length > 1) {
            final String s2 = split[1];
            if (s2 != null) {
                split[1] = s2.trim();
            }
        }
        final List<String> list = Arrays.asList(split);
        if (list.size() > 1) {
            return b(list.get(0), list.get(1));
        }
        return b(list.get(0), null);
    }
    
    public static Status b(final String str, final String str2) {
        str.hashCode();
        final int hashCode = str.hashCode();
        int n = -1;
        switch (hashCode) {
            case 2082564316: {
                if (!str.equals("UNSUPPORTED_TENANT_OPERATION")) {
                    break;
                }
                n = 80;
                break;
            }
            case 2063209097: {
                if (!str.equals("EMAIL_CHANGE_NEEDS_VERIFICATION")) {
                    break;
                }
                n = 79;
                break;
            }
            case 1898790704: {
                if (!str.equals("MISSING_SESSION_INFO")) {
                    break;
                }
                n = 78;
                break;
            }
            case 1803454477: {
                if (!str.equals("MISSING_CONTINUE_URI")) {
                    break;
                }
                n = 77;
                break;
            }
            case 1497901284: {
                if (!str.equals("TOO_MANY_ATTEMPTS_TRY_LATER")) {
                    break;
                }
                n = 76;
                break;
            }
            case 1494923453: {
                if (!str.equals("INVALID_APP_CREDENTIAL")) {
                    break;
                }
                n = 75;
                break;
            }
            case 1442968770: {
                if (!str.equals("INVALID_PHONE_NUMBER")) {
                    break;
                }
                n = 74;
                break;
            }
            case 1433767024: {
                if (!str.equals("USER_DISABLED")) {
                    break;
                }
                n = 73;
                break;
            }
            case 1388786705: {
                if (!str.equals("INVALID_IDENTIFIER")) {
                    break;
                }
                n = 72;
                break;
            }
            case 1308491624: {
                if (!str.equals("MISSING_RECAPTCHA_TOKEN")) {
                    break;
                }
                n = 71;
                break;
            }
            case 1226505451: {
                if (!str.equals("FEDERATED_USER_ID_ALREADY_LINKED")) {
                    break;
                }
                n = 70;
                break;
            }
            case 1199811910: {
                if (!str.equals("MISSING_CODE")) {
                    break;
                }
                n = 69;
                break;
            }
            case 1141576252: {
                if (!str.equals("SESSION_EXPIRED")) {
                    break;
                }
                n = 68;
                break;
            }
            case 1113992697: {
                if (!str.equals("INVALID_RECAPTCHA_TOKEN")) {
                    break;
                }
                n = 67;
                break;
            }
            case 1107081238: {
                if (!str.equals("<<Network Error>>")) {
                    break;
                }
                n = 66;
                break;
            }
            case 1094975491: {
                if (!str.equals("INVALID_PASSWORD")) {
                    break;
                }
                n = 65;
                break;
            }
            case 1072360691: {
                if (!str.equals("INVALID_CUSTOM_TOKEN")) {
                    break;
                }
                n = 64;
                break;
            }
            case 1034932393: {
                if (!str.equals("INVALID_PENDING_TOKEN")) {
                    break;
                }
                n = 63;
                break;
            }
            case 989000548: {
                if (!str.equals("RESET_PASSWORD_EXCEED_LIMIT")) {
                    break;
                }
                n = 62;
                break;
            }
            case 922685102: {
                if (!str.equals("INVALID_MESSAGE_PAYLOAD")) {
                    break;
                }
                n = 61;
                break;
            }
            case 895302372: {
                if (!str.equals("MISSING_CLIENT_IDENTIFIER")) {
                    break;
                }
                n = 60;
                break;
            }
            case 886186878: {
                if (!str.equals("REQUIRES_SECOND_FACTOR_AUTH")) {
                    break;
                }
                n = 59;
                break;
            }
            case 844240628: {
                if (!str.equals("WEB_CONTEXT_CANCELED")) {
                    break;
                }
                n = 58;
                break;
            }
            case 819646646: {
                if (!str.equals("CREDENTIAL_MISMATCH")) {
                    break;
                }
                n = 57;
                break;
            }
            case 799258561: {
                if (!str.equals("INVALID_PROVIDER_ID")) {
                    break;
                }
                n = 56;
                break;
            }
            case 786916712: {
                if (!str.equals("INVALID_VERIFICATION_PROOF")) {
                    break;
                }
                n = 55;
                break;
            }
            case 745638750: {
                if (!str.equals("INVALID_MFA_PENDING_CREDENTIAL")) {
                    break;
                }
                n = 54;
                break;
            }
            case 605031096: {
                if (!str.equals("REJECTED_CREDENTIAL")) {
                    break;
                }
                n = 53;
                break;
            }
            case 582457886: {
                if (!str.equals("UNVERIFIED_EMAIL")) {
                    break;
                }
                n = 52;
                break;
            }
            case 542728406: {
                if (!str.equals("PASSWORD_LOGIN_DISABLED")) {
                    break;
                }
                n = 51;
                break;
            }
            case 530628231: {
                if (!str.equals("MISSING_RECAPTCHA_VERSION")) {
                    break;
                }
                n = 50;
                break;
            }
            case 492515765: {
                if (!str.equals("MISSING_CLIENT_TYPE")) {
                    break;
                }
                n = 49;
                break;
            }
            case 492072102: {
                if (!str.equals("WEB_STORAGE_UNSUPPORTED")) {
                    break;
                }
                n = 48;
                break;
            }
            case 491979549: {
                if (!str.equals("INVALID_ID_TOKEN")) {
                    break;
                }
                n = 47;
                break;
            }
            case 483847807: {
                if (!str.equals("EMAIL_EXISTS")) {
                    break;
                }
                n = 46;
                break;
            }
            case 429251986: {
                if (!str.equals("UNSUPPORTED_PASSTHROUGH_OPERATION")) {
                    break;
                }
                n = 45;
                break;
            }
            case 423563023: {
                if (!str.equals("MISSING_MFA_PENDING_CREDENTIAL")) {
                    break;
                }
                n = 44;
                break;
            }
            case 408411681: {
                if (!str.equals("INVALID_DYNAMIC_LINK_DOMAIN")) {
                    break;
                }
                n = 43;
                break;
            }
            case 278802867: {
                if (!str.equals("MISSING_PHONE_NUMBER")) {
                    break;
                }
                n = 42;
                break;
            }
            case 269327773: {
                if (!str.equals("INVALID_SENDER")) {
                    break;
                }
                n = 41;
                break;
            }
            case 210308040: {
                if (!str.equals("UNSUPPORTED_FIRST_FACTOR")) {
                    break;
                }
                n = 40;
                break;
            }
            case 15352275: {
                if (!str.equals("EMAIL_NOT_FOUND")) {
                    break;
                }
                n = 39;
                break;
            }
            case -40686718: {
                if (!str.equals("WEAK_PASSWORD")) {
                    break;
                }
                n = 38;
                break;
            }
            case -52772551: {
                if (!str.equals("CAPTCHA_CHECK_FAILED")) {
                    break;
                }
                n = 37;
                break;
            }
            case -75433118: {
                if (!str.equals("USER_NOT_FOUND")) {
                    break;
                }
                n = 36;
                break;
            }
            case -122667194: {
                if (!str.equals("MISSING_MFA_ENROLLMENT_ID")) {
                    break;
                }
                n = 35;
                break;
            }
            case -217128228: {
                if (!str.equals("SECOND_FACTOR_LIMIT_EXCEEDED")) {
                    break;
                }
                n = 34;
                break;
            }
            case -294485423: {
                if (!str.equals("WEB_INTERNAL_ERROR")) {
                    break;
                }
                n = 33;
                break;
            }
            case -333672188: {
                if (!str.equals("OPERATION_NOT_ALLOWED")) {
                    break;
                }
                n = 32;
                break;
            }
            case -380728810: {
                if (!str.equals("INVALID_RECAPTCHA_ACTION")) {
                    break;
                }
                n = 31;
                break;
            }
            case -406804866: {
                if (!str.equals("INVALID_LOGIN_CREDENTIALS")) {
                    break;
                }
                n = 30;
                break;
            }
            case -505579581: {
                if (!str.equals("INVALID_REQ_TYPE")) {
                    break;
                }
                n = 29;
                break;
            }
            case -595928767: {
                if (!str.equals("TIMEOUT")) {
                    break;
                }
                n = 28;
                break;
            }
            case -646022241: {
                if (!str.equals("CREDENTIAL_TOO_OLD_LOGIN_AGAIN")) {
                    break;
                }
                n = 27;
                break;
            }
            case -736207500: {
                if (!str.equals("MISSING_PASSWORD")) {
                    break;
                }
                n = 26;
                break;
            }
            case -749743758: {
                if (!str.equals("MFA_ENROLLMENT_NOT_FOUND")) {
                    break;
                }
                n = 25;
                break;
            }
            case -828507413: {
                if (!str.equals("NO_SUCH_PROVIDER")) {
                    break;
                }
                n = 24;
                break;
            }
            case -863830559: {
                if (!str.equals("INVALID_CERT_HASH")) {
                    break;
                }
                n = 23;
                break;
            }
            case -974503964: {
                if (!str.equals("MISSING_OR_INVALID_NONCE")) {
                    break;
                }
                n = 22;
                break;
            }
            case -1063710844: {
                if (!str.equals("ADMIN_ONLY_OPERATION")) {
                    break;
                }
                n = 21;
                break;
            }
            case -1112393964: {
                if (!str.equals("INVALID_EMAIL")) {
                    break;
                }
                n = 20;
                break;
            }
            case -1202691903: {
                if (!str.equals("SECOND_FACTOR_EXISTS")) {
                    break;
                }
                n = 19;
                break;
            }
            case -1232010689: {
                if (!str.equals("INVALID_SESSION_INFO")) {
                    break;
                }
                n = 18;
                break;
            }
            case -1242922234: {
                if (!str.equals("ALTERNATE_CLIENT_IDENTIFIER_REQUIRED")) {
                    break;
                }
                n = 17;
                break;
            }
            case -1340100504: {
                if (!str.equals("INVALID_TENANT_ID")) {
                    break;
                }
                n = 16;
                break;
            }
            case -1345867105: {
                if (!str.equals("TOKEN_EXPIRED")) {
                    break;
                }
                n = 15;
                break;
            }
            case -1421414571: {
                if (!str.equals("INVALID_CODE")) {
                    break;
                }
                n = 14;
                break;
            }
            case -1458751677: {
                if (!str.equals("MISSING_EMAIL")) {
                    break;
                }
                n = 13;
                break;
            }
            case -1583894766: {
                if (!str.equals("INVALID_OOB_CODE")) {
                    break;
                }
                n = 12;
                break;
            }
            case -1584641425: {
                if (!str.equals("UNAUTHORIZED_DOMAIN")) {
                    break;
                }
                n = 11;
                break;
            }
            case -1587614300: {
                if (!str.equals("EXPIRED_OOB_CODE")) {
                    break;
                }
                n = 10;
                break;
            }
            case -1603818979: {
                if (!str.equals("RECAPTCHA_NOT_ENABLED")) {
                    break;
                }
                n = 9;
                break;
            }
            case -1699246888: {
                if (!str.equals("INVALID_RECAPTCHA_VERSION")) {
                    break;
                }
                n = 8;
                break;
            }
            case -1774756919: {
                if (!str.equals("WEB_NETWORK_REQUEST_FAILED")) {
                    break;
                }
                n = 7;
                break;
            }
            case -1800638118: {
                if (!str.equals("QUOTA_EXCEEDED")) {
                    break;
                }
                n = 6;
                break;
            }
            case -1944433728: {
                if (!str.equals("DYNAMIC_LINK_NOT_ACTIVATED")) {
                    break;
                }
                n = 5;
                break;
            }
            case -2001169389: {
                if (!str.equals("INVALID_IDP_RESPONSE")) {
                    break;
                }
                n = 4;
                break;
            }
            case -2005236790: {
                if (!str.equals("INTERNAL_SUCCESS_SIGN_OUT")) {
                    break;
                }
                n = 3;
                break;
            }
            case -2014808264: {
                if (!str.equals("WEB_CONTEXT_ALREADY_PRESENTED")) {
                    break;
                }
                n = 2;
                break;
            }
            case -2065866930: {
                if (!str.equals("INVALID_RECIPIENT_EMAIL")) {
                    break;
                }
                n = 1;
                break;
            }
            case -2130504259: {
                if (!str.equals("USER_CANCELLED")) {
                    break;
                }
                n = 0;
                break;
            }
        }
        int n2 = 0;
        switch (n) {
            default: {
                n2 = 17499;
                break;
            }
            case 80: {
                n2 = 17073;
                break;
            }
            case 79: {
                n2 = 17090;
                break;
            }
            case 78: {
                n2 = 17045;
                break;
            }
            case 77: {
                n2 = 17040;
                break;
            }
            case 75: {
                n2 = 17028;
                break;
            }
            case 74: {
                n2 = 17042;
                break;
            }
            case 73: {
                n2 = 17005;
                break;
            }
            case 71: {
                n2 = 17201;
                break;
            }
            case 70: {
                n2 = 17025;
                break;
            }
            case 69: {
                n2 = 17043;
                break;
            }
            case 68: {
                n2 = 17051;
                break;
            }
            case 67: {
                n2 = 17202;
                break;
            }
            case 65: {
                n2 = 17009;
                break;
            }
            case 64: {
                n2 = 17000;
                break;
            }
            case 62:
            case 76: {
                n2 = 17010;
                break;
            }
            case 61: {
                n2 = 17031;
                break;
            }
            case 60: {
                n2 = 17093;
                break;
            }
            case 59: {
                n2 = 17078;
                break;
            }
            case 58: {
                n2 = 17058;
                break;
            }
            case 57: {
                n2 = 17002;
                break;
            }
            case 56: {
                n2 = 17071;
                break;
            }
            case 55: {
                n2 = 17049;
                break;
            }
            case 54: {
                n2 = 17083;
                break;
            }
            case 53: {
                n2 = 17075;
                break;
            }
            case 52: {
                n2 = 17086;
                break;
            }
            case 50: {
                n2 = 17205;
                break;
            }
            case 49: {
                n2 = 17204;
                break;
            }
            case 48: {
                n2 = 17065;
                break;
            }
            case 47: {
                n2 = 17017;
                break;
            }
            case 46: {
                n2 = 17007;
                break;
            }
            case 45: {
                n2 = 17095;
                break;
            }
            case 44: {
                n2 = 17081;
                break;
            }
            case 43: {
                n2 = 17074;
                break;
            }
            case 42: {
                n2 = 17041;
                break;
            }
            case 41: {
                n2 = 17032;
                break;
            }
            case 40: {
                n2 = 17089;
                break;
            }
            case 38: {
                n2 = 17026;
                break;
            }
            case 37: {
                n2 = 17056;
                break;
            }
            case 36:
            case 39: {
                n2 = 17011;
                break;
            }
            case 35: {
                n2 = 17082;
                break;
            }
            case 34: {
                n2 = 17088;
                break;
            }
            case 33: {
                n2 = 17062;
                break;
            }
            case 32:
            case 51: {
                n2 = 17006;
                break;
            }
            case 31: {
                n2 = 17203;
                break;
            }
            case 29: {
                n2 = 17207;
                break;
            }
            case 28:
            case 66: {
                n2 = 17020;
                break;
            }
            case 27: {
                n2 = 17014;
                break;
            }
            case 26: {
                n2 = 17035;
                break;
            }
            case 25: {
                n2 = 17084;
                break;
            }
            case 24: {
                n2 = 17016;
                break;
            }
            case 23: {
                n2 = 17064;
                break;
            }
            case 22: {
                n2 = 17094;
                break;
            }
            case 21: {
                n2 = 17085;
                break;
            }
            case 20:
            case 72: {
                n2 = 17008;
                break;
            }
            case 19: {
                n2 = 17087;
                break;
            }
            case 18: {
                n2 = 17046;
                break;
            }
            case 17: {
                n2 = 18002;
                break;
            }
            case 16: {
                n2 = 17079;
                break;
            }
            case 15: {
                n2 = 17021;
                break;
            }
            case 14: {
                n2 = 17044;
                break;
            }
            case 13: {
                n2 = 17034;
                break;
            }
            case 12: {
                n2 = 17030;
                break;
            }
            case 11: {
                n2 = 17038;
                break;
            }
            case 10: {
                n2 = 17029;
                break;
            }
            case 9: {
                n2 = 17200;
                break;
            }
            case 8: {
                n2 = 17206;
                break;
            }
            case 7: {
                n2 = 17061;
                break;
            }
            case 6: {
                n2 = 17052;
                break;
            }
            case 5: {
                n2 = 17068;
                break;
            }
            case 4:
            case 30:
            case 63: {
                n2 = 17004;
                break;
            }
            case 3: {
                n2 = 17091;
                break;
            }
            case 2: {
                n2 = 17057;
                break;
            }
            case 1: {
                n2 = 17033;
                break;
            }
            case 0: {
                n2 = 18001;
                break;
            }
        }
        if (n2 != 17499) {
            return new Status(n2, str2);
        }
        if (str2 != null) {
            final StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(":");
            sb.append(str2);
            return new Status(n2, sb.toString());
        }
        return new Status(n2, str);
    }
}

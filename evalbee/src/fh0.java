import java.util.Objects;
import java.util.Arrays;
import java.io.IOException;
import java.util.Iterator;

// 
// Decompiled by Procyon v0.6.0
// 

public class fh0
{
    public final String a;
    
    public fh0(final fh0 fh0) {
        this.a = fh0.a;
    }
    
    public fh0(final String s) {
        this.a = (String)i71.r(s);
    }
    
    public static fh0 g(final char c) {
        return new fh0(String.valueOf(c));
    }
    
    public static fh0 h(final String s) {
        return new fh0(s);
    }
    
    public Appendable a(final Appendable appendable, final Iterator iterator) {
        i71.r(appendable);
        if (iterator.hasNext()) {
            while (true) {
                appendable.append(this.i(iterator.next()));
                if (!iterator.hasNext()) {
                    break;
                }
                appendable.append(this.a);
            }
        }
        return appendable;
    }
    
    public final StringBuilder b(final StringBuilder sb, final Iterable iterable) {
        return this.c(sb, iterable.iterator());
    }
    
    public final StringBuilder c(final StringBuilder sb, final Iterator iterator) {
        try {
            this.a(sb, iterator);
            return sb;
        }
        catch (final IOException detailMessage) {
            throw new AssertionError((Object)detailMessage);
        }
    }
    
    public final String d(final Iterable iterable) {
        return this.e(iterable.iterator());
    }
    
    public final String e(final Iterator iterator) {
        return this.c(new StringBuilder(), iterator).toString();
    }
    
    public final String f(final Object[] a) {
        return this.d(Arrays.asList(a));
    }
    
    public CharSequence i(final Object obj) {
        Objects.requireNonNull(obj);
        CharSequence string;
        if (obj instanceof CharSequence) {
            string = (CharSequence)obj;
        }
        else {
            string = obj.toString();
        }
        return string;
    }
    
    public fh0 j(final String s) {
        i71.r(s);
        return new fh0(this, this, s) {
            public final String b;
            public final fh0 c;
            
            @Override
            public CharSequence i(final Object o) {
                CharSequence charSequence;
                if (o == null) {
                    charSequence = this.b;
                }
                else {
                    charSequence = this.c.i(o);
                }
                return charSequence;
            }
            
            @Override
            public fh0 j(final String s) {
                throw new UnsupportedOperationException("already specified useForNull");
            }
        };
    }
}

import com.google.android.gms.common.internal.Preconditions;
import android.net.Uri;
import android.text.TextUtils;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class io1
{
    public static String a(final String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return "";
        }
        if (!s.startsWith("/") && !s.endsWith("/") && !s.contains("//")) {
            return s;
        }
        final StringBuilder sb = new StringBuilder();
        for (final String str : s.split("/", -1)) {
            if (!TextUtils.isEmpty((CharSequence)str)) {
                if (sb.length() > 0) {
                    sb.append("/");
                }
                sb.append(str);
            }
        }
        return sb.toString();
    }
    
    public static String b(final String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return "";
        }
        return c(Uri.encode(s));
    }
    
    public static String c(final String s) {
        Preconditions.checkNotNull(s);
        return s.replace("%2F", "/");
    }
}

import android.os.Build$VERSION;
import android.os.Build;
import com.google.android.gms.tasks.Task;
import android.content.SharedPreferences;
import com.google.firebase.crashlytics.internal.common.CommonUtils;
import java.util.Locale;
import java.util.UUID;
import android.content.Context;
import java.util.regex.Pattern;

// 
// Decompiled by Procyon v0.6.0
// 

public class de0 implements of0
{
    public static final Pattern g;
    public static final String h;
    public final qf0 a;
    public final Context b;
    public final String c;
    public final r20 d;
    public final dp e;
    public a f;
    
    static {
        g = Pattern.compile("[^\\p{Alnum}]");
        h = Pattern.quote("/");
    }
    
    public de0(final Context b, final String c, final r20 d, final dp e) {
        if (b == null) {
            throw new IllegalArgumentException("appContext must not be null");
        }
        if (c != null) {
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
            this.a = new qf0();
            return;
        }
        throw new IllegalArgumentException("appIdentifier must not be null");
    }
    
    public static String c() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SYN_");
        sb.append(UUID.randomUUID().toString());
        return sb.toString();
    }
    
    public static String e(String lowerCase) {
        if (lowerCase == null) {
            lowerCase = null;
        }
        else {
            lowerCase = de0.g.matcher(lowerCase).replaceAll("").toLowerCase(Locale.US);
        }
        return lowerCase;
    }
    
    public static boolean k(final String s) {
        return s != null && s.startsWith("SYN_");
    }
    
    @Override
    public a a() {
        synchronized (this) {
            if (!this.n()) {
                return this.f;
            }
            zl0.f().i("Determining Crashlytics installation ID...");
            final SharedPreferences q = CommonUtils.q(this.b);
            final String string = q.getString("firebase.installation.id", (String)null);
            final zl0 f = zl0.f();
            final StringBuilder sb = new StringBuilder();
            sb.append("Cached Firebase Installation ID: ");
            sb.append(string);
            f.i(sb.toString());
            a f3;
            if (this.e.d()) {
                final String d = this.d();
                final zl0 f2 = zl0.f();
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Fetched Firebase Installation ID: ");
                sb2.append(d);
                f2.i(sb2.toString());
                String c;
                if ((c = d) == null) {
                    if (string == null) {
                        c = c();
                    }
                    else {
                        c = string;
                    }
                }
                if (c.equals(string)) {
                    f3 = of0.a.a(this.l(q), c);
                }
                else {
                    f3 = of0.a.a(this.b(c, q), c);
                }
            }
            else if (k(string)) {
                f3 = of0.a.b(this.l(q));
            }
            else {
                f3 = of0.a.b(this.b(c(), q));
            }
            this.f = f3;
            final zl0 f4 = zl0.f();
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Install IDs: ");
            sb3.append(this.f);
            f4.i(sb3.toString());
            return this.f;
        }
    }
    
    public final String b(final String str, final SharedPreferences sharedPreferences) {
        synchronized (this) {
            final String e = e(UUID.randomUUID().toString());
            final zl0 f = zl0.f();
            final StringBuilder sb = new StringBuilder();
            sb.append("Created new Crashlytics installation ID: ");
            sb.append(e);
            sb.append(" for FID: ");
            sb.append(str);
            f.i(sb.toString());
            sharedPreferences.edit().putString("crashlytics.installation.id", e).putString("firebase.installation.id", str).apply();
            return e;
        }
    }
    
    public String d() {
        final Task id = this.d.getId();
        String s;
        try {
            s = (String)v22.f(id);
        }
        catch (final Exception ex) {
            zl0.f().l("Failed to retrieve Firebase Installation ID.", ex);
            s = null;
        }
        return s;
    }
    
    public String f() {
        return this.c;
    }
    
    public String g() {
        return this.a.a(this.b);
    }
    
    public String h() {
        return String.format(Locale.US, "%s/%s", this.m(Build.MANUFACTURER), this.m(Build.MODEL));
    }
    
    public String i() {
        return this.m(Build$VERSION.INCREMENTAL);
    }
    
    public String j() {
        return this.m(Build$VERSION.RELEASE);
    }
    
    public final String l(final SharedPreferences sharedPreferences) {
        return sharedPreferences.getString("crashlytics.installation.id", (String)null);
    }
    
    public final String m(final String s) {
        return s.replaceAll(de0.h, "");
    }
    
    public final boolean n() {
        final a f = this.f;
        return f == null || (f.d() == null && this.e.d());
    }
}

import java.util.Random;
import java.util.List;
import java.util.Collections;
import java.util.Arrays;
import org.json.JSONException;
import org.json.JSONArray;
import com.ekodroid.omrevaluator.serializable.ResponseModel.PurchaseAccount;
import com.google.firebase.auth.FirebaseAuth;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.ReportAction;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.ReportPendingActionFile;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.ExamAction;
import java.util.Collection;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.ExamsPendingActionFile;
import java.util.ArrayList;
import android.widget.Toast;
import android.widget.TextView;
import android.widget.ImageView;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.content.Intent;
import android.app.Activity;
import com.ekodroid.omrevaluator.serializable.SerialData;
import android.content.SharedPreferences;
import java.text.ParseException;
import java.util.Date;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import android.view.inputmethod.InputMethodManager;
import android.view.View;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class a91
{
    public static int A() {
        return 20;
    }
    
    public static void B(final Context context, final View view) {
        final InputMethodManager inputMethodManager = (InputMethodManager)context.getSystemService("input_method");
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
    
    public static boolean C(final String source, final int n) {
        try {
            final Date parse = new SimpleDateFormat("yyyy-MM-dd").parse(source);
            final Calendar instance = Calendar.getInstance();
            instance.setTime(parse);
            instance.add(5, n);
            return new Date().after(instance.getTime());
        }
        catch (final ParseException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public static boolean D(final int n, final SharedPreferences sharedPreferences, final int i, final String s) {
        try {
            final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            final StringBuilder sb = new StringBuilder();
            sb.append(ok.x);
            sb.append(i);
            final Date parse = simpleDateFormat.parse(b.g(sharedPreferences.getString(sb.toString(), b.i(simpleDateFormat.format(new Date()), s)), s));
            final Calendar instance = Calendar.getInstance();
            instance.setTime(parse);
            instance.add(5, n);
            return !new Date().after(instance.getTime());
        }
        catch (final Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public static boolean E(final SharedPreferences sharedPreferences, final String s, final int i) {
        final StringBuilder sb = new StringBuilder();
        sb.append(ok.z);
        sb.append(i);
        final String string = sharedPreferences.getString(sb.toString(), "");
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(ok.A);
        sb2.append(i);
        final String string2 = sharedPreferences.getString(sb2.toString(), "");
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(ok.B);
        sb3.append(i);
        final String string3 = sharedPreferences.getString(sb3.toString(), "");
        try {
            final SerialData serialData = (SerialData)new gc0().j(b.g(string2, b.g(string3, s)), SerialData.class);
            final SerialData serialData2 = (SerialData)new gc0().j(b.g(string, s), SerialData.class);
            gk.f(serialData2.getStream());
            gk.g(serialData.getStream());
            gk.d(serialData2.getId());
            gk.e(serialData.getId());
            return true;
        }
        catch (final Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public static void F(final Activity activity, final String str, final String s) {
        try {
            final Intent intent = new Intent("android.intent.action.SEND");
            intent.setPackage("com.whatsapp");
            intent.setAction("android.intent.action.SEND");
            intent.setType("text/plain");
            intent.putExtra("android.intent.extra.TEXT", s);
            final StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append("@s.whatsapp.net");
            intent.putExtra("jid", sb.toString());
            activity.startActivity(intent);
        }
        catch (final Exception ex) {
            G((Context)activity, 2131886902, 2131230909, 2131231086);
        }
    }
    
    public static void G(final Context context, final int text, final int imageResource, final int n) {
        final View inflate = LayoutInflater.from(context).inflate(2131493106, (ViewGroup)null);
        final ImageView imageView = (ImageView)inflate.findViewById(2131296697);
        ((TextView)inflate.findViewById(2131297305)).setText(text);
        imageView.setImageResource(imageResource);
        inflate.setBackground(g7.b(context, n));
        final Toast toast = new Toast(context);
        toast.setDuration(0);
        toast.setView(inflate);
        toast.show();
    }
    
    public static void H(final Context context, final String text, final int imageResource, final int n) {
        final View inflate = LayoutInflater.from(context).inflate(2131493106, (ViewGroup)null);
        final ImageView imageView = (ImageView)inflate.findViewById(2131296697);
        ((TextView)inflate.findViewById(2131297305)).setText((CharSequence)text);
        imageView.setImageResource(imageResource);
        inflate.setBackground(g7.b(context, n));
        final Toast toast = new Toast(context);
        toast.setDuration(0);
        toast.setView(inflate);
        toast.show();
    }
    
    public static ExamsPendingActionFile I(final Context context, final ArrayList c) {
        if (c == null) {
            context.getSharedPreferences("MyPref", 0).edit().putString("SYNC_EXAMS_ACTIONS", (String)null).commit();
        }
        ExamsPendingActionFile i;
        if ((i = i(context)) == null) {
            i = new ExamsPendingActionFile();
        }
        i.getExamPendingActions().addAll(c);
        context.getSharedPreferences("MyPref", 0).edit().putString("SYNC_EXAMS_ACTIONS", new gc0().s(i)).commit();
        return i;
    }
    
    public static void J(final Context context, final long n) {
        context.getSharedPreferences("MyPref", 0).edit().putLong("SYNC_EXAMS", n).commit();
    }
    
    public static ReportPendingActionFile K(final Context context, final ArrayList c) {
        if (c == null) {
            context.getSharedPreferences("MyPref", 0).edit().putString("SYNC_REPORTS_ACTIONS", (String)null).commit();
        }
        ReportPendingActionFile t;
        if ((t = t(context)) == null) {
            t = new ReportPendingActionFile();
        }
        t.getReportPendingActions().addAll(c);
        context.getSharedPreferences("MyPref", 0).edit().putString("SYNC_REPORTS_ACTIONS", new gc0().s(t)).commit();
        return t;
    }
    
    public static void L(final Context context, final long n) {
        context.getSharedPreferences("MyPref", 0).edit().putLong("SYNC_STUDENTS", n).commit();
    }
    
    public static boolean a(final Context context) {
        return c(context) || dj1.b < 200;
    }
    
    public static void b(final Context context) {
        final String email = FirebaseAuth.getInstance().e().getEmail();
        final String string = context.getSharedPreferences("MyPref", 0).getString("LAST_LOGGED_IN", (String)null);
        if (string != null && !string.equals(email)) {
            xu1.a(context);
        }
        context.getSharedPreferences("MyPref", 0).edit().putString("LAST_LOGGED_IN", email).commit();
    }
    
    public static boolean c(final Context context) {
        final boolean b = false;
        final String string = context.getSharedPreferences("MyPref", 0).getString(ok.y, "");
        try {
            final PurchaseAccount purchaseAccount = (PurchaseAccount)new gc0().j(b.g(string, FirebaseAuth.getInstance().e().O()), PurchaseAccount.class);
            if (purchaseAccount != null) {
                boolean b2 = b;
                if (purchaseAccount.getStorageExpMinutes() > n()) {
                    final boolean equals = purchaseAccount.getAccountType().equals(ok.T);
                    b2 = b;
                    if (!equals) {
                        b2 = true;
                    }
                }
                return b2;
            }
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }
    
    public static int d(final int n, final Context context) {
        return (int)(n * context.getResources().getDisplayMetrics().density);
    }
    
    public static String e() {
        return "com.ekodroid.omrevaluator";
    }
    
    public static int f() {
        return 541;
    }
    
    public static String g() {
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    }
    
    public static ArrayList h(final String s) {
        final ArrayList list = new ArrayList();
        try {
            final JSONArray jsonArray = new JSONArray(s);
            for (int i = 0; i < jsonArray.length(); ++i) {
                list.add(jsonArray.getDouble(i));
            }
        }
        catch (final JSONException ex) {
            ((Throwable)ex).printStackTrace();
        }
        return list;
    }
    
    public static ExamsPendingActionFile i(final Context context) {
        try {
            final ExamsPendingActionFile examsPendingActionFile = (ExamsPendingActionFile)new gc0().j(context.getSharedPreferences("MyPref", 0).getString("SYNC_EXAMS_ACTIONS", (String)null), ExamsPendingActionFile.class);
            if (examsPendingActionFile != null) {
                return examsPendingActionFile;
            }
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
    
    public static long j(final Context context) {
        return context.getSharedPreferences("MyPref", 0).getLong("SYNC_EXAMS", 0L);
    }
    
    public static String k() {
        return "8768";
    }
    
    public static String l(String s) {
        if (s != null && !s.isEmpty()) {
            final String[] split = s.split(" ");
            final StringBuilder sb = new StringBuilder();
            for (final String s2 : split) {
                if (!s2.trim().isEmpty()) {
                    sb.append(s2.trim().charAt(0));
                }
            }
            final String s3 = s = sb.toString();
            if (s3.length() > 2) {
                s = s3.substring(0, 2);
            }
            return s.toUpperCase();
        }
        return "";
    }
    
    public static ArrayList m(final String s, final Double[] a) {
        final ArrayList h = h(s);
        h.addAll(Arrays.asList(a));
        Collections.sort((List<Comparable>)h);
        Collections.reverse(h);
        return h;
    }
    
    public static int n() {
        return (int)(System.currentTimeMillis() / 60000L);
    }
    
    public static ArrayList o(final String s, final Double[] a) {
        final ArrayList h = h(s);
        h.addAll(Arrays.asList(a));
        Collections.sort((List<Comparable>)h);
        return h;
    }
    
    public static String p() {
        return "8769";
    }
    
    public static String q() {
        return "8767";
    }
    
    public static PurchaseAccount r(final Context context) {
        final String string = context.getSharedPreferences("MyPref", 0).getString(ok.y, "");
        try {
            return (PurchaseAccount)new gc0().j(b.g(string, FirebaseAuth.getInstance().e().O()), PurchaseAccount.class);
        }
        catch (final Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    public static int[] s() {
        final int[] array = new int[16];
        final Random random = new Random();
        for (int i = 0; i < 16; ++i) {
            array[i] = random.nextInt(100);
        }
        return array;
    }
    
    public static ReportPendingActionFile t(final Context context) {
        try {
            final ReportPendingActionFile reportPendingActionFile = (ReportPendingActionFile)new gc0().j(context.getSharedPreferences("MyPref", 0).getString("SYNC_REPORTS_ACTIONS", (String)null), ReportPendingActionFile.class);
            if (reportPendingActionFile != null) {
                return reportPendingActionFile;
            }
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
    
    public static String u() {
        return "server1.omrevaluator.com";
    }
    
    public static String v() {
        return "evalbee.com";
    }
    
    public static String w() {
        return u();
    }
    
    public static String x() {
        return "evalbee.com";
    }
    
    public static ArrayList y(final String s) {
        final ArrayList list = new ArrayList();
        try {
            final JSONArray jsonArray = new JSONArray(s);
            for (int i = 0; i < jsonArray.length(); ++i) {
                list.add(jsonArray.getString(i));
            }
        }
        catch (final JSONException ex) {
            ((Throwable)ex).printStackTrace();
        }
        return list;
    }
    
    public static long z(final Context context) {
        return context.getSharedPreferences("MyPref", 0).getLong("SYNC_STUDENTS", 0L);
    }
}

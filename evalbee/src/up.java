import com.google.gson.stream.JsonToken;
import java.text.DateFormat;
import java.util.Locale;
import java.util.ArrayList;
import java.util.Date;
import com.google.gson.reflect.TypeToken;
import java.util.List;

// 
// Decompiled by Procyon v0.6.0
// 

public final class up extends hz1
{
    public static final iz1 b;
    public final List a;
    
    static {
        b = new iz1() {
            @Override
            public hz1 a(final gc0 gc0, final TypeToken typeToken) {
                up up;
                if (typeToken.getRawType() == Date.class) {
                    up = new up();
                }
                else {
                    up = null;
                }
                return up;
            }
        };
    }
    
    public up() {
        final ArrayList a = new ArrayList();
        this.a = a;
        final Locale us = Locale.US;
        a.add(DateFormat.getDateTimeInstance(2, 2, us));
        if (!Locale.getDefault().equals(us)) {
            a.add(DateFormat.getDateTimeInstance(2, 2));
        }
        if (wg0.d()) {
            a.add(v61.c(2, 2));
        }
    }
    
    public final Date e(final rh0 p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokevirtual   rh0.i0:()Ljava/lang/String;
        //     4: astore_2       
        //     5: aload_0        
        //     6: getfield        up.a:Ljava/util/List;
        //     9: astore_3       
        //    10: aload_3        
        //    11: monitorenter   
        //    12: aload_0        
        //    13: getfield        up.a:Ljava/util/List;
        //    16: invokeinterface java/util/List.iterator:()Ljava/util/Iterator;
        //    21: astore          4
        //    23: aload           4
        //    25: invokeinterface java/util/Iterator.hasNext:()Z
        //    30: ifeq            58
        //    33: aload           4
        //    35: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //    40: checkcast       Ljava/text/DateFormat;
        //    43: astore          5
        //    45: aload           5
        //    47: aload_2        
        //    48: invokevirtual   java/text/DateFormat.parse:(Ljava/lang/String;)Ljava/util/Date;
        //    51: astore          5
        //    53: aload_3        
        //    54: monitorexit    
        //    55: aload           5
        //    57: areturn        
        //    58: aload_3        
        //    59: monitorexit    
        //    60: new             Ljava/text/ParsePosition;
        //    63: astore_3       
        //    64: aload_3        
        //    65: iconst_0       
        //    66: invokespecial   java/text/ParsePosition.<init>:(I)V
        //    69: aload_2        
        //    70: aload_3        
        //    71: invokestatic    yd0.c:(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/util/Date;
        //    74: astore_3       
        //    75: aload_3        
        //    76: areturn        
        //    77: astore          4
        //    79: new             Ljava/lang/StringBuilder;
        //    82: dup            
        //    83: invokespecial   java/lang/StringBuilder.<init>:()V
        //    86: astore_3       
        //    87: aload_3        
        //    88: ldc             "Failed parsing '"
        //    90: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    93: pop            
        //    94: aload_3        
        //    95: aload_2        
        //    96: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    99: pop            
        //   100: aload_3        
        //   101: ldc             "' as Date; at path "
        //   103: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   106: pop            
        //   107: aload_3        
        //   108: aload_1        
        //   109: invokevirtual   rh0.j:()Ljava/lang/String;
        //   112: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   115: pop            
        //   116: new             Lcom/google/gson/JsonSyntaxException;
        //   119: dup            
        //   120: aload_3        
        //   121: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   124: aload           4
        //   126: invokespecial   com/google/gson/JsonSyntaxException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   129: athrow         
        //   130: astore_1       
        //   131: aload_3        
        //   132: monitorexit    
        //   133: aload_1        
        //   134: athrow         
        //   135: astore          5
        //   137: goto            23
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                      
        //  -----  -----  -----  -----  --------------------------
        //  12     23     130    135    Any
        //  23     45     130    135    Any
        //  45     53     135    140    Ljava/text/ParseException;
        //  45     53     130    135    Any
        //  53     55     130    135    Any
        //  58     60     130    135    Any
        //  60     75     77     130    Ljava/text/ParseException;
        //  131    133    130    135    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0058:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public Date f(final rh0 rh0) {
        if (rh0.o0() == JsonToken.NULL) {
            rh0.R();
            return null;
        }
        return this.e(rh0);
    }
    
    public void g(final vh0 vh0, final Date date) {
        if (date == null) {
            vh0.u();
            return;
        }
        final DateFormat dateFormat = this.a.get(0);
        synchronized (this.a) {
            final String format = dateFormat.format(date);
            monitorexit(this.a);
            vh0.r0(format);
        }
    }
}

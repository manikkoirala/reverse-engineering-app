import android.view.ViewGroup;
import android.view.ViewGroup$MarginLayoutParams;
import android.widget.TextView;
import android.view.ViewGroup$LayoutParams;
import android.widget.LinearLayout$LayoutParams;
import android.widget.LinearLayout;
import android.os.Bundle;
import android.view.View;
import android.view.View$OnClickListener;
import androidx.appcompat.widget.Toolbar;
import android.widget.EditText;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class ti0 extends u80
{
    public Context a;
    public String b;
    public y01 c;
    public String[] d;
    public EditText[] e;
    public int f;
    
    public ti0(final Context a, final String b, final y01 c, final String[] d, final int f) {
        super(a);
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.f = f;
    }
    
    public final void a() {
        final Toolbar toolbar = (Toolbar)this.findViewById(2131297351);
        toolbar.setTitle(this.b);
        toolbar.setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ti0 a;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492975);
        this.a();
        final LinearLayout linearLayout = (LinearLayout)this.findViewById(2131296777);
        this.e = new EditText[this.d.length];
        final int d = a91.d(12, this.a);
        final LinearLayout$LayoutParams linearLayout$LayoutParams = new LinearLayout$LayoutParams(-2, -2);
        final LinearLayout$LayoutParams linearLayout$LayoutParams2 = new LinearLayout$LayoutParams(-2, -2);
        final int d2 = a91.d(16, this.a);
        int i = 0;
        ((ViewGroup$MarginLayoutParams)linearLayout$LayoutParams2).setMargins(0, 0, d2, 0);
        while (i < this.e.length) {
            final LinearLayout b = yo.b(this.a);
            final TextView c = yo.c(this.a, this.d[i], 100);
            ((View)c).setTextAlignment(6);
            c.setPadding(d, d, d, d);
            ((View)(this.e[i] = yo.a(this.a, this.d[i], this.f, 100))).setPadding(d, d, d, d);
            ((ViewGroup)b).addView((View)c, (ViewGroup$LayoutParams)linearLayout$LayoutParams2);
            ((ViewGroup)b).addView((View)this.e[i], (ViewGroup$LayoutParams)linearLayout$LayoutParams);
            ((ViewGroup)linearLayout).addView((View)b);
            ++i;
        }
        this.findViewById(2131296433).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ti0 a;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
        this.findViewById(2131296467).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ti0 a;
            
            public void onClick(final View view) {
                final String[] array = new String[this.a.e.length];
                int n = 0;
                while (true) {
                    final ti0 a = this.a;
                    final EditText[] e = a.e;
                    if (n >= e.length) {
                        final y01 c = a.c;
                        if (c != null) {
                            c.a(array);
                        }
                        this.a.dismiss();
                        return;
                    }
                    final String string = e[n].getText().toString();
                    array[n] = string;
                    if (string.trim().length() < 1) {
                        a91.G(this.a.a, 2131886261, 2131230909, 2131231086);
                        return;
                    }
                    ++n;
                }
            }
        });
    }
}

import android.app.Dialog;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.PendingJoinRequestDto;
import android.app.ProgressDialog;
import androidx.appcompat.app.a;
import android.os.Bundle;
import com.google.firebase.analytics.FirebaseAnalytics;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.OrgProfile;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.ekodroid.omrevaluator.more.models.Teacher;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.PendingJoinRequest;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class mg0
{
    public Context a;
    public y01 b;
    
    public mg0(final Context a, final y01 b) {
        this.b = b;
        this.a = a;
        this.b();
    }
    
    public final void b() {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(this.a, 2131951953);
        materialAlertDialogBuilder.setTitle(2131886370);
        final OrgProfile instance = OrgProfile.getInstance(this.a);
        final PendingJoinRequest request = instance.getRequest();
        final StringBuilder sb = new StringBuilder();
        sb.append(request.getSentByName());
        sb.append(" (");
        sb.append(request.getSentByEmail());
        sb.append(") had invited you to join ");
        sb.append(request.getOrgName());
        sb.append("'s organization account as a Teacher");
        materialAlertDialogBuilder.setMessage((CharSequence)sb.toString());
        materialAlertDialogBuilder.setPositiveButton(2131886108, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this, request) {
            public final PendingJoinRequest a;
            public final mg0 b;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                this.b.c(this.a, Teacher.MemberStatus.ACCEPTED);
                FirebaseAnalytics.getInstance(this.b.a).a("ADD_TEACHER_ACCEPT", null);
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.setNegativeButton(2131886227, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this, request) {
            public final PendingJoinRequest a;
            public final mg0 b;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                this.b.c(this.a, Teacher.MemberStatus.REJECTED);
                FirebaseAnalytics.getInstance(this.b.a).a("ADD_TEACHER_DECLINE", null);
                dialogInterface.dismiss();
            }
        });
        final a create = materialAlertDialogBuilder.create();
        if (instance.getRequest() == null) {
            create.dismiss();
        }
        materialAlertDialogBuilder.create().show();
    }
    
    public final void c(final PendingJoinRequest pendingJoinRequest, final Teacher.MemberStatus memberStatus) {
        final ProgressDialog progressDialog = new ProgressDialog(this.a);
        progressDialog.setMessage((CharSequence)"Please wait, updating request...");
        ((Dialog)progressDialog).show();
        new i61(new PendingJoinRequestDto(pendingJoinRequest, memberStatus), this.a, new zg(this, progressDialog) {
            public final ProgressDialog a;
            public final mg0 b;
            
            @Override
            public void a(final boolean b, final int n, final Object o) {
                ((Dialog)this.a).dismiss();
                if (b) {
                    OrgProfile.reset(this.b.a);
                    final y01 b2 = this.b.b;
                    if (b2 != null) {
                        b2.a(null);
                    }
                }
                else {
                    FirebaseAnalytics.getInstance(this.b.a).a("INVITE_RESPONSE_FAIL", null);
                    a91.H(this.b.a, "Update failed, please try again", 2131230909, 2131231086);
                }
            }
        }).g();
    }
}

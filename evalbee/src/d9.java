// 
// Decompiled by Procyon v0.6.0
// 

public abstract class d9
{
    public static boolean a(final char c) {
        return c >= 'a' && c <= 'z';
    }
    
    public static boolean b(final char c) {
        return c >= 'A' && c <= 'Z';
    }
    
    public static String c(final String s) {
        for (int length = s.length(), i = 0; i < length; ++i) {
            if (b(s.charAt(i))) {
                final char[] charArray = s.toCharArray();
                while (i < length) {
                    final char c = charArray[i];
                    if (b(c)) {
                        charArray[i] = (char)(c ^ ' ');
                    }
                    ++i;
                }
                return String.valueOf(charArray);
            }
        }
        return s;
    }
    
    public static char d(final char c) {
        char c2 = c;
        if (a(c)) {
            c2 = (char)(c ^ ' ');
        }
        return c2;
    }
    
    public static String e(final String s) {
        for (int length = s.length(), i = 0; i < length; ++i) {
            if (a(s.charAt(i))) {
                final char[] charArray = s.toCharArray();
                while (i < length) {
                    final char c = charArray[i];
                    if (a(c)) {
                        charArray[i] = (char)(c ^ ' ');
                    }
                    ++i;
                }
                return String.valueOf(charArray);
            }
        }
        return s;
    }
}

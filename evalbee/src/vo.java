import android.os.IInterface;
import android.os.IBinder;
import android.app.PendingIntent;
import android.content.ComponentName;

// 
// Decompiled by Procyon v0.6.0
// 

public final class vo
{
    public final Object a;
    public final td0 b;
    public final sd0 c;
    public final ComponentName d;
    public final PendingIntent e;
    
    public vo(final td0 b, final sd0 c, final ComponentName d, final PendingIntent e) {
        this.a = new Object();
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
    }
    
    public IBinder a() {
        return ((IInterface)this.c).asBinder();
    }
    
    public ComponentName b() {
        return this.d;
    }
    
    public PendingIntent c() {
        return this.e;
    }
}

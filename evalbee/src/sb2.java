import com.google.android.gms.common.util.DefaultClock;
import com.google.android.gms.internal.firebase_auth_api.zzg;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Handler;
import android.os.HandlerThread;
import com.google.android.gms.common.logging.Logger;

// 
// Decompiled by Procyon v0.6.0
// 

public final class sb2
{
    public static Logger h;
    public final r10 a;
    public volatile long b;
    public volatile long c;
    public long d;
    public HandlerThread e;
    public Handler f;
    public Runnable g;
    
    static {
        sb2.h = new Logger("TokenRefresher", new String[] { "FirebaseAuth:" });
    }
    
    public sb2(final r10 r10) {
        sb2.h.v("Initializing TokenRefresher", new Object[0]);
        final r10 a = Preconditions.checkNotNull(r10);
        this.a = a;
        ((Thread)(this.e = new HandlerThread("TokenRefresher", 10))).start();
        this.f = (Handler)new zzg(this.e.getLooper());
        this.g = new qb2(this, a.o());
        this.d = 300000L;
    }
    
    public final void b() {
        this.f.removeCallbacks(this.g);
    }
    
    public final void c() {
        final Logger h = sb2.h;
        final long b = this.b;
        final long d = this.d;
        final StringBuilder sb = new StringBuilder("Scheduling refresh for ");
        sb.append(b - d);
        h.v(sb.toString(), new Object[0]);
        this.b();
        this.c = Math.max(this.b - DefaultClock.getInstance().currentTimeMillis() - this.d, 0L) / 1000L;
        this.f.postDelayed(this.g, this.c * 1000L);
    }
    
    public final void d() {
        final int n = (int)this.c;
        long c;
        if (n != 30 && n != 60 && n != 120 && n != 240 && n != 480) {
            if (n != 960) {
                c = 30L;
            }
            else {
                c = 960L;
            }
        }
        else {
            c = 2L * this.c;
        }
        this.c = c;
        this.b = DefaultClock.getInstance().currentTimeMillis() + this.c * 1000L;
        final Logger h = sb2.h;
        final long b = this.b;
        final StringBuilder sb = new StringBuilder("Scheduling refresh for ");
        sb.append(b);
        h.v(sb.toString(), new Object[0]);
        this.f.postDelayed(this.g, this.c * 1000L);
    }
}

import java.util.Collections;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public class oa0 extends xy0
{
    public oa0(final kq1 kq1, final r10 r10, final long lng) {
        super(kq1, r10);
        if (lng != 0L) {
            final StringBuilder sb = new StringBuilder();
            sb.append("bytes=");
            sb.append(lng);
            sb.append("-");
            super.G("Range", sb.toString());
        }
    }
    
    @Override
    public String e() {
        return "GET";
    }
    
    @Override
    public Map l() {
        return Collections.singletonMap("alt", "media");
    }
}

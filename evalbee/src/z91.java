import android.view.View;
import androidx.appcompat.app.a;
import android.view.View$OnClickListener;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import android.widget.Button;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class z91
{
    public Context a;
    public Button b;
    
    public z91(final Context a) {
        this.a = a;
        this.a();
    }
    
    public final void a() {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(this.a, 2131951953);
        final View inflate = ((LayoutInflater)this.a.getSystemService("layout_inflater")).inflate(2131492979, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        materialAlertDialogBuilder.setCancelable(false);
        this.b = (Button)inflate.findViewById(2131296440);
        final a create = materialAlertDialogBuilder.create();
        ((View)this.b).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, create) {
            public final a a;
            public final z91 b;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
        create.show();
    }
}

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.d;

// 
// Decompiled by Procyon v0.6.0
// 

public class q61
{
    public ee1 a;
    public y01 b;
    public String c;
    
    public q61(final ee1 a, final y01 b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("https://");
        sb.append(a91.u());
        sb.append(":8759/sms/syncLink");
        this.c = sb.toString();
        this.a = a;
        this.b = b;
        this.c();
    }
    
    public final void b(final Object o) {
        final y01 b = this.b;
        if (b != null) {
            b.a(o);
        }
    }
    
    public final void c() {
        final hr1 hr1 = new hr1(this, 1, this.c, new d.b(this) {
            public final q61 a;
            
            public void b(final String s) {
                if (s != null) {
                    this.a.b(s);
                }
            }
        }, new d.a(this) {
            public final q61 a;
            
            @Override
            public void a(final VolleyError volleyError) {
                this.a.b(null);
            }
        }) {
            public final q61 w;
            
            @Override
            public byte[] k() {
                return null;
            }
            
            @Override
            public String l() {
                return "application/json";
            }
        };
        hr1.L(new wq(30000, 0, 1.0f));
        this.a.a(hr1);
    }
}

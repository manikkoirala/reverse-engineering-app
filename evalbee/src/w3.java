import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import android.os.Bundle;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.ekodroid.omrevaluator.database.repositories.ClassRepository;
import com.ekodroid.omrevaluator.database.StudentDataModel;
import android.view.View;
import android.view.View$OnClickListener;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import com.google.android.material.textfield.TextInputLayout;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class w3 extends u80
{
    public Context a;
    public y01 b;
    public String c;
    public String d;
    public String e;
    public String f;
    public String g;
    public String h;
    public TextInputLayout i;
    public TextInputLayout j;
    public TextInputLayout k;
    public TextInputLayout l;
    public TextInputLayout m;
    public Button n;
    public TextWatcher p;
    
    public w3(final Context a, final String h, final y01 b, final String c, final String d, final String g, final String e, final String f) {
        super(a);
        this.p = (TextWatcher)new TextWatcher(this) {
            public final w3 a;
            
            public void afterTextChanged(final Editable editable) {
                final String string = this.a.i.getEditText().getText().toString();
                final String string2 = this.a.j.getEditText().getText().toString();
                Button button;
                boolean enabled;
                if (!string.trim().equals("") && !string2.equals("")) {
                    button = w3.a(this.a);
                    enabled = true;
                }
                else {
                    button = w3.a(this.a);
                    enabled = false;
                }
                ((View)button).setEnabled(enabled);
            }
            
            public void beforeTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
            }
            
            public void onTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
            }
        };
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = h;
    }
    
    public static /* synthetic */ Button a(final w3 w3) {
        return w3.n;
    }
    
    public static /* synthetic */ String b(final w3 w3) {
        return w3.g;
    }
    
    public static /* synthetic */ y01 c(final w3 w3) {
        return w3.b;
    }
    
    public final void d() {
        ((View)this.n).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final w3 a;
            
            public void onClick(final View view) {
                final String trim = this.a.i.getEditText().getText().toString().trim();
                final String trim2 = this.a.m.getEditText().getText().toString().trim();
                final String trim3 = this.a.j.getEditText().getText().toString().trim();
                final String trim4 = this.a.l.getEditText().getText().toString().trim();
                if (trim.trim().equals("")) {
                    a91.G(this.a.a, 2131886267, 2131230909, 2131231086);
                    return;
                }
                if (trim3.equals("")) {
                    a91.G(this.a.a, 2131886269, 2131230909, 2131231086);
                    return;
                }
                if (trim2.length() > 0 && trim2.trim().indexOf(64) < 1) {
                    a91.G(this.a.a, 2131886490, 2131230909, 2131231086);
                    return;
                }
                if (trim4.length() > 0 && (!trim4.matches("[0-9]+") || trim4.length() <= 7)) {
                    a91.G(this.a.a, 2131886491, 2131230909, 2131231086);
                    return;
                }
                final StudentDataModel studentDataModel = new StudentDataModel(Integer.parseInt(trim), trim3, trim2, trim4, w3.b(this.a));
                ClassRepository.getInstance(this.a.a).deleteStudent(Integer.parseInt(trim), w3.b(this.a));
                ClassRepository.getInstance(this.a.a).saveOrUpdateStudent(studentDataModel);
                FirebaseAnalytics.getInstance(this.a.a).a("StudentAdd", null);
                w3.c(this.a).a(null);
                this.a.dismiss();
            }
        });
    }
    
    public final void e() {
        ((Toolbar)this.findViewById(2131297335)).setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final w3 a;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492961);
        this.e();
        this.n = (Button)this.findViewById(2131296408);
        this.i = (TextInputLayout)this.findViewById(2131297180);
        this.j = (TextInputLayout)this.findViewById(2131297181);
        this.l = (TextInputLayout)this.findViewById(2131297179);
        this.m = (TextInputLayout)this.findViewById(2131297173);
        this.k = (TextInputLayout)this.findViewById(2131297172);
        ((TextView)this.j.getEditText()).addTextChangedListener(this.p);
        ((TextView)this.i.getEditText()).addTextChangedListener(this.p);
        ((TextView)this.k.getEditText()).setText((CharSequence)this.g);
        ((TextView)this.j.getEditText()).setText((CharSequence)this.d);
        ((TextView)this.i.getEditText()).setText((CharSequence)String.valueOf(this.c));
        ((TextView)this.m.getEditText()).setText((CharSequence)this.e);
        ((TextView)this.l.getEditText()).setText((CharSequence)this.f);
        this.d();
        this.findViewById(2131296430).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final w3 a;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
    }
}

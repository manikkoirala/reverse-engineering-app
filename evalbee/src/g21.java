// 
// Decompiled by Procyon v0.6.0
// 

public class g21 implements r91, jr
{
    public static final a c;
    public static final r91 d;
    public a a;
    public volatile r91 b;
    
    static {
        c = new d21();
        d = new e21();
    }
    
    public g21(final a a, final r91 b) {
        this.a = a;
        this.b = b;
    }
    
    public static g21 e() {
        return new g21(g21.c, g21.d);
    }
    
    public static g21 i(final r91 r91) {
        return new g21(null, r91);
    }
    
    @Override
    public void a(final a a) {
        final r91 b = this.b;
        final r91 d = g21.d;
        if (b != d) {
            a.a(b);
            return;
        }
        synchronized (this) {
            final r91 b2 = this.b;
            r91 r91;
            if (b2 != d) {
                r91 = b2;
            }
            else {
                this.a = new f21(this.a, a);
                r91 = null;
            }
            monitorexit(this);
            if (r91 != null) {
                a.a(b2);
            }
        }
    }
    
    @Override
    public Object get() {
        return this.b.get();
    }
    
    public void j(final r91 b) {
        if (this.b == g21.d) {
            synchronized (this) {
                final a a = this.a;
                this.a = null;
                this.b = b;
                monitorexit(this);
                a.a(b);
                return;
            }
        }
        throw new IllegalStateException("provide() can be called only once.");
    }
}

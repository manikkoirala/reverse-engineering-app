import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.internal.firebase-auth-api.zzagt;
import com.google.android.gms.common.internal.Preconditions;
import android.os.Parcelable$Creator;

// 
// Decompiled by Procyon v0.6.0
// 

public class gz1 extends v9
{
    public static final Parcelable$Creator<gz1> CREATOR;
    public String a;
    public String b;
    
    static {
        CREATOR = (Parcelable$Creator)new dc2();
    }
    
    public gz1(final String s, final String s2) {
        this.a = Preconditions.checkNotEmpty(s);
        this.b = Preconditions.checkNotEmpty(s2);
    }
    
    public static zzagt J(final gz1 gz1, final String s) {
        Preconditions.checkNotNull(gz1);
        return (zzagt)new com.google.android.gms.internal.firebase_auth_api.zzagt((String)null, gz1.a, gz1.i(), (String)null, gz1.b, (String)null, s, (String)null, (String)null);
    }
    
    @Override
    public String E() {
        return "twitter.com";
    }
    
    @Override
    public final v9 H() {
        return new gz1(this.a, this.b);
    }
    
    @Override
    public String i() {
        return "twitter.com";
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.a, false);
        SafeParcelWriter.writeString(parcel, 2, this.b, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

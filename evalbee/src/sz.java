import com.google.protobuf.n;
import com.google.protobuf.m;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class sz
{
    public static final m a;
    public static final m b;
    
    static {
        a = new n();
        b = c();
    }
    
    public static m a() {
        final m b = sz.b;
        if (b != null) {
            return b;
        }
        throw new IllegalStateException("Protobuf runtime is not correctly loaded.");
    }
    
    public static m b() {
        return sz.a;
    }
    
    public static m c() {
        try {
            return (m)Class.forName("com.google.protobuf.ExtensionSchemaFull").getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
        }
        catch (final Exception ex) {
            return null;
        }
    }
}

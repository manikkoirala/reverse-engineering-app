import androidx.appcompat.app.a;
import android.view.View$OnClickListener;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemSelectedListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.SpinnerAdapter;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import com.ekodroid.omrevaluator.templateui.models.TemplateBuilder;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.ekodroid.omrevaluator.templateui.models.TemplateParams2;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class tb0
{
    public Context a;
    public y01 b;
    public TemplateParams2 c;
    public int d;
    public int e;
    
    public tb0(final Context a, final y01 b, final TemplateParams2 c) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.e();
    }
    
    public static /* synthetic */ int a(final tb0 tb0) {
        return tb0.d;
    }
    
    public static /* synthetic */ int b(final tb0 tb0, final int d) {
        return tb0.d = d;
    }
    
    public static /* synthetic */ int c(final tb0 tb0) {
        return tb0.e;
    }
    
    public static /* synthetic */ int d(final tb0 tb0, final int e) {
        return tb0.e = e;
    }
    
    public final void e() {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(this.a, 2131951953);
        final View inflate = ((LayoutInflater)this.a.getSystemService("layout_inflater")).inflate(2131492992, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        materialAlertDialogBuilder.setTitle(2131886784);
        materialAlertDialogBuilder.setCancelable(false);
        final a create = materialAlertDialogBuilder.create();
        final int o = new TemplateBuilder(null, null).o(this.c);
        final Spinner spinner = (Spinner)inflate.findViewById(2131297081);
        final Spinner spinner2 = (Spinner)inflate.findViewById(2131297083);
        spinner.setAdapter((SpinnerAdapter)new ArrayAdapter(this.a, 2131493104, (Object[])gr1.d));
        spinner2.setAdapter((SpinnerAdapter)new ArrayAdapter(this.a, 2131493104, (Object[])gr1.e));
        final TextView textView = (TextView)inflate.findViewById(2131297225);
        final Button button = (Button)inflate.findViewById(2131296475);
        ((View)textView).setVisibility(8);
        ((AdapterView)spinner).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener(this, textView, button, spinner, spinner2, o) {
            public final TextView a;
            public final Button b;
            public final Spinner c;
            public final Spinner d;
            public final int e;
            public final tb0 f;
            
            public void onItemSelected(final AdapterView adapterView, final View view, final int n, final long n2) {
                ((View)this.a).setVisibility(8);
                ((View)this.b).setEnabled(true);
                ((TextView)this.b).setTextColor(this.f.a.getResources().getColor(2131099724));
                tb0.b(this.f, Integer.parseInt(((AdapterView)this.c).getSelectedItem().toString()));
                tb0.d(this.f, Integer.parseInt(((AdapterView)this.d).getSelectedItem().toString()));
                if (this.e - 5 > tb0.a(this.f) * (tb0.c(this.f) * 5 + 1)) {
                    ((View)this.a).setVisibility(0);
                    ((View)this.b).setEnabled(false);
                    ((TextView)this.b).setTextColor(this.f.a.getResources().getColor(2131099716));
                }
            }
            
            public void onNothingSelected(final AdapterView adapterView) {
            }
        });
        ((AdapterView)spinner2).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener(this, textView, button, spinner, spinner2, o) {
            public final TextView a;
            public final Button b;
            public final Spinner c;
            public final Spinner d;
            public final int e;
            public final tb0 f;
            
            public void onItemSelected(final AdapterView adapterView, final View view, final int n, final long n2) {
                ((View)this.a).setVisibility(4);
                ((View)this.b).setEnabled(true);
                ((TextView)this.b).setTextColor(this.f.a.getResources().getColor(2131099724));
                tb0.b(this.f, Integer.parseInt(((AdapterView)this.c).getSelectedItem().toString()));
                tb0.d(this.f, Integer.parseInt(((AdapterView)this.d).getSelectedItem().toString()));
                if (this.e - 5 > tb0.a(this.f) * (tb0.c(this.f) * 5 + 1)) {
                    ((View)this.a).setVisibility(0);
                    ((View)this.b).setEnabled(false);
                    ((TextView)this.b).setTextColor(this.f.a.getResources().getColor(2131099716));
                }
            }
            
            public void onNothingSelected(final AdapterView adapterView) {
            }
        });
        ((View)button).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, create) {
            public final a a;
            public final tb0 b;
            
            public void onClick(final View view) {
                final tb0 b = this.b;
                b.b.a(new int[] { tb0.a(b), tb0.c(this.b) });
                this.a.dismiss();
            }
        });
        create.show();
    }
}

import android.os.Build$VERSION;
import android.view.View;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class cx1
{
    public static void a(final View view, final CharSequence charSequence) {
        if (Build$VERSION.SDK_INT >= 26) {
            a.a(view, charSequence);
        }
        else {
            fx1.h(view, charSequence);
        }
    }
    
    public abstract static class a
    {
        public static void a(final View view, final CharSequence tooltipText) {
            view.setTooltipText(tooltipText);
        }
    }
}

import android.os.ParcelFileDescriptor;
import android.content.res.AssetManager;
import java.lang.reflect.Array;
import java.util.Map;
import android.content.ContentResolver;
import java.nio.ByteBuffer;
import java.io.IOException;
import android.graphics.Typeface$Builder;
import android.os.CancellationSignal;
import android.graphics.fonts.FontVariationAxis;
import android.graphics.Typeface;
import android.content.res.Resources;
import android.content.Context;
import java.lang.reflect.InvocationTargetException;
import android.util.Log;
import java.lang.reflect.Method;
import java.lang.reflect.Constructor;

// 
// Decompiled by Procyon v0.6.0
// 

public class vz1 extends tz1
{
    public final Class g;
    public final Constructor h;
    public final Method i;
    public final Method j;
    public final Method k;
    public final Method l;
    public final Method m;
    
    public vz1() {
        Class v = null;
        Constructor w = null;
        Method s = null;
        Method t = null;
        Method x = null;
        Method r = null;
        Method u = null;
        Label_0120: {
            try {
                v = this.v();
                w = this.w(v);
                s = this.s(v);
                t = this.t(v);
                x = this.x(v);
                r = this.r(v);
                u = this.u(v);
                break Label_0120;
            }
            catch (final NoSuchMethodException t) {}
            catch (final ClassNotFoundException ex) {}
            final StringBuilder sb = new StringBuilder();
            sb.append("Unable to collect necessary methods for class ");
            sb.append(((NoSuchMethodException)t).getClass().getName());
            Log.e("TypefaceCompatApi26Impl", sb.toString(), (Throwable)t);
            v = null;
            w = null;
            s = null;
            t = (x = s);
            r = (u = x);
        }
        this.g = v;
        this.h = w;
        this.i = s;
        this.j = t;
        this.k = x;
        this.l = r;
        this.m = u;
    }
    
    private Object l() {
        try {
            return this.h.newInstance(new Object[0]);
        }
        catch (final IllegalAccessException | InstantiationException | InvocationTargetException ex) {
            return null;
        }
    }
    
    @Override
    public Typeface a(final Context context, final j70.c c, final Resources resources, int i) {
        if (!this.q()) {
            return super.a(context, c, resources, i);
        }
        final Object l = this.l();
        if (l == null) {
            return null;
        }
        final j70.d[] a = c.a();
        int length;
        j70.d d;
        for (length = a.length, i = 0; i < length; ++i) {
            d = a[i];
            if (!this.n(context, l, d.a(), d.c(), d.e(), d.f() ? 1 : 0, FontVariationAxis.fromFontVariationSettings(d.d()))) {
                this.m(l);
                return null;
            }
        }
        if (!this.p(l)) {
            return null;
        }
        return this.i(l);
    }
    
    @Override
    public Typeface b(Context openFileDescriptor, final CancellationSignal cancellationSignal, final k70.b[] array, final int n) {
        if (array.length < 1) {
            return null;
        }
        if (!this.q()) {
            final k70.b g = this.g(array, n);
            final ContentResolver contentResolver = openFileDescriptor.getContentResolver();
            try {
                openFileDescriptor = (Context)contentResolver.openFileDescriptor(g.d(), "r", cancellationSignal);
                if (openFileDescriptor == null) {
                    if (openFileDescriptor != null) {
                        ((ParcelFileDescriptor)openFileDescriptor).close();
                    }
                    return null;
                }
                try {
                    final Typeface build = new Typeface$Builder(((ParcelFileDescriptor)openFileDescriptor).getFileDescriptor()).setWeight(g.e()).setItalic(g.f()).build();
                    ((ParcelFileDescriptor)openFileDescriptor).close();
                    return build;
                }
                finally {
                    try {
                        ((ParcelFileDescriptor)openFileDescriptor).close();
                    }
                    finally {
                        final Throwable exception;
                        ((Throwable)cancellationSignal).addSuppressed(exception);
                    }
                }
            }
            catch (final IOException ex) {
                return null;
            }
        }
        final Map h = zz1.h(openFileDescriptor, array, cancellationSignal);
        final Object l = this.l();
        if (l == null) {
            return null;
        }
        final int length = array.length;
        boolean b = false;
        for (final k70.b b2 : array) {
            final ByteBuffer byteBuffer = h.get(b2.d());
            if (byteBuffer != null) {
                if (!this.o(l, byteBuffer, b2.c(), b2.e(), b2.f() ? 1 : 0)) {
                    this.m(l);
                    return null;
                }
                b = true;
            }
        }
        if (!b) {
            this.m(l);
            return null;
        }
        if (!this.p(l)) {
            return null;
        }
        final Typeface j = this.i(l);
        if (j == null) {
            return null;
        }
        return Typeface.create(j, n);
    }
    
    @Override
    public Typeface d(final Context context, final Resources resources, final int n, final String s, final int n2) {
        if (!this.q()) {
            return super.d(context, resources, n, s, n2);
        }
        final Object l = this.l();
        if (l == null) {
            return null;
        }
        if (!this.n(context, l, s, 0, -1, -1, null)) {
            this.m(l);
            return null;
        }
        if (!this.p(l)) {
            return null;
        }
        return this.i(l);
    }
    
    public Typeface i(final Object o) {
        try {
            final Object instance = Array.newInstance(this.g, 1);
            Array.set(instance, 0, o);
            return (Typeface)this.m.invoke(null, instance, -1, -1);
        }
        catch (final IllegalAccessException | InvocationTargetException ex) {
            return null;
        }
    }
    
    public final void m(final Object obj) {
        try {
            this.l.invoke(obj, new Object[0]);
        }
        catch (final IllegalAccessException | InvocationTargetException ex) {}
    }
    
    public final boolean n(final Context context, final Object obj, final String s, final int i, final int j, final int k, final FontVariationAxis[] array) {
        try {
            return (boolean)this.i.invoke(obj, context.getAssets(), s, 0, Boolean.FALSE, i, j, k, array);
        }
        catch (final IllegalAccessException | InvocationTargetException ex) {
            return false;
        }
    }
    
    public final boolean o(final Object obj, final ByteBuffer byteBuffer, final int i, final int j, final int k) {
        try {
            return (boolean)this.j.invoke(obj, byteBuffer, i, null, j, k);
        }
        catch (final IllegalAccessException | InvocationTargetException ex) {
            return false;
        }
    }
    
    public final boolean p(final Object obj) {
        try {
            return (boolean)this.k.invoke(obj, new Object[0]);
        }
        catch (final IllegalAccessException | InvocationTargetException ex) {
            return false;
        }
    }
    
    public final boolean q() {
        if (this.i == null) {
            Log.w("TypefaceCompatApi26Impl", "Unable to collect necessary private methods. Fallback to legacy implementation.");
        }
        return this.i != null;
    }
    
    public Method r(final Class clazz) {
        return clazz.getMethod("abortCreation", (Class[])new Class[0]);
    }
    
    public Method s(final Class clazz) {
        final Class<Integer> type = Integer.TYPE;
        return clazz.getMethod("addFontFromAssetManager", AssetManager.class, String.class, type, Boolean.TYPE, type, type, type, FontVariationAxis[].class);
    }
    
    public Method t(final Class clazz) {
        final Class<Integer> type = Integer.TYPE;
        return clazz.getMethod("addFontFromBuffer", ByteBuffer.class, type, FontVariationAxis[].class, type, type);
    }
    
    public Method u(Class class1) {
        class1 = Array.newInstance(class1, 1).getClass();
        final Class<Integer> type = Integer.TYPE;
        final Method declaredMethod = Typeface.class.getDeclaredMethod("createFromFamiliesWithDefault", class1, type, type);
        declaredMethod.setAccessible(true);
        return declaredMethod;
    }
    
    public Class v() {
        return Class.forName("android.graphics.FontFamily");
    }
    
    public Constructor w(final Class clazz) {
        return clazz.getConstructor((Class[])new Class[0]);
    }
    
    public Method x(final Class clazz) {
        return clazz.getMethod("freeze", (Class[])new Class[0]);
    }
}

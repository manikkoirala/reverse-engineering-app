import android.os.BaseBundle;
import android.view.accessibility.AccessibilityNodeInfo;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityNodeProvider;
import android.view.accessibility.AccessibilityEvent;
import java.lang.ref.WeakReference;
import android.util.SparseArray;
import android.text.style.ClickableSpan;
import java.util.Collections;
import java.util.List;
import android.view.View;
import android.view.View$AccessibilityDelegate;

// 
// Decompiled by Procyon v0.6.0
// 

public class p0
{
    private static final View$AccessibilityDelegate DEFAULT_DELEGATE;
    private final View$AccessibilityDelegate mBridge;
    private final View$AccessibilityDelegate mOriginalDelegate;
    
    static {
        DEFAULT_DELEGATE = new View$AccessibilityDelegate();
    }
    
    public p0() {
        this(p0.DEFAULT_DELEGATE);
    }
    
    public p0(final View$AccessibilityDelegate mOriginalDelegate) {
        this.mOriginalDelegate = mOriginalDelegate;
        this.mBridge = new a(this);
    }
    
    public static List<n1.a> getActionList(final View view) {
        List<Object> emptyList;
        if ((emptyList = (List)view.getTag(fb1.H)) == null) {
            emptyList = Collections.emptyList();
        }
        return (List<n1.a>)emptyList;
    }
    
    public final boolean a(final ClickableSpan clickableSpan, final View view) {
        if (clickableSpan != null) {
            final ClickableSpan[] p2 = n1.p(view.createAccessibilityNodeInfo().getText());
            for (int n = 0; p2 != null && n < p2.length; ++n) {
                if (clickableSpan.equals(p2[n])) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public final boolean b(final int n, final View view) {
        final SparseArray sparseArray = (SparseArray)view.getTag(fb1.I);
        if (sparseArray != null) {
            final WeakReference weakReference = (WeakReference)sparseArray.get(n);
            if (weakReference != null) {
                final ClickableSpan clickableSpan = (ClickableSpan)weakReference.get();
                if (this.a(clickableSpan, view)) {
                    clickableSpan.onClick(view);
                    return true;
                }
            }
        }
        return false;
    }
    
    public boolean dispatchPopulateAccessibilityEvent(final View view, final AccessibilityEvent accessibilityEvent) {
        return this.mOriginalDelegate.dispatchPopulateAccessibilityEvent(view, accessibilityEvent);
    }
    
    public o1 getAccessibilityNodeProvider(final View view) {
        final AccessibilityNodeProvider a = b.a(this.mOriginalDelegate, view);
        if (a != null) {
            return new o1(a);
        }
        return null;
    }
    
    public View$AccessibilityDelegate getBridge() {
        return this.mBridge;
    }
    
    public void onInitializeAccessibilityEvent(final View view, final AccessibilityEvent accessibilityEvent) {
        this.mOriginalDelegate.onInitializeAccessibilityEvent(view, accessibilityEvent);
    }
    
    public void onInitializeAccessibilityNodeInfo(final View view, final n1 n1) {
        this.mOriginalDelegate.onInitializeAccessibilityNodeInfo(view, n1.A0());
    }
    
    public void onPopulateAccessibilityEvent(final View view, final AccessibilityEvent accessibilityEvent) {
        this.mOriginalDelegate.onPopulateAccessibilityEvent(view, accessibilityEvent);
    }
    
    public boolean onRequestSendAccessibilityEvent(final ViewGroup viewGroup, final View view, final AccessibilityEvent accessibilityEvent) {
        return this.mOriginalDelegate.onRequestSendAccessibilityEvent(viewGroup, view, accessibilityEvent);
    }
    
    public boolean performAccessibilityAction(final View view, final int n, final Bundle bundle) {
        final List<n1.a> actionList = getActionList(view);
        final int n2 = 0;
        int n3 = 0;
        boolean d;
        while (true) {
            d = (n2 != 0);
            if (n3 >= actionList.size()) {
                break;
            }
            final n1.a a = actionList.get(n3);
            if (a.b() == n) {
                d = a.d(view, bundle);
                break;
            }
            ++n3;
        }
        boolean b = d;
        if (!d) {
            b = p0.b.b(this.mOriginalDelegate, view, n, bundle);
        }
        boolean b2 = b;
        if (!b) {
            b2 = b;
            if (n == fb1.a) {
                b2 = b;
                if (bundle != null) {
                    b2 = this.b(((BaseBundle)bundle).getInt("ACCESSIBILITY_CLICKABLE_SPAN_ID", -1), view);
                }
            }
        }
        return b2;
    }
    
    public void sendAccessibilityEvent(final View view, final int n) {
        this.mOriginalDelegate.sendAccessibilityEvent(view, n);
    }
    
    public void sendAccessibilityEventUnchecked(final View view, final AccessibilityEvent accessibilityEvent) {
        this.mOriginalDelegate.sendAccessibilityEventUnchecked(view, accessibilityEvent);
    }
    
    public static final class a extends View$AccessibilityDelegate
    {
        public final p0 a;
        
        public a(final p0 a) {
            this.a = a;
        }
        
        public boolean dispatchPopulateAccessibilityEvent(final View view, final AccessibilityEvent accessibilityEvent) {
            return this.a.dispatchPopulateAccessibilityEvent(view, accessibilityEvent);
        }
        
        public AccessibilityNodeProvider getAccessibilityNodeProvider(final View view) {
            final o1 accessibilityNodeProvider = this.a.getAccessibilityNodeProvider(view);
            AccessibilityNodeProvider accessibilityNodeProvider2;
            if (accessibilityNodeProvider != null) {
                accessibilityNodeProvider2 = (AccessibilityNodeProvider)accessibilityNodeProvider.e();
            }
            else {
                accessibilityNodeProvider2 = null;
            }
            return accessibilityNodeProvider2;
        }
        
        public void onInitializeAccessibilityEvent(final View view, final AccessibilityEvent accessibilityEvent) {
            this.a.onInitializeAccessibilityEvent(view, accessibilityEvent);
        }
        
        public void onInitializeAccessibilityNodeInfo(final View view, final AccessibilityNodeInfo accessibilityNodeInfo) {
            final n1 b0 = n1.B0(accessibilityNodeInfo);
            b0.s0(o32.X(view));
            b0.i0(o32.S(view));
            b0.n0(o32.p(view));
            b0.w0(o32.I(view));
            this.a.onInitializeAccessibilityNodeInfo(view, b0);
            b0.e(accessibilityNodeInfo.getText(), view);
            final List<n1.a> actionList = p0.getActionList(view);
            for (int i = 0; i < actionList.size(); ++i) {
                b0.b((n1.a)actionList.get(i));
            }
        }
        
        public void onPopulateAccessibilityEvent(final View view, final AccessibilityEvent accessibilityEvent) {
            this.a.onPopulateAccessibilityEvent(view, accessibilityEvent);
        }
        
        public boolean onRequestSendAccessibilityEvent(final ViewGroup viewGroup, final View view, final AccessibilityEvent accessibilityEvent) {
            return this.a.onRequestSendAccessibilityEvent(viewGroup, view, accessibilityEvent);
        }
        
        public boolean performAccessibilityAction(final View view, final int n, final Bundle bundle) {
            return this.a.performAccessibilityAction(view, n, bundle);
        }
        
        public void sendAccessibilityEvent(final View view, final int n) {
            this.a.sendAccessibilityEvent(view, n);
        }
        
        public void sendAccessibilityEventUnchecked(final View view, final AccessibilityEvent accessibilityEvent) {
            this.a.sendAccessibilityEventUnchecked(view, accessibilityEvent);
        }
    }
    
    public abstract static class b
    {
        public static AccessibilityNodeProvider a(final View$AccessibilityDelegate view$AccessibilityDelegate, final View view) {
            return view$AccessibilityDelegate.getAccessibilityNodeProvider(view);
        }
        
        public static boolean b(final View$AccessibilityDelegate view$AccessibilityDelegate, final View view, final int n, final Bundle bundle) {
            return view$AccessibilityDelegate.performAccessibilityAction(view, n, bundle);
        }
    }
}

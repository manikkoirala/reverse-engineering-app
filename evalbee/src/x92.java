import android.database.Cursor;
import java.util.ArrayList;
import android.os.CancellationSignal;
import java.util.Set;
import java.util.Collections;
import java.util.List;
import androidx.room.SharedSQLiteStatement;
import androidx.room.RoomDatabase;

// 
// Decompiled by Procyon v0.6.0
// 

public final class x92 implements w92
{
    public final RoomDatabase a;
    public final ix b;
    public final SharedSQLiteStatement c;
    
    public x92(final RoomDatabase a) {
        this.a = a;
        this.b = new ix(this, a) {
            public final x92 d;
            
            @Override
            public String e() {
                return "INSERT OR IGNORE INTO `WorkTag` (`tag`,`work_spec_id`) VALUES (?,?)";
            }
            
            public void k(final ws1 ws1, final v92 v92) {
                if (v92.a() == null) {
                    ws1.I(1);
                }
                else {
                    ws1.y(1, v92.a());
                }
                if (v92.b() == null) {
                    ws1.I(2);
                }
                else {
                    ws1.y(2, v92.b());
                }
            }
        };
        this.c = new SharedSQLiteStatement(this, a) {
            public final x92 d;
            
            @Override
            public String e() {
                return "DELETE FROM worktag WHERE work_spec_id=?";
            }
        };
    }
    
    public static List e() {
        return Collections.emptyList();
    }
    
    @Override
    public void a(final v92 v92) {
        this.a.d();
        this.a.e();
        try {
            this.b.j(v92);
            this.a.D();
        }
        finally {
            this.a.i();
        }
    }
    
    @Override
    public void b(final String s, final Set set) {
        w92.a.a(this, s, set);
    }
    
    @Override
    public List c(String string) {
        final sf1 c = sf1.c("SELECT DISTINCT tag FROM worktag WHERE work_spec_id=?", 1);
        if (string == null) {
            c.I(1);
        }
        else {
            c.y(1, string);
        }
        this.a.d();
        final Cursor b = bp.b(this.a, c, false, null);
        try {
            final ArrayList list = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                if (b.isNull(0)) {
                    string = null;
                }
                else {
                    string = b.getString(0);
                }
                list.add((Object)string);
            }
            return list;
        }
        finally {
            b.close();
            c.release();
        }
    }
}

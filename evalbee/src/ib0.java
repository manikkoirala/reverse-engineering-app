import android.os.BaseBundle;
import java.io.Serializable;
import com.google.firebase.heartbeatinfo.HeartBeatInfo;
import java.util.concurrent.ExecutionException;
import android.text.TextUtils;
import com.google.android.gms.tasks.Tasks;
import android.os.Build$VERSION;
import android.util.Log;
import java.security.NoSuchAlgorithmException;
import java.security.MessageDigest;
import com.google.android.gms.tasks.Continuation;
import java.util.concurrent.Executor;
import java.io.IOException;
import android.os.Bundle;
import android.util.Base64;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.cloudmessaging.Rpc;

// 
// Decompiled by Procyon v0.6.0
// 

public class ib0
{
    public final r10 a;
    public final cw0 b;
    public final Rpc c;
    public final r91 d;
    public final r91 e;
    public final r20 f;
    
    public ib0(final r10 a, final cw0 b, final Rpc c, final r91 d, final r91 e, final r20 f) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
    }
    
    public ib0(final r10 r10, final cw0 cw0, final r91 r11, final r91 r12, final r20 r13) {
        this(r10, cw0, new Rpc(r10.l()), r11, r12, r13);
    }
    
    public static String b(final byte[] array) {
        return Base64.encodeToString(array, 11);
    }
    
    public static boolean g(final String anObject) {
        return "SERVICE_NOT_AVAILABLE".equals(anObject) || "INTERNAL_SERVER_ERROR".equals(anObject) || "InternalServerError".equals(anObject);
    }
    
    public final Task c(final Task task) {
        return task.continueWith((Executor)new xu0(), (Continuation)new hb0(this));
    }
    
    public final String d() {
        final String o = this.a.o();
        try {
            return b(MessageDigest.getInstance("SHA-1").digest(o.getBytes()));
        }
        catch (final NoSuchAlgorithmException ex) {
            return "[HASH-ERROR]";
        }
    }
    
    public Task e() {
        return this.c(this.j(cw0.c(this.a), "*", new Bundle()));
    }
    
    public final String f(final Bundle obj) {
        if (obj == null) {
            throw new IOException("SERVICE_NOT_AVAILABLE");
        }
        final String string = ((BaseBundle)obj).getString("registration_id");
        if (string != null) {
            return string;
        }
        final String string2 = ((BaseBundle)obj).getString("unregistered");
        if (string2 != null) {
            return string2;
        }
        final String string3 = ((BaseBundle)obj).getString("error");
        if ("RST".equals(string3)) {
            throw new IOException("INSTANCE_ID_RESET");
        }
        if (string3 != null) {
            throw new IOException(string3);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unexpected response: ");
        sb.append(obj);
        Log.w("FirebaseMessaging", sb.toString(), new Throwable());
        throw new IOException("SERVICE_NOT_AVAILABLE");
    }
    
    public final void i(String b, final String s, final Bundle bundle) {
        ((BaseBundle)bundle).putString("scope", s);
        ((BaseBundle)bundle).putString("sender", (String)b);
        ((BaseBundle)bundle).putString("subtype", (String)b);
        ((BaseBundle)bundle).putString("gmp_app_id", this.a.p().c());
        ((BaseBundle)bundle).putString("gmsv", Integer.toString(this.b.d()));
        ((BaseBundle)bundle).putString("osv", Integer.toString(Build$VERSION.SDK_INT));
        ((BaseBundle)bundle).putString("app_ver", this.b.a());
        ((BaseBundle)bundle).putString("app_ver_name", this.b.b());
        ((BaseBundle)bundle).putString("firebase-app-name-hash", this.d());
        Label_0166: {
            try {
                b = ((pf0)Tasks.await(this.f.a(false))).b();
                if (!TextUtils.isEmpty((CharSequence)b)) {
                    ((BaseBundle)bundle).putString("Goog-Firebase-Installations-Auth", (String)b);
                    break Label_0166;
                }
                Log.w("FirebaseMessaging", "FIS auth token is empty");
                break Label_0166;
            }
            catch (final InterruptedException b) {}
            catch (final ExecutionException ex) {}
            Log.e("FirebaseMessaging", "Failed to get FIS auth token", (Throwable)b);
        }
        ((BaseBundle)bundle).putString("appid", (String)Tasks.await(this.f.getId()));
        final StringBuilder sb = new StringBuilder();
        sb.append("fcm-");
        sb.append("23.4.0");
        ((BaseBundle)bundle).putString("cliv", sb.toString());
        final HeartBeatInfo heartBeatInfo = (HeartBeatInfo)this.e.get();
        final v12 v12 = (v12)this.d.get();
        if (heartBeatInfo != null && v12 != null) {
            final HeartBeatInfo.HeartBeat a = heartBeatInfo.a("fire-iid");
            if (a != HeartBeatInfo.HeartBeat.NONE) {
                ((BaseBundle)bundle).putString("Firebase-Client-Log-Type", Integer.toString(a.getCode()));
                ((BaseBundle)bundle).putString("Firebase-Client", v12.a());
            }
        }
    }
    
    public final Task j(final String ex, final String s, final Bundle bundle) {
        try {
            this.i((String)ex, s, bundle);
            return this.c.send(bundle);
        }
        catch (final ExecutionException ex) {}
        catch (final InterruptedException ex2) {}
        return Tasks.forException((Exception)ex);
    }
    
    public Task k(final String s, final String s2) {
        final Bundle bundle = new Bundle();
        final StringBuilder sb = new StringBuilder();
        sb.append("/topics/");
        sb.append(s2);
        ((BaseBundle)bundle).putString("gcm.topic", sb.toString());
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("/topics/");
        sb2.append(s2);
        return this.c(this.j(s, sb2.toString(), bundle));
    }
    
    public Task l(final String s, final String s2) {
        final Bundle bundle = new Bundle();
        final StringBuilder sb = new StringBuilder();
        sb.append("/topics/");
        sb.append(s2);
        ((BaseBundle)bundle).putString("gcm.topic", sb.toString());
        ((BaseBundle)bundle).putString("delete", "1");
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("/topics/");
        sb2.append(s2);
        return this.c(this.j(s, sb2.toString(), bundle));
    }
}

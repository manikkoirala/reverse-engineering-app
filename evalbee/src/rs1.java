import android.content.ContentResolver;
import java.util.List;
import android.database.Cursor;

// 
// Decompiled by Procyon v0.6.0
// 

public final class rs1
{
    public static final rs1 a;
    
    static {
        a = new rs1();
    }
    
    public static final List a(final Cursor cursor) {
        fg0.e((Object)cursor, "cursor");
        final List notificationUris = cursor.getNotificationUris();
        fg0.b((Object)notificationUris);
        return notificationUris;
    }
    
    public static final void b(final Cursor cursor, final ContentResolver contentResolver, final List list) {
        fg0.e((Object)cursor, "cursor");
        fg0.e((Object)contentResolver, "cr");
        fg0.e((Object)list, "uris");
        cursor.setNotificationUris(contentResolver, list);
    }
}

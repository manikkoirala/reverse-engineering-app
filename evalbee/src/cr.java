import android.text.TextUtils;
import java.util.HashMap;
import java.util.Map;
import java.io.IOException;
import org.json.JSONObject;

// 
// Decompiled by Procyon v0.6.0
// 

public class cr implements bn1
{
    public final String a;
    public final jd0 b;
    public final zl0 c;
    
    public cr(final String s, final jd0 jd0) {
        this(s, jd0, zl0.f());
    }
    
    public cr(final String a, final jd0 b, final zl0 c) {
        if (a != null) {
            this.c = c;
            this.b = b;
            this.a = a;
            return;
        }
        throw new IllegalArgumentException("url must not be null.");
    }
    
    @Override
    public JSONObject a(final an1 an1, final boolean b) {
        if (b) {
            JSONObject g;
            try {
                final Map f = this.f(an1);
                final hd0 b2 = this.b(this.d(f), an1);
                final zl0 c = this.c;
                final StringBuilder sb = new StringBuilder();
                sb.append("Requesting settings from ");
                sb.append(this.a);
                c.b(sb.toString());
                final zl0 c2 = this.c;
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Settings query params were: ");
                sb2.append(f);
                c2.i(sb2.toString());
                g = this.g(b2.c());
            }
            catch (final IOException ex) {
                this.c.e("Settings request failed.", ex);
                g = null;
            }
            return g;
        }
        throw new RuntimeException("An invalid data collection token was used.");
    }
    
    public final hd0 b(final hd0 hd0, final an1 an1) {
        this.c(hd0, "X-CRASHLYTICS-GOOGLE-APP-ID", an1.a);
        this.c(hd0, "X-CRASHLYTICS-API-CLIENT-TYPE", "android");
        this.c(hd0, "X-CRASHLYTICS-API-CLIENT-VERSION", bn.i());
        this.c(hd0, "Accept", "application/json");
        this.c(hd0, "X-CRASHLYTICS-DEVICE-MODEL", an1.b);
        this.c(hd0, "X-CRASHLYTICS-OS-BUILD-VERSION", an1.c);
        this.c(hd0, "X-CRASHLYTICS-OS-DISPLAY-VERSION", an1.d);
        this.c(hd0, "X-CRASHLYTICS-INSTALLATION-ID", an1.e.a().c());
        return hd0;
    }
    
    public final void c(final hd0 hd0, final String s, final String s2) {
        if (s2 != null) {
            hd0.d(s, s2);
        }
    }
    
    public hd0 d(final Map map) {
        final hd0 a = this.b.a(this.a, map);
        final StringBuilder sb = new StringBuilder();
        sb.append("Crashlytics Android SDK/");
        sb.append(bn.i());
        return a.d("User-Agent", sb.toString()).d("X-CRASHLYTICS-DEVELOPER-TOKEN", "470fa2b4ae81cd56ecbcda9735803434cec591fa");
    }
    
    public final JSONObject e(final String str) {
        try {
            return new JSONObject(str);
        }
        catch (final Exception ex) {
            final zl0 c = this.c;
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed to parse settings JSON from ");
            sb.append(this.a);
            c.l(sb.toString(), ex);
            final zl0 c2 = this.c;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Settings response ");
            sb2.append(str);
            c2.k(sb2.toString());
            return null;
        }
    }
    
    public final Map f(final an1 an1) {
        final HashMap hashMap = new HashMap();
        hashMap.put("build_version", an1.h);
        hashMap.put("display_version", an1.g);
        hashMap.put("source", Integer.toString(an1.i));
        final String f = an1.f;
        if (!TextUtils.isEmpty((CharSequence)f)) {
            hashMap.put("instance", f);
        }
        return hashMap;
    }
    
    public JSONObject g(final kd0 kd0) {
        final int b = kd0.b();
        final zl0 c = this.c;
        final StringBuilder sb = new StringBuilder();
        sb.append("Settings response code was: ");
        sb.append(b);
        c.i(sb.toString());
        JSONObject e;
        if (this.h(b)) {
            e = this.e(kd0.a());
        }
        else {
            final zl0 c2 = this.c;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Settings request failed; (status: ");
            sb2.append(b);
            sb2.append(") from ");
            sb2.append(this.a);
            c2.d(sb2.toString());
            e = null;
        }
        return e;
    }
    
    public boolean h(final int n) {
        return n == 200 || n == 201 || n == 202 || n == 203;
    }
}

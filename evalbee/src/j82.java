import android.text.TextUtils;
import java.util.HashSet;
import java.util.Set;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import androidx.work.d;
import java.util.List;
import androidx.work.ExistingWorkPolicy;

// 
// Decompiled by Procyon v0.6.0
// 

public class j82 extends i82
{
    public static final String j;
    public final c92 a;
    public final String b;
    public final ExistingWorkPolicy c;
    public final List d;
    public final List e;
    public final List f;
    public final List g;
    public boolean h;
    public d i;
    
    static {
        j = xl0.i("WorkContinuationImpl");
    }
    
    public j82(final c92 c92, final String s, final ExistingWorkPolicy existingWorkPolicy, final List list) {
        this(c92, s, existingWorkPolicy, list, null);
    }
    
    public j82(final c92 a, final String b, final ExistingWorkPolicy c, final List d, final List g) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.g = g;
        this.e = new ArrayList(d.size());
        this.f = new ArrayList();
        if (g != null) {
            final Iterator iterator = g.iterator();
            while (iterator.hasNext()) {
                this.f.addAll(((j82)iterator.next()).f);
            }
        }
        for (int i = 0; i < d.size(); ++i) {
            if (c == ExistingWorkPolicy.REPLACE && ((n92)d.get(i)).d().g() != Long.MAX_VALUE) {
                throw new IllegalArgumentException("Next Schedule Time Override must be used with ExistingPeriodicWorkPolicyUPDATE (preferably) or KEEP");
            }
            final String b2 = d.get(i).b();
            this.e.add(b2);
            this.f.add(b2);
        }
    }
    
    public j82(final c92 c92, final List list) {
        this(c92, null, ExistingWorkPolicy.KEEP, list, null);
    }
    
    public static boolean i(final j82 j82, final Set set) {
        set.addAll(j82.c());
        final Set l = l(j82);
        final Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
            if (l.contains(iterator.next())) {
                return true;
            }
        }
        final List e = j82.e();
        if (e != null && !e.isEmpty()) {
            final Iterator iterator2 = e.iterator();
            while (iterator2.hasNext()) {
                if (i((j82)iterator2.next(), set)) {
                    return true;
                }
            }
        }
        set.removeAll(j82.c());
        return false;
    }
    
    public static Set l(final j82 j82) {
        final HashSet set = new HashSet();
        final List e = j82.e();
        if (e != null && !e.isEmpty()) {
            final Iterator iterator = e.iterator();
            while (iterator.hasNext()) {
                set.addAll(((j82)iterator.next()).c());
            }
        }
        return set;
    }
    
    public d a() {
        if (!this.h) {
            final fx fx = new fx(this);
            this.a.p().b(fx);
            this.i = fx.d();
        }
        else {
            final xl0 e = xl0.e();
            final String j = j82.j;
            final StringBuilder sb = new StringBuilder();
            sb.append("Already enqueued work ids (");
            sb.append(TextUtils.join((CharSequence)", ", (Iterable)this.e));
            sb.append(")");
            e.k(j, sb.toString());
        }
        return this.i;
    }
    
    public ExistingWorkPolicy b() {
        return this.c;
    }
    
    public List c() {
        return this.e;
    }
    
    public String d() {
        return this.b;
    }
    
    public List e() {
        return this.g;
    }
    
    public List f() {
        return this.d;
    }
    
    public c92 g() {
        return this.a;
    }
    
    public boolean h() {
        return i(this, new HashSet());
    }
    
    public boolean j() {
        return this.h;
    }
    
    public void k() {
        this.h = true;
    }
}

import android.widget.TextView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import com.ekodroid.omrevaluator.util.DataModels.SortAnswerResponseList;
import java.util.ArrayList;
import android.content.Context;
import android.widget.BaseAdapter;

// 
// Decompiled by Procyon v0.6.0
// 

public class i3 extends BaseAdapter
{
    public Context a;
    public ArrayList b;
    public SortAnswerResponseList.AnswerResponseSortBy c;
    
    public i3(final Context a, final ArrayList b, final SortAnswerResponseList.AnswerResponseSortBy c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public int getCount() {
        return this.b.size();
    }
    
    public Object getItem(final int index) {
        return this.b.get(index);
    }
    
    public long getItemId(final int n) {
        return n;
    }
    
    public View getView(final int index, final View view, final ViewGroup viewGroup) {
        final h5 h5 = this.b.get(index);
        final View inflate = ((LayoutInflater)this.a.getSystemService("layout_inflater")).inflate(2131493022, (ViewGroup)null);
        final TextView textView = (TextView)inflate.findViewById(2131297264);
        final TextView textView2 = (TextView)inflate.findViewById(2131297238);
        final TextView textView3 = (TextView)inflate.findViewById(2131297218);
        final TextView textView4 = (TextView)inflate.findViewById(2131297308);
        final StringBuilder sb = new StringBuilder();
        sb.append(h5.c());
        sb.append("");
        textView.setText((CharSequence)sb.toString());
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(h5.b());
        sb2.append("");
        textView2.setText((CharSequence)sb2.toString());
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(h5.a());
        sb3.append("");
        textView3.setText((CharSequence)sb3.toString());
        final StringBuilder sb4 = new StringBuilder();
        sb4.append(h5.d());
        sb4.append("");
        textView4.setText((CharSequence)sb4.toString());
        return inflate;
    }
}

import android.net.Uri$Builder;
import com.google.android.gms.common.internal.Preconditions;
import android.net.Uri;
import android.text.TextUtils;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import android.util.Log;
import com.google.android.gms.tasks.Tasks;
import java.util.concurrent.TimeUnit;
import com.google.android.gms.common.internal.Objects;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class m22
{
    public static boolean a(final Object o, final Object o2) {
        return Objects.equal(o, o2);
    }
    
    public static String b(dg0 b) {
        if (b == null) {
            return null;
        }
        try {
            final u5 u5 = (u5)Tasks.await(((dg0)b).a(false), 30000L, TimeUnit.MILLISECONDS);
            if (u5.a() != null) {
                b = (TimeoutException)new StringBuilder();
                ((StringBuilder)b).append("Error getting App Check token; using placeholder token instead. Error: ");
                ((StringBuilder)b).append(u5.a());
                Log.w("StorageUtil", ((StringBuilder)b).toString());
            }
            b = (TimeoutException)u5.b();
            return (String)b;
        }
        catch (final TimeoutException b) {}
        catch (final InterruptedException b) {}
        catch (final ExecutionException ex) {}
        final StringBuilder sb = new StringBuilder();
        sb.append("Unexpected error getting App Check token: ");
        sb.append(b);
        Log.e("StorageUtil", sb.toString());
        return null;
    }
    
    public static String c(zf0 c) {
        while (true) {
            if (c != null) {
                try {
                    c = (TimeoutException)((ya0)Tasks.await(((zf0)c).c(false), 30000L, TimeUnit.MILLISECONDS)).c();
                    while (true) {
                        if (!TextUtils.isEmpty((CharSequence)c)) {
                            return (String)c;
                        }
                        Log.w("StorageUtil", "no auth token for request");
                        return null;
                        c = null;
                        continue;
                    }
                }
                catch (final TimeoutException c) {}
                catch (final InterruptedException c) {}
                catch (final ExecutionException ex) {}
                final StringBuilder sb = new StringBuilder();
                sb.append("error getting token ");
                sb.append(c);
                Log.e("StorageUtil", sb.toString());
                return null;
            }
            continue;
        }
    }
    
    public static Uri d(final r10 r10, String str) {
        if (TextUtils.isEmpty((CharSequence)str)) {
            return null;
        }
        final Uri k = xy0.k;
        if (str.toLowerCase().startsWith("gs://")) {
            final String b = io1.b(io1.a(str.substring(5)));
            final StringBuilder sb = new StringBuilder();
            sb.append("gs://");
            sb.append(b);
            return Uri.parse(sb.toString());
        }
        final Uri parse = Uri.parse(str);
        str = parse.getScheme();
        if (str != null && (a(str.toLowerCase(), "http") || a(str.toLowerCase(), "https"))) {
            final int index = parse.getAuthority().toLowerCase().indexOf(k.getAuthority());
            str = io1.c(parse.getEncodedPath());
            String substring3;
            if (index == 0 && str.startsWith("/")) {
                final int index2 = str.indexOf("/b/", 0);
                final int n = index2 + 3;
                final int index3 = str.indexOf("/", n);
                final int index4 = str.indexOf("/o/", 0);
                if (index2 == -1 || index3 == -1) {
                    Log.w("StorageUtil", "Firebase Storage URLs must point to an object in your Storage Bucket. Please obtain a URL using the Firebase Console or getDownloadUrl().");
                    throw new IllegalArgumentException("Firebase Storage URLs must point to an object in your Storage Bucket. Please obtain a URL using the Firebase Console or getDownloadUrl().");
                }
                final String substring = str.substring(n, index3);
                String substring2;
                if (index4 != -1) {
                    substring2 = str.substring(index4 + 3);
                }
                else {
                    substring2 = "";
                }
                str = substring2;
                substring3 = substring;
            }
            else {
                if (index <= 1) {
                    Log.w("StorageUtil", "Firebase Storage URLs must point to an object in your Storage Bucket. Please obtain a URL using the Firebase Console or getDownloadUrl().");
                    throw new IllegalArgumentException("Firebase Storage URLs must point to an object in your Storage Bucket. Please obtain a URL using the Firebase Console or getDownloadUrl().");
                }
                substring3 = parse.getAuthority().substring(0, index - 1);
            }
            Preconditions.checkNotEmpty(substring3, "No bucket specified");
            return new Uri$Builder().scheme("gs").authority(substring3).encodedPath(str).build();
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("FirebaseStorage is unable to support the scheme:");
        sb2.append(str);
        Log.w("StorageUtil", sb2.toString());
        throw new IllegalArgumentException("Uri scheme");
    }
}

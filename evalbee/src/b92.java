import androidx.work.WorkerParameters;

// 
// Decompiled by Procyon v0.6.0
// 

public final class b92 implements a92
{
    public final q81 a;
    public final hu1 b;
    
    public b92(final q81 a, final hu1 b) {
        fg0.e((Object)a, "processor");
        fg0.e((Object)b, "workTaskExecutor");
        this.a = a;
        this.b = b;
    }
    
    @Override
    public void a(final op1 op1, final WorkerParameters.a a) {
        fg0.e((Object)op1, "workSpecId");
        this.b.b(new qp1(this.a, op1, a));
    }
    
    @Override
    public void d(final op1 op1, final int n) {
        fg0.e((Object)op1, "workSpecId");
        this.b.b(new cq1(this.a, op1, false, n));
    }
}

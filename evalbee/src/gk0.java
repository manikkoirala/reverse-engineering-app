import android.widget.AbsListView;
import android.widget.ListView;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class gk0
{
    public static boolean a(final ListView listView, final int n) {
        return a.a(listView, n);
    }
    
    public static void b(final ListView listView, final int n) {
        a.b(listView, n);
    }
    
    public abstract static class a
    {
        public static boolean a(final ListView listView, final int n) {
            return ((AbsListView)listView).canScrollList(n);
        }
        
        public static void b(final ListView listView, final int n) {
            ((AbsListView)listView).scrollListBy(n);
        }
    }
}

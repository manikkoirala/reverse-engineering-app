import com.google.android.recaptcha.RecaptchaAction;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.internal.firebase_auth_api.zzag;
import java.util.HashMap;
import com.google.firebase.auth.FirebaseAuth;
import com.google.android.gms.internal.firebase-auth-api.zzafk;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ud2
{
    public Map a;
    public zzafk b;
    public r10 c;
    public FirebaseAuth d;
    public td2 e;
    
    public ud2(final r10 r10, final FirebaseAuth firebaseAuth) {
        this(r10, firebaseAuth, new rd2());
    }
    
    public ud2(final r10 c, final FirebaseAuth d, final td2 e) {
        this.a = new HashMap();
        this.c = c;
        this.d = d;
        this.e = e;
    }
    
    public static String f(final String s) {
        String s2 = s;
        if (zzag.zzc(s)) {
            s2 = "*";
        }
        return s2;
    }
    
    public final Task a(String f, final Boolean b) {
        f = f(f);
        if (!b) {
            final Task e = this.e(f);
            if (e != null) {
                return e;
            }
        }
        return this.d.t("RECAPTCHA_ENTERPRISE").continueWithTask((Continuation)new xd2(this, f));
    }
    
    public final Task b(final String s, final Boolean b, final RecaptchaAction recaptchaAction) {
        final String f = f(s);
        final Task e = this.e(f);
        Task a;
        if (b || (a = e) == null) {
            a = this.a(f, b);
        }
        return a.continueWithTask((Continuation)new vd2(this, recaptchaAction));
    }
    
    public final boolean d(final String s) {
        final zzafk b = this.b;
        return b != null && ((com.google.android.gms.internal.firebase_auth_api.zzafk)b).zzb(s);
    }
    
    public final Task e(final String s) {
        return this.a.get(s);
    }
}

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import android.content.Context;
import android.content.pm.PackageManager$NameNotFoundException;
import com.google.android.gms.tasks.Tasks;
import java.util.concurrent.Callable;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Continuation;
import java.util.concurrent.Executor;
import com.google.firebase.crashlytics.internal.settings.a;
import com.google.firebase.crashlytics.internal.common.CommonUtils;
import com.google.firebase.sessions.api.SessionSubscriber;
import com.google.firebase.sessions.api.FirebaseSessionsDependencies;

// 
// Decompiled by Procyon v0.6.0
// 

public class j20
{
    public final bn a;
    
    public j20(final bn a) {
        this.a = a;
    }
    
    public static j20 a(final r10 r10, final r20 r11, final jr jr, final jr jr2, final jr jr3) {
        final Context l = r10.l();
        final String packageName = l.getPackageName();
        final zl0 f = zl0.f();
        final StringBuilder sb = new StringBuilder();
        sb.append("Initializing Firebase Crashlytics ");
        sb.append(bn.i());
        sb.append(" for ");
        sb.append(packageName);
        f.g(sb.toString());
        final z00 z00 = new z00(l);
        final dp dp = new dp(r10);
        final de0 de0 = new de0(l, packageName, r11, dp);
        final gn gn = new gn(jr);
        final l4 l2 = new l4(jr2);
        final ExecutorService c = vy.c("Crashlytics Exception Handler");
        final wm wm = new wm(dp, z00);
        FirebaseSessionsDependencies.e(wm);
        final bn bn = new bn(r10, de0, gn, dp, l2.e(), l2.d(), z00, c, wm, new hd1(jr3));
        final String c2 = r10.p().c();
        final String m = CommonUtils.m(l);
        final List j = CommonUtils.j(l);
        final zl0 f2 = zl0.f();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Mapping file ID is: ");
        sb2.append(m);
        f2.b(sb2.toString());
        for (final zc zc : j) {
            zl0.f().b(String.format("Build id for %s on %s: %s", zc.c(), zc.a(), zc.b()));
        }
        final ts ts = new ts(l);
        try {
            final s7 a = s7.a(l, de0, c2, m, j, ts);
            final zl0 f3 = zl0.f();
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Installer package name is: ");
            sb3.append(a.d);
            f3.i(sb3.toString());
            final ExecutorService c3 = vy.c("com.google.firebase.crashlytics.startup");
            final a i = com.google.firebase.crashlytics.internal.settings.a.l(l, c2, de0, new jd0(), a.f, a.g, z00, dp);
            i.p(c3).continueWith((Executor)c3, (Continuation)new Continuation() {
                public Object then(final Task task) {
                    if (!task.isSuccessful()) {
                        zl0.f().e("Error fetching settings.", task.getException());
                    }
                    return null;
                }
            });
            Tasks.call((Executor)c3, (Callable)new Callable(bn.n(a, i), bn, i) {
                public final boolean a;
                public final bn b;
                public final a c;
                
                public Void a() {
                    if (this.a) {
                        this.b.g(this.c);
                    }
                    return null;
                }
            });
            return new j20(bn);
        }
        catch (final PackageManager$NameNotFoundException ex) {
            zl0.f().e("Error retrieving app package info.", (Throwable)ex);
            return null;
        }
    }
}

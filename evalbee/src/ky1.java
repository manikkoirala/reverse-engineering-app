import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ky1
{
    public final tk a;
    public final rb b;
    public final tk c;
    public final tk d;
    
    public ky1(final Context context, final hu1 hu1, final tk a, final rb b, final tk c, final tk d) {
        fg0.e((Object)context, "context");
        fg0.e((Object)hu1, "taskExecutor");
        fg0.e((Object)a, "batteryChargingTracker");
        fg0.e((Object)b, "batteryNotLowTracker");
        fg0.e((Object)c, "networkStateTracker");
        fg0.e((Object)d, "storageNotLowTracker");
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
    
    public final tk a() {
        return this.a;
    }
    
    public final rb b() {
        return this.b;
    }
    
    public final tk c() {
        return this.c;
    }
    
    public final tk d() {
        return this.d;
    }
}

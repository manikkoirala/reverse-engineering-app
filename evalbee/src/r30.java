import com.google.android.gms.internal.firebase-auth-api.zzafn;
import com.google.firebase.auth.FirebaseAuth;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.tasks.Task;
import java.util.List;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class r30 extends AbstractSafeParcelable implements c22
{
    public abstract s30 E();
    
    public abstract ex0 H();
    
    public abstract List J();
    
    public abstract String K();
    
    public abstract String O();
    
    public abstract boolean R();
    
    public Task Z(final g22 g22) {
        Preconditions.checkNotNull(g22);
        return FirebaseAuth.getInstance(this.i0()).r(this, g22);
    }
    
    public abstract String getDisplayName();
    
    public abstract String getEmail();
    
    public Task i(final boolean b) {
        return FirebaseAuth.getInstance(this.i0()).s(this, b);
    }
    
    public abstract r10 i0();
    
    public abstract r30 m0(final List p0);
    
    public abstract void o0(final zzafn p0);
    
    public abstract r30 p0();
    
    public abstract void q0(final List p0);
    
    public abstract zzafn r0();
    
    public abstract List s0();
    
    public abstract String zzd();
    
    public abstract String zze();
}

import android.text.TextUtils;

// 
// Decompiled by Procyon v0.6.0
// 

public final class sc0
{
    public final String a;
    public final String b;
    
    public sc0(final String a, final String b) {
        this.a = a;
        this.b = b;
    }
    
    public final String a() {
        return this.a;
    }
    
    public final String b() {
        return this.b;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && sc0.class == o.getClass()) {
            final sc0 sc0 = (sc0)o;
            if (!TextUtils.equals((CharSequence)this.a, (CharSequence)sc0.a) || !TextUtils.equals((CharSequence)this.b, (CharSequence)sc0.b)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return this.a.hashCode() * 31 + this.b.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Header[name=");
        sb.append(this.a);
        sb.append(",value=");
        sb.append(this.b);
        sb.append("]");
        return sb.toString();
    }
}

import android.os.Trace;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class hy1
{
    public static long a;
    public static Method b;
    
    public static void a(final String s) {
        iy1.a(s);
    }
    
    public static void b() {
        iy1.b();
    }
    
    public static void c(final String str, final Exception ex) {
        if (!(ex instanceof InvocationTargetException)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unable to call ");
            sb.append(str);
            sb.append(" via reflection");
            Log.v("Trace", sb.toString(), (Throwable)ex);
            return;
        }
        final Throwable cause = ex.getCause();
        if (cause instanceof RuntimeException) {
            throw (RuntimeException)cause;
        }
        throw new RuntimeException(cause);
    }
    
    public static boolean d() {
        try {
            if (hy1.b == null) {
                return gy1.a();
            }
            return e();
        }
        catch (final NoSuchMethodError | NoClassDefFoundError noSuchMethodError | NoClassDefFoundError) {
            return e();
        }
    }
    
    public static boolean e() {
        try {
            if (hy1.b == null) {
                hy1.a = Trace.class.getField("TRACE_TAG_APP").getLong(null);
                hy1.b = Trace.class.getMethod("isTagEnabled", Long.TYPE);
            }
            return (boolean)hy1.b.invoke(null, hy1.a);
        }
        catch (final Exception ex) {
            c("isTagEnabled", ex);
            return false;
        }
    }
}

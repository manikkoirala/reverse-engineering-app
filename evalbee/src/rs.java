import android.database.Cursor;
import java.util.ArrayList;
import android.os.CancellationSignal;
import java.util.Collections;
import java.util.List;
import androidx.room.RoomDatabase;

// 
// Decompiled by Procyon v0.6.0
// 

public final class rs implements qs
{
    public final RoomDatabase a;
    public final ix b;
    
    public rs(final RoomDatabase a) {
        this.a = a;
        this.b = new ix(this, a) {
            public final rs d;
            
            @Override
            public String e() {
                return "INSERT OR IGNORE INTO `Dependency` (`work_spec_id`,`prerequisite_id`) VALUES (?,?)";
            }
            
            public void k(final ws1 ws1, final ns ns) {
                if (ns.b() == null) {
                    ws1.I(1);
                }
                else {
                    ws1.y(1, ns.b());
                }
                if (ns.a() == null) {
                    ws1.I(2);
                }
                else {
                    ws1.y(2, ns.a());
                }
            }
        };
    }
    
    public static List e() {
        return Collections.emptyList();
    }
    
    @Override
    public List a(String string) {
        final sf1 c = sf1.c("SELECT work_spec_id FROM dependency WHERE prerequisite_id=?", 1);
        if (string == null) {
            c.I(1);
        }
        else {
            c.y(1, string);
        }
        this.a.d();
        final Cursor b = bp.b(this.a, c, false, null);
        try {
            final ArrayList list = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                if (b.isNull(0)) {
                    string = null;
                }
                else {
                    string = b.getString(0);
                }
                list.add((Object)string);
            }
            return list;
        }
        finally {
            b.close();
            c.release();
        }
    }
    
    @Override
    public boolean b(final String s) {
        final boolean b = true;
        final sf1 c = sf1.c("SELECT COUNT(*)=0 FROM dependency WHERE work_spec_id=? AND prerequisite_id IN (SELECT id FROM workspec WHERE state!=2)", 1);
        if (s == null) {
            c.I(1);
        }
        else {
            c.y(1, s);
        }
        this.a.d();
        final RoomDatabase a = this.a;
        boolean b2 = false;
        final Cursor b3 = bp.b(a, c, false, null);
        try {
            if (b3.moveToFirst()) {
                b2 = (b3.getInt(0) != 0 && b);
            }
            return b2;
        }
        finally {
            b3.close();
            c.release();
        }
    }
    
    @Override
    public void c(final ns ns) {
        this.a.d();
        this.a.e();
        try {
            this.b.j(ns);
            this.a.D();
        }
        finally {
            this.a.i();
        }
    }
    
    @Override
    public boolean d(final String s) {
        final boolean b = true;
        final sf1 c = sf1.c("SELECT COUNT(*)>0 FROM dependency WHERE prerequisite_id=?", 1);
        if (s == null) {
            c.I(1);
        }
        else {
            c.y(1, s);
        }
        this.a.d();
        final RoomDatabase a = this.a;
        boolean b2 = false;
        final Cursor b3 = bp.b(a, c, false, null);
        try {
            if (b3.moveToFirst()) {
                b2 = (b3.getInt(0) != 0 && b);
            }
            return b2;
        }
        finally {
            b3.close();
            c.release();
        }
    }
}

import java.security.AccessController;
import java.lang.reflect.AccessibleObject;
import java.security.PrivilegedAction;
import java.lang.reflect.Field;
import java.util.HashMap;
import com.google.gson.reflect.TypeToken;
import java.util.Iterator;
import java.util.Map;
import java.util.ArrayDeque;
import java.util.StringTokenizer;
import java.util.Locale;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.util.Currency;
import java.util.UUID;
import java.net.InetAddress;
import java.net.URISyntaxException;
import com.google.gson.JsonIOException;
import java.net.URI;
import java.net.URL;
import com.google.gson.internal.LazilyParsedNumber;
import java.math.BigInteger;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicIntegerArray;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonToken;
import java.util.BitSet;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class kz1
{
    public static final hz1 A;
    public static final hz1 B;
    public static final iz1 C;
    public static final hz1 D;
    public static final iz1 E;
    public static final hz1 F;
    public static final iz1 G;
    public static final hz1 H;
    public static final iz1 I;
    public static final hz1 J;
    public static final iz1 K;
    public static final hz1 L;
    public static final iz1 M;
    public static final hz1 N;
    public static final iz1 O;
    public static final hz1 P;
    public static final iz1 Q;
    public static final hz1 R;
    public static final iz1 S;
    public static final hz1 T;
    public static final iz1 U;
    public static final hz1 V;
    public static final iz1 W;
    public static final iz1 X;
    public static final hz1 a;
    public static final iz1 b;
    public static final hz1 c;
    public static final iz1 d;
    public static final hz1 e;
    public static final hz1 f;
    public static final iz1 g;
    public static final hz1 h;
    public static final iz1 i;
    public static final hz1 j;
    public static final iz1 k;
    public static final hz1 l;
    public static final iz1 m;
    public static final hz1 n;
    public static final iz1 o;
    public static final hz1 p;
    public static final iz1 q;
    public static final hz1 r;
    public static final iz1 s;
    public static final hz1 t;
    public static final hz1 u;
    public static final hz1 v;
    public static final hz1 w;
    public static final iz1 x;
    public static final hz1 y;
    public static final hz1 z;
    
    static {
        b = a(Class.class, a = new hz1() {
            public Class e(final rh0 rh0) {
                throw new UnsupportedOperationException("Attempted to deserialize a java.lang.Class. Forgot to register a type adapter?");
            }
            
            public void f(final vh0 vh0, final Class clazz) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Attempted to serialize java.lang.Class: ");
                sb.append(clazz.getName());
                sb.append(". Forgot to register a type adapter?");
                throw new UnsupportedOperationException(sb.toString());
            }
        }.a());
        d = a(BitSet.class, c = new hz1() {
            public BitSet e(final rh0 rh0) {
                final BitSet set = new BitSet();
                rh0.a();
                JsonToken obj = rh0.o0();
                int bitIndex = 0;
                while (obj != JsonToken.END_ARRAY) {
                    final int n = kz1$a0.a[obj.ordinal()];
                    boolean x = true;
                    if (n != 1 && n != 2) {
                        if (n != 3) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Invalid bitset value type: ");
                            sb.append(obj);
                            sb.append("; at path ");
                            sb.append(rh0.getPath());
                            throw new JsonSyntaxException(sb.toString());
                        }
                        x = rh0.x();
                    }
                    else {
                        final int h = rh0.H();
                        if (h == 0) {
                            x = false;
                        }
                        else if (h != 1) {
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Invalid bitset value ");
                            sb2.append(h);
                            sb2.append(", expected 0 or 1; at path ");
                            sb2.append(rh0.j());
                            throw new JsonSyntaxException(sb2.toString());
                        }
                    }
                    if (x) {
                        set.set(bitIndex);
                    }
                    ++bitIndex;
                    obj = rh0.o0();
                }
                rh0.f();
                return set;
            }
            
            public void f(final vh0 vh0, final BitSet set) {
                vh0.c();
                for (int length = set.length(), i = 0; i < length; ++i) {
                    vh0.o0(set.get(i) ? 1 : 0);
                }
                vh0.f();
            }
        }.a());
        final hz1 hz1 = e = new hz1() {
            public Boolean e(final rh0 rh0) {
                final JsonToken o0 = rh0.o0();
                if (o0 == JsonToken.NULL) {
                    rh0.R();
                    return null;
                }
                if (o0 == JsonToken.STRING) {
                    return Boolean.parseBoolean(rh0.i0());
                }
                return rh0.x();
            }
            
            public void f(final vh0 vh0, final Boolean b) {
                vh0.p0(b);
            }
        };
        f = new hz1() {
            public Boolean e(final rh0 rh0) {
                if (rh0.o0() == JsonToken.NULL) {
                    rh0.R();
                    return null;
                }
                return Boolean.valueOf(rh0.i0());
            }
            
            public void f(final vh0 vh0, final Boolean b) {
                String string;
                if (b == null) {
                    string = "null";
                }
                else {
                    string = b.toString();
                }
                vh0.r0(string);
            }
        };
        g = b(Boolean.TYPE, Boolean.class, hz1);
        i = b(Byte.TYPE, Byte.class, h = new hz1() {
            public Number e(final rh0 rh0) {
                if (rh0.o0() == JsonToken.NULL) {
                    rh0.R();
                    return null;
                }
                try {
                    final int h = rh0.H();
                    if (h <= 255 && h >= -128) {
                        return (byte)h;
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Lossy conversion from ");
                    sb.append(h);
                    sb.append(" to byte; at path ");
                    sb.append(rh0.j());
                    throw new JsonSyntaxException(sb.toString());
                }
                catch (final NumberFormatException ex) {
                    throw new JsonSyntaxException(ex);
                }
            }
            
            public void f(final vh0 vh0, final Number n) {
                if (n == null) {
                    vh0.u();
                }
                else {
                    vh0.o0(n.byteValue());
                }
            }
        });
        k = b(Short.TYPE, Short.class, j = new hz1() {
            public Number e(final rh0 rh0) {
                if (rh0.o0() == JsonToken.NULL) {
                    rh0.R();
                    return null;
                }
                try {
                    final int h = rh0.H();
                    if (h <= 65535 && h >= -32768) {
                        return (short)h;
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Lossy conversion from ");
                    sb.append(h);
                    sb.append(" to short; at path ");
                    sb.append(rh0.j());
                    throw new JsonSyntaxException(sb.toString());
                }
                catch (final NumberFormatException ex) {
                    throw new JsonSyntaxException(ex);
                }
            }
            
            public void f(final vh0 vh0, final Number n) {
                if (n == null) {
                    vh0.u();
                }
                else {
                    vh0.o0(n.shortValue());
                }
            }
        });
        m = b(Integer.TYPE, Integer.class, l = new hz1() {
            public Number e(final rh0 rh0) {
                if (rh0.o0() == JsonToken.NULL) {
                    rh0.R();
                    return null;
                }
                try {
                    return rh0.H();
                }
                catch (final NumberFormatException ex) {
                    throw new JsonSyntaxException(ex);
                }
            }
            
            public void f(final vh0 vh0, final Number n) {
                if (n == null) {
                    vh0.u();
                }
                else {
                    vh0.o0(n.intValue());
                }
            }
        });
        o = a(AtomicInteger.class, n = new hz1() {
            public AtomicInteger e(final rh0 rh0) {
                try {
                    return new AtomicInteger(rh0.H());
                }
                catch (final NumberFormatException ex) {
                    throw new JsonSyntaxException(ex);
                }
            }
            
            public void f(final vh0 vh0, final AtomicInteger atomicInteger) {
                vh0.o0(atomicInteger.get());
            }
        }.a());
        q = a(AtomicBoolean.class, p = new hz1() {
            public AtomicBoolean e(final rh0 rh0) {
                return new AtomicBoolean(rh0.x());
            }
            
            public void f(final vh0 vh0, final AtomicBoolean atomicBoolean) {
                vh0.s0(atomicBoolean.get());
            }
        }.a());
        s = a(AtomicIntegerArray.class, r = new hz1() {
            public AtomicIntegerArray e(final rh0 rh0) {
                final ArrayList list = new ArrayList();
                rh0.a();
                while (rh0.k()) {
                    try {
                        list.add(rh0.H());
                        continue;
                    }
                    catch (final NumberFormatException ex) {
                        throw new JsonSyntaxException(ex);
                    }
                    break;
                }
                rh0.f();
                final int size = list.size();
                final AtomicIntegerArray atomicIntegerArray = new AtomicIntegerArray(size);
                for (int i = 0; i < size; ++i) {
                    atomicIntegerArray.set(i, (int)list.get(i));
                }
                return atomicIntegerArray;
            }
            
            public void f(final vh0 vh0, final AtomicIntegerArray atomicIntegerArray) {
                vh0.c();
                for (int length = atomicIntegerArray.length(), i = 0; i < length; ++i) {
                    vh0.o0(atomicIntegerArray.get(i));
                }
                vh0.f();
            }
        }.a());
        t = new hz1() {
            public Number e(final rh0 rh0) {
                if (rh0.o0() == JsonToken.NULL) {
                    rh0.R();
                    return null;
                }
                try {
                    return rh0.J();
                }
                catch (final NumberFormatException ex) {
                    throw new JsonSyntaxException(ex);
                }
            }
            
            public void f(final vh0 vh0, final Number n) {
                if (n == null) {
                    vh0.u();
                }
                else {
                    vh0.o0(n.longValue());
                }
            }
        };
        u = new hz1() {
            public Number e(final rh0 rh0) {
                if (rh0.o0() == JsonToken.NULL) {
                    rh0.R();
                    return null;
                }
                return (float)rh0.E();
            }
            
            public void f(final vh0 vh0, Number value) {
                if (value == null) {
                    vh0.u();
                }
                else {
                    if (!(value instanceof Float)) {
                        value = value.floatValue();
                    }
                    vh0.q0(value);
                }
            }
        };
        v = new hz1() {
            public Number e(final rh0 rh0) {
                if (rh0.o0() == JsonToken.NULL) {
                    rh0.R();
                    return null;
                }
                return rh0.E();
            }
            
            public void f(final vh0 vh0, final Number n) {
                if (n == null) {
                    vh0.u();
                }
                else {
                    vh0.m0(n.doubleValue());
                }
            }
        };
        x = b(Character.TYPE, Character.class, w = new hz1() {
            public Character e(final rh0 rh0) {
                if (rh0.o0() == JsonToken.NULL) {
                    rh0.R();
                    return null;
                }
                final String i0 = rh0.i0();
                if (i0.length() == 1) {
                    return i0.charAt(0);
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Expecting character, got: ");
                sb.append(i0);
                sb.append("; at ");
                sb.append(rh0.j());
                throw new JsonSyntaxException(sb.toString());
            }
            
            public void f(final vh0 vh0, final Character obj) {
                String value;
                if (obj == null) {
                    value = null;
                }
                else {
                    value = String.valueOf(obj);
                }
                vh0.r0(value);
            }
        });
        final hz1 hz2 = y = new hz1() {
            public String e(final rh0 rh0) {
                final JsonToken o0 = rh0.o0();
                if (o0 == JsonToken.NULL) {
                    rh0.R();
                    return null;
                }
                if (o0 == JsonToken.BOOLEAN) {
                    return Boolean.toString(rh0.x());
                }
                return rh0.i0();
            }
            
            public void f(final vh0 vh0, final String s) {
                vh0.r0(s);
            }
        };
        z = new hz1() {
            public BigDecimal e(final rh0 rh0) {
                if (rh0.o0() == JsonToken.NULL) {
                    rh0.R();
                    return null;
                }
                final String i0 = rh0.i0();
                try {
                    return new BigDecimal(i0);
                }
                catch (final NumberFormatException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Failed parsing '");
                    sb.append(i0);
                    sb.append("' as BigDecimal; at path ");
                    sb.append(rh0.j());
                    throw new JsonSyntaxException(sb.toString(), ex);
                }
            }
            
            public void f(final vh0 vh0, final BigDecimal bigDecimal) {
                vh0.q0(bigDecimal);
            }
        };
        A = new hz1() {
            public BigInteger e(final rh0 rh0) {
                if (rh0.o0() == JsonToken.NULL) {
                    rh0.R();
                    return null;
                }
                final String i0 = rh0.i0();
                try {
                    return new BigInteger(i0);
                }
                catch (final NumberFormatException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Failed parsing '");
                    sb.append(i0);
                    sb.append("' as BigInteger; at path ");
                    sb.append(rh0.j());
                    throw new JsonSyntaxException(sb.toString(), ex);
                }
            }
            
            public void f(final vh0 vh0, final BigInteger bigInteger) {
                vh0.q0(bigInteger);
            }
        };
        B = new hz1() {
            public LazilyParsedNumber e(final rh0 rh0) {
                if (rh0.o0() == JsonToken.NULL) {
                    rh0.R();
                    return null;
                }
                return new LazilyParsedNumber(rh0.i0());
            }
            
            public void f(final vh0 vh0, final LazilyParsedNumber lazilyParsedNumber) {
                vh0.q0(lazilyParsedNumber);
            }
        };
        C = a(String.class, hz2);
        E = a(StringBuilder.class, D = new hz1() {
            public StringBuilder e(final rh0 rh0) {
                if (rh0.o0() == JsonToken.NULL) {
                    rh0.R();
                    return null;
                }
                return new StringBuilder(rh0.i0());
            }
            
            public void f(final vh0 vh0, final StringBuilder sb) {
                String string;
                if (sb == null) {
                    string = null;
                }
                else {
                    string = sb.toString();
                }
                vh0.r0(string);
            }
        });
        G = a(StringBuffer.class, F = new hz1() {
            public StringBuffer e(final rh0 rh0) {
                if (rh0.o0() == JsonToken.NULL) {
                    rh0.R();
                    return null;
                }
                return new StringBuffer(rh0.i0());
            }
            
            public void f(final vh0 vh0, final StringBuffer sb) {
                String string;
                if (sb == null) {
                    string = null;
                }
                else {
                    string = sb.toString();
                }
                vh0.r0(string);
            }
        });
        I = a(URL.class, H = new hz1() {
            public URL e(final rh0 rh0) {
                final JsonToken o0 = rh0.o0();
                final JsonToken null = JsonToken.NULL;
                final URL url = null;
                if (o0 == null) {
                    rh0.R();
                    return null;
                }
                final String i0 = rh0.i0();
                URL url2;
                if ("null".equals(i0)) {
                    url2 = url;
                }
                else {
                    url2 = new URL(i0);
                }
                return url2;
            }
            
            public void f(final vh0 vh0, final URL url) {
                String externalForm;
                if (url == null) {
                    externalForm = null;
                }
                else {
                    externalForm = url.toExternalForm();
                }
                vh0.r0(externalForm);
            }
        });
        K = a(URI.class, J = new hz1() {
            public URI e(final rh0 rh0) {
                final JsonToken o0 = rh0.o0();
                final JsonToken null = JsonToken.NULL;
                final URI uri = null;
                if (o0 == null) {
                    rh0.R();
                    return null;
                }
                try {
                    final String i0 = rh0.i0();
                    URI uri2;
                    if ("null".equals(i0)) {
                        uri2 = uri;
                    }
                    else {
                        uri2 = new URI(i0);
                    }
                    return uri2;
                }
                catch (final URISyntaxException ex) {
                    throw new JsonIOException(ex);
                }
            }
            
            public void f(final vh0 vh0, final URI uri) {
                String asciiString;
                if (uri == null) {
                    asciiString = null;
                }
                else {
                    asciiString = uri.toASCIIString();
                }
                vh0.r0(asciiString);
            }
        });
        M = d(InetAddress.class, L = new hz1() {
            public InetAddress e(final rh0 rh0) {
                if (rh0.o0() == JsonToken.NULL) {
                    rh0.R();
                    return null;
                }
                return InetAddress.getByName(rh0.i0());
            }
            
            public void f(final vh0 vh0, final InetAddress inetAddress) {
                String hostAddress;
                if (inetAddress == null) {
                    hostAddress = null;
                }
                else {
                    hostAddress = inetAddress.getHostAddress();
                }
                vh0.r0(hostAddress);
            }
        });
        O = a(UUID.class, N = new hz1() {
            public UUID e(final rh0 rh0) {
                if (rh0.o0() == JsonToken.NULL) {
                    rh0.R();
                    return null;
                }
                final String i0 = rh0.i0();
                try {
                    return UUID.fromString(i0);
                }
                catch (final IllegalArgumentException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Failed parsing '");
                    sb.append(i0);
                    sb.append("' as UUID; at path ");
                    sb.append(rh0.j());
                    throw new JsonSyntaxException(sb.toString(), ex);
                }
            }
            
            public void f(final vh0 vh0, final UUID uuid) {
                String string;
                if (uuid == null) {
                    string = null;
                }
                else {
                    string = uuid.toString();
                }
                vh0.r0(string);
            }
        });
        Q = a(Currency.class, P = new hz1() {
            public Currency e(final rh0 rh0) {
                final String i0 = rh0.i0();
                try {
                    return Currency.getInstance(i0);
                }
                catch (final IllegalArgumentException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Failed parsing '");
                    sb.append(i0);
                    sb.append("' as Currency; at path ");
                    sb.append(rh0.j());
                    throw new JsonSyntaxException(sb.toString(), ex);
                }
            }
            
            public void f(final vh0 vh0, final Currency currency) {
                vh0.r0(currency.getCurrencyCode());
            }
        }.a());
        S = c(Calendar.class, GregorianCalendar.class, R = new hz1() {
            public Calendar e(final rh0 rh0) {
                if (rh0.o0() == JsonToken.NULL) {
                    rh0.R();
                    return null;
                }
                rh0.b();
                int year = 0;
                int month = 0;
                int hourOfDay;
                final int n = hourOfDay = month;
                int second;
                int minute = second = hourOfDay;
                int dayOfMonth = n;
                while (rh0.o0() != JsonToken.END_OBJECT) {
                    final String k = rh0.K();
                    final int h = rh0.H();
                    if ("year".equals(k)) {
                        year = h;
                    }
                    else if ("month".equals(k)) {
                        month = h;
                    }
                    else if ("dayOfMonth".equals(k)) {
                        dayOfMonth = h;
                    }
                    else if ("hourOfDay".equals(k)) {
                        hourOfDay = h;
                    }
                    else if ("minute".equals(k)) {
                        minute = h;
                    }
                    else {
                        if (!"second".equals(k)) {
                            continue;
                        }
                        second = h;
                    }
                }
                rh0.g();
                return new GregorianCalendar(year, month, dayOfMonth, hourOfDay, minute, second);
            }
            
            public void f(final vh0 vh0, final Calendar calendar) {
                if (calendar == null) {
                    vh0.u();
                    return;
                }
                vh0.d();
                vh0.o("year");
                vh0.o0(calendar.get(1));
                vh0.o("month");
                vh0.o0(calendar.get(2));
                vh0.o("dayOfMonth");
                vh0.o0(calendar.get(5));
                vh0.o("hourOfDay");
                vh0.o0(calendar.get(11));
                vh0.o("minute");
                vh0.o0(calendar.get(12));
                vh0.o("second");
                vh0.o0(calendar.get(13));
                vh0.g();
            }
        });
        U = a(Locale.class, T = new hz1() {
            public Locale e(final rh0 rh0) {
                final JsonToken o0 = rh0.o0();
                final JsonToken null = JsonToken.NULL;
                String nextToken = null;
                if (o0 == null) {
                    rh0.R();
                    return null;
                }
                final StringTokenizer stringTokenizer = new StringTokenizer(rh0.i0(), "_");
                String nextToken2;
                if (stringTokenizer.hasMoreElements()) {
                    nextToken2 = stringTokenizer.nextToken();
                }
                else {
                    nextToken2 = null;
                }
                String nextToken3;
                if (stringTokenizer.hasMoreElements()) {
                    nextToken3 = stringTokenizer.nextToken();
                }
                else {
                    nextToken3 = null;
                }
                if (stringTokenizer.hasMoreElements()) {
                    nextToken = stringTokenizer.nextToken();
                }
                if (nextToken3 == null && nextToken == null) {
                    return new Locale(nextToken2);
                }
                if (nextToken == null) {
                    return new Locale(nextToken2, nextToken3);
                }
                return new Locale(nextToken2, nextToken3, nextToken);
            }
            
            public void f(final vh0 vh0, final Locale locale) {
                String string;
                if (locale == null) {
                    string = null;
                }
                else {
                    string = locale.toString();
                }
                vh0.r0(string);
            }
        });
        W = d(nh0.class, V = new hz1() {
            public nh0 e(final rh0 rh0) {
                final JsonToken o0 = rh0.o0();
                nh0 g = this.g(rh0, o0);
                if (g == null) {
                    return this.f(rh0, o0);
                }
                final ArrayDeque arrayDeque = new ArrayDeque();
                while (true) {
                    if (rh0.k()) {
                        String k;
                        if (g instanceof ph0) {
                            k = rh0.K();
                        }
                        else {
                            k = null;
                        }
                        final JsonToken o2 = rh0.o0();
                        final nh0 g2 = this.g(rh0, o2);
                        final boolean b = g2 != null;
                        nh0 f = g2;
                        if (g2 == null) {
                            f = this.f(rh0, o2);
                        }
                        if (g instanceof ih0) {
                            ((ih0)g).n(f);
                        }
                        else {
                            ((ph0)g).n(k, f);
                        }
                        if (!b) {
                            continue;
                        }
                        arrayDeque.addLast(g);
                        g = f;
                    }
                    else {
                        if (g instanceof ih0) {
                            rh0.f();
                        }
                        else {
                            rh0.g();
                        }
                        if (arrayDeque.isEmpty()) {
                            break;
                        }
                        g = (nh0)arrayDeque.removeLast();
                    }
                }
                return g;
            }
            
            public final nh0 f(final rh0 rh0, final JsonToken obj) {
                final int n = kz1$a0.a[obj.ordinal()];
                if (n == 1) {
                    return new qh0(new LazilyParsedNumber(rh0.i0()));
                }
                if (n == 2) {
                    return new qh0(rh0.i0());
                }
                if (n == 3) {
                    return new qh0(rh0.x());
                }
                if (n == 6) {
                    rh0.R();
                    return oh0.a;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Unexpected token: ");
                sb.append(obj);
                throw new IllegalStateException(sb.toString());
            }
            
            public final nh0 g(final rh0 rh0, final JsonToken jsonToken) {
                final int n = kz1$a0.a[jsonToken.ordinal()];
                if (n == 4) {
                    rh0.a();
                    return new ih0();
                }
                if (n != 5) {
                    return null;
                }
                rh0.b();
                return new ph0();
            }
            
            public void h(final vh0 vh0, final nh0 nh0) {
                if (nh0 != null && !nh0.i()) {
                    if (nh0.m()) {
                        final qh0 c = nh0.c();
                        if (c.t()) {
                            vh0.q0(c.o());
                        }
                        else if (c.r()) {
                            vh0.s0(c.n());
                        }
                        else {
                            vh0.r0(c.p());
                        }
                    }
                    else if (nh0.g()) {
                        vh0.c();
                        final Iterator iterator = nh0.a().iterator();
                        while (iterator.hasNext()) {
                            this.h(vh0, (nh0)iterator.next());
                        }
                        vh0.f();
                    }
                    else {
                        if (!nh0.l()) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Couldn't write ");
                            sb.append(nh0.getClass());
                            throw new IllegalArgumentException(sb.toString());
                        }
                        vh0.d();
                        for (final Map.Entry<String, V> entry : nh0.b().entrySet()) {
                            vh0.o(entry.getKey());
                            this.h(vh0, (nh0)entry.getValue());
                        }
                        vh0.g();
                    }
                }
                else {
                    vh0.u();
                }
            }
        });
        X = new iz1() {
            @Override
            public hz1 a(final gc0 gc0, final TypeToken typeToken) {
                final Class rawType = typeToken.getRawType();
                if (Enum.class.isAssignableFrom(rawType) && rawType != Enum.class) {
                    Class superclass = rawType;
                    if (!rawType.isEnum()) {
                        superclass = rawType.getSuperclass();
                    }
                    return new i0(superclass);
                }
                return null;
            }
        };
    }
    
    public static iz1 a(final Class clazz, final hz1 hz1) {
        return new iz1(clazz, hz1) {
            public final Class a;
            public final hz1 b;
            
            @Override
            public hz1 a(final gc0 gc0, final TypeToken typeToken) {
                hz1 b;
                if (typeToken.getRawType() == this.a) {
                    b = this.b;
                }
                else {
                    b = null;
                }
                return b;
            }
            
            @Override
            public String toString() {
                final StringBuilder sb = new StringBuilder();
                sb.append("Factory[type=");
                sb.append(this.a.getName());
                sb.append(",adapter=");
                sb.append(this.b);
                sb.append("]");
                return sb.toString();
            }
        };
    }
    
    public static iz1 b(final Class clazz, final Class clazz2, final hz1 hz1) {
        return new iz1(clazz, clazz2, hz1) {
            public final Class a;
            public final Class b;
            public final hz1 c;
            
            @Override
            public hz1 a(final gc0 gc0, final TypeToken typeToken) {
                final Class rawType = typeToken.getRawType();
                hz1 c;
                if (rawType != this.a && rawType != this.b) {
                    c = null;
                }
                else {
                    c = this.c;
                }
                return c;
            }
            
            @Override
            public String toString() {
                final StringBuilder sb = new StringBuilder();
                sb.append("Factory[type=");
                sb.append(this.b.getName());
                sb.append("+");
                sb.append(this.a.getName());
                sb.append(",adapter=");
                sb.append(this.c);
                sb.append("]");
                return sb.toString();
            }
        };
    }
    
    public static iz1 c(final Class clazz, final Class clazz2, final hz1 hz1) {
        return new iz1(clazz, clazz2, hz1) {
            public final Class a;
            public final Class b;
            public final hz1 c;
            
            @Override
            public hz1 a(final gc0 gc0, final TypeToken typeToken) {
                final Class rawType = typeToken.getRawType();
                hz1 c;
                if (rawType != this.a && rawType != this.b) {
                    c = null;
                }
                else {
                    c = this.c;
                }
                return c;
            }
            
            @Override
            public String toString() {
                final StringBuilder sb = new StringBuilder();
                sb.append("Factory[type=");
                sb.append(this.a.getName());
                sb.append("+");
                sb.append(this.b.getName());
                sb.append(",adapter=");
                sb.append(this.c);
                sb.append("]");
                return sb.toString();
            }
        };
    }
    
    public static iz1 d(final Class clazz, final hz1 hz1) {
        return new iz1(clazz, hz1) {
            public final Class a;
            public final hz1 b;
            
            @Override
            public hz1 a(final gc0 gc0, final TypeToken typeToken) {
                final Class rawType = typeToken.getRawType();
                if (!this.a.isAssignableFrom(rawType)) {
                    return null;
                }
                return new hz1(this, rawType) {
                    public final Class a;
                    public final kz1$z b;
                    
                    @Override
                    public Object b(final rh0 rh0) {
                        final Object b = this.b.b.b(rh0);
                        if (b != null && !this.a.isInstance(b)) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Expected a ");
                            sb.append(this.a.getName());
                            sb.append(" but was ");
                            sb.append(b.getClass().getName());
                            sb.append("; at path ");
                            sb.append(rh0.j());
                            throw new JsonSyntaxException(sb.toString());
                        }
                        return b;
                    }
                    
                    @Override
                    public void d(final vh0 vh0, final Object o) {
                        this.b.b.d(vh0, o);
                    }
                };
            }
            
            @Override
            public String toString() {
                final StringBuilder sb = new StringBuilder();
                sb.append("Factory[typeHierarchy=");
                sb.append(this.a.getName());
                sb.append(",adapter=");
                sb.append(this.b);
                sb.append("]");
                return sb.toString();
            }
        };
    }
    
    public static final class i0 extends hz1
    {
        public final Map a;
        public final Map b;
        public final Map c;
        
        public i0(final Class clazz) {
            this.a = new HashMap();
            this.b = new HashMap();
            this.c = new HashMap();
            try {
                for (final Field field : AccessController.doPrivileged((PrivilegedAction<Field[]>)new PrivilegedAction(this, clazz) {
                    public final Class a;
                    public final i0 b;
                    
                    public Field[] a() {
                        final Field[] declaredFields = this.a.getDeclaredFields();
                        final ArrayList list = new ArrayList<Field>(declaredFields.length);
                        for (final Field e : declaredFields) {
                            if (e.isEnumConstant()) {
                                list.add(e);
                            }
                        }
                        final Field[] array = list.toArray(new Field[0]);
                        AccessibleObject.setAccessible(array, true);
                        return array;
                    }
                })) {
                    final Enum enum1 = (Enum)field.get(null);
                    String name = enum1.name();
                    final String string = enum1.toString();
                    final ml1 ml1 = field.getAnnotation(ml1.class);
                    if (ml1 != null) {
                        final String value = ml1.value();
                        final String[] alternate = ml1.alternate();
                        final int length2 = alternate.length;
                        int n = 0;
                        while (true) {
                            name = value;
                            if (n >= length2) {
                                break;
                            }
                            this.a.put(alternate[n], enum1);
                            ++n;
                        }
                    }
                    this.a.put(name, enum1);
                    this.b.put(string, enum1);
                    this.c.put(enum1, name);
                }
            }
            catch (final IllegalAccessException detailMessage) {
                throw new AssertionError((Object)detailMessage);
            }
        }
        
        public Enum e(final rh0 rh0) {
            if (rh0.o0() == JsonToken.NULL) {
                rh0.R();
                return null;
            }
            final String i0 = rh0.i0();
            Enum enum1;
            if ((enum1 = this.a.get(i0)) == null) {
                enum1 = this.b.get(i0);
            }
            return enum1;
        }
        
        public void f(final vh0 vh0, final Enum enum1) {
            String s;
            if (enum1 == null) {
                s = null;
            }
            else {
                s = this.c.get(enum1);
            }
            vh0.r0(s);
        }
    }
}

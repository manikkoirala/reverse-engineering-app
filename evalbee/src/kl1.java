import java.util.concurrent.Executor;
import java.util.ArrayDeque;

// 
// Decompiled by Procyon v0.6.0
// 

public class kl1 implements jl1
{
    public final ArrayDeque a;
    public final Executor b;
    public Runnable c;
    public final Object d;
    
    public kl1(final Executor b) {
        this.b = b;
        this.a = new ArrayDeque();
        this.d = new Object();
    }
    
    @Override
    public boolean E() {
        synchronized (this.d) {
            return !this.a.isEmpty();
        }
    }
    
    public void a() {
        final Runnable c = this.a.poll();
        this.c = c;
        if (c != null) {
            this.b.execute(c);
        }
    }
    
    @Override
    public void execute(final Runnable runnable) {
        synchronized (this.d) {
            this.a.add(new a(this, runnable));
            if (this.c == null) {
                this.a();
            }
        }
    }
    
    public static class a implements Runnable
    {
        public final kl1 a;
        public final Runnable b;
        
        public a(final kl1 a, final Runnable b) {
            this.a = a;
            this.b = b;
        }
        
        @Override
        public void run() {
            try {
                this.b.run();
                synchronized (this.a.d) {
                    this.a.a();
                }
            }
            finally {
                synchronized (this.a.d) {
                    this.a.a();
                    monitorexit(this.a.d);
                }
            }
        }
    }
}

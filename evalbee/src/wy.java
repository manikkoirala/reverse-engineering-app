import android.os.AsyncTask;
import com.google.android.gms.tasks.TaskExecutors;
import java.util.concurrent.Executor;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class wy
{
    public static final Executor a;
    public static final Executor b;
    public static final Executor c;
    
    static {
        a = TaskExecutors.MAIN_THREAD;
        b = new xu0();
        c = new ew1(4, AsyncTask.THREAD_POOL_EXECUTOR);
    }
}

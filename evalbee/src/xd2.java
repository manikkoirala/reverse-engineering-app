import java.util.List;
import android.app.Application;
import android.util.Log;
import android.text.TextUtils;
import com.google.android.gms.internal.firebase_auth_api.zzab;
import com.google.android.gms.internal.firebase_auth_api.zzag;
import com.google.android.gms.internal.firebase_auth_api.zzafk;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.auth.internal.zzbt;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Continuation;

// 
// Decompiled by Procyon v0.6.0
// 

public final class xd2 implements Continuation
{
    public final String a;
    public final ud2 b;
    
    public xd2(final ud2 b, final String a) {
        this.b = b;
        this.a = a;
    }
}

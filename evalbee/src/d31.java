// 
// Decompiled by Procyon v0.6.0
// 

public abstract class d31
{
    public static double[][] a;
    
    static {
        d31.a = new double[][] { { 1.0 } };
    }
    
    public static double a(final int n, final int n2) {
        monitorenter(d31.class);
        if (n >= 0 && n2 >= 0) {
            if (n2 <= n) {
                try {
                    final double[][] a = d31.a;
                    if (n >= a.length) {
                        final int n3 = a.length * 2;
                        double[][] a2;
                        if (n > n3) {
                            a2 = new double[n + 1][];
                        }
                        else {
                            a2 = new double[n3 + 1][];
                        }
                        int n4 = 0;
                        double[][] a3;
                        while (true) {
                            a3 = d31.a;
                            if (n4 >= a3.length) {
                                break;
                            }
                            a2[n4] = a3[n4];
                            ++n4;
                        }
                        for (int i = a3.length; i < a2.length; ++i) {
                            (a2[i] = new double[i / 2 + 1])[0] = 1.0;
                            int n5 = 1;
                            while (true) {
                                final double[] array = a2[i];
                                if (n5 >= array.length) {
                                    break;
                                }
                                final double[] array2 = a2[i - 1];
                                final double n6 = array2[n5 - 1];
                                double n7;
                                if (n5 < array2.length) {
                                    n7 = n6 + array2[n5];
                                }
                                else {
                                    n7 = n6 * 2.0;
                                }
                                array[n5] = n7;
                                ++n5;
                            }
                        }
                        d31.a = a2;
                    }
                    int n8 = n2;
                    if (n2 * 2 > n) {
                        n8 = n - n2;
                    }
                    return d31.a[n][n8];
                }
                finally {
                    monitorexit(d31.class);
                }
            }
        }
        monitorexit(d31.class);
        return 0.0;
    }
}

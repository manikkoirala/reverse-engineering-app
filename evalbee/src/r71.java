import android.content.SharedPreferences;
import android.content.Context;
import androidx.work.impl.WorkDatabase;

// 
// Decompiled by Procyon v0.6.0
// 

public class r71
{
    public final WorkDatabase a;
    
    public r71(final WorkDatabase a) {
        this.a = a;
    }
    
    public static void c(final Context context, final ss1 ss1) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences("androidx.work.util.preferences", 0);
        if (!sharedPreferences.contains("reschedule_needed") && !sharedPreferences.contains("last_cancel_all_time_ms")) {
            return;
        }
        long l = 0L;
        final long long1 = sharedPreferences.getLong("last_cancel_all_time_ms", 0L);
        if (sharedPreferences.getBoolean("reschedule_needed", false)) {
            l = 1L;
        }
        ss1.m();
        try {
            ss1.T("INSERT OR REPLACE INTO `Preference` (`key`, `long_value`) VALUES (@key, @long_value)", new Object[] { "last_cancel_all_time_ms", long1 });
            ss1.T("INSERT OR REPLACE INTO `Preference` (`key`, `long_value`) VALUES (@key, @long_value)", new Object[] { "reschedule_needed", l });
            sharedPreferences.edit().clear().apply();
            ss1.S();
        }
        finally {
            ss1.X();
        }
    }
    
    public long a() {
        final Long b = this.a.G().b("last_force_stop_ms");
        if (b != null) {
            return b;
        }
        return 0L;
    }
    
    public boolean b() {
        final Long b = this.a.G().b("reschedule_needed");
        return b != null && b == 1L;
    }
    
    public void d(final long l) {
        this.a.G().a(new n71("last_force_stop_ms", l));
    }
    
    public void e(final boolean b) {
        this.a.G().a(new n71("reschedule_needed", b));
    }
}

import androidx.room.AutoClosingRoomOpenHelper;

// 
// Decompiled by Procyon v0.6.0
// 

public final class z9 implements c
{
    public final c a;
    public final y9 b;
    
    public z9(final c a, final y9 b) {
        fg0.e((Object)a, "delegate");
        fg0.e((Object)b, "autoCloser");
        this.a = a;
        this.b = b;
    }
    
    public AutoClosingRoomOpenHelper b(final ts1.b b) {
        fg0.e((Object)b, "configuration");
        return new AutoClosingRoomOpenHelper(this.a.a(b), this.b);
    }
}

import android.os.BaseBundle;
import android.util.SizeF;
import android.util.Size;
import android.os.IBinder;
import java.io.Serializable;
import android.os.Parcelable;
import android.os.Bundle;
import kotlin.Pair;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class id
{
    public static final Bundle a(final Pair... array) {
        fg0.e((Object)array, "pairs");
        final Bundle bundle = new Bundle(array.length);
        for (final Pair pair : array) {
            final String s = (String)pair.component1();
            final Object component2 = pair.component2();
            Label_0780: {
                if (component2 == null) {
                    ((BaseBundle)bundle).putString(s, (String)null);
                }
                else if (component2 instanceof Boolean) {
                    ((BaseBundle)bundle).putBoolean(s, (boolean)component2);
                }
                else if (component2 instanceof Byte) {
                    bundle.putByte(s, ((Number)component2).byteValue());
                }
                else if (component2 instanceof Character) {
                    bundle.putChar(s, (char)component2);
                }
                else if (component2 instanceof Double) {
                    ((BaseBundle)bundle).putDouble(s, ((Number)component2).doubleValue());
                }
                else if (component2 instanceof Float) {
                    bundle.putFloat(s, ((Number)component2).floatValue());
                }
                else if (component2 instanceof Integer) {
                    ((BaseBundle)bundle).putInt(s, ((Number)component2).intValue());
                }
                else if (component2 instanceof Long) {
                    ((BaseBundle)bundle).putLong(s, ((Number)component2).longValue());
                }
                else if (component2 instanceof Short) {
                    bundle.putShort(s, ((Number)component2).shortValue());
                }
                else if (component2 instanceof Bundle) {
                    bundle.putBundle(s, (Bundle)component2);
                }
                else if (component2 instanceof CharSequence) {
                    bundle.putCharSequence(s, (CharSequence)component2);
                }
                else if (component2 instanceof Parcelable) {
                    bundle.putParcelable(s, (Parcelable)component2);
                }
                else if (component2 instanceof boolean[]) {
                    ((BaseBundle)bundle).putBooleanArray(s, (boolean[])component2);
                }
                else if (component2 instanceof byte[]) {
                    bundle.putByteArray(s, (byte[])component2);
                }
                else if (component2 instanceof char[]) {
                    bundle.putCharArray(s, (char[])component2);
                }
                else if (component2 instanceof double[]) {
                    ((BaseBundle)bundle).putDoubleArray(s, (double[])component2);
                }
                else if (component2 instanceof float[]) {
                    bundle.putFloatArray(s, (float[])component2);
                }
                else if (component2 instanceof int[]) {
                    ((BaseBundle)bundle).putIntArray(s, (int[])component2);
                }
                else if (component2 instanceof long[]) {
                    ((BaseBundle)bundle).putLongArray(s, (long[])component2);
                }
                else if (component2 instanceof short[]) {
                    bundle.putShortArray(s, (short[])component2);
                }
                else {
                    if (component2 instanceof Object[]) {
                        final Class<?> componentType = ((short[])component2).getClass().getComponentType();
                        fg0.b((Object)componentType);
                        if (Parcelable.class.isAssignableFrom(componentType)) {
                            fg0.c(component2, "null cannot be cast to non-null type kotlin.Array<android.os.Parcelable>");
                            bundle.putParcelableArray(s, (Parcelable[])component2);
                            break Label_0780;
                        }
                        if (String.class.isAssignableFrom(componentType)) {
                            fg0.c(component2, "null cannot be cast to non-null type kotlin.Array<kotlin.String>");
                            ((BaseBundle)bundle).putStringArray(s, (String[])component2);
                            break Label_0780;
                        }
                        if (CharSequence.class.isAssignableFrom(componentType)) {
                            fg0.c(component2, "null cannot be cast to non-null type kotlin.Array<kotlin.CharSequence>");
                            bundle.putCharSequenceArray(s, (CharSequence[])component2);
                            break Label_0780;
                        }
                        if (!Serializable.class.isAssignableFrom(componentType)) {
                            final String canonicalName = componentType.getCanonicalName();
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Illegal value array type ");
                            sb.append(canonicalName);
                            sb.append(" for key \"");
                            sb.append(s);
                            sb.append('\"');
                            throw new IllegalArgumentException(sb.toString());
                        }
                    }
                    else if (!(component2 instanceof Serializable)) {
                        if (component2 instanceof IBinder) {
                            ed.a(bundle, s, (IBinder)component2);
                            break Label_0780;
                        }
                        if (component2 instanceof Size) {
                            fd.a(bundle, s, (Size)component2);
                            break Label_0780;
                        }
                        if (component2 instanceof SizeF) {
                            fd.b(bundle, s, (SizeF)component2);
                            break Label_0780;
                        }
                        final String canonicalName2 = ((SizeF)component2).getClass().getCanonicalName();
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Illegal value type ");
                        sb2.append(canonicalName2);
                        sb2.append(" for key \"");
                        sb2.append(s);
                        sb2.append('\"');
                        throw new IllegalArgumentException(sb2.toString());
                    }
                    bundle.putSerializable(s, (Serializable)component2);
                }
            }
        }
        return bundle;
    }
}

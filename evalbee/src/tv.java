import android.os.IInterface;
import android.os.IBinder;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.ClientSettings;
import android.os.Looper;
import android.content.Context;
import com.google.android.gms.common.internal.GmsClient;

// 
// Decompiled by Procyon v0.6.0
// 

public class tv extends GmsClient
{
    public tv(final Context context, final Looper looper, final ClientSettings clientSettings, final GoogleApiClient.ConnectionCallbacks connectionCallbacks, final GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, 131, clientSettings, connectionCallbacks, onConnectionFailedListener);
    }
    
    public ud0 a(final IBinder binder) {
        return ud0.a.p(binder);
    }
    
    @Override
    public int getMinApkVersion() {
        return 12451000;
    }
    
    @Override
    public String getServiceDescriptor() {
        return "com.google.firebase.dynamiclinks.internal.IDynamicLinksService";
    }
    
    @Override
    public String getStartServiceAction() {
        return "com.google.firebase.dynamiclinks.service.START";
    }
    
    @Override
    public boolean usesClientTelemetry() {
        return true;
    }
}

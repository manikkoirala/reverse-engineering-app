import com.google.android.gms.tasks.OnSuccessListener;
import io.grpc.ForwardingClientCall;
import com.google.android.gms.tasks.OnCompleteListener;
import io.grpc.MethodDescriptor;
import io.grpc.Status;
import io.grpc.ClientCall$Listener;
import com.google.android.gms.tasks.Task;
import io.grpc.ClientCall;
import io.grpc.CallCredentials;
import android.content.Context;
import io.grpc.Metadata$AsciiMarshaller;
import io.grpc.Metadata;
import com.google.firebase.firestore.util.AsyncQueue;
import io.grpc.Metadata$Key;

// 
// Decompiled by Procyon v0.6.0
// 

public class w30
{
    public static final Metadata$Key g;
    public static final Metadata$Key h;
    public static final Metadata$Key i;
    public static volatile String j;
    public final AsyncQueue a;
    public final eo b;
    public final eo c;
    public final ec0 d;
    public final String e;
    public final fc0 f;
    
    static {
        final Metadata$AsciiMarshaller ascii_STRING_MARSHALLER = Metadata.ASCII_STRING_MARSHALLER;
        g = Metadata$Key.of("x-goog-api-client", ascii_STRING_MARSHALLER);
        h = Metadata$Key.of("google-cloud-resource-prefix", ascii_STRING_MARSHALLER);
        i = Metadata$Key.of("x-goog-request-params", ascii_STRING_MARSHALLER);
        w30.j = "gl-java/";
    }
    
    public w30(final AsyncQueue a, final Context context, final eo b, final eo c, final rp rp, final fc0 f) {
        this.a = a;
        this.f = f;
        this.b = b;
        this.c = c;
        this.d = new ec0(a, context, rp, new u30(b, c));
        final qp a2 = rp.a();
        this.e = String.format("projects/%s/databases/%s", a2.f(), a2.e());
    }
    
    public static /* synthetic */ AsyncQueue b(final w30 w30) {
        return w30.a;
    }
    
    public static void h(final String j) {
        w30.j = j;
    }
    
    public final String c() {
        return String.format("%s fire/%s grpc/", w30.j, "24.10.0");
    }
    
    public void d() {
        this.b.b();
        this.c.b();
    }
    
    public final Metadata f() {
        final Metadata metadata = new Metadata();
        metadata.put(w30.g, (Object)this.c());
        metadata.put(w30.h, (Object)this.e);
        metadata.put(w30.i, (Object)this.e);
        final fc0 f = this.f;
        if (f != null) {
            f.a(metadata);
        }
        return metadata;
    }
    
    public ClientCall g(final MethodDescriptor methodDescriptor, final pe0 pe0) {
        final ClientCall[] array = { null };
        final Task i = this.d.i(methodDescriptor);
        i.addOnCompleteListener(this.a.j(), (OnCompleteListener)new v30(this, array, pe0));
        return (ClientCall)new ForwardingClientCall(this, array, i) {
            public final ClientCall[] a;
            public final Task b;
            public final w30 c;
            
            public ClientCall delegate() {
                g9.d(this.a[0] != null, "ClientCall used before onOpen() callback", new Object[0]);
                return this.a[0];
            }
            
            public void halfClose() {
                if (this.a[0] == null) {
                    this.b.addOnSuccessListener(w30.b(this.c).j(), (OnSuccessListener)new x30());
                }
                else {
                    super.halfClose();
                }
            }
        };
    }
}

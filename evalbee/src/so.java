import android.os.RemoteException;
import android.os.Parcelable;
import android.app.PendingIntent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.os.Handler;
import android.content.ServiceConnection;
import android.text.TextUtils;
import android.content.Intent;
import android.content.Context;
import android.content.ComponentName;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class so
{
    public final td0 a;
    public final ComponentName b;
    public final Context c;
    
    public so(final td0 a, final ComponentName b, final Context c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public static boolean a(final Context context, final String package1, final uo uo) {
        uo.setApplicationContext(context.getApplicationContext());
        final Intent intent = new Intent("android.support.customtabs.action.CustomTabsService");
        if (!TextUtils.isEmpty((CharSequence)package1)) {
            intent.setPackage(package1);
        }
        return context.bindService(intent, (ServiceConnection)uo, 33);
    }
    
    public final sd0.a b(final ro ro) {
        return new sd0.a(this, ro) {
            public Handler a = new Handler(Looper.getMainLooper());
            public final so b;
            
            public Bundle b(final String s, final Bundle bundle) {
                return null;
            }
            
            public void g(final int n, final int n2, final Bundle bundle) {
            }
            
            public void h(final int n, final Bundle bundle) {
            }
            
            public void j(final String s, final Bundle bundle) {
            }
            
            public void m(final String s, final Bundle bundle) {
            }
            
            public void n(final Bundle bundle) {
            }
            
            public void o(final int n, final Uri uri, final boolean b, final Bundle bundle) {
            }
        };
    }
    
    public vo c(final ro ro) {
        return this.d(ro, null);
    }
    
    public final vo d(ro ro, final PendingIntent pendingIntent) {
        final sd0.a b = this.b(ro);
        ro = null;
        Label_0048: {
            if (pendingIntent == null) {
                break Label_0048;
            }
            try {
                final Bundle bundle = new Bundle();
                bundle.putParcelable("android.support.customtabs.extra.SESSION_ID", (Parcelable)pendingIntent);
                boolean b2 = this.a.d(b, bundle);
                while (true) {
                    if (!b2) {
                        return null;
                    }
                    ro = (ro)new vo(this.a, b, this.b, pendingIntent);
                    return (vo)ro;
                    b2 = this.a.c(b);
                    continue;
                }
            }
            catch (final RemoteException ex) {
                return (vo)ro;
            }
        }
    }
    
    public boolean e(final long n) {
        try {
            return this.a.f(n);
        }
        catch (final RemoteException ex) {
            return false;
        }
    }
}

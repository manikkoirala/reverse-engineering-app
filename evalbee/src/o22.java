import java.util.HashMap;
import java.util.List;
import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import com.google.firebase.firestore.FirebaseFirestoreException;
import java.util.SortedSet;
import android.os.Handler;
import android.os.Looper;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.StatusException;
import com.google.protobuf.ByteString;
import java.util.Iterator;
import com.google.android.gms.tasks.Task;
import java.util.Map;
import java.util.Comparator;
import java.security.SecureRandom;
import com.google.android.gms.tasks.Continuation;
import java.util.Random;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class o22
{
    public static final Random a;
    public static final Continuation b;
    
    static {
        a = new SecureRandom();
        b = (Continuation)new h22();
    }
    
    public static Continuation A() {
        return o22.b;
    }
    
    public static Object e(final Iterator iterator) {
        Object next;
        if (iterator.hasNext()) {
            next = iterator.next();
        }
        else {
            next = null;
        }
        return next;
    }
    
    public static Comparator f() {
        return new i22();
    }
    
    public static int g(final boolean b, final boolean b2) {
        if (b == b2) {
            return 0;
        }
        if (b) {
            return 1;
        }
        return -1;
    }
    
    public static int h(final byte[] array, final byte[] array2) {
        for (int min = Math.min(array.length, array2.length), i = 0; i < min; ++i) {
            final int n = array[i] & 0xFF;
            final int n2 = array2[i] & 0xFF;
            if (n < n2) {
                return -1;
            }
            if (n > n2) {
                return 1;
            }
        }
        return k(array.length, array2.length);
    }
    
    public static int i(final ByteString byteString, final ByteString byteString2) {
        for (int min = Math.min(byteString.size(), byteString2.size()), i = 0; i < min; ++i) {
            final int n = byteString.byteAt(i) & 0xFF;
            final int n2 = byteString2.byteAt(i) & 0xFF;
            if (n < n2) {
                return -1;
            }
            if (n > n2) {
                return 1;
            }
        }
        return k(byteString.size(), byteString2.size());
    }
    
    public static int j(final double n, final double n2) {
        return n01.c(n, n2);
    }
    
    public static int k(final int n, final int n2) {
        if (n < n2) {
            return -1;
        }
        if (n > n2) {
            return 1;
        }
        return 0;
    }
    
    public static int l(final long n, final long n2) {
        return n01.a(n, n2);
    }
    
    public static int m(final double n, final long n2) {
        return n01.b(n, n2);
    }
    
    public static Exception n(final Exception ex) {
        Status status;
        if (ex instanceof StatusException) {
            status = ((StatusException)ex).getStatus();
        }
        else {
            if (!(ex instanceof StatusRuntimeException)) {
                return ex;
            }
            status = ((StatusRuntimeException)ex).getStatus();
        }
        return r(status);
    }
    
    public static void o(final RuntimeException ex) {
        new Handler(Looper.getMainLooper()).post((Runnable)new j22(ex));
    }
    
    public static void p(final Iterator iterator, final Iterator iterator2, final Comparator comparator, final cl cl, final cl cl2) {
    Label_0000:
        while (true) {
            while (true) {
                Object o = e(iterator);
            Label_0006:
                while (true) {
                    for (Object e = e(iterator2); o != null || e != null; o = e(iterator)) {
                        boolean b = false;
                        boolean b2 = true;
                        Label_0090: {
                            if (o != null && e != null) {
                                final int compare = comparator.compare(o, e);
                                if (compare < 0) {
                                    break Label_0090;
                                }
                                if (compare <= 0) {
                                    b2 = false;
                                    break Label_0090;
                                }
                            }
                            else if (o != null) {
                                break Label_0090;
                            }
                            b2 = false;
                            b = true;
                        }
                        if (b) {
                            cl.accept(e);
                            continue Label_0006;
                        }
                        if (!b2) {
                            continue Label_0000;
                        }
                        cl2.accept(o);
                    }
                    break Label_0000;
                }
            }
            break;
        }
    }
    
    public static void q(final SortedSet set, final SortedSet set2, final cl cl, final cl cl2) {
        final Iterator iterator = set.iterator();
        final Iterator iterator2 = set2.iterator();
        Comparator comparator;
        if (set.comparator() != null) {
            comparator = set.comparator();
        }
        else {
            comparator = new k22();
        }
        p(iterator, iterator2, comparator, cl, cl2);
    }
    
    public static FirebaseFirestoreException r(final Status status) {
        final StatusException exception = status.asException();
        return new FirebaseFirestoreException(((Throwable)exception).getMessage(), FirebaseFirestoreException.Code.fromValue(status.getCode().value()), (Throwable)exception);
    }
    
    public static Map s(final Map map, final int n, final Comparator comparator) {
        if (map.size() <= n) {
            return map;
        }
        final ArrayList list = new ArrayList(map.entrySet());
        Collections.sort((List<Object>)list, new l22(comparator));
        final HashMap hashMap = new HashMap();
        for (int i = 0; i < n; ++i) {
            hashMap.put(((Map.Entry)list.get(i)).getKey(), ((Map.Entry)list.get(i)).getValue());
        }
        return hashMap;
    }
    
    public static StringBuilder x(final CharSequence charSequence, final int n, final CharSequence s) {
        final StringBuilder sb = new StringBuilder();
        if (n != 0) {
            sb.append(charSequence);
            for (int i = 1; i < n; ++i) {
                sb.append(s);
                sb.append(charSequence);
            }
        }
        return sb;
    }
    
    public static String y(final ByteString byteString) {
        final int size = byteString.size();
        final StringBuilder sb = new StringBuilder(size * 2);
        for (int i = 0; i < size; ++i) {
            final int n = byteString.byteAt(i) & 0xFF;
            sb.append(Character.forDigit(n >>> 4, 16));
            sb.append(Character.forDigit(n & 0xF, 16));
        }
        return sb.toString();
    }
    
    public static String z(final Object o) {
        String name;
        if (o == null) {
            name = "null";
        }
        else {
            name = o.getClass().getName();
        }
        return name;
    }
}

import java.util.Iterator;
import com.google.android.gms.common.internal.Preconditions;
import java.util.concurrent.Executor;
import android.app.Activity;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.HashMap;
import java.util.Queue;

// 
// Decompiled by Procyon v0.6.0
// 

public class nu1
{
    public final Queue a;
    public final HashMap b;
    public zq1 c;
    public int d;
    public a e;
    
    public nu1(final zq1 c, final int d, final a e) {
        this.a = new ConcurrentLinkedQueue();
        this.b = new HashMap();
        this.c = c;
        this.d = d;
        this.e = e;
    }
    
    public void d(final Activity activity, final Executor executor, final Object key) {
        Preconditions.checkNotNull(key);
        synchronized (this.c.H()) {
            final int a = this.c.A();
            final int d = this.d;
            boolean b = true;
            final boolean b2 = (a & d) != 0x0;
            this.a.add(key);
            final lo1 value = new lo1(executor);
            this.b.put(key, value);
            if (activity != null) {
                if (activity.isDestroyed()) {
                    b = false;
                }
                Preconditions.checkArgument(b, (Object)"Activity is already destroyed!");
                i2.a().c(activity, key, new lu1(this, key));
            }
            monitorexit(this.c.H());
            if (b2) {
                value.a(new mu1(this, key, this.c.d0()));
            }
        }
    }
    
    public void h() {
        if ((this.c.A() & this.d) != 0x0) {
            final zq1.a d0 = this.c.d0();
            for (final Object next : this.a) {
                final lo1 lo1 = this.b.get(next);
                if (lo1 != null) {
                    lo1.a(new ku1(this, next, d0));
                }
            }
        }
    }
    
    public void i(final Object key) {
        Preconditions.checkNotNull(key);
        synchronized (this.c.H()) {
            this.b.remove(key);
            this.a.remove(key);
            i2.a().b(key);
        }
    }
    
    public interface a
    {
        void a(final Object p0, final Object p1);
    }
}

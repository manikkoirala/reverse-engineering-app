import java.util.Date;
import android.graphics.Color;
import java.text.ParseException;
import android.text.format.DateFormat;
import java.text.SimpleDateFormat;
import android.widget.ImageView;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import android.content.Context;
import java.util.ArrayList;
import android.util.SparseBooleanArray;
import android.widget.BaseAdapter;

// 
// Decompiled by Procyon v0.6.0
// 

public class f3 extends BaseAdapter
{
    public SparseBooleanArray a;
    public ArrayList b;
    public Context c;
    
    public f3(final ArrayList b, final Context c) {
        this.a = new SparseBooleanArray();
        this.b = b;
        this.c = c;
    }
    
    public SparseBooleanArray a() {
        return this.a;
    }
    
    public void b() {
        this.a = new SparseBooleanArray();
        this.notifyDataSetChanged();
    }
    
    public final void c(final int n, final boolean b) {
        if (b) {
            this.a.put(n, b);
        }
        else {
            this.a.delete(n);
        }
        this.notifyDataSetChanged();
    }
    
    public void d(final int n) {
        this.c(n, this.a.get(n) ^ true);
    }
    
    public int getCount() {
        return this.b.size();
    }
    
    public Object getItem(final int index) {
        return this.b.get(index);
    }
    
    public long getItemId(final int n) {
        return n;
    }
    
    public View getView(int n, View view, ViewGroup viewGroup) {
        final c8 c8 = this.b.get(n);
        final View inflate = ((LayoutInflater)this.c.getSystemService("layout_inflater")).inflate(2131493019, (ViewGroup)null);
        final TextView textView = (TextView)inflate.findViewById(2131297326);
        final TextView textView2 = (TextView)inflate.findViewById(2131297306);
        final TextView textView3 = (TextView)inflate.findViewById(2131297327);
        final TextView textView4 = (TextView)inflate.findViewById(2131297212);
        viewGroup = (ViewGroup)inflate.findViewById(2131297261);
        final TextView textView5 = (TextView)inflate.findViewById(2131297377);
        final TextView textView6 = (TextView)inflate.findViewById(2131297378);
        final ProgressBar progressBar = (ProgressBar)inflate.findViewById(2131296971);
        final FrameLayout frameLayout = (FrameLayout)inflate.findViewById(2131296781);
        view = inflate.findViewById(2131296684);
        final ImageView imageView = (ImageView)inflate.findViewById(2131296683);
        textView.setText((CharSequence)c8.d());
        final StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(c8.e());
        textView2.setText((CharSequence)sb.toString());
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(c8.a());
        sb2.append("");
        textView3.setText((CharSequence)sb2.toString());
        textView4.setText((CharSequence)c8.b());
        final String c9 = c8.c();
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            final Date parse = simpleDateFormat.parse(c9);
            textView5.setText((CharSequence)DateFormat.format((CharSequence)"dd", parse));
            textView6.setText((CharSequence)DateFormat.format((CharSequence)"MMM", parse));
        }
        catch (final ParseException ex) {
            ex.printStackTrace();
        }
        Label_0516: {
            StringBuilder sb3;
            if (c8.i()) {
                ((View)frameLayout).setVisibility(0);
                ((ImageView)view).setVisibility(8);
                imageView.setVisibility(8);
                if (c8.f() <= 0) {
                    break Label_0516;
                }
                progressBar.setIndeterminate(false);
                progressBar.setProgress(c8.f());
                sb3 = new StringBuilder();
            }
            else {
                if (c8.j()) {
                    ((View)frameLayout).setVisibility(8);
                    ((ImageView)view).setVisibility(0);
                    imageView.setVisibility(8);
                    break Label_0516;
                }
                if (!c8.h()) {
                    ((View)frameLayout).setVisibility(8);
                    ((ImageView)view).setVisibility(8);
                    imageView.setVisibility(0);
                    break Label_0516;
                }
                ((ImageView)view).setVisibility(8);
                imageView.setVisibility(8);
                ((View)frameLayout).setVisibility(0);
                progressBar.setIndeterminate(false);
                progressBar.setProgress(c8.f());
                sb3 = new StringBuilder();
            }
            sb3.append(c8.f());
            sb3.append("");
            ((TextView)viewGroup).setText((CharSequence)sb3.toString());
        }
        if (this.a.get(n, false)) {
            n = this.c.getResources().getColor(2131099717);
        }
        else {
            n = Color.rgb(255, 255, 255);
        }
        inflate.setBackgroundColor(n);
        return inflate;
    }
}

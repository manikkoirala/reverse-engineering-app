import android.os.IBinder;
import android.os.Bundle;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class hd
{
    public static void a(final Bundle bundle, final String s, final IBinder binder) {
        a.b(bundle, s, binder);
    }
    
    public abstract static class a
    {
        public static IBinder a(final Bundle bundle, final String s) {
            return bundle.getBinder(s);
        }
        
        public static void b(final Bundle bundle, final String s, final IBinder binder) {
            bundle.putBinder(s, binder);
        }
    }
}

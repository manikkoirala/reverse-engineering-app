import java.util.Iterator;
import com.google.firebase.auth.FirebaseAuth;

// 
// Decompiled by Procyon v0.6.0
// 

public final class uj2 implements Runnable
{
    public final FirebaseAuth a;
    public final cg0 b;
    
    public uj2(final FirebaseAuth a, final cg0 b) {
        this.a = a;
        this.b = b;
    }
    
    @Override
    public final void run() {
        final Iterator iterator = FirebaseAuth.M(this.a).iterator();
        while (iterator.hasNext()) {
            ((ee0)iterator.next()).a(this.b);
        }
        final Iterator iterator2 = FirebaseAuth.K(this.a).iterator();
        if (!iterator2.hasNext()) {
            return;
        }
        zu0.a(iterator2.next());
        throw null;
    }
}

import java.util.Iterator;
import java.util.List;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class ws
{
    public static final String a;
    
    static {
        final String i = xl0.i("DiagnosticsWrkr");
        fg0.d((Object)i, "tagWithPrefix(\"DiagnosticsWrkr\")");
        a = i;
    }
    
    public static final String c(final p92 p4, final String str, final Integer obj, final String str2) {
        final StringBuilder sb = new StringBuilder();
        sb.append('\n');
        sb.append(p4.a);
        sb.append("\t ");
        sb.append(p4.c);
        sb.append("\t ");
        sb.append(obj);
        sb.append("\t ");
        sb.append(p4.b.name());
        sb.append("\t ");
        sb.append(str);
        sb.append("\t ");
        sb.append(str2);
        sb.append('\t');
        return sb.toString();
    }
    
    public static final String d(final g92 g92, final w92 w92, final gt1 gt1, final List list) {
        final StringBuilder sb = new StringBuilder();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("\n Id \t Class Name\t ");
        sb2.append("Job Id");
        sb2.append("\t State\t Unique Name\t Tags\t");
        sb.append(sb2.toString());
        for (final p92 p4 : list) {
            final ft1 e = gt1.e(u92.a(p4));
            Integer value;
            if (e != null) {
                value = e.c;
            }
            else {
                value = null;
            }
            sb.append(c(p4, vh.D((Iterable)g92.a(p4.a), (CharSequence)",", (CharSequence)null, (CharSequence)null, 0, (CharSequence)null, (c90)null, 62, (Object)null), value, vh.D((Iterable)w92.c(p4.a), (CharSequence)",", (CharSequence)null, (CharSequence)null, 0, (CharSequence)null, (c90)null, 62, (Object)null)));
        }
        final String string = sb.toString();
        fg0.d((Object)string, "StringBuilder().apply(builderAction).toString()");
        return string;
    }
}

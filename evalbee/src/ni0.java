import android.graphics.Typeface;
import android.graphics.Color;
import android.graphics.Paint$Align;
import android.graphics.Paint$Style;
import android.graphics.Bitmap;
import android.graphics.Bitmap$Config;
import android.graphics.Paint;
import android.graphics.Canvas;
import java.util.ArrayList;

// 
// Decompiled by Procyon v0.6.0
// 

public class ni0
{
    public ArrayList a;
    public int b;
    public int c;
    public int d;
    public int[] e;
    public int f;
    public int g;
    public int h;
    public Canvas i;
    public Paint j;
    public Paint k;
    public Paint l;
    public Paint m;
    public String n;
    public String o;
    
    public ni0() {
        this.c = 1;
        this.d = 0;
        this.e = new int[4];
        this.f = 1200;
        this.g = 1800;
        this.h = 100;
        this.a = new ArrayList();
        final int f = this.f;
        final int c = this.c;
        final Bitmap bitmap = Bitmap.createBitmap(f * c, this.g * c, Bitmap$Config.ARGB_8888);
        (this.i = new Canvas(bitmap)).drawARGB(255, 255, 255, 255);
        this.a.add(bitmap);
        (this.j = new Paint()).setStyle(Paint$Style.FILL_AND_STROKE);
        this.j.setAntiAlias(true);
        this.j.setColor(-16777216);
        this.j.setStrokeWidth(1.0f);
        this.j.setTextAlign(Paint$Align.CENTER);
        final int[] e = this.e;
        final int c2 = this.c;
        e[1] = (e[0] = c2 * 100);
        e[3] = (e[2] = c2 * 100);
        (this.k = new Paint()).setStyle(Paint$Style.FILL);
        this.k.setAntiAlias(true);
        this.k.setColor(Color.rgb(200, 255, 200));
        (this.l = new Paint()).setStyle(Paint$Style.FILL);
        this.l.setAntiAlias(true);
        this.l.setColor(Color.rgb(255, 200, 200));
        (this.m = new Paint()).setStyle(Paint$Style.FILL);
        this.m.setAntiAlias(true);
        this.m.setColor(Color.rgb(255, 255, 200));
    }
    
    public void a(final String s) {
        final int n = this.c * 30;
        this.j.setTextSize((float)n);
        final int n2 = this.f / 2;
        final int c = this.c;
        this.j.setTypeface(Typeface.DEFAULT_BOLD);
        this.j.setTextAlign(Paint$Align.CENTER);
        this.i.drawText(s, (float)(n2 * c), (float)(this.e[0] + n / 2 - 2), this.j);
        final int[] e = this.e;
        final int n3 = e[0];
        final int c2 = this.c;
        e[0] = n3 + c2 * 60;
        e[1] += c2 * 60;
        e[2] += c2 * 60;
        e[3] += c2 * 60;
    }
    
    public final void b() {
        final int f = this.f;
        final int c = this.c;
        this.a.add(Bitmap.createBitmap(f * c, this.g * c, Bitmap$Config.ARGB_8888));
        ++this.b;
        (this.i = new Canvas((Bitmap)this.a.get(this.b))).drawARGB(255, 255, 255, 255);
        final int[] e = this.e;
        final int c2 = this.c;
        e[1] = (e[0] = c2 * 100);
        e[3] = (e[2] = c2 * 100);
        this.d = 0;
        this.h = 100;
    }
    
    public void c(final String s, String e, final boolean b) {
        final ArrayList list = new ArrayList();
        final int length = e.length();
        final int n = 0;
        if (length < 13) {
            list.add(e);
        }
        else {
            final String[] split = e.split(",");
            e = "";
            for (int i = 0; i < split.length; ++i) {
                String str = e;
                if (e.length() > 12) {
                    list.add(e);
                    str = "";
                }
                final StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(split[i]);
                final String str2 = e = sb.toString();
                if (i != split.length - 1) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append(str2);
                    sb2.append(",");
                    e = sb2.toString();
                }
            }
            list.add(e);
        }
        if (this.g * this.c - this.e[this.d] < list.size() * 50) {
            final int d = this.d;
            if (d < 3) {
                this.d = d + 1;
                this.h += 270;
            }
            else {
                this.b();
            }
            this.c(this.n, this.o, b);
        }
        final Paint paint = new Paint();
        paint.setStyle(Paint$Style.STROKE);
        paint.setAntiAlias(true);
        paint.setColor(-16777216);
        final int n2 = this.c * 20;
        this.j.setTextSize((float)n2);
        final int n3 = this.c * 23;
        this.j.setTextAlign(Paint$Align.CENTER);
        Paint paint2;
        Typeface typeface;
        if (b) {
            paint2 = this.j;
            typeface = Typeface.DEFAULT_BOLD;
        }
        else {
            paint2 = this.j;
            typeface = Typeface.DEFAULT;
        }
        paint2.setTypeface(typeface);
        final Canvas j = this.i;
        final int h = this.h;
        final int n4 = n3 * 2;
        final float n5 = (float)(h + n4);
        final int n6 = this.e[this.d];
        final int n7 = n2 / 2;
        j.drawText(s, n5, (float)(n6 + n7 - 2), this.j);
        for (int k = n; k < list.size(); ++k) {
            this.i.drawText((String)list.get(k), (float)(this.h + n3 * 7), (float)(this.e[this.d] + this.c * 30 * k + n7 - 2), this.j);
        }
        this.f(this.i, this.h + n4, this.e[this.d] + this.c * 15 * (list.size() - 1), n3 * 4, this.c * 30 * list.size(), paint);
        this.f(this.i, this.h + n3 * 7, this.e[this.d] + this.c * 15 * (list.size() - 1), n3 * 6, this.c * 30 * list.size(), paint);
        final int[] e2 = this.e;
        final int d2 = this.d;
        e2[d2] += this.c * 30 * list.size();
    }
    
    public void d(final String n, final String o, final boolean b) {
        this.c(this.n = n, this.o = o, b);
    }
    
    public void e(final String s, final boolean b) {
        final Paint paint = new Paint();
        paint.setStyle(Paint$Style.STROKE);
        paint.setAntiAlias(true);
        paint.setColor(-16777216);
        final int n = this.c * 20;
        this.j.setTextSize((float)n);
        final int n2 = this.c * 115;
        this.j.setTextAlign(Paint$Align.CENTER);
        this.j.setTypeface(Typeface.DEFAULT);
        this.i.drawText(s, (float)(this.h + n2), (float)(this.e[this.d] + n / 2 - 2), this.j);
        this.f(this.i, this.h + n2, this.e[this.d], n2 * 2, this.c * 30, paint);
        final int[] e = this.e;
        final int d = this.d;
        e[d] += this.c * 30;
    }
    
    public final void f(final Canvas canvas, final int n, final int n2, int n3, int n4, final Paint paint) {
        n3 /= 2;
        final float n5 = (float)(n - n3);
        n4 /= 2;
        canvas.drawRect(n5, (float)(n2 - n4), (float)(n + n3), (float)(n2 + n4), paint);
    }
    
    public ArrayList g() {
        return this.a;
    }
}

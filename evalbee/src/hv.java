import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.Canvas;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.graphics.Paint$Cap;
import android.graphics.Paint$Join;
import android.graphics.Paint$Style;
import android.content.Context;
import android.graphics.Path;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;

// 
// Decompiled by Procyon v0.6.0
// 

public class hv extends Drawable
{
    public static final float m;
    public final Paint a;
    public float b;
    public float c;
    public float d;
    public float e;
    public boolean f;
    public final Path g;
    public final int h;
    public boolean i;
    public float j;
    public float k;
    public int l;
    
    static {
        m = (float)Math.toRadians(45.0);
    }
    
    public hv(final Context context) {
        final Paint a = new Paint();
        this.a = a;
        this.g = new Path();
        this.i = false;
        this.l = 2;
        a.setStyle(Paint$Style.STROKE);
        a.setStrokeJoin(Paint$Join.MITER);
        a.setStrokeCap(Paint$Cap.BUTT);
        a.setAntiAlias(true);
        final TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes((AttributeSet)null, bc1.Z0, sa1.y, tb1.b);
        this.c(obtainStyledAttributes.getColor(bc1.d1, 0));
        this.b(obtainStyledAttributes.getDimension(bc1.h1, 0.0f));
        this.f(obtainStyledAttributes.getBoolean(bc1.g1, true));
        this.d((float)Math.round(obtainStyledAttributes.getDimension(bc1.f1, 0.0f)));
        this.h = obtainStyledAttributes.getDimensionPixelSize(bc1.e1, 0);
        this.c = (float)Math.round(obtainStyledAttributes.getDimension(bc1.c1, 0.0f));
        this.b = (float)Math.round(obtainStyledAttributes.getDimension(bc1.a1, 0.0f));
        this.d = obtainStyledAttributes.getDimension(bc1.b1, 0.0f);
        obtainStyledAttributes.recycle();
    }
    
    public static float a(final float n, final float n2, final float n3) {
        return n + (n2 - n) * n3;
    }
    
    public void b(final float strokeWidth) {
        if (this.a.getStrokeWidth() != strokeWidth) {
            this.a.setStrokeWidth(strokeWidth);
            this.k = (float)(strokeWidth / 2.0f * Math.cos(hv.m));
            this.invalidateSelf();
        }
    }
    
    public void c(final int color) {
        if (color != this.a.getColor()) {
            this.a.setColor(color);
            this.invalidateSelf();
        }
    }
    
    public void d(final float e) {
        if (e != this.e) {
            this.e = e;
            this.invalidateSelf();
        }
    }
    
    public void draw(final Canvas canvas) {
        final Rect bounds = this.getBounds();
        final int l = this.l;
        boolean b2;
        final boolean b = b2 = false;
        Label_0065: {
            if (l != 0) {
                if (l != 1) {
                    if (l != 3) {
                        b2 = b;
                        if (wu.f(this) != 1) {
                            break Label_0065;
                        }
                    }
                    else {
                        b2 = b;
                        if (wu.f(this) != 0) {
                            break Label_0065;
                        }
                    }
                }
                b2 = true;
            }
        }
        final float b3 = this.b;
        final float a = a(this.c, (float)Math.sqrt(b3 * b3 * 2.0f), this.j);
        final float a2 = a(this.c, this.d, this.j);
        final float n = (float)Math.round(a(0.0f, this.k, this.j));
        final float a3 = a(0.0f, hv.m, this.j);
        float n2;
        if (b2) {
            n2 = 0.0f;
        }
        else {
            n2 = -180.0f;
        }
        float n3;
        if (b2) {
            n3 = 180.0f;
        }
        else {
            n3 = 0.0f;
        }
        final float a4 = a(n2, n3, this.j);
        final double n4 = a;
        final double n5 = a3;
        final float n6 = (float)Math.round(Math.cos(n5) * n4);
        final float n7 = (float)Math.round(n4 * Math.sin(n5));
        this.g.rewind();
        final float a5 = a(this.e + this.a.getStrokeWidth(), -this.k, this.j);
        final float n8 = -a2 / 2.0f;
        this.g.moveTo(n8 + n, 0.0f);
        this.g.rLineTo(a2 - n * 2.0f, 0.0f);
        this.g.moveTo(n8, a5);
        this.g.rLineTo(n6, n7);
        this.g.moveTo(n8, -a5);
        this.g.rLineTo(n6, -n7);
        this.g.close();
        canvas.save();
        final float strokeWidth = this.a.getStrokeWidth();
        final float n9 = (float)bounds.height();
        final float e = this.e;
        canvas.translate((float)bounds.centerX(), (int)(n9 - 3.0f * strokeWidth - 2.0f * e) / 4 * 2 + (strokeWidth * 1.5f + e));
        if (this.f) {
            int n10;
            if (this.i ^ b2) {
                n10 = -1;
            }
            else {
                n10 = 1;
            }
            canvas.rotate(a4 * n10);
        }
        else if (b2) {
            canvas.rotate(180.0f);
        }
        canvas.drawPath(this.g, this.a);
        canvas.restore();
    }
    
    public void e(final float j) {
        if (this.j != j) {
            this.j = j;
            this.invalidateSelf();
        }
    }
    
    public void f(final boolean f) {
        if (this.f != f) {
            this.f = f;
            this.invalidateSelf();
        }
    }
    
    public int getIntrinsicHeight() {
        return this.h;
    }
    
    public int getIntrinsicWidth() {
        return this.h;
    }
    
    public int getOpacity() {
        return -3;
    }
    
    public void setAlpha(final int alpha) {
        if (alpha != this.a.getAlpha()) {
            this.a.setAlpha(alpha);
            this.invalidateSelf();
        }
    }
    
    public void setColorFilter(final ColorFilter colorFilter) {
        this.a.setColorFilter(colorFilter);
        this.invalidateSelf();
    }
}

import java.util.concurrent.Future;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class t02
{
    public static Object a(final Future future) {
        boolean b = false;
        try {
            return future.get();
        }
        catch (final InterruptedException ex) {
            b = true;
            return future.get();
        }
        finally {
            if (b) {
                Thread.currentThread().interrupt();
            }
        }
    }
}

import android.view.MotionEvent;
import android.content.res.TypedArray;
import android.content.res.Configuration;
import android.view.View$MeasureSpec;
import android.view.View;
import android.view.ContextThemeWrapper;
import android.util.TypedValue;
import android.util.AttributeSet;
import androidx.appcompat.widget.a;
import androidx.appcompat.widget.ActionMenuView;
import android.content.Context;
import android.view.ViewGroup;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class c extends ViewGroup
{
    public final a a;
    public final Context b;
    public ActionMenuView c;
    public androidx.appcompat.widget.a d;
    public int e;
    public i42 f;
    public boolean g;
    public boolean h;
    
    public c(final Context b, final AttributeSet set, final int n) {
        super(b, set, n);
        this.a = new a();
        final TypedValue typedValue = new TypedValue();
        if (b.getTheme().resolveAttribute(sa1.a, typedValue, true) && typedValue.resourceId != 0) {
            this.b = (Context)new ContextThemeWrapper(b, typedValue.resourceId);
        }
        else {
            this.b = b;
        }
    }
    
    public static /* synthetic */ void a(final c c, final int visibility) {
        ((View)c).setVisibility(visibility);
    }
    
    public static /* synthetic */ void b(final c c, final int visibility) {
        ((View)c).setVisibility(visibility);
    }
    
    public static int d(int n, final int n2, final boolean b) {
        if (b) {
            n -= n2;
        }
        else {
            n += n2;
        }
        return n;
    }
    
    public int c(final View view, final int n, final int n2, final int n3) {
        view.measure(View$MeasureSpec.makeMeasureSpec(n, Integer.MIN_VALUE), n2);
        return Math.max(0, n - view.getMeasuredWidth() - n3);
    }
    
    public int e(final View view, int n, int n2, final int n3, final boolean b) {
        final int measuredWidth = view.getMeasuredWidth();
        final int measuredHeight = view.getMeasuredHeight();
        n2 += (n3 - measuredHeight) / 2;
        if (b) {
            view.layout(n - measuredWidth, n2, n, measuredHeight + n2);
        }
        else {
            view.layout(n, n2, n + measuredWidth, measuredHeight + n2);
        }
        n = measuredWidth;
        if (b) {
            n = -measuredWidth;
        }
        return n;
    }
    
    public i42 f(final int n, final long n2) {
        final i42 f = this.f;
        if (f != null) {
            f.c();
        }
        i42 i42;
        if (n == 0) {
            if (((View)this).getVisibility() != 0) {
                ((View)this).setAlpha(0.0f);
            }
            i42 = o32.e((View)this).b(1.0f);
        }
        else {
            i42 = o32.e((View)this).b(0.0f);
        }
        i42.f(n2);
        i42.h(this.a.d(i42, n));
        return i42;
    }
    
    public int getAnimatedVisibility() {
        if (this.f != null) {
            return this.a.b;
        }
        return ((View)this).getVisibility();
    }
    
    public int getContentHeight() {
        return this.e;
    }
    
    public void onConfigurationChanged(final Configuration configuration) {
        super.onConfigurationChanged(configuration);
        final TypedArray obtainStyledAttributes = ((View)this).getContext().obtainStyledAttributes((AttributeSet)null, bc1.a, sa1.c, 0);
        this.setContentHeight(obtainStyledAttributes.getLayoutDimension(bc1.j, 0));
        obtainStyledAttributes.recycle();
        final androidx.appcompat.widget.a d = this.d;
        if (d != null) {
            d.x(configuration);
        }
    }
    
    public boolean onHoverEvent(final MotionEvent motionEvent) {
        final int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 9) {
            this.h = false;
        }
        if (!this.h) {
            final boolean onHoverEvent = super.onHoverEvent(motionEvent);
            if (actionMasked == 9 && !onHoverEvent) {
                this.h = true;
            }
        }
        if (actionMasked == 10 || actionMasked == 3) {
            this.h = false;
        }
        return true;
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        final int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            this.g = false;
        }
        if (!this.g) {
            final boolean onTouchEvent = super.onTouchEvent(motionEvent);
            if (actionMasked == 0 && !onTouchEvent) {
                this.g = true;
            }
        }
        if (actionMasked == 1 || actionMasked == 3) {
            this.g = false;
        }
        return true;
    }
    
    public abstract void setContentHeight(final int p0);
    
    public void setVisibility(final int visibility) {
        if (visibility != ((View)this).getVisibility()) {
            final i42 f = this.f;
            if (f != null) {
                f.c();
            }
            super.setVisibility(visibility);
        }
    }
    
    public class a implements k42
    {
        public boolean a;
        public int b;
        public final c c;
        
        public a(final c c) {
            this.c = c;
            this.a = false;
        }
        
        @Override
        public void a(final View view) {
            this.a = true;
        }
        
        @Override
        public void b(final View view) {
            if (this.a) {
                return;
            }
            final c c = this.c;
            c.f = null;
            c.b(c, this.b);
        }
        
        @Override
        public void c(final View view) {
            c.a(this.c, 0);
            this.a = false;
        }
        
        public a d(final i42 f, final int b) {
            this.c.f = f;
            this.b = b;
            return this;
        }
    }
}

import java.util.EmptyStackException;
import java.util.NoSuchElementException;
import java.util.AbstractMap;
import com.google.firebase.database.collection.g;
import java.util.Map;
import java.util.Comparator;
import com.google.firebase.database.collection.LLRBNode;
import java.util.ArrayDeque;
import java.util.Iterator;

// 
// Decompiled by Procyon v0.6.0
// 

public class le0 implements Iterator
{
    public final ArrayDeque a;
    public final boolean b;
    
    public le0(LLRBNode llrbNode, final Object o, final Comparator comparator, final boolean b) {
        this.a = new ArrayDeque();
        this.b = b;
        while (!llrbNode.isEmpty()) {
            int n;
            if (o != null) {
                final Object key = llrbNode.getKey();
                if (b) {
                    n = comparator.compare(o, key);
                }
                else {
                    n = comparator.compare(key, o);
                }
            }
            else {
                n = 1;
            }
            Label_0098: {
                if (n < 0) {
                    if (!b) {
                        break Label_0098;
                    }
                }
                else {
                    if (n == 0) {
                        this.a.push(llrbNode);
                        break;
                    }
                    this.a.push(llrbNode);
                    if (b) {
                        break Label_0098;
                    }
                }
                llrbNode = llrbNode.getLeft();
                continue;
            }
            llrbNode = llrbNode.getRight();
        }
    }
    
    public Map.Entry b() {
        try {
            final g g = this.a.pop();
            final AbstractMap.SimpleEntry simpleEntry = new AbstractMap.SimpleEntry(g.getKey(), g.getValue());
            if (this.b) {
                for (LLRBNode llrbNode = g.getLeft(); !llrbNode.isEmpty(); llrbNode = llrbNode.getRight()) {
                    this.a.push(llrbNode);
                }
            }
            else {
                for (LLRBNode llrbNode2 = g.getRight(); !llrbNode2.isEmpty(); llrbNode2 = llrbNode2.getLeft()) {
                    this.a.push(llrbNode2);
                }
            }
            return (Map.Entry)simpleEntry;
        }
        catch (final EmptyStackException ex) {
            throw new NoSuchElementException();
        }
    }
    
    @Override
    public boolean hasNext() {
        return this.a.size() > 0;
    }
    
    @Override
    public void remove() {
        throw new UnsupportedOperationException("remove called on immutable collection");
    }
}

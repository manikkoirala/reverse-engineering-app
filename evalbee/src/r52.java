import android.content.ComponentName;
import com.google.android.gms.tasks.OnCompleteListener;
import android.content.Context;
import com.google.android.gms.tasks.Task;
import android.content.Intent;
import java.util.concurrent.TimeUnit;
import com.google.android.gms.stats.WakeLock;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class r52
{
    public static final long a;
    public static final Object b;
    public static WakeLock c;
    
    static {
        a = TimeUnit.MINUTES.toMillis(1L);
        b = new Object();
    }
    
    public static void b(final Context context) {
        if (r52.c == null) {
            (r52.c = new WakeLock(context, 1, "wake:com.google.firebase.iid.WakeLockHolder")).setReferenceCounted(true);
        }
    }
    
    public static void c(final Intent intent) {
        synchronized (r52.b) {
            if (r52.c != null && d(intent)) {
                g(intent, false);
                r52.c.release();
            }
        }
    }
    
    public static boolean d(final Intent intent) {
        return intent.getBooleanExtra("com.google.firebase.iid.WakeLockHolder.wakefulintent", false);
    }
    
    public static void f(final Context context, final h82 h82, final Intent intent) {
        synchronized (r52.b) {
            b(context);
            final boolean d = d(intent);
            g(intent, true);
            if (!d) {
                r52.c.acquire(r52.a);
            }
            h82.c(intent).addOnCompleteListener((OnCompleteListener)new q52(intent));
        }
    }
    
    public static void g(final Intent intent, final boolean b) {
        intent.putExtra("com.google.firebase.iid.WakeLockHolder.wakefulintent", b);
    }
    
    public static ComponentName h(final Context context, final Intent intent) {
        synchronized (r52.b) {
            b(context);
            final boolean d = d(intent);
            g(intent, true);
            final ComponentName startService = context.startService(intent);
            if (startService == null) {
                return null;
            }
            if (!d) {
                r52.c.acquire(r52.a);
            }
            return startService;
        }
    }
}

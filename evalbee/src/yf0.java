import android.app.PendingIntent;
import android.os.Parcel;
import android.content.Intent;
import android.content.IntentSender;
import org.jetbrains.annotations.NotNull;
import android.os.Parcelable$Creator;
import android.os.Parcelable;

// 
// Decompiled by Procyon v0.6.0
// 

public final class yf0 implements Parcelable
{
    @NotNull
    public static final Parcelable$Creator<yf0> CREATOR;
    public static final c e;
    public final IntentSender a;
    public final Intent b;
    public final int c;
    public final int d;
    
    static {
        e = new c(null);
        CREATOR = (Parcelable$Creator)new Parcelable$Creator() {
            public yf0 a(final Parcel parcel) {
                fg0.e((Object)parcel, "inParcel");
                return new yf0(parcel);
            }
            
            public yf0[] b(final int n) {
                return new yf0[n];
            }
        };
    }
    
    public yf0(final IntentSender a, final Intent b, final int c, final int d) {
        fg0.e((Object)a, "intentSender");
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
    
    public yf0(final Parcel parcel) {
        fg0.e((Object)parcel, "parcel");
        final Parcelable parcelable = parcel.readParcelable(IntentSender.class.getClassLoader());
        fg0.b((Object)parcelable);
        this((IntentSender)parcelable, (Intent)parcel.readParcelable(Intent.class.getClassLoader()), parcel.readInt(), parcel.readInt());
    }
    
    public final Intent b() {
        return this.b;
    }
    
    public final int c() {
        return this.c;
    }
    
    public final int d() {
        return this.d;
    }
    
    public int describeContents() {
        return 0;
    }
    
    public final IntentSender e() {
        return this.a;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        fg0.e((Object)parcel, "dest");
        parcel.writeParcelable((Parcelable)this.a, n);
        parcel.writeParcelable((Parcelable)this.b, n);
        parcel.writeInt(this.c);
        parcel.writeInt(this.d);
    }
    
    public static final class a
    {
        public final IntentSender a;
        public Intent b;
        public int c;
        public int d;
        
        public a(final PendingIntent pendingIntent) {
            fg0.e((Object)pendingIntent, "pendingIntent");
            final IntentSender intentSender = pendingIntent.getIntentSender();
            fg0.d((Object)intentSender, "pendingIntent.intentSender");
            this(intentSender);
        }
        
        public a(final IntentSender a) {
            fg0.e((Object)a, "intentSender");
            this.a = a;
        }
        
        public final yf0 a() {
            return new yf0(this.a, this.b, this.c, this.d);
        }
        
        public final a b(final Intent b) {
            this.b = b;
            return this;
        }
        
        public final a c(final int d, final int c) {
            this.d = d;
            this.c = c;
            return this;
        }
    }
    
    public static final class c
    {
    }
}

import android.net.Uri;
import java.util.concurrent.Executor;
import android.os.Handler;
import android.graphics.Typeface;
import android.os.CancellationSignal;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class k70
{
    public static Typeface a(final Context context, final CancellationSignal cancellationSignal, final b[] array) {
        return sz1.b(context, cancellationSignal, array, 0);
    }
    
    public static a b(final Context context, final CancellationSignal cancellationSignal, final g70 g70) {
        return f70.e(context, g70, cancellationSignal);
    }
    
    public static Typeface c(final Context context, final g70 g70, final int n, final boolean b, final int n2, final Handler handler, final c c) {
        final re re = new re(c, handler);
        if (b) {
            return i70.e(context, g70, re, n, n2);
        }
        return i70.d(context, g70, n, null, re);
    }
    
    public static class a
    {
        public final int a;
        public final b[] b;
        
        public a(final int a, final b[] b) {
            this.a = a;
            this.b = b;
        }
        
        public static a a(final int n, final b[] array) {
            return new a(n, array);
        }
        
        public b[] b() {
            return this.b;
        }
        
        public int c() {
            return this.a;
        }
    }
    
    public static class b
    {
        public final Uri a;
        public final int b;
        public final int c;
        public final boolean d;
        public final int e;
        
        public b(final Uri uri, final int b, final int c, final boolean d, final int e) {
            this.a = (Uri)l71.g(uri);
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
        }
        
        public static b a(final Uri uri, final int n, final int n2, final boolean b, final int n3) {
            return new b(uri, n, n2, b, n3);
        }
        
        public int b() {
            return this.e;
        }
        
        public int c() {
            return this.b;
        }
        
        public Uri d() {
            return this.a;
        }
        
        public int e() {
            return this.c;
        }
        
        public boolean f() {
            return this.d;
        }
    }
    
    public abstract static class c
    {
        public abstract void a(final int p0);
        
        public abstract void b(final Typeface p0);
    }
}

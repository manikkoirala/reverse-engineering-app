import org.json.JSONException;
import android.util.Log;
import org.json.JSONObject;
import java.util.Map;
import com.google.android.gms.common.internal.Preconditions;

// 
// Decompiled by Procyon v0.6.0
// 

public final class eq extends s5
{
    public static final String d = "eq";
    public final String a;
    public final long b;
    public final long c;
    
    public eq(final String a, final long c, final long b) {
        Preconditions.checkNotEmpty(a);
        this.a = a;
        this.c = c;
        this.b = b;
    }
    
    public static eq c(final String s) {
        Preconditions.checkNotNull(s);
        final Map b = ww1.b(s);
        final long e = e(b, "iat");
        return new eq(s, (e(b, "exp") - e) * 1000L, e * 1000L);
    }
    
    public static eq d(String string) {
        try {
            final JSONObject jsonObject = new JSONObject(string);
            string = jsonObject.getString("token");
            return new eq(string, jsonObject.getLong("expiresIn"), jsonObject.getLong("receivedAt"));
        }
        catch (final JSONException ex) {
            final String d = eq.d;
            final StringBuilder sb = new StringBuilder();
            sb.append("Could not deserialize token: ");
            sb.append(((Throwable)ex).getMessage());
            Log.e(d, sb.toString());
            return null;
        }
    }
    
    public static long e(final Map map, final String s) {
        Preconditions.checkNotNull(map);
        Preconditions.checkNotEmpty(s);
        final Integer n = map.get(s);
        long longValue;
        if (n == null) {
            longValue = 0L;
        }
        else {
            longValue = n;
        }
        return longValue;
    }
    
    @Override
    public long a() {
        return this.b + this.c;
    }
    
    @Override
    public String b() {
        return this.a;
    }
}

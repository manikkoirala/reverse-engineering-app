import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.FirebaseAuth;
import com.android.volley.Request;
import java.util.HashMap;
import java.util.Map;
import com.android.volley.VolleyError;
import com.android.volley.d;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class ma0
{
    public ee1 a;
    public zg b;
    public Context c;
    public int d;
    public String e;
    
    public ma0(final Context c, final zg b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("https://");
        sb.append(a91.w());
        sb.append(":8759/excel_count");
        this.e = sb.toString();
        this.c = c;
        this.a = new n52(c, a91.u()).b();
        this.b = b;
        this.e();
    }
    
    public final void c(final String s) {
        final hr1 hr1 = new hr1(this, 0, this.e, new d.b(this) {
            public final ma0 a;
            
            public void b(final String s) {
                final ma0 a = this.a;
                final int d = a.d;
                if (d < 300) {
                    a.d(true, d, null);
                }
            }
        }, new d.a(this) {
            public final ma0 a;
            
            @Override
            public void a(final VolleyError volleyError) {
                this.a.d(false, 400, null);
            }
        }, s) {
            public final String w;
            public final ma0 x;
            
            @Override
            public d G(final yy0 yy0) {
                this.x.d = yy0.a;
                return super.G(yy0);
            }
            
            @Override
            public byte[] k() {
                return null;
            }
            
            @Override
            public Map o() {
                final HashMap hashMap = new HashMap();
                hashMap.put("x-user-token", this.w);
                return hashMap;
            }
        };
        hr1.L(new wq(40000, 0, 1.0f));
        this.a.a(hr1);
    }
    
    public final void d(final boolean b, final int n, final Object o) {
        final zg b2 = this.b;
        if (b2 != null) {
            b2.a(b, n, o);
        }
    }
    
    public final void e() {
        FirebaseAuth.getInstance().e().i(false).addOnCompleteListener((OnCompleteListener)new OnCompleteListener(this) {
            public final ma0 a;
            
            public void onComplete(final Task task) {
                if (task.isSuccessful()) {
                    this.a.c(((ya0)task.getResult()).c());
                }
                else {
                    this.a.d(false, 400, null);
                }
            }
        });
    }
}

import android.animation.Animator$AnimatorPauseListener;
import android.animation.AnimatorListenerAdapter;
import android.animation.Animator;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class d5
{
    public static void a(final Animator animator, final AnimatorListenerAdapter animatorListenerAdapter) {
        animator.addPauseListener((Animator$AnimatorPauseListener)animatorListenerAdapter);
    }
    
    public static void b(final Animator animator) {
        animator.pause();
    }
    
    public static void c(final Animator animator) {
        animator.resume();
    }
}

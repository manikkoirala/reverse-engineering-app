// 
// Decompiled by Procyon v0.6.0
// 

public final class wl1
{
    public final String a;
    public final String b;
    public final int c;
    public final long d;
    
    public wl1(final String a, final String b, final int c, final long d) {
        fg0.e((Object)a, "sessionId");
        fg0.e((Object)b, "firstSessionId");
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
    
    public final String a() {
        return this.b;
    }
    
    public final String b() {
        return this.a;
    }
    
    public final int c() {
        return this.c;
    }
    
    public final long d() {
        return this.d;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof wl1)) {
            return false;
        }
        final wl1 wl1 = (wl1)o;
        return fg0.a((Object)this.a, (Object)wl1.a) && fg0.a((Object)this.b, (Object)wl1.b) && this.c == wl1.c && this.d == wl1.d;
    }
    
    @Override
    public int hashCode() {
        return ((this.a.hashCode() * 31 + this.b.hashCode()) * 31 + Integer.hashCode(this.c)) * 31 + Long.hashCode(this.d);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SessionDetails(sessionId=");
        sb.append(this.a);
        sb.append(", firstSessionId=");
        sb.append(this.b);
        sb.append(", sessionIndex=");
        sb.append(this.c);
        sb.append(", sessionStartTimestampUs=");
        sb.append(this.d);
        sb.append(')');
        return sb.toString();
    }
}

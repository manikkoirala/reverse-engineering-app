import android.os.Parcel;
import android.os.IBinder;
import android.os.Binder;
import android.os.IInterface;

// 
// Decompiled by Procyon v0.6.0
// 

public interface wd0 extends IInterface
{
    int i(final vd0 p0, final String p1);
    
    void k(final int p0, final String[] p1);
    
    void l(final vd0 p0, final int p1);
    
    public abstract static class a extends Binder implements wd0
    {
        public a() {
            this.attachInterface((IInterface)this, "androidx.room.IMultiInstanceInvalidationService");
        }
        
        public static wd0 p(final IBinder binder) {
            if (binder == null) {
                return null;
            }
            final IInterface queryLocalInterface = binder.queryLocalInterface("androidx.room.IMultiInstanceInvalidationService");
            if (queryLocalInterface != null && queryLocalInterface instanceof wd0) {
                return (wd0)queryLocalInterface;
            }
            return new wd0.a.a(binder);
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(int i, final Parcel parcel, final Parcel parcel2, final int n) {
            if (i >= 1 && i <= 16777215) {
                parcel.enforceInterface("androidx.room.IMultiInstanceInvalidationService");
            }
            if (i != 1598968902) {
                if (i != 1) {
                    if (i != 2) {
                        if (i != 3) {
                            return super.onTransact(i, parcel, parcel2, n);
                        }
                        this.k(parcel.readInt(), parcel.createStringArray());
                    }
                    else {
                        this.l(vd0.a.p(parcel.readStrongBinder()), parcel.readInt());
                        parcel2.writeNoException();
                    }
                }
                else {
                    i = this.i(vd0.a.p(parcel.readStrongBinder()), parcel.readString());
                    parcel2.writeNoException();
                    parcel2.writeInt(i);
                }
                return true;
            }
            parcel2.writeString("androidx.room.IMultiInstanceInvalidationService");
            return true;
        }
        
        public static class a implements wd0
        {
            public IBinder a;
            
            public a(final IBinder a) {
                this.a = a;
            }
            
            public IBinder asBinder() {
                return this.a;
            }
            
            @Override
            public int i(final vd0 vd0, final String s) {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.room.IMultiInstanceInvalidationService");
                    obtain.writeStrongInterface((IInterface)vd0);
                    obtain.writeString(s);
                    this.a.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void k(final int n, final String[] array) {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.room.IMultiInstanceInvalidationService");
                    obtain.writeInt(n);
                    obtain.writeStringArray(array);
                    this.a.transact(3, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
        }
    }
}

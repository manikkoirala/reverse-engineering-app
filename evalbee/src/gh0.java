import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

// 
// Decompiled by Procyon v0.6.0
// 

@Retention(RetentionPolicy.RUNTIME)
public @interface gh0 {
    boolean nullSafe() default true;
    
    Class value();
}

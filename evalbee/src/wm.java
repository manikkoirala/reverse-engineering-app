import com.google.firebase.sessions.api.SessionSubscriber;

// 
// Decompiled by Procyon v0.6.0
// 

public class wm implements SessionSubscriber
{
    public final dp a;
    public final vm b;
    
    public wm(final dp a, final z00 z00) {
        this.a = a;
        this.b = new vm(z00);
    }
    
    @Override
    public boolean a() {
        return this.a.d();
    }
    
    @Override
    public Name b() {
        return Name.CRASHLYTICS;
    }
    
    @Override
    public void c(final a obj) {
        final zl0 f = zl0.f();
        final StringBuilder sb = new StringBuilder();
        sb.append("App Quality Sessions session changed: ");
        sb.append(obj);
        f.b(sb.toString());
        this.b.h(obj.a());
    }
    
    public String d(final String s) {
        return this.b.c(s);
    }
    
    public void e(final String s) {
        this.b.i(s);
    }
}

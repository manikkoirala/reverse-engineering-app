import androidx.datastore.preferences.protobuf.GeneratedMessageLite;
import androidx.datastore.preferences.a;
import java.util.Set;
import java.util.List;
import androidx.datastore.core.CorruptionException;
import kotlin.NoWhenBranchMatchedException;
import java.io.OutputStream;
import java.util.Iterator;
import androidx.datastore.preferences.core.MutablePreferences;
import androidx.datastore.preferences.PreferencesProto$Value;
import java.util.Map;
import java.io.InputStream;

// 
// Decompiled by Procyon v0.6.0
// 

public final class y71 implements nl1
{
    public static final y71 a;
    public static final String b;
    
    static {
        a = new y71();
        b = "preferences_pb";
    }
    
    @Override
    public Object a(final InputStream inputStream, final vl vl) {
        final x71 a = v71.a.a(inputStream);
        final MutablePreferences b = t71.b(new s71.b[0]);
        final Map m = a.M();
        fg0.d((Object)m, "preferencesProto.preferencesMap");
        for (final Map.Entry<String, V> entry : m.entrySet()) {
            final String s = entry.getKey();
            final PreferencesProto$Value preferencesProto$Value = (PreferencesProto$Value)entry.getValue();
            final y71 a2 = y71.a;
            fg0.d((Object)s, "name");
            fg0.d((Object)preferencesProto$Value, "value");
            a2.c(s, preferencesProto$Value, b);
        }
        return b.d();
    }
    
    public final void c(final String s, final PreferencesProto$Value preferencesProto$Value, final MutablePreferences mutablePreferences) {
        final PreferencesProto$Value.ValueCase z = preferencesProto$Value.Z();
        int n;
        if (z == null) {
            n = -1;
        }
        else {
            n = y71.a.a[z.ordinal()];
        }
        s71.a a = null;
        Object o2 = null;
        switch (n) {
            default: {
                throw new NoWhenBranchMatchedException();
            }
            case 8: {
                throw new CorruptionException("Value not set.", null, 2, null);
            }
            case 7: {
                a = u71.g(s);
                final List o = preferencesProto$Value.Y().O();
                fg0.d((Object)o, "value.stringSet.stringsList");
                o2 = vh.S((Iterable)o);
                break;
            }
            case 6: {
                a = u71.f(s);
                o2 = preferencesProto$Value.X();
                fg0.d(o2, "value.string");
                break;
            }
            case 5: {
                a = u71.e(s);
                o2 = preferencesProto$Value.W();
                break;
            }
            case 4: {
                a = u71.d(s);
                o2 = preferencesProto$Value.V();
                break;
            }
            case 3: {
                a = u71.b(s);
                o2 = preferencesProto$Value.T();
                break;
            }
            case 2: {
                a = u71.c(s);
                o2 = preferencesProto$Value.U();
                break;
            }
            case 1: {
                a = u71.a(s);
                o2 = preferencesProto$Value.R();
                break;
            }
            case -1: {
                throw new CorruptionException("Value case is null.", null, 2, null);
            }
        }
        mutablePreferences.j(a, o2);
    }
    
    public s71 d() {
        return t71.a();
    }
    
    public final String e() {
        return y71.b;
    }
    
    public final PreferencesProto$Value f(final Object o) {
        GeneratedMessageLite generatedMessageLite;
        String s;
        if (o instanceof Boolean) {
            generatedMessageLite = ((GeneratedMessageLite.a)PreferencesProto$Value.a0().z((boolean)o)).p();
            s = "newBuilder().setBoolean(value).build()";
        }
        else if (o instanceof Float) {
            generatedMessageLite = ((GeneratedMessageLite.a)PreferencesProto$Value.a0().B(((Number)o).floatValue())).p();
            s = "newBuilder().setFloat(value).build()";
        }
        else if (o instanceof Double) {
            generatedMessageLite = ((GeneratedMessageLite.a)PreferencesProto$Value.a0().A(((Number)o).doubleValue())).p();
            s = "newBuilder().setDouble(value).build()";
        }
        else if (o instanceof Integer) {
            generatedMessageLite = ((GeneratedMessageLite.a)PreferencesProto$Value.a0().C(((Number)o).intValue())).p();
            s = "newBuilder().setInteger(value).build()";
        }
        else if (o instanceof Long) {
            generatedMessageLite = ((GeneratedMessageLite.a)PreferencesProto$Value.a0().D(((Number)o).longValue())).p();
            s = "newBuilder().setLong(value).build()";
        }
        else if (o instanceof String) {
            generatedMessageLite = ((GeneratedMessageLite.a)PreferencesProto$Value.a0().E((String)o)).p();
            s = "newBuilder().setString(value).build()";
        }
        else {
            if (!(o instanceof Set)) {
                throw new IllegalStateException(fg0.m("PreferencesSerializer does not support type: ", (Object)o.getClass().getName()));
            }
            generatedMessageLite = ((GeneratedMessageLite.a)PreferencesProto$Value.a0().F(androidx.datastore.preferences.a.P().z((Iterable)o))).p();
            s = "newBuilder().setStringSet(\n                    StringSet.newBuilder().addAllStrings(value as Set<String>)\n                ).build()";
        }
        fg0.d((Object)generatedMessageLite, s);
        return (PreferencesProto$Value)generatedMessageLite;
    }
    
    public Object g(final s71 s71, final OutputStream outputStream, final vl vl) {
        final Map a = s71.a();
        final x71.a p3 = x71.P();
        for (final Map.Entry<s71.a, V> entry : a.entrySet()) {
            p3.z(entry.getKey().a(), this.f(entry.getValue()));
        }
        ((GeneratedMessageLite.a)p3).p().n(outputStream);
        return u02.a;
    }
}

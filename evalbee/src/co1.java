import java.util.Map;
import java.util.ConcurrentModificationException;

// 
// Decompiled by Procyon v0.6.0
// 

public class co1
{
    public static Object[] d;
    public static int e;
    public static Object[] f;
    public static int g;
    public int[] a;
    public Object[] b;
    public int c;
    
    public co1() {
        this.a = el.a;
        this.b = el.c;
        this.c = 0;
    }
    
    public co1(final int n) {
        if (n == 0) {
            this.a = el.a;
            this.b = el.c;
        }
        else {
            this.a(n);
        }
        this.c = 0;
    }
    
    public co1(final co1 co1) {
        this();
        if (co1 != null) {
            this.j(co1);
        }
    }
    
    private void a(final int n) {
        Label_0145: {
            if (n == 8) {
                synchronized (co1.class) {
                    final Object[] f = co1.f;
                    if (f != null) {
                        this.b = f;
                        co1.f = (Object[])f[0];
                        this.a = (int[])f[1];
                        f[0] = (f[1] = null);
                        --co1.g;
                        return;
                    }
                    break Label_0145;
                }
            }
            if (n == 4) {
                synchronized (co1.class) {
                    final Object[] d = co1.d;
                    if (d != null) {
                        this.b = d;
                        co1.d = (Object[])d[0];
                        this.a = (int[])d[1];
                        d[0] = (d[1] = null);
                        --co1.e;
                        return;
                    }
                }
            }
        }
        this.a = new int[n];
        this.b = new Object[n << 1];
    }
    
    public static int b(final int[] array, int a, final int n) {
        try {
            a = el.a(array, a, n);
            return a;
        }
        catch (final ArrayIndexOutOfBoundsException ex) {
            throw new ConcurrentModificationException();
        }
    }
    
    public static void d(final int[] array, final Object[] array2, int i) {
        if (array.length == 8) {
            synchronized (co1.class) {
                if (co1.g < 10) {
                    array2[0] = co1.f;
                    array2[1] = array;
                    for (i = (i << 1) - 1; i >= 2; --i) {
                        array2[i] = null;
                    }
                    co1.f = array2;
                    ++co1.g;
                }
                return;
            }
        }
        if (array.length == 4) {
            synchronized (co1.class) {
                if (co1.e < 10) {
                    array2[0] = co1.d;
                    array2[1] = array;
                    for (i = (i << 1) - 1; i >= 2; --i) {
                        array2[i] = null;
                    }
                    co1.d = array2;
                    ++co1.e;
                }
            }
        }
    }
    
    public void c(final int n) {
        final int c = this.c;
        final int[] a = this.a;
        if (a.length < n) {
            final Object[] b = this.b;
            this.a(n);
            if (this.c > 0) {
                System.arraycopy(a, 0, this.a, 0, c);
                System.arraycopy(b, 0, this.b, 0, c << 1);
            }
            d(a, b, c);
        }
        if (this.c == c) {
            return;
        }
        throw new ConcurrentModificationException();
    }
    
    public void clear() {
        final int c = this.c;
        if (c > 0) {
            final int[] a = this.a;
            final Object[] b = this.b;
            this.a = el.a;
            this.b = el.c;
            this.c = 0;
            d(a, b, c);
        }
        if (this.c <= 0) {
            return;
        }
        throw new ConcurrentModificationException();
    }
    
    public boolean containsKey(final Object o) {
        return this.f(o) >= 0;
    }
    
    public boolean containsValue(final Object o) {
        return this.h(o) >= 0;
    }
    
    public int e(final Object o, final int n) {
        final int c = this.c;
        if (c == 0) {
            return -1;
        }
        final int b = b(this.a, c, n);
        if (b < 0) {
            return b;
        }
        if (o.equals(this.b[b << 1])) {
            return b;
        }
        int n2;
        for (n2 = b + 1; n2 < c && this.a[n2] == n; ++n2) {
            if (o.equals(this.b[n2 << 1])) {
                return n2;
            }
        }
        for (int n3 = b - 1; n3 >= 0 && this.a[n3] == n; --n3) {
            if (o.equals(this.b[n3 << 1])) {
                return n3;
            }
        }
        return ~n2;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof co1) {
            final co1 co1 = (co1)obj;
            if (this.size() != co1.size()) {
                return false;
            }
            int i = 0;
            try {
                while (i < this.c) {
                    obj = this.i(i);
                    final Object m = this.m(i);
                    final Object value = co1.get(obj);
                    if (m == null) {
                        if (value != null || !co1.containsKey(obj)) {
                            return false;
                        }
                    }
                    else if (!m.equals(value)) {
                        return false;
                    }
                    ++i;
                }
                return true;
            }
            catch (final NullPointerException | ClassCastException ex) {
                return false;
            }
        }
        if (!(obj instanceof Map)) {
            return false;
        }
        final Map map = (Map)obj;
        if (this.size() != map.size()) {
            return false;
        }
        int j = 0;
        try {
            while (j < this.c) {
                final Object k = this.i(j);
                final Object l = this.m(j);
                obj = map.get(k);
                if (l == null) {
                    if (obj != null || !map.containsKey(k)) {
                        return false;
                    }
                }
                else if (!l.equals(obj)) {
                    return false;
                }
                ++j;
            }
            return true;
        }
        catch (final NullPointerException | ClassCastException ex2) {
            return false;
        }
    }
    
    public int f(final Object o) {
        int n;
        if (o == null) {
            n = this.g();
        }
        else {
            n = this.e(o, o.hashCode());
        }
        return n;
    }
    
    public int g() {
        final int c = this.c;
        if (c == 0) {
            return -1;
        }
        int b = b(this.a, c, 0);
        if (b < 0) {
            return b;
        }
        if (this.b[b << 1] == null) {
            return b;
        }
        int n;
        for (n = b + 1; n < c && this.a[n] == 0; ++n) {
            if (this.b[n << 1] == null) {
                return n;
            }
        }
        --b;
        while (b >= 0 && this.a[b] == 0) {
            if (this.b[b << 1] == null) {
                return b;
            }
            --b;
        }
        return ~n;
    }
    
    public Object get(final Object o) {
        return this.getOrDefault(o, null);
    }
    
    public Object getOrDefault(final Object o, Object o2) {
        final int f = this.f(o);
        if (f >= 0) {
            o2 = this.b[(f << 1) + 1];
        }
        return o2;
    }
    
    int h(final Object o) {
        final int n = this.c * 2;
        final Object[] b = this.b;
        if (o == null) {
            for (int i = 1; i < n; i += 2) {
                if (b[i] == null) {
                    return i >> 1;
                }
            }
        }
        else {
            for (int j = 1; j < n; j += 2) {
                if (o.equals(b[j])) {
                    return j >> 1;
                }
            }
        }
        return -1;
    }
    
    @Override
    public int hashCode() {
        final int[] a = this.a;
        final Object[] b = this.b;
        final int c = this.c;
        int n = 1;
        int i = 0;
        int n2 = 0;
        while (i < c) {
            final Object o = b[n];
            final int n3 = a[i];
            int hashCode;
            if (o == null) {
                hashCode = 0;
            }
            else {
                hashCode = o.hashCode();
            }
            n2 += (hashCode ^ n3);
            ++i;
            n += 2;
        }
        return n2;
    }
    
    public Object i(final int n) {
        return this.b[n << 1];
    }
    
    public boolean isEmpty() {
        return this.c <= 0;
    }
    
    public void j(final co1 co1) {
        final int c = co1.c;
        this.c(this.c + c);
        final int c2 = this.c;
        int i = 0;
        if (c2 == 0) {
            if (c > 0) {
                System.arraycopy(co1.a, 0, this.a, 0, c);
                System.arraycopy(co1.b, 0, this.b, 0, c << 1);
                this.c = c;
            }
        }
        else {
            while (i < c) {
                this.put(co1.i(i), co1.m(i));
                ++i;
            }
        }
    }
    
    public Object k(int c) {
        final Object[] b = this.b;
        final int n = c << 1;
        final Object o = b[n + 1];
        final int c2 = this.c;
        final int n2 = 0;
        if (c2 <= 1) {
            d(this.a, b, c2);
            this.a = el.a;
            this.b = el.c;
            c = n2;
        }
        else {
            final int n3 = c2 - 1;
            final int[] a = this.a;
            final int length = a.length;
            int n4 = 8;
            if (length > 8 && c2 < a.length / 3) {
                if (c2 > 8) {
                    n4 = c2 + (c2 >> 1);
                }
                this.a(n4);
                if (c2 != this.c) {
                    throw new ConcurrentModificationException();
                }
                if (c > 0) {
                    System.arraycopy(a, 0, this.a, 0, c);
                    System.arraycopy(b, 0, this.b, 0, n);
                }
                if (c < n3) {
                    final int n5 = c + 1;
                    final int[] a2 = this.a;
                    final int n6 = n3 - c;
                    System.arraycopy(a, n5, a2, c, n6);
                    System.arraycopy(b, n5 << 1, this.b, n, n6 << 1);
                }
            }
            else {
                if (c < n3) {
                    final int n7 = c + 1;
                    final int n8 = n3 - c;
                    System.arraycopy(a, n7, a, c, n8);
                    final Object[] b2 = this.b;
                    System.arraycopy(b2, n7 << 1, b2, n, n8 << 1);
                }
                final Object[] b3 = this.b;
                c = n3 << 1;
                b3[c + 1] = (b3[c] = null);
            }
            c = n3;
        }
        if (c2 == this.c) {
            this.c = c;
            return o;
        }
        throw new ConcurrentModificationException();
    }
    
    public Object l(int n, final Object o) {
        n = (n << 1) + 1;
        final Object[] b = this.b;
        final Object o2 = b[n];
        b[n] = o;
        return o2;
    }
    
    public Object m(final int n) {
        return this.b[(n << 1) + 1];
    }
    
    public Object put(Object o, final Object o2) {
        final int c = this.c;
        int n;
        int hashCode;
        if (o == null) {
            n = this.g();
            hashCode = 0;
        }
        else {
            hashCode = o.hashCode();
            n = this.e(o, hashCode);
        }
        if (n >= 0) {
            final int n2 = (n << 1) + 1;
            final Object[] b = this.b;
            o = b[n2];
            b[n2] = o2;
            return o;
        }
        final int n3 = ~n;
        final int[] a = this.a;
        if (c >= a.length) {
            int n4 = 8;
            if (c >= 8) {
                n4 = (c >> 1) + c;
            }
            else if (c < 4) {
                n4 = 4;
            }
            final Object[] b2 = this.b;
            this.a(n4);
            if (c != this.c) {
                throw new ConcurrentModificationException();
            }
            final int[] a2 = this.a;
            if (a2.length > 0) {
                System.arraycopy(a, 0, a2, 0, a.length);
                System.arraycopy(b2, 0, this.b, 0, b2.length);
            }
            d(a, b2, c);
        }
        if (n3 < c) {
            final int[] a3 = this.a;
            final int n5 = n3 + 1;
            System.arraycopy(a3, n3, a3, n5, c - n3);
            final Object[] b3 = this.b;
            System.arraycopy(b3, n3 << 1, b3, n5 << 1, this.c - n3 << 1);
        }
        final int c2 = this.c;
        if (c == c2) {
            final int[] a4 = this.a;
            if (n3 < a4.length) {
                a4[n3] = hashCode;
                final Object[] b4 = this.b;
                final int n6 = n3 << 1;
                b4[n6] = o;
                b4[n6 + 1] = o2;
                this.c = c2 + 1;
                return null;
            }
        }
        throw new ConcurrentModificationException();
    }
    
    public Object putIfAbsent(final Object o, final Object o2) {
        Object o3;
        if ((o3 = this.get(o)) == null) {
            o3 = this.put(o, o2);
        }
        return o3;
    }
    
    public Object remove(final Object o) {
        final int f = this.f(o);
        if (f >= 0) {
            return this.k(f);
        }
        return null;
    }
    
    public boolean remove(Object m, final Object o) {
        final int f = this.f(m);
        if (f >= 0) {
            m = this.m(f);
            if (o == m || (o != null && o.equals(m))) {
                this.k(f);
                return true;
            }
        }
        return false;
    }
    
    public Object replace(final Object o, final Object o2) {
        final int f = this.f(o);
        if (f >= 0) {
            return this.l(f, o2);
        }
        return null;
    }
    
    public boolean replace(Object m, final Object o, final Object o2) {
        final int f = this.f(m);
        if (f >= 0) {
            m = this.m(f);
            if (m == o || (o != null && o.equals(m))) {
                this.l(f, o2);
                return true;
            }
        }
        return false;
    }
    
    public int size() {
        return this.c;
    }
    
    @Override
    public String toString() {
        if (this.isEmpty()) {
            return "{}";
        }
        final StringBuilder sb = new StringBuilder(this.c * 28);
        sb.append('{');
        for (int i = 0; i < this.c; ++i) {
            if (i > 0) {
                sb.append(", ");
            }
            final Object j = this.i(i);
            if (j != this) {
                sb.append(j);
            }
            else {
                sb.append("(this Map)");
            }
            sb.append('=');
            final Object m = this.m(i);
            if (m != this) {
                sb.append(m);
            }
            else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }
}

import java.util.Date;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import android.os.Parcelable;

// 
// Decompiled by Procyon v0.6.0
// 

public final class pw1 implements Comparable, Parcelable
{
    public static final Parcelable$Creator<pw1> CREATOR;
    public final long a;
    public final int b;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator() {
            public pw1 a(final Parcel parcel) {
                return new pw1(parcel);
            }
            
            public pw1[] b(final int n) {
                return new pw1[n];
            }
        };
    }
    
    public pw1(final long a, final int b) {
        g(a, b);
        this.a = a;
        this.b = b;
    }
    
    public pw1(final Parcel parcel) {
        this.a = parcel.readLong();
        this.b = parcel.readInt();
    }
    
    public pw1(final Date date) {
        final long time = date.getTime();
        final long n = time / 1000L;
        final int n2 = (int)(time % 1000L) * 1000000;
        long a = n;
        int b = n2;
        if (n2 < 0) {
            a = n - 1L;
            b = n2 + 1000000000;
        }
        g(a, b);
        this.a = a;
        this.b = b;
    }
    
    public static pw1 f() {
        return new pw1(new Date());
    }
    
    public static void g(final long n, final int n2) {
        final boolean b = true;
        k71.a(n2 >= 0, "Timestamp nanoseconds out of range: %s", n2);
        k71.a(n2 < 1.0E9, "Timestamp nanoseconds out of range: %s", n2);
        k71.a(n >= -62135596800L, "Timestamp seconds out of range: %s", n);
        k71.a(n < 253402300800L && b, "Timestamp seconds out of range: %s", n);
    }
    
    public int c(final pw1 pw1) {
        final long a = this.a;
        final long a2 = pw1.a;
        if (a == a2) {
            return Integer.signum(this.b - pw1.b);
        }
        return Long.signum(a - a2);
    }
    
    public int d() {
        return this.b;
    }
    
    public int describeContents() {
        return 0;
    }
    
    public long e() {
        return this.a;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (!(o instanceof pw1)) {
            return false;
        }
        if (this.c((pw1)o) != 0) {
            b = false;
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        final long a = this.a;
        return ((int)a * 37 * 37 + (int)(a >> 32)) * 37 + this.b;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Timestamp(seconds=");
        sb.append(this.a);
        sb.append(", nanoseconds=");
        sb.append(this.b);
        sb.append(")");
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeLong(this.a);
        parcel.writeInt(this.b);
    }
}

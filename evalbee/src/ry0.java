import android.net.ConnectivityManager$NetworkCallback;
import android.net.NetworkCapabilities;
import org.jetbrains.annotations.Nullable;
import android.net.Network;
import org.jetbrains.annotations.NotNull;
import android.net.ConnectivityManager;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class ry0
{
    @Nullable
    public static final NetworkCapabilities a(@NotNull final ConnectivityManager connectivityManager, @Nullable final Network network) {
        fg0.e((Object)connectivityManager, "<this>");
        return connectivityManager.getNetworkCapabilities(network);
    }
    
    public static final boolean b(@NotNull final NetworkCapabilities networkCapabilities, final int n) {
        fg0.e((Object)networkCapabilities, "<this>");
        return networkCapabilities.hasCapability(n);
    }
    
    public static final void c(@NotNull final ConnectivityManager connectivityManager, @NotNull final ConnectivityManager$NetworkCallback connectivityManager$NetworkCallback) {
        fg0.e((Object)connectivityManager, "<this>");
        fg0.e((Object)connectivityManager$NetworkCallback, "networkCallback");
        connectivityManager.unregisterNetworkCallback(connectivityManager$NetworkCallback);
    }
}

import android.view.ViewGroup;
import android.view.ViewGroup$MarginLayoutParams;
import android.widget.TextView;
import android.view.ViewGroup$LayoutParams;
import android.widget.LinearLayout$LayoutParams;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View$OnClickListener;
import androidx.appcompat.widget.Toolbar;
import android.widget.ProgressBar;
import android.widget.LinearLayout;
import android.widget.EditText;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class ea1 extends u80
{
    public Context a;
    public String b;
    public y01 c;
    public String[] d;
    public EditText[] e;
    public int f;
    public LinearLayout[] g;
    public LinearLayout h;
    public ProgressBar i;
    
    public ea1(final Context a, final String b, final y01 c, final String[] d, final int f) {
        super(a);
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.f = f;
    }
    
    public final void a() {
        final Toolbar toolbar = (Toolbar)this.findViewById(2131297354);
        toolbar.setTitle(this.b);
        toolbar.setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ea1 a;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492981);
        this.getWindow().setLayout(-1, -1);
        this.a();
        this.h = (LinearLayout)this.findViewById(2131296777);
        this.i = (ProgressBar)this.findViewById(2131296972);
        new d(null).execute((Object[])new Void[0]);
        this.findViewById(2131296433).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ea1 a;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
        this.findViewById(2131296467).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ea1 a;
            
            public void onClick(final View view) {
                final String[] array = new String[this.a.e.length];
                int n = 0;
                while (true) {
                    final ea1 a = this.a;
                    final EditText[] e = a.e;
                    if (n >= e.length) {
                        final y01 c = a.c;
                        if (c != null) {
                            c.a(array);
                        }
                        this.a.dismiss();
                        return;
                    }
                    final String string = e[n].getText().toString();
                    array[n] = string;
                    if (string.trim().length() < 1) {
                        a91.G(this.a.a, 2131886261, 2131230909, 2131231086);
                        return;
                    }
                    ++n;
                }
            }
        });
    }
    
    public class d extends AsyncTask
    {
        public final ea1 a;
        
        public d(final ea1 a) {
            this.a = a;
        }
        
        public Void a(final Void... array) {
            final ea1 a = this.a;
            a.e = new EditText[a.d.length];
            final LinearLayout$LayoutParams linearLayout$LayoutParams = new LinearLayout$LayoutParams(-2, -2);
            final int d = a91.d(16, this.a.a);
            int n = 0;
            ((ViewGroup$MarginLayoutParams)linearLayout$LayoutParams).setMargins(0, 0, d, 0);
            final LinearLayout$LayoutParams linearLayout$LayoutParams2 = new LinearLayout$LayoutParams(-2, a91.d(40, this.a.a));
            final ea1 a2 = this.a;
            a2.g = new LinearLayout[a2.d.length];
            while (true) {
                final ea1 a3 = this.a;
                if (n >= a3.e.length) {
                    break;
                }
                a3.g[n] = yo.b(a3.a);
                final ea1 a4 = this.a;
                final TextView c = yo.c(a4.a, a4.d[n], 50);
                final ea1 a5 = this.a;
                a5.e[n] = yo.a(a5.a, a5.d[n], a5.f, 80);
                ((ViewGroup)this.a.g[n]).addView((View)c, (ViewGroup$LayoutParams)linearLayout$LayoutParams);
                final ea1 a6 = this.a;
                ((ViewGroup)a6.g[n]).addView((View)a6.e[n], (ViewGroup$LayoutParams)linearLayout$LayoutParams2);
                ++n;
            }
            return null;
        }
        
        public void b(final Void void1) {
            ((ViewGroup)this.a.h).removeAllViews();
            final LinearLayout[] g = this.a.g;
            for (int length = g.length, i = 0; i < length; ++i) {
                ((ViewGroup)this.a.h).addView((View)g[i]);
            }
            ((View)this.a.i).setVisibility(8);
        }
        
        public void onPreExecute() {
            ((View)this.a.i).setVisibility(0);
        }
    }
}

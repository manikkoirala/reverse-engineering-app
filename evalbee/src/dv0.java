import java.util.HashSet;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Set;
import com.google.firebase.firestore.local.e;

// 
// Decompiled by Procyon v0.6.0
// 

public class dv0 implements wc1
{
    public xc1 a;
    public final e b;
    public Set c;
    
    public dv0(final e b) {
        this.b = b;
    }
    
    @Override
    public void a(final du du) {
        this.c.add(du);
    }
    
    public final boolean b(final du du) {
        if (this.b.r().j(du)) {
            return true;
        }
        if (this.i(du)) {
            return true;
        }
        final xc1 a = this.a;
        return a != null && a.c(du);
    }
    
    @Override
    public void c(final du du) {
        this.c.remove(du);
    }
    
    @Override
    public void d(final du du) {
        this.c.add(du);
    }
    
    @Override
    public void e() {
        final hv0 q = this.b.q();
        final ArrayList list = new ArrayList();
        for (final du du : this.c) {
            if (!this.b(du)) {
                list.add(du);
            }
        }
        q.removeAll(list);
        this.c = null;
    }
    
    @Override
    public void f() {
        this.c = new HashSet();
    }
    
    @Override
    public void g(final au1 au1) {
        final iv0 r = this.b.r();
        final Iterator iterator = r.h(au1.h()).iterator();
        while (iterator.hasNext()) {
            this.c.add(iterator.next());
        }
        r.q(au1);
    }
    
    @Override
    public void h(final du du) {
        if (this.b(du)) {
            this.c.remove(du);
        }
        else {
            this.c.add(du);
        }
    }
    
    public final boolean i(final du du) {
        final Iterator iterator = this.b.p().iterator();
        while (iterator.hasNext()) {
            if (((fv0)iterator.next()).k(du)) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public long l() {
        return -1L;
    }
    
    @Override
    public void o(final xc1 a) {
        this.a = a;
    }
}

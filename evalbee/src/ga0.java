import java.util.Iterator;
import java.util.List;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.lang.reflect.Constructor;
import com.google.common.collect.Ordering;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class ga0
{
    public static final Ordering a;
    
    static {
        a = Ordering.natural().onResultOf(new m90() {
            public Boolean a(final Constructor constructor) {
                return Arrays.asList(constructor.getParameterTypes()).contains(String.class);
            }
        }).reverse();
    }
    
    public static void a(final Class clazz) {
        i71.m(c(clazz), "Futures.getChecked exception type (%s) must not be a RuntimeException", clazz);
        i71.m(b(clazz), "Futures.getChecked exception type (%s) must be an accessible class with an accessible constructor whose parameters (if any) must be of type String and/or Throwable", clazz);
    }
    
    public static boolean b(final Class clazz) {
        try {
            e(clazz, new Exception());
            return true;
        }
        catch (final Exception ex) {
            return false;
        }
    }
    
    public static boolean c(final Class clazz) {
        return RuntimeException.class.isAssignableFrom(clazz) ^ true;
    }
    
    public static Object d(final Constructor constructor, final Throwable t) {
        final Class[] parameterTypes = constructor.getParameterTypes();
        final Object[] initargs = new Object[parameterTypes.length];
        for (int i = 0; i < parameterTypes.length; ++i) {
            final Class clazz = parameterTypes[i];
            if (clazz.equals(String.class)) {
                initargs[i] = t.toString();
            }
            else {
                if (!clazz.equals(Throwable.class)) {
                    return null;
                }
                initargs[i] = t;
            }
        }
        try {
            return constructor.newInstance(initargs);
        }
        catch (final IllegalArgumentException | InstantiationException | IllegalAccessException | InvocationTargetException ex) {
            return null;
        }
    }
    
    public static Exception e(final Class obj, final Throwable t) {
        final Iterator iterator = f(Arrays.asList(obj.getConstructors())).iterator();
        while (iterator.hasNext()) {
            final Exception ex = (Exception)d((Constructor)iterator.next(), t);
            if (ex != null) {
                if (ex.getCause() == null) {
                    ex.initCause(t);
                }
                return ex;
            }
        }
        final String value = String.valueOf(obj);
        final StringBuilder sb = new StringBuilder(value.length() + 82);
        sb.append("No appropriate constructor for exception of type ");
        sb.append(value);
        sb.append(" in response to chained exception");
        throw new IllegalArgumentException(sb.toString(), t);
    }
    
    public static List f(final List list) {
        return ga0.a.sortedCopy((Iterable<Object>)list);
    }
}

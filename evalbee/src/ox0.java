import java.util.Iterator;
import android.graphics.Bitmap;
import java.util.ArrayList;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class ox0
{
    public static ArrayList a;
    public static ArrayList b;
    public static ArrayList c;
    public static int d;
    public static int e;
    
    static {
        ox0.a = new ArrayList();
        ox0.b = new ArrayList();
        ox0.c = new ArrayList();
        ox0.d = 0;
        ox0.e = 0;
    }
    
    public static void a() {
        if (ox0.a.size() > 0) {
            for (final Bitmap bitmap : ox0.a) {
                if (bitmap != null && !bitmap.isRecycled()) {
                    bitmap.recycle();
                }
            }
        }
        ox0.a = new ArrayList();
        ox0.b = new ArrayList();
        ox0.c = new ArrayList();
        ox0.d = 0;
        ox0.e = 0;
    }
}

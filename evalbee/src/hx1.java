import java.math.RoundingMode;
import java.util.Collections;
import java.util.Arrays;
import java.util.List;
import java.util.Iterator;
import java.util.Comparator;

// 
// Decompiled by Procyon v0.6.0
// 

public final class hx1
{
    public final int a;
    public final Comparator b;
    public final Object[] c;
    public int d;
    public Object e;
    
    public hx1(final Comparator comparator, final int a) {
        this.b = (Comparator)i71.s(comparator, "comparator");
        this.a = a;
        final boolean b = true;
        i71.h(a >= 0, "k (%s) must be >= 0", a);
        i71.h(a <= 1073741823 && b, "k (%s) must be <= Integer.MAX_VALUE / 2", a);
        this.c = new Object[tf0.b(a, 2)];
        this.d = 0;
        this.e = null;
    }
    
    public static hx1 a(final int n, final Comparator comparator) {
        return new hx1(comparator, n);
    }
    
    public void b(final Object o) {
        final int a = this.a;
        if (a == 0) {
            return;
        }
        final int d = this.d;
        if (d == 0) {
            this.c[0] = o;
            this.e = o;
            this.d = 1;
        }
        else if (d < a) {
            final Object[] c = this.c;
            this.d = d + 1;
            c[d] = o;
            if (this.b.compare(o, k01.a(this.e)) > 0) {
                this.e = o;
            }
        }
        else if (this.b.compare(o, k01.a(this.e)) < 0) {
            final Object[] c2 = this.c;
            final int d2 = this.d;
            final int d3 = d2 + 1;
            this.d = d3;
            c2[d2] = o;
            if (d3 == this.a * 2) {
                this.g();
            }
        }
    }
    
    public void c(final Iterator iterator) {
        while (iterator.hasNext()) {
            this.b(iterator.next());
        }
    }
    
    public final int d(int n, final int n2, int n3) {
        final Object a = k01.a(this.c[n3]);
        final Object[] c = this.c;
        c[n3] = c[n2];
        n3 = n;
        for (int i = n; i < n2; ++i, n3 = n) {
            n = n3;
            if (this.b.compare(k01.a(this.c[i]), a) < 0) {
                this.e(n3, i);
                n = n3 + 1;
            }
        }
        final Object[] c2 = this.c;
        c2[n2] = c2[n3];
        c2[n3] = a;
        return n3;
    }
    
    public final void e(final int n, final int n2) {
        final Object[] c = this.c;
        final Object o = c[n];
        c[n] = c[n2];
        c[n2] = o;
    }
    
    public List f() {
        Arrays.sort(this.c, 0, this.d, this.b);
        final int d = this.d;
        final int a = this.a;
        if (d > a) {
            final Object[] c = this.c;
            Arrays.fill(c, a, c.length, null);
            final int a2 = this.a;
            this.d = a2;
            this.e = this.c[a2 - 1];
        }
        return Collections.unmodifiableList((List<?>)Arrays.asList((T[])Arrays.copyOf(this.c, this.d)));
    }
    
    public final void g() {
        int n = this.a * 2 - 1;
        final int f = tf0.f(n + 0, RoundingMode.CEILING);
        int n2 = 0;
        int n3 = 0;
        int n4 = 0;
    Label_0153:
        while (true) {
            int i;
            int d;
            int max;
            do {
                int n5 = n4;
                if (n2 < n) {
                    d = this.d(n2, n, n2 + n + 1 >>> 1);
                    final int a = this.a;
                    if (d > a) {
                        --d;
                        max = n2;
                        n5 = n4;
                    }
                    else {
                        n5 = n4;
                        if (d >= a) {
                            break Label_0153;
                        }
                        max = Math.max(d, n2 + 1);
                        n5 = d;
                        d = n;
                    }
                    i = n3 + 1;
                    n = d;
                    n2 = max;
                    n3 = i;
                    n4 = n5;
                    continue;
                }
                this.d = this.a;
                this.e = k01.a(this.c[n5]);
                while (++n5 < this.a) {
                    if (this.b.compare(k01.a(this.c[n5]), k01.a(this.e)) > 0) {
                        this.e = this.c[n5];
                    }
                }
                return;
            } while (i < f * 3);
            Arrays.sort(this.c, max, d + 1, this.b);
            continue Label_0153;
        }
    }
}

import java.util.Calendar;
import java.io.Serializable;
import java.text.ParseException;
import java.util.GregorianCalendar;
import java.util.Date;
import java.text.ParsePosition;
import java.util.TimeZone;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class yd0
{
    public static final TimeZone a;
    
    static {
        a = TimeZone.getTimeZone("UTC");
    }
    
    public static boolean a(final String s, final int index, final char c) {
        return index < s.length() && s.charAt(index) == c;
    }
    
    public static int b(final String s, int i) {
        while (i < s.length()) {
            final char char1 = s.charAt(i);
            if (char1 < '0' || char1 > '9') {
                return i;
            }
            ++i;
        }
        return s.length();
    }
    
    public static Date c(String string, final ParsePosition parsePosition) {
        Serializable s = null;
        try {
            final int index = parsePosition.getIndex();
            final int n = index + 4;
            final int d = d(string, index, n);
            int n2 = n;
            if (a(string, n, '-')) {
                n2 = n + 1;
            }
            final int n3 = n2 + 2;
            final int d2 = d(string, n2, n3);
            int n4 = n3;
            if (a(string, n3, '-')) {
                n4 = n3 + 1;
            }
            int n5 = n4 + 2;
            final int d3 = d(string, n4, n5);
            final boolean a = a(string, n5, 'T');
            if (!a && string.length() <= n5) {
                s = new GregorianCalendar(d, d2 - 1, d3);
                ((Calendar)s).setLenient(false);
                parsePosition.setIndex(n5);
                return ((Calendar)s).getTime();
            }
            Label_0438: {
                if (!a) {
                    break Label_0438;
                }
                final int n6 = n5 + 1;
                final int n7 = n6 + 2;
                final int d4 = d(string, n6, n7);
                int n8 = n7;
                if (a(string, n7, ':')) {
                    n8 = n7 + 1;
                }
                final int n9 = n8 + 2;
                final int d5 = d(string, n8, n9);
                int index2 = n9;
                if (a(string, n9, ':')) {
                    index2 = n9 + 1;
                }
                Label_0421: {
                    if (string.length() <= index2) {
                        break Label_0421;
                    }
                    final char char1 = string.charAt(index2);
                    if (char1 == 'Z' || char1 == '+' || char1 == '-') {
                        break Label_0421;
                    }
                    int n10 = index2 + 2;
                    int d6;
                    final int n11 = d6 = d(string, index2, n10);
                    if (n11 > 59 && (d6 = n11) < 63) {
                        d6 = 59;
                    }
                    Label_0396: {
                        if (!a(string, n10, '.')) {
                            break Label_0396;
                        }
                        ++n10;
                        try {
                            final int b = b(string, n10 + 1);
                            final int min = Math.min(b, n10 + 3);
                            int d7 = d(string, n10, min);
                            final int n12 = min - n10;
                            if (n12 != 1) {
                                if (n12 == 2) {
                                    d7 *= 10;
                                }
                            }
                            else {
                                d7 *= 100;
                            }
                            final int n13 = d5;
                            final int n14 = d7;
                            int value = d4;
                            n5 = b;
                            int value2 = n13;
                            int value3 = n14;
                            while (true) {
                                while (true) {
                                    if (string.length() > n5) {
                                        final char char2 = string.charAt(n5);
                                        if (char2 == 'Z') {
                                            s = yd0.a;
                                            ++n5;
                                        }
                                        else {
                                            if (char2 != '+' && char2 != '-') {
                                                s = new(java.lang.IndexOutOfBoundsException.class)();
                                                final StringBuilder sb = new StringBuilder();
                                                sb.append("Invalid time zone indicator '");
                                                sb.append(char2);
                                                sb.append("'");
                                                new IndexOutOfBoundsException(sb.toString());
                                                throw s;
                                            }
                                            s = string.substring(n5);
                                            if (((String)s).length() < 5) {
                                                final StringBuilder sb2 = new StringBuilder();
                                                sb2.append((String)s);
                                                sb2.append("00");
                                                s = sb2.toString();
                                            }
                                            n5 += ((String)s).length();
                                            if (!"+0000".equals(s) && !"+00:00".equals(s)) {
                                                final StringBuilder sb3 = new StringBuilder();
                                                sb3.append("GMT");
                                                sb3.append((String)s);
                                                final String string2 = sb3.toString();
                                                s = TimeZone.getTimeZone(string2);
                                                final String id = ((TimeZone)s).getID();
                                                if (!id.equals(string2) && !id.replace(":", "").equals(string2)) {
                                                    final StringBuilder sb4 = new StringBuilder();
                                                    sb4.append("Mismatching time zone indicator: ");
                                                    sb4.append(string2);
                                                    sb4.append(" given, resolves to ");
                                                    sb4.append(((TimeZone)s).getID());
                                                    throw new IndexOutOfBoundsException(sb4.toString());
                                                }
                                            }
                                            else {
                                                s = yd0.a;
                                            }
                                        }
                                        final GregorianCalendar gregorianCalendar = new GregorianCalendar((TimeZone)s);
                                        gregorianCalendar.setLenient(false);
                                        gregorianCalendar.set(1, d);
                                        gregorianCalendar.set(2, d2 - 1);
                                        gregorianCalendar.set(5, d3);
                                        gregorianCalendar.set(11, value);
                                        gregorianCalendar.set(12, value2);
                                        gregorianCalendar.set(13, d6);
                                        gregorianCalendar.set(14, value3);
                                        parsePosition.setIndex(n5);
                                        return gregorianCalendar.getTime();
                                    }
                                    s = new IllegalArgumentException("No time zone indicator");
                                    throw s;
                                    final int n15 = d5;
                                    value = d4;
                                    n5 = n10;
                                    final int n16 = 0;
                                    value2 = n15;
                                    value3 = n16;
                                    continue;
                                    value3 = 0;
                                    d6 = 0;
                                    continue;
                                }
                                final int n17 = d5;
                                n5 = index2;
                                value = d4;
                                value2 = n17;
                                continue;
                            }
                            value = 0;
                            value2 = 0;
                        }
                        catch (final IllegalArgumentException s) {}
                        catch (final NumberFormatException s) {}
                        catch (final IndexOutOfBoundsException ex) {}
                    }
                }
            }
        }
        catch (final IllegalArgumentException ex2) {}
        catch (final NumberFormatException ex3) {}
        catch (final IndexOutOfBoundsException ex4) {}
        if (string == null) {
            string = null;
        }
        else {
            final StringBuilder sb5 = new StringBuilder();
            sb5.append('\"');
            sb5.append(string);
            sb5.append('\"');
            string = sb5.toString();
        }
        final String message = ((Throwable)s).getMessage();
        String string3 = null;
        Label_1017: {
            if (message != null) {
                string3 = message;
                if (!message.isEmpty()) {
                    break Label_1017;
                }
            }
            final StringBuilder sb6 = new StringBuilder();
            sb6.append("(");
            sb6.append(((IllegalArgumentException)s).getClass().getName());
            sb6.append(")");
            string3 = sb6.toString();
        }
        final StringBuilder sb7 = new StringBuilder();
        sb7.append("Failed to parse date [");
        sb7.append(string);
        sb7.append("]: ");
        sb7.append(string3);
        final ParseException ex5 = new ParseException(sb7.toString(), parsePosition.getIndex());
        ex5.initCause((Throwable)s);
        throw ex5;
    }
    
    public static int d(final String s, final int beginIndex, final int n) {
        if (beginIndex >= 0 && n <= s.length() && beginIndex <= n) {
            int i;
            int n2;
            if (beginIndex < n) {
                i = beginIndex + 1;
                final int digit = Character.digit(s.charAt(beginIndex), 10);
                if (digit < 0) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Invalid number: ");
                    sb.append(s.substring(beginIndex, n));
                    throw new NumberFormatException(sb.toString());
                }
                n2 = -digit;
            }
            else {
                n2 = 0;
                i = beginIndex;
            }
            while (i < n) {
                final int digit2 = Character.digit(s.charAt(i), 10);
                if (digit2 < 0) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Invalid number: ");
                    sb2.append(s.substring(beginIndex, n));
                    throw new NumberFormatException(sb2.toString());
                }
                n2 = n2 * 10 - digit2;
                ++i;
            }
            return -n2;
        }
        throw new NumberFormatException(s);
    }
}

import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public final class fe1 extends kw0
{
    public final Context c;
    
    public fe1(final Context c, final int n, final int n2) {
        fg0.e((Object)c, "mContext");
        super(n, n2);
        this.c = c;
    }
    
    @Override
    public void a(final ss1 ss1) {
        fg0.e((Object)ss1, "db");
        if (super.b >= 10) {
            ss1.T("INSERT OR REPLACE INTO `Preference` (`key`, `long_value`) VALUES (@key, @long_value)", new Object[] { "reschedule_needed", 1 });
        }
        else {
            this.c.getSharedPreferences("androidx.work.util.preferences", 0).edit().putBoolean("reschedule_needed", true).apply();
        }
    }
}

import com.google.common.base.AbstractIterator;
import java.util.Iterator;

// 
// Decompiled by Procyon v0.6.0
// 

public final class yo1
{
    public final gg a;
    public final boolean b;
    public final d c;
    public final int d;
    
    public yo1(final d d) {
        this(d, false, gg.i(), Integer.MAX_VALUE);
    }
    
    public yo1(final d c, final boolean b, final gg a, final int d) {
        this.c = c;
        this.b = b;
        this.a = a;
        this.d = d;
    }
    
    public static /* synthetic */ gg b(final yo1 yo1) {
        return yo1.a;
    }
    
    public static /* synthetic */ boolean c(final yo1 yo1) {
        return yo1.b;
    }
    
    public static /* synthetic */ int d(final yo1 yo1) {
        return yo1.d;
    }
    
    public static yo1 e(final char c) {
        return f(gg.f(c));
    }
    
    public static yo1 f(final gg gg) {
        i71.r(gg);
        return new yo1((d)new d(gg) {
            public final gg a;
            
            public c b(final yo1 yo1, final CharSequence charSequence) {
                return new c(this, yo1, charSequence) {
                    public final yo1$a h;
                    
                    @Override
                    public int f(final int n) {
                        return n + 1;
                    }
                    
                    @Override
                    public int g(final int n) {
                        return this.h.a.e(super.c, n);
                    }
                };
            }
        });
    }
    
    public Iterable g(final CharSequence charSequence) {
        i71.r(charSequence);
        return new Iterable(this, charSequence) {
            public final CharSequence a;
            public final yo1 b;
            
            @Override
            public Iterator iterator() {
                return this.b.h(this.a);
            }
            
            @Override
            public String toString() {
                final fh0 h = fh0.h(", ");
                final StringBuilder sb = new StringBuilder();
                sb.append('[');
                final StringBuilder b = h.b(sb, this);
                b.append(']');
                return b.toString();
            }
        };
    }
    
    public final Iterator h(final CharSequence charSequence) {
        return this.c.a(this, charSequence);
    }
    
    public yo1 i() {
        return this.j(gg.k());
    }
    
    public yo1 j(final gg gg) {
        i71.r(gg);
        return new yo1(this.c, this.b, gg, this.d);
    }
    
    public abstract static class c extends AbstractIterator
    {
        public final CharSequence c;
        public final gg d;
        public final boolean e;
        public int f;
        public int g;
        
        public c(final yo1 yo1, final CharSequence c) {
            this.f = 0;
            this.d = yo1.b(yo1);
            this.e = yo1.c(yo1);
            this.g = yo1.d(yo1);
            this.c = c;
        }
        
        public String e() {
            while (true) {
                final int f = this.f;
                int n;
                int i;
                while (true) {
                    final int f2 = this.f;
                    if (f2 == -1) {
                        return (String)this.c();
                    }
                    n = this.g(f2);
                    if (n == -1) {
                        n = this.c.length();
                        this.f = -1;
                    }
                    else {
                        this.f = this.f(n);
                    }
                    final int f3 = this.f;
                    i = f;
                    if (f3 != f) {
                        break;
                    }
                    if ((this.f = f3 + 1) <= this.c.length()) {
                        continue;
                    }
                    this.f = -1;
                }
                int n2;
                while (i < (n2 = n)) {
                    n2 = n;
                    if (!this.d.g(this.c.charAt(i))) {
                        break;
                    }
                    ++i;
                }
                while (n2 > i && this.d.g(this.c.charAt(n2 - 1))) {
                    --n2;
                }
                if (this.e && i == n2) {
                    continue;
                }
                final int g = this.g;
                if (g == 1) {
                    int length = this.c.length();
                    this.f = -1;
                    while (true) {
                        n2 = length;
                        if (length <= i) {
                            break;
                        }
                        n2 = length;
                        if (!this.d.g(this.c.charAt(length - 1))) {
                            break;
                        }
                        --length;
                    }
                }
                else {
                    this.g = g - 1;
                }
                return this.c.subSequence(i, n2).toString();
            }
        }
        
        public abstract int f(final int p0);
        
        public abstract int g(final int p0);
    }
    
    public interface d
    {
        Iterator a(final yo1 p0, final CharSequence p1);
    }
}

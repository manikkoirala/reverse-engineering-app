import java.lang.reflect.InvocationTargetException;
import android.os.Handler$Callback;
import android.os.Build$VERSION;
import android.os.Looper;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.ThreadFactory;
import android.os.Handler;
import java.util.concurrent.ExecutorService;

// 
// Decompiled by Procyon v0.6.0
// 

public class dr extends iu1
{
    public final Object a;
    public final ExecutorService b;
    public volatile Handler c;
    
    public dr() {
        this.a = new Object();
        this.b = Executors.newFixedThreadPool(4, new ThreadFactory(this) {
            public final AtomicInteger a = new AtomicInteger(0);
            public final dr b;
            
            @Override
            public Thread newThread(final Runnable target) {
                final Thread thread = new Thread(target);
                final StringBuilder sb = new StringBuilder();
                sb.append("arch_disk_io_");
                sb.append(this.a.getAndIncrement());
                thread.setName(sb.toString());
                return thread;
            }
        });
    }
    
    public static Handler d(final Looper looper) {
        if (Build$VERSION.SDK_INT >= 28) {
            return b.a(looper);
        }
        try {
            return Handler.class.getDeclaredConstructor(Looper.class, Handler$Callback.class, Boolean.TYPE).newInstance(looper, null, Boolean.TRUE);
        }
        catch (final InvocationTargetException ex) {
            return new Handler(looper);
        }
        catch (final IllegalAccessException | InstantiationException | NoSuchMethodException ex2) {
            return new Handler(looper);
        }
    }
    
    @Override
    public void a(final Runnable runnable) {
        this.b.execute(runnable);
    }
    
    @Override
    public boolean b() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }
    
    @Override
    public void c(final Runnable runnable) {
        if (this.c == null) {
            synchronized (this.a) {
                if (this.c == null) {
                    this.c = d(Looper.getMainLooper());
                }
            }
        }
        this.c.post(runnable);
    }
    
    public abstract static class b
    {
        public static Handler a(final Looper looper) {
            return Handler.createAsync(looper);
        }
    }
}

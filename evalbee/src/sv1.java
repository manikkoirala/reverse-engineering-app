import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

// 
// Decompiled by Procyon v0.6.0
// 

public final class sv1
{
    public String a;
    public Boolean b;
    public Integer c;
    public Thread.UncaughtExceptionHandler d;
    public ThreadFactory e;
    
    public sv1() {
        this.a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = null;
    }
    
    public static ThreadFactory c(final sv1 sv1) {
        final String a = sv1.a;
        final Boolean b = sv1.b;
        final Integer c = sv1.c;
        final Thread.UncaughtExceptionHandler d = sv1.d;
        ThreadFactory threadFactory = sv1.e;
        if (threadFactory == null) {
            threadFactory = Executors.defaultThreadFactory();
        }
        AtomicLong atomicLong;
        if (a != null) {
            atomicLong = new AtomicLong(0L);
        }
        else {
            atomicLong = null;
        }
        return new ThreadFactory(threadFactory, a, atomicLong, b, c, d) {
            public final ThreadFactory a;
            public final String b;
            public final AtomicLong c;
            public final Boolean d;
            public final Integer e;
            public final Thread.UncaughtExceptionHandler f;
            
            @Override
            public Thread newThread(final Runnable runnable) {
                final Thread thread = this.a.newThread(runnable);
                final String b = this.b;
                if (b != null) {
                    final AtomicLong c = this.c;
                    Objects.requireNonNull(c);
                    thread.setName(d(b, new Object[] { c.getAndIncrement() }));
                }
                final Boolean d = this.d;
                if (d != null) {
                    thread.setDaemon(d);
                }
                final Integer e = this.e;
                if (e != null) {
                    thread.setPriority(e);
                }
                final Thread.UncaughtExceptionHandler f = this.f;
                if (f != null) {
                    thread.setUncaughtExceptionHandler(f);
                }
                return thread;
            }
        };
    }
    
    public static String d(final String format, final Object... args) {
        return String.format(Locale.ROOT, format, args);
    }
    
    public ThreadFactory b() {
        return c(this);
    }
    
    public sv1 e(final boolean b) {
        this.b = b;
        return this;
    }
    
    public sv1 f(final String a) {
        d(a, 0);
        this.a = a;
        return this;
    }
}

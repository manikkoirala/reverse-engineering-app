import com.google.protobuf.ByteString;
import java.math.RoundingMode;
import java.util.Arrays;

// 
// Decompiled by Procyon v0.6.0
// 

public class h21
{
    public static final byte[][] c;
    public byte[] a;
    public int b;
    
    static {
        c = new byte[][] { { 0, 0 }, { -128, 0 }, { -64, 0 }, { -32, 0 }, { -16, 0 }, { -8, 0 }, { -4, 0 }, { -2, 0 }, { -1, 0 }, { -1, -128 }, { -1, -64 } };
    }
    
    public h21() {
        this.b = 0;
        this.a = new byte[1024];
    }
    
    public byte[] a() {
        return Arrays.copyOf(this.a, this.b);
    }
    
    public final void b(int newLength) {
        final int n = newLength + this.b;
        final byte[] a = this.a;
        if (n <= a.length) {
            return;
        }
        newLength = a.length * 2;
        if (newLength < n) {
            newLength = n;
        }
        this.a = Arrays.copyOf(a, newLength);
    }
    
    public void c(final byte[] array) {
        this.b(array.length);
        for (int length = array.length, i = 0; i < length; ++i) {
            this.a[this.b++] = array[i];
        }
    }
    
    public final int d(final long n) {
        long i = n;
        if (n < 0L) {
            i = ~n;
        }
        return uf0.a(64 - Long.numberOfLeadingZeros(i) + 1, 7, RoundingMode.UP);
    }
    
    public final int e(final long i) {
        return uf0.a(64 - Long.numberOfLeadingZeros(i), 8, RoundingMode.UP);
    }
    
    public final void f(final byte b) {
        if (b == 0) {
            this.l((byte)0);
            this.l((byte)(-1));
        }
        else if (b == -1) {
            this.l((byte)(-1));
            this.l((byte)0);
        }
        else {
            this.l(b);
        }
    }
    
    public final void g(final byte b) {
        if (b == 0) {
            this.m((byte)0);
            this.m((byte)(-1));
        }
        else if (b == -1) {
            this.m((byte)(-1));
            this.m((byte)0);
        }
        else {
            this.m(b);
        }
    }
    
    public void h(final ByteString byteString) {
        for (int i = 0; i < byteString.size(); ++i) {
            this.f(byteString.byteAt(i));
        }
        this.p();
    }
    
    public void i(final ByteString byteString) {
        for (int i = 0; i < byteString.size(); ++i) {
            this.g(byteString.byteAt(i));
        }
        this.q();
    }
    
    public void j(final double value) {
        final long doubleToLongBits = Double.doubleToLongBits(value);
        long n;
        if (doubleToLongBits < 0L) {
            n = -1L;
        }
        else {
            n = Long.MIN_VALUE;
        }
        this.t(doubleToLongBits ^ n);
    }
    
    public void k(final double value) {
        final long doubleToLongBits = Double.doubleToLongBits(value);
        long n;
        if (doubleToLongBits < 0L) {
            n = -1L;
        }
        else {
            n = Long.MIN_VALUE;
        }
        this.u(doubleToLongBits ^ n);
    }
    
    public final void l(final byte b) {
        this.b(1);
        this.a[this.b++] = b;
    }
    
    public final void m(final byte b) {
        this.b(1);
        this.a[this.b++] = (byte)~b;
    }
    
    public void n() {
        this.l((byte)(-1));
        this.l((byte)(-1));
    }
    
    public void o() {
        this.m((byte)(-1));
        this.m((byte)(-1));
    }
    
    public final void p() {
        this.l((byte)0);
        this.l((byte)1);
    }
    
    public final void q() {
        this.m((byte)0);
        this.m((byte)1);
    }
    
    public void r(long n) {
        final long n2 = lcmp(n, 0L);
        long n3;
        if (n2 < 0) {
            n3 = ~n;
        }
        else {
            n3 = n;
        }
        if (n3 < 64L) {
            this.b(1);
            this.a[this.b++] = (byte)(n ^ (long)h21.c[1][0]);
            return;
        }
        final int d = this.d(n3);
        this.b(d);
        if (d >= 2) {
            byte b;
            if (n2 < 0) {
                b = -1;
            }
            else {
                b = 0;
            }
            final int b2 = this.b;
            int n4;
            if (d == 10) {
                n4 = b2 + 2;
                final byte[] a = this.a;
                a[b2 + 1] = (a[b2] = b);
            }
            else if (d == 9) {
                n4 = b2 + 1;
                this.a[b2] = b;
            }
            else {
                n4 = b2;
            }
            for (int i = d - 1 + b2; i >= n4; --i) {
                this.a[i] = (byte)(0xFFL & n);
                n >>= 8;
            }
            final byte[] a2 = this.a;
            final int b3 = this.b;
            final byte b4 = a2[b3];
            final byte[] array = h21.c[d];
            a2[b3] = (byte)(b4 ^ array[0]);
            final int n5 = b3 + 1;
            a2[n5] ^= array[1];
            this.b = b3 + d;
            return;
        }
        throw new AssertionError((Object)String.format("Invalid length (%d) returned by signedNumLength", d));
    }
    
    public void s(final long n) {
        this.r(~n);
    }
    
    public void t(long n) {
        final int e = this.e(n);
        this.b(e + 1);
        final byte[] a = this.a;
        final int b = this.b;
        final int b2 = b + 1;
        this.b = b2;
        a[b] = (byte)e;
        int n2 = b2 + e - 1;
        int b3;
        while (true) {
            b3 = this.b;
            if (n2 < b3) {
                break;
            }
            this.a[n2] = (byte)(0xFFL & n);
            n >>>= 8;
            --n2;
        }
        this.b = b3 + e;
    }
    
    public void u(long n) {
        final int e = this.e(n);
        this.b(e + 1);
        final byte[] a = this.a;
        final int b = this.b;
        final int b2 = b + 1;
        this.b = b2;
        a[b] = (byte)~e;
        int n2 = b2 + e - 1;
        int b3;
        while (true) {
            b3 = this.b;
            if (n2 < b3) {
                break;
            }
            this.a[n2] = (byte)~(0xFFL & n);
            n >>>= 8;
            --n2;
        }
        this.b = b3 + e;
    }
    
    public void v(final CharSequence seq) {
        for (int length = seq.length(), i = 0; i < length; ++i) {
            int n = seq.charAt(i);
            if (n >= 128) {
                Label_0130: {
                    int n2;
                    if (n < 2048) {
                        n2 = (n >>> 6 | 0x3C0);
                    }
                    else {
                        if (n >= 55296 && 57343 >= n) {
                            n = Character.codePointAt(seq, i);
                            ++i;
                            this.f((byte)(n >>> 18 | 0xF0));
                            this.f((byte)((n >>> 12 & 0x3F) | 0x80));
                            this.f((byte)((n >>> 6 & 0x3F) | 0x80));
                            break Label_0130;
                        }
                        this.f((byte)(n >>> 12 | 0x1E0));
                        n2 = ((n >>> 6 & 0x3F) | 0x80);
                    }
                    this.f((byte)n2);
                }
                n = ((n & 0x3F) | 0x80);
            }
            this.f((byte)n);
        }
        this.p();
    }
    
    public void w(final CharSequence seq) {
        for (int length = seq.length(), i = 0; i < length; ++i) {
            int n = seq.charAt(i);
            if (n >= 128) {
                Label_0130: {
                    int n2;
                    if (n < 2048) {
                        n2 = (n >>> 6 | 0x3C0);
                    }
                    else {
                        if (n >= 55296 && 57343 >= n) {
                            n = Character.codePointAt(seq, i);
                            ++i;
                            this.g((byte)(n >>> 18 | 0xF0));
                            this.g((byte)((n >>> 12 & 0x3F) | 0x80));
                            this.g((byte)((n >>> 6 & 0x3F) | 0x80));
                            break Label_0130;
                        }
                        this.g((byte)(n >>> 12 | 0x1E0));
                        n2 = ((n >>> 6 & 0x3F) | 0x80);
                    }
                    this.g((byte)n2);
                }
                n = ((n & 0x3F) | 0x80);
            }
            this.g((byte)n);
        }
        this.q();
    }
}

import android.widget.CompoundButton;
import com.ekodroid.omrevaluator.util.DataModels.SortResultList;
import android.view.View;
import android.view.View$OnClickListener;
import androidx.appcompat.widget.Toolbar;
import android.os.Bundle;
import android.widget.RadioButton;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class l10 extends u80
{
    public Context a;
    public j10 b;
    public ud1 c;
    public RadioButton d;
    public RadioButton e;
    public RadioButton f;
    public RadioButton g;
    public RadioButton h;
    public RadioButton i;
    
    public l10(final Context a, final ud1 c, final j10 b) {
        super(a);
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public static /* synthetic */ j10 a(final l10 l10) {
        return l10.b;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492983);
        ((Toolbar)this.findViewById(2131297345)).setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final l10 a;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
        this.d = (RadioButton)this.findViewById(2131297012);
        this.e = (RadioButton)this.findViewById(2131297010);
        this.g = (RadioButton)this.findViewById(2131297011);
        this.f = (RadioButton)this.findViewById(2131297009);
        this.h = (RadioButton)this.findViewById(2131297005);
        this.i = (RadioButton)this.findViewById(2131297008);
        this.findViewById(2131296388).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final l10 a;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
        this.findViewById(2131296416).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final l10 a;
            
            public void onClick(final View view) {
                if (l10.a(this.a) != null) {
                    final j10 a = l10.a(this.a);
                    SortResultList.ResultSortBy resultSortBy;
                    if (((CompoundButton)this.a.e).isChecked()) {
                        resultSortBy = SortResultList.ResultSortBy.NAME;
                    }
                    else if (((CompoundButton)this.a.d).isChecked()) {
                        resultSortBy = SortResultList.ResultSortBy.ROLLNO;
                    }
                    else if (((CompoundButton)this.a.g).isChecked()) {
                        resultSortBy = SortResultList.ResultSortBy.RANK;
                    }
                    else {
                        resultSortBy = SortResultList.ResultSortBy.MARK;
                    }
                    a.a(new ud1(resultSortBy, ((CompoundButton)this.a.h).isChecked()));
                }
                this.a.dismiss();
            }
        });
        final ud1 c = this.c;
        if (c != null) {
            final RadioButton i = this.i;
            final boolean b = c.b;
            final boolean b2 = true;
            ((CompoundButton)i).setChecked(b ^ true);
            ((CompoundButton)this.h).setChecked(this.c.b);
            ((CompoundButton)this.e).setChecked(this.c.a == SortResultList.ResultSortBy.NAME);
            ((CompoundButton)this.d).setChecked(this.c.a == SortResultList.ResultSortBy.ROLLNO);
            ((CompoundButton)this.g).setChecked(this.c.a == SortResultList.ResultSortBy.RANK);
            ((CompoundButton)this.f).setChecked(this.c.a == SortResultList.ResultSortBy.MARK && b2);
        }
    }
}

import android.view.SubMenu;
import android.view.View;
import android.view.MenuItem;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class d2
{
    public final Context a;
    public a b;
    public b c;
    
    public d2(final Context a) {
        this.a = a;
    }
    
    public abstract boolean a();
    
    public abstract boolean b();
    
    public abstract View c(final MenuItem p0);
    
    public abstract boolean d();
    
    public abstract void e(final SubMenu p0);
    
    public abstract boolean f();
    
    public void g() {
        this.c = null;
        this.b = null;
    }
    
    public void h(final a b) {
        this.b = b;
    }
    
    public abstract void i(final b p0);
    
    public interface a
    {
    }
    
    public interface b
    {
        void onActionProviderVisibilityChanged(final boolean p0);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

public class r82 extends kw0
{
    public r82() {
        super(17, 18);
    }
    
    @Override
    public void a(final ss1 ss1) {
        ss1.M("ALTER TABLE `WorkSpec` ADD COLUMN `next_schedule_time_override` INTEGER NOT NULL DEFAULT 9223372036854775807");
        ss1.M("ALTER TABLE `WorkSpec` ADD COLUMN `next_schedule_time_override_generation` INTEGER NOT NULL DEFAULT 0");
    }
}

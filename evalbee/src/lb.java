import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.widgets.g;
import androidx.constraintlayout.core.widgets.analyzer.c;
import androidx.constraintlayout.core.widgets.h;
import androidx.constraintlayout.core.widgets.a;
import androidx.constraintlayout.core.widgets.f;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import androidx.constraintlayout.core.widgets.d;
import java.util.ArrayList;

// 
// Decompiled by Procyon v0.6.0
// 

public class lb
{
    public final ArrayList a;
    public a b;
    public d c;
    
    public lb(final d c) {
        this.a = new ArrayList();
        this.b = new a();
        this.c = c;
    }
    
    public final boolean a(final b b, final ConstraintWidget constraintWidget, int j) {
        this.b.a = constraintWidget.A();
        this.b.b = constraintWidget.T();
        this.b.c = constraintWidget.W();
        this.b.d = constraintWidget.x();
        final a b2 = this.b;
        b2.i = false;
        b2.j = j;
        final ConstraintWidget.DimensionBehaviour a = b2.a;
        final ConstraintWidget.DimensionBehaviour match_CONSTRAINT = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
        if (a == match_CONSTRAINT) {
            j = 1;
        }
        else {
            j = 0;
        }
        final boolean b3 = b2.b == match_CONSTRAINT;
        if (j != 0 && constraintWidget.d0 > 0.0f) {
            j = 1;
        }
        else {
            j = 0;
        }
        final boolean b4 = b3 && constraintWidget.d0 > 0.0f;
        if (j != 0 && constraintWidget.y[0] == 4) {
            b2.a = ConstraintWidget.DimensionBehaviour.FIXED;
        }
        if (b4 && constraintWidget.y[1] == 4) {
            b2.b = ConstraintWidget.DimensionBehaviour.FIXED;
        }
        b.a(constraintWidget, b2);
        constraintWidget.k1(this.b.e);
        constraintWidget.L0(this.b.f);
        constraintWidget.K0(this.b.h);
        constraintWidget.A0(this.b.g);
        final a b5 = this.b;
        b5.j = lb.a.k;
        return b5.i;
    }
    
    public final void b(final d d) {
        final int size = d.L0.size();
        final boolean u1 = d.U1(64);
        final b j1 = d.J1();
        for (int i = 0; i < size; ++i) {
            final ConstraintWidget constraintWidget = d.L0.get(i);
            if (!(constraintWidget instanceof f)) {
                if (!(constraintWidget instanceof androidx.constraintlayout.core.widgets.a)) {
                    if (!constraintWidget.l0()) {
                        if (u1) {
                            final c e = constraintWidget.e;
                            if (e != null) {
                                final androidx.constraintlayout.core.widgets.analyzer.d f = constraintWidget.f;
                                if (f != null && e.e.j && f.e.j) {
                                    continue;
                                }
                            }
                        }
                        final ConstraintWidget.DimensionBehaviour u2 = constraintWidget.u(0);
                        final int n = 1;
                        final ConstraintWidget.DimensionBehaviour u3 = constraintWidget.u(1);
                        final ConstraintWidget.DimensionBehaviour match_CONSTRAINT = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                        final boolean b = u2 == match_CONSTRAINT && constraintWidget.w != 1 && u3 == match_CONSTRAINT && constraintWidget.x != 1;
                        int n5 = 0;
                        Label_0339: {
                            int n2 = 0;
                            Label_0337: {
                                if ((n2 = (b ? 1 : 0)) == 0) {
                                    n2 = (b ? 1 : 0);
                                    if (d.U1(1)) {
                                        n2 = (b ? 1 : 0);
                                        if (!(constraintWidget instanceof h)) {
                                            int n3 = b ? 1 : 0;
                                            if (u2 == match_CONSTRAINT) {
                                                n3 = (b ? 1 : 0);
                                                if (constraintWidget.w == 0) {
                                                    n3 = (b ? 1 : 0);
                                                    if (u3 != match_CONSTRAINT) {
                                                        n3 = (b ? 1 : 0);
                                                        if (!constraintWidget.i0()) {
                                                            n3 = 1;
                                                        }
                                                    }
                                                }
                                            }
                                            int n4 = n3;
                                            if (u3 == match_CONSTRAINT) {
                                                n4 = n3;
                                                if (constraintWidget.x == 0) {
                                                    n4 = n3;
                                                    if (u2 != match_CONSTRAINT) {
                                                        n4 = n3;
                                                        if (!constraintWidget.i0()) {
                                                            n4 = 1;
                                                        }
                                                    }
                                                }
                                            }
                                            if (u2 != match_CONSTRAINT) {
                                                n2 = n4;
                                                if (u3 != match_CONSTRAINT) {
                                                    break Label_0337;
                                                }
                                            }
                                            n2 = n4;
                                            if (constraintWidget.d0 > 0.0f) {
                                                n5 = n;
                                                break Label_0339;
                                            }
                                        }
                                    }
                                }
                            }
                            n5 = n2;
                        }
                        if (n5 == 0) {
                            this.a(j1, constraintWidget, lb.a.k);
                        }
                    }
                }
            }
        }
        j1.b();
    }
    
    public final void c(final d d, final String s, final int n, final int n2, final int n3) {
        final int i = d.I();
        final int h = d.H();
        d.a1(0);
        d.Z0(0);
        d.k1(n2);
        d.L0(n3);
        d.a1(i);
        d.Z0(h);
        this.c.Y1(n);
        this.c.s1();
    }
    
    public long d(final d d, int n, int a, int k1, int n2, int w, int i, int b, int n3, int size) {
        final b j1 = d.J1();
        size = d.L0.size();
        final int w2 = d.W();
        final int x = d.x();
        final boolean b2 = g.b(n, 128);
        if (!b2 && !g.b(n, 64)) {
            n = 0;
        }
        else {
            n = 1;
        }
        k1 = n;
        Label_0238: {
            if (n != 0) {
                a = 0;
                while (true) {
                    k1 = n;
                    if (a >= size) {
                        break Label_0238;
                    }
                    final ConstraintWidget constraintWidget = d.L0.get(a);
                    final ConstraintWidget.DimensionBehaviour a2 = constraintWidget.A();
                    final ConstraintWidget.DimensionBehaviour match_CONSTRAINT = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                    if (a2 == match_CONSTRAINT) {
                        k1 = 1;
                    }
                    else {
                        k1 = 0;
                    }
                    if (constraintWidget.T() == match_CONSTRAINT) {
                        n3 = 1;
                    }
                    else {
                        n3 = 0;
                    }
                    if (k1 != 0 && n3 != 0 && constraintWidget.v() > 0.0f) {
                        k1 = 1;
                    }
                    else {
                        k1 = 0;
                    }
                    if (constraintWidget.i0() && k1 != 0) {
                        break;
                    }
                    if (constraintWidget.k0() && k1 != 0) {
                        break;
                    }
                    if (constraintWidget instanceof h) {
                        break;
                    }
                    if (constraintWidget.i0()) {
                        break;
                    }
                    if (constraintWidget.k0()) {
                        break;
                    }
                    ++a;
                }
                k1 = 0;
            }
        }
        if (k1 != 0) {
            final boolean r = androidx.constraintlayout.core.c.r;
        }
        if ((n2 == 1073741824 && i == 1073741824) || b2) {
            n = 1;
        }
        else {
            n = 0;
        }
        final int n4 = k1 & n;
        boolean b4;
        if (n4 != 0) {
            n = Math.min(d.G(), w);
            a = Math.min(d.F(), b);
            if (n2 == 1073741824 && d.W() != n) {
                d.k1(n);
                d.N1();
            }
            if (i == 1073741824 && d.x() != a) {
                d.L0(a);
                d.N1();
            }
            boolean b3;
            if (n2 == 1073741824 && i == 1073741824) {
                b3 = d.F1(b2);
                n = 2;
            }
            else {
                b3 = d.G1(b2);
                if (n2 == 1073741824) {
                    b3 &= d.H1(b2, 0);
                    n = 1;
                }
                else {
                    n = 0;
                }
                if (i == 1073741824) {
                    b3 &= d.H1(b2, 1);
                    ++n;
                }
            }
            b4 = b3;
            a = n;
            if (b3) {
                d.p1(n2 == 1073741824, i == 1073741824);
                b4 = b3;
                a = n;
            }
        }
        else {
            b4 = false;
            a = 0;
        }
        if (!b4 || a != 2) {
            k1 = d.K1();
            if (size > 0) {
                this.b(d);
            }
            this.e(d);
            b = this.a.size();
            if (size > 0) {
                this.c(d, "First pass", 0, w2, x);
            }
            if (b > 0) {
                final ConstraintWidget.DimensionBehaviour a3 = d.A();
                final ConstraintWidget.DimensionBehaviour wrap_CONTENT = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                if (a3 == wrap_CONTENT) {
                    n3 = 1;
                }
                else {
                    n3 = 0;
                }
                if (d.T() == wrap_CONTENT) {
                    size = 1;
                }
                else {
                    size = 0;
                }
                a = Math.max(d.W(), this.c.I());
                n = Math.max(d.x(), this.c.H());
                i = 0;
                n2 = 0;
                while (i < b) {
                    final ConstraintWidget constraintWidget2 = this.a.get(i);
                    if (!(constraintWidget2 instanceof h)) {
                        w = n2;
                    }
                    else {
                        w = constraintWidget2.W();
                        final int x2 = constraintWidget2.x();
                        final boolean a4 = this.a(j1, constraintWidget2, lb.a.l);
                        final int w3 = constraintWidget2.W();
                        final int x3 = constraintWidget2.x();
                        if (w3 != w) {
                            constraintWidget2.k1(w3);
                            n2 = a;
                            if (n3 != 0 && constraintWidget2.M() > (n2 = a)) {
                                n2 = Math.max(a, constraintWidget2.M() + constraintWidget2.o(ConstraintAnchor.Type.RIGHT).f());
                            }
                            w = 1;
                            a = n2;
                        }
                        else {
                            w = ((a4 ? 1 : 0) | n2);
                        }
                        n2 = n;
                        if (x3 != x2) {
                            constraintWidget2.L0(x3);
                            n2 = n;
                            if (size != 0 && constraintWidget2.r() > (n2 = n)) {
                                n2 = Math.max(n, constraintWidget2.r() + constraintWidget2.o(ConstraintAnchor.Type.BOTTOM).f());
                            }
                            w = 1;
                        }
                        w |= (((h)constraintWidget2).F1() ? 1 : 0);
                        n = n2;
                    }
                    ++i;
                    n2 = w;
                }
                final int n5 = 0;
                i = n2;
                n2 = b;
                w = n4;
                int l = n5;
                while (l < 2) {
                    int n6;
                    for (int index = 0; index < n2; ++index, a = n6, i = b) {
                        final ConstraintWidget constraintWidget3 = this.a.get(index);
                        if (!(constraintWidget3 instanceof fd0) || constraintWidget3 instanceof h) {
                            if (!(constraintWidget3 instanceof f)) {
                                if (constraintWidget3.V() != 8) {
                                    if (w == 0 || !constraintWidget3.e.e.j || !constraintWidget3.f.e.j) {
                                        if (!(constraintWidget3 instanceof h)) {
                                            final int w4 = constraintWidget3.W();
                                            final int x4 = constraintWidget3.x();
                                            final int p10 = constraintWidget3.p();
                                            b = lb.a.l;
                                            if (l == 1) {
                                                b = lb.a.m;
                                            }
                                            b = ((this.a(j1, constraintWidget3, b) ? 1 : 0) | i);
                                            final int w5 = constraintWidget3.W();
                                            final int x5 = constraintWidget3.x();
                                            i = a;
                                            if (w5 != w4) {
                                                constraintWidget3.k1(w5);
                                                i = a;
                                                if (n3 != 0 && constraintWidget3.M() > (i = a)) {
                                                    i = Math.max(a, constraintWidget3.M() + constraintWidget3.o(ConstraintAnchor.Type.RIGHT).f());
                                                }
                                                b = 1;
                                            }
                                            a = n;
                                            if (x5 != x4) {
                                                constraintWidget3.L0(x5);
                                                a = n;
                                                if (size != 0 && constraintWidget3.r() > (a = n)) {
                                                    a = Math.max(n, constraintWidget3.r() + constraintWidget3.o(ConstraintAnchor.Type.BOTTOM).f());
                                                }
                                                b = 1;
                                            }
                                            if (constraintWidget3.Z() && p10 != constraintWidget3.p()) {
                                                b = 1;
                                                n6 = i;
                                                n = a;
                                                continue;
                                            }
                                            n = a;
                                            n6 = i;
                                            continue;
                                        }
                                    }
                                }
                            }
                        }
                        n6 = a;
                        b = i;
                    }
                    if (i == 0) {
                        break;
                    }
                    ++l;
                    this.c(d, "intermediate pass", l, w2, x);
                    i = 0;
                }
            }
            d.X1(k1);
        }
        return 0L;
    }
    
    public void e(final d d) {
        this.a.clear();
        for (int size = d.L0.size(), i = 0; i < size; ++i) {
            final ConstraintWidget e = d.L0.get(i);
            final ConstraintWidget.DimensionBehaviour a = e.A();
            final ConstraintWidget.DimensionBehaviour match_CONSTRAINT = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
            if (a == match_CONSTRAINT || e.T() == match_CONSTRAINT) {
                this.a.add(e);
            }
        }
        d.N1();
    }
    
    public static class a
    {
        public static int k = 0;
        public static int l = 1;
        public static int m = 2;
        public ConstraintWidget.DimensionBehaviour a;
        public ConstraintWidget.DimensionBehaviour b;
        public int c;
        public int d;
        public int e;
        public int f;
        public int g;
        public boolean h;
        public boolean i;
        public int j;
    }
    
    public interface b
    {
        void a(final ConstraintWidget p0, final a p1);
        
        void b();
    }
}

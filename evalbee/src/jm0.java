import java.util.Locale;
import java.util.Map;
import java.util.LinkedHashMap;

// 
// Decompiled by Procyon v0.6.0
// 

public class jm0
{
    private int createCount;
    private int evictionCount;
    private int hitCount;
    private final LinkedHashMap<Object, Object> map;
    private int maxSize;
    private int missCount;
    private int putCount;
    private int size;
    
    public jm0(final int maxSize) {
        if (maxSize > 0) {
            this.maxSize = maxSize;
            this.map = new LinkedHashMap<Object, Object>(0, 0.75f, true);
            return;
        }
        throw new IllegalArgumentException("maxSize <= 0");
    }
    
    public final int a(final Object obj, final Object obj2) {
        final int size = this.sizeOf(obj, obj2);
        if (size >= 0) {
            return size;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Negative size: ");
        sb.append(obj);
        sb.append("=");
        sb.append(obj2);
        throw new IllegalStateException(sb.toString());
    }
    
    public Object create(final Object o) {
        return null;
    }
    
    public final int createCount() {
        synchronized (this) {
            return this.createCount;
        }
    }
    
    public void entryRemoved(final boolean b, final Object o, final Object o2, final Object o3) {
    }
    
    public final void evictAll() {
        this.trimToSize(-1);
    }
    
    public final int evictionCount() {
        synchronized (this) {
            return this.evictionCount;
        }
    }
    
    public final Object get(final Object key) {
        if (key != null) {
            synchronized (this) {
                final Object value = this.map.get(key);
                if (value != null) {
                    ++this.hitCount;
                    return value;
                }
                ++this.missCount;
                monitorexit(this);
                final Object create = this.create(key);
                if (create == null) {
                    return null;
                }
                synchronized (this) {
                    ++this.createCount;
                    final Object put = this.map.put(key, create);
                    if (put != null) {
                        this.map.put(key, put);
                    }
                    else {
                        this.size += this.a(key, create);
                    }
                    monitorexit(this);
                    if (put != null) {
                        this.entryRemoved(false, key, create, put);
                        return put;
                    }
                    this.trimToSize(this.maxSize);
                    return create;
                }
            }
        }
        throw new NullPointerException("key == null");
    }
    
    public final int hitCount() {
        synchronized (this) {
            return this.hitCount;
        }
    }
    
    public final int maxSize() {
        synchronized (this) {
            return this.maxSize;
        }
    }
    
    public final int missCount() {
        synchronized (this) {
            return this.missCount;
        }
    }
    
    public final Object put(final Object key, final Object value) {
        if (key != null && value != null) {
            synchronized (this) {
                ++this.putCount;
                this.size += this.a(key, value);
                final Object put = this.map.put(key, value);
                if (put != null) {
                    this.size -= this.a(key, put);
                }
                monitorexit(this);
                if (put != null) {
                    this.entryRemoved(false, key, put, value);
                }
                this.trimToSize(this.maxSize);
                return put;
            }
        }
        throw new NullPointerException("key == null || value == null");
    }
    
    public final int putCount() {
        synchronized (this) {
            return this.putCount;
        }
    }
    
    public final Object remove(final Object key) {
        if (key != null) {
            synchronized (this) {
                final Object remove = this.map.remove(key);
                if (remove != null) {
                    this.size -= this.a(key, remove);
                }
                monitorexit(this);
                if (remove != null) {
                    this.entryRemoved(false, key, remove, null);
                }
                return remove;
            }
        }
        throw new NullPointerException("key == null");
    }
    
    public void resize(final int maxSize) {
        if (maxSize > 0) {
            synchronized (this) {
                this.maxSize = maxSize;
                monitorexit(this);
                this.trimToSize(maxSize);
                return;
            }
        }
        throw new IllegalArgumentException("maxSize <= 0");
    }
    
    public final int size() {
        synchronized (this) {
            return this.size;
        }
    }
    
    public int sizeOf(final Object o, final Object o2) {
        return 1;
    }
    
    public final Map<Object, Object> snapshot() {
        synchronized (this) {
            return new LinkedHashMap<Object, Object>(this.map);
        }
    }
    
    @Override
    public final String toString() {
        synchronized (this) {
            final int hitCount = this.hitCount;
            final int n = this.missCount + hitCount;
            int i;
            if (n != 0) {
                i = hitCount * 100 / n;
            }
            else {
                i = 0;
            }
            return String.format(Locale.US, "LruCache[maxSize=%d,hits=%d,misses=%d,hitRate=%d%%]", this.maxSize, this.hitCount, this.missCount, i);
        }
    }
    
    public void trimToSize(final int n) {
        while (true) {
            synchronized (this) {
                if (this.size < 0 || (this.map.isEmpty() && this.size != 0)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(this.getClass().getName());
                    sb.append(".sizeOf() is reporting inconsistent results!");
                    throw new IllegalStateException(sb.toString());
                }
                if (this.size <= n || this.map.isEmpty()) {
                    return;
                }
                final Map.Entry entry = (Map.Entry)this.map.entrySet().iterator().next();
                final Object key = entry.getKey();
                final Object value = entry.getValue();
                this.map.remove(key);
                this.size -= this.a(key, value);
                ++this.evictionCount;
                monitorexit(this);
                this.entryRemoved(true, key, value, null);
            }
        }
    }
}

import java.util.Objects;
import java.io.Writer;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class er1
{
    public static void a(final nh0 nh0, final vh0 vh0) {
        kz1.V.d(vh0, nh0);
    }
    
    public static Writer b(final Appendable appendable) {
        Writer writer;
        if (appendable instanceof Writer) {
            writer = (Writer)appendable;
        }
        else {
            writer = new b(appendable);
        }
        return writer;
    }
    
    public static final class b extends Writer
    {
        public final Appendable a;
        public final a b;
        
        public b(final Appendable a) {
            this.b = new a(null);
            this.a = a;
        }
        
        @Override
        public Writer append(final CharSequence charSequence) {
            this.a.append(charSequence);
            return this;
        }
        
        @Override
        public Writer append(final CharSequence charSequence, final int n, final int n2) {
            this.a.append(charSequence, n, n2);
            return this;
        }
        
        @Override
        public void close() {
        }
        
        @Override
        public void flush() {
        }
        
        @Override
        public void write(final int n) {
            this.a.append((char)n);
        }
        
        @Override
        public void write(final String obj, final int n, final int n2) {
            Objects.requireNonNull(obj);
            this.a.append(obj, n, n2 + n);
        }
        
        @Override
        public void write(final char[] array, final int n, final int n2) {
            this.b.a(array);
            this.a.append(this.b, n, n2 + n);
        }
        
        public static class a implements CharSequence
        {
            public char[] a;
            public String b;
            
            public void a(final char[] a) {
                this.a = a;
                this.b = null;
            }
            
            @Override
            public char charAt(final int n) {
                return this.a[n];
            }
            
            @Override
            public int length() {
                return this.a.length;
            }
            
            @Override
            public CharSequence subSequence(final int offset, final int n) {
                return new String(this.a, offset, n - offset);
            }
            
            @Override
            public String toString() {
                if (this.b == null) {
                    this.b = new String(this.a);
                }
                return this.b;
            }
        }
    }
}

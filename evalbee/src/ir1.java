import java.util.logging.Level;
import java.util.logging.Logger;
import com.google.common.base.b;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class ir1
{
    public static String a(final String s) {
        return b.b(s);
    }
    
    public static boolean b(final String s) {
        return b.f(s);
    }
    
    public static String c(final String obj, final Object... array) {
        final String value = String.valueOf(obj);
        final int n = 0;
        Object[] array2;
        if (array == null) {
            array2 = new Object[] { "(Object[])null" };
        }
        else {
            int n2 = 0;
            while (true) {
                array2 = array;
                if (n2 >= array.length) {
                    break;
                }
                array[n2] = d(array[n2]);
                ++n2;
            }
        }
        final StringBuilder sb = new StringBuilder(value.length() + array2.length * 16);
        int start = 0;
        int i;
        for (i = n; i < array2.length; ++i) {
            final int index = value.indexOf("%s", start);
            if (index == -1) {
                break;
            }
            sb.append(value, start, index);
            sb.append(array2[i]);
            start = index + 2;
        }
        sb.append(value, start, value.length());
        if (i < array2.length) {
            sb.append(" [");
            final int n3 = i + 1;
            sb.append(array2[i]);
            for (int j = n3; j < array2.length; ++j) {
                sb.append(", ");
                sb.append(array2[j]);
            }
            sb.append(']');
        }
        return sb.toString();
    }
    
    public static String d(final Object o) {
        if (o == null) {
            return "null";
        }
        try {
            return o.toString();
        }
        catch (final Exception thrown) {
            final String name = o.getClass().getName();
            final String hexString = Integer.toHexString(System.identityHashCode(o));
            final StringBuilder sb = new StringBuilder(name.length() + 1 + String.valueOf(hexString).length());
            sb.append(name);
            sb.append('@');
            sb.append(hexString);
            final String string = sb.toString();
            final Logger logger = Logger.getLogger("com.google.common.base.Strings");
            final Level warning = Level.WARNING;
            final String value = String.valueOf(string);
            String concat;
            if (value.length() != 0) {
                concat = "Exception during lenientFormat for ".concat(value);
            }
            else {
                concat = new String("Exception during lenientFormat for ");
            }
            logger.log(warning, concat, thrown);
            final String name2 = thrown.getClass().getName();
            final StringBuilder sb2 = new StringBuilder(String.valueOf(string).length() + 9 + name2.length());
            sb2.append("<");
            sb2.append(string);
            sb2.append(" threw ");
            sb2.append(name2);
            sb2.append(">");
            return sb2.toString();
        }
    }
}

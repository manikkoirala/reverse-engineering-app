import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.GZIPOutputStream;
import java.io.ByteArrayOutputStream;
import com.google.firebase.crashlytics.internal.model.CrashlyticsReport;

// 
// Decompiled by Procyon v0.6.0
// 

public class td implements ey0
{
    public final byte[] a;
    public final String b;
    public final String c;
    
    public td(final String b, final String c, final byte[] a) {
        this.b = b;
        this.c = c;
        this.a = a;
    }
    
    @Override
    public String a() {
        return this.c;
    }
    
    @Override
    public CrashlyticsReport.d.b b() {
        final byte[] c = this.c();
        Object a;
        if (c == null) {
            a = null;
        }
        else {
            a = CrashlyticsReport.d.b.a().b(c).c(this.b).a();
        }
        return (CrashlyticsReport.d.b)a;
    }
    
    public final byte[] c() {
        if (this.d()) {
            return null;
        }
        try {
            final ByteArrayOutputStream out = new ByteArrayOutputStream();
            try {
                final GZIPOutputStream gzipOutputStream = new GZIPOutputStream(out);
                try {
                    gzipOutputStream.write(this.a);
                    gzipOutputStream.finish();
                    final byte[] byteArray = out.toByteArray();
                    gzipOutputStream.close();
                    out.close();
                    return byteArray;
                }
                finally {
                    try {
                        gzipOutputStream.close();
                    }
                    finally {
                        final Throwable t;
                        final Throwable exception;
                        t.addSuppressed(exception);
                    }
                }
            }
            finally {
                try {
                    out.close();
                }
                finally {
                    final Throwable t2;
                    final Throwable exception2;
                    t2.addSuppressed(exception2);
                }
            }
        }
        catch (final IOException ex) {
            return null;
        }
    }
    
    public final boolean d() {
        final byte[] a = this.a;
        return a == null || a.length == 0;
    }
    
    @Override
    public InputStream getStream() {
        InputStream inputStream;
        if (this.d()) {
            inputStream = null;
        }
        else {
            inputStream = new ByteArrayInputStream(this.a);
        }
        return inputStream;
    }
}

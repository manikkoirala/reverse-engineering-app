import android.graphics.Shader$TileMode;
import java.util.List;
import java.util.ArrayList;
import android.content.res.TypedArray;
import android.graphics.RadialGradient;
import android.graphics.SweepGradient;
import android.graphics.LinearGradient;
import org.xmlpull.v1.XmlPullParserException;
import android.graphics.Shader;
import android.content.res.Resources$Theme;
import android.util.AttributeSet;
import org.xmlpull.v1.XmlPullParser;
import android.content.res.Resources;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class ob0
{
    public static a a(final a a, final int n, final int n2, final boolean b, final int n3) {
        if (a != null) {
            return a;
        }
        if (b) {
            return new a(n, n3, n2);
        }
        return new a(n, n2);
    }
    
    public static Shader b(final Resources resources, final XmlPullParser xmlPullParser, final AttributeSet set, final Resources$Theme resources$Theme) {
        final String name = xmlPullParser.getName();
        if (!name.equals("gradient")) {
            final StringBuilder sb = new StringBuilder();
            sb.append(xmlPullParser.getPositionDescription());
            sb.append(": invalid gradient color tag ");
            sb.append(name);
            throw new XmlPullParserException(sb.toString());
        }
        final TypedArray i = rz1.i(resources, resources$Theme, set, xb1.A);
        final float f = rz1.f(i, xmlPullParser, "startX", xb1.J, 0.0f);
        final float f2 = rz1.f(i, xmlPullParser, "startY", xb1.K, 0.0f);
        final float f3 = rz1.f(i, xmlPullParser, "endX", xb1.L, 0.0f);
        final float f4 = rz1.f(i, xmlPullParser, "endY", xb1.M, 0.0f);
        final float f5 = rz1.f(i, xmlPullParser, "centerX", xb1.E, 0.0f);
        final float f6 = rz1.f(i, xmlPullParser, "centerY", xb1.F, 0.0f);
        final int g = rz1.g(i, xmlPullParser, "type", xb1.D, 0);
        final int b = rz1.b(i, xmlPullParser, "startColor", xb1.B, 0);
        final boolean h = rz1.h(xmlPullParser, "centerColor");
        final int b2 = rz1.b(i, xmlPullParser, "centerColor", xb1.I, 0);
        final int b3 = rz1.b(i, xmlPullParser, "endColor", xb1.C, 0);
        final int g2 = rz1.g(i, xmlPullParser, "tileMode", xb1.H, 0);
        final float f7 = rz1.f(i, xmlPullParser, "gradientRadius", xb1.G, 0.0f);
        i.recycle();
        final a a = a(c(resources, xmlPullParser, set, resources$Theme), b, b3, h, b2);
        if (g != 1) {
            if (g != 2) {
                return (Shader)new LinearGradient(f, f2, f3, f4, a.a, a.b, d(g2));
            }
            return (Shader)new SweepGradient(f5, f6, a.a, a.b);
        }
        else {
            if (f7 > 0.0f) {
                return (Shader)new RadialGradient(f5, f6, f7, a.a, a.b, d(g2));
            }
            throw new XmlPullParserException("<gradient> tag requires 'gradientRadius' attribute with radial type");
        }
    }
    
    public static a c(final Resources resources, final XmlPullParser xmlPullParser, final AttributeSet set, final Resources$Theme resources$Theme) {
        final int n = xmlPullParser.getDepth() + 1;
        final ArrayList list = new ArrayList(20);
        final ArrayList list2 = new ArrayList(20);
        while (true) {
            final int next = xmlPullParser.next();
            if (next == 1) {
                break;
            }
            final int depth = xmlPullParser.getDepth();
            if (depth < n && next == 3) {
                break;
            }
            if (next != 2) {
                continue;
            }
            if (depth > n) {
                continue;
            }
            if (!xmlPullParser.getName().equals("item")) {
                continue;
            }
            final TypedArray i = rz1.i(resources, resources$Theme, set, xb1.N);
            final int o = xb1.O;
            final boolean hasValue = i.hasValue(o);
            final int p4 = xb1.P;
            final boolean hasValue2 = i.hasValue(p4);
            if (!hasValue || !hasValue2) {
                final StringBuilder sb = new StringBuilder();
                sb.append(xmlPullParser.getPositionDescription());
                sb.append(": <item> tag requires a 'color' attribute and a 'offset' attribute!");
                throw new XmlPullParserException(sb.toString());
            }
            final int color = i.getColor(o, 0);
            final float float1 = i.getFloat(p4, 0.0f);
            i.recycle();
            list2.add(color);
            list.add(float1);
        }
        if (list2.size() > 0) {
            return new a(list2, list);
        }
        return null;
    }
    
    public static Shader$TileMode d(final int n) {
        if (n == 1) {
            return Shader$TileMode.REPEAT;
        }
        if (n != 2) {
            return Shader$TileMode.CLAMP;
        }
        return Shader$TileMode.MIRROR;
    }
    
    public static final class a
    {
        public final int[] a;
        public final float[] b;
        
        public a(final int n, final int n2) {
            this.a = new int[] { n, n2 };
            this.b = new float[] { 0.0f, 1.0f };
        }
        
        public a(final int n, final int n2, final int n3) {
            this.a = new int[] { n, n2, n3 };
            this.b = new float[] { 0.0f, 0.5f, 1.0f };
        }
        
        public a(final List list, final List list2) {
            final int size = list.size();
            this.a = new int[size];
            this.b = new float[size];
            for (int i = 0; i < size; ++i) {
                this.a[i] = (int)list.get(i);
                this.b[i] = (float)list2.get(i);
            }
        }
    }
}

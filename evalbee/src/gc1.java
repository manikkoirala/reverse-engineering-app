import com.google.common.collect.Range;
import java.util.Set;

// 
// Decompiled by Procyon v0.6.0
// 

public interface gc1
{
    Set asRanges();
    
    gc1 complement();
    
    boolean encloses(final Range p0);
    
    boolean isEmpty();
    
    void removeAll(final gc1 p0);
}

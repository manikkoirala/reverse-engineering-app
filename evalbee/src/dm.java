import androidx.appcompat.app.a;
import android.app.Dialog;
import android.widget.TextView;
import android.widget.Button;
import android.view.View$OnFocusChangeListener;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemClickListener;
import android.os.Bundle;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import androidx.appcompat.widget.Toolbar;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import com.ekodroid.omrevaluator.database.repositories.ResultRepository;
import com.ekodroid.omrevaluator.database.TemplateDataJsonModel;
import android.widget.ListAdapter;
import com.ekodroid.omrevaluator.database.repositories.ClassRepository;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import android.app.DatePickerDialog;
import android.widget.DatePicker;
import android.app.DatePickerDialog$OnDateSetListener;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import android.view.View;
import android.view.View$OnClickListener;
import java.util.ArrayList;
import android.widget.AutoCompleteTextView;
import com.google.android.material.textfield.TextInputLayout;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class dm extends u80
{
    public Context a;
    public y01 b;
    public ExamId c;
    public TextInputLayout d;
    public AutoCompleteTextView e;
    public TextInputLayout f;
    public ArrayList g;
    public View$OnClickListener h;
    
    public dm(final Context a, final ExamId c, final y01 b) {
        super(a);
        this.h = (View$OnClickListener)new View$OnClickListener(this) {
            public final dm a;
            
            public void onClick(final View view) {
                a91.B(this.a.a, view);
                final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                final Calendar instance = Calendar.getInstance();
                ((Dialog)new DatePickerDialog(this.a.a, (DatePickerDialog$OnDateSetListener)new DatePickerDialog$OnDateSetListener(this, instance, simpleDateFormat) {
                    public final Calendar a;
                    public final SimpleDateFormat b;
                    public final dm$a c;
                    
                    public void onDateSet(final DatePicker datePicker, final int year, final int month, final int date) {
                        this.a.set(year, month, date);
                        ((TextView)this.c.a.d.getEditText()).setText((CharSequence)this.b.format(this.a.getTime()));
                    }
                }, instance.get(1), instance.get(2), instance.get(5))).show();
            }
        };
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public static /* synthetic */ ArrayList a(final dm dm) {
        return dm.g;
    }
    
    public static /* synthetic */ AutoCompleteTextView b(final dm dm) {
        return dm.e;
    }
    
    public static /* synthetic */ ExamId f(final dm dm) {
        return dm.c;
    }
    
    public static /* synthetic */ y01 h(final dm dm) {
        return dm.b;
    }
    
    public final boolean i(final ExamId examId) {
        return TemplateRepository.getInstance(this.a).getTemplateJson(examId) != null;
    }
    
    public final void j() {
        (this.g = ClassRepository.getInstance(this.a).getAllClassNames()).add(this.a.getString(2131886120));
        this.e.setAdapter((ListAdapter)new h3(this.a, this.g));
    }
    
    public final boolean k(final ExamId examId, final ExamId examId2) {
        final TemplateRepository instance = TemplateRepository.getInstance(this.a);
        final SheetTemplate2 sheetTemplate = TemplateRepository.getInstance(this.a).getTemplateJson(examId2).getSheetTemplate();
        sheetTemplate.setName(examId.getExamName());
        final TemplateDataJsonModel templateDataJsonModel = new TemplateDataJsonModel(examId.getExamName(), examId.getClassName(), examId.getExamDate(), sheetTemplate, b10.a(this.a, examId.getExamDate()), null);
        final TemplateDataJsonModel templateJson = instance.getTemplateJson(examId);
        instance.deleteTemplateJson(examId);
        ResultRepository.getInstance(this.a).deleteAllResultForExam(examId);
        if (templateJson != null) {
            ResultRepository.getInstance(this.a).deleteAllSheetImageForExam(examId, templateJson.getFolderPath());
        }
        return instance.saveOrUpdateTemplateJson(templateDataJsonModel);
    }
    
    public final void l() {
        final Toolbar toolbar = (Toolbar)this.findViewById(2131297357);
        toolbar.setTitle(2131886206);
        toolbar.setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final dm a;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
    }
    
    public final void m(final ExamId examId) {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(this.a, 2131951953);
        final StringBuilder sb = new StringBuilder();
        sb.append(examId.getExamName());
        sb.append(" ");
        sb.append(this.a.getResources().getString(2131886534));
        materialAlertDialogBuilder.setMessage((CharSequence)sb.toString()).setPositiveButton(2131886903, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this, examId) {
            public final ExamId a;
            public final dm b;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                final dm b = this.b;
                if (b.k(this.a, dm.f(b))) {
                    a91.G(this.b.a, 2131886292, 2131230927, 2131231085);
                    dm.h(this.b).a(null);
                }
                dialogInterface.dismiss();
            }
        }).setNegativeButton(2131886657, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final dm a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                dialogInterface.dismiss();
            }
        }).create();
        ((a.a)materialAlertDialogBuilder).show();
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492987);
        this.l();
        final TextInputLayout textInputLayout = (TextInputLayout)this.findViewById(2131297175);
        this.f = (TextInputLayout)this.findViewById(2131297172);
        this.d = (TextInputLayout)this.findViewById(2131297174);
        this.e = (AutoCompleteTextView)this.f.getEditText();
        this.j();
        this.e.setOnItemClickListener((AdapterView$OnItemClickListener)new AdapterView$OnItemClickListener(this) {
            public final dm a;
            
            public void onItemClick(final AdapterView adapterView, final View view, final int index, final long n) {
                if (dm.a(this.a).get(index).equals(this.a.a.getString(2131886120))) {
                    ((TextView)dm.b(this.a)).setText((CharSequence)null);
                    new t3(this.a.a, new y01(this) {
                        public final dm$c a;
                        
                        @Override
                        public void a(final Object o) {
                            a91.G(this.a.a.a, 2131886653, 2131230927, 2131231085);
                            this.a.a.j();
                            dm.b(this.a.a).showDropDown();
                        }
                    });
                }
            }
        });
        ((View)this.d.getEditText()).setOnFocusChangeListener((View$OnFocusChangeListener)new View$OnFocusChangeListener(this) {
            public final dm a;
            
            public void onFocusChange(final View view, final boolean b) {
                if (b) {
                    a91.B(this.a.a, view);
                }
            }
        });
        ((TextView)this.d.getEditText()).setInputType(0);
        ((View)this.d.getEditText()).setOnClickListener(this.h);
        this.d.setEndIconOnClickListener(this.h);
        ((View)this.findViewById(2131296462)).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, textInputLayout) {
            public final TextInputLayout a;
            public final dm b;
            
            public void onClick(final View view) {
                final String string = ((TextView)dm.b(this.b)).getEditableText().toString();
                final String string2 = this.a.getEditText().getText().toString();
                final String string3 = this.b.d.getEditText().getText().toString();
                if (string.trim().equals("")) {
                    a91.G(this.b.a, 2131886262, 2131230909, 2131231086);
                    return;
                }
                if (string2.trim().equals("")) {
                    a91.G(this.b.a, 2131886264, 2131230909, 2131231086);
                    return;
                }
                if (string3.trim().equals("")) {
                    a91.G(this.b.a, 2131886263, 2131230909, 2131231086);
                    return;
                }
                final ExamId examId = new ExamId(string2, string, string3);
                if (this.b.i(examId)) {
                    this.b.m(examId);
                }
                else {
                    final dm b = this.b;
                    if (!b.k(examId, dm.f(b))) {
                        return;
                    }
                    dm.h(this.b).a(string);
                }
                this.b.dismiss();
            }
        });
        this.findViewById(2131296392).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final dm a;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
    }
}

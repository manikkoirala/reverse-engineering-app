import java.util.concurrent.TimeoutException;
import java.util.concurrent.CancellationException;
import java.util.concurrent.TimeUnit;
import android.os.Looper;
import com.google.android.gms.tasks.Continuation;
import java.util.concurrent.Executor;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import java.util.concurrent.ExecutorService;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class v22
{
    public static final ExecutorService a;
    
    static {
        a = vy.c("awaitEvenIfOnMainThread task continuation executor");
    }
    
    public static Object f(final Task task) {
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        task.continueWith((Executor)v22.a, (Continuation)new p22(countDownLatch));
        long timeout;
        if (Looper.getMainLooper() == Looper.myLooper()) {
            timeout = 3L;
        }
        else {
            timeout = 4L;
        }
        countDownLatch.await(timeout, TimeUnit.SECONDS);
        if (task.isSuccessful()) {
            return task.getResult();
        }
        if (task.isCanceled()) {
            throw new CancellationException("Task is already canceled");
        }
        if (task.isComplete()) {
            throw new IllegalStateException(task.getException());
        }
        throw new TimeoutException();
    }
    
    public static boolean g(final CountDownLatch countDownLatch, long nanoTime, final TimeUnit timeUnit) {
        final boolean b = false;
        int n = 0;
        int n2 = b ? 1 : 0;
        try {
            final long nanos = timeUnit.toNanos(nanoTime);
            n2 = (b ? 1 : 0);
            final long nanoTime2 = System.nanoTime();
            nanoTime = nanos;
            while (true) {
                n2 = n;
                try {
                    return countDownLatch.await(nanoTime, TimeUnit.NANOSECONDS);
                }
                catch (final InterruptedException ex) {
                    n2 = 1;
                    n = 1;
                    nanoTime = System.nanoTime();
                    nanoTime = nanoTime2 + nanos - nanoTime;
                }
            }
        }
        finally {
            if (n2 != 0) {
                Thread.currentThread().interrupt();
            }
        }
    }
    
    public static Task h(final Executor executor, final Callable callable) {
        final TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        executor.execute(new r22(callable, executor, taskCompletionSource));
        return taskCompletionSource.getTask();
    }
    
    public static Task n(final Task task, final Task task2) {
        final TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        final t22 t22 = new t22(taskCompletionSource);
        task.continueWith((Continuation)t22);
        task2.continueWith((Continuation)t22);
        return taskCompletionSource.getTask();
    }
    
    public static Task o(final Executor executor, final Task task, final Task task2) {
        final TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        final q22 q22 = new q22(taskCompletionSource);
        task.continueWith(executor, (Continuation)q22);
        task2.continueWith(executor, (Continuation)q22);
        return taskCompletionSource.getTask();
    }
}

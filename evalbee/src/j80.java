import android.os.Parcelable;
import android.view.View;
import androidx.lifecycle.Lifecycle;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.q;
import androidx.fragment.app.k;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class j80 extends u21
{
    public final k c;
    public final int d;
    public q e;
    public Fragment f;
    public boolean g;
    
    public j80(final k k) {
        this(k, 0);
    }
    
    public j80(final k c, final int d) {
        this.e = null;
        this.f = null;
        this.c = c;
        this.d = d;
    }
    
    public static String r(final int i, final long lng) {
        final StringBuilder sb = new StringBuilder();
        sb.append("android:switcher:");
        sb.append(i);
        sb.append(":");
        sb.append(lng);
        return sb.toString();
    }
    
    @Override
    public void a(final ViewGroup viewGroup, final int n, final Object o) {
        final Fragment fragment = (Fragment)o;
        if (this.e == null) {
            this.e = this.c.m();
        }
        this.e.k(fragment);
        if (fragment.equals(this.f)) {
            this.f = null;
        }
    }
    
    @Override
    public void b(final ViewGroup viewGroup) {
        final q e = this.e;
        if (e != null) {
            if (!this.g) {
                try {
                    this.g = true;
                    e.j();
                }
                finally {
                    this.g = false;
                }
            }
            this.e = null;
        }
    }
    
    @Override
    public Object g(final ViewGroup viewGroup, final int n) {
        if (this.e == null) {
            this.e = this.c.m();
        }
        final long q = this.q(n);
        final Fragment i0 = this.c.i0(r(((View)viewGroup).getId(), q));
        Fragment fragment;
        if (i0 != null) {
            this.e.f(i0);
            fragment = i0;
        }
        else {
            final Fragment p2 = this.p(n);
            this.e.b(((View)viewGroup).getId(), p2, r(((View)viewGroup).getId(), q));
            fragment = p2;
        }
        if (fragment != this.f) {
            fragment.setMenuVisibility(false);
            if (this.d == 1) {
                this.e.q(fragment, Lifecycle.State.STARTED);
            }
            else {
                fragment.setUserVisibleHint(false);
            }
        }
        return fragment;
    }
    
    @Override
    public boolean h(final View view, final Object o) {
        return ((Fragment)o).getView() == view;
    }
    
    @Override
    public void j(final Parcelable parcelable, final ClassLoader classLoader) {
    }
    
    @Override
    public Parcelable k() {
        return null;
    }
    
    @Override
    public void l(final ViewGroup viewGroup, final int n, final Object o) {
        final Fragment f = (Fragment)o;
        final Fragment f2 = this.f;
        if (f != f2) {
            if (f2 != null) {
                f2.setMenuVisibility(false);
                if (this.d == 1) {
                    if (this.e == null) {
                        this.e = this.c.m();
                    }
                    this.e.q(this.f, Lifecycle.State.STARTED);
                }
                else {
                    this.f.setUserVisibleHint(false);
                }
            }
            f.setMenuVisibility(true);
            if (this.d == 1) {
                if (this.e == null) {
                    this.e = this.c.m();
                }
                this.e.q(f, Lifecycle.State.RESUMED);
            }
            else {
                f.setUserVisibleHint(true);
            }
            this.f = f;
        }
    }
    
    @Override
    public void n(final ViewGroup viewGroup) {
        if (((View)viewGroup).getId() != -1) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("ViewPager with adapter ");
        sb.append(this);
        sb.append(" requires a view id");
        throw new IllegalStateException(sb.toString());
    }
    
    public abstract Fragment p(final int p0);
    
    public long q(final int n) {
        return n;
    }
}

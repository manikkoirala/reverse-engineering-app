import android.os.Bundle;
import android.content.res.AssetFileDescriptor;
import java.io.InputStream;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.graphics.Movie;
import android.content.res.Resources$Theme;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.content.res.Configuration;
import android.content.res.ColorStateList;
import android.content.res.XmlResourceParser;
import android.content.res.Resources;
import android.content.Context;
import java.lang.ref.WeakReference;

// 
// Decompiled by Procyon v0.6.0
// 

public class f32 extends qe1
{
    public static boolean c = false;
    public final WeakReference b;
    
    public f32(final Context referent, final Resources resources) {
        super(resources);
        this.b = new WeakReference((T)referent);
    }
    
    public static boolean b() {
        return f32.c;
    }
    
    public static boolean c() {
        b();
        return false;
    }
    
    public Drawable getDrawable(final int n) {
        final Context context = (Context)this.b.get();
        if (context != null) {
            return je1.g().s(context, this, n);
        }
        return this.a(n);
    }
}

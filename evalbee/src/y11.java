import android.view.ViewGroup$MarginLayoutParams;
import com.ekodroid.omrevaluator.templateui.models.Point2Double;
import com.ekodroid.omrevaluator.templateui.models.AnswerOption;
import com.ekodroid.omrevaluator.templateui.models.MatrixOptionPayload;
import com.ekodroid.omrevaluator.templateui.models.NumericalOptionPayload;
import com.ekodroid.omrevaluator.templateui.models.DecimalOptionPayload;
import android.view.ViewGroup;
import android.widget.ImageView$ScaleType;
import com.google.android.flexbox.FlexboxLayout;
import android.view.ViewGroup$LayoutParams;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout$LayoutParams;
import android.widget.LinearLayout;
import android.graphics.Bitmap;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import java.util.ArrayList;
import com.ekodroid.omrevaluator.templateui.scanner.AnswerValue;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class y11
{
    public Context a;
    public AnswerValue b;
    public ArrayList c;
    public int d;
    public int e;
    
    public y11(final Context a, final AnswerValue b, final SheetTemplate2 sheetTemplate2, final sn1 sn1, final Bitmap bitmap) {
        this.c = new ArrayList();
        this.d = 32;
        this.e = 40;
        this.a(this.a = a, this.b = b, sheetTemplate2, sn1, bitmap);
    }
    
    public final void a(final Context context, final AnswerValue answerValue, final SheetTemplate2 sheetTemplate2, final sn1 sn1, final Bitmap bitmap) {
        switch (y11$a.a[answerValue.getType().ordinal()]) {
            case 7: {
                this.b(context, answerValue, sheetTemplate2, sn1, bitmap);
                break;
            }
            case 6: {
                this.e(context, answerValue, sheetTemplate2, sn1, bitmap);
                break;
            }
            case 5: {
                this.d(context, answerValue, sheetTemplate2, sn1, bitmap);
                break;
            }
            case 1:
            case 2:
            case 3:
            case 4:
            case 8:
            case 9:
            case 10: {
                this.f(context, answerValue, sheetTemplate2, sn1, bitmap);
                break;
            }
        }
    }
    
    public final void b(final Context context, final AnswerValue answerValue, final SheetTemplate2 sheetTemplate2, final sn1 sn1, final Bitmap bitmap) {
        final DecimalOptionPayload b = bc.b(answerValue.getPayload());
        final Bitmap[] h = this.h(answerValue, sheetTemplate2, sn1, bitmap);
        final int g = this.g(8, context);
        final int g2 = this.g(this.d, context);
        int n;
        if (b.decimalAllowed) {
            n = 11;
        }
        else {
            n = 10;
        }
        final int digits = b.digits;
        if (b.hasNegetive) {
            final LinearLayout e = new LinearLayout(context);
            ((ViewGroup$MarginLayoutParams)new LinearLayout$LayoutParams(g2, g2)).setMargins(g, g, g, g);
            final LinearLayout$LayoutParams linearLayout$LayoutParams = new LinearLayout$LayoutParams(this.g(this.e, context), g2);
            ((ViewGroup$MarginLayoutParams)linearLayout$LayoutParams).setMargins(g, g, g, g);
            ((ViewGroup)e).addView((View)new ImageView(context), (ViewGroup$LayoutParams)linearLayout$LayoutParams);
            final FlexboxLayout flexboxLayout = new FlexboxLayout(context);
            flexboxLayout.setFlexDirection(0);
            flexboxLayout.setFlexWrap(1);
            final FlexboxLayout.LayoutParams layoutParams = new FlexboxLayout.LayoutParams(g2, g2);
            layoutParams.setMargins(g, g, g, g);
            final ImageView imageView = new ImageView(context);
            imageView.setScaleType(ImageView$ScaleType.FIT_XY);
            imageView.setAdjustViewBounds(true);
            imageView.setImageBitmap(h[h.length - 1]);
            flexboxLayout.addView((View)imageView, (ViewGroup$LayoutParams)layoutParams);
            final LinearLayout$LayoutParams linearLayout$LayoutParams2 = new LinearLayout$LayoutParams(0, -2);
            linearLayout$LayoutParams2.weight = 1.0f;
            ((ViewGroup)e).addView((View)flexboxLayout, (ViewGroup$LayoutParams)linearLayout$LayoutParams2);
            this.c.add(e);
        }
        for (int i = 0; i < n; ++i) {
            final LinearLayout e2 = new LinearLayout(context);
            ((ViewGroup$MarginLayoutParams)new LinearLayout$LayoutParams(g2, g2)).setMargins(g, g, g, g);
            final LinearLayout$LayoutParams linearLayout$LayoutParams3 = new LinearLayout$LayoutParams(this.g(this.e, context), g2);
            ((ViewGroup$MarginLayoutParams)linearLayout$LayoutParams3).setMargins(g, g, g, g);
            ((ViewGroup)e2).addView((View)new ImageView(context), (ViewGroup$LayoutParams)linearLayout$LayoutParams3);
            final FlexboxLayout flexboxLayout2 = new FlexboxLayout(context);
            flexboxLayout2.setFlexDirection(0);
            flexboxLayout2.setFlexWrap(1);
            final FlexboxLayout.LayoutParams layoutParams2 = new FlexboxLayout.LayoutParams(g2, g2);
            layoutParams2.setMargins(g, g, g, g);
            for (int j = 0; j < digits; ++j) {
                final ImageView imageView2 = new ImageView(context);
                imageView2.setScaleType(ImageView$ScaleType.FIT_XY);
                imageView2.setAdjustViewBounds(true);
                imageView2.setImageBitmap(h[i * digits + j]);
                flexboxLayout2.addView((View)imageView2, (ViewGroup$LayoutParams)layoutParams2);
            }
            final LinearLayout$LayoutParams linearLayout$LayoutParams4 = new LinearLayout$LayoutParams(0, -2);
            linearLayout$LayoutParams4.weight = 1.0f;
            ((ViewGroup)e2).addView((View)flexboxLayout2, (ViewGroup$LayoutParams)linearLayout$LayoutParams4);
            this.c.add(e2);
        }
    }
    
    public final void c(final Context context, final AnswerValue answerValue, final SheetTemplate2 sheetTemplate2, final sn1 sn1, final Bitmap bitmap) {
        final NumericalOptionPayload d = bc.d(answerValue.getPayload());
        final Bitmap[] h = this.h(answerValue, sheetTemplate2, sn1, bitmap);
        final LinearLayout$LayoutParams linearLayout$LayoutParams = new LinearLayout$LayoutParams(0, -1);
        linearLayout$LayoutParams.rightMargin = this.g(16, context);
        linearLayout$LayoutParams.weight = 1.0f;
        final int g = this.g(8, context);
        if (d.hasNegetive) {
            final LinearLayout e = new LinearLayout(context);
            ((ViewGroup)e).addView((View)new ImageView(context), (ViewGroup$LayoutParams)linearLayout$LayoutParams);
            for (int i = 0; i < 5; ++i) {
                final ImageView imageView = new ImageView(context);
                if (i == 1) {
                    imageView.setScaleType(ImageView$ScaleType.FIT_XY);
                    imageView.setAdjustViewBounds(true);
                    imageView.setImageBitmap(h[h.length - 1]);
                }
                ((ViewGroup)e).addView((View)imageView, (ViewGroup$LayoutParams)linearLayout$LayoutParams);
            }
            ((View)e).setPadding(g, g, g, g);
            this.c.add(e);
        }
        final int n = (d.digits - 1) / 5;
        for (int j = 0; j < 10; ++j) {
            for (int k = 0; k < n + 1; ++k) {
                final LinearLayout e2 = new LinearLayout(context);
                ((ViewGroup)e2).addView((View)new ImageView(context), (ViewGroup$LayoutParams)linearLayout$LayoutParams);
                for (int l = 0; l < 5; ++l) {
                    final ImageView imageView2 = new ImageView(context);
                    final int n2 = k * 5 + l;
                    if (n2 < d.digits) {
                        imageView2.setScaleType(ImageView$ScaleType.FIT_XY);
                        imageView2.setAdjustViewBounds(true);
                        imageView2.setImageBitmap(h[d.digits * j + n2]);
                    }
                    ((ViewGroup)e2).addView((View)imageView2, (ViewGroup$LayoutParams)linearLayout$LayoutParams);
                }
                ((View)e2).setPadding(g, g, g, g);
                this.c.add(e2);
            }
        }
    }
    
    public final void d(final Context context, final AnswerValue answerValue, final SheetTemplate2 sheetTemplate2, final sn1 sn1, final Bitmap bitmap) {
        final int g = this.g(8, context);
        final int g2 = this.g(this.d, context);
        final MatrixOptionPayload c = bc.c(answerValue.getPayload());
        int primaryOptions;
        int secondaryOptions;
        if (c != null) {
            primaryOptions = c.primaryOptions;
            secondaryOptions = c.secondaryOptions;
        }
        else {
            primaryOptions = 4;
            secondaryOptions = 5;
        }
        final Bitmap[] h = this.h(answerValue, sheetTemplate2, sn1, bitmap);
        for (int i = 0; i < primaryOptions; ++i) {
            final LinearLayout e = new LinearLayout(context);
            ((ViewGroup$MarginLayoutParams)new LinearLayout$LayoutParams(g2, g2)).setMargins(g, g, g, g);
            final LinearLayout$LayoutParams linearLayout$LayoutParams = new LinearLayout$LayoutParams(this.g(this.e, context), g2);
            ((ViewGroup$MarginLayoutParams)linearLayout$LayoutParams).setMargins(g, g, g, g);
            ((ViewGroup)e).addView((View)new ImageView(context), (ViewGroup$LayoutParams)linearLayout$LayoutParams);
            final FlexboxLayout flexboxLayout = new FlexboxLayout(context);
            flexboxLayout.setFlexDirection(0);
            flexboxLayout.setFlexWrap(1);
            final FlexboxLayout.LayoutParams layoutParams = new FlexboxLayout.LayoutParams(g2, g2);
            layoutParams.setMargins(g, g, g, g);
            for (int j = 0; j < secondaryOptions; ++j) {
                final ImageView imageView = new ImageView(context);
                imageView.setScaleType(ImageView$ScaleType.FIT_XY);
                imageView.setAdjustViewBounds(true);
                imageView.setImageBitmap(h[i * secondaryOptions + j]);
                flexboxLayout.addView((View)imageView, (ViewGroup$LayoutParams)layoutParams);
            }
            final LinearLayout$LayoutParams linearLayout$LayoutParams2 = new LinearLayout$LayoutParams(0, -2);
            linearLayout$LayoutParams2.weight = 1.0f;
            ((ViewGroup)e).addView((View)flexboxLayout, (ViewGroup$LayoutParams)linearLayout$LayoutParams2);
            this.c.add(e);
        }
    }
    
    public final void e(final Context context, final AnswerValue answerValue, final SheetTemplate2 sheetTemplate2, final sn1 sn1, final Bitmap bitmap) {
        final NumericalOptionPayload d = bc.d(answerValue.getPayload());
        if (d != null && (d.hasNegetive || d.digits > 1)) {
            this.c(context, answerValue, sheetTemplate2, sn1, bitmap);
            return;
        }
        final Bitmap[] h = this.h(answerValue, sheetTemplate2, sn1, bitmap);
        final LinearLayout$LayoutParams linearLayout$LayoutParams = new LinearLayout$LayoutParams(0, -1);
        linearLayout$LayoutParams.rightMargin = this.g(16, context);
        linearLayout$LayoutParams.weight = 1.0f;
        final int g = this.g(8, context);
        for (int i = 0; i < 2; ++i) {
            final LinearLayout e = new LinearLayout(context);
            ((ViewGroup)e).addView((View)new ImageView(context), (ViewGroup$LayoutParams)linearLayout$LayoutParams);
            for (int j = 0; j < 5; ++j) {
                final ImageView imageView = new ImageView(context);
                imageView.setScaleType(ImageView$ScaleType.FIT_XY);
                imageView.setAdjustViewBounds(true);
                imageView.setImageBitmap(h[i * 5 + j]);
                ((ViewGroup)e).addView((View)imageView, (ViewGroup$LayoutParams)linearLayout$LayoutParams);
            }
            ((View)e).setPadding(g, g, g, g);
            this.c.add(e);
        }
    }
    
    public final void f(final Context context, final AnswerValue answerValue, final SheetTemplate2 sheetTemplate2, final sn1 sn1, final Bitmap bitmap) {
        final Bitmap[] h = this.h(answerValue, sheetTemplate2, sn1, bitmap);
        final int g = this.g(8, context);
        final int g2 = this.g(this.d, context);
        final LinearLayout e = new LinearLayout(context);
        ((ViewGroup$MarginLayoutParams)new LinearLayout$LayoutParams(g2, g2)).setMargins(g, g, g, g);
        final LinearLayout$LayoutParams linearLayout$LayoutParams = new LinearLayout$LayoutParams(this.g(this.e, context), g2);
        ((ViewGroup$MarginLayoutParams)linearLayout$LayoutParams).setMargins(g, g, g, g);
        ((ViewGroup)e).addView((View)new ImageView(context), (ViewGroup$LayoutParams)linearLayout$LayoutParams);
        final FlexboxLayout flexboxLayout = new FlexboxLayout(context);
        flexboxLayout.setFlexDirection(0);
        flexboxLayout.setFlexWrap(1);
        final FlexboxLayout.LayoutParams layoutParams = new FlexboxLayout.LayoutParams(g2, g2);
        layoutParams.setMargins(g, g, g, g);
        for (int i = 0; i < h.length; ++i) {
            final ImageView imageView = new ImageView(context);
            imageView.setScaleType(ImageView$ScaleType.FIT_XY);
            imageView.setAdjustViewBounds(true);
            imageView.setImageBitmap(h[i]);
            flexboxLayout.addView((View)imageView, (ViewGroup$LayoutParams)layoutParams);
        }
        final LinearLayout$LayoutParams linearLayout$LayoutParams2 = new LinearLayout$LayoutParams(0, -2);
        linearLayout$LayoutParams2.weight = 1.0f;
        ((ViewGroup)e).addView((View)flexboxLayout, (ViewGroup$LayoutParams)linearLayout$LayoutParams2);
        this.c.add(e);
    }
    
    public final int g(final int n, final Context context) {
        return (int)(n * context.getResources().getDisplayMetrics().density);
    }
    
    public final Bitmap[] h(final AnswerValue answerValue, final SheetTemplate2 sheetTemplate2, final sn1 sn1, final Bitmap bitmap) {
        final int n = (int)sn1.f().getBubbleSize() * 2;
        final AnswerOption answerOption = sheetTemplate2.getAnswerOptions().get(answerValue.getQuestionNumber());
        int i = 0;
        final Point2Double[] b = sn1.b(answerOption, sheetTemplate2.getColumnWidthInBubbles(0));
        final Bitmap[] array = new Bitmap[b.length];
        while (i < b.length) {
            final Point2Double point2Double = b[i];
            final int n2 = (int)point2Double.x;
            final int n3 = n / 2;
            array[i] = Bitmap.createBitmap(bitmap, n2 - n3, (int)point2Double.y - n3, n, n);
            ++i;
        }
        return array;
    }
    
    public ArrayList i() {
        return this.c;
    }
}

import android.content.Intent;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import android.net.NetworkInfo;
import android.net.ConnectivityManager;
import android.util.Log;
import android.os.PowerManager;
import android.os.PowerManager$WakeLock;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class dy1 implements Runnable
{
    public static final Object f;
    public static Boolean g;
    public static Boolean h;
    public final Context a;
    public final cw0 b;
    public final PowerManager$WakeLock c;
    public final cy1 d;
    public final long e;
    
    static {
        f = new Object();
    }
    
    public dy1(final cy1 d, final Context a, final cw0 b, final long e) {
        this.d = d;
        this.a = a;
        this.e = e;
        this.b = b;
        this.c = ((PowerManager)a.getSystemService("power")).newWakeLock(1, "wake:com.google.firebase.messaging");
    }
    
    public static /* synthetic */ cy1 c(final dy1 dy1) {
        return dy1.d;
    }
    
    public static /* synthetic */ Context d(final dy1 dy1) {
        return dy1.a;
    }
    
    public static String e(final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Missing Permission: ");
        sb.append(str);
        sb.append(". This permission should normally be included by the manifest merger, but may needed to be manually added to your manifest");
        return sb.toString();
    }
    
    public static boolean f(final Context context) {
        synchronized (dy1.f) {
            final Boolean h = dy1.h;
            boolean b;
            if (h == null) {
                b = g(context, "android.permission.ACCESS_NETWORK_STATE", h);
            }
            else {
                b = h;
            }
            return dy1.h = b;
        }
    }
    
    public static boolean g(final Context context, final String s, final Boolean b) {
        if (b != null) {
            return b;
        }
        final boolean b2 = context.checkCallingOrSelfPermission(s) == 0;
        if (!b2 && Log.isLoggable("FirebaseMessaging", 3)) {
            Log.d("FirebaseMessaging", e(s));
        }
        return b2;
    }
    
    public static boolean h(final Context context) {
        synchronized (dy1.f) {
            final Boolean g = dy1.g;
            boolean b;
            if (g == null) {
                b = g(context, "android.permission.WAKE_LOCK", g);
            }
            else {
                b = g;
            }
            return dy1.g = b;
        }
    }
    
    public static boolean j() {
        return Log.isLoggable("FirebaseMessaging", 3);
    }
    
    public final boolean i() {
        synchronized (this) {
            final ConnectivityManager connectivityManager = (ConnectivityManager)this.a.getSystemService("connectivity");
            NetworkInfo activeNetworkInfo;
            if (connectivityManager != null) {
                activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            }
            else {
                activeNetworkInfo = null;
            }
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }
    }
    
    @Override
    public void run() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        dy1.a:Landroid/content/Context;
        //     4: invokestatic    dy1.h:(Landroid/content/Context;)Z
        //     7: ifeq            20
        //    10: aload_0        
        //    11: getfield        dy1.c:Landroid/os/PowerManager$WakeLock;
        //    14: getstatic       com/google/firebase/messaging/a.a:J
        //    17: invokevirtual   android/os/PowerManager$WakeLock.acquire:(J)V
        //    20: aload_0        
        //    21: getfield        dy1.d:Lcy1;
        //    24: iconst_1       
        //    25: invokevirtual   cy1.m:(Z)V
        //    28: aload_0        
        //    29: getfield        dy1.b:Lcw0;
        //    32: invokevirtual   cw0.g:()Z
        //    35: ifne            76
        //    38: aload_0        
        //    39: getfield        dy1.d:Lcy1;
        //    42: iconst_0       
        //    43: invokevirtual   cy1.m:(Z)V
        //    46: aload_0        
        //    47: getfield        dy1.a:Landroid/content/Context;
        //    50: invokestatic    dy1.h:(Landroid/content/Context;)Z
        //    53: ifeq            75
        //    56: aload_0        
        //    57: getfield        dy1.c:Landroid/os/PowerManager$WakeLock;
        //    60: invokevirtual   android/os/PowerManager$WakeLock.release:()V
        //    63: goto            75
        //    66: astore_1       
        //    67: ldc             "FirebaseMessaging"
        //    69: ldc             "TopicsSyncTask's wakelock was already released due to timeout."
        //    71: invokestatic    android/util/Log.i:(Ljava/lang/String;Ljava/lang/String;)I
        //    74: pop            
        //    75: return         
        //    76: aload_0        
        //    77: getfield        dy1.a:Landroid/content/Context;
        //    80: invokestatic    dy1.f:(Landroid/content/Context;)Z
        //    83: ifeq            137
        //    86: aload_0        
        //    87: invokevirtual   dy1.i:()Z
        //    90: ifne            137
        //    93: new             Ldy1$a;
        //    96: astore_1       
        //    97: aload_1        
        //    98: aload_0        
        //    99: aload_0        
        //   100: invokespecial   dy1$a.<init>:(Ldy1;Ldy1;)V
        //   103: aload_1        
        //   104: invokevirtual   dy1$a.a:()V
        //   107: aload_0        
        //   108: getfield        dy1.a:Landroid/content/Context;
        //   111: invokestatic    dy1.h:(Landroid/content/Context;)Z
        //   114: ifeq            136
        //   117: aload_0        
        //   118: getfield        dy1.c:Landroid/os/PowerManager$WakeLock;
        //   121: invokevirtual   android/os/PowerManager$WakeLock.release:()V
        //   124: goto            136
        //   127: astore_1       
        //   128: ldc             "FirebaseMessaging"
        //   130: ldc             "TopicsSyncTask's wakelock was already released due to timeout."
        //   132: invokestatic    android/util/Log.i:(Ljava/lang/String;Ljava/lang/String;)I
        //   135: pop            
        //   136: return         
        //   137: aload_0        
        //   138: getfield        dy1.d:Lcy1;
        //   141: invokevirtual   cy1.p:()Z
        //   144: ifeq            158
        //   147: aload_0        
        //   148: getfield        dy1.d:Lcy1;
        //   151: iconst_0       
        //   152: invokevirtual   cy1.m:(Z)V
        //   155: goto            169
        //   158: aload_0        
        //   159: getfield        dy1.d:Lcy1;
        //   162: aload_0        
        //   163: getfield        dy1.e:J
        //   166: invokevirtual   cy1.q:(J)V
        //   169: aload_0        
        //   170: getfield        dy1.a:Landroid/content/Context;
        //   173: invokestatic    dy1.h:(Landroid/content/Context;)Z
        //   176: ifeq            265
        //   179: aload_0        
        //   180: getfield        dy1.c:Landroid/os/PowerManager$WakeLock;
        //   183: astore_1       
        //   184: goto            249
        //   187: astore_1       
        //   188: goto            266
        //   191: astore_2       
        //   192: new             Ljava/lang/StringBuilder;
        //   195: astore_1       
        //   196: aload_1        
        //   197: invokespecial   java/lang/StringBuilder.<init>:()V
        //   200: aload_1        
        //   201: ldc             "Failed to sync topics. Won't retry sync. "
        //   203: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   206: pop            
        //   207: aload_1        
        //   208: aload_2        
        //   209: invokevirtual   java/lang/Throwable.getMessage:()Ljava/lang/String;
        //   212: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   215: pop            
        //   216: ldc             "FirebaseMessaging"
        //   218: aload_1        
        //   219: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   222: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;)I
        //   225: pop            
        //   226: aload_0        
        //   227: getfield        dy1.d:Lcy1;
        //   230: iconst_0       
        //   231: invokevirtual   cy1.m:(Z)V
        //   234: aload_0        
        //   235: getfield        dy1.a:Landroid/content/Context;
        //   238: invokestatic    dy1.h:(Landroid/content/Context;)Z
        //   241: ifeq            265
        //   244: aload_0        
        //   245: getfield        dy1.c:Landroid/os/PowerManager$WakeLock;
        //   248: astore_1       
        //   249: aload_1        
        //   250: invokevirtual   android/os/PowerManager$WakeLock.release:()V
        //   253: goto            265
        //   256: astore_1       
        //   257: ldc             "FirebaseMessaging"
        //   259: ldc             "TopicsSyncTask's wakelock was already released due to timeout."
        //   261: invokestatic    android/util/Log.i:(Ljava/lang/String;Ljava/lang/String;)I
        //   264: pop            
        //   265: return         
        //   266: aload_0        
        //   267: getfield        dy1.a:Landroid/content/Context;
        //   270: invokestatic    dy1.h:(Landroid/content/Context;)Z
        //   273: ifeq            295
        //   276: aload_0        
        //   277: getfield        dy1.c:Landroid/os/PowerManager$WakeLock;
        //   280: invokevirtual   android/os/PowerManager$WakeLock.release:()V
        //   283: goto            295
        //   286: astore_2       
        //   287: ldc             "FirebaseMessaging"
        //   289: ldc             "TopicsSyncTask's wakelock was already released due to timeout."
        //   291: invokestatic    android/util/Log.i:(Ljava/lang/String;Ljava/lang/String;)I
        //   294: pop            
        //   295: aload_1        
        //   296: athrow         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                        
        //  -----  -----  -----  -----  ----------------------------
        //  20     46     191    249    Ljava/io/IOException;
        //  20     46     187    297    Any
        //  56     63     66     75     Ljava/lang/RuntimeException;
        //  76     107    191    249    Ljava/io/IOException;
        //  76     107    187    297    Any
        //  117    124    127    136    Ljava/lang/RuntimeException;
        //  137    155    191    249    Ljava/io/IOException;
        //  137    155    187    297    Any
        //  158    169    191    249    Ljava/io/IOException;
        //  158    169    187    297    Any
        //  179    184    256    265    Ljava/lang/RuntimeException;
        //  192    234    187    297    Any
        //  244    249    256    265    Ljava/lang/RuntimeException;
        //  249    253    256    265    Ljava/lang/RuntimeException;
        //  276    283    286    295    Ljava/lang/RuntimeException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 141 out of bounds for length 141
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:248)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:372)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:459)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public class a extends BroadcastReceiver
    {
        public dy1 a;
        public final dy1 b;
        
        public a(final dy1 b, final dy1 a) {
            this.b = b;
            this.a = a;
        }
        
        public void a() {
            if (j()) {
                Log.d("FirebaseMessaging", "Connectivity change received registered");
            }
            dy1.d(this.b).registerReceiver((BroadcastReceiver)this, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }
        
        public void onReceive(final Context context, final Intent intent) {
            synchronized (this) {
                final dy1 a = this.a;
                if (a == null) {
                    return;
                }
                if (!a.i()) {
                    return;
                }
                if (j()) {
                    Log.d("FirebaseMessaging", "Connectivity changed. Starting background sync.");
                }
                dy1.c(this.a).l(this.a, 0L);
                context.unregisterReceiver((BroadcastReceiver)this);
                this.a = null;
            }
        }
    }
}

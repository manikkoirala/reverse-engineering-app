// 
// Decompiled by Procyon v0.6.0
// 

public final class qo1 implements Comparable
{
    public static final qo1 b;
    public final pw1 a;
    
    static {
        b = new qo1(new pw1(0L, 0));
    }
    
    public qo1(final pw1 a) {
        this.a = a;
    }
    
    public int a(final qo1 qo1) {
        return this.a.c(qo1.a);
    }
    
    public pw1 c() {
        return this.a;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (!(o instanceof qo1)) {
            return false;
        }
        if (this.a((qo1)o) != 0) {
            b = false;
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        return this.c().hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SnapshotVersion(seconds=");
        sb.append(this.a.e());
        sb.append(", nanos=");
        sb.append(this.a.d());
        sb.append(")");
        return sb.toString();
    }
}

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewOverlay;

// 
// Decompiled by Procyon v0.6.0
// 

public class d42 implements e42
{
    public final ViewOverlay a;
    
    public d42(final View view) {
        this.a = view.getOverlay();
    }
    
    @Override
    public void add(final Drawable drawable) {
        this.a.add(drawable);
    }
    
    @Override
    public void remove(final Drawable drawable) {
        this.a.remove(drawable);
    }
}

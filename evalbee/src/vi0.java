import android.util.AttributeSet;
import java.lang.ref.WeakReference;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParser;
import java.util.ArrayDeque;
import java.util.Deque;

// 
// Decompiled by Procyon v0.6.0
// 

public class vi0
{
    public final Deque a;
    
    public vi0() {
        this.a = new ArrayDeque();
    }
    
    public static boolean b(final XmlPullParser xmlPullParser) {
        boolean b2;
        final boolean b = b2 = true;
        if (xmlPullParser == null) {
            return b2;
        }
        b2 = b;
        try {
            if (xmlPullParser.getEventType() != 3) {
                b2 = (xmlPullParser.getEventType() == 1 && b);
            }
            return b2;
        }
        catch (final XmlPullParserException ex) {
            b2 = b;
            return b2;
        }
    }
    
    public static XmlPullParser c(final Deque deque) {
        while (!deque.isEmpty()) {
            final XmlPullParser xmlPullParser = deque.peek().get();
            if (!b(xmlPullParser)) {
                return xmlPullParser;
            }
            deque.pop();
        }
        return null;
    }
    
    public static boolean d(final XmlPullParser xmlPullParser, final XmlPullParser xmlPullParser2) {
        if (xmlPullParser2 == null || xmlPullParser == xmlPullParser2) {
            return false;
        }
        try {
            if (xmlPullParser2.getEventType() == 2) {
                return "include".equals(xmlPullParser2.getName());
            }
            return false;
        }
        catch (final XmlPullParserException ex) {
            return false;
        }
    }
    
    public boolean a(final AttributeSet set) {
        if (set instanceof XmlPullParser) {
            final XmlPullParser referent = (XmlPullParser)set;
            if (referent.getDepth() == 1) {
                final XmlPullParser c = c(this.a);
                this.a.push(new WeakReference(referent));
                if (d(referent, c)) {
                    return true;
                }
            }
        }
        return false;
    }
}

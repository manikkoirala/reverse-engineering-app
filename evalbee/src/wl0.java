import android.util.Log;
import java.io.Writer;

// 
// Decompiled by Procyon v0.6.0
// 

public final class wl0 extends Writer
{
    public final String a;
    public StringBuilder b;
    
    public wl0(final String a) {
        this.b = new StringBuilder(128);
        this.a = a;
    }
    
    public final void a() {
        if (this.b.length() > 0) {
            Log.d(this.a, this.b.toString());
            final StringBuilder b = this.b;
            b.delete(0, b.length());
        }
    }
    
    @Override
    public void close() {
        this.a();
    }
    
    @Override
    public void flush() {
        this.a();
    }
    
    @Override
    public void write(final char[] array, final int n, final int n2) {
        for (int i = 0; i < n2; ++i) {
            final char c = array[n + i];
            if (c == '\n') {
                this.a();
            }
            else {
                this.b.append(c);
            }
        }
    }
}

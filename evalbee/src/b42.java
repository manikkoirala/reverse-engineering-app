import java.util.HashSet;
import java.util.Collection;
import java.util.Set;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public class b42
{
    public final Map a;
    
    public b42() {
        this.a = new LinkedHashMap();
    }
    
    public final void a() {
        final Iterator iterator = this.a.values().iterator();
        while (iterator.hasNext()) {
            ((y32)iterator.next()).a();
        }
        this.a.clear();
    }
    
    public final y32 b(final String s) {
        fg0.e((Object)s, "key");
        return this.a.get(s);
    }
    
    public final Set c() {
        return new HashSet(this.a.keySet());
    }
    
    public final void d(final String s, final y32 y32) {
        fg0.e((Object)s, "key");
        fg0.e((Object)y32, "viewModel");
        final y32 y33 = this.a.put(s, y32);
        if (y33 != null) {
            y33.d();
        }
    }
}

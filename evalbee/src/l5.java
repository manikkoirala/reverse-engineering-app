import java.io.File;
import org.jetbrains.annotations.NotNull;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public final class l5
{
    public static final l5 a;
    
    static {
        a = new l5();
    }
    
    @NotNull
    public final File a(@NotNull final Context context) {
        fg0.e((Object)context, "context");
        final File noBackupFilesDir = context.getNoBackupFilesDir();
        fg0.d((Object)noBackupFilesDir, "context.noBackupFilesDir");
        return noBackupFilesDir;
    }
}

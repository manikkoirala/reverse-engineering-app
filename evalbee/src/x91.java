import java.util.Iterator;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import com.ekodroid.omrevaluator.database.TemplateDataJsonModel;
import com.ekodroid.omrevaluator.database.StudentDataModel;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.PublishMetaDataDto;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.StudentResultPartialResponse;
import com.ekodroid.omrevaluator.database.ResultDataJsonModel;
import java.util.ArrayList;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.ExamPartialResponse;
import com.ekodroid.omrevaluator.serializable.SharedType;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.Published;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import com.ekodroid.omrevaluator.database.repositories.ClassRepository;
import com.ekodroid.omrevaluator.database.repositories.ResultRepository;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.PublishRequest;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class x91
{
    public static PublishRequest a(final Context context, final ExamId examId, final boolean b, final boolean b2, final Long n, final String s, final String s2) {
        final PublishRequest publishRequest = new PublishRequest();
        publishRequest.expiresIn = 31536000000L;
        final ResultRepository instance = ResultRepository.getInstance(context);
        final ClassRepository instance2 = ClassRepository.getInstance(context);
        final TemplateDataJsonModel templateJson = TemplateRepository.getInstance(context).getTemplateJson(examId);
        publishRequest.cloudId = templateJson.getCloudId();
        if (!templateJson.isCloudSyncOn()) {
            publishRequest.expiresIn = 604800000L;
            final SheetTemplate2 sheetTemplate = templateJson.getSheetTemplate();
            Published published;
            if (templateJson.isPublished()) {
                published = Published.PUBLISHED;
            }
            else {
                published = Published.NOT_PUBLISHED;
            }
            publishRequest.examData = new ExamPartialResponse(null, templateJson.getLastUpdatedOn(), examId, new gc0().s(sheetTemplate), sheetTemplate.getTemplateVersion(), published, SharedType.PUBLIC, null, templateJson.isSyncImages());
            final ArrayList<ResultDataJsonModel> allNotSyncedResults = instance.getAllNotSyncedResults(examId);
            final ArrayList studentResults = new ArrayList();
            for (final ResultDataJsonModel resultDataJsonModel : allNotSyncedResults) {
                Published published2;
                if (resultDataJsonModel.isPublished()) {
                    published2 = Published.PUBLISHED;
                }
                else {
                    published2 = Published.NOT_PUBLISHED;
                }
                studentResults.add(new StudentResultPartialResponse(resultDataJsonModel.getRollNo(), 0L, resultDataJsonModel.getResultItemString(), 0, published2));
            }
            publishRequest.studentResults = studentResults;
        }
        final ArrayList<StudentDataModel> students = instance2.getStudents(examId.getClassName());
        publishRequest.publishMetaDataDtos = new ArrayList<PublishMetaDataDto>();
        for (final StudentDataModel studentDataModel : students) {
            if (studentDataModel.getEmailId() != null && studentDataModel.getEmailId().contains("@") && studentDataModel.getEmailId().length() > 5) {
                publishRequest.publishMetaDataDtos.add(new PublishMetaDataDto(studentDataModel.getStudentName(), studentDataModel.getEmailId(), studentDataModel.getRollNO(), s, s2, b2, b));
            }
        }
        return publishRequest;
    }
    
    public static PublishRequest b(final Context context, final ExamId examId, final boolean b, final boolean b2, final Long n, final String s, final String s2, final int n2) {
        final PublishRequest publishRequest = new PublishRequest();
        publishRequest.expiresIn = 31536000000L;
        final ClassRepository instance = ClassRepository.getInstance(context);
        publishRequest.cloudId = TemplateRepository.getInstance(context).getTemplateJson(examId).getCloudId();
        final StudentDataModel student = instance.getStudent(n2, examId.getClassName());
        publishRequest.publishMetaDataDtos = new ArrayList<PublishMetaDataDto>();
        if (student.getEmailId() != null && student.getEmailId().contains("@") && student.getEmailId().length() > 5) {
            publishRequest.publishMetaDataDtos.add(new PublishMetaDataDto(student.getStudentName(), student.getEmailId(), student.getRollNO(), s, s2, b2, b));
        }
        return publishRequest;
    }
}

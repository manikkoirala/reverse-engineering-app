import android.view.View;
import android.view.WindowId;

// 
// Decompiled by Procyon v0.6.0
// 

public class f62 implements g62
{
    public final WindowId a;
    
    public f62(final View view) {
        this.a = view.getWindowId();
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof f62 && ((f62)o).a.equals((Object)this.a);
    }
    
    @Override
    public int hashCode() {
        return this.a.hashCode();
    }
}

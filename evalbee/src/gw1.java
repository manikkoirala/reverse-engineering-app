import java.io.Writer;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class gw1
{
    public static final Object a;
    public static final Method b;
    public static final Method c;
    
    static {
        final Object o = a = b();
        final Method method = null;
        Method a2;
        if (o == null) {
            a2 = null;
        }
        else {
            a2 = a();
        }
        b = a2;
        Method d;
        if (o == null) {
            d = method;
        }
        else {
            d = d(o);
        }
        c = d;
    }
    
    public static Method a() {
        return c("getStackTraceElement", Throwable.class, Integer.TYPE);
    }
    
    public static Object b() {
        Object invoke = null;
        try {
            invoke = Class.forName("sun.misc.SharedSecrets", false, null).getMethod("getJavaLangAccess", (Class<?>[])new Class[0]).invoke(null, new Object[0]);
            return invoke;
        }
        catch (final ThreadDeath invoke) {
            throw invoke;
        }
        finally {
            return invoke;
        }
    }
    
    public static Method c(final String name, final Class... parameterTypes) {
        try {
            return Class.forName("sun.misc.JavaLangAccess", false, null).getMethod(name, (Class<?>[])parameterTypes);
        }
        catch (final ThreadDeath threadDeath) {
            throw threadDeath;
        }
        finally {
            return null;
        }
    }
    
    public static Method d(final Object obj) {
        try {
            final Method c = c("getStackTraceDepth", Throwable.class);
            if (c == null) {
                return null;
            }
            c.invoke(obj, new Throwable());
            return c;
        }
        catch (final UnsupportedOperationException | IllegalAccessException | InvocationTargetException ex) {
            return null;
        }
    }
    
    public static String e(final Throwable t) {
        final StringWriter out = new StringWriter();
        t.printStackTrace(new PrintWriter(out));
        return out.toString();
    }
    
    public static void f(final Throwable t) {
        i71.r(t);
        if (t instanceof RuntimeException) {
            throw (RuntimeException)t;
        }
        if (!(t instanceof Error)) {
            return;
        }
        throw (Error)t;
    }
}

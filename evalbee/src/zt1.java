import com.google.firebase.database.collection.c;
import com.google.protobuf.ByteString;

// 
// Decompiled by Procyon v0.6.0
// 

public final class zt1
{
    public final ByteString a;
    public final boolean b;
    public final c c;
    public final c d;
    public final c e;
    
    public zt1(final ByteString a, final boolean b, final c c, final c d, final c e) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
    }
    
    public static zt1 a(final boolean b, final ByteString byteString) {
        return new zt1(byteString, b, du.e(), du.e(), du.e());
    }
    
    public c b() {
        return this.c;
    }
    
    public c c() {
        return this.d;
    }
    
    public c d() {
        return this.e;
    }
    
    public ByteString e() {
        return this.a;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o != null && zt1.class == o.getClass()) {
            final zt1 zt1 = (zt1)o;
            return this.b == zt1.b && this.a.equals(zt1.a) && this.c.equals(zt1.c) && this.d.equals(zt1.d) && this.e.equals(zt1.e);
        }
        return false;
    }
    
    public boolean f() {
        return this.b;
    }
    
    @Override
    public int hashCode() {
        return (((this.a.hashCode() * 31 + (this.b ? 1 : 0)) * 31 + this.c.hashCode()) * 31 + this.d.hashCode()) * 31 + this.e.hashCode();
    }
}

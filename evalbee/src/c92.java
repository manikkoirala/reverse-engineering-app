import java.util.UUID;
import androidx.work.d;
import androidx.work.impl.WorkManagerImplExtKt;
import androidx.work.impl.utils.ForceStopRunnable;
import java.util.concurrent.Executor;
import android.content.BroadcastReceiver$PendingResult;
import java.util.List;
import androidx.work.impl.WorkDatabase;
import androidx.work.a;
import android.content.Context;
import androidx.work.WorkManager;

// 
// Decompiled by Procyon v0.6.0
// 

public class c92 extends WorkManager
{
    public static final String k;
    public static c92 l;
    public static c92 m;
    public static final Object n;
    public Context a;
    public androidx.work.a b;
    public WorkDatabase c;
    public hu1 d;
    public List e;
    public q81 f;
    public r71 g;
    public boolean h;
    public BroadcastReceiver$PendingResult i;
    public final ky1 j;
    
    static {
        k = xl0.i("WorkManagerImpl");
        c92.l = null;
        c92.m = null;
        n = new Object();
    }
    
    public c92(Context applicationContext, final androidx.work.a b, final hu1 d, final WorkDatabase c, final List e, final q81 f, final ky1 j) {
        this.h = false;
        applicationContext = applicationContext.getApplicationContext();
        if (!c92.a.a(applicationContext)) {
            xl0.h(new xl0.a(b.j()));
            this.a = applicationContext;
            this.d = d;
            this.c = c;
            this.f = f;
            this.j = j;
            this.b = b;
            this.e = e;
            this.g = new r71(c);
            nj1.g(e, this.f, d.d(), this.c, b);
            this.d.b(new ForceStopRunnable(applicationContext, this));
            return;
        }
        throw new IllegalStateException("Cannot initialize WorkManager in direct boot mode");
    }
    
    public static void e(Context applicationContext, final androidx.work.a a) {
        synchronized (c92.n) {
            final c92 l = c92.l;
            if (l != null && c92.m != null) {
                throw new IllegalStateException("WorkManager is already initialized.  Did you try to initialize it manually without disabling WorkManagerInitializer? See WorkManager#initialize(Context, Configuration) or the class level Javadoc for more information.");
            }
            if (l == null) {
                applicationContext = applicationContext.getApplicationContext();
                if (c92.m == null) {
                    c92.m = WorkManagerImplExtKt.c(applicationContext, a);
                }
                c92.l = c92.m;
            }
        }
    }
    
    public static c92 i() {
        synchronized (c92.n) {
            final c92 l = c92.l;
            if (l != null) {
                return l;
            }
            return c92.m;
        }
    }
    
    public static c92 j(final Context context) {
        synchronized (c92.n) {
            final c92 i = i();
            if (i != null) {
                return i;
            }
            context.getApplicationContext();
            throw new IllegalStateException("WorkManager is not initialized properly.  You have explicitly disabled WorkManagerInitializer in your manifest, have not manually called WorkManager#initialize at this point, and your Application does not implement Configuration.Provider.");
        }
    }
    
    @Override
    public d a(final String s) {
        final bf d = bf.d(s, this);
        this.d.b(d);
        return d.e();
    }
    
    @Override
    public d c(final List list) {
        if (!list.isEmpty()) {
            return new j82(this, list).a();
        }
        throw new IllegalArgumentException("enqueue needs at least one WorkRequest.");
    }
    
    public d f(final UUID uuid) {
        final bf b = bf.b(uuid, this);
        this.d.b(b);
        return b.e();
    }
    
    public Context g() {
        return this.a;
    }
    
    public androidx.work.a h() {
        return this.b;
    }
    
    public r71 k() {
        return this.g;
    }
    
    public q81 l() {
        return this.f;
    }
    
    public List m() {
        return this.e;
    }
    
    public ky1 n() {
        return this.j;
    }
    
    public WorkDatabase o() {
        return this.c;
    }
    
    public hu1 p() {
        return this.d;
    }
    
    public void q() {
        synchronized (c92.n) {
            this.h = true;
            final BroadcastReceiver$PendingResult i = this.i;
            if (i != null) {
                i.finish();
                this.i = null;
            }
        }
    }
    
    public void r() {
        pt1.c(this.g());
        this.o().K().u();
        nj1.h(this.h(), this.o(), this.m());
    }
    
    public void s(final BroadcastReceiver$PendingResult i) {
        synchronized (c92.n) {
            final BroadcastReceiver$PendingResult j = this.i;
            if (j != null) {
                j.finish();
            }
            this.i = i;
            if (this.h) {
                i.finish();
                this.i = null;
            }
        }
    }
    
    public void t(final x82 x82) {
        this.d.b(new cq1(this.f, new op1(x82), true));
    }
    
    public abstract static class a
    {
        public static boolean a(final Context context) {
            return context.isDeviceProtectedStorage();
        }
    }
}

import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.text.method.NumberKeyListener;
import android.text.method.KeyListener;
import android.widget.EditText;

// 
// Decompiled by Procyon v0.6.0
// 

public class u6
{
    public final EditText a;
    public final iw b;
    
    public u6(final EditText a) {
        this.a = a;
        this.b = new iw(a, false);
    }
    
    public KeyListener a(final KeyListener keyListener) {
        KeyListener a = keyListener;
        if (this.b(keyListener)) {
            a = this.b.a(keyListener);
        }
        return a;
    }
    
    public boolean b(final KeyListener keyListener) {
        return keyListener instanceof NumberKeyListener ^ true;
    }
    
    public boolean c() {
        return this.b.b();
    }
    
    public void d(final AttributeSet set, int u0) {
        final TypedArray obtainStyledAttributes = ((View)this.a).getContext().obtainStyledAttributes(set, bc1.g0, u0, 0);
        try {
            u0 = bc1.u0;
            final boolean hasValue = obtainStyledAttributes.hasValue(u0);
            boolean boolean1 = true;
            if (hasValue) {
                boolean1 = obtainStyledAttributes.getBoolean(u0, true);
            }
            obtainStyledAttributes.recycle();
            this.f(boolean1);
        }
        finally {
            obtainStyledAttributes.recycle();
        }
    }
    
    public InputConnection e(final InputConnection inputConnection, final EditorInfo editorInfo) {
        return this.b.c(inputConnection, editorInfo);
    }
    
    public void f(final boolean b) {
        this.b.d(b);
    }
}

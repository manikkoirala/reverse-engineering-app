import java.io.OutputStream;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.FirebaseAuth;
import java.util.HashMap;
import java.util.Map;
import java.nio.charset.StandardCharsets;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.d;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.File;
import android.content.Context;
import com.ekodroid.omrevaluator.serializable.ResultData;

// 
// Decompiled by Procyon v0.6.0
// 

public class f61
{
    public ee1 a;
    public zg b;
    public ResultData c;
    public Context d;
    public String e;
    public String f;
    
    public f61(final Context d, final ResultData c, final ee1 a, final String f, final zg b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("https://");
        sb.append(a91.w());
        sb.append(":8759/excelDownload3");
        this.e = sb.toString();
        this.d = d;
        this.a = a;
        this.b = b;
        this.c = c;
        this.f = f;
        this.g();
    }
    
    public static /* synthetic */ ResultData c(final f61 f61) {
        return f61.c;
    }
    
    public void d(final byte[] array) {
        final File file = null;
        Label_0191: {
            if (array != null) {
                Object externalFilesDir = this.d.getExternalFilesDir(ok.c);
                if (!((File)externalFilesDir).exists()) {
                    ((File)externalFilesDir).mkdirs();
                }
                final StringBuilder sb = new StringBuilder();
                sb.append(this.c.getSubjectName().replaceAll("[^a-zA-Z0-9_-]", "_"));
                sb.append(".xlsx");
                final File file2 = new File((File)externalFilesDir, sb.toString());
                Label_0169: {
                    FileOutputStream fileOutputStream = null;
                    try {
                        fileOutputStream = (FileOutputStream)(externalFilesDir = new FileOutputStream(file2));
                        try {
                            final FileOutputStream fileOutputStream2 = fileOutputStream;
                            final byte[] array2 = array;
                            fileOutputStream2.write(array2);
                            final FileOutputStream fileOutputStream3 = fileOutputStream;
                            fileOutputStream3.flush();
                            final FileOutputStream fileOutputStream4 = fileOutputStream;
                            fileOutputStream4.close();
                            final f61 f61 = this;
                            final boolean b = true;
                            final int n = 200;
                            final File file3 = file2;
                            final String s = file3.getAbsolutePath();
                            f61.f(b, n, s);
                            return;
                        }
                        catch (final Exception externalFilesDir) {
                            final Object o = fileOutputStream;
                            final File file4 = (File)externalFilesDir;
                        }
                    }
                    catch (final Exception file4) {
                        final Object o = null;
                    }
                    finally {
                        externalFilesDir = file;
                        break Label_0169;
                    }
                    try {
                        final FileOutputStream fileOutputStream2 = fileOutputStream;
                        final byte[] array2 = array;
                        fileOutputStream2.write(array2);
                        final FileOutputStream fileOutputStream3 = fileOutputStream;
                        fileOutputStream3.flush();
                        final FileOutputStream fileOutputStream4 = fileOutputStream;
                        fileOutputStream4.close();
                        final f61 f61 = this;
                        final boolean b = true;
                        final int n = 200;
                        final File file3 = file2;
                        final String s = file3.getAbsolutePath();
                        f61.f(b, n, s);
                        return;
                        final Object o;
                        externalFilesDir = o;
                        final File file4;
                        ((Throwable)file4).printStackTrace();
                        iftrue(Label_0191:)(o == null);
                        try {
                            ((OutputStream)o).flush();
                            ((FileOutputStream)o).close();
                        }
                        catch (final IOException ex) {
                            ex.printStackTrace();
                        }
                        break Label_0191;
                    }
                    finally {}
                }
                if (externalFilesDir != null) {
                    try {
                        ((OutputStream)externalFilesDir).flush();
                        ((FileOutputStream)externalFilesDir).close();
                    }
                    catch (final IOException ex2) {
                        ex2.printStackTrace();
                    }
                }
            }
        }
        this.f(false, 400, null);
    }
    
    public final void e(final String s) {
        final Request request = new Request(this, 1, this.e, new d.a(this) {
            public final f61 a;
            
            @Override
            public void a(final VolleyError volleyError) {
                volleyError.printStackTrace();
                this.a.f(false, 400, null);
            }
        }, s) {
            public final String t;
            public final f61 v;
            
            @Override
            public d G(final yy0 yy0) {
                return d.c(yy0.b, id0.e(yy0));
            }
            
            public void Q(final byte[] bytes) {
                if (bytes.length > 5) {
                    this.v.d(bytes);
                }
                else {
                    this.v.f(true, 218, Integer.parseInt(new String(bytes, StandardCharsets.UTF_8)));
                }
            }
            
            @Override
            public byte[] k() {
                try {
                    return new gc0().s(f61.c(this.v)).getBytes("UTF-8");
                }
                catch (final Exception ex) {
                    ex.printStackTrace();
                    return null;
                }
            }
            
            @Override
            public String l() {
                return "application/json";
            }
            
            @Override
            public Map o() {
                final HashMap hashMap = new HashMap();
                hashMap.put("x-user-token", this.t);
                return hashMap;
            }
        };
        request.L(new wq(100000, 0, 1.0f));
        this.a.a(request);
    }
    
    public final void f(final boolean b, final int n, final Object o) {
        final zg b2 = this.b;
        if (b2 != null) {
            b2.a(b, n, o);
        }
    }
    
    public final void g() {
        FirebaseAuth.getInstance().e().i(false).addOnCompleteListener((OnCompleteListener)new OnCompleteListener(this) {
            public final f61 a;
            
            public void onComplete(final Task task) {
                if (task.isSuccessful()) {
                    this.a.e(((ya0)task.getResult()).c());
                }
                else {
                    this.a.f(false, 400, null);
                }
            }
        });
    }
    
    public void h() {
        this.b = null;
    }
}

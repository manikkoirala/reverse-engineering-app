import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.android.gms.common.internal.Preconditions;

// 
// Decompiled by Procyon v0.6.0
// 

public final class qb2 implements Runnable
{
    public final String a;
    public final sb2 b;
    
    public qb2(final sb2 b, final String s) {
        this.b = b;
        this.a = Preconditions.checkNotEmpty(s);
    }
    
    @Override
    public final void run() {
        final FirebaseAuth instance = FirebaseAuth.getInstance(r10.n(this.a));
        if (instance.e() != null) {
            final Task c = instance.c(true);
            sb2.a().v("Token refreshing started", new Object[0]);
            c.addOnFailureListener((OnFailureListener)new vb2(this));
        }
    }
}

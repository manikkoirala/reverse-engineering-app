import android.os.Build$VERSION;
import androidx.work.c;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class v82 implements Runnable
{
    public static final String g;
    public final um1 a;
    public final Context b;
    public final p92 c;
    public final c d;
    public final r70 e;
    public final hu1 f;
    
    static {
        g = xl0.i("WorkForegroundRunnable");
    }
    
    public v82(final Context b, final p92 c, final c d, final r70 e, final hu1 f) {
        this.a = um1.s();
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
    }
    
    public ik0 b() {
        return this.a;
    }
    
    @Override
    public void run() {
        if (this.c.q && Build$VERSION.SDK_INT < 31) {
            final um1 s = um1.s();
            this.f.c().execute(new u82(this, s));
            s.addListener(new Runnable(this, s) {
                public final um1 a;
                public final v82 b;
                
                @Override
                public void run() {
                    if (this.b.a.isCancelled()) {
                        return;
                    }
                    try {
                        final p70 p70 = (p70)this.a.get();
                        if (p70 == null) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Worker was marked important (");
                            sb.append(this.b.c.c);
                            sb.append(") but did not provide ForegroundInfo");
                            throw new IllegalStateException(sb.toString());
                        }
                        final xl0 e = xl0.e();
                        final String g = v82.g;
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Updating notification for ");
                        sb2.append(this.b.c.c);
                        e.a(g, sb2.toString());
                        final v82 b = this.b;
                        b.a.q(b.e.a(b.b, b.d.getId(), p70));
                    }
                    finally {
                        final Throwable t;
                        this.b.a.p(t);
                    }
                }
            }, this.f.c());
            return;
        }
        this.a.o(null);
    }
}

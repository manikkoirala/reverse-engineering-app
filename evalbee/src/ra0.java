import android.content.pm.PackageInfo;
import com.android.volley.Request;
import android.content.pm.PackageManager$NameNotFoundException;
import com.ekodroid.omrevaluator.serializable.SerialData;
import com.android.volley.VolleyError;
import android.content.SharedPreferences$Editor;
import com.ekodroid.omrevaluator.serializable.AppConfig;
import com.ekodroid.omrevaluator.serializable.SessionProfile;
import com.android.volley.d;
import java.util.Date;
import java.text.SimpleDateFormat;
import android.content.Context;
import android.content.SharedPreferences;

// 
// Decompiled by Procyon v0.6.0
// 

public class ra0
{
    public String a;
    public SharedPreferences b;
    public ee1 c;
    public y01 d;
    public String e;
    public Context f;
    public int g;
    public String h;
    
    public ra0(final Context f, final ee1 c, final y01 d, final String e) {
        final StringBuilder sb = new StringBuilder();
        sb.append("https://");
        sb.append(a91.u());
        sb.append(":8759/sessionProfile2");
        this.a = sb.toString();
        this.g = 0;
        this.h = null;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.b = f.getApplicationContext().getSharedPreferences("MyPref", 0);
        this.i();
    }
    
    public static /* synthetic */ String a(final ra0 ra0) {
        return ra0.e;
    }
    
    public static /* synthetic */ int b(final ra0 ra0) {
        return ra0.g;
    }
    
    public static /* synthetic */ int c(final ra0 ra0, final int g) {
        return ra0.g = g;
    }
    
    public static /* synthetic */ String d(final ra0 ra0) {
        return ra0.h;
    }
    
    public static /* synthetic */ String e(final ra0 ra0, final String h) {
        return ra0.h = h;
    }
    
    public static /* synthetic */ Context g(final ra0 ra0) {
        return ra0.f;
    }
    
    public static String h() {
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    }
    
    public final void i() {
        final hr1 hr1 = new hr1(this, 1, this.a, new d.b(this) {
            public final ra0 a;
            
            public void b(final String s) {
                try {
                    final SessionProfile sessionProfile = (SessionProfile)new gc0().j(s, SessionProfile.class);
                    final AppConfig appConfig = (AppConfig)new gc0().j(b.g(sessionProfile.getData(), ra0.a(this.a)), AppConfig.class);
                    final int latestVersion = appConfig.getLatestVersion();
                    final int blockedVersion = appConfig.getBlockedVersion();
                    final String syncdate = appConfig.getSyncdate();
                    SharedPreferences$Editor sharedPreferences$Editor;
                    if (latestVersion <= ra0.b(this.a) && blockedVersion <= ra0.b(this.a)) {
                        final SharedPreferences$Editor edit = this.a.b.edit();
                        final StringBuilder sb = new StringBuilder();
                        sb.append(ok.x);
                        sb.append(ra0.b(this.a));
                        edit.putString(sb.toString(), syncdate).commit();
                        final SharedPreferences$Editor edit2 = this.a.b.edit();
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append(ok.z);
                        sb2.append(ra0.b(this.a));
                        edit2.putString(sb2.toString(), ra0.d(this.a)).commit();
                        final SharedPreferences$Editor edit3 = this.a.b.edit();
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append(ok.A);
                        sb3.append(ra0.b(this.a));
                        edit3.putString(sb3.toString(), sessionProfile.getConfigData()).commit();
                        final SharedPreferences$Editor edit4 = this.a.b.edit();
                        final StringBuilder sb4 = new StringBuilder();
                        sb4.append(ok.B);
                        sb4.append(ra0.b(this.a));
                        sharedPreferences$Editor = edit4.putString(sb4.toString(), sessionProfile.getConfigId());
                    }
                    else {
                        this.a.b.edit().putInt(ok.C, latestVersion).commit();
                        this.a.b.edit().putInt(ok.D, blockedVersion).commit();
                        final SharedPreferences$Editor edit5 = this.a.b.edit();
                        final StringBuilder sb5 = new StringBuilder();
                        sb5.append(ok.x);
                        sb5.append(latestVersion);
                        sharedPreferences$Editor = edit5.putString(sb5.toString(), syncdate);
                    }
                    sharedPreferences$Editor.commit();
                    this.a.j("Success");
                }
                catch (final Exception ex) {
                    ex.printStackTrace();
                    this.a.j(null);
                }
            }
        }, new d.a(this) {
            public final ra0 a;
            
            @Override
            public void a(final VolleyError volleyError) {
                this.a.j(null);
            }
        }) {
            public final ra0 w;
            
            @Override
            public byte[] k() {
                Object o = null;
                try {
                    o = ra0.g(this.w).getPackageManager().getPackageInfo(ra0.g(this.w).getPackageName(), 0);
                    ra0.c(this.w, ((PackageInfo)o).versionCode);
                    o = ra0.h();
                    final AppConfig appConfig = new AppConfig((String)o, ra0.b(this.w), false, a91.n(), ra0.b(this.w));
                    o = b.i(ra0.a(this.w), "phoneKey");
                    final String i = b.i(new gc0().s(appConfig), ra0.a(this.w));
                    ra0.e(this.w, b.i(new gc0().s(new SerialData(a91.n(), a91.s())), ra0.a(this.w)));
                    final SessionProfile sessionProfile = new SessionProfile((String)o, i, ra0.d(this.w), null);
                    o = new gc0();
                    o = ((gc0)o).s(sessionProfile).getBytes("UTF-8");
                    return (byte[])o;
                }
                catch (final Exception o) {}
                catch (final PackageManager$NameNotFoundException ex) {}
                ((Throwable)o).printStackTrace();
                return null;
            }
            
            @Override
            public String l() {
                return "application/json";
            }
        };
        hr1.L(new wq(40000, 0, 1.0f));
        this.c.a(hr1);
    }
    
    public final void j(final Object o) {
        final y01 d = this.d;
        if (d != null) {
            d.a(o);
        }
    }
}

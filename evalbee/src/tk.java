import java.util.Iterator;
import java.util.List;
import java.util.LinkedHashSet;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class tk
{
    public final hu1 a;
    public final Context b;
    public final Object c;
    public final LinkedHashSet d;
    public Object e;
    
    public tk(Context applicationContext, final hu1 a) {
        fg0.e((Object)applicationContext, "context");
        fg0.e((Object)a, "taskExecutor");
        this.a = a;
        applicationContext = applicationContext.getApplicationContext();
        fg0.d((Object)applicationContext, "context.applicationContext");
        this.b = applicationContext;
        this.c = new Object();
        this.d = new LinkedHashSet();
    }
    
    public static final void b(final List list, final tk tk) {
        fg0.e((Object)list, "$listenersList");
        fg0.e((Object)tk, "this$0");
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            ((rk)iterator.next()).a(tk.e);
        }
    }
    
    public final void c(final rk e) {
        fg0.e((Object)e, "listener");
        synchronized (this.c) {
            if (this.d.add(e)) {
                if (this.d.size() == 1) {
                    this.e = this.e();
                    final xl0 e2 = xl0.e();
                    final String a = uk.a();
                    final StringBuilder sb = new StringBuilder();
                    sb.append(this.getClass().getSimpleName());
                    sb.append(": initial state = ");
                    sb.append(this.e);
                    e2.a(a, sb.toString());
                    this.h();
                }
                e.a(this.e);
            }
            final u02 a2 = u02.a;
        }
    }
    
    public final Context d() {
        return this.b;
    }
    
    public abstract Object e();
    
    public final void f(final rk o) {
        fg0.e((Object)o, "listener");
        synchronized (this.c) {
            if (this.d.remove(o) && this.d.isEmpty()) {
                this.i();
            }
            final u02 a = u02.a;
        }
    }
    
    public final void g(final Object e) {
        synchronized (this.c) {
            final Object e2 = this.e;
            if (e2 != null && fg0.a(e2, e)) {
                return;
            }
            this.e = e;
            this.a.c().execute(new sk(vh.P((Iterable)this.d), this));
            final u02 a = u02.a;
        }
    }
    
    public abstract void h();
    
    public abstract void i();
}

import java.util.Iterator;
import android.view.ViewGroup$LayoutParams;
import com.ekodroid.omrevaluator.templateui.models.AnswerOption;
import com.ekodroid.omrevaluator.templateui.models.AnswerOptionKey;
import android.widget.TableLayout$LayoutParams;
import android.widget.LinearLayout;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import com.ekodroid.omrevaluator.templateui.models.LabelProfile;
import java.util.ArrayList;
import android.content.Context;
import android.widget.BaseAdapter;

// 
// Decompiled by Procyon v0.6.0
// 

public class k3 extends BaseAdapter
{
    public Context a;
    public ArrayList b;
    public ArrayList c;
    public k5.f d;
    public LabelProfile e;
    
    public k3(final Context a, final ArrayList b, final ArrayList c, final k5.f d, final LabelProfile e) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
    }
    
    public int getCount() {
        return this.b.size();
    }
    
    public Object getItem(final int index) {
        return this.b.get(index);
    }
    
    public long getItemId(final int n) {
        return n;
    }
    
    public View getView(final int index, final View view, final ViewGroup viewGroup) {
        final View inflate = ((LayoutInflater)this.a.getSystemService("layout_inflater")).inflate(2131493024, (ViewGroup)null);
        final LinearLayout linearLayout = (LinearLayout)inflate.findViewById(2131297146);
        final TableLayout$LayoutParams tableLayout$LayoutParams = new TableLayout$LayoutParams(-1, -2);
        ((ViewGroup)linearLayout).removeAllViews();
        final Iterator iterator = new k5(this.a, this.b.get(index), this.c.get(index + 1), this.d, this.e).m().iterator();
        while (iterator.hasNext()) {
            ((ViewGroup)linearLayout).addView((View)iterator.next(), (ViewGroup$LayoutParams)tableLayout$LayoutParams);
        }
        return inflate;
    }
}

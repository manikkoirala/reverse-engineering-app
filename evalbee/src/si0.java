import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.view.View$OnClickListener;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import java.util.Iterator;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemSelectedListener;
import android.widget.SpinnerAdapter;
import com.ekodroid.omrevaluator.database.LabelProfileJsonModel;
import java.util.ArrayList;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import java.io.Serializable;
import android.content.Intent;
import com.ekodroid.omrevaluator.templateui.createtemplate.LabelsActivity;
import androidx.appcompat.app.a;
import com.ekodroid.omrevaluator.templateui.models.LabelProfile;
import android.widget.Spinner;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class si0
{
    public Context a;
    public y01 b;
    public Spinner c;
    public LabelProfile d;
    public a e;
    
    public si0(final Context a, final y01 b) {
        this.a = a;
        this.b = b;
        this.f();
    }
    
    public static /* synthetic */ LabelProfile a(final si0 si0) {
        return si0.d;
    }
    
    public static /* synthetic */ LabelProfile b(final si0 si0, final LabelProfile d) {
        return si0.d = d;
    }
    
    public final void d(final LabelProfile labelProfile) {
        this.e.dismiss();
        final Intent intent = new Intent(this.a, (Class)LabelsActivity.class);
        intent.putExtra("LABEL_PROFILE", (Serializable)labelProfile);
        this.a.startActivity(intent);
    }
    
    public final void e() {
        final ArrayList<LabelProfileJsonModel> allLabelProfileJson = TemplateRepository.getInstance(this.a).getAllLabelProfileJson();
        final ArrayList list = new ArrayList();
        list.add(ok.a);
        list.add(ok.b);
        final Iterator<LabelProfileJsonModel> iterator = allLabelProfileJson.iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next().getProfileName());
        }
        this.c.setAdapter((SpinnerAdapter)new p3(this.a, list));
        ((AdapterView)this.c).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener(this, list) {
            public final ArrayList a;
            public final si0 b;
            
            public void onItemSelected(final AdapterView adapterView, final View view, final int index, final long n) {
                if (index == 0) {
                    si0.b(this.b, null);
                    return;
                }
                if (index == 1) {
                    this.b.d(null);
                    return;
                }
                si0.b(this.b, TemplateRepository.getInstance(this.b.a).getLabelProfileJsonModel(this.a.get(index)).getLabelProfile());
            }
            
            public void onNothingSelected(final AdapterView adapterView) {
            }
        });
        ((AdapterView)this.c).setSelection(0);
    }
    
    public final void f() {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(this.a, 2131951953);
        final View inflate = ((LayoutInflater)this.a.getSystemService("layout_inflater")).inflate(2131492995, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        materialAlertDialogBuilder.setTitle(2131886816);
        this.c = (Spinner)inflate.findViewById(2131297094);
        this.e();
        inflate.findViewById(2131296661).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final si0 a;
            
            public void onClick(final View view) {
                final si0 a = this.a;
                a.d(si0.a(a));
            }
        });
        materialAlertDialogBuilder.setPositiveButton(2131886759, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final si0 a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                final si0 a = this.a;
                a.b.a(si0.a(a));
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.setNegativeButton(2131886163, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final si0 a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                dialogInterface.dismiss();
            }
        });
        (this.e = materialAlertDialogBuilder.create()).show();
    }
}

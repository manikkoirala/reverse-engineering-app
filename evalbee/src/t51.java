import android.content.res.Resources;
import android.graphics.Bitmap;
import android.content.Context;
import android.view.PointerIcon;

// 
// Decompiled by Procyon v0.6.0
// 

public final class t51
{
    public final PointerIcon a;
    
    public t51(final PointerIcon a) {
        this.a = a;
    }
    
    public static t51 b(final Context context, final int n) {
        return new t51(a.b(context, n));
    }
    
    public Object a() {
        return this.a;
    }
    
    public abstract static class a
    {
        public static PointerIcon a(final Bitmap bitmap, final float n, final float n2) {
            return PointerIcon.create(bitmap, n, n2);
        }
        
        public static PointerIcon b(final Context context, final int n) {
            return PointerIcon.getSystemIcon(context, n);
        }
        
        public static PointerIcon c(final Resources resources, final int n) {
            return PointerIcon.load(resources, n);
        }
    }
}

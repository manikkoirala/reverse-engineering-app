import com.google.firebase.crashlytics.internal.common.CommonUtils;
import java.io.InputStream;
import java.io.IOException;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class ts
{
    public final Context a;
    public b b;
    
    public ts(final Context a) {
        this.a = a;
        this.b = null;
    }
    
    public static /* synthetic */ Context a(final ts ts) {
        return ts.a;
    }
    
    public final boolean c(final String s) {
        if (this.a.getAssets() == null) {
            return false;
        }
        try {
            try (final InputStream open = this.a.getAssets().open(s)) {}
            return true;
        }
        catch (final IOException ex) {
            return false;
        }
    }
    
    public String d() {
        return ts.b.a(this.f());
    }
    
    public String e() {
        return ts.b.b(this.f());
    }
    
    public final b f() {
        if (this.b == null) {
            this.b = new b(null);
        }
        return this.b;
    }
    
    public class b
    {
        public final String a;
        public final String b;
        public final ts c;
        
        public b(final ts c) {
            this.c = c;
            final int p = CommonUtils.p(ts.a(c), "com.google.firebase.crashlytics.unity_version", "string");
            if (p != 0) {
                this.a = "Unity";
                final String string = ts.a(c).getResources().getString(p);
                this.b = string;
                final zl0 f = zl0.f();
                final StringBuilder sb = new StringBuilder();
                sb.append("Unity Editor version is: ");
                sb.append(string);
                f.i(sb.toString());
                return;
            }
            if (c.c("flutter_assets/NOTICES.Z")) {
                this.a = "Flutter";
                this.b = null;
                zl0.f().i("Development platform is: Flutter");
                return;
            }
            this.a = null;
            this.b = null;
        }
        
        public static /* synthetic */ String a(final b b) {
            return b.a;
        }
        
        public static /* synthetic */ String b(final b b) {
            return b.b;
        }
    }
}

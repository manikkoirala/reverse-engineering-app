import android.content.ComponentName;
import android.os.Bundle;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import android.os.Handler;
import java.util.concurrent.Executor;
import android.graphics.drawable.Drawable;
import android.content.res.ColorStateList;
import java.io.File;
import android.os.Build$VERSION;
import android.os.Process;
import android.text.TextUtils;
import android.content.Context;
import android.util.TypedValue;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class sl
{
    private static final String DYNAMIC_RECEIVER_NOT_EXPORTED_PERMISSION_SUFFIX = ".DYNAMIC_RECEIVER_NOT_EXPORTED_PERMISSION";
    public static final int RECEIVER_EXPORTED = 2;
    public static final int RECEIVER_NOT_EXPORTED = 4;
    public static final int RECEIVER_VISIBLE_TO_INSTANT_APPS = 1;
    private static final String TAG = "ContextCompat";
    private static final Object sLock;
    private static final Object sSync;
    private static TypedValue sTempValue;
    
    static {
        sLock = new Object();
        sSync = new Object();
    }
    
    public static int checkSelfPermission(final Context context, final String s) {
        c11.d(s, "permission must be non-null");
        if (!yc.c() && TextUtils.equals((CharSequence)"android.permission.POST_NOTIFICATIONS", (CharSequence)s)) {
            int n;
            if (i01.b(context).a()) {
                n = 0;
            }
            else {
                n = -1;
            }
            return n;
        }
        return context.checkPermission(s, Process.myPid(), Process.myUid());
    }
    
    public static Context createDeviceProtectedStorageContext(final Context context) {
        return e.a(context);
    }
    
    public static String getAttributionTag(final Context context) {
        if (Build$VERSION.SDK_INT >= 30) {
            return h.a(context);
        }
        return null;
    }
    
    public static File getCodeCacheDir(final Context context) {
        return c.a(context);
    }
    
    public static int getColor(final Context context, final int n) {
        return d.a(context, n);
    }
    
    public static ColorStateList getColorStateList(final Context context, final int n) {
        return le1.d(context.getResources(), n, context.getTheme());
    }
    
    public static File getDataDir(final Context context) {
        return e.b(context);
    }
    
    public static Drawable getDrawable(final Context context, final int n) {
        return c.b(context, n);
    }
    
    public static File[] getExternalCacheDirs(final Context context) {
        return b.a(context);
    }
    
    public static File[] getExternalFilesDirs(final Context context, final String s) {
        return b.b(context, s);
    }
    
    public static Executor getMainExecutor(final Context context) {
        if (Build$VERSION.SDK_INT >= 28) {
            return g.a(context);
        }
        return sy.a(new Handler(context.getMainLooper()));
    }
    
    public static File getNoBackupFilesDir(final Context context) {
        return c.c(context);
    }
    
    public static File[] getObbDirs(final Context context) {
        return b.c(context);
    }
    
    public static <T> T getSystemService(final Context context, final Class<T> clazz) {
        return d.b(context, clazz);
    }
    
    public static String getSystemServiceName(final Context context, final Class<?> clazz) {
        return d.c(context, clazz);
    }
    
    public static boolean isDeviceProtectedStorage(final Context context) {
        return e.c(context);
    }
    
    public static String obtainAndCheckReceiverPermission(final Context context) {
        final StringBuilder sb = new StringBuilder();
        sb.append(context.getPackageName());
        sb.append(".DYNAMIC_RECEIVER_NOT_EXPORTED_PERMISSION");
        final String string = sb.toString();
        if (c51.b(context, string) == 0) {
            return string;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Permission ");
        sb2.append(string);
        sb2.append(" is required by your application to receive broadcasts, please add it to your manifest");
        throw new RuntimeException(sb2.toString());
    }
    
    public static Intent registerReceiver(final Context context, final BroadcastReceiver broadcastReceiver, final IntentFilter intentFilter, final int n) {
        return registerReceiver(context, broadcastReceiver, intentFilter, null, null, n);
    }
    
    public static Intent registerReceiver(final Context context, final BroadcastReceiver broadcastReceiver, final IntentFilter intentFilter, final String s, final Handler handler, int n) {
        final int n2 = n & 0x1;
        if (n2 != 0 && (n & 0x4) != 0x0) {
            throw new IllegalArgumentException("Cannot specify both RECEIVER_VISIBLE_TO_INSTANT_APPS and RECEIVER_NOT_EXPORTED");
        }
        int n3 = n;
        if (n2 != 0) {
            n3 = (n | 0x2);
        }
        n = (n3 & 0x2);
        if (n == 0 && (n3 & 0x4) == 0x0) {
            throw new IllegalArgumentException("One of either RECEIVER_EXPORTED or RECEIVER_NOT_EXPORTED is required");
        }
        if (n != 0 && (n3 & 0x4) != 0x0) {
            throw new IllegalArgumentException("Cannot specify both RECEIVER_EXPORTED and RECEIVER_NOT_EXPORTED");
        }
        if (yc.c()) {
            return i.a(context, broadcastReceiver, intentFilter, s, handler, n3);
        }
        if (Build$VERSION.SDK_INT >= 26) {
            return f.a(context, broadcastReceiver, intentFilter, s, handler, n3);
        }
        if ((n3 & 0x4) != 0x0 && s == null) {
            return context.registerReceiver(broadcastReceiver, intentFilter, obtainAndCheckReceiverPermission(context), handler);
        }
        return context.registerReceiver(broadcastReceiver, intentFilter, s, handler);
    }
    
    public static boolean startActivities(final Context context, final Intent[] array) {
        return startActivities(context, array, null);
    }
    
    public static boolean startActivities(final Context context, final Intent[] array, final Bundle bundle) {
        a.a(context, array, bundle);
        return true;
    }
    
    public static void startActivity(final Context context, final Intent intent, final Bundle bundle) {
        a.b(context, intent, bundle);
    }
    
    public static void startForegroundService(final Context context, final Intent intent) {
        if (Build$VERSION.SDK_INT >= 26) {
            f.b(context, intent);
        }
        else {
            context.startService(intent);
        }
    }
    
    public abstract static class a
    {
        public static void a(final Context context, final Intent[] array, final Bundle bundle) {
            context.startActivities(array, bundle);
        }
        
        public static void b(final Context context, final Intent intent, final Bundle bundle) {
            context.startActivity(intent, bundle);
        }
    }
    
    public abstract static class b
    {
        public static File[] a(final Context context) {
            return context.getExternalCacheDirs();
        }
        
        public static File[] b(final Context context, final String s) {
            return context.getExternalFilesDirs(s);
        }
        
        public static File[] c(final Context context) {
            return context.getObbDirs();
        }
    }
    
    public abstract static class c
    {
        public static File a(final Context context) {
            return context.getCodeCacheDir();
        }
        
        public static Drawable b(final Context context, final int n) {
            return context.getDrawable(n);
        }
        
        public static File c(final Context context) {
            return context.getNoBackupFilesDir();
        }
    }
    
    public abstract static class d
    {
        public static int a(final Context context, final int n) {
            return context.getColor(n);
        }
        
        public static <T> T b(final Context context, final Class<T> clazz) {
            return (T)context.getSystemService((Class)clazz);
        }
        
        public static String c(final Context context, final Class<?> clazz) {
            return context.getSystemServiceName((Class)clazz);
        }
    }
    
    public abstract static class e
    {
        public static Context a(final Context context) {
            return context.createDeviceProtectedStorageContext();
        }
        
        public static File b(final Context context) {
            return context.getDataDir();
        }
        
        public static boolean c(final Context context) {
            return context.isDeviceProtectedStorage();
        }
    }
    
    public abstract static class f
    {
        public static Intent a(final Context context, final BroadcastReceiver broadcastReceiver, final IntentFilter intentFilter, final String s, final Handler handler, final int n) {
            if ((n & 0x4) != 0x0 && s == null) {
                return context.registerReceiver(broadcastReceiver, intentFilter, sl.obtainAndCheckReceiverPermission(context), handler);
            }
            return context.registerReceiver(broadcastReceiver, intentFilter, s, handler, n & 0x1);
        }
        
        public static ComponentName b(final Context context, final Intent intent) {
            return context.startForegroundService(intent);
        }
    }
    
    public abstract static class g
    {
        public static Executor a(final Context context) {
            return context.getMainExecutor();
        }
    }
    
    public abstract static class h
    {
        public static String a(final Context context) {
            return context.getAttributionTag();
        }
    }
    
    public abstract static class i
    {
        public static Intent a(final Context context, final BroadcastReceiver broadcastReceiver, final IntentFilter intentFilter, final String s, final Handler handler, final int n) {
            return context.registerReceiver(broadcastReceiver, intentFilter, s, handler, n);
        }
    }
}

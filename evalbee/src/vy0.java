import android.os.Build$VERSION;
import androidx.work.NetworkType;
import androidx.work.impl.constraints.controllers.ConstraintController;

// 
// Decompiled by Procyon v0.6.0
// 

public final class vy0 extends ConstraintController
{
    public static final a c;
    public static final String d;
    public final int b;
    
    static {
        c = new a(null);
        final String i = xl0.i("NetworkMeteredCtrlr");
        fg0.d((Object)i, "tagWithPrefix(\"NetworkMeteredCtrlr\")");
        d = i;
    }
    
    public vy0(final tk tk) {
        fg0.e((Object)tk, "tracker");
        super(tk);
        this.b = 7;
    }
    
    @Override
    public int b() {
        return this.b;
    }
    
    @Override
    public boolean c(final p92 p) {
        fg0.e((Object)p, "workSpec");
        return p.j.d() == NetworkType.METERED;
    }
    
    public boolean g(final zy0 zy0) {
        fg0.e((Object)zy0, "value");
        final int sdk_INT = Build$VERSION.SDK_INT;
        boolean b = false;
        if (sdk_INT < 26) {
            xl0.e().a(vy0.d, "Metered network constraint is not supported before API 26, only checking for connected state.");
            if (zy0.a()) {
                return b;
            }
        }
        else if (zy0.a() && zy0.b()) {
            return b;
        }
        b = true;
        return b;
    }
    
    public static final class a
    {
    }
}

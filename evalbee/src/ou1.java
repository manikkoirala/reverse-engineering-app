import java.util.Iterator;
import android.os.Bundle;
import android.content.pm.PackageManager$NameNotFoundException;
import android.util.Log;
import android.content.ComponentName;
import android.app.Activity;
import android.content.Intent;
import android.content.Context;
import java.util.ArrayList;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ou1 implements Iterable
{
    public final ArrayList a;
    public final Context b;
    
    public ou1(final Context b) {
        this.a = new ArrayList();
        this.b = b;
    }
    
    public static ou1 g(final Context context) {
        return new ou1(context);
    }
    
    public ou1 a(final Intent e) {
        this.a.add(e);
        return this;
    }
    
    public ou1 b(final Activity activity) {
        Intent b;
        if (activity instanceof a) {
            b = ((a)activity).b();
        }
        else {
            b = null;
        }
        Intent a = b;
        if (b == null) {
            a = hy0.a(activity);
        }
        if (a != null) {
            ComponentName componentName;
            if ((componentName = a.getComponent()) == null) {
                componentName = a.resolveActivity(this.b.getPackageManager());
            }
            this.c(componentName);
            this.a(a);
        }
        return this;
    }
    
    public ou1 c(ComponentName component) {
        final int size = this.a.size();
        try {
            Context context = this.b;
            while (true) {
                final Intent b = hy0.b(context, component);
                if (b == null) {
                    break;
                }
                this.a.add(size, b);
                context = this.b;
                component = b.getComponent();
            }
            return this;
        }
        catch (final PackageManager$NameNotFoundException cause) {
            Log.e("TaskStackBuilder", "Bad ComponentName while traversing activity parent metadata");
            throw new IllegalArgumentException((Throwable)cause);
        }
    }
    
    public void i() {
        this.l(null);
    }
    
    @Override
    public Iterator iterator() {
        return this.a.iterator();
    }
    
    public void l(final Bundle bundle) {
        if (!this.a.isEmpty()) {
            final Intent[] array = this.a.toArray(new Intent[0]);
            array[0] = new Intent(array[0]).addFlags(268484608);
            if (!sl.startActivities(this.b, array, bundle)) {
                final Intent intent = new Intent(array[array.length - 1]);
                intent.addFlags(268435456);
                this.b.startActivity(intent);
            }
            return;
        }
        throw new IllegalStateException("No intents added to TaskStackBuilder; cannot startActivities");
    }
    
    public interface a
    {
        Intent b();
    }
}

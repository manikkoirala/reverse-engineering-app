import io.grpc.stub.AbstractAsyncStub;
import io.grpc.stub.AbstractStub;
import io.grpc.CallOptions;
import io.grpc.stub.AbstractStub$StubFactory;
import io.grpc.Channel;
import com.google.firestore.v1.m;
import com.google.firestore.v1.l;
import com.google.firestore.v1.ListenResponse;
import com.google.protobuf.a0;
import io.grpc.protobuf.lite.ProtoLiteUtils;
import com.google.firestore.v1.ListenRequest;
import io.grpc.MethodDescriptor$MethodType;
import io.grpc.MethodDescriptor;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class h40
{
    public static volatile MethodDescriptor a;
    public static volatile MethodDescriptor b;
    
    public static MethodDescriptor a() {
        final MethodDescriptor b;
        if ((b = h40.b) == null) {
            synchronized (h40.class) {
                if (h40.b == null) {
                    h40.b = MethodDescriptor.newBuilder().setType(MethodDescriptor$MethodType.BIDI_STREAMING).setFullMethodName(MethodDescriptor.generateFullMethodName("google.firestore.v1.Firestore", "Listen")).setSampledToLocalTracing(true).setRequestMarshaller(ProtoLiteUtils.marshaller((a0)ListenRequest.e0())).setResponseMarshaller(ProtoLiteUtils.marshaller((a0)ListenResponse.a0())).build();
                }
            }
        }
        return b;
    }
    
    public static MethodDescriptor b() {
        final MethodDescriptor a;
        if ((a = h40.a) == null) {
            synchronized (h40.class) {
                if (h40.a == null) {
                    h40.a = MethodDescriptor.newBuilder().setType(MethodDescriptor$MethodType.BIDI_STREAMING).setFullMethodName(MethodDescriptor.generateFullMethodName("google.firestore.v1.Firestore", "Write")).setSampledToLocalTracing(true).setRequestMarshaller(ProtoLiteUtils.marshaller((a0)l.f0())).setResponseMarshaller(ProtoLiteUtils.marshaller((a0)m.b0())).build();
                }
            }
        }
        return a;
    }
    
    public static b c(final Channel channel) {
        return (b)AbstractAsyncStub.newStub((AbstractStub$StubFactory)new AbstractStub$StubFactory() {
            public b a(final Channel channel, final CallOptions callOptions) {
                return new b(channel, callOptions, null);
            }
        }, channel);
    }
    
    public static final class b extends AbstractAsyncStub
    {
        public b(final Channel channel, final CallOptions callOptions) {
            super(channel, callOptions);
        }
        
        public b a(final Channel channel, final CallOptions callOptions) {
            return new b(channel, callOptions);
        }
    }
}

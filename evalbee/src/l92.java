import androidx.work.WorkInfo$State;
import androidx.work.b;
import java.util.UUID;
import android.content.Context;
import androidx.work.impl.WorkDatabase;

// 
// Decompiled by Procyon v0.6.0
// 

public class l92 implements e91
{
    public static final String c;
    public final WorkDatabase a;
    public final hu1 b;
    
    static {
        c = xl0.i("WorkProgressUpdater");
    }
    
    public l92(final WorkDatabase a, final hu1 b) {
        this.a = a;
        this.b = b;
    }
    
    @Override
    public ik0 a(final Context context, final UUID uuid, final b b) {
        final um1 s = um1.s();
        this.b.b(new Runnable(this, uuid, b, s) {
            public final UUID a;
            public final b b;
            public final um1 c;
            public final l92 d;
            
            @Override
            public void run() {
                final String string = this.a.toString();
                final xl0 e = xl0.e();
                final String c = l92.c;
                final StringBuilder sb = new StringBuilder();
                sb.append("Updating progress for ");
                sb.append(this.a);
                sb.append(" (");
                sb.append(this.b);
                sb.append(")");
                e.a(c, sb.toString());
                this.d.a.e();
                final Throwable t2;
                try {
                    final p92 s = this.d.a.K().s(string);
                    if (s != null) {
                        if (s.b == WorkInfo$State.RUNNING) {
                            this.d.a.J().b(new i92(string, this.b));
                        }
                        else {
                            final xl0 e2 = xl0.e();
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Ignoring setProgressAsync(...). WorkSpec (");
                            sb2.append(string);
                            sb2.append(") is not in a RUNNING state.");
                            e2.k(c, sb2.toString());
                        }
                        this.c.o(null);
                        this.d.a.D();
                        return;
                    }
                    throw new IllegalStateException("Calls to setProgressAsync() must complete before a ListenableWorker signals completion of work by returning an instance of Result.");
                }
                finally {
                    final xl0 xl0 = xl0.e();
                    final String s2 = l92.c;
                    final String s3 = "Error updating Worker progress";
                    final Throwable t = t2;
                    xl0.d(s2, s3, t);
                    final Runnable runnable = this;
                    final um1 um1 = runnable.c;
                    final Throwable t3 = t2;
                    um1.p(t3);
                }
                try {
                    final xl0 xl0 = xl0.e();
                    final String s2 = l92.c;
                    final String s3 = "Error updating Worker progress";
                    final Throwable t = t2;
                    xl0.d(s2, s3, t);
                    final Runnable runnable = this;
                    final um1 um1 = runnable.c;
                    final Throwable t3 = t2;
                    um1.p(t3);
                }
                finally {
                    this.d.a.i();
                }
            }
        });
        return s;
    }
}

import android.widget.AbsListView;
import android.view.AbsSavedState;
import android.widget.AbsSpinner;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import android.view.View$BaseSavedState;
import android.widget.PopupWindow$OnDismissListener;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemClickListener;
import android.database.DataSetObserver;
import android.content.DialogInterface;
import android.widget.ListView;
import android.util.Log;
import androidx.appcompat.app.a;
import android.content.DialogInterface$OnClickListener;
import android.widget.ThemedSpinnerAdapter;
import android.widget.ListAdapter;
import android.widget.Adapter;
import android.view.MotionEvent;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver$OnGlobalLayoutListener;
import android.os.Parcelable;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import android.view.View;
import android.view.ViewGroup$LayoutParams;
import android.view.ViewGroup;
import android.view.View$MeasureSpec;
import android.graphics.drawable.Drawable;
import android.content.res.Resources$Theme;
import android.util.AttributeSet;
import android.graphics.Rect;
import android.widget.SpinnerAdapter;
import android.content.Context;
import android.widget.Spinner;

// 
// Decompiled by Procyon v0.6.0
// 

public class j7 extends Spinner
{
    public static final int[] i;
    public final x5 a;
    public final Context b;
    public w70 c;
    public SpinnerAdapter d;
    public final boolean e;
    public j f;
    public int g;
    public final Rect h;
    
    static {
        i = new int[] { 16843505 };
    }
    
    public j7(final Context context, final AttributeSet set) {
        this(context, set, sa1.K);
    }
    
    public j7(final Context context, final AttributeSet set, final int n) {
        this(context, set, n, -1);
    }
    
    public j7(final Context context, final AttributeSet set, final int n, final int n2) {
        this(context, set, n, n2, null);
    }
    
    public j7(final Context p0, final AttributeSet p1, final int p2, final int p3, final Resources$Theme p4) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: aload_1        
        //     2: aload_2        
        //     3: iload_3        
        //     4: invokespecial   android/widget/Spinner.<init>:(Landroid/content/Context;Landroid/util/AttributeSet;I)V
        //     7: aload_0        
        //     8: new             Landroid/graphics/Rect;
        //    11: dup            
        //    12: invokespecial   android/graphics/Rect.<init>:()V
        //    15: putfield        j7.h:Landroid/graphics/Rect;
        //    18: aload_0        
        //    19: aload_0        
        //    20: invokevirtual   android/view/View.getContext:()Landroid/content/Context;
        //    23: invokestatic    pv1.a:(Landroid/view/View;Landroid/content/Context;)V
        //    26: aload_1        
        //    27: aload_2        
        //    28: getstatic       bc1.F2:[I
        //    31: iload_3        
        //    32: iconst_0       
        //    33: invokestatic    tw1.v:(Landroid/content/Context;Landroid/util/AttributeSet;[III)Ltw1;
        //    36: astore          9
        //    38: aload_0        
        //    39: new             Lx5;
        //    42: dup            
        //    43: aload_0        
        //    44: invokespecial   x5.<init>:(Landroid/view/View;)V
        //    47: putfield        j7.a:Lx5;
        //    50: aload           5
        //    52: ifnull          76
        //    55: new             Lul;
        //    58: dup            
        //    59: aload_1        
        //    60: aload           5
        //    62: invokespecial   ul.<init>:(Landroid/content/Context;Landroid/content/res/Resources$Theme;)V
        //    65: astore          5
        //    67: aload_0        
        //    68: aload           5
        //    70: putfield        j7.b:Landroid/content/Context;
        //    73: goto            112
        //    76: aload           9
        //    78: getstatic       bc1.K2:I
        //    81: iconst_0       
        //    82: invokevirtual   tw1.n:(II)I
        //    85: istore          6
        //    87: iload           6
        //    89: ifeq            107
        //    92: new             Lul;
        //    95: dup            
        //    96: aload_1        
        //    97: iload           6
        //    99: invokespecial   ul.<init>:(Landroid/content/Context;I)V
        //   102: astore          5
        //   104: goto            67
        //   107: aload_0        
        //   108: aload_1        
        //   109: putfield        j7.b:Landroid/content/Context;
        //   112: aconst_null    
        //   113: astore          7
        //   115: iload           4
        //   117: istore          6
        //   119: iload           4
        //   121: iconst_m1      
        //   122: if_icmpne       244
        //   125: aload_1        
        //   126: aload_2        
        //   127: getstatic       j7.i:[I
        //   130: iload_3        
        //   131: iconst_0       
        //   132: invokevirtual   android/content/Context.obtainStyledAttributes:(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
        //   135: astore          5
        //   137: iload           4
        //   139: istore          6
        //   141: aload           5
        //   143: astore          8
        //   145: aload           5
        //   147: astore          7
        //   149: aload           5
        //   151: iconst_0       
        //   152: invokevirtual   android/content/res/TypedArray.hasValue:(I)Z
        //   155: ifeq            175
        //   158: aload           5
        //   160: astore          7
        //   162: aload           5
        //   164: iconst_0       
        //   165: iconst_0       
        //   166: invokevirtual   android/content/res/TypedArray.getInt:(II)I
        //   169: istore          6
        //   171: aload           5
        //   173: astore          8
        //   175: aload           8
        //   177: invokevirtual   android/content/res/TypedArray.recycle:()V
        //   180: goto            244
        //   183: astore          8
        //   185: goto            197
        //   188: astore_1       
        //   189: goto            232
        //   192: astore          8
        //   194: aconst_null    
        //   195: astore          5
        //   197: aload           5
        //   199: astore          7
        //   201: ldc             "AppCompatSpinner"
        //   203: ldc             "Could not read android:spinnerMode"
        //   205: aload           8
        //   207: invokestatic    android/util/Log.i:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   210: pop            
        //   211: iload           4
        //   213: istore          6
        //   215: aload           5
        //   217: ifnull          244
        //   220: iload           4
        //   222: istore          6
        //   224: aload           5
        //   226: astore          8
        //   228: goto            175
        //   231: astore_1       
        //   232: aload           7
        //   234: ifnull          242
        //   237: aload           7
        //   239: invokevirtual   android/content/res/TypedArray.recycle:()V
        //   242: aload_1        
        //   243: athrow         
        //   244: iload           6
        //   246: ifeq            358
        //   249: iload           6
        //   251: iconst_1       
        //   252: if_icmpeq       258
        //   255: goto            389
        //   258: new             Lj7$h;
        //   261: dup            
        //   262: aload_0        
        //   263: aload_0        
        //   264: getfield        j7.b:Landroid/content/Context;
        //   267: aload_2        
        //   268: iload_3        
        //   269: invokespecial   j7$h.<init>:(Lj7;Landroid/content/Context;Landroid/util/AttributeSet;I)V
        //   272: astore          7
        //   274: aload_0        
        //   275: getfield        j7.b:Landroid/content/Context;
        //   278: aload_2        
        //   279: getstatic       bc1.F2:[I
        //   282: iload_3        
        //   283: iconst_0       
        //   284: invokestatic    tw1.v:(Landroid/content/Context;Landroid/util/AttributeSet;[III)Ltw1;
        //   287: astore          5
        //   289: aload_0        
        //   290: aload           5
        //   292: getstatic       bc1.J2:I
        //   295: bipush          -2
        //   297: invokevirtual   tw1.m:(II)I
        //   300: putfield        j7.g:I
        //   303: aload           7
        //   305: aload           5
        //   307: getstatic       bc1.H2:I
        //   310: invokevirtual   tw1.g:(I)Landroid/graphics/drawable/Drawable;
        //   313: invokevirtual   ek0.setBackgroundDrawable:(Landroid/graphics/drawable/Drawable;)V
        //   316: aload           7
        //   318: aload           9
        //   320: getstatic       bc1.I2:I
        //   323: invokevirtual   tw1.o:(I)Ljava/lang/String;
        //   326: invokevirtual   j7$h.l:(Ljava/lang/CharSequence;)V
        //   329: aload           5
        //   331: invokevirtual   tw1.w:()V
        //   334: aload_0        
        //   335: aload           7
        //   337: putfield        j7.f:Lj7$j;
        //   340: aload_0        
        //   341: new             Lj7$a;
        //   344: dup            
        //   345: aload_0        
        //   346: aload_0        
        //   347: aload           7
        //   349: invokespecial   j7$a.<init>:(Lj7;Landroid/view/View;Lj7$h;)V
        //   352: putfield        j7.c:Lw70;
        //   355: goto            389
        //   358: new             Lj7$f;
        //   361: dup            
        //   362: aload_0        
        //   363: invokespecial   j7$f.<init>:(Lj7;)V
        //   366: astore          5
        //   368: aload_0        
        //   369: aload           5
        //   371: putfield        j7.f:Lj7$j;
        //   374: aload           5
        //   376: aload           9
        //   378: getstatic       bc1.I2:I
        //   381: invokevirtual   tw1.o:(I)Ljava/lang/String;
        //   384: invokeinterface j7$j.l:(Ljava/lang/CharSequence;)V
        //   389: aload           9
        //   391: getstatic       bc1.G2:I
        //   394: invokevirtual   tw1.q:(I)[Ljava/lang/CharSequence;
        //   397: astore          5
        //   399: aload           5
        //   401: ifnull          429
        //   404: new             Landroid/widget/ArrayAdapter;
        //   407: dup            
        //   408: aload_1        
        //   409: ldc             17367048
        //   411: aload           5
        //   413: invokespecial   android/widget/ArrayAdapter.<init>:(Landroid/content/Context;I[Ljava/lang/Object;)V
        //   416: astore_1       
        //   417: aload_1        
        //   418: getstatic       ob1.t:I
        //   421: invokevirtual   android/widget/ArrayAdapter.setDropDownViewResource:(I)V
        //   424: aload_0        
        //   425: aload_1        
        //   426: invokevirtual   j7.setAdapter:(Landroid/widget/SpinnerAdapter;)V
        //   429: aload           9
        //   431: invokevirtual   tw1.w:()V
        //   434: aload_0        
        //   435: iconst_1       
        //   436: putfield        j7.e:Z
        //   439: aload_0        
        //   440: getfield        j7.d:Landroid/widget/SpinnerAdapter;
        //   443: astore_1       
        //   444: aload_1        
        //   445: ifnull          458
        //   448: aload_0        
        //   449: aload_1        
        //   450: invokevirtual   j7.setAdapter:(Landroid/widget/SpinnerAdapter;)V
        //   453: aload_0        
        //   454: aconst_null    
        //   455: putfield        j7.d:Landroid/widget/SpinnerAdapter;
        //   458: aload_0        
        //   459: getfield        j7.a:Lx5;
        //   462: aload_2        
        //   463: iload_3        
        //   464: invokevirtual   x5.e:(Landroid/util/AttributeSet;I)V
        //   467: return         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  125    137    192    197    Ljava/lang/Exception;
        //  125    137    188    192    Any
        //  149    158    183    188    Ljava/lang/Exception;
        //  149    158    231    232    Any
        //  162    171    183    188    Ljava/lang/Exception;
        //  162    171    231    232    Any
        //  201    211    231    232    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0175:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createConstructor(AstBuilder.java:799)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:635)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public int a(final SpinnerAdapter spinnerAdapter, final Drawable drawable) {
        int n = 0;
        if (spinnerAdapter == null) {
            return 0;
        }
        final int measureSpec = View$MeasureSpec.makeMeasureSpec(((View)this).getMeasuredWidth(), 0);
        final int measureSpec2 = View$MeasureSpec.makeMeasureSpec(((View)this).getMeasuredHeight(), 0);
        final int max = Math.max(0, ((AdapterView)this).getSelectedItemPosition());
        final int min = Math.min(((Adapter)spinnerAdapter).getCount(), max + 15);
        int i = Math.max(0, max - (15 - (min - max)));
        View view = null;
        int max2 = 0;
        while (i < min) {
            final int itemViewType = ((Adapter)spinnerAdapter).getItemViewType(i);
            int n2;
            if (itemViewType != (n2 = n)) {
                view = null;
                n2 = itemViewType;
            }
            view = ((Adapter)spinnerAdapter).getView(i, view, (ViewGroup)this);
            if (view.getLayoutParams() == null) {
                view.setLayoutParams(new ViewGroup$LayoutParams(-2, -2));
            }
            view.measure(measureSpec, measureSpec2);
            max2 = Math.max(max2, view.getMeasuredWidth());
            ++i;
            n = n2;
        }
        int n3 = max2;
        if (drawable != null) {
            drawable.getPadding(this.h);
            final Rect h = this.h;
            n3 = max2 + (h.left + h.right);
        }
        return n3;
    }
    
    public void b() {
        this.f.e(j7.d.b((View)this), j7.d.a((View)this));
    }
    
    public void drawableStateChanged() {
        super.drawableStateChanged();
        final x5 a = this.a;
        if (a != null) {
            a.b();
        }
    }
    
    public int getDropDownHorizontalOffset() {
        final j f = this.f;
        if (f != null) {
            return f.i();
        }
        return super.getDropDownHorizontalOffset();
    }
    
    public int getDropDownVerticalOffset() {
        final j f = this.f;
        if (f != null) {
            return f.f();
        }
        return super.getDropDownVerticalOffset();
    }
    
    public int getDropDownWidth() {
        if (this.f != null) {
            return this.g;
        }
        return super.getDropDownWidth();
    }
    
    public final j getInternalPopup() {
        return this.f;
    }
    
    public Drawable getPopupBackground() {
        final j f = this.f;
        if (f != null) {
            return f.getBackground();
        }
        return super.getPopupBackground();
    }
    
    public Context getPopupContext() {
        return this.b;
    }
    
    public CharSequence getPrompt() {
        final j f = this.f;
        CharSequence charSequence;
        if (f != null) {
            charSequence = f.k();
        }
        else {
            charSequence = super.getPrompt();
        }
        return charSequence;
    }
    
    public ColorStateList getSupportBackgroundTintList() {
        final x5 a = this.a;
        ColorStateList c;
        if (a != null) {
            c = a.c();
        }
        else {
            c = null;
        }
        return c;
    }
    
    public PorterDuff$Mode getSupportBackgroundTintMode() {
        final x5 a = this.a;
        PorterDuff$Mode d;
        if (a != null) {
            d = a.d();
        }
        else {
            d = null;
        }
        return d;
    }
    
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        final j f = this.f;
        if (f != null && f.a()) {
            this.f.dismiss();
        }
    }
    
    public void onMeasure(final int n, final int n2) {
        super.onMeasure(n, n2);
        if (this.f != null && View$MeasureSpec.getMode(n) == Integer.MIN_VALUE) {
            ((View)this).setMeasuredDimension(Math.min(Math.max(((View)this).getMeasuredWidth(), this.a(((AbsSpinner)this).getAdapter(), ((View)this).getBackground())), View$MeasureSpec.getSize(n)), ((View)this).getMeasuredHeight());
        }
    }
    
    public void onRestoreInstanceState(final Parcelable parcelable) {
        final i i = (i)parcelable;
        super.onRestoreInstanceState(((AbsSavedState)i).getSuperState());
        if (i.a) {
            final ViewTreeObserver viewTreeObserver = ((View)this).getViewTreeObserver();
            if (viewTreeObserver != null) {
                viewTreeObserver.addOnGlobalLayoutListener((ViewTreeObserver$OnGlobalLayoutListener)new ViewTreeObserver$OnGlobalLayoutListener(this) {
                    public final j7 a;
                    
                    public void onGlobalLayout() {
                        if (!this.a.getInternalPopup().a()) {
                            this.a.b();
                        }
                        final ViewTreeObserver viewTreeObserver = ((View)this.a).getViewTreeObserver();
                        if (viewTreeObserver != null) {
                            j7.c.a(viewTreeObserver, (ViewTreeObserver$OnGlobalLayoutListener)this);
                        }
                    }
                });
            }
        }
    }
    
    public Parcelable onSaveInstanceState() {
        final i i = new i(super.onSaveInstanceState());
        final j f = this.f;
        i.a = (f != null && f.a());
        return (Parcelable)i;
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        final w70 c = this.c;
        return (c != null && c.onTouch((View)this, motionEvent)) || super.onTouchEvent(motionEvent);
    }
    
    public boolean performClick() {
        final j f = this.f;
        if (f != null) {
            if (!f.a()) {
                this.b();
            }
            return true;
        }
        return super.performClick();
    }
    
    public void setAdapter(final SpinnerAdapter spinnerAdapter) {
        if (!this.e) {
            this.d = spinnerAdapter;
            return;
        }
        super.setAdapter(spinnerAdapter);
        if (this.f != null) {
            Context context;
            if ((context = this.b) == null) {
                context = ((View)this).getContext();
            }
            this.f.m((ListAdapter)new g(spinnerAdapter, context.getTheme()));
        }
    }
    
    public void setBackgroundDrawable(final Drawable backgroundDrawable) {
        super.setBackgroundDrawable(backgroundDrawable);
        final x5 a = this.a;
        if (a != null) {
            a.f(backgroundDrawable);
        }
    }
    
    public void setBackgroundResource(final int backgroundResource) {
        super.setBackgroundResource(backgroundResource);
        final x5 a = this.a;
        if (a != null) {
            a.g(backgroundResource);
        }
    }
    
    public void setDropDownHorizontalOffset(final int dropDownHorizontalOffset) {
        final j f = this.f;
        if (f != null) {
            f.d(dropDownHorizontalOffset);
            this.f.j(dropDownHorizontalOffset);
        }
        else {
            super.setDropDownHorizontalOffset(dropDownHorizontalOffset);
        }
    }
    
    public void setDropDownVerticalOffset(final int dropDownVerticalOffset) {
        final j f = this.f;
        if (f != null) {
            f.c(dropDownVerticalOffset);
        }
        else {
            super.setDropDownVerticalOffset(dropDownVerticalOffset);
        }
    }
    
    public void setDropDownWidth(final int n) {
        if (this.f != null) {
            this.g = n;
        }
        else {
            super.setDropDownWidth(n);
        }
    }
    
    public void setPopupBackgroundDrawable(final Drawable drawable) {
        final j f = this.f;
        if (f != null) {
            f.setBackgroundDrawable(drawable);
        }
        else {
            super.setPopupBackgroundDrawable(drawable);
        }
    }
    
    public void setPopupBackgroundResource(final int n) {
        this.setPopupBackgroundDrawable(g7.b(this.getPopupContext(), n));
    }
    
    public void setPrompt(final CharSequence prompt) {
        final j f = this.f;
        if (f != null) {
            f.l(prompt);
        }
        else {
            super.setPrompt(prompt);
        }
    }
    
    public void setSupportBackgroundTintList(final ColorStateList list) {
        final x5 a = this.a;
        if (a != null) {
            a.i(list);
        }
    }
    
    public void setSupportBackgroundTintMode(final PorterDuff$Mode porterDuff$Mode) {
        final x5 a = this.a;
        if (a != null) {
            a.j(porterDuff$Mode);
        }
    }
    
    public abstract static final class c
    {
        public static void a(final ViewTreeObserver viewTreeObserver, final ViewTreeObserver$OnGlobalLayoutListener viewTreeObserver$OnGlobalLayoutListener) {
            viewTreeObserver.removeOnGlobalLayoutListener(viewTreeObserver$OnGlobalLayoutListener);
        }
    }
    
    public abstract static final class d
    {
        public static int a(final View view) {
            return view.getTextAlignment();
        }
        
        public static int b(final View view) {
            return view.getTextDirection();
        }
        
        public static void c(final View view, final int textAlignment) {
            view.setTextAlignment(textAlignment);
        }
        
        public static void d(final View view, final int textDirection) {
            view.setTextDirection(textDirection);
        }
    }
    
    public abstract static final class e
    {
        public static void a(final ThemedSpinnerAdapter themedSpinnerAdapter, final Resources$Theme dropDownViewTheme) {
            if (!c11.a(themedSpinnerAdapter.getDropDownViewTheme(), dropDownViewTheme)) {
                themedSpinnerAdapter.setDropDownViewTheme(dropDownViewTheme);
            }
        }
    }
    
    public class f implements j, DialogInterface$OnClickListener
    {
        public a a;
        public ListAdapter b;
        public CharSequence c;
        public final j7 d;
        
        public f(final j7 d) {
            this.d = d;
        }
        
        @Override
        public boolean a() {
            final a a = this.a;
            return a != null && a.isShowing();
        }
        
        @Override
        public void c(final int n) {
            Log.e("AppCompatSpinner", "Cannot set vertical offset for MODE_DIALOG, ignoring");
        }
        
        @Override
        public void d(final int n) {
            Log.e("AppCompatSpinner", "Cannot set horizontal (original) offset for MODE_DIALOG, ignoring");
        }
        
        @Override
        public void dismiss() {
            final a a = this.a;
            if (a != null) {
                a.dismiss();
                this.a = null;
            }
        }
        
        @Override
        public void e(final int n, final int n2) {
            if (this.b == null) {
                return;
            }
            final a.a a = new a.a(this.d.getPopupContext());
            final CharSequence c = this.c;
            if (c != null) {
                a.setTitle(c);
            }
            final a create = a.setSingleChoiceItems(this.b, ((AdapterView)this.d).getSelectedItemPosition(), (DialogInterface$OnClickListener)this).create();
            this.a = create;
            final ListView d = create.d();
            j7.d.d((View)d, n);
            j7.d.c((View)d, n2);
            this.a.show();
        }
        
        @Override
        public int f() {
            return 0;
        }
        
        @Override
        public Drawable getBackground() {
            return null;
        }
        
        @Override
        public int i() {
            return 0;
        }
        
        @Override
        public void j(final int n) {
            Log.e("AppCompatSpinner", "Cannot set horizontal offset for MODE_DIALOG, ignoring");
        }
        
        @Override
        public CharSequence k() {
            return this.c;
        }
        
        @Override
        public void l(final CharSequence c) {
            this.c = c;
        }
        
        @Override
        public void m(final ListAdapter b) {
            this.b = b;
        }
        
        public void onClick(final DialogInterface dialogInterface, final int selection) {
            ((AdapterView)this.d).setSelection(selection);
            if (((AdapterView)this.d).getOnItemClickListener() != null) {
                ((AdapterView)this.d).performItemClick((View)null, selection, ((Adapter)this.b).getItemId(selection));
            }
            this.dismiss();
        }
        
        @Override
        public void setBackgroundDrawable(final Drawable drawable) {
            Log.e("AppCompatSpinner", "Cannot set popup background for MODE_DIALOG, ignoring");
        }
    }
    
    public interface j
    {
        boolean a();
        
        void c(final int p0);
        
        void d(final int p0);
        
        void dismiss();
        
        void e(final int p0, final int p1);
        
        int f();
        
        Drawable getBackground();
        
        int i();
        
        void j(final int p0);
        
        CharSequence k();
        
        void l(final CharSequence p0);
        
        void m(final ListAdapter p0);
        
        void setBackgroundDrawable(final Drawable p0);
    }
    
    public static class g implements ListAdapter, SpinnerAdapter
    {
        public SpinnerAdapter a;
        public ListAdapter b;
        
        public g(final SpinnerAdapter a, final Resources$Theme resources$Theme) {
            this.a = a;
            if (a instanceof ListAdapter) {
                this.b = (ListAdapter)a;
            }
            if (resources$Theme != null && a instanceof ThemedSpinnerAdapter) {
                e.a((ThemedSpinnerAdapter)a, resources$Theme);
            }
        }
        
        public boolean areAllItemsEnabled() {
            final ListAdapter b = this.b;
            return b == null || b.areAllItemsEnabled();
        }
        
        public int getCount() {
            final SpinnerAdapter a = this.a;
            int count;
            if (a == null) {
                count = 0;
            }
            else {
                count = ((Adapter)a).getCount();
            }
            return count;
        }
        
        public View getDropDownView(final int n, View dropDownView, final ViewGroup viewGroup) {
            final SpinnerAdapter a = this.a;
            if (a == null) {
                dropDownView = null;
            }
            else {
                dropDownView = a.getDropDownView(n, dropDownView, viewGroup);
            }
            return dropDownView;
        }
        
        public Object getItem(final int n) {
            final SpinnerAdapter a = this.a;
            Object item;
            if (a == null) {
                item = null;
            }
            else {
                item = ((Adapter)a).getItem(n);
            }
            return item;
        }
        
        public long getItemId(final int n) {
            final SpinnerAdapter a = this.a;
            long itemId;
            if (a == null) {
                itemId = -1L;
            }
            else {
                itemId = ((Adapter)a).getItemId(n);
            }
            return itemId;
        }
        
        public int getItemViewType(final int n) {
            return 0;
        }
        
        public View getView(final int n, final View view, final ViewGroup viewGroup) {
            return this.getDropDownView(n, view, viewGroup);
        }
        
        public int getViewTypeCount() {
            return 1;
        }
        
        public boolean hasStableIds() {
            final SpinnerAdapter a = this.a;
            return a != null && ((Adapter)a).hasStableIds();
        }
        
        public boolean isEmpty() {
            return this.getCount() == 0;
        }
        
        public boolean isEnabled(final int n) {
            final ListAdapter b = this.b;
            return b == null || b.isEnabled(n);
        }
        
        public void registerDataSetObserver(final DataSetObserver dataSetObserver) {
            final SpinnerAdapter a = this.a;
            if (a != null) {
                ((Adapter)a).registerDataSetObserver(dataSetObserver);
            }
        }
        
        public void unregisterDataSetObserver(final DataSetObserver dataSetObserver) {
            final SpinnerAdapter a = this.a;
            if (a != null) {
                ((Adapter)a).unregisterDataSetObserver(dataSetObserver);
            }
        }
    }
    
    public class h extends ek0 implements j
    {
        public CharSequence Q;
        public ListAdapter U;
        public final Rect V;
        public int W;
        public final j7 Y;
        
        public h(final j7 y, final Context context, final AttributeSet set, final int n) {
            this.Y = y;
            super(context, set, n);
            this.V = new Rect();
            this.A((View)y);
            this.G(true);
            this.M(0);
            this.I((AdapterView$OnItemClickListener)new AdapterView$OnItemClickListener(this, y) {
                public final j7 a;
                public final h b;
                
                public void onItemClick(final AdapterView adapterView, final View view, final int selection, final long n) {
                    ((AdapterView)this.b.Y).setSelection(selection);
                    if (((AdapterView)this.b.Y).getOnItemClickListener() != null) {
                        final h b = this.b;
                        ((AdapterView)b.Y).performItemClick(view, selection, ((Adapter)b.U).getItemId(selection));
                    }
                    this.b.dismiss();
                }
            });
        }
        
        public void Q() {
            final Drawable background = this.getBackground();
            int right;
            if (background != null) {
                background.getPadding(this.Y.h);
                if (u42.b((View)this.Y)) {
                    right = this.Y.h.right;
                }
                else {
                    right = -this.Y.h.left;
                }
            }
            else {
                final Rect h = this.Y.h;
                h.right = 0;
                h.left = 0;
                right = 0;
            }
            final int paddingLeft = ((View)this.Y).getPaddingLeft();
            final int paddingRight = ((View)this.Y).getPaddingRight();
            final int width = ((View)this.Y).getWidth();
            final j7 y = this.Y;
            final int g = y.g;
            Label_0243: {
                int max;
                if (g == -2) {
                    final int a = y.a((SpinnerAdapter)this.U, this.getBackground());
                    final int widthPixels = ((View)this.Y).getContext().getResources().getDisplayMetrics().widthPixels;
                    final Rect h2 = this.Y.h;
                    final int n = widthPixels - h2.left - h2.right;
                    int a2;
                    if ((a2 = a) > n) {
                        a2 = n;
                    }
                    max = Math.max(a2, width - paddingLeft - paddingRight);
                }
                else {
                    if (g != -1) {
                        this.C(g);
                        break Label_0243;
                    }
                    max = width - paddingLeft - paddingRight;
                }
                this.C(max);
            }
            int n2;
            if (u42.b((View)this.Y)) {
                n2 = right + (width - paddingRight - this.w() - this.R());
            }
            else {
                n2 = right + (paddingLeft + this.R());
            }
            this.j(n2);
        }
        
        public int R() {
            return this.W;
        }
        
        public boolean S(final View view) {
            return o32.T(view) && view.getGlobalVisibleRect(this.V);
        }
        
        @Override
        public void d(final int w) {
            this.W = w;
        }
        
        @Override
        public void e(final int n, final int n2) {
            final boolean a = this.a();
            this.Q();
            this.F(2);
            super.show();
            final ListView h = this.h();
            ((AbsListView)h).setChoiceMode(1);
            j7.d.d((View)h, n);
            j7.d.c((View)h, n2);
            this.N(((AdapterView)this.Y).getSelectedItemPosition());
            if (a) {
                return;
            }
            final ViewTreeObserver viewTreeObserver = ((View)this.Y).getViewTreeObserver();
            if (viewTreeObserver != null) {
                final ViewTreeObserver$OnGlobalLayoutListener viewTreeObserver$OnGlobalLayoutListener = (ViewTreeObserver$OnGlobalLayoutListener)new ViewTreeObserver$OnGlobalLayoutListener(this) {
                    public final h a;
                    
                    public void onGlobalLayout() {
                        final h a = this.a;
                        if (!a.S((View)a.Y)) {
                            this.a.dismiss();
                        }
                        else {
                            this.a.Q();
                            this.a.show();
                        }
                    }
                };
                viewTreeObserver.addOnGlobalLayoutListener((ViewTreeObserver$OnGlobalLayoutListener)viewTreeObserver$OnGlobalLayoutListener);
                this.H((PopupWindow$OnDismissListener)new PopupWindow$OnDismissListener(this, viewTreeObserver$OnGlobalLayoutListener) {
                    public final ViewTreeObserver$OnGlobalLayoutListener a;
                    public final h b;
                    
                    public void onDismiss() {
                        final ViewTreeObserver viewTreeObserver = ((View)this.b.Y).getViewTreeObserver();
                        if (viewTreeObserver != null) {
                            viewTreeObserver.removeGlobalOnLayoutListener(this.a);
                        }
                    }
                });
            }
        }
        
        @Override
        public CharSequence k() {
            return this.Q;
        }
        
        @Override
        public void l(final CharSequence q) {
            this.Q = q;
        }
        
        @Override
        public void m(final ListAdapter u) {
            super.m(u);
            this.U = u;
        }
    }
    
    public static class i extends View$BaseSavedState
    {
        public static final Parcelable$Creator<i> CREATOR;
        public boolean a;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$Creator() {
                public i a(final Parcel parcel) {
                    return new i(parcel);
                }
                
                public i[] b(final int n) {
                    return new i[n];
                }
            };
        }
        
        public i(final Parcel parcel) {
            super(parcel);
            this.a = (parcel.readByte() != 0);
        }
        
        public i(final Parcelable parcelable) {
            super(parcelable);
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            super.writeToParcel(parcel, n);
            parcel.writeByte((byte)(byte)(this.a ? 1 : 0));
        }
    }
}

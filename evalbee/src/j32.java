import android.os.Parcelable;
import android.text.TextUtils;
import android.os.Parcel;
import android.util.SparseIntArray;
import androidx.versionedparcelable.VersionedParcel;

// 
// Decompiled by Procyon v0.6.0
// 

public class j32 extends VersionedParcel
{
    public final SparseIntArray d;
    public final Parcel e;
    public final int f;
    public final int g;
    public final String h;
    public int i;
    public int j;
    public int k;
    
    public j32(final Parcel parcel) {
        this(parcel, parcel.dataPosition(), parcel.dataSize(), "", new r8(), new r8(), new r8());
    }
    
    public j32(final Parcel e, final int n, final int g, final String h, final r8 r8, final r8 r9, final r8 r10) {
        super(r8, r9, r10);
        this.d = new SparseIntArray();
        this.i = -1;
        this.k = -1;
        this.e = e;
        this.f = n;
        this.g = g;
        this.j = n;
        this.h = h;
    }
    
    @Override
    public void A(final byte[] array) {
        if (array != null) {
            this.e.writeInt(array.length);
            this.e.writeByteArray(array);
        }
        else {
            this.e.writeInt(-1);
        }
    }
    
    @Override
    public void C(final CharSequence charSequence) {
        TextUtils.writeToParcel(charSequence, this.e, 0);
    }
    
    @Override
    public void E(final int n) {
        this.e.writeInt(n);
    }
    
    @Override
    public void G(final Parcelable parcelable) {
        this.e.writeParcelable(parcelable, 0);
    }
    
    @Override
    public void I(final String s) {
        this.e.writeString(s);
    }
    
    @Override
    public void a() {
        final int i = this.i;
        if (i >= 0) {
            final int value = this.d.get(i);
            final int dataPosition = this.e.dataPosition();
            this.e.setDataPosition(value);
            this.e.writeInt(dataPosition - value);
            this.e.setDataPosition(dataPosition);
        }
    }
    
    @Override
    public VersionedParcel b() {
        final Parcel e = this.e;
        final int dataPosition = e.dataPosition();
        int n;
        if ((n = this.j) == this.f) {
            n = this.g;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(this.h);
        sb.append("  ");
        return new j32(e, dataPosition, n, sb.toString(), super.a, super.b, super.c);
    }
    
    @Override
    public boolean g() {
        return this.e.readInt() != 0;
    }
    
    @Override
    public byte[] i() {
        final int int1 = this.e.readInt();
        if (int1 < 0) {
            return null;
        }
        final byte[] array = new byte[int1];
        this.e.readByteArray(array);
        return array;
    }
    
    @Override
    public CharSequence k() {
        return (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(this.e);
    }
    
    @Override
    public boolean m(final int i) {
        while (true) {
            final int j = this.j;
            final int g = this.g;
            boolean b = true;
            if (j >= g) {
                if (this.k != i) {
                    b = false;
                }
                return b;
            }
            final int k = this.k;
            if (k == i) {
                return true;
            }
            if (String.valueOf(k).compareTo(String.valueOf(i)) > 0) {
                return false;
            }
            this.e.setDataPosition(this.j);
            final int int1 = this.e.readInt();
            this.k = this.e.readInt();
            this.j += int1;
        }
    }
    
    @Override
    public int o() {
        return this.e.readInt();
    }
    
    @Override
    public Parcelable q() {
        return this.e.readParcelable(this.getClass().getClassLoader());
    }
    
    @Override
    public String s() {
        return this.e.readString();
    }
    
    @Override
    public void w(final int i) {
        this.a();
        this.i = i;
        this.d.put(i, this.e.dataPosition());
        this.E(0);
        this.E(i);
    }
    
    @Override
    public void y(final boolean b) {
        this.e.writeInt((int)(b ? 1 : 0));
    }
}

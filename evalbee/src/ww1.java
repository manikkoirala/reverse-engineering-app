import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import java.io.UnsupportedEncodingException;
import android.util.Base64;
import com.google.android.gms.common.internal.Preconditions;
import java.util.Collections;
import org.json.JSONObject;
import android.text.TextUtils;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class ww1
{
    public static Map a(final String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return null;
        }
        try {
            final JSONObject jsonObject = new JSONObject(s);
            if (jsonObject != JSONObject.NULL) {
                return d(jsonObject);
            }
            return null;
        }
        catch (final Exception obj) {
            final yl0 f = yl0.f();
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed to parse JSONObject into Map:\n");
            sb.append(obj);
            f.b(sb.toString());
            return Collections.emptyMap();
        }
    }
    
    public static Map b(String str) {
        Preconditions.checkNotEmpty(str);
        final String[] split = str.split("\\.", -1);
        StringBuilder sb;
        yl0 f2;
        if (split.length < 2) {
            final yl0 f = yl0.f();
            sb = new StringBuilder();
            sb.append("Invalid token (too few subsections):\n");
            sb.append(str);
            f2 = f;
        }
        else {
            str = split[1];
            try {
                Map<Object, Object> map;
                if ((map = a(new String(Base64.decode(str, 11), "UTF-8"))) == null) {
                    map = Collections.emptyMap();
                }
                return map;
            }
            catch (final UnsupportedEncodingException obj) {
                f2 = yl0.f();
                sb = new StringBuilder();
                sb.append("Unable to decode token (charset unknown):\n");
                sb.append(obj);
            }
        }
        f2.d(sb.toString());
        return Collections.emptyMap();
    }
    
    public static List c(final JSONArray jsonArray) {
        final ArrayList list = new ArrayList();
        for (int i = 0; i < jsonArray.length(); ++i) {
            final Object value = jsonArray.get(i);
            Object o;
            if (value instanceof JSONArray) {
                o = c((JSONArray)value);
            }
            else {
                o = value;
                if (value instanceof JSONObject) {
                    o = d((JSONObject)value);
                }
            }
            list.add(o);
        }
        return list;
    }
    
    public static Map d(final JSONObject jsonObject) {
        final r8 r8 = new r8();
        final Iterator keys = jsonObject.keys();
        while (keys.hasNext()) {
            final String s = keys.next();
            final Object value = jsonObject.get(s);
            Object o;
            if (value instanceof JSONArray) {
                o = c((JSONArray)value);
            }
            else if (value instanceof JSONObject) {
                o = d((JSONObject)value);
            }
            else {
                o = value;
                if (value.equals(JSONObject.NULL)) {
                    o = null;
                }
            }
            r8.put(s, (List)o);
        }
        return r8;
    }
}

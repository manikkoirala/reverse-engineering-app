import android.app.PendingIntent;
import androidx.work.impl.background.systemalarm.a;
import android.app.AlarmManager;
import androidx.work.impl.WorkDatabase;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class b4
{
    public static final String a;
    
    static {
        a = xl0.i("Alarms");
    }
    
    public static void a(final Context context, final WorkDatabase workDatabase, final x82 obj) {
        final gt1 h = workDatabase.H();
        final ft1 e = h.e(obj);
        if (e != null) {
            b(context, obj, e.c);
            final xl0 e2 = xl0.e();
            final String a = b4.a;
            final StringBuilder sb = new StringBuilder();
            sb.append("Removing SystemIdInfo for workSpecId (");
            sb.append(obj);
            sb.append(")");
            e2.a(a, sb.toString());
            h.a(obj);
        }
    }
    
    public static void b(final Context context, final x82 obj, final int i) {
        final AlarmManager alarmManager = (AlarmManager)context.getSystemService("alarm");
        final PendingIntent service = PendingIntent.getService(context, i, androidx.work.impl.background.systemalarm.a.b(context, obj), 603979776);
        if (service != null && alarmManager != null) {
            final xl0 e = xl0.e();
            final String a = b4.a;
            final StringBuilder sb = new StringBuilder();
            sb.append("Cancelling existing alarm with (workSpecId, systemId) (");
            sb.append(obj);
            sb.append(", ");
            sb.append(i);
            sb.append(")");
            e.a(a, sb.toString());
            alarmManager.cancel(service);
        }
    }
    
    public static void c(final Context context, final WorkDatabase workDatabase, final x82 x82, final long n) {
        final gt1 h = workDatabase.H();
        final ft1 e = h.e(x82);
        int n2;
        if (e != null) {
            b(context, x82, e.c);
            n2 = e.c;
        }
        else {
            n2 = new be0(workDatabase).c();
            h.g(it1.a(x82, n2));
        }
        d(context, x82, n2, n);
    }
    
    public static void d(final Context context, final x82 x82, final int n, final long n2) {
        final AlarmManager alarmManager = (AlarmManager)context.getSystemService("alarm");
        final PendingIntent service = PendingIntent.getService(context, n, androidx.work.impl.background.systemalarm.a.b(context, x82), 201326592);
        if (alarmManager != null) {
            b4.a.a(alarmManager, 0, n2, service);
        }
    }
    
    public abstract static class a
    {
        public static void a(final AlarmManager alarmManager, final int n, final long n2, final PendingIntent pendingIntent) {
            alarmManager.setExact(n, n2, pendingIntent);
        }
    }
}

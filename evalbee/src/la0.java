import com.google.firebase.installations.local.b;
import com.google.android.gms.tasks.TaskCompletionSource;

// 
// Decompiled by Procyon v0.6.0
// 

public class la0 implements yp1
{
    public final u22 a;
    public final TaskCompletionSource b;
    
    public la0(final u22 a, final TaskCompletionSource b) {
        this.a = a;
        this.b = b;
    }
    
    @Override
    public boolean a(final Exception ex) {
        this.b.trySetException(ex);
        return true;
    }
    
    @Override
    public boolean b(final b b) {
        if (b.k() && !this.a.f(b)) {
            this.b.setResult((Object)pf0.a().b(b.b()).d(b.c()).c(b.h()).a());
            return true;
        }
        return false;
    }
}

import com.google.android.gms.common.internal.Objects;
import android.text.TextUtils;
import com.google.android.gms.common.internal.StringResourceValueReader;
import android.content.Context;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.Strings;

// 
// Decompiled by Procyon v0.6.0
// 

public final class e30
{
    public final String a;
    public final String b;
    public final String c;
    public final String d;
    public final String e;
    public final String f;
    public final String g;
    
    public e30(final String b, final String a, final String c, final String d, final String e, final String f, final String g) {
        Preconditions.checkState(Strings.isEmptyOrWhitespace(b) ^ true, (Object)"ApplicationId must be set.");
        this.b = b;
        this.a = a;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
    }
    
    public static e30 a(final Context context) {
        final StringResourceValueReader stringResourceValueReader = new StringResourceValueReader(context);
        final String string = stringResourceValueReader.getString("google_app_id");
        if (TextUtils.isEmpty((CharSequence)string)) {
            return null;
        }
        return new e30(string, stringResourceValueReader.getString("google_api_key"), stringResourceValueReader.getString("firebase_database_url"), stringResourceValueReader.getString("ga_trackingId"), stringResourceValueReader.getString("gcm_defaultSenderId"), stringResourceValueReader.getString("google_storage_bucket"), stringResourceValueReader.getString("project_id"));
    }
    
    public String b() {
        return this.a;
    }
    
    public String c() {
        return this.b;
    }
    
    public String d() {
        return this.e;
    }
    
    public String e() {
        return this.g;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof e30;
        final boolean b2 = false;
        if (!b) {
            return false;
        }
        final e30 e30 = (e30)o;
        boolean b3 = b2;
        if (Objects.equal(this.b, e30.b)) {
            b3 = b2;
            if (Objects.equal(this.a, e30.a)) {
                b3 = b2;
                if (Objects.equal(this.c, e30.c)) {
                    b3 = b2;
                    if (Objects.equal(this.d, e30.d)) {
                        b3 = b2;
                        if (Objects.equal(this.e, e30.e)) {
                            b3 = b2;
                            if (Objects.equal(this.f, e30.f)) {
                                b3 = b2;
                                if (Objects.equal(this.g, e30.g)) {
                                    b3 = true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return b3;
    }
    
    public String f() {
        return this.f;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.b, this.a, this.c, this.d, this.e, this.f, this.g);
    }
    
    @Override
    public String toString() {
        return Objects.toStringHelper(this).add("applicationId", this.b).add("apiKey", this.a).add("databaseUrl", this.c).add("gcmSenderId", this.e).add("storageBucket", this.f).add("projectId", this.g).toString();
    }
}

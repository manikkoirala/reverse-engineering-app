import android.content.res.Resources$Theme;
import android.widget.TextView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import java.util.ArrayList;
import android.content.Context;
import android.widget.SpinnerAdapter;
import android.widget.BaseAdapter;

// 
// Decompiled by Procyon v0.6.0
// 

public class p3 extends BaseAdapter implements SpinnerAdapter
{
    public Context a;
    public ArrayList b;
    
    public p3(final Context a, final ArrayList b) {
        this.a = a;
        this.b = b;
    }
    
    public int getCount() {
        return this.b.size();
    }
    
    public View getDropDownView(final int n, View inflate, final ViewGroup viewGroup) {
        inflate = ((LayoutInflater)this.a.getSystemService("layout_inflater")).inflate(2131493016, (ViewGroup)null);
        final TextView textView = (TextView)inflate.findViewById(2131297286);
        textView.setText((CharSequence)this.b.get(n));
        textView.setPadding(a91.d(15, this.a), a91.d(15, this.a), a91.d(15, this.a), a91.d(15, this.a));
        if (this.b.get(n).equals(ok.b)) {
            textView.setTextColor(this.a.getResources().getColor(2131099724, (Resources$Theme)null));
        }
        return inflate;
    }
    
    public Object getItem(final int index) {
        return this.b.get(index);
    }
    
    public long getItemId(final int n) {
        return n;
    }
    
    public View getView(final int index, final View view, final ViewGroup viewGroup) {
        final String text = this.b.get(index);
        final View inflate = ((LayoutInflater)this.a.getSystemService("layout_inflater")).inflate(2131493016, (ViewGroup)null);
        ((TextView)inflate.findViewById(2131297286)).setText((CharSequence)text);
        return inflate;
    }
}

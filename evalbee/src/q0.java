import android.view.accessibility.AccessibilityEvent;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class q0
{
    public static int a(final AccessibilityEvent accessibilityEvent) {
        return a.a(accessibilityEvent);
    }
    
    public static void b(final AccessibilityEvent accessibilityEvent, final int n) {
        a.b(accessibilityEvent, n);
    }
    
    public abstract static class a
    {
        public static int a(final AccessibilityEvent accessibilityEvent) {
            return accessibilityEvent.getContentChangeTypes();
        }
        
        public static void b(final AccessibilityEvent accessibilityEvent, final int contentChangeTypes) {
            accessibilityEvent.setContentChangeTypes(contentChangeTypes);
        }
    }
}

import java.util.concurrent.Executor;

// 
// Decompiled by Procyon v0.6.0
// 

public class b8 extends iu1
{
    public static volatile b8 c;
    public static final Executor d;
    public static final Executor e;
    public iu1 a;
    public final iu1 b;
    
    static {
        d = new z7();
        e = new a8();
    }
    
    public b8() {
        final dr dr = new dr();
        this.b = dr;
        this.a = dr;
    }
    
    public static Executor f() {
        return b8.e;
    }
    
    public static b8 g() {
        if (b8.c != null) {
            return b8.c;
        }
        synchronized (b8.class) {
            if (b8.c == null) {
                b8.c = new b8();
            }
            return b8.c;
        }
    }
    
    @Override
    public void a(final Runnable runnable) {
        this.a.a(runnable);
    }
    
    @Override
    public boolean b() {
        return this.a.b();
    }
    
    @Override
    public void c(final Runnable runnable) {
        this.a.c(runnable);
    }
}

import java.lang.ref.WeakReference;
import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.widgets.b;
import androidx.constraintlayout.core.widgets.d;
import java.util.Iterator;
import androidx.constraintlayout.core.c;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import java.util.ArrayList;

// 
// Decompiled by Procyon v0.6.0
// 

public class b62
{
    public static int g;
    public ArrayList a;
    public int b;
    public boolean c;
    public int d;
    public ArrayList e;
    public int f;
    
    public b62(final int d) {
        this.a = new ArrayList();
        this.c = false;
        this.e = null;
        this.f = -1;
        final int g = b62.g;
        b62.g = g + 1;
        this.b = g;
        this.d = d;
    }
    
    public boolean a(final ConstraintWidget constraintWidget) {
        if (this.a.contains(constraintWidget)) {
            return false;
        }
        this.a.add(constraintWidget);
        return true;
    }
    
    public void b(final ArrayList list) {
        final int size = this.a.size();
        if (this.f != -1 && size > 0) {
            for (int i = 0; i < list.size(); ++i) {
                final b62 b62 = list.get(i);
                if (this.f == b62.b) {
                    this.g(this.d, b62);
                }
            }
        }
        if (size == 0) {
            list.remove(this);
        }
    }
    
    public int c() {
        return this.b;
    }
    
    public int d() {
        return this.d;
    }
    
    public final String e() {
        final int d = this.d;
        if (d == 0) {
            return "Horizontal";
        }
        if (d == 1) {
            return "Vertical";
        }
        if (d == 2) {
            return "Both";
        }
        return "Unknown";
    }
    
    public int f(final c c, final int n) {
        if (this.a.size() == 0) {
            return 0;
        }
        return this.j(c, this.a, n);
    }
    
    public void g(final int n, final b62 b62) {
        for (final ConstraintWidget constraintWidget : this.a) {
            b62.a(constraintWidget);
            final int c = b62.c();
            if (n == 0) {
                constraintWidget.I0 = c;
            }
            else {
                constraintWidget.J0 = c;
            }
        }
        this.f = b62.b;
    }
    
    public void h(final boolean c) {
        this.c = c;
    }
    
    public void i(final int d) {
        this.d = d;
    }
    
    public final int j(final c c, final ArrayList list, int n) {
        final int n2 = 0;
        final d d = (d)list.get(0).K();
        c.E();
        d.g(c, false);
        for (int i = 0; i < list.size(); ++i) {
            ((ConstraintWidget)list.get(i)).g(c, false);
        }
        if (n == 0 && d.W0 > 0) {
            androidx.constraintlayout.core.widgets.b.b(d, c, list, 0);
        }
        if (n == 1 && d.X0 > 0) {
            androidx.constraintlayout.core.widgets.b.b(d, c, list, 1);
        }
        try {
            c.A();
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
        this.e = new ArrayList();
        for (int j = n2; j < list.size(); ++j) {
            this.e.add(new a((ConstraintWidget)list.get(j), c, n));
        }
        ConstraintAnchor constraintAnchor;
        if (n == 0) {
            n = c.y(d.O);
            constraintAnchor = d.Q;
        }
        else {
            n = c.y(d.P);
            constraintAnchor = d.R;
        }
        final int y = c.y(constraintAnchor);
        c.E();
        return y - n;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.e());
        sb.append(" [");
        sb.append(this.b);
        sb.append("] <");
        String s = sb.toString();
        for (final ConstraintWidget constraintWidget : this.a) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(s);
            sb2.append(" ");
            sb2.append(constraintWidget.t());
            s = sb2.toString();
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(s);
        sb3.append(" >");
        return sb3.toString();
    }
    
    public class a
    {
        public WeakReference a;
        public int b;
        public int c;
        public int d;
        public int e;
        public int f;
        public int g;
        public final b62 h;
        
        public a(final b62 h, final ConstraintWidget referent, final c c, final int g) {
            this.h = h;
            this.a = new WeakReference((T)referent);
            this.b = c.y(referent.O);
            this.c = c.y(referent.P);
            this.d = c.y(referent.Q);
            this.e = c.y(referent.R);
            this.f = c.y(referent.S);
            this.g = g;
        }
    }
}

import android.graphics.Matrix;
import android.view.View;

// 
// Decompiled by Procyon v0.6.0
// 

public class f52 extends e52
{
    @Override
    public float b(final View view) {
        return w42.a(view);
    }
    
    @Override
    public void d(final View view, final int n, final int n2, final int n3, final int n4) {
        b52.a(view, n, n2, n3, n4);
    }
    
    @Override
    public void e(final View view, final float n) {
        v42.a(view, n);
    }
    
    @Override
    public void f(final View view, final int n) {
        d52.a(view, n);
    }
    
    @Override
    public void g(final View view, final Matrix matrix) {
        z42.a(view, matrix);
    }
    
    @Override
    public void h(final View view, final Matrix matrix) {
        y42.a(view, matrix);
    }
}

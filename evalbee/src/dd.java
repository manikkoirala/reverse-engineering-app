import android.app.Dialog;
import java.util.Iterator;
import com.ekodroid.omrevaluator.database.ClassDataModel;
import java.util.Collection;
import java.util.HashSet;
import com.ekodroid.omrevaluator.database.repositories.ClassRepository;
import com.ekodroid.omrevaluator.database.StudentDataModel;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import java.util.ArrayList;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class dd
{
    public Context a;
    public ArrayList b;
    public y01 c;
    
    public dd(final Context a, final ArrayList b, final y01 c) {
        new ArrayList();
        this.a = a;
        this.b = b;
        this.c = c;
        new b(null).execute((Object[])new Void[0]);
    }
    
    public static /* synthetic */ Context a(final dd dd) {
        return dd.a;
    }
    
    public static /* synthetic */ y01 b(final dd dd) {
        return dd.c;
    }
    
    public class b extends AsyncTask
    {
        public ProgressDialog a;
        public final dd b;
        
        public b(final dd b) {
            this.b = b;
        }
        
        public Void a(final Void... array) {
            final ArrayList c = new ArrayList();
            for (final StudentDataModel studentDataModel : this.b.b) {
                ClassRepository.getInstance(dd.a(this.b)).deleteStudent(studentDataModel.getRollNO(), studentDataModel.getClassName());
                ClassRepository.getInstance(dd.a(this.b)).saveOrUpdateStudent(studentDataModel);
                c.add(studentDataModel.getClassName());
            }
            final ClassRepository instance = ClassRepository.getInstance(dd.a(this.b));
            final Iterator iterator2 = new HashSet(c).iterator();
            while (iterator2.hasNext()) {
                instance.saveOrUpdateClass(new ClassDataModel((String)iterator2.next(), null, false));
            }
            return null;
        }
        
        public void b(final Void void1) {
            if (((Dialog)this.a).isShowing()) {
                ((Dialog)this.a).dismiss();
            }
            if (dd.b(this.b) != null) {
                dd.b(this.b).a(null);
            }
        }
        
        public void onPreExecute() {
            (this.a = new ProgressDialog(dd.a(this.b))).setMessage((CharSequence)"Importing students, please wait...");
            ((Dialog)this.a).setCancelable(false);
            ((Dialog)this.a).show();
        }
    }
}

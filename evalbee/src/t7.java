import android.os.BaseBundle;
import java.util.List;
import java.util.Iterator;
import android.os.Bundle;
import android.content.pm.PackageManager$NameNotFoundException;
import androidx.startup.StartupException;
import android.content.ComponentName;
import androidx.startup.InitializationProvider;
import java.util.HashMap;
import java.util.HashSet;
import android.content.Context;
import java.util.Set;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public final class t7
{
    public static volatile t7 d;
    public static final Object e;
    public final Map a;
    public final Set b;
    public final Context c;
    
    static {
        e = new Object();
    }
    
    public t7(final Context context) {
        this.c = context.getApplicationContext();
        this.b = new HashSet();
        this.a = new HashMap();
    }
    
    public static t7 e(final Context context) {
        if (t7.d == null) {
            synchronized (t7.e) {
                if (t7.d == null) {
                    t7.d = new t7(context);
                }
            }
        }
        return t7.d;
    }
    
    public void a() {
        try {
            try {
                hy1.a("Startup");
                this.b(this.c.getPackageManager().getProviderInfo(new ComponentName(this.c.getPackageName(), InitializationProvider.class.getName()), 128).metaData);
                hy1.b();
                return;
            }
            finally {}
        }
        catch (final PackageManager$NameNotFoundException ex) {
            throw new StartupException((Throwable)ex);
        }
        hy1.b();
    }
    
    public void b(final Bundle bundle) {
        final String string = this.c.getString(pb1.a);
        if (bundle != null) {
            try {
                final HashSet set = new HashSet();
                for (final String className : ((BaseBundle)bundle).keySet()) {
                    if (string.equals(((BaseBundle)bundle).getString(className, (String)null))) {
                        final Class<?> forName = Class.forName(className);
                        if (!af0.class.isAssignableFrom(forName)) {
                            continue;
                        }
                        this.b.add(forName);
                    }
                }
                final Iterator iterator2 = this.b.iterator();
                while (iterator2.hasNext()) {
                    this.d((Class)iterator2.next(), set);
                }
            }
            catch (final ClassNotFoundException ex) {
                throw new StartupException(ex);
            }
        }
    }
    
    public Object c(final Class clazz) {
        synchronized (t7.e) {
            Object o;
            if ((o = this.a.get(clazz)) == null) {
                o = this.d(clazz, new HashSet());
            }
            return o;
        }
    }
    
    public final Object d(final Class clazz, final Set set) {
        Label_0013: {
            if (!hy1.d()) {
                break Label_0013;
            }
            try {
                hy1.a(clazz.getSimpleName());
                if (!set.contains(clazz)) {
                    if (!this.a.containsKey(clazz)) {
                        set.add(clazz);
                        try {
                            final af0 af0 = clazz.getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
                            final List dependencies = af0.dependencies();
                            if (!dependencies.isEmpty()) {
                                for (final Class clazz2 : dependencies) {
                                    if (!this.a.containsKey(clazz2)) {
                                        this.d(clazz2, set);
                                    }
                                }
                            }
                            final Object a = af0.a(this.c);
                            set.remove(clazz);
                            this.a.put(clazz, a);
                            return;
                        }
                        finally {
                            final Throwable t;
                            throw new StartupException(t);
                        }
                    }
                    return this.a.get(clazz);
                }
                throw new IllegalStateException(String.format("Cannot initialize %s. Cycle detected.", clazz.getName()));
            }
            finally {
                hy1.b();
            }
        }
    }
    
    public Object f(final Class clazz) {
        return this.c(clazz);
    }
    
    public boolean g(final Class clazz) {
        return this.b.contains(clazz);
    }
}

import android.os.BaseBundle;
import android.os.Bundle;
import com.google.android.gms.measurement.internal.zzii;
import com.google.android.gms.measurement.internal.zzij;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class lc2
{
    public static final ImmutableSet a;
    public static final ImmutableList b;
    public static final ImmutableList c;
    public static final ImmutableList d;
    public static final ImmutableList e;
    public static final ImmutableList f;
    
    static {
        a = ImmutableSet.of("_in", "_xa", "_xu", "_aq", "_aa", "_ai", "_ac", "campaign_details", "_ug", "_iapx", "_exp_set", "_exp_clear", "_exp_activate", "_exp_timeout", "_exp_expire");
        b = ImmutableList.of("_e", "_f", "_iap", "_s", "_au", "_ui", "_cd");
        c = ImmutableList.of("auto", "app", "am");
        d = ImmutableList.of("_r", "_dbg");
        e = new ImmutableList.a().j((Object[])zzij.zza).j((Object[])zzij.zzb).l();
        f = ImmutableList.of("^_ltv_[A-Z]{3}$", "^_cc[1-5]{1}$");
    }
    
    public static String a(final String s) {
        final String zza = zzii.zza(s);
        if (zza != null) {
            return zza;
        }
        return s;
    }
    
    public static void b(final String anObject, final String anObject2, final Bundle bundle) {
        if ("clx".equals(anObject) && "_ae".equals(anObject2)) {
            ((BaseBundle)bundle).putLong("_r", 1L);
        }
    }
    
    public static boolean c(final String s, final Bundle bundle) {
        if (lc2.b.contains(s)) {
            return false;
        }
        if (bundle != null) {
            final ImmutableList d = lc2.d;
            final int size = d.size();
            int i = 0;
            while (i < size) {
                final Object value = d.get(i);
                ++i;
                if (((BaseBundle)bundle).containsKey((String)value)) {
                    return false;
                }
            }
        }
        return true;
    }
    
    public static boolean d(final String s, final String anObject) {
        if ("_ce1".equals(anObject) || "_ce2".equals(anObject)) {
            return s.equals("fcm") || s.equals("frc");
        }
        if ("_ln".equals(anObject)) {
            return s.equals("fcm") || s.equals("fiam");
        }
        if (lc2.e.contains(anObject)) {
            return false;
        }
        final ImmutableList f = lc2.f;
        final int size = f.size();
        int i = 0;
        while (i < size) {
            final Object value = f.get(i);
            ++i;
            if (anObject.matches((String)value)) {
                return false;
            }
        }
        return true;
    }
    
    public static boolean e(String s, final String anObject, final Bundle bundle) {
        if (!"_cmp".equals(anObject)) {
            return true;
        }
        if (!g(s)) {
            return false;
        }
        if (bundle == null) {
            return false;
        }
        final ImmutableList d = lc2.d;
        final int size = d.size();
        int i = 0;
        while (i < size) {
            final Object value = d.get(i);
            ++i;
            if (((BaseBundle)bundle).containsKey((String)value)) {
                return false;
            }
        }
        s.hashCode();
        final int hashCode = s.hashCode();
        int n = -1;
        switch (hashCode) {
            case 3142703: {
                if (!s.equals("fiam")) {
                    break;
                }
                n = 2;
                break;
            }
            case 101230: {
                if (!s.equals("fdl")) {
                    break;
                }
                n = 1;
                break;
            }
            case 101200: {
                if (!s.equals("fcm")) {
                    break;
                }
                n = 0;
                break;
            }
        }
        switch (n) {
            default: {
                return false;
            }
            case 2: {
                s = "fiam_integration";
                break;
            }
            case 1: {
                s = "fdl_integration";
                break;
            }
            case 0: {
                s = "fcm_integration";
                break;
            }
        }
        ((BaseBundle)bundle).putString("_cis", s);
        return true;
    }
    
    public static boolean f(final String s) {
        return !lc2.a.contains(s);
    }
    
    public static boolean g(final String s) {
        return !lc2.c.contains(s);
    }
}

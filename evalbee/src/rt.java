import java.util.List;
import android.graphics.Rect;
import android.os.Build$VERSION;
import android.view.DisplayCutout;

// 
// Decompiled by Procyon v0.6.0
// 

public final class rt
{
    public final DisplayCutout a;
    
    public rt(final DisplayCutout a) {
        this.a = a;
    }
    
    public static rt e(final DisplayCutout displayCutout) {
        rt rt;
        if (displayCutout == null) {
            rt = null;
        }
        else {
            rt = new rt(displayCutout);
        }
        return rt;
    }
    
    public int a() {
        if (Build$VERSION.SDK_INT >= 28) {
            return rt.a.c(this.a);
        }
        return 0;
    }
    
    public int b() {
        if (Build$VERSION.SDK_INT >= 28) {
            return rt.a.d(this.a);
        }
        return 0;
    }
    
    public int c() {
        if (Build$VERSION.SDK_INT >= 28) {
            return rt.a.e(this.a);
        }
        return 0;
    }
    
    public int d() {
        if (Build$VERSION.SDK_INT >= 28) {
            return rt.a.f(this.a);
        }
        return 0;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o != null && rt.class == o.getClass() && c11.a(this.a, ((rt)o).a));
    }
    
    @Override
    public int hashCode() {
        final DisplayCutout a = this.a;
        int a2;
        if (a == null) {
            a2 = 0;
        }
        else {
            a2 = qt.a(a);
        }
        return a2;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("DisplayCutoutCompat{");
        sb.append(this.a);
        sb.append("}");
        return sb.toString();
    }
    
    public abstract static class a
    {
        public static DisplayCutout a(final Rect rect, final List<Rect> list) {
            return new DisplayCutout(rect, (List)list);
        }
        
        public static List<Rect> b(final DisplayCutout displayCutout) {
            return displayCutout.getBoundingRects();
        }
        
        public static int c(final DisplayCutout displayCutout) {
            return displayCutout.getSafeInsetBottom();
        }
        
        public static int d(final DisplayCutout displayCutout) {
            return displayCutout.getSafeInsetLeft();
        }
        
        public static int e(final DisplayCutout displayCutout) {
            return displayCutout.getSafeInsetRight();
        }
        
        public static int f(final DisplayCutout displayCutout) {
            return displayCutout.getSafeInsetTop();
        }
    }
}

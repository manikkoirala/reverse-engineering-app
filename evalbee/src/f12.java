// 
// Decompiled by Procyon v0.6.0
// 

public abstract class f12
{
    public static byte a(final long n) {
        i71.j(n >> 8 == 0L, "out of range: %s", n);
        return (byte)n;
    }
    
    public static int b(final byte b, final byte b2) {
        return c(b) - c(b2);
    }
    
    public static int c(final byte b) {
        return b & 0xFF;
    }
}

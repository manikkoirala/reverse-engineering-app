import java.util.concurrent.Callable;
import android.util.Base64;
import com.google.android.gms.common.util.PlatformVersion;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.Tasks;
import android.util.Log;
import com.google.android.gms.tasks.Task;
import android.content.Intent;
import java.util.concurrent.Executor;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class i00
{
    public static final Object c;
    public static h82 d;
    public final Context a;
    public final Executor b;
    
    static {
        c = new Object();
    }
    
    public i00(final Context a) {
        this.a = a;
        this.b = new xu0();
    }
    
    public static Task e(final Context context, final Intent intent, final boolean b) {
        if (Log.isLoggable("FirebaseMessaging", 3)) {
            Log.d("FirebaseMessaging", "Binding to service");
        }
        final h82 f = f(context, "com.google.firebase.MESSAGING_EVENT");
        if (b) {
            if (sl1.b().e(context)) {
                r52.f(context, f, intent);
            }
            else {
                f.c(intent);
            }
            return Tasks.forResult((Object)(-1));
        }
        return f.c(intent).continueWith((Executor)new xu0(), (Continuation)new h00());
    }
    
    public static h82 f(final Context context, final String s) {
        synchronized (i00.c) {
            if (i00.d == null) {
                i00.d = new h82(context, s);
            }
            return i00.d;
        }
    }
    
    public Task k(final Intent intent) {
        final String stringExtra = intent.getStringExtra("gcm.rawData64");
        if (stringExtra != null) {
            intent.putExtra("rawData", Base64.decode(stringExtra, 0));
            intent.removeExtra("gcm.rawData64");
        }
        return this.l(this.a, intent);
    }
    
    public Task l(final Context context, final Intent intent) {
        final boolean atLeastO = PlatformVersion.isAtLeastO();
        boolean b = true;
        final boolean b2 = atLeastO && context.getApplicationInfo().targetSdkVersion >= 26;
        if ((intent.getFlags() & 0x10000000) == 0x0) {
            b = false;
        }
        if (b2 && !b) {
            return e(context, intent, b);
        }
        return Tasks.call(this.b, (Callable)new e00(context, intent)).continueWithTask(this.b, (Continuation)new f00(context, intent, b));
    }
}

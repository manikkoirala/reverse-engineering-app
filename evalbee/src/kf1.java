import android.view.View;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.widget.TextView;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class kf1
{
    public Context a;
    public y01 b;
    public String c;
    public String d;
    
    public kf1(final Context a, final String c, final String d, final y01 b) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.b();
    }
    
    public static /* synthetic */ y01 a(final kf1 kf1) {
        return kf1.b;
    }
    
    public final void b() {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(this.a, 2131951953);
        final View inflate = ((LayoutInflater)this.a.getSystemService("layout_inflater")).inflate(2131492984, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        materialAlertDialogBuilder.setTitle(2131886887);
        materialAlertDialogBuilder.setCancelable(false);
        ((TextView)inflate.findViewById(2131297222)).setText((CharSequence)this.c);
        ((TextView)inflate.findViewById(2131297252)).setText((CharSequence)this.d);
        materialAlertDialogBuilder.setPositiveButton(2131886898, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final kf1 a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                if (kf1.a(this.a) != null) {
                    kf1.a(this.a).a("WATCH_VIDEO");
                }
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.setNegativeButton(2131886886, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final kf1 a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                if (kf1.a(this.a) != null) {
                    kf1.a(this.a).a("UPGRADE");
                }
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.setNeutralButton(2131886163, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final kf1 a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.create().show();
    }
}

import android.util.Base64;
import android.util.Xml;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.xmlpull.v1.XmlPullParserException;
import android.content.res.Resources;
import org.xmlpull.v1.XmlPullParser;
import android.content.res.TypedArray;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class j70
{
    public static int a(final TypedArray typedArray, final int n) {
        return a.a(typedArray, n);
    }
    
    public static b b(final XmlPullParser xmlPullParser, final Resources resources) {
        int next;
        do {
            next = xmlPullParser.next();
        } while (next != 2 && next != 1);
        if (next == 2) {
            return d(xmlPullParser, resources);
        }
        throw new XmlPullParserException("No start tag found");
    }
    
    public static List c(final Resources resources, int i) {
        if (i == 0) {
            return Collections.emptyList();
        }
        final TypedArray obtainTypedArray = resources.obtainTypedArray(i);
        try {
            if (obtainTypedArray.length() == 0) {
                return Collections.emptyList();
            }
            final ArrayList list = new ArrayList();
            if (a(obtainTypedArray, 0) == 1) {
                int resourceId;
                for (i = 0; i < obtainTypedArray.length(); ++i) {
                    resourceId = obtainTypedArray.getResourceId(i, 0);
                    if (resourceId != 0) {
                        list.add(h(resources.getStringArray(resourceId)));
                    }
                }
            }
            else {
                list.add(h(resources.getStringArray(i)));
            }
            return list;
        }
        finally {
            obtainTypedArray.recycle();
        }
    }
    
    public static b d(final XmlPullParser xmlPullParser, final Resources resources) {
        xmlPullParser.require(2, (String)null, "font-family");
        if (xmlPullParser.getName().equals("font-family")) {
            return e(xmlPullParser, resources);
        }
        g(xmlPullParser);
        return null;
    }
    
    public static b e(final XmlPullParser xmlPullParser, final Resources resources) {
        final TypedArray obtainAttributes = resources.obtainAttributes(Xml.asAttributeSet(xmlPullParser), xb1.h);
        final String string = obtainAttributes.getString(xb1.i);
        final String string2 = obtainAttributes.getString(xb1.m);
        final String string3 = obtainAttributes.getString(xb1.n);
        final int resourceId = obtainAttributes.getResourceId(xb1.j, 0);
        final int integer = obtainAttributes.getInteger(xb1.k, 1);
        final int integer2 = obtainAttributes.getInteger(xb1.l, 500);
        final String string4 = obtainAttributes.getString(xb1.o);
        obtainAttributes.recycle();
        if (string != null && string2 != null && string3 != null) {
            while (xmlPullParser.next() != 3) {
                g(xmlPullParser);
            }
            return (b)new e(new g70(string, string2, string3, c(resources, resourceId)), integer, integer2, string4);
        }
        final ArrayList list = new ArrayList();
        while (xmlPullParser.next() != 3) {
            if (xmlPullParser.getEventType() != 2) {
                continue;
            }
            if (xmlPullParser.getName().equals("font")) {
                list.add(f(xmlPullParser, resources));
            }
            else {
                g(xmlPullParser);
            }
        }
        if (list.isEmpty()) {
            return null;
        }
        return (b)new c((d[])list.toArray(new d[0]));
    }
    
    public static d f(final XmlPullParser xmlPullParser, final Resources resources) {
        final TypedArray obtainAttributes = resources.obtainAttributes(Xml.asAttributeSet(xmlPullParser), xb1.p);
        int n = xb1.y;
        if (!obtainAttributes.hasValue(n)) {
            n = xb1.r;
        }
        final int int1 = obtainAttributes.getInt(n, 400);
        int n2 = xb1.w;
        if (!obtainAttributes.hasValue(n2)) {
            n2 = xb1.s;
        }
        final boolean b = 1 == obtainAttributes.getInt(n2, 0);
        int n3 = xb1.z;
        if (!obtainAttributes.hasValue(n3)) {
            n3 = xb1.t;
        }
        int n4 = xb1.x;
        if (!obtainAttributes.hasValue(n4)) {
            n4 = xb1.u;
        }
        final String string = obtainAttributes.getString(n4);
        final int int2 = obtainAttributes.getInt(n3, 0);
        int n5 = xb1.v;
        if (!obtainAttributes.hasValue(n5)) {
            n5 = xb1.q;
        }
        final int resourceId = obtainAttributes.getResourceId(n5, 0);
        final String string2 = obtainAttributes.getString(n5);
        obtainAttributes.recycle();
        while (xmlPullParser.next() != 3) {
            g(xmlPullParser);
        }
        return new d(string2, int1, b, string, int2, resourceId);
    }
    
    public static void g(final XmlPullParser xmlPullParser) {
        int i = 1;
        while (i > 0) {
            final int next = xmlPullParser.next();
            if (next != 2) {
                if (next != 3) {
                    continue;
                }
                --i;
            }
            else {
                ++i;
            }
        }
    }
    
    public static List h(final String[] array) {
        final ArrayList list = new ArrayList();
        for (int length = array.length, i = 0; i < length; ++i) {
            list.add(Base64.decode(array[i], 0));
        }
        return list;
    }
    
    public abstract static class a
    {
        public static int a(final TypedArray typedArray, final int n) {
            return typedArray.getType(n);
        }
    }
    
    public interface b
    {
    }
    
    public static final class c implements b
    {
        public final d[] a;
        
        public c(final d[] a) {
            this.a = a;
        }
        
        public d[] a() {
            return this.a;
        }
    }
    
    public static final class d
    {
        public final String a;
        public final int b;
        public final boolean c;
        public final String d;
        public final int e;
        public final int f;
        
        public d(final String a, final int b, final boolean c, final String d, final int e, final int f) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
            this.f = f;
        }
        
        public String a() {
            return this.a;
        }
        
        public int b() {
            return this.f;
        }
        
        public int c() {
            return this.e;
        }
        
        public String d() {
            return this.d;
        }
        
        public int e() {
            return this.b;
        }
        
        public boolean f() {
            return this.c;
        }
    }
    
    public static final class e implements b
    {
        public final g70 a;
        public final int b;
        public final int c;
        public final String d;
        
        public e(final g70 a, final int c, final int b, final String d) {
            this.a = a;
            this.c = c;
            this.b = b;
            this.d = d;
        }
        
        public int a() {
            return this.c;
        }
        
        public g70 b() {
            return this.a;
        }
        
        public String c() {
            return this.d;
        }
        
        public int d() {
            return this.b;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

public final class pw0 extends kw0
{
    public static final pw0 c;
    
    static {
        c = new pw0();
    }
    
    public pw0() {
        super(1, 2);
    }
    
    @Override
    public void a(final ss1 ss1) {
        fg0.e((Object)ss1, "db");
        ss1.M("\n    CREATE TABLE IF NOT EXISTS `SystemIdInfo` (`work_spec_id` TEXT NOT NULL, `system_id`\n    INTEGER NOT NULL, PRIMARY KEY(`work_spec_id`), FOREIGN KEY(`work_spec_id`)\n    REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )\n    ");
        ss1.M("\n    INSERT INTO SystemIdInfo(work_spec_id, system_id)\n    SELECT work_spec_id, alarm_id AS system_id FROM alarmInfo\n    ");
        ss1.M("DROP TABLE IF EXISTS alarmInfo");
        ss1.M("\n                INSERT OR IGNORE INTO worktag(tag, work_spec_id)\n                SELECT worker_class_name AS tag, id AS work_spec_id FROM workspec\n                ");
    }
}

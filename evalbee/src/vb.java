import android.text.SpannableStringBuilder;
import java.util.Locale;

// 
// Decompiled by Procyon v0.6.0
// 

public final class vb
{
    public static final zu1 d;
    public static final String e;
    public static final String f;
    public static final vb g;
    public static final vb h;
    public final boolean a;
    public final int b;
    public final zu1 c;
    
    static {
        final zu1 zu1 = d = av1.c;
        e = Character.toString('\u200e');
        f = Character.toString('\u200f');
        g = new vb(false, 2, zu1);
        h = new vb(true, 2, zu1);
    }
    
    public vb(final boolean a, final int b, final zu1 c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public static int a(final CharSequence charSequence) {
        return new b(charSequence, false).d();
    }
    
    public static int b(final CharSequence charSequence) {
        return new b(charSequence, false).e();
    }
    
    public static vb c() {
        return new a().a();
    }
    
    public static boolean e(final Locale locale) {
        final int a = mv1.a(locale);
        boolean b = true;
        if (a != 1) {
            b = false;
        }
        return b;
    }
    
    public boolean d() {
        return (this.b & 0x2) != 0x0;
    }
    
    public final String f(final CharSequence charSequence, final zu1 zu1) {
        final boolean a = zu1.a(charSequence, 0, charSequence.length());
        if (!this.a && (a || b(charSequence) == 1)) {
            return vb.e;
        }
        if (this.a && (!a || b(charSequence) == -1)) {
            return vb.f;
        }
        return "";
    }
    
    public final String g(final CharSequence charSequence, final zu1 zu1) {
        final boolean a = zu1.a(charSequence, 0, charSequence.length());
        if (!this.a && (a || a(charSequence) == 1)) {
            return vb.e;
        }
        if (this.a && (!a || a(charSequence) == -1)) {
            return vb.f;
        }
        return "";
    }
    
    public CharSequence h(final CharSequence charSequence) {
        return this.i(charSequence, this.c, true);
    }
    
    public CharSequence i(final CharSequence charSequence, zu1 zu1, final boolean b) {
        if (charSequence == null) {
            return null;
        }
        final boolean a = zu1.a(charSequence, 0, charSequence.length());
        final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        if (this.d() && b) {
            if (a) {
                zu1 = av1.b;
            }
            else {
                zu1 = av1.a;
            }
            spannableStringBuilder.append((CharSequence)this.g(charSequence, zu1));
        }
        if (a != this.a) {
            char c;
            if (a) {
                c = '\u202b';
            }
            else {
                c = '\u202a';
            }
            spannableStringBuilder.append(c);
            spannableStringBuilder.append(charSequence);
            spannableStringBuilder.append('\u202c');
        }
        else {
            spannableStringBuilder.append(charSequence);
        }
        if (b) {
            if (a) {
                zu1 = av1.b;
            }
            else {
                zu1 = av1.a;
            }
            spannableStringBuilder.append((CharSequence)this.f(charSequence, zu1));
        }
        return (CharSequence)spannableStringBuilder;
    }
    
    public String j(final String s) {
        return this.k(s, this.c, true);
    }
    
    public String k(final String s, final zu1 zu1, final boolean b) {
        if (s == null) {
            return null;
        }
        return this.i(s, zu1, b).toString();
    }
    
    public static final class a
    {
        public boolean a;
        public int b;
        public zu1 c;
        
        public a() {
            this.c(vb.e(Locale.getDefault()));
        }
        
        public static vb b(final boolean b) {
            vb vb;
            if (b) {
                vb = vb.h;
            }
            else {
                vb = vb.g;
            }
            return vb;
        }
        
        public vb a() {
            if (this.b == 2 && this.c == vb.d) {
                return b(this.a);
            }
            return new vb(this.a, this.b, this.c);
        }
        
        public final void c(final boolean a) {
            this.a = a;
            this.c = vb.d;
            this.b = 2;
        }
    }
    
    public static class b
    {
        public static final byte[] f;
        public final CharSequence a;
        public final boolean b;
        public final int c;
        public int d;
        public char e;
        
        static {
            f = new byte[1792];
            for (int i = 0; i < 1792; ++i) {
                b.f[i] = Character.getDirectionality(i);
            }
        }
        
        public b(final CharSequence a, final boolean b) {
            this.a = a;
            this.b = b;
            this.c = a.length();
        }
        
        public static byte c(final char ch) {
            byte directionality;
            if (ch < '\u0700') {
                directionality = b.f[ch];
            }
            else {
                directionality = Character.getDirectionality(ch);
            }
            return directionality;
        }
        
        public byte a() {
            final char char1 = this.a.charAt(this.d - 1);
            this.e = char1;
            if (Character.isLowSurrogate(char1)) {
                final int codePointBefore = Character.codePointBefore(this.a, this.d);
                this.d -= Character.charCount(codePointBefore);
                return Character.getDirectionality(codePointBefore);
            }
            --this.d;
            byte b = c(this.e);
            if (this.b) {
                final char e = this.e;
                if (e == '>') {
                    b = this.h();
                }
                else {
                    b = b;
                    if (e == ';') {
                        b = this.f();
                    }
                }
            }
            return b;
        }
        
        public byte b() {
            final char char1 = this.a.charAt(this.d);
            this.e = char1;
            if (Character.isHighSurrogate(char1)) {
                final int codePoint = Character.codePointAt(this.a, this.d);
                this.d += Character.charCount(codePoint);
                return Character.getDirectionality(codePoint);
            }
            ++this.d;
            byte b = c(this.e);
            if (this.b) {
                final char e = this.e;
                if (e == '<') {
                    b = this.i();
                }
                else {
                    b = b;
                    if (e == '&') {
                        b = this.g();
                    }
                }
            }
            return b;
        }
        
        public int d() {
            this.d = 0;
            int n = 0;
            int n3;
            int n2 = n3 = 0;
            while (this.d < this.c && n == 0) {
                final byte b = this.b();
                if (b != 0) {
                    if (b != 1 && b != 2) {
                        if (b == 9) {
                            continue;
                        }
                        switch (b) {
                            case 18: {
                                --n3;
                                n2 = 0;
                                continue;
                            }
                            case 16:
                            case 17: {
                                ++n3;
                                n2 = 1;
                                continue;
                            }
                            case 14:
                            case 15: {
                                ++n3;
                                n2 = -1;
                                continue;
                            }
                        }
                    }
                    else if (n3 == 0) {
                        return 1;
                    }
                }
                else if (n3 == 0) {
                    return -1;
                }
                n = n3;
            }
            if (n == 0) {
                return 0;
            }
            if (n2 != 0) {
                return n2;
            }
            while (this.d > 0) {
                switch (this.a()) {
                    default: {
                        continue;
                    }
                    case 18: {
                        ++n3;
                        continue;
                    }
                    case 16:
                    case 17: {
                        if (n == n3) {
                            return 1;
                        }
                        break;
                    }
                    case 14:
                    case 15: {
                        if (n == n3) {
                            return -1;
                        }
                        break;
                    }
                }
                --n3;
            }
            return 0;
        }
        
        public int e() {
            this.d = this.c;
            int n = 0;
            while (true) {
                int n3;
                final int n2 = n3 = n;
                int n4 = 0;
            Label_0156:
                while (true) {
                    n4 = n3;
                    if (this.d <= 0) {
                        return 0;
                    }
                    final byte a = this.a();
                    if (a != 0) {
                        if (a != 1 && a != 2) {
                            n3 = n4;
                            if (a == 9) {
                                continue;
                            }
                            switch (a) {
                                default: {
                                    n3 = n4;
                                    if (n2 == 0) {
                                        break Label_0156;
                                    }
                                    continue;
                                }
                                case 18: {
                                    n3 = n4 + 1;
                                    continue;
                                }
                                case 16:
                                case 17: {
                                    if (n2 == n4) {
                                        return 1;
                                    }
                                    break;
                                }
                                case 14:
                                case 15: {
                                    if (n2 == n4) {
                                        return -1;
                                    }
                                    break;
                                }
                            }
                            n3 = n4 - 1;
                        }
                        else {
                            if (n4 == 0) {
                                return 1;
                            }
                            n3 = n4;
                            if (n2 == 0) {
                                break;
                            }
                            continue;
                        }
                    }
                    else {
                        if (n4 == 0) {
                            return -1;
                        }
                        n3 = n4;
                        if (n2 == 0) {
                            break;
                        }
                        continue;
                    }
                }
                n = n4;
            }
        }
        
        public final byte f() {
            final int d = this.d;
            char char1;
            do {
                int d2 = this.d;
                if (d2 <= 0) {
                    break;
                }
                final CharSequence a = this.a;
                --d2;
                this.d = d2;
                char1 = a.charAt(d2);
                if ((this.e = char1) == '&') {
                    return 12;
                }
            } while (char1 != ';');
            this.d = d;
            this.e = ';';
            return 13;
        }
        
        public final byte g() {
            int d;
            CharSequence a;
            do {
                d = this.d;
                if (d >= this.c) {
                    break;
                }
                a = this.a;
                this.d = d + 1;
            } while ((this.e = a.charAt(d)) != ';');
            return 12;
        }
        
        public final byte h() {
            final int d = this.d;
            while (true) {
                int d2 = this.d;
                if (d2 <= 0) {
                    break;
                }
                final CharSequence a = this.a;
                --d2;
                this.d = d2;
                final char char1 = a.charAt(d2);
                if ((this.e = char1) == '<') {
                    return 12;
                }
                if (char1 == '>') {
                    break;
                }
                if (char1 != '\"' && char1 != '\'') {
                    continue;
                }
                int d3;
                CharSequence a2;
                do {
                    d3 = this.d;
                    if (d3 <= 0) {
                        break;
                    }
                    a2 = this.a;
                    --d3;
                    this.d = d3;
                } while ((this.e = a2.charAt(d3)) != char1);
            }
            this.d = d;
            this.e = '>';
            return 13;
        }
        
        public final byte i() {
            final int d = this.d;
            while (true) {
                final int d2 = this.d;
                if (d2 >= this.c) {
                    this.d = d;
                    this.e = '<';
                    return 13;
                }
                final CharSequence a = this.a;
                this.d = d2 + 1;
                final char char1 = a.charAt(d2);
                if ((this.e = char1) == '>') {
                    return 12;
                }
                if (char1 != '\"' && char1 != '\'') {
                    continue;
                }
                int d3;
                CharSequence a2;
                do {
                    d3 = this.d;
                    if (d3 >= this.c) {
                        break;
                    }
                    a2 = this.a;
                    this.d = d3 + 1;
                } while ((this.e = a2.charAt(d3)) != char1);
            }
        }
    }
}

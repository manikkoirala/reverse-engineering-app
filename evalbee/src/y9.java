import android.os.SystemClock;
import android.os.Looper;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.Executor;
import android.os.Handler;

// 
// Decompiled by Procyon v0.6.0
// 

public final class y9
{
    public static final a m;
    public ts1 a;
    public final Handler b;
    public Runnable c;
    public final Object d;
    public long e;
    public final Executor f;
    public int g;
    public long h;
    public ss1 i;
    public boolean j;
    public final Runnable k;
    public final Runnable l;
    
    static {
        m = new a(null);
    }
    
    public y9(final long duration, final TimeUnit timeUnit, final Executor f) {
        fg0.e((Object)timeUnit, "autoCloseTimeUnit");
        fg0.e((Object)f, "autoCloseExecutor");
        this.b = new Handler(Looper.getMainLooper());
        this.d = new Object();
        this.e = timeUnit.toMillis(duration);
        this.f = f;
        this.h = SystemClock.uptimeMillis();
        this.k = new w9(this);
        this.l = new x9(this);
    }
    
    public static final void c(final y9 y9) {
        fg0.e((Object)y9, "this$0");
        synchronized (y9.d) {
            if (SystemClock.uptimeMillis() - y9.h < y9.e) {
                return;
            }
            if (y9.g != 0) {
                return;
            }
            final Runnable c = y9.c;
            u02 a;
            if (c != null) {
                c.run();
                a = u02.a;
            }
            else {
                a = null;
            }
            if (a != null) {
                final ss1 i = y9.i;
                if (i != null && i.isOpen()) {
                    i.close();
                }
                y9.i = null;
                final u02 a2 = u02.a;
                return;
            }
            throw new IllegalStateException("onAutoCloseCallback is null but it should have been set before use. Please file a bug against Room at: https://issuetracker.google.com/issues/new?component=413107&template=1096568".toString());
        }
    }
    
    public static final void f(final y9 y9) {
        fg0.e((Object)y9, "this$0");
        y9.f.execute(y9.l);
    }
    
    public final void d() {
        synchronized (this.d) {
            this.j = true;
            try (final ss1 i = this.i) {}
            this.i = null;
            final u02 a = u02.a;
        }
    }
    
    public final void e() {
        synchronized (this.d) {
            final int g = this.g;
            if (g > 0) {
                if ((this.g = g - 1) == 0) {
                    if (this.i == null) {
                        return;
                    }
                    this.b.postDelayed(this.k, this.e);
                }
                final u02 a = u02.a;
                return;
            }
            throw new IllegalStateException("ref count is 0 or lower but we're supposed to decrement".toString());
        }
    }
    
    public final Object g(final c90 c90) {
        fg0.e((Object)c90, "block");
        try {
            return c90.invoke((Object)this.j());
        }
        finally {
            this.e();
        }
    }
    
    public final ss1 h() {
        return this.i;
    }
    
    public final ts1 i() {
        final ts1 a = this.a;
        if (a != null) {
            return a;
        }
        fg0.t("delegateOpenHelper");
        return null;
    }
    
    public final ss1 j() {
        synchronized (this.d) {
            this.b.removeCallbacks(this.k);
            ++this.g;
            if (!(this.j ^ true)) {
                throw new IllegalStateException("Attempting to open already closed database.".toString());
            }
            final ss1 i = this.i;
            if (i != null && i.isOpen()) {
                return i;
            }
            return this.i = this.i().F();
        }
    }
    
    public final void k(final ts1 ts1) {
        fg0.e((Object)ts1, "delegateOpenHelper");
        this.n(ts1);
    }
    
    public final boolean l() {
        return this.j ^ true;
    }
    
    public final void m(final Runnable c) {
        fg0.e((Object)c, "onAutoClose");
        this.c = c;
    }
    
    public final void n(final ts1 a) {
        fg0.e((Object)a, "<set-?>");
        this.a = a;
    }
    
    public static final class a
    {
    }
}

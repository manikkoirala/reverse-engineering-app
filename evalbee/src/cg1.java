import java.util.concurrent.Callable;
import java.io.File;

// 
// Decompiled by Procyon v0.6.0
// 

public final class cg1 implements c
{
    public final String a;
    public final File b;
    public final Callable c;
    public final c d;
    
    public cg1(final String a, final File b, final Callable c, final c d) {
        fg0.e((Object)d, "mDelegate");
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
    
    @Override
    public ts1 a(final ts1.b b) {
        fg0.e((Object)b, "configuration");
        return new bg1(b.a, this.a, this.b, this.c, b.c.a, this.d.a(b));
    }
}

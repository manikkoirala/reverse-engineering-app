import android.graphics.Typeface;
import android.os.Handler;

// 
// Decompiled by Procyon v0.6.0
// 

public class re
{
    public final k70.c a;
    public final Handler b;
    
    public re(final k70.c a, final Handler b) {
        this.a = a;
        this.b = b;
    }
    
    public final void a(final int n) {
        this.b.post((Runnable)new Runnable(this, this.a, n) {
            public final k70.c a;
            public final int b;
            public final re c;
            
            @Override
            public void run() {
                this.a.a(this.b);
            }
        });
    }
    
    public void b(final i70.e e) {
        if (e.a()) {
            this.c(e.a);
        }
        else {
            this.a(e.b);
        }
    }
    
    public final void c(final Typeface typeface) {
        this.b.post((Runnable)new Runnable(this, this.a, typeface) {
            public final k70.c a;
            public final Typeface b;
            public final re c;
            
            @Override
            public void run() {
                this.a.b(this.b);
            }
        });
    }
}

import java.util.Objects;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.io.Writer;

// 
// Decompiled by Procyon v0.6.0
// 

public final class th0 extends vh0
{
    public static final Writer q;
    public static final qh0 t;
    public final List m;
    public String n;
    public nh0 p;
    
    static {
        q = new Writer() {
            @Override
            public void close() {
                throw new AssertionError();
            }
            
            @Override
            public void flush() {
                throw new AssertionError();
            }
            
            @Override
            public void write(final char[] array, final int n, final int n2) {
                throw new AssertionError();
            }
        };
        t = new qh0("closed");
    }
    
    public th0() {
        super(th0.q);
        this.m = new ArrayList();
        this.p = oh0.a;
    }
    
    @Override
    public vh0 c() {
        final ih0 ih0 = new ih0();
        this.w0(ih0);
        this.m.add(ih0);
        return this;
    }
    
    @Override
    public void close() {
        if (this.m.isEmpty()) {
            this.m.add(th0.t);
            return;
        }
        throw new IOException("Incomplete document");
    }
    
    @Override
    public vh0 d() {
        final ph0 ph0 = new ph0();
        this.w0(ph0);
        this.m.add(ph0);
        return this;
    }
    
    @Override
    public vh0 f() {
        if (this.m.isEmpty() || this.n != null) {
            throw new IllegalStateException();
        }
        if (this.v0() instanceof ih0) {
            final List m = this.m;
            m.remove(m.size() - 1);
            return this;
        }
        throw new IllegalStateException();
    }
    
    @Override
    public void flush() {
    }
    
    @Override
    public vh0 g() {
        if (this.m.isEmpty() || this.n != null) {
            throw new IllegalStateException();
        }
        if (this.v0() instanceof ph0) {
            final List m = this.m;
            m.remove(m.size() - 1);
            return this;
        }
        throw new IllegalStateException();
    }
    
    @Override
    public vh0 m0(final double n) {
        if (!this.j() && (Double.isNaN(n) || Double.isInfinite(n))) {
            final StringBuilder sb = new StringBuilder();
            sb.append("JSON forbids NaN and infinities: ");
            sb.append(n);
            throw new IllegalArgumentException(sb.toString());
        }
        this.w0(new qh0(n));
        return this;
    }
    
    @Override
    public vh0 o(final String s) {
        Objects.requireNonNull(s, "name == null");
        if (this.m.isEmpty() || this.n != null) {
            throw new IllegalStateException();
        }
        if (this.v0() instanceof ph0) {
            this.n = s;
            return this;
        }
        throw new IllegalStateException();
    }
    
    @Override
    public vh0 o0(final long l) {
        this.w0(new qh0(l));
        return this;
    }
    
    @Override
    public vh0 p0(final Boolean b) {
        if (b == null) {
            return this.u();
        }
        this.w0(new qh0(b));
        return this;
    }
    
    @Override
    public vh0 q0(final Number obj) {
        if (obj == null) {
            return this.u();
        }
        if (!this.j()) {
            final double doubleValue = obj.doubleValue();
            if (Double.isNaN(doubleValue) || Double.isInfinite(doubleValue)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("JSON forbids NaN and infinities: ");
                sb.append(obj);
                throw new IllegalArgumentException(sb.toString());
            }
        }
        this.w0(new qh0(obj));
        return this;
    }
    
    @Override
    public vh0 r0(final String s) {
        if (s == null) {
            return this.u();
        }
        this.w0(new qh0(s));
        return this;
    }
    
    @Override
    public vh0 s0(final boolean b) {
        this.w0(new qh0(b));
        return this;
    }
    
    @Override
    public vh0 u() {
        this.w0(oh0.a);
        return this;
    }
    
    public nh0 u0() {
        if (this.m.isEmpty()) {
            return this.p;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Expected one JSON element but was ");
        sb.append(this.m);
        throw new IllegalStateException(sb.toString());
    }
    
    public final nh0 v0() {
        final List m = this.m;
        return (nh0)m.get(m.size() - 1);
    }
    
    public final void w0(final nh0 p) {
        if (this.n != null) {
            if (!p.i() || this.h()) {
                ((ph0)this.v0()).n(this.n, p);
            }
            this.n = null;
        }
        else if (this.m.isEmpty()) {
            this.p = p;
        }
        else {
            final nh0 v0 = this.v0();
            if (!(v0 instanceof ih0)) {
                throw new IllegalStateException();
            }
            ((ih0)v0).n(p);
        }
    }
}
